﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Multialmacenes.Datos;


/// <summary>
/// Summary description for Cls_Cat_Alm_Multialmacenes_Negocio
/// </summary>
/// 
namespace JAPAMI.Multialmacenes.Negocio
{
    public class Cls_Cat_Alm_Multialmacenes_Negocio
    {
        ///*******************************************************************************
        /// VARIABLES INTERNAS 
        ///******************************************************************************
        #region Variables_Internas
        private String Almacen_ID;
        private String Nombre;

        private String Descripcion;
        private String Responsable_ID;
        private String No_Empleado;
        private String Nombre_Empleado;


        private String Apellido_Paterno;
        private String Apellido_Materno;


        #endregion

        ///*******************************************************************************
        /// VARIABLES PUBLICAS
        ///*******************************************************************************
        #region Variables_Publicas


        public String P_Almacen_ID
        {
            get { return Almacen_ID; }
            set { Almacen_ID = value; }
        }
        
        public String P_Nombre
        {
            get { return Nombre; }
            set { Nombre = value; }
        }

        public String P_Descripcion
        {
            get { return Descripcion; }
            set { Descripcion = value; }
        }

        public String P_Responsable_ID
        {
            get { return Responsable_ID; }
            set { Responsable_ID = value; }
        }

        public String P_No_Empleado
        {
            get { return No_Empleado; }
            set { No_Empleado = value; }
        }

        public String P_Nombre_Empleado
        {
            get { return Nombre_Empleado; }
            set { Nombre_Empleado = value; }
        }

        public String P_Apellido_Paterno
        {
            get { return Apellido_Paterno; }
            set { Apellido_Paterno = value; }
        }
        
        public String P_Apellido_Materno
        {
            get { return Apellido_Materno; }
            set { Apellido_Materno = value; }
        }

 
        #endregion

        public DataTable Consulta_Multialmacenes()
        {
            return Cls_Cat_Alm_Multialmacenes_Datos.Consulta_Multialmacenes(this);
        }

        public DataTable Consultar_Empleado()
        {
            return Cls_Cat_Alm_Multialmacenes_Datos.Consultar_Empleado(this);
        }

        public String Alta_Multialmacen()
        {
            return Cls_Cat_Alm_Multialmacenes_Datos.Alta_Multialmacen(this);
        }

        public String Modificar_Multialmacenes()
        {
            return Cls_Cat_Alm_Multialmacenes_Datos.Modificar_Multialmacenes(this);
        }
    }
}//fin del namespace