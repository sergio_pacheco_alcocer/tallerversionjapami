﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Salidas_Multialmacenes.Datos;
/// <summary>
/// Summary description for Cls_Ope_Alm_Salidas_Multi_Negocio
/// </summary>
/// 
namespace JAPAMI.Salidas_Multialmacenes.Negocios
{
    public class Cls_Ope_Alm_Salidas_Multi_Negocio
    {


        ///*******************************************************************************
        /// VARIABLES INTERNAS 
        ///******************************************************************************
        #region Variables_Internas
        private String No_Salida;
        private String Almacen_ID;
        private String Estatus;
        private String Empleado_Recibio_ID;
        private String Empleado_Almacen_ID;

       
        private String Fecha_Hora;
        private String IVA;
        private String Subtotal;
        private String Total;
        private DataTable Dt_Productos;

        private String No_Empleado;
        private String Nombre_Empleado;
        private String Apellido_Paterno;
        private String Apellido_Materno;

        private String Nombre_Producto;
        private String Producto_ID;

       
        

        #endregion

        ///*******************************************************************************
        /// VARIABLES PUBLICAS
        ///*******************************************************************************
        #region Variables_Publicas

        public String P_No_Salida
        {
            get { return No_Salida; }
            set { No_Salida = value; }
        }

        public String P_Almacen_ID
        {
            get { return Almacen_ID; }
            set { Almacen_ID = value; }
        }

        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }

        public String P_Empleado_Recibio_ID
        {
            get { return Empleado_Recibio_ID; }
            set { Empleado_Recibio_ID = value; }
        }

        public String P_Fecha_Hora
        {
            get { return Fecha_Hora; }
            set { Fecha_Hora = value; }
        }

        public String P_IVA
        {
            get { return IVA; }
            set { IVA = value; }
        }

        public String P_Subtotal
        {
            get { return Subtotal; }
            set { Subtotal = value; }
        }

        public String P_Total
        {
            get { return Total; }
            set { Total = value; }
        }

        public DataTable P_Dt_Productos
        {
            get { return Dt_Productos; }
            set { Dt_Productos = value; }
        }

        public String P_Empleado_Almacen_ID
        {
            get { return Empleado_Almacen_ID; }
            set { Empleado_Almacen_ID = value; }
        }

        public String P_Nombre_Producto
        {
            get { return Nombre_Producto; }
            set { Nombre_Producto = value; }
        }

        public String P_Producto_ID
        {
            get { return Producto_ID; }
            set { Producto_ID = value; }
        }

        public String P_No_Empleado
        {
            get { return No_Empleado; }
            set { No_Empleado = value; }
        }

        public String P_Nombre_Empleado
        {
            get { return Nombre_Empleado; }
            set { Nombre_Empleado = value; }
        }

        public String P_Apellido_Paterno
        {
            get { return Apellido_Paterno; }
            set { Apellido_Paterno = value; }
        }

        public String P_Apellido_Materno
        {
            get { return Apellido_Materno; }
            set { Apellido_Materno = value; }
        }
        #endregion


        public DataTable Consultar_Productos()
        {
            return Cls_Ope_Alm_Salidas_Multi_Datos.Consultar_Productos(this);
        }

        public DataTable Consultar_Empleado()
        {
            return Cls_Ope_Alm_Salidas_Multi_Datos.Consultar_Empleado(this);
        }

        public DataTable Consultar_Almacen_Logueado()
        {
            return Cls_Ope_Alm_Salidas_Multi_Datos.Consultar_Almacen_Logueado();
        }

        public String Alta_Salidas()
        {
            return Cls_Ope_Alm_Salidas_Multi_Datos.Alta_Salidas(this);
        }

        public DataTable Consultar_Salidas()
        {
            return Cls_Ope_Alm_Salidas_Multi_Datos.Consultar_Salidas(this);

        }

        public DataTable Consultar_Detalle_Salidas()
        {
            return Cls_Ope_Alm_Salidas_Multi_Datos.Consultar_Detalle_Salidas(this);

        }

        
    }
}