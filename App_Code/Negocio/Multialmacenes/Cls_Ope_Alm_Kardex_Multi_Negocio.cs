﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Kardex_Multialmacen.Datos;

/// <summary>
/// Summary description for Cls_Ope_Alm_Kardex_Multi_Negocio
/// </summary>
/// 
namespace JAPAMI.Kardex_Multialmacen.Negocio
{
    public class Cls_Ope_Alm_Kardex_Multi_Negocio
    {
        ///*******************************************************************************
        /// VARIABLES INTERNAS 
        ///******************************************************************************
        #region Variables_Internas
        private String Clave;
        private String Fecha_Inicio;
        private String Fecha_Final;
        private String Almacen_ID;
        private bool Todos_Productos;

     

        #endregion

        ///*******************************************************************************
        /// VARIABLES PUBLICAS
        ///*******************************************************************************
        #region Variables_Publicas
        
        public String P_Clave
        {
            get { return Clave; }
            set { Clave = value; }
        }

        public String P_Fecha_Inicio
        {
            get { return Fecha_Inicio; }
            set { Fecha_Inicio = value; }
        }

        public String P_Fecha_Final
        {
            get { return Fecha_Final; }
            set { Fecha_Final = value; }
        }

        public String P_Almacen_ID
        {
            get { return Almacen_ID; }
            set { Almacen_ID = value; }
        }

        public bool P_Todos_Productos
        {
            get { return Todos_Productos; }
            set { Todos_Productos = value; }
        }
        #endregion


        ///*******************************************************************************
        /// VARIABLES Metodos
        ///*******************************************************************************
        #region Metodos
        
        public DataTable Consultar_Almacenes()
        {
            return Cls_Ope_Alm_Kardex_Multi_Datos.Consultar_Almacenes();
        }

        public DataTable Consultar_Productos()
        {
            return Cls_Ope_Alm_Kardex_Multi_Datos.Consultar_Productos(this);
        }
        
        public DataTable Consultar_Traspasos()
        {
            return Cls_Ope_Alm_Kardex_Multi_Datos.Consultar_Traspasos(this);
        }

        public DataTable Consultar_Salidas()
        {
            return Cls_Ope_Alm_Kardex_Multi_Datos.Consultar_Salidas(this);
        }

        #endregion
    }
}