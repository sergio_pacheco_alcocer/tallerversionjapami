﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Catalogo_Unidades_Responsables.Datos;

namespace JAPAMI.Catalogo_Unidades_Responsables.Negocio
{
    public class Cls_Cat_Unidades_Responsables_Negocio
    {
        public Cls_Cat_Unidades_Responsables_Negocio()
        {
        }

        #region (Variables Locales)
        private String Unidad_Responsable_ID;
        private String Nombre;
        private String Estatus;
        private String Comentarios;
        private String Usuario;
        private String Area_Funcional_ID;
        private String Clave;
        private String Grupo_Dependencia_ID;
        private String Busqueda;
        private String Tipo_Consulta;
        private String Clave_Contabilidad;

       
        #endregion

        #region (Variables Publicas)

        public String P_Clave_Contabilidad
        {
            get { return Clave_Contabilidad; }
            set { Clave_Contabilidad = value; }
        }

        public String P_Unidad_Responsable_ID
        {
            get { return Unidad_Responsable_ID; }
            set { Unidad_Responsable_ID = value; }
        }

        public String P_Nombre
        {
            get { return Nombre; }
            set { Nombre = value; }
        }

        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }

        public String P_Comentarios
        {
            get { return Comentarios; }
            set { Comentarios = value; }
        }

        public String P_Usuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }

        public String P_Area_Funcional_ID
        {
            get { return Area_Funcional_ID; }
            set { Area_Funcional_ID = value; }
        }

        public String P_Clave
        {
            get { return Clave; }
            set { Clave = value; }
        }

        public String P_Grupo_Dependencia_ID
        {
            get { return Grupo_Dependencia_ID; }
            set { Grupo_Dependencia_ID = value; }
        }

        public String P_Busqueda
        {
            get { return Busqueda; }
            set { Busqueda = value; }
        }

        public String P_Tipo_Consulta
        {
            get { return Tipo_Consulta; }
            set { Tipo_Consulta = value; }
        }
        #endregion

        #region (Metodos)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:   Alta_Unidad_Responsable
        /// DESCRIPCION :           Dar de alta una unidad responsable
        /// PARAMETROS  :           
        /// CREO        :           Noe Mosqueda Valadez
        /// FECHA_CREO  :           18/Junio/2012 14:00
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public void Alta_Unidad_Responsable()
        {
            Cls_Cat_Unidades_Responsables_Datos.Alta_Unidad_Responsable(this);
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:   Baja_Unidad_Responsable
        /// DESCRIPCION :           Dar de baja una unidad responsable
        /// PARAMETROS  :           
        /// CREO        :           Noe Mosqueda Valadez
        /// FECHA_CREO  :           18/Junio/2012 14:00
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public void Baja_Unidad_Responsable()
        {
            Cls_Cat_Unidades_Responsables_Datos.Baja_Unidad_Responsable(this);
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:   Cambio_Unidad_Responsable
        /// DESCRIPCION :           Modificar una unidad responsable
        /// PARAMETROS  :           
        /// CREO        :           Noe Mosqueda Valadez
        /// FECHA_CREO  :           18/Junio/2012 19:00
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public void Cambio_Unidad_Responsable()
        {
            Cls_Cat_Unidades_Responsables_Datos.Cambio_Unidad_Responsable(this);
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:   Valida_Unidad_Responsable
        /// DESCRIPCION :           Validar si una unidad responsable ya existe
        /// PARAMETROS  :           
        /// CREO        :           Noe Mosqueda Valadez
        /// FECHA_CREO  :           18/Junio/2012 19:00
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public String Valida_Unidad_Responsable()
        {
            return Cls_Cat_Unidades_Responsables_Datos.Valida_Unidad_Responsable(this);
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:   Consulta_Unidades_Responsables
        /// DESCRIPCION :           Consultar las unidades responsables
        /// PARAMETROS  :           
        /// CREO        :           Noe Mosqueda Valadez
        /// FECHA_CREO  :           18/Junio/2012 19:00
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public DataTable Consulta_Unidades_Responsables()
        {
            return Cls_Cat_Unidades_Responsables_Datos.Consulta_Unidades_Responsables(this);
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION : Consultar_Area_Funcional
        /// DESCRIPCION          : Metodo para obtener las areas funcionales del catalogo
        /// PARAMETROS           :           
        /// CREO                 : Leslie González Vázquez
        /// FECHA_CREO           : 05/Agosto/2013
        ///*******************************************************************************
        public DataTable Consultar_Area_Funcional() 
        {
            return Cls_Cat_Unidades_Responsables_Datos.Obtener_Area_Funcional();
        }
        #endregion
    }
}