﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Alertas.Datos;


/// <summary>
/// Summary description for Cls_Ope_Alertas_Sistema_Negocio
/// </summary>
/// 
namespace JAPAMI.Alertas.Negocio
{
    public class Cls_Ope_Alertas_Sistema_Negocio
    {
        ///*******************************************************************************
        /// VARIABLES INTERNAS 
        ///******************************************************************************
        #region Variables_Internas


        #endregion

        ///*******************************************************************************
        /// VARIABLES PUBLICAS
        ///*******************************************************************************
        #region Variables_Publicas

        #endregion

        ///*******************************************************************************
        /// METODOS
        ///*******************************************************************************
        #region Metodos

        public DataTable Consultar_Requisiciones_Entregadas()
        {
            return Cls_Ope_Alertas_Sistema_Datos.Consultar_Requisiciones_Entregadas();
        }



        #endregion


    }
}