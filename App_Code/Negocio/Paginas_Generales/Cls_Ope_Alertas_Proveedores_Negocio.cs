﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Alertas_Proveedores.Datos;
using System.Data;

/// <summary>
/// Summary description for Cls_Ope_Alertas_Proveedores_Negocio
/// </summary>
namespace JAPAMI.Alertas_Proveedores.Negocio
{
    public class Cls_Ope_Alertas_Proveedores_Negocio
    {
        ///*******************************************************************************
        /// VARIABLES INTERNAS 
        ///******************************************************************************
        #region Variables_Internas
        private String Proveedor_ID;

        #endregion

        ///*******************************************************************************
        /// VARIABLES PUBLICAS
        ///*******************************************************************************
        #region Variables_Publicas

        #endregion

        ///*******************************************************************************
        /// METODOS
        ///*******************************************************************************
        #region Metodos

        public DataTable Consultar_Ordenes_Asignadas()
        {
            return Cls_Ope_Alertas_Proveedores_Datos.Consultar_Ordenes_Asignadas(this);
        }
        #endregion
    }
}