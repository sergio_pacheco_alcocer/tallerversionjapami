﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Bitacora_Eventos;
using JAPAMI.Constantes;
using JAPAMI.Correo.Datos;
using SharpContent.ApplicationBlocks.Data;

namespace JAPAMI.Correo.Negocio
{
    public class Cls_Cat_Com_Correo_Negocio
    {
        #region (Variables Locales)
        
        private String Correo;
        private String Password;
        private String Direccion_IP;
        private String Ruta_Compras;
        private String Ruta_Gerencia;
        private String Campo;
        DataTable Dt_Correo;

        #endregion

        #region Variables Internas
        
        public String P_Password
        {
            get { return Password; }
            set { Password = value; }
        }        

        public String P_Correo
        {
            get { return Correo; }
            set { Correo = value; }
        }
        public String P_Direccion_IP
        {
            get { return Direccion_IP; }
            set { Direccion_IP = value; }
        }
        public String P_Ruta_Compras
        {
            get { return Ruta_Compras; }
            set { Ruta_Compras = value; }
        }
        public String P_Ruta_Gerencia
        {
            get { return Ruta_Gerencia; }
            set { Ruta_Gerencia= value; }
        }
        public String P_Campo
        {
            get { return Campo; }
            set { Campo = value; }
        }

        #endregion

        #region Metodos

            public Cls_Cat_Com_Correo_Negocio()
            {
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consulta_Correo
            ///DESCRIPCIÓN: Metodo que obtiene una dataset de acuerdo a los datos a modificar o modificados
            ///PARAMETROS: 
            ///CREO: David Herrera Rincon
            ///FECHA_CREO: 16/Enero/2013
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************         
            public DataTable Consulta_Correo()
            {
                return Cls_Cat_Com_Correo_Datos.Consultar_Correo(this);
            }              
            
            //*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Alta_Correo
            ///DESCRIPCIÓN: Llama a la clase de datos que se encarga de la conexion a la base de datos enviando 
            ///un objeto de esta clase para sacar sus valores
            ///PARAMETROS:
            ///CREO: David Herrera Rincon
            ///FECHA_CREO: 16/Enero/2013
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            public void Alta_Correo()
            {
                Cls_Cat_Com_Correo_Datos.Alta_Correo(this);
            }
            
            //*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Modificar_Correo
            ///DESCRIPCIÓN: Llama a la clase de datos que se encarga de la conexion a la base 
            ///de datos enviando 
            ///un objeto de esta clase para sacar sus valores
            ///PARAMETROS:
            ///CREO: David Herrera Rincon
            ///FECHA_CREO: 16/Enero/2013
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            public void Modificar_Correo()
            {
                Cls_Cat_Com_Correo_Datos.Modificar_Correo(this);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Imagenes
            ///DESCRIPCIÓN: Metodo que obtiene una dataset de acuerdo a los datos a modificar o modificados
            ///PARAMETROS: 
            ///CREO: David Herrera Rincon
            ///FECHA_CREO: 16/Enero/2013
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************         
            public DataTable Consultar_Imagenes()
            {
                return Cls_Cat_Com_Correo_Datos.Consultar_Imagenes(this);
            }

            //*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Alta_Imagen
            ///DESCRIPCIÓN: Llama a la clase de datos que se encarga de la conexion a la base de datos enviando 
            ///un objeto de esta clase para sacar sus valores
            ///PARAMETROS:
            ///CREO: David Herrera Rincon
            ///FECHA_CREO: 17/Enero/2013
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            public void Alta_Imagen()
            {
                Cls_Cat_Com_Correo_Datos.Alta_Imagen(this);
            }

            //*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Modificar_Imagen
            ///DESCRIPCIÓN: Llama a la clase de datos que se encarga de la conexion a la base 
            ///de datos enviando 
            ///un objeto de esta clase para sacar sus valores
            ///PARAMETROS:
            ///CREO: David Herrera Rincon
            ///FECHA_CREO: 17/Enero/2013
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            public void Modificar_Imagen()
            {
                Cls_Cat_Com_Correo_Datos.Modificar_Imagen(this);
            }
        #endregion
    }
}