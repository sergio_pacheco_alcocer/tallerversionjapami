﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Actualizar_Proveedores.Datos;



/// <summary>
/// Summary description for Cls_Cat_Com_Actualizar_Proveedores_Negocio
/// </summary>
/// 
namespace JAPAMI.Actualizar_Proveedores.Negocio
{
    public class Cls_Cat_Com_Actualizar_Proveedores_Negocio
    {
        public Cls_Cat_Com_Actualizar_Proveedores_Negocio()
        {

        }
        #region VARIABLES LOCALES
        private String Proveedor_ID;
        private String Nombre;
        private String Compania;
        private String Fecha_Actualizacion;
        private String Fecha_Registro;
        private String RFC;
        private String Contacto;
       
        private String Partida_Clave;
        private String Concepto_ID;

        private DataTable Dt_Acualizar;
        private DataTable Dt_Insertar;
        private DataTable Dt_Partidas;
        private DataTable Dt_Conceptos;


        #endregion

        #region VARIBLES PUBLICAS
        public String P_Proveedor_ID
        {
            get { return Proveedor_ID; }
            set { Proveedor_ID = value; }
        }
        public String P_Nombre
        {
            get { return Nombre; }
            set { Nombre = value; }
        }
        public String P_Compania
        {
            get { return Compania; }
            set { Compania = value; }
        }
       
        public String P_Fecha_Registro
        {
            get { return Fecha_Registro; }
            set { Fecha_Registro = value; }
        }
        public String P_Fecha_Actualizacion
        {
            get { return Fecha_Actualizacion; }
            set { Fecha_Actualizacion = value; }
        }
        public String P_Partida_Clave
        {
            get { return Partida_Clave; }
            set { Partida_Clave = value; }
        }
        public String P_Concepto_ID
        {
            get { return Concepto_ID; }
            set { Concepto_ID = value; }
        }
        public DataTable P_Dt_Acualizar
        {
            get { return Dt_Acualizar; }
            set { Dt_Acualizar = value; }
        }
        public DataTable P_Dt_Insertar
        {
            get { return Dt_Insertar; }
            set { Dt_Insertar = value; }
        }
        public DataTable P_Dt_Partidas
        {
            get { return Dt_Partidas; }
            set { Dt_Partidas = value; }
        }
        public DataTable P_Dt_Conceptos
        {
            get { return Dt_Conceptos; }
            set { Dt_Conceptos = value; }
        }
        public String P_RFC
        {
            get { return RFC; }
            set { RFC = value; }
        }
        public String P_Contacto
        {
            get { return Contacto; }
            set { Contacto = value; }
        }
        #endregion

        #region METODOS
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Proveedor
        ///DESCRIPCIÓN          : Metodo para obtener los datos de un proveedor
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 11/Septiembre/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Proveedor()
        {
            return Cls_Cat_Com_Actializar_Proveedores_Datos.Consultar_proveedor(this);

        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Partidas
        ///DESCRIPCIÓN          : Metodo para conssultar si la relacion de partida/proveedor ya esta insertada 
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 11/Septiembre/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Detalles_Partidas()
        {
            return Cls_Cat_Com_Actializar_Proveedores_Datos.Consultar_Detalles_Partidas(this);

        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Datos_Parida
        ///DESCRIPCIÓN          : Metodo para obtener los datos de laspartidas que se van ainsertar en la tabla
        ///                       CAT_COM_DET_PART_PROV
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 11/Septiembre/2012 
        ///*********************************************************************************************************
        public DataTable Obtener_Datos_Parida()
        {
            return Cls_Cat_Com_Actializar_Proveedores_Datos.Obtener_Datos_Parida(this);

        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos_Detalles
        ///DESCRIPCIÓN          : Metodo para consultar los conceptos que pertenecen a un proveedor
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 11/Septiembre/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Conceptos_Detalles()
        {
            return Cls_Cat_Com_Actializar_Proveedores_Datos.Consultar_Conceptos_Detalles(this);

        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Proveedores
        ///DESCRIPCIÓN          : Metodo para dar de alta y actualizar los proveedores de la graga masiva
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 13/Septiembre/2012 
        ///*********************************************************************************************************
        public String Alta_Proveedores()
        {
            return Cls_Cat_Com_Actializar_Proveedores_Datos.Alta_Proveedor(this);

        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Proveedor_Por_Datos
        ///DESCRIPCIÓN          : Metodo para consultar si un proveedor esta dado de alta si tomar en cuenta su ID
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 01/Octubre/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Proveedor_Por_Datos()
        {
            return Cls_Cat_Com_Actializar_Proveedores_Datos.Consultar_Proveedor_Datos(this);
        }
        #endregion
    }
}
