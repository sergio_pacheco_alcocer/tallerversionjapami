﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Cat_Tipos_Resguardos.Datos;
/// <summary>
/// Summary description for Cls_Cat_Com_Tipos_Resguardos
/// </summary>
namespace JAPAMI.Cat_Tipos_Resguardos.Negocio
{
    public class Cls_Cat_Com_Tipos_Resguardos_Negocio
    {
        //***************************************************
        //VARIABLES INTERNAS
        //***************************************************
        #region Variables_Internas
        private String Tipo_Resguardo_ID;
        private String Estatus;
        private String Nombre;
        private String Descripcion;
        private DataTable Dt_Tipos_Resguardos;

        #endregion 

        //***************************************************
        //VARIABLES PUBLICAS
        //***************************************************
        #region Variables_Publicas
        //public String P_Tipo_Resguardo_ID { get; set; }
        //public String P_Estatus { get; set; }
        //public String P_Nombre { get; set; }
        //public String P_Descripcion { get; set; }
        //public DataTable Dt_Tipos_Resguardos { get; set; }
        public string P_Tipo_Resguardo_ID
        {
            get { return Tipo_Resguardo_ID; }
            set { Tipo_Resguardo_ID = value; }
        }
        public string P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public string P_Nombre
        {
            get { return Nombre; }
            set { Nombre = value; }
        }
        public string P_Descripcion
        {
            get { return Descripcion; }
            set { Descripcion = value; }
        }
        public DataTable P_Dt_Tipos_Resguardos
        {
            get { return Dt_Tipos_Resguardos; }
            set { Dt_Tipos_Resguardos = value; }
        }
        #endregion

        //***************************************************
        //METODOS
        //***************************************************
        #region Metodos
        public void Alta_Tipo_Resguardo() 
        {
            Cls_Cat_Com_Tipos_Resguardos_Datos.Alta_Tipo_Resguardo(this);
        }
        public void Elimnar_Tipo_Resguardo()
        {
            Cls_Cat_Com_Tipos_Resguardos_Datos.Eliminar_Tipo_Resguardo(this);
        }
        public void Modifiacar_Tipo_resguardo()
        {
            Cls_Cat_Com_Tipos_Resguardos_Datos.Modificar_Tipo_Resguardo(this);
        }
        public DataTable Consultar_Tipo_Resguardo()
        {
            return Cls_Cat_Com_Tipos_Resguardos_Datos.Consultar_Tipo_Resguardo(this);
        }
        #endregion


    }//Fin de la clase
}//Fin del namespace
