﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Compras.Manejo_Historial_Cambios.Datos;
using JAPAMI.Seguimiento_Requisicion.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Pat_Manejo_Historial_Cambios_Negocio
/// </summary>
namespace JAPAMI.Compras.Manejo_Historial_Cambios.Negocio {
    public class Cls_Ope_Com_Manejo_Historial_Cambios_Negocio {

        #region Variables Internas

        private String No_Requisicion = String.Empty;
        private Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Cls_Negocio_Actualizado = null;
        private Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Cls_Negocio_Anterior = null;

        #endregion

        #region Variables Internas

        public String P_No_Requisicion
        {
            set { No_Requisicion = value; }
            get { return No_Requisicion; }
        }

        public Cls_Ope_Com_Seguimiento_Requisiciones_Negocio P_Cls_Negocio_Actualizado
        {
            set { Cls_Negocio_Actualizado = value; }
            get { return Cls_Negocio_Actualizado; }
        }
        public Cls_Ope_Com_Seguimiento_Requisiciones_Negocio P_Cls_Negocio_Anterior
        {
            set { Cls_Negocio_Anterior = value; }
            get { return Cls_Negocio_Anterior; }
        }

        #endregion

        #region Metodos

            public DataTable Obtener_Tabla_Cambios()
            {
                DataTable Dt_Cambios = Estructura_DataTable_Cambios();
                if (!String.IsNullOrEmpty(P_No_Requisicion)) {
                    Cls_Negocio_Anterior = new Cls_Ope_Com_Seguimiento_Requisiciones_Negocio();
                    Cls_Negocio_Anterior.P_Requisicion_ID = P_No_Requisicion.Trim();
                    Cls_Negocio_Anterior = Cls_Negocio_Anterior.Obtener_Detalles_Fechas_Requisicion();
                    if (Cls_Negocio_Anterior != null && Cls_Negocio_Actualizado != null) { 
                        //Fecha Recepción
                        if (!String.Format("{0:ddMMyyyy}", Cls_Negocio_Anterior.P_Fecha_Recepcion).Equals(String.Format("{0:ddMMyyyy}", Cls_Negocio_Actualizado.P_Fecha_Recepcion))) {
                            String Anterior = ((!String.Format("{0:ddMMyyyy}", Cls_Negocio_Anterior.P_Fecha_Recepcion).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) ? String.Format("{0:dd/MMMMMMMMMMMMMMMMMM/yyyy}", Cls_Negocio_Anterior.P_Fecha_Recepcion) : "");
                            String Actual = ((!String.Format("{0:ddMMyyyy}", Cls_Negocio_Actualizado.P_Fecha_Recepcion).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) ? String.Format("{0:dd/MMMMMMMMMMMMMMMMMM/yyyy}", Cls_Negocio_Actualizado.P_Fecha_Recepcion) : "");
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, "Fecha Recepción", Anterior, Actual);
                        }
                        //Empleado Recepción
                        if (!Cls_Negocio_Anterior.P_Empleado_Recepcion.Equals(Cls_Negocio_Actualizado.P_Empleado_Recepcion)) {
                            String Anterior = ((!String.IsNullOrEmpty(Cls_Negocio_Anterior.P_Empleado_Recepcion)) ? Cls_Negocio_Anterior.P_Empleado_Recepcion : "");
                            String Actual = ((!String.IsNullOrEmpty(Cls_Negocio_Actualizado.P_Empleado_Recepcion)) ? Cls_Negocio_Actualizado.P_Empleado_Recepcion : "");
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, "Nombre Recepción", Anterior, Actual);
                        }
                        //Fecha Envio
                        if (!String.Format("{0:ddMMyyyy}", Cls_Negocio_Anterior.P_Fecha_Envio).Equals(String.Format("{0:ddMMyyyy}", Cls_Negocio_Actualizado.P_Fecha_Envio)))
                        {
                            String Anterior = ((!String.Format("{0:ddMMyyyy}", Cls_Negocio_Anterior.P_Fecha_Envio).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) ? String.Format("{0:dd/MMMMMMMMMMMMMMMMMM/yyyy}", Cls_Negocio_Anterior.P_Fecha_Envio) : "");
                            String Actual = ((!String.Format("{0:ddMMyyyy}", Cls_Negocio_Actualizado.P_Fecha_Envio).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) ? String.Format("{0:dd/MMMMMMMMMMMMMMMMMM/yyyy}", Cls_Negocio_Actualizado.P_Fecha_Envio) : "");
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, "Fecha Envio", Anterior, Actual);
                        }
                        //Empleado Envio
                        if (!Cls_Negocio_Anterior.P_Empleado_Envio.Equals(Cls_Negocio_Actualizado.P_Empleado_Envio))
                        {
                            String Anterior = ((!String.IsNullOrEmpty(Cls_Negocio_Anterior.P_Empleado_Envio)) ? Cls_Negocio_Anterior.P_Empleado_Envio : "");
                            String Actual = ((!String.IsNullOrEmpty(Cls_Negocio_Actualizado.P_Empleado_Envio)) ? Cls_Negocio_Actualizado.P_Empleado_Envio : "");
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, "Nombre Envio", Anterior, Actual);
                        }
                        //Fecha Entrada
                        if (!String.Format("{0:ddMMyyyy}", Cls_Negocio_Anterior.P_Fecha_Entrada).Equals(String.Format("{0:ddMMyyyy}", Cls_Negocio_Actualizado.P_Fecha_Entrada)))
                        {
                            String Anterior = ((!String.Format("{0:ddMMyyyy}", Cls_Negocio_Anterior.P_Fecha_Entrada).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) ? String.Format("{0:dd/MMMMMMMMMMMMMMMMMM/yyyy}", Cls_Negocio_Anterior.P_Fecha_Entrada) : "");
                            String Actual = ((!String.Format("{0:ddMMyyyy}", Cls_Negocio_Actualizado.P_Fecha_Entrada).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) ? String.Format("{0:dd/MMMMMMMMMMMMMMMMMM/yyyy}", Cls_Negocio_Actualizado.P_Fecha_Entrada) : "");
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, "Fecha Entrada", Anterior, Actual);
                        }
                        //Empleado Entrada
                        if (!Cls_Negocio_Anterior.P_Empleado_Entrada.Equals(Cls_Negocio_Actualizado.P_Empleado_Entrada))
                        {
                            String Anterior = ((!String.IsNullOrEmpty(Cls_Negocio_Anterior.P_Empleado_Entrada)) ? Cls_Negocio_Anterior.P_Empleado_Entrada : "");
                            String Actual = ((!String.IsNullOrEmpty(Cls_Negocio_Actualizado.P_Empleado_Entrada)) ? Cls_Negocio_Actualizado.P_Empleado_Entrada : "");
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, "Nombre Entrada", Anterior, Actual);
                        }
                        //Fecha Entrega
                        if (!String.Format("{0:ddMMyyyy}", Cls_Negocio_Anterior.P_Fecha_Entrega).Equals(String.Format("{0:ddMMyyyy}", Cls_Negocio_Actualizado.P_Fecha_Entrega)))
                        {
                            String Anterior = ((!String.Format("{0:ddMMyyyy}", Cls_Negocio_Anterior.P_Fecha_Entrega).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) ? String.Format("{0:dd/MMMMMMMMMMMMMMMMMM/yyyy}", Cls_Negocio_Anterior.P_Fecha_Entrega) : "");
                            String Actual = ((!String.Format("{0:ddMMyyyy}", Cls_Negocio_Actualizado.P_Fecha_Entrega).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) ? String.Format("{0:dd/MMMMMMMMMMMMMMMMMM/yyyy}", Cls_Negocio_Actualizado.P_Fecha_Entrega) : "");
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, "Fecha Entrega", Anterior, Actual);
                        }
                        //Empleado Entrega
                        if (!Cls_Negocio_Anterior.P_Empleado_Entrega.Equals(Cls_Negocio_Actualizado.P_Empleado_Entrega))
                        {
                            String Anterior = ((!String.IsNullOrEmpty(Cls_Negocio_Anterior.P_Empleado_Entrega)) ? Cls_Negocio_Anterior.P_Empleado_Entrega : "");
                            String Actual = ((!String.IsNullOrEmpty(Cls_Negocio_Actualizado.P_Empleado_Entrega)) ? Cls_Negocio_Actualizado.P_Empleado_Entrega : "");
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, "Nombre Entrega", Anterior, Actual);
                        }
                    }
                }   
                return Dt_Cambios;
            }

            private DataTable Estructura_DataTable_Cambios() {
                DataTable Dt_Estructura_Cambios = new DataTable("CAMBIOS");
                Dt_Estructura_Cambios.Columns.Add("No_Registro", Type.GetType("System.Int32"));
                Dt_Estructura_Cambios.Columns.Add("No_Detalle", Type.GetType("System.Int32"));
                Dt_Estructura_Cambios.Columns.Add("Campo", Type.GetType("System.String"));
                Dt_Estructura_Cambios.Columns.Add("Valor_Anterior", Type.GetType("System.String"));
                Dt_Estructura_Cambios.Columns.Add("Valor_Nuevo", Type.GetType("System.String"));
                return Dt_Estructura_Cambios;
            }

            private void Agregar_Fila_Estructura_Cambios(ref DataTable Dt_Estructura_Cambios, String Campo, String Valor_Anterior, String Valor_Nuevo) {
                DataRow Fila_Nueva = Dt_Estructura_Cambios.NewRow();
                Fila_Nueva["Campo"] = Campo.Trim();
                Fila_Nueva["Valor_Anterior"] = ((Valor_Anterior.Trim().Length > 0) ? Valor_Anterior.Trim() : "<<Vacio>>");
                Fila_Nueva["Valor_Nuevo"] = ((Valor_Nuevo.Trim().Length > 0) ? Valor_Nuevo.Trim() : "<<Vacio>>");
                Dt_Estructura_Cambios.Rows.Add(Fila_Nueva);
            }

            public DataTable Consultar_Cambios_Bien() {
                return Cls_Ope_Com_Manejo_Historial_Cambios_Datos.Consultar_Cambios(this);
            }

        #endregion
    }
}
