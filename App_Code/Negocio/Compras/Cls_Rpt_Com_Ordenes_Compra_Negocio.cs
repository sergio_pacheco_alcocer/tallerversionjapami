﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Reporte_Ordenes_Compra.Datos;
/// <summary>
/// Summary description for Cls_Rpt_Com_Ordenes_Compra_Negocio
/// </summary>
namespace JAPAMI.Reporte_Ordenes_Compra.Negocio
{
    public class Cls_Rpt_Com_Ordenes_Compra_Negocio
    {
        public Cls_Rpt_Com_Ordenes_Compra_Negocio()
        {
        }

        #region Variables Locales
            String Busqueda;
            String Producto_ID;
            String Proveedor_ID;
            String Tipo_Consulta;
            String Estatus;
            DateTime Fecha_Inicio;
            DateTime Fecha_Fin;
            String Tipo_Consulta_Detalles;
            String Clave;
            String Nombre;
            Int64 Ref_JAPAMI;
            String Unidad_ID;
            String Impuesto_ID;
            String Tipo;
            String Stock;
            String Partida_Generica_ID;
            String Partida_Especifica_ID;
        #endregion

        #region Variables Publicas
            public String P_Busqueda
            {
                get { return Busqueda; }
                set { Busqueda = value; }
            }

            public String P_Producto_ID
            {
                get { return Producto_ID; }
                set { Producto_ID = value; }
            }

            public String P_Proveedor_ID
            {
                get { return Proveedor_ID; }
                set { Proveedor_ID = value; }
            }

            public String P_Tipo_Consulta
            {
                get { return Tipo_Consulta; }
                set { Tipo_Consulta = value; }
            }

            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }


            public DateTime P_Fecha_Inicio
            {
                get { return Fecha_Inicio; }
                set { Fecha_Inicio = value; }
            }

            public DateTime P_Fecha_Fin
            {
                get { return Fecha_Fin; }
                set { Fecha_Fin = value; }
            }

            public String P_Tipo_Consulta_Detalles
            {
                get { return Tipo_Consulta_Detalles; }
                set { Tipo_Consulta_Detalles = value; }
            }

            public String P_Clave
            {
                get { return Clave; }
                set { Clave = value; }
            }

            public String P_Nombre
            {
                get { return Nombre; }
                set { Nombre = value; }
            }

            public Int64 P_Ref_JAPAMI
            {
                get { return Ref_JAPAMI; }
                set { Ref_JAPAMI = value; }
            }

            public String P_Unidad_ID
            {
                get { return Unidad_ID; }
                set { Unidad_ID = value; }
            }

            public String P_Impuesto_ID
            {
                get { return Impuesto_ID; }
                set { Impuesto_ID = value; }
            }

            public String P_Tipo
            {
                get { return Tipo; }
                set { Tipo = value; }
            }

            public String P_Stock
            {
                get { return Stock; }
                set { Stock = value; }
            }

            public String P_Partida_Generica_ID
            {
                get { return Partida_Generica_ID; }
                set { Partida_Generica_ID = value; }
            }

            public String P_Partida_Especifica_ID
            {
                get { return Partida_Especifica_ID; }
                set { Partida_Especifica_ID = value; }
            }
        #endregion

        #region Metodos
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Productos
            ///DESCRIPCION:             Consultar los productos de acuerdo a un criterio de busqueda por aproximacion
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              31/Marzo/2012 10:48
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consulta_Productos()
            {
                return Cls_Rpt_Com_Ordenes_Compra_Datos.Consulta_Productos(this);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Proveedores
            ///DESCRIPCION:             Consultar los proveedores de acuerdo a un criterio de busqueda por aproximacion
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              31/Marzo/2012 11:16
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consulta_Proveedores()
            {
                return Cls_Rpt_Com_Ordenes_Compra_Datos.Consulta_Proveedores(this);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Ordenes_Compra
            ///DESCRIPCION:             Consultar las ordenes de compra por proveedor o por producto
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              09/Abril/2012 12:43
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consulta_Ordenes_Compra()
            {
                return Cls_Rpt_Com_Ordenes_Compra_Datos.Consulta_Ordenes_Compra(this);
            }
        #endregion
    }
}