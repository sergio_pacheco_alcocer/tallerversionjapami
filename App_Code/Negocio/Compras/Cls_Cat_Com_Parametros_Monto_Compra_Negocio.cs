﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Parametros_Monto_Compra.Datos;

/// <summary>
/// Summary description for Cls_Cat_Com_Parametros_Monto_Compra
/// </summary>
namespace JAPAMI.Parametros_Monto_Compra.Negocios
{
    public class Cls_Cat_Com_Parametros_Monto_Compra_Negocio
    {
        public Cls_Cat_Com_Parametros_Monto_Compra_Negocio()
        {
        }
        #region Variables_Locales
        private String Monto_Compra;
        #endregion

        #region Variables_Publicas
        public String P_Monto_Compra
        {
            get { return Monto_Compra; }
            set { Monto_Compra = value; }
        }
        #endregion

        #region Metodos
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Parametro_Monto_Compras
        ///DESCRIPCIÓN          : Metodo para consultar los parametros registrados en la base de datos
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 10/Noviembre/2012 01:09pm
        ///*********************************************************************************************************
        public DataTable Consultar_Parametro_Monto_Compras()
        {
            return Cls_Cat_Com_Parametros_Monto_Compra_Datos.Consultar_Monto_Compra();

        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Monto_Compra
        ///DESCRIPCIÓN          : Metodo para dar insertar el parametro en caso de que no exista ninguna fila en la 
        ///                       tabla de parametros
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 10/Noviembre/2012 01:12pm
        ///*********************************************************************************************************
        public Boolean Alta_Monto_Compra()
        {
             return Cls_Cat_Com_Parametros_Monto_Compra_Datos.Alta_Parametro_Monto_Compra(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Monto_Compra
        ///DESCRIPCIÓN          : Metodo para dar insertar el parametro en caso de que no exista ninguna fila en la 
        ///                       tabla de parametros
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 10/Noviembre/2012 01:18pm
        ///*********************************************************************************************************
        public Boolean Modificar_Monto_Compra()
        {
            return Cls_Cat_Com_Parametros_Monto_Compra_Datos.Modificar_Parametro_Monto_Compra(this);
        }
        #endregion
    }
}
