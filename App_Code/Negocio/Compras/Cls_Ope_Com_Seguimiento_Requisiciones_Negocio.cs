﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Seguimiento_Requisicion.Datos;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Ope_Com_Requisiciones_Negocio
/// </summary>
/// 

namespace JAPAMI.Seguimiento_Requisicion.Negocio
{
    public class Cls_Ope_Com_Seguimiento_Requisiciones_Negocio
    {
        #region Variables Privadas

            private String Dependencia_ID;       
            private String Requisicion_ID;
            private String Estatus;       
            private String Tipo;
            private String Tipo_Articulo;
            private String Fecha_Inicial;
            private String Fecha_Final;        
            private String Cotizador_ID;
            private String No_Orden_Compra;
            private DateTime Fecha_Envio = new DateTime();
            private DateTime Fecha_Recepcion = new DateTime();
            private DateTime Fecha_Entrada = new DateTime();
            private DateTime Fecha_Entrega = new DateTime();
            private String Empleado_Envio = String.Empty;
            private String Empleado_Recepcion = String.Empty;
            private String Empleado_Entrada = String.Empty;
            private String Empleado_Entrega = String.Empty;
            private String Estatus_Bien = String.Empty;
            private String Nombre_Bien = String.Empty;
            private String No_Serie = String.Empty;
            private String Modelo = String.Empty;
            private String No_Inventario = String.Empty;
            private String Bien_Id = String.Empty;
            private SqlTransaction Trans;
            private SqlCommand Cmmd;
            private DataTable Dt_Cambios = new DataTable();
            private String Producto;

        #endregion 

        #region Variables Publicas
            public String P_Producto
            {
                get { return Producto; }
                set { Producto = value; }
            }
        
            public String P_Cotizador_ID
            {
                get { return Cotizador_ID; }
                set { Cotizador_ID = value; }
            }
            public String P_Fecha_Inicial
            {
                get { return Fecha_Inicial; }
                set { Fecha_Inicial = value; }
            }
            public String P_Fecha_Final
            {
                get { return Fecha_Final; }
                set { Fecha_Final = value; }
            }
            public DateTime P_Fecha_Envio
            {
                get { return Fecha_Envio; }
                set { Fecha_Envio = value; }
            }
            public DateTime P_Fecha_Recepcion
            {
                get { return Fecha_Recepcion; }
                set { Fecha_Recepcion = value; }
            }
            public DateTime P_Fecha_Entrada
            {
                get { return Fecha_Entrada; }
                set { Fecha_Entrada = value; }
            }
            public DateTime P_Fecha_Entrega
            {
                get { return Fecha_Entrega; }
                set { Fecha_Entrega = value; }
            }
            public String P_Empleado_Envio
            {
                get { return Empleado_Envio; }
                set { Empleado_Envio = value; }
            }
            public String P_Empleado_Recepcion
            {
                get { return Empleado_Recepcion; }
                set { Empleado_Recepcion = value; }
            }
            public String P_Empleado_Entrada
            {
                get { return Empleado_Entrada; }
                set { Empleado_Entrada = value; }
            }
            public String P_Empleado_Entrega
            {
                get { return Empleado_Entrega; }
                set { Empleado_Entrega = value; }
            }
            public String P_Tipo
            {
                get { return Tipo; }
                set { Tipo = value; }
            }
            public String P_Estatus_Bien
            {
                get { return Estatus_Bien; }
                set { Estatus_Bien = value; }
            }
            public String P_No_Inventario
            {
                get { return No_Inventario; }
                set { No_Inventario = value; }
            }
            public String P_Bien_Id
            {
                get { return Bien_Id; }
                set { Bien_Id = value; }
            }
            public String P_No_Serie
            {
                get { return No_Serie; }
                set { No_Serie = value; }
            }
            public String P_Modelo
            {
                get { return Modelo; }
                set { Modelo = value; }
            }
            public String P_Nombre_Bien
            {
                get { return Nombre_Bien; }
                set { Nombre_Bien = value; }
            }
            public String P_Tipo_Articulo
            {
                get { return Tipo_Articulo; }
                set { Tipo_Articulo = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Requisicion_ID
            {
                get { return Requisicion_ID; }
                set { Requisicion_ID = value; }
            }     
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }
            public String P_No_Orden_Compra
            {
                get { return No_Orden_Compra; }
                set { No_Orden_Compra = value; }
            }
            public SqlTransaction P_Trans
            {
                get { return Trans; }
                set { Trans = value; }
            }
            public SqlCommand P_Cmmd
            {
                get { return Cmmd; }
                set { Cmmd = value; }
            }
            public DataTable P_Dt_Cambios
            {
                get { return Dt_Cambios; }
                set { Dt_Cambios = value; }
            }
        #endregion
       
        #region Metodos

            public Cls_Ope_Com_Seguimiento_Requisiciones_Negocio()
            {
            }
            
            public DataTable Consultar_Requisiciones()
            {
                return Cls_Ope_Com_Seguimiento_Requisiciones_Datos.Consultar_Requisiciones(this);
            }
            public DataTable Consultar_Requisiciones_Generales()
            {
                return Cls_Ope_Com_Seguimiento_Requisiciones_Datos.Consultar_Requisiciones_Generales(this);
            }

            public DataTable Consultar_Requisiciones_Fechas_Bienes()
            {
                return Cls_Ope_Com_Seguimiento_Requisiciones_Datos.Consultar_Requisiciones_Fechas_Bienes(this);
            }

            public DataTable Consultar_Bienes_Requisiciones()
            {
                return Cls_Ope_Com_Seguimiento_Requisiciones_Datos.Consultar_Bienes_Requisiciones(this);
            }
            
            public DataTable Consultar_Historial_Requisicion()
            {
                return Cls_Ope_Com_Seguimiento_Requisiciones_Datos.Consultar_Historial_Requisicion(this);
            }      

            public DataTable Consultar_Cotizadores()
            {
                return Cls_Ope_Com_Seguimiento_Requisiciones_Datos.Consultar_Cotizadores(this);
            }
            public DataTable Consultar_Proveedor()
            {
                return Cls_Ope_Com_Seguimiento_Requisiciones_Datos.Consultar_Proveedor(this);
            }
            public void Insertar_Fechas()
            {
                Cls_Ope_Com_Seguimiento_Requisiciones_Datos.Insertar_Fechas(this);
            }
            public DataTable Consultar_Entradas()
            {
                return Cls_Ope_Com_Seguimiento_Requisiciones_Datos.Consultar_Entradas(this);
            }
            public DataTable Consultar_Salidas()
            {
                return Cls_Ope_Com_Seguimiento_Requisiciones_Datos.Consultar_Salidas(this);
            }
            public DataTable Consultar_Contra_Recibos()
            {
                return Cls_Ope_Com_Seguimiento_Requisiciones_Datos.Consultar_Contra_Recibos(this);
            }

            public Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Obtener_Detalles_Fechas_Requisicion() {
                return Cls_Ope_Com_Seguimiento_Requisiciones_Datos.Obtener_Detalles_Fechas_Requisicion(this);
            }

        #endregion 
    }
}
