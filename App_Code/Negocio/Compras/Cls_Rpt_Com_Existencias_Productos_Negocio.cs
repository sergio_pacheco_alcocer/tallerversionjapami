﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Rpt_Com_Existencias_productos.Datos;
/// <summary>
/// Summary description for Cls_Rpt_Com_Existencias_Productos_Negocio
/// </summary>
/// 
namespace JAPAMI.Rpt_Com_Existencias_productos.Negocio
{
    public class Cls_Rpt_Com_Existencias_Productos_Negocio
    {
        public Cls_Rpt_Com_Existencias_Productos_Negocio()
        {

        }
        #region Variables Locales
        private String Clave_Partida;
        private String Nombre_Producto;
        private String Alamacen_General; // Especifica si el producto pertenece al almacen gereal o al almacen de papelería
        private String Partida_ID;
        #endregion

        #region Variables Publicas
        public String P_Clave_Partida 
        {
            get { return Clave_Partida; }
            set { Clave_Partida = value; }
        }
        public String P_Nombre_Producto 
        {
            get { return Nombre_Producto; }
            set { Nombre_Producto = value; }
        }
        public String P_Alamacen_General 
        {
            get { return Alamacen_General; }
            set { Alamacen_General = value; }
        }
        public String P_Partida_ID
        {
            get { return Partida_ID; }
            set { Partida_ID = value; }
        }
        #endregion

        #region Metodos
        public DataTable Concultar_Partidas_Especificas() 
        {
            return Cls_Rpt_Com_Existencias_Productos_Datos.Consultar_Partidas_Especificas(this);
        }
        public DataTable Consultar_Productos() 
        {
            return Cls_Rpt_Com_Existencias_Productos_Datos.Consultar_Productos(this);
        }
        public DataTable Consultar_Clave_Partia_Especifica() 
        {
            return Cls_Rpt_Com_Existencias_Productos_Datos.Consultar_Clave_Partida_Especifica(this);
        }
        #endregion
    }
}
