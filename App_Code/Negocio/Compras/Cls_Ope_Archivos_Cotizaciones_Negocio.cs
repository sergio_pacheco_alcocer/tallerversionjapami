﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Archivos_Cotizaciones.Datos;

/// <summary>
/// Summary description for Cls_Ope_Archivos_Cotizaciones_Negocio
/// </summary>
namespace JAPAMI.Archivos_Cotizaciones.Negocio
{
    public class Cls_Ope_Archivos_Cotizaciones_Negocio
    {
        public Cls_Ope_Archivos_Cotizaciones_Negocio()
        {
        }

        #region Variables Locales
            int No_Requisicion;
            DataTable Dt_Archivos;
            DataTable Dt_Archivos_Eliminados;
        #endregion

        #region Variables Publicas
            public int P_No_Requisicion
            {
                get { return No_Requisicion; }
                set { No_Requisicion = value; }
            }

            public DataTable P_Dt_Archivos
            {
                get { return Dt_Archivos; }
                set { Dt_Archivos = value; }
            }

            public DataTable P_Dt_Archivos_Eliminados
            {
                get { return Dt_Archivos_Eliminados; }
                set { Dt_Archivos_Eliminados = value; }
            }
        #endregion

        #region Metodos
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Archivos
            ///DESCRIPCION:             hacer la consulta de los archivos de una cotizacion
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              09/Marzo/2012 13:39
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consulta_Archivos()
            {
                return Cls_Ope_Archivos_Cotizaciones_Datos.Consulta_Archivos(this);
            }

            public void Agregar_Archivos_Cotizacion()
            {
                Cls_Ope_Archivos_Cotizaciones_Datos.Agregar_Archivos_Cotizacion(this);
            }

            public void Eliminar_Archivos_Cotizaciones()
            {
                Cls_Ope_Archivos_Cotizaciones_Datos.Eliminar_Archivos_Cotizaciones(this);
            }
        #endregion
    }
}