﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Rpt_Orden_Compra.Datos;


/// <summary>
/// Summary description for Cls_Com_Orden_Compra_Negocio
/// </summary>
namespace JAPAMI.Rpt_Orden_Compra.Negocio
{

    public class Cls_Com_Orden_Compra_Negocio
    {
        ///*******************************************************************
        ///VARIABLES INTERNAS
        ///*******************************************************************
        #region Variables Internas
            private String Tipo;
            private String Tipo_Articulo;
            private String Estatus;
            private String Fecha_Inicial;
            private String Fecha_Final;
            private DateTime Filtro_Fecha_Inicial = new DateTime();
            private DateTime Filtro_Fecha_Final = new DateTime();
            private String Dependencia_ID;
            private String Proveedor_ID;
        #endregion

        ///*******************************************************************
        ///VARIABLES PUBLICAS
        ///*******************************************************************
        #region Variables Publicas
            public String P_Tipo_Articulo
            {
                get { return Tipo_Articulo; }
                set { Tipo_Articulo = value; }
            }
            public String P_Tipo
            {
                get { return Tipo; }
                set { Tipo = value; }
            }

            public String P_Fecha_Inicial
            {
                get { return Fecha_Inicial; }
                set { Fecha_Inicial = value; }
            }
            public String P_Fecha_Final
            {
                get { return Fecha_Final; }
                set { Fecha_Final = value; }
            }

            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public DateTime P_Filtro_Fecha_Inicial
            {
                get { return Filtro_Fecha_Inicial; }
                set { Filtro_Fecha_Inicial = value; }
            }
            public DateTime P_Filtro_Fecha_Final
            {
                get { return Filtro_Fecha_Final; }
                set { Filtro_Fecha_Final = value; }
            }
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }
            public String P_Proveedor_ID
            {
                get { return Proveedor_ID; }
                set { Proveedor_ID = value; }
            }
        #endregion

        ///*******************************************************************
        ///VARIABLES METODOS
        ///*******************************************************************
        #region Metodos

            public DataSet Consultar_Ordenes_Compra()
            {
                return Cls_Com_Orden_Compra_Datos.Consultar_Ordenes_Compra(this);

            }

            public DataTable Consultar_Ordenes_Servicio()
            {
                return Cls_Com_Orden_Compra_Datos.Consultar_Ordenes_Servicio(this);

            }

            public DataTable Consultar_Dependencias()
            {
                return Cls_Com_Orden_Compra_Datos.Consultar_Dependencias(this);
            }

        #endregion


    }

}