﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Archivos_Requisiciones.Datos;

/// <summary>
/// Summary description for Cls_Ope_Archivos_Requisiciones_Negocio
/// </summary>
namespace JAPAMI.Archivos_Requisiciones.Negocios
{
    public class Cls_Ope_Archivos_Requisiciones_Negocio
    {
        #region Variables Locales
            int No_Requisicion;
            DataTable Dt_Archivos;
            DataTable Dt_Archivos_Eliminados;
            Int64 No_Archivo;
        #endregion

        #region Variables Publicas
            public int P_No_Requisicion
            {
                get { return No_Requisicion; }
                set { No_Requisicion = value; }
            }

            public DataTable P_Dt_Archivos
            {
                get { return Dt_Archivos; }
                set { Dt_Archivos = value; }
            }

            public DataTable P_Dt_Archivos_Eliminados
            {
                get { return Dt_Archivos_Eliminados; }
                set { Dt_Archivos_Eliminados = value; }
            }

            public Int64 P_No_Archivo
            {
                get { return No_Archivo; }
                set { No_Archivo = value; }
            }
        #endregion


        public Cls_Ope_Archivos_Requisiciones_Negocio()
        {
        }

        #region Metodos
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Alta_Archivos_Requisicion
            ///DESCRIPCION:             Dar de alta en la base de datos los nombres de los archivos a ingresar en la base de datos
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              05/Marzo/2012 09:56
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public void Alta_Archivos_Requisicion()
            {
                Cls_Ope_Archivos_Requisiciones_Datos.Alta_Archivos_Requisicion(this);
            }


            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Eliminar_Archivos_Requisicion
            ///DESCRIPCION:             Eliminar los archivos de la requisicion
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              05/Marzo/2012 10:18
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public void Eliminar_Archivos_Requisicion()
            {
                Cls_Ope_Archivos_Requisiciones_Datos.Eliminar_Archivos_Requisicion(this);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Archivos
            ///DESCRIPCION:             hacer la consulta de los archivos de una requisicion
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              05/Marzo/2012 19:00
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consulta_Archivos()
            {
                return Cls_Ope_Archivos_Requisiciones_Datos.Consulta_Archivos(this);
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consultar_Datos_Archivo
            ///DESCRIPCION:             Consulta los datos de un archivo especifico
            ///PARAMETROS:              
            ///CREO:                    Jennyfer Ivonne Ceja Lemus
            ///FECHA_CREO:              19/Diciembre/2012 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consultar_Datos_Archivo() 
            {
                return Cls_Ope_Archivos_Requisiciones_Datos.Consulta_Datos_Archivo(this);
            }

        #endregion
    }
}