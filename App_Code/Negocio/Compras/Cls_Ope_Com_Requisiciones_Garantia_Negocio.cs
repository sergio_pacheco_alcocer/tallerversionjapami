﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Requisiciones_Garantia.Datos;

/// <summary>
/// Summary description for Cls_Ope_Com_Requisiciones_Garantia_Negocio
/// </summary>
/// 

namespace JAPAMI.Requisiciones_Garantia.Negocio
{
    public class Cls_Ope_Com_Requisiciones_Garantia_Negocio
    {

        #region Variables Internas

            private String Folio = String.Empty;
            private String Estatus = String.Empty;
            private String Nombre = String.Empty;
            private String Dependencia_ID = String.Empty;
            private String Empleado_ID = String.Empty;
            private String No_Requisicion = String.Empty;
            private String No_Requisicion_Referencia = String.Empty;
            private String Justificacion = String.Empty;
            private DateTime Fecha_Elaboracion = new DateTime();
            private String Nombre_Usuario = String.Empty;
            private String Usuario_ID = String.Empty;
            private String Comentarios = String.Empty;

            private DateTime Fecha_Inicial = new DateTime();
            private DateTime Fecha_Final = new DateTime();
            private DataTable Dt_Comentarios = new DataTable();
            private DataTable Dt_Historial = new DataTable();
            private String Proveedor = String.Empty;
            private String Resultado = String.Empty;
            private String Diagnositco = String.Empty;

        #endregion Variables Internas

        #region Variables Publicas

            public String P_Proveedor
            {
                set { Proveedor = value; }
                get { return Proveedor; }
            }
            public String P_Resultado
            {
                set { Resultado = value; }
                get { return Resultado; }
            }
            public String P_Diagnositco
            {
                set { Diagnositco = value; }
                get { return Diagnositco; }
            }

            public String P_Folio
            {
                set { Folio = value; }
                get { return Folio; }
            }
            public String P_Nombre_Usuario
            {
                set { Nombre_Usuario = value; }
                get { return Nombre_Usuario; }
            }
            public String P_Usuario_ID
            {
                set { Usuario_ID = value; }
                get { return Usuario_ID; }
            }
            public String P_Estatus
            {
                set { Estatus = value; }
                get { return Estatus; }
            }

            public String P_Nombre
            {
                set { Nombre = value; }
                get { return Nombre; }
            }

            public String P_Dependencia_ID
            {
                set { Dependencia_ID = value; }
                get { return Dependencia_ID; }
            }

            public String P_Empleado_ID
            {
                set { Empleado_ID = value; }
                get { return Empleado_ID; }
            }

            public String P_No_Requisicion
            {
                set { No_Requisicion = value; }
                get { return No_Requisicion; }
            }

            public String P_No_Requisicion_Referencia
            {
                set { No_Requisicion_Referencia = value; }
                get { return No_Requisicion_Referencia; }
            }
            public String P_Justificacion
            {
                set { Justificacion = value; }
                get { return Justificacion; }
            }
            public DateTime P_Fecha_Elaboracion
            {
                set { Fecha_Elaboracion = value; }
                get { return Fecha_Elaboracion; }
            }
            public DateTime P_Fecha_Inicial
            {
                set { Fecha_Inicial = value; }
                get { return Fecha_Inicial; }
            }
            public DateTime P_Fecha_Final
            {
                set { Fecha_Final= value; }
                get { return Fecha_Final; }
            }
            public String P_Comentarios
            {
                set { Comentarios = value; }
                get { return Comentarios; }
            }

            public DataTable P_Dt_Comentarios
            {
                set { Dt_Comentarios = value; }
                get { return Dt_Comentarios; }
            }

            public DataTable P_Dt_Historial
            {
                set { Dt_Historial = value; }
                get { return Dt_Historial; }
            }

        #endregion Variables Publicas

        #region Metodos

            public DataTable Consultar_Dependencias()
            {
                return Cls_Ope_Com_Requisiciones_Garantia_Datos.Consultar_Dependencias(this);
            }

            public DataTable Consultar_Dependencias_Empleado()
            {
                return Cls_Ope_Com_Requisiciones_Garantia_Datos.Consultar_Dependencias_Empleado(this);
            }

            public DataTable Consultar_Requisiciones()
            {
                return Cls_Ope_Com_Requisiciones_Garantia_Datos.Consultar_Requisiciones(this);
            }

            public DataTable Consultar_Servicios_Requisicion()
            {
                return Cls_Ope_Com_Requisiciones_Garantia_Datos.Consultar_Servicios_Requisicion(this);
            }

            public DataTable Consultar_Bienes_Requisicion()
            {
                return Cls_Ope_Com_Requisiciones_Garantia_Datos.Consultar_Bienes_Requisicion(this);
            }

            public Cls_Ope_Com_Requisiciones_Garantia_Negocio Alta_Requisicion_Garantia() {
                return Cls_Ope_Com_Requisiciones_Garantia_Datos.Alta_Requisicion_Garantia(this);
            }

            public void Modifica_Requisicion_Garantia()
            {
                Cls_Ope_Com_Requisiciones_Garantia_Datos.Modifica_Requisicion_Garantia(this);
            }

            public DataTable Consultar_Requisiciones_Garantia(){
                return Cls_Ope_Com_Requisiciones_Garantia_Datos.Consultar_Requisiciones_Garantia(this);
            }

            public Cls_Ope_Com_Requisiciones_Garantia_Negocio Consultar_Detalle_Requisicion_Garantia()
            {
                return Cls_Ope_Com_Requisiciones_Garantia_Datos.Consultar_Detalle_Requisicion_Garantia(this);
            }
            public DataTable Consultar_Proveedores_Requisiciones_Garantia() {
                return Cls_Ope_Com_Requisiciones_Garantia_Datos.Consultar_Proveedores_Requisiciones_Garantia(this);
            }

            public DataTable Consultar_Cabecera_Reporte_Requisiciones_Garantia()
            {
                return Cls_Ope_Com_Requisiciones_Garantia_Datos.Consultar_Cabecera_Reporte_Requisiciones_Garantia(this);
            }
        #endregion Metodos

    }
}
