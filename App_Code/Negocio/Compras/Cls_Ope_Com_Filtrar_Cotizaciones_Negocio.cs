﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Filtrar_Cotizaciones.Datos;

/// <summary>
/// Summary description for Cls_Ope_Com_Filtrar_Cotizaciones_Negocio
/// </summary>
namespace JAPAMI.Filtrar_Cotizaciones.Negocio
{
    public class Cls_Ope_Com_Filtrar_Cotizaciones_Negocio
    {
        #region Varibles Internas

        private String Cotizador_ID;
        private String Estatus;
        private String Requisicion;
        private String Fecha_Inicial;
        private String Fecha_Final;

        
        #endregion

        #region Variables Publicas
        
        public String P_Cotizador_ID
        {
            get { return Cotizador_ID; }
            set { Cotizador_ID = value; }
        }

        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }

        public String P_Requisicion
        {
            get { return Requisicion; }
            set { Requisicion = value; }
        }
        
        public String P_Fecha_Inicial
        {
            get { return Fecha_Inicial; }
            set { Fecha_Inicial = value; }
        }
        
        public String P_Fecha_Final
        {
            get { return Fecha_Final; }
            set { Fecha_Final = value; }
        }

        #endregion

        #region Metodos

        public void Modificacion_Estatus() {
            Cls_Ope_Com_Filtrar_Cotizaciones_Datos.Modificar_Estatus(this);
        }

        public DataTable Consultar_Requisiciones()
        {
            return Cls_Ope_Com_Filtrar_Cotizaciones_Datos.Consultar_Requisiciones(this);
        }

        public DataTable Consultar_Requisiciones_Busqueda()
        {
            return Cls_Ope_Com_Filtrar_Cotizaciones_Datos.Consultar_Requisiciones_Busqueda(this);
        }


        #endregion
    }
}
