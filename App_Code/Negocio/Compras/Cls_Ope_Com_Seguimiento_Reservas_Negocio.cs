﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Seguimiento_Reservas.Datos;


/// <summary>
/// Summary description for Cls_Ope_Com_Seguimiento_Reservas_Negocio
/// </summary>
/// 
namespace JAPAMI.Seguimiento_Reservas.Negocio
{
    

    public class Cls_Ope_Com_Seguimiento_Reservas_Negocio
    {

        ///*******************************************************************************
        /// VARIABLES INTERNAS
        ///*******************************************************************************
        #region

        private String Proveedor_ID;


        private String Empleado_ID;


        private String No_Empledo;


        private String Nombre_Empleado;


        private String Padron_Prov;


        private String Razon_Social;

        
        #endregion

        ///*******************************************************************************
        /// VARIABLES PUBLICAS
        ///*******************************************************************************
        #region
        public String P_Proveedor_ID
        {
            get { return Proveedor_ID; }
            set { Proveedor_ID = value; }
        }

        public String P_Empleado_ID
        {
            get { return Empleado_ID; }
            set { Empleado_ID = value; }
        }

        public String P_No_Empledo
        {
            get { return No_Empledo; }
            set { No_Empledo = value; }
        }

        public String P_Nombre_Empleado
        {
            get { return Nombre_Empleado; }
            set { Nombre_Empleado = value; }
        }
        public String P_Padron_Prov
        {
            get { return Padron_Prov; }
            set { Padron_Prov = value; }
        }

        public String P_Razon_Social
        {
            get { return Razon_Social; }
            set { Razon_Social = value; }
        }

        #endregion

        ///*******************************************************************************
        /// METODOS
        ///*******************************************************************************
        #region
        public DataTable Consultar_Proveedor()
        {
            return Cls_Ope_Com_Seguimiento_Reservas_Datos.Consultar_Proveedor(this);
        }


        public DataTable Consultar_Empleado()
        {
            return Cls_Ope_Com_Seguimiento_Reservas_Datos.Consultar_Empleado(this);
        }
        #endregion
    }//din del class
}//fin del namespace