﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Ope_Com_Entrada_Facturas_Servicios_Generales.Datos;
using System.Data;
/// <summary>
/// Summary description for Cls_Ope_Com_Entrada_Facturas_Negocio
/// </summary>
namespace JAPAMI.Ope_Com_Entrada_Facturas_Servicios_Generales.Negocio
{
    public class Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Negocio
    {
        #region Variables Internas
        private String Proveedor_ID = String.Empty;
        private String No_Requisicion = String.Empty;
        private String No_Orden_Compra = String.Empty;
        private DateTime Fecha_Inicio = new DateTime();
        private DateTime Fecha_Final = new DateTime();
        private String Estatus = String.Empty;
        private DataTable Dt_Datos = new DataTable();
        private String Usuario = String.Empty;
        #endregion Variables Internas

        #region Variables Publicas
        public String P_Proveedor_ID
        {
            set { Proveedor_ID = value; }
            get { return Proveedor_ID; }
        }
        public String P_No_Requisicion
        {
            set { No_Requisicion = value; }
            get { return No_Requisicion; }
        }
        public String P_No_Orden_Compra
        {
            set { No_Orden_Compra = value; }
            get { return No_Orden_Compra; }
        }
        public DateTime P_Fecha_Inicio
        {
            set { Fecha_Inicio = value; }
            get { return Fecha_Inicio; }
        }
        public DateTime P_Fecha_Final
        {
            set { Fecha_Final = value; }
            get { return Fecha_Final; }
        }
        public String P_Estatus
        {
            set { Estatus = value; }
            get { return Estatus; }
        }
        public DataTable P_Dt_Datos {
            set { Dt_Datos = value; }
            get { return Dt_Datos; }
        }
        public String P_Usuario
        {
            set { Usuario = value; }
            get { return Usuario; }
        }
        #endregion Variables Internas

        #region Metodos
        public DataTable Consultar_Ordenes_Servicio() {
            return Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Datos.Consultar_Ordenes_Servicio(this);
        }
        public DataTable Consultar_Proveedores() {
            return Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Datos.Consultar_Proveedores(this);
        }
        public DataTable Consultar_Detalles_Orden_Servicio() {
            return Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Datos.Consultar_Detalles_Orden_Servicio(this);
        }
        public void Alta_Facturas() {
            Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Datos.Alta_Facturas(this);
        }
        #endregion Metodos

    }
}