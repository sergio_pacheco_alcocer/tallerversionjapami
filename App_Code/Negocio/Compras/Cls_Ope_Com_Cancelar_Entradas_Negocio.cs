﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Cancelar_Entradas.Datos;

namespace JAPAMI.Cancelar_Entradas.Negocio
{
    public class Cls_Ope_Com_Cancelar_Entradas_Negocio
    {
        #region Variables Privadas

            private String No_Requisicion;
            private String No_Entrada;
            private String Estatus;
            private String Total;
            private String Comentario;
            private String Fecha_Inicio;
            private String Fecha_Fin;

            private DataTable Detalles;
        
        #endregion 

        #region Variables Publicas

            public String P_No_Requisicion
            {
                get { return No_Requisicion; }
                set { No_Requisicion = value; }
            } 
            public String P_No_Entrada
            {
                get { return No_Entrada; }
                set { No_Entrada = value; }
            }            
            public String P_Fecha_Inicio
            {
                get { return Fecha_Inicio; }
                set { Fecha_Inicio = value; }
            }
            public String P_Fecha_Fin
            {
                get { return Fecha_Fin; }
                set { Fecha_Fin = value; }
            }

            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Comentario
            {
                get { return Comentario; }
                set { Comentario = value; }
            }
            public String P_Total
            {
                get { return Total; }
                set { Total = value; }
            }
            public DataTable P_Dt_Detalles
            {
                get { return Detalles; }
                set { Detalles = value; }
            }

        #endregion

        #region Metodos

            public Cls_Ope_Com_Cancelar_Entradas_Negocio()
        {
        }

            public DataTable Consultar_Entradas() 
            {
                return Cls_Ope_Com_Cancelar_Entradas_Datos.Consultar_Entradas(this);
            }

            public DataTable Consultar_Entradas_Detalles()
            {
                return Cls_Ope_Com_Cancelar_Entradas_Datos.Consultar_Entradas_Detalles(this);
            }

            public String Cancelar_Entrada()
            {
                return Cls_Ope_Com_Cancelar_Entradas_Datos.Cancelar_Entrada(this);
            }
        
        #endregion 
    }
}
