﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
using System.Data;
using JAPAMI.Cancelar_Requisiciones.Datos;

namespace JAPAMI.Cancelar_Requisiciones.Negocio
{
    public class Cls_Ope_Com_Cancelar_Requisiciones_Negocio
    {
        #region Variables Privadas

            private String Dependencia_ID;
            private String Requisicion_ID;
            private String Estatus;
            private String Tipo;
            private String Tipo_Articulo;
            private String Fecha_Inicial;
            private String Fecha_Final;
            private String Cotizador_ID;
            private String No_Orden_Compra;
            private String Folio;
            private String Comentarios;
            private String Proveedor_ID;
            private DataTable Dt_Productos;

           

        #endregion

        #region Variables Publicas

            public DataTable P_Dt_Productos
            {
                get { return Dt_Productos; }
                set { Dt_Productos = value; }
            }

            public String P_Cotizador_ID
            {
                get { return Cotizador_ID; }
                set { Cotizador_ID = value; }
            }
            public String P_Fecha_Inicial
            {
                get { return Fecha_Inicial; }
                set { Fecha_Inicial = value; }
            }
            public String P_Fecha_Final
            {
                get { return Fecha_Final; }
                set { Fecha_Final = value; }
            }
            public String P_Tipo
            {
                get { return Tipo; }
                set { Tipo = value; }
            }
            public String P_Tipo_Articulo
            {
                get { return Tipo_Articulo; }
                set { Tipo_Articulo = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Requisicion_ID
            {
                get { return Requisicion_ID; }
                set { Requisicion_ID = value; }
            }
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }
            public String P_No_Orden_Compra
            {
                get { return No_Orden_Compra; }
                set { No_Orden_Compra = value; }
            }
            public String P_Folio
            {
                get { return Folio; }
                set { Folio = value; }
            }
            public String P_Comentarios
            {
                get { return Comentarios; }
                set { Comentarios = value; }
            }
            public String P_Proveedor_ID
            {
                get { return Proveedor_ID; }
                set { Proveedor_ID = value; }
            }

        #endregion

        #region Metodos

            public DataTable Consultar_Requisiciones()
            {
                return Cls_Ope_Com_Cancelar_Requisiciones_Datos.Consultar_Requisiciones(this);
            }
            public DataTable Consultar_Proveedores_Requisicion()
            {
                return Cls_Ope_Com_Cancelar_Requisiciones_Datos.Consultar_Proveedores_Requisicion(this);
            }
            public DataTable Consultar_Productos_Requisicion()
            {
                return Cls_Ope_Com_Cancelar_Requisiciones_Datos.Consultar_Productos_Requisicion(this);
            }
            public void Modificar_Requisicion()
            {
                Cls_Ope_Com_Cancelar_Requisiciones_Datos.Modificar_Requisicion(this);
            }
            public String Crear_Requisicion()
            {
                return Cls_Ope_Com_Cancelar_Requisiciones_Datos.Crear_Requisicion(this);
            }

        #endregion
    }
}