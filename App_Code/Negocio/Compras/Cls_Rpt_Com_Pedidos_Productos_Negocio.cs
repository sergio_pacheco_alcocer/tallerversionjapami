﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Rpt_Com_Pedidos_Proproductos.Datos;

/// <summary>
/// Summary description for Cls_Rpt_Com_Pedidos_Productos_Negocio
/// </summary>
namespace JAPAMI.Rpt_Com_Pedidos_Proproductos.Negocios
{
    public class Cls_Rpt_Com_Pedidos_Productos_Negocio
    {
        public Cls_Rpt_Com_Pedidos_Productos_Negocio()
        {
        }
        #region VARIABLES LOCALES
        private String Dependencia_ID;
        private String Partida_Especifica_ID;
        private String Producto_ID;
        private String Fecha_Inicio;
        private String Fecha_Fin;
        private String Estatus;
        private String Gerencia_ID;
        private String Proveedor;
        private String Producto;

               
       
        #endregion

        public String P_Producto
        {
            get { return Producto; }
            set { Producto = value; }
        } 

        public String P_Proveedor
        {
            get { return Proveedor; }
            set { Proveedor = value; }
        }
        #region VARIBLES PUBLICAS

        public String P_Gerencia_ID
        {
            get { return Gerencia_ID; }
            set { Gerencia_ID = value; }
        }
        public String P_Dependencia_ID
        {
            get { return Dependencia_ID; }
            set { Dependencia_ID = value; }
        }
        public String P_Partida_Especifica_ID
        {
            get { return Partida_Especifica_ID; }
            set { Partida_Especifica_ID = value; }
        }
        public String P_Producto_ID
        {
            get { return Producto_ID; }
            set { Producto_ID = value; }
        }
        public String P_Fecha_Inicio
        {
            get { return Fecha_Inicio; }
            set { Fecha_Inicio = value; }
        }

        public String P_Fecha_Fin
        {
            get { return Fecha_Fin; }
            set { Fecha_Fin = value; }
        }

        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        #endregion

        #region  METODOS
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:Consulta_Unidad_Responsable
        /// DESCRIPCION         :Metodo que consulta las unidades responables
        /// PARAMETROS          :            
        /// CREO                :Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO          :16/Octubre/2012 05:48pm
        /// MODIFICO            :     
        /// FECHA_MODIFICO      : 
        /// CAUSA_MODIFICACION  :     
        ///*******************************************************************************/
        public DataTable Consulta_Unidad_Responsable()
        {
            return Cls_Rpt_Com_Pedidos_Productos_Datos.Consultar_Unidad_Responsable(this);
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:Consulta_Partidas_Presupuestadas
        /// DESCRIPCION         :Metodo que consulta las partidas especificas que tienen presupuesto
        ///                     :para la unidad responsable del usuario.
        /// PARAMETROS          :            
        /// CREO                :Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO          :16/Octubre/2012 06:22pm
        /// MODIFICO            :     
        /// FECHA_MODIFICO      : 
        /// CAUSA_MODIFICACION  :     
        ///*******************************************************************************/
        public DataTable Consulta_Partidas_Presupuestadas()
        {
            return Cls_Rpt_Com_Pedidos_Productos_Datos.Consultar_Partidas_De_Un_Programa(this);
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:Consulta_Partidas_Presupuestadas
        /// DESCRIPCION         :Metodo que consulta las partidas especificas que tienen presupuesto
        ///                     :para la unidad responsable del usuario.
        /// PARAMETROS          :            
        /// CREO                :Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO          :16/Octubre/2012 06:22pm
        /// MODIFICO            :     
        /// FECHA_MODIFICO      : 
        /// CAUSA_MODIFICACION  :     
        ///*******************************************************************************/
        public DataTable Consultar_Gerencias()
        {
            return Cls_Rpt_Com_Pedidos_Productos_Datos.Consultar_Gerencias(this);
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:Consulta_Productos_Por_Partida
        /// DESCRIPCION         :Metodo que consulta los productos que pertenecen a una partida especifica
        /// PARAMETROS          :            
        /// CREO                :Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO          :16/Octubre/2012 06:29pm
        /// MODIFICO            :     
        /// FECHA_MODIFICO      : 
        /// CAUSA_MODIFICACION  :     
        ///*******************************************************************************/
        public DataTable Consulta_Productos_Por_Partida()
        {
            return Cls_Rpt_Com_Pedidos_Productos_Datos.Consultar_Productos_Por_Partida(this);
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:Consulta_Pedido_Productos
        /// DESCRIPCION         :Consulta los pedidos de una unidad responsables segun los filtros seleccionados
        ///                     :por el usuario
        /// PARAMETROS          :            
        /// CREO                :Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO          :16/Octubre/2012 01:19 pm
        /// MODIFICO            :     
        /// FECHA_MODIFICO      : 
        /// CAUSA_MODIFICACION  :     
        ///*******************************************************************************/
        public DataTable Consulta_Pedido_Productos()
        {
            return Cls_Rpt_Com_Pedidos_Productos_Datos.Consultar_Pedido_Productos(this);
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:Consulta_Unidades_Responsables
        /// DESCRIPCION         :Metodo que consulta las unidades responables
        /// PARAMETROS          :            
        /// CREO                :David Herrera rincon
        /// FECHA_CREO          :14/Enero/2013
        /// MODIFICO            :     
        /// FECHA_MODIFICO      : 
        /// CAUSA_MODIFICACION  :     
        ///*******************************************************************************/
        public DataTable Consulta_Unidades_Responsables()
        {
            return Cls_Rpt_Com_Pedidos_Productos_Datos.Consultar_Unidades_Responsables(this);
        }
        #endregion
    }
}
