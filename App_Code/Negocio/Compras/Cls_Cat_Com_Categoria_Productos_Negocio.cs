﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Categoria_Productos.Datos;

/// <summary>
/// Summary description for Cls_Cat_Com_Categoria_Productos_Negocio
/// </summary>
namespace JAPAMI.Categoria_Productos.Negocio
{
    public class Cls_Cat_Com_Categoria_Productos_Negocio
    {
        public Cls_Cat_Com_Categoria_Productos_Negocio()
        {
        }

        #region (Variables Locales)
        private String Categoria_ID;
        private String Nombre;
        private String Estatus;
        private String Descripcion;
        private String Usuario;
        private String Busqueda;
        #endregion

        #region (Variables Publicas)
        public String P_Categoria_ID
        {
            get { return Categoria_ID; }
            set { Categoria_ID = value; }
        }

        public String P_Nombre
        {
            get { return Nombre; }
            set { Nombre = value; }
        }

        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }

        public String P_Descripcion
        {
            get { return Descripcion; }
            set { Descripcion = value; }
        }

        public String P_Usuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }

        public String P_Busqueda
        {
            get { return Busqueda; }
            set { Busqueda = value; }
        }
        #endregion

        #region (Metodos)
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consulta_Categoria_Productos
        /// 	DESCRIPCION:    Consultar las categorias de los productos
        /// 	PARAMETROS:     
        /// 	CREO:           Noe Mosqueda Valadez
        /// 	FECHA_CREO:     02/Mayo/2012 19:00
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        public DataTable Consulta_Categoria_Productos()
        {
            return Cls_Cat_Com_Categoria_Productos_Datos.Consulta_Categoria_Productos(this);
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Alta_Categoria_Productos
        /// 	DESCRIPCION:    Dar de alta la categoria de los productos
        /// 	PARAMETROS:     
        /// 	CREO:           Noe Mosqueda Valadez
        /// 	FECHA_CREO:     03/Mayo/2012 12:45
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        public void Alta_Categoria_Productos()
        {
            Cls_Cat_Com_Categoria_Productos_Datos.Alta_Categoria_Productos(this);
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Baja_Categoria_Productos
        /// 	DESCRIPCION:    Dar de baja la categoria de los productos
        /// 	PARAMETROS:     
        /// 	CREO:           Noe Mosqueda Valadez
        /// 	FECHA_CREO:     03/Mayo/2012 13:20
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        public void Baja_Categoria_Productos()
        {
            Cls_Cat_Com_Categoria_Productos_Datos.Baja_Categoria_Productos(this);
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Cambio_Categoria_Productos
        /// 	DESCRIPCION:    Modificar la categoria de los productos
        /// 	PARAMETROS:     
        /// 	CREO:           Noe Mosqueda Valadez
        /// 	FECHA_CREO:     03/Mayo/2012 13:50
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        public void Cambio_Categoria_Productos()
        {
            Cls_Cat_Com_Categoria_Productos_Datos.Cambio_Categoria_Productos(this);
        }
        #endregion
    }
}