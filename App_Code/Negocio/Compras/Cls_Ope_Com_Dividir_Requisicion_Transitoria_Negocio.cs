﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Ope_Com_Dividir_Requisicion_Transitoria.Datos;

/// <summary>
/// Summary description for Cls_Ope_Com_Dividir_Requisicion_Transitoria_Negocio
/// </summary>
namespace JAPAMI.Ope_Com_Dividir_Requisicion_Transitoria.Negocios
{
    public class Cls_Ope_Com_Dividir_Requisicion_Transitoria_Negocio
    {
        public Cls_Ope_Com_Dividir_Requisicion_Transitoria_Negocio()
        {
        }
        ///*******************************************************************************
        /// VARIABLES LOCALES 
        ///******************************************************************************
        #region VARIABLES_LOCALES
        private String No_Requisicion_ID;
        private DataTable Dt_Productos;
        private String Comentarios;
        private String Tipo_Articulo;
        #endregion
        ///*******************************************************************************
        /// VARIABLES PUBLICAS
        ///*******************************************************************************
        #region VARIABLES_PUBLICAS
        public String P_No_Requisicion_ID
        {
            get { return No_Requisicion_ID; }
            set { No_Requisicion_ID = value; }
        }
        public DataTable P_Dt_Productos
        {
            get { return Dt_Productos; }
            set { Dt_Productos = value; }
        }
        public String P_Comentarios
        {
            get { return Comentarios; }
            set { Comentarios = value; }
        }
        public String P_Tipo_Articulo
        {
            get { return Tipo_Articulo; }
            set { Tipo_Articulo = value; }
        }
        #endregion

        ///*******************************************************************************
        /// METODOS
        ///*******************************************************************************
        #region METODOS
        public DataTable Consulta_Requisicion_Transitorias() 
        {
            return Cls_Ope_Com_Dividir_Requisicion_Transitoria_Datos.Consulta_Requisicion_Transitoria(this);
        }
        public DataTable Consulta_Detalles_Requisicion_Transitorias()
        {
            return Cls_Ope_Com_Dividir_Requisicion_Transitoria_Datos.Consulta_Detalle_Requisicion_Transitoria(this);
        }
        public DataTable Consulta_Requisiciones_Generadas()
        {
            return Cls_Ope_Com_Dividir_Requisicion_Transitoria_Datos.Consultar_Requisiciones_Hijas(this);
        }
        public String Crear_Requisicion_Transitoria()
        {
            return Cls_Ope_Com_Dividir_Requisicion_Transitoria_Datos.Convertir_Requisicion_Transitoria(this);
        }
        public int Actualizar_Requisicion_Origen() 
        {
            return Cls_Ope_Com_Dividir_Requisicion_Transitoria_Datos.Modificar_Requisicion_Original(this);
        }
        #endregion


    }
}
