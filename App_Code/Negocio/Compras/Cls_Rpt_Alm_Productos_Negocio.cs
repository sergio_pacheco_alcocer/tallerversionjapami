﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Productos_Almacen.Datos;

/// <summary>
/// Summary description for Cls_Rpt_Alm_Productos_Negocio
/// </summary>
namespace JAPAMI.Productos_Almacen.Negocio
{
    public class Cls_Rpt_Alm_Productos_Negocio
    {
        public Cls_Rpt_Alm_Productos_Negocio()
        {
        }

        #region Variables Locales
            String Producto_ID;
            String Unidad_ID;
            String Estatus;
            String Tipo;
            String Stock;
            String Clave;
            Int64 Ref_JAPAMI;
            String Tipo_Consulta;
            String Impuesto_ID;
            String Partida_Generica_ID;
            String Partida_Especifica_ID;
            String Nombre;
        #endregion

        #region Variables Publicas
            public String P_Producto_ID
            {
                get { return Producto_ID; }
                set { Producto_ID = value; }
            }

            public String P_Unidad_ID
            {
                get { return Unidad_ID; }
                set { Unidad_ID = value; }
            }

            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }

            public String P_Tipo
            {
                get { return Tipo; }
                set { Tipo = value; }
            }

            public String P_Stock
            {
                get { return Stock; }
                set { Stock = value; }
            }

            public String P_Clave
            {
                get { return Clave; }
                set { Clave = value; }
            }

            public Int64 P_Ref_JAPAMI
            {
                get { return Ref_JAPAMI; }
                set { Ref_JAPAMI = value; }
            }

            public String P_Tipo_Consulta
            {
                get { return Tipo_Consulta; }
                set { Tipo_Consulta = value; }
            }

            public String P_Impuesto_ID
            {
                get { return Impuesto_ID; }
                set { Impuesto_ID = value; }
            }

            public String P_Partida_Generica_ID
            {
                get { return Partida_Generica_ID; }
                set { Partida_Generica_ID = value; }
            }

            public String P_Partida_Especifica_ID
            {
                get { return Partida_Especifica_ID; }
                set { Partida_Especifica_ID = value; }
            }

            public String P_Nombre
            {
                get { return Nombre; }
                set { Nombre = value; }
            }
        #endregion

        #region Metodos
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Unidades
            ///DESCRIPCION:             Consulta de las unidades
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              21/Marzo/2012 10:41
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consulta_Unidades()
            {
                return Cls_Rpt_Alm_Productos_Datos.Consulta_Unidades();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Impuestos
            ///DESCRIPCION:             Consulta de los impuestos
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              21/Marzo/2012 10:49
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consulta_Impuestos()
            {
                return Cls_Rpt_Alm_Productos_Datos.Consulta_Impuestos();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Tipos
            ///DESCRIPCION:             Consultar los tipos de Productos
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              21/Marzo/2012 11:22
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consulta_Tipos()
            {
                return Cls_Rpt_Alm_Productos_Datos.Consulta_Tipos();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Partidas_Genericas
            ///DESCRIPCION:             Consultar las partidas genericas
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              21/Marzo/2012 16:54
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consulta_Partidas_Genericas()
            {
                return Cls_Rpt_Alm_Productos_Datos.Consulta_Partidas_Genericas();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Partidas_Especificas
            ///DESCRIPCION:             Consultar las partidas especificas
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              21/Marzo/2012 17:22
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consulta_Partidas_Especificas()
            {
                return Cls_Rpt_Alm_Productos_Datos.Consulta_Partidas_Especificas(this);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Productos
            ///DESCRIPCION:             Consultar los productos con filtros
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              21/Marzo/2012 13:59
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consulta_Productos()
            {
                return Cls_Rpt_Alm_Productos_Datos.Consulta_Productos(this);
            }
        #endregion
    }
}