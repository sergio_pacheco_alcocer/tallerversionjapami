﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Archivos_Ordenes_Compra.Datos;

/// <summary>
/// Summary description for Cls_Ope_Archivos_Ordenes_Compra_Negocio
/// </summary>
namespace JAPAMI.Archivos_Ordenes_Compra.Negocios
{
    public class Cls_Ope_Archivos_Ordenes_Compra_Negocio
    {
        #region Variables Locales
            int No_Orden_Compra;
            DataTable Dt_Archivos;
            DataTable Dt_Archivos_Eliminados;
            Int64 No_Archivo;
        #endregion

        #region Variables Publicas
            public int P_No_Orden_Compra
            {
                get { return No_Orden_Compra; }
                set { No_Orden_Compra = value; }
            }

            public DataTable P_Dt_Archivos
            {
                get { return Dt_Archivos; }
                set { Dt_Archivos = value; }
            }

            public DataTable P_Dt_Archivos_Eliminados
            {
                get { return Dt_Archivos_Eliminados; }
                set { Dt_Archivos_Eliminados = value; }
            }

            public Int64 P_No_Archivo
            {
                get { return No_Archivo; }
                set { No_Archivo = value; }
            }
        #endregion
        
            public Cls_Ope_Archivos_Ordenes_Compra_Negocio()
        {
        }

        #region Metodos
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Alta_Archivos_Onden_Compra
            ///DESCRIPCION:             Dar de alta en la base de datos los nombres de los archivos a ingresar en la base de datos
            ///PARAMETROS:              
            ///CREO:                    David Herrera Rincon
            ///FECHA_CREO:              12/Enero/2013
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public void Alta_Archivos_Onden_Compra()
            {
                Cls_Ope_Archivos_Ordenes_Compra_Datos.Alta_Archivos_Onden_Compra(this);
            }


            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Eliminar_Archivos_Orden_Compra
            ///DESCRIPCION:             Eliminar los archivos de la Orden de Compra
            ///PARAMETROS:              
            ///CREO:                    David Herrera Rincon
            ///FECHA_CREO:              12/Enero/2013
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public void Eliminar_Archivos_Orden_Compra()
            {
                Cls_Ope_Archivos_Ordenes_Compra_Datos.Eliminar_Archivos_Orden_Compra(this);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Archivos
            ///DESCRIPCION:             hacer la consulta de los archivos de una Orden de Compra
            ///PARAMETROS:              
            ///CREO:                    David Herrera Rincon
            ///FECHA_CREO:              12/Enero/2013
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consulta_Archivos()
            {
                return Cls_Ope_Archivos_Ordenes_Compra_Datos.Consulta_Archivos(this);
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consultar_Datos_Archivo
            ///DESCRIPCION:             Consulta los datos de un archivo especifico
            ///PARAMETROS:              
            ///CREO:                    David Herrera Rincon
            ///FECHA_CREO:              12/Enero/2013
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consultar_Datos_Archivo() 
            {
                return Cls_Ope_Archivos_Ordenes_Compra_Datos.Consulta_Datos_Archivo(this);
            }

        #endregion
    }
}