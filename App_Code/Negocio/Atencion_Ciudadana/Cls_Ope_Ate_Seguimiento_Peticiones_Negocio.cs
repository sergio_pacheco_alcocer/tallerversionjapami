﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Seguimiento_Peticiones.Datos;
namespace JAPAMI.Seguimiento_Peticiones.Negocios
{
    public class Cls_Ope_Ate_Seguimiento_Peticiones_Negocio
    {
        #region Variables Internas
        private String Seguimiento_ID;
        private String Peticion_ID;
        private String Asunto_ID;
        private String Area_ID;
        private String Dependencia_ID;
        private String Observaciones;
        private String Fecha_Asignacion;
        #endregion

        #region Variables Publicas

        public String P_Seguimiento_ID
        {
            get { return Seguimiento_ID; }
            set { Seguimiento_ID = value; }
        }
        public String P_Peticion_ID
        {
            get { return Peticion_ID; }
            set { Peticion_ID = value; }
        }
        public String P_Asunto_ID
        {
            get { return Asunto_ID; }
            set { Asunto_ID = value; }
        }
        public String P_Area_ID
        {
            get { return Area_ID; }
            set { Area_ID = value; }
        }
        public String P_Dependencia_ID
        {
            get { return Dependencia_ID; }
            set { Dependencia_ID = value; }
        }
        public String P_Observaciones
        {
            get { return Observaciones; }
            set { Observaciones = value; }
        }
        public String P_Fecha_Asignacion
        {
            get { return Fecha_Asignacion; }
            set { Fecha_Asignacion = value; }
        }
        #endregion

        #region Metodos
        public DataSet Consultar_Seguimiento() {
            return Cls_Ope_Ate_Seguimiento_Peticiones_Datos.Consultar_Seguimiento(this);
        }
        public int Alta_Seguimiento() {
            return Cls_Ope_Ate_Seguimiento_Peticiones_Datos.Alta_Seguimiento(this);
        }
        #endregion
    }
}
