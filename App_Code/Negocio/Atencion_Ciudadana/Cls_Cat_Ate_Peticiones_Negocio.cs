﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Registro_Peticion.Datos;
namespace JAPAMI.Registro_Peticion.Negocios
{
    public class Cls_Cat_Ate_Peticiones_Negocio
    {
        #region Variables Locales
        //Propiedades de la tabla Ope_Ate_Peticiones
        private String Peticion_ID;
        private String Asunto_ID;
        private String Folio;
        private String Estatus;
        private String Peticion;
        private String Fecha_Peticion;
        private String Fecha_Solucion_Probable;
        private String Fecha_Solucion_Real;
        private String Nivel_Importancia;
        private String Nombre;
        private String Apellido_Paterno;
        private String Apellido_Materno;
        private String Calle_No;
        private String Colonia_ID;
        private String Localidad;
        private int Edad;
        private String Sexo;
        private String Telefono;
        private String Email;
        private String Descripcion_Solucion;
        private String Genera_Noticia;
        private String Codigo_Postal;
        private String Origen;
        
        private String Dependencia_ID;
        private String Descripcion_Cambio;
        private String Fecha_Asignacion_Cambio;
        private String Area_ID;

        private String Usuario_Creo;
        private String Fecha_Creo;
        private String Usuario_Modifico;
        private String Fecha_Modifico;
        
        private String Asignado;

        private String Fecha_Inicio;
        private String Fecha_Final;

        #endregion

        #region Variables Publicas
        public String P_Asignado
        {
            get { return Asignado; }
            set { Asignado = value; }
        }
        public String P_Peticion_ID
        {
            get { return Peticion_ID; }
            set { Peticion_ID = value; }
        }

        public String P_Asunto_ID
        {
            get { return Asunto_ID; }
            set { Asunto_ID = value; }
        }

        public String P_Folio
        {
            get { return Folio; }
            set { Folio = value; }
        }

        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }

        public String P_Peticion
        {
            get { return Peticion; }
            set { Peticion = value; }
        }

        public String P_Fecha_Peticion
        {
            get { return Fecha_Peticion; }
            set { Fecha_Peticion = value; }
        }

        public String P_Fecha_Solucion_Probable
        {
            get { return Fecha_Solucion_Probable; }
            set { Fecha_Solucion_Probable = value; }
        }

        public String P_Fecha_Solucion_Real
        {
            get { return Fecha_Solucion_Real; }
            set { Fecha_Solucion_Real = value; }
        }

        public String P_Nivel_Importancia
        {
            get { return Nivel_Importancia; }
            set { Nivel_Importancia = value; }
        }
        public String P_Nombre
        {
            get { return Nombre; }
            set { Nombre = value; }
        }
        public String P_Apellido_Paterno
        {
            get { return Apellido_Paterno; }
            set { Apellido_Paterno = value; }
        }
        public String P_Apellido_Materno
        {
            get { return Apellido_Materno; }
            set { Apellido_Materno = value; }
        }
        public String P_Calle_No
        {
            get { return Calle_No; }
            set { Calle_No = value; }
        }
        public String P_Colonia_ID
        {
            get { return Colonia_ID; }
            set { Colonia_ID = value; }
        }
        public String P_Localidad
        {
            get { return Localidad; }
            set { Localidad = value; }
        }
        public int P_Edad
        {
            get { return Edad; }
            set { Edad = value; }
        }
        public String P_Sexo
        {
            get { return Sexo; }
            set { Sexo = value; }
        }
        public String P_Telefono
        {
            get { return Telefono; }
            set { Telefono = value; }
        }

        public String P_Email
        {
            get { return Email; }
            set { Email = value; }
        }
        public String P_Descripcion_Solucion
        {
            get { return Descripcion_Solucion; }
            set { Descripcion_Solucion = value; }
        }
        public String P_Genera_Noticia
        {
            get { return Genera_Noticia; }
            set { Genera_Noticia = value; }
        }

        public String P_Codigo_Postal
        {
            get { return Codigo_Postal; }
            set { Codigo_Postal = value; }
        }

        public String P_Origen
        {
            get { return Origen; }
            set { Origen = value; }
        }

        //
        public String P_Dependencia_ID
        {
            get { return Dependencia_ID; }
            set { Dependencia_ID = value; }
        }

        public String P_Descripcion_Cambio
        {
            get { return Descripcion_Cambio; }
            set { Descripcion_Cambio = value; }
        }

        public String P_Fecha_Asignacion_Cambio
        {
            get { return Fecha_Asignacion_Cambio; }
            set { Fecha_Asignacion_Cambio = value; }
        }

        public String P_Area_ID
        {
            get { return Area_ID; }
            set { Area_ID = value; }
        }
        public String P_Usuario_Creo
        {
            get { return Usuario_Creo; }
            set { Usuario_Creo = value; }
        }
        public String P_Fecha_Creo
        {
            get { return Fecha_Creo; }
            set { Fecha_Creo = value; }
        }
        public String P_Usuario_Modifico
        {
            get { return Usuario_Modifico; }
            set { Usuario_Modifico = value; }
        }
        public String P_Fecha_Modifico
        {
            get { return Fecha_Modifico; }
            set { Fecha_Modifico = value; }
        }
        public String P_Fecha_Inicio
        {
            get { return Fecha_Inicio; }
            set { Fecha_Inicio = value; }
        }
        public String P_Fecha_Final
        {
            get { return Fecha_Final; }
            set { Fecha_Final = value; }
        }


        #endregion

        #region Metodos

        public void Alta_Peticion()
        {
            Cls_Cat_Ate_Peticiones_Datos.Alta_Peticion(this);
        }
        public void Alta_Peticion_Ciudadano()
        {
            Cls_Cat_Ate_Peticiones_Datos.Alta_Peticion_Ciudadano(this);
        }
        public void Modificar_Peticion()
        {
            Cls_Cat_Ate_Peticiones_Datos.Modificar_Peticion(this);
        }
        public DataTable Consulta_Peticion()
        {
            return Cls_Cat_Ate_Peticiones_Datos.Consulta_Peticion(this);
        }
        public DataTable Consulta_Peticion_Folio()
        {
            return null;
        }
        public DataTable Consulta_Peticion_Bandeja()
        {
            return Cls_Cat_Ate_Peticiones_Datos.Consulta_Peticion_Bandeja(this);
        }
        public DataTable Consulta_Peticion_Bandeja_No_Asignados()
        {
            return Cls_Cat_Ate_Peticiones_Datos.Consultar_Peticion_Bandeja_No_Asignados(this);
        } 
        public void Modificar_Peticion_Reasignacion()
        {
            Cls_Cat_Ate_Peticiones_Datos.Modificar_Peticion_Reasignacion(this);
        }
        public void Modificar_Peticion_Solucion()
        {
            Cls_Cat_Ate_Peticiones_Datos.Modificar_Peticion_Solucion(this);
        }
        public DataTable Consulta_Peticion_Respuesta()
        {
            return Cls_Cat_Ate_Peticiones_Datos.Consulta_Peticion_Respuesta(this);
        }
        public DataTable Consulta_Peticion_Correo_Solucion()
        {
            return Cls_Cat_Ate_Peticiones_Datos.Consulta_Peticion_Correo_Solucion(this);
        }
        public DataTable Consulta_Peticion_Seguimiento()
        {
            return Cls_Cat_Ate_Peticiones_Datos.Consulta_Peticion_Seguimiento(this);
        }
        public string Consulta_Folio()
        {
            return Cls_Cat_Ate_Peticiones_Datos.Consulta_Folio();
        }
#endregion            
        
    }
}
