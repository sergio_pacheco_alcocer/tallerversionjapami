﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace JAPAMI.Control_Patrimonial_Parametros.Autocompletados.Negocio
{
    public class Cls_Ope_Pat_Autocompletados_Negocio
    {
        public Cls_Ope_Pat_Autocompletados_Negocio()
        {
        }
        public static string RequstForm(string name)
        {
            return (HttpContext.Current.Request.Form[name] == null ? String.Empty : HttpContext.Current.Request.Form[name].ToString().Trim());
        }
        public static string RequstString(string sParam)
        {
            return (HttpContext.Current.Request[sParam] == null ? String.Empty : HttpContext.Current.Request[sParam].ToString().Trim());
        }
        public static int RequstInt(string sParam)
        {
            int iValue;
            String sValue = RequstString(sParam);
            int.TryParse(sValue, out iValue);
            return iValue;
        }
        public static string accion
        {
            get { return RequstString("accion"); }
        }
        public static string Obtener_Letra
        {
            get { return RequstString("q"); }
        }
    }
}