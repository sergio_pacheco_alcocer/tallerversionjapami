﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Control_Patrimonial_Operacion_Depreciacion_Bienes.Datos;
/// <summary>
/// Summary description for Cls_Ope_Pat_Depreciacion_Bienes_Negocio
/// </summary>
/// 
namespace JAPAMI.Control_Patrimonial_Operacion_Depreciacion_Bienes.Negocio {
    public class Cls_Ope_Pat_Depreciacion_Bienes_Negocio {

        #region Variables Internas

            private String Tipo_Activo_ID = null;
            private String Clase_Activo_ID = null;
            private String Estatus = null;
            private String Tipo_Bien = null;
            private String Usuario_ID = null;
            private String Nombre_Usuario = null;
            private DataTable Dt_Bienes_Depreciar = null;
            private DateTime Fecha_Limite = new DateTime();
            private Int32 Valor_Minimo = -1;
             
        #endregion

        #region Variables Publicas

            public String P_Tipo_Activo_ID
            {
                get { return Tipo_Activo_ID; }
                set { Tipo_Activo_ID = value; }
            }
            public String P_Clase_Activo_ID
            {
                get { return Clase_Activo_ID; }
                set { Clase_Activo_ID = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public DataTable P_Dt_Bienes_Depreciar
            {
                get { return Dt_Bienes_Depreciar; }
                set { Dt_Bienes_Depreciar = value; }
            }
            public String P_Tipo_Bien
            {
                get { return Tipo_Bien; }
                set { Tipo_Bien = value; }
            }
            public String P_Usuario_ID
            {
                get { return Usuario_ID; }
                set { Usuario_ID = value; }
            }
            public String P_Nombre_Usuario
            {
                get { return Nombre_Usuario; }
                set { Nombre_Usuario = value; }
            }
            public DateTime P_Fecha_Limite {
                set { Fecha_Limite = value; }
                get { return Fecha_Limite; }
            }
            public Int32 P_Valor_Minimo
            {
                get { return Valor_Minimo; }
                set { Valor_Minimo = value; }
            }

        #endregion

        #region Metodos

            public DataTable Consultar_Bienes_Muebles() {
                return Cls_Ope_Pat_Depreciacion_Bienes_Datos.Consultar_Bienes_Muebles(this);
            }
            public DataTable Consultar_Vehiculos() {
                return Cls_Ope_Pat_Depreciacion_Bienes_Datos.Consultar_Vehiculos(this);
            }
            public String[] Actualizar_Bienes_Depreciados()
            {
                 return Cls_Ope_Pat_Depreciacion_Bienes_Datos.Actualizar_Bienes_Depreciados(this);
            }
            public DataTable Consultar_Bienes_Inmuebles()
            {
                return Cls_Ope_Pat_Depreciacion_Bienes_Datos.Consultar_Bienes_Inmuebles(this);
            }

        #endregion	

	}
}
