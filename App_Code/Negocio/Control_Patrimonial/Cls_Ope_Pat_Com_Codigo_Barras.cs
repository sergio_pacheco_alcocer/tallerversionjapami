﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.IO;

/// <summary>
/// Summary description for Cls_Ope_Pat_Com_Codigo_Barras
/// </summary>
namespace JAPAMI.Control_Patrimonial_Codigo_Barras.Ayudante
{
    public class Cls_Ope_Pat_Com_Codigo_Barras
    {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: testnum
        ///DESCRIPCIÓN: testnum
        ///PROPIEDADES:    
        ///CREO: Sergio Pacheco
        ///FECHA_CREO: Abril/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void testnum(ref int mini, string chaine, int i)
        {
            char letra;
            mini = mini - 1;
            if (i + mini < chaine.Length)
            {
                while (mini >= 0)
                {
                    letra = Convert.ToChar(chaine.Substring(i + mini, 1));
                    if (letra < 48 || letra > 57)
                    {
                        break;
                    }

                    mini = mini - 1;
                }
            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: code128
        ///DESCRIPCIÓN: code128
        ///PROPIEDADES:    
        ///CREO: Sergio Pacheco
        ///FECHA_CREO: Abril/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static string code128(string chaine)
        {

            string respuesta = "";
            char letra;
            int i = 0;
            long checksum = 0;
            int mini = 0;
            int dummy = 0;
            bool tableB = false;

            if (chaine.Length > 0)
            {

                //Vérifier si caractères valides
                //Check for valid characters


                for (i = 0; i < chaine.Length; i++)
                { //inicio for
                    letra = Convert.ToChar(chaine.Substring(i, 1));

                    if ((letra >= 32 && letra <= 126) || letra == 203)
                    {

                    }
                    else
                    {
                        i = 0;
                        break;
                    }
                } //fin for

                respuesta = "";
                tableB = true;

                if (i > 0)
                {
                    i = 0;
                    while (i < chaine.Length)
                    {

                        if (tableB)
                        {

                            mini = (i == 0 || i + 3 == chaine.Length ? 4 : 6);
                            testnum(ref mini, chaine, i);
                            if (mini < 0)
                            {
                                if (i == 0)
                                    respuesta = Convert.ToString(Convert.ToChar(210));
                                else
                                    respuesta = respuesta + Convert.ToString(Convert.ToChar(204));

                                tableB = false;
                            }
                            else
                            {
                                if (i == 0)
                                    respuesta = Convert.ToString(Convert.ToChar(209));
                            }

                        } // fin table b  

                        if (!tableB)
                        {
                            mini = 2;
                            testnum(ref mini, chaine, i);
                            if (mini < 0)
                            {
                                dummy = Convert.ToInt32(chaine.Substring(i, 2));
                                dummy = (dummy < 95 ? dummy + 32 : dummy + 105);
                                respuesta = respuesta + Convert.ToString(Convert.ToChar(dummy));
                                i = i + 2;
                            }
                            else
                            {
                                respuesta = respuesta + Convert.ToString(Convert.ToChar(205));
                                tableB = true;
                            }


                        } // fin table b negado


                        if (tableB)
                        {
                            respuesta = respuesta + chaine.Substring(i, 1);
                            i = i + 1;
                        }



                    } // fin while


                    for (i = 0; i < respuesta.Length; i++)
                    {

                        dummy = Convert.ToChar(respuesta.Substring(i, 1));
                        dummy = (dummy < 127 ? dummy - 32 : dummy - 105);
                        if (i == 0)
                            checksum = dummy;
                        checksum = (checksum + (i) * dummy) % 103;

                    }

                    checksum = (checksum < 95 ? checksum + 32 : checksum + 105);

                    respuesta = respuesta + Convert.ToString(Convert.ToChar(checksum)) + Convert.ToString(Convert.ToChar(211));

                } // fin segundo if


            }// fin primer if
            return respuesta;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Imagen_Codigo_Barras
        ///DESCRIPCIÓN: Imagen_Codigo_Barras
        ///PROPIEDADES:    
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: Abril/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static Byte[] Imagen_Codigo_Barras(String Codigo_Barras) {
            Byte[] Img_Bytes = null;
            try {
                Bitmap Imagen_MapaBits = CrearCodigo(Codigo_Barras);
                MemoryStream ms = new MemoryStream();
                Imagen_MapaBits.Save(ms, ImageFormat.Png);
                Img_Bytes = ms.ToArray();
            } catch (Exception Ex) {
                throw new Exception(Ex.Message);
            }
            return Img_Bytes;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: CrearCodigo
        ///DESCRIPCIÓN: CrearCodigo
        ///PROPIEDADES:    
        ///CREO: Sergio Pacheco
        ///FECHA_CREO: Abril/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static Bitmap CrearCodigo(string Code)
        {

            string text = code128(Code);
            Bitmap bitmap = new Bitmap(1, 1);
            Font font = new Font("Code 128", 800, FontStyle.Regular, GraphicsUnit.Pixel);
            Graphics graphics = Graphics.FromImage(bitmap);
            int width = (int)graphics.MeasureString(text, font).Width;
            int height = (int)graphics.MeasureString(text, font).Height;
            bitmap = new Bitmap(bitmap, new Size(width, height));
            graphics = Graphics.FromImage(bitmap);
            graphics.Clear(Color.White);
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            graphics.DrawString(text, font, new SolidBrush(Color.FromArgb(0, 0, 0)), 0, 0);
            graphics.Flush();

            return bitmap;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Imagen_Codigo_Barras
        ///DESCRIPCIÓN: Imagen_Codigo_Barras
        ///PROPIEDADES:    
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: Abril/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static String Guardar_Imagen_Codigo_Barras(String Codigo_Barras)
        {
            String Url_Imagen = "C:\\PROYECTOS_CONTEL\\JAPAMI\\BarCode_" + Codigo_Barras + ".png";
            try
            {
                Bitmap Imagen_MapaBits = CrearCodigo(Codigo_Barras);
                MemoryStream ms = new MemoryStream();
                Imagen_MapaBits.Save(Url_Imagen, ImageFormat.Png);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Url_Imagen;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Codigo_Barras_Embebido
        ///DESCRIPCIÓN: Obtener_Codigo_Barras_Embebido
        ///PROPIEDADES:    
        ///CREO: Antonio Salvador Benavides Guardado
        ///FECHA_CREO: Abril/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static String Obtener_Codigo_Barras_Embebido(Bitmap Bitmap_Codigo_Barras, ImageFormat Extension_Imagen)
        {
            MemoryStream Mem_Stream = new MemoryStream();
            String B64_String_Codigo_Barras = "";
            String Codigo_Barras_Embebido = "";

            Bitmap_Codigo_Barras.Save(Mem_Stream, Extension_Imagen);
            Byte[] Bytes_Bitmap_Codigo_Barras = Mem_Stream.ToArray();
            B64_String_Codigo_Barras =
            Convert.ToBase64String(Bytes_Bitmap_Codigo_Barras);
            Codigo_Barras_Embebido = "data:image/" + Extension_Imagen + ";base64," + B64_String_Codigo_Barras;
            return Codigo_Barras_Embebido;
        }


    }
}