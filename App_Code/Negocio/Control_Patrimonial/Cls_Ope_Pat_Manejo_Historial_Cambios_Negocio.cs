﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Control_Patrimonial_Operacion_Bienes_Muebles.Negocio;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using JAPAMI.Control_Patrimonial_Bienes_Economicos.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clases_Activos.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clasificaciones.Negocio;
using JAPAMI.Grupos_Dependencias.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Control_Patrimonial_Catalogo_Zonas.Negocio;
using JAPAMI.Catalogo_Compras_Marcas.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Materiales.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Colores.Negocio;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Combustible.Negocio;
using JAPAMI.Control_Patrimonial.Manejo_Historial_Cambios.Datos;
using JAPAMI.Control_Patrimonial_Parametros.Autocompletados.Datos;

/// <summary>
/// Summary description for Cls_Ope_Pat_Manejo_Historial_Cambios_Negocio
/// </summary>
namespace JAPAMI.Control_Patrimonial.Manejo_Historial_Cambios.Negocio {
    public class Cls_Ope_Pat_Manejo_Historial_Cambios_Negocio {

        #region Variables Internas

            private String Bien_ID;
            private String Tipo_Bien;
            private Cls_Ope_Pat_Com_Bienes_Muebles_Negocio BM_Anterior = null;
            private Cls_Ope_Pat_Com_Bienes_Muebles_Negocio BM_Actualizado = null;
            private Cls_Ope_Pat_Com_Vehiculos_Negocio VH_Anterior = null;
            private Cls_Ope_Pat_Com_Vehiculos_Negocio VH_Actualizado = null;
            private Cls_Ope_Pat_Bienes_Economicos_Negocio BE_Anterior = null;
            private Cls_Ope_Pat_Bienes_Economicos_Negocio BE_Actualizado = null;
            private DataTable Campos_Comparados = null;

        #endregion

        #region Variables Internas

            public String P_Bien_ID
            {
                set { Bien_ID = value; }
                get { return Bien_ID; }
            }
            public String P_Tipo_Bien
            {
                set { Tipo_Bien = value; }
                get { return Tipo_Bien; }
            }
            public Cls_Ope_Pat_Com_Bienes_Muebles_Negocio P_BM_Anterior
            {
                set { BM_Anterior = value; }
                get { return BM_Anterior; }
            }
            public Cls_Ope_Pat_Com_Bienes_Muebles_Negocio P_BM_Actualizado
            {
                set { BM_Actualizado = value; }
                get { return BM_Actualizado; }
            }
            public Cls_Ope_Pat_Com_Vehiculos_Negocio P_VH_Anterior
            {
                set { VH_Anterior = value; }
                get { return VH_Anterior; }
            }
            public Cls_Ope_Pat_Com_Vehiculos_Negocio P_VH_Actualizado
            {
                set { VH_Actualizado = value; }
                get { return VH_Actualizado; }
            }
            public DataTable P_Campos_Comparados
            {
                set { Campos_Comparados = value; }
                get { return Campos_Comparados; }
            }
            public Cls_Ope_Pat_Bienes_Economicos_Negocio P_BE_Anterior
            {
                set { BE_Anterior = value; }
                get { return BE_Anterior; }
            }
            public Cls_Ope_Pat_Bienes_Economicos_Negocio P_BE_Actualizado
            {
                set { BE_Actualizado= value; }
                get { return BE_Actualizado; }
            }

        #endregion

        #region Metodos

            public DataTable Obtener_Tabla_Cambios()
            {
                DataTable Dt_Cambios = Estructura_DataTable_Cambios();
                if (Tipo_Bien.Equals("BIEN_MUEBLE"))
                {
                    if (P_BM_Anterior == null)
                    {
                        P_BM_Anterior = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
                        P_BM_Anterior.P_Bien_Mueble_ID = Bien_ID;
                        P_BM_Anterior = P_BM_Anterior.Consultar_Detalles_Bien_Mueble();
                    }

                    if (P_BM_Actualizado != null)
                    {
                        ///Cuenta Contable
                        if (!P_BM_Actualizado.P_Cuenta_Contable_ID.Trim().Equals(P_BM_Anterior.P_Cuenta_Contable_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Clase de Activo";
                            if (P_BM_Actualizado.P_Cuenta_Contable_ID.Trim().Length > 0) {
                                DataTable Dt_Temporal = Cls_Ope_Pat_Autocompletados_Datos.Consultar_Cuentas_Contables("", P_BM_Actualizado.P_Cuenta_Contable_ID.Trim());
                                if (Dt_Temporal != null) if (Dt_Temporal.Rows.Count > 0) Valor_Nuevo = Dt_Temporal.Rows[0]["cuenta_descripcion"].ToString().Trim();
                            }
                            if (P_BM_Anterior.P_Cuenta_Contable_ID.Trim().Length > 0)
                            {
                                DataTable Dt_Temporal = Cls_Ope_Pat_Autocompletados_Datos.Consultar_Cuentas_Contables("", P_BM_Anterior.P_Cuenta_Contable_ID.Trim());
                                if (Dt_Temporal != null) if (Dt_Temporal.Rows.Count > 0) Valor_Anterior = Dt_Temporal.Rows[0]["cuenta_descripcion"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Clase de Activo
                        if (!P_BM_Actualizado.P_Clase_Activo_ID.Trim().Equals(P_BM_Anterior.P_Clase_Activo_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Clase de Activo";
                            Cls_Cat_Pat_Com_Clases_Activo_Negocio Catalogo_Negocio = new Cls_Cat_Pat_Com_Clases_Activo_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "CLASES_ACTIVOS";
                            Catalogo_Negocio.P_Clase_Activo_ID = P_BM_Anterior.P_Clase_Activo_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["CLAVE_DESCRIPCION"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Pat_Com_Clases_Activo_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "CLASES_ACTIVOS";
                            Catalogo_Negocio.P_Clase_Activo_ID = P_BM_Actualizado.P_Clase_Activo_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["CLAVE_DESCRIPCION"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Tipo de Activo
                        if (!P_BM_Actualizado.P_Clasificacion_ID.Trim().Equals(P_BM_Anterior.P_Clasificacion_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Tipo de Activo";
                            Cls_Cat_Pat_Com_Clasificaciones_Negocio Catalogo_Negocio = new Cls_Cat_Pat_Com_Clasificaciones_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "CLASIFICACIONES";
                            Catalogo_Negocio.P_Clasificacion_ID = P_BM_Anterior.P_Clasificacion_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["CLAVE_DESCRIPCION"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Pat_Com_Clasificaciones_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "CLASIFICACIONES";
                            Catalogo_Negocio.P_Clasificacion_ID = P_BM_Actualizado.P_Clasificacion_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["CLAVE_DESCRIPCION"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Gerencia
                        if (!P_BM_Actualizado.P_Gerencia_ID.Trim().Equals(P_BM_Anterior.P_Gerencia_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Gerencia";
                            Cls_Cat_Grupos_Dependencias_Negocio Catalogo_Negocio = new Cls_Cat_Grupos_Dependencias_Negocio();
                            Catalogo_Negocio.P_Grupo_Dependencia_ID = P_BM_Anterior.P_Gerencia_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_Grupos_Dependencias();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0][Cat_Grupos_Dependencias.Campo_Nombre].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Grupos_Dependencias_Negocio();
                            Catalogo_Negocio.P_Grupo_Dependencia_ID = P_BM_Actualizado.P_Gerencia_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_Grupos_Dependencias();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0][Cat_Grupos_Dependencias.Campo_Nombre].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Unidad Responsable
                        if (!P_BM_Actualizado.P_Dependencia_ID.Trim().Equals(P_BM_Anterior.P_Dependencia_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Unidad Responsable";
                            Cls_Cat_Dependencias_Negocio Catalogo_Negocio = new Cls_Cat_Dependencias_Negocio();
                            Catalogo_Negocio.P_Dependencia_ID = P_BM_Anterior.P_Dependencia_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consulta_Dependencias();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["CLAVE_NOMBRE"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Dependencias_Negocio();
                            Catalogo_Negocio.P_Dependencia_ID = P_BM_Actualizado.P_Dependencia_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consulta_Dependencias();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["CLAVE_NOMBRE"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Zona
                        if (!P_BM_Actualizado.P_Zona.Trim().Equals(P_BM_Anterior.P_Zona.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Ubicación Física";
                            Cls_Cat_Pat_Com_Zonas_Negocio Catalogo_Negocio = new Cls_Cat_Pat_Com_Zonas_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "ZONAS";
                            Catalogo_Negocio.P_Zona_ID = P_BM_Anterior.P_Zona.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Pat_Com_Zonas_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "ZONAS";
                            Catalogo_Negocio.P_Zona_ID = P_BM_Actualizado.P_Zona.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Número de Serie
                        if (!P_BM_Actualizado.P_Numero_Serie.Trim().Equals(P_BM_Anterior.P_Numero_Serie.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Número de Serie";
                            Valor_Anterior = P_BM_Anterior.P_Numero_Serie.Trim();
                            Valor_Nuevo = P_BM_Actualizado.P_Numero_Serie.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Marca
                        if (!P_BM_Actualizado.P_Marca_ID.Trim().Equals(P_BM_Anterior.P_Marca_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Marca";
                            Cls_Cat_Com_Marcas_Negocio Catalogo_Negocio = new Cls_Cat_Com_Marcas_Negocio();
                            Catalogo_Negocio.P_Marca_ID = P_BM_Anterior.P_Marca_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consulta_Marcas();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0][Cat_Com_Marcas.Campo_Nombre].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Com_Marcas_Negocio();
                            Catalogo_Negocio.P_Marca_ID = P_BM_Actualizado.P_Marca_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consulta_Marcas();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0][Cat_Com_Marcas.Campo_Nombre].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Modelo
                        if (!P_BM_Actualizado.P_Modelo.Trim().Equals(P_BM_Anterior.P_Modelo.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Modelo";
                            Valor_Anterior = P_BM_Anterior.P_Modelo.Trim();
                            Valor_Nuevo = P_BM_Actualizado.P_Modelo.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Material
                        if (!P_BM_Actualizado.P_Material_ID.Trim().Equals(P_BM_Anterior.P_Material_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Material";
                            Cls_Cat_Pat_Com_Materiales_Negocio Catalogo_Negocio = new Cls_Cat_Pat_Com_Materiales_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "MATERIALES";
                            Catalogo_Negocio.P_Material_ID = P_BM_Anterior.P_Material_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Pat_Com_Materiales_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "MATERIALES";
                            Catalogo_Negocio.P_Material_ID = P_BM_Actualizado.P_Material_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Color
                        if (!P_BM_Actualizado.P_Color_ID.Trim().Equals(P_BM_Anterior.P_Color_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Color";
                            Cls_Cat_Pat_Com_Colores_Negocio Catalogo_Negocio = new Cls_Cat_Pat_Com_Colores_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "COLORES";
                            Catalogo_Negocio.P_Color_ID = P_BM_Anterior.P_Color_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Pat_Com_Colores_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "COLORES";
                            Catalogo_Negocio.P_Color_ID = P_BM_Actualizado.P_Color_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Fecha de Adquisicion
                        if (!String.Format("{0:ddMMyyyy}", P_BM_Actualizado.P_Fecha_Adquisicion_).Trim().Equals(String.Format("{0:ddMMyyyy}", P_BM_Anterior.P_Fecha_Adquisicion_).Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Fecha de Adquisición";
                            Valor_Anterior = String.Format("{0:dd/MM/yyyy}", P_BM_Anterior.P_Fecha_Adquisicion_).Trim();
                            Valor_Nuevo = String.Format("{0:dd/MM/yyyy}", P_BM_Actualizado.P_Fecha_Adquisicion_).Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Proveedor
                        if (!P_BM_Actualizado.P_Razon_Social_Proveedor.Trim().Equals(P_BM_Anterior.P_Razon_Social_Proveedor.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Proveedor";
                            Valor_Anterior = P_BM_Anterior.P_Razon_Social_Proveedor.Trim();
                            Valor_Nuevo = P_BM_Actualizado.P_Razon_Social_Proveedor.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Costo Inicial
                        if (!P_BM_Actualizado.P_Costo_Inicial.ToString().Trim().Equals(P_BM_Anterior.P_Costo_Inicial.ToString().Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Costo Inicial";
                            Valor_Anterior = String.Format("{0:c}", P_BM_Anterior.P_Costo_Inicial).Trim();
                            Valor_Nuevo = String.Format("{0:c}", P_BM_Actualizado.P_Costo_Inicial).Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Estatus
                        if (!P_BM_Actualizado.P_Estatus.Trim().Equals(P_BM_Anterior.P_Estatus.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Estatus";
                            Valor_Anterior = P_BM_Anterior.P_Estatus.Trim();
                            Valor_Nuevo = P_BM_Actualizado.P_Estatus.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Estado
                        if (!P_BM_Actualizado.P_Estado.Trim().Equals(P_BM_Anterior.P_Estado.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Estado";
                            Valor_Anterior = P_BM_Anterior.P_Estado.Trim();
                            Valor_Nuevo = P_BM_Actualizado.P_Estado.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Garantia
                        if (!P_BM_Actualizado.P_Garantia.Trim().Equals(P_BM_Anterior.P_Garantia.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Garantia";
                            Valor_Anterior = P_BM_Anterior.P_Garantia.Trim();
                            Valor_Nuevo = P_BM_Actualizado.P_Garantia.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///No. Inventario Anterio
                        if (!P_BM_Actualizado.P_Numero_Inventario_Anterior.Trim().Equals(P_BM_Anterior.P_Numero_Inventario_Anterior.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "No. Inventario Anterior";
                            Valor_Anterior = P_BM_Anterior.P_Numero_Inventario_Anterior.Trim();
                            Valor_Nuevo = P_BM_Actualizado.P_Numero_Inventario_Anterior.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Observaciones
                        if (!P_BM_Actualizado.P_Observaciones.Trim().Equals(P_BM_Anterior.P_Observaciones.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Observaciones";
                            Valor_Anterior = P_BM_Anterior.P_Observaciones.Trim();
                            Valor_Nuevo = P_BM_Actualizado.P_Observaciones.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Motivo de Baja
                        if (!P_BM_Actualizado.P_Motivo_Baja.Trim().Equals(P_BM_Anterior.P_Motivo_Baja.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Motivo de Baja";
                            Valor_Anterior = P_BM_Anterior.P_Motivo_Baja.Trim();
                            Valor_Nuevo = P_BM_Actualizado.P_Motivo_Baja.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                    }
                }
                else if (Tipo_Bien.Equals("VEHICULO"))
                {
                    if (P_VH_Anterior == null)
                    {
                        P_VH_Anterior = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
                        P_VH_Anterior.P_Vehiculo_ID = Bien_ID;
                        P_VH_Anterior = P_VH_Anterior.Consultar_Detalles_Vehiculo();
                    }
                    if (P_VH_Actualizado != null)
                    {
                        ///Cuenta Contable
                        if (!P_VH_Actualizado.P_Cuenta_Contable_ID.Trim().Equals(P_VH_Anterior.P_Cuenta_Contable_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Clase de Activo";
                            if (P_VH_Actualizado.P_Cuenta_Contable_ID.Trim().Length > 0)
                            {
                                DataTable Dt_Temporal = Cls_Ope_Pat_Autocompletados_Datos.Consultar_Cuentas_Contables("", P_VH_Actualizado.P_Cuenta_Contable_ID.Trim());
                                if (Dt_Temporal != null) if (Dt_Temporal.Rows.Count > 0) Valor_Nuevo = Dt_Temporal.Rows[0]["cuenta_descripcion"].ToString().Trim();
                            }
                            if (P_VH_Anterior.P_Cuenta_Contable_ID.Trim().Length > 0)
                            {
                                DataTable Dt_Temporal = Cls_Ope_Pat_Autocompletados_Datos.Consultar_Cuentas_Contables("", P_VH_Anterior.P_Cuenta_Contable_ID.Trim());
                                if (Dt_Temporal != null) if (Dt_Temporal.Rows.Count > 0) Valor_Anterior = Dt_Temporal.Rows[0]["cuenta_descripcion"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Clase de Activo
                        if (!P_VH_Actualizado.P_Clase_Activo_ID.Trim().Equals(P_VH_Anterior.P_Clase_Activo_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Clase de Activo";
                            Cls_Cat_Pat_Com_Clases_Activo_Negocio Catalogo_Negocio = new Cls_Cat_Pat_Com_Clases_Activo_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "CLASES_ACTIVOS";
                            Catalogo_Negocio.P_Clase_Activo_ID = P_VH_Anterior.P_Clase_Activo_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["CLAVE_DESCRIPCION"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Pat_Com_Clases_Activo_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "CLASES_ACTIVOS";
                            Catalogo_Negocio.P_Clase_Activo_ID = P_VH_Actualizado.P_Clase_Activo_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["CLAVE_DESCRIPCION"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Clase de Activo
                        if (!P_VH_Actualizado.P_Clasificacion_ID.Trim().Equals(P_VH_Anterior.P_Clasificacion_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Tipo de Activo";
                            Cls_Cat_Pat_Com_Clasificaciones_Negocio Catalogo_Negocio = new Cls_Cat_Pat_Com_Clasificaciones_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "CLASIFICACIONES";
                            Catalogo_Negocio.P_Clasificacion_ID = P_VH_Anterior.P_Clasificacion_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["CLAVE_DESCRIPCION"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Pat_Com_Clasificaciones_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "CLASIFICACIONES";
                            Catalogo_Negocio.P_Clasificacion_ID = P_VH_Actualizado.P_Clasificacion_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["CLAVE_DESCRIPCION"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Gerencia
                        if (!P_VH_Actualizado.P_Gerencia_ID.Trim().Equals(P_VH_Anterior.P_Gerencia_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Gerencia";
                            Cls_Cat_Grupos_Dependencias_Negocio Catalogo_Negocio = new Cls_Cat_Grupos_Dependencias_Negocio();
                            Catalogo_Negocio.P_Grupo_Dependencia_ID = P_VH_Anterior.P_Gerencia_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_Grupos_Dependencias();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0][Cat_Grupos_Dependencias.Campo_Nombre].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Grupos_Dependencias_Negocio();
                            Catalogo_Negocio.P_Grupo_Dependencia_ID = P_VH_Actualizado.P_Gerencia_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_Grupos_Dependencias();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0][Cat_Grupos_Dependencias.Campo_Nombre].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Unidad Responsable
                        if (!P_VH_Actualizado.P_Dependencia_ID.Trim().Equals(P_VH_Anterior.P_Dependencia_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Unidad Responsable";
                            Cls_Cat_Dependencias_Negocio Catalogo_Negocio = new Cls_Cat_Dependencias_Negocio();
                            Catalogo_Negocio.P_Dependencia_ID = P_VH_Anterior.P_Dependencia_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consulta_Dependencias();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["CLAVE_NOMBRE"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Dependencias_Negocio();
                            Catalogo_Negocio.P_Dependencia_ID = P_VH_Actualizado.P_Dependencia_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consulta_Dependencias();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["CLAVE_NOMBRE"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Zona
                        if (!P_VH_Actualizado.P_Zona_ID.Trim().Equals(P_VH_Anterior.P_Zona_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Ubicación Física";
                            Cls_Cat_Pat_Com_Zonas_Negocio Catalogo_Negocio = new Cls_Cat_Pat_Com_Zonas_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "ZONAS";
                            Catalogo_Negocio.P_Zona_ID = P_VH_Anterior.P_Zona_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Pat_Com_Zonas_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "ZONAS";
                            Catalogo_Negocio.P_Zona_ID = P_VH_Actualizado.P_Zona_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Número de Serie
                        if (!P_VH_Actualizado.P_Serie_Carroceria.Trim().Equals(P_VH_Anterior.P_Serie_Carroceria.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Número de Serie";
                            Valor_Anterior = P_VH_Anterior.P_Serie_Carroceria.Trim();
                            Valor_Nuevo = P_VH_Actualizado.P_Serie_Carroceria.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Marca
                        if (!P_VH_Actualizado.P_Marca_ID.Trim().Equals(P_VH_Anterior.P_Marca_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Marca";
                            Cls_Cat_Com_Marcas_Negocio Catalogo_Negocio = new Cls_Cat_Com_Marcas_Negocio();
                            Catalogo_Negocio.P_Marca_ID = P_VH_Anterior.P_Marca_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consulta_Marcas();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0][Cat_Com_Marcas.Campo_Nombre].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Com_Marcas_Negocio();
                            Catalogo_Negocio.P_Marca_ID = P_VH_Actualizado.P_Marca_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consulta_Marcas();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0][Cat_Com_Marcas.Campo_Nombre].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Modelo
                        if (!P_VH_Actualizado.P_Modelo_ID.Trim().Equals(P_VH_Anterior.P_Modelo_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Modelo";
                            Valor_Anterior = P_VH_Anterior.P_Modelo_ID.Trim();
                            Valor_Nuevo = P_VH_Actualizado.P_Modelo_ID.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Color
                        if (!P_VH_Actualizado.P_Color_ID.Trim().Equals(P_VH_Anterior.P_Color_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Color";
                            Cls_Cat_Pat_Com_Colores_Negocio Catalogo_Negocio = new Cls_Cat_Pat_Com_Colores_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "COLORES";
                            Catalogo_Negocio.P_Color_ID = P_VH_Anterior.P_Color_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Pat_Com_Colores_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "COLORES";
                            Catalogo_Negocio.P_Color_ID = P_VH_Actualizado.P_Color_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Tipo de Combustible
                        if (!P_VH_Actualizado.P_Tipo_Combustible_ID.Trim().Equals(P_VH_Anterior.P_Tipo_Combustible_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Tipo de Combustible";
                            Cls_Cat_Pat_Com_Tipos_Combustible_Negocio Catalogo_Negocio = new Cls_Cat_Pat_Com_Tipos_Combustible_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "TIPOS_COMBUSTIBLE";
                            Catalogo_Negocio.P_Tipo_Combustible_ID = P_VH_Anterior.P_Tipo_Combustible_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Pat_Com_Tipos_Combustible_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "TIPOS_COMBUSTIBLE";
                            Catalogo_Negocio.P_Tipo_Combustible_ID = P_VH_Actualizado.P_Tipo_Combustible_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Placas
                        if (!P_VH_Actualizado.P_Placas.Trim().Equals(P_VH_Anterior.P_Placas.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Placas";
                            Valor_Anterior = P_VH_Anterior.P_Placas.Trim();
                            Valor_Nuevo = P_VH_Actualizado.P_Placas.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///No. Economico
                        if (!P_VH_Actualizado.P_Numero_Economico_.Trim().Equals(P_VH_Anterior.P_Numero_Economico_.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "No. Economico";
                            Valor_Anterior = P_VH_Anterior.P_Numero_Economico_.Trim();
                            Valor_Nuevo = P_VH_Actualizado.P_Numero_Economico_.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Año de Fabricación
                        if (!P_VH_Actualizado.P_Anio_Fabricacion.ToString().Trim().Equals(P_VH_Anterior.P_Anio_Fabricacion.ToString().Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Año de Fabricación";
                            Valor_Anterior = P_VH_Anterior.P_Anio_Fabricacion.ToString().Trim();
                            Valor_Nuevo = P_VH_Actualizado.P_Anio_Fabricacion.ToString().Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///No. Cilindros
                        if (!P_VH_Actualizado.P_Numero_Cilindros.ToString().Trim().Equals(P_VH_Anterior.P_Numero_Cilindros.ToString().Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "No. Cilindros";
                            Valor_Anterior = P_VH_Anterior.P_Numero_Cilindros.ToString().Trim();
                            Valor_Nuevo = P_VH_Actualizado.P_Numero_Cilindros.ToString().Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Kilomentraje
                        if (!P_VH_Actualizado.P_Kilometraje.ToString().Trim().Equals(P_VH_Anterior.P_Kilometraje.ToString().Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Kilomentraje";
                            Valor_Anterior = P_VH_Anterior.P_Kilometraje.ToString().Trim();
                            Valor_Nuevo = P_VH_Actualizado.P_Kilometraje.ToString().Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Fecha de Adquisicion
                        if (!String.Format("{0:ddMMyyyy}", P_VH_Actualizado.P_Fecha_Adquisicion).Trim().Equals(String.Format("{0:ddMMyyyy}", P_VH_Anterior.P_Fecha_Adquisicion).Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Fecha de Adquisición";
                            Valor_Anterior = String.Format("{0:dd/MM/yyyy}", P_VH_Anterior.P_Fecha_Adquisicion).Trim();
                            Valor_Nuevo = String.Format("{0:dd/MM/yyyy}", P_VH_Actualizado.P_Fecha_Adquisicion).Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Factura
                        if (!P_VH_Actualizado.P_No_Factura_.Trim().Equals(P_VH_Anterior.P_No_Factura_.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Factura";
                            Valor_Anterior = P_VH_Anterior.P_No_Factura_.Trim();
                            Valor_Nuevo = P_VH_Actualizado.P_No_Factura_.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Proveedor
                        if (!P_VH_Actualizado.P_Razon_Social_Proveedor.Trim().Equals(P_VH_Anterior.P_Razon_Social_Proveedor.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Proveedor";
                            Valor_Anterior = P_VH_Anterior.P_Razon_Social_Proveedor.Trim();
                            Valor_Nuevo = P_VH_Actualizado.P_Razon_Social_Proveedor.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Costo Inicial
                        if (!P_VH_Actualizado.P_Costo_Inicial.ToString().Trim().Equals(P_VH_Anterior.P_Costo_Inicial.ToString().Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Costo Inicial";
                            Valor_Anterior = String.Format("{0:c}", P_VH_Anterior.P_Costo_Inicial).Trim();
                            Valor_Nuevo = String.Format("{0:c}", P_VH_Actualizado.P_Costo_Inicial).Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Estatus
                        if (!P_VH_Actualizado.P_Estatus.Trim().Equals(P_VH_Anterior.P_Estatus.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Estatus";
                            Valor_Anterior = P_VH_Actualizado.P_Estatus.Trim();
                            Valor_Nuevo = P_VH_Anterior.P_Estatus.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Observaciones
                        if (!P_VH_Actualizado.P_Observaciones.Trim().Equals(P_VH_Anterior.P_Observaciones.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Observaciones";
                            Valor_Anterior = P_VH_Anterior.P_Observaciones.Trim();
                            Valor_Nuevo = P_VH_Actualizado.P_Observaciones.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Motivo de Baja
                        if (!P_VH_Actualizado.P_Motivo_Baja.Trim().Equals(P_VH_Anterior.P_Motivo_Baja.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Motivo de Baja";
                            Valor_Anterior = P_VH_Anterior.P_Motivo_Baja.Trim();
                            Valor_Nuevo = P_VH_Actualizado.P_Motivo_Baja.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                    }
                }
                else if (Tipo_Bien.Equals("BIEN_ECONOMICO"))
                {
                    if (P_BE_Anterior == null)
                    {
                        P_BE_Anterior = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                        P_BE_Anterior.P_Bien_ID = Convert.ToInt32(Bien_ID);
                        P_BE_Anterior = P_BE_Anterior.Consultar_Detalles_Bien_Economico();
                    }
                    if (P_BE_Actualizado != null)
                    {
                        ///Gerencia
                        if (!P_BE_Actualizado.P_Gerencia_ID.Trim().Equals(P_BE_Anterior.P_Gerencia_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Gerencia";
                            Cls_Cat_Grupos_Dependencias_Negocio Catalogo_Negocio = new Cls_Cat_Grupos_Dependencias_Negocio();
                            Catalogo_Negocio.P_Grupo_Dependencia_ID = P_BE_Anterior.P_Gerencia_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_Grupos_Dependencias();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0][Cat_Grupos_Dependencias.Campo_Nombre].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Grupos_Dependencias_Negocio();
                            Catalogo_Negocio.P_Grupo_Dependencia_ID = P_BE_Actualizado.P_Gerencia_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_Grupos_Dependencias();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0][Cat_Grupos_Dependencias.Campo_Nombre].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Unidad Responsable
                        if (!P_BE_Actualizado.P_Dependencia_ID.Trim().Equals(P_BE_Anterior.P_Dependencia_ID.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Unidad Responsable";
                            Cls_Cat_Dependencias_Negocio Catalogo_Negocio = new Cls_Cat_Dependencias_Negocio();
                            Catalogo_Negocio.P_Dependencia_ID = P_BE_Anterior.P_Dependencia_ID.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consulta_Dependencias();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["CLAVE_NOMBRE"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Dependencias_Negocio();
                            Catalogo_Negocio.P_Dependencia_ID = P_BE_Actualizado.P_Dependencia_ID.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consulta_Dependencias();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["CLAVE_NOMBRE"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Marca
                        if (!P_BE_Actualizado.P_Marca.Trim().Equals(P_BE_Anterior.P_Marca.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Marca";
                            Cls_Cat_Com_Marcas_Negocio Catalogo_Negocio = new Cls_Cat_Com_Marcas_Negocio();
                            Catalogo_Negocio.P_Marca_ID = P_BE_Anterior.P_Marca.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consulta_Marcas();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0][Cat_Com_Marcas.Campo_Nombre].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Com_Marcas_Negocio();
                            Catalogo_Negocio.P_Marca_ID = P_BE_Actualizado.P_Marca.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consulta_Marcas();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0][Cat_Com_Marcas.Campo_Nombre].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Modelo
                        if (!P_BE_Actualizado.P_Modelo.Trim().Equals(P_BE_Anterior.P_Modelo.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Modelo";
                            Valor_Anterior = P_BE_Anterior.P_Modelo.Trim();
                            Valor_Nuevo = P_BE_Actualizado.P_Modelo.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Material
                        if (!P_BE_Actualizado.P_Material.Trim().Equals(P_BE_Anterior.P_Material.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Material";
                            Cls_Cat_Pat_Com_Materiales_Negocio Catalogo_Negocio = new Cls_Cat_Pat_Com_Materiales_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "MATERIALES";
                            Catalogo_Negocio.P_Material_ID = P_BE_Anterior.P_Material.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Pat_Com_Materiales_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "MATERIALES";
                            Catalogo_Negocio.P_Material_ID = P_BE_Actualizado.P_Material.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Color
                        if (!P_BE_Actualizado.P_Color.Trim().Equals(P_BE_Anterior.P_Color.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Color";
                            Cls_Cat_Pat_Com_Colores_Negocio Catalogo_Negocio = new Cls_Cat_Pat_Com_Colores_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "COLORES";
                            Catalogo_Negocio.P_Color_ID = P_BE_Anterior.P_Color.Trim();
                            DataTable Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Anterior = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Catalogo_Negocio = new Cls_Cat_Pat_Com_Colores_Negocio();
                            Catalogo_Negocio.P_Tipo_DataTable = "COLORES";
                            Catalogo_Negocio.P_Color_ID = P_BE_Actualizado.P_Color.Trim();
                            Dt_Temporal = Catalogo_Negocio.Consultar_DataTable();
                            if (Dt_Temporal.Rows.Count == 1)
                            {
                                Valor_Nuevo = Dt_Temporal.Rows[0]["DESCRIPCION"].ToString().Trim();
                            }
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Número de Serie
                        if (!P_BE_Actualizado.P_Numero_Serie.Trim().Equals(P_BE_Anterior.P_Numero_Serie.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Número de Serie";
                            Valor_Anterior = P_BE_Anterior.P_Numero_Serie.Trim();
                            Valor_Nuevo = P_BE_Actualizado.P_Numero_Serie.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Costo Inicial
                        if (!P_BE_Actualizado.P_Costo_Inicial.ToString().Trim().Equals(P_BE_Anterior.P_Costo_Inicial.ToString().Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Costo Inicial";
                            Valor_Anterior = P_BE_Anterior.P_Costo_Inicial.ToString().Trim();
                            Valor_Nuevo = P_BE_Actualizado.P_Costo_Inicial.ToString().Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Fecha de Adquisicion
                        if (!String.Format("{0:ddMMyyyy}", P_BE_Actualizado.P_Fecha_Adquisicion).Trim().Equals(String.Format("{0:ddMMyyyy}", P_BE_Anterior.P_Fecha_Adquisicion).Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Fecha de Adquisición";
                            Valor_Anterior = String.Format("{0:dd/MM/yyyy}", P_BE_Anterior.P_Fecha_Adquisicion).Trim();
                            Valor_Nuevo = String.Format("{0:dd/MM/yyyy}", P_BE_Actualizado.P_Fecha_Adquisicion).Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Estatus
                        if (!P_BE_Actualizado.P_Estatus.Trim().Equals(P_BE_Anterior.P_Estatus.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Estatus";
                            Valor_Anterior = P_BE_Actualizado.P_Estatus.Trim();
                            Valor_Nuevo = P_BE_Anterior.P_Estatus.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Estado
                        if (!P_BE_Actualizado.P_Estado.Trim().Equals(P_BE_Anterior.P_Estado.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Estado";
                            Valor_Anterior = P_BE_Actualizado.P_Estado.Trim();
                            Valor_Nuevo = P_BE_Anterior.P_Estado.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Observaciones
                        if (!P_BE_Actualizado.P_Comentarios.Trim().Equals(P_BE_Anterior.P_Comentarios.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Observaciones";
                            Valor_Anterior = P_BE_Anterior.P_Comentarios.Trim();
                            Valor_Nuevo = P_BE_Actualizado.P_Comentarios.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                        ///Motivo de Baja
                        if (!P_BE_Actualizado.P_Motivo_Baja.Trim().Equals(P_BE_Actualizado.P_Motivo_Baja.Trim()))
                        {
                            String Valor_Anterior = "", Valor_Nuevo = "", Campo = "";
                            Campo = "Motivo de Baja";
                            Valor_Anterior = P_BE_Anterior.P_Motivo_Baja.Trim();
                            Valor_Nuevo = P_BE_Actualizado.P_Motivo_Baja.Trim();
                            Agregar_Fila_Estructura_Cambios(ref Dt_Cambios, Campo, Valor_Anterior, Valor_Nuevo);
                        }
                    }
                }
                return Dt_Cambios;
            }

            private DataTable Estructura_DataTable_Cambios() {
                DataTable Dt_Estructura_Cambios = new DataTable("CAMBIOS");
                Dt_Estructura_Cambios.Columns.Add("No_Registro", Type.GetType("System.Int32"));
                Dt_Estructura_Cambios.Columns.Add("No_Detalle", Type.GetType("System.Int32"));
                Dt_Estructura_Cambios.Columns.Add("Campo", Type.GetType("System.String"));
                Dt_Estructura_Cambios.Columns.Add("Valor_Anterior", Type.GetType("System.String"));
                Dt_Estructura_Cambios.Columns.Add("Valor_Nuevo", Type.GetType("System.String"));
                return Dt_Estructura_Cambios;
            }

            private void Agregar_Fila_Estructura_Cambios(ref DataTable Dt_Estructura_Cambios, String Campo, String Valor_Anterior, String Valor_Nuevo) {
                DataRow Fila_Nueva = Dt_Estructura_Cambios.NewRow();
                Fila_Nueva["Campo"] = Campo.Trim();
                Fila_Nueva["Valor_Anterior"] = ((Valor_Anterior.Trim().Length > 0) ? Valor_Anterior.Trim() : "<<Vacio>>");
                Fila_Nueva["Valor_Nuevo"] = ((Valor_Nuevo.Trim().Length > 0) ? Valor_Nuevo.Trim() : "<<Vacio>>");
                Dt_Estructura_Cambios.Rows.Add(Fila_Nueva);
            }

            public DataTable Consultar_Cambios_Bien() {
                return Cls_Ope_Pat_Manejo_Historial_Cambios_Datos.Consultar_Cambios_Bien(this);
            }

        #endregion
    }
}
