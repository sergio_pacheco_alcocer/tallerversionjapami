﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Catalogo_Cajas.Datos;

/// <summary>
/// Summary description for Cls_Cat_Pre_Cajas_Negocio
/// </summary>

namespace JAPAMI.Catalogo_Cajas.Negocio
{
    public class Cls_Cat_Pre_Cajas_Negocio
    {
        #region Variables Internas

        private String Caja_ID;    
        private String Clave;
        private String Estatus;
        private Int32 Numero_De_Caja;
        private String Modulo;
        private String Comentarios;
        private DateTime Fecha_Creo;
        private DateTime Fecha_Modifico;
        private String Usuario_Creo;
        private String Usuario_Modifico;

        #endregion 

        #region Variables Publicas

        public String P_Caja_ID
        {
            get { return Caja_ID; }
            set { Caja_ID = value; }
        }

        public String P_Clave
        {
            get { return Clave; }
            set { Clave = value; }
        }

        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }

        public Int32 P_Numero_De_Caja
        {
            get { return Numero_De_Caja; }
            set { Numero_De_Caja = value; }
        }

        public String P_Modulo
        {
            get { return Modulo; }
            set { Modulo = value; }
        }

        public String P_Comentarios
        {
            get { return Comentarios; }
            set { Comentarios = value; }
        }

        public String P_Usuario_Creo
        {
            get { return Usuario_Creo; }
            set { Usuario_Creo = value; }
        }

        public DateTime P_Fecha_Creo
        {
            get { return Fecha_Creo; }
            set { Fecha_Creo = value; }
        }

        public String P_Usuario_Modifico
        {
            get { return Usuario_Modifico; }
            set { Usuario_Modifico = value; }
        }

        public DateTime P_Fecha_Modifico
        {
            get { return Fecha_Modifico; }
            set { Fecha_Modifico = value; }
        }

        #endregion

        #region Metodos

        public void Alta_Caja()
        {
            Cls_Cat_Pre_Cajas_Datos.Alta_Cajas(this);
        }

        public void Modificar_Caja()
        {
            Cls_Cat_Pre_Cajas_Datos.Modificar_Cajas(this);
        }

        public void Eliminar_Caja()
        {
            Cls_Cat_Pre_Cajas_Datos.Eliminar_Caja(this);
        }

        public Cls_Cat_Pre_Cajas_Negocio Consultar_Datos_Caja()
        {
            return Cls_Cat_Pre_Cajas_Datos.Consultar_Datos_Cajas(this);
        }

        public DataTable Consultar_Caja()
        {
            return Cls_Cat_Pre_Cajas_Datos.Consultar_Cajas(this);
        }

        #endregion

    }

}