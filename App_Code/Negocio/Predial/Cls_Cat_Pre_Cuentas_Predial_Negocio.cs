﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Catalogo_Cuentas_Predial.Datos;

/// <summary>
/// Summary description for Cls_Cat_Pre_Cuentas_Predial_Negocio
/// </summary>

namespace JAPAMI.Catalogo_Cuentas_Predial.Negocio
{
    public class Cls_Cat_Pre_Cuentas_Predial_Negocio
    {
        
        #region Variables Internas

            private String Cuenta_Predial_ID;
            private String Cuenta_Predial;
            private String Calle_ID;
            private String Propietario_ID;
            private String Copropietario_ID;
            private String Estado_Predio_ID;
            private String Tipo_Predio_ID;
            private String Uso_Suelo_ID;
            private String Impuesto_ID_Predial;
            private String Cuota_Minima_ID;
            private String Cuenta_Origen;
            private String Estatus;
            private String No_Exterior;
            private String No_Interior;
            private Double Superficie_Construida;
            private Double Superficie_Total;
            private String Clave_Catastral;
            private Double Valor_Fiscal;
            private String Efectos;
            private String Periodo_Corriente;
            private Double Cuota_Anual;
            private Double Porcentaje_Exencion;
            private String Cuota_Fija;
            private DateTime Termino_Exencion;
            private DateTime Fecha_Avaluo;
            private String Usuario;
            private String Campos_Dinamicos;
            private String Filtros_Dinamicos;
            private String Agrupar_Dinamico;
            private String Ordenar_Dinamico;
            private Boolean Incluir_Campos_Foraneos;
        
        #endregion

        #region Variables Publicas

            public String P_Cuenta_Predial_ID
            {
                get { return Cuenta_Predial_ID; }
                set { Cuenta_Predial_ID = value; }
            }

            public String P_Cuenta_Predial
            {
                get { return Cuenta_Predial; }
                set { Cuenta_Predial = value; }
            }

            public String P_Calle_ID
            {
                get {
                    if (Calle_ID == null)
                    {
                        Calle_ID = "NULL";
                    }
                    else
                    {
                        Calle_ID = "'" + Calle_ID + "'";
                    }
                    return Calle_ID; 
                }
                set { Calle_ID = value; }
            }

            public String P_Propietario_ID
            {
                get {
                    if (Propietario_ID == null)
                    {
                        Propietario_ID = "NULL";
                    }
                    else
                    {
                        Propietario_ID = "'" + Propietario_ID + "'";
                    }
                    return Propietario_ID;
                }
                set { Propietario_ID = value; }
            }

            public String P_Copropietario_ID
            {
                get {
                    if (Copropietario_ID == null)
                    {
                        Copropietario_ID = "NULL";
                    }
                    else
                    {
                        Copropietario_ID = "'" + Copropietario_ID + "'";
                    }
                    return Copropietario_ID;
                }
                set { Copropietario_ID = value; }
            }

            public String P_Estado_Predio_ID
            {
                get {
                    if (Estado_Predio_ID == null)
                    {
                        Estado_Predio_ID = "NULL";
                    }
                    else
                    {
                        Estado_Predio_ID = "'" + Estado_Predio_ID + "'";
                    }
                    return Estado_Predio_ID;
                }
                set { Estado_Predio_ID = value; }
            }

            public String P_Tipo_Predio_ID
            {
                get {
                    if (Tipo_Predio_ID == null)
                    {
                        Tipo_Predio_ID = "NULL";
                    }
                    else
                    {
                        Tipo_Predio_ID = "'" + Tipo_Predio_ID + "'";
                    }
                    return Tipo_Predio_ID;
                }
                set { Tipo_Predio_ID = value; }
            }

            public String P_Uso_Suelo_ID
            {
                get {
                    if (Uso_Suelo_ID == null)
                    {
                        Uso_Suelo_ID = "NULL";
                    }
                    else
                    {
                        Uso_Suelo_ID = "'" + Uso_Suelo_ID + "'";
                    }
                    return Uso_Suelo_ID;
                }
                set { Uso_Suelo_ID = value; }
            }

            public String P_Impuesto_ID_Predial
            {
                get {
                    if (Impuesto_ID_Predial == null)
                    {
                        Impuesto_ID_Predial = "NULL";
                    }
                    else
                    {
                        Impuesto_ID_Predial = "'" + Impuesto_ID_Predial + "'";
                    }
                    return Impuesto_ID_Predial;
                }
                set { Impuesto_ID_Predial = value; }
            }

            public String P_Cuota_Minima_ID
            {
                get {
                    if (Cuota_Minima_ID == null)
                    {
                        Cuota_Minima_ID = "NULL";
                    }
                    else
                    {
                        Cuota_Minima_ID = "'" + Cuota_Minima_ID + "'";
                    }
                    return Cuota_Minima_ID;
                }
                set { Cuota_Minima_ID = value; }
            }

            public String P_Cuenta_Origen
            {
                get { return Cuenta_Origen; }
                set { Cuenta_Origen = value; }
            }

            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }

            public String P_No_Exterior
            {
                get { return No_Exterior; }
                set { No_Exterior = value; }
            }

            public String P_No_Interior
            {
                get { return No_Interior; }
                set { No_Interior = value; }
            }

            public Double P_Superficie_Construida
            {
                get { return Superficie_Construida; }
                set { Superficie_Construida = value; }
            }

            public Double P_Superficie_Total
            {
                get { return Superficie_Total; }
                set { Superficie_Total = value; }
            }

            public String P_Clave_Catastral
            {
                get { return Clave_Catastral; }
                set { Clave_Catastral = value; }
            }

            public Double P_Valor_Fiscal
            {
                get { return Valor_Fiscal; }
                set { Valor_Fiscal = value; }
            }

            public String P_Efectos
            {
                get { return Efectos; }
                set { Efectos = value; }
            }

            public String P_Periodo_Corriente
            {
                get { return Periodo_Corriente; }
                set { Periodo_Corriente = value; }
            }

            public Double P_Cuota_Anual
            {
                get { return Cuota_Anual; }
                set { Cuota_Anual = value; }
            }

            public Double P_Porcentaje_Exencion
            {
                get { return Porcentaje_Exencion; }
                set { Porcentaje_Exencion = value; }
            }

            public String P_Cuota_Fija
            {
                get { return Cuota_Fija; }
                set { Cuota_Fija = value; }
            }

            public DateTime P_Termino_Exencion
            {
                get { return Termino_Exencion; }
                set { Termino_Exencion = value; }
            }

            public DateTime P_Fecha_Avaluo
            {
                get { return Fecha_Avaluo; }
                set { Fecha_Avaluo = value; }
            }

            public String P_Usuario
            {
                get { return Usuario; }
                set { Usuario = value; }
            }

            public String P_Campos_Dinamicos
            {
                get { return Campos_Dinamicos; }
                set { Campos_Dinamicos = value.Trim(); }
            }

            public String P_Filtros_Dinamicos
            {
                get { return Filtros_Dinamicos; }
                set { Filtros_Dinamicos = value.Trim(); }
            }

            public String P_Agrupar_Dinamico
            {
                get { return Agrupar_Dinamico; }
                set { Agrupar_Dinamico = value.Trim(); }
            }

            public String P_Ordenar_Dinamico
            {
                get { return Ordenar_Dinamico; }
                set { Ordenar_Dinamico = value.Trim(); }
            }

            public Boolean P_Incluir_Campos_Foraneos
            {
                get { return Incluir_Campos_Foraneos; }
                set { Incluir_Campos_Foraneos = value; }
            }

        #endregion

        #region Metodos

            public Boolean Alta_Cuenta(){
                return Cls_Cat_Pre_Cuentas_Predial_Datos.Alta_Cuenta(this);
            }

            public Boolean Modifcar_Cuenta()
            {
                return Cls_Cat_Pre_Cuentas_Predial_Datos.Modificar_Cuenta(this);
            }

            public DataTable Consultar_Cuenta(){
                return Cls_Cat_Pre_Cuentas_Predial_Datos.Consultar_Cuentas(this);
            }

            public Boolean Eliminar_Cuenta()
            {
                return Cls_Cat_Pre_Cuentas_Predial_Datos.Eliminar_Cuenta(this);
            }
        #endregion

    }
}