﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Catalogo_Modulos.Datos;

/// <summary>
/// Summary description for Cls_Cat_Pre_Modulos_Negocio
/// </summary>

namespace JAPAMI.Catalogo_Modulos.Negocio
{
    public class Cls_Cat_Pre_Modulos_Negocio
    {
        #region Variables Internas

        private String Modulo_Id;
        private String Estatus;
        private String Clave;
        private String Ubicacion;
        private String Descripcion;
        private String Usuario_Creo;
        private String Usuario_Modifico;
        private DateTime Fecha_Creo;
        private DateTime Fecha_Modifico;

        #endregion

        #region Variables Publicas

        public String P_Id_Modulo
        {
            get { return Modulo_Id; }
            set { Modulo_Id = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_Clave
        {
            get { return Clave; }
            set { Clave = value; }
        }
        public String P_Ubicacion
        {
            get { return Ubicacion; }
            set { Ubicacion = value; }
        }
        public String P_Descripcion
        {
            get { return Descripcion; }
            set { Descripcion = value; }
        }
        public String P_Usuario_Creo
        {
            get { return Usuario_Creo; }
            set { Usuario_Creo = value; }
        }
        public String P_Usuario_Modifico
        {
            get { return Usuario_Modifico; }
            set { Usuario_Modifico = value; }
        }
        public DateTime P_Fecha_Creo
        {
            get { return Fecha_Creo; }
            set { Fecha_Creo = value; }
        }
        public DateTime P_Fecha_Modifico
        {
            get { return Fecha_Modifico; }
            set { Fecha_Modifico = value; }
        }

        #endregion

        #region Metodos

        public void Alta_Modulo()
        {
            Cls_Cat_Pre_Modulos_Datos.Alta_Modulos(this);
        }

        public void Modificar_Modulo()
        {
            Cls_Cat_Pre_Modulos_Datos.Modificar_Modulos(this);
        }

        public void Eliminar_Modulo()
        {
            Cls_Cat_Pre_Modulos_Datos.Eliminar_Modulo(this);
        }

        public Cls_Cat_Pre_Modulos_Negocio Consultar_Datos_Modulo()
        {
            return Cls_Cat_Pre_Modulos_Datos.Consultar_Datos_Modulos(this);
        }

        public DataTable Consultar_Modulo()
        {
            return Cls_Cat_Pre_Modulos_Datos.Consultar_Modulos();
        }

        public DataTable Consultar_Nombre_Modulos()
        {
            return Cls_Cat_Pre_Modulos_Datos.Consultar_Modulos();
        }

        #endregion

    }
}