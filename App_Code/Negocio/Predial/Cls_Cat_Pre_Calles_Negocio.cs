﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Catalogo_Calles.Datos;

/// <summary>
/// Summary description for Cls_Cat_Pre_Calles_Negocio
/// </summary>
namespace JAPAMI.Catalogo_Calles.Negocio {
    public class Cls_Cat_Pre_Calles_Negocio {

        #region Variables Internas

            private String Calle_ID;
            private String Nombre;
            private String Tipo_Calle;
            private String Usuario;
            private DataTable Calles_Colonias;
            private String Campos_Dinamicos;
            private String Filtros_Dinamicos;
            private String Agrupar_Dinamico;
            private String Ordenar_Dinamico;
            private Boolean Incluir_Campos_Foraneos;

            private String Nombre_Calle;
            private String Nombre_Colonia;
            private Boolean Mostrar_Nombre_Calle_Nombre_Colonia;
            private String Clave_Colonia;
            private String Clave_Calle;
            private DataTable Colonias;
            private String Comentarios;
            private String Estatus;

        #endregion

        #region Variables Publicas


            public String P_Comentarios
            {
                get { return Comentarios; }
                set { Comentarios = value; }
            }

            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }



            public DataTable P_Colonias
            {
                get { return Colonias; }
                set { Colonias = value; }
            }

            public Boolean P_Mostrar_Nombre_Calle_Nombre_Colonia
            {
                get { return Mostrar_Nombre_Calle_Nombre_Colonia; }
                set { Mostrar_Nombre_Calle_Nombre_Colonia = value; }
            }

            public String P_Clave_Colonia
            {
                get { return Clave_Colonia; }
                set { Clave_Colonia = value; }
            }

            public String P_Clave
            {
                get { return Clave_Calle; }
                set { Clave_Calle = value; }
            }

            public String P_Nombre_Calle
            {
                get { return Nombre_Calle; }
                set { Nombre_Calle = value; }
            }

            public String P_Nombre_Colonia
            {
                get { return Nombre_Colonia; }
                set { Nombre_Colonia = value; }
            }



            public String P_Calle_ID
            {
                get { return Calle_ID; }
                set { Calle_ID = value; }
            }

            public String P_Nombre
            {
                get { return Nombre; }
                set { Nombre = value; }
            }

            public String P_Tipo_Calle
            {
                get { return Tipo_Calle; }
                set { Tipo_Calle = value; }
            }

            public String P_Usuario
            {
                get { return Usuario; }
                set { Usuario = value; }
            }

            public DataTable P_Calles_Colonias
            {
                get { return Calles_Colonias; }
                set { Calles_Colonias = value; }
            }

            public String P_Campos_Dinamicos
            {
                get { return Campos_Dinamicos; }
                set { Campos_Dinamicos = value.Trim(); }
            }

            public String P_Filtros_Dinamicos
            {
                get { return Filtros_Dinamicos; }
                set { Filtros_Dinamicos = value.Trim(); }
            }

            public String P_Agrupar_Dinamico
            {
                get { return Agrupar_Dinamico; }
                set { Agrupar_Dinamico = value.Trim(); }
            }

            public String P_Ordenar_Dinamico
            {
                get { return Ordenar_Dinamico; }
                set { Ordenar_Dinamico = value.Trim(); }
            }

            public Boolean P_Incluir_Campos_Foraneos
            {
                get { return Incluir_Campos_Foraneos; }
                set { Incluir_Campos_Foraneos = value; }
            }

        #endregion

        #region Metodos

            public void Alta_Calle() {
                Cls_Cat_Pre_Calles_Datos.Alta_Calle(this);
            }   

            public void Modificar_Calle(){
                Cls_Cat_Pre_Calles_Datos.Modificar_Calle(this);    
            }

            public void Eliminar_Calle(){
                Cls_Cat_Pre_Calles_Datos.Eliminar_Calle(this);
            }

            public DataTable Consultar_Calles() {
                return Cls_Cat_Pre_Calles_Datos.Consultar_Calles(this);
            }

            public Cls_Cat_Pre_Calles_Negocio Consultar_Datos_Calle(){
                return Cls_Cat_Pre_Calles_Datos.Consultar_Datos_Calle(this);
            }


            public DataTable Consultar_Colonias() {
                return Cls_Cat_Pre_Calles_Datos.Consultar_Colonias();
            }

            public DataTable Consultar_Nombre() //Busqueda
            {
                return Cls_Cat_Pre_Calles_Datos.Consultar_Nombre(this);
            }
           

        #endregion

    }
}