﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Catalogo_Fraccionamientos.Datos;

/// <summary>
/// Summary description for Cls_Cat_Pre_Fraccionamientos_Negocio
/// </summary>

namespace JAPAMI.Catalogo_Fraccionamientos.Negocio{
    public class Cls_Cat_Pre_Fraccionamientos_Negocio{

        #region Variables Internas

            private String Fraccionamiento_ID;
            private String Identificador;
            private String Descripcion;
            private String Estatus;
            private String Usuario;
            private DataTable Fraccionamientos_Impuestos;
        
        #endregion

        #region Variables Publicas

            public String P_Fraccionamiento_ID
            {
                get { return Fraccionamiento_ID; }
                set { Fraccionamiento_ID = value; }
            }

            public String P_Identificador
            {
                get { return Identificador; }
                set { Identificador = value; }
            }

            public String P_Descripcion
            {
                get { return Descripcion; }
                set { Descripcion = value; }
            }

            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }

            public String P_Usuario
            {
                get { return Usuario; }
                set { Usuario = value; }
            }

            public DataTable P_Fraccionamientos_Impuestos
            {
                get { return Fraccionamientos_Impuestos; }
                set { Fraccionamientos_Impuestos = value; }
            }

        #endregion

        #region Metodos

            public void Alta_Fraccionamiento() {
                Cls_Cat_Pre_Fraccionamientos_Datos.Alta_Fraccionamiento(this);
            }   

            public void Modificar_Fraccionamiento(){
                Cls_Cat_Pre_Fraccionamientos_Datos.Modificar_Fraccionamiento(this);
            }

            public void Eliminar_Fraccionamiento(){
                Cls_Cat_Pre_Fraccionamientos_Datos.Eliminar_Fraccionamiento(this);
            }

            public DataTable Consultar_Fraccionamientos() {
                return Cls_Cat_Pre_Fraccionamientos_Datos.Consultar_Fraccionamientos(this);
            }

            public Cls_Cat_Pre_Fraccionamientos_Negocio Consultar_Datos_Fraccionamiento() {
                return Cls_Cat_Pre_Fraccionamientos_Datos.Consultar_Datos_Fraccionamiento(this);
            }

        #endregion

    }
}