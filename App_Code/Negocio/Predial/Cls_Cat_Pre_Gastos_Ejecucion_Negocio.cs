﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Catalogo_Gastos_Ejecucion.Datos;

/// <summary>
/// Summary description for Cls_Cat_Pre_Gastos_Ejecucion_Negocio
/// </summary>

namespace JAPAMI.Catalogo_Gastos_Ejecucion.Negocio{
public class Cls_Cat_Pre_Gastos_Ejecucion_Negocio{

    #region Variables Internas

        private String Gastos_Ejecucion_ID;
        private String Identificador;
        private String Descripcion;
        private String Estatus;
        private String Usuario;
        private DataTable Gastos_Tasas;

    #endregion

    #region Variables Publicas

        public String P_Gastos_Ejecucion_ID
        {
            get { return Gastos_Ejecucion_ID; }
            set { Gastos_Ejecucion_ID = value; }
        }

        public String P_Identificador
        {
            get { return Identificador; }
            set { Identificador = value; }
        }

        public String P_Descripcion
        {
            get { return Descripcion; }
            set { Descripcion = value; }
        }

        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }

        public String P_Usuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }

        public DataTable P_Gastos_Tasas
        {
            get { return Gastos_Tasas; }
            set { Gastos_Tasas = value; }
        }

    #endregion

    #region Metodos

        public void Alta_Gasto_Ejecucion(){
            Cls_Cat_Pre_Gastos_Ejecucion_Datos.Alta_Gasto_Ejecucion(this);
        }

        public void Modificar_Gasto_Ejecucion(){
            Cls_Cat_Pre_Gastos_Ejecucion_Datos.Modificar_Gasto_Ejecucion(this);
        }

        public void Eliminar_Gasto_Ejecucion(){
            Cls_Cat_Pre_Gastos_Ejecucion_Datos.Eliminar_Gasto_Ejecucion(this);
        }

        public DataTable Consultar_Gastos_Ejecucion(){
            return Cls_Cat_Pre_Gastos_Ejecucion_Datos.Consultar_Gastos_Ejecucion(this);
        }

        public Cls_Cat_Pre_Gastos_Ejecucion_Negocio Consultar_Datos_Gasto_Ejecucion(){
            return Cls_Cat_Pre_Gastos_Ejecucion_Datos.Consultar_Datos_Gasto_Ejecucion(this);
        }

    #endregion

}
}