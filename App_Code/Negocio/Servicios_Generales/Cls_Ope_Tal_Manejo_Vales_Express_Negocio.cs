﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Manejo_Vales_Express.Datos;

/// <summary>
/// Summary description for Cls_Ope_Tal_Manejo_Vales_Express_Negocio
/// </summary>
namespace JAPAMI.Taller_Mecanico.Operacion_Manejo_Vales_Express.Negocio
{
    public class Cls_Ope_Tal_Manejo_Vales_Express_Negocio
    {
        #region Variables Internas

            private String No_Registro = String.Empty;
            private String Vehiculo_ID = String.Empty;
            private String Usuario_Entrega_ID = String.Empty;
            private String Usuario_Recibe_ID = String.Empty;
            private String Dependencia_ID = String.Empty;
            private String Observaciones = String.Empty;
            private String Estatus = String.Empty;
            private String Usuario_Creo = String.Empty;
            private DateTime Fecha_Creo = new DateTime();
            private String Usuario_Modifico = String.Empty;
            private DateTime Fecha_Modifico = new DateTime();
            private DataTable Dt_Refacciones = new DataTable();

        #endregion

        #region Variables Publicas

            public String P_No_Registro
            {
                get { return No_Registro; }
                set { No_Registro = value; }
            }
            public String P_Vehiculo_ID
            {
                get { return Vehiculo_ID; }
                set { Vehiculo_ID = value; }
            }
            public String P_Usuario_Entrega_ID
            {
                get { return Usuario_Entrega_ID; }
                set { Usuario_Entrega_ID = value; }
            }
            public String P_Usuario_Recibe_ID
            {
                get { return Usuario_Recibe_ID; }
                set { Usuario_Recibe_ID = value; }
            }
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }
            public String P_Observaciones
            {
                get { return Observaciones; }
                set { Observaciones = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Usuario_Creo
            {
                get { return Usuario_Creo; }
                set { Usuario_Creo = value; }
            }
            public DateTime P_Fecha_Creo
            {
                get { return Fecha_Creo; }
                set { Fecha_Creo = value; }
            }
            public String P_Usuario_Modifico
            {
                get { return Usuario_Modifico; }
                set { Usuario_Modifico = value; }
            }
            public DateTime P_Fecha_Modifico
            {
                get { return Fecha_Modifico; }
                set { Fecha_Modifico = value; }
            }
            public DataTable P_Dt_Refacciones
            {
                get { return Dt_Refacciones; }
                set { Dt_Refacciones = value; }
            }

        #endregion

        #region Metodos

            public Cls_Ope_Tal_Manejo_Vales_Express_Negocio Alta_Vales_Express()
            {
                return Cls_Ope_Tal_Manejo_Vales_Express_Datos.Alta_Vales_Express(this);
            }

            public Cls_Ope_Tal_Manejo_Vales_Express_Negocio Modificar_Vales_Express()
            {
                return Cls_Ope_Tal_Manejo_Vales_Express_Datos.Modificar_Vales_Express(this);
            }

            public Cls_Ope_Tal_Manejo_Vales_Express_Negocio Cancelar_Vales_Express()
            {
                return Cls_Ope_Tal_Manejo_Vales_Express_Datos.Cancelar_Vales_Express(this);
            }

            public DataTable Consultar_Vales_Express()
            {
                return Cls_Ope_Tal_Manejo_Vales_Express_Datos.Consultar_Vales_Express(this);
            }

            public Cls_Ope_Tal_Manejo_Vales_Express_Negocio Consultar_Detalles_Vales_Express()
            {
                return Cls_Ope_Tal_Manejo_Vales_Express_Datos.Consultar_Detalles_Vales_Express(this);
            }

        #endregion
    }
}
