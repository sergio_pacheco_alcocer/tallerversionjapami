﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Catalogo_Tarjetas_Gasolina.Datos;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Ope_Facturas_Tarjetas_Gasolina.Negocio;
using System.Collections.Generic;

/// <summary>
/// Summary description for Cls_Cat_Tal_Tarjetas_Gasolina_Negocio
/// </summary>
namespace JAPAMI.Taller_Mecanico.Catalogo_Tarjetas_Gasolina.Negocio
{
    public class Cls_Cat_Tal_Tarjetas_Gasolina_Negocio
    {
        #region Variables Internas

            private String Tarjeta_ID = String.Empty;
            private String Tipo_Bien = String.Empty;
            private String Numero_Tarjeta = String.Empty;
            private String Emp_Asig_Tarjeta_ID = String.Empty;
            private String Vehiculo_ID = String.Empty;
            private String Bien_ID = String.Empty;
            private Double Saldo_Inicial = (-1.0);
            private Double Saldo_Cargado = (-1.0);
            private Double Saldo_Consumido = (-1.0);
            private Double Saldo_Actual = (-1.0);
            private String Comentarios = String.Empty;
            private String Usuario_Creo = String.Empty;
            private DateTime Fecha_Creo = new DateTime();
            private String Usuario_Modifico = String.Empty;
            private DateTime Fecha_Modifico = new DateTime();
            private String Estatus = String.Empty;
            private String Partida_ID = String.Empty;
            private String Dependencia_ID = String.Empty;
            private String Programa_Proyecto_ID = String.Empty;

            private String Numero_Registro = String.Empty;
            private String Numero_Movimiento = String.Empty;
            private DateTime Fecha_Movimiento = new DateTime();
            private String Tipo_Movimiento = String.Empty;
            private String Empleado_Realizo_ID = String.Empty;
            private Double Saldo_Movimiento = (-1.0);
            private DataTable Dt_Historial = new DataTable();

            private String Numero_Empleado = String.Empty;
            private String Nombre_Empleado = String.Empty;
            private String Rfc = String.Empty;
            private String Unidad_Responsble = String.Empty;
            private String Numero_Inventario = String.Empty;
            private String Numero_Economico = String.Empty;

            private Boolean Ignorar_Reserva = false;

            private SqlTransaction Trans;
            private SqlCommand Cmmd;
            private String Fecha_Inicial;
            private String Fecha_Final;

            private List<Cls_Ope_Tal_Facturas_Movimientos_Tarjetas_Negocio> obj_Bean_Facturas = new List<Cls_Ope_Tal_Facturas_Movimientos_Tarjetas_Negocio>();
            private String Movimientos = String.Empty;
        #endregion

        #region Variables Publicas

            public String P_Tarjeta_ID
            {
                get { return Tarjeta_ID; }
                set { Tarjeta_ID = value; }
            }
            public String P_Numero_Tarjeta
            {
                get { return Numero_Tarjeta; }
                set { Numero_Tarjeta = value; }
            }
            public String P_Emp_Asig_Tarjeta_ID
            {
                get { return Emp_Asig_Tarjeta_ID; }
                set { Emp_Asig_Tarjeta_ID = value; }
            }
            public String P_Vehiculo_ID
            {
                get { return Vehiculo_ID; }
                set { Vehiculo_ID = value; }
            }
            public String P_Bien_ID
            {
                get { return Bien_ID; }
                set { Bien_ID = value; }
            }
            public Double P_Saldo_Inicial
            {
                get { return Saldo_Inicial; }
                set { Saldo_Inicial = value; }
            }
            public Double P_Saldo_Cargado
            {
                get { return Saldo_Cargado; }
                set { Saldo_Cargado = value; }
            }
            public Double P_Saldo_Consumido
            {
                get { return Saldo_Consumido; }
                set { Saldo_Consumido = value; }
            }
            public Double P_Saldo_Actual
            {
                get { return Saldo_Actual; }
                set { Saldo_Actual = value; }
            }
            public String P_Comentarios
            {
                get { return Comentarios; }
                set { Comentarios = value; }
            }
            public String P_Usuario_Creo
            {
                get { return Usuario_Creo; }
                set { Usuario_Creo = value; }
            }
            public DateTime P_Fecha_Creo
            {
                get { return Fecha_Creo; }
                set { Fecha_Creo = value; }
            }
            public String P_Usuario_Modifico
            {
                get { return Usuario_Modifico; }
                set { Usuario_Modifico = value; }
            }
            public DateTime P_Fecha_Modifico
            {
                get { return Fecha_Modifico; }
                set { Fecha_Modifico = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }

            public String P_Numero_Registro
            {
                get { return Numero_Registro; }
                set { Numero_Registro = value; }
            }
            public String P_Numero_Movimiento
            {
                get { return Numero_Movimiento; }
                set { Numero_Movimiento = value; }
            }
            public DateTime P_Fecha_Movimiento
            {
                get { return Fecha_Movimiento; }
                set { Fecha_Movimiento = value; }
            }
            public String P_Tipo_Movimiento
            {
                get { return Tipo_Movimiento; }
                set { Tipo_Movimiento = value; }
            }
            public String P_Empleado_Realizo_ID
            {
                get { return Empleado_Realizo_ID; }
                set { Empleado_Realizo_ID = value; }
            }
            public Double P_Saldo_Movimiento
            {
                get { return Saldo_Movimiento; }
                set { Saldo_Movimiento = value; }
            }
            public DataTable P_Dt_Historial
            {
                get { return Dt_Historial; }
                set { Dt_Historial = value; }
            }

            public String P_Numero_Empleado
            {
                get { return Numero_Empleado; }
                set { Numero_Empleado = value; }
            }
            public String P_Nombre_Empleado
            {
                get { return Nombre_Empleado; }
                set { Nombre_Empleado = value; }
            }
            public String P_Rfc
            {
                get { return Rfc; }
                set { Rfc = value; }
            }
            public String P_Unidad_Responsble
            {
                get { return Unidad_Responsble; }
                set { Unidad_Responsble = value; }
            }
            public String P_Numero_Inventario
            {
                get { return Numero_Inventario; }
                set { Numero_Inventario = value; }
            }
            public String P_Numero_Economico
            {
                get { return Numero_Economico; }
                set { Numero_Economico = value; }
            }
            public String P_Partida_ID
            {
                get { return Partida_ID; }
                set { Partida_ID = value; }
            }
            public String P_Programa_Proyecto_ID
            {
                get { return Programa_Proyecto_ID; }
                set { Programa_Proyecto_ID = value; }
            }
            public String P_Tipo_Bien
            {
                get { return Tipo_Bien; }
                set { Tipo_Bien = value; }
            }
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }
            public Boolean P_Ignorar_Reserva
                {
                    get { return Ignorar_Reserva; }
                    set { Ignorar_Reserva = value; }
                }
            public SqlTransaction P_Trans
            {
                get { return Trans; }
                set { Trans = value; }
            }
            public SqlCommand P_Cmmd
            {
                get { return Cmmd; }
                set { Cmmd = value; }
            }
            public String P_Fecha_Inicial
            {
                get { return Fecha_Inicial; }
                set { Fecha_Inicial = value; }
            }
            public String P_Fecha_Final
            {
                get { return Fecha_Final; }
                set { Fecha_Final = value; }
            }
            public String P_Movimientos
            {
                get { return Movimientos; }
                set { Movimientos = value; }
            }
            public List<Cls_Ope_Tal_Facturas_Movimientos_Tarjetas_Negocio> P_obj_Bean_Facturas
            {
                get { return obj_Bean_Facturas; }
                set { obj_Bean_Facturas = value; }
            }
        #endregion

        #region Metodos

            public void Alta_Tarjetas_Gasolina()
            {
                Cls_Cat_Tal_Tarjetas_Gasolina_Datos.Alta_Tarjetas_Gasolina(this);
            }
            public void Alta_Facturas_Movimientos_Tarjetas()
            {
                Cls_Cat_Tal_Tarjetas_Gasolina_Datos.Alta_Facturas_Movimientos_Tarjetas(this);
            }
            public void Modificar_Tarjetas_Gasolina()
            {
                Cls_Cat_Tal_Tarjetas_Gasolina_Datos.Modificar_Tarjetas_Gasolina(this);
            }

            public DataTable Consultar_Tarjetas_Gasolina()
            {
                return Cls_Cat_Tal_Tarjetas_Gasolina_Datos.Consultar_Tarjetas_Gasolina(this);
            }

            public DataTable Consultar_Movimientos_Detalles()
            {
                return Cls_Cat_Tal_Tarjetas_Gasolina_Datos.Consultar_Movimientos_Detalles(this);
            }

            public Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Consultar_Detalles_Tarjetas_Gasolina()
            {
                return Cls_Cat_Tal_Tarjetas_Gasolina_Datos.Consultar_Detalles_Tarjetas_Gasolina(this);
            }

            public void Eliminar_Tarjetas_Gasolina()
            {
                Cls_Cat_Tal_Tarjetas_Gasolina_Datos.Eliminar_Tarjetas_Gasolina(this);
            }

            public void Agregar_Movimiento_Tarjete_Gasolina()
            {
                Cls_Cat_Tal_Tarjetas_Gasolina_Datos.Agregar_Movimiento_Tarjete_Gasolina(this);
            }

            public void Actualizar_Saldos_Movimientos() {
                Cls_Cat_Tal_Tarjetas_Gasolina_Datos.Actualizar_Saldos_Movimientos(this);
            }

            public void Cancelar_Reserva()
            {
                Cls_Cat_Tal_Tarjetas_Gasolina_Datos.Cancelar_Reserva(this);
            }

        #endregion
    }
}
