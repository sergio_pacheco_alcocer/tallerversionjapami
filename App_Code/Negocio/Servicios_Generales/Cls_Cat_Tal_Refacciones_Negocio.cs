﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Catalogo_Taller_Refacciones.Datos;

namespace JAPAMI.Catalogo_Taller_Refacciones.Negocio
{
    public class Cls_Cat_Tal_Refacciones_Negocio
    {        
        #region Variables Internas

            private String Refaccion_ID;
            private String Unidad_ID;        
            private String Costo_Unitario;        
            private String Clave;
            private String Estatus;
            private String Tipo;        
            private String Nombre;
            private String Descripcion;
            private String Filtros_Dinamicos;
            private String Campos_Dinamicos;
            private Int32 Existencia = (-1);
            private Int32 Comprometido = (-1);
            private Int32 Disponible = (-1);
            private Int32 Maximo = (-1);
            private Int32 Minimo = (-1);
            private Int32 Reorden = (-1);
            private String Partida_ID;
            private Int32 Inicial = (-1);
            private String Impuesto_ID;
            private String Impuesto_2_ID;
            private String Ubicacion;
            private String Tipo_Refaccion_ID;

        #endregion 

        #region Variables Publicas
            public String P_Unidad_ID
            {
                get { return Unidad_ID; }
                set { Unidad_ID = value; }
            }
            public String P_Costo_Unitario
            {
                get { return Costo_Unitario; }
                set { Costo_Unitario = value; }
            }
            public String P_Campos_Dinamicos
            {
                get { return Campos_Dinamicos; }
                set { Campos_Dinamicos = value; }
            }

            public String P_Refaccion_ID
            {
                get { return Refaccion_ID; }
                set { Refaccion_ID = value; }
            }

            public String P_Clave
            {
                get { return Clave; }
                set { Clave = value; }
            }

            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }

            public String P_Nombre
            {
                get { return Nombre; }
                set { Nombre = value; }
            }

            public String P_Descripcion
            {
                get { return Descripcion; }
                set { Descripcion = value; }
            }

            public String P_Filtro
            {
                get { return Filtros_Dinamicos; }
                set { Filtros_Dinamicos = value; }
            }

            public String P_Tipo
            {
                get { return Tipo; }
                set { Tipo = value; }
            }
            public Int32 P_Existencia
            {
                get { return Existencia; }
                set { Existencia = value; }
            }
            public Int32 P_Comprometido
            {
                get { return Comprometido; }
                set { Comprometido = value; }
            }
            public Int32 P_Disponible
            {
                get { return Disponible; }
                set { Disponible = value; }
            }
            public Int32 P_Maximo
            {
                get { return Maximo; }
                set { Maximo = value; }
            }
            public Int32 P_Minimo
            {
                get { return Minimo; }
                set { Minimo = value; }
            }
            public Int32 P_Reorden
            {
                get { return Reorden; }
                set { Reorden = value; }
            }
            public String P_Partida_ID
            {
                get { return Partida_ID; }
                set { Partida_ID = value; }
            }
            public Int32 P_Inicial
            {
                get { return Inicial; }
                set { Inicial = value; }
            }
            public String P_Impuesto_ID
            {
                get { return Impuesto_ID; }
                set { Impuesto_ID = value; }
            }
            public String P_Impuesto_2_ID
            {
                get { return Impuesto_2_ID; }
                set { Impuesto_2_ID = value; }
            }
            public String P_Ubicacion
            {
                get { return Ubicacion; }
                set { Ubicacion = value; }
            }
            public String P_Tipo_Refaccion_ID
            {
                get { return Tipo_Refaccion_ID; }
                set { Tipo_Refaccion_ID = value; }
            } 
        #endregion

        #region Metodos

            public void Alta_Refaccion()
            {
                Cls_Cat_Tal_Refacciones_Datos.Alta_Refaccion(this);
            }

            public void Modificar_Refaccion()
            {
                Cls_Cat_Tal_Refacciones_Datos.Modificar_Refaccion(this);
            }

            public void Eliminar_Refaccion()
            {
                Cls_Cat_Tal_Refacciones_Datos.Eliminar_Refaccion(this);
            }

            public Cls_Cat_Tal_Refacciones_Negocio Consultar_Datos()
            {
                return Cls_Cat_Tal_Refacciones_Datos.Consultar_Datos(this);
            }

            public DataTable Consultar_Refaccion()
            {
                return Cls_Cat_Tal_Refacciones_Datos.Consultar_Refaccion(this);
            }
            public DataTable Consulta_Busqueda_Refacciones()
            {
                return Cls_Cat_Tal_Refacciones_Datos.Consulta_Busqueda_Refacciones(this);
            }

        #endregion

     }    
}