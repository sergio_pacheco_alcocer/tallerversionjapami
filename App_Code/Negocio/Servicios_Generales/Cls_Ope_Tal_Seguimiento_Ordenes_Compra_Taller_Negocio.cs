﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Seguimiento_Ordenes_Compra_Taller.Datos;

/// <summary>
/// Summary description for Cls_Ope_Tal_Seguimiento_Ordenes_Compra_Taller_Negocio
/// </summary>
namespace JAPAMI.Seguimiento_Ordenes_Compra_Taller.Negocio
{
    public class Cls_Ope_Tal_Seguimiento_Ordenes_Compra_Taller_Negocio
    {
        #region Variables Internas

        private String No_OC = String.Empty;
        private DataTable Dt_Datos = new DataTable();
        private DateTime Fecha_Inicio = new DateTime();
        private DateTime Fecha_Fin = new DateTime();

        #endregion Variables Internas

        #region Variables Publicas

        public String P_No_OC {
            set { No_OC = value; }
            get { return No_OC; }
        }
        public DataTable P_Dt_Datos
        {
            set { Dt_Datos = value; }
            get { return Dt_Datos; }
        }
        public DateTime P_Fecha_Inicio
        {
            get { return Fecha_Inicio; }
            set { Fecha_Inicio = value; }
        }
        public DateTime P_Fecha_Fin
        {
            get { return Fecha_Fin; }
            set { Fecha_Fin = value; }
        }

        #endregion Variables Publicas

        #region Metodos

        public DataTable Consultar_Listado_Ordenes_Compra() {
            return Cls_Ope_Tal_Seguimiento_Ordenes_Compra_Taller_Datos.Consultar_Listado_Ordenes_Compra(this);
        }

        public DataTable Consultar_Historial_Ordenes_Compra()
        {
            return Cls_Ope_Tal_Seguimiento_Ordenes_Compra_Taller_Datos.Consultar_Historial_Ordenes_Compra(this);
        }

        #endregion Metodos

    }
}
