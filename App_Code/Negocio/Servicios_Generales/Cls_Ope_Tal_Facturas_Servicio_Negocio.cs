﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Taller_Mecanico.Operacion_Facturas_Servicio.Datos;
using System.Data;
using System.Data.SqlClient;

namespace JAPAMI.Taller_Mecanico.Operacion_Facturas_Servicio.Negocio
{
    public class Cls_Ope_Tal_Facturas_Servicio_Negocio
    {
        #region "Variables Internas"

        private Int32 No_Servicio = (-1);
        private Int32 No_Contrarecibo = (-1);
        private Int32 No_Entrada = (-1);
        private Int32 No_Solicitud = (-1);
        private Int32 No_Asignacion_Proveedor = (-1);
        private Int32 No_Seguimiento_Proveedor = (-1);
        private String Mecanico_ID = String.Empty;
        private String Diagnostico = String.Empty;
        private String Usuario = String.Empty;
        private String Estatus = String.Empty;
        private String Estatus_Servicio = String.Empty;        
        private DateTime Fecha_Inicio_Revision = new DateTime();
        private DateTime Fecha_Fin_Revision = new DateTime();
        private String Reparacion = String.Empty;
        private String Proveedor_ID = String.Empty;
        private String Observaciones = String.Empty;
        private String Tipo = String.Empty;        
        private String Campos_Dinamicos = String.Empty;
        private String Campos_Dinamicos_Proveedor = String.Empty;
        private String Filtros_Dinamicos = String.Empty;
        private DataTable Dt_Archivos = new DataTable();        
        private Int32 No_Factura_Servicio = (-1);
        private Int32 No_Factura = (-1);
        private Int32 Folio_Solicitud = (-1);
        private Double Monto = (-1);
        private String Nombre_Archivo = String.Empty;
        private String Ruta_Archivo = String.Empty;
        private String No_Solicitud_Pago = String.Empty;
        private Double Monto_Solicitud_Pago = (-1);
        private Boolean Cerrar_Servicio = false;
        private SqlTransaction Trans;
        private SqlCommand Cmmd;

        #endregion

        #region "Variables Públicas"

        public SqlTransaction P_Trans
        {
            get { return Trans; }
            set { Trans = value; }
        }
        public SqlCommand P_Cmmd
        {
            get { return Cmmd; }
            set { Cmmd = value; }
        }
        public Boolean P_Cerrar_Servicio
        {
            get { return Cerrar_Servicio; }
            set { Cerrar_Servicio = value; }
        }
        public Int32 P_No_Servicio
        {
            get { return No_Servicio; }
            set { No_Servicio = value; }
        }
        public Int32 P_No_Contrarecibo
        {
            get { return No_Contrarecibo; }
            set { No_Contrarecibo = value; }
        }
        public Int32 P_No_Entrada
        {
            get { return No_Entrada; }
            set { No_Entrada = value; }
        }
        public Int32 P_No_Solicitud
        {
            get { return No_Solicitud; }
            set { No_Solicitud = value; }
        }
        public Int32 P_Folio_Solicitud
        {
            get { return Folio_Solicitud; }
            set { Folio_Solicitud = value; }
        }
        public Int32 P_No_Seguimiento_Proveedor
        {
            get { return No_Seguimiento_Proveedor; }
            set { No_Seguimiento_Proveedor = value; }
        }
        public Int32 P_No_Factura_Servicio
        {
            get { return No_Factura_Servicio; }
            set { No_Factura_Servicio = value; }
        }
        public Int32 P_No_Factura
        {
            get { return No_Factura; }
            set { No_Factura = value; }
        }
        public Double P_Costo
        {
            get { return Monto; }
            set { Monto = value; }
        }
        public String P_Mecanico_ID
        {
            get { return Mecanico_ID; }
            set { Mecanico_ID = value; }
        }
        public String P_Diagnostico
        {
            get { return Diagnostico; }
            set { Diagnostico = value; }
        }
        public String P_Usuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_Estatus_Servicio
        {
            get { return Estatus_Servicio; }
            set { Estatus_Servicio = value; }
        }
        public DateTime P_Fecha_Inicio_Revision
        {
            get { return Fecha_Inicio_Revision; }
            set { Fecha_Inicio_Revision = value; }
        }
        public DateTime P_Fecha_Fin_Revision
        {
            get { return Fecha_Fin_Revision; }
            set { Fecha_Fin_Revision = value; }
        }
        public String P_Reparacion
        {
            get { return Reparacion; }
            set { Reparacion = value; }
        }
        public Int32 P_No_Asignacion_Proveedor
        {
            get { return No_Asignacion_Proveedor; }
            set { No_Asignacion_Proveedor = value; }
        }
        public String P_Proveedor_ID
        {
            get { return Proveedor_ID; }
            set { Proveedor_ID = value; }
        }
        public String P_Observaciones
        {
            get { return Observaciones; }
            set { Observaciones = value; }
        }
        public String P_Tipo
        {
            get { return Tipo; }
            set { Tipo = value; }
        }
        public String P_Nombre_Archivo
        {
            get { return Nombre_Archivo; }
            set { Nombre_Archivo = value; }
        }
        public String P_Ruta_Archivo
        {
            get { return Ruta_Archivo; }
            set { Ruta_Archivo = value; }
        }
        public String P_Filtros_Dinamicos
        {
            get { return Filtros_Dinamicos; }
            set { Filtros_Dinamicos = value; }
        }
        public String P_Campos_Dinamicos
        {
            get { return Campos_Dinamicos; }
            set { Campos_Dinamicos = value; }
        }
        public String P_Campos_Dinamicos_Proveedor
        {
            get { return Campos_Dinamicos_Proveedor; }
            set { Campos_Dinamicos_Proveedor = value; }
        }
        public DataTable P_Dt_Archivos
        {
            get { return Dt_Archivos; }
            set { Dt_Archivos = value; }
        }
        public String P_No_Solicitud_Pago
        {
            get { return No_Solicitud_Pago; }
            set { No_Solicitud_Pago = value; }
        }
        public Double P_Monto_Solicitud_Pago
        {
            get { return Monto_Solicitud_Pago; }
            set { Monto_Solicitud_Pago = value; }
        }
        #endregion

        #region [Metodos]
        public void Alta_Proveedor_Servicio()
        {
            Cls_Ope_Tal_Facturas_Servicio_Datos.Alta_Proveedor_Servicio(this);
        }
        public DataTable Consulta_Proveedor_Servicio()
        {
            return Cls_Ope_Tal_Facturas_Servicio_Datos.Consulta_Proveedor_Servicio(this);
        }
        public void Modificar_Proveedor_Servicio()
        {
            Cls_Ope_Tal_Facturas_Servicio_Datos.Modificar_Proveedor_Servicio(this);
        }
        public DataTable Consultar_Asignaciones_Servicios() {
            return Cls_Ope_Tal_Facturas_Servicio_Datos.Consultar_Asignaciones_Servicios(this);
        }
        public Cls_Ope_Tal_Facturas_Servicio_Negocio Consultar_Detalles_Asignacion_Servicio()
        {
            return Cls_Ope_Tal_Facturas_Servicio_Datos.Consultar_Detalles_Asignacion_Servicio(this);
        }
        public void Alta_Factura()
        {
            Cls_Ope_Tal_Facturas_Servicio_Datos.Alta_Factura(this);
        }
        public DataTable Consulta_Factura()
        {
            return Cls_Ope_Tal_Facturas_Servicio_Datos.Consulta_Factura(this);
        }
        public Cls_Ope_Tal_Facturas_Servicio_Negocio Crear_Contrarecibo()
        {
            return Cls_Ope_Tal_Facturas_Servicio_Datos.Crear_Contrarecibo(this);
        }
        #endregion

        
    }
}