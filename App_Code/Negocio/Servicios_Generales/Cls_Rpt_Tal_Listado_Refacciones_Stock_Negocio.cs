﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Reporte_Listado_Refacciones_Stock.Datos;

/// <summary>
/// Summary description for Cls_Rpt_Tal_Listado_Refacciones_Stock_Negocio
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Reporte_Listado_Refacciones_Stock.Negocio {
    public class Cls_Rpt_Tal_Listado_Refacciones_Stock_Negocio {

        #region VARIABLES INTERNAS

            private String Ubicacion = String.Empty;
            private String Tipo_Refaccion_ID = String.Empty;
            private String Capitulo_ID = String.Empty;
            private String Concepto_ID = String.Empty;
            private String Partida_Generica_ID = String.Empty;
            private String Partida_Especifica_ID = String.Empty;

        #endregion

        #region VARIABLES PUBLICAS

            public String P_Ubicacion {
                get { return Ubicacion; }
                set { Ubicacion = value; }
            }
            public String P_Tipo_Refaccion_ID
            {
                get { return Tipo_Refaccion_ID; }
                set { Tipo_Refaccion_ID = value; }
            }
            public String P_Capitulo_ID
            {
                get { return Capitulo_ID; }
                set { Capitulo_ID = value; }
            }
            public String P_Concepto_ID
            {
                get { return Concepto_ID; }
                set { Concepto_ID = value; }
            }
            public String P_Partida_Generica_ID
            {
                get { return Partida_Generica_ID; }
                set { Partida_Generica_ID = value; }
            }
            public String P_Partida_Especifica_ID
            {
                get { return Partida_Especifica_ID; }
                set { Partida_Especifica_ID = value; }
            }

        #endregion

        #region METODOS

            public DataTable Consultar_Listado_Refacciones_Stock() {
                return Cls_Rpt_Tal_Listado_Refacciones_Stock_Datos.Consultar_Listado_Refacciones_Stock(this);
            }            

        #endregion

	}
}