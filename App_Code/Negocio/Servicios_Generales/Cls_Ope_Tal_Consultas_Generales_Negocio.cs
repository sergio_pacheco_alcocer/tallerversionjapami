﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Datos;
using System.Text;
using System.Data.SqlClient;
/// <summary>
/// Summary description for Cls_Tal_Ope_Consultas_Generales_Negocio
/// </summary>

namespace JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio { 

    public class Cls_Ope_Tal_Consultas_Generales_Negocio {

        #region "Variables Internas"

            private String Estatus = String.Empty;
            private String Vehiculo_ID = String.Empty;
            private String Bien_Mueble_ID = String.Empty;
            private String No_Inventario = String.Empty;
            private String No_Economico = String.Empty;
            private String Dependencia_ID = String.Empty;
            private String Empleado_ID = String.Empty;
            private String No_Empleado = String.Empty;
            private String RFC_Empleado = String.Empty;
            private String Nombre_Empleado = String.Empty;
            private String Nombre = String.Empty;
            private String Tipo = String.Empty;
            private String Parent = String.Empty;
            private Int32 No_Servicio = (-1);
            private Int32 No_Solicitud = (-1);
            private String No_Serie = String.Empty;
            private String Clave_UR = String.Empty;
            private String Nombre_UR = String.Empty;
            private String Clave = String.Empty;
            private String Descripcion = String.Empty;
            private String Capitulo_ID = String.Empty;
            private String Concepto_ID = String.Empty;
            private String Partida_Generica_ID = String.Empty;
            private String Partida_Especifica_ID = String.Empty;
            private String Grupo_Dependencia_ID = String.Empty;
            private SqlTransaction Trans;
            private SqlCommand Cmmd;

        #endregion 
        
        #region "Variables Publicas"

            public SqlTransaction P_Trans
            {
                get { return Trans; }
                set { Trans = value; }
            }
            public SqlCommand P_Cmmd
            {
                get { return Cmmd; }
                set { Cmmd = value; }
            }
            public String P_Estatus{
                set { Estatus = value; }
                get { return Estatus; }
            }
            public String P_Vehiculo_ID
            {
                set { Vehiculo_ID = value; }
                get { return Vehiculo_ID; }
            }
            public String P_Bien_Mueble_ID
            {
                set { Bien_Mueble_ID = value; }
                get { return Bien_Mueble_ID; }
            }
            public String P_No_Inventario
            {
                set { No_Inventario = value; }
                get { return No_Inventario; }
            }
            public String P_No_Economico
            {
                set { No_Economico = value; }
                get { return No_Economico; }
            }
            public String P_No_Serie
            {
                set { No_Serie = value; }
                get { return No_Serie; }
            }
            public String P_Dependencia_ID
            {
                set { Dependencia_ID = value; }
                get { return Dependencia_ID; }
            }
            public String P_Empleado_ID
            {
                set { Empleado_ID = value; }
                get { return Empleado_ID; }
            }
            public String P_No_Empleado
            {
                set { No_Empleado = value; }
                get { return No_Empleado; }
            }
            public String P_RFC_Empleado
            {
                set { RFC_Empleado = value; }
                get { return RFC_Empleado; }
            }
            public String P_Nombre_Empleado
            {
                set { Nombre_Empleado = value; }
                get { return Nombre_Empleado; }
            }
            public String P_Nombre
            {
                set { Nombre = value; }
                get { return Nombre; }
            }
            public String P_Tipo
            {
                set { Tipo = value; }
                get { return Tipo; }
            }
            public String P_Parent
            {
                set { Parent = value; }
                get { return Parent; }
            }
            public Int32 P_No_Solicitud
            {
                set { No_Solicitud = value; }
                get { return No_Solicitud; }
            }
            public Int32 P_No_Servicio
            {
                set { No_Servicio = value; }
                get { return No_Servicio; }
            }
            public String P_Clave_UR
            {
                set { Clave_UR = value; }
                get { return Clave_UR; }
            }
            public String P_Nombre_UR
            {
                set { Nombre_UR = value; }
                get { return Nombre_UR; }
            }
            public String P_Clave
            {
                set { Clave = value; }
                get { return Clave; }
            }
            public String P_Descripcion
            {
                set { Descripcion = value; }
                get { return Descripcion; }
            }
            public String P_Capitulo_ID
            {
                set { Capitulo_ID = value; }
                get { return Capitulo_ID; }
            }
            public String P_Concepto_ID
            {
                set { Concepto_ID = value; }
                get { return Concepto_ID; }
            }
            public String P_Partida_Generica_ID
            {
                set { Partida_Generica_ID = value; }
                get { return Partida_Generica_ID; }
            }
            public String P_Partida_Especifica_ID
            {
                set { Partida_Especifica_ID = value; }
                get { return Partida_Especifica_ID; }
            }
            public String P_Grupo_Dependencia_ID
            {
                set { Grupo_Dependencia_ID = value; }
                get { return Grupo_Dependencia_ID; }
            }

        #endregion 
        
        #region "Metodos"

            public DataTable Consultar_Unidades_Responsables() {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Unidades_Responsables(this);
            }
            public DataTable Consultar_Grupos_Unidades_Responsables()
            {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Grupos_Unidades_Responsables(this);
            }            
            public DataTable Consultar_Vehiculos() {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Vehiculos(this);
            }       
            public DataTable Consultar_Bien_Mueble() {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Bien_Mueble(this);
            }
            public DataTable Consultar_Empleados() {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Empleados(this);
            }
            public DataTable Consultar_Partes_SubPartes_Vehiculo() {
                DataTable Dt_Completo = new DataTable();
                Dt_Completo.Columns.Add("PARTE_ID", Type.GetType("System.String"));
                Dt_Completo.Columns.Add("PARTE_NOMBRE", Type.GetType("System.String"));
                Dt_Completo.Columns.Add("SUBPARTE_ID", Type.GetType("System.String"));
                Dt_Completo.Columns.Add("SUBPARTE_NOMBRE", Type.GetType("System.String"));
                this.P_Estatus = "VIGENTE";
                this.P_Tipo = "PARTE";
                Boolean Bnd_Coma = false;
                DataTable Dt_Partes = Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Partes(this);
                StringBuilder Str_Builder = new StringBuilder();
                foreach (DataRow Fila in Dt_Partes.Rows) {
                    if (Bnd_Coma) { Str_Builder.Append(","); }
                    Str_Builder.Append("'" + Fila["PARTE_ID"].ToString().Trim() + "'");
                    Bnd_Coma = true;
                }
                this.P_Parent = Str_Builder.ToString();
                this.P_Tipo = "SUBPARTE";
                DataTable Dt_SubPartes = Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Partes(this);
                foreach (DataRow Fila in Dt_Partes.Rows) {
                    String Parte_ID = Fila["PARTE_ID"].ToString().Trim();
                    String Parte_Nombre = Fila["NOMBRE"].ToString().Trim();
                    DataRow[] Filas_Filtradas_SubPartes = Dt_SubPartes.Select("PARENT_ID = '" + Parte_ID.Trim() + "'");
                    foreach (DataRow Sub_Fila in Filas_Filtradas_SubPartes) {
                        String SubParte_ID = Sub_Fila["PARTE_ID"].ToString().Trim();
                        String SubParte_Nombre = Sub_Fila["NOMBRE"].ToString().Trim();

                        //Agregar Fila
                        DataRow Fila_Nueva = Dt_Completo.NewRow();
                        Fila_Nueva["PARTE_ID"] = Parte_ID;
                        Fila_Nueva["PARTE_NOMBRE"] = Parte_Nombre;
                        Fila_Nueva["SUBPARTE_ID"] =SubParte_ID;
                        Fila_Nueva["SUBPARTE_NOMBRE"] = SubParte_Nombre;
                        Dt_Completo.Rows.Add(Fila_Nueva);
                    }
                }

                return Dt_Completo;
            }

            public DataTable Consultar_Proveedor_Servicio() {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Proveedor_Servicio(this);
            }

            public DataTable Consultar_Capitulos()
            {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Capitulos(this);
            }
            public String Consultar_Codigo_Programatico_Servicios(int No_Solicitud,ref SqlCommand Cmd)
            {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Codigo_Programatico_Servicios(No_Solicitud, ref Cmd);
            }
            public DataTable Consultar_Conceptos()
            {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Conceptos(this);
            }
            public DataTable Consultar_Partidas_Genericas()
            {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Partidas_Genericas(this);
            }
            public DataTable Consultar_Partidas_Especificas()
            {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Partidas_Especificas(this);
            }
            public DataTable Consultar_Tabla_Relacionada_SGC()
            {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Tabla_Relacionada_SGC(this);
            }
            public DataTable Consultar_Fuentes_Financiamiento()
            {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Fuentes_Financiamiento(this);
            }
            public DataTable Consultar_Proyectos_Programas()
            {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Proyectos_Programas(this);
            }
            public DataTable Consultar_Resguardantes_Vehiculos()
            {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Resguardantes_Vehiculos(this);
            }
            public DataTable Consultar_Resguardantes_Bienes()
            {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Resguardantes_Bienes(this);
            }
            public DataTable Consultar_Tipos_Solicitud_Pago()
            {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Tipos_Solicitud_Pago(this);
            }
      
            public DataTable Consultar_Partes_SubPartes_Vehiculo_Revista_Mecanica() {
                DataTable Dt_Completo = new DataTable();
                Dt_Completo.Columns.Add("PARTE_ID", Type.GetType("System.String"));
                Dt_Completo.Columns.Add("PARTE_NOMBRE", Type.GetType("System.String"));
                Dt_Completo.Columns.Add("SUBPARTE_ID", Type.GetType("System.String"));
                Dt_Completo.Columns.Add("SUBPARTE_NOMBRE", Type.GetType("System.String"));
                this.P_Estatus = "VIGENTE";
                this.P_Tipo = "PARTE";
                Boolean Bnd_Coma = false;
                DataTable Dt_Partes = Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Partes_Revista_Mecanica(this);
                StringBuilder Str_Builder = new StringBuilder();
                foreach (DataRow Fila in Dt_Partes.Rows) {
                    if (Bnd_Coma) { Str_Builder.Append(","); }
                    Str_Builder.Append("'" + Fila["PARTE_ID"].ToString().Trim() + "'");
                    Bnd_Coma = true;
                }
                this.P_Parent = Str_Builder.ToString();
                this.P_Tipo = "SUBPARTE";
                DataTable Dt_SubPartes = Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Partes_Revista_Mecanica(this);
                foreach (DataRow Fila in Dt_Partes.Rows) {
                    String Parte_ID = Fila["PARTE_ID"].ToString().Trim();
                    String Parte_Nombre = Fila["NOMBRE"].ToString().Trim();
                    DataRow[] Filas_Filtradas_SubPartes = Dt_SubPartes.Select("PARENT_ID = '" + Parte_ID.Trim() + "'");
                    foreach (DataRow Sub_Fila in Filas_Filtradas_SubPartes) {
                        String SubParte_ID = Sub_Fila["PARTE_ID"].ToString().Trim();
                        String SubParte_Nombre = Sub_Fila["NOMBRE"].ToString().Trim();

                        //Agregar Fila
                        DataRow Fila_Nueva = Dt_Completo.NewRow();
                        Fila_Nueva["PARTE_ID"] = Parte_ID;
                        Fila_Nueva["PARTE_NOMBRE"] = Parte_Nombre;
                        Fila_Nueva["SUBPARTE_ID"] =SubParte_ID;
                        Fila_Nueva["SUBPARTE_NOMBRE"] = SubParte_Nombre;
                        Dt_Completo.Rows.Add(Fila_Nueva);
                    }
                }
                return Dt_Completo;
            }

            public DataTable Consultar_Proveedores() {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Proveedores(this);
            }
            public String Consultar_Proyecto_Programa_Solicitud()
            {
                return Cls_Ope_Tal_Consultas_Generales_Datos.Consultar_Proyecto_Programa_Solicitud(this);
            }

        #endregion 

    }

}