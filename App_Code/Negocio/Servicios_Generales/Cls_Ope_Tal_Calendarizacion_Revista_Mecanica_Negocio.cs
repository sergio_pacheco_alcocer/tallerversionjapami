﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Operacion_Calendarizacion_Revista_Mecanica.Datos;

/// <summary>
/// Summary description for Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Operacion_Calendarizacion_Revista_Mecanica.Negocio {
    public class Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio {
	            
        #region Variables Internas

            private String Vehiculo_ID = String.Empty;
            private String Periodo_ID = String.Empty;
            private DateTime Fecha_Ultima_Revision = new DateTime();
            private DateTime Fecha_Proxima_Revision = new DateTime();
            private String Usuario = String.Empty;
            private SqlTransaction Trans;
            private SqlCommand Cmmd;

        #endregion

        #region Variables Publicas

            public SqlTransaction P_Trans
            {
                get { return Trans; }
                set { Trans = value; }
            }
            public SqlCommand P_Cmmd
            {
                get { return Cmmd; }
                set { Cmmd = value; }
            }
            public String P_Vehiculo_ID {
                get { return Vehiculo_ID; }
                set { Vehiculo_ID = value; }
            }
            public String P_Periodo_ID {
                get { return Periodo_ID; }
                set { Periodo_ID = value; }
            }
            public DateTime P_Fecha_Ultima_Revision {
                get { return Fecha_Ultima_Revision; }
                set { Fecha_Ultima_Revision = value; }
            }
            public DateTime P_Fecha_Proxima_Revision {
                get { return Fecha_Proxima_Revision; }
                set { Fecha_Proxima_Revision = value; }
            }
            public String P_Usuario {
                get { return Usuario; }
                set { Usuario = value; }
            }

        #endregion

        #region Metodos

            public void Actualizar_Registro() {
                Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Datos.Actualizar_Registro(this);
            }
            public Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio Consultar_Detalles_Registro(){
                return Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Datos.Consultar_Detalles_Registro(this);
            }
            public void Sincronizacion_Revista_Mecanica_Periodo(){
                Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Datos.Sincronizacion_Revista_Mecanica_Periodo(this);    
            }
            public DataTable Consulta_General_Calendarizacion_Revistas_Mecanicas() {
                return Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Datos.Consulta_General_Calendarizacion_Revistas_Mecanicas(this);
            }
            
        #endregion

	}
}
