﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Seguimiento_Requisiciones_Taller.Datos;

/// <summary>
/// Summary description for Cls_Ope_Tal_Seguimiento_Requisiciones_Negocio
/// </summary>
/// 
namespace JAPAMI.Seguimiento_Requisiciones_Taller.Negocio {
    public class Cls_Ope_Tal_Seguimiento_Requisiciones_Negocio
    {
        #region Variables Privadas

            private String Dependencia_ID = String.Empty;
            private String Requisicion_ID = String.Empty;
            private String Estatus = String.Empty;
            private DateTime Fecha_Inicio = new DateTime();
            private DateTime Fecha_Fin = new DateTime();

        #endregion 

        #region Variables Publicas

            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Requisicion_ID
            {
                get { return Requisicion_ID; }
                set { Requisicion_ID = value; }
            }     
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }
            public DateTime P_Fecha_Inicio
            {
                get { return Fecha_Inicio; }
                set { Fecha_Inicio = value; }
            }
            public DateTime P_Fecha_Fin
            {
                get { return Fecha_Fin; }
                set { Fecha_Fin = value; }
            }

        #endregion
       
        #region Metodos

            public DataTable Consultar_Listado_Requisiciones() {
                return Cls_Ope_Tal_Seguimiento_Requisiciones_Datos.Consultar_Listado_Requisiciones(this);
            }

            public DataTable Consultar_Historial_Requisiciones() {
                return Cls_Ope_Tal_Seguimiento_Requisiciones_Datos.Consultar_Historial_Requisiciones(this);
            }

            public DataTable Consultar_Historial_Observaciones() {
                return Cls_Ope_Tal_Seguimiento_Requisiciones_Datos.Consultar_Historial_Observaciones(this);
            }

        #endregion 
    }
}