﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Administracion_Proveedores_Servicios_Bien_Mueble.Datos;
using System.Text;

/// <summary>
/// Summary description for Cls_Ope_Tal_Administracion_Proveedores_Servicios_Bien_Mueble_Negocio
/// </summary>

namespace JAPAMI.Taller_Mecanico.Administracion_Proveedores_Servicios_Bien_Mueble.Negocio { 
    public class Cls_Ope_Tal_Administracion_Proveedores_Servicios_Bien_Mueble_Negocio {

        #region "Variables Internas"

            private Int32 No_Solicitud = (-1);
            private String Usuario = String.Empty;
            private DataTable Dt_Proveedores = new DataTable();

        #endregion 
        
        #region "Variables Publicas"

            public Int32 P_No_Solicitud {
                set { No_Solicitud = value; }
                get { return No_Solicitud; }
            }
            public String P_Usuario
            {
                set { Usuario = value; }
                get { return Usuario; }
            }
            public DataTable P_Dt_Proveedores
            {
                set { Dt_Proveedores = value; }
                get { return Dt_Proveedores; }
            }

        #endregion 
        
        #region "Metodos"

            public void Alta_Proveedores_Servicio() {
                Cls_Ope_Tal_Administracion_Proveedores_Servicios_Bien_Mueble_Datos.Alta_Proveedores_Servicio(this);
            }
        
        #endregion

	}
}