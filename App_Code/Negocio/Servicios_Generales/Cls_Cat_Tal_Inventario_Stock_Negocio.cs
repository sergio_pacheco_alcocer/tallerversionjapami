﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Taller_Mecanico.Catalogo_Inventario_Stock.Datos;

/// <summary>
/// Summary description for Cls_Cat_Tal_Inventario_Stock_Negocio
/// </summary>
namespace JAPAMI.Taller_Mecanico.Catalogo_Inventario_Stock.Negocio
{
    public class Cls_Cat_Tal_Inventario_Stock_Negocio
    {
        #region Variables Internas

            private String Refaccion_ID = String.Empty;
            private DataTable Dt_Salidas_Express = new DataTable();
            private DataTable Dt_Salidas = new DataTable();

        #endregion

        #region Variables Publicas

            public String P_Refaccion_ID
            {
                get { return Refaccion_ID; }
                set { Refaccion_ID = value; }
            }
            public DataTable P_Dt_Salidas_Express
            {
                get { return Dt_Salidas_Express; }
                set { Dt_Salidas_Express = value; }
            }
            public DataTable P_Dt_Salidas
            {
                get { return Dt_Salidas; }
                set { Dt_Salidas = value; }
            }

        #endregion

        #region Metodos

            public DataTable Consultar_Salidas_Express()
            {
                return Cls_Cat_Tal_Inventario_Stock_Datos.Consultar_Salidas_Express(this);
            }

            public DataTable Consultar_Salidas_Almacen()
            {
                return Cls_Cat_Tal_Inventario_Stock_Datos.Consultar_Salidas_Almacen(this);
            }

        #endregion
    }
}