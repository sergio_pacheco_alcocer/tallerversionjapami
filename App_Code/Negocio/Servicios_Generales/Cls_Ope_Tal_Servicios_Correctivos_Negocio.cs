﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Datos;

/// <summary>
/// Summary description for Cls_Ope_Tal_Servicios_Correctivos_Negocio
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Negocio {
    public class Cls_Ope_Tal_Servicios_Correctivos_Negocio {

        #region "Variables Internas"

            private Int32 No_Servicio = (-1);
            private Int32 No_Entrada = (-1);
            private Int32 No_Solicitud = (-1);
            private String Mecanico_ID = String.Empty;
            private String Tipo_Trabajo_ID = String.Empty;
            private String Tipo_Bien = String.Empty;
            private String Tipo_Servicio = String.Empty;
            private String Diagnostico = String.Empty;
            private String Usuario = String.Empty;
            private String Estatus = String.Empty;
            private String Estatus_Solicitud = String.Empty;
            private String No_Economico = String.Empty;
            private DateTime Fecha_Inicio_Revision = new DateTime();
            private DateTime Fecha_Fin_Revision = new DateTime();
            private String Reparacion = String.Empty;
            private DateTime Fecha_Entrega_Probable = new DateTime();
            private DataTable Dt_Refacciones = new DataTable();
            private String Usuario_ID = String.Empty;
            private String Procedencia = String.Empty;
            private Double Costo_Hora = (-1);
            private Int32 No_Inventario = (-1);
            private Int32 Folio_Solicitud = (-1);
            private String Ordernar_Dinamico = String.Empty;            
            private SqlTransaction Trans;
            private SqlCommand Cmmd;

        #endregion

        #region "Variables Públicas"

            public SqlTransaction P_Trans
            {
                get { return Trans; }
                set { Trans = value; }
            }
            public SqlCommand P_Cmmd
            {
                get { return Cmmd; }
                set { Cmmd = value; }
            }
            public Int32 P_No_Servicio {
                get { return No_Servicio; }
                set { No_Servicio = value; }
            }
            public Int32 P_No_Entrada
            {
                get { return No_Entrada; }
                set { No_Entrada = value; }
            }
            public Int32 P_No_Solicitud
            {
                get { return No_Solicitud; }
                set { No_Solicitud = value; }
            }
            public String P_Mecanico_ID
            {
                get { return Mecanico_ID; }
                set { Mecanico_ID = value; }
            }
            public String P_Tipo_Trabajo_ID
            {
                get { return Tipo_Trabajo_ID; }
                set { Tipo_Trabajo_ID = value; }
            }
            public String P_Tipo_Bien
            {
                get { return Tipo_Bien; }
                set { Tipo_Bien = value; }
            }
            public String P_Tipo_Servicio
            {
                get { return Tipo_Servicio; }
                set { Tipo_Servicio = value; }
            }
            public String P_Diagnostico
            {
                get { return Diagnostico; }
                set { Diagnostico = value; }
            }
            public String P_Usuario
            {
                get { return Usuario; }
                set { Usuario = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public DateTime P_Fecha_Inicio_Revision
            {
                get { return Fecha_Inicio_Revision; }
                set { Fecha_Inicio_Revision = value; }
            }
            public DateTime P_Fecha_Fin_Revision
            {
                get { return Fecha_Fin_Revision; }
                set { Fecha_Fin_Revision = value; }
            }
            public String P_Reparacion
            {
                get { return Reparacion; }
                set { Reparacion = value; }
            }
            public DateTime P_Fecha_Entrega_Probable
            {
                get { return Fecha_Entrega_Probable; }
                set { Fecha_Entrega_Probable = value; }
            }
            public DataTable P_Dt_Refacciones
            {
                get { return Dt_Refacciones; }
                set { Dt_Refacciones = value; }
            }
            public String P_Estatus_Solicitud
            {
                get { return Estatus_Solicitud; }
                set { Estatus_Solicitud = value; }
            }
            public String P_Usuario_ID
            {
                get { return Usuario_ID; }
                set { Usuario_ID = value; }
            }
            public String P_Procedencia
            {
                get { return Procedencia; }
                set { Procedencia = value; }
            }
            public Double P_Costo_Hora
            {
                get { return Costo_Hora; }
                set { Costo_Hora = value; }
            }
            public Int32 P_No_Inventario
            {
                get { return No_Inventario; }
                set { No_Inventario = value; }
            }
            public String P_No_Economico
            {
                get { return No_Economico; }
                set { No_Economico = value; }
            }
            public Int32 P_Folio_Solicitud
            {
                get { return Folio_Solicitud; }
                set { Folio_Solicitud = value; }
            }
            public String P_Ordernar_Dinamico
            {
                get { return Ordernar_Dinamico; }
                set { Ordernar_Dinamico = value; }
            }
        
        #endregion

        #region "Metodos"

            public void Alta_Servicio_Correctivo() {
                Cls_Ope_Tal_Servicios_Correctivos_Datos.Alta_Servicio_Correctivo(this);
            }

            public Int32 Alta_Servicio_Correctivo(ref SqlCommand Cmd) {
                return Cls_Ope_Tal_Servicios_Correctivos_Datos.Alta_Servicio_Correctivo(this, ref Cmd);
            }

            public void Modifica_Servicio_Correctivo() {
                Cls_Ope_Tal_Servicios_Correctivos_Datos.Modifica_Servicio_Correctivo(this);
            }

            public void Asignar_Diagnostico_Servicio_Correctivo() {
                Cls_Ope_Tal_Servicios_Correctivos_Datos.Asignar_Diagnostico_Servicio_Correctivo(this);
            }

            public void Cerrar_Servicio_Correctivo() {
                Cls_Ope_Tal_Servicios_Correctivos_Datos.Cerrar_Servicio_Correctivo(this);
            }

            public Cls_Ope_Tal_Servicios_Correctivos_Negocio Consultar_Detalles_Servicio_Correctivo() {
                return Cls_Ope_Tal_Servicios_Correctivos_Datos.Consultar_Detalles_Servicio_Correctivo(this);
            }

            public DataTable Consultar_Servicios_Correctivos() {
                return Cls_Ope_Tal_Servicios_Correctivos_Datos.Consultar_Servicios_Correctivos(this);
            }
            public void Modifica_Servicio_Correctivo_Costo()
            {
                Cls_Ope_Tal_Servicios_Correctivos_Datos.Modifica_Servicio_Correctivo_Costo(this);
            }


        #endregion
	
    }
}