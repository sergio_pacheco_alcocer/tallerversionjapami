﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace JAPAMI.Taller_Mecanico.Ope_Facturas_Tarjetas_Gasolina.Negocio
{
    public class Cls_Ope_Tal_Facturas_Movimientos_Tarjetas_Negocio
    {
        #region (Variable Públicas)

        public string folio_factura { set; get; }
        public string fecha_factura { set; get; }
        public string monto_factura { set; get; }

        public string iva_factura { set; get; }
        public string total_factura { set; get; }        

        #endregion
    }
}