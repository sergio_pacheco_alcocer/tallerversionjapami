﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Cancelar_Ordenes.Datos;

/// <summary>
/// Summary description for Cls_Ope_Tal_Cancelar_Ordenes_Compra_Negocio
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Cancelar_Ordenes.Negocio
{
    public class Cls_Ope_Tal_Cancelar_Ordenes_Compra_Negocio
    {
        #region Variables_Internas
            
            private String No_Orden_Compra;
            private String No_Requisicio;
            private String Estatus;
            private String Fecha_Inicio;
            private String Fecha_Fin;
            private String Folio_Busqueda;
            private String Monto_Total;
            private String Motivo_Cancelacion;
            private String Listado_Almacen;
            private String Dependencia_Id;
            private String Tipo_Bien = String.Empty;
            private String No_Inventario = String.Empty;
            private String No_Economico = String.Empty;
            private String Usuario = String.Empty;

        #endregion

        #region Variables_Publicas
            
            public String P_No_Orden_Compra
            {
                get { return No_Orden_Compra; }
                set { No_Orden_Compra = value; }
            }
            public String P_No_Requisicio
            {
                get { return No_Requisicio; }
                set { No_Requisicio = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Dependencia_Id
            {
                get { return Dependencia_Id; }
                set { Dependencia_Id = value; }
            }
            public String P_No_Inventario
            {
                get { return No_Inventario; }
                set { No_Inventario = value; }
            }
            public String P_No_Economico
            {
                get { return No_Economico; }
                set { No_Economico = value; }
            }
            public String P_Tipo_Bien
            {
                get { return Tipo_Bien; }
                set { Tipo_Bien = value; }
            }
            public String P_Fecha_Inicio
            {
                get { return Fecha_Inicio; }
                set { Fecha_Inicio = value; }
            }
            public String P_Fecha_Fin
            {
                get { return Fecha_Fin; }
                set { Fecha_Fin = value; }
            }
            public String P_Folio_Busqueda
            {
                get { return Folio_Busqueda; }
                set { Folio_Busqueda = value; }
            }
            public String P_Monto_Total
            {
                get { return Monto_Total; }
                set { Monto_Total = value; }
            }
            public String P_Motivo_Cancelacion
            {
                get { return Motivo_Cancelacion; }
                set { Motivo_Cancelacion = value; }
            }
            public String P_Listado_Almacen
            {
                get { return Listado_Almacen; }
                set { Listado_Almacen = value; }
            }
            public String P_Usuario
            {
                get { return Usuario; }
                set { Usuario = value; }
            }

        #endregion

        #region Metodos

            public DataTable Consultar_Ordenes_Compra()
            {
                return Cls_Ope_Tal_Cancelar_Ordenes_Compra_Datos.Consultar_Ordenes_Compra(this);
            }
            
            public DataTable Consultar_Productos_Servicios()
            {
                return Cls_Ope_Tal_Cancelar_Ordenes_Compra_Datos.Consultar_Productos_Servicios(this);
            }
            public void Liberar_Presupuesto_Cancelacion_Total()
            {
                Cls_Ope_Tal_Cancelar_Ordenes_Compra_Datos.Liberar_Presupuesto_Cancelacion_Total(this);
            }
            public String Modificar_Orden_Compra()
            {
                return Cls_Ope_Tal_Cancelar_Ordenes_Compra_Datos.Modificar_Orden_Compra(this);
            }
            public String Liberar_Presupuesto_Cancelacion_Parcial()
            {
                return Cls_Ope_Tal_Cancelar_Ordenes_Compra_Datos.Liberar_Presupuesto_Cancelacion_Parcial(this);
            }

        #endregion

    }
}