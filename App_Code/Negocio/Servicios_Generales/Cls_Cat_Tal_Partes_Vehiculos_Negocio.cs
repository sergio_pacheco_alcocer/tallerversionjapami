﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Catalogo_Partes_Vehiculos.Datos;

/// <summary>
/// Summary description for Cls_Cat_Tal_Partes_Vehiculos_Negocio
/// </summary>
namespace JAPAMI.Taller_Mecanico.Catalogo_Partes_Vehiculos.Negocio
{
    public class Cls_Cat_Tal_Partes_Vehiculos_Negocio {

        #region Variables Internas

            private String Parte_ID = String.Empty;
            private String Nombre = String.Empty;
            private String Tipo = String.Empty;
            private String Parent = String.Empty;
            private String Estatus = String.Empty;
            private String Usuario = String.Empty;

        #endregion

        #region Variables Publicas

            public String P_Parte_ID {
                get { return Parte_ID; }
                set { Parte_ID = value; }
            }
            public String P_Nombre
            {
                get { return Nombre; }
                set { Nombre = value; }
            }
            public String P_Tipo
            {
                get { return Tipo; }
                set { Tipo = value; }
            }
            public String P_Parent
            {
                get { return Parent; }
                set { Parent = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Usuario
            {
                get { return Usuario; }
                set { Usuario = value; }
            }

        #endregion

        #region Metodos

            public void Alta_Parte() {
                Cls_Cat_Tal_Partes_Vehiculos_Datos.Alta_Parte(this);
            }

            public void Modificar_Parte() {
                Cls_Cat_Tal_Partes_Vehiculos_Datos.Modificar_Parte(this);
            }

            public DataTable Consultar_Partes() {
                return Cls_Cat_Tal_Partes_Vehiculos_Datos.Consultar_Partes(this);
            }

            public Cls_Cat_Tal_Partes_Vehiculos_Negocio Consultar_Detalles_Parte(){
                return Cls_Cat_Tal_Partes_Vehiculos_Datos.Consultar_Detalles_Parte(this);
            }

            public void Eliminar_Parte() {
                Cls_Cat_Tal_Partes_Vehiculos_Datos.Eliminar_Parte(this);
            }

        #endregion

    }
}