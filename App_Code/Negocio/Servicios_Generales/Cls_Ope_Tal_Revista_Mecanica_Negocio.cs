﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Operacion_Revista_Mecanica.Datos;

/// <summary>
/// Summary description for Cls_Ope_Tal_Revista_Mecanica_Negocio
/// </summary>

namespace JAPAMI.Taller_Mecanico.Operacion_Revista_Mecanica.Negocio {
    public class Cls_Ope_Tal_Revista_Mecanica_Negocio {

        #region "Variables Internas"

            private Int32 No_Registro = (-1);
            private Int32 No_Entrada = (-1);
            private Int32 No_Solicitud = (-1);
            private String Mecanico_ID = String.Empty;
            private String Diagnostico = String.Empty;
            private String Usuario = String.Empty;
            private String Estatus = String.Empty;
            private String Estatus_Solicitud = String.Empty;
            private DateTime Fecha_Inicio_Revision = new DateTime();
            private DateTime Fecha_Fin_Revision = new DateTime();
            private DateTime Fecha_Entrega_Probable = new DateTime();
            private String Usuario_ID = String.Empty;
            private String Comentarios = String.Empty;
            private String Vehiculo_ID = String.Empty;
            private DataTable Dt_Detalles_Revista = new DataTable();
            private SqlTransaction Trans;
            private SqlCommand Cmmd;

        #endregion

        #region "Variables Públicas"

            public SqlTransaction P_Trans
            {
                get { return Trans; }
                set { Trans = value; }
            }
            public SqlCommand P_Cmmd
            {
                get { return Cmmd; }
                set { Cmmd = value; }
            }
            public Int32 P_No_Registro
            {
                get { return No_Registro; }
                set { No_Registro = value; }
            }
            public Int32 P_No_Entrada
            {
                get { return No_Entrada; }
                set { No_Entrada = value; }
            }
            public Int32 P_No_Solicitud
            {
                get { return No_Solicitud; }
                set { No_Solicitud = value; }
            }
            public String P_Mecanico_ID
            {
                get { return Mecanico_ID; }
                set { Mecanico_ID = value; }
            }
            public String P_Diagnostico
            {
                get { return Diagnostico; }
                set { Diagnostico = value; }
            }
            public String P_Usuario
            {
                get { return Usuario; }
                set { Usuario = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Estatus_Solicitud
            {
                get { return Estatus_Solicitud; }
                set { Estatus_Solicitud = value; }
            }
            public DateTime P_Fecha_Inicio_Revision
            {
                get { return Fecha_Inicio_Revision; }
                set { Fecha_Inicio_Revision = value; }
            }
            public DateTime P_Fecha_Fin_Revision
            {
                get { return Fecha_Fin_Revision; }
                set { Fecha_Fin_Revision = value; }
            }
            public DateTime P_Fecha_Entrega_Probable
            {
                get { return Fecha_Entrega_Probable; }
                set { Fecha_Entrega_Probable = value; }
            }
            public String P_Usuario_ID
            {
                get { return Usuario_ID; }
                set { Usuario_ID = value; }
            }
            public String P_Comentarios
            {
                get { return Comentarios; }
                set { Comentarios = value; }
            }
            public String P_Vehiculo_ID
            {
                get { return Vehiculo_ID; }
                set { Vehiculo_ID = value; }
            }
            public DataTable P_Dt_Detalles_Revista
            {
                get { return Dt_Detalles_Revista; }
                set { Dt_Detalles_Revista = value; }
            }

        #endregion

        #region "Metodos"

            public void Alta_Revista_Mecanica() {
                Cls_Ope_Tal_Revista_Mecanica_Datos.Alta_Revista_Mecanica(this);
            }

            public void Alta_Revista_Mecanica(ref SqlCommand Cmd) {
                Cls_Ope_Tal_Revista_Mecanica_Datos.Alta_Revista_Mecanica(this, ref Cmd);
            }

            public void Modifica_Revista_Mecanica() {
                Cls_Ope_Tal_Revista_Mecanica_Datos.Modifica_Revista_Mecanica(this);
            }

            public void Asignar_Diagnostico_Revista_Mecanica() {
                Cls_Ope_Tal_Revista_Mecanica_Datos.Asignar_Diagnostico_Revista_Mecanica(this);
            }

            public Cls_Ope_Tal_Revista_Mecanica_Negocio Consultar_Detalles_Revista_Mecanica() {
                return Cls_Ope_Tal_Revista_Mecanica_Datos.Consultar_Detalles_Revista_Mecanica(this);
            }

            public DataTable Consultar_Revistas_Mecanicas()
            {
                return Cls_Ope_Tal_Revista_Mecanica_Datos.Consultar_Revistas_Mecanicas(this);
            }


        #endregion

	}
}