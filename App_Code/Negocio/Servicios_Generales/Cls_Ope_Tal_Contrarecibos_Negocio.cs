﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Almacen.Contrarecibos_Caja.Datos;

/// <summary>
/// Summary description for Cls_Ope_Alm_Contrarecibos_Negocio
/// </summary>
namespace JAPAMI.Almacen.Contrarecibos_Caja.Negocio
{
    public class Cls_Ope_Alm_Contrarecibos_Caja_Negocio
    {
        public Cls_Ope_Alm_Contrarecibos_Caja_Negocio()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region (Variables Locales)
        private Int64 No_Contra_Recibo;
        private String Fecha_Recepcion;
        private String Fecha_Pago;
        private Double Importe_Total;
        private String Usuario;
        private String Estatus;
        private String Ruta;
        private Double Factura_ID;
        private String No_Factura_Proveedor;
        private Double Importe_Factura;
        private String Fecha_Factura;
        private Int64 No_Orden_Compra;
        private String Proveedor_ID;
        private DataTable Dt_Facturas;
        private String Fecha_Inicio;
        private String Fecha_Fin;
        private String Busqueda;
        private String Proveedor;
        private String Usuario_ID;
        private String Fecha_Cancelacion;
        private String Motivo_Cancelacion;
        private DateTime Fecha_Recepcion_dt;
        #endregion

        #region (Variables Publicas)
        public Int64 P_No_Contra_Recibo
        {
            get { return No_Contra_Recibo; }
            set { No_Contra_Recibo = value; }
        }

        public String P_Fecha_Recepcion
        {
            get { return Fecha_Recepcion; }
            set { Fecha_Recepcion = value; }
        }

        public String P_Fecha_Pago
        {
            get { return Fecha_Pago; }
            set { Fecha_Pago = value; }
        }

        public Double P_Importe_Total
        {
            get { return Importe_Total; }
            set { Importe_Total = value; }
        }

        public String P_Usuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }

        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }

        public String P_Ruta
        {
            get { return Ruta; }
            set { Ruta = value; }
        }

        public Double P_Factura_ID
        {
            get { return Factura_ID; }
            set { Factura_ID = value; }
        }

        public String P_No_Factura_Proveedor
        {
            get { return No_Factura_Proveedor; }
            set { No_Factura_Proveedor = value; }
        }

        public Double P_Importe_Factura
        {
            get { return Importe_Factura; }
            set { Importe_Factura = value; }
        }

        public String P_Fecha_Factura
        {
            get { return Fecha_Factura; }
            set { Fecha_Factura = value; }
        }

        public Int64 P_No_Orden_Compra
        {
            get { return No_Orden_Compra; }
            set { No_Orden_Compra = value; }
        }

        public String P_Proveedor_ID
        {
            get { return Proveedor_ID; }
            set { Proveedor_ID = value; }
        }

        public DataTable P_Dt_Facturas
        {
            get { return Dt_Facturas; }
            set { Dt_Facturas = value; }
        }

        public String P_Fecha_Inicio
        {
            get { return Fecha_Inicio; }
            set { Fecha_Inicio = value; }
        }

        public String P_Fecha_Fin
        {
            get { return Fecha_Fin; }
            set { Fecha_Fin = value; }
        }

        public String P_Busqueda
        {
            get { return Busqueda; }
            set { Busqueda = value; }
        }

        public String P_Proveedor
        {
            get { return Proveedor; }
            set { Proveedor = value; }
        }

        public String P_Usuario_ID
        {
            get { return Usuario_ID; }
            set { Usuario_ID = value; }
        }

        public String P_Fecha_Cancelacion
        {
            get { return Fecha_Cancelacion; }
            set { Fecha_Cancelacion = value; }
        }

        public String P_Motivo_Cancelacion
        {
            get { return Motivo_Cancelacion; }
            set { Motivo_Cancelacion = value; }
        }

        public DateTime P_Fecha_Recepcion_dt
        {
            get { return Fecha_Recepcion_dt; }
            set { Fecha_Recepcion_dt = value; }
        }
        #endregion

        #region (Metodos)
        public Int64 Alta_Contrarecibo()
        {
            return Cls_Ope_Alm_Contrarecibos_Caja_Datos.Alta_Contrarecibo(this);
        }

        public void Modificar_Contrarecibo()
        {
            Cls_Ope_Alm_Contrarecibos_Caja_Datos.Modificar_Contrarecibo(this);
        }

        public DataTable Consulta_Contrarecibos()
        {
            return Cls_Ope_Alm_Contrarecibos_Caja_Datos.Consulta_Contrarecibos(this);
        }

        public DataSet Consulta_Datos_Contrarecibo()
        {
            return Cls_Ope_Alm_Contrarecibos_Caja_Datos.Consulta_Datos_Contrarecibo(this);
        }

        public DataTable Consulta_Proveedores()
        {
            return Cls_Ope_Alm_Contrarecibos_Caja_Datos.Consulta_Proveedores(this);
        }

        public Int64 Proximo_No_Contrarecibo()
        {
            return Cls_Ope_Alm_Contrarecibos_Caja_Datos.Proximo_No_Contrarecibo();
        }

        public DataSet Consulta_Reporte_Contrarecibo()
        {
            return Cls_Ope_Alm_Contrarecibos_Caja_Datos.Consulta_Reporte_Contrarecibo(this);
        }

        public DataTable Consulta_Solo_Contrarecibos()
        {
            return Cls_Ope_Alm_Contrarecibos_Caja_Datos.Consulta_Solo_Contrarecibos(this);
        }

        public void Cancelar_Contrarecibo()
        {
            Cls_Ope_Alm_Contrarecibos_Caja_Datos.Cancelar_Contrarecibo(this);
        }

        public DateTime Consulta_Fecha_Pago()
        {
            return Cls_Ope_Alm_Contrarecibos_Caja_Datos.Consulta_Fecha_Pago(this);
        }
        #endregion
    }
}