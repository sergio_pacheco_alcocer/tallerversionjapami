﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Taller_Mecanico.Operacion_Asignacion_Proveedor.Datos;
using System.Data;
using System.Data.SqlClient;

namespace JAPAMI.Taller_Mecanico.Operacion_Asignacion_Proveedor.Negocio
{
    public class Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio
    {
        #region "Variables Internas"

        private Int32 No_Servicio = (-1);
        private Int32 No_Entrada = (-1);
        private Int32 No_Solicitud = (-1);
        private Int32 No_Asignacion_Proveedor = (-1);
        private Int32 No_Seguimiento_Proveedor = (-1);
        private String Dependencia_ID = String.Empty;
        private String No_Economico = String.Empty;
        private String Mecanico_ID = String.Empty;
        private String Diagnostico = String.Empty;
        private String Usuario = String.Empty;
        private String Estatus = String.Empty;
        private String Estatus_Servicio = String.Empty;
        private String Cargo = String.Empty;
        private String Abono = String.Empty;        
        private DateTime Fecha_Inicio_Revision = new DateTime();
        private DataTable Dt_Manejo_Psp = new DataTable();
        private DateTime Fecha_Fin_Revision = new DateTime();
        private String Reparacion = String.Empty;
        private String Proveedor_ID = String.Empty;
        private String Nombre_Proveedor = String.Empty;
        private String Observaciones = String.Empty;
        private String Tipo = String.Empty;
        private String Tipo_Bien = String.Empty;
        private String No_Inventario = String.Empty;
        private Double Costo = (-1);
        private Double Costo_Extra = (-1);
        private String Campos_Dinamicos = String.Empty;
        private String Campos_Dinamicos_Proveedor = String.Empty;
        private String Filtros_Dinamicos = String.Empty;
        private SqlTransaction Trans;
        private SqlCommand Cmmd;

        #endregion

        #region "Variables Públicas"

        public Int32 P_No_Servicio
        {
            get { return No_Servicio; }
            set { No_Servicio = value; }
        }
        public String P_Dependencia_ID {
            get { return Dependencia_ID; }
            set { Dependencia_ID = value; }
        }
        public Int32 P_No_Entrada
        {
            get { return No_Entrada; }
            set { No_Entrada = value; }
        }
        public Int32 P_No_Solicitud
        {
            get { return No_Solicitud; }
            set { No_Solicitud = value; }
        }
        public Int32 P_No_Seguimiento_Proveedor
        {
            get { return No_Seguimiento_Proveedor; }
            set { No_Seguimiento_Proveedor = value; }
        }
        public SqlTransaction P_Trans
        {
            get { return Trans; }
            set { Trans = value; }
        }
        public SqlCommand P_Cmmd
        {
            get { return Cmmd; }
            set { Cmmd = value; }
        }
        public Double P_Costo
        {
            get { return Costo; }
            set { Costo = value; }
        }
        public Double P_Costo_Extra
        {
            get { return Costo_Extra; }
            set { Costo_Extra = value; }
        }
        public String P_Cargo
        {
            get { return Cargo; }
            set { Cargo = value; }
        }
        public String P_Abono
        {
            get { return Abono; }
            set { Abono = value; }
        }
        public String P_Mecanico_ID
        {
            get { return Mecanico_ID; }
            set { Mecanico_ID = value; }
        }
        public String P_Diagnostico
        {
            get { return Diagnostico; }
            set { Diagnostico = value; }
        }
        public String P_Usuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_Estatus_Servicio
        {
            get { return Estatus_Servicio; }
            set { Estatus_Servicio = value; }
        }
        public DateTime P_Fecha_Inicio_Revision
        {
            get { return Fecha_Inicio_Revision; }
            set { Fecha_Inicio_Revision = value; }
        }
        public DateTime P_Fecha_Fin_Revision
        {
            get { return Fecha_Fin_Revision; }
            set { Fecha_Fin_Revision = value; }
        }
        public String P_Reparacion
        {
            get { return Reparacion; }
            set { Reparacion = value; }
        }
        public Int32 P_No_Asignacion_Proveedor
        {
            get { return No_Asignacion_Proveedor; }
            set { No_Asignacion_Proveedor = value; }
        }
        public String P_Proveedor_ID
        {
            get { return Proveedor_ID; }
            set { Proveedor_ID = value; }
        }
        public String P_Observaciones
        {
            get { return Observaciones; }
            set { Observaciones = value; }
        }
        public String P_Tipo
        {
            get { return Tipo; }
            set { Tipo = value; }
        }
        public String P_Tipo_Bien
        {
            get { return Tipo_Bien; }
            set { Tipo_Bien = value; }
        }
        public String P_Filtros_Dinamicos
        {
            get { return Filtros_Dinamicos; }
            set { Filtros_Dinamicos = value; }
        }
        public String P_Campos_Dinamicos
        {
            get { return Campos_Dinamicos; }
            set { Campos_Dinamicos = value; }
        }
        public String P_Campos_Dinamicos_Proveedor
        {
            get { return Campos_Dinamicos_Proveedor; }
            set { Campos_Dinamicos_Proveedor = value; }
        }
        public String P_Nombre_Proveedor
        {
            get { return Nombre_Proveedor; }
            set { Nombre_Proveedor = value; }
        }
        public String P_No_Inventario
        {
            get { return No_Inventario; }
            set { No_Inventario = value; }
        }
        public String P_No_Economico
        {
            get { return No_Economico; }
            set { No_Economico = value; }
        }
        public DataTable P_Dt_Manejo_Psp
        {
            get { return Dt_Manejo_Psp; }
            set { Dt_Manejo_Psp = value; }
        }
        #endregion

        #region [Metodos]
        public void Alta_Proveedor_Servicio()
        {
            Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos.Alta_Proveedor_Servicio(this);
        }
        public DataTable Consulta_Proveedor_Servicio()
        {
            return Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos.Consulta_Proveedor_Servicio(this);
        }
        public void Modificar_Proveedor_Servicio()
        {
            Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos.Modificar_Proveedor_Servicio(this);
        }
        public DataTable Consultar_Asignaciones_Servicios() {
            return Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos.Consultar_Asignaciones_Servicios(this);
        }
        public Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Consultar_Detalles_Asignacion_Servicio() {
            return Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos.Consultar_Detalles_Asignacion_Servicio(this);
        }
        public void Alta_Seguimiento_Proveedor()
        {
            Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos.Alta_Seguimiento_Proveedor(this);
        }
        public DataTable Consulta_Seguimiento_Proveedor()
        {
            return Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos.Consulta_Seguimiento_Proveedor(this);
        }
        public bool Consultamos_Presupuesto_Existente()
        {
            return Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos.Consultamos_Presupuesto_Existente(this);
        }
        public void Alta_Seguimiento_Mecanico()
        {
            Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos.Alta_Seguimiento_Mecanico(this);
        }
        public DataTable Consulta_Seguimiento_Mecanico()
        {
            return Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos.Consulta_Seguimiento_Mecanico(this);
        }
        public void Cancelar_Reserva_Servicio()
        {
            Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos.Cancelar_Reserva_Servicio(this);
        }
        public int Crear_Reserva_Servicio()
        {
            return Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos.Crear_Reserva_Servicio(this);
        }
        public void Modificar_Reserva_Servicio()
        {
            Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos.Modificar_Reserva_Servicio(this);
        }
        public void Extender_Reserva_Servicio()
        {
            Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos.Extender_Reserva_Servicio(this);
        }
        #endregion


        
    }
}