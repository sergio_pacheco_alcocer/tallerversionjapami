﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Taller_Mecanico.Catalogo_Tipos_Refacciones.Datos;
using System.Data;

namespace JAPAMI.Taller_Mecanico.Catalogo_Tipos_Refacciones.Negocio
{
    public class Cls_Cat_Tal_Tipos_Refacciones_Negocio
    {
        public Cls_Cat_Tal_Tipos_Refacciones_Negocio()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region Variables Internas

        private String Tipo_Refaccion_ID = String.Empty;
        private String Nombre = String.Empty;
        private String Estatus = String.Empty;
        private String Clave = String.Empty;
        private String Descripcion = String.Empty;
        private String Usuario = String.Empty;

        #endregion

        #region Variables Publicas

        public String P_Tipo_Refaccion_ID
        {
            get { return Tipo_Refaccion_ID; }
            set { Tipo_Refaccion_ID = value; }
        }
        public String P_Nombre
        {
            get { return Nombre; }
            set { Nombre = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_Clave
        {
            get { return Clave; }
            set { Clave = value; }
        }
        public String P_Descripcion
        {
            get { return Descripcion; }
            set { Descripcion = value; }
        }
        public String P_Usuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }

        #endregion

        #region Metodos

        public void Alta_Nivel()
        {
            Cls_Cat_Tal_Tipos_Refacciones_Datos.Alta_Tipo_Refaccion(this);
        }

        public void Modificar_Nivel_Mecanico()
        {
            Cls_Cat_Tal_Tipos_Refacciones_Datos.Modificar_Tipos_Refacciones(this);
        }

        public DataTable Consultar_Nivel()
        {
            return Cls_Cat_Tal_Tipos_Refacciones_Datos.Consultar_Tipos_Refacciones(this);
        }

        public Cls_Cat_Tal_Tipos_Refacciones_Negocio Consultar_Detalles_Nivel()
        {
            return Cls_Cat_Tal_Tipos_Refacciones_Datos.Consultar_Detalles_Tipos(this);
        }
       
        #endregion
    }
}