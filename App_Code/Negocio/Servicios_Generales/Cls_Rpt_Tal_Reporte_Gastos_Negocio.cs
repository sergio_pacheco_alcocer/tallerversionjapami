﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Reporte_Gastos.Datos;

namespace JAPAMI.Taller_Mecanico.Reporte_Gastos.Negocio {
    public class Cls_Rpt_Tal_Reporte_Gastos_Negocio {
        
        #region Variables Internas

            private String Vehiculo_ID = String.Empty;
            private String Dependencia_ID = String.Empty;
            private String Tipo_Servicio = String.Empty;
            private String Filtro = String.Empty;
            private Int32 Numero_Inventario = -1;
            private String Tipo_Trabajo = String.Empty;
            private String Tipo_Reparacion = String.Empty;
            private DateTime F_Recep_Ini = new DateTime();
            private DateTime F_Recep_Fin = new DateTime();
            private String Proveedor_ID = String.Empty;
            private String Nombre_Mecanico = String.Empty;

        #endregion

        #region Variables Publicas

            public String P_Vehiculo_ID
            {
                get { return Vehiculo_ID; }
                set { Vehiculo_ID = value; }
            }
            public Int32 P_Numero_Inventario
            {
                get { return Numero_Inventario; }
                set { Numero_Inventario = value; }
            }
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }
            public String P_Tipo_Servicio
            {
                get { return Tipo_Servicio; }
                set { Tipo_Servicio = value; }
            }
            public String P_Tipo_Trabajo
            {
                get { return Tipo_Trabajo; }
                set { Tipo_Trabajo = value; }
            }
            public String P_Tipo_Reparacion
            {
                get { return Tipo_Reparacion; }
                set { Tipo_Reparacion = value; }
            }
            public String P_Filtro
            {
                get { return Filtro; }
                set { Filtro = value; }
            }
            public String P_Nombre_Mecanico
            {
                get { return Nombre_Mecanico; }
                set { Nombre_Mecanico = value; }
            }
            public DateTime P_F_Recep_Ini
            {
                get { return F_Recep_Ini; }
                set { F_Recep_Ini = value; }
            }
            public DateTime P_F_Recep_Fin
            {
                get { return F_Recep_Fin; }
                set { F_Recep_Fin = value; }
            }
            public String P_Proveedor_ID
            {
                get { return Proveedor_ID; }
                set { Proveedor_ID = value; }
            }

        #endregion

        #region Metodos

            public DataTable Consulta_Servicios() {
                DataTable Dt_Resultados = new DataTable();
                if(P_Tipo_Servicio.Trim().Equals("TODOS")){
                    Dt_Resultados = Consulta_Servicios_Preventivos();
                    Dt_Resultados.Merge(Consulta_Servicios_Correctivos());
                    if (String.IsNullOrEmpty(P_Proveedor_ID))
                    {
                        Dt_Resultados.Merge(Consulta_Servicios_Revista_Mecanica());
                    }
                } else if(P_Tipo_Servicio.Trim().Equals("SERVICIO_PREVENTIVO")){
                    Dt_Resultados = Consulta_Servicios_Preventivos();
                } else if (P_Tipo_Servicio.Trim().Equals("SERVICIO_CORRECTIVO")) {
                    Dt_Resultados = Consulta_Servicios_Correctivos();
                } else if(P_Tipo_Servicio.Trim().Equals("REVISTA_MECANICA") && String.IsNullOrEmpty(P_Proveedor_ID)){
                    Dt_Resultados = Consulta_Servicios_Revista_Mecanica();
                }
                return Dt_Resultados;
            }

            public DataTable Consulta_Servicios_Preventivos() {
                return Cls_Rpt_Tal_Reporte_Gastos_Datos.Consulta_Servicios_Preventivos(this);
            }
            public DataTable Consulta_Gastos()
            {
                return Cls_Rpt_Tal_Reporte_Gastos_Datos.Consulta_Gastos(this);
            }
            public DataTable Consulta_Reporte_Refacciones()
            {
                return Cls_Rpt_Tal_Reporte_Gastos_Datos.Consulta_Reporte_Refacciones(this);
            }
            public DataTable Consulta_Servicios_Correctivos() {
                return Cls_Rpt_Tal_Reporte_Gastos_Datos.Consulta_Servicios_Correctivos(this);
            }

            public DataTable Consulta_Servicios_Revista_Mecanica() {
                return Cls_Rpt_Tal_Reporte_Gastos_Datos.Consulta_Servicios_Revista_Mecanica(this);
            }

        #endregion

	}
}