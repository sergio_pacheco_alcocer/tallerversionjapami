﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Taller_Mecanico.Catalogo_Tipos_Trabajo.Datos;
using System.Data;

namespace JAPAMI.Taller_Mecanico.Catalogo_Tipos_Trabajo.Negocio
{
    public class Cls_Cat_Tal_Tipos_Trabajo_Negocio
    {
        public Cls_Cat_Tal_Tipos_Trabajo_Negocio()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region Variables Internas

        private String Tipo_Trabajo_ID = String.Empty;
        private String Nombre = String.Empty;
        private String Estatus = String.Empty;
        private String Usuario = String.Empty;
        private String Clave = String.Empty;
        private String Descripcion = String.Empty;
        private String Filtros_Dinamicos = String.Empty;

        #endregion

        #region Variables Publicas

        public String P_Tipo_Trabajo_ID
        {
            get { return Tipo_Trabajo_ID; }
            set { Tipo_Trabajo_ID = value; }
        }
        public String P_Filtros_Dinamicos
        {
            get { return Filtros_Dinamicos; }
            set { Filtros_Dinamicos = value; }
        }
        public String P_Nombre
        {
            get { return Nombre; }
            set { Nombre = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_Clave
        {
            get { return Clave; }
            set { Clave = value; }
        }
        public String P_Descripcion
        {
            get { return Descripcion; }
            set { Descripcion = value; }
        }
        public String P_Usuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }

        #endregion

        #region Metodos

        public void Alta_Tipos_Trabajo()
        {
            Cls_Cat_Tal_Tipos_Trabajo_Datos.Alta_Tipo_Trabajo(this);
        }

        public void Modificar_Tipos_Trabajo()
        {
            Cls_Cat_Tal_Tipos_Trabajo_Datos.Modificar_Tipos_Trabajo(this);
        }

        public DataTable Consultar_Tipos_Trabajo()
        {
            return Cls_Cat_Tal_Tipos_Trabajo_Datos.Consultar_Tipos_Trabajo(this);
        }

        public Cls_Cat_Tal_Tipos_Trabajo_Negocio Consultar_Detalles_Tipos()
        {
            return Cls_Cat_Tal_Tipos_Trabajo_Datos.Consultar_Detalles_Tipos(this);
        }
       
        #endregion
    }
}