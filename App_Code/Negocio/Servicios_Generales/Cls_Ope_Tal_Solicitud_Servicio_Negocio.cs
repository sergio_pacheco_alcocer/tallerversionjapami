﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Datos;

/// <summary>
/// Summary description for Cls_Ope_Tal_Generacion_Solicitud_Servicio_Negocio
/// </summary>

namespace JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio {
    public class Cls_Ope_Tal_Solicitud_Servicio_Negocio {

        #region "Variables Internas"

            private Int32 No_Solicitud = (-1);
            private String Folio_Solicitud = String.Empty;
            private DateTime Fecha_Elaboracion = new DateTime();
            private String Tipo_Bien = "TODOS";
            private String Tipo_Servicio = String.Empty;
            private String Reparacion = String.Empty;
            private String Dependencia_ID = String.Empty;
            private String Programa_Proyecto_ID = String.Empty;
            private String Bien_ID = String.Empty;
            private String Descripcion_Servicio = String.Empty;
            private String Estatus = String.Empty;
            private String Empleado_Solicito_ID = String.Empty;
            private String Usuario = String.Empty;
            private DateTime Busq_Fecha_Elaboracion_Inicial = new DateTime();
            private DateTime Busq_Fecha_Elaboracion_Final = new DateTime();
            private String Empleado_Autorizo_ID = String.Empty;
            private String Motivo_Rechazo = String.Empty;
            private DateTime Fecha_Recepcion_Programada = new DateTime();
            private DateTime Fecha_Llantas = new DateTime();
            private DateTime Fecha_Bateria = new DateTime();
            private DateTime Fecha_Recepcion_Real = new DateTime();
            private Double Kilometraje = -1.0;
            private String Procedencia = String.Empty;
            private String Correo_Electronico = String.Empty;
            private Int32 No_Inventario = (-1);
            private String No_Economico = String.Empty;
            private DataTable Dt_Seguimiento = new DataTable();
            private DateTime Busq_Fecha_Recepcion_Inicial = new DateTime();
            private DateTime Busq_Fecha_Recepcion_Final = new DateTime();
            private Int32 No_Contrarecibo = (-1);
            private Int32 No_Reserva = (-1);
            private DateTime Fecha_Contrarecibo = new DateTime();
            private SqlTransaction Trans = null;
            private SqlCommand Cmmd = null;
            private String Cotizador_ID = null;
            private String Ordenar_Listado = String.Empty;

        #endregion

        #region "Variables Públicas"

            public Int32 P_No_Solicitud {
                set { No_Solicitud = value; }
                get { return No_Solicitud; }
            }
            public String P_Folio_Solicitud
            {
                set { Folio_Solicitud = value; }
                get { return Folio_Solicitud; }
            }
            public DateTime P_Fecha_Elaboracion
            {
                set { Fecha_Elaboracion = value; }
                get { return Fecha_Elaboracion; }
            }
            public DateTime P_Fecha_Llantas
            {
                set { Fecha_Llantas = value; }
                get { return Fecha_Llantas; }
            }
            public DateTime P_Fecha_Bateria
            {
                set { Fecha_Bateria = value; }
                get { return Fecha_Bateria; }
            }
            public String P_Tipo_Bien
            {
                set { Tipo_Bien = value; }
                get { return Tipo_Bien; }
            }
            public String P_Tipo_Servicio
            {
                set { Tipo_Servicio = value; }
                get { return Tipo_Servicio; }
            }
            public String P_Reparacion
            {
                set { Reparacion = value; }
                get { return Reparacion; }
            }
            public String P_Dependencia_ID
            {
                set { Dependencia_ID = value; }
                get { return Dependencia_ID; }
            }
            public String P_Programa_Proyecto_ID
            {
                set { Programa_Proyecto_ID = value; }
                get { return Programa_Proyecto_ID; }
            }
            public String P_Bien_ID
            {
                set { Bien_ID = value; }
                get { return Bien_ID; }
            }
            public String P_Descripcion_Servicio
            {
                set { Descripcion_Servicio = value; }
                get { return Descripcion_Servicio; }
            }
            public String P_Estatus
            {
                set { Estatus = value; }
                get { return Estatus; }
            }
            public String P_Empleado_Solicito_ID
            {
                set { Empleado_Solicito_ID = value; }
                get { return Empleado_Solicito_ID; }
            }
            public String P_Usuario
            {
                set { Usuario = value; }
                get { return Usuario; }
            }
            public DateTime P_Busq_Fecha_Elaboracion_Inicial
            {
                set { Busq_Fecha_Elaboracion_Inicial = value; }
                get { return Busq_Fecha_Elaboracion_Inicial; }
            }
            public DateTime P_Busq_Fecha_Elaboracion_Final
            {
                set { Busq_Fecha_Elaboracion_Final = value; }
                get { return Busq_Fecha_Elaboracion_Final; }
            }
            public String P_Empleado_Autorizo_ID
            {
                set { Empleado_Autorizo_ID = value; }
                get { return Empleado_Autorizo_ID; }
            }
            public String P_Motivo_Rechazo
            {
                set { Motivo_Rechazo = value; }
                get { return Motivo_Rechazo; }
            }
            public DateTime P_Fecha_Recepcion_Programada
            {
                set { Fecha_Recepcion_Programada = value; }
                get { return Fecha_Recepcion_Programada; }
            }
            public DateTime P_Fecha_Recepcion_Real
            {
                set { Fecha_Recepcion_Real = value; }
                get { return Fecha_Recepcion_Real; }
            }
            public Double P_Kilometraje {
                get { return Kilometraje; }
                set { Kilometraje = value; }
            }
            public String P_Procedencia
            {
                set { Procedencia = value; }
                get { return Procedencia; }
            }
            public String P_Correo_Electronico
            {
                set { Correo_Electronico = value; }
                get { return Correo_Electronico; }
            }
            public Int32 P_No_Inventario
            {
                set { No_Inventario = value; }
                get { return No_Inventario; }
            }
            public String P_No_Economico
            {
                set { No_Economico = value; }
                get { return No_Economico; }
            }
            public DataTable P_Dt_Seguimiento
            {
                set { Dt_Seguimiento = value; }
                get { return Dt_Seguimiento; }
            }
            public DateTime P_Busq_Fecha_Recepcion_Inicial
            {
                set { Busq_Fecha_Recepcion_Inicial = value; }
                get { return Busq_Fecha_Recepcion_Inicial; }
            }
            public DateTime P_Busq_Fecha_Recepcion_Final
            {
                set { Busq_Fecha_Recepcion_Final = value; }
                get { return Busq_Fecha_Recepcion_Final; }
            }
            public Int32 P_No_Contrarecibo
            {
                set { No_Contrarecibo = value; }
                get { return No_Contrarecibo; }
            }
            public Int32 P_No_Reserva
            {
                set { No_Reserva = value; }
                get { return No_Reserva; }
            }
            public DateTime P_Fecha_Contrarecibo
            {
                set { Fecha_Contrarecibo = value; }
                get { return Fecha_Contrarecibo; }
            }
            public SqlTransaction P_Trans
            {
                get { return Trans; }
                set { Trans = value; }
            }
            public SqlCommand P_Cmmd
            {
                get { return Cmmd; }
                set { Cmmd = value; }
            }
            public String P_Cotizador_ID {
                get { return Cotizador_ID; }
                set { Cotizador_ID = value; }
            }
            public String P_Ordenar_Listado
            {
                get { return Ordenar_Listado; }
                set { Ordenar_Listado = value; }
            }
        #endregion

        #region "Metodos"

            public void Alta_Solicitud_Servicio() {
                Cls_Ope_Tal_Solicitud_Servicio_Datos.Alta_Solicitud_Servicio(this);
            }
            public void Modifica_Solicitud_Servicio() {
                Cls_Ope_Tal_Solicitud_Servicio_Datos.Modifica_Solicitud_Servicio(this);   
            }
            public Cls_Ope_Tal_Solicitud_Servicio_Negocio Consultar_Detalles_Solicitud_Servicio() {
                return Cls_Ope_Tal_Solicitud_Servicio_Datos.Consultar_Detalles_Solicitud_Servicio(this);
            }
            public DataTable Consultar_Listado_Solicitudes_Servicio() {
                return Cls_Ope_Tal_Solicitud_Servicio_Datos.Consultar_Listado_Solicitudes_Servicio(this);
            }
            public void Autorizacion_Solicitud_Servicio() {
                Cls_Ope_Tal_Solicitud_Servicio_Datos.Autorizacion_Solicitud_Servicio(this);
            }
            public Int32 Autorizacion_Solicitud_Servicio_Interno() {
                return Cls_Ope_Tal_Solicitud_Servicio_Datos.Autorizacion_Solicitud_Servicio_Interno(this);
            }
            public void Autorizacion_Interna_Solicitud_Servicio() {
                Cls_Ope_Tal_Solicitud_Servicio_Datos.Autorizacion_Interna_Solicitud_Servicio(this);
            }
            public void Asignar_Cotizador_Solicitud() {
                Cls_Ope_Tal_Solicitud_Servicio_Datos.Asignar_Cotizador_Solicitud(this);
            }
        #endregion

    }
}