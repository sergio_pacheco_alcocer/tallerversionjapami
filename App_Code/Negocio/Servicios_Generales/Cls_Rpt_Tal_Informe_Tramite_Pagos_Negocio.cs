﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Reporte_Informe_Tramite_Pagos.Datos;

/// <summary>
/// Summary description for Cls_Rpt_Tal_Informe_Tramite_Pagos_Negocio
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Reporte_Informe_Tramite_Pagos.Negocio{
    public class Cls_Rpt_Tal_Informe_Tramite_Pagos_Negocio {

        #region "Variables Internas"

            private DateTime Fecha_Inicial = new DateTime();
            private DateTime Fecha_Final = new DateTime();

        #endregion

        #region "Variables Publicas"

            public DateTime P_Fecha_Inicial {
                get { return Fecha_Inicial; }
                set { Fecha_Inicial = value; }
            }
            public DateTime P_Fecha_Final
            {
                get { return Fecha_Final; }
                set { Fecha_Final = value; }

            }

        #endregion

        #region "Metodos"

            public DataTable Consultar_Informe_Tramite_Pagos() {
                DataTable Dt_Datos_Unificados = new DataTable();
                Dt_Datos_Unificados = Cls_Rpt_Tal_Informe_Tramite_Pagos_Datos.Consultar_Informe_Tramite_Pagos_Serv_Preventivos(this).Copy();
                Dt_Datos_Unificados.Merge(Cls_Rpt_Tal_Informe_Tramite_Pagos_Datos.Consultar_Informe_Tramite_Pagos_Serv_Correctivos(this).Copy());
                return Dt_Datos_Unificados;
            }

        #endregion

    }
}