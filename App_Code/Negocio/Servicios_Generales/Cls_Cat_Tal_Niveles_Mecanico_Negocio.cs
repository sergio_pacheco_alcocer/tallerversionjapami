﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Taller_Mecanico.Catalogo_Niveles_Mecanico.Datos;
using System.Data;

namespace JAPAMI.Taller_Mecanico.Catalogo_Niveles_Mecanico.Negocio
{
    public class Cls_Cat_Tal_Niveles_Mecanico_Negocio
    {
        public Cls_Cat_Tal_Niveles_Mecanico_Negocio()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region Variables Internas

        private String Nivel_ID = String.Empty;
        private String Nombre = String.Empty;
        private String Estatus = String.Empty;
        private String Clave = String.Empty;
        private String Descripcion = String.Empty;
        private String Usuario = String.Empty;

        #endregion

        #region Variables Publicas

        public String P_Nivel_ID
        {
            get { return Nivel_ID; }
            set { Nivel_ID = value; }
        }
        public String P_Nombre
        {
            get { return Nombre; }
            set { Nombre = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_Clave
        {
            get { return Clave; }
            set { Clave = value; }
        }
        public String P_Descripcion
        {
            get { return Descripcion; }
            set { Descripcion = value; }
        }
        public String P_Usuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }

        #endregion

        #region Metodos

        public void Alta_Nivel()
        {
            Cls_Cat_Tal_Niveles_Mecanico_Datos.Alta_Nivel(this);
        }

        public void Modificar_Nivel_Mecanico()
        {
            Cls_Cat_Tal_Niveles_Mecanico_Datos.Modificar_Nivel_Mecanico(this);
        }

        public DataTable Consultar_Nivel()
        {
            return Cls_Cat_Tal_Niveles_Mecanico_Datos.Consultar_Nivel_Mecanicos(this);
        }

        public Cls_Cat_Tal_Niveles_Mecanico_Negocio Consultar_Detalles_Nivel()
        {
            return Cls_Cat_Tal_Niveles_Mecanico_Datos.Consultar_Detalles_Mecanico(this);
        }

        public void Eliminar_Nivel_Mecanico() {
            Cls_Cat_Tal_Niveles_Mecanico_Datos.Eliminar_Nivel_Mecanico(this);
        }

        #endregion
    }
}