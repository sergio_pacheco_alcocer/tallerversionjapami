﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Unidades_Responsables_Rol.Datos;
using JAPAMI.Sessiones;
/// <summary>
/// Summary description for Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Operacion_Unidades_Responsables_Rol.Negocio {
    public class Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio {
        
        #region Variables Internas

            private DataTable Dt_Unidades_Responsables = null;
            private String Usuario = String.Empty;
            private String Estatus = String.Empty;
            private String No_Empleado = String.Empty;

        #endregion

        #region Variables Publicas

            public DataTable P_Dt_Unidades_Responsables {
                get { return Dt_Unidades_Responsables; }
                set { Dt_Unidades_Responsables = value; }
            }
            public String P_Usuario {
                get { return Usuario; }
                set { Usuario = value; }
            }
            public String P_No_Empleado
            {
                get { return No_Empleado; }
                set { No_Empleado = value; }
            }
            public String P_Estatus {
                get { return Estatus; }
                set { Estatus = value; }
            }

        #endregion

        #region Metodos

            public void Actualizar_Listado_UR() {
                Cls_Ope_Tal_Unidades_Responsables_Rol_Datos.Actualizar_Listado_UR(this);
            }
            public DataTable Consultar_Listado_UR(){
                return Cls_Ope_Tal_Unidades_Responsables_Rol_Datos.Consultar_Listado_UR(this);
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Acceso_Unidades_Responsables
            ///DESCRIPCIÓN: Se valida el Acceso a Otras UR.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 30/Mayo/2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///******************************************************************************* 
            public Boolean Validar_Acceso_Unidades_Responsables()
            {
                Boolean Accesos_Totales = false;
                this.P_Estatus = "ACTIVO";
                DataTable Dt_UR = this.Consultar_Listado_UR();
                DataRow[] Filas = Dt_UR.Select("DEPENDENCIA_ID = '" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "'");
                if (Filas.Length > 0) { Accesos_Totales = true; }
                return Accesos_Totales;
            }
        #endregion

	}
}
