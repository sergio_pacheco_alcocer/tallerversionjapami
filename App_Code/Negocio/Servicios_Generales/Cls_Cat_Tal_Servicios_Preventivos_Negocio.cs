﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Catalogo_Taller_Servicios_Preventivos.Datos;

namespace JAPAMI.Catalogo_Taller_Servicios_Preventivos.Negocio
{
    public class Cls_Cat_Tal_Servicios_Preventivos_Negocio
    {	    
		    #region Variables Internas

            private String Servicios_Prev_ID;
            private String Servicios_Det_Prev_ID;
            private Int32 Cantidad;
            private String No_Servicio;
            private Int32 No_Entrada = (-1);
            private String Mecanico_ID = String.Empty;
            private String Usuario = String.Empty;
            private String No_Listado;
            private String Refaccion_ID;
            private String Clave;
            private String Estatus;
            private String Nombre;
            private String Descripcion;
            private String Tipo_Servicio;
            private String Filtros_Dinamicos;
            private DataTable Dt_Refacciones_Servicios;
                    
            #endregion 

            #region Variables Publicas

            public String P_Servicios_Det_Prev_ID
            {
                get { return Servicios_Det_Prev_ID; }
                set { Servicios_Det_Prev_ID = value; }
            }
            public String P_No_Listado
            {
                get { return No_Listado; }
                set { No_Listado = value; }
            }
            public String P_No_Servicio
            {
                get { return No_Servicio; }
                set { No_Servicio = value; }
            }
            public Int32 P_No_Entrada
            {
                get { return No_Entrada; }
                set { No_Entrada = value; }
            }
            public String P_Mecanico_ID
            {
                get { return Mecanico_ID; }
                set { Mecanico_ID = value; }
            }
            public String P_Usuario
            {
                get { return Usuario; }
                set { Usuario = value; }
            }
            public String P_Tipo_Servicio
            {
                get { return Tipo_Servicio; }
                set { Tipo_Servicio = value; }
            }
            public Int32 P_Cantidad
            {
                get { return Cantidad; }
                set { Cantidad = value; }
            }

            public String P_Refaccion_ID
            {
                get { return Refaccion_ID; }
                set { Refaccion_ID = value; }
            }

            public DataTable P_Dt_Refacciones_Servicios
            {
                get { return Dt_Refacciones_Servicios; }
                set { Dt_Refacciones_Servicios = value; }
            }

            public String P_Servicio_ID
            {
                get { return Servicios_Prev_ID; }
                set { Servicios_Prev_ID = value; }
            }

            public String P_Clave
            {
                get { return Clave; }
                set { Clave = value; }
            }

            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }

            public String P_Nombre
            {
                get { return Nombre; }
                set { Nombre = value; }
            }

            public String P_Descripcion
            {
                get { return Descripcion; }
                set { Descripcion = value; }
            }

            public String P_Filtro
            {
                get { return Filtros_Dinamicos; }
                set { Filtros_Dinamicos = value; }
            }

            #endregion

            #region Metodos

            public void Alta_Servicio()
            {
                Cls_Cat_Tal_Servicios_Preventivos_Datos.Alta_Servicios(this);
            }

            public void Alta_Listado()
            {
                Cls_Cat_Tal_Servicios_Preventivos_Datos.Alta_Listado(this);
            }

            public void Modificar_Servicio()
            {
                Cls_Cat_Tal_Servicios_Preventivos_Datos.Modificar_Servicios(this);
            }

            public void Modificar_Listado()
            {
                Cls_Cat_Tal_Servicios_Preventivos_Datos.Modificar_Listado(this);
            }

            public void Eliminar_Servicio()
            {
                Cls_Cat_Tal_Servicios_Preventivos_Datos.Eliminar_Servicios(this);
            }

            public Cls_Cat_Tal_Servicios_Preventivos_Negocio Consultar_Datos()
            {
                return Cls_Cat_Tal_Servicios_Preventivos_Datos.Consultar_Datos(this);
            }

            public DataTable Consultar_Servicio()
            {
                return Cls_Cat_Tal_Servicios_Preventivos_Datos.Consultar_Servicios(this);
            }

            public DataTable Consultar_Refaccion_Servicio()
            {
                return Cls_Cat_Tal_Servicios_Preventivos_Datos.Consultar_Refaccion_Servicio(this);
            }

            public DataTable Consultar_Listado()
            {
                return Cls_Cat_Tal_Servicios_Preventivos_Datos.Consultar_Listado(this);
            }
            public DataTable Consulta_General()
            {
                return Cls_Cat_Tal_Servicios_Preventivos_Datos.Consulta_General(this);
            }
            public void Modifica_Servicio_Correctivo()
            {
                Cls_Cat_Tal_Servicios_Preventivos_Datos.Modifica_Servicio_Correctivo(this);
            }

            public void Modifica_Servicio_Preventivo()
            {
                Cls_Cat_Tal_Servicios_Preventivos_Datos.Modifica_Servicio_Preventivo(this);
            }
            #endregion


            
    }
    }
