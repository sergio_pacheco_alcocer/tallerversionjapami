﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Proveedor_Entrada_Salida.Datos;
/// <summary>
/// Summary description for Cls_Ope_Tal_Proveedor_Entrada_Salida_Negocio
/// </summary>

namespace JAPAMI.Taller_Mecanico.Operacion_Proveedor_Entrada_Salida.Negocio {
    public class Cls_Ope_Tal_Proveedor_Entrada_Salida_Negocio {

        #region "Variables Internas"

            private Int32 No_Registro = (-1);
            private Int32 No_Asignacion = (-1);
            private Int32 No_Servicio = (-1);
            private DateTime Fecha_Salida = new DateTime();
            private Double Kilometraje_Salida = (-1.0);
            private String Comentarios_Salida = String.Empty;
            private DateTime Fecha_Entrada = new DateTime();
            private Double Kilometraje_Entrada = (-1.0);
            private String Comentarios_Entrada = String.Empty;
            private String Estatus = String.Empty;
            private String Tipo = String.Empty;
            private String Tipo_Bien = String.Empty;
            private String Tipo_Servicio = String.Empty;
            private String Usuario = String.Empty;
            private DataTable Dt_Detalles_Entrada = null;
            private DataTable Dt_Detalles_Salida = null;
            private String Estatus_Servicio = String.Empty;

        #endregion

        #region "Variables Públicas"

            public Int32 P_No_Registro {
                set { No_Registro = value; }
                get { return No_Registro; }
            }
            public Int32 P_No_Asignacion {
                set { No_Asignacion = value; }
                get { return No_Asignacion; }
            }
            public Int32 P_No_Servicio
            {
                set { No_Servicio = value; }
                get { return No_Servicio; }
            }
            public DateTime P_Fecha_Salida {
                set { Fecha_Salida = value; }
                get { return Fecha_Salida; }
            }
            public Double P_Kilometraje_Salida {
                set { Kilometraje_Salida = value; }
                get { return Kilometraje_Salida; }
            }
            public String P_Comentarios_Salida {
                set { Comentarios_Salida = value; }
                get { return Comentarios_Salida; }
            }
            public DateTime P_Fecha_Entrada {
                set { Fecha_Entrada = value; }
                get { return Fecha_Entrada; }
            }
            public Double P_Kilometraje_Entrada {
                set { Kilometraje_Entrada = value; }
                get { return Kilometraje_Entrada; }
            }
            public String P_Comentarios_Entrada {
                set { Comentarios_Entrada = value; }
                get { return Comentarios_Entrada; }
            }
            public String P_Estatus
            {
                set { Estatus = value; }
                get { return Estatus; }
            }
            public String P_Tipo
            {
                set { Tipo = value; }
                get { return Tipo; }
            }
            public String P_Tipo_Bien
            {
                set { Tipo_Bien = value; }
                get { return Tipo_Bien; }
            }
            public String P_Tipo_Servicio
            {
                set { Tipo_Servicio = value; }
                get { return Tipo_Servicio; }
            }
            public String P_Usuario
            {
                set { Usuario = value; }
                get { return Usuario; }
            }
            public DataTable P_Dt_Detalles_Entrada
            {
                set { Dt_Detalles_Entrada = value; }
                get { return Dt_Detalles_Entrada; }
            }
            public DataTable P_Dt_Detalles_Salida
            {
                set { Dt_Detalles_Salida = value; }
                get { return Dt_Detalles_Salida; }
            }
            public String P_Estatus_Servicio
            {
                set { Estatus_Servicio = value; }
                get { return Estatus_Servicio; }
            }

        #endregion

        #region "Metodos"

            public void Alta_Registro_Salida() {
                Cls_Ope_Tal_Proveedor_Entrada_Salida_Datos.Alta_Registro_Salida(this);
            }
            public void Alta_Registro_Entrada()
            {
                Cls_Ope_Tal_Proveedor_Entrada_Salida_Datos.Alta_Registro_Entrada(this);
            }
            public Cls_Ope_Tal_Proveedor_Entrada_Salida_Negocio Consultar_Detalles_Proveedor_Entrada_Salida() {
                return Cls_Ope_Tal_Proveedor_Entrada_Salida_Datos.Consultar_Detalles_Proveedor_Entrada_Salida(this);
            }

        #endregion

	}
}