﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Catalogo_Periodos_Revista.Datos;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Cat_Tal_Periodos_Revista_Negocio
/// </summary>
namespace JAPAMI.Taller_Mecanico.Catalogo_Periodos_Revista.Negocio
{
    public class Cls_Cat_Tal_Periodos_Revista_Negocio
    {
        #region Variables Internas

            private String Periodo_ID = String.Empty;
            private String Nombre = String.Empty;
            private String Periodo_Mes = String.Empty;
            private String Estatus = String.Empty;
            private String Usuario_Creo = String.Empty;
            private DateTime Fecha_Creo = new DateTime();
            private String Usuario_Modifico = String.Empty;
            private DateTime Fecha_Modifico = new DateTime();
            private SqlTransaction Trans;
            private SqlCommand Cmmd;


        #endregion

        #region Variables Publicas

            public SqlTransaction P_Trans
            {
                get { return Trans; }
                set { Trans = value; }
            }
            public SqlCommand P_Cmmd
            {
                get { return Cmmd; }
                set { Cmmd = value; }
            }

            public String P_Periodo_ID
            {
                get { return Periodo_ID; }
                set { Periodo_ID = value; }
            }
            public String P_Nombre
            {
                get { return Nombre; }
                set { Nombre = value; }
            }
            public String P_Periodo_Mes
            {
                get { return Periodo_Mes; }
                set { Periodo_Mes = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Usuario_Creo
            {
                get { return Usuario_Creo; }
                set { Usuario_Creo = value; }
            }
            public DateTime P_Fecha_Creo
            {
                get { return Fecha_Creo; }
                set { Fecha_Creo = value; }
            }
            public String P_Usuario_Modifico
            {
                get { return Usuario_Modifico; }
                set { Usuario_Modifico = value; }
            }
            public DateTime P_Fecha_Modifico
            {
                get { return Fecha_Modifico; }
                set { Fecha_Modifico = value; }
            }

        #endregion

        #region Metodos

            public void Alta_Periodos_Revista()
            {
                Cls_Cat_Tal_Periodos_Revista_Datos.Alta_Periodos_Revista(this);
            }

            public void Eliminar_Periodos_Revista()
            {
                Cls_Cat_Tal_Periodos_Revista_Datos.Eliminar_Periodos_Revista(this);
            }

            public void Modificar_Periodos_Revista()
            {
                Cls_Cat_Tal_Periodos_Revista_Datos.Modificar_Periodos_Revista(this);
            }

            public DataTable Consultar_Periodos_Revista()
            {
                return Cls_Cat_Tal_Periodos_Revista_Datos.Consultar_Periodos_Revista(this);
            }

            public Cls_Cat_Tal_Periodos_Revista_Negocio Consultar_Detalles_Periodo_Revista()
            {
                return Cls_Cat_Tal_Periodos_Revista_Datos.Consultar_Detalles_Periodo_Revista(this);
            }

        #endregion
    }
}
