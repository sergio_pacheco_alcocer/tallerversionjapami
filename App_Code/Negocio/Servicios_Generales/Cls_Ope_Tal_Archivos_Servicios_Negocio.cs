﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Operacion_Archivos_Servicios.Datos;
/// <summary>
/// Summary description for Cls_Ope_Tal_Archivos_Servicios_Negocio
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Operacion_Archivos_Servicios.Negocio
{
    public class Cls_Ope_Tal_Archivos_Servicios_Negocio
    {
        #region Variables Internas

        private DataTable Dt_Imagenes_Servicio = null;
        private String Usuario = String.Empty;
        private String Estatus = String.Empty;
        private String Nombre_Archivo = String.Empty;
        private Int32 No_Solicitud = 0;
        private Int32 Imagen_Servicio_ID = 0;
        private DateTime Fecha = new DateTime();
        private SqlCommand Cmd;
        private SqlTransaction Trans;
        #endregion

        #region Variables Publicas

        public DataTable P_Dt_Imagenes_Servicio
        {
            get { return Dt_Imagenes_Servicio; }
            set { Dt_Imagenes_Servicio = value; }
        }
        public String P_Usuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_Nombre_Archivo
        {
            get { return Nombre_Archivo; }
            set { Nombre_Archivo = value; }
        }
        public Int32 P_No_Solicitud
        {
            get { return No_Solicitud; }
            set { No_Solicitud = value; }
        }
        public Int32 P_Imagen_Servicio_ID
        {
            get { return Imagen_Servicio_ID; }
            set { Imagen_Servicio_ID = value; }
        }
        public DateTime P_Fecha
        {
            get { return Fecha; }
            set { Fecha = value; }
        }
        public SqlCommand P_Cmd
        {
            get { return Cmd;}
            set { Cmd = value; }
        }
        public SqlTransaction P_Trans
        {
            get { return Trans; }
            set { Trans = value; }
        }

        #endregion

        #region Metodos

        public void Alta_Imagenes_Servicio()
        {
            Cls_Ope_Tal_Archivos_Servicios_Datos.Alta_Imagenes_Servicio(this);
        }
        public DataTable Consulta_Imagenes_Servicio()
        {
            return Cls_Ope_Tal_Archivos_Servicios_Datos.Consulta_Imagenes_Servicio(this);
        }
        public Cls_Ope_Tal_Archivos_Servicios_Negocio()
        {
        }
        public void Eliminar_Imagen()
        {
            Cls_Ope_Tal_Archivos_Servicios_Datos.Eliminar_Imagen(this);
        }
        #endregion

        
    }
}
