﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Taller_Mecanico.Catalogo_Detalles_Rev_Mec.Datos;

/// <summary>
/// Summary description for Cls_Cat_Tal_Detalles_Rev_Mec_Negocio
/// </summary>
namespace JAPAMI.Taller_Mecanico.Catalogo_Detalles_Rev_Mec.Negocio
{
    public class Cls_Cat_Tal_Detalles_Rev_Mec_Negocio
    {
        #region Variables Internas

            private String Detalle_ID = String.Empty;
            private String Nombre = String.Empty;
            private String Estatus = String.Empty;
            private String Tipo = String.Empty;
            private String Parent = String.Empty;
            private String Usuario_Creo = String.Empty;
            private DateTime Fecha_Creo = new DateTime();
            private String Usuario_Modifico = String.Empty;
            private DateTime Fecha_Modifico = new DateTime();

        #endregion

        #region Variables Publicas

            public String P_Detalle_ID
            {
                get { return Detalle_ID; }
                set { Detalle_ID = value; }
            }
            public String P_Nombre
            {
                get { return Nombre; }
                set { Nombre = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Tipo
            {
                get { return Tipo; }
                set { Tipo = value; }
            }
            public String P_Parent
            {
                get { return Parent; }
                set { Parent = value; }
            }
            public String P_Usuario_Creo
            {
                get { return Usuario_Creo; }
                set { Usuario_Creo = value; }
            }
            public DateTime P_Fecha_Creo
            {
                get { return Fecha_Creo; }
                set { Fecha_Creo = value; }
            }
            public String P_Usuario_Modifico
            {
                get { return Usuario_Modifico; }
                set { Usuario_Modifico = value; }
            }
            public DateTime P_Fecha_Modifico
            {
                get { return Fecha_Modifico; }
                set { Fecha_Modifico = value; }
            }

        #endregion

        #region Metodos

            public void Alta_Detalles_Rev_Mec()
            {
                Cls_Cat_Tal_Detalles_Rev_Mec_Datos.Alta_Detalles_Rev_Mec(this);
            }

            public void Modificar_Detalles_Rev_Mec()
            {
                Cls_Cat_Tal_Detalles_Rev_Mec_Datos.Modificar_Detalles_Rev_Mec(this);
            }

            public System.Data.DataTable Consultar_Detalles_Rev_Mec()
            {
                return Cls_Cat_Tal_Detalles_Rev_Mec_Datos.Consultar_Detalles_Rev_Mec(this);
            }

            public void Eliminar_Detalles_Rev_Mec()
            {
                Cls_Cat_Tal_Detalles_Rev_Mec_Datos.Eliminar_Detalles_Rev_Mec(this);
            }

        #endregion
    }
}
