﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Salidas_Vehiculos.Datos;

/// <summary>
/// Summary description for Cls_Ope_Tal_Salidas_Vehiculos_Negocio
/// </summary>
namespace JAPAMI.Taller_Mecanico.Operacion_Salidas_Vehiculos.Negocio {
    public class Cls_Ope_Tal_Salidas_Vehiculos_Negocio {
        
        #region "Variables Internas"

            private Int32 No_Salida = (-1);
            private Int32 No_Entrada = (-1);
            private String Tipo_Salida = String.Empty;
            private String Tipo_Bien = String.Empty;
            private String Vehiculo_ID = String.Empty;
            private DateTime Fecha_Salida = new DateTime();
            private Int32 No_Solicitud = (-1);
            private String Empleado_Entrego_ID = String.Empty;
            private String Empleado_Recibio_ID = String.Empty;
            private String Estatus = String.Empty;
            private String Usuario = String.Empty;
            private Double Kilometraje = (-1.0);
            private DataTable Dt_Detalles = new DataTable();
            private String Tipo_Solicitud = String.Empty;
            private String Comentarios = String.Empty;
            private DataTable Dt_Archivos = new DataTable();

        #endregion

        #region "Variables Públicas"

            public Int32 P_No_Salida
            {
                set { No_Salida = value; }
                get { return No_Salida; }
            }
            public Int32 P_No_Entrada
            {
                set { No_Entrada = value; }
                get { return No_Entrada; }
            }
            public String P_Tipo_Salida
            {
                set { Tipo_Salida = value; }
                get { return Tipo_Salida; }
            }
            public String P_Tipo_Bien
            {
                set { Tipo_Bien = value; }
                get { return Tipo_Bien; }
            }
            public String P_Vehiculo_ID {
                set { Vehiculo_ID = value; }
                get { return Vehiculo_ID; }
            }
            public DateTime P_Fecha_Salida
            {
                set { Fecha_Salida = value; }
                get { return Fecha_Salida; }
            }
            public Int32 P_No_Solicitud {
                set { No_Solicitud = value; }
                get { return No_Solicitud; }
            }
            public String P_Empleado_Entrego_ID
            {
                set { Empleado_Entrego_ID = value; }
                get { return Empleado_Entrego_ID; }
            }
            public String P_Empleado_Recibio_ID
            {
                set { Empleado_Recibio_ID = value; }
                get { return Empleado_Recibio_ID; }
            }
            public String P_Estatus
            {
                set { Estatus = value; }
                get { return Estatus; }
            }
            public String P_Usuario
            {
                set { Usuario = value; }
                get { return Usuario; }
            }
            public Double P_Kilometraje
            {
                set { Kilometraje = value; }
                get { return Kilometraje; }
            }
            public DataTable P_Dt_Detalles {
                set { Dt_Detalles = value; }
                get { return Dt_Detalles; }
            }
            public String P_Tipo_Solicitud {
                get { return Tipo_Solicitud; }
                set { Tipo_Solicitud = value; }
            }
            public String P_Comentarios
            {
                get { return Comentarios; }
                set { Comentarios = value; }
            }
            public DataTable P_Dt_Archivos
            {
                get { return Dt_Archivos; }
                set { Dt_Archivos = value; }
            }
        #endregion

        #region "Metodos"

            public void Alta_Salida_Vehiculo() {
                Cls_Ope_Tal_Salidas_Vehiculos_Datos.Alta_Salida_Vehiculo(this);
            }
            public DataSet Consultar_Datos_Reporte_Salida_Revista()
            {
                return Cls_Ope_Tal_Salidas_Vehiculos_Datos.Consultar_Datos_Reporte_Salida_Revista(this);
            }
            public Cls_Ope_Tal_Salidas_Vehiculos_Negocio Consultar_Detalles_Salida_Vehiculo()
            {
                return Cls_Ope_Tal_Salidas_Vehiculos_Datos.Consultar_Detalles_Salida_Vehiculo(this);
            }
        #endregion

	}
}
