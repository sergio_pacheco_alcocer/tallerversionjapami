﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Reporte_Solicitud_Servicio.Datos;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Partes_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Catalogo_Detalles_Rev_Mec.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Revista_Mecanica.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Tal_Rpt_Solicitud_Servicio_Ayudante
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Reporte_Solicitud_Servicio.Negocio {
    public class Cls_Rpt_Tal_Solicitud_Servicio_Negocio {

        #region [Variables Internas]

            private Int32 No_Solicitud = (-1);
            private HttpServerUtility Server_ = null;
            private String No_Servicio = String.Empty;
            private String Tipo_Servicio = String.Empty;
            private String Tipo_Bien = String.Empty;

        #endregion

        #region [Variables Publicas]

            public Int32 P_No_Solicitud {
                get { return No_Solicitud; }
                set { No_Solicitud = value; }
            }

            public HttpServerUtility P_Server_
            {
                get { return Server_; }
                set { Server_ = value; }
            }
            public String P_No_Servicio
            {
                get { return No_Servicio; }
                set { No_Servicio = value; }
            }
            public String P_Tipo_Servicio
            {
                get { return Tipo_Servicio; }
                set { Tipo_Servicio = value; }
            }
            public String P_Tipo_Bien
            {
                get { return Tipo_Bien; }
                set { Tipo_Bien = value; }
            }
        #endregion

        #region [Metodos]

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Crear_Reporte_Solicitud_Servicio
            ///DESCRIPCIÓN: Gestiona la Creacion del Reporte de Orden de Trabajo
            ///PARAMETROS:  
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 24/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            public DataSet Crear_Reporte_Solicitud_Servicio() {
                Ds_Rpt_Tal_Solicitud_Servicio Ds_Reporte = new Ds_Rpt_Tal_Solicitud_Servicio();
                DataTable Dt_Con_Datos_Solicitud = Cls_Rpt_Tal_Solicitud_Servicio_Datos.Consulta_Datos_Solicitud(this);
                DataSet Ds_Consulta = null;
                DataTable Dt_Rep_Datos_Solicitud = Ds_Reporte.Tables["DT_GENERALES"].Clone();
                String Codigo_Programatico = "";
                SqlCommand Cmd = null;
                Dt_Rep_Datos_Solicitud.TableName = "DT_GENERALES";

                if (Dt_Con_Datos_Solicitud != null) {
                    if (Dt_Con_Datos_Solicitud.Rows.Count > 0) {
                        //Cargar Datos Generales.
                        DataRow Fila_Nueva = Dt_Rep_Datos_Solicitud.NewRow();
                        foreach (DataColumn Columa in Dt_Con_Datos_Solicitud.Columns) {
                            if (Dt_Rep_Datos_Solicitud.Columns.Contains(Columa.ColumnName)) {
                                Fila_Nueva[Columa.ColumnName] = Dt_Con_Datos_Solicitud.Rows[0][Columa.ColumnName];
                            }
                        }
                        Dt_Rep_Datos_Solicitud.Rows.Add(Fila_Nueva);
                        Codigo_Programatico = new Cls_Ope_Tal_Consultas_Generales_Negocio().Consultar_Codigo_Programatico_Servicios(this.No_Solicitud, ref Cmd);
                        //Cargar Datos Especificos.
                        Dt_Rep_Datos_Solicitud.Rows[0].SetField("CODIGO_PROGRAMATICO", Codigo_Programatico);
                        if (Dt_Con_Datos_Solicitud.Rows[0]["TIPO_SOLICITUD"].ToString().Trim().Equals("REVISTA MECANICA"))
                        {
                            DataTable Dt_Datos = Cls_Rpt_Tal_Solicitud_Servicio_Datos.Consulta_Datos_Revista_Mecanica(this);
                            if (Dt_Datos != null) {
                                if (Dt_Datos.Rows.Count > 0) {
                                    Dt_Rep_Datos_Solicitud.Rows[0].SetField("PROVEEDOR_ASIGNADO", Dt_Datos.Rows[0]["MECANICO"].ToString().Trim());
                                    Dt_Rep_Datos_Solicitud.Rows[0].SetField("DIAGNISTICO_MECANICO", Dt_Datos.Rows[0]["DIAGNOSTICO_MECANICO"].ToString().Trim());
                                }
                            }
                        } else if (Dt_Con_Datos_Solicitud.Rows[0]["TIPO_SOLICITUD"].ToString().Trim().Equals("SERVICIO CORRECTIVO")) {
                            DataTable Dt_Datos = Cls_Rpt_Tal_Solicitud_Servicio_Datos.Consulta_Datos_Servicio_Correctivo(this);
                            if (Dt_Datos != null) {
                                if (Dt_Datos.Rows.Count > 0) {
                                    if (Dt_Datos.Rows[0]["REPARACION"].ToString().Trim().Equals("INTERNA"))
                                    {
                                        Dt_Rep_Datos_Solicitud.Rows[0].SetField("COSTO_TOTAL", Cls_Rpt_Tal_Solicitud_Servicio_Datos.Consulta_Costo_Total_Servicio(Dt_Rep_Datos_Solicitud.Rows[0]["NO_SOLICITUD"].ToString()).ToString());
                                        Dt_Rep_Datos_Solicitud.Rows[0].SetField("REPARACION", "TRABAJO INTERNO");
                                        Dt_Rep_Datos_Solicitud.Rows[0].SetField("PROVEEDOR_ASIGNADO", Dt_Datos.Rows[0]["MECANICO"].ToString().Trim());
                                        Dt_Rep_Datos_Solicitud.Rows[0].SetField("DIAGNISTICO_MECANICO", Dt_Datos.Rows[0]["DIAGNOSTICO_MECANICO"].ToString().Trim());
                                        DataTable Dt_Seguimiento = Cls_Rpt_Tal_Solicitud_Servicio_Datos.Consulta_Ultimo_Seg_Mecanico(this);
                                        if (Dt_Seguimiento.Rows.Count > 0)
                                            Dt_Rep_Datos_Solicitud.Rows[0].SetField("SERVICIO_REALIZADO", Dt_Seguimiento.Rows[0]["SERVICIO_REALIZADO"].ToString().Trim());
                                    }
                                    else if (Dt_Datos.Rows[0]["REPARACION"].ToString().Trim().Equals("EXTERNA"))
                                    {
                                        Dt_Rep_Datos_Solicitud.Rows[0].SetField("REPARACION", "TALLER EXTERNO");
                                        if (!string.IsNullOrEmpty(Dt_Datos.Rows[0]["PROVEEDOR"].ToString().Trim()))
                                            Dt_Rep_Datos_Solicitud.Rows[0].SetField("PROVEEDOR_ASIGNADO", Dt_Datos.Rows[0]["PROVEEDOR"].ToString().Trim());
                                        else
                                            Dt_Rep_Datos_Solicitud.Rows[0].SetField("PROVEEDOR_ASIGNADO", Dt_Datos.Rows[0]["MECANICO"].ToString().Trim());
                                        Dt_Rep_Datos_Solicitud.Rows[0].SetField("DIAGNISTICO_MECANICO", Dt_Datos.Rows[0]["DIAGNOSTICO_PROVEEDOR"].ToString().Trim());
                                        DataTable Dt_Seguimiento = Cls_Rpt_Tal_Solicitud_Servicio_Datos.Consulta_Ultimo_Seg_Proveedor(this);
                                        if (Dt_Seguimiento.Rows.Count>0)
                                            Dt_Rep_Datos_Solicitud.Rows[0].SetField("SERVICIO_REALIZADO", Dt_Seguimiento.Rows[0]["SERVICIO_REALIZADO"].ToString().Trim());
                                        Dt_Rep_Datos_Solicitud.Rows[0].SetField("COSTO_TOTAL", Cls_Rpt_Tal_Solicitud_Servicio_Datos.Consulta_Costo_Total_Servicio(Dt_Datos.Rows[0]["NO_SERVICIO"].ToString(), Dt_Rep_Datos_Solicitud.Rows[0]["TIPO_SOLICITUD"].ToString().Trim().Replace(" ","_")).ToString());

                                    }
                                }
                            }
                        } else if (Dt_Con_Datos_Solicitud.Rows[0]["TIPO_SOLICITUD"].ToString().Trim().Equals("SERVICIO PREVENTIVO")) {
                            DataTable Dt_Datos = Cls_Rpt_Tal_Solicitud_Servicio_Datos.Consulta_Datos_Servicio_Preventivo(this);
                            if (Dt_Datos != null) {
                                if (Dt_Datos.Rows.Count > 0) {
                                    if (Dt_Datos.Rows[0]["REPARACION"].ToString().Trim().Equals("INTERNA"))
                                    {
                                        Dt_Rep_Datos_Solicitud.Rows[0].SetField("COSTO_TOTAL", Cls_Rpt_Tal_Solicitud_Servicio_Datos.Consulta_Costo_Total_Servicio(Dt_Rep_Datos_Solicitud.Rows[0]["NO_SOLICITUD"].ToString()).ToString());
                                        DataTable Dt_Seguimiento = Cls_Rpt_Tal_Solicitud_Servicio_Datos.Consulta_Ultimo_Seg_Mecanico(this);
                                        if (Dt_Seguimiento.Rows.Count > 0)
                                        Dt_Rep_Datos_Solicitud.Rows[0].SetField("SERVICIO_REALIZADO", Dt_Seguimiento.Rows[0]["SERVICIO_REALIZADO"].ToString().Trim());
                                        Dt_Rep_Datos_Solicitud.Rows[0].SetField("PROVEEDOR_ASIGNADO", Dt_Datos.Rows[0]["MECANICO"].ToString().Trim());
                                        Dt_Rep_Datos_Solicitud.Rows[0].SetField("DIAGNISTICO_MECANICO", Dt_Datos.Rows[0]["DIAGNOSTICO_MECANICO"].ToString().Trim());
                                    }
                                    else if (Dt_Datos.Rows[0]["REPARACION"].ToString().Trim().Equals("EXTERNA"))
                                    {
                                        if (!string.IsNullOrEmpty(Dt_Datos.Rows[0]["PROVEEDOR"].ToString().Trim()))
                                            Dt_Rep_Datos_Solicitud.Rows[0].SetField("PROVEEDOR_ASIGNADO", Dt_Datos.Rows[0]["PROVEEDOR"].ToString().Trim());
                                        else
                                            Dt_Rep_Datos_Solicitud.Rows[0].SetField("PROVEEDOR_ASIGNADO", Dt_Datos.Rows[0]["MECANICO"].ToString().Trim());
                                        Dt_Rep_Datos_Solicitud.Rows[0].SetField("DIAGNISTICO_MECANICO", Dt_Datos.Rows[0]["DIAGNOSTICO_PROVEEDOR"].ToString().Trim());
                                        DataTable Dt_Seguimiento = Cls_Rpt_Tal_Solicitud_Servicio_Datos.Consulta_Ultimo_Seg_Proveedor(this);
                                        if (Dt_Seguimiento.Rows.Count > 0)
                                            Dt_Rep_Datos_Solicitud.Rows[0].SetField("SERVICIO_REALIZADO", Dt_Seguimiento.Rows[0]["SERVICIO_REALIZADO"].ToString().Trim());
                                        Dt_Rep_Datos_Solicitud.Rows[0].SetField("COSTO_TOTAL", Cls_Rpt_Tal_Solicitud_Servicio_Datos.Consulta_Costo_Total_Servicio(Dt_Datos.Rows[0]["NO_SERVICIO"].ToString(), Dt_Rep_Datos_Solicitud.Rows[0]["TIPO_SOLICITUD"].ToString().Trim().Replace(" ", "_")).ToString());
                                    }
                                }
                            }
                        }
                        else if (Dt_Con_Datos_Solicitud.Rows[0]["TIPO_SOLICITUD"].ToString().Trim().Equals("VERIFICACION"))
                        {
                            DataTable Dt_Datos = Cls_Rpt_Tal_Solicitud_Servicio_Datos.Consulta_Datos_Servicio_Preventivo(this);
                            Dt_Rep_Datos_Solicitud.Rows[0].SetField("REPARACION", "TALLER EXTERNO");
                            Dt_Rep_Datos_Solicitud.Rows[0].SetField("EMPLEADO_RECIBE", Dt_Rep_Datos_Solicitud.Rows[0]["EMPLEADO_ENTREGA"].ToString());
                            if (Dt_Datos != null)
                            {
                                if (Dt_Datos.Rows.Count > 0)
                                {
                                    Dt_Rep_Datos_Solicitud.Rows[0].SetField("PROVEEDOR_ASIGNADO", Dt_Datos.Rows[0]["PROVEEDOR"].ToString().Trim());
                                    Dt_Rep_Datos_Solicitud.Rows[0].SetField("DIAGNISTICO_MECANICO", Dt_Datos.Rows[0]["DIAGNOSTICO_PROVEEDOR"].ToString().Trim());
                                }
                            }
                        }

                        if (!Dt_Con_Datos_Solicitud.Rows[0]["TIPO_SOLICITUD"].ToString().Trim().Equals("REVISTA MECANICA")) {
                            DataTable Dt_SubPartes = new DataTable("DT_SUBPARTES");
                            DataTable Dt_Partes_Completo = new DataTable("DT_PARTES_COMPLETO");
                            DataTable Dt_Partes = new DataTable("DT_PARTES");

                            Cls_Ope_Tal_Entradas_Vehiculos_Negocio Sol_Negocio = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
                            Sol_Negocio.P_No_Solicitud = P_No_Solicitud;
                            Sol_Negocio = Sol_Negocio.Consultar_Detalles_Entrada_Vehiculo();
                            Dt_Partes_Completo = Sol_Negocio.P_Dt_Detalles;
                            
                            //Int32 No_Registros_Columna = 0;

                            if (Dt_Partes_Completo.Rows.Count > 0) {
                                Dt_Partes = Dt_Partes_Completo.DefaultView.ToTable(true, "PARTE_ID", "PARTE_NOMBRE");
                                Dt_Partes = Separar_Partes_Columnas(Dt_Partes);
                                Dt_Partes.TableName = "DT_PARTES";
                                Dt_SubPartes = Dt_Partes_Completo;
                                Dt_SubPartes.TableName = "DT_SUBPARTES";
                            } else {
                                Dt_SubPartes = new Cls_Ope_Tal_Consultas_Generales_Negocio().Consultar_Partes_SubPartes_Vehiculo();
                                Dt_SubPartes.TableName = "DT_SUBPARTES";
                                Dt_SubPartes.Columns.Add("VALOR_SI", Type.GetType("System.String"));
                                Dt_SubPartes.Columns.Add("VALOR_NO", Type.GetType("System.String"));

                                Cls_Cat_Tal_Partes_Vehiculos_Negocio Parte_Negocio = new Cls_Cat_Tal_Partes_Vehiculos_Negocio();
                                Parte_Negocio.P_Tipo = "PARTE";
                                Parte_Negocio.P_Estatus = "VIGENTE";
                                Dt_Partes_Completo = Parte_Negocio.Consultar_Partes();
                                Dt_Partes_Completo.Columns["NOMBRE"].ColumnName = "PARTE_NOMBRE";

                                Dt_Partes = Separar_Partes_Columnas(Dt_Partes_Completo);
                                Dt_Partes.TableName = "DT_PARTES";
                            }

                            //Se crea El DataSet de los Datos
                            Ds_Consulta = new DataSet();
                            Ds_Consulta.Tables.Add(Dt_Rep_Datos_Solicitud.Copy());
                            Ds_Consulta.Tables.Add(Dt_SubPartes.Copy());
                            Ds_Consulta.Tables.Add(Dt_Partes.Copy());
                            Ds_Consulta.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(Server_));
                        } else {
                            DataTable Dt_Partes_Totales = new DataTable("DT_PARTES_TOTALES");
                            DataTable Dt_Detalles_1 = new DataTable("DT_DETALLES_1");
                            DataTable Dt_Detalles_2 = new DataTable("DT_DETALLES_2");
                            DataTable Dt_Detalles_3 = new DataTable("DT_DETALLES_3");
                            Cls_Ope_Tal_Revista_Mecanica_Negocio Rev_Negocio = new Cls_Ope_Tal_Revista_Mecanica_Negocio();
                            Rev_Negocio.P_No_Solicitud = P_No_Solicitud;
                            Rev_Negocio = Rev_Negocio.Consultar_Detalles_Revista_Mecanica();
                            Dt_Partes_Totales = Rev_Negocio.P_Dt_Detalles_Revista;

                            Int32 No_Registros_Columna = 0;

                            if (Dt_Partes_Totales.Rows.Count > 0) {
                                No_Registros_Columna = Convert.ToInt32(Math.Floor(Convert.ToDouble(Dt_Partes_Totales.Rows.Count / 3)) + 1);
                                Dt_Detalles_1 = Separar_Partes_Columnas_Revista_Mecanica(Dt_Partes_Totales, 0, No_Registros_Columna);
                                Dt_Detalles_2 = Separar_Partes_Columnas_Revista_Mecanica(Dt_Partes_Totales, No_Registros_Columna, No_Registros_Columna);
                                Dt_Detalles_3 = Separar_Partes_Columnas_Revista_Mecanica(Dt_Partes_Totales, (No_Registros_Columna * 2), No_Registros_Columna);
                                Dt_Detalles_1.TableName = "DT_DETALLES_1";
                                Dt_Detalles_2.TableName = "DT_DETALLES_2";
                                Dt_Detalles_3.TableName = "DT_DETALLES_3";
                                Int32 Total = Dt_Partes_Totales.Rows.Count;
                                Total = Total + Dt_Detalles_1.DefaultView.ToTable(true, "PARTE_ID").Rows.Count;
                                Total = Total + Dt_Detalles_2.DefaultView.ToTable(true, "PARTE_ID").Rows.Count;
                                Total = Total + Dt_Detalles_3.DefaultView.ToTable(true, "PARTE_ID").Rows.Count;
                                No_Registros_Columna = Convert.ToInt32(Math.Floor(Convert.ToDouble(Total / 3)) + 1);
                                Dt_Detalles_1 = Separar_Partes_Columnas_Revista_Mecanica(Dt_Partes_Totales, 0, No_Registros_Columna);
                                Dt_Detalles_2 = Separar_Partes_Columnas_Revista_Mecanica(Dt_Partes_Totales, No_Registros_Columna, No_Registros_Columna);
                                Dt_Detalles_3 = Separar_Partes_Columnas_Revista_Mecanica(Dt_Partes_Totales, (No_Registros_Columna * 2), No_Registros_Columna);
                                Dt_Detalles_1.TableName = "DT_DETALLES_1";
                                Dt_Detalles_2.TableName = "DT_DETALLES_2";
                                Dt_Detalles_3.TableName = "DT_DETALLES_3";
                            } else {
                                Dt_Partes_Totales = new Cls_Ope_Tal_Consultas_Generales_Negocio().Consultar_Partes_SubPartes_Vehiculo_Revista_Mecanica();
                                Dt_Partes_Totales.Columns.Add("VALOR", Type.GetType("System.String"));
                                No_Registros_Columna = Convert.ToInt32(Math.Floor(Convert.ToDouble(Dt_Partes_Totales.Rows.Count / 3)) + 1);
                                Dt_Detalles_1 = Separar_Partes_Columnas_Revista_Mecanica(Dt_Partes_Totales, 0, No_Registros_Columna);
                                Dt_Detalles_2 = Separar_Partes_Columnas_Revista_Mecanica(Dt_Partes_Totales, No_Registros_Columna, No_Registros_Columna);
                                Dt_Detalles_3 = Separar_Partes_Columnas_Revista_Mecanica(Dt_Partes_Totales, (No_Registros_Columna * 2), No_Registros_Columna);
                                Dt_Detalles_1.TableName = "DT_DETALLES_1";
                                Dt_Detalles_2.TableName = "DT_DETALLES_2";
                                Dt_Detalles_3.TableName = "DT_DETALLES_3";
                                Int32 Total = Dt_Partes_Totales.Rows.Count;
                                Total = Total + Dt_Detalles_1.DefaultView.ToTable(true, "PARTE_ID").Rows.Count;
                                Total = Total + Dt_Detalles_2.DefaultView.ToTable(true, "PARTE_ID").Rows.Count;
                                Total = Total + Dt_Detalles_3.DefaultView.ToTable(true, "PARTE_ID").Rows.Count;
                                No_Registros_Columna = Convert.ToInt32(Math.Floor(Convert.ToDouble(Total / 3)) + 1);
                                Dt_Detalles_1 = Separar_Partes_Columnas_Revista_Mecanica(Dt_Partes_Totales, 0, No_Registros_Columna);
                                Dt_Detalles_2 = Separar_Partes_Columnas_Revista_Mecanica(Dt_Partes_Totales, No_Registros_Columna, No_Registros_Columna);
                                Dt_Detalles_3 = Separar_Partes_Columnas_Revista_Mecanica(Dt_Partes_Totales, (No_Registros_Columna * 2), No_Registros_Columna);
                                Dt_Detalles_1.TableName = "DT_DETALLES_1";
                                Dt_Detalles_2.TableName = "DT_DETALLES_2";
                                Dt_Detalles_3.TableName = "DT_DETALLES_3";
                            }

                            //Se crea El DataSet de los Datos
                            Ds_Consulta = new DataSet();
                            Ds_Consulta.Tables.Add(Dt_Rep_Datos_Solicitud.Copy());
                            Ds_Consulta.Tables.Add(Dt_Detalles_1.Copy());
                            Ds_Consulta.Tables.Add(Dt_Detalles_2.Copy());
                            Ds_Consulta.Tables.Add(Dt_Detalles_3.Copy());
                            Ds_Consulta.Tables.Add(new Cls_Tal_Parametros_Negocio().Obtener_Tabla_Reporte(Server_));
                        }
                    }
                }
                return Ds_Consulta;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consulta_Datos_Solicitud
            ///DESCRIPCIÓN: 
            ///PARAMETROS:  
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 24/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            public DataTable Consulta_Datos_Solicitud() {
               return Cls_Rpt_Tal_Solicitud_Servicio_Datos.Consulta_Datos_Solicitud(this);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Separar_Partes_Columnas
            ///DESCRIPCIÓN: Separa en 3 columnas los datos de las tablas
            ///PARAMETROS:  
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 24/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private DataTable Separar_Partes_Columnas(DataTable Dt_Datos) {
                DataTable Dt_Partes_Separadas = new DataTable();
                Dt_Partes_Separadas.Columns.Add("PARTE_ID_1", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_NOMBRE_1", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_ID_2", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_NOMBRE_2", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_ID_3", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_NOMBRE_3", Type.GetType("System.String"));
                Int32 Fila_Actual = -1;
                Int32 Columna_Toca = 1;
                foreach (DataRow Fila_Leida in Dt_Datos.Rows) {
                    if (Columna_Toca == 1) {
                        DataRow Fila_Nueva = Dt_Partes_Separadas.NewRow();
                        Fila_Nueva["PARTE_ID_1"] = Fila_Leida["PARTE_ID"].ToString().Trim();
                        Fila_Nueva["PARTE_NOMBRE_1"] = Fila_Leida["PARTE_NOMBRE"].ToString().Trim();
                        Dt_Partes_Separadas.Rows.Add(Fila_Nueva);
                        Fila_Actual++;
                        Columna_Toca = 2;
                    } else if (Columna_Toca == 2) {
                        Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_ID_2", Fila_Leida["PARTE_ID"].ToString().Trim());
                        Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_NOMBRE_2", Fila_Leida["PARTE_NOMBRE"].ToString().Trim());
                        Columna_Toca = 3;
                    } else if (Columna_Toca == 3) {
                        Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_ID_3", Fila_Leida["PARTE_ID"].ToString().Trim());
                        Dt_Partes_Separadas.Rows[Fila_Actual].SetField("PARTE_NOMBRE_3", Fila_Leida["PARTE_NOMBRE"].ToString().Trim());
                        Columna_Toca = 1;
                    }
                }
                return Dt_Partes_Separadas;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Separar_Partes_Columnas
            ///DESCRIPCIÓN: Separa en 3 columnas los datos de las tablas
            ///PARAMETROS:  
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: 24/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private DataTable Separar_Partes_Columnas_Revista_Mecanica(DataTable Dt_Datos, Int32 Inicial, Int32 No_Iteraciones) {
                DataTable Dt_Partes_Separadas = new DataTable();
                Dt_Partes_Separadas.Columns.Add("PARTE_ID", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("PARTE_NOMBRE", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("SUBPARTE_ID", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("SUBPARTE_NOMBRE", Type.GetType("System.String"));
                Dt_Partes_Separadas.Columns.Add("VALOR", Type.GetType("System.String"));
                for (Int32 Contador = Inicial; Contador < (Inicial + No_Iteraciones); Contador++) {
                    if (Contador < Dt_Datos.Rows.Count) {
                        DataRow Fila = Dt_Partes_Separadas.NewRow();
                        Fila["PARTE_ID"] = Dt_Datos.Rows[Contador]["PARTE_ID"].ToString().Trim();
                        Fila["PARTE_NOMBRE"] = Dt_Datos.Rows[Contador]["PARTE_NOMBRE"].ToString().Trim();
                        Fila["SUBPARTE_ID"] = Dt_Datos.Rows[Contador]["SUBPARTE_ID"].ToString().Trim();
                        Fila["SUBPARTE_NOMBRE"] = Dt_Datos.Rows[Contador]["SUBPARTE_NOMBRE"].ToString().Trim();
                        Fila["VALOR"] = Dt_Datos.Rows[Contador]["VALOR"].ToString().Trim();
                        Dt_Partes_Separadas.Rows.Add(Fila);
                    } else {
                        break;
                    }
                }
                return Dt_Partes_Separadas;
            }

        #endregion

    }
}