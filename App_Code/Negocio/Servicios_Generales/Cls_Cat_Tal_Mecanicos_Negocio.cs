﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Catalogo_Mecanicos.Datos;

/// <summary>
/// Summary description for Cls_Cat_Tal_Mecanicos_Negocio
/// </summary>
namespace JAPAMI.Taller_Mecanico.Catalogo_Mecanicos.Negocio
{
    public class Cls_Cat_Tal_Mecanicos_Negocio {

        #region Variables Internas

            private String Mecanico_ID = String.Empty;
            private String Empleado_ID = String.Empty;
            private String Estatus = String.Empty;
            private String Comentarios = String.Empty;
            private String Nivel = String.Empty;
            private Double Costo_Hora = (-1.0);
            private String Usuario = String.Empty;
            private String No_Empleado = String.Empty;
            private String Nombre_Empleado = String.Empty;

        #endregion

        #region Variables Publicas

            public String P_Mecanico_ID {
                get { return Mecanico_ID; }
                set { Mecanico_ID = value; }
            }
            public String P_Empleado_ID {
                get { return Empleado_ID; }
                set { Empleado_ID = value; }
            }
            public String P_Estatus {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Comentarios
            {
                get { return Comentarios; }
                set { Comentarios = value; }
            }
            public String P_Nivel
            {
                get { return Nivel; }
                set { Nivel = value; }
            }
            public Double P_Costo_Hora
            {
                get { return Costo_Hora; }
                set { Costo_Hora = value; }
            }
            public String P_Usuario {
                get { return Usuario; }
                set { Usuario = value; }
            }
            public String P_No_Empleado
            {
                get { return No_Empleado; }
                set { No_Empleado = value; }
            }
            public String P_Nombre_Empleado
            {
                get { return Nombre_Empleado; }
                set { Nombre_Empleado = value; }
            }

        #endregion

        #region Metodos

            public void Alta_Mecanico() {
                Cls_Cat_Tal_Mecanicos_Datos.Alta_Mecanico(this);
            }

            public void Modificar_Mecanico() {
                Cls_Cat_Tal_Mecanicos_Datos.Modificar_Mecanico(this);
            }

            public DataTable Consultar_Mecanicos() {
                return Cls_Cat_Tal_Mecanicos_Datos.Consultar_Mecanicos(this);
            }

            public Cls_Cat_Tal_Mecanicos_Negocio Consultar_Detalles_Mecanico(){
                return Cls_Cat_Tal_Mecanicos_Datos.Consultar_Detalles_Mecanico(this);
            }

            public void Eliminar_Mecanico() {
                Cls_Cat_Tal_Mecanicos_Datos.Eliminar_Mecanico(this);
            }

        #endregion

	}
}