﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Catalogo_Documentos.Datos;

/// <summary>
/// Summary description for Cls_Cat_Tal_Documentos_Negocio
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Catalogo_Documentos.Negocio
{
    public class Cls_Cat_Tal_Documentos_Negocio {

        #region Variables Internas

            private String Documento_ID = String.Empty;
            private String Nombre = String.Empty;
            private String Tipo = String.Empty;
            private String Comentarios = String.Empty;
            private String Usuario = String.Empty;
            private String OrderBy = String.Empty;

        #endregion

        #region Variables Publicas

            public String P_Documento_ID {
                get { return Documento_ID; }
                set { Documento_ID = value; }
            }
            public String P_Nombre {
                get { return Nombre; }
                set { Nombre = value; }
            }
            public String P_Tipo {
                get { return Tipo; }
                set { Tipo = value; }
            }
            public String P_Comentarios
            {
                get { return Comentarios; }
                set { Comentarios = value; }
            }
            public String P_Usuario
            {
                get { return Usuario; }
                set { Usuario = value; }
            }
            public String P_OrderBy
            {
                get { return OrderBy; }
                set { OrderBy = value; }
            }

        #endregion

        #region Metodos

            public void Alta_Documento() {
                Cls_Cat_Tal_Documentos_Datos.Alta_Documento(this);
            }

            public void Modificar_Documento() {
                Cls_Cat_Tal_Documentos_Datos.Modificar_Documento(this);
            }

            public DataTable Consultar_Documentos() {
                return Cls_Cat_Tal_Documentos_Datos.Consultar_Documentos(this);
            }

            public Cls_Cat_Tal_Documentos_Negocio Consultar_Detalles_Documento(){
                return Cls_Cat_Tal_Documentos_Datos.Consultar_Detalles_Documento(this);
            }

            public void Eliminar_Documento() {
                Cls_Cat_Tal_Documentos_Datos.Eliminar_Documento(this);
            }

        #endregion

	}
}
