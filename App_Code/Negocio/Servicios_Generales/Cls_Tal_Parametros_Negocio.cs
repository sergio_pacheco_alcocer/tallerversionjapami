﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Taller_Mecanico.Parametros.Datos;
using System.Text;
using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Data.SqlClient;

namespace JAPAMI.Taller_Mecanico.Parametros.Negocio
{

    public class Cls_Tal_Parametros_Negocio
    {
        #region Variables Internas

            private String Fuente_Financiamiento_ID = String.Empty;
            private String Partida_ID = String.Empty;
            private String Programa_ID = String.Empty;
            private String Partida_Generica_ID = String.Empty;
            private String Concepto_ID = String.Empty;
            private String Capitulo_ID = String.Empty;
            private String Dependencia_ID = String.Empty;
            private String Numero_Economico = String.Empty;
            private String Tipo_Bien = String.Empty;
            private String Director = String.Empty;
            private String Coordinador = String.Empty;
            private String Almacen = String.Empty;
            private String Oficial_Mayor = String.Empty;
            private String Tipo_Solicitud_Pago_ID = String.Empty;
            private String Proveedor_Gasolina_Id = String.Empty;
            private String Logo_Municipio = String.Empty;
            private String Logo_Estado = String.Empty;
            private String Nombre_Proveedor = String.Empty;
            private SqlCommand Cmmd;

        #endregion

        #region Variables Publicas

            public String P_Fuente_Financiamiento_ID
            {
                get { return Fuente_Financiamiento_ID; }
                set { Fuente_Financiamiento_ID = value; }
            }
            public String P_Partida_ID
            {
                get { return Partida_ID; }
                set { Partida_ID = value; }
            }            
            public String P_Programa_ID
            {
                get { return Programa_ID; }
                set { Programa_ID = value; }
            }
            public String P_Partida_Generica_ID
            {
                get { return Partida_Generica_ID; }
                set { Partida_Generica_ID = value; }
            }
            public String P_Concepto_ID
            {
                get { return Concepto_ID; }
                set { Concepto_ID = value; }
            }
            public String P_Capitulo_ID
            {
                get { return Capitulo_ID; }
                set { Capitulo_ID = value; }
            }
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }
            public String P_Numero_Economico
            {
                get { return Numero_Economico; }
                set { Numero_Economico = value; }
            }
            public String P_Tipo_Bien
            {
                get { return Tipo_Bien; }
                set { Tipo_Bien = value; }
            }
            public String P_Director
            {
                get { return Director; }
                set { Director = value; }
            }
            public String P_Coordinador
            {
                get { return Coordinador; }
                set { Coordinador = value; }
            }
            public String P_Almacen
            {
                get { return Almacen; }
                set { Almacen = value; }
            }
            public String P_Oficial_Mayor
            {
                get { return Oficial_Mayor; }
                set { Oficial_Mayor = value; }
            }
            public String P_Nombre_Proveedor
            {
                get { return Nombre_Proveedor; }
                set { Nombre_Proveedor = value; }
            }
            public String P_Tipo_Solicitud_Pago_ID
            {
                get { return Tipo_Solicitud_Pago_ID; }
                set { Tipo_Solicitud_Pago_ID = value; }
            }
            public String P_Proveedor_Gasolina_Id
            {
                get { return Proveedor_Gasolina_Id; }
                set { Proveedor_Gasolina_Id = value; }
            }
            public String P_Logo_Municipio
            {
                get { return Logo_Municipio; }
                set { Logo_Municipio = value; }
            }
            public String P_Logo_Estado
            {
                get { return Logo_Estado; }
                set { Logo_Estado = value; }
            }
            public SqlCommand P_Cmmd
            {
                get { return Cmmd; }
                set { Cmmd = value; }
            }

        #endregion

        #region Metodos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Parametros
            ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a dar de
            ///                         Alta en la Base de Datos.
            ///CREO                 : Jesus Toledo Rodriguez
            ///FECHA_CREO           : 25/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public void Alta_Parametros()
            {
                Cls_Tal_Parametros_Datos.Alta_Parametros(this);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Parametros     
            ///DESCRIPCIÓN          : Consultar un registro
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros de la capa de negocio
            ///CREO                 : Jesus Toledo Rodriguez
            ///FECHA_CREO           : 31/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public Cls_Tal_Parametros_Negocio Consulta_Parametros()
            {
                return Cls_Tal_Parametros_Datos.Consulta_Parametros(this);
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Parametros     
            ///DESCRIPCIÓN          : Consultar un registro
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros de la capa de negocio
            ///CREO                 : Jesus Toledo Rodriguez
            ///FECHA_CREO           : 31/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public Cls_Tal_Parametros_Negocio Consulta_Parametros_Presupuesto(String _Numero_Economico, String _Tipo_Bien)
            {
                this.Numero_Economico = _Numero_Economico;
                this.Tipo_Bien = _Tipo_Bien;
                return Cls_Tal_Parametros_Datos.Consulta_Parametros_Presupuesto(this);
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_Tabla_Reporte
            ///DESCRIPCIÓN: Obtiene una tabla de los logos para los reportes
            ///PROPIEDADES:   
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: Octubre/2012 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            public DataTable Obtener_Tabla_Reporte(HttpServerUtility Server) {
                Cls_Tal_Parametros_Negocio Negocio = new Cls_Tal_Parametros_Negocio();
                Negocio = Negocio.Consulta_Parametros();

                DataTable Dt_Datos = new DataTable("DT_LOGOS");
                Dt_Datos.Columns.Add("LOGO_MUNICIPIO", Type.GetType("System.Byte[]"));
                Dt_Datos.Columns.Add("LOGO_ESTADO", Type.GetType("System.Byte[]"));
                DataRow Fila = Dt_Datos.NewRow();
                if (!String.IsNullOrEmpty(Negocio.P_Logo_Municipio)) {
                    String Archivo = Server.MapPath("../../ARCHIVOS_TALLER_MUNICIPAL/PARAMETROS") + "/" + Negocio.P_Logo_Municipio.Trim();
                    if (System.IO.File.Exists(Archivo)) Fila["LOGO_MUNICIPIO"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Archivo));
                }
                if (!String.IsNullOrEmpty(Negocio.P_Logo_Estado)) {
                    String Archivo = Server.MapPath("../../ARCHIVOS_TALLER_MUNICIPAL/PARAMETROS") + "/" + Negocio.P_Logo_Estado.Trim();
                    if (System.IO.File.Exists(Archivo)) Fila["LOGO_ESTADO"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Archivo));
                }
                Dt_Datos.Rows.Add(Fila);
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Convertir_Imagen_A_Cadena_Bytes
            ///DESCRIPCIÓN: Convierte la Imagen a una Cadena de Bytes.
            ///PROPIEDADES:   1.  P_Imagen.  Imagen a Convertir.    
            ///CREO: Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO: Marzo/2012 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private Byte[] Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image P_Imagen) {
                Byte[] Img_Bytes = null;
                try {
                    if (P_Imagen != null) {
                        MemoryStream MS_Tmp = new MemoryStream();
                        P_Imagen.Save(MS_Tmp, P_Imagen.RawFormat);
                        Img_Bytes = MS_Tmp.GetBuffer();
                        MS_Tmp.Close();
                    }
                } catch (Exception Ex) {

                }
                return Img_Bytes;
            }

        #endregion

    }

}