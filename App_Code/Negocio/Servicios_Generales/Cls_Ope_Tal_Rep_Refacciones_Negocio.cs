﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Reporte.Refacciones.Datos;
using System.Text;
/// <summary>

namespace JAPAMI.Taller_Mecanico.Reporte.Refacciones.Negocio 
{

    public class Cls_Ope_Tal_Rep_Refacciones_Negocio
    {
        #region "Variables Internas"

        private String Empleado_ID = String.Empty;
        private String Unir_Tablas = String.Empty;
        private String No_Empleado = String.Empty;
        private String RFC_Empleado = String.Empty;
        private String Nombre_Empleado = String.Empty;
        private String Nombre = String.Empty;
        private String Tipo = String.Empty;
        private String Parent = String.Empty;
        private Int32 No_Servicio = (-1);
        private String Clave_UR = String.Empty;
        private String Nombre_UR = String.Empty;
        private String Clave = String.Empty;
        private String Descripcion = String.Empty;
        private String Capitulo_ID = String.Empty;
        private String Concepto_ID = String.Empty;
        private String Partida_Generica_ID = String.Empty;
        private String Partida_Especifica_ID = String.Empty;
        private String Campos_Dinamicos = String.Empty;
        private String Campos_Foraneos = String.Empty;
        private String Join = String.Empty;
        private String Tabla_Consultar = String.Empty;
        private String Filtros_Dinamicos = String.Empty;
        private String Ordenar_Dinamico = String.Empty;
        private Int32 No_Solicitud = (-1);
        private String Folio_Solicitud = String.Empty;
        private DateTime Fecha_Elaboracion = new DateTime();
        private String Tipo_Servicio = String.Empty;
        private String Dependencia_ID = String.Empty;
        private String Vehiculo_ID = String.Empty;
        private String Descripcion_Servicio = String.Empty;
        private String Estatus = String.Empty;
        private String Empleado_Solicito_ID = String.Empty;
        private String Usuario = String.Empty;
        private DateTime Busq_Fecha_Elaboracion_Inicial = new DateTime();
        private DateTime Busq_Fecha_Elaboracion_Final = new DateTime();
        private String Empleado_Autorizo_ID = String.Empty;
        private String Motivo_Rechazo = String.Empty;
        private DateTime Fecha_Recepcion_Programada = new DateTime();
        private DateTime Fecha_Recepcion_Real = new DateTime();
        private Double Kilometraje = -1.0;
        private String Procedencia = String.Empty;
        private String Correo_Electronico = String.Empty;
        private Int32 No_Inventario = (-1);
        private String No_Economico = String.Empty;
        private DataTable Dt_Seguimiento = new DataTable();
        private DateTime Busq_Fecha_Recepcion_Inicial = new DateTime();
        private DateTime Busq_Fecha_Recepcion_Final = new DateTime();
        #endregion

        #region "Variables Publicas"
        public Int32 P_No_Solicitud
        {
            set { No_Solicitud = value; }
            get { return No_Solicitud; }
        }
        public String P_Folio_Solicitud
        {
            set { Folio_Solicitud = value; }
            get { return Folio_Solicitud; }
        }
        public DateTime P_Fecha_Elaboracion
        {
            set { Fecha_Elaboracion = value; }
            get { return Fecha_Elaboracion; }
        }
        public String P_Tipo_Servicio
        {
            set { Tipo_Servicio = value; }
            get { return Tipo_Servicio; }
        }        
        
        public String P_Descripcion_Servicio
        {
            set { Descripcion_Servicio = value; }
            get { return Descripcion_Servicio; }
        }
        
        public String P_Empleado_Solicito_ID
        {
            set { Empleado_Solicito_ID = value; }
            get { return Empleado_Solicito_ID; }
        }
        public String P_Usuario
        {
            set { Usuario = value; }
            get { return Usuario; }
        }
        public DateTime P_Busq_Fecha_Elaboracion_Inicial
        {
            set { Busq_Fecha_Elaboracion_Inicial = value; }
            get { return Busq_Fecha_Elaboracion_Inicial; }
        }
        public DateTime P_Busq_Fecha_Elaboracion_Final
        {
            set { Busq_Fecha_Elaboracion_Final = value; }
            get { return Busq_Fecha_Elaboracion_Final; }
        }
        public String P_Empleado_Autorizo_ID
        {
            set { Empleado_Autorizo_ID = value; }
            get { return Empleado_Autorizo_ID; }
        }
        public String P_Motivo_Rechazo
        {
            set { Motivo_Rechazo = value; }
            get { return Motivo_Rechazo; }
        }
        public DateTime P_Fecha_Recepcion_Programada
        {
            set { Fecha_Recepcion_Programada = value; }
            get { return Fecha_Recepcion_Programada; }
        }
        public DateTime P_Fecha_Recepcion_Real
        {
            set { Fecha_Recepcion_Real = value; }
            get { return Fecha_Recepcion_Real; }
        }
        public Double P_Kilometraje
        {
            get { return Kilometraje; }
            set { Kilometraje = value; }
        }
        public String P_Procedencia
        {
            set { Procedencia = value; }
            get { return Procedencia; }
        }
        public String P_Correo_Electronico
        {
            set { Correo_Electronico = value; }
            get { return Correo_Electronico; }
        }
        public Int32 P_No_Inventario
        {
            set { No_Inventario = value; }
            get { return No_Inventario; }
        }
        public String P_No_Economico
        {
            set { No_Economico = value; }
            get { return No_Economico; }
        }
        public DataTable P_Dt_Seguimiento
        {
            set { Dt_Seguimiento = value; }
            get { return Dt_Seguimiento; }
        }
        public DateTime P_Busq_Fecha_Recepcion_Inicial
        {
            set { Busq_Fecha_Recepcion_Inicial = value; }
            get { return Busq_Fecha_Recepcion_Inicial; }
        }
        public DateTime P_Busq_Fecha_Recepcion_Final
        {
            set { Busq_Fecha_Recepcion_Final = value; }
            get { return Busq_Fecha_Recepcion_Final; }
        }
        public String P_Estatus
        {
            set { Estatus = value; }
            get { return Estatus; }
        }
        public String P_Vehiculo_ID
        {
            set { Vehiculo_ID = value; }
            get { return Vehiculo_ID; }
        }        
        public String P_Dependencia_ID
        {
            set { Dependencia_ID = value; }
            get { return Dependencia_ID; }
        }
        public String P_Empleado_ID
        {
            set { Empleado_ID = value; }
            get { return Empleado_ID; }
        }
        public String P_No_Empleado
        {
            set { No_Empleado = value; }
            get { return No_Empleado; }
        }
        public String P_RFC_Empleado
        {
            set { RFC_Empleado = value; }
            get { return RFC_Empleado; }
        }
        public String P_Nombre_Empleado
        {
            set { Nombre_Empleado = value; }
            get { return Nombre_Empleado; }
        }
        public String P_Nombre
        {
            set { Nombre = value; }
            get { return Nombre; }
        }
        public String P_Tipo
        {
            set { Tipo = value; }
            get { return Tipo; }
        }
        public String P_Parent
        {
            set { Parent = value; }
            get { return Parent; }
        }
        public Int32 P_No_Servicio
        {
            set { No_Servicio = value; }
            get { return No_Servicio; }
        }
        public String P_Clave_UR
        {
            set { Clave_UR = value; }
            get { return Clave_UR; }
        }
        public String P_Nombre_UR
        {
            set { Nombre_UR = value; }
            get { return Nombre_UR; }
        }
        public String P_Clave
        {
            set { Clave = value; }
            get { return Clave; }
        }
        public String P_Descripcion
        {
            set { Descripcion = value; }
            get { return Descripcion; }
        }
        public String P_Capitulo_ID
        {
            set { Capitulo_ID = value; }
            get { return Capitulo_ID; }
        }
        public String P_Concepto_ID
        {
            set { Concepto_ID = value; }
            get { return Concepto_ID; }
        }
        public String P_Partida_Generica_ID
        {
            set { Partida_Generica_ID = value; }
            get { return Partida_Generica_ID; }
        }
        public String P_Partida_Especifica_ID
        {
            set { Partida_Especifica_ID = value; }
            get { return Partida_Especifica_ID; }
        }
        public String P_Campos_Dinamicos
        {
            set { Campos_Dinamicos = value; }
            get { return Campos_Dinamicos; }
        }
        public String P_Campos_Foraneos
        {
            set { Campos_Foraneos = value; }
            get { return Campos_Foraneos; }
        }
        public String P_Join
        {
            set { Join = value; }
            get { return Join; }
        }
        public String P_Tabla_Consultar
        {
            set { Tabla_Consultar = value; }
            get { return Tabla_Consultar; }
        }
        public String P_Filtros_Dinamicos
        {
            set { Filtros_Dinamicos = value; }
            get { return Filtros_Dinamicos; }
        }
        public String P_Ordenar_Dinamico
        {
            set { Ordenar_Dinamico = value; }
            get { return Ordenar_Dinamico; }
        }
        public String P_Unir_Tablas
        {
            set { Unir_Tablas = value; }
            get { return Unir_Tablas; }
        }        
        #endregion

        #region Metodos
        public DataTable Consultar_Datos_Reporte() 
        {
                return Cls_Ope_Tal_Reporte_Refacciones_Datos.Consultar_Datos_Reporte(this);
            }
        
        #endregion
    }
}