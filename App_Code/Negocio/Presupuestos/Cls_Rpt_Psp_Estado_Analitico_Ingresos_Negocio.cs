﻿using System;
using System.Data;
using JAPAMI.Rpt_Psp_Estado_Analitico_Ingresos.Datos;

namespace JAPAMI.Rpt_Psp_Estado_Analitico_Ingresos.Negocio
{
    public class Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio
    {
        #region(Variables Privadas)
            private String Anio;
            private String Dependencia_ID;
            private String Programa_ID;
            private String Area_Funcional_ID;
            private String Fte_Financiamiento_ID;
            private String Partida_ID;
            private String Estatus;
            private String Acumulado_Mes;
            private String Importe_Mes;
        #endregion

        #region(Variables Publicas)
            //get y set de P_Anio
            public String P_Anio
            {
                get { return Anio; }
                set { Anio = value; }
            }

            //get y set de P_Estatus
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }

            //get y set de P_Dependencia_ID
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }

            //get y set de P_Programa_ID
            public String P_Programa_ID
            {
                get { return Programa_ID; }
                set { Programa_ID = value; }
            }

            //get y set de P_Area_Funcional_ID
            public String P_Area_Funcional_ID
            {
                get { return Area_Funcional_ID; }
                set { Area_Funcional_ID = value; }
            }

            //get y set de P_Fte_Financiamiento_ID
            public String P_Fte_Financiamiento_ID
            {
                get { return Fte_Financiamiento_ID; }
                set { Fte_Financiamiento_ID = value; }
            }

            //get y set de P_Partida_ID
            public String P_Partida_ID
            {
                get { return Partida_ID; }
                set { Partida_ID = value; }
            }

            //get y set de P_Acumulado_Mes
            public String P_Acumulado_Mes
            {
                get { return Acumulado_Mes; }
                set { Acumulado_Mes = value; }
            }

            //get y set de P_Importe_Mes
            public String P_Importe_Mes
            {
                get { return Importe_Mes; }
                set { Importe_Mes = value; }
            }
        #endregion

        #region Metodos_Aprobado
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Anios
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los años
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 02/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Anios()
            {
                return Cls_Rpt_Psp_Estado_Analitico_Ingresos_Datos.Consultar_Anios();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_FF
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_FF()
            {
                return Cls_Rpt_Psp_Estado_Analitico_Ingresos_Datos.Consultar_FF();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Pronostico_Ingresos
            ///DESCRIPCIÓN          : Metodo para obtener los datos del pronostico de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 02/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Pronostico_Ingresos()
            {
                return Cls_Rpt_Psp_Estado_Analitico_Ingresos_Datos.Consultar_Pronostico_Ingresos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_FF
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_FF_Presupuesto()
            {
                return Cls_Rpt_Psp_Estado_Analitico_Ingresos_Datos.Consultar_FF_Presupuestos();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Pronostico_Ingresos
            ///DESCRIPCIÓN          : Metodo para obtener los datos del pronostico de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 02/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Presupuesto_Ingresos()
            {
                return Cls_Rpt_Psp_Estado_Analitico_Ingresos_Datos.Consultar_Presupuesto_Ingresos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Mod_Ingresos
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las modificaciones del presupuesto de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 02/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Mod_Ingresos()
            {
                return Cls_Rpt_Psp_Estado_Analitico_Ingresos_Datos.Consultar_Mov_Ingresos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Resumen_Presupuesto_Ingresos
            ///DESCRIPCIÓN          : Metodo para obtener los datos del pronostico de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Armando Zavala Moreno
            ///FECHA_CREO           : 07/Agosto/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Resumen_Presupuesto_Ingresos()
            {
                return Cls_Rpt_Psp_Estado_Analitico_Ingresos_Datos.Consultar_Resumen_Presupuesto_Ingresos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Resumen_Presupuesto_Ingresos
            ///DESCRIPCIÓN          : Metodo para obtener los datos del pronostico de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Ramón Baeza Yépez
            ///FECHA_CREO           : 11/Junio/2013
            ///*********************************************************************************************************
            public DataTable Consultar_Estado()
            {
                return Cls_Rpt_Psp_Estado_Analitico_Ingresos_Datos.Consultar_Estado(this);
            }
        #endregion
    }
}
