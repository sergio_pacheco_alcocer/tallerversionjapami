﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Tipo_Presupuesto.Datos;

namespace JAPAMI.Tipo_Presupuesto.Negocio
{
    public class Cls_Ope_Psp_Tipo_Presupuesto_Negocio
    {
        #region VARIABLES INTERNAS
            private String Anio;
            private String Tipo_Presupuesto;
            private String Usuario_Creo;
        #endregion

        #region VARIABLES PUBLICAS

            //get y set de P_Anio
            public String P_Anio
            {
                get { return Anio; }
                set { Anio = value; }
            }

            //get y set de P_Tipo_Presupuesto
            public String P_Tipo_Presupuesto
            {
                get { return Tipo_Presupuesto; }
                set { Tipo_Presupuesto = value; }
            }

            //get y set de P_Usuario_Creo
            public String P_Usuario_Creo
            {
                get { return Usuario_Creo; }
                set { Usuario_Creo = value; }
            }
        #endregion

        #region MÉTODOS
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Anio
            ///DESCRIPCIÓN          : Metodo para obtener los datos del año activo
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Febrero/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Anio()
        {
            return Cls_Ope_Psp_Tipo_Presupuesto_Datos.Consultar_Anio();
        }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Parametros
            ///DESCRIPCIÓN          : Metodo para dar de alta los parametros del ejercer del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Febrero/2012 
            ///*********************************************************************************************************
            public Boolean  Alta_Parametros()
            {
                return Cls_Ope_Psp_Tipo_Presupuesto_Datos.Guardar_Parametros(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Parametros
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los parametros
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Febrero/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Parametros()
            {
                return Cls_Ope_Psp_Tipo_Presupuesto_Datos.Consultar_Parametros();
            }

        #endregion
    }
}
