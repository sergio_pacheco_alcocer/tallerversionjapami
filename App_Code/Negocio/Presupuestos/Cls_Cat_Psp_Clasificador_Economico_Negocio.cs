﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Clasificador_Economico.Datos;

namespace JAPAMI.Clasificador_Economico.Negocio
{
    public class Cls_Cat_Psp_Clasificador_Economico_Negocio
    {
        public Cls_Cat_Psp_Clasificador_Economico_Negocio()
        {
        }

        #region Variables Locales
            private String CE_ID;
            private String Clave;
            private String Nombre;
            private String Descripcion;      
        #endregion

        #region Variables Publicas
            public String P_CE_ID
            {
                get { return CE_ID; }
                set { CE_ID = value; }
            }    
            public String P_Clave 
            {
                get { return Clave; }
                set { Clave = value; }
            }
            public String P_Nombre
            {
                get { return Nombre; }
                set { Nombre = value; }
            }
            public String P_Descripcion
            {
                get { return Descripcion; }
                set { Descripcion = value; }
            }            
        #endregion

        #region Metodos
        
            public DataTable Consultar_Fechas_Pago()
            {
                return Cls_Cat_Psp_Clasificador_Economico_Datos.Consultar_Clasificador(this);
            }            
            public String Agregar()
            {
                return Cls_Cat_Psp_Clasificador_Economico_Datos.Agregar(this);
            }            
            public String Actualizar()
            {
                return Cls_Cat_Psp_Clasificador_Economico_Datos.Actualizar(this);
            }
            public String Eliminar()
            {
                return Cls_Cat_Psp_Clasificador_Economico_Datos.Eliminar(this);
            }
            public DataTable Validar_Clave()
            {
                return Cls_Cat_Psp_Clasificador_Economico_Datos.Validar_Clave(this);
            }

        #endregion
    }
}
