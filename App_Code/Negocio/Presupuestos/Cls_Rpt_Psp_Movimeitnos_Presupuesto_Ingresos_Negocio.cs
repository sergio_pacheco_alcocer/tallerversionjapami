﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Rpt_Psp_Movimeitnos_Presupuesto_Ingresos.Datos;

namespace JAPAMI.Rpt_Psp_Movimeitnos_Presupuesto_Ingresos.Negocio
{
    public class Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Negocio
    {
        #region(Variables Privadas)
            private String Anio;
            private String No_Movimiento_Ing;
            private String Fte_Financiamiento_ID;
            private String Estatus;
            private String Rubro_ID;
            private String Tipo_ID;
            private String Clase_ID;
            private String Concepto_ID;
            private String SubConcepto_ID;
        #endregion

        #region(Variables Publicas)
            //get y set de P_Anio
            public String P_Anio
            {
                get { return Anio; }
                set { Anio = value; }
            }

            //get y set de P_Estatus
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }

            //get y set de P_No_Movimiento_Ing
            public String P_No_Movimiento_Ing
            {
                get { return No_Movimiento_Ing; }
                set { No_Movimiento_Ing = value; }
            }

            //get y set de P_Fte_Financiamiento_ID
            public String P_Fte_Financiamiento_ID
            {
                get { return Fte_Financiamiento_ID; }
                set { Fte_Financiamiento_ID = value; }
            }

            //get y set de P_Rubro_ID
            public String P_Rubro_ID
            {
                get { return Rubro_ID; }
                set { Rubro_ID = value; }
            }

            //get y set de P_Tipo_ID
            public String P_Tipo_ID
            {
                get { return Tipo_ID; }
                set { Tipo_ID = value; }
            }

            //get y set de P_Clase_ID
            public String P_Clase_ID
            {
                get { return Clase_ID; }
                set { Clase_ID = value; }
            }

            //get y set de P_Concepto_ID
            public String P_Concepto_ID
            {
                get { return Concepto_ID; }
                set { Concepto_ID = value; }
            }

            //get y set de P_SubConcepto_ID
            public String P_SubConcepto_ID
            {
                get { return SubConcepto_ID; }
                set { SubConcepto_ID = value; }
            }
        #endregion

        #region Metodos_Aprobado
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Anios
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los años
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Anios()
            {
                return Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Datos.Consultar_Anios();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_No_Modificacion
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las modificaciones de los años
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_No_Modificacion()
            {
                return Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Datos.Consultar_No_Modificacion(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Movimientos_Autorizado
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las modificaciones de los años
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Movimientos_Autorizado()
            {
                return Cls_Rpt_Psp_Movimeitnos_Presupuesto_Ingresos_Datos.Consultar_Movimientos_Autorizado(this);
            }
        #endregion
    }
}
