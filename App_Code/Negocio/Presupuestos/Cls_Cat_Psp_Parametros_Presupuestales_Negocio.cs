﻿using System;
using System.Data;
using JAPAMI.Parametros_Presupuestales.Datos;

namespace JAPAMI.Parametros_Presupuestales.Negocio
{
    public class Cls_Cat_Psp_Parametros_Presupuestales_Negocio
    {
        #region(Variables)
            public String P_Fte_Financiamiento_ID_Ingresos { get; set; }
            public String P_Programa_ID_Ingresos { get; set; }
            public String P_Fte_Financiamiento_ID_Inversiones { get; set; }
            public String P_Programa_ID_Inversiones { get; set; }
            public String P_Cta_Orden_Ing_Estimado { get; set; }
            public String P_Cta_Orden_Ing_Ampliacion { get; set; }
            public String P_Cta_Orden_Ing_Reduccion { get; set; }
            public String P_Cta_Orden_Ing_Modificado { get; set; }
            public String P_Cta_Orden_Ing_Por_Recaudar { get; set; }
            public String P_Cta_Orden_Ing_Devengado { get; set; }
            public String P_Cta_Orden_Ing_Recaudado { get; set; }
            public String P_Cta_Orden_Egr_Aprobado { get; set; }
            public String P_Cta_Orden_Egr_Ampliacion { get; set; }
            public String P_Cta_Orden_Egr_Reduccion { get; set; }
            public String P_Cta_Orden_Egr_Ampliacion_Interna { get; set; }
            public String P_Cta_Orden_Egr_Reduccion_Interna { get; set; }
            public String P_Cta_Orden_Egr_Modificado { get; set; }
            public String P_Cta_Orden_Egr_Por_Ejercer { get; set; }
            public String P_Cta_Orden_Egr_Comprometido { get; set; }
            public String P_Cta_Orden_Egr_Devengado { get; set; }
            public String P_Cta_Orden_Egr_Ejercido { get; set; }
            public String P_Cta_Orden_Egr_Pagado { get; set; }
            public String P_Aplicar_Disponible_Acumulado { get; set; }
            public String P_Tipo_Poliza_ID_Psp_Ing { get; set; }
            public String P_Tipo_Poliza_ID_Psp_Egr { get; set; }
            public String P_Usuario_Modifico { get; set; }
            public String P_Clasificacion_Administrativa { get; set; }
        #endregion

        #region(Metodos)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Parametros_Presupuestales
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los parametros de presupuestos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Marzo/2013 01:45 PM
            ///*********************************************************************************************************
            public DataTable Consultar_Parametros_Presupuestales()
            {
                return Cls_Cat_Psp_Parametros_Presupuestales_Datos.Consultar_Parametros_Presupuestales();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Parametros_Presupuestales
            ///DESCRIPCIÓN          : Metodo para modificar los datos de los parametros de presupuestos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Marzo/2013 04:27 PM 
            ///*********************************************************************************************************
            public Boolean Modificar_Parametros_Presupuestales()
            {
                return Cls_Cat_Psp_Parametros_Presupuestales_Datos.Modificar_Parametros_Presupuestales(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Cuentas_Contables
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las cuentas contables que inician con 8
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Marzo/2013 04:28  PM
            ///*********************************************************************************************************
            public DataTable Consultar_Cuentas_Contables()
            {
                return Cls_Cat_Psp_Parametros_Presupuestales_Datos.Consultar_Cuentas_Contables();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Fuente_Financiamiento
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Marzo/2013 04:47  PM
            ///*********************************************************************************************************
            public DataTable Consultar_Fuente_Financiamiento()
            {
                return Cls_Cat_Psp_Parametros_Presupuestales_Datos.Consultar_Fuente_Financiamiento();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Programas
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Marzo/2013 04:48  PM
            ///*********************************************************************************************************
            public DataTable Consultar_Programas()
            {
                return Cls_Cat_Psp_Parametros_Presupuestales_Datos.Consultar_Programas();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Tipo_Poliza
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los tipos de polizas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 02/Abril/2013 10:00 AM
            ///*********************************************************************************************************
            public DataTable Consultar_Tipo_Poliza()
            {
                return Cls_Cat_Psp_Parametros_Presupuestales_Datos.Consultar_Tipo_Poliza();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Clasificacion_Administrativa
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las clasificaciones administrativas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 24/Mayo/2013
            ///*********************************************************************************************************
            public DataTable Consultar_Clasificacion_Administrativa()
            {
                return Cls_Cat_Psp_Parametros_Presupuestales_Datos.Consultar_Clasificacion_Administrativa();
            }
        #endregion
    }
}
