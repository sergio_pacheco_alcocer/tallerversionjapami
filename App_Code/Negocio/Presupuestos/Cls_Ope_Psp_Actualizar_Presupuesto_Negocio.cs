﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Actualizar_Presupuesto.Datos;

/// <summary>
/// Summary description for Cls_Ope_Psp_Actualizar_Presupuesto_Negocio
/// </summary>
/// 
namespace JAPAMI.Actualizar_Presupuesto.Negocio
{
    public class Cls_Ope_Psp_Actualizar_Presupuesto_Negocio
    {

        ///*******************************************************************************
        /// VARIABLES INTERNAS 
        ///******************************************************************************
        #region Variables_Internas
        private DataTable Dt_Presupuesto;


        #endregion

        ///*******************************************************************************
        /// VARIABLES PUBLICAS
        ///*******************************************************************************
        #region Variables_Publicas
        
        public DataTable P_Dt_Presupuesto
        {
            get { return Dt_Presupuesto; }
            set { Dt_Presupuesto = value; }
        }

        #endregion
        ///*******************************************************************************
        /// METODOS
        ///*******************************************************************************
        #region Metodos

        public  DataTable Consultar_Fte_Financiamiento()
        {
            return Cls_Ope_Psp_Actualizar_Presupuesto_Datos.Consultar_Fte_Financiamiento();
        }

        public DataTable Consultar_Areas_Funcionales()
        {
            return Cls_Ope_Psp_Actualizar_Presupuesto_Datos.Consultar_Areas_Funcionales();
        }
        public DataTable Consultar_Programas()
        {
            return Cls_Ope_Psp_Actualizar_Presupuesto_Datos.Consultar_Programas();
        }

        public DataTable Consultar_Unidad_Responsable()
        {
            return Cls_Ope_Psp_Actualizar_Presupuesto_Datos.Consultar_Unidad_Responsable();
        }

        public DataTable Consultar_Partidas()
        {
            return Cls_Ope_Psp_Actualizar_Presupuesto_Datos.Consultar_Partidas();
        }

        public String Actualizar_Presupuesto()
        {
            return Cls_Ope_Psp_Actualizar_Presupuesto_Datos.Actualizar_Presupuesto(this);
        }

        #endregion
    }
}