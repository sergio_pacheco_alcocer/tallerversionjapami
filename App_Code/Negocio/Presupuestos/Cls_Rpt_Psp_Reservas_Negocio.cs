﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Reporte_Reservas.Datos;

/// <summary>
/// Summary description for Cls_Rpt_Psp_Reservas_Negocio
/// </summary>
namespace JAPAMI.Reporte_Reservas.Negocio
{
    public class Cls_Rpt_Psp_Reservas_Negocio
    {
        #region Variables Privadas
        private String Anio;
        private String Unidad_Responsable;
        private String Partida;
        private String Fecha_Inicial;
        private String Fecha_Final;
      
        #endregion

        #region Variables publicas

        public String P_Anio
        {
            get { return Anio; }
            set { Anio = value; }
        }

        public String P_Unidad_Responsable
        {
            get { return Unidad_Responsable; }
            set { Unidad_Responsable = value; }
        }

        public String P_Partida
        {
            get { return Partida; }
            set { Partida = value; }
        }

        public String P_Fecha_Inicial
        {
            get { return Fecha_Inicial; }
            set { Fecha_Inicial = value; }
        }

        public String P_Fecha_Final
        {
            get { return Fecha_Final; }
            set { Fecha_Final = value; }
        }
        #endregion

        #region Metodos

        public DataTable Consultar_Anio()
        {
            return Cls_Rpt_Psp_Reservas_Datos.consultar_Anio();
        }

        public DataTable Consultar_Unidad_Reponsable()
        {
            return Cls_Rpt_Psp_Reservas_Datos.Consultar_Unidad_Responsable(this);
        }

        public DataTable Consultar_Partida()
        {
            return Cls_Rpt_Psp_Reservas_Datos.Consultar_Partida(this);
        }

        public DataTable Consulta_Reservas()
        {
            return Cls_Rpt_Psp_Reservas_Datos.Consulta_Reservas(this);
        }

        public DataTable Consultar_Movimiento_Reserva(int no_requisicion) {
            return Cls_Rpt_Psp_Reservas_Datos.Consultar_Historial_Reservas(no_requisicion);
        }

        public DataTable Consulta_Reservas_Para_Reporte()
        {
            return Cls_Rpt_Psp_Reservas_Datos.Consulta_Reservas_Para_Reporte(this);
        }

        #endregion
    }
}