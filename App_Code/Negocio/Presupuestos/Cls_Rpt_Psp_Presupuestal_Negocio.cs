﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Rpt_Psp_Presupuestal.Datos;

namespace JAPAMI.Rpt_Psp_Presupuestal.Negocio
{
    public class Cls_Rpt_Psp_Presupuestal_Negocio
    {
        #region(Variables Privadas)
            private String Anio;
            private String Dependencia_ID;
            private String Programa_ID;
            private String Area_Funcional_ID;
            private String Fte_Financiamiento_ID;
            private String Partida_ID;
        #endregion

        #region(Variables Publicas)
            //get y set de P_Anio
            public String P_Anio
            {
                get { return Anio; }
                set { Anio = value; }
            }

            //get y set de P_Dependencia_ID
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }

            //get y set de P_Programa_ID
            public String P_Programa_ID
            {
                get { return Programa_ID; }
                set { Programa_ID = value; }
            }

            //get y set de P_Area_Funcional_ID
            public String P_Area_Funcional_ID
            {
                get { return Area_Funcional_ID; }
                set { Area_Funcional_ID = value; }
            }

            //get y set de P_Fte_Financiamiento_ID
            public String P_Fte_Financiamiento_ID
            {
                get { return Fte_Financiamiento_ID; }
                set { Fte_Financiamiento_ID = value; }
            }

            //get y set de P_Partida_ID
            public String P_Partida_ID
            {
                get { return Partida_ID; }
                set { Partida_ID = value; }
            }
        #endregion

        #region Metodos_Aprobado
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Anios
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los años
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 24/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Anios()
            {
                return Cls_Rpt_Psp_Presupuestal_Datos.Consultar_Anios();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Analitico_Ingresos
            ///DESCRIPCIÓN          : Metodo para obtener los datos del Estado Analitico de Ingresos Presupuestarios
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 24/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Analitico_Ingresos()
            {
                return Cls_Rpt_Psp_Presupuestal_Datos.Consultar_Analitico_Ingresos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Analitico_Ejercido_Psp_Egresos
            ///DESCRIPCIÓN          : Metodo para obtener los datos del Estado Analitico de Ingresos Presupuestarios
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 24/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Analitico_Ejercido_Psp_Egresos()
            {
                return Cls_Rpt_Psp_Presupuestal_Datos.Consultar_Analitico_Ejercido_Psp_Egresos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Analitico_Ejercido_Psp_Egresos_COG
            ///DESCRIPCIÓN          : Metodo para obtener los datos del Estado Analitico de Ingresos Presupuestarios
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 24/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Analitico_Ejercido_Psp_Egresos_COG()
            {
                return Cls_Rpt_Psp_Presupuestal_Datos.Consultar_Analitico_Ejercido_Psp_Egresos_COG(this);
            }
        #endregion

    }
}
