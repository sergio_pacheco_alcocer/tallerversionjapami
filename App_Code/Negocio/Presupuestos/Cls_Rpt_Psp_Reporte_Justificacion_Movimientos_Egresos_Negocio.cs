﻿using System;
using System.Data;
using JAPAMI.Reporte_Justificacion_Movimientos_Egresos.Datos;

/// <summary>
/// Summary description for Cls_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos_Negocio
/// </summary>
namespace JAPAMI.Reporte_Justificacion_Movimientos_Egresos.Negocio
{
    public class Cls_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos_Negocio
    {
        #region (Variables)
            public String No_Movimiento { get; set; }
            public String Anio { get; set; }
        #endregion

        #region (Metodos)
            ///****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Reporte_Mov_Egresos
            ///DESCRIPCIÓN          : Metodo para generar el reporte de movimientos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 31/Julio/2013
            ///*****************************************************************************************************************
            public DataTable Reporte_Mov_Egresos() 
            {
                return Cls_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos_Datos.Reporte_Mov_Egresos(this);
            }

            ///****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Reporte_Mov_Ingresos
            ///DESCRIPCIÓN          : Metodo para generar el reporte de movimientos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 01/Agosto/2013
            ///*****************************************************************************************************************
            public DataTable Reporte_Mov_Ingresos()
            {
                return Cls_Rpt_Psp_Reporte_Justificacion_Movimientos_Egresos_Datos.Reporte_Mov_Ingresos(this);
            }
        #endregion
    }
}
