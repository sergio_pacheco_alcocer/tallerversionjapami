﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Layout_Cuentas_Contables.Datos;

namespace JAPAMI.Layout_Cuentas_Contables.Negocios
{
    public class Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio
    {
        #region VARIABLES INTERNAS
        private DataTable Dt_Presupuesto;
        private String Anio;
        #endregion

        #region VARIABLES PUBLICAS

        //get y set de P_Tipo_ID
        public DataTable P_Dt_Presupuesto
        {
            get { return Dt_Presupuesto; }
            set { Dt_Presupuesto = value; }
        }
        public String P_Anio
        {
            get { return Anio; }
            set { Anio = value; }
        }
        #endregion

        #region Metodos
        public DataTable Consultar_Fuente_Financiamiento()
        {
            return Cls_Ope_Psp_Layout_Cuentas_Contables_Datos.Consultar_Fuente_Financiamiento(this);
        }
        public DataTable Consultar_Area_Funcional()
        {
            return Cls_Ope_Psp_Layout_Cuentas_Contables_Datos.Consultar_Area_Funcional(this);
        }
        public DataTable Consultar_Programas()
        {
            return Cls_Ope_Psp_Layout_Cuentas_Contables_Datos.Consultar_Programas(this);
        }
        public DataTable Consultar_Dependencias()
        {
            return Cls_Ope_Psp_Layout_Cuentas_Contables_Datos.Consultar_Dependencias(this);
        }
        public DataTable Consultar_Partidas()
        {
            return Cls_Ope_Psp_Layout_Cuentas_Contables_Datos.Consultar_Partidas(this);
        }
        public DataTable Consultar_Cuentas_Contables()
        {
            return Cls_Ope_Psp_Layout_Cuentas_Contables_Datos.Consultar_Cuentas_Contables(this);
        }
        public DataTable Consultar_Anio()
        {
            return Cls_Ope_Psp_Layout_Cuentas_Contables_Datos.Consultar_Anio(this);
        }
        public void Alta()
        {
            Cls_Ope_Psp_Layout_Cuentas_Contables_Datos.Alta(this);
        }
        public DataTable Obtener_Anio_Presupuestado() 
        {
            return Cls_Ope_Psp_Layout_Cuentas_Contables_Datos.Obtener_Anio_Presupuestado();
        }
        #endregion
    }
}
