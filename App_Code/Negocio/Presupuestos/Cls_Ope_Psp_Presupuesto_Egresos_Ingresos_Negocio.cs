﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Psp_Presupuesto_Egresos_Ingresos.Datos;

namespace JAPAMI.Psp_Presupuesto_Egresos_Ingresos.Negocio
{
    public class Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos
    {
        #region(Variables Privadas)
        //VARIABLES GENERALES
            private String Anio;
            private String Busqueda;
            private String Tipo_Presupuesto;
            private String Periodo_Inicial;
            private String Periodo_Final;
            private String Fecha_Inicial;
            private String Fecha_Final;
            private String Tipo_Usuario;
            private String Empleado_ID;
            private String Tipo_Detalle;
            private String Rol_ID;
            //public String P_Orden { set; get; }
            //public String P_Campo { set; get; }

        //VARIABLES PSP ING
            private String Fte_Financiamiento_Ing;
            private String Programa_Ing_ID;
            private String Rubro_ID;
            private String Tipo_ID;
            private String Clase_Ing_ID;
            private String Concepto_Ing_ID;
            private String SubConcepto_ID;
        //VARIABLES FINALES PSP ING
            private String ProgramaF_Ing_ID;
            private String RubroF_ID;
            private String TipoF_ID;
            private String ClaseF_Ing_ID;
            private String ConceptoF_Ing_ID;
            private String SubConceptoF_ID;

        //VARIABLES PSP EGR
            private String Fte_Financiamiento;
            private String Area_Funcional_ID;
            private String Capitulo_ID;
            private String Concepto_ID;
            private String Partida_Generica_ID;
            private String Partida_ID;
            private String Programa_ID;
            private String Dependencia_ID;
            private String Gpo_Dependencia_ID;
        //VARIABLES FINALES PSP EGR
            private String CapituloF_ID;
            private String ConceptoF_ID;
            private String Partida_GenericaF_ID;
            private String PartidaF_ID;
            private String ProgramaF_ID;
            private String DependenciaF_ID;
            private String Gpo_DependenciaF_ID;
        #endregion

        #region(Variables Publicas)
            #region(GENERALES)
                //get y set de P_Anio
                public String P_Anio
                {
                    get { return Anio; }
                    set { Anio = value; }
                }

                //get y set de P_Rol_ID
                public String P_Rol_ID
                {
                    get { return Rol_ID; }
                    set { Rol_ID = value; }
                }

                //get y set de P_Busqueda
                public String P_Busqueda
                {
                    get { return Busqueda; }
                    set { Busqueda = value; }
                }

                //get y set de P_Tipo_Presupuesto
                public String P_Tipo_Presupuesto
                {
                    get { return Tipo_Presupuesto; }
                    set { Tipo_Presupuesto = value; }
                }

                //get y set de P_Fecha_Inicial
                public String P_Fecha_Inicial
                {
                    get { return Fecha_Inicial; }
                    set { Fecha_Inicial = value; }
                }

                //get y set de P_Fecha_Final
                public String P_Fecha_Final
                {
                    get { return Fecha_Final; }
                    set { Fecha_Final = value; }
                }

                //get y set de P_Periodo_Inicial
                public String P_Periodo_Inicial
                {
                    get { return Periodo_Inicial; }
                    set { Periodo_Inicial = value; }
                }

                //get y set de P_Periodo_Final
                public String P_Periodo_Final
                {
                    get { return Periodo_Final; }
                    set { Periodo_Final = value; }
                }

                //get y set de P_Tipo_Usuario
                public String P_Tipo_Usuario
                {
                    get { return Tipo_Usuario; }
                    set { Tipo_Usuario = value; }
                }

                //get y set de P_Empleado_ID
                public String P_Empleado_ID
                {
                    get { return Empleado_ID; }
                    set { Empleado_ID = value; }
                }

                //get y set de P_Tipo_Detalle
                public String P_Tipo_Detalle
                {
                    get { return Tipo_Detalle; }
                    set { Tipo_Detalle = value; }
                }
            #endregion

            #region(PSP ING)
                //get y set de P_Rubro_ID
                public String P_Rubro_ID
                {
                    get { return Rubro_ID; }
                    set { Rubro_ID = value; }
                }

                //get y set de P_Tipo_ID
                public String P_Tipo_ID
                {
                    get { return Tipo_ID; }
                    set { Tipo_ID = value; }
                }

                //get y set de P_Clase_Ing_ID
                public String P_Clase_Ing_ID
                {
                    get { return Clase_Ing_ID; }
                    set { Clase_Ing_ID = value; }
                }

                //get y set de P_Concepto_Ing_ID
                public String P_Concepto_Ing_ID
                {
                    get { return Concepto_Ing_ID; }
                    set { Concepto_Ing_ID = value; }
                }

                //get y set de P_SubConcepto_IDD
                public String P_SubConcepto_ID
                {
                    get { return SubConcepto_ID; }
                    set { SubConcepto_ID = value; }
                }

                //get y set de P_RubroF_ID
                public String P_RubroF_ID
                {
                    get { return RubroF_ID; }
                    set { RubroF_ID = value; }
                }

                //get y set de P_TipoF_ID
                public String P_TipoF_ID
                {
                    get { return TipoF_ID; }
                    set { TipoF_ID = value; }
                }

                //get y set de P_ClaseF_Ing_ID
                public String P_ClaseF_Ing_ID
                {
                    get { return ClaseF_Ing_ID; }
                    set { ClaseF_Ing_ID = value; }
                }

                //get y set de P_ConceptoF_Ing_ID
                public String P_ConceptoF_Ing_ID
                {
                    get { return ConceptoF_Ing_ID; }
                    set { ConceptoF_Ing_ID = value; }
                }

                //get y set de P_SubConceptoF_IDD
                public String P_SubConceptoF_ID
                {
                    get { return SubConceptoF_ID; }
                    set { SubConceptoF_ID = value; }
                }

                //get y set de P_Fte_Financiamiento_Ing
                public String P_Fte_Financiamiento_Ing
                {
                    get { return Fte_Financiamiento_Ing; }
                    set { Fte_Financiamiento_Ing = value; }
                }

                //get y set de P_Programa_Ing_ID
                public String P_Programa_Ing_ID
                {
                    get { return Programa_Ing_ID; }
                    set { Programa_Ing_ID = value; }
                }

                //get y set de P_ProgramaF_Ing_ID
                public String P_ProgramaF_Ing_ID
                {
                    get { return ProgramaF_Ing_ID; }
                    set { ProgramaF_Ing_ID = value; }
                }
            #endregion

            #region(PSP EGR)
                //get y set de P_Fte_Financiamiento
                public String P_Fte_Financiamiento
                {
                    get { return Fte_Financiamiento; }
                    set { Fte_Financiamiento = value; }
                }

                //get y set de P_Capitulo_ID
                public String P_Capitulo_ID
                {
                    get { return Capitulo_ID; }
                    set { Capitulo_ID = value; }
                }

                //get y set de P_Concepto_ID
                public String P_Concepto_ID
                {
                    get { return Concepto_ID; }
                    set { Concepto_ID = value; }
                }

                //get y set de P_Partida_Generica_ID
                public String P_Partida_Generica_ID
                {
                    get { return Partida_Generica_ID; }
                    set { Partida_Generica_ID = value; }
                }

                //get y set de P_Partida_ID
                public String P_Partida_ID
                {
                    get { return Partida_ID; }
                    set { Partida_ID = value; }
                }

                //get y set de P_Programa_ID
                public String P_Programa_ID
                {
                    get { return Programa_ID; }
                    set { Programa_ID = value; }
                }

                //get y set de P_Dependencia_ID
                public String P_Dependencia_ID
                {
                    get { return Dependencia_ID; }
                    set { Dependencia_ID = value; }
                }

                //get y set de P_Gpo_Dependencia_ID
                public String P_Gpo_Dependencia_ID
                {
                    get { return Gpo_Dependencia_ID; }
                    set { Gpo_Dependencia_ID = value; }
                }

                //get y set de P_Area_Funcional_ID
                public String P_Area_Funcional_ID
                {
                    get { return Area_Funcional_ID; }
                    set { Area_Funcional_ID = value; }
                }

                //get y set de P_CapituloF_ID
                public String P_CapituloF_ID
                {
                    get { return CapituloF_ID; }
                    set { CapituloF_ID = value; }
                }

                //get y set de P_ConceptoF_ID
                public String P_ConceptoF_ID
                {
                    get { return ConceptoF_ID; }
                    set { ConceptoF_ID = value; }
                }

                //get y set de P_Partida_GenericaF_ID
                public String P_Partida_GenericaF_ID
                {
                    get { return Partida_GenericaF_ID; }
                    set { Partida_GenericaF_ID = value; }
                }

                //get y set de P_PartidaF_ID
                public String P_PartidaF_ID
                {
                    get { return PartidaF_ID; }
                    set { PartidaF_ID = value; }
                }

                //get y set de P_ProgramaF_ID
                public String P_ProgramaF_ID
                {
                    get { return ProgramaF_ID; }
                    set { ProgramaF_ID = value; }
                }

                //get y set de P_DependenciaF_ID
                public String P_DependenciaF_ID
                {
                    get { return DependenciaF_ID; }
                    set { DependenciaF_ID = value; }
                }

                //get y set de P_Gpo_DependenciaF_ID
                public String P_Gpo_DependenciaF_ID
                {
                    get { return Gpo_DependenciaF_ID; }
                    set { Gpo_DependenciaF_ID = value; }
                }
            #endregion
        #endregion

        #region (Metodos)

            #region (Generales)
                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Anios
                ///DESCRIPCIÓN          : Metodo para obtener los datos de los años
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Abril/2012 
                ///*********************************************************************************************************
                public DataTable Consultar_Anios()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Anios(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_FF
                ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento del presupuesto
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Abril/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_FF()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_FF(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Programa
                ///DESCRIPCIÓN          : Metodo para obtener los programas
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Programa()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Proyectos(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Obtener_Gpo_Dependencia
                ///DESCRIPCIÓN          : Metodo para obtener los datos del grupo dependencia de un empleado
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 18/Junio/2012  
                ///*********************************************************************************************************
                public DataTable Obtener_Gpo_Dependencia()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Gpo_Dependencia(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Rol
                ///DESCRIPCIÓN          : Metodo para obtener si el rol es de empleado o administrativo
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 08/marzo/2012 
                ///*********************************************************************************************************
                public String Consultar_Rol()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Rol(this.P_Rol_ID);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Dependencias_Usuarios
                ///DESCRIPCIÓN          : Metodo para obtener si el empleado pertenece a varias dependencias
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 14/marzo/2013 
                ///*********************************************************************************************************
                public DataTable Consultar_Dependencias_Usuarios()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Dependencias_Usuarios(this);
                }
            #endregion

            #region (Psp Ing)
                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Rubros
                ///DESCRIPCIÓN          : Metodo para obtener los rubros
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Rubros()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Rubros(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos
                ///DESCRIPCIÓN          : Metodo para obtener los tipos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Tipos()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Tipos(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Clases
                ///DESCRIPCIÓN          : Metodo para obtener los Clases
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Clases()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Clases(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos_Ing
                ///DESCRIPCIÓN          : Metodo para obtener los conceptos de ingresos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Conceptos_Ing()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Conceptos_Ing(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_SubConceptos
                ///DESCRIPCIÓN          : Metodo para obtener los subconceptos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_SubConceptos()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_SubConceptos(this);
                }
            #endregion

            #region (Psp Egr)
                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_AF
                ///DESCRIPCIÓN          : Metodo para obtener las areas funcionales del presupuesto
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 17/Agosto/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_AF()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_AF(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Gpo_Dependencia
                ///DESCRIPCIÓN          : Metodo para obtener las los grupos de las dependencias
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 17/Agosto/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Gpo_Dependencia()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Dependencias(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_UR
                ///DESCRIPCIÓN          : Metodo para obtener las dependencias
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_UR()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Dependencias(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Capitulos
                ///DESCRIPCIÓN          : Metodo para obtener los Capitulos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Capitulos()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Capitulo(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos
                ///DESCRIPCIÓN          : Metodo para obtener los conceptos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Conceptos()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Concepto(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas_Gen
                ///DESCRIPCIÓN          : Metodo para obtener las partidas genericas
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Partidas_Gen()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Partida_Generica(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas
                ///DESCRIPCIÓN          : Metodo para obtener las partidas
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Partidas()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Partida(this);
                }
            #endregion

            #region (Nivel Ing)
                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_SubConceptos
                ///DESCRIPCIÓN          : Metodo para obtener los subconceptos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 24/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_SubConceptos()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_SubConceptos(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_Conceptos
                ///DESCRIPCIÓN          : Metodo para obtener los conceptos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 24/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_Conceptos_Ing()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_Conceptos_Ing(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_Clases
                ///DESCRIPCIÓN          : Metodo para obtener las clases
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 24/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_Clases()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_Clases(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_Tipos
                ///DESCRIPCIÓN          : Metodo para obtener los tipos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 24/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_Tipos()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_Tipos(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_Rubros
                ///DESCRIPCIÓN          : Metodo para obtener las rubros
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 24/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_Rubros()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_Rubros(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_FF_Ing
                ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento de ingresos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 24/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_FF_Ing()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_FF_Ing(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_PP_Ing
                ///DESCRIPCIÓN          : Metodo para obtener laor programas de ingresos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 24/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_PP_Ing()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_PP_Ing(this);
                }
            #endregion

            #region (Nivel Egr)
                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_Capitulo
                ///DESCRIPCIÓN          : Metodo para obtener el nivel de capitulos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_Capitulo()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_Capitulos(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_Concepto
                ///DESCRIPCIÓN          : Metodo para obtener el nivel de conceptos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_Concepto()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_Conceptos(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_Partida_Gen
                ///DESCRIPCIÓN          : Metodo para obtener el nivel de partidas genericas
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_Partida_Gen()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_Partida_Gen(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_Partidas
                ///DESCRIPCIÓN          : Metodo para obtener el nivel de partidas
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 25/Mayo/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_Partidas()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_Partida(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_FF
                ///DESCRIPCIÓN          : Metodo para obtener el nivel de fuentes de financiamiento de egresos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Agosto/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_FF()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_FF(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_AF
                ///DESCRIPCIÓN          : Metodo para obtener el nivel de areas funcionales de egresos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Agosto/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_AF()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_AF(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_PP
                ///DESCRIPCIÓN          : Metodo para obtener el nivel de programas de egresos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Agosto/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_PP()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_PP(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_UR
                ///DESCRIPCIÓN          : Metodo para obtener el nivel de unidades responsables de egresos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 23/Agosto/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Nivel_UR()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Nivel_UR(this);
                }
            #endregion

            #region (Det Ing)
                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Det_Modificacion_Ing
                ///DESCRIPCIÓN          : Metodo para obtener los detalles de los movimientos del presupuesto de ingresos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 24/Agosto/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Det_Modificacion_Ing()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Det_Modificacion_Ing(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Det_Recaudado_Ing
                ///DESCRIPCIÓN          : Metodo para obtener los detalles de lo recaudado del presupuesto de ingresos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 28/Agosto/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Det_Recaudado_Ing()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Det_Recaudado_Ing(this);
                }
            #endregion

            #region (Det Egr)
                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Det_Modificacion_Egr
                ///DESCRIPCIÓN          : Metodo para obtener los detalles de los movimientos del presupuesto de egresos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 27/Agosto/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Det_Modificacion_Egr()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Det_Modificacion_Egr(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Det_Movimiento_Contable_Egr_Pre_Comprometido
                ///DESCRIPCIÓN          : Metodo para obtener los detalles de los movimientos contables del presupuesto de 
                ///                     : egresos (precomprometido)
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 29/Agosto/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Det_Movimiento_Contable_Egr_Pre_Comprometido()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Det_Movimiento_Contable_Egr_Pre_Comprometido(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Det_Movimiento_Contable_Egr_Comprometido
                ///DESCRIPCIÓN          : Metodo para obtener los detalles de los movimientos contables del presupuesto de 
                ///                     : egresos (comprometido)
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 29/Agosto/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Det_Movimiento_Contable_Egr_Comprometido()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Det_Movimiento_Contable_Egr_Comprometido(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Det_Movimiento_Contable_Egr
                ///DESCRIPCIÓN          : Metodo para obtener los detalles de los movimientos contables del presupuesto de 
                ///                     : egresos (devengado, pagado y ejercido)
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 16/Agosto/2013  
                ///*********************************************************************************************************
                public DataTable Consultar_Det_Movimiento_Contable_Egr_Dev()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Det_Movimiento_Contable_Egr_Dev(this);
                }
            #endregion

            #region (Reporte)
                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Psp_Egr
                ///DESCRIPCIÓN          : Metodo para obtener el presupuesto de egresos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 30/Agosto/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Psp_Egr()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Psp_Egr(this);
                }

                ///********************************************************************************************************
                ///NOMBRE DE LA FUNCIÓN : Consultar_Psp_Ing
                ///DESCRIPCIÓN          : Metodo para obtener el presupuesto de ingresos
                ///PROPIEDADES          :
                ///CREO                 : Leslie González Vázquez
                ///FECHA_CREO           : 30/Agosto/2012  
                ///*********************************************************************************************************
                public DataTable Consultar_Psp_Ing()
                {
                    return Cls_Ope_Psp_Presupuesto_Datos_Egresos_Ingresos.Consultar_Psp_Ing(this);
                }
            #endregion
        #endregion
    }
}
