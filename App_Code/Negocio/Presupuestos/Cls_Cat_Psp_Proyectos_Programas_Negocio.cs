﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Proyectos_Programas.Datos;

namespace JAPAMI.Proyectos_Programas.Negocio
{
    public class Cls_Cat_Psp_Proyectos_Programas_Negocio
    {
        public Cls_Cat_Psp_Proyectos_Programas_Negocio()
        {
        }

        #region Variables Locales
            private String ID;
            private String Clave;
            private String Nombre;
            private String Estatus;
            private String Area_ID;
            private String Descripcion;      
        #endregion

        #region Variables Publicas
            public String P_ID
            {
                get { return ID; }
                set { ID = value; }
            }    
            public String P_Clave 
            {
                get { return Clave; }
                set { Clave = value; }
            }
            public String P_Nombre
            {
                get { return Nombre; }
                set { Nombre = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Area_ID
            {
                get { return Area_ID; }
                set { Area_ID = value; }
            }
            public String P_Descripcion
            {
                get { return Descripcion; }
                set { Descripcion = value; }
            }            
        #endregion

        #region Metodos

            public DataTable Consultar_Proyectos()
            {
                return Cls_Cat_Psp_Proyectos_Programas_Datos.Consultar_Proyectos(this);
            }            
            public String Agregar()
            {
                return Cls_Cat_Psp_Proyectos_Programas_Datos.Agregar(this);
            }            
            public String Actualizar()
            {
                return Cls_Cat_Psp_Proyectos_Programas_Datos.Actualizar(this);
            }
            public String Eliminar()
            {
                return Cls_Cat_Psp_Proyectos_Programas_Datos.Eliminar(this);
            }
            public DataTable Consultar_Areas()
            {
                return Cls_Cat_Psp_Proyectos_Programas_Datos.Consultar_Areas(this);
            }

        #endregion
    }
}
