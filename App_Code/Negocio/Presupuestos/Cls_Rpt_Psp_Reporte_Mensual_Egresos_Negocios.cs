﻿using System;
using System.Data;
using JAPAMI.Rpt_Psp_Reporte_Mensual_Egresos.Datos;

namespace JAPAMI.Rpt_Psp_Reporte_Mensual_Egresos.Negocios
{
    public class Cls_Rpt_Psp_Reporte_Mensual_Egresos_Negocios
    {
        #region(Variables Privadas)
            String Fte_Financiamiento_ID;
            String Programa_ID;
            String Area_Funcional_ID;
            String Dependencia_ID;
            String Partida_ID;
        #endregion

        #region(Variables Publicas)
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }

            public String P_Area_Funcional_ID
            {
                get { return Area_Funcional_ID; }
                set { Area_Funcional_ID = value; }
            }

            public String P_Programa_ID
            {
                get { return Programa_ID; }
                set { Programa_ID = value; }
            }

            public String P_Partida_ID
            {
                get { return Partida_ID; }
                set { Partida_ID = value; }
            }

            public String P_Fte_Financiamiento_ID
            {
                get { return Fte_Financiamiento_ID; }
                set { Fte_Financiamiento_ID = value; }
            }

        #endregion

        #region (Metodos)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Presupuesto_Mensual
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los tipos de reportes
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Febrero/2013
            ///*********************************************************************************************************
            public DataTable Consultar_Presupuesto_Mensual()
            {
                return Cls_Rpt_Psp_Reporte_Mensual_Egresos_Datos.Consultar_Presupuesto_Mensual(this);
            }
        #endregion
    }
}
