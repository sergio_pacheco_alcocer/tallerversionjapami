﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Reporte_Movimiento_Presupuestal.Datos;

/// <summary>
/// Summary description for Cls_Rpt_Psp_Movimiento_Presupuestal_Negocio
/// </summary>
namespace JAPAMI.Reporte_Movimiento_Presupuestal.Negocio
{
    public class Cls_Rpt_Psp_Movimiento_Presupuestal_Negocio
    {
        public Cls_Rpt_Psp_Movimiento_Presupuestal_Negocio()
        {
        }

        #region (Variables Locales)
        private DateTime Fecha_Inicial;
        private DateTime Fecha_Final;
        private String Tipo_Operacion;
        private String Unidad_Responsable_ID_Origen;
        private String Fuente_Financiamiento_ID_Origen;
        private String Programa_ID_Origen;
        private String Capitulo_ID_Origen;
        private String Partida_ID_Origen;
        private String Unidad_Responsable_ID_Destino;
        private String Fuente_Financiamiento_ID_Destino;
        private String Programa_ID_Destino;
        private String Capitulo_ID_Destino;
        private String Partida_ID_Destino;
        private String Busqueda;
        #endregion

        #region (Variables Publicas)
        public DateTime P_Fecha_Inicial
        {
            get { return Fecha_Inicial; }
            set { Fecha_Inicial = value; }
        }

        public DateTime P_Fecha_Final
        {
            get { return Fecha_Final; }
            set { Fecha_Final = value; }
        }

        public String P_Tipo_Operacion
        {
            get { return Tipo_Operacion; }
            set { Tipo_Operacion = value; }
        }

        public String P_Unidad_Responsable_ID_Origen
        {
            get { return Unidad_Responsable_ID_Origen; }
            set { Unidad_Responsable_ID_Origen = value; }
        }

        public String P_Fuente_Financiamiento_ID_Origen
        {
            get { return Fuente_Financiamiento_ID_Origen; }
            set { Fuente_Financiamiento_ID_Origen = value; }
        }

        public String P_Programa_ID_Origen
        {
            get { return Programa_ID_Origen; }
            set { Programa_ID_Origen = value; }
        }

        public String P_Capitulo_ID_Origen
        {
            get { return Capitulo_ID_Origen; }
            set { Capitulo_ID_Origen = value; }
        }

        public String P_Partida_ID_Origen
        {
            get { return Partida_ID_Origen; }
            set { Partida_ID_Origen = value; }
        }

        public String P_Unidad_Responsable_ID_Destino
        {
            get { return Unidad_Responsable_ID_Destino; }
            set { Unidad_Responsable_ID_Destino = value; }
        }

        public String P_Fuente_Financiamiento_ID_Destino
        {
            get { return Fuente_Financiamiento_ID_Destino; }
            set { Fuente_Financiamiento_ID_Destino = value; }
        }

        public String P_Programa_ID_Destino
        {
            get { return Programa_ID_Destino; }
            set { Programa_ID_Destino = value; }
        }

        public String P_Capitulo_ID_Destino
        {
            get { return Capitulo_ID_Destino; }
            set { Capitulo_ID_Destino = value; }
        }

        public String P_Partida_ID_Destino
        {
            get { return Partida_ID_Destino; }
            set { Partida_ID_Destino = value; }
        }

        public String P_Busqueda
        {
            get { return Busqueda; }
            set { Busqueda = value; }
        }
        #endregion

        #region (Metodos)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:   Consulta_Reporte_Movimiento_Presupuestal
        /// DESCRIPCION :           Consulta para los movimientos presupuestales de acuerdo a diversos criterios
        /// PARAMETROS  :           
        /// CREO        :           Noe Mosqueda Valadez
        /// FECHA_CREO  :           01/Junio/2012 12:40
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public DataTable Consulta_Reporte_Movimiento_Presupuestal()
        {
            return Cls_Rpt_Psp_Movimiento_Presupuestal_Datos.Consulta_Reporte_Movimiento_Presupuestal(this);
        }
        #endregion
    }
}