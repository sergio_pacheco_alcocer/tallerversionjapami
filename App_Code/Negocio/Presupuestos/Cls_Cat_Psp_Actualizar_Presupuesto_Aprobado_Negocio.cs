﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Actualizar_Presupuesto_Aprobado.Datos;

/// <summary>
/// Summary description for Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado
/// </summary>
/// 
namespace JAPAMI.Actualizar_Presupuesto_Aprobado.Negocios
{
    public class Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio
    {
        public Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio()
        {
        }

        #region VARIABLES PRIVADAS
        DataTable Dt_Insertar_Presupuesto;
        DataTable Dt_Actualizar_Presupuesto;
        String Clave_Fuente_Financiamiento;
        String Clave_Dependencia;
        String Clave_Programa;
        String Clave_Partida;
        String Clave_Subnivel;
        String Clave_Capitulo;
        String Anio;
        String Programa_ID;
        String Dependencia_ID;
        String Clave_Area_Funcional;

        #endregion
       
        #region VARIABLES PUBLICAS
        //get y set de Dt_Insertar_Presupuesto
        public DataTable P_Dt_Insertar_Presupuesto 
        {
            get { return Dt_Insertar_Presupuesto; }
            set { Dt_Insertar_Presupuesto = value; }
        }
        //get y set de Dt_Actualizar_Presupuesto
        public DataTable P_Dt_Actualizar_Presupuesto 
        {
            get { return Dt_Actualizar_Presupuesto; }
            set { Dt_Actualizar_Presupuesto = value; }
        }
        //get y set de Clave_Fuente_Financiamiento
        public String P_Clave_Fuente_Financiamiento 
        {
            get { return Clave_Fuente_Financiamiento; }
            set { Clave_Fuente_Financiamiento = value; }
        }
        //get y set de Clave_Dependencia
        public String P_Clave_Dependencia
        {
            get { return Clave_Dependencia; }
            set { Clave_Dependencia = value; }
        }
        //get y set de Clave_Programa
        public String P_Clave_Programa
        {
            get { return Clave_Programa; }
            set { Clave_Programa = value; }
        }
        //get y set de Clave_Partida
        public String P_Clave_Partidaa
        {
            get { return Clave_Partida; }
            set { Clave_Partida = value; }
        }

        
        //get y set Clave_Subnivel
        public String P_Clave_Subnivel
        {
            get { return Clave_Subnivel; }
            set { Clave_Subnivel = value; }
        }
        //get y set de Anio
        public String P_Anio
        {
            get { return Anio; }
            set { Anio = value; }
        }
        //get y set de Programa_ID
        public String P_Programa_ID
        {
            get { return Programa_ID; }
            set { Programa_ID = value; }
        }
        //get y set de Dependencia_ID
        public String P_Dependencia_ID
        {
            get { return Dependencia_ID; }
            set { Dependencia_ID = value; }
        }
        //get y set de Clave_Capitulo
        public String P_Clave_Capitulo
        {
            get { return Clave_Capitulo; }
            set { Clave_Capitulo = value; }
        }
        //get y set de Clave_Area_Funcional
        public String P_Clave_Area_Funcional
        {
            get { return Clave_Area_Funcional; }
            set { Clave_Area_Funcional = value; }
        }
        #endregion

        #region METODOS
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Dependencia
        ///DESCRIPCIÓN          : Metodo para conocer si una dependencia existe
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 17/Septiembre/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Dependencia()
        {
            return Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Datos.Consultar_Dependencia(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Fte_Financiamiento
        ///DESCRIPCIÓN          : Metodo para conocer si una fuente de financiamiento existe
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 17/Septiembre/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Fte_Financiamiento()
        {
            return Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Datos.Consultar_Fte_Financiamiento(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Programa
        ///DESCRIPCIÓN          : Metodo para conocer si un programa existe
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 17/Septiembre/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Programa()
        {
            return Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Datos.Consultar_Programa(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Capitulo
        ///DESCRIPCIÓN          : Metodo para conocer si un capitulo existe
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 17/Septiembre/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Capitulo()
        {
            return Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Datos.Consultar_Capitulo(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partida
        ///DESCRIPCIÓN          : Metodo para conocer si una parida existe
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 17/Septiembre/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Partida()
        {
            return Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Datos.Consultar_Partida(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Relacion_Programa_Dependencia
        ///DESCRIPCIÓN          : Metodo para conocer si una relacion programa-dependencia existe por medio de los id's
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 17/Septiembre/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Relacion_Programa_Dependencia()
        {
            return Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Datos.Consultar_Relacion_Programa_Dependencia(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Relacion_Partida_Capitulo
        ///DESCRIPCIÓN          : Metodo para conocer si una partida pertenece a cierto capitulo
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 17/Septiembre/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Relacion_Partida_Capitulo()
        {
            return Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Datos.Consultar_Relacion_Partida_Capitulo(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Presupuesto_Aprobado
        ///DESCRIPCIÓN          : Metodo que obtiene un registro de la tabla Ope_Psp_Presupuesto_Aprobado 
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 17/Septiembre/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Presupuesto_Aprobado()
        {
            return Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Datos.Consultar_Presupuesto_Aprobado(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Subnivel_Presupuestal
        ///DESCRIPCIÓN          : Metodo que obtiene un registro de la tabla Cat_psp_Subnivel_Presupuestal 
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 17/Septiembre/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Subnivel_Presupuestal()
        {
            return Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Datos.Consultar_Subnivel_Presupuestal(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Presupuesto_Aprobado
        ///DESCRIPCIÓN          : Metodo que  da de alta o actualiza los registros leidos del documento de excel 
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 19/Septiembre/2012 
        ///*********************************************************************************************************
        public String Alta_Presupuesto_Aprobado()
        {
            return Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Datos.Alta_Presupuesto_Aprobado(this);
        }
        #endregion

    }
}
