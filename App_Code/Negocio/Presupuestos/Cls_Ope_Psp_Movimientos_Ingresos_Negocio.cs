﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Ope_Psp_Movimientos_Ingresos.Datos;

namespace JAPAMI.Ope_Psp_Movimientos_Ingresos.Negocio
{
    public class Cls_Ope_Psp_Movimientos_Ingresos_Negocio
    {
        #region(Variables Privadas)
            private String Estatus;
            private String Fecha_Inicio;
            private String Fecha_Fin;
            private String Comentario;
            private String Rubro_ID;
            private String Tipo_ID;
            private String Clase_ID;
            private String SubConcepto_ID;
            private String Anio;
            private String Fte_Financiamiento;
            private String Total_Modificado;
            private DataTable Dt_Anexos;
            private DataTable Dt_Mov_Conceptos;
            private DataTable Dt_Mov_Autorizados;
            private String No_Movimiento_Ing;
            private String Usuario_Creo;
            private String Usuario_Modifico;
            private String Tipo_Solicitud;
            private String Busqueda;
            private String Programa_ID;
        #endregion

        #region(Variables Publicas)
            //get y set de P_Estatus
            public String P_Estatus {get; set;}

            //get y set de P_Fecha_Inicio
            public String P_Fecha_Inicio {get;set;}

            //get y set de P_Fecha_Fin
            public String P_Fecha_Fin
            {
                get { return Fecha_Fin; }
                set { Fecha_Fin = value; }
            }

            //get y set de P_Tipo_Solicitud
            public String P_Tipo_Solicitud
            {
                get { return Tipo_Solicitud; }
                set { Tipo_Solicitud = value; }
            }

            //get y set de P_Total_Modificados
            public String P_Total_Modificado { get; set; }

            //get y set de P_Rubro_ID
            public String P_Rubro_ID
            {
                get { return Rubro_ID; }
                set { Rubro_ID = value; }
            }

            //get y set de P_Tipo_ID
            public String P_Tipo_ID
            {
                get { return Tipo_ID; }
                set { Tipo_ID = value; }
            }

            //get y set de P_Clase_ID
            public String P_Clase_ID
            {
                get { return Clase_ID; }
                set { Clase_ID = value; }
            }

            //get y set de P_Concepto_ID
            public String P_Concepto_ID { get; set; }

            //get y set de P_SubConcepto_ID
            public String P_SubConcepto_ID { get; set;}

            //get y set de P_Programa_ID
            public String P_Programa_ID { get; set; }

            //get y set de P_Fte_Financiamiento
            public String P_Fte_Financiamiento
            {
                get { return Fte_Financiamiento; }
                set { Fte_Financiamiento = value; }
            }
        
            //get y set de P_Anio
            public String P_Anio
            {
                get { return Anio; }
                set { Anio = value; }
            }

            //get y set de P_Usuario_Creo
            public String P_Usuario_Creo
            {
                get { return Usuario_Creo; }
                set { Usuario_Creo = value; }
            }

            //get y set de P_Usuario_Modifico
            public String P_Usuario_Modifico
            {
                get { return Usuario_Modifico; }
                set { Usuario_Modifico = value; }
            }

            //get y set de P_Comentario
            public String P_Comentario
            {
                get { return Comentario; }
                set { Comentario  = value; }
            }

            //get y set de P_Busqueda
            public String P_Busqueda
            {
                get { return Busqueda; }
                set { Busqueda = value; }
            }

            //get y set de P_Dt_Anexos
            public DataTable P_Dt_Anexos { get; set; }

            //get y set de P_Mov_Conceptos
            public DataTable P_Dt_Mov_Conceptos { get; set; }

            //get y set de P_Mov_Conceptos
            public DataTable P_Dt_Mov_Autorizados { get; set; }

            //get y set de P_No_Movimiento_Ing
            public String P_No_Movimiento_Ing { get; set; }

        #endregion

        #region Metodos_Aprobado
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Rubros
            ///DESCRIPCIÓN          : Metodo para obtener los rubros del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Rubros()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Consultar_Rubros(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_FF
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_FF()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Consultar_FF();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Conceptos()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Consultar_Conceptos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Movimientos
            ///DESCRIPCIÓN          : Metodo para obtener los movimientos del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Movimientos()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Consultar_Movimientos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Modificaciones
            ///DESCRIPCIÓN          : Metodo para obtener las midificaciones del presupuesto de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 09/Mayo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Modificaciones()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Consultar_Modificaciones(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Movimientos
            ///DESCRIPCIÓN          : Metodo para dar de alta los movimientos de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Abril/2012 
            ///*********************************************************************************************************
            public String Alta_Movimientos()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Alta_Movimientos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Movimientos
            ///DESCRIPCIÓN          : Metodo para modificar los movimientos de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Abril/2012 
            ///*********************************************************************************************************
            public String Modificar_Movimientos()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Modificar_Movimientos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Conceptos_Ing()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Consultar_Conceptos_Ing(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Actualizar_Movimientos
            ///DESCRIPCIÓN          : Metodo para actualizar los datos de los movimientos de ingresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2012 
            ///*********************************************************************************************************
            public Boolean Actualizar_Movimientos()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Actualizar_Movimientos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Estatus_Movimientos
            ///DESCRIPCIÓN          : Metodo para obtener el estatus de la ultima modificacion
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2012 
            ///*********************************************************************************************************
            public String Consultar_Estatus_Movimientos()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Consultar_Estatus_Movimientos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Mov_Egresos
            ///DESCRIPCIÓN          : Metodo para obtener el estatus de la ultima modificacion de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 07/Junio/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Mov_Egresos()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Consultar_Mov_Egresos();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Estatus_Mov_Ing
            ///DESCRIPCIÓN          : Metodo para obtener el estatus de la ultima modificacion de egresos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 07/Junio/2012 
            ///*********************************************************************************************************
            public String Consultar_Estatus_Mov_Ing()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Consultar_Estatus_Mov_Ing(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Programas
            ///DESCRIPCIÓN          : Metodo para obtener los programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Julio/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Programas()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Consultar_Programas(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Fte_Financiamiento
            ///DESCRIPCIÓN          : Metodo para consultar los datos de las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 13/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consulta_Fte_Financiamiento()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Consulta_Fte_Financiamiento();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Fte_Financiamiento_Ramo_XXXIII
            ///DESCRIPCIÓN          : Metodo para consultar los datos de las fuentes de financiamiento de ramo 33
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Noviembre/2012 
            ///*********************************************************************************************************
            public DataTable Consulta_Fte_Financiamiento_Ramo_XXXIII()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Consulta_Fte_Financiamiento_Ramo_XXXIII();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Movimientos_Ramo33
            ///DESCRIPCIÓN          : Metodo para modificar los movimientos de ingresos de ramo 33
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Noviembre/2012 
            ///*********************************************************************************************************
            public String Alta_Movimientos_Ramo33()
            {
                return Cls_Ope_Psp_Movimientos_Ingresos_Datos.Alta_Movimientos_Ramo33(this);
            }
        #endregion
    }
}
