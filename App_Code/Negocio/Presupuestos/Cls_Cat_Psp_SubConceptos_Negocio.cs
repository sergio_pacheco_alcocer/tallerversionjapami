﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Cls_Cat_Psp_SubConcepto.Datos;

namespace JAPAMI.Cls_Cat_Psp_SubConcepto.Negocio
{
    public class Cls_Cat_Psp_SubConceptos_Negocio
    {
        #region VARIABLES INTERNAS
        private String Tipo_ID;
        private String Clase_Ing_ID;
        private String Concepto_Ing_ID;
        private String SubConcepto_Ing_ID;
        private String Cuenta_Contable_ID;
        private String Rubro_ID;
        private String Anio;
        private String Clave;
        private String Descripcion;
        private String Cantidad_Calculo;
        private String Estatus;
        private String Usuario_Creo;
        private String Usuario_Modifico;
        private String Tipo_Calculo;
        private String Impuesto;
        private String Categoria;
        private String Requiere_IVA;
        private String Importe;

        private String Campos_Dinamicos;
        private String Filtros_Dinamicos;
        private String Agrupar_Dinamico;
        private String Ordenar_Dinamico;
        private Boolean Incluir_Campos_Foraneos;
        private String Join;
        private String Unir_Tablas;
        #endregion

        #region VARIABLES PUBLICAS

        //get y set de P_Tipo_ID
        public String P_Tipo_ID
        {
            get { return Tipo_ID; }
            set { Tipo_ID = value; }
        }

        //get y set de P_Importe
        public String P_Importe
        {
            get { return Importe; }
            set { Importe = value; }
        }

        //get y set de P_Categoria
        public String P_Categoria
        {
            get { return Categoria; }
            set { Categoria = value; }
        }

        //get y set de P_Requiere_IVA
        public String P_Requiere_IVA
        {
            get { return Requiere_IVA; }
            set { Requiere_IVA = value; }
        }

        //get y set de P_Impuesto
        public String P_Impuesto
        {
            get { return Impuesto; }
            set { Impuesto = value; }
        }

        //get y set de P_Tipo_Calculo
        public String P_Tipo_Calculo
        {
            get { return Tipo_Calculo; }
            set { Tipo_Calculo = value; }
        }

        //get y set de P_Clase_Ing_ID
        public String P_Clase_Ing_ID
        {
            get { return Clase_Ing_ID; }
            set { Clase_Ing_ID = value; }
        }

        //get y set de Anio
        public String P_Anio
        {
            get { return Anio; }
            set { Anio = value; }
        }

        //get y set de P_Concepto_Ing_ID
        public String P_Concepto_Ing_ID
        {
            get { return Concepto_Ing_ID; }
            set { Concepto_Ing_ID = value; }
        }

        //get y set de P_SubConcepto_Ing_ID
        public String P_SubConcepto_Ing_ID
        {
            get { return SubConcepto_Ing_ID; }
            set { SubConcepto_Ing_ID = value; }
        }

        public String P_Cuenta_Contable_ID
        {
            get { return Cuenta_Contable_ID; }
            set { Cuenta_Contable_ID = value; }
        }

        //get y set de P_Rubro_ID
        public String P_Rubro_ID
        {
            get { return Rubro_ID; }
            set { Rubro_ID = value; }
        }

        //get y set de Clave
        public String P_Clave
        {
            get { return Clave; }
            set { Clave = value; }
        }

        //get y set de P_Descripcion
        public String P_Descripcion
        {
            get { return Descripcion; }
            set { Descripcion = value; }
        }

        //get y set de P_Cantidad_Calculo
        public String P_Cantidad_Calculo
        {
            get { return Cantidad_Calculo; }
            set { Cantidad_Calculo = value; }
        }

        //get y set de P_Estatus
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }

        //get y set de P_Usuario_Creo
        public String P_Usuario_Creo
        {
            get { return Usuario_Creo; }
            set { Usuario_Creo = value; }
        }

        //get y set de P_Usuario_Modifico
        public String P_Usuario_Modifico
        {
            get { return Usuario_Modifico; }
            set { Usuario_Modifico = value; }
        }


        public String P_Campos_Dinamicos
        {
            get { return Campos_Dinamicos; }
            set { Campos_Dinamicos = value; }
        }

        public String P_Filtros_Dinamicos
        {
            get { return Filtros_Dinamicos; }
            set { Filtros_Dinamicos = value; }
        }

        public String P_Agrupar_Dinamico
        {
            get { return Agrupar_Dinamico; }
            set { Agrupar_Dinamico = value; }
        }

        public String P_Ordenar_Dinamico
        {
            get { return Ordenar_Dinamico; }
            set { Ordenar_Dinamico = value; }
        }

        public Boolean P_Incluir_Campos_Foraneos
        {
            get { return Incluir_Campos_Foraneos; }
            set { Incluir_Campos_Foraneos = value; }
        }

        public String P_Join
        {
            get { return Join; }
            set { Join = value; }
        }

        public String P_Unir_Tablas
        {
            get { return Unir_Tablas; }
            set { Unir_Tablas = value; }
        }
        #endregion

        #region MÉTODOS
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_SubConcepto_Ing
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los subconceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 07/Mayo/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_SubConcepto_Ing()
        {
            return Cls_Cat_Psp_SubConcepto_Datos.Consultar_SubConcepto_Ing(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_SubConcepto_Concepto_Ing
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los conceptos y los subconceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 07/Mayo/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_SubConcepto_Concepto_Ing()
        {
            return Cls_Cat_Psp_SubConcepto_Datos.Consultar_SubConcepto_Concepto_Ing(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Completos_Ing
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los Tipos y lasc clases, rubros y conceptos y subconceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 07/Mayo/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Datos_Completos_Ing()
        {
            return Cls_Cat_Psp_SubConcepto_Datos.Consultar_Datos_Completos_Ing(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Guardar_SubConcepto_Ing
        ///DESCRIPCIÓN          : Metodo para guardar los datos de los subconceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 07/Mayo/2012 
        ///*********************************************************************************************************
        public Boolean Guardar_SubConcepto_Ing()
        {
            return Cls_Cat_Psp_SubConcepto_Datos.Alta_SubConcepto_Ing(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_SubConcepto_Ing
        ///DESCRIPCIÓN          : Metodo para modificar los datos de los subconceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 07/Mayo/2012
        ///*********************************************************************************************************
        public Boolean Modificar_SubConcepto_Ing()
        {
            return Cls_Cat_Psp_SubConcepto_Datos.Modificar_SubConcepto_Ing(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar_SubConcepto_Ing
        ///DESCRIPCIÓN          : Metodo para eliminar los datos de las subconceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 07/Mayo/2012 
        ///*********************************************************************************************************
        public Boolean Eliminar_SubConcepto_Ing()
        {
            return Cls_Cat_Psp_SubConcepto_Datos.Eliminar_SubConcepto_Ing(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Concepto_Ing
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los conceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 07/Mayo/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Concepto_Ing()
        {
            return Cls_Cat_Psp_SubConcepto_Datos.Consultar_Concepto_Ing(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Cuentas_Contables
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los datos de las cuentas contables
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 19/Mayo/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Cuentas_Contables()
        {
            return Cls_Cat_Psp_SubConcepto_Datos.Consultar_Cuenta_Contable(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Categorias
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los datos de los tipos de categorias
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 25/Marzo/2013 
        ///*********************************************************************************************************
        public DataTable Consultar_Categorias()
        {
            return Cls_Cat_Psp_SubConcepto_Datos.Consultar_Categorias();
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Impuestos
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los datos de los tipos de impuestos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 25/Marzo/2013 
        ///*********************************************************************************************************
        public DataTable Consultar_Impuestos()
        {
            return Cls_Cat_Psp_SubConcepto_Datos.Consultar_Impuestos();
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_SubConceptos_Ing
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los SubConceptos
        ///PROPIEDADES          :
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 21/Mayo/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_SubConceptos_Ing()
        {
            return Cls_Cat_Psp_SubConcepto_Datos.Consultar_SubConceptos_Ing(this);
        }

        #endregion
    }
}