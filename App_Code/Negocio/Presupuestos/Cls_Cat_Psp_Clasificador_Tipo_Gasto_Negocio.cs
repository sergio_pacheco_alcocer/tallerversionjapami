﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Clasificador_Tipo_Gasto.Datos;

namespace JAPAMI.Clasificador_Tipo_Gasto.Negocio
{
    public class Cls_Cat_Psp_Clasificador_Tipo_Gasto_Negocio
    {
        public Cls_Cat_Psp_Clasificador_Tipo_Gasto_Negocio()
        {
        }

        #region Variables Locales
            private String CTG_ID;
            private String Clave;
            private String Nombre;
            private String Descripcion;      
        #endregion

        #region Variables Publicas
            public String P_CTG_ID
            {
                get { return CTG_ID; }
                set { CTG_ID = value; }
            }    
            public String P_Clave 
            {
                get { return Clave; }
                set { Clave = value; }
            }
            public String P_Nombre
            {
                get { return Nombre; }
                set { Nombre = value; }
            }
            public String P_Descripcion
            {
                get { return Descripcion; }
                set { Descripcion = value; }
            }            
        #endregion

        #region Metodos
        
            public DataTable Consultar_Fechas_Pago()
            {
                return Cls_Cat_Psp_Clasificador_Tipo_Gasto_Datos.Consultar_Clasificador(this);
            }            
            public String Agregar()
            {
                return Cls_Cat_Psp_Clasificador_Tipo_Gasto_Datos.Agregar(this);
            }            
            public String Actualizar()
            {
                return Cls_Cat_Psp_Clasificador_Tipo_Gasto_Datos.Actualizar(this);
            }
            public String Eliminar()
            {
                return Cls_Cat_Psp_Clasificador_Tipo_Gasto_Datos.Eliminar(this);
            }
            public DataTable Validar_Clave()
            {
                return Cls_Cat_Psp_Clasificador_Tipo_Gasto_Datos.Validar_Clave(this);
            }

        #endregion
    }
}
