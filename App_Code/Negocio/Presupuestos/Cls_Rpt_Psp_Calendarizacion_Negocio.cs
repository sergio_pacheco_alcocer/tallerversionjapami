﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Presupuestos_Reporte_Calendarizacion.Datos;

/// <summary>
/// Summary description for Cls_Rpt_Psp_Calendarizacion
/// </summary>
namespace JAPAMI.Presupuestos_Reporte_Calendarizacion.Negocio
{
    public class Cls_Rpt_Psp_Calendarizacion_Negocio
    {
        #region Variables Privadas
        private String Anio = null;
        private String Unidad_Responsable = null;
        private String Partida = null;

        #endregion

        #region Variable publicas
        public String P_Anio
        {
            get { return Anio; }
            set { Anio = value; }
        }
        public String P_Unidad_Responsable
        {
            get { return Unidad_Responsable; }
            set { Unidad_Responsable = value; }
        }
        public String P_Partida
        {
            get { return Partida; }
            set { Partida = value; }
        }
        #endregion

        #region Metodos
        public DataTable Consultar_Anio() {
            return Cls_Rpt_Psp_Calendarizacion_Datos.consultar_Anio();
        }

        public DataTable Consultar_Unidad_Reponsable() {
            return Cls_Rpt_Psp_Calendarizacion_Datos.Consultar_Unidad_Responsable(this);
        }

        public DataTable Consultar_Partida() {
            return Cls_Rpt_Psp_Calendarizacion_Datos.Consultar_Partida(this);
        }

        public DataTable Consulta_Calendarizacion() {
            return Cls_Rpt_Psp_Calendarizacion_Datos.Consulta_Calendarizacion(this);        
        }

        #endregion
        
    }
}
