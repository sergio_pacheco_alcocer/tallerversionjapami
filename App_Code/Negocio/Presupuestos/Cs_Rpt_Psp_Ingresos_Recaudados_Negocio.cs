﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Rpt_Ingresos_Recaudados.Datos;

namespace JAPAMI.Rpt_Ingresos_Recaudados.Negocio
{
    public class Cs_Rpt_Psp_Ingresos_Recaudados_Negocio
    {
        #region(Variables Privadas)
            private String Anio;
            private String Fte_Financiamiento_ID;
        #endregion

        #region(Variables Publicas)
        //get y set de P_Anio
        public String P_Anio
        {
            get { return Anio; }
            set { Anio = value; }
        }

        //get y set de P_Fte_Financiamiento_ID
        public String P_Fte_Financiamiento_ID
        {
            get { return Fte_Financiamiento_ID; }
            set { Fte_Financiamiento_ID = value; }
        }

        #endregion

        #region Metodos_Aprobado
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Anios
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los años
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 15/Junio/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Anios()
        {
            return Cs_Rpt_Psp_Ingresos_Recaudados_Datos.Consultar_Anios();
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_FF
        ///DESCRIPCIÓN          : Metodo para obtener los datos de las fuentes de financiamiento
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 15/Junio/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_FF()
        {
            return Cs_Rpt_Psp_Ingresos_Recaudados_Datos.Consultar_FF();
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Presupuesto_Ingresos
        ///DESCRIPCIÓN          : Metodo para obtener los datos de las fuentes de financiamiento
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 15/Junio/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Presupuesto_Ingresos()
        {
            return Cs_Rpt_Psp_Ingresos_Recaudados_Datos.Consultar_Presupuesto_Ingresos(this);
        }

        #endregion
    }
}
