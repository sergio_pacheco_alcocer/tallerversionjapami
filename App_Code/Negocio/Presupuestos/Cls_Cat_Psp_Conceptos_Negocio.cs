﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Catalogo_Psp_Conceptos.Datos;
using System.Data;

namespace JAPAMI.Catalogo_Psp_Conceptos.Negocio
{
    public class Cls_Cat_Psp_Conceptos_Negocio
    {
        public Cls_Cat_Psp_Conceptos_Negocio()
        {
        }

        #region Variables Locales
        private string Concepto_ID;
        private string Clave;
        private string Descripcion;
        private string Estatus;
        private String Anio;
        private String Banco_ID;
        private String Dependencia_ID;
        private string Clase_ID;
        private string Cuenta_Contable_ID;
        private string Usuario;

        private String Campos_Dinamicos;
        private String Filtros_Dinamicos;
        private String Agrupar_Dinamico;
        private String Ordenar_Dinamico;
        private Boolean Incluir_Campos_Foraneos;
        private String Join;
        private String Unir_Tablas;
        private DataTable Dt_Fuentes_Financiamiento;
        #endregion

        #region Variables Globales

        public string P_Usuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }

        public string P_Dependencia_ID
        {
            get { return Dependencia_ID; }
            set { Dependencia_ID = value; }
        }

        public string P_Anio
        {
            get { return Anio; }
            set { Anio = value; }
        }

        public string P_Banco_ID
        {
            get { return Banco_ID; }
            set { Banco_ID = value; }
        }

        public string P_Concepto_ID
        {
            get { return Concepto_ID; }
            set { Concepto_ID = value; }
        }


        public string P_Clave
        {
            get { return Clave; }
            set { Clave = value; }
        }


        public string P_Descripcion
        {
            get { return Descripcion; }
            set { Descripcion = value; }
        }


        public string P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }

        public string P_Clase_ID
        {
            get { return Clase_ID; }
            set { Clase_ID = value; }
        }

        public string P_Cuenta_Contable_ID
        {
            get { return Cuenta_Contable_ID; }
            set { Cuenta_Contable_ID = value; }
        }


        public String P_Campos_Dinamicos
        {
            get { return Campos_Dinamicos; }
            set { Campos_Dinamicos = value; }
        }

        public String P_Filtros_Dinamicos
        {
            get { return Filtros_Dinamicos; }
            set { Filtros_Dinamicos = value; }
        }

        public String P_Agrupar_Dinamico
        {
            get { return Agrupar_Dinamico; }
            set { Agrupar_Dinamico = value; }
        }

        public String P_Ordenar_Dinamico
        {
            get { return Ordenar_Dinamico; }
            set { Ordenar_Dinamico = value; }
        }

        public Boolean P_Incluir_Campos_Foraneos
        {
            get { return Incluir_Campos_Foraneos; }
            set { Incluir_Campos_Foraneos = value; }
        }

        public String P_Join
        {
            get { return Join; }
            set { Join = value; }
        }

        public String P_Unir_Tablas
        {
            get { return Unir_Tablas; }
            set { Unir_Tablas = value; }
        }

        public DataTable P_Dt_Fuentes_Financiamiento
        {
            get { return Dt_Fuentes_Financiamiento; }
            set { Dt_Fuentes_Financiamiento = value; }
        }
        #endregion

        #region Metodos

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Alta_Concepto
        ///DESCRIPCIÓN: Insertar un registro de un nuevo concepto SAP
        ///PARAMETROS: 
        ///CREO: sergio  manuel gallardo andrade
        ///FECHA_CREO: 03/20/2012 
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public void Alta_Concepto()
        {
            Cls_Cat_Psp_Conceptos_Datos.Alta_Concepto(this);
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Baja_Concepto
        ///DESCRIPCIÓN: Eliminar un registro de un concepto SAP existente
        ///PARAMETROS: 
        ///CREO: sergio  manuel gallardo andrade
        ///FECHA_CREO: 03/20/2012 
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************        
        public void Baja_Concepto()
        {
            Cls_Cat_Psp_Conceptos_Datos.Baja_Concepto(this);
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cambio_Concepto
        ///DESCRIPCIÓN: Modificar un registro de un concepto SAP existente
        ///PARAMETROS: 
        ///CREO: sergio  manuel gallardo andrade
        ///FECHA_CREO: 03/20/2012 
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************        
        public void Cambio_Concepto()
        {
            Cls_Cat_Psp_Conceptos_Datos.Cambio_Concepto(this);
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Consulta_Concepto
        ///DESCRIPCIÓN: Consultar uno o mas registros de un concepto SAP existente
        ///PARAMETROS: 
        ///CREO: sergio  manuel gallardo andrade
        ///FECHA_CREO: 03/20/2012 
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************        
        public DataTable Consulta_Concepto()
        {
            return Cls_Cat_Psp_Conceptos_Datos.Consulta_Concepto(this);
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Consulta_Clases
        ///DESCRIPCIÓN: Consultar los capitulos existentes
        ///PARAMETROS: 
        ///CREO: sergio  manuel gallardo andrade
        ///FECHA_CREO: 03/20/2012 
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************        
        public DataTable Consulta_Clases()
        {
            return Cls_Cat_Psp_Conceptos_Datos.Consulta_Clases();
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_SubConceptos
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los SubConceptos
        ///PROPIEDADES          :
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 21/Mayo/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Conceptos_Ing()
        {
            return Cls_Cat_Psp_Conceptos_Datos.Consultar_Conceptos_Ing(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Dependencias
        ///DESCRIPCIÓN          : Metodo para obtener los datos de las ur
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 13/Junio/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Dependencias()
        {
            return Cls_Cat_Psp_Conceptos_Datos.Consultar_UR();
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Banco
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los bancos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 13/Junio/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Banco()
        {
            return Cls_Cat_Psp_Conceptos_Datos.Consultar_Banco();
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Fuentes_Financiamiento
        ///DESCRIPCIÓN          : Metodo para obtener los datos de las fuentes de financiamiento
        ///PROPIEDADES          :
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 22/Junio/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Fuentes_Financiamiento()
        {
            return Cls_Cat_Psp_Conceptos_Datos.Consultar_Fuentes_Financiamiento();
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Fuentes_Por_Concepto
        ///DESCRIPCIÓN          : Metodo para obtener los datos de las fuentes de financiamiento
        ///PROPIEDADES          :
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 22/Junio/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Fuentes_Por_Concepto()
        {
            return Cls_Cat_Psp_Conceptos_Datos.Consultar_Fuentes_Por_Concepto(this);
        }

        #endregion

    }
}