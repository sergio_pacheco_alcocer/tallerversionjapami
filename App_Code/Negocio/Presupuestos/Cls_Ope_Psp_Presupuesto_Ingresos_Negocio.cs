﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Presupuesto_Ingresos.Datos;

namespace JAPAMI.Presupuesto_Ingresos.Negocio
{
    public class Cls_Ope_Psp_Presupuesto_Ingresos_Negocio
    {
        #region(Variables Privadas)
            private String Rubro_ID;
            private String Tipo_ID;
            private String Clase_ID;
            private String Concepto_ID;
            private String SubConcepto_ID;
            private String Anio;
            private String Fte_Financiamiento;
        #endregion

        #region(Variables Publicas)
            //get y set de P_Rubro_ID
            public String P_Rubro_ID
            {
                get { return Rubro_ID; }
                set { Rubro_ID = value; }
            }

            //get y set de P_Tipo_ID
            public String P_Tipo_ID
            {
                get { return Tipo_ID; }
                set { Tipo_ID = value; }
            }

            //get y set de P_Clase_ID
            public String P_Clase_ID
            {
                get { return Clase_ID; }
                set { Clase_ID = value; }
            }

            //get y set de P_Concepto_ID
            public String P_Concepto_ID
            {
                get { return Concepto_ID; }
                set { Concepto_ID = value; }
            }

            //get y set de P_SubConcepto_IDD
            public String P_SubConcepto_ID
            {
                get { return SubConcepto_ID; }
                set { SubConcepto_ID = value; }
            }

            //get y set de P_Fte_Financiamiento
            public String P_Fte_Financiamiento
            {
                get { return Fte_Financiamiento; }
                set { Fte_Financiamiento = value; }
            }

            //get y set de P_Anio
            public String P_Anio
            {
                get { return Anio; }
                set { Anio = value; }
            }
        #endregion

        #region Metodos_Aprobado
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Anios
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los años
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Anios()
            {
                return Cls_Ope_Psp_Presupuesto_Ingresos_Datos.Consultar_Anios();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_FF
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_FF()
            {
                return Cls_Ope_Psp_Presupuesto_Ingresos_Datos.Consultar_FF(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Rubros
            ///DESCRIPCIÓN          : Metodo para obtener los rubros del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Rubros()
            {
                return Cls_Ope_Psp_Presupuesto_Ingresos_Datos.Consultar_Rubros(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos
            ///DESCRIPCIÓN          : Metodo para obtener los tipos del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Tipos()
            {
                return Cls_Ope_Psp_Presupuesto_Ingresos_Datos.Consultar_Tipos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Clases
            ///DESCRIPCIÓN          : Metodo para obtener las clases del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Clases()
            {
                return Cls_Ope_Psp_Presupuesto_Ingresos_Datos.Consultar_Clases(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Conceptos()
            {
                return Cls_Ope_Psp_Presupuesto_Ingresos_Datos.Consultar_Conceptos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_SubConceptos
            ///DESCRIPCIÓN          : Metodo para obtener los subconceptos del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 19/mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_SubConceptos()
            {
                return Cls_Ope_Psp_Presupuesto_Ingresos_Datos.Consultar_SubConceptos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_FF_Ing
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_FF_Ing()
            {
                return Cls_Ope_Psp_Presupuesto_Ingresos_Datos.Consultar_FF_Ing(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Rubros
            ///DESCRIPCIÓN          : Metodo para obtener los rubros del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Rubros_Ing()
            {
                return Cls_Ope_Psp_Presupuesto_Ingresos_Datos.Consultar_Rubros_Ing(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos
            ///DESCRIPCIÓN          : Metodo para obtener los tipos del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Tipos_Ing()
            {
                return Cls_Ope_Psp_Presupuesto_Ingresos_Datos.Consultar_Tipos_Ing(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Clases
            ///DESCRIPCIÓN          : Metodo para obtener las clases del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Clases_Ing()
            {
                return Cls_Ope_Psp_Presupuesto_Ingresos_Datos.Consultar_Clases_Ing(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Conceptos_Ing()
            {
                return Cls_Ope_Psp_Presupuesto_Ingresos_Datos.Consultar_Conceptos_Ing(this);
            }
        #endregion
    }
}
