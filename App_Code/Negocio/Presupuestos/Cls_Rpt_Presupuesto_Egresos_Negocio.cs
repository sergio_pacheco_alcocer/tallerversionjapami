﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Reporte_Presupuesto_Egresos.Datos;

namespace JAPAMI.Reporte_Presupuesto_Egresos.Negocio
{
    public class Cls_Rpt_Presupuesto_Egresos_Negocio
    {
        #region(Variables Privadas)
            String Grupo_Dependencia_ID;
            String Dependencia_ID;
            String Programa_ID;
            String Partida_ID;
            String Anio;
            String Fte_Financiamiento_ID;
            String Tipo_Reporte;
            String Tipo_Operacion;
        #endregion

        #region(Variables Publicas)
            public String P_Grupo_Dependencia_ID
            {
                get { return Grupo_Dependencia_ID; }
                set { Grupo_Dependencia_ID = value; }
            }
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }
            public String P_Programa_ID
            {
                get { return Programa_ID; }
                set { Programa_ID = value; }
            }
            public String P_Partida_ID
            {
                get { return Partida_ID; }
                set { Partida_ID = value; }
            }
            public String P_Anio
            {
                get { return Anio; }
                set { Anio = value; }
            }
            public String P_Fte_Financiamiento_ID
            {
                get { return Fte_Financiamiento_ID; }
                set { Fte_Financiamiento_ID = value; }
            }
            public String P_Tipo_Reporte
            {
                get { return Tipo_Reporte; }
                set { Tipo_Reporte = value; }
            }

            public String P_Tipo_Operacion
            {
                get { return Tipo_Operacion; }
                set { Tipo_Operacion = value; }
            }
        #endregion

        #region(Metodos)
            public DataTable Consultar_Dependencias()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Dependencias(this);
            }
            public DataTable Consultar_Presupuesto_Dependencias()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Presupuesto_Dependencias(this);
            }
            public DataTable Consultar_Presupuesto_Programa()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Presupuesto_Programa(this);
            }
            public DataTable Consultar_Presupuesto_Partida()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Presupuesto_Partida(this);
            }
            public DataTable Consultar_Presupuesto_Año()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Presupuesto_Año(this);
            }
        #endregion

        #region Metodos Aprobados
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Tipo_Reporte
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los tipos de reportes
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 28/Mayo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Tipo_Reporte()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Tipo_Reporte(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Dependencia_PSP
            ///DESCRIPCIÓN          : Metodo para obtener las dependencias
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Dependencia_PSP()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Datos_Dependencia_PSP(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Dependencia_CAL
            ///DESCRIPCIÓN          : Metodo para obtener las Dependencia
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Dependencia_CAL()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Datos_Dependencia_CAL(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_UR_PSP
            ///DESCRIPCIÓN          : Metodo para obtener las unidades responsables
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_UR_PSP()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Datos_UR_PSP(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_UR_CAL
            ///DESCRIPCIÓN          : Metodo para obtener las unidades responsables
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_UR_CAL()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Datos_UR_CAL(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Programas_PSP
            ///DESCRIPCIÓN          : Metodo para obtener los programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Programas_PSP()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Datos_Programas_PSP(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Programas_CAL
            ///DESCRIPCIÓN          : Metodo para obtener los programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Programas_CAL()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Datos_Programas_CAL(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Fte_Financiamiento_PSP
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Fte_Financiamiento_PSP()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Datos_Fte_Financiamiento_PSP(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Fte_Financiamiento_CAL
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Fte_Financiamiento_CAL()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Datos_Fte_Financiamiento_CAL(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Partida_PSP
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las partidas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Partida_PSP()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Datos_Partida_PSP(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Partida_CAL
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las partidas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Partida_CAL()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Datos_Partida_CAL(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Analitico_PSP
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las partidas detalladamente
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Analitico_PSP()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Datos_Analitico_PSP(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Analitico_CAL
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las partidas detalladamente
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Analitico_CAL()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Datos_Analitico_CAL(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Analitico_MOD
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las partidas detalladamente
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Analitico_MOD()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Datos_Analitico_MOD(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Base
            ///DESCRIPCIÓN          : Metodo para obtener los base del reporte de movimientos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 28/Mayo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Base()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Tipo_Reporte(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Gpo_Dependencia
            ///DESCRIPCIÓN          : Metodo para obtener los base del grupo dependencia
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 18/Febrero/2013 
            ///*********************************************************************************************************
            public DataTable Consultar_Gpo_Dependencia()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Gpo_Dependencia(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Importes_Datos_Analitico_MOD
            ///DESCRIPCIÓN          : Metodo para obtener los importes de los datos de las partidas detalladamente 
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 22/Febrero/2013 
            ///*********************************************************************************************************
            public DataTable Consultar_Importes_Datos_Analitico_MOD()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Importes_Datos_Analitico_MOD(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Modificaciones_Analitico_MOD
            ///DESCRIPCIÓN          : Metodo para obtener los importes de los datos de las modificaciones 
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 11/Marzo/2013 
            ///*********************************************************************************************************
            public DataTable Consultar_Modificaciones_Analitico_MOD()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Modificaciones_Analitico_MOD(this);
            }
        #endregion

        #region (Obtener Clasificacion Administrativa)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Modificaciones_Analitico_MOD
            ///DESCRIPCIÓN          : Metodo para obtener la clave de la clasificacion administrativa
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2013 
            ///*********************************************************************************************************
            public String Consultar_Clasificacion_Administrativa()
            {
                return Cls_Rpt_Presupuesto_Egresos_Datos.Consultar_Clasificacion_Administrativa();
            }
        #endregion
    }
}
