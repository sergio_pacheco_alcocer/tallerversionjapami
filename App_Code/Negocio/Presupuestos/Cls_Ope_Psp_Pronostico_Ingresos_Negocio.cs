﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Ope_Psp_Pronosticos_Ingresos.Datos;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data;

namespace JAPAMI.Ope_Psp_Pronosticos_Ingresos.Negocio
{
    public class Cls_Ope_Psp_Pronostico_Ingresos_Negocio
    {
        #region VARIABLES INTERNAS
            private String Pronostico_Ing_ID;
            private String Anio;
            private String Tipo_ID;
            private String Clase_Ing_ID;
            private String Concepto_Ing_ID;
            private String Rubro_ID;
            private String Clave;
            private String Descripcion;
            private String Estatus;
            private String Total;
            private String Usuario_Creo;
            private String Usuario_Modifico;
            private DataTable Dt_Datos;
            private String Busqueda;
            private String Programa_ID;
            public String P_SubConcepto_Ing_ID { get; set; }
        #endregion

        #region VARIABLES PUBLICAS
            //get y set de P_Pronostico_Ing_ID
            public String P_Pronostico_Ing_ID
            {
                get { return Pronostico_Ing_ID; }
                set { Pronostico_Ing_ID = value; }
            }

            //get y set de P_Anio
            public String P_Anio
            {
                get { return Anio; }
                set { Anio = value; }
            }

            //get y set de P_Busqueda
            public String P_Busqueda
            {
                get { return Busqueda; }
                set { Busqueda = value; }
            }

            //get y set de P_Programa_ID
            public String P_Programa_ID
            {
                get { return Programa_ID; }
                set { Programa_ID = value; }
            }

            //get y set de P_Total
            public String P_Total
            {
                get { return Total; }
                set { Total = value; }
            }
            
            //get y set de P_Tipo_ID
            public String P_Tipo_ID
        {
            get { return Tipo_ID; }
            set { Tipo_ID = value; }
        }

            //get y set de P_Clase_Ing_ID
            public String P_Clase_Ing_ID
            {
                get { return Clase_Ing_ID; }
                set { Clase_Ing_ID = value; }
            }

            //get y set de P_Concepto_Ing_ID
            public String P_Concepto_Ing_ID
            {
                get { return Concepto_Ing_ID; }
                set { Concepto_Ing_ID = value; }
            }

            //get y set de P_Rubro_ID
            public String P_Rubro_ID
            {
                get { return Rubro_ID; }
                set { Rubro_ID = value; }
            }

            //get y set de Clave
            public String P_Clave
            {
                get { return Clave; }
                set { Clave = value; }
            }

            //get y set de P_Descripcion
            public String P_Descripcion
            {
                get { return Descripcion; }
                set { Descripcion = value; }
            }

            //get y set de P_Estatus
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }

            //get y set de P_Usuario_Creo
            public String P_Usuario_Creo
            {
                get { return Usuario_Creo; }
                set { Usuario_Creo = value; }
            }

            //get y set de P_Usuario_Modifico
            public String P_Usuario_Modifico
            {
                get { return Usuario_Modifico; }
                set { Usuario_Modifico = value; }
            }

            //get y set de P_Dt_Datos
            public DataTable P_Dt_Datos
            {
                get { return Dt_Datos; }
                set { Dt_Datos = value; }
            }

        #endregion

        #region MÉTODOS
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Guardar_Registros
            ///DESCRIPCIÓN          : Metodo para guardar los datos de los registros
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 28/marzo/2012 
            ///*********************************************************************************************************
            public Boolean Guardar_Registros()
            {
                return Cls_Ope_Psp_Pronostico_Ingresos_Datos.Guardar_Registros(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Pronostico
            ///DESCRIPCIÓN          : Metodo para consultar los datos de los registros
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 28/Marzo/2012 
            ///*********************************************************************************************************
            public DataTable Consulta_Pronostico()
            {
                return Cls_Ope_Psp_Pronostico_Ingresos_Datos.Consulta_Pronostico(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Registros
            ///DESCRIPCIÓN          : Metodo para modificar los datos de los registros
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 28/marzo/2012 
            ///*********************************************************************************************************
            public Boolean Modificar_Registros()
            {
                return Cls_Ope_Psp_Pronostico_Ingresos_Datos.Modificar_Registros(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Fte_Financiamiento
            ///DESCRIPCIÓN          : Metodo para consultar los datos de las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 13/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consulta_Fte_Financiamiento()
            {
                return Cls_Ope_Psp_Pronostico_Ingresos_Datos.Consulta_Fte_Financiamiento();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los Tipos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 15/Marzo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Tipos()
            {
                return Cls_Ope_Psp_Pronostico_Ingresos_Datos.Consultar_Tipos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Rubros
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los rubros
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 15/Marzo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Rubros()
            {
                return Cls_Ope_Psp_Pronostico_Ingresos_Datos.Consultar_Rubros(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Clase_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las clases
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 20/Marzo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Clase_Ing()
            {
                return Cls_Ope_Psp_Pronostico_Ingresos_Datos.Consultar_Clase_Ing(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Fte_Financiamiento_Todas
            ///DESCRIPCIÓN          : Metodo para consultar los datos de las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consulta_Fte_Financiamiento_Todas()
            {
                return Cls_Ope_Psp_Pronostico_Ingresos_Datos.Consulta_Fte_Financiamiento_Todas();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Concepto_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los conceptos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 24/Julio/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Concepto_Ing()
            {
                return Cls_Ope_Psp_Pronostico_Ingresos_Datos.Consultar_Concepto_Ing(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_SubConcepto_Ing
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los subconceptos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 07/Mayo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_SubConcepto_Ing()
            {
                return Cls_Ope_Psp_Pronostico_Ingresos_Datos.Consultar_SubConcepto_Ing(this);
            }
        #endregion
    }
}