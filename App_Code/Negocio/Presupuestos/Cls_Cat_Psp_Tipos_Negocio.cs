﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Cat_Psp_Tipos.Datos;

namespace JAPAMI.Cat_Psp_Tipos.Negocio
{
    public class Cls_Cat_Psp_Tipos_Negocio
    {
        #region VARIABLES INTERNAS
        private String Tipo_ID;
        private String Rubro_ID;
        private String Clave;
        private String Anio;
        private String Descripcion;
        private String Estatus;
        private String Usuario_Creo;
        private String Usuario_Modifico;

        private String Campos_Dinamicos;
        private String Filtros_Dinamicos;
        private String Agrupar_Dinamico;
        private String Ordenar_Dinamico;
        private String Join;
        private String Unir_Tablas;
        #endregion

        #region VARIABLES PUBLICAS

        //get y set de P_Tipo_ID
        public String P_Tipo_ID
        {
            get { return Tipo_ID; }
            set { Tipo_ID = value; }
        }

        //get y set de P_Rubro_ID
        public String P_Rubro_ID
        {
            get { return Rubro_ID; }
            set { Rubro_ID = value; }
        }

        //get y set de Clave
        public String P_Clave
        {
            get { return Clave; }
            set { Clave = value; }
        }

        //get y set de P_Descripcion
        public String P_Descripcion
        {
            get { return Descripcion; }
            set { Descripcion = value; }
        }

        //get y set de P_Estatus
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }

        //get y set de P_Usuario_Creo
        public String P_Usuario_Creo
        {
            get { return Usuario_Creo; }
            set { Usuario_Creo = value; }
        }

        //get y set de P_Usuario_Modifico
        public String P_Usuario_Modifico
        {
            get { return Usuario_Modifico; }
            set { Usuario_Modifico = value; }
        }

        //get y set de Anio
        public String P_Anio
        {
            get { return Anio; }
            set { Anio = value; }
        }


        public String P_Campos_Dinamicos
        {
            get { return Campos_Dinamicos; }
            set { Campos_Dinamicos = value; }
        }

        public String P_Filtros_Dinamicos
        {
            get { return Filtros_Dinamicos; }
            set { Filtros_Dinamicos = value; }
        }

        public String P_Agrupar_Dinamico
        {
            get { return Agrupar_Dinamico; }
            set { Agrupar_Dinamico = value; }
        }

        public String P_Ordenar_Dinamico
        {
            get { return Ordenar_Dinamico; }
            set { Ordenar_Dinamico = value; }
        }

        public String P_Join
        {
            get { return Join; }
            set { Join = value; }
        }

        public String P_Unir_Tablas
        {
            get { return Unir_Tablas; }
            set { Unir_Tablas = value; }
        }
        #endregion

        #region MÉTODOS
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los Tipos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 15/Marzo/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Tipos()
        {
            return Cls_Cat_Psp_Tipos_Datos.Consultar_Tipos(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos2
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los Tipos
        ///PROPIEDADES          :
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 21/Mayo/2012
        ///*********************************************************************************************************
        public DataTable Consultar_Tipos2()
        {
            return Cls_Cat_Psp_Tipos_Datos.Consultar_Tipos2(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos_Rubros
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los Tipos y los rubros
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 15/Marzo/2012 
        ///*********************************************************************************************************
        public DataTable Consultar_Tipos_Rubros()
        {
            return Cls_Cat_Psp_Tipos_Datos.Consultar_Tipos_Rubros(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Guardar_Tipos
        ///DESCRIPCIÓN          : Metodo para guardar los datos de los Tipos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 15/Marzo/2012 
        ///*********************************************************************************************************
        public Boolean Guardar_Tipos()
        {
            return Cls_Cat_Psp_Tipos_Datos.Alta_Tipos(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Tipos
        ///DESCRIPCIÓN          : Metodo para modificar los datos de los tipos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 15/Marzo/2012
        ///*********************************************************************************************************
        public Boolean Modificar_Tipos()
        {
            return Cls_Cat_Psp_Tipos_Datos.Modificar_Tipo(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar_Tipos
        ///DESCRIPCIÓN          : Metodo para eliminar los datos de los tipos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 15/Marzo/2012 
        ///*********************************************************************************************************
        public Boolean Eliminar_Tipos()
        {
            return Cls_Cat_Psp_Tipos_Datos.Eliminar_Tipo(this);
        }
        #endregion
    }
}
