﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Movimiento_Presupuestal_Ingresos.Datos;

namespace JAPAMI.Movimiento_Presupuestal_Ingresos.Negocio
{
    public class Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos
    {
        #region(Variables Privadas)
            private String No_Solicitud;
            private String Codigo_Programatico_De;//es el codigo Origen
            private String Codigo_Programatico_Al;//Es el codigo Destino
            private String Fuente_Financiera;
            private String Area_Funcional;
            private String Programa;
            private String Responsable;
            private String Partida;
            private String Monto;
            private String Justificacion;
            private String Estatus;
            private String Usuario_Creo;
            private String Tipo_Operacion;
            private String Origen_Fuente_Financiamiento_Id;
            private String Destino_Fuente_Financiamiento_Id;
            private String Origen_Area_Funcional_Id;
            private String Destino_Area_Funcional_Id;
            private String Origen_Programa_Id;
            private String Destino_Programa_Id;
            private String Origen_Partida_Id;
            private String Destino_Partida_Id;
            private String Origen_Dependencia_Id;
            private String Destino_Dependencia_Id;
            private String Comentario;
            private String Fecha_Inicio;
            private String Fecha_Final;

            private String Fuente_Financiamiento_ID;
            private String Area_Funcional_ID;
            private String Programa_ID;
            private String Capitulo_ID;
            private String Partida_Especifica_ID;
            private String Anio;
            private String Dependencia_ID_Busqueda;
            private String Mes_Origen;
            private String Mes_Destino;
            private String Mes_Actual;
            private String Tipo_Usuario;
            private String Tipo_Partida;
            private DataTable Meses;
        #endregion

        #region(Variables Publicas)
            public String P_No_Solicitud
            {
                get { return No_Solicitud; }
                set { No_Solicitud = value; }
            }

            public String P_Codigo_Programatico_De
            {
                get { return Codigo_Programatico_De; }
                set { Codigo_Programatico_De = value; }
            }

            public String P_Tipo_Usuario
            {
                get { return Tipo_Usuario; }
                set { Tipo_Usuario = value; }
            }

            public DataTable P_Meses
            {
                get { return Meses; }
                set { Meses = value; }
            }

            public String P_Codigo_Programatico_Al
            {
                get { return Codigo_Programatico_Al; }
                set { Codigo_Programatico_Al = value; }
            }
            public String P_Fuente_Financiera
            {
                get { return Fuente_Financiera; }
                set { Fuente_Financiera = value; }
            }
            public String P_Area_Funcional
            {
                get { return Area_Funcional; }
                set { Area_Funcional = value; }
            }
            public String P_Programa
            {
                get { return Programa; }
                set { Programa = value; }
            }
            public String P_Responsable
            {
                get { return Responsable; }
                set { Responsable = value; }
            }
            public String P_Partida
            {
                get { return Partida; }
                set { Partida = value; }
            }

            public String P_Mes_Origen
            {
                get { return Mes_Origen; }
                set { Mes_Origen  = value; }
            }

            public String P_Mes_Actual
            {
                get { return Mes_Actual; }
                set { Mes_Actual = value; }
            }

            public String P_Mes_Destino
            {
                get { return Mes_Destino; }
                set { Mes_Destino = value; }
            }

            public String P_Monto
            {
                get { return Monto; }
                set { Monto = value; }
            }
            public String P_Justificacion
            {
                get { return Justificacion; }
                set { Justificacion = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Usuario_Creo
            {
                get { return Usuario_Creo; }
                set { Usuario_Creo = value; }
            }
            public String P_Tipo_Operacion
            {
                get { return Tipo_Operacion; }
                set { Tipo_Operacion = value; }
            }
            public String P_Origen_Fuente_Financiamiento_Id
            {
                get { return Origen_Fuente_Financiamiento_Id; }
                set { Origen_Fuente_Financiamiento_Id = value; }
            }
            public String P_Destino_Fuente_Financiamiento_Id
            {
                get { return Destino_Fuente_Financiamiento_Id; }
                set { Destino_Fuente_Financiamiento_Id = value; }
            }
            public String P_Destino_Area_Funcional_Id
            {
                get { return Destino_Area_Funcional_Id; }
                set { Destino_Area_Funcional_Id = value; }
            }
            public String P_Origen_Area_Funcional_Id
            {
                get { return Origen_Area_Funcional_Id; }
                set { Origen_Area_Funcional_Id = value; }
            }
            public String P_Origen_Programa_Id
            {
                get { return Origen_Programa_Id; }
                set { Origen_Programa_Id = value; }
            }
            public String P_Destino_Programa_Id
            {
                get { return Destino_Programa_Id; }
                set { Destino_Programa_Id = value; }
            }
            public String P_Origen_Partida_Id
            {
                get { return Origen_Partida_Id; }
                set { Origen_Partida_Id = value; }
            }
            public String P_Destino_Partida_Id
            {
                get { return Destino_Partida_Id; }
                set { Destino_Partida_Id = value; }
            }
            public String P_Origen_Dependencia_Id
            {
                get { return Origen_Dependencia_Id; }
                set { Origen_Dependencia_Id = value; }
            }
            public String P_Destino_Dependencia_Id
            {
                get { return Destino_Dependencia_Id; }
                set { Destino_Dependencia_Id = value; }
            }
            public String P_Comentario
            {
                get { return Comentario; }
                set { Comentario = value; }
            }
            public String P_Fecha_Inicio
            {
                get { return Fecha_Inicio; }
                set { Fecha_Inicio = value; }
            }
            public String P_Fecha_Final
            {
                get { return Fecha_Final; }
                set { Fecha_Final = value; }
            }


            //get y set de P_Fuente_Financiamiento_ID
            public String P_Fuente_Financiamiento_ID
            {
                get { return Fuente_Financiamiento_ID; }
                set { Fuente_Financiamiento_ID = value; }
            }

            //get y set de P_Area_Funcional_ID
            public String P_Area_Funcional_ID
            {
                get { return Area_Funcional_ID; }
                set { Area_Funcional_ID = value; }
            }

            //get y set de P_Programa_ID
            public String P_Programa_ID
            {
                get { return Programa_ID; }
                set { Programa_ID = value; }
            }

            //get y set de P_Dependencia_ID_Busqueda
            public String P_Dependencia_ID_Busqueda
            {
                get { return Dependencia_ID_Busqueda; }
                set { Dependencia_ID_Busqueda = value; }
            }

            //get y set de P_Capitulo_ID
            public String P_Capitulo_ID
            {
                get { return Capitulo_ID; }
                set { Capitulo_ID = value; }
            }

            //get y set de P_Partida_Especifica_ID
            public String P_Partida_Especifica_ID
            {
                get { return Partida_Especifica_ID; }
                set { Partida_Especifica_ID = value; }
            }

            //get y set de P_Anio
            public String P_Anio
            {
                get { return Anio; }
                set { Anio = value; }
            }

            //get y set de P_Tipo_Partida
            public String P_Tipo_Partida
            {
                get { return Tipo_Partida; }
                set { Tipo_Partida = value; }
            }

        #endregion

        #region(Metodos)
            public Boolean Alta_Movimiento()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Alta_Movimiento(this);
            }
            public Boolean Alta_Comentario()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Alta_Comentario(this);
            }
            public Boolean Modificar_Movimiento()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Modificar_Movimiento(this);
            }
            
            public Boolean Eliminar_Movimiento()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Eliminar_Movimiento(this);
            }
            public DataTable Consulta_Movimiento()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consultar_Movimiento(this);
            }
            public DataTable Consulta_Movimiento_Fecha()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consulta_Movimiento_Fecha(this);
            }
            public DataTable Consultar_Programa()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consultar_Programa(this);
            }
            public DataTable Consultar_Area_Funciona()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consultar_Area_Funcional(this);
            }

            public DataTable Consultar_Like_Movimiento()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consultar_Like_Movimiento(this);
            }
            public DataTable Consultar_Dependencia_Ordenada()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consultar_Dependencia_Ordenada(this);
            }
            public DataTable Consulta_Datos_Partidas()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consulta_Datos_Partidas(this);
            }
            public DataTable Consulta_Datos_Comentarios()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consulta_Datos_Comentarios(this);
            }

            public DataTable Consulta_Movimiento_Btn_Busqueda()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consulta_Movimiento_Btn_Busqueda(this);
            }

            public DataTable Consulta_Detalles_Importes()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consultar_Detalles_Importes(this);
            }
        #endregion

        #region Metodos_Aprobado
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Fuente_Financiamiento
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las fuentes de fiananciamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 2/Marzo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Fuente_Financiamiento()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consultar_Fuente_Financiamiento(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Programa
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 2/Marzo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Programas_Aprobados()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consultar_Programas_Aprobados(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas_Especificas
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las partidas especificas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 2/Marzo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Partidas_Especificas()
            {
                 return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consultar_Partida_Especifica(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Capitulos
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los capitulos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 2/Marzo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Capitulos()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consultar_Capitulos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Capitulos
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los capitulos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Marzo/2012  
            ///*********************************************************************************************************
            public DataTable Obtener_Capitulos()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Obtener_Capitulos();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_URs
            ///DESCRIPCIÓN          : Metodo para obtener si el rol es de empleado o administrativo
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 08/marzo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_URs()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consultar_URs(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_URs
            ///DESCRIPCIÓN          : Metodo para obtener si el rol es de empleado o administrativo
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 08/marzo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Fuente_Financiamiento_Ramo33()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Ingresos_Datos.Consultar_Fuente_Financiamiento_Ramo33();
            }
        #endregion
    }
}