﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Subniveles_Presupuesto.Datos;



/// <summary>
/// Summary description for Cls_Cat_Psp_Subnivel_Presupuesto_Negocio
/// </summary>
/// 
namespace JAPAMI.Subniveles_Presupuesto.Negocio{
public class Cls_Cat_Psp_Subnivel_Presupuesto_Negocio
{
    
        #region Variables Internas
            private String Partida_ID;
            private String Subnivel_Presupuestal;
            private String Anio;
            private String Estatus;
            private String Fecha_Creo;
            private String Usuario_Creo;
            private String Fecha_Modifico;
            private String Usuario_Modifico;
            private String Dependencia_ID;
            private String Programa_ID;
            private String FTE_Financiamiento_ID;
            private String Capitulo_ID;
            private String Area_Funcional_ID;
            private String Descripcion;
            private String Subnivel_Presupuestal_ID;
            private String Codigo_Programatico;
        #endregion

        #region Variables_Publicas
            // get y set de variable Codigo_Programatico
            public String P_Codigo_Programatico
            {
                get { return Codigo_Programatico; }
                set { Codigo_Programatico = value; }
            }
            // get y set de variable Partida_ID
            public String P_Partida_ID
            {
                get { return Partida_ID; }
                set { Partida_ID = value; }
            }
            // get y set de variable Clave
            public String P_Subnivel_Presupuestal
            {
                get { return Subnivel_Presupuestal; }
                set { Subnivel_Presupuestal = value; }
            }

            // get y set de variable Anio
            public String P_Anio
            {
                get { return Anio; }
                set { Anio = value; }
            }
        
            //get y set de variable Estatus
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }

            // get y set de variable fecha creo
            public String P_Fecha_Creo
            {
                get { return Fecha_Creo; }
                set { Fecha_Creo = value; }
            }
            // get y set de variable usuario creo
            public String P_Usuario_Creo
            {
                get { return Usuario_Creo; }
                set { Usuario_Creo = value; }
            }
            // get y set de variable fecha modifico
            public String P_Fecha_Modifico
            {
                get { return Fecha_Modifico; }
                set { Fecha_Modifico = value; }
            }
            // get y set de variable usuario modifico
            public String P_Usuario_Modifico
            {
                get { return Usuario_Modifico; }
                set { Usuario_Modifico = value; }
            }
            //get y set de variable Dependencia_ID
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }
            //get y set de variable Programa_ID
            public String P_Programa_ID
            {
                get { return Programa_ID; }
                set { Programa_ID = value; }
            }
            //get y set de variable FTE_Financiamiento_ID
            public String P_FTE_Financiamiento_ID
            {
                get { return FTE_Financiamiento_ID; }
                set { FTE_Financiamiento_ID = value; }
            }
            //get y set de variable Capitulo_ID
            public String P_Capitulo_ID
            {
                get { return Capitulo_ID; }
                set { Capitulo_ID = value; }
            }
            //get y set de variable Area_Funcional_ID
            public String P_Area_Funcional_ID
            {
                get { return Area_Funcional_ID; }
                set { Area_Funcional_ID = value; }
            }
            //get y set de variable Descripcion
            public String P_Descripcion
            {
                get { return Descripcion; }
                set { Descripcion = value; }
            }
            // get y set de variable Clave
            public String P_Subnivel_Presupuestal_ID
            {
                get { return Subnivel_Presupuestal_ID; }
                set { Subnivel_Presupuestal_ID = value; }
            }

    
        #endregion

        #region Metodos
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_SubNivel_Presupuestos
            ///DESCRIPCIÓN          : Metodo para obtener los datos de SubNivel_Presupuestos
            ///PROPIEDADES          :
            ///CREO                 : Luis Daniel Guzmám Malagón
            ///FECHA_CREO           : 13/Julio/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_SubNivel_Presupuestos()
            {
                return Cls_Cat_Psp_Subnivel_Presupuesto_Datos.consulta_SubNivel_Presupuestos(this);
                
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_SubNivel_Presupuestos_Para_GridView
            ///DESCRIPCIÓN          : Metodo para obtener los datos de SubNivel_Presupuestos
            ///PROPIEDADES          :
            ///CREO                 : Luis Daniel Guzmám Malagón
            ///FECHA_CREO           : 16/Julio/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_SubNivel_Presupuestos_Para_GridView()
            {
                return Cls_Cat_Psp_Subnivel_Presupuesto_Datos.consulta_SubNivel_Presupuestos_Para_GridView(this);

            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Insertar_SubNivel_presupuestos
            ///DESCRIPCIÓN          : Metodo para insertar los datos de los SubNivel_Presupuestos
            ///PROPIEDADES          :
            ///CREO                 : Luis Daniel Guzmán Malagón
            ///FECHA_CREO           : 13/Julio/2012 
            ///*********************************************************************************************************
            public Boolean Insertar_SubNivel_Presupuestos()
            {
                return Cls_Cat_Psp_Subnivel_Presupuesto_Datos.Alta_SubNivel_Presupuestos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_SubNivel_Presupuestos
            ///DESCRIPCIÓN          : Metodo para modificar los datos de SubNivel_Presuùestos
            ///PROPIEDADES          :
            ///CREO                 : Luis Daniel Guzmán Malagón
            ///FECHA_CREO           : 13/Julio/2012 
            ///*********************************************************************************************************
            public Boolean Modificar_SubNivel_Presupuestos()
            {
                return Cls_Cat_Psp_Subnivel_Presupuesto_Datos.Modificar_SubNivel_Presupuestos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Eliminar_SubNivel_Presupuestos
            ///DESCRIPCIÓN          : Metodo para eliminar los datos de SubNivel_Presupuestos
            ///PROPIEDADES          :
            ///CREO                 : Luis Daniel Guzmán Malagón
            ///FECHA_CREO           : 13/Julio/2012 
            ///*********************************************************************************************************
            public Boolean Eliminar_SubNivel_Presupuestos()
            {
                return Cls_Cat_Psp_Subnivel_Presupuesto_Datos.Eliminar_SubNivel_Presupuestos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Unidades_Responsables
            ///DESCRIPCIÓN          : Metodo para consultar las unidades responsables 
            ///PROPIEDADES          :
            ///CREO                 : Jennyfer Ivonne Ceja Lemus
            ///FECHA_CREO           : 17/Agosto/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Unidades_Responsables()
            {
                return Cls_Cat_Psp_Subnivel_Presupuesto_Datos.Consultar_Unidades_Responsables(this);
            }
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_FTE_Financiamiento
            ///DESCRIPCIÓN          : Metodo para consultar las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Jennyfer Ivonne Ceja Lemus
            ///FECHA_CREO           : 17/Agosto/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_FTE_Financiamiento()
            {
                return Cls_Cat_Psp_Subnivel_Presupuesto_Datos.Consultar_FTE_Financiamiento(this);
            }
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Programas
            ///DESCRIPCIÓN          : Metodo para consultar las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Jennyfer Ivonne Ceja Lemus
            ///FECHA_CREO           : 17/Agosto/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Programas()
            {
                return Cls_Cat_Psp_Subnivel_Presupuesto_Datos.Consultar_Programa(this);
            }
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Capitulos
            ///DESCRIPCIÓN          : Metodo para consultar las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Jennyfer Ivonne Ceja Lemus
            ///FECHA_CREO           : 17/Agosto/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Capitulos()
            {
                return Cls_Cat_Psp_Subnivel_Presupuesto_Datos.Consultar_Capitulos(this);
            }
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas
            ///DESCRIPCIÓN          : Metodo para consultar las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Jennyfer Ivonne Ceja Lemus
            ///FECHA_CREO           : 17/Agosto/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Partidas()
            {
                return Cls_Cat_Psp_Subnivel_Presupuesto_Datos.Consultar_Partidas_Especificas(this);
            }
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Area_Funcional
            ///DESCRIPCIÓN          : Metodo para consultar el area fucnional de una Unidad Responsable
            ///PROPIEDADES          :
            ///CREO                 : Jennyfer Ivonne Ceja Lemus
            ///FECHA_CREO           : 18/Agosto/2012 
            ///*********************************************************************************************************
            public DataTable Obtener_Area_Funcional()
            {
                return Cls_Cat_Psp_Subnivel_Presupuesto_Datos.Obtener_Area_Funcional(this);
            }


        #endregion
}
}