﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Movimiento_Presupuestal.Datos;

namespace JAPAMI.Movimiento_Presupuestal.Negocio
{
    public class Cls_Ope_Psp_Movimiento_Presupuestal_Negocio
    {
        #region(Variables Privadas)
            private String No_Solicitud;
            private String Total_Modificado;
            private String Importe;
            private String Justificacion;
            private String Estatus;
            private String Usuario_Creo;
            private String Comentario;
            private String Fecha_Inicio;
            private String Fecha_Final;
            private String Fuente_Financiamiento_ID;
            private String Area_Funcional_ID;
            private String Programa_ID;
            private String Capitulo_ID;
            private String Partida_Especifica_ID;
            private String Anio;
            private String Dependencia_ID_Busqueda;
            private String Mes_Actual;
            private String Tipo_Usuario;
            private String Tipo_Egreso;
            private String Tipo_Solicitud;
            private String No_Modificacion;
            private String Busqueda;
            private DataTable Dt_Mov;
            private DataTable Dt_Anexos;
            private DataTable Dt_Solicitante;
        #endregion

        #region(Variables Publicas)
            public String P_No_Solicitud
            {
                get { return No_Solicitud; }
                set { No_Solicitud = value; }
            }

            public String P_No_Modificacion
            {
                get { return No_Modificacion; }
                set { No_Modificacion = value; }
            }

            public String P_Tipo_Usuario
            {
                get { return Tipo_Usuario; }
                set { Tipo_Usuario = value; }
            }
            public DataTable P_Dt_Mov
            {
                get { return Dt_Mov; }
                set { Dt_Mov = value; }
            }

            public DataTable P_Dt_Anexos
            {
                get { return Dt_Anexos; }
                set { Dt_Anexos = value; }
            }

            public String P_Mes_Actual
            {
                get { return Mes_Actual; }
                set { Mes_Actual = value; }
            }
            public String P_Importe
            {
                get { return Importe; }
                set { Importe = value; }
            }
            public String P_Total_Modificado
            {
                get { return Total_Modificado; }
                set { Total_Modificado = value; }
            }
            public String P_Justificacion
            {
                get { return Justificacion; }
                set { Justificacion = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Usuario_Creo
            {
                get { return Usuario_Creo; }
                set { Usuario_Creo = value; }
            }
            public String P_Comentario
            {
                get { return Comentario; }
                set { Comentario = value; }
            }
            public String P_Fecha_Inicio
            {
                get { return Fecha_Inicio; }
                set { Fecha_Inicio = value; }
            }
            public String P_Fecha_Final
            {
                get { return Fecha_Final; }
                set { Fecha_Final = value; }
            }

            //get y set de P_Fuente_Financiamiento_ID
            public String P_Fuente_Financiamiento_ID
            {
                get { return Fuente_Financiamiento_ID; }
                set { Fuente_Financiamiento_ID = value; }
            }

            //get y set de P_Area_Funcional_ID
            public String P_Area_Funcional_ID
            {
                get { return Area_Funcional_ID; }
                set { Area_Funcional_ID = value; }
            }

            //get y set de P_Programa_ID
            public String P_Programa_ID
            {
                get { return Programa_ID; }
                set { Programa_ID = value; }
            }

            //get y set de P_Dependencia_ID_Busqueda
            public String P_Dependencia_ID_Busqueda
            {
                get { return Dependencia_ID_Busqueda; }
                set { Dependencia_ID_Busqueda = value; }
            }

            //get y set de P_Busqueda
            public String P_Busqueda
            {
                get { return Busqueda; }
                set { Busqueda = value; }
            }

            //get y set de P_Tipo_Solicitud
            public String P_Tipo_Solicitud
            {
                get { return Tipo_Solicitud; }
                set { Tipo_Solicitud = value; }
            }

            //get y set de P_Capitulo_ID
            public String P_Capitulo_ID
            {
                get { return Capitulo_ID; }
                set { Capitulo_ID = value; }
            }

            //get y set de P_Partida_Especifica_ID
            public String P_Partida_Especifica_ID
            {
                get { return Partida_Especifica_ID; }
                set { Partida_Especifica_ID = value; }
            }

            //get y set de P_Anio
            public String P_Anio
            {
                get { return Anio; }
                set { Anio = value; }
            }

            //get y set de P_Tipo_Egreso
            public String P_Tipo_Egreso
            {
                get { return Tipo_Egreso; }
                set { Tipo_Egreso = value; }
            }

            //get y set de P_Dt_Solicitante
            public DataTable P_Dt_Solicitante
            {
                get { return Dt_Solicitante; }
                set { Dt_Solicitante = value; }
            }
        #endregion

        #region(Metodos)
            public DataTable Alta_Movimiento()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Alta_Movimiento(this);
            }
            public Boolean Alta_Comentario()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Alta_Comentario(this);
            }
            public Boolean Modificar_Movimiento()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Modificar_Movimiento(this);
            }
            
            public Boolean Eliminar_Movimiento()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Eliminar_Movimiento(this);
            }
            public DataTable Consulta_Movimiento()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Movimiento(this);
            }
            public DataTable Consulta_Movimiento_Fecha()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consulta_Movimiento_Fecha(this);
            }
            public DataTable Consultar_Programa()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Programa(this);
            }
            public DataTable Consultar_Area_Funciona()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Area_Funcional(this);
            }

            public DataTable Consultar_Like_Movimiento()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Like_Movimiento(this);
            }
            public DataTable Consultar_Dependencia_Ordenada()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Dependencia_Ordenada(this);
            }
            public DataTable Consulta_Datos_Partidas()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consulta_Datos_Partidas(this);
            }
            public DataTable Consulta_Datos_Comentarios()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consulta_Datos_Comentarios(this);
            }

            public DataTable Consulta_Movimiento_Btn_Busqueda()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consulta_Movimiento_Btn_Busqueda(this);
            }

            public DataTable Consulta_Detalles_Importes()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Detalles_Importes(this);
            }
        #endregion

        #region Metodos_Aprobado
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Fuente_Financiamiento
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las fuentes de fiananciamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 2/Marzo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Fuente_Financiamiento()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Fuente_Financiamiento(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Programa
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 2/Marzo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Programas_Aprobados()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Programas_Aprobados(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas_Especificas
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las partidas especificas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 2/Marzo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Partidas_Especificas()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Partida_Especifica(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Capitulos
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los capitulos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 2/Marzo/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Capitulos()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Capitulos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Capitulos
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los capitulos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Marzo/2012  
            ///*********************************************************************************************************
            public DataTable Obtener_Capitulos()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Obtener_Capitulos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_URs
            ///DESCRIPCIÓN          : Metodo para obtener si el rol es de empleado o administrativo
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 08/marzo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Fuente_Financiamiento_Ramo33()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Fuente_Financiamiento_Ramo33();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Usuario_Ramo33
            ///DESCRIPCIÓN          : Metodo para obtener los datos del empleado de ramo 33
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 10/mayo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Usuario_Ramo33()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Usuario_Ramo33();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Modificaciones
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las modificaciones
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/mayo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Modificaciones()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Datos_Modificaciones(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Movimientos
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los movimientos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/mayo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Movimientos()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Datos_Movimientos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Movimientos_Det
            ///DESCRIPCIÓN          : Metodo para obtener los datos dde los detalles de los movimientos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/mayo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Movimientos_Det()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Datos_Movimientos_Det(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Autorizacion_Mov
            ///DESCRIPCIÓN          : Metodo para actualizar los datos de los movimientos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/mayo/2012 
            ///*********************************************************************************************************
            public String Modificar_Autorizacion_Mov()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Modificar_Autorizacion_Mov(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Coordinador_UR
            ///DESCRIPCIÓN          : Metodo para obtener los datos del coordinador del area
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Junio/2012 
            ///*********************************************************************************************************
            public DataTable Obtener_Coordinador_UR()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Obtener_Coordinador_UR(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Movimientos
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los movimientos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/mayo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Datos_Movimientos_Tipo()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Datos_Modificacion_Tipo(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Programa_Unidades_Responsables
            ///DESCRIPCIÓN          : Metodo para obtener los datos de LOS PROGRAMAS RELACIONADOS A LAS FUENTES DE FINANCIAMIENTO
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/mayo/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Programa_Unidades_Responsables()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Programa_Unidades_Responsables(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Modif_Solicitud_Mov
            ///DESCRIPCIÓN          : Metodo para modificar los datos de las solicitudes
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 09/Julio/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Modif_Solicitud_Mov()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Modif_Solicitud_Mov(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Estatus_Modificacion
            ///DESCRIPCIÓN          : Metodo para modificar los datos de las solicitudes
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 09/Julio/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Estatus_Modificacion()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Estatus_Modificacion(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Estatus_Mod_Sig
            ///DESCRIPCIÓN          : Metodo para modificar los datos de las solicitudes
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 20/Julio/2012 
            ///*********************************************************************************************************
            public Boolean Consultar_Estatus_Mod_Sig()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Estatus_Mod_Sig(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Anexos
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los anexos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 06/Septiembre/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Anexos()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Anexos(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_No_Solicitud
            ///DESCRIPCIÓN          : Metodo para obtener el numero de solicitud siguiente
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 06/Septiembre/2012  
            ///*********************************************************************************************************
            public Int32 Consultar_No_Solicitud()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_No_Solicitud();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Convenio_Programa
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los convenios de los programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 19/Septiembre/2012  
            ///*********************************************************************************************************
            public DataTable Consultar_Convenio_Programa()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Convenio_Programa(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Ur_Programas_Psp
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las dependencias de los programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 19/Marzo/2013
            ///*********************************************************************************************************
            public DataTable Consultar_Ur_Programas_Psp()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Ur_Programas_Psp(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Ur_Programas
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las dependencias de los programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 19/Marzo/2013
            ///*********************************************************************************************************
            public DataTable Consultar_Ur_Programas()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Consultar_Ur_Programas(this);
            }
        #endregion

        #region (Adecuaciones)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Adecuaciones
            ///DESCRIPCIÓN          : Metodo para dar de alta los dato de las adecuaciones
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Febrero/2013
            ///*********************************************************************************************************
            public DataTable Alta_Adecuaciones()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Alta_Adecuaciones(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Adecuaciones
            ///DESCRIPCIÓN          : Metodo para modificar los dato de las adecuaciones
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Febrero/2013
            ///*********************************************************************************************************
            public DataTable Modificar_Adecuaciones()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Modificar_Adecuaciones(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Autorizacion_Adecuaciones
            ///DESCRIPCIÓN          : Metodo para autorizar los dato de las adecuaciones
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Febrero/2013
            ///*********************************************************************************************************
            public String Autorizacion_Adecuaciones()
            {
                return Cls_Ope_Psp_Movimiento_Presupuestal_Datos.Autorizacion_Adecuaciones(this);
            }
        #endregion
    }
}