﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Clasificador_Administrativo.Datos;

namespace JAPAMI.Clasificador_Administrativo.Negocio
{
    public class Cls_Cat_Psp_Clasificador_Administrativo_Negocio
    {
        public Cls_Cat_Psp_Clasificador_Administrativo_Negocio()
        {
        }

        #region Variables Locales
            private String CADM_ID;
            private String Clave;
            private String Nombre;
            private String Descripcion;      
        #endregion

        #region Variables Publicas
            public String P_CADM_ID
            {
                get { return CADM_ID; }
                set { CADM_ID = value; }
            }    
            public String P_Clave 
            {
                get { return Clave; }
                set { Clave = value; }
            }
            public String P_Nombre
            {
                get { return Nombre; }
                set { Nombre = value; }
            }
            public String P_Descripcion
            {
                get { return Descripcion; }
                set { Descripcion = value; }
            }            
        #endregion

        #region Metodos

            public DataTable Consultar_Clasificador()
            {
                return Cls_Cat_Psp_Clasificador_Administrativo_Datos.Consultar_Clasificador(this);
            }            
            public String Agregar()
            {
                return Cls_Cat_Psp_Clasificador_Administrativo_Datos.Agregar(this);
            }            
            public String Actualizar()
            {
                return Cls_Cat_Psp_Clasificador_Administrativo_Datos.Actualizar(this);
            }
            public String Eliminar()
            {
                return Cls_Cat_Psp_Clasificador_Administrativo_Datos.Eliminar(this);
            }
            public DataTable Validar_Clave()
            {
                return Cls_Cat_Psp_Clasificador_Administrativo_Datos.Validar_Clave(this);
            }

        #endregion
    }
}
