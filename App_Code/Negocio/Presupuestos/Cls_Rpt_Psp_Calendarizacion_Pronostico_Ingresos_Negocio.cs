﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Calendarizacion_Pronostico_Ingresos.Datos;

namespace JAPAMI.Calendarizacion_Pronostico_Ingresos.Negocio
{
    public class Cls_Rpt_Psp_Calendarizacion_Pronostico_Ingresos
    {
        #region Variables Privadas
        public DateTime Fecha{get;set;}      
        #endregion
        #region Metodos

        public DataTable Pronostico_Ingresos()
        {
            return Cls_Rpt_Psp_Calendarizacion_Pronostico_Ingresos_Datos.Pronostico_Ingresos(this);
        }
        #endregion
    }
}