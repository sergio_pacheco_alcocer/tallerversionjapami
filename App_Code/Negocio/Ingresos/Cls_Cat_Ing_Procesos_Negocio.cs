﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Cls_Cat_Ing_Procesos.Datos;

namespace JAPAMI.Cls_Cat_Ing_Procesos.Negocio
{

    public class Cls_Cat_Ing_Procesos_Negocio
    {
        #region Variables Internas

        private String Proceso_ID;
        private String Descripcion;
        private String Estatus;
        private String Usuario;

        private String Campos_Dinamicos;
        private String Filtros_Dinamicos;
        private String Agrupar_Dinamico;
        private String Ordenar_Dinamico;
        private String Join;

        private SqlCommand Cmmd;
        #endregion

        #region Variables Publicas

        public String P_Proceso_ID
        {
            get { return Proceso_ID; }
            set { Proceso_ID = value; }
        }

        public String P_Descripcion
        {
            get { return Descripcion; }
            set { Descripcion = value; }
        }

        public string P_Usuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }

        public string P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }


        public String P_Campos_Dinamicos
        {
            get { return Campos_Dinamicos; }
            set { Campos_Dinamicos = value; }
        }

        public String P_Filtros_Dinamicos
        {
            get { return Filtros_Dinamicos; }
            set { Filtros_Dinamicos = value; }
        }

        public String P_Agrupar_Dinamico
        {
            get { return Agrupar_Dinamico; }
            set { Agrupar_Dinamico = value; }
        }

        public String P_Ordenar_Dinamico
        {
            get { return Ordenar_Dinamico; }
            set { Ordenar_Dinamico = value; }
        }

        public String P_Join
        {
            get { return Join; }
            set { Join = value; }
        }


        public SqlCommand P_Cmmd
        {
            get { return Cmmd; }
            set { Cmmd = value; }
        }
        #endregion

        #region Metodos

        public Boolean Alta_Proceso()
        {
            return Cls_Cat_Ing_Procesos_Datos.Alta_Proceso(this);
        }

        public Boolean Modificar_Proceso()
        {
            return Cls_Cat_Ing_Procesos_Datos.Modificar_Proceso(this);
        }

        public Boolean Eliminar_Proceso()
        {
            return Cls_Cat_Ing_Procesos_Datos.Eliminar_Proceso(this);
        }

        public DataTable Consultar_Procesos()
        {
            return Cls_Cat_Ing_Procesos_Datos.Consultar_Procesos(this);
        }
        #endregion

    }
}