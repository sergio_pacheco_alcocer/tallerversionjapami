﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Seguimiento_Contrarecibos.Datos;

/// <summary>
/// Summary description for Cls_Alm_Seguimiento_Contrarecibos_Negocio
/// </summary>
/// 

namespace JAPAMI.Seguimiento_Contrarecibos.Negocio
{
    public class Cls_Alm_Seguimiento_Contrarecibos_Negocio
    {
        #region Variables Privadas

            private String No_Contra_Recibo;
            private String No_Orden_Compra;
            private String Fecha_Inicio;
            private String Fecha_Fin;
            private String Estatus_Contrarecibo = String.Empty;
        
        #endregion 

        #region Variables Publicas

            public String P_No_Contra_Recibo
            {
                get { return No_Contra_Recibo; }
                set { No_Contra_Recibo = value; }
            }
            public String P_No_Orden_Compra
            {
                get { return No_Orden_Compra; }
                set { No_Orden_Compra = value; }
            }
            public String P_Fecha_Inicio
            {
                get { return Fecha_Inicio; }
                set { Fecha_Inicio = value; }
            }
            public String P_Fecha_Fin
            {
                get { return Fecha_Fin; }
                set { Fecha_Fin = value; }
            }
            public String P_Estatus_Contrarecibo
            {
                get { return Estatus_Contrarecibo; }
                set { Estatus_Contrarecibo = value; }
            }

        #endregion

        #region Metodos

        public Cls_Alm_Seguimiento_Contrarecibos_Negocio()
        {
        }

        public DataTable Consultar_Contra_Recibos() 
        {
            return Cls_Alm_Seguimiento_Contrarecibos_Datos.Consultar_Contra_Recibos(this);
        }

        public DataTable Consultar_Registro_Facturas()
        {
            return Cls_Alm_Seguimiento_Contrarecibos_Datos.Consultar_Registro_Facturas(this);
        }

        public DataTable Consultar_Seguimiento()
        {
            return Cls_Alm_Seguimiento_Contrarecibos_Datos.Consultar_Seguimiento(this);
        }
        
        #endregion 
    }
}
