﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Ope_Alm_Autorizacion_Servicios.Datos;

/// <summary>
/// Summary description for Cls_Ope_Alm_Autorizacion_Compras_Negocio
/// </summary>
namespace JAPAMI.Ope_Alm_Autorizacion_Servicios.Negocio
{
    public class Cls_Ope_Alm_Autorizacion_Servicios_Negocio
    {
        public Cls_Ope_Alm_Autorizacion_Servicios_Negocio()
        {
        }
        #region Variables Locales
        private String No_Contra_Recibo;
        private String Empleado_Recibido_ID;
        private String Estatus_Entrada;
        private String Estatus_Salida;
        private String Comentarios;
        private String Fecha_Inicio;
        private String Fecha_Fin;
        #endregion

        #region Variables Publicas
        public String P_Fecha_Inicio 
        {
            get { return Fecha_Inicio; }
            set { Fecha_Inicio = value; }
        }
        public String P_Fecha_Fin
        {
            get { return Fecha_Fin; }
            set { Fecha_Fin = value; }
        }
        public String P_No_Contra_Recibo
        {
            get { return No_Contra_Recibo; }
            set { No_Contra_Recibo = value; }
        }
        public String P_Empleado_Recibido_ID
        {
            get { return Empleado_Recibido_ID; }
            set { Empleado_Recibido_ID = value; }
        }        
        public String P_Estatus_Entrada
        {
            get { return Estatus_Entrada; }
            set { Estatus_Entrada = value; }
        }
        public String P_Estatus_Salida
        {
            get { return Estatus_Salida; }
            set { Estatus_Salida = value; }
        }
        public String P_Comentarios
        {
            get { return Comentarios; }
            set { Comentarios = value; }
        }
        #endregion

        #region Metodos
        
        
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Contra_Recibos
        ///DESCRIPCIÓN          : Consulta los contrarecibos para enlistarlos
        ///PROPIEDADES          :
        ///CREO                 : David Herrra rincon
        ///FECHA_CREO           : 28/Enero/2013 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        public DataTable Consultar_Contra_Recibos()
        {
            return Cls_Ope_Alm_Autorizacion_Servicios_Datos.Consultar_Contra_Recibos(this);
        }
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Autorizar_Contra_Recibo
        ///DESCRIPCIÓN          : Autoriza los contrarecibos
        ///PROPIEDADES          :
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 28/Enero/2013 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        public String Autorizar_Contra_Recibo()
        {
            return Cls_Ope_Alm_Autorizacion_Servicios_Datos.Autorizar_Contra_Recibo(this);
        }
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Rechazar_Contra_Recibo
        ///DESCRIPCIÓN          : rechaza el contrarecibo seleccionado
        ///PROPIEDADES          :
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 28/Enero/2013 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        public String Rechazar_Contra_Recibo()
        {
            return Cls_Ope_Alm_Autorizacion_Servicios_Datos.Rechazar_Contra_Recibo(this);
        }
        #endregion
    }
}
