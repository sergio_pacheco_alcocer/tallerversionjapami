﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Rpt_Alm_Orden_Entrada.Datos;

namespace JAPAMI.Rpt_Alm_Orden_Entrada.Negocio
{
    public class Cls_Rpt_Alm_Orden_Entrada_Negocio
    {
        #region VARIABLES LOCALES

            private String Requisicion_ID;
            private String Orden_Entrada;
            private String Orden_Compra;
            private DateTime Fecha_Inicio;
            private DateTime Fecha_Fin;
            private String Dependencia_ID;

        #endregion

        #region VARIABLES PUBLICAS

            public String P_Requisicion_ID
            {
                get { return Requisicion_ID; }
                set { Requisicion_ID = value; }
            }
            public String P_Orden_Entrada
            {
                get { return Orden_Entrada; }
                set { Orden_Entrada = value; }
            }
            public String P_Orden_Compra
            {
                get { return Orden_Compra; }
                set { Orden_Compra = value; }
            }
            public DateTime P_Fecha_Inicio
            {
                get { return Fecha_Inicio; }
                set { Fecha_Inicio = value; }
            }
            public DateTime P_Fecha_Fin
            {
                get { return Fecha_Fin; }
                set { Fecha_Fin = value; }
            }
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }

        #endregion

        #region METODOS

            public DataTable Consultar_Ordenes_Entrada()
            {
                return Cls_Rpt_Alm_Orden_Entrada_Datos.Consultar_Ordenes_Entrada(this);
            }
            public DataSet Detalles_Orden_Entrada()
            {
                return Cls_Rpt_Alm_Orden_Entrada_Datos.Detalles_Orden_Entrada(this);
            }

        #endregion
    }
}
