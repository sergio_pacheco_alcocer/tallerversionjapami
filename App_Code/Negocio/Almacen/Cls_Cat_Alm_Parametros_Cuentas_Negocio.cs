﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Parametros_Almacen_Cuentas.Datos;

namespace JAPAMI.Parametros_Almacen_Cuentas.Negocio
{
    public class Cls_Cat_Alm_Parametros_Cuentas_Negocio
    {
        #region Variables Locales

            private String Cta_Cont_Alm_General_ID;
            private String Cta_Cont_Alm_Papeleria_ID;
            private String Cta_IVA_Pendiente;
            private String Cta_IVA_Acreditable;
            private SqlCommand Cmmd;
        #endregion

        #region Variables Internas

            public String P_Cta_IVA_Acreditable
            {
                get { return Cta_IVA_Acreditable; }
                set { Cta_IVA_Acreditable = value; }
            }

            public String P_Cta_IVA_Pendiente
            {
                get { return Cta_IVA_Pendiente; }
                set { Cta_IVA_Pendiente = value; }
            }

            public String P_Cta_Cont_Alm_General_ID
            {
                get { return Cta_Cont_Alm_General_ID; }
                set { Cta_Cont_Alm_General_ID = value; }
            }
            public String P_Cta_Cont_Alm_Papeleria_ID
            {
                get { return Cta_Cont_Alm_Papeleria_ID; }
                set { Cta_Cont_Alm_Papeleria_ID = value; }
            }

            public SqlCommand P_Cmmd
            {
                get { return Cmmd; }
                set { Cmmd = value; }
            }
        #endregion

        #region Metodos

            public DataTable Consultar_Cuentas()
            {
                return Cls_Cat_Alm_Parametros_Cuentas_Datos.Consultar_Cuentas(this);
            }
            public void Modificar_Cuentas()
            {
                Cls_Cat_Alm_Parametros_Cuentas_Datos.Modificar_Cuentas(this);
            }
            public DataTable Obtener_Parametros_Poliza()
            {
               return  Cls_Cat_Alm_Parametros_Cuentas_Datos.Obtener_Parametros_Poliza(this);
            }
        #endregion
    }
}