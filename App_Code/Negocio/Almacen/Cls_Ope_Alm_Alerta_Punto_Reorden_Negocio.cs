﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using JAPAMI.Alerta_Pto_Reorden.Datos;

/// <summary>
/// Summary description for Cls_Ope_Alm_Alerta_Punto_Reorden_Negocio
/// </summary7
/// 
namespace JAPAMI.Alerta_Pto_Reorden.Negocio
{
    public class Cls_Ope_Alm_Alerta_Punto_Reorden_Negocio
    {

        ///*******************************************************************************
        /// VARIABLES INTERNAS 
        ///******************************************************************************
        #region Variables_Internas

        #endregion

        ///*******************************************************************************
        /// VARIABLES PUBLICAS
        ///*******************************************************************************
        #region Variables_Publicas

        #endregion


        ///*******************************************************************************
        /// METODOS
        ///*******************************************************************************
        #region Metodos

        public DataTable Consultar_Productos_Reorden()
        {
            return Cls_Ope_Alm_Alerta_Punto_Reorden_Datos.Consultar_Productos_Reorden();
        }

        #endregion


    }
}
