﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Cancelar_Salidas.Datos;

namespace JAPAMI.Cancelar_Salidas.Negocio
{
    public class Cls_Ope_Alm_Cancelar_Salidas_Negocio
    {
        #region Variables Privadas

            private String No_Requisicion;
            private String No_Salida;
            private String Estatus;
            private String Total;
            private String Comentario;
            private String Fecha_Inicio;
            private String Fecha_Fin;

            private DataTable Detalles;
        
        #endregion 

        #region Variables Publicas

            public String P_No_Requisicion
            {
                get { return No_Requisicion; }
                set { No_Requisicion = value; }
            }
            public String P_No_Salida
            {
                get { return No_Salida; }
                set { No_Salida = value; }
            }            
            public String P_Fecha_Inicio
            {
                get { return Fecha_Inicio; }
                set { Fecha_Inicio = value; }
            }
            public String P_Fecha_Fin
            {
                get { return Fecha_Fin; }
                set { Fecha_Fin = value; }
            }

            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Comentario
            {
                get { return Comentario; }
                set { Comentario = value; }
            }
            public String P_Total
            {
                get { return Total; }
                set { Total = value; }
            }
            public DataTable P_Dt_Detalles
            {
                get { return Detalles; }
                set { Detalles = value; }
            }

        #endregion

        #region Metodos

            public Cls_Ope_Alm_Cancelar_Salidas_Negocio()
        {
        }

            public DataTable Consultar_Salidas() 
            {
                return Cls_Ope_Alm_Cancelar_Salidas_Datos.Consultar_Salidas(this);
            }

            public DataTable Consultar_Salidas_Detalles()
            {
                return Cls_Ope_Alm_Cancelar_Salidas_Datos.Consultar_Salidas_Detalles(this);
            }

            public String Cancelar_Salidas()
            {
                return Cls_Ope_Alm_Cancelar_Salidas_Datos.Cancelar_Salidas(this);
            }
        
        #endregion 
    }
}
