﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Ope_Alm_Fechas_Pago.Datos;

/// <summary>
/// Summary description for Cls_Ope_Alm_Fechas_Pago_Negocio
/// </summary>
namespace JAPAMI.Ope_Alm_Fechas_Pago.Negocio
{
    public class Cls_Ope_Alm_Fechas_Pago_Negocio
    {
        public Cls_Ope_Alm_Fechas_Pago_Negocio()
        {
        }
        #region Variables Locales
            private String Anio_No_Fecha_Pago;
            private String Fecha_Recepcion;
            private String Fecha_Pago;
            private String Comentarios;        
        #endregion

        #region Variables Publicas
            public String P_Anio_No_Fecha_Pago 
            {
                get { return Anio_No_Fecha_Pago; }
                set { Anio_No_Fecha_Pago = value; }
            }
            public String P_Fecha_Recepcion
            {
                get { return Fecha_Recepcion; }
                set { Fecha_Recepcion = value; }
            }
            public String P_Fecha_Pago
            {
                get { return Fecha_Pago; }
                set { Fecha_Pago = value; }
            }
            public String P_Comentarios
            {
                get { return Comentarios; }
                set { Comentarios = value; }
            }
        #endregion

        #region Metodos
        
        public DataTable Consultar_Fechas_Pago()
        {
            return Cls_Ope_Alm_Fechas_Pago_Datos.Consultar_Fechas_Pago(this);
        }
        
        public String Agregar_Fechas()
        {
            return Cls_Ope_Alm_Fechas_Pago_Datos.Agregar_Fechas(this);
        }
        
        public String Actualizar_Fecha()
        {
            return Cls_Ope_Alm_Fechas_Pago_Datos.Actualizar_Fecha(this);
        }
        public DataTable Consultar_Ultimo()
        {
            return Cls_Ope_Alm_Fechas_Pago_Datos.Consultar_Ultimo(this);
        }

        public void Eliminar_Fecha()
        {
            Cls_Ope_Alm_Fechas_Pago_Datos.Eliminar_Fecha(this);
        }
        #endregion
    }
}
