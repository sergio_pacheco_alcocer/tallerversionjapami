﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Bitacora_Eventos;
using JAPAMI.Constantes;
using JAPAMI.Parametros_Almacen.Datos;
using SharpContent.ApplicationBlocks.Data;

namespace JAPAMI.Parametros_Almacen.Negocio
{
    public class Cls_Cat_Alm_Parametros_Negocio
    {
        #region (Variables Locales)        
            private String Dependencia;
            private String Programa;
            private String Parametros_ID;
            private String Dependencia_ID;
            private String Programa_ID;
        #endregion

        #region Variables Internas
            public String P_Parametros_ID
            {
                get { return Parametros_ID; }
                set { Parametros_ID = value; }
            }
            public String P_Dependencia
            {
                get { return Dependencia; }
                set { Dependencia = value; }
            }
            public String P_Programa
            {
                get { return Programa; }
                set { Programa = value; }
            }
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }
            public String P_Programa_ID
            {
                get { return Programa_ID; }
                set { Programa_ID = value; }
            }
        #endregion

        #region Metodos

        public Cls_Cat_Alm_Parametros_Negocio()
            {
            }                  
            public DataTable Consultar_Parametros()
            {
                return Cls_Cat_Alm_Parametros_Datos.Consultar_Parametros(this);
            }

            public void Alta()
            {
                Cls_Cat_Alm_Parametros_Datos.Alta(this);
            }
            public void Modificar()
            {
                Cls_Cat_Alm_Parametros_Datos.Modificar(this);
            }
            public DataTable Consultar_Dependencias()
            {
                return Cls_Cat_Alm_Parametros_Datos.Consultar_Dependencias(this);
            }
            public DataTable Consultar_Proyectos()
            {
                return Cls_Cat_Alm_Parametros_Datos.Consultar_Proyectos(this);
            }
        #endregion
    }
}