﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Reporte_Alm_Consumo_Stock.Datos;

/// <summary>
/// Summary description for Cls_Rpt_Com_Consumo_Stock_Negocio
/// </summary>
namespace JAPAMI.Reporte_Alm_Consumo_Stock.Negocio
{
    public class Cls_Rpt_Alm_Consumo_Stock_Negocio
    {
        public Cls_Rpt_Alm_Consumo_Stock_Negocio()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #region VARIABLES LOCALES
        private String Grupo_Dependencia_ID;
        private String Nombre_Producto;
        private String Clave_Producto;
        private String Categoria_Producto;
        private String Mes;
        private String Anio;
        private String Producto_ID;
        private String Dependencia_ID;
        private String Nombre_Mes;
        #endregion

        #region VARIABLES PUBLICAS
        public String P_Grupo_Dependencia_ID
        {
            get { return Grupo_Dependencia_ID; }
            set { Grupo_Dependencia_ID = value; }
        }
        public String P_Nombre_Producto
        {
            get { return Nombre_Producto; }
            set { Nombre_Producto = value; }
        }
        public String P_Clave_Producto
        {
            get { return Clave_Producto; }
            set { Clave_Producto = value; }
        }
        public String P_Categoria_Producto
        {
            get { return Categoria_Producto; }
            set { Categoria_Producto = value; }
        }
        public String P_Mes
        {
            get { return Mes; }
            set { Mes = value; }
        }
        public String P_Anio
        {
            get { return Anio; }
            set { Anio = value; }
        }
        public String P_Producto_ID
        {
            get { return Producto_ID; }
            set { Producto_ID = value; }
        }
        public String P_Dependencia_ID
        {
            get { return Dependencia_ID; }
            set { Dependencia_ID = value; }
        }
        public String P_Nombre_Mes
        {
            get { return Nombre_Mes; }
            set { Nombre_Mes = value; }
        }
        #endregion

        #region METODOS
        ///******************************************************************************
        /// NOMBRE DE LA CLASE:     Consultar_Gerencias
        /// DESCRIPCION:            Realizar la consulta de las gerencias o Grupos dependencias
        ///                         registrados en el sistema
        /// PARAMETROS :            
        /// CREO       :            Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO :            09/Octubre/2012 06:54
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public DataTable Consultar_Gerencias()
        {
            return Cls_Rpt_Alm_Consumo_Stock_Datos.Consulta_Gerencias();
        }
        ///******************************************************************************
        /// NOMBRE DE LA CLASE:     Consultar_Unidades_Responsables
        /// DESCRIPCION:           Realizar las dependencias de acuerdo al Grupo_Dependencia 
        ///                         o Gerencia a la que pertenece
        /// PARAMETROS :            
        /// CREO       :            Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO :            09/Octubre/2012 06:54
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public DataTable Consultar_Unidades_Responsables()
        {
            return Cls_Rpt_Alm_Consumo_Stock_Datos.Consulta_Dependencias(this);
        }
        ///******************************************************************************
        /// NOMBRE DE LA CLASE:     Consultar_Categorias
        /// DESCRIPCION:            Realizar la consulta de los categorias registradas en el sistema
        /// PARAMETROS :            
        /// CREO       :            Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO :            09/Octubre/2012 06:54
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public DataTable Consultar_Categorias()
        {
            return Cls_Rpt_Alm_Consumo_Stock_Datos.Consulta_Categorias(this);
        }
        ///******************************************************************************
        /// NOMBRE DE LA CLASE:     Consultar_Productos_Busqueda
        /// DESCRIPCION:            Realizar la consulta de los productos de acuerdo a 
        ///                         los criterios de busqueda
        /// PARAMETROS :            
        /// CREO       :            Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO :            09/Octubre/2012 06:54
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public DataTable Consultar_Productos_Busqueda()
        {
            return Cls_Rpt_Alm_Consumo_Stock_Datos.Consultar_Productos_Busqueda(this);
        }
        ///******************************************************************************
        /// NOMBRE DE LA CLASE:     Consultar_Productos
        /// DESCRIPCION:            Realizar la consulta los productos de stock
        /// PARAMETROS :            
        /// CREO       :            Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO :            09/Octubre/2012 06:54
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public DataTable Consultar_Productos()
        {
            return Cls_Rpt_Alm_Consumo_Stock_Datos.Consulta_Productos();
        }
        ///******************************************************************************
        /// NOMBRE DE LA CLASE:     Consultar_Reporte_Por_Mes_especifico
        /// DESCRIPCION:            Cosnulta las saldas de un producto en un mes especifico
        ///                         y de acuerdo a los criterios de busqueda
        /// PARAMETROS :            
        /// CREO       :            Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO :            11/Octubre/2012 11:50 am
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public DataTable Consultar_Reporte_Por_Mes_especifico()
        {
            return Cls_Rpt_Alm_Consumo_Stock_Datos.Consulta_Reporte_Producto_Entregado_Mes_Especifico(this);
        }
        ///******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Reporte_Salidas_Anual_De_Un_Producto
        /// DESCRIPCION:            Cosnulta las saldas de un producto de cada mes del año tecleado
        /// PARAMETROS :            
        /// CREO       :            Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO :            11/Octubre/2012 05:50 pm
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public DataTable Consulta_Reporte_Salidas_Anual_De_Un_Producto()
        {
            return Cls_Rpt_Alm_Consumo_Stock_Datos.Consulta_Reporte_Salidas_Anual(this);
        }
        ///******************************************************************************
        /// NOMBRE DEL METODO:      Consulta_Detalle_Mes_Especifico
        /// DESCRIPCION:            Cosnulta la cantidad total del producto entregado por el mes,
        ///                         el total gastado por ese producto durante el mes
        ///                         la comparacion de la cantidad con respecto al mes anterior
        /// PARAMETROS :            
        /// CREO       :            Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO :            15/Octubre/2012 12:24 pm
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public DataTable Consulta_Detalle_Mes_Especifico()
        {
            return Cls_Rpt_Alm_Consumo_Stock_Datos.Consulta_Detalle_Salida_Mes_Especifico(this);
        }
        #endregion
    }
}
