﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Requisiciones_Stock.Datos;

/// <summary>
/// Summary description for Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio
/// </summary>
/// 

namespace JAPAMI.Requisiciones_Stock.Negocio
{
    public class Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio
    {
        #region Variables Locales

        private String Tipo_Data_Table;
        private String No_Requisicion;
        private String Fecha_Inicial;
        private String Fecha_Final;
        private String Dependencia_ID;
        private String Area_ID;
        private String Estatus;
        private String Empleado_Recibio_ID;
        private String Empleado_Almacen_ID;
        private String Nombre_Empleado_Almacen;
        private DataTable Dt_Productos_Requisicion;
        private Int64 No_Orden_Salida;
        private String Proyecto_Programa_ID;
        private String Subtotal;
        private String No_Empleado;
        private String Mensaje;
        private String Almacen_General;
        private String Comentarios;
        private String Observaciones_ID;
        private String Historial_ID;
        private String Observaciones;
        private String Clave_Producto;

        public String P_Clave_Producto
        {
            get { return Clave_Producto; }
            set { Clave_Producto = value; }
        }

        public String P_Mensaje
        {
            get { return Mensaje; }
            set { Mensaje = value; }
        }
        public String P_Subtotal
        {
            get { return Subtotal; }
            set { Subtotal = value; }
        }
        private String Iva;

        public String P_Iva
        {
            get { return Iva; }
            set { Iva = value; }
        }
        private String Total;

        public String P_Total
        {
            get { return Total; }
            set { Total = value; }
        }

        public String P_No_Empleado
        {
            get { return No_Empleado; }
            set { No_Empleado = value; }
        }
        
        #endregion

        #region Variables Publicas

        public String P_No_Requisicion
        {
            get { return No_Requisicion; }
            set { No_Requisicion = value; }
        }

        public String P_Fecha_Inicial
        {
            get { return Fecha_Inicial; }
            set { Fecha_Inicial = value; }
        }

        public String P_Fecha_Final
        {
            get { return Fecha_Final; }
            set { Fecha_Final = value; }
        }

        public String P_Dependencia_ID
        {
            get { return Dependencia_ID; }
            set { Dependencia_ID = value; }
        }

        public String P_Area_ID
        {
            get { return Area_ID; }
            set { Area_ID = value; }
        }

        public String P_Tipo_Data_Table
        {
            get { return Tipo_Data_Table; }
            set { Tipo_Data_Table = value; }
        }

        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
       

        public String P_Empleado_Recibio_ID
        {
            get { return Empleado_Recibio_ID; }
            set { Empleado_Recibio_ID = value; }
        }

        public String P_Empleado_Almacen_ID
        {
            get { return Empleado_Almacen_ID; }
            set { Empleado_Almacen_ID = value; }
        }

        public String P_Nombre_Empleado_Almacen
        {
            get { return Nombre_Empleado_Almacen; }
            set { Nombre_Empleado_Almacen = value; }
        }

        public Int64 P_No_Orden_Salida
        {
            get { return No_Orden_Salida; }
            set { No_Orden_Salida = value; }
        }
        public DataTable P_Dt_Productos_Requisicion
        {
            get { return Dt_Productos_Requisicion; }
            set { Dt_Productos_Requisicion = value; }
        }
        public String P_Proyecto_Programa_ID
        {
            get { return Proyecto_Programa_ID; }
            set { Proyecto_Programa_ID = value; }
        }
        public String P_Almacen_General
        {
            get { return Almacen_General; }
            set { Almacen_General = value; }
        }
        public String P_Comentarios
        {
            get { return Comentarios; }
            set { Comentarios = value; }
        }
        public String P_Observaciones_ID
        {
            get { return Observaciones_ID; }
            set { Observaciones_ID = value; }
        }
        public String P_Historial_ID
        {
            get { return Historial_ID; }
            set { Historial_ID = value; }
        }

        public String P_Observaciones
        {
            get { return Observaciones; }
            set { Observaciones = value; }
        }
        #endregion

        #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Existencia_Producto
        ///DESCRIPCIÓN:          Método utilizado para consultar la existencia de algun producto
        ///PARAMETROS:   
        ///CREO:                 Susana Trigueros Armenta
        ///FECHA_CREO:           20/Sep/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Consultar_Existencia_Producto()
        {
            return Cls_Ope_Com_Alm_Requisiciones_Stock_Datos.Consultar_Existencia_Producto(this);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Requisiciones
        ///DESCRIPCIÓN:          Método utilizado para consultar las requisiciones de stock de almacén
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           22/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Consulta_Requisiciones()
        {
            return Cls_Ope_Com_Alm_Requisiciones_Stock_Datos.Consulta_Requisiciones(this);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Detalles_Requisicion
        ///DESCRIPCIÓN:          Método utilizado para consultar la información general de la requisición
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           22/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Consulta_Detalles_Requisicion()
        {
            return Cls_Ope_Com_Alm_Requisiciones_Stock_Datos.Consulta_Detalles_Requisicion(this);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Detalles_Requisicion
        ///DESCRIPCIÓN:          Método utilizado para consultar los productos de las requisiciones de stock de almacén
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           22/Junio/2011  
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Consulta_Productos_Requisicion()
        {
            return Cls_Ope_Com_Alm_Requisiciones_Stock_Datos.Consulta_Productos_Requisicion(this);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_DataTable
        ///DESCRIPCIÓN:          Metodo que manda llamar a su metodo correspondiente de la capa de datos
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           22/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Consultar_DataTable()
        {
            return Cls_Ope_Com_Alm_Requisiciones_Stock_Datos.Consultar_DataTable(this);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Pragrama_Financiamiento
        ///DESCRIPCIÓN:          Metodo que manda llamar a su método correspondiente de la capa de datos
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           23/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Consulta_Pragrama_Financiamiento()
        {
            return Cls_Ope_Com_Alm_Requisiciones_Stock_Datos.Consulta_Pragrama_Financiamiento(this);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Orden_Salida
        ///DESCRIPCIÓN:          Metodo que manda llamar al método donde se da de alta la orden de salida
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           23/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public long Alta_Orden_Salida()
        {
            return Cls_Ope_Com_Alm_Requisiciones_Stock_Datos.Alta_Orden_Salida(this);
        }



        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Informacion_General_OS
        ///DESCRIPCIÓN:          Metodo que manda llamar a su método en el cual se consulta
        ///                      la información general de la orden de salida.
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           24/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Consultar_Informacion_General_OS()
        {
            return Cls_Ope_Com_Alm_Requisiciones_Stock_Datos.Consultar_Informacion_General_OS(this);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Detalles_Orden_Salida
        ///DESCRIPCIÓN:          Método que manda llamar a su método en el cual se consultan
        ///                      los detalles de la orden de salida.
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           24/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Consultar_Detalles_Orden_Salida()
        {
            return Cls_Ope_Com_Alm_Requisiciones_Stock_Datos.Consultar_Detalles_Orden_Salida(this);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Rechazar_Requisicion
        ///DESCRIPCIÓN:          Método que rechaza la requisicion
        ///                      
        ///PARAMETROS:   
        ///CREO:                 David Herrera Rincon
        ///FECHA_CREO:           19/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public String Rechazar_Requisicion()
        {
            return Cls_Ope_Com_Alm_Requisiciones_Stock_Datos.Rechazar_Requisicion(this);
        }

        public DataSet Reporte_Orden_Salida_Stock()
        {
            return Cls_Ope_Com_Alm_Requisiciones_Stock_Datos.Reporte_Orden_Salida_Stock(this);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Observaciones
        ///DESCRIPCIÓN:          Método que manda llamar a su método en el cual se consultan
        ///                      los comentarios
        ///PARAMETROS:   
        ///CREO:                 David Herrera Rincón
        ///FECHA_CREO:           27/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Consulta_Observaciones()
        {
            return Cls_Ope_Com_Alm_Requisiciones_Stock_Datos.Consulta_Observaciones(this);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Observaciones
        ///DESCRIPCIÓN:          Método que manda llamar a su método en el cual se consultan
        ///                      los comentarios
        ///PARAMETROS:   
        ///CREO:                 David Herrera Rincón
        ///FECHA_CREO:           27/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Llenar_Combo(String Consulta)
        {
            return Cls_Ope_Com_Alm_Requisiciones_Stock_Datos.Llenar_Combo(this, Consulta);
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Requisiciones
        ///DESCRIPCIÓN:          Método utilizado para consultar las requisiciones de stock de almacén
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           22/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public int Realizar_Traspaso_Presupuestal_Almacen()
        {
            return Cls_Ope_Com_Alm_Requisiciones_Stock_Datos.Realizar_Traspaso_Presupuestal_Almacen(this);
        }

        #endregion

    }
}