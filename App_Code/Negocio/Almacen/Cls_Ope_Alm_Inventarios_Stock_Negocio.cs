﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Inventarios_De_Stock.Datos;

/// <summary>
/// Summary description for Cls_Ope_Com_Cuadro_Comparativo_Negocio
/// </summary>
/// 
namespace JAPAMI.Inventarios_De_Stock.Negocio
{
    public class Cls_Ope_Alm_Inventarios_Stock_Negocio
    {
        #region VARIABLES INTERNAS
        //Atributos Inventario
        private String Clave_Producto = String.Empty;
        private String Clave_Anterior;
        private String Almacen_ID;
        private String Nombre_Producto;
        private String Partida_ID;
        //Atributos Producto
        private String Producto_ID;
        private String Minimo;
        private String Maximo;
        private String Reorden;
        private String Almacen_General;
        private bool Existencias_Cero;
        private String Ordenar = String.Empty;

        
        #endregion

        #region VARIABLES PUBLICAS

        public bool P_Existencias_Cero
        {
            get { return Existencias_Cero; }
            set { Existencias_Cero = value; }
        }

        public String P_Almacen_General
        {
            get { return Almacen_General; }
            set { Almacen_General = value; }
        }
        public String P_Clave_Producto
        {
            get { return Clave_Producto; }
            set { Clave_Producto = value; }
        }
        public String P_Almacen_ID
        {
            get { return Almacen_ID; }
            set { Almacen_ID = value; }
        }
        public String P_Nombre_Producto
        {
            get { return Nombre_Producto; }
            set { Nombre_Producto = value; }
        }
        public String P_Partida_ID
        {
            get { return Partida_ID; }
            set { Partida_ID = value; }
        }
        public String P_Producto_ID
        {
            get { return Producto_ID; }
            set { Producto_ID = value; }
        }
        public String P_Minimo
        {
            get { return Minimo; }
            set { Minimo = value; }
        }
        public String P_Maximo
        {
            get { return Maximo; }
            set { Maximo = value; }
        }
        public String P_Reorden
        {
            get { return Reorden; }
            set { Reorden = value; }
        }
        public String P_Ordenar
        {
            get { return Ordenar; }
            set { Ordenar = value; }
        }
        public String P_Clave_Anterior
        {
            get { return Clave_Anterior; }
            set { Clave_Anterior = value; }
        } 
        #endregion

        #region VARIABLES METODOS
             
        public double Consultar_Costeo_Inventario()
        {
            return Cls_Ope_Alm_Inventario_Stock_Datos.Consultar_Costeo_Inventario(this);
        }
        public DataTable Consultar_Inventario_Stock()
        {
            return Cls_Ope_Alm_Inventario_Stock_Datos.Consultar_Inventario_Stock(this);
        }
        public DataTable Consultar_Partidas_Stock()
        {
            return Cls_Ope_Alm_Inventario_Stock_Datos.Consultar_Partidas_Stock(this);
        }
        public bool Modificar_Producto()
        {
            return Cls_Ope_Alm_Inventario_Stock_Datos.Modificar_Producto(this);
        }
        #endregion
    }
}