﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Salidas_S_Registro.Datos;
using System.Data;

/// <summary>
/// Summary description for Cls_Ope_Alm_Salidas_Sin_Registro_Negocio
/// </summary>
/// 
namespace JAPAMI.Salidas_S_Registro.Negocio
{
    public class Cls_Ope_Alm_Salidas_Sin_Registro_Negocio
    {
       
        ///*******************************************************************************
        /// VARIABLES INTERNAS 
        ///******************************************************************************
        #region Variables_Internas
        private Int64 No_Orden_Salida;        
        private String Unidad_R_ID;
        private String Clave_UR;       
        private String Partida_ID;
        private String Clave_Partida;        
        private String Empleado_Recibio_ID;
        private String Empleado_Almacen_ID;
        private String Empleado;
        private String Producto;       
        private DataTable Dt_Productos;
        private String Subtotal;
        private String IVA;
        private String Total;
        private String Producto_ID;
        private String Observaciones;      
        

        #endregion

        ///*******************************************************************************
        /// VARIABLES PUBLICAS
        ///*******************************************************************************
        #region Variables_Publicas

        public String P_Observaciones
        {
            get { return Observaciones; }
            set { Observaciones = value; }
        }

        public Int64 P_No_Orden_Salida
        {
            get { return No_Orden_Salida; }
            set { No_Orden_Salida = value; }
        }

        public String P_Producto_ID
        {
            get { return Producto_ID; }
            set { Producto_ID = value; }
        }
        public String P_Producto
        {
            get { return Producto; }
            set { Producto = value; }
        }
        public String P_Empleado
        {
            get { return Empleado; }
            set { Empleado = value; }
        }
        public String P_Clave_Partida
        {
            get { return Clave_Partida; }
            set { Clave_Partida = value; }
        }

        public String P_Clave_UR
        {
            get { return Clave_UR; }
            set { Clave_UR = value; }
        }
        public String P_Total
        {
            get { return Total; }
            set { Total = value; }
        }
        public String P_IVA
        {
            get { return IVA; }
            set { IVA = value; }
        }
        public String P_Subtotal
        {
            get { return Subtotal; }
            set { Subtotal = value; }
        }
        public DataTable P_Dt_Productos
        {
            get { return Dt_Productos; }
            set { Dt_Productos = value; }
        }

        public String P_Empleado_Almacen_ID
        {
            get { return Empleado_Almacen_ID; }
            set { Empleado_Almacen_ID = value; }
        }
        public String P_Empleado_Recibio_ID
        {
            get { return Empleado_Recibio_ID; }
            set { Empleado_Recibio_ID = value; }
        }
        public String P_Partida_ID
        {
            get { return Partida_ID; }
            set { Partida_ID = value; }
        }
        public String P_Unidad_R_ID
        {
            get { return Unidad_R_ID; }
            set { Unidad_R_ID = value; }
        }
        #endregion

        ///*******************************************************************************
        /// METODOS
        ///*******************************************************************************
        #region Metodos

        public DataTable Consultar_Informacion_General_OS()
        {
            return Cls_Ope_Alm_Salidas_Sin_Registro_Datos.Consultar_Informacion_General_OS(this);
        }

        public DataTable Consultar_Detalles_Orden_Salida()
        {
            return Cls_Ope_Alm_Salidas_Sin_Registro_Datos.Consultar_Detalles_Orden_Salida(this);
        }
      

        public DataTable Consulta_UR()
        {
            return Cls_Ope_Alm_Salidas_Sin_Registro_Datos.Consulta_UR(this);
        }
        public DataTable Consulta_Partida()
        {
            return Cls_Ope_Alm_Salidas_Sin_Registro_Datos.Consulta_Partida(this);
        }

        public DataTable Consulta_Empleado()
        {
            return Cls_Ope_Alm_Salidas_Sin_Registro_Datos.Consulta_Empleado(this);
        }
        public DataTable Consulta_Producto()
        {
            return Cls_Ope_Alm_Salidas_Sin_Registro_Datos.Consulta_Producto(this);
        }

        public Int64 Insertar_Salida()
        {
            return Cls_Ope_Alm_Salidas_Sin_Registro_Datos.Insertar_Salida(this);
        }
        #endregion

    }
}
