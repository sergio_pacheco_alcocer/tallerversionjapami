﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Productos_Chatarra.Datos;

/// <summary>
/// Summary description for Cls_Ope_Alm_Productos_Chatarra_Negocio
/// </summary>
namespace JAPAMI.Productos_Chatarra.Negocio
{
    public class Cls_Ope_Alm_Productos_Chatarra_Negocio
    {
        public Cls_Ope_Alm_Productos_Chatarra_Negocio()
        {
        }

        #region Variables Locales
            String Producto_ID;
            String Unidad_ID;
            String Estatus;
            String Tipo;
            String Stock;
            String Clave;
            Int64 Ref_JAPAMI;
            String Tipo_Consulta;
            String Impuesto_ID;
            String Partida_Generica_ID;
            String Partida_Especifica_ID;
            String Nombre;
            Boolean Resguardos;
            Double Subtotal;
            Double IVA;
            Double Total;
            int Cantidad;
            Double Costo;
            Double Costo_Promedio;
            Double Importe;
            Int64 No_Resguardo;
            DateTime Fecha_Inicio;
            DateTime Fecha_Fin;
        #endregion

        #region Variables Publicas
            public String P_Producto_ID
            {
                get { return Producto_ID; }
                set { Producto_ID = value; }
            }

            public String P_Unidad_ID
            {
                get { return Unidad_ID; }
                set { Unidad_ID = value; }
            }

            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }

            public String P_Tipo
            {
                get { return Tipo; }
                set { Tipo = value; }
            }

            public String P_Stock
            {
                get { return Stock; }
                set { Stock = value; }
            }

            public String P_Clave
            {
                get { return Clave; }
                set { Clave = value; }
            }

            public Int64 P_Ref_JAPAMI
            {
                get { return Ref_JAPAMI; }
                set { Ref_JAPAMI = value; }
            }

            public String P_Tipo_Consulta
            {
                get { return Tipo_Consulta; }
                set { Tipo_Consulta = value; }
            }

            public String P_Impuesto_ID
            {
                get { return Impuesto_ID; }
                set { Impuesto_ID = value; }
            }

            public String P_Partida_Generica_ID
            {
                get { return Partida_Generica_ID; }
                set { Partida_Generica_ID = value; }
            }

            public String P_Partida_Especifica_ID
            {
                get { return Partida_Especifica_ID; }
                set { Partida_Especifica_ID = value; }
            }

            public String P_Nombre
            {
                get { return Nombre; }
                set { Nombre = value; }
            }

            public Boolean P_Resguardos
            {
                get { return Resguardos; }
                set { Resguardos = value; }
            }

            public Double P_Subtotal
            {
                get { return Subtotal; }
                set { Subtotal = value; }
            }

            public Double P_IVA
            {
                get { return IVA; }
                set { IVA = value; }
            }

            public Double P_Total
            {
                get { return Total; }
                set { Total = value; }
            }

            public int P_Cantidad
            {
                get { return Cantidad; }
                set { Cantidad = value; }
            }

            public Double P_Costo
            {
                get { return Costo; }
                set { Costo = value; }
            }

            public Double P_Costo_Promedio
            {
                get { return Costo_Promedio; }
                set { Costo_Promedio = value; }
            }

            public Double P_Importe
            {
                get { return Importe; }
                set { Importe = value; }
            }

            public Int64 P_No_Resguardo
            {
                get { return No_Resguardo; }
                set { No_Resguardo = value; }
            }

            public DateTime P_Fecha_Inicio
            {
                get { return Fecha_Inicio; }
                set { Fecha_Inicio = value; }
            }

            public DateTime P_Fecha_Fin
            {
                get { return Fecha_Fin; }
                set { Fecha_Fin = value; }
            }

        #endregion

        #region Metodos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Productos
            ///DESCRIPCION:             Consultar los productos con filtros
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              21/Marzo/2012 13:59
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consulta_Productos()
            {
                return Cls_Ope_Alm_Productos_Chatarra_Datos.Consulta_Productos(this);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Impuestos
            ///DESCRIPCION:             Consultar los impuestos
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              28/Marzo/2012 11:59
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public Double Consulta_Impuestos()
            {
                return Cls_Ope_Alm_Productos_Chatarra_Datos.Consulta_Impuestos(this);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Entrada_Chatarra
            ///DESCRIPCION:             Darle entrada a los productos chatarra
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              28/Marzo/2012 12:31
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public Int64 Entrada_Chatarra()
            {
                return Cls_Ope_Alm_Productos_Chatarra_Datos.Entrada_Chatarra(this);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Consulta_Productos_Simple
            ///DESCRIPCION:             Consultar los datos de un producto particular
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              28/Marzo/2012 13:47
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Consulta_Productos_Simple()
            {
                return Cls_Ope_Alm_Productos_Chatarra_Datos.Consulta_Productos_Simple(this);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Reporte_Productos_Chatarra
            ///DESCRIPCION:             Consultar los productos chatarra
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              28/Marzo/2012 19:00
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Reporte_Productos_Chatarra()
            {
                return Cls_Ope_Alm_Productos_Chatarra_Datos.Reporte_Productos_Chatarra(this);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Reporte_Productos_Chatarra_Detalles
            ///DESCRIPCION:             Consultar los productos chatarra a detalle con las salidas
            ///PARAMETROS:              
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              28/Marzo/2012 16:24
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            public DataTable Reporte_Productos_Chatarra_Detalles()
            {
                return Cls_Ope_Alm_Productos_Chatarra_Datos.Reporte_Productos_Chatarra_Detalles(this);
            }

        #endregion
    }
}