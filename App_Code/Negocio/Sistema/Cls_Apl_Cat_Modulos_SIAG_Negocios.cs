﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Modulos.Datos;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Apl_Cat_Modulos_SIAG_Negocios
/// </summary>
/// 
namespace JAPAMI.Modulos.Negocio
{
    public class Cls_Apl_Cat_Modulos_SIAG_Negocios
    {
        public Cls_Apl_Cat_Modulos_SIAG_Negocios()
        {
        }
        #region (Variables Internas)
        //Propiedades
        private String Modulo_ID;
        private String Nombre;
        private String Nombre_Usuario;

        #endregion

        #region (Variables Publicas)
        public String P_Modulo_ID
        {
            get { return Modulo_ID; }
            set { Modulo_ID = value; }
        }
        public String P_Nombre
        {
            get { return Nombre; }
            set { Nombre = value; }
        }
        public String P_Nombre_Usuario
        {
            get { return Nombre_Usuario; }
            set { Nombre_Usuario = value; }
        }

        #endregion

        #region (Metodos)
        public void Alta_Modulo()
        {
            Cls_Apl_Cat_Modulos_SIAG_Datos.Alta_Modulo(this);
        }
        public void Modificar_Modulo()
        {
            Cls_Apl_Cat_Modulos_SIAG_Datos.Modificar_Modulo(this);
        }
        public void Eliminar_Modulo()
        {
            Cls_Apl_Cat_Modulos_SIAG_Datos.Eliminar_Modulo(this);
        }
        public DataTable Consulta_Modulo()
        {
            return Cls_Apl_Cat_Modulos_SIAG_Datos.Consulta_Modulo(this);
        }
        public DataTable Consulta_Modulo_By_Name()
        {
            return Cls_Apl_Cat_Modulos_SIAG_Datos.Consulta_Modulo_By_Nombre(this);
        }
        #endregion
    }
}
