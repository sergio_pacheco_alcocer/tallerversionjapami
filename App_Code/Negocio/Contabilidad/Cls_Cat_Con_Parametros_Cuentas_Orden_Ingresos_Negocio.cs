﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
using System.Data;
using JAPAMI.Parametros_Cuentas_Orden_Ingresos.Datos;

namespace JAPAMI.Parametros_Cuentas_Orden_Ingresos.Negocio
{
    public class Cls_Cat_Con_Parametros_Cuentas_Orden_Ingresos_Negocio
    {
        #region Variables Privadas

            private String ID;
            private String Momento_Presupuestal;
            private String Cuenta_Contable_ID;
            private String Descripcion;

        #endregion

        #region Variables Publicas

            public String P_ID
            {
                get { return ID; }
                set { ID = value; }
            }
            public String P_Momento_Presupuestal
            {
                get { return Momento_Presupuestal; }
                set { Momento_Presupuestal = value; }
            }
            public String P_Cuenta_Contable_ID
            {
                get { return Cuenta_Contable_ID; }
                set { Cuenta_Contable_ID = value; }
            }
            public String P_Descripcion
            {
                get { return Descripcion; }
                set { Descripcion = value; }
            }

        #endregion

        #region Metodos

            public DataTable Consultar_Cuentas_Orden()
            {
                return Cls_Cat_Con_Parametros_Cuentas_Orden_Ingresos_Datos.Consultar_Cuentas_Orden(this);
            }
            public void Modificar_Cuenta_Orden()
            {
                Cls_Cat_Con_Parametros_Cuentas_Orden_Ingresos_Datos.Modificar_Cuenta_Orden(this);
            }
            public void Crear_Cuenta_Orden()
            {
                Cls_Cat_Con_Parametros_Cuentas_Orden_Ingresos_Datos.Crear_Cuenta_Orden(this);
            }

        #endregion
    }
}
