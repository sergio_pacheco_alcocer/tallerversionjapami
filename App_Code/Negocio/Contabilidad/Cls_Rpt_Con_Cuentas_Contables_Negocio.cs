﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Reporte_Cuentas_Contables.Datos;

/// <summary>
/// Summary description for Cls_Rpt_Con_Cuentas_Contables_Negocio
/// </summary>
namespace JAPAMI.Reporte_Cuentas_Contables.Negocio
{
    public class Cls_Rpt_Con_Cuentas_Contables_Negocio
    {
        public Cls_Rpt_Con_Cuentas_Contables_Negocio()
        {
        }

        #region (Variables Locales)
        private String Nivel_ID;
        private String Tipo_Cuenta;
        private String Partida_ID;
        private String Afectable;
        private String Dependencia_ID;
        #endregion

        #region (Variables Publicas)
        public String P_Nivel_ID
        {
            get { return Nivel_ID; }
            set { Nivel_ID = value; }
        }

        public String P_Tipo_Cuenta
        {
            get { return Tipo_Cuenta; }
            set { Tipo_Cuenta = value; }
        }

        public String P_Partida_ID
        {
            get { return Partida_ID; }
            set { Partida_ID = value; }
        }

        public String P_Afectable
        {
            get { return Afectable; }
            set { Afectable = value; }
        }

        public String P_Dependencia_ID
        {
            get { return Dependencia_ID; }
            set { Dependencia_ID = value; }
        }
        #endregion

        #region (Metodos)
        /// *************************************************************************************
        /// NOMBRE:              Consulta_Cuentas_Contables
        /// DESCRIPCION:         Consultar las cuentas contables respecto a diversos filtros
        /// PARÁMETROS:          
        /// USUARIO CREO:        Noe Mosqueda Valadez
        /// FECHA CREO:          20/Abril/2012 11:52
        /// USUARIO MODIFICO:    
        /// FECHA MODIFICO:      
        /// CAUSA MODIFICACION:  
        /// *************************************************************************************
        public DataTable Consulta_Cuentas_Contables()
        {
            return Cls_Rpt_Con_Cuentas_Contables_Datos.Consulta_Cuentas_Contables(this);
        }
        #endregion
    }
}