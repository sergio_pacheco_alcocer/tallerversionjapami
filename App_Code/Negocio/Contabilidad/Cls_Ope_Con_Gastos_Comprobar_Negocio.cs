﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Gastos_Comprobar.Datos;

namespace JAPAMI.Gastos_Comprobar.Negocio
{
    public class Cls_Ope_Con_Gastos_Comprobar_Negocio
    {
        #region(Variables Privadas)
        private String Estatus;
        private String No_Solicitud;
        private String No_Reserva;
        private String Partida_ID;
        private String Monto_Aprobado;
        private String Usuario_Aprobo;
        private DataTable Dt_Documentos_Comprobados;
        private DataTable Dt_Partidas_Poliza;
        private DataTable Dt_Deudas;
        private String Usuario_Creo;
        private String Tipo_Documento;
        private SqlCommand Cmmd;
        private String Partidas;
        #endregion

        #region Variables publicas
        public String P_No_Solicitud
        {
            get { return No_Solicitud; }
            set { No_Solicitud = value; }
        }
        public String P_Partidas
        {
            get { return Partidas; }
            set { Partidas = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_No_Reserva
        {
            get { return No_Reserva; }
            set { No_Reserva = value; }
        }
        public String P_Partida_ID
        {
            get { return Partida_ID; }
            set { Partida_ID = value; }
        }
        public DataTable P_Dt_Partidas_Poliza
        {
            get { return Dt_Partidas_Poliza; }
            set { Dt_Partidas_Poliza = value; }
        }
        public DataTable P_Dt_Deudas
        {
            get { return Dt_Deudas; }
            set { Dt_Deudas = value; }
        }
        public String P_Monto_Aprobado
        {
            get { return Monto_Aprobado; }
            set { Monto_Aprobado = value; }
        }
        public String P_Usuario_Aprobo
        {
            get { return Usuario_Aprobo; }
            set { Usuario_Aprobo = value; }
        }
        public String P_Usuario_Creo
        {
            get { return Usuario_Creo; }
            set { Usuario_Creo = value; }
        }
        public String P_Tipo_Documento
        {
            get { return Tipo_Documento; }
            set { Tipo_Documento = value; }
        }
        public DataTable P_Dt_Documentos_Comprobados
        {
            get { return Dt_Documentos_Comprobados; }
            set { Dt_Documentos_Comprobados = value; }
        }

        public SqlCommand P_Cmmd
        {
            get { return Cmmd; }
            set { Cmmd = value; }
        }
        #endregion

        #region Metodos
        public DataTable Consultar_Estatus()
        {
            return Cls_Ope_Con_Gastos_Comprobar_Datos.Consultar_Estatus(this);
        }
        public DataTable Consultar_Reserva_Patida()
        {
            return Cls_Ope_Con_Gastos_Comprobar_Datos.Consultar_Reserva_Patida(this);
        }
        public DataTable Consultar_Nombre_Patida()
        {
            return Cls_Ope_Con_Gastos_Comprobar_Datos.Consultar_Nombre_Patida(this);
        }
        public DataTable Consultar_Cuenta_Banco()
        {
            return Cls_Ope_Con_Gastos_Comprobar_Datos.Consultar_Cuenta_Banco(this);
        }
        public Boolean Cambiar_Datos_Comprobacion_Solicitud_Pago()
        {
            return Cls_Ope_Con_Gastos_Comprobar_Datos.Cambiar_Datos_Comprobacion_Solicitud_Pago(this);
        }
        public void Alta_Documentos_Comprobados()
        {
            Cls_Ope_Con_Gastos_Comprobar_Datos.Alta_Documentos_Comprobados(this);
        }
        public String Alta_Comprobacion_Gastos()
        {
            return Cls_Ope_Con_Gastos_Comprobar_Datos.Alta_Comprobacion_Gastos(this);
        }
        public String Consultar_No_Deuda()
        {
            return Cls_Ope_Con_Gastos_Comprobar_Datos.Consultar_No_Deuda(this);
        }
        #endregion
    }
}
