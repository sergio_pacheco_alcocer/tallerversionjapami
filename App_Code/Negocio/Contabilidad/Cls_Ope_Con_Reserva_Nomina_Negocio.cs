﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Generar_Reserva_Nomina.Datos;

/// <summary>
/// Summary description for Cls_Ope_Con_Reservas_Negocio
/// </summary>
/// 

namespace JAPAMI.Generar_Reserva_Nomina.Negocio
{
    public class Cls_Ope_Con_Reserva_Nomina_Negocio
    {
        #region Variables Privadas
            private String Beneficiario;
            private String Dependencia_ID;
            private String Area_ID;
            private String Proyecto_Programa_ID;
            private String Folio;
            private String Estatus;
            private String Comentarios;
            private String Fecha_Inicial;
            private String Fecha_Final;
            private String No_Reserva;
            private String Fuente_Financiamiento;
            private String Importe;
            private String Usuario_Modifico;
            private String Partida_ID;
            private String Ramo_33;
            private String Anio_Presupuesto;
            private String Tipo_Reserva;
            private String Grupo_Dependencia;
            private SqlCommand Cmmd;
            private String Filtro_Campo_Mes;
            private String No_Deuda;
            public String P_Tipo_Psp { set; get;}
            public DataTable P_Dt_Detelles { set; get; }
            public String P_Tipo_Modificacion { set; get; }
        #endregion 

        #region Variables Publicas

            public String P_Ramo_33
            {
                get { return Ramo_33; }
                set { Ramo_33 = value; }
            }
            public String P_Partida_ID
            {
                get{return Partida_ID;}
                set{Partida_ID = value;}
            }
            public String P_Anio_Presupuesto
            {
                get { return Anio_Presupuesto; }
                set { Anio_Presupuesto = value; }
            }

            public String P_Usuario_Modifico 
            {
                get { return Usuario_Modifico; }
                set { Usuario_Modifico = value; }
            } 
            public String P_Importe
            {
                get { return Importe; }
                set { Importe = value; }
            }
            public String P_Beneficiario
            {
                get { return Beneficiario; }
                set { Beneficiario = value; }
            }
            public String P_No_Reserva
            {
                get { return No_Reserva; }
                set { No_Reserva = value; }
            }
            public String P_Fuente_Financiamiento
            {
                get { return Fuente_Financiamiento; }
                set { Fuente_Financiamiento = value; }
            }

            public String P_Fecha_Inicial
            {
                get { return Fecha_Inicial; }
                set { Fecha_Inicial = value; }
            }
            public String P_Fecha_Final
            {
                get { return Fecha_Final; }
                set { Fecha_Final = value; }
            }

            public String P_Comentarios
            {
                get { return Comentarios; }
                set { Comentarios = value; }
            }

            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }

            public String P_Folio
            {
                get { return Folio; }
                set { Folio = value; }
            }
            public String P_Proyecto_Programa_ID
            {
                get { return Proyecto_Programa_ID; }
                set { Proyecto_Programa_ID = value; }
            }
            public String P_Area_ID
            {
                get { return Area_ID; }
                set { Area_ID = value; }
            }
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }
            public String P_Tipo_Reserva
            {
                get { return Tipo_Reserva; }
                set { Tipo_Reserva = value; }
            }

            public String P_Grupo_Dependencia
            {
                get { return Grupo_Dependencia; }
                set { Grupo_Dependencia = value; }
            }

            public SqlCommand P_Cmmd
            {
                get { return Cmmd; }
                set { Cmmd = value; }
            }


            public String P_Filtro_Campo_Mes
            {
                get { return Filtro_Campo_Mes; }
                set { Filtro_Campo_Mes = value; }
            }

            public String P_No_Deuda
            {
                get { return No_Deuda; }
                set { No_Deuda = value; }
            }

        #endregion

        #region Metodos

        public DataTable Consultar_Proyectos_Programas() 
        {
            return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Proyectos_Programas(this);
        }

        public DataTable Consultar_Partidas_De_Un_Programa() 
        {
            return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Partidas_De_Un_Programa(this);
        }

        public DataTable Consultar_Reservas()
        {
            return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Reservas(this);
        }

        public DataTable Consultar_Fuentes_Financiamiento()
        {
            return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Fuentes_Financiamiento(this);
        }

        public DataTable Consultar_Presupuesto_Partidas()
        {
            return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Presupuesto_Partidas(this);
        }

        public void Modificar_Reserva()
        {
            Cls_Ope_Con_Reserva_Nomina_Datos.Modificar_Reserva(this);
        }

        public DataTable Consultar_Reservas_Detalladas() 
        {
            return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Reservas_Detalladas(this);
        }

        public DataTable Consultar_Reservas_Unidad_Responsable()
        {
            return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Reservas_Unidad_Responsable(this);
        }

        public Boolean Modificar_Tipo_Reserva()
        {
            return Cls_Ope_Con_Reserva_Nomina_Datos.Modificar_Tipo_Reserva(this);
        }

        //Se utiliza en el formulario de servicios generales
        public DataTable Consultar_Programas_Dependencia()
        {
            return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Programas_Dependencia(this);
        }

        public DataTable Consultar_Dependencia_Partida()
        {
            return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Dependencia_Partida(this);
        }

        public DataTable Consultar_Disponible_Partida()
        {
            return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Disponible_Partida(this);
        }
        public String  Consultar_Reserva_Nomina_Anio()
        {
            return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Reserva_Nomina_Anio(this);
        }
        #endregion 

        #region (Metodos Reserva Nomina)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Dependencias
            ///DESCRIPCIÓN          : Metodo para obtener las dependencias que contienes presupuesto disponible o comprometido
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Marzo/2013
            ///*********************************************************************************************************
            public DataTable Consultar_Dependencias() 
            {
                return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Dependencias(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Anio_Presupuesto
            ///DESCRIPCIÓN          : Metodo para obtener los años presupuestados
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Marzo/2013
            ///*********************************************************************************************************
            public DataTable Consultar_Anio_Presupuesto()
            {
                return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Anio_Presupuesto();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Reserva_Nomina
            ///DESCRIPCIÓN          : Metodo para obtener los datos de la reserva de nomina
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Marzo/2013
            ///*********************************************************************************************************
            public DataTable Consultar_Reserva_Nomina()
            {
                return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Reserva_Nomina();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Afectar_Presupuesto_Nomina
            ///DESCRIPCIÓN          : Metodo para obtener afectar los datos del presupuesto de la reserva de nomina
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 29/Marzo/2013
            ///*********************************************************************************************************
            public int Afectar_Presupuesto_Nomina()
            {
                return Cls_Ope_Con_Reserva_Nomina_Datos.Afectar_Presupuesto_Nomina(this);
            }

            public DataTable Consultar_Reservas_Nomina_Detalladas()
            {
                return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Reservas_Nomina_Detalladas(this);
            }

            public void Modificar_Saldos_Reserva()
            {
                Cls_Ope_Con_Reserva_Nomina_Datos.Modificar_Saldos_Reserva(this);
            }

            public Double Cancelar_Reserva_Nomina(String No_Reserva, SqlCommand P_Comando, DataTable Dt_Detalles, String Anio)
            {
                return  Cls_Ope_Con_Reserva_Nomina_Datos.Cancelar_Reserva_Nomina(No_Reserva, P_Comando, Dt_Detalles, Anio);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Programa
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los programas 
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Septiembre/2013
            ///*********************************************************************************************************
            public DataTable Consultar_Programa()
            {
                return Cls_Ope_Con_Reserva_Nomina_Datos.Consultar_Programa(this);
            }
        #endregion
    }
}
