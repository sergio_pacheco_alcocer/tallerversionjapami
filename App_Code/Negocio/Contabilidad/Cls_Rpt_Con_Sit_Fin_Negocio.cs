﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Datos;

namespace JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Negocio
{
    public class Cls_Rpt_Con_Sit_Fin_Negocio
    {
        #region Variables Internas
        private String Mes_Año;
        private String Mes_Año_Ant;
        private String Año;
        private String filtro;
        private String Filtro1;
        private String Filtro2;
        private String Filtro3;
        private String Filtro4;
        private String Filtro5;
        private String Filtro6;
        private String Filtro7;
        private String Filtro8;
        #endregion

        #region Variables Publicas
        public String P_Mes_Año
        {
            get { return Mes_Año; }
            set { Mes_Año = value; }
        }
        public String P_Mes_Año_Ant
        {
            get { return Mes_Año_Ant; }
            set { Mes_Año_Ant = value; }
        }
        public String P_filtro
        {
            get { return filtro; }
            set { filtro = value; }
        }
        public String P_Año
        {
            get { return Año; }
            set { Año = value; }
        }
        public String P_Filtro1
        {
            get { return Filtro1; }
            set { Filtro1 = value; }
        }
        public String P_Filtro2
        {
            get { return Filtro2; }
            set { Filtro2 = value; }
        }
        public String P_Filtro3
        {
            get { return Filtro3; }
            set { Filtro3 = value; }
        }
        public String P_Filtro4
        {
            get { return Filtro4; }
            set { Filtro4 = value; }
        }
        public String P_Filtro5
        {
            get { return Filtro5; }
            set { Filtro5 = value; }
        }
        public String P_Filtro6
        {
            get { return Filtro6; }
            set { Filtro6 = value; }
        }
        public String P_Filtro7
        {
            get { return Filtro7; }
            set { Filtro7 = value; }
        }
        public String P_Filtro8
        {
            get { return Filtro8; }
            set { Filtro8 = value; }
        }
        #endregion

        #region Metodos
        public DataTable Consulta_Meses_Cerrados()
        {
            return Cls_Rpt_Con_Sit_Fin_Datos.Consulta_Meses_Cerrados(this);
        }
        public DataTable Consulta_Anios()
        {
            return Cls_Rpt_Con_Sit_Fin_Datos.Consulta_Anios(this);
        }
        public DataTable Consulta_Situacion_Financiera()
        {
            return Cls_Rpt_Con_Sit_Fin_Datos.Consulta_Situacion_Financiera(this);
        }
        public DataTable Consulta_Situacion_Financiera_Acumulado()
        {
            return Cls_Rpt_Con_Sit_Fin_Datos.Consulta_Situacion_Financiera_Acumulado(this);
        }
        public DataTable Consulta_Situacion_Financiera_Activo_Acumulado()
        {
            return Cls_Rpt_Con_Sit_Fin_Datos.Consulta_Situacion_Financiera_Activo_Acumulado(this);
        }
        public DataTable Consulta_Cuentas()
        {
            return Cls_Rpt_Con_Sit_Fin_Datos.Consulta_Cuentas(this);
        }
        public DataTable Consulta_Cuentas_Tercer_Nivel()
        {
            return Cls_Rpt_Con_Sit_Fin_Datos.Consulta_Cuentas_Tercer_Nivel(this);
        }
        public DataTable Consulta_Situacion_Financiera2()
        {
            return Cls_Rpt_Con_Sit_Fin_Datos.Consulta_Situacion_Financiera2(this);
        }
        public DataTable Consulta_Cuentas2()
        {
            return Cls_Rpt_Con_Sit_Fin_Datos.Consulta_Cuentas2(this);
        }
        public DataTable Consulta_Situacion_Financiera_Activo()
        {
            return Cls_Rpt_Con_Sit_Fin_Datos.Consulta_Situacion_Financiera_Activo(this);
        }
        public DataTable Consulta_Movimientos_Contables()
        {
            return Cls_Rpt_Con_Sit_Fin_Datos.Consulta_Movimientos_Contables(this);
        }
        #endregion
    }
}
