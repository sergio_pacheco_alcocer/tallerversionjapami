﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Matriz.Datos;

namespace JAPAMI.Matriz.Negocio
{
    public class Cls_Cat_Con_Matriz_Negocio
    {
        #region (Variables_Internas)
        private String Matriz_ID;
        private String No_Matriz;
        private String Cuenta_Abono;
        private String Cuenta_Cargo;
        private String Nombre;
        private String Tipo_Matriz;
        private String Tipo_Gasto;
        private String Usuario_Creo;
        private String Usuario_Modifico;
        private DataTable Datos_Matriz_Cuenta;
        private String Nivel;
        #endregion

        #region (Variables_Publicas)

        public String P_Nombre
        {
            get { return Nombre; }
            set { Nombre = value; }
        }

        public String P_Matriz_ID
        {
            get { return Matriz_ID; }
            set { Matriz_ID = value; }
        }

        public String P_No_Matriz
        {
            get { return No_Matriz; }
            set { No_Matriz = value; }
        }

        public String P_Cuenta_Abono
        {
            get { return Cuenta_Abono; }
            set { Cuenta_Abono = value; }
        }

        public String P_Cuenta_Cargo
        {
            get { return Cuenta_Cargo; }
            set { Cuenta_Cargo = value; }
        }

        public String P_Tipo_Matriz
        {
            get { return Tipo_Matriz; }
            set { Tipo_Matriz = value; }
        }

        public String P_Tipo_Gasto
        {
            get { return Tipo_Gasto; }
            set { Tipo_Gasto = value; }
        }

        public String P_Usuario_Creo
        {
            get { return Usuario_Creo; }
            set { Usuario_Creo = value; }
        }

        public String P_Usuario_Modifico
        {
            get { return Usuario_Modifico; }
            set { Usuario_Modifico = value; }
        }

        public DataTable P_Datos_Matriz_Cuenta
        {
            get { return Datos_Matriz_Cuenta; }
            set { Datos_Matriz_Cuenta = value; }
        }

        public String P_Nivel
        {
            get { return Nivel; }
            set { Nivel = value; }
        }
        #endregion

        #region (Metodos)

        public void Alta_Cuenta_Matriz()
        {
            Cls_Cat_Con_Matriz_Datos.Alta_Cuenta_Matriz(this);
        }

        public void Eliminar_Cuenta_Matriz()
        {
            Cls_Cat_Con_Matriz_Datos.Eliminar_Cuenta_Matriz(this);
        }

        public void Actualizar_Cuenta_Matriz()
        {
            Cls_Cat_Con_Matriz_Datos.Actualizar_Cuenta_Matriz(this);
        }

        public DataTable Consulta_Matriz()
        {
            return Cls_Cat_Con_Matriz_Datos.Consultar_Matriz(this);
        }

        public DataTable Consulta_Cuenta()
        {
            return Cls_Cat_Con_Matriz_Datos.Consultar_Cuentas_Contables(this);
        }

        #endregion
    }
}