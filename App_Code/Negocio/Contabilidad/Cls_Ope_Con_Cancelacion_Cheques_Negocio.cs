﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Ope_Con_Cancelacion_Cheques.Datos;

/// <summary>
/// Summary description for Cls_Ope_Con_Cancelacion_Cheques_Negocio
/// </summary>
namespace JAPAMI.Ope_Con_Cancelacion_Cheques.Negocio
{
    public class Cls_Ope_Con_Cancelacion_Cheques_Negocio
    {
        public Cls_Ope_Con_Cancelacion_Cheques_Negocio()
        {
        }
        #region Variables Locales
        private String Fecha_Emision_Inicio;
        private String Fecha_Emision_Fin;
        private String Tipo_Beneficiario;
        private String Beneficiario;
        private String Folio_Cheque;
        private String No_Cheque_ID;
        private String Estatus_Entrada;
        private String Estatus_Salida;
        private String Comentarios;
        private DataTable Dt_Cheques;
        #endregion

        #region Variables Publicas
        public String P_Fecha_Emision_Inicio 
        {
            get { return Fecha_Emision_Inicio; }
            set { Fecha_Emision_Inicio = value; }
        }
        public String P_Fecha_Emision_Fin
        {
            get { return Fecha_Emision_Fin; }
            set { Fecha_Emision_Fin = value; }
        }
        public String P_Tipo_Beneficiario
        {
            get { return Tipo_Beneficiario; }
            set { Tipo_Beneficiario = value; }
        }
        public String P_Beneficiario
        {
            get { return Beneficiario; }
            set { Beneficiario = value; }
        }
        public String P_Folio_Cheque
        {
            get { return Folio_Cheque; }
            set { Folio_Cheque = value; }
        }
        public String P_No_Cheque_ID
        {
            get { return No_Cheque_ID; }
            set { No_Cheque_ID = value; }
        }
        public String P_Estatus_Entrada
        {
            get { return Estatus_Entrada; }
            set { Estatus_Entrada = value; }
        }
        public String P_Estatus_Salida
        {
            get { return Estatus_Salida; }
            set { Estatus_Salida = value; }
        }
        public String P_Comentarios
        {
            get { return Comentarios; }
            set { Comentarios = value; }
        }
        public DataTable P_Dt_Cheques   
        {
            get { return Dt_Cheques; }
            set { Dt_Cheques = value; }
        }
        #endregion

        #region Metodos
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos_Beneficiario
        ///DESCRIPCIÓN          : Consulta los tipos de beneficiario que hay
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 21/Diciembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        public DataTable Consultar_Tipos_Beneficiario()
        {
            return Cls_Ope_Con_Cancelacion_Cheques_Datos.Consultar_Tipos_Beneficiarios();
        }
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Beneficiarios
        ///DESCRIPCIÓN          : Consulta los beneficiarios de acuerdo al nombre
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 21/Diciembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        public DataTable Consultar_Beneficiarios()
        {
            return Cls_Ope_Con_Cancelacion_Cheques_Datos.Consultar_Beneficiarios(this);
        }
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Lista_De_Cheques
        ///DESCRIPCIÓN          : Consulta los cheques para enlistarlos
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 21/Diciembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        public DataTable Consultar_Lista_De_Cheques()
        {
            return Cls_Ope_Con_Cancelacion_Cheques_Datos.Consultar_Cheques_Listado(this);
        }        
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Rechazar_Cheque
        ///DESCRIPCIÓN          : rechaza el cheque seleccionado
        ///PROPIEDADES          :
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 26/Diciembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        public String Cancelar_Cheque()
        {
            return Cls_Ope_Con_Cancelacion_Cheques_Datos.Cancelar_Cheque(this);
        }
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Rechazar_Cheque_Masiva
        ///DESCRIPCIÓN          : rechaza el cheque seleccionado
        ///PROPIEDADES          :
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 12/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        public String Rechazar_Cheque_Masiva()
        {
            return Cls_Ope_Con_Cancelacion_Cheques_Datos.Rechazar_Cheque_Masiva(this);
        }
        #endregion
    }
}
