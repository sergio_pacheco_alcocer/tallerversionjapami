﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Reporte_Proveedores_Pagos.Datos;

namespace JAPAMI.Reporte_Proveedores_Pagos.Negocio
{
    public class Cls_Rpt_Con_Proveedores_Pagos_Negocio
    {
        #region Variables Internas
        private String Proveedor_ID;
        private String Fecha_Inicial;
        private String Fecha_Final;

        #endregion

        #region Variables Publicas
        public String P_Proveedor_ID
        {
            get { return Proveedor_ID; }
            set { Proveedor_ID = value; }
        }
        public String P_Fecha_Inicial
        {
            get { return Fecha_Inicial; }
            set { Fecha_Inicial = value; }
        }
        public String P_Fecha_Final
        {
            get { return Fecha_Final; }
            set { Fecha_Final = value; }
        }
        #endregion

        #region Metodos
        public DataTable Consulta_Proveedores_Pago()
        {
            return Cls_Rpt_Con_Proveedores_Pagos_Datos.Consulta_Proveedores_Pago(this);
        }
        #endregion
    }
}
