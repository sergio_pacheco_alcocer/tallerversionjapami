﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Reporte_Polizas.Datos;

/// <summary>
/// Summary description for Cls_Ope_Con_Reporte_Polizas_Negocio
/// </summary>
/// 
namespace JAPAMI.Reporte_Polizas.Negocios
{
    public class Cls_Ope_Con_Reporte_Polizas_Negocio
    {
        ///*******************************************************************************
        /// VARIABLES INTERNAS 
        ///******************************************************************************
        #region Variables_Internas

        private String Fecha_Inicial;
        private String Fecha_Fin;
        private String Unidad_Responsable_ID;
        private String Cuenta_Contable;
        private String Concepto;
        private String No_Poliza;
        private String Mes_Ano;
        private String Tipo_Poliza_ID;


        #endregion

        ///*******************************************************************************
        /// VARIABLES PUBLICAS
        ///*******************************************************************************
        #region Variables_Publicas

        public String P_Tipo_Poliza_ID
        {
            get { return Tipo_Poliza_ID; }
            set { Tipo_Poliza_ID = value; }
        }

        public String P_Mes_Ano
        {
            get { return Mes_Ano; }
            set { Mes_Ano = value; }
        }
        
        public String P_No_Poliza
        {
            get { return No_Poliza; }
            set { No_Poliza = value; }
        }

        public String P_Fecha_Inicial
        {
            get { return Fecha_Inicial; }
            set { Fecha_Inicial = value; }
        }

        public String P_Fecha_Fin
        {
            get { return Fecha_Fin; }
            set { Fecha_Fin = value; }
        }
        public String P_Unidad_Responsable_ID
        {
            get { return Unidad_Responsable_ID; }
            set { Unidad_Responsable_ID = value; }
        }
        public String P_Cuenta_Contable
        {
            get { return Cuenta_Contable; }
            set { Cuenta_Contable = value; }
        }

        public String P_Concepto
        {
            get { return Concepto; }
            set { Concepto = value; }
        }

        #endregion


        ///*******************************************************************************
        /// METODOS
        ///*******************************************************************************
        #region Metodos

        public DataTable Consulta_Unidad_Responsable()
        {
            return Cls_Ope_Con_Reporte_Polizas_Datos.Consulta_Unidad_Responsable(this);
        }

        public DataTable Consultar_Polizas()
        {
            return Cls_Ope_Con_Reporte_Polizas_Datos.Consultar_Polizas(this);
        }

        public DataTable Consultar_Detalle_Polizas()
        {
            return Cls_Ope_Con_Reporte_Polizas_Datos.Consultar_Detalle_Polizas(this);
        }

        #endregion
    }
}