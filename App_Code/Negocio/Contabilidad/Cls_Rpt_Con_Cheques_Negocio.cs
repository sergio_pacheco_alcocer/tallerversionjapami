﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Reporte_Cheques.Datos;

/// <summary>
/// Summary description for Cls_Rpt_Con_Cheques_Negocio
/// </summary>
namespace JAPAMI.Reporte_Cheques.Negocio
{
    public class Cls_Rpt_Con_Cheques_Negocio
    {
        public Cls_Rpt_Con_Cheques_Negocio()
        {
        }

        #region (Variables Internas)
        private String Banco_ID;
        private String Estatus;
        private DateTime Fecha_Inicio;
        private DateTime Fecha_Fin;
        private String Tipo;
        private String Busqueda;
        private String Empleado_ID;
        private String Proveedor_ID;
        #endregion

        #region (Variables Publicas)
        public String P_Banco_ID
        {
            get { return Banco_ID; }
            set { Banco_ID = value; }
        }

        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }

        public DateTime P_Fecha_Inicio
        {
            get { return Fecha_Inicio; }
            set { Fecha_Inicio = value; }
        }

        public DateTime P_Fecha_Fin
        {
            get { return Fecha_Fin; }
            set { Fecha_Fin = value; }
        }

        public String P_Tipo
        {
            get { return Tipo; }
            set { Tipo = value; }
        }

        public String P_Busqueda
        {
            get { return Busqueda; }
            set { Busqueda = value; }
        }

        public String P_Empleado_ID
        {
            get { return Empleado_ID; }
            set { Empleado_ID = value; }
        }

        public String P_Proveedor_ID
        {
            get { return Proveedor_ID; }
            set { Proveedor_ID = value; }
        }
        #endregion

        #region (Metodos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Cheques
        ///DESCRIPCION:             Consulta de los cheques con diversos filtros
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              17/Abril/2012 16:35
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public DataTable Consulta_Cheques()
        {
            return Cls_Rpt_Con_Cheques_Datos.Consulta_Cheques(this);
        }
        #endregion
    }
}