﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Catalogo_Con_Proveedores.Datos;


namespace JAPAMI.Catalogo_Con_Proveedores.Negocio
{

    public class Cls_Cat_Con_Proveedores_Negocio
    {
        public Cls_Cat_Con_Proveedores_Negocio()
        {
        }
        //Priopiedades
        private String Proveedor_ID;
        private String Razon_Social;
        private String Nombre_Comercial;
        private String RFC;
        private String Contacto;
        private String Estatus;
        private String Direccion;
        private String Colonia;
        private String Ciudad;
        private String Estado;
        private int CP;
        private String Telefono_1;
        private String Telefono_2;
        private String Nextel;
        private String Fax;
        private String Correo_Electronico;
        private String Password;
        private String Tipo_Pago;
        private int Dias_Credito;
        private String Forma_Pago;
        private String Comentarios;
        private String Cuenta;
        private String Nombre_Usuario;
        private String Busqueda;
        private String Prueba;
        private String Usuario;
        private String Concepto_ID;
        private DataTable Dt_Partidas_Proveedor;
        private String Actualizacion;
        private String Fecha_Actualizacion;
        private String Representante_Legal;
        private String Tipo_Persona_Fiscal;
        private String Tipo;
        private DataTable Dt_Conceptos_Proveedor;
        private String Cuenta_Acreedor_ID;
        private String Cuenta_Deudor_ID;
        private String Cuenta_Proveedor_ID;
        private String Cuenta_Contratista_ID;
        private String Cuenta_Predial_ID;
        private String Cuenta_Judicial_ID;
        private String Cuenta_Nomina_ID;
        private String Cuenta_Anticipo_Deudor_ID;
        private bool Nueva_Actualizacion;
        private String Banco_Proveedor_ID;
        private String Banco_ID;
        private String Clabe;
        private String Banco_Proveedor;
        private String CURP;
        private String Filtro_Dinamico;
        private String Sucursal_Banco_Proveedor;
        private String E_Mail_Transferencia;
        private string Tipo_Cuenta;
        private String Proveedor_Bancario;

        public String P_Filtro_Dinamico
        {
            get { return Filtro_Dinamico; }
            set { Filtro_Dinamico = value; }
        }
        public String P_Clabe
        {
            get { return Clabe; }
            set { Clabe = value; }
        }
        public String P_Banco_ID
        {
            get { return Banco_ID; }
            set { Banco_ID = value; }
        }
        public String P_CURP
        {
            get { return CURP; }
            set { CURP = value; }
        }
        public String P_Banco_Proveedor_ID
        {
            get { return Banco_Proveedor_ID; }
            set { Banco_Proveedor_ID = value; }
        }
        public bool P_Nueva_Actualizacion
        {
            get { return Nueva_Actualizacion; }
            set { Nueva_Actualizacion = value; }
        }
        public DataTable P_Dt_Conceptos_Proveedor
        {
            get { return Dt_Conceptos_Proveedor; }
            set { Dt_Conceptos_Proveedor = value; }
        }
        public String P_tipo
        {
            get { return Tipo; }
            set { Tipo = value; }
        }
        public String P_Tipo_Persona_Fiscal
        {
            get { return Tipo_Persona_Fiscal; }
            set { Tipo_Persona_Fiscal = value; }
        }
        public String P_Representante_Legal
        {
            get { return Representante_Legal; }
            set { Representante_Legal = value; }
        }
        public String P_Fecha_Actualizacion
        {
            get { return Fecha_Actualizacion; }
            set { Fecha_Actualizacion = value; }
        }
        public String P_Actualizacion
        {
            get { return Actualizacion; }
            set { Actualizacion = value; }
        }
        public DataTable P_Dt_Partidas_Proveedor
        {
            get { return Dt_Partidas_Proveedor; }
            set { Dt_Partidas_Proveedor = value; }
        }
        public String P_Concepto_ID
        {
            get { return Concepto_ID; }
            set { Concepto_ID = value; }
        }
        public String P_Usuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }
        public String P_Prueba
        {
            get { return Prueba; }
            set { Prueba = value; }
        }
        public String P_Cuenta
        {
            get { return Cuenta; }
            set { Cuenta = value; }
        }
        public String P_Busqueda
        {
            get { return Busqueda; }
            set { Busqueda = value; }
        }
        public String P_Proveedor_ID
        {
            get { return Proveedor_ID; }
            set { Proveedor_ID = value; }
        }
        public String P_Razon_Social
        {
            get { return Razon_Social; }
            set { Razon_Social = value; }
        }
        public String P_Nombre_Comercial
        {
            get { return Nombre_Comercial; }
            set { Nombre_Comercial = value; }
        }
        public String P_RFC
        {
            get { return RFC; }
            set { RFC = value; }
        }
        public String P_Contacto
        {
            get { return Contacto; }
            set { Contacto = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_Direccion
        {
            get { return Direccion; }
            set { Direccion = value; }
        }
        public String P_Colonia
        {
            get { return Colonia; }
            set { Colonia = value; }
        }
        public String P_Ciudad
        {
            get { return Ciudad; }
            set { Ciudad = value; }
        }
        public String P_Estado
        {
            get { return Estado; }
            set { Estado = value; }
        }
        public int P_CP
        {
            get { return CP; }
            set { CP = value; }
        }
        public String P_Telefono_1
        {
            get { return Telefono_1; }
            set { Telefono_1 = value; }
        }
        public String P_Telefono_2
        {
            get { return Telefono_2; }
            set { Telefono_2 = value; }
        }
        public String P_Nextel
        {
            get { return Nextel; }
            set { Nextel = value; }
        }
        public String P_Fax
        {
            get { return Fax; }
            set { Fax = value; }
        }
        public String P_Correo_Electronico
        {
            get { return Correo_Electronico; }
            set { Correo_Electronico = value; }
        }
        public String P_Password
        {
            get { return Password; }
            set { Password = value; }
        }
        public String P_Tipo_Pago
        {
            get { return Tipo_Pago; }
            set { Tipo_Pago = value; }
        }
        public int P_Dias_Credito
        {
            get { return Dias_Credito; }
            set { Dias_Credito = value; }
        }
        public String P_Forma_Pago
        {
            get { return Forma_Pago; }
            set { Forma_Pago = value; }
        }
        public String P_Comentarios
        {
            get { return Comentarios; }
            set { Comentarios = value; }
        }
        public String P_Nombre_Usuario
        {
            get { return Nombre_Usuario; }
            set { Nombre_Usuario = value; }
        }
        public String P_Cuenta_Nomina_ID
        {
            get { return Cuenta_Nomina_ID; }
            set { Cuenta_Nomina_ID = value; }
        }
        public String P_Cuenta_Acreedor_ID
        {
            get { return Cuenta_Acreedor_ID; }
            set { Cuenta_Acreedor_ID = value; }
        }
        public String P_Cuenta_Deudor_ID
        {
            get { return Cuenta_Deudor_ID; }
            set { Cuenta_Deudor_ID = value; }
        }
        public String P_Cuenta_Proveedor_ID
        {
            get { return Cuenta_Proveedor_ID; }
            set { Cuenta_Proveedor_ID = value; }
        }
        public String P_Cuenta_Contratista_ID
        {
            get { return Cuenta_Contratista_ID; }
            set { Cuenta_Contratista_ID = value; }
        }
        public String P_Cuenta_Predial_ID
        {
            get { return Cuenta_Predial_ID; }
            set { Cuenta_Predial_ID = value; }
        }
        public String P_Cuenta_Judicial_ID
        {
            get { return Cuenta_Judicial_ID; }
            set { Cuenta_Judicial_ID = value; }
        }
        public String P_Banco_Proveedor
        {
            get { return Banco_Proveedor; }
            set { Banco_Proveedor = value; }
        }
        public String P_Cuenta_Anticipo_Deudor_ID
        {
            get { return Cuenta_Anticipo_Deudor_ID; }
            set { Cuenta_Anticipo_Deudor_ID = value; }
        }
        public String P_Sucursal_Banco_Proveedor
        {
            get { return Sucursal_Banco_Proveedor; }
            set { Sucursal_Banco_Proveedor = value; }
        }

        public String P_E_Mail_Transferencia
        {
            get { return E_Mail_Transferencia; }
            set { E_Mail_Transferencia = value; }
        }

        public string P_Tipo_Cuenta
        {
            get { return Tipo_Cuenta; }
            set { Tipo_Cuenta = value; }
        }
        public string P_Proveedor_Bancario
        {
            get { return Proveedor_Bancario; }
            set { Proveedor_Bancario = value; }
        }
        //Metodos
        public String Alta_Proveedor()
        {
            return Cls_Cat_Con_Proveedores_Datos.Alta_Proveedor(this);
        }

        public void Baja_Proveedores()
        {
            Cls_Cat_Con_Proveedores_Datos.Baja_Proveedores(this);
        }

        public String Modificar_Proveedor()
        {
            return Cls_Cat_Con_Proveedores_Datos.Modificar_Proveedor(this);
        }

        public DataTable Consulta_Proveedores()
        {
            return Cls_Cat_Con_Proveedores_Datos.Consulta_Proveedores(this);
        }


        public DataTable Consultar_Partidas_Especificas()
        {
            return Cls_Cat_Con_Proveedores_Datos.Consultar_Partidas_Especificas(this);
        }

        public DataTable Consultar_Detalle_Partidas()
        {
            return Cls_Cat_Con_Proveedores_Datos.Consultar_Detalle_Partidas(this);
        }

        public DataTable Consultar_Detalles_Conceptos()
        {
            return Cls_Cat_Con_Proveedores_Datos.Consultar_Detalles_Conceptos(this);
        }

        public DataTable Consulta_Avanzada_Proveedor()
        {
            return Cls_Cat_Con_Proveedores_Datos.Consulta_Avanzada_Proveedor(this);
        }

        public void Alta_Detalle_Partidas()
        {
            Cls_Cat_Con_Proveedores_Datos.Alta_Detalle_Partidas(this);
        }

        public DataTable Consultar_Conceptos()
        {
            return Cls_Cat_Con_Proveedores_Datos.Consultar_Conceptos(this);
        }

        public DataTable Consulta_Datos_Proveedores()
        {
            return Cls_Cat_Con_Proveedores_Datos.Consulta_Datos_Proveedor(this);
        }
        public DataTable Validar_Proveedor()
        {
            return Cls_Cat_Con_Proveedores_Datos.Validar_Proveedor(this);
        }

        public DataTable Consultar_Actualizaciones_Proveedores()
        {
            return Cls_Cat_Con_Proveedores_Datos.Consultar_Actualizaciones_Proveedores(this);
        }
        public DataTable Consultar_Datos_Bancos()
        {
            return Cls_Cat_Con_Proveedores_Datos.Consultar_Datos_Bancos(this);
        }
        public Boolean Actualiza_Bacos_Datos()
        {
            return Cls_Cat_Con_Proveedores_Datos.Actualiza_Bacos_Datos(this);
        }
    }

}