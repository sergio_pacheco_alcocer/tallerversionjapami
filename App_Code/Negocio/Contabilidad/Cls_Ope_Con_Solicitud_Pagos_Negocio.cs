﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Solicitud_Pagos.Datos;

namespace JAPAMI.Solicitud_Pagos.Negocio
{
    public class Cls_Ope_Con_Solicitud_Pagos_Negocio
    {
        #region(Variables Internas)
            private String No_Solicitud_Pago;
            private Double No_Reserva;
            private Double No_Reserva_Anterior;            
            private String No_Poliza;
            private String Tipo_Poliza_ID;
            private String Mes_Ano;
            private String Tipo_Solicitud_Pago_ID;
            private String Empleado_ID_Jefe_Area;
            private String Empleado_ID_Contabilidad;
            private String Proveedor_ID;
            private String Empleado_ID;   
            private String Dependencia_ID;
            private String Concepto;
            private String Fecha_Solicitud;
            private Decimal Monto;
            private Double Monto_Anterior;
            private String No_Factura;
            private String Fecha_Factura;
            private String Fecha_Inicial;            
            private String Fecha_Final;            
            private String Estatus;
            private String Beneficiario;
            private String Fecha_Autorizo_Rechazo_Jefe;
            private String Comentarios_Jefe_Area;
            private String Fecha_Autorizo_Rechazo_Contabi;
            private String Comentarios_Contabilidad;
            private String Fecha_Autorizo_Rechazo_Ejercido;
            private String Comentarios_Ejercido;
            private String Nombre_Usuario;
            private String Usuario_Recibio;
            private String Fecha_recepcion_Documentos;
            private String Comentario_Recepcion;
            private String Modifica_Solicitud;
            private String Partida_ID;
            private String Tipo_Documento;
            private DataTable Dt_Detalles_Poliza;
            private DataTable Dt_Detalles_Solicitud;
            private String Partida;
            private String Limite_Inferior;
            private String Fecha_Recibio;
            private String Fecha_Recibio_Contabilidad;
            private String Fecha_Recibio_Ejercido;
            private String Usuario_Recibio_Ejercido;
            private String Usuario_Recibio_Contabilidad;
            private String Usuario_Autorizo_Contabilidad;
            private String Usuario_Autorizo_Ejercido;
            private String Comentario_Finanzas;
            private String Concepto_Poliza;
            private String Dependencia_Grupo;
            private String Recurso;
            private String Beneficiario_ID;
            private String Tipo_Beneficiario;
            private SqlCommand Cmmd;
            private String Servicios_Generales;
        #endregion
        #region(Variables Publicas)
            public String P_Usuario_Recibio_Ejercido
            {
                get { return Usuario_Recibio_Ejercido; }
                set { Usuario_Recibio_Ejercido = value; }
            }
            public String P_Usuario_Recibio_Contabilidad
            {
                get { return Usuario_Recibio_Contabilidad; }
                set { Usuario_Recibio_Contabilidad = value; }
            }
            public String P_Usuario_Autorizo_Contabilidad
            {
                get { return Usuario_Autorizo_Contabilidad; }
                set { Usuario_Autorizo_Contabilidad = value; }
            }
            public String P_Usuario_Autorizo_Ejercido
            {
                get { return Usuario_Autorizo_Ejercido; }
                set { Usuario_Autorizo_Ejercido = value; }
            }
            public String P_Fecha_Recibio_Ejercido
            {
                get { return Fecha_Recibio_Ejercido; }
                set { Fecha_Recibio_Ejercido = value; }
            }
            public String P_Fecha_Recibio_Contabilidad
            {
                get { return Fecha_Recibio_Contabilidad; }
                set { Fecha_Recibio_Contabilidad = value; }
            }
            public String P_Partida
            {
                get { return Partida; }
                set { Partida = value; }
            }
            public String P_Modifica_Solicitud
            {
                get { return Modifica_Solicitud; }
                set { Modifica_Solicitud = value; }
            }
            public String P_Comentario_Recepcion
            {
                get { return Comentario_Recepcion; }
                set { Comentario_Recepcion = value; }
            }
            public String P_Usuario_Recibio
            {
                get { return Usuario_Recibio; }
                set { Usuario_Recibio = value; }
            }
            public String P_Fecha_recepcion_Documentos
            {
                get { return Fecha_recepcion_Documentos; }
                set { Fecha_recepcion_Documentos = value; }
            }
            public String P_No_Solicitud_Pago
            {
                get { return No_Solicitud_Pago; }
                set { No_Solicitud_Pago = value; }
            }
            public String P_Servicios_Generales
            {
                get { return Servicios_Generales; }
                set { Servicios_Generales = value; }
            }
        
            public Double P_No_Reserva
            {
                get { return No_Reserva; }
                set { No_Reserva = value; }
            }
            public String P_No_Poliza
            {
                get { return No_Poliza; }
                set { No_Poliza = value; }
            }
            public String P_Tipo_Poliza_ID
            {
                get { return Tipo_Poliza_ID; }
                set { Tipo_Poliza_ID = value; }
            }
            public String P_Mes_Ano
            {
                get { return Mes_Ano; }
                set { Mes_Ano = value; }
            }
            public String P_Tipo_Solicitud_Pago_ID
            {
                get { return Tipo_Solicitud_Pago_ID; }
                set { Tipo_Solicitud_Pago_ID = value; }
            }
            public String P_Empleado_ID_Jefe_Area
            {
                get { return Empleado_ID_Jefe_Area; }
                set { Empleado_ID_Jefe_Area = value; }
            }
            public String P_Empleado_ID_Contabilidad
            {
                get { return Empleado_ID_Contabilidad; }
                set { Empleado_ID_Contabilidad = value; }
            }
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }
            public String P_Proveedor_ID
            {
                get { return Proveedor_ID; }
                set { Proveedor_ID = value; }
            }
            public String P_Empleado_ID
            {
                get { return Empleado_ID; }
                set { Empleado_ID = value; }
            }
            public String P_Concepto
            {
                get { return Concepto; }
                set { Concepto = value; }
            }
            public String P_Fecha_Solicitud
            {
                get { return Fecha_Solicitud; }
                set { Fecha_Solicitud = value; }
            }
            public Decimal P_Monto
            {
                get { return Monto; }
                set { Monto = value; }
            }
            public String P_No_Factura
            {
                get { return No_Factura; }
                set { No_Factura = value; }
            }
            public String P_Fecha_Factura
            {
                get { return Fecha_Factura; }
                set { Fecha_Factura = value; }
            }
            public String P_Fecha_Inicial
            {
                get { return Fecha_Inicial; }
                set { Fecha_Inicial = value; }
            }
            public String P_Fecha_Final
            {
                get { return Fecha_Final; }
                set { Fecha_Final = value; }
            }
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }
            public String P_Beneficiario
            {
                get { return Beneficiario; }
                set { Beneficiario = value; }
            }
            public String P_Fecha_Autorizo_Rechazo_Jefe
            {
                get { return Fecha_Autorizo_Rechazo_Jefe; }
                set { Fecha_Autorizo_Rechazo_Jefe = value; }
            }
            public String P_Comentarios_Jefe_Area
            {
                get { return Comentarios_Jefe_Area; }
                set { Comentarios_Jefe_Area = value; }
            }
            public String P_Fecha_Autorizo_Rechazo_Ejercido
            {
                get { return Fecha_Autorizo_Rechazo_Ejercido; }
                set { Fecha_Autorizo_Rechazo_Ejercido = value; }
            }
            public String P_Comentarios_Ejercido
            {
                get { return Comentarios_Ejercido; }
                set { Comentarios_Ejercido = value; }
            }
            public String P_Fecha_Autorizo_Rechazo_Contabi
            {
                get { return Fecha_Autorizo_Rechazo_Contabi; }
                set { Fecha_Autorizo_Rechazo_Contabi = value; }
            }
            public String P_Comentarios_Contabilidad
            {
                get { return Comentarios_Contabilidad; }
                set { Comentarios_Contabilidad = value; }
            }
            public String P_Nombre_Usuario
            {
                get { return Nombre_Usuario; }
                set { Nombre_Usuario = value; }
            }
            public Double P_No_Reserva_Anterior
            {
                get { return No_Reserva_Anterior; }
                set { No_Reserva_Anterior = value; }
            }
            public Double P_Monto_Anterior
            {
                get { return Monto_Anterior; }
                set { Monto_Anterior = value; }
            }
            public DataTable P_Dt_Detalles_Poliza
            {
                get { return Dt_Detalles_Poliza; }
                set { Dt_Detalles_Poliza = value; }
            }
            public DataTable P_Dt_Detalles_Solicitud
            {
                get { return Dt_Detalles_Solicitud; }
                set { Dt_Detalles_Solicitud = value; }
            }
            public String P_Partida_ID
            {
                get { return Partida_ID; }
                set { Partida_ID = value; }
            }
            public String P_Tipo_Documento
            {
                get { return Tipo_Documento; }
                set { Tipo_Documento = value; }
            }
            public String P_Limite_Inferior
            {
                get { return Limite_Inferior; }
                set { Limite_Inferior = value; }
            }
            public String P_Fecha_Recibio
            {
                get { return Fecha_Recibio; }
                set { Fecha_Recibio = value; }
            }
            public String P_Comentario_Finanzas
            {
                get { return Comentario_Finanzas; }
                set { Comentario_Finanzas = value; }
            }
            public String P_Concepto_Poliza
            {
                get { return Concepto_Poliza; }
                set { Concepto_Poliza = value; }
            }
            public String P_Dependencia_Grupo
            {
                get { return Dependencia_Grupo; }
                set { Dependencia_Grupo = value; }
            }
            public String P_Recurso
            {
                get { return Recurso; }
                set { Recurso = value; }
            }
            public String P_Beneficiario_ID
            {
                get { return Beneficiario_ID; }
                set { Beneficiario_ID = value; }
            }
            public String P_Tipo_Beneficiario
            {
                get { return Tipo_Beneficiario; }
                set { Tipo_Beneficiario = value; }
            }
            public SqlCommand P_Cmmd
            {
                get { return Cmmd; }
                set { Cmmd = value; }
            }
        #endregion
        #region(Metodos)
            public String Alta_Solicitud_Pago()
            {
               return Cls_Ope_Con_Solicitud_Pagos_Datos.Alta_Solicitud_Pago(this);
            }
            public String Alta_Poliza_Finiquito()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Alta_Poliza_Finiquito(this);
            }
            public String Alta_Solicitud_Pago_Sin_Poliza()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Alta_Solicitud_Pago_Sin_Poliza(this);
            }
            public String Modificar_Solicitud_Pago()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Modificar_Solicitud_Pago(this);
            }
            public String Modificar_Solicitud_Finiquito()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Modificar_Solicitud_Finiquito(this);
            }
            public String Rechaza_Solicitud_Pago()
            {
               return Cls_Ope_Con_Solicitud_Pagos_Datos.Rechaza_Solicitud_Pago(this);
            }
            public String Rechaza_Solicitud_Finiquito()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Rechaza_Solicitud_Finiquito(this);
            }
            public String Rechaza_Solicitud_Pago_Sin_Poliza()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Rechaza_Solicitud_Pago_Sin_Poliza(this);
            }
            public String Modificar_Solicitud_Pago_Sin_Poliza()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Modificar_Solicitud_Pago_Sin_Poliza(this);
            }
            public void Eliminar_Solicitud_Pago()
            {
                Cls_Ope_Con_Solicitud_Pagos_Datos.Eliminar_Solicitud_Pago(this);
            }
            public DataTable Consultar_Solicitud_Pago()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_Solicitud_Pago(this);
            }
            public DataTable Consulta_Reservas()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consulta_Reservas(this);
            }
            public DataTable Consulta_Datos_Reserva()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consulta_Datos_Reserva(this);
            }
            public DataTable Consulta_Datos_Solicitud_Pago()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consulta_Datos_Solicitud_Pago(this);
            }
            public DataTable Consulta_Datos_Solicitud_Pago_Gastos()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consulta_Datos_Solicitud_Pago_Gastos(this);
            }
        
            public DataTable Consulta_Datos_Solicitud_Pago_Servicios_Generales()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consulta_Datos_Solicitud_Pago_Servicios_Generales(this);
            }
            public DataTable Consulta_Cuenta_Contable_Proveedor()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consulta_Cuenta_Contable_Proveedor(this);
            }
            public DataTable Consulta_Detalles_Solicitud()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consulta_Detalles_Solicitud(this);
            }
            public DataTable Consulta_Detalles_Solicitud_Finiquito()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consulta_Detalles_Solicitud_Finiquito(this);
            }
            public DataTable Consulta_Solicitud_Pagos_con_Detalles()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consulta_Solicitud_Pagos_con_Detalles(this);
            }
            public DataTable Consulta_Parametro_ISR()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consulta_Parametro_ISR(this);
            }
            public void Alta_Solicitud_Pago_Factoraje()
            {
                Cls_Ope_Con_Solicitud_Pagos_Datos.Alta_Solicitud_Pago_Factoraje(this);
            }
            //se ocupa en el Avance fisico de la obra
            public DataTable Consulta_Solicitud_Pagos_Anticipo_Estimaciones()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consulta_Solicitud_Pagos_Anticipo_Estimaciones(this);
            }
            public DataTable Consulta_Solicitud_Detalles_Pagos_Anticipo_Estimaciones()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consulta_Solicitud_Detalles_Pagos_Anticipo_Estimaciones(this);
            }
            public DataTable Consultar_Datos_Poliza_Solicitud()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_Datos_Poliza_Solicitud(this);
            }
            public Boolean Incluir_Reserva()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Incluir_Reserva(this);
            }
            public DataTable Consultar_Datos_Solicitud_Finiquito()
            {
                return Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_Datos_Solicitud_Finiquito(this);
            }
        #endregion
    }
}