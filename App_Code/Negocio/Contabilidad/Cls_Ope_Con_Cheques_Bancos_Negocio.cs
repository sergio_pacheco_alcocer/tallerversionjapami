﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Cheques_Bancos.Datos;

namespace JAPAMI.Cheques_Bancos.Negocio
{
    public class Cls_Ope_Con_Cheques_Bancos_Negocio
    {
        #region(Variables Privadas)
        private String Banco_ID;
        private String Nombre_Banco;
        private String Folio_Inicial;
        private String Folio_Final;
        private String Folio_Actual;
        private String Usuario;
        private String Comisiones;
        private String Comision_Transferencia;
        private String Iva_Comision;
        private String Banco_Clabe;
        private String No_Cuenta_Banco;
        private String Comision_Spay;
        private String IVA_Spay;
        private SqlCommand Cmmd;
        #endregion

        #region(Variables Publicas)
        public String P_Banco_ID
        {
            get { return Banco_ID; }
            set { Banco_ID = value; }
        }
        public String P_Nombre_Banco
        {
            get { return Nombre_Banco; }
            set { Nombre_Banco = value; }
        }
        public String P_Folio_Inicial
        {
            get { return Folio_Inicial; }
            set { Folio_Inicial = value; }
        }
        public String P_Folio_Final
        {
            get { return Folio_Final; }
            set { Folio_Final = value; }
        }
        public String P_Folio_Actual
        {
            get { return Folio_Actual; }
            set { Folio_Actual = value; }
        }
        public String P_Usuario
        {
            get { return Usuario; }
            set { Usuario = value; }
        }
        public String P_Comisiones
        {
            get { return Comisiones; }
            set { Comisiones = value; }
        }
        public String P_Comision_Transferencia
        {
            get { return Comision_Transferencia; }
            set { Comision_Transferencia = value; }
        }
        public String P_Iva_Comision
        {
            get { return Iva_Comision; }
            set { Iva_Comision = value; }
        }
        public String P_Banco_Clabe
        {
            get { return Banco_Clabe; }
            set { Banco_Clabe = value; }
        }
        public String P_No_Cuenta_Banco
        {
            get { return No_Cuenta_Banco; }
            set { No_Cuenta_Banco = value; }
        }
        public String P_Comision_Spay
        {
            get { return Comision_Spay; }
            set { Comision_Spay = value; }
        }
        public String P_IVA_Spay
        {
            get { return IVA_Spay; }
            set { IVA_Spay = value; }
        }
        public SqlCommand P_Cmmd
        {
            get { return Cmmd; }
            set { Cmmd = value; }
        }
        #endregion

        #region(Metodos)
        public DataTable Consultar_Bancos_Like()
        {
            return Cls_Ope_Con_Cheques_Bancos_Datos.Consultar_Bancos_Like(this);
        }
        public DataTable Consultar_Bancos()
        {
            return Cls_Ope_Con_Cheques_Bancos_Datos.Consultar_Bancos(this);
        }
        public DataTable Consultar_Folio_Actual()
        {
            return Cls_Ope_Con_Cheques_Bancos_Datos.Consultar_Folio_Actual(this);
        }
        public Boolean Modificar_Folio_Banco()
        {
            return Cls_Ope_Con_Cheques_Bancos_Datos.Modificar_Folio_Banco(this);
        }
        public Boolean Modificar_Comisiones_Bancos()
        {
            return Cls_Ope_Con_Cheques_Bancos_Datos.Modificar_Comisiones_Bancos(this);
        }
        public Boolean Modificar_Folio_Actual()
        {
            return Cls_Ope_Con_Cheques_Bancos_Datos.Modificar_Folio_Actual(this);
        }
        public DataTable Consultar_Bancos_Existentes()
        {
            return Cls_Ope_Con_Cheques_Bancos_Datos.Consultar_Bancos_Existentes(this);
        }
        public DataTable Consultar_Cuenta_Bancos()
        {
            return Cls_Ope_Con_Cheques_Bancos_Datos.Consultar_Cuenta_Bancos(this);
        }
        public DataTable Consultar_Cuenta_Bancos_con_Descripcion()
        {
            return Cls_Ope_Con_Cheques_Bancos_Datos.Consultar_Cuenta_Bancos_con_Descripcion(this);
        }
        #endregion
    }
}