﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Cuentas_Gastos.Datos;

namespace JAPAMI.Cuentas_Gastos.Negocio
{
    public class Cls_Cat_Con_Cuentas_Gastos_Negocio
    {
        #region(Variables Privadas)
        private String Gasto_Id;
        private String Cuenta_Contable_Id;
        private String Descripcion;
        private String Estatus;
        private String Concepto;
        private String Usuario_Creo;
        private String Usuario_Modifico;
        private String Partida_ID;
        private DataTable Dt_Detalles;
        #endregion

        #region(Variables Publicas)
        public String P_Gasto_Id
        {
            get { return Gasto_Id; }
            set { Gasto_Id = value; }
        }
        public DataTable P_Dt_Detalles
        {
            get { return Dt_Detalles; }
            set { Dt_Detalles = value; }
        }
        public String P_Cuenta_Contable_Id
        {
            get { return Cuenta_Contable_Id; }
            set { Cuenta_Contable_Id = value; }
        }
        public String P_Descripcion
        {
            get { return Descripcion; }
            set { Descripcion = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_Concepto
        {
            get { return Concepto; }
            set { Concepto = value; }
        }
        public String P_Usuario_Creo
        {
            get { return Usuario_Creo; }
            set { Usuario_Creo = value; }
        }
        public String P_Usuario_Modifico
        {
            get { return Usuario_Modifico; }
            set { Usuario_Modifico = value; }
        }
        public String P_Partida_ID
        {
            get { return Partida_ID; }
            set { Partida_ID = value; }
        }
        #endregion

        #region Metodos_Aprobado
        public Boolean Alta_Gasto()
        {
            return Cls_Cat_Con_Cuentas_Gastos_Datos.Alta_Gasto(this);
        }
        public DataTable Consultar_Gastos()
        {
            return Cls_Cat_Con_Cuentas_Gastos_Datos.Consultar_Gastos(this);
        }
        public DataTable Consultar_Gastos_Detalle()
        {
            return Cls_Cat_Con_Cuentas_Gastos_Datos.Consultar_Gastos_Detalle(this);
        }
        
        public Boolean Elimina_Gasto()
        {
            return Cls_Cat_Con_Cuentas_Gastos_Datos.Elimina_Gasto(this);
        }
        public Boolean Actualiza_Gasto()
        {
            return Cls_Cat_Con_Cuentas_Gastos_Datos.Actualiza_Gasto(this);
        }
        #endregion
    }
}
