﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Reporte_Gasto.Datos;

/// <summary>
/// Summary description for Cls_Rpt_Gastos_Negocio
/// </summary>
namespace JAPAMI.Reporte_Gasto.Negocio
{
    public class Cls_Rpt_Gastos_Negocio
    {
        public Cls_Rpt_Gastos_Negocio()
        {
        }

        #region (Variables Locales)
        private String Unidad_Responsable_ID;
        private String Gerencia_ID;
        private DateTime Fecha_Inicio;
        private DateTime Fecha_Fin;
        #endregion

        #region (Variables Publicas)
        public String P_Unidad_Responsable_ID
        {
            get { return Unidad_Responsable_ID; }
            set { Unidad_Responsable_ID = value; }
        }

        public String P_Gerencia_ID
        {
            get { return Gerencia_ID; }
            set { Gerencia_ID = value; }
        }

        public DateTime P_Fecha_Inicio
        {
            get { return Fecha_Inicio; }
            set { Fecha_Inicio = value; }
        }

        public DateTime P_Fecha_Fin
        {
            get { return Fecha_Fin; }
            set { Fecha_Fin = value; }
        }
        #endregion

        #region (Metodos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Gerencias
        ///DESCRIPCION:             Consultar las gerencias de las Unidades Responsables
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              23/Abril/2012 18:40
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public DataTable Consulta_Gerencias()
        {
            return Cls_Rpt_Gastos_Datos.Consulta_Gerencias();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Unidades_Responsables
        ///DESCRIPCION:             Consulta de las unidades responsables con filtro de gerencia
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              23/Abril/2012 18:57
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public DataTable Consulta_Unidades_Responsables()
        {
            return Cls_Rpt_Gastos_Datos.Consulta_Unidades_Responsables(this);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Gasto
        ///DESCRIPCION:             Consulta para el gasto de acuerdo a la unidad responsable o la gerencia
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              24/Abril/2012 18:06
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public DataTable Consulta_Gasto()
        {
            return Cls_Rpt_Gastos_Datos.Consulta_Gasto(this);
        }
        #endregion
    }
}