﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Reporte_Basicos_Contabilidad.Datos;

namespace JAPAMI.Reporte_Basicos_Contabilidad.Negocio
{
    public class Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio
    {
        public Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio()
        {
        }
        #region Variables Internas
            private String Cuenta_Inicial;
            private String Cuenta_Final;
            private String Saldos_Ceros;
            private String Mes_Anio;
            private String Montos_Cero;
            private String Fecha_Inicial;
            private String Fecha_Final;
            private String Poliza_Inicial;            
            private String Poliza_Final;
            private String Tipo_Polizas;
            private String Tipo_Solicitud;
            private String Nivel_Cuenta;
            private String Cuenta;
            private String No_Poliza;
            //se utiliza en el reporte de cheques por fecha y banco
            private String Nombre_Banco;
            private String Dependencia_ID;
            private String Nivel_ID;
            private int Anio;
            private String Mes;
            private String Concepto;
            private SqlCommand Cmmd;
        #endregion
        #region Variables Publicas
            public String P_Cuenta_Inicial
            {
                get { return Cuenta_Inicial; }
                set { Cuenta_Inicial = value; }
            }
            public String P_Cuenta_Final
            {
                get { return Cuenta_Final; }
                set { Cuenta_Final = value; }
            }
            public String P_Saldos_Ceros
            {
                get { return Saldos_Ceros; }
                set { Saldos_Ceros = value; }
            }
            public String P_Mes_Anio
            {
                get { return Mes_Anio; }
                set { Mes_Anio = value; }
            }
            public String P_Montos_Cero
            {
                get { return Montos_Cero; }
                set { Montos_Cero = value; }
            }
            public String P_Fecha_Inicial
            {
                get { return Fecha_Inicial; }
                set { Fecha_Inicial = value; }
            }
            public String P_Fecha_Final
            {
                get { return Fecha_Final; }
                set { Fecha_Final = value; }
            }
            public String P_Poliza_Inicial
            {
                get { return Poliza_Inicial; }
                set { Poliza_Inicial = value; }
            }
            public String P_Poliza_Final
            {
                get { return Poliza_Final; }
                set { Poliza_Final = value; }
            }
            public String P_Tipo_Polizas
            {
                get { return Tipo_Polizas; }
                set { Tipo_Polizas = value; }
            }
            public String P_Tipo_Solicitud
            {
                get { return Tipo_Solicitud; }
                set { Tipo_Solicitud = value; }
            }
            public String P_Nombre_Banco
            {
                get { return Nombre_Banco; }
                set { Nombre_Banco = value; }
            }
            public String P_Nivel_Cuenta
            {
                get { return Nivel_Cuenta; }
                set { Nivel_Cuenta = value; }
            }
            public String P_Cuenta
            {
                get { return Cuenta; }
                set { Cuenta = value; }
            }
            public String P_No_Poliza
            {
                get { return No_Poliza; }
                set { No_Poliza = value; }
            }

            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }
            public String P_Nivel_ID
            {
                get { return Nivel_ID; }
                set { Nivel_ID = value; }
            }

            public int P_Anio
            {
                get { return Anio; }
                set { Anio = value; }
            }

            public String P_Mes
            {
                get { return Mes; }
                set { Mes = value; }
            }
            public String P_Concepto
            {
                get { return Concepto; }
                set { Concepto = value; }
            }
            public SqlCommand P_Cmmd
            {
                get { return Cmmd; }
                set { Cmmd = value; }
            }
        #endregion
        #region Metodos
            public DataTable Consulta_Balanza_Mensual_Debe_Haber()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Balanza_Mensual_Debe_Haber(this);
            }
            public DataTable Consulta_Balanza_Mensual()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Balanza_Mensual(this);
            }
            public DataTable Consulta_Diario_General()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Diario_General(this);
            }
            public DataTable Consulta_Diario_General_Detalles()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Diario_General_Detalles(this);
            }
            public DataTable Consulta_Libro_Mayor()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Libro_Mayor(this);
            }
            public DataTable Consulta_Cuentas_Libro_Mayor()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Cuentas_Libro_Mayor(this);
            }
            public DataTable Consulta_Tipo_Solicitud()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Tipo_Solicitud(this);
            }
            public DataTable Consulta_Libro_Diario()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Libro_Diario(this);
            }
            public DataSet Consulta_Tipo_Poliza()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Tipo_Poliza(this);
            }
            public DataTable Consulta_Detalles_Poliza()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Detalles_Poliza(this);
            }
               //se utiliza en el reporte de cheques por fecha y banco 
            public DataTable Consulta_Cheques_Por_Fecha()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Cheques_Por_Fecha(this);
            }
            public DataTable Consulta_Cheques_Banco_Fecha()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Cheques_Banco_Fecha(this);
            }
                //Se utilizan el el reporte de cuentas contables
            public DataTable Consulta_Cuentas_Por_Nivel()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Cuentas_Por_Nivel(this);
            }
            public DataTable Consultar_Cuentas_Por_Genero()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consultar_Cuentas_Por_Genero(this);
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Contables
            /// DESCRIPCION: Consultar las cuentas contables de acuerdo al nivel y a la dependencia
            /// PARAMETROS : 
            /// CREO       : Noe Mosqueda Valadez
            /// FECHA_CREO : 16/Abril/2012 11:41
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public DataTable Consulta_Cuentas_Contables()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Cuentas_Contables(this);
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Contables_Concatenada
            /// DESCRIPCION: Consultar las cuentas contables de acuerdo al nivel y a la dependencia
            /// PARAMETROS : 
            /// CREO       : Noe Mosqueda Valadez
            /// FECHA_CREO : 16/Abril/2012 11:41
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public DataTable Consulta_Cuentas_Contables_Concatenada()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Cuentas_Contables_Concatenada(this);
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Unidad_Responsable
            /// DESCRIPCION: Consultar las Unidades Responsables
            /// PARAMETROS : 
            /// CREO       : Susana Trigueros Armenta
            /// FECHA_CREO : 18/Abril/2012 9:56
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public DataTable Consulta_Unidad_Responsable()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Unidad_Responsable(this);
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Nueva_Balanza_Mensual
            /// DESCRIPCION: Consulta para el nuevo formato de la balnza mensual
            /// PARAMETROS : 
            /// CREO       : Noe Mosqueda Valadez
            /// FECHA_CREO : 24/Mayo/2012 16:50
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public DataSet Consulta_Nueva_Balanza_Mensual()
            {
                return Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos.Consulta_Nueva_Balanza_Mensual(this);
            }
        #endregion
    }
}