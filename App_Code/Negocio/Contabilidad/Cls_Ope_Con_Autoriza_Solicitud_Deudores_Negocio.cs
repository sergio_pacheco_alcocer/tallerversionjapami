﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Autoriza_Solicitud_Deudores.Datos;
/// <summary>
/// Summary description for Cls_Ope_Con_Autoriza_Solicitud_Pago
/// </summary>
/// 
namespace JAPAMI.Autoriza_Solicitud_Deudores.Negocio
{
    public class Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio
    {
        #region "Variables_Internas"
        private string No_Deuda;
        private string Mes_Anio;
        private string Estatus;
        private string Fecha_Solicitud;
        private string Fecha_Autoriza_Cancela;
        private string Tipo_Movimiento;
        private string Importe;
        private string Dependencia_ID;
        private string Usuario_Autorizo;
        private string Empleado_ID_Jefe_Area;
        private string Comentario;
        //autoriza finanzas
        private string Forma_Pago;
        private string Cuenta_Banco_Pago;
        private string Fecha_Autorizo_Rechazo_Finanzas;
        private string Usuario_Asigno_Banco;
        private string Comentario_Finanza;
        //Pago a deudores
        private string No_Pago;
        private string Fecha_Pago;
        private string Beneficiario_Pago;
        private string No_Cheque;
        private string Referencia;
        private string No_Partidas;
        private DataTable  Dt_Detalles_Poliza;
        private string Fecha_Inicio;
        private string Fecha_Final;
        private string Motivo_Cancelacion;
        private string Transferencia;
        private string Cuenta_Transferencia;
        //Autoriza Transferencia
        private string Banco;
        private string Tipo_Deudor;
        private string Deudor_ID;
        private DataTable Dt_Datos_Completos;
        private DataTable Dt_Datos_Agrupados;
        private string Monto_Comision;
        private string Monto_Iva;
        private String No_Vale_Deudor;
        private SqlCommand Cmmd;
        #endregion

        #region ""Variables Externas"
        public string P_No_Deuda
        {
            get { return No_Deuda; }
            set { No_Deuda = value; }
        }
        public string P_Mes_Anio {
            get { return Mes_Anio; }
            set { Mes_Anio = value; }
        }
        public string P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public string P_Fecha_Solicitud
        {
            get { return Fecha_Solicitud; }
            set { Fecha_Solicitud = value; }
        }
        public string P_Fecha_Autoriza_Cancela
        {
            get { return Fecha_Autoriza_Cancela; }
            set { Fecha_Autoriza_Cancela = value; }
        }
        public string P_Empleado_ID_Jefe_Area
        {
            get { return Empleado_ID_Jefe_Area; }
            set { Empleado_ID_Jefe_Area = value; }
        }
        public string P_Tipo_Movimiento
        {
            get { return Tipo_Movimiento; }
            set { Tipo_Movimiento = value; }
        }
        public string P_Importe
        {
            get { return Importe; }
            set { Importe = value; }
        }
        public string P_Dependencia_ID
        {
            get { return Dependencia_ID; }
            set { Dependencia_ID = value; }
        }
        public string P_Usuario_Autorizo
        {
            get { return Usuario_Autorizo; }
            set { Usuario_Autorizo = value; }
        }
        public string P_Comentario
        {
            get { return Comentario; }
            set { Comentario = value; }
        }
        public string P_Forma_Pago
        {
            get { return Forma_Pago; }
            set { Forma_Pago = value; }
        }
        public string P_Cuenta_Banco_Pago
        {
            get { return Cuenta_Banco_Pago; }
            set { Cuenta_Banco_Pago = value; }
        }
        public string P_Fecha_Autorizo_Rechazo_Finanzas
        {
            get { return Fecha_Autorizo_Rechazo_Finanzas; }
            set { Fecha_Autorizo_Rechazo_Finanzas = value; }
        }
        public string P_Usuario_Asigno_Banco
        {
            get { return Usuario_Asigno_Banco; }
            set { Usuario_Asigno_Banco = value; }
        }
        public string P_Comentario_Finanza
        {
            get { return Comentario_Finanza; }
            set { Comentario_Finanza = value; }
        }
        public string P_No_Pago
        {
            get { return No_Pago; }
            set { No_Pago = value; }
        }
        public string P_Fecha_Pago
        {
            get { return Fecha_Pago; }
            set { Fecha_Pago = value; }
        }
        public string P_Beneficiario_Pago
        {
            get { return Beneficiario_Pago; }
            set { Beneficiario_Pago = value; }
        }
        public string P_No_Cheque
        {
            get { return No_Cheque; }
            set { No_Cheque = value; }
        }
        public string P_Referencia
        {
            get { return Referencia; }
            set { Referencia = value; }
        }
        public string P_No_Partidas
        {
            get { return No_Partidas; }
            set { No_Partidas = value; }
        }
        public DataTable  P_Dt_Detalles_Poliza
        {
            get { return Dt_Detalles_Poliza; }
            set { Dt_Detalles_Poliza = value; }
        }
        public string P_Fecha_Inicio
        {
            get { return Fecha_Inicio; }
            set { Fecha_Inicio = value; }
        }
        public string P_Fecha_Final
        {
            get { return Fecha_Final; }
            set { Fecha_Final = value; }
        }
        public string P_Motivo_Cancelacion
        {
            get { return Motivo_Cancelacion; }
            set { Motivo_Cancelacion = value; }
        }
        public string P_Transferencia
        {
            get { return Transferencia; }
            set { Transferencia = value; }
        }
        public string P_Cuenta_Transferencia
        {
            get { return Cuenta_Transferencia; }
            set { Cuenta_Transferencia = value; }
        }
        public string P_Banco
        {
            get { return Banco; }
            set { Banco = value; }
        }
        public string P_Deudor_ID
        {
            get { return Deudor_ID; }
            set { Deudor_ID = value; }
        }
        public string P_Tipo_Deudor
        {
            get { return Tipo_Deudor; }
            set { Tipo_Deudor = value; }
        }
        public DataTable P_Dt_Datos_Agrupados
        {
            get { return Dt_Datos_Agrupados; }
            set { Dt_Datos_Agrupados = value; }
        }
        public DataTable P_Dt_Datos_Completos
        {
            get { return Dt_Datos_Completos; }
            set { Dt_Datos_Completos = value; }
        }
        public string P_Monto_Comision
        {
            get { return Monto_Comision; }
            set { Monto_Comision = value; }
        }
        public string P_Monto_Iva
        {
            get { return Monto_Iva; }
            set { Monto_Iva = value; }
        }

        public SqlCommand P_Cmmd
        {
            get { return Cmmd; }
            set { Cmmd = value; }
        }
        public String P_No_Vale_Deudor
        {
            get { return No_Vale_Deudor; }
            set { No_Vale_Deudor = value; }
        }
        #endregion

        #region "Metodos"
        public DataTable Consulta_Solicitudes_SinAutorizar()
        {
            return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Consulta_Solicitudes_SinAutotizar(this);
        }
        public void Cambiar_Estatus_Solicitud()
        {
            Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Cambiar_Estatus_Solicitud(this);
        }
        public DataTable Consulta_Solicitudes_PorPagar()
        {
            return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Consulta_Solicitudes_PorPagar(this);
        }
        public DataTable Consulta_Solicitudes()
        {
            return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Consulta_Solicitudes(this);
        }
        
        
        #region "Autoriza Finanzas"
        public DataTable Consulta_Solicitudes_Autorizadas()
        {
            return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Consulta_Solicitudes_Autorizadas(this);
        }
        public void Autorizacion_de_Solicitud()
        {
            Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Autorizacion_de_Solicitud(this);
        }
        public DataTable Consulta_Datos_Solicitud_Pago()
        {
            return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Consulta_Datos_Solicitud_Pago(this);
        }
        #endregion
        //Pago a deudores

         public String Alta_Cheque()
        {
            return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Alta_Cheque(this);
        }
         public String Alta_Vale()
         {
             return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Alta_Vale(this);
         }
         public String Comprobar_Vale()
         {
             return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Comprobacion_Vale(this);
         }
         public DataTable Consulta_Solicitudes_Pagadas_o_Porpagar()
         {
             return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Consulta_Solicitudes_Pagadas_o_Porpagar(this);
         }
         public DataTable Consulta_Solicitud_Pago()
         {
             return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Consultar_Solicitud_Pago(this);
         }
         public void Modificar_Pago()
         {
             Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Modificar_Pago(this);
         }
         public DataTable Consultar_Datos_Bancarios()
         {
             return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Consultar_Datos_Bancarios(this);
         }
         public DataTable Consultar_Datos_Bancarios_Proveedor()
         {
             return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Consultar_Datos_Bancarios_Proveedor(this);
         }
         public DataTable Consulta_Datos_transferencia()
         {
             return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Consulta_Datos_transferencia(this);
         }
         public void Alta_Transferencia()
         {
             Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Alta_Transferencia(this);
         }
        //se utilizan en el porceso de autorizacion de transferencias
         public DataTable Consultar_Solicitud_Transferencia()
         {
             return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Consultar_Solicitud_Transferencia(this);
         }
         public DataTable Consulta_Datos_Deudor()
         {
             return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Consulta_Datos_Deudor(this);
         }
         public Boolean Modificar_Transferencia()
         {
             return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Modificar_Transferencia(this);
         }
         public String Alta_Pago()
         {
             return Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos.Alta_Pago(this);
         }
        #endregion
    }
}


