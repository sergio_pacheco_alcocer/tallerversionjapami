﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Reporte_Gastos_Gerencia.Datos;

/// <summary>
/// Summary description for Cls_Rpt_Con_Gastos_Gerencias_Negocio
/// </summary>
namespace JAPAMI.Reporte_Gastos_Gerencia.Negocio
{
    public class Cls_Rpt_Con_Gastos_Gerencias_Negocio
    {
        public Cls_Rpt_Con_Gastos_Gerencias_Negocio()
        {
        }

        #region (Variables Internas)
        string Grupo_Dependencia_ID;
        string Dependencia_ID;
        string Clave_Grupo_Dependencia;
        string Clave_Dependencia;
        string Cuenta_Padre;
        string Cuentas_Hijo;
        string Mes_Anio;
        int Anio;
        int Mes_Inicial;
        int Mes_Final;
        #endregion

        #region (Variables Publicas)
        public string P_Grupo_Dependencia_ID
        {
            get { return Grupo_Dependencia_ID; }
            set { Grupo_Dependencia_ID = value; }
        }
        public string P_Dependencia_ID
        {
            get { return Dependencia_ID; }
            set { Dependencia_ID = value; }
        }
        public string P_Clave_Grupo_Dependencia
        {
            get { return Clave_Grupo_Dependencia; }
            set { Clave_Grupo_Dependencia = value; }
        }
        public string P_Clave_Dependencia
        {
            get { return Clave_Dependencia; }
            set { Clave_Dependencia = value; }
        }
        public string P_Mes_Anio
        {
            get { return Mes_Anio; }
            set { Mes_Anio = value; }
        }
        public string P_Cuentas_Hijo
        {
            get { return Cuentas_Hijo; }
            set { Cuentas_Hijo=value; }
        }
        public string P_Cuenta_Padre
        {
            get { return Cuenta_Padre; }
            set { Cuenta_Padre = value; }
        }
        public int P_Anio
        {
            get { return Anio; }
            set { Anio = value; }
        }
        public int P_Mes_Inicial
        {
            get { return Mes_Inicial; }
            set { Mes_Inicial = value; }
        }
        public int P_Mes_Final
        {
            get { return Mes_Final; }
            set { Mes_Final = value; }
        }
        #endregion
        #region (Metodos)
        public DataTable Consulta_Anios_Cerrados()
        {
            return Cls_Rpt_Con_Gastos_Gerencia_Datos.Consulta_Anios_Cerrados();
        }
        public DataTable Consultar_Gerencias()
        {
            return Cls_Rpt_Con_Gastos_Gerencia_Datos.Consultar_Gerencias(this);
        }
        public DataTable Consulta_Grupo_Gerencial()
        {
            return Cls_Rpt_Con_Gastos_Gerencia_Datos.Consulta_Grupo_Gerencial(this);
        }
        public DataTable Consulta_Meses_Anio()
        {
            return Cls_Rpt_Con_Gastos_Gerencia_Datos.Consulta_Meses_Anio(this);
        }

        public DataTable Consulta_Gastos_Gerencia()
        {
            return Cls_Rpt_Con_Gastos_Gerencia_Datos.Consulta_Gastos_Gerencia(this);
        }

        public DataTable Consultar_Cuentas_Por_Grupo_y_Gernecia()
        {
            return Cls_Rpt_Con_Gastos_Gerencia_Datos.Consultar_Cuentas_Por_Grupo_y_Gernecia(this);
        }

        public DataTable Consultar_Cuenta_Padre()
        {
            return Cls_Rpt_Con_Gastos_Gerencia_Datos.Consultar_Cuenta_Padre(this);
        }
        public Decimal Consultar_Monto_Por_Mes()
        {
            return Cls_Rpt_Con_Gastos_Gerencia_Datos.Consultar_Monto_Por_Mes(this);
        }
        #endregion
    }
}