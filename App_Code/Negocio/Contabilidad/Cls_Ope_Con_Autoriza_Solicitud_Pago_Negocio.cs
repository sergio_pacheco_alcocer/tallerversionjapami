﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Autoriza_Solicitud_Pago.Datos;
/// <summary>
/// Summary description for Cls_Ope_Con_Autoriza_Solicitud_Pago
/// </summary>
/// 
namespace JAPAMI.Autoriza_Solicitud_Pago.Negocio
{
    public class Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio
    {
        #region "Variables_Internas"
        private string No_Solicitud_Pago;
        private string No_Compromiso;
        private string Mes_Anio;
        private string Estatus;
        private string Empleado_ID_Jefe;
        private string Fecha_Solicitud;
        private string Tipo_Solicitud_Pago_ID;
        private string Comentario;
        private string Monto;
        private string Fecha_Autorizo_Rechazo_Jefe;
        private string Usuario_Creo;
        private string Fecha_Creo;
        private string Usuario_Modifico;
        private string Fecha_Modifico;
        private string Documentos;
        private string Dependencia_ID;
        private string Tipo_Recurso;
        private string Usuario_Autorizo;
        private string Usuario_Recibio_Doc_Fisica;
        private string Usuario_Autorizo_Documentos;
        private string Usuario_Recibio_Contabilidad;
        private string Grupo_Dependencia;
        private SqlCommand Cmmd;
        #endregion

        #region ""Variables Externas"
        public string P_Tipo_Recurso
        {
            get { return Tipo_Recurso; }
            set { Tipo_Recurso = value; }
        }
        public string P_No_Solicitud_Pago
        {
            get { return No_Solicitud_Pago; }
            set { No_Solicitud_Pago = value; }
        }
        public string P_No_Compromiso
        {
            get { return No_Compromiso; }
            set { No_Compromiso = value;}
        }
        public string P_Mes_Anio {
            get { return Mes_Anio; }
            set { Mes_Anio = value; }
        }
        public string P_Empleado_ID_Jefe
        {
            get { return Empleado_ID_Jefe; }
            set { Empleado_ID_Jefe = value; }

        }
        public string P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public string P_Fecha_Solicitud
        {
            get { return Fecha_Solicitud; }
            set { Fecha_Solicitud = value; }
        }
        public string P_Tipo_Solicitud_Pago_ID
        {
            get { return Tipo_Solicitud_Pago_ID; }
            set { Tipo_Solicitud_Pago_ID = value; }
        }
        public string P_Comentario
        {
            get { return Comentario; }
            set { Comentario = value; }
        }
        public string P_Monto
        {
            get { return Monto; }
            set { Monto = value; }
        }
        public string P_Usuario_Creo
        {
            get { return Usuario_Creo; }
            set { Usuario_Creo = value; }
        }
        public string P_Fecha_Creo
        {
            get { return Fecha_Creo; }
            set { Fecha_Creo = value; }
        }
        public string P_Usuario_Modifico
        {
            get { return Usuario_Modifico; }
            set { Usuario_Modifico = value; }
        }
        public string P_Fecha_Modifico
        {
            get { return Fecha_Modifico; }
            set { Fecha_Modifico = value; }
        }
        public string P_Fecha_Autorizo_Rechazo_Jefe
        {
            get { return Fecha_Autorizo_Rechazo_Jefe; }
            set { Fecha_Autorizo_Rechazo_Jefe = value; }
        }
        public string P_Documentos
        {
            get { return Documentos; }
            set { Documentos = value; }
        }

        public string P_Dependencia_ID
        {
            get { return Dependencia_ID; }
            set { Dependencia_ID = value; }
        }
        public string P_Usuario_Autorizo
        {
            get { return Usuario_Autorizo; }
            set { Usuario_Autorizo = value; }
        }
        public string P_Usuario_Recibio_Doc_Fisica
        {
            get { return Usuario_Recibio_Doc_Fisica; }
            set { Usuario_Recibio_Doc_Fisica = value; }
        }
        public string P_Usuario_Autorizo_Documentos
        {
            get { return Usuario_Autorizo_Documentos; }
            set { Usuario_Autorizo_Documentos = value; }
        }
        public string P_Usuario_Recibio_Contabilidad
        {
            get { return Usuario_Recibio_Contabilidad; }
            set { Usuario_Recibio_Contabilidad = value; }
        }
        public string P_Grupo_Dependencia
        {
            get { return Grupo_Dependencia; }
            set { Grupo_Dependencia = value; }
        }

        public SqlCommand P_Cmmd
        {
            get { return Cmmd; }
            set { Cmmd = value; }
        }
         

        #endregion

        #region "Metodos"
        public DataTable Consulta_Solicitudes_SinAutorizar()
        {
            return Cls_Ope_Con_Autoriza_Solicitud_Pago_Datos.Consulta_Solicitudes_SinAutotizar(this);
        }
        public void Cambiar_Estatus_Solicitud_Pago()
        {
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Datos.Cambiar_Estatus_Solicitud_Pago(this);
        }

        public DataTable Consulta_Solicitudes()
        {
            return Cls_Ope_Con_Autoriza_Solicitud_Pago_Datos.Consulta_Solicitudes(this);
        }
        public void Modificar_Estatus_Recepcion_Pago()
        {
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Datos.Modificar_Estatus_Recepcion_Documentos(this);
        }

        public DataTable Consultar_Detalles() 
        {
            return Cls_Ope_Con_Autoriza_Solicitud_Pago_Datos.Consulta_Detalles(this);
        }
        public DataTable Consulta_Documentos()
        {
            return Cls_Ope_Con_Autoriza_Solicitud_Pago_Datos.Consulta_Documentos(this);
        }
        #endregion
    }
}


