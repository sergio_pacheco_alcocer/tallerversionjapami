﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Deudores.Datos;

namespace JAPAMI.Deudores.Negocios
{
    public class Cls_Ope_Con_Deudores_Negocio
    {
        #region (Variables_Internas)
        private String No_Deuda;
        private String Deudor;
        private String Deudor_ID;
        private String Tipo_Deudor;
        private String Tipo_Movimiento;
        private String Estatus;
        private String Concepto;
        private String Importe;
        private String Fecha_Limite;
        private String Nombre_Usuario;
        private String Dependencia_ID;
        private String Fecha_Inicial;
        private String Fecha_Final;
        private String Tipo_Comprobacion;
        private String Tipo_Solicitud_ID;
        private SqlCommand Cmmd;
        private int No_Reserva;
        private string No_Factura_Solicitud;
        private DateTime Fecha_Factura_Solicitud;
        private DataTable Dt_Detalles_Solicitud_Pago;
        private String Vale;

        #endregion
        #region (Variables_Externas)
        public String P_No_Deuda
        {
            get { return No_Deuda; }
            set { No_Deuda = value; }
        }
        public String P_Deudor
        {
            get { return Deudor; }
            set { Deudor = value; }
        }
        public String P_Deudor_ID
        {
            get { return Deudor_ID; }
            set { Deudor_ID = value; }
        }
        public String P_Tipo_Deudor
        {
            get { return Tipo_Deudor; }
            set { Tipo_Deudor = value; }
        }
        public String P_Tipo_Movimiento
        {
            get { return Tipo_Movimiento; }
            set { Tipo_Movimiento = value; }
        }
        public String P_Tipo_Solicitud_ID
        {
            get { return Tipo_Solicitud_ID; }
            set { Tipo_Solicitud_ID = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_Concepto
        {
            get { return Concepto; }
            set { Concepto = value; }
        }
        public String P_Importe
        {
            get { return Importe; }
            set { Importe = value; }
        }
        public String P_Fecha_Limite
        {
            get { return Fecha_Limite; }
            set { Fecha_Limite = value; }
        }
        public String P_Nombre_Usuario
        {
            get { return Nombre_Usuario; }
            set { Nombre_Usuario = value; }
        }
        public String P_Dependencia_ID
        {
            get { return Dependencia_ID; }
            set { Dependencia_ID = value; }
        }
        public String P_Fecha_Inicial
        {
            get { return Fecha_Inicial; }
            set { Fecha_Inicial = value; }
        }
        public String P_Fecha_Final
        {
            get { return Fecha_Final; }
            set { Fecha_Final = value; }
        }
        public String P_Tipo_Comprobacion
        {
            get { return Tipo_Comprobacion; }
            set { Tipo_Comprobacion = value; }
        }
        public SqlCommand P_Cmmd
        {
            get { return Cmmd ; }
            set { Cmmd = value; }
        }

        public int P_No_Reserva
        {
            get { return No_Reserva; }
            set { No_Reserva = value; }
        }

        public string P_No_Factura_Solicitud
        {
            get { return No_Factura_Solicitud; }
            set { No_Factura_Solicitud = value; }
        }

        public DateTime P_Fecha_Factura_Solicitud
        {
            get { return Fecha_Factura_Solicitud; }
            set { Fecha_Factura_Solicitud = value; }
        }
        public DataTable P_Dt_Detalles_Solicitud_Pago
        {
            get { return Dt_Detalles_Solicitud_Pago; }
            set { Dt_Detalles_Solicitud_Pago = value; }
        }
        public String P_Vale
        {
            get { return Vale; }
            set { Vale = value; }
        }
        #endregion
        #region (Metodos)
            public DataTable Consulta_Proveedores_Deudores()
            {
                return Cls_Ope_Con_Deudores_Datos.Consulta_Proveedores_Deudores(this);
            }
            public DataTable Consulta_Empleados_Deudores()
            {
                return Cls_Ope_Con_Deudores_Datos.Consulta_Empleados_Deudores(this);
            }
            public DataTable Consulta_Deudores()
            {
                return Cls_Ope_Con_Deudores_Datos.Consulta_Deudores(this);
            }
            public DataTable Consulta_Deudores_Activos()
            {
                return Cls_Ope_Con_Deudores_Datos.Consulta_Deudores_Activos(this);
            }
            public String Alta_Deudor()
            {
                return Cls_Ope_Con_Deudores_Datos.Alta_Deudor(this);
            }
            public void Modificar_Deudor()
            {
                Cls_Ope_Con_Deudores_Datos.Modificar_Deudor(this);
            }
            public void Modificar_Saldo_Deudor()
            {
                Cls_Ope_Con_Deudores_Datos.Modificar_Saldo_Deudor(this);
            }
            public DataTable Consulta_Deudores_Por_Deudor()
            {
                return Cls_Ope_Con_Deudores_Datos.Consulta_Deudores_Por_Deudor(this);
            }
            public DataTable Consulta_Banco_Monto_Deudas()
            {
                return Cls_Ope_Con_Deudores_Datos.Consulta_Banco_Monto_Deudas(this);
            }
            public DataTable Consulta_Tipo_Solicitud_Pagos_Combo()
            {
                return Cls_Ope_Con_Deudores_Datos.Consulta_Tipo_Solicitud_Pagos_Combo(this);
            }
            public DataTable Consulta_Datos_impresion()
            {
                return Cls_Ope_Con_Deudores_Datos.Consulta_Datos_impresion(this);
            }
            public DataTable Consulta_Cuentas_Contables_De_Reserva()
            {
                return Cls_Ope_Con_Deudores_Datos.Consulta_Cuentas_Contables_De_Reserva(this);
            }
            //public void Eliminar_Tipo_Poliza()
            //{
            //    Cls_Cat_Con_Tipo_Polizas_Datos.Eliminar_Tipo_Poliza(this);
            //}
            //public DataTable Consulta_Datos_Tipo_Poliza()
            //{
            //    return Cls_Cat_Con_Tipo_Polizas_Datos.Consulta_Datos_Tipo_Poliza(this);
            //}
            //public DataTable Consulta_Tipos_Poliza()
            //{
            //    return Cls_Cat_Con_Tipo_Polizas_Datos.Consulta_Tipos_Poliza(this);
            //}

            public string Crear_Solicitud_Pago_Interna()
            {
                return Cls_Ope_Con_Deudores_Datos.Crear_Solicitud_Pago_Interna(this);
            }
            public DataTable Consultar_Datos_Vale()
            {
                return Cls_Ope_Con_Deudores_Datos.Consultar_Datos_Vale(this);
            }
        #endregion
    }
}