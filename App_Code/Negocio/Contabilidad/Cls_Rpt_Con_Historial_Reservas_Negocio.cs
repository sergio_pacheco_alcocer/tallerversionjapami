﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Reporte_Historail_Reservas.Datos;

/// <summary>
/// Summary description for Cls_Rpt_Con_Historial_Reservas_Negocio
/// </summary>
public class Cls_Rpt_Con_Historial_Reservas_Negocio
{
    #region Variables Internas
    
        private String No_Reserva;
        private String Estatus;
        private String Concepto;
        private String Fecha;
        private String Dependencia_Id;
        private String Proyecto_Programas;
        private String Partida;
        private String Importe_Inicial;
        private String Saldo;
        private String Financiamiento_Id;
        private String Capitulo_Id;

    #endregion
   
    #region Variables Publicas

    public String P_No_Reserva
    {
        get { return No_Reserva; }
        set { No_Reserva = value; }
    }
    public String P_Estatus
    {
        get { return Estatus; }
        set { Estatus = value; }
    }
    public String P_Concepto
    {
        get { return Concepto; }
        set { Concepto = value; }
    }
    public String P_Fecha
    {
        get { return Fecha; }
        set { Fecha = value; }
    }
    public String P_Dependencia_Id
    {
        get { return Dependencia_Id; }
        set { Dependencia_Id = value; }
    }
    public String P_Proyecto_Programas
    {
        get { return Proyecto_Programas; }
        set { Proyecto_Programas = value; }
    }
    public String P_Partida
    {
        get { return Partida; }
        set { Partida = value; }
    }
    public String P_Importe_Inicial
    {
        get { return Importe_Inicial; }
        set { Importe_Inicial = value; }
    }
    public String P_Saldo
    {
        get { return Saldo; }
        set { Saldo = value; }
    }
    public String P_Financiamiento_Id
    {
        get { return Financiamiento_Id; }
        set { Financiamiento_Id = value; }
    }
    public String P_Capitulo_Id
    {
        get { return Capitulo_Id; }
        set { Capitulo_Id = value; }
    }

    #endregion

    #region Metodos
    
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Consulta_Historial_Reservas
    ///DESCRIPCION:             Consulta del historial reservas con diversos filtros
    ///PARAMETROS:              
    ///CREO:                    Luis Daniel Guzmán Malagón
    ///FECHA_CREO:              18/Julio/2012 16:35
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    ///
    public DataTable Consulta_Historial_Reservas() {
        return Cls_Rpt_Con_Historial_Reservas_Datos.Consulta_Historial_Reservas(this);
    }

    #endregion 
}
