﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Ope_Con_Servicios_Generales.Datos;

/// <summary>
/// Summary description for Cls_Ope_Con_Servicios_Generales_Negocio
/// </summary>

namespace JAPAMI.Ope_Con_Servicios_Generales.Negocio
{
    public class Cls_Ope_Con_Servicios_Generales_Negocio
    {
        #region Variables Internas
        private String No_Solicitud_Pago = String.Empty;
        private Int32 No_Reserva = (-1);
        private String Cuenta_Gasto_ID = String.Empty;
        private String Partida_ID = String.Empty;
        private Int32 Anio = (-1);
        private Double Total = 0.0;
        private String Proveedor_ID = String.Empty;
        private String Aprox_Proveedor = String.Empty;
        private String Mes = String.Empty;
        private String Gasto_ID = String.Empty;
        private String Tipo_Solicitud_ID = String.Empty;
        private String No_Factura = String.Empty;
        private DateTime Fecha_Factura = new DateTime();
        private String Concepto = String.Empty;
        private DataTable Dt_Detalles_Presupuesto = new DataTable();
        private DataTable Dt_Detalles_Reserva = new DataTable();
        private DataTable Dt_Detalles_Solicitud_Pago = new DataTable();
        private String Cuenta_Contable_Proveedor_ID = String.Empty;
        private String Estatus = String.Empty;
        private String Usuario_ID = String.Empty;
        private String Usuario_Nombre = String.Empty;
        #endregion Variables Internas
        #region Variables Publicas
        public String P_No_Solicitud_Pago
        {
            set { No_Solicitud_Pago = value; }
            get { return No_Solicitud_Pago; }
        }
        public Int32 P_No_Reserva
        {
            set { No_Reserva = value; }
            get { return No_Reserva; }
        }
        public String P_Cuenta_Gasto_ID
        {
            set { Cuenta_Gasto_ID = value; }
            get { return Cuenta_Gasto_ID; }
        }
        public String P_Partida_ID
        {
            set { Partida_ID = value; }
            get { return Partida_ID; }
        }
        public Int32 P_Anio
        {
            set { Anio = value; }
            get { return Anio; }
        }
        public Double P_Total
        {
            set { Total = value; }
            get { return Total; }
        }
        public String P_Proveedor_ID
        {
            set { Proveedor_ID = value; }
            get { return Proveedor_ID; }
        }
        public String P_Aprox_Proveedor
        {
            set { Aprox_Proveedor = value; }
            get { return Aprox_Proveedor; }
        }
        public String P_Mes
        {
            set { Mes = value; }
            get { return Mes; }
        }
        public String P_Gasto_ID
        {
            set { Gasto_ID = value; }
            get { return Gasto_ID; }
        }
        public String P_Tipo_Solicitud_ID
        {
            set { Tipo_Solicitud_ID = value; }
            get { return Tipo_Solicitud_ID; }
        }
        public String P_No_Factura
        {
            set { No_Factura = value; }
            get { return No_Factura; }
        }
        public DateTime P_Fecha_Factura
        {
            set { Fecha_Factura = value; }
            get { return Fecha_Factura; }
        }
        public String P_Concepto
        {
            set { Concepto = value; }
            get { return Concepto; }
        }
        public DataTable P_Dt_Detalles_Presupuesto
        {
            set { Dt_Detalles_Presupuesto = value; }
            get { return Dt_Detalles_Presupuesto; }
        }
        public DataTable P_Dt_Detalles_Reserva
        {
            set { Dt_Detalles_Reserva = value; }
            get { return Dt_Detalles_Reserva; }
        }
        public DataTable P_Dt_Detalles_Solicitud_Pago
        {
            set { Dt_Detalles_Solicitud_Pago = value; }
            get { return Dt_Detalles_Solicitud_Pago; }
        }
        public String P_Cuenta_Contable_Proveedor_ID {
            set { Cuenta_Contable_Proveedor_ID = value; }
            get { return Cuenta_Contable_Proveedor_ID; }
        }
        public String P_Estatus {
            set { Estatus = value; }
            get { return Estatus; }
        }
        public String P_Usuario_ID
        {
            set { Usuario_ID = value; }
            get { return Usuario_ID; }
        }
        public String P_Usuario_Nombre
        {
            set { Usuario_Nombre = value; }
            get { return Usuario_Nombre; }
        }
        #endregion Variables Publicas
        #region Metodos
        public DataTable Consultar_Dependencias_Partida()
        {
            return Cls_Ope_Con_Servicios_Generales_Datos.Consultar_Dependencias_Partida(this);
        }
        public DataTable Consultar_Proveedores() {
            return Cls_Ope_Con_Servicios_Generales_Datos.Consultar_Proveedores(this);
        }
        public DataTable Consultar_Fuentes_Financiamiento() {
            return Cls_Ope_Con_Servicios_Generales_Datos.Consultar_Fuentes_Financiamiento(this);
        }
        public String Alta_Reserva_Solicitud_Pago() {
            return Cls_Ope_Con_Servicios_Generales_Datos.Alta_Reserva_Solicitud_Pago(this);
        }
        public DataTable Consultar_Partida_Gasto() {
            return Cls_Ope_Con_Servicios_Generales_Datos.Consultar_Partida_Gasto(this);
        }
        #endregion Metodos

    }
}