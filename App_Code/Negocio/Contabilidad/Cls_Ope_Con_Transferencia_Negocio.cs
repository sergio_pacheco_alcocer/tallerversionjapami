﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Contabilidad_Transferencia.Datos;

namespace JAPAMI.Contabilidad_Transferencia.Negocio
{
    public class Cls_Ope_Con_Transferencia_Negocio
    {
        #region(Variables Privadas)
        private String Estatus;
        private String No_solicitud_Pago;
        private String Banco;
        private String Fecha;
        private String Fecha_Transferencia;
        private String Orden;
        private String Tipo_Filtro;
        private String Tipo_Pago;
        #endregion

        #region Variables publicas
        public String P_No_solicitud_Pago
        {
            get { return No_solicitud_Pago; }
            set { No_solicitud_Pago = value; }
        }
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public String P_Banco
        {
            get { return Banco; }
            set { Banco = value; }
        }
        public String P_Fecha
        {
            get { return Fecha; }
            set { Fecha = value; }
        }
        public String P_Fecha_Transferencia
        {
            get { return Fecha_Transferencia; }
            set { Fecha_Transferencia = value; }
        }
        public String P_Orden
        {
            get { return Orden; }
            set { Orden = value; }
        }
        public String P_Tipo_Filtro
        {
            get { return Tipo_Filtro; }
            set { Tipo_Filtro = value; }
        }
        public String P_Tipo_Pago
        {
            get { return Tipo_Pago; }
            set { Tipo_Pago = value; }
        }
        #endregion

        #region Metodos

       
        public Boolean Modificar_Transferencia()
        {
            return Cls_Ope_Con_Transferencia_Datos.Modificar_Transferencia(this);
        }

        public DataTable Consultar_Datos_Bancarios_Proveedor()
        {
            return Cls_Ope_Con_Transferencia_Datos.Consultar_Datos_Bancarios_Proveedor(this);
        }
        public DataTable Consultar_Solicitud_Transferencia()
        {
            return Cls_Ope_Con_Transferencia_Datos.Consultar_Solicitud_Transferencia(this);
        }
        //se utilizan en el reporte de transferencias
        public DataTable Consultar_orden_Transferencia()
        {
            return Cls_Ope_Con_Transferencia_Datos.Consultar_orden_Transferencia(this);
        }
        public DataTable Consultar_Transferencias_Por_Orden()
        {
            return Cls_Ope_Con_Transferencia_Datos.Consultar_Transferencias_Por_Orden(this);
        }
        public DataTable Consultar_Transferencias_Detalles()
        {
            return Cls_Ope_Con_Transferencia_Datos.Consultar_Transferencias_Detalles(this);
        }
        #endregion
    }
}