﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Ope_Con_Seguimiento_De_Cheques.Datos;

/// <summary>
/// Summary description for Cls_Ope_Con_Seguimiento_Cheques_Negocio
/// </summary>
namespace JAPAMI.Ope_Con_Seguimiento_De_Cheques.Negocios
{
    public class Cls_Ope_Con_Seguimiento_Cheques_Negocio
    {
        public Cls_Ope_Con_Seguimiento_Cheques_Negocio()
        {
        }
        #region Variables privadas 
        private String Fecha_Emición_Inicio;
        private String Fecha_Emición_Fin;
        private String Tipo_Benefciario;
        private String Beneficiario;
        private String Etapa;
        private String No_Cheque;
        #endregion

        #region Variables Públicas
        public String P_Fecha_Emición_Inicio
        {
            get{return Fecha_Emición_Inicio;}
            set{Fecha_Emición_Inicio = value;}
        }
        public String P_Fecha_Emición_Fin
        {
            get{return Fecha_Emición_Fin;}
            set { Fecha_Emición_Fin = value; }
        }
        public String P_Tipo_Benefciario 
        {
            get { return Tipo_Benefciario; }
            set { Tipo_Benefciario = value; }
        }
        public String P_Beneficiario 
        {
            get { return Beneficiario; }
            set { Beneficiario = value; }
        }
        public String P_Etapa 
        {
            get { return Etapa; }
            set { Etapa = value; }
        }
        public String P_No_Cheque
        {
            get { return No_Cheque; }
            set { No_Cheque = value; }
        }
        #endregion

        #region Metodos
        public DataTable Consultar_Tipos_Beneficiarios() 
        {
            return Cls_Ope_Con_Seguimiento_Cheques_Datos.Consultar_Tipos_Beneficiarios();
        }
        public DataTable Consultar_Beneficiario() 
        {
            return Cls_Ope_Con_Seguimiento_Cheques_Datos.Consultar_Beneficiarios(this);
        }
        public DataTable Consultar_Cheques_Listado() 
        {
            return Cls_Ope_Con_Seguimiento_Cheques_Datos.Consultar_Cheques_Listado(this);
        }
        public DataTable Consultar_Detalle_Cheque() 
        {
            return Cls_Ope_Con_Seguimiento_Cheques_Datos.Consultar_Detalle_Cheque(this);
        }
        #endregion
    }
}