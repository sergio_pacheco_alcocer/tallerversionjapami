﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Cat_Contratos.Datos;

namespace JAPAMI.Cat_Contratos.Negocio
{
    public class Cls_Con_Cat_Contratos_Negocios
    {
        #region VARIABLES INTERNAS
        private String Contrato_ID;
        private String Num_Contrato;
        private String Nom_Contrato;
        private String Fecha_Ini;
        private String Fecha_Fin;
        private String Estatus;
        private String Proveedor_ID;
        private String Busqueda_Proveedor;
        private String Busqueda;
        private String Importe_Total;
        private String Anticipo_Monto;
        private String Anticipo_Porcentaje;
        private String Amortizado;
        private String Por_Amortizar;
        private String Saldo_Total;
        private String Avance_Fisico;
        private String Avance_Financiero;
        private String Usuario_Creo;
        private String Usuario_Modifico;
        private String Convenio_ID;
        private String Programa_ID;
        private String Saldo_Contrato;
        private String Fecha_Inicial_Original;
        private String Fecha_Final_Original;
        private String Importe_Original;
        private String Motivo;
        private String Archivo;
        private String Ruta_Archivo;
        private DataTable Dt_Acciones;
        private String Cuenta_Contrato_ID;
        private String No_Reserva;
        private String Forma_Pago;
        private String Ampliacion_Monto;

        private String Fianza_Garantia;
        private String Fecha_Fianza;
        private String Monto_Fianza;
        private String Fianza_Anticipo;
        private String Fecha_Finiquito;
        private String Modalidad;
        private String Localidad;

        //contratos detalles
        private String Anio;
        private String FF_ID;
        private String FF;
        private String PP_ID;
        private String PP;
        private String P_ID;
        private String P;
        private String UR_ID;
        private String UR;
        private DataTable Dt_Detalles;
        private String Accion_ID;

        private SqlCommand Cmmd;
        #endregion

        #region VARIABLES PUBLICAS
        //get y set de P_Anio
        public String P_Anio
        {
            get { return Anio; }
            set { Anio = value; }
        }

        //get y set de P_Contrato_ID
        public String P_Contrato_ID
        {
            get { return Contrato_ID; }
            set { Contrato_ID = value; }
        }
        //get y set de Num_Contrato
        public String P_Num_Contrato
        {
            get { return Num_Contrato; }
            set { Num_Contrato = value; }
        }

        //get y set de P_Nom_Contrato
        public String P_Nom_Contrato
        {
            get { return Nom_Contrato; }
            set { Nom_Contrato = value; }
        }

        //get y set de P_Fecha_Ini
        public String P_Fecha_Ini
        {
            get { return Fecha_Ini; }
            set { Fecha_Ini = value; }
        }

        //get y set de P_Fecha_Fin
        public String P_Fecha_Fin
        {
            get { return Fecha_Fin; }
            set { Fecha_Fin = value; }
        }

        //get y set de Proveedor_ID
        public String P_Proveedor_ID
        {
            get { return Proveedor_ID; }
            set { Proveedor_ID = value; }
        }

        //get y set de P_Busqueda_Proveedor
        public String P_Busqueda_Proveedor
        {
            get { return Busqueda_Proveedor; }
            set { Busqueda_Proveedor = value; }
        }

        //get y set de P_Busqueda
        public String P_Busqueda
        {
            get { return Busqueda; }
            set { Busqueda = value; }
        }

        //get y set de P_Importe_Total
        public String P_Importe_Total
        {
            get { return Importe_Total; }
            set { Importe_Total = value; }
        }

        //get y set de P_Estatus
        public String P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }

        //get y set de P_Usuario_Creo
        public String P_Usuario_Creo
        {
            get { return Usuario_Creo; }
            set { Usuario_Creo = value; }
        }

        //get y set de P_Usuario_Modifico
        public String P_Usuario_Modifico
        {
            get { return Usuario_Modifico; }
            set { Usuario_Modifico = value; }
        }

        //get y set de P_Anticipo_Monto
        public String P_Anticipo_Monto
        {
            get { return Anticipo_Monto; }
            set { Anticipo_Monto = value; }
        }

        //get y set de P_Anticipo_Porcentaje
        public String P_Anticipo_Porcentaje
        {
            get { return Anticipo_Porcentaje; }
            set { Anticipo_Porcentaje = value; }
        }

        //get y set de P_Amortizado
        public String P_Amortizado
        {
            get { return Amortizado; }
            set { Amortizado = value; }
        }

        //get y set de P_Por_Amortizar
        public String P_Por_Amortizar
        {
            get { return Por_Amortizar; }
            set { Por_Amortizar = value; }
        }

        //get y set de P_Saldo_Total
        public String P_Saldo_Total
        {
            get { return Saldo_Total; }
            set { Saldo_Total = value; }
        }

        //get y set de P_Avance_Fisico
        public String P_Avance_Fisico
        {
            get { return Avance_Fisico; }
            set { Avance_Fisico = value; }
        }

        //get y set de P_Avance_Financiero
        public String P_Avance_Financiero
        {
            get { return Avance_Financiero; }
            set { Avance_Financiero = value; }
        }

        //get y set de P_FF_ID
        public String P_FF_ID
        {
            get { return FF_ID; }
            set { FF_ID = value; }
        }

        //get y set de P_FF
        public String P_FF
        {
            get { return FF; }
            set { FF = value; }
        }

        //get y set de P_PP_ID
        public String P_PP_ID
        {
            get { return PP_ID; }
            set { PP_ID = value; }
        }

        //get y set de P_PP
        public String P_PP
        {
            get { return PP; }
            set { PP = value; }
        }

        //get y set de P_P_ID
        public String P_P_ID
        {
            get { return P_ID; }
            set { P_ID = value; }
        }

        //get y set de P_P
        public String P_P
        {
            get { return P; }
            set { P = value; }
        }

        //get y set de P_UR_ID
        public String P_UR_ID
        {
            get { return UR_ID; }
            set { UR_ID = value; }
        }

        //get y set de P_UR
        public String P_UR
        {
            get { return UR; }
            set { UR = value; }
        }

        //get y set de P_Dt_Detalles
        public DataTable P_Dt_Detalles
        {
            get { return Dt_Detalles; }
            set { Dt_Detalles = value; }
        }
        //get y set de P_Convenio_ID
        public String P_Convenio_ID
        {
            get { return Convenio_ID; }
            set { Convenio_ID = value; }
        }
        //get y set de P_Programa_ID
        public String P_Programa_ID
        {
            get { return Programa_ID; }
            set { Programa_ID = value; }
        }
        //get y set de P_Programa_ID
        public String P_Saldo_Contrato
        {
            get { return Saldo_Contrato; }
            set { Saldo_Contrato = value; }
        }

        public String P_Fecha_Inicial_Original
        {
            get { return Fecha_Inicial_Original; }
            set { Fecha_Inicial_Original = value; }
        }
        public String P_Fecha_Final_Original
        {
            get { return Fecha_Final_Original; }
            set { Fecha_Final_Original = value; }
        }
        public String P_Importe_Original
        {
            get { return Importe_Original; }
            set { Importe_Original = value; }
        }
        public String P_Motivo
        {
            get { return Motivo; }
            set { Motivo = value; }
        }
        public String P_Archivo
        {
            get { return Archivo; }
            set { Archivo = value; }
        }
        public String P_Ruta_Archivo
        {
            get { return Ruta_Archivo; }
            set { Ruta_Archivo = value; }
        }
        public String P_Accion_ID
        {
            get { return Accion_ID; }
            set { Accion_ID = value; }
        }
        public DataTable P_Dt_Acciones
        {
            get { return Dt_Acciones; }
            set { Dt_Acciones = value; }
        }
        //get y set de P_No_Reserva
        public String P_No_Reserva
        {
            get { return No_Reserva; }
            set { No_Reserva = value; }
        }
        //get y set de P_Programa_ID
        public String P_Cuenta_Contrato_ID
        {
            get { return Cuenta_Contrato_ID; }
            set { Cuenta_Contrato_ID = value; }
        }
        //get y set de P_Anio
        public String P_Forma_Pago
        {
            get { return Forma_Pago; }
            set { Forma_Pago = value; }
        }
        public String P_Ampliacion_Monto
        {
            get { return Ampliacion_Monto; }
            set { Ampliacion_Monto = value; }
        }
        //get y set de P_Fianza_Garantia
        public String P_Fianza_Garantia
        {
            get { return Fianza_Garantia; }
            set { Fianza_Garantia = value; }
        }
        //get y set de P_Fecha_Fianza
        public String P_Fecha_Fianza
        {
            get { return Fecha_Fianza; }
            set { Fecha_Fianza = value; }
        }
        //get y set de P_Monto_Fianza
        public String P_Monto_Fianza
        {
            get { return Monto_Fianza; }
            set { Monto_Fianza = value; }
        }
        //get y set de P_Fianza_Anticipo
        public String P_Fianza_Anticipo
        {
            get { return Fianza_Anticipo; }
            set { Fianza_Anticipo = value; }
        }
        //get y set de P_Fecha_Finiquito
        public String P_Fecha_Finiquito
        {
            get { return Fecha_Finiquito; }
            set { Fecha_Finiquito = value; }
        }
        //get y set de P_Modalidad
        public String P_Modalidad
        {
            get { return Modalidad; }
            set { Modalidad = value; }
        }

        //get y set de P_Localidad
        public String P_Localidad
        {
            get { return Localidad; }
            set { Localidad = value; }
        }

        public SqlCommand P_Cmmd
        {
            get { return Cmmd; }
            set { Cmmd = value; }
        }
        #endregion

        #region MÉTODOS
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Contratos
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los contratos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Agosto/2012
        ///*********************************************************************************************************
        public DataTable Consultar_Contratos()
        {
            return Cls_Cat_Con_Contratos_Datos.Consultar_Contratos(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Contratos_Detalles
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los detalles de los contratos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Agosto/2012
        ///*********************************************************************************************************
        public DataTable Consultar_Contratos_Detalles()
        {
            return Cls_Cat_Con_Contratos_Datos.Consultar_Contratos_Detalles(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Dependencias
        ///DESCRIPCIÓN          : Metodo para obtener los datos de las dependencias
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Agosto/2012
        ///*********************************************************************************************************
        public DataTable Consultar_Dependencias()
        {
            return Cls_Cat_Con_Contratos_Datos.Consultar_Dependencias(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Acciones_Programa_Convenio
        ///DESCRIPCIÓN          : Metodo para obtener los datos del presupuesto de un programa
        ///PROPIEDADES          :
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 14/Septiembre/2012
        ///*********************************************************************************************************
        public DataTable Consultar_Acciones_Programa_Convenio()
        {
            return Cls_Cat_Con_Contratos_Datos.Consultar_Acciones_Programa_Convenio(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Contratos_Detalles
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los detalles de los contratos
        ///PROPIEDADES          :
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 17/Septimebre/2012
        ///*********************************************************************************************************
        public DataTable Consultar_Contratos_Acciones()
        {
            return Cls_Cat_Con_Contratos_Datos.Consultar_Contratos_Acciones(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Programas
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los programas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Agosto/2012
        ///*********************************************************************************************************
        public DataTable Consultar_Programas()
        {
            return Cls_Cat_Con_Contratos_Datos.Consultar_Programas(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Fuentes
        ///DESCRIPCIÓN          : Metodo para obtener los datos de las fuentes de financiamiento
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Agosto/2012
        ///*********************************************************************************************************
        public DataTable Consultar_Fuentes()
        {
            return Cls_Cat_Con_Contratos_Datos.Consultar_Fuentes(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas
        ///DESCRIPCIÓN          : Metodo para obtener los datos de las partidas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Agosto/2012
        ///*********************************************************************************************************
        public DataTable Consultar_Partidas()
        {
            return Cls_Cat_Con_Contratos_Datos.Consultar_Partidas(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Presupuesto
        ///DESCRIPCIÓN          : Metodo para obtener los datos del presupuesto de un programa
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Agosto/2012
        ///*********************************************************************************************************
        public DataTable Consultar_Presupuesto()
        {
            return Cls_Cat_Con_Contratos_Datos.Consultar_Presupuesto(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Proveedor
        ///DESCRIPCIÓN          : Metodo para obtener los datos de un prooveedor
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Agosto/2012
        ///*********************************************************************************************************
        public DataTable Consultar_Proveedor()
        {
            return Cls_Cat_Con_Contratos_Datos.Consultar_Proveedor(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_contratos
        ///DESCRIPCIÓN          : Metodo para guardar los datos de los contratos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Agosto/2012
        ///*********************************************************************************************************
        public String Alta_contratos()
        {
            return Cls_Cat_Con_Contratos_Datos.Alta_contratos(this);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Contratos
        ///DESCRIPCIÓN          : Metodo para modificar los datos de los contratos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Agosto/2012
        ///*********************************************************************************************************
        public Boolean Modificar_Contratos()
        {
            return Cls_Cat_Con_Contratos_Datos.Modificar_Contratos(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Contratos_Ampliacion
        ///DESCRIPCIÓN          : Metodo para modificar los datos de los contratos
        ///PROPIEDADES          :
        ///CREO                 : Armando Zavala Moreno
        ///FECHA_CREO           : 03/Septiembre/2012
        ///*********************************************************************************************************
        public Boolean Modificar_Contratos_Ampliacion()
        {
            return Cls_Cat_Con_Contratos_Datos.Modificar_Contratos_Ampliacion(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Guardar_Archivo_Directo
        ///DESCRIPCIÓN          : Metodo para guardar un documento de la ampliacion de contrato
        ///PROPIEDADES          :
        ///CREO                 : Armando Zavala Moreno
        ///FECHA_CREO           : 18/Septiembre/2012
        ///*********************************************************************************************************
        public String Guardar_Archivo_Directo()
        {
            return Cls_Cat_Con_Contratos_Datos.Guardar_Archivo_Directo(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Saldo_Disponible
        ///DESCRIPCIÓN          : Metodo para consultar el saldo disponible de un contrato
        ///PROPIEDADES          :
        ///CREO                 : Armando Zavala Moreno
        ///FECHA_CREO           : 19/Septiembre/2012
        ///*********************************************************************************************************
        public Double Saldo_Disponible()
        {
            return Cls_Cat_Con_Contratos_Datos.Saldo_Disponible(this);
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Contratos_Saldo_Ampliacion
        ///DESCRIPCIÓN          : Metodo para consultar el saldo disponible de un contrato
        ///PROPIEDADES          :
        ///CREO                 : Armando Zavala Moreno
        ///FECHA_CREO           : 20/Septiembre/2012
        ///*********************************************************************************************************
        public Boolean Modificar_Contratos_Saldo_Ampliacion()
        {
            return Cls_Cat_Con_Contratos_Datos.Modificar_Contratos_Saldo_Ampliacion(this);
        }
        #endregion
    }
}
