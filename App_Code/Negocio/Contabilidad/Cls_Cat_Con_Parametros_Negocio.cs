﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Parametros_Contabilidad.Datos;

namespace JAPAMI.Parametros_Contabilidad.Negocio
{
    public class Cls_Cat_Con_Parametros_Negocio
    {
        #region (Variables_Internas)
            private string Parametro_Contabilidad_ID;
            private string Mascara_Cuenta_Contable;
            private string Mes_Contable;
            private string Usuario_Creo;
            private string Fecha_Creo;
            private string Usuario_Modifico;
            private string Fecha_Modifico;
            private string No_Empleado;
            private string Usuario_Autoriza_Inv;
            private string Usuario_Autoriza_Inv_2;
            private string IVA;
            private string Retencion_IVA;
            private string Cedular;
            private string ISR;
            private string Iva_Inversiones;
            private string CAP;
            private string ICIC;
            private string RAPCE;
            private string DIVO;
            private string SEFUPU;
            private string OBS;
            private string Est_Y_Proy;
            private string Lab_Control_C;
            private string CTA_Retencion_IVA;
            private string CTA_Cedular;
            private string CTA_ISR;
            private string CTA_CAP;
            private string CTA_ICIC;
            private string CTA_RAPCE;
            private string CTA_DIVO;
            private string CTA_SEFUPU;
            private string CTA_OBS;
            private string CTA_Est_Y_Proy;
            private string CTA_Lab_Control_C;
            public string P_Fte_Municipal { get; set; }
            public string P_Fte_Ramo33 { get; set; }
            public string P_Programa_Municipal { get; set; }
            public string P_Programa_Ramo33 { get; set; }
            public string P_Busqueda { get; set; }
            public string P_Tipo_Fte { get; set; }
            public string Devengado_Ing;
            public string Obras_Proceso;
            public string ISR_ARRENDAMIENTO;
            public string CEDULAR_ARRENDAMIENTO;
            public string HONO_ARRENDAMIENTO;
            public string ISR_ARRENDAMIENTO_MONTO;
            public string CEDULAR_ARRENDAMIENTO_MONTO;
            public string HONO_ARRENDAMIENTO_MONTO;
            public string CTA_BAJA_ACTIVO;
            public string Tipo_Poliza_Baja_ID;
            public SqlCommand Cmmd;
            public string Campo_Escondido_CONAC;
            public string CTA_COMISION_BANCARIA;
            private String Cuenta_Banco_Otros;
            private String Cuenta_Por_Pagar_Deudor;
            private String Tipo_Solicitud_Revolvente;
            private String Tipo_Caja_Chica;
            private String Monto_Caja_Chica;
            private String Deudor_Caja_Chica;
            private String Usuario_Autoriza_Gastos;
            private String Dias_Comprobacion;
        #endregion

        #region (Variables_Publicas)
            public string P_Deudor_Caja_Chica
            {
                get { return Deudor_Caja_Chica; }
                set { Deudor_Caja_Chica = value; }
            }
            public string P_Monto_Caja_Chica
            {
                get {return Monto_Caja_Chica; }
                set{ Monto_Caja_Chica=value;}
            }
            public string P_Cuenta_Banco_Otros
            {
                get { return Cuenta_Banco_Otros; }
                set { Cuenta_Banco_Otros = value; }
            }
            public string P_Cuenta_Por_Pagar_Deudor
            {
                get { return Cuenta_Por_Pagar_Deudor; }
                set { Cuenta_Por_Pagar_Deudor = value; }
            }
            public string P_Tipo_Caja_Chica
            {
                get { return Tipo_Caja_Chica; }
                set { Tipo_Caja_Chica = value; }
            }
            public string P_Tipo_Solicitud_Revolvente
            {
                get { return Tipo_Solicitud_Revolvente; }
                set { Tipo_Solicitud_Revolvente = value; }
            }
            public string P_Iva_Inversiones
            {
                get { return Iva_Inversiones; }
                set { Iva_Inversiones = value;}
            }
            public string P_CAP
            {
                get { return CAP; }
                set { CAP = value; }
            }
            public string P_ICIC
            {
                get { return ICIC; }
                set { ICIC = value; }
            }
            public string P_RAPCE
            {
                get { return RAPCE; }
                set { RAPCE = value; }
            }
            public string P_DIVO
            {
                get { return DIVO; }
                set { DIVO = value; }
            }
            public string P_SEFUPU
            {
                get { return SEFUPU; }
                set { SEFUPU = value; }
            }
            public string P_OBS
            {
                get { return OBS; }
                set { OBS = value; }
            }
            public string P_Est_Y_Proy
            {
                get { return Est_Y_Proy; }
                set { Est_Y_Proy = value; }
            }
            public string P_Lab_Control_C
            {
                get { return Lab_Control_C; }
                set { Lab_Control_C = value; }
            }
            public string P_CTA_CAP
            {
                get { return CTA_CAP; }
                set { CTA_CAP = value; }
            }
            public string P_CTA_ICIC
            {
                get { return CTA_ICIC; }
                set { CTA_ICIC = value; }
            }
            public string P_CTA_RAPCE
            {
                get { return CTA_RAPCE; }
                set { CTA_RAPCE = value; }
            }
            public string P_CTA_DIVO
            {
                get { return CTA_DIVO; }
                set { CTA_DIVO = value; }
            }
            public string P_CTA_SEFUPU
            {
                get { return CTA_SEFUPU; }
                set { CTA_SEFUPU = value; }
            }
            public string P_CTA_OBS
            {
                get { return CTA_OBS; }
                set { CTA_OBS = value; }
            }
            public string P_CTA_Est_Y_Proy
            {
                get { return CTA_Est_Y_Proy; }
                set { CTA_Est_Y_Proy = value; }
            }
            public string P_CTA_Lab_Control_C
            {
                get { return CTA_Lab_Control_C; }
                set { CTA_Lab_Control_C = value; }
            }  
            public string P_No_Empleado
            {
                get { return No_Empleado; }
                set { No_Empleado = value; }
            }
            public string P_Usuario_Autoriza_Inv
            {
                get { return Usuario_Autoriza_Inv; }
                set { Usuario_Autoriza_Inv = value; }
            }
            public string P_Usuario_Autoriza_Inv_2
            {
                get { return Usuario_Autoriza_Inv_2; }
                set { Usuario_Autoriza_Inv_2 = value; }
            }
            public string P_Mes_Contable
            {
                get { return Mes_Contable; }
                set { Mes_Contable = value; }
            }
            public string P_Parametro_Contabilidad_ID
            {
                get { return Parametro_Contabilidad_ID; }
                set { Parametro_Contabilidad_ID = value; }
            }
            public string P_Mascara_Cuenta_Contable
            {
                get { return Mascara_Cuenta_Contable; }
                set { Mascara_Cuenta_Contable = value; }
            }
            public string P_Usuario_Creo
            {
                get { return Usuario_Creo; }
                set { Usuario_Creo = value; }
            }
            public string P_Fecha_Creo
            {
                get { return Fecha_Creo; }
                set { Fecha_Creo = value; }
            }
            public string P_Usuario_Modifico
            {
                get { return Usuario_Modifico; }
                set { Usuario_Modifico = value; }
            }
            public string P_Fecha_Modifico
            {
                get { return Fecha_Modifico; }
                set { Fecha_Modifico = value; }
            }
            public string P_IVA
            {
                get { return IVA; }
                set { IVA = value; }
            }
            public string P_Retencion_IVA
            {
                get { return Retencion_IVA; }
                set { Retencion_IVA = value; }
            }
            public string P_Cedular
            {
                get { return Cedular; }
                set { Cedular = value; }
            }
            public string P_ISR
            {
                get { return ISR; }
                set { ISR = value; }
            }
            public string P_CTA_Retencion_IVA
            {
                get { return CTA_Retencion_IVA; }
                set { CTA_Retencion_IVA = value; }
            }
            public string P_CTA_Cedular
            {
                get { return CTA_Cedular; }
                set { CTA_Cedular = value; }
            }
            public string P_CTA_ISR
            {
                get { return CTA_ISR; }
                set { CTA_ISR = value; }
            }
            public string P_Devengado_Ing
            {
                get { return Devengado_Ing; }
                set { Devengado_Ing = value; }
            }
            public string P_Obras_Proceso
            {
                get { return Obras_Proceso; }
                set { Obras_Proceso = value; }
            }
            public string P_ISR_Arrendamiento
            {
                get { return ISR_ARRENDAMIENTO; }
                set { ISR_ARRENDAMIENTO = value; }
            }
            public string P_Cedular_Arrendamiento
            {
                get { return CEDULAR_ARRENDAMIENTO; }
                set { CEDULAR_ARRENDAMIENTO = value; }
            }
            public string P_Honorarios_Arrendamiento
            {
                get { return HONO_ARRENDAMIENTO; }
                set { HONO_ARRENDAMIENTO = value; }
            }
            public string P_ISR_Arrendamiento_Monto
            {
                get { return ISR_ARRENDAMIENTO_MONTO; }
                set { ISR_ARRENDAMIENTO_MONTO = value; }
            }
            public string P_Cedular_Arrendamiento_Monto
            {
                get { return CEDULAR_ARRENDAMIENTO_MONTO; }
                set { CEDULAR_ARRENDAMIENTO_MONTO = value; }
            }
            public string P_Honorarios_Arrendamiento_Monto
            {
                get { return HONO_ARRENDAMIENTO_MONTO; }
                set { HONO_ARRENDAMIENTO_MONTO = value; }
            }
            public string P_CTA_Baja_Activo
            {
                get { return CTA_BAJA_ACTIVO; }
                set { CTA_BAJA_ACTIVO = value; }
            }
            public string P_Tipo_Poliza_Baja_ID
            {
                get
                {
                    return Tipo_Poliza_Baja_ID;
                }
                set
                {
                    Tipo_Poliza_Baja_ID = value;
                }
            }
            public SqlCommand P_Cmmd
            {
                get { return Cmmd; }
                set { Cmmd = value; }
            }

            public string P_Campo_Escondido_CONAC
            {
                get { return Campo_Escondido_CONAC; }
                set { Campo_Escondido_CONAC = value; }
            }

            public string P_CTA_COMISION_BANCARIA
            {
                get { return CTA_COMISION_BANCARIA; }
                set { CTA_COMISION_BANCARIA = value; }
            }
            public string P_Usuario_Autoriza_Gastos
            {
                get { return Usuario_Autoriza_Gastos; }
                set { Usuario_Autoriza_Gastos = value; }
            }
            public string P_Dias_Comprobacion
            {
                get { return Dias_Comprobacion; }
                set { Dias_Comprobacion = value; }
            }
        #endregion

        #region (Metodos)
            public void Alta_Parametros()
            {
                Cls_Cat_Con_Parametros_Datos.Alta_Parametros(this);
            }
            public DataTable Consulta_Parametros()
            {
                return Cls_Cat_Con_Parametros_Datos.Consulta_Parametros();
            }
            public void Modificar_Parametros()
            {
                Cls_Cat_Con_Parametros_Datos.Modificar_Parametros(this);
            }
            public void Modificar_Parametros_Autorizacion()
            {
                Cls_Cat_Con_Parametros_Datos.Modificar_Parametros_Autorizacion(this);
            }
            public DataTable Consulta_Datos_Parametros()
            {
                return Cls_Cat_Con_Parametros_Datos.Consulta_Datos_Parametros(this);
            }
            public DataTable Consulta_Datos_Parametros_2()
            {
                return Cls_Cat_Con_Parametros_Datos.Consulta_Datos_Parametros_2();
            }
            public DataTable Consulta_Empleados()
            {
                return Cls_Cat_Con_Parametros_Datos.Consulta_Empleados(this);
            }
        #endregion

        #region (Datos Ingresos)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Fte_Financiamiento
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 03/Diciembre/2012 
            ///*********************************************************************************************************
            public DataTable Consulta_Fte_Financiamiento()
            {
                return Cls_Cat_Con_Parametros_Datos.Consulta_Fte_Financiamiento(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Programas
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 03/Diciembre/2012 
            ///*********************************************************************************************************
            public DataTable Consulta_Programas()
            {
                return Cls_Cat_Con_Parametros_Datos.Consulta_Programas(this);
            }
        #endregion
    }
}
