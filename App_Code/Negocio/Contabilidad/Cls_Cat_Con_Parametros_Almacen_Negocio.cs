﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Con_Parametros_Almacen.Datos;

/// <summary>
/// Summary description for Cls_Cat_Con_Parametros_Almacen_Negocio
/// </summary>
/// 
namespace JAPAMI.Con_Parametros_Almacen.Negocio
{

    public class Cls_Cat_Con_Parametros_Almacen_Negocio
    {
        ///*******************************************************************************
        /// VARIABLES INTERNAS 
        ///******************************************************************************
        #region Variables_Internas
        private String Cuenta_Almacen_General;
        private String Cuenta_Almacen_Papeleria;
        private String Cuenta_IVA_Acreditable;
        private String Cuenta_IVA_Pendiente;

        
        private String Tipo_Poliza_Egresos_ID;
        private String Tipo_Poliza_Diario_ID;
        private String Cuenta_Busqueda;
        private String Cuenta_ID;
        private String Cuentas_ID;

        #endregion

        ///*******************************************************************************
        /// VARIABLES PUBLICAS
        ///*******************************************************************************
        #region Variables_Publicas
        public String P_Cuenta_IVA_Pendiente
        {
            get { return Cuenta_IVA_Pendiente; }
            set { Cuenta_IVA_Pendiente = value; }
        }
        public String P_Cuenta_Almacen_General
        {
            get { return Cuenta_Almacen_General; }
            set { Cuenta_Almacen_General = value; }
        }

        public String P_Cuenta_Almacen_Papeleria
        {
            get { return Cuenta_Almacen_Papeleria; }
            set { Cuenta_Almacen_Papeleria = value; }
        }
        public String P_Cuenta_IVA_Acreditable
        {
            get { return Cuenta_IVA_Acreditable; }
            set { Cuenta_IVA_Acreditable = value; }
        }

        public String P_Tipo_Poliza_Egresos_ID
        {
            get { return Tipo_Poliza_Egresos_ID; }
            set { Tipo_Poliza_Egresos_ID = value; }
        }

        public String P_Tipo_Poliza_Diario_ID
        {
            get { return Tipo_Poliza_Diario_ID; }
            set { Tipo_Poliza_Diario_ID = value; }
        }

        public String P_Cuenta_Busqueda
        {
            get { return Cuenta_Busqueda; }
            set { Cuenta_Busqueda = value; }
        }

        public String P_Cuenta_ID
        {
            get { return Cuenta_ID; }
            set { Cuenta_ID = value; }
        }

        public String P_Cuentas_ID
        {
            get { return Cuentas_ID; }
            set { Cuentas_ID = value; }
        }
        #endregion

        ///*******************************************************************************
        /// METODOS
        ///*******************************************************************************
        #region Metodos

        public DataTable Consultar_Tipos_Polizas()
        {
            return Cls_Cat_Con_Parametros_Almacen_Datos.Consultar_Tipos_Polizas(this);
        }
        public DataTable Consultar_Parametros()
        {
            return Cls_Cat_Con_Parametros_Almacen_Datos.Consultar_Parametros(this);
        }
        public DataTable Consultar_Cuentas_Contables()
        {
            return Cls_Cat_Con_Parametros_Almacen_Datos.Consultar_Cuentas_Cotables(this);
        }
        public void Alta_Parametros_Almacen() 
        {
            Cls_Cat_Con_Parametros_Almacen_Datos.Alta_Parametros_Almacen(this);
        }
        public void Modificar_Parametros_Almacen()
        {
            Cls_Cat_Con_Parametros_Almacen_Datos.Modificar_Parametros_Almacen(this);
        }
        public DataTable Consultar_Cuentas_Almacen()
        {
            return Cls_Cat_Con_Parametros_Almacen_Datos.Consultar_Cuentas_Almacen(this);
        }
        #endregion


    }//Fin del Class
}//Fin del namespace