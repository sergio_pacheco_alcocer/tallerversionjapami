﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Autoriza_Ejercido.Datos;
/// <summary>
/// Summary description for Cls_Ope_Con_Autoriza_Solicitud_Pago
/// </summary>
/// 
namespace JAPAMI.Autoriza_Ejercido.Negocio
{
    public class Cls_Ope_Con_Autoriza_Ejercido_Negocio
    {
        #region "Variables_Internas"
        private string No_Solicitud_Pago;
        private string Mes_Anio;
        private string Estatus;
        private string Empleado_ID_Ejercido;
        private string Fecha_Solicitud;
        private string Tipo_Solicitud_Pago_ID;
        private string Comentario;
        private string Monto;
        private string Fecha_Autorizo_Rechazo_Ejercido;
        private string Usuario_Creo;
        private string Fecha_Creo;
        private string Usuario_Modifico;
        private string Fecha_Modifico;
        private string Tipo_Recurso;
        private string Usuario_Recibio_Ejercido;
        private string Fecha_Recibio_Ejercido;
        private string Usuario_Autorizo_Ejercido;
        private string Usuario_Recibio_Pagado;
        private string Forma_Pago;
        private string Cuenta_Banco_Pago;
        private SqlCommand Cmmd;
        #endregion

        #region ""Variables Externas"
        public string P_Tipo_Recurso
        {
            get { return Tipo_Recurso; }
            set { Tipo_Recurso = value; }
        }
        public string P_Empleado_ID_Ejercido
        {
            get { return Empleado_ID_Ejercido; }
            set { Empleado_ID_Ejercido = value; }
        }
        public string P_No_Solicitud_Pago
        {
            get { return No_Solicitud_Pago; }
            set { No_Solicitud_Pago = value; }
        }
        public string P_Mes_Anio {
            get { return Mes_Anio; }
            set { Mes_Anio = value; }
        }
        public string P_Estatus
        {
            get { return Estatus; }
            set { Estatus = value; }
        }
        public string P_Fecha_Solicitud
        {
            get { return Fecha_Solicitud; }
            set { Fecha_Solicitud = value; }
        }
        public string P_Tipo_Solicitud_Pago_ID
        {
            get { return Tipo_Solicitud_Pago_ID; }
            set { Tipo_Solicitud_Pago_ID = value; }
        }
        public string P_Comentario
        {
            get { return Comentario; }
            set { Comentario = value; }
        }
        public string P_Monto
        {
            get { return Monto; }
            set { Monto = value; }
        }
        public string P_Usuario_Creo
        {
            get { return Usuario_Creo; }
            set { Usuario_Creo = value; }
        }
        public string P_Fecha_Creo
        {
            get { return Fecha_Creo; }
            set { Fecha_Creo = value; }
        }
        public string P_Usuario_Modifico
        {
            get { return Usuario_Modifico; }
            set { Usuario_Modifico = value; }
        }
        public string P_Fecha_Modifico
        {
            get { return Fecha_Modifico; }
            set { Fecha_Modifico = value; }
        }
        public string P_Fecha_Autorizo_Rechazo_Ejercido
        {
            get { return Fecha_Autorizo_Rechazo_Ejercido; }
            set { Fecha_Autorizo_Rechazo_Ejercido = value; }
        }
        public string P_Usuario_Recibio_Ejercido
        {
            get { return Usuario_Recibio_Ejercido; }
            set { Usuario_Recibio_Ejercido = value; }
        }
        public string P_Fecha_Recibio_Ejercido
        {
            get { return Fecha_Recibio_Ejercido; }
            set { Fecha_Recibio_Ejercido = value; }
        }
        public string P_Usuario_Autorizo_Ejercido
        {
            get { return Usuario_Autorizo_Ejercido; }
            set { Usuario_Autorizo_Ejercido = value; }
        }
        public string P_Usuario_Recibio_Pagado
        {
            get { return Usuario_Recibio_Pagado; }
            set { Usuario_Recibio_Pagado = value; }
        }
        public string P_Forma_Pago
        {
            get { return Forma_Pago; }
            set { Forma_Pago = value; }
        }
        public string P_Cuenta_Banco_Pago
        {
            get { return Cuenta_Banco_Pago; }
            set { Cuenta_Banco_Pago = value; }
        }

        public SqlCommand P_Cmmd
        {
            get { return Cmmd; }
            set { Cmmd = value; }
        }
        #endregion

        #region "Metodos"
        public DataTable Consulta_Solicitudes_SinEjercer()
        {
            return Cls_Ope_Con_Autoriza_Ejercido_Datos.Consulta_Solicitudes_SinEjercer(this);
        }
        public void Cambiar_Estatus_Solicitud_Pago()
        {
            Cls_Ope_Con_Autoriza_Ejercido_Datos.Cambiar_Estatus_Solicitud_Pago(this);
        }
        public DataTable Consultar_Solicitud_Pago()
        {
            return Cls_Ope_Con_Autoriza_Ejercido_Datos.Consultar_Solicitud_Pago(this);
        }
        public DataTable Consulta_Solicitud_Pago_Completa()
        {
            return Cls_Ope_Con_Autoriza_Ejercido_Datos.Consulta_Solicitud_Pago_Completa(this);
        }
        #endregion
    }
}


