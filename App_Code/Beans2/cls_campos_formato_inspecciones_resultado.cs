﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Formato_Recibo_formato_inpsecciones_resultado
{


    public class cls_campos_formato_inspecciones_resultado
    {

        public string folio { get; set; }
        public string motivo { get; set; }
        public string solicitud { get; set; }
        public string fecha_solicitud { get; set; }
        public string estatus { get; set; }
        public string usuario_creo { get; set; }
        public string usuario_registro { get; set; }
        public string observacion_inicial { get; set; }
        public string observacion_final { get; set; }
        public string motivo_cancelacion { get; set; }
        public string no_inspeccion { get; set; }
        public string fecha_realizacion { get; set; }
        public string inspector { get; set; }
        public string numero_exterior { get; set; }
        public string numero_interior { get; set; }
        public string nombre_calle { get; set; }
        public string colonia { get; set; }
        public string no_cuenta { get; set; }
        public string procede_inspeccion { get; set; }



    }
}