﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Parametros_Busqueda.PagosEstablecimiento{
 
    public class Cls_Parametros_Pagos_establecimiento
    {

        public string fecha_inicio { get; set; }
        public string fecha_final {get; set;}
        public string institucion { get; set; }
        public string sucursal { get; set; }
        public string estado { get; set; }

   
    }    
}


