﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Parametros_Busqueda.Descargas {

    public class cls_parametros_descargas
    {
        public cls_parametros_descargas()
        {

        }

        public string fecha_inicio { get; set; }
        public string fecha_final { get; set; }
        public string nombre { get; set; }
    }

}


