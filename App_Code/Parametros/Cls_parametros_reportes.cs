﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;




namespace JAPAMI.Parametros.reportes
{


    /// <summary>
    /// Summary description for Cls_parametros_reportes
    /// </summary>
    public class Cls_parametros_reportes
    {
        public Cls_parametros_reportes()
        {
          
        }

        public static string RequstForm(string name)
        {

            return (HttpContext.Current.Request.Form[name] == null ? string.Empty : HttpContext.Current.Request.Form[name].ToString().Trim());

        }

        public static string RequstString(string sParam)
        {

            return (HttpContext.Current.Request[sParam] == null ? string.Empty : HttpContext.Current.Request[sParam].ToString().Trim());

        }

        public static int RequstInt(string sParam)
        {

            int iValue;

            string sValue = RequstString(sParam);

            int.TryParse(sValue, out iValue);

            return iValue;

        }

        public static string accion
        {
            get { return RequstString("accion"); }
        }

        public static string controles
        {
            get { return RequstString("controles"); }
        
        }

        public static string fechaInicio
        {
            get { return RequstString("fecha_inicio"); }
        }

        public static string fechaFinal {
            get { return RequstString("fecha_final"); }
        }

        public static string no_cuenta {
            get { return RequstString("nocuenta"); }
          
        }

        public static string descripcion_edicion {
            get { return RequstString("descripcion_edicion"); }    
        }



        public static string obtenerNombreUsuario
        {
            get { return RequstString("nombre"); }
        }


        public static string obtenerCadenaBuscar
        {
            get { return RequstString("cadena_buscar"); }
        }


        public static string nombreReporte {        
            get {return RequstString("nombre_reporte");}
        }

        public static string nota_cargo_id {
            get { return RequstString("nota_cargo_id"); }
        }

        public static string bonificacion_id {
            get { return RequstString("bonificacion_id"); }
        }

        public static string region {
            get { return RequstString("region"); }
        }

        public static string no_folio {
            get { return RequstString("no_folio"); }
        }

        public static string operacion_caja_id
        {
            get { return RequstString("operacion_caja_id"); }
        }

        public static string no_factura_recibo {
            get { return RequstString("no_factura_recibo"); }
        }

        public static string abono
        {
            get { return RequstString("abono"); }
        }

        public static string no_diverso
        {
            get { return RequstString("no_diverso"); }
        }

        public static string no_Bonificacion
        {
            get { return RequstString("no_Bonificacion"); }
        }


        public static string no_factura_recibo_parcial
        {
            get { return RequstString("no_factura_recibo_parcial"); }
        }


        public static string no_contrato {
            get { return RequstString("no_contrato"); }
        }

        public static string no_Pago_Convenio
        {
            get { return RequstString("noPagoConvenio"); }
        }


        public static string no_retiro {

            get { return RequstString("no_retiro"); }

        }

        public static string no_corte
        {
           get {return RequstString("no_corte"); }
        }

       

        public static string datosDistrubicionMoneda {
            get { return RequstString("datos_distribucion_moneda"); }
        
        }

    }


}