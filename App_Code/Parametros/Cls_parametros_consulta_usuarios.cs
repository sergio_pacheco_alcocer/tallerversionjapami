﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Parametros.Consulta_usuario {

    public class Cls_parametros_consulta_usuarios
    {
        public Cls_parametros_consulta_usuarios()
        {

        }

        public static string RequstForm(string name)
        {

            return (HttpContext.Current.Request.Form[name] == null ? string.Empty : HttpContext.Current.Request.Form[name].ToString().Trim());

        }

        public static string RequstString(string sParam)
        {

            return (HttpContext.Current.Request[sParam] == null ? string.Empty : HttpContext.Current.Request[sParam].ToString().Trim());

        }

        public static int RequstInt(string sParam)
        {

            int iValue;

            string sValue = RequstString(sParam);

            int.TryParse(sValue, out iValue);

            return iValue;

        }


        public static string accion
        {
            get { return RequstString("accion"); }
        }

        public static string obtenerNoCuenta
        {
            get { return RequstString("nocuenta"); }
        }


        public static string obtenerNoConvenio
        {
            get { return RequstString("noconvenio"); }
        }


        public static string obtenerNombreUsuario
        {
            get { return RequstString("nombre"); }
        }

        public static string obtenerNoFactura { 
           get { return RequstString("no_factura_recibo");}
        }

        public static string obtenerDatos {
            get { return RequstString("datos"); }
        }

        public static string id_factura_recibo_historial
        {
            get { return RequstString("id_factura_recibo_historial"); }
        }

        public static string obtener_letra
        {
            get { return RequstString("q"); }
        }

    }


}

