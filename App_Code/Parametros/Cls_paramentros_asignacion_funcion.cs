﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Cls_paramentros_asignacion_funcion
/// </summary>
/// 
namespace JAPAMI.Parametros.Asignacion_Funcion
{
    public class Cls_paramentros_asignacion_funcion
    {
        public Cls_paramentros_asignacion_funcion()
        { }

        public static string RequstForm(string name)
        {

            return (HttpContext.Current.Request.Form[name] == null ? string.Empty : HttpContext.Current.Request.Form[name].ToString().Trim());

        }

        public static string RequstString(string sParam)
        {

            return (HttpContext.Current.Request[sParam] == null ? string.Empty : HttpContext.Current.Request[sParam].ToString().Trim());

        }

        public static int RequstInt(string sParam)
        {

            int iValue;

            string sValue = RequstString(sParam);

            int.TryParse(sValue, out iValue);

            return iValue;

        }

        public static string accion {
            get { return RequstString("accion"); }  
        }

        public static string texto_buscar {
            get { return RequstString("busqueda"); }
        }



    }

}

