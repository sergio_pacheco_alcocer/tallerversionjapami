﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Cls_Parametros_Solicitudes_Contrato_Servicio
/// </summary>
/// 
namespace JAPAMI.Solicitudes_Contrato_Servicio.Parametros
{
    public class Cls_Parametros_Solicitudes_Contrato_Servicio
    {
        public Cls_Parametros_Solicitudes_Contrato_Servicio() { }

        public static string RequstForm(string name)
        {
            return (HttpContext.Current.Request.Form[name] == null ? string.Empty : HttpContext.Current.Request.Form[name].ToString().Trim());
        }

        public static string RequstString(string sParam)
        {
            return (HttpContext.Current.Request[sParam] == null ? string.Empty : HttpContext.Current.Request[sParam].ToString().Trim());
        }

        public static int RequstInt(string sParam)
        {
            int iValue;

            string sValue = RequstString(sParam);

            int.TryParse(sValue, out iValue);

            return iValue;
        }

        public static string accion
        {
            get { return RequstString("accion"); }
        }

        public static string colonia_id
        {
            get { return RequstString("colonia_id"); }
        }

        public static string obtener_letra
        {
            get { return RequstString("q"); }
        }
    }
}