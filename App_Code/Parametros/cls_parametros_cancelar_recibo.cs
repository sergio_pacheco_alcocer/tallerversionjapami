﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace JAPAMI.Parametros.cancelar_recibos {

    public class cls_parametros_cancelar_recibo
    {
        public static string RequstForm(string name)
        {

            return (HttpContext.Current.Request.Form[name] == null ? string.Empty : HttpContext.Current.Request.Form[name].ToString().Trim());

        }

        public static string RequstString(string sParam)
        {

            return (HttpContext.Current.Request[sParam] == null ? string.Empty : HttpContext.Current.Request[sParam].ToString().Trim());

        }

        public static string accion
        {
            get { return RequstString("accion"); }
        }


        public static string fecha_inicio
        {
            get { return RequstString("f_inicio"); }
        }

        public static string fecha_final
        {
            get { return RequstString("f_final"); }
        }

        public static string estado_pago
        {
            get { return RequstString("estado"); }
        }

        public static string obtener_bonificacion_id {
            get { return RequstString("bonificacion_id"); }
        }

        public static string obtener_no_recibo {
            get { return RequstString("no_recibo"); }
        }
        public static string No_Cuenta
        {
            get { return RequstString("no_cuenta"); }
        }

    }

}


