﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Cls_Parametros_Ope_Con_Polizas
/// </summary>
namespace JAPAMI.Parametros.Ope_Con_Polizas
{
    public class Cls_Parametros_Ope_Con_Polizas
    {
        public Cls_Parametros_Ope_Con_Polizas()
        {
        }

        public static String Entrega_Cadena_Request(String parametro)
        {
            //Declaracion de variables
            String resultado = String.Empty; //variable para el resultado

            try
            {
                //Verificar si existe el parametro
                if (HttpContext.Current.Request[parametro] != null && HttpContext.Current.Request[parametro] != String.Empty)
                {
                    resultado = HttpContext.Current.Request[parametro].ToString().Trim();
                }

                //Entregar resultado
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static Int64 Entrega_Entero_Request(String parametro)
        {
            //Declaracion de variables
            Int64 resultado = 0; //variable para el resultado
            String strResultado = String.Empty; //variable auxiliar para el resultado

            try
            {
                //Verificar si existe el parametro
                if (HttpContext.Current.Request[parametro] != null && HttpContext.Current.Request[parametro] != String.Empty)
                {
                    //Obtener el valor del parametro como una cadena
                    strResultado = HttpContext.Current.Request[parametro].ToString().Trim();

                    //Convertir la cadena a entero
                    Int64.TryParse(strResultado, out resultado);
                }

                //Entregar resultado 
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static int Entrega_Entero_min_Request(String parametro)
        {
            //Declaracion de variables
            int resultado = 0; //variable para el resultado
            String strResultado = String.Empty; //variable auxiliar para el resultado

            try
            {
                //Verificar si existe el parametro
                if (HttpContext.Current.Request[parametro] != null && HttpContext.Current.Request[parametro] != String.Empty)
                {
                    //Obtener el valor del parametro como una cadena
                    strResultado = HttpContext.Current.Request[parametro].ToString().Trim();

                    //Convertir la cadena a entero
                    int.TryParse(strResultado, out resultado);
                }

                //Entregar resultado 
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static String Parametro_Tipo_Consulta()
        {
            return Entrega_Cadena_Request("Tipo_Consulta");
        }

        public static String Parametro_Tipo_Poliza_ID()
        {
            return Entrega_Cadena_Request("Tipo_Poliza_ID");
        }

        public static String Parametro_Fecha()
        {
            return Entrega_Cadena_Request("Fecha");
        }

        public static String Parametro_Busqueda()
        {
            return Entrega_Cadena_Request("Busqueda");
        }

        public static String Parametro_Autocompletar()
        {
            return Entrega_Cadena_Request("q");
        }

        public static String Parametro_Datos_Poliza()
        {
            return Entrega_Cadena_Request("Datos_Poliza");
        }
    }
}