﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Cls_Parametros_Validacion_Lecturas
/// </summary>
namespace JAPAMI.Parametros.Validacion_Lecturas
{
    public class Cls_Parametros_Validacion_Lecturas
    {
        public Cls_Parametros_Validacion_Lecturas(){}

        public static String Entrega_Cadena_Request(String parametro)
        {
            //Declaracion de variables
            String resultado = String.Empty; //variable para el resultado

            try
            {
                //Verificar si existe el parametro
                if (HttpContext.Current.Request[parametro] != null && HttpContext.Current.Request[parametro] != String.Empty)
                {
                    resultado = HttpContext.Current.Request[parametro].ToString().Trim();
                }

                //Entregar resultado
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static Int64 Entrega_Entero_Request(String parametro)
        {
            //Declaracion de variables
            Int64 resultado = 0; //variable para el resultado
            String strResultado = String.Empty; //variable auxiliar para el resultado

            try
            {
                //Verificar si existe el parametro
                if (HttpContext.Current.Request[parametro] != null && HttpContext.Current.Request[parametro] != String.Empty)
                {
                    //Obtener el valor del parametro como una cadena
                    strResultado = HttpContext.Current.Request[parametro].ToString().Trim();
    
                    //Convertir la cadena a entero
                    Int64.TryParse(strResultado, out resultado);
                }

                //Entregar resultado 
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static int Entrega_Entero_min_Request(String parametro)
        {
            //Declaracion de variables
            int resultado = 0; //variable para el resultado
            String strResultado = String.Empty; //variable auxiliar para el resultado

            try
            {
                //Verificar si existe el parametro
                if (HttpContext.Current.Request[parametro] != null && HttpContext.Current.Request[parametro] != String.Empty)
                {
                    //Obtener el valor del parametro como una cadena
                    strResultado = HttpContext.Current.Request[parametro].ToString().Trim();

                    //Convertir la cadena a entero
                    int.TryParse(strResultado, out resultado);
                }

                //Entregar resultado 
                return resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static String Parametro_Tipo_Consulta()
        {
            return Entrega_Cadena_Request("Tipo_Consulta");
        }

        public static String Parametro_Titulo() {
            return Entrega_Cadena_Request("titulo");
        }

        public static String Parametro_Region_ID()
        {
            return Entrega_Cadena_Request("Region_ID");
        }

        public static String Parametro_Sector_ID()
        {
            return Entrega_Cadena_Request("Sector_ID");
        }

        public static Int64 Parametro_Mes()
        {
            return Entrega_Entero_Request("Mes");
        }

        public static Int64 Parametro_Anio()
        {
            return Entrega_Entero_Request("Anio");
        }

        public static Int64 Parametro_Tipo_Reporte()
        {
            return Entrega_Entero_Request("Tipo_Reporte");
        }

        public static String Parametro_Validacion_Lecturas()
        {
            return Entrega_Cadena_Request("Validacion_Lecturas");
        }

        public static int Parametro_Pagina()
        {
            return Entrega_Entero_min_Request("Pagina");
        }

        public static int Parametro_Cantidad_Renglones()
        {
            return Entrega_Entero_min_Request("Cantidad_Renglones");
        }
        public static Int64 Parametro_Consumo()
        {
            return Entrega_Entero_Request("Consumo");
        }
    }
}