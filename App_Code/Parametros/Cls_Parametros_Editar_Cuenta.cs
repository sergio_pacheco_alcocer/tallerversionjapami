﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;




namespace JAPAMI.Parametros_cuenta_usuario {

public class Cls_Parametros_Editar_Cuenta
{
	public Cls_Parametros_Editar_Cuenta()
	{
	
	}



    public static string RequstForm(string name)
    {

        return (HttpContext.Current.Request.Form[name] == null ? string.Empty : HttpContext.Current.Request.Form[name].ToString().Trim());

    }

    public static string RequstString(string sParam)
    {

        return (HttpContext.Current.Request[sParam] == null ? string.Empty : HttpContext.Current.Request[sParam].ToString().Trim());

    }

    public static int RequstInt(string sParam)
    {

        int iValue;

        string sValue = RequstString(sParam);

        int.TryParse(sValue, out iValue);

        return iValue;

    }


    public static string accion
    {
        get { return RequstString("accion"); }
    }

    public static string obtenerColonia_id
    {
        get { return RequstString("colonia_id"); }
    }


    public static string obtenerRegion_id
    {
        get { return RequstString("region_id"); }
    }


    public static string obtenerSector_id
    {
        get { return RequstString("sector_id"); }
    }

    

     public static string obtenerGiro_id
    {
        get { return RequstString("giro_id"); }
    }

    public static string obtener_letra
    {
        get { return RequstString("q"); }
    }




}

}