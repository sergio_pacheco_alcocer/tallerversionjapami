﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;




namespace JAPAMI.Parametros.Progamacion_lectura
{
    public class Cls_parametros_programacion_lectura
    {

        public static string RequstString(String sParam)
        {
            if (HttpContext.Current.Request[sParam] == null)
            {
                return String.Empty;
            }
            else {
                return HttpContext.Current.Request[sParam].ToString().Trim();
            }

        }


        public static Int64 RequstInt(String sParam)
        {
            Int64 iValue;
            String sValue;

            sValue = RequstString(sParam);
            Int64.TryParse(sValue, out iValue);

            return iValue;
        }


        public static string obtenerAccion()
        {
            return RequstString("accion");
        }

        public static string obtener_REGION_ID()
        {
            return RequstString("REGION_ID");
        }

        public static string obtener_SECTOR_ID() {
            return RequstString("SECTOR_ID");
        }

        public static string obtener_MANZANA_ID() {
            return RequstString("MANZANA_ID");
        }

        public static string obtener_COLONIA_ID() {
            return RequstString("COLONIA_ID");
        }

        public static string obtener_Ruta_Reparto_ID() {
            return RequstString("Ruta_Reparto_ID");
        }

        public static string obtener_nombre_colonia() {
            return RequstString("nombre");
        }
        public static string obtenerTipo() {
            return RequstString("tipo");
        }

        public static string obtener_nombre_manzana()
        {
            return RequstString("nombre_manzana");
        }


    }


}



