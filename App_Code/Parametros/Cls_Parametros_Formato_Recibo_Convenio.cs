﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;



namespace JAPAMI.Parametros.Recibo_Convenio
{

    public class Cls_Parametros_Formato_Recibo_Convenio
    {
        public Cls_Parametros_Formato_Recibo_Convenio()
        {

        }
        public static string RequstForm(string name)
        {

            return (HttpContext.Current.Request.Form[name] == null ? string.Empty : HttpContext.Current.Request.Form[name].ToString().Trim());

        }
        public static string RequstString(string sParam)
        {

            return (HttpContext.Current.Request[sParam] == null ? string.Empty : HttpContext.Current.Request[sParam].ToString().Trim());

        }
        public static int RequstInt(string sParam)
        {

            int iValue;

            string sValue = RequstString(sParam);

            int.TryParse(sValue, out iValue);

            return iValue;

        }      
        public static string no_Pago_Convenio
        {
            get { return RequstString("noPagoConvenio"); }
        }
        public static string no_factura
        {
            get { return RequstString("nofactura"); }
        }
        public static string codigo_barras
        {
            get { return RequstString("codigobarras"); }
        }
        public static string fecha_inicio_periodo
        {
            get { return RequstString("fechainicioperiodo"); }
        }
        public static string fecha_termino_periodo
        {
            get { return RequstString("fechaterminoperiodo"); }
        }
        public static string no_cuenta
        {
            get { return RequstString("nocuenta"); }
        }
        public static string fecha_emision
        {
            get { return RequstString("fechaemision"); }
        }
        public static string fecha_limite_pago
        {
            get { return RequstString("fechalimitepago"); }
        }      
        public static string periodo_facturacion
        {
            get { return RequstString("periodofacturacion"); }
        }
        public static string numero_region        
        {
            get { return RequstString("numeroregion"); }
        }
        public static string numero_sector 
        {
            get { return RequstString("numerosector"); }
        }
        public static string numero_reparto 
        {
            get { return RequstString("numeroreparto"); }
        }
        public static string usuario 
        {
            get { return RequstString("usuario"); }
        }
        public static string domicilio 
        {
            get { return RequstString("domicilio"); }
        }
        public static string colonia
        {
            get { return RequstString("colonia"); }
        }
        public static string tarifa
        {
            get { return RequstString("tarifa"); }
        }
        public static string giro
        {
            get { return RequstString("giro"); }
        }
        public static string medidor 
        {
            get { return RequstString("medidor"); }
        }
        public static string total_iva 
        {
            get { return RequstString("totaliva"); }
        }
        public static string rfc 
        {
            get { return RequstString("rfc"); }
        }
        public static string fecha_formato 
        {
            get { return RequstString("fechaformato"); }
        }
        public static string Nombre_Reporte
        {
            get { return RequstString("NombreReporte"); }
        }
        public static string tasa_iva
        {
            get { return  RequstString("tasa_iva"); }
        }
        public static string suma_importe_conceptos_cobros
        {
            get { return RequstString("suma_importe_conceptos_cobros"); }
        }
        public static string suma_iva_conceptos_cobros
        {
            get { return RequstString("suma_iva_conceptos_cobros"); }
        }
        public static string total_pagar
        {
            get { return RequstString("total_pagar"); }
        }
    }
        
}