﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace JAPAMI.Parametros.subir_archivo { 

public class Cls_parametros_subir_archivo
{
	public Cls_parametros_subir_archivo()
	{	}


    public static string RequstForm(string name)
    {

        return (HttpContext.Current.Request.Form[name] == null ? string.Empty : HttpContext.Current.Request.Form[name].ToString().Trim());

    }

    public static string RequstString(string sParam)
    {

        return (HttpContext.Current.Request[sParam] == null ? string.Empty : HttpContext.Current.Request[sParam].ToString().Trim());

    }

    public static int RequstInt(string sParam)
    {

        int iValue;

        string sValue = RequstString(sParam);

        int.TryParse(sValue, out iValue);

        return iValue;

    }

    public static string accion
    {
        get { return RequstString("accion"); }
    }

    public static string accion1()
    {
        return RequstString("accion");
    }

    public static string nombre_archivo {
        get { return RequstString("nombre_archivo"); }
    }
      
}

}