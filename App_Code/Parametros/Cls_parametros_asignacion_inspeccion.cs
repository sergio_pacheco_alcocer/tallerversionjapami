﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace JAPAMI.Parametros.Asignacion_Inspeccion {

    public class Cls_parametros_asignacion_inspeccion
    {
        public Cls_parametros_asignacion_inspeccion()
        { }

        public static string RequstForm(string name)
        {

            return (HttpContext.Current.Request.Form[name] == null ? string.Empty : HttpContext.Current.Request.Form[name].ToString().Trim());

        }

        public static string RequstString(string sParam)
        {

            return (HttpContext.Current.Request[sParam] == null ? string.Empty : HttpContext.Current.Request[sParam].ToString().Trim());

        }

        public static int RequstInt(string sParam)
        {

            int iValue;

            string sValue = RequstString(sParam);

            int.TryParse(sValue, out iValue);

            return iValue;

        }

        public static string accion
        {
            get { return RequstString("accion"); }
        }

        public static string obtener_letra
        {
            get { return RequstString("q");}
        }

        public static string datos_busqueda {
            get { return RequstString("datos_busqueda"); }
        }

        public static string estado_inspeccion {
            get { return RequstString("estado"); }
        }

    }

}



