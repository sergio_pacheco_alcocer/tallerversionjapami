﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;



namespace JAPAMI.Parametros.Operacion_Caja {


    public class Cls_parametros_operacion_caja
    {
        public Cls_parametros_operacion_caja()
        {  }


        public static string RequstForm(string name)
        {

            return (HttpContext.Current.Request.Form[name] == null ? string.Empty : HttpContext.Current.Request.Form[name].ToString().Trim());

        }

        public static string RequstString(string sParam)
        {

            return (HttpContext.Current.Request[sParam] == null ? string.Empty : HttpContext.Current.Request[sParam].ToString().Trim());

        }

        public static int RequstInt(string sParam)
        {

            int iValue;

            string sValue = RequstString(sParam);

            int.TryParse(sValue, out iValue);

            return iValue;

        }


        public static string accion
        {
            get { return RequstString("accion"); }
        }


        public static string empleado_id
        {
            get { return RequstString("empleado_id"); }
        }

        

        public static string operacion_caja_id {
            get { return RequstString("operacion_caja_id"); }
        }


    }


}

