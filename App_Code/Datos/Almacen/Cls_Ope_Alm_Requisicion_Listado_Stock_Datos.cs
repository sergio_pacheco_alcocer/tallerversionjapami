﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Generar_Req_Listado.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Generar_Requisicion.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;


/// <summary>
/// Summary description for Cls_Ope_Alm_Requisicion_Listado_Stock_Datos
/// </summary>
/// 

namespace JAPAMI.Generar_Req_Listado.Datos
{
    public class Cls_Ope_Alm_Requisicion_Listado_Stock_Datos
    {
        
        public static DataTable Consulta_Listado_Almacen(Cls_Ope_Alm_Requisicion_Listado_Stock_Negocio Clase_Negocio)
        {
            String Mi_SQL = "SELECT LISTADO." + Ope_Com_Listado.Campo_Folio;
            Mi_SQL += ", LISTADO." + Ope_Com_Listado.Campo_Listado_ID;
            Mi_SQL += ", REPLACE(CONVERT(VARCHAR(11),LISTADO." + Ope_Com_Listado.Campo_Fecha_Creo + ",106),' ','/') AS FECHA_CREO";
            Mi_SQL += ", LISTADO." + Ope_Com_Listado.Campo_Tipo;
            Mi_SQL += ", LISTADO." + Ope_Com_Listado.Campo_Estatus;
            Mi_SQL += ", LISTADO." + Ope_Com_Listado.Campo_Total;
            Mi_SQL += ", LISTADO." + Ope_Com_Listado.Campo_Comentarios;
            Mi_SQL += " FROM " + Ope_Com_Listado.Tabla_Ope_Com_Listado + " LISTADO";
            Mi_SQL += " WHERE " + Ope_Com_Listado.Campo_Estatus + "='AUTORIZADA'";

            if (Clase_Negocio.P_Listado_ID != null)
            {
                Mi_SQL = "SELECT * FROM " + Ope_Com_Listado.Tabla_Ope_Com_Listado + " WHERE " + Ope_Com_Listado.Campo_Listado_ID + "='" +Clase_Negocio.P_Listado_ID +"'";
            }
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        public static DataTable Consulta_Listado_Detalle(Cls_Ope_Alm_Requisicion_Listado_Stock_Negocio Clase_Negocio)
        {

            String Mi_SQL = "SELECT LISTADO." + Ope_Com_Listado_Detalle.Campo_No_Producto_ID + " AS PRODUCTO_ID";
            Mi_SQL += ", PRODUCTO." + Cat_Com_Productos.Campo_Clave;
            Mi_SQL += ", PRODUCTO." + Cat_Com_Productos.Campo_Nombre + " AS PRODUCTO_NOMBRE";
            Mi_SQL += ", PRODUCTO." + Cat_Com_Productos.Campo_Descripcion;
            Mi_SQL += ", PRODUCTO." + Cat_Com_Productos.Campo_Disponible;
            Mi_SQL += ", PRODUCTO." + Cat_Com_Productos.Campo_Reorden;
            Mi_SQL += ", PRODUCTO." + Cat_Com_Productos.Campo_Partida_ID;
            Mi_SQL += ", PRODUCTO." + Cat_Com_Productos.Campo_Costo + " AS PRECIO_UNITARIO ";
            Mi_SQL += ",(SELECT " + Cat_Sap_Concepto.Campo_Clave + " +' '+ " + Cat_Sap_Concepto.Campo_Descripcion + " FROM ";
            Mi_SQL += Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
            Mi_SQL += Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
            Mi_SQL += Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
            Mi_SQL += "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
            Mi_SQL += "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM ";
            Mi_SQL += Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = PRODUCTO.";
            Mi_SQL += Cat_Com_Productos.Campo_Partida_ID + "))) AS CONCEPTO";
            Mi_SQL += ",(SELECT " + Cat_Sap_Concepto.Campo_Concepto_ID + " FROM ";
            Mi_SQL += Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
            Mi_SQL += Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
            Mi_SQL += Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
            Mi_SQL += "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
            Mi_SQL += "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM ";
            Mi_SQL += Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = PRODUCTO.";
            Mi_SQL += Cat_Com_Productos.Campo_Partida_ID + "))) AS CONCEPTO_ID";
            Mi_SQL += ", LISTADO." + Ope_Com_Listado_Detalle.Campo_Cantidad;
            Mi_SQL += ", LISTADO." + Ope_Com_Listado_Detalle.Campo_Costo_Compra;
            Mi_SQL += ", LISTADO." + Ope_Com_Listado_Detalle.Campo_Importe;
            Mi_SQL += ", LISTADO." + Ope_Com_Listado_Detalle.Campo_Monto_IVA;
            Mi_SQL += ", LISTADO." + Ope_Com_Listado_Detalle.Campo_Monto_IEPS;
            Mi_SQL += ", LISTADO." + Ope_Com_Listado_Detalle.Campo_Porcentaje_IVA;
            Mi_SQL += ", LISTADO." + Ope_Com_Listado_Detalle.Campo_Porcentaje_IEPS;
            Mi_SQL += " FROM " + Ope_Com_Listado_Detalle.Tabla_Ope_Com_Listado_Detalle + " LISTADO";
            Mi_SQL += " JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTO";
            Mi_SQL += " ON PRODUCTO." + Cat_Com_Productos.Campo_Producto_ID + " = LISTADO." + Ope_Com_Listado_Detalle.Campo_No_Producto_ID;
            Mi_SQL += " WHERE LISTADO." + Ope_Com_Listado_Detalle.Campo_No_Listado_ID + " = '" +Clase_Negocio.P_Listado_ID + "'";
            Mi_SQL += " AND LISTADO." + Ope_Com_Listado_Detalle.Campo_Borrado + " IS NULL ";
            Mi_SQL += " AND LISTADO." + Ope_Com_Listado_Detalle.Campo_No_Requisicion + " IS NULL ";
            Mi_SQL += " ORDER BY PRODUCTO." + Cat_Com_Productos.Campo_Nombre;

            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }

        public static bool Borrar_Productos_Listado(Cls_Ope_Alm_Requisicion_Listado_Stock_Negocio Clase_Negocio)
        {
            String Mi_SQL = "";
            bool Operacion_Realizada= false;
            try
            {
                //Recorremos el listado para eliminar 
                for (int i = 0; i < Clase_Negocio.P_Dt_Productos.Rows.Count; i++)
                {
                    Mi_SQL = "UPDATE " + Ope_Com_Listado_Detalle.Tabla_Ope_Com_Listado_Detalle;
                    Mi_SQL += " SET " + Ope_Com_Listado_Detalle.Campo_Borrado + "='SI'";
                    Mi_SQL += ", " + Ope_Com_Listado_Detalle.Campo_Motivo_Borrado + "='" + Clase_Negocio.P_Motivo_Borrado + "'";
                    Mi_SQL += " WHERE " + Ope_Com_Listado_Detalle.Campo_No_Listado_ID;
                    Mi_SQL += "='" + Clase_Negocio.P_Listado_ID + "'";
                    Mi_SQL += " AND " + Ope_Com_Listado_Detalle.Campo_No_Producto_ID;
                    Mi_SQL += "='" + Clase_Negocio.P_Dt_Productos.Rows[i]["Producto_ID"].ToString().Trim() +"'";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                Operacion_Realizada = true;
            }
            catch
            {
                Operacion_Realizada = false;
            }

            return Operacion_Realizada;
        }

        public static String Convertir_Requisicion_Transitoria(Cls_Ope_Alm_Requisicion_Listado_Stock_Negocio Clase_Negocio)
        {
            //Declaracion de variables
            String Mensaje_Error = "";
            String Id_Requisicion = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            object aux; //Objeto auxiliar para las consultas escalares
            SqlDataAdapter Da; //variable para el adaptador
            DataTable Dt_Parametros = new DataTable(); //Tabla para los parametros del listado
            DataTable Dt_Autorizo = new DataTable(); //Tabla para consultar quien autorizo
            string Folio_Listado = string.Empty; //variable para el folio del listado

            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Da = new SqlDataAdapter(Cmd);
            int No_Reserva = 0; //variable para el numero de la reserva

            try
            {
                //Consultamos el id de la dependencia de Almacen que se encuentra en los parametros 
                String Mi_SQL = "SELECT * FROM " + Cat_Alm_Parametros_Listado.Tabla_Cat_Alm_Parametros_Listado;

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = Cmd;
                Da.Fill(Dt_Parametros);

                //Asignar los valores de los parametros de almacen
                //String Partida_Esp_Almacen_Global = Dt_Parametros.Rows[0][Cat_Alm_Parametros_Listado.Campo_Partida_Esp_Almacen_Global].ToString();
                String Programa_ID_Almacen = Dt_Parametros.Rows[0][Cat_Alm_Parametros_Listado.Campo_Proyecto_Programa_ID].ToString();
                String Dependencia_ID_Almacen = Dt_Parametros.Rows[0][Cat_Alm_Parametros_Listado.Campo_Unidad_Responsable_ID].ToString();
                String FF_ID_Almacen = Dt_Parametros.Rows[0][Cat_Alm_Parametros_Listado.Campo_Fte_Financiamiento_ID].ToString();    

                //Consultamos la Persona que autorizo el listado para asignarlo en la requisicion que se va crear
                Mi_SQL = "";
                Mi_SQL = "SELECT LIS." + Ope_Com_Listado.Campo_Empleado_Autorizacion_ID;
                Mi_SQL += ", (SELECT " + Cat_Empleados.Campo_Nombre;
                Mi_SQL += "+' '+" + Cat_Empleados.Campo_Apellido_Paterno;
                Mi_SQL += "+' '+" + Cat_Empleados.Campo_Apellido_Materno;
                Mi_SQL += " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
                Mi_SQL += " WHERE " + Cat_Empleados.Campo_Empleado_ID;
                Mi_SQL += " = LIS." + Ope_Com_Listado.Campo_Empleado_Autorizacion_ID;
                Mi_SQL += ") AS EMPLEADO_AUTORIZO";
                Mi_SQL += ", LIS." + Ope_Com_Listado.Campo_No_Partida_ID;
                Mi_SQL += ", LIS." + Ope_Com_Listado.Campo_Comentarios;
                Mi_SQL += " FROM " + Ope_Com_Listado.Tabla_Ope_Com_Listado + " LIS";
                Mi_SQL += " WHERE LIS." + Ope_Com_Listado.Campo_Listado_ID;
                Mi_SQL += "='" + Clase_Negocio.P_Listado_ID + "'";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = Cmd;
                Da.Fill(Dt_Autorizo);

                String Empleado_Autorizo = "";

                if (Dt_Autorizo.Rows.Count != 0)
                    Empleado_Autorizo = Dt_Autorizo.Rows[0][Ope_Com_Listado.Campo_Empleado_Autorizacion_ID].ToString().Trim();

                //Generamos el id de la requisiciion 
                Mi_SQL = "SELECT MAX(" + Ope_Com_Requisiciones.Campo_Requisicion_ID + ") FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " ";
                Cmd.CommandText = Mi_SQL;
                aux = Cmd.ExecuteScalar();

                //Verificar si no es nulo
                if (aux != null)
                {
                    if (aux != DBNull.Value)
                    {
                        Id_Requisicion = (Convert.ToInt32(aux) + 1).ToString().Trim();
                    }
                    else
                    {
                        Id_Requisicion = "1";
                    }
                }
                else
                {
                    Id_Requisicion = "1";
                }
                //Construimos el codigo Programatico
                Mi_SQL = "SELECT RTRIM(" + Cat_SAP_Fuente_Financiamiento.Campo_Clave +
                             ") +'-' +(SELECT RTRIM(" + Cat_SAP_Area_Funcional.Campo_Clave +
                             ") FROM " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional +
                             " WHERE " + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + "='00001') +'-'+" +
                             " (SELECT RTRIM(" + Cat_Sap_Proyectos_Programas.Campo_Clave +
                             ") FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas+ " WHERE " +
                             Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " ='" + Programa_ID_Almacen.Trim() + "') +'-'+" +
                             " (SELECT RTRIM(" + Cat_Dependencias.Campo_Clave +
                             ")  FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                             " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " ='" + Dependencia_ID_Almacen.Trim() + "') +'-'+" + 
                             " (SELECT RTRIM(" + Cat_Sap_Partidas_Especificas.Campo_Clave +
                             ") FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas +
                             " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "='" + 
                             Clase_Negocio.P_Dt_Productos.Rows[0]["Partida_ID"].ToString().Trim() + "')"+
                             " FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento +
                             " WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID +
                             "= '" + FF_ID_Almacen.Trim() + "'";

                Cmd.CommandText = Mi_SQL;
                object codigo_programatico = Cmd.ExecuteScalar();


                Mi_SQL = "INSERT INTO " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                    " (" + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Codigo_Programatico +
                    ", " + Ope_Com_Requisiciones.Campo_Folio +
                    ", " + Ope_Com_Requisiciones.Campo_Estatus +
                    ", " + Ope_Com_Requisiciones.Campo_Tipo +
                    ", " + Ope_Com_Requisiciones.Campo_Fase +
                    ", " + Ope_Com_Requisiciones.Campo_Usuario_Creo +
                    ", " + Ope_Com_Requisiciones.Campo_Fecha_Creo +
                    ", " + Ope_Com_Requisiciones.Campo_Empleado_Filtrado_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Fecha_Filtrado +
                    ", " + Ope_Com_Requisiciones.Campo_Tipo_Articulo +
                    ", " + Ope_Com_Requisiciones.Campo_Empleado_Construccion_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Fecha_Construccion +
                    ", " + Ope_Com_Requisiciones.Campo_Empleado_Generacion_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Fecha_Generacion +
                    ", " + Ope_Com_Requisiciones.Campo_Empleado_Autorizacion_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion +
                    ", " + Ope_Com_Requisiciones.Campo_Listado_Almacen +
                    ", " + Ope_Com_Requisiciones.Campo_Partida_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Justificacion_Compra;
                if (Clase_Negocio.P_Comentarios.ToString().Trim() != String.Empty)
                {
                    Mi_SQL = Mi_SQL +
                        ", " + Ope_Com_Requisiciones.Campo_Comentarios;
                }
                Mi_SQL = Mi_SQL +
                    ") VALUES ('" + Id_Requisicion + "','" + Dependencia_ID_Almacen  + "','" +
                    codigo_programatico.ToString().Trim() + "','" +
                    "RQ-" + Id_Requisicion + "','" +
                    "AUTORIZADA','" +
                    "TRANSITORIA','" +
                    "REQUISICION','" +
                    Cls_Sessiones.Nombre_Empleado + "',GETDATE()," +
                    "'" + Cls_Sessiones.Empleado_ID + "',GETDATE(),'PRODUCTO'," +
                    "'" + Cls_Sessiones.Empleado_ID + "',GETDATE()," +
                    "'" + Cls_Sessiones.Empleado_ID + "',GETDATE()," +
                    "'" + Empleado_Autorizo + "',GETDATE(),'SI','" +
                    Clase_Negocio.P_Dt_Productos.Rows[0]["Partida_ID"].ToString().Trim() +
                    "','" + Dt_Autorizo.Rows[0][Ope_Com_Listado.Campo_Comentarios].ToString().Trim() ;
                if (Clase_Negocio.P_Comentarios.ToString().Trim() != String.Empty)
                {
                    Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Comentarios.ToString().Trim();
                }
                    Mi_SQL  = Mi_SQL + "')";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                //Ahora asignamos el id de la requisicion al detalle de listado de almacen, esto para realizar la relacion en caso de ser necesario
                for (int i = 0; i < Clase_Negocio.P_Dt_Productos.Rows.Count; i++)
                {
                    Mi_SQL = "UPDATE " + Ope_Com_Listado_Detalle.Tabla_Ope_Com_Listado_Detalle;
                    Mi_SQL += " SET " + Ope_Com_Listado_Detalle.Campo_No_Requisicion;
                    Mi_SQL += " = '" + Id_Requisicion + "'";
                    Mi_SQL += " WHERE " + Ope_Com_Listado_Detalle.Campo_No_Producto_ID + "='";
                    Mi_SQL += Clase_Negocio.P_Dt_Productos.Rows[i]["Producto_ID"].ToString().Trim() + "'";
                    Mi_SQL += " AND " + Ope_Com_Listado_Detalle.Campo_No_Listado_ID + "='";
                    Mi_SQL += Clase_Negocio.P_Listado_ID.ToString().Trim() + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                //Ahora recorremos el data de los productos del listado y los pasamos a la requisicion

                //Calculamos el IEPS, IVA y Subtotal de la requisicion de acuerdo a los productos que le pertenecen a esta
                //Variable que almacena la suma de todos los valores del IVA que tiene cada producto del detalle
                double IVA_Acumulado = 0;
                //Variable que almacena la suma de todos los valores del IEPS que tiene cada producto del detalle
                double IEPS_Acumulado = 0;
                //Variable que almacena la suma del costo compra sin tomar en cuenta el aumento por impuestos
                double Subtotal = 0;
                double Total = 0;
                double Subtotal_Producto = 0;
                double IVA_Producto = 0;
                double IEPS_Producto = 0;
                if (Clase_Negocio.P_Dt_Productos.Rows.Count != 0)
                {
                    for (int i = 0; i < Clase_Negocio.P_Dt_Productos.Rows.Count; i++)
                    {
                        //obtenemos el Subtotal ya que el costo compra solo es el precio unitario sin la multiplicacion de la cantidad
                        Subtotal_Producto = double.Parse(Clase_Negocio.P_Dt_Productos.Rows[i]["Precio_Unitario"].ToString()) * double.Parse(Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Listado_Detalle.Campo_Cantidad].ToString());
                        IVA_Producto = double.Parse(Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Listado_Detalle.Campo_Monto_IVA].ToString());
                        IEPS_Producto = double.Parse(Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Listado_Detalle.Campo_Monto_IEPS].ToString());

                        Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID + "),0) FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                        Cmd.CommandText = Mi_SQL;
                        Object Obj = Cmd.ExecuteScalar();
                        String Ope_Com_Req_Producto_ID = (Convert.ToInt32(Obj) + 1).ToString();

                        Mi_SQL = "INSERT INTO ";
                        Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                        Mi_SQL += " (" + Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Requisicion_ID;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Prod_Serv_ID;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Partida_ID;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Cantidad;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Usuario_Creo;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Fecha_Creo;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Precio_Unitario;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Monto_IVA;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Monto_IEPS;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Porcentaje_IVA;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Porcentaje_IEPS;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Importe;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Monto_Total;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Tipo;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Clave;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Nombre_Giro;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Giro_ID;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID;
                        Mi_SQL += " ) VALUES ";
                        Mi_SQL += "('" + Ope_Com_Req_Producto_ID;
                        Mi_SQL += "','" + Id_Requisicion;
                        Mi_SQL += "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Producto_ID"].ToString().Trim();
                        Mi_SQL += "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Partida_ID"].ToString().Trim();
                        Mi_SQL += "','" + Programa_ID_Almacen.Trim();
                        Mi_SQL += "', " + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Listado_Detalle.Campo_Cantidad].ToString();
                        Mi_SQL += ",'" + Cls_Sessiones.Nombre_Empleado;
                        Mi_SQL += "',GETDATE()";
                        Mi_SQL += ",'" + Clase_Negocio.P_Dt_Productos.Rows[i]["Precio_Unitario"].ToString();
                        Mi_SQL += "','" + IVA_Producto;
                        Mi_SQL += "','" + IEPS_Producto;
                        Mi_SQL += "','" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Listado_Detalle.Campo_Porcentaje_IVA].ToString();
                        Mi_SQL += "','" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Listado_Detalle.Campo_Porcentaje_IEPS].ToString();
                        Mi_SQL += "','" + Subtotal_Producto;
                        Mi_SQL += "','" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Listado_Detalle.Campo_Importe].ToString();
                        Mi_SQL += "','PRODUCTO";
                        Mi_SQL += "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Clave"].ToString().Trim();
                        Mi_SQL += "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Concepto"].ToString().Trim();
                        Mi_SQL += "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Concepto_ID"].ToString().Trim();
                        Mi_SQL += "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Producto_Nombre"].ToString().Trim();
                        Mi_SQL += "' +';'+'" + Clase_Negocio.P_Dt_Productos.Rows[i]["Descripcion"].ToString().Trim() ;
                        Mi_SQL += "','" + FF_ID_Almacen.Trim() + "')";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        IVA_Acumulado = IVA_Acumulado + IVA_Producto;
                        IEPS_Acumulado = IEPS_Acumulado + IEPS_Producto;
                        //Como el Costo Compra es el precio unitario sinla cantidad se multiplica el presio unitario sin impuesto por la cantidad para obtener el Subtotal
                        Subtotal = Subtotal + Subtotal_Producto;
                        Total = Total + double.Parse(Clase_Negocio.P_Dt_Productos.Rows[i]["Importe"].ToString());
                        //REgresamos a valores ceros los acumulados
                        IVA_Producto = 0;
                        IEPS_Producto = 0;
                        Subtotal_Producto = 0;
                    }
                    //Actualizamos la requisicion con los nuevos valores de IVA, IEPs y subtotal 
                    Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                            " SET " + Ope_Com_Requisiciones.Campo_IVA + " ='" + IVA_Acumulado.ToString() +
                            "', " + Ope_Com_Requisiciones.Campo_IEPS + "='" + IEPS_Acumulado.ToString() +
                            "', " + Ope_Com_Requisiciones.Campo_Subtotal + "='" + Subtotal.ToString() +
                            "', " + Ope_Com_Requisiciones.Campo_Total + "='" + Total.ToString() +
                            "' WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + "='" + Id_Requisicion + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //REALIZAMOS EL INSERT EN LA TABLA DE HISTORIAL DE ESTATUS DE REQUISIONES 
                    Cls_Ope_Com_Requisiciones_Negocio Requisicion = new Cls_Ope_Com_Requisiciones_Negocio();
                    Requisicion.Registrar_Historial("EN CONSTRUCCION", Id_Requisicion);
                    Requisicion.Registrar_Historial("GENERADA", Id_Requisicion);
                    Requisicion.Registrar_Historial("AUTORIZADA", Id_Requisicion);

                    //Consultar el folio del listado
                    Mi_SQL = "SELECT " + Ope_Com_Listado.Campo_Folio + " FROM " + Ope_Com_Listado.Tabla_Ope_Com_Listado + " " +
                        "WHERE " + Ope_Com_Listado.Campo_Listado_ID + " = '" + Clase_Negocio.P_Listado_ID + "' ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    aux = Cmd.ExecuteScalar();

                    //Verificar si el listado no es nulo
                    if (aux != null)
                    {
                        if (aux != DBNull.Value)
                        {
                            //Colocar el folio del listado
                            Folio_Listado = aux.ToString().Trim();

                            //Consultar el numero de reserva de acuerdo al folio del listado
                            Mi_SQL = "SELECT " + Ope_Psp_Reservas.Campo_No_Reserva + " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " " +
                                "WHERE " + Ope_Psp_Reservas.Campo_Concepto + " = '" + Folio_Listado + "' ";

                            //Ejecutar consulta
                            Cmd.CommandText = Mi_SQL;
                            aux = Cmd.ExecuteScalar();

                            //Verificar si no es nulo
                            if (aux != null)
                            {
                                if (aux != DBNull.Value)
                                {
                                    No_Reserva = Convert.ToInt32(aux);
                                }
                            }
                        }
                    }

                    //Colocar el numero de la reserva (creada en el listado) en la requisicion
                    Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " SET " + Ope_Com_Requisiciones.Campo_Num_Reserva + " = " + No_Reserva.ToString() + " " +
                        "WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Id_Requisicion + " ";
                    
                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Ejecutar la transaccion
                    Trans.Commit();                  

                }//fin del if
            }
            catch(Exception EX)
            {
                EX.ToString();
                Trans.Rollback();
                Mensaje_Error = EX.Message;
                Id_Requisicion = "0";
            }
            finally
            {
                Cn.Close();
            }
            return Id_Requisicion;
        }//fin de Convertir_Requisicion_Transitoria


        public static DataTable Consultar_Requisiciones_Listado(Cls_Ope_Alm_Requisicion_Listado_Stock_Negocio Clase_Negocio)
        {
            //Consultamos las requisiciones listado 
            String Mi_SQL = "SELECT " + Ope_Com_Requisiciones.Campo_Folio;
            Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Total;
            Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
            Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL += " IN (SELECT " + Ope_Com_Listado_Detalle.Campo_No_Requisicion + " FROM ";
            Mi_SQL += Ope_Com_Listado_Detalle.Tabla_Ope_Com_Listado_Detalle + " WHERE ";
            Mi_SQL += Ope_Com_Listado_Detalle.Campo_No_Listado_ID + "='" + Clase_Negocio.P_Listado_ID.Trim()+ "'";
            Mi_SQL += " GROUP BY(" + Ope_Com_Listado_Detalle.Campo_No_Requisicion + "))";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }

        public static bool Modificar_Listado(Cls_Ope_Alm_Requisicion_Listado_Stock_Negocio Clase_Negocio)
        {
            bool Operacion_Realizada = false;
            String Mi_SQL = "";
            try
            {
                Mi_SQL = "UPDATE " + Ope_Com_Listado.Tabla_Ope_Com_Listado;
                Mi_SQL += " SET " + Ope_Com_Listado.Campo_Estatus;
                Mi_SQL += "='" + Clase_Negocio.P_Estatus +"'";
                Mi_SQL += " WHERE " + Ope_Com_Listado.Campo_Listado_ID;
                Mi_SQL += " ='" + Clase_Negocio.P_Listado_ID + "'";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                Operacion_Realizada = true;
            }
            catch
            {
                Operacion_Realizada = false;
            }


            return Operacion_Realizada;
        }

    }//fin del class
}//fin del namespace