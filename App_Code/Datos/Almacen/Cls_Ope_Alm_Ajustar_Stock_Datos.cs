﻿using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Ajustar_Stock.Negocio;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using System.Data.OleDb;
using System.Data.Common;
using System.Windows.Forms;
using System.Text;

/// <summary>
/// Summary description for Cls_Ope_Alm_Ajustar_Stock_Datos
/// </summary>
/// 
namespace JAPAMI.Ajustar_Stock.Datos
{
    public class Cls_Ope_Alm_Ajustar_Stock_Datos
    {
        public Cls_Ope_Alm_Ajustar_Stock_Datos()
        {
        }
    #region MÉTODOS

        public static DataTable Consultar_Productos(Cls_Ope_Alm_Ajustar_Stock_Negocio Negocio)
        {
            String Mi_SQL = " SELECT * FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                            " WHERE UPPER(" + Cat_Com_Productos.Campo_Nombre +
                            ") LIKE UPPER('%" + Negocio.P_Producto + "%') ";
                            
            if (!String.IsNullOrEmpty(Negocio.P_Clave)) 
            {
                Mi_SQL += " AND " + Cat_Com_Productos.Campo_Clave + " = '" + Negocio.P_Clave + "'";
            }
            DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            DataTable Dt_Table = null;
            if (_DataSet != null && _DataSet.Tables.Count > 0 && _DataSet.Tables[0].Rows.Count > 0)
            {
                Dt_Table = _DataSet.Tables[0];
            }
            return Dt_Table;
        }
        public static int Actualizar_Productos(Cls_Ope_Alm_Ajustar_Stock_Negocio Negocio)
        {
            try
            {
                String Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                                " SET " + Cat_Com_Productos.Campo_Existencia + " = " + Negocio.P_Existencia + "," +
                                Cat_Com_Productos.Campo_Disponible + " = " + Negocio.P_Disponible +
                                " WHERE " + Cat_Com_Productos.Campo_Clave + " = '" + Negocio.P_Clave + "'";
                int Rows = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                return Rows;
            }
            catch(Exception Ex)
            {
                Ex.ToString();
                return 0;
            }
        }
        public static DataTable Consultar_Ajustes_Inventario(Cls_Ope_Alm_Ajustar_Stock_Negocio Negocio)
        {
            DataTable Dt_Tabla = null;
            try
            {
                String Mi_SQL = "SELECT * FROM " + Ope_Alm_Ajustes_Inv_Stock.Tabla_Ope_Alm_Ajustes_Inv_Stock +
                               " WHERE " + 
                               " TO_DATE(TO_CHAR(" + Ope_Alm_Ajustes_Inv_Stock.Campo_Fecha_Creo + ",'DD-MM-YYYY'))" +
                               " >= '" + Negocio.P_Fecha_Inicial + "' AND " +
                               "TO_DATE(TO_CHAR(" + Ope_Alm_Ajustes_Inv_Stock.Campo_Fecha_Creo + ",'DD-MM-YYYY'))" +
                                            " <= '" + Negocio.P_Fecha_Final + "'";
                if (!String.IsNullOrEmpty(Negocio.P_No_Ajuste))
                {
                    Mi_SQL = "SELECT * FROM " + Ope_Alm_Ajustes_Inv_Stock.Tabla_Ope_Alm_Ajustes_Inv_Stock +
                                   " WHERE " + Ope_Alm_Ajustes_Inv_Stock.Campo_No_Ajuste + " = " + Negocio.P_No_Ajuste;
                }
                Dt_Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch(Exception Ex)
            {
                Ex.ToString();
                Dt_Tabla = null;
            }
            return Dt_Tabla;
        }

        public static int Guardar_Ajustes_Inventario(Cls_Ope_Alm_Ajustar_Stock_Negocio Negocio)
        {
            int Registros_Afectados = 0;
            int No_Ajuste = 0;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                No_Ajuste =
                    Obtener_Consecutivo(Ope_Alm_Ajustes_Inv_Stock.Campo_No_Ajuste,
                    Ope_Alm_Ajustes_Inv_Stock.Tabla_Ope_Alm_Ajustes_Inv_Stock);
                String Mi_SQL = "INSERT INTO " + Ope_Alm_Ajustes_Inv_Stock.Tabla_Ope_Alm_Ajustes_Inv_Stock +
                "(" + Ope_Alm_Ajustes_Inv_Stock.Campo_No_Ajuste + "," +
                Ope_Alm_Ajustes_Inv_Stock.Campo_Fecha_Hora + "," +
                Ope_Alm_Ajustes_Inv_Stock.Campo_Motivo_Ajuste_Coor + "," +
                Ope_Alm_Ajustes_Inv_Stock.Campo_Empleado_Elaboro_ID + "," +
                Ope_Alm_Ajustes_Inv_Stock.Campo_Fecha_Elaboro + "," +
                Ope_Alm_Ajustes_Inv_Stock.Campo_Estatus + "," +
                Ope_Alm_Ajustes_Inv_Stock.Campo_Usuario_Creo + "," +
                Ope_Alm_Ajustes_Inv_Stock.Campo_Fecha_Creo + ") VALUES (" +
                No_Ajuste + ",GETDATE(),'" + Negocio.P_Motivo_Ajuste_Coordinador + "','" +
                Cls_Sessiones.Empleado_ID + "',GETDATE(),'" + Negocio.P_Estatus + "','" +
                Cls_Sessiones.Nombre_Empleado + "',GETDATE())";
                Cmd.CommandText = Mi_SQL;
                Registros_Afectados = Cmd.ExecuteNonQuery();

                foreach (DataRow Producto in Negocio.P_Dt_Productos_Ajustados.Rows)
                {
                    Mi_SQL = "INSERT INTO " + Ope_Alm_Ajustes_Detalles.Tabla_Ope_Alm_Ajustes_Almacen +
                    "(" + Ope_Alm_Ajustes_Detalles.Campo_No_Ajuste + "," +
                    Ope_Alm_Ajustes_Detalles.Campo_Producto_ID + "," +
                    Ope_Alm_Ajustes_Detalles.Campo_Existencia_Sistema + "," +
                    Ope_Alm_Ajustes_Detalles.Campo_Conteo_Fisico + "," +
                    Ope_Alm_Ajustes_Detalles.Campo_Diferencia + "," +
                    Ope_Alm_Ajustes_Detalles.Campo_Tipo_Movimiento + "," +
                    Ope_Alm_Ajustes_Detalles.Campo_Importe_Diferencia + "," +
                    Ope_Alm_Ajustes_Detalles.Campo_Precio_Promedio + "," +
                    Ope_Alm_Ajustes_Detalles.Campo_Nombre_Descipcion + "," +
                    Ope_Alm_Ajustes_Detalles.Campo_Usuario_Creo + "," +
                    Ope_Alm_Ajustes_Detalles.Campo_Fecha_Creo + ") VALUES (" +
                    No_Ajuste + "," +
                    "'" + Producto[Ope_Alm_Ajustes_Detalles.Campo_Producto_ID].ToString() + "'," +
                    "" + Producto[Ope_Alm_Ajustes_Detalles.Campo_Existencia_Sistema].ToString() + "," +
                    "" + Producto[Ope_Alm_Ajustes_Detalles.Campo_Conteo_Fisico].ToString() + "," +
                    "" + Producto[Ope_Alm_Ajustes_Detalles.Campo_Diferencia].ToString() + "," +
                    "'" + Producto[Ope_Alm_Ajustes_Detalles.Campo_Tipo_Movimiento].ToString() + "'," +
                    "" + Producto[Ope_Alm_Ajustes_Detalles.Campo_Importe_Diferencia].ToString() + "," +
                    "" + Producto[Ope_Alm_Ajustes_Detalles.Campo_Precio_Promedio].ToString() + "," +
                    "'" + Producto[Ope_Alm_Ajustes_Detalles.Campo_Nombre_Descipcion].ToString() + "'," +
                    "'" + Cls_Sessiones.Nombre_Empleado + "',GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                Trans.Commit();
            }
            catch (Exception Ex)
            {
                Trans.Rollback();               
                No_Ajuste = 0;
                throw new Exception(Ex.ToString());
            }
            finally
            {
                Cn.Close();
            }
            return No_Ajuste;
        }


        public static int Aplicar_Ajuste_Inventario(Cls_Ope_Alm_Ajustar_Stock_Negocio Negocio)
        {
            int Registros_Afectados = 0;
            int No_Ajuste = 0;
            int Existencia = 0;
            int Disponible = 0;
            int Comprometido = 0;
            int Diferencia = 0;
            String Stock = "";
            DataTable Dt_Existencia = null;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                String Mi_SQL = "SELECT * FROM " + Ope_Alm_Ajustes_Detalles.Tabla_Ope_Alm_Ajustes_Almacen +
                " WHERE " + Ope_Alm_Ajustes_Detalles.Campo_No_Ajuste + " = " + Negocio.P_No_Ajuste;
                DataTable Dt_Productos_De_Ajuste = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                foreach (DataRow Producto in Dt_Productos_De_Ajuste.Rows)
                {
                    Mi_SQL = "SELECT " + Cat_Com_Productos.Campo_Existencia + "," +
                    Cat_Com_Productos.Campo_Comprometido + "," + Cat_Com_Productos.Campo_Disponible + "," + Cat_Com_Productos.Campo_Stock +
                    " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                    " WHERE " + Cat_Com_Productos.Campo_Producto_ID + " ='" + Producto[Cat_Com_Productos.Campo_Producto_ID].ToString().Trim() + "'";
                    Dt_Existencia = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                    Existencia = int.Parse(Dt_Existencia.Rows[0][Cat_Com_Productos.Campo_Existencia].ToString().Trim());
                    Disponible = int.Parse(Dt_Existencia.Rows[0][Cat_Com_Productos.Campo_Disponible].ToString().Trim());
                    Comprometido = int.Parse(Dt_Existencia.Rows[0][Cat_Com_Productos.Campo_Comprometido].ToString().Trim());
                    Stock = Dt_Existencia.Rows[0][Cat_Com_Productos.Campo_Stock].ToString().Trim();
                    Diferencia = int.Parse(Producto["DIFERENCIA"].ToString().Trim());
                    //Existencia = Producto["TIPO_MOVIMIENTO"].ToString().Trim() == "ENTRADA" ? Existencia + Diferencia : Existencia - Diferencia;
                    //Disponible = Producto["TIPO_MOVIMIENTO"].ToString().Trim() == "ENTRADA" ? Disponible + Diferencia : Disponible - Diferencia;
                    //hacer update
                    Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " SET ";
                    if (Producto["TIPO_MOVIMIENTO"].ToString().Trim() == "ENTRADA")
                    {                        
                        Mi_SQL += Cat_Com_Productos.Campo_Existencia + " = " + Cat_Com_Productos.Campo_Existencia + " + " + Diferencia;
                        if(Stock =="SI")
                            Mi_SQL += "," + Cat_Com_Productos.Campo_Disponible + " = " + Cat_Com_Productos.Campo_Disponible + " + " + Diferencia;
                    }
                    else if (Producto["TIPO_MOVIMIENTO"].ToString().Trim() == "SALIDA")
                    {
                        Mi_SQL += Cat_Com_Productos.Campo_Existencia + " = " + Cat_Com_Productos.Campo_Existencia + " - " + Diferencia ;
                        if (Stock == "SI")
                            Mi_SQL +="," + Cat_Com_Productos.Campo_Comprometido + " = " + Cat_Com_Productos.Campo_Comprometido + " - " + Diferencia;
                        //Mi_SQL += ", " + Cat_Com_Productos.Campo_Comprometido + " = " + (Comprometido - Diferencia);
                    }

                    Mi_SQL += " WHERE " + Cat_Com_Productos.Campo_Producto_ID + " = '";
                    Mi_SQL += Producto[Cat_Com_Productos.Campo_Producto_ID].ToString().Trim() + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                Trans.Commit();
                Registros_Afectados = 1;
            }
            catch (Exception Ex)
            {
                Trans.Rollback();
                Ex.ToString();
                Registros_Afectados = 0;
                throw new Exception(Ex.ToString());
            }
            finally
            {
                Cn.Close();
            }
            return Registros_Afectados;
        }

        public static DataTable Consultar_Productos_De_Ajuste(Cls_Ope_Alm_Ajustar_Stock_Negocio Negocio)
        {
            DataTable Dt_tabla = null;
            try
            {
                String Mi_SQL = "SELECT * FROM " + Ope_Alm_Ajustes_Detalles.Tabla_Ope_Alm_Ajustes_Almacen +
                " WHERE " + Ope_Alm_Ajustes_Detalles.Campo_No_Ajuste + " = " + Negocio.P_No_Ajuste;
                Dt_tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                Dt_tabla = null;
                throw new Exception(Ex.ToString());
            }
            return Dt_tabla;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),0) FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Leer_Excel
        ///DESCRIPCIÓN:          Metodo que consulta un archivo EXCEL
        ///PARAMETROS:           String sqlExcel.- string que contiene el select
        ///CREO:                 Susana Trigueros Armenta
        ///FECHA_CREO:           23/Mayo/2011 
        ///MODIFICO:             Salvador Hernández Ramírez
        ///FECHA_MODIFICO:       26/Mayo/2011 
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataSet Leer_Excel(Cls_Ope_Alm_Ajustar_Stock_Negocio Clase_Negocio)
        {
            //Para empezar definimos la conexión OleDb a nuestro fichero Excel.
            //String Rta = @MapPath("../../Archivos/PRESUPUESTO_IRAPUATO.xls");
            String Rta = Clase_Negocio.P_DirPath;
            string sConnectionString = "";// @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Rta + ";Extended Properties=Excel 8.0;";



            if (Rta.Contains(".xlsx"))       // Formar la cadena de conexion si el archivo es Exceml xml
            {
                sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                        "Data Source=" + Rta + ";" +
                        "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
            }
            else if (Rta.Contains(".xls"))   // Formar la cadena de conexion si el archivo es Exceml binario
            {
                sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                        "Data Source=" + Rta + ";" +
                        "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
                //sConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                //        "Data Source=" + Rta + ";" +
                //        "Extended Properties=Excel 8.0;";
            }

            //Definimos el DataSet donde insertaremos los datos que leemos del excel
            DataSet DS = new DataSet();

            //Definimos la conexión OleDb al fichero Excel y la abrimos
            OleDbConnection oledbConn = new OleDbConnection(sConnectionString);
            oledbConn.Open();

            //Creamos un comand para ejecutar la sentencia SELECT.
            OleDbCommand oledbCmd = new OleDbCommand(Clase_Negocio.P_SqlExcel, oledbConn);

            //Creamos un dataAdapter para leer los datos y asocialor al DataSet.
            OleDbDataAdapter da = new OleDbDataAdapter(oledbCmd);
            da.Fill(DS);
            return DS;
        }


        public static DataTable Consultar_Productos_Excel(Cls_Ope_Alm_Ajustar_Stock_Negocio Clase_Negocio)
        {
            String Claves_Productos ="";
            if(Clase_Negocio.P_Dt_Productos_Excel.Rows.Count > 0)
            {
                for(int i=0; i < Clase_Negocio.P_Dt_Productos_Excel.Rows.Count;i++)
                {
                    
                    if (Clase_Negocio.P_Dt_Productos_Excel.Rows[i][0].ToString().Trim() == String.Empty)
                    {
                        break;
                    }
                    else
                    {
                        if(i==0)
                            Claves_Productos = Claves_Productos +"'" + Clase_Negocio.P_Dt_Productos_Excel.Rows[i][0].ToString().Trim() +"'";
                        else
                            Claves_Productos = Claves_Productos +  ",'" + Clase_Negocio.P_Dt_Productos_Excel.Rows[i][0].ToString().Trim() + "'";
                    }
                }//fin del for
            }//fin del if

            StringBuilder Mi_SQL = new StringBuilder();
            Mi_SQL.Append("SELECT NULL AS NO_AJUSTE, " + Cat_Com_Productos.Campo_Producto_ID);
            Mi_SQL.Append(", " + Cat_Com_Productos.Campo_Existencia + " AS EXISTENCIA_SISTEMA");
            Mi_SQL.Append(", NULL AS CONTEO_FISICO ");
            Mi_SQL.Append(", NULL AS DIFERENCIA");
            Mi_SQL.Append(", '' AS TIPO_MOVIMIENTO");
            Mi_SQL.Append(", NULL AS IMPORTE_DIFERENCIA");
            Mi_SQL.Append(", " + Cat_Com_Productos.Campo_Costo + " AS PRECIO_PROMEDIO" );
            Mi_SQL.Append(", " + Cat_Com_Productos.Campo_Nombre + " +' '+" + Cat_Com_Productos.Campo_Descripcion + " AS NOMBRE_DESCRIPCION");
            Mi_SQL.Append(", NULL AS FECHA_CREO");
            Mi_SQL.Append(", NULL AS USUARIO_CREO");
            Mi_SQL.Append(", NULL AS FECHA_MODIFICO");
            Mi_SQL.Append(", NULL AS USUARIO_MODIFICO");
            Mi_SQL.Append(", " + Cat_Com_Productos.Campo_Clave);
            Mi_SQL.Append(" FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos);
            Mi_SQL.Append(" WHERE " + Cat_Com_Productos.Campo_Clave + " IN (" + Claves_Productos + ")");



            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
        }
    #endregion
        
    }
}
