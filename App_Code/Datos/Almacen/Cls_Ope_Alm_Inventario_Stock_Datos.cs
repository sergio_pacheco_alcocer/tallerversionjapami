﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Inventarios_De_Stock.Negocio;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;

/// <summary>
/// Summary description for Cls_Ope_Com_Cuadro_Comparativo_Datos
/// </summary>
/// 
namespace JAPAMI.Inventarios_De_Stock.Datos
{
    public class Cls_Ope_Alm_Inventario_Stock_Datos
    {
        #region METODOS 
        public Cls_Ope_Alm_Inventario_Stock_Datos()
        {

        }
        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consultar_Costeo_Inventario
        /// DESCRIPCION:            
        /// PARAMETROS :           
        /// CREO       :            Gustavo Angeles Cruz
        /// FECHA_CREO :            15/Julio/2011  
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static double Consultar_Costeo_Inventario(Cls_Ope_Alm_Inventarios_Stock_Negocio Negocio)
        {
            DataTable Dt_Tabla = null;
            double Acumulado = 0.0;
            String Mi_SQL = "SELECT SUM(EXISTENCIA*COSTO_PROMEDIO) ACUMULADO FROM CAT_COM_PRODUCTOS  WHERE STOCK = 'SI'";
            DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            if (_DataSet != null && _DataSet.Tables.Count > 0)
            {
                Dt_Tabla = _DataSet.Tables[0];
                try
                {
                    Acumulado = double.Parse(Dt_Tabla.Rows[0]["ACUMULADO"].ToString());                   
                }
                catch (Exception Ex) 
                {
                    Ex.ToString();
                    Acumulado = 0.0;
                }
            }
            return Acumulado;
        }
        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consultar_Inventario_Stock
        /// DESCRIPCION:            
        /// PARAMETROS :           
        /// CREO       :            Gustavo Angeles Cruz
        /// FECHA_CREO :            7/Julio/2011  
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consultar_Inventario_Stock(Cls_Ope_Alm_Inventarios_Stock_Negocio Negocio)
        {
            DataTable Dt_Tabla = null;
            String Mi_SQL = "SELECT CAT_COM_PRODUCTOS.*,(ISNULL(EXISTENCIA, 0)*ISNULL(COSTO_PROMEDIO, 0)) COSTO_PROMEDIO_T";
            Mi_SQL = Mi_SQL + ", (ISNULL(EXISTENCIA, 0)*ISNULL(COSTO, 0)) ULTIMO_COSTO";
            Mi_SQL = Mi_SQL + ", (COSTO_PROMEDIO) PROMEDIO";
            Mi_SQL = Mi_SQL + ", (COSTO) U_COSTO";
            Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Com_Unidades.Campo_Abreviatura + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID + "=CAT_COM_PRODUCTOS." + Cat_Com_Productos.Campo_Unidad_ID + ") UNIDAD";
            Mi_SQL = Mi_SQL +  "  FROM CAT_COM_PRODUCTOS WHERE ESTATUS='ACTIVO'";
            if (!string.IsNullOrEmpty(Negocio.P_Partida_ID))
            {
                Mi_SQL += " AND " + Cat_Com_Productos.Campo_Partida_Especifica_ID + " = '" + Negocio.P_Partida_ID + "'";
            }
            if (!string.IsNullOrEmpty(Negocio.P_Nombre_Producto))
            {
                Mi_SQL += " AND UPPER(" + Cat_Com_Productos.Campo_Nombre + ") LIKE UPPER('%" +
                Negocio.P_Nombre_Producto + "%') ";
            }
            if (!string.IsNullOrEmpty(Negocio.P_Clave_Producto))
            {
                Mi_SQL = Mi_SQL + " AND UPPER(CLAVE) = UPPER('" + Negocio.P_Clave_Producto + "')";
                
                //Mi_SQL = "SELECT CAT_COM_PRODUCTOS.*,(ISNULL(EXISTENCIA, 0)*ISNULL(COSTO_PROMEDIO, 0)) ACUMULADO" +
                //    ", (SELECT " + Cat_Com_Unidades.Campo_Abreviatura + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades +
                //    " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID + "=CAT_COM_PRODUCTOS." + Cat_Com_Productos.Campo_Unidad_ID + ") UNIDAD" +
                //    " FROM CAT_COM_PRODUCTOS WHERE ESTATUS='ACTIVO' AND UPPER(CLAVE) = UPPER('" + Negocio.P_Clave_Producto + "')";
            }
            if (!string.IsNullOrEmpty(Negocio.P_Clave_Anterior))
            {
                Mi_SQL = Mi_SQL + " AND UPPER(" + Cat_Com_Productos.Campo_REF_JAPAMI + ") = UPPER('" + Negocio.P_Clave_Anterior + "')";
                //Mi_SQL = "SELECT CAT_COM_PRODUCTOS.*,(ISNULL(EXISTENCIA, 0)*ISNULL(COSTO_PROMEDIO, 0)) ACUMULADO" +
                //    ", (SELECT " + Cat_Com_Unidades.Campo_Abreviatura + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades +
                //    " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID + "=CAT_COM_PRODUCTOS." + Cat_Com_Productos.Campo_Unidad_ID + ") UNIDAD" +
                //    " FROM CAT_COM_PRODUCTOS WHERE ESTATUS='ACTIVO' 
            }

            if (Negocio.P_Existencias_Cero == false)
            {
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Productos.Campo_Existencia + "!= 0";
            }

            if (!string.IsNullOrEmpty(Negocio.P_Almacen_General))
            {
                if (Negocio.P_Almacen_General == "SI")
                {
                    Mi_SQL = Mi_SQL + " AND CAT_COM_PRODUCTOS." + Cat_Com_Productos.Campo_Almacen_General + "='SI' ";
                }
                if (Negocio.P_Almacen_General == "NO")
                {
                    Mi_SQL = Mi_SQL + " AND (CAT_COM_PRODUCTOS." + Cat_Com_Productos.Campo_Almacen_General + "='NO' OR CAT_COM_PRODUCTOS." + Cat_Com_Productos.Campo_Almacen_General + " IS NULL) ";
                }
            }
            if (!String.IsNullOrEmpty(Negocio.P_Ordenar)) {
                if (Negocio.P_Ordenar.Equals("CLAVE_ANTERIOR")) Mi_SQL += " ORDER BY " + Cat_Com_Productos.Campo_REF_JAPAMI + " ASC";
                if (Negocio.P_Ordenar.Equals("CLAVE_SIAC")) Mi_SQL += " ORDER BY " + Cat_Com_Productos.Campo_Clave + " ASC";
                if (Negocio.P_Ordenar.Equals("NOMBRE")) Mi_SQL += " ORDER BY " + Cat_Com_Productos.Campo_Nombre + " ASC";
            } else {
                Mi_SQL += " ORDER BY " + Cat_Com_Productos.Campo_Nombre + " ASC";
            }
            
                //Negocio.P_No_Requisicion + " GROUP BY (PROVEEDOR_ID)";
            DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            if (_DataSet != null && _DataSet.Tables.Count > 0)
            {
                Dt_Tabla = _DataSet.Tables[0];
            }
            return Dt_Tabla;
        }
        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consultar_Precios_Cotizados
        /// DESCRIPCION:            
        /// PARAMETROS :           
        /// CREO       :            Gustavo Angeles Cruz
        /// FECHA_CREO :            6/Julio/2011  
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consultar_Partidas_Stock(Cls_Ope_Alm_Inventarios_Stock_Negocio Negocio)
        {

            DataTable Dt_Tabla = null;
            String Mi_SQL = "SELECT " + 
                Cat_Com_Partidas.Campo_Partida_ID + "," +
                Cat_Com_Partidas.Campo_Clave + " +' '+ " + Cat_Com_Partidas.Campo_Descripcion + " CLAVE_NOMBRE" +
                " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas + " WHERE " + Cat_Com_Partidas.Campo_Partida_ID +
                " IN (" +
                "SELECT DISTINCT(" + Cat_Com_Productos.Campo_Partida_ID + ")" +
                " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " WHERE " + Cat_Com_Productos.Campo_Stock + " = 'SI'" +                
                ")";
            DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);                                 
            if (_DataSet != null && _DataSet.Tables.Count > 0)
            {
                Dt_Tabla = _DataSet.Tables[0];
            }
            return Dt_Tabla;
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Modificar_Producto
        /// DESCRIPCION:            
        /// PARAMETROS :           
        /// CREO       :            Gustavo Angeles Cruz
        /// FECHA_CREO :            6/Julio/2011  
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static bool Modificar_Producto(Cls_Ope_Alm_Inventarios_Stock_Negocio Negocio)
        {
            int afectados = 0;
            try
            {
                DataTable Dt_Tabla = null;
                String Mi_SQL = "UPDATE " +
                Cat_Com_Productos.Tabla_Cat_Com_Productos +
                " SET " +
                Cat_Com_Productos.Campo_Minimo + " = " + Negocio.P_Minimo + "," +
                Cat_Com_Productos.Campo_Maximo + " = " + Negocio.P_Maximo + "," +
                Cat_Com_Productos.Campo_Reorden + " = " + Negocio.P_Reorden +
                " WHERE " + Cat_Com_Productos.Campo_Producto_ID + " ='" + Negocio.P_Producto_ID + "'";
                afectados =  SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (Exception Ex)
            {
                String Str = Ex.ToString();
                afectados = 0;
            }
            if (afectados > 0)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
        #endregion
    }        
}
