﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Requisiciones_Stock.Negocio;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Bitacora_Eventos;
using JAPAMI.Generar_Requisicion.Negocio;
using JAPAMI.Polizas.Negocios;
using System.Text;
using JAPAMI.Manejo_Presupuesto.Datos;

/// <summary>
/// Summary description for 
/// </summary>
/// 

namespace JAPAMI.Requisiciones_Stock.Datos
{
    public class Cls_Ope_Com_Alm_Requisiciones_Stock_Datos
    {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Requisiciones
        ///DESCRIPCIÓN:          Método utilizado para consultar las requisiciones de stock de almacén
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           22/junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static int Realizar_Traspaso_Presupuestal_Almacen(Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Datos)
        {
            StringBuilder MI_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion;//Variable para la conexión para la base de datos   
            SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            DataTable Dt_Mov = new DataTable();
            Int32 No_Movimiento_Egr;
            Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Comando = new SqlCommand();
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Transaction = Transaccion;
            Comando.Connection = Conexion;

            String Anio = String.Format("{0:yyyy}", DateTime.Now);
            //obtenemos el id maximo de la tabla de movimientos de egresos
            MI_SQL.Append("SELECT ISNULL(MAX(" + Ope_Psp_Movimiento_Egr_Det.Campo_Movimiento_ID + "), 0)");
            MI_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
            MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = 0");

            No_Movimiento_Egr = Convert.ToInt32(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, MI_SQL.ToString()));

            MI_SQL.Append("INSERT INTO " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "(");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Movimiento_ID + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Area_Funcional_ID + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Anio + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Enero + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Febrero + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Marzo + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Abril + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Mayo + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Junio + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Julio + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Agosto + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Septiembre + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Octubre + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Noviembre + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Diciembre + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Egreso + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Movimiento + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Usuario + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Justificacion + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Creo + ", ");
            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo + ") ");
            MI_SQL.Append(" VALUES(0,");
            MI_SQL.Append((No_Movimiento_Egr +1) + ", ");
            //MI_SQL.Append("'" + Dr["FF_ID"].ToString().Trim() + "', ");
            //MI_SQL.Append("'" + Dr["UR_ID"].ToString().Trim() + "', ");
            //MI_SQL.Append("'" + Dr["PROGRAMA_ID"].ToString().Trim() + "', ");
            //MI_SQL.Append("'" + Dr["PARTIDA_ID"].ToString().Trim() + "', ");
            //MI_SQL.Append("'" + Dr["AF_ID"].ToString().Trim() + "', ");
            //MI_SQL.Append(Anio + ", ");
            //MI_SQL.Append("'" + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "") + "', ");
            //MI_SQL.Append("'" + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "") + "', ");
            //MI_SQL.Append("'" + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "") + "', ");
            //MI_SQL.Append("'" + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "") + "', ");
            //MI_SQL.Append("'" + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "") + "', ");
            //MI_SQL.Append("'" + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "") + "', ");
            //MI_SQL.Append("'" + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "") + "', ");
            //MI_SQL.Append("'" + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "") + "', ");
            //MI_SQL.Append("'" + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
            //MI_SQL.Append("'" + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "") + "', ");
            //MI_SQL.Append("'" + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
            //MI_SQL.Append("'" + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
            //MI_SQL.Append("'" + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + "', ");
            //MI_SQL.Append("'" + Dr["ESTATUS"].ToString().Trim() + "', ");
            //MI_SQL.Append("'" + Dr["TIPO_OPERACION"].ToString().Trim() + "', ");
            //MI_SQL.Append("'" + Dr["TIPO_PARTIDA"].ToString().Trim() + "', ");
            //MI_SQL.Append("'" + Dr["TIPO_EGRESO"].ToString().Trim() + "', ");
            //MI_SQL.Append("'" + Dr["TIPO_MOVIMIENTO"].ToString().Trim() + "', ");
            //MI_SQL.Append("'ALMACEN', ");
            //MI_SQL.Append("'" + Dr["JUSTIFICACION"].ToString().Trim().ToUpper() + "', ");
            //MI_SQL.Append("'" + Datos.P_Usuario_Creo.Trim() + "', ");
            MI_SQL.Append("GETDATE()) ");

            Comando.CommandText = MI_SQL.ToString();
            Comando.ExecuteNonQuery();

            return 0;
        }

         ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Existencia_Producto
        ///DESCRIPCIÓN:          Consultar el detalle de un producto
        ///PARAMETROS:   
        ///CREO:                 Susana Trigueros Armenta
        ///FECHA_CREO:           20/Sep/2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Existencia_Producto(Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Datos)
        {
            String Mi_SQL = "SELECT * FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Productos.Campo_Clave + "=" + Datos.P_Clave_Producto.Trim();
            DataTable Dt_Productos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Productos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Requisiciones
        ///DESCRIPCIÓN:          Método utilizado para consultar las requisiciones de stock de almacén
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           22/junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Requisiciones(Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Datos)
        {
            // Declaración de variables
            String Mi_SQL = null;  
            DataTable Dt_Requisiciones = new DataTable();

            Mi_SQL = "SELECT " + "REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + ""; //NO_REQUISICION
            Mi_SQL += ", DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " as UNIDAD_RESPONSABLE";
            Mi_SQL += ", DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + " as UNIDAD_RESPONSABLE_ID";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Usuario_Creo + "";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio + "";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Estatus + "";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + "";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Total + "";
            Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICIONES ";
            Mi_SQL += " JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
            Mi_SQL += " ON REQUISICIONES." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
            Mi_SQL += " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID;
            Mi_SQL += " JOIN " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQUISICION_PRODUCTO ";
            Mi_SQL += " ON REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = REQUISICION_PRODUCTO." + Ope_Com_Req_Producto.Campo_Requisicion_ID;
            Mi_SQL += " JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS ";
            Mi_SQL += " ON REQUISICION_PRODUCTO." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID; 
            Mi_SQL += " WHERE  REQUISICIONES." + Ope_Com_Requisiciones.Campo_Tipo + "= '" + "STOCK'";
            Mi_SQL += " and  ( REQUISICIONES." + Ope_Com_Requisiciones.Campo_Estatus + "= '" + "ALMACEN'"; // Nota en esta parte debe ir Surtida
            Mi_SQL += " or  REQUISICIONES." + Ope_Com_Requisiciones.Campo_Estatus + "= '" + "PARCIAL')"; // Nota en esta parte debe ir Surtida

            if (Datos.P_No_Requisicion != null)
            {
                Mi_SQL += "AND REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " like '%" + Datos.P_No_Requisicion + "%'";
            }

            if (Datos.P_Dependencia_ID != null)
            {
                Mi_SQL += " and  REQUISICIONES." + Ope_Com_Requisiciones.Campo_Dependencia_ID + "= '" + Datos.P_Dependencia_ID + "'";
            }

            if (Datos.P_Comentarios != null)
            {
                Mi_SQL += " and  REQUISICIONES." + Ope_Com_Requisiciones.Campo_Usuario_Creo + "= '" + Datos.P_Comentarios + "'";
            }

            if ((Datos.P_Fecha_Inicial != null) && (Datos.P_Fecha_Final != null))
            {
                Mi_SQL += " AND " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " BETWEEN '" + Datos.P_Fecha_Inicial + "'" +
                " AND '" + Datos.P_Fecha_Final + "'";
            }
            if (!String.IsNullOrEmpty(Datos.P_Almacen_General)) 
            {
                Mi_SQL += " AND PRODUCTOS." + Cat_Com_Productos.Campo_Almacen_General + " = 'SI' ";
            }
            else
            {
                Mi_SQL += " AND (PRODUCTOS." + Cat_Com_Productos.Campo_Almacen_General + " = 'NO' OR PRODUCTOS." ;
                Mi_SQL +=  Cat_Com_Productos.Campo_Almacen_General + " IS NULL ) ";
            }
            Mi_SQL += " GROUP BY ";
            Mi_SQL += "REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + ""; //NO_REQUISICION
            Mi_SQL += ", DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre;
            Mi_SQL += ", DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID;
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Usuario_Creo + "";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio + "";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Estatus + "";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + "";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Total + "";

            Mi_SQL += " order by REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID;

            Dt_Requisiciones = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Requisiciones;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Detalles_Requisiciones
        ///DESCRIPCIÓN:          Método utilizado para consultar los detalles de la requisicion
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           22/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Detalles_Requisicion(Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Datos)
        {
            // Declaración de variables
            String Mi_SQL = null;
            DataTable Dt_Requisicion = new DataTable();

            //Mi_SQL = "SELECT " + "REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + ""; //NO_REQUISICION
            //Mi_SQL += ", DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " as UNIDAD_RESPONSABLE";
            //Mi_SQL += ", DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + " as UNIDAD_RESPONSABLE_ID";
            //Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio + "";
            //Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + "";
            //Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Justificacion_Compra + " as COMENTARIOS";
            //Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_IVA + "";
            //Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Subtotal+ "";
            //Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Total + "";
            //Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICIONES ";
            //Mi_SQL += " JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
            //Mi_SQL += " ON REQUISICIONES." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
            //Mi_SQL += " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID;
            //Mi_SQL += " WHERE  REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + "= '" +  Datos.P_No_Requisicion+ "'";

            Mi_SQL = "SELECT " + "REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + ""; //NO_REQUISICION
            Mi_SQL += ", DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " as UNIDAD_RESPONSABLE";
            Mi_SQL += ", DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + " as UNIDAD_RESPONSABLE_ID";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio + "";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + "";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Justificacion_Compra + " as COMENTARIOS";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_IVA + " as MONTO_IVA";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Subtotal + " as SUBTOTAL";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Total + " AS MONTO_TOTAL";
            Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICIONES ";
            Mi_SQL += " JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
            Mi_SQL += " ON REQUISICIONES." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
            Mi_SQL += " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID;
            Mi_SQL += " WHERE  REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + "= '" + Datos.P_No_Requisicion + "'";




            Dt_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Requisicion;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Productos_Requisicion
        ///DESCRIPCIÓN:          Método utilizado el programa y la fuente de financiomiento
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           23/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Pragrama_Financiamiento(Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Datos)
        {
            DataTable Dt_Consulta = new DataTable();
            String Mi_SQL = null;

            Mi_SQL = " SELECT DISTINCT" + " PROYECTOS_PROGRAMAS." + Cat_Com_Proyectos_Programas.Campo_Descripcion + " as PROYECTO_PROGRAMA ";
            Mi_SQL += ", PROYECTOS_PROGRAMAS." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + " ";
            Mi_SQL += ", FINANCIAMIENTO." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion+ " as FINANCIAMIENTO ";
            Mi_SQL += " FROM " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + " PROYECTOS_PROGRAMAS ";
            Mi_SQL += ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PRODUCTO ";
            Mi_SQL += ", " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FINANCIAMIENTO ";
            Mi_SQL += " WHERE REQ_PRODUCTO. " + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID + " = ";
            Mi_SQL += " PROYECTOS_PROGRAMAS. " + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + " ";
            Mi_SQL += " AND REQ_PRODUCTO. " + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID + " = ";
            Mi_SQL += " FINANCIAMIENTO. " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " ";
            Mi_SQL += " AND REQ_PRODUCTO. " + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = ";
            Mi_SQL += " '" + Datos.P_No_Requisicion + "' ";

            Dt_Consulta = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Consulta;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Productos_Requisicion
        ///DESCRIPCIÓN:          Método utilizado para consultar los productos de 
        ///                      las requisiciones de stock de almacén
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           22/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Productos_Requisicion(Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Datos)
        {
            // Declaración de variables
            String Mi_SQL = null;
            DataSet Ds_Productos_Requisicion = null;
            DataTable Dt_Productos_Requisicion = new DataTable();

            Mi_SQL = "SELECT " + " REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " as PRODUCTO_ID";
            Mi_SQL += ", PRODUCTOS." + Cat_Com_Productos.Campo_Clave + "";
            Mi_SQL += ", PRODUCTOS." + Cat_Com_Productos.Campo_Nombre + " as NOMBRE_PRODUCTO";
            Mi_SQL += ", PRODUCTOS." + Cat_Com_Productos.Campo_Descripcion + " as DESCRIPCION";
            Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Cantidad + " as CANTIDAD_SOLICITADA";
            Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Cantidad_Entregada + " ";
            Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Precio_Unitario + " as PRECIO";
            Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Porcentaje_IVA;
            Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Importe + " as SUBTOTAL";
            Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Monto_IVA + "";
            Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Monto_Total + " ";
            Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Partida_ID + "";
            Mi_SQL += ", ( SELECT " + Cat_Com_Unidades.Campo_Abreviatura + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " WHERE ";
            Mi_SQL += Cat_Com_Unidades.Campo_Unidad_ID + " = ( SELECT " + Cat_Com_Unidades.Campo_Unidad_ID + "  FROM ";
            Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " WHERE " + Cat_Com_Productos.Campo_Producto_ID + " = ";
            Mi_SQL += Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " )) AS UNIDAD ";
            Mi_SQL += " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PRODUCTO";
            Mi_SQL += " JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS";
            Mi_SQL += " ON REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID;
            Mi_SQL += " = PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID;

            Mi_SQL += " WHERE  REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Requisicion_ID + "= '" + Datos.P_No_Requisicion + "'";


            Dt_Productos_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Productos_Requisicion;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_DataTable
        ///DESCRIPCIÓN:          Método utilizado para consultar loas dependencias y las áreas
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           22/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_DataTable(Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Datos)
        {
            // Declaración de Variables
            String Mi_SQL = null;
            DataSet Ds_Consulta = null;
            DataTable Dt_consulta = new DataTable();

            try
            {
                if (Datos.P_Tipo_Data_Table.Equals("DEPENDENCIAS"))
                {
                    Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + " AS DEPENDENCIA_ID, " + Cat_Dependencias.Campo_Nombre + " AS NOMBRE";
                    Mi_SQL += " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
                    Mi_SQL += " ORDER BY " + Cat_Dependencias.Campo_Nombre;
                }
                else if (Datos.P_Tipo_Data_Table.Equals("EMPLEADOS_UR"))
                {
                    Mi_SQL += " SELECT " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + "  +' '+";
                    Mi_SQL += Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + "  +' '+";
                    Mi_SQL += Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + "  as EMPLEADO, ";
                    Mi_SQL += Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " as EMPLEADO_ID";
                    Mi_SQL += " FROM " + Cat_Empleados.Tabla_Cat_Empleados + ", " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                    Mi_SQL += " WHERE " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Dependencia_ID + "=";
                    Mi_SQL += "" + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
                    Mi_SQL += " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = ";
                    Mi_SQL += "'" + Datos.P_No_Requisicion + "'";
                    Mi_SQL += " ORDER BY " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre;
                }
                else if (Datos.P_Tipo_Data_Table.Equals("EMPLEADOS"))
                {
                    Mi_SQL += " SELECT " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + "  +' '+";
                    Mi_SQL += Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + "  +' '+";
                    Mi_SQL += Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + "  as EMPLEADO, ";
                    Mi_SQL += Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " as EMPLEADO_ID";
                    Mi_SQL += " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
                    Mi_SQL += " WHERE " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_No_Empleado;
                    Mi_SQL += " = '" + Datos.P_No_Empleado + "'";
                    Mi_SQL += " ORDER BY " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre;
                }

                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Consulta = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Consulta != null)
                {
                    Dt_consulta= Ds_Consulta.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_consulta;
        }



        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Alta_orden_Salida
        /// DESCRIPCION:            Dar de alta la orden de salida de material
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos para al operacion
        /// CREO       :            Salvador Hernández Ramírez
        /// FECHA_CREO :            23/Junio/2010 

        ///*******************************************************************************/
        public static long Alta_Orden_Salida(Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Datos)
        {
            // Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            Object Aux; // Variable auxiliar para las consultas
            String Mensaje = String.Empty; //Variable para el mensaje de error
            DataTable Dt_Aux = new DataTable(); //Tabla auxiliar para las consultas
            SqlDataAdapter Obj_Adaptador; //Adapatador para el llenado de las tablas

            Double Monto_Comprometido = 0.0; // Variable para el monto comprometido
            Double Monto_Ejercido = 0.0;    // Variable para el monto ejercido

            String No_Asignacion = String.Empty; // Variable para el No de Asignacion            
            String Partida_ID = String.Empty; // Variable para el ID de la partida
            String Proyecto_Programa_ID = String.Empty; // Variable para el ID del programa o proyecto
            String Dependencia_ID = String.Empty; // Variable para el ID de la dependencia
            Double Monto_Total = 0.0; // Variable para el monto total de los detalles de la requisicion

            // Variables utilizadas para actualizar los productos
            Double Cantidad_Comprometida = 0; // Variable para la cantidad Comprometida
            Double Cantidad_Existente = 0; // Variable para la cantidad Existente
            String Tipo_Salida_ID = "";

            Double SubTotal_Prod_Req = 0.0;
            Double IVA_Prod_Req = 0.0;
            Double Total_Prod_Req = 0.0;
            Double Cantidad_Entregada = 0;
            Double Cantidad_A_Entregar = 0;
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Comando = new SqlCommand();
            Obj_Adaptador = new SqlDataAdapter();
            Obj_Conexion.Open();
            Obj_Transaccion = Obj_Conexion.BeginTransaction();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;

            try
            {               

                //Asignar consulta para el Maximo ID
                Mi_SQL = "SELECT ISNULL(MAX(" + Alm_Com_Salidas.Campo_No_Salida + "), 0) FROM " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Aux = Obj_Comando.ExecuteScalar();

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                    Datos.P_No_Orden_Salida = Convert.ToInt64(Aux) + 1;
                else
                    Datos.P_No_Orden_Salida = 1;

                // Consulta para los ID de la dependencia, area, etc
                Mi_SQL = "SELECT " + Ope_Com_Requisiciones.Campo_Dependencia_ID + ", " + Ope_Com_Requisiciones.Campo_Area_ID + " ";
                Mi_SQL += "FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " ";
                Mi_SQL += "WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Datos.P_No_Requisicion.ToString().Trim() + " ";

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Adaptador.SelectCommand = Obj_Comando;
                Obj_Adaptador.Fill(Dt_Aux);

                //Verificar si la consulta arrojo resultado
                if (Dt_Aux.Rows.Count > 0)
                {
                    Datos.P_Dependencia_ID = Dt_Aux.Rows[0][0].ToString().Trim(); // Colocar los valores en las variables
                    Datos.P_Area_ID = Dt_Aux.Rows[0][1].ToString().Trim();
                }
                else
                {
                    throw new Exception("Datos no encontrados requisicion No. " + Datos.P_No_Requisicion.ToString().Trim());
                }

                // For utilizado para calcular los montos de la requisición
                foreach (DataRow Dr_Producto in  Datos.P_Dt_Productos_Requisicion.Rows)
                {
                    SubTotal_Prod_Req += Convert.ToDouble(Dr_Producto["SUBTOTAL"]);
                    IVA_Prod_Req += Convert.ToDouble(Dr_Producto["MONTO_IVA"]);
                    Total_Prod_Req += Convert.ToDouble(Dr_Producto["TOTAL"]);
                }


                // Consulta para dar de alta la salida
                Mi_SQL = "INSERT INTO " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + " (" + Alm_Com_Salidas.Campo_No_Salida + ", ";
                Mi_SQL += Alm_Com_Salidas.Campo_Dependencia_ID + ", ";
                Mi_SQL += Alm_Com_Salidas.Campo_Empleado_Solicito_ID + ", " + Alm_Com_Salidas.Campo_Requisicion_ID + ", ";
                Mi_SQL += Alm_Com_Salidas.Campo_Usuario_Creo + ", ";
                Mi_SQL += Alm_Com_Salidas.Campo_Fecha_Creo + ", " + Alm_Com_Salidas.Campo_Empleado_Almacen_ID + ", ";
                Mi_SQL += Alm_Com_Salidas.Campo_Subtotal + " , " + Alm_Com_Salidas.Campo_IVA + ", " + Alm_Com_Salidas.Campo_Total + ", OBSERVACIONES) ";
                Mi_SQL += " VALUES(" + Datos.P_No_Orden_Salida + ", ";
                Mi_SQL += "'" + Datos.P_Dependencia_ID + "', '" + Datos.P_Empleado_Recibio_ID + "', ";
                Mi_SQL += Datos.P_No_Requisicion.ToString().Trim() + ", ";
                //Mi_SQL += "'" + Datos.P_Nombre_Empleado_Almacen + "', GETDATE(), '" + Datos.P_Empleado_Almacen_ID + "', ";
                Mi_SQL += "'" + Cls_Sessiones.Nombre_Empleado + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID.Trim() + "', ";
                Mi_SQL += SubTotal_Prod_Req + ", " + IVA_Prod_Req + ", " + Total_Prod_Req + ", '" + Datos.P_Observaciones + "')";
                
                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                String Alta_poliza = Alta_Poliza(Datos.P_No_Orden_Salida.ToString(), Datos.P_No_Requisicion.ToString().Trim(), Total_Prod_Req, Obj_Comando);
                if (Alta_poliza != "Alta_Exitosa")
                {
                    throw new Exception(Alta_poliza);
                }

                // Consulta para la actualizacion del estatus de la requisicion 
                Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " ";
                Mi_SQL += "SET " + Ope_Com_Requisiciones.Campo_Estatus + " = '" + Datos.P_Estatus.ToString().Trim() + "', ";
                Mi_SQL += Ope_Com_Requisiciones.Campo_Fecha_Surtido + " = GETDATE(), ";
                //Mi_SQL += Ope_Com_Requisiciones.Campo_Empleado_Surtido_ID + " = '" + Datos.P_Empleado_Almacen_ID + "' ";
                Mi_SQL += Ope_Com_Requisiciones.Campo_Empleado_Surtido_ID + " = '" + Cls_Sessiones.Empleado_ID.Trim() + "' ";
                Mi_SQL += "WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Datos.P_No_Requisicion.ToString().Trim() + " ";

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();


                // Se Guarda el Historial de la requisición
                Cls_Ope_Com_Requisiciones_Negocio Requisiciones = new Cls_Ope_Com_Requisiciones_Negocio();
                Requisiciones.Registrar_Historial(Datos.P_Estatus.ToString().Trim(), Datos.P_No_Requisicion.ToString().Trim(), Obj_Comando);
                //Se traspasa el dinero a la cuenta contable de almacen 
                Mensaje = Cls_Ope_Psp_Manejo_Presupuesto.Traspaso_Consumo_Stock_Almacen(Total_Prod_Req, Datos.P_No_Requisicion, Obj_Comando);
                

                // Verificar si tiene datos la tabla enviada con las cantidades entregadas
                if (Datos.P_Dt_Productos_Requisicion.Rows.Count > 0)
                {
                    // Ciclo para el desplazamiento de la tabla
                    for (int Cont_Elementos = 0; Cont_Elementos < Datos.P_Dt_Productos_Requisicion.Rows.Count; Cont_Elementos++)
                    {
                        if (Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["PRECIO"].ToString().Trim() == null || Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["PRECIO"].ToString().Trim() == "") // Se realiza esta validación por que luego el precio es 0 para que no marque error
                            Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["PRECIO"] = 0;

                        //Consulta para dar de alta los detalles de la salida
                        Mi_SQL = "INSERT INTO " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + " (" + Alm_Com_Salidas_Detalles.Campo_No_Salida + ", ";
                        Mi_SQL += Alm_Com_Salidas_Detalles.Campo_Producto_ID + ", " + Alm_Com_Salidas_Detalles.Campo_Cantidad + ", ";
                        Mi_SQL += Alm_Com_Salidas_Detalles.Campo_Costo + ", " + Alm_Com_Salidas_Detalles.Campo_Costo_Promedio + ", ";
                        Mi_SQL += Alm_Com_Salidas_Detalles.Campo_Subtotal + ", " + Alm_Com_Salidas_Detalles.Campo_IVA + ", ";
                        Mi_SQL += Alm_Com_Salidas_Detalles.Campo_Importe + ") VALUES(" + Datos.P_No_Orden_Salida + ", ";
                        Mi_SQL += "'" + Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["PRODUCTO_ID"].ToString().Trim() + "', ";
                        Mi_SQL += Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["CANTIDAD_A_ENTREGAR"].ToString().Trim() + ", ";
                        Mi_SQL += Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["PRECIO"].ToString().Trim() + ", ";
                        Mi_SQL += Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["PRECIO"].ToString().Trim() + ", ";
                        Mi_SQL += Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["SUBTOTAL"].ToString().Trim() + ", ";
                        Mi_SQL += Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["MONTO_IVA"].ToString().Trim() + ", ";
                        Mi_SQL += Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["TOTAL"].ToString().Trim() + ")";

                        //Ejecutar consulta
                        Obj_Comando.CommandText = Mi_SQL;
                        Obj_Comando.ExecuteNonQuery();

                        // SE ACTUALIZA LA CANTIDAD ENTREGADA DE PRODUCTOS EN LA TABLA REQ_PRODUCTOS
                        Double.TryParse(Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["CANTIDAD_ENTREGADA"].ToString().Trim(), out Cantidad_Entregada);
                        Double.TryParse(Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["CANTIDAD_A_ENTREGAR"].ToString().Trim(), out Cantidad_A_Entregar);

                        Cantidad_Entregada = Cantidad_Entregada + Cantidad_A_Entregar; // Se suman las cantidades, lo que se va a entregar, con lo que se entrego

                        // Consulta para la actualizacion del estatus de la requisicion 
                        Mi_SQL = "UPDATE " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " ";
                        Mi_SQL += "SET " + Ope_Com_Req_Producto.Campo_Cantidad_Entregada + " = " + Cantidad_Entregada + " ";
                        Mi_SQL += " WHERE " + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = '" + Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["PRODUCTO_ID"].ToString().Trim() + "' ";
                        Mi_SQL += " AND " + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + Datos.P_No_Requisicion.ToString().Trim() + " ";
                       
                        //Ejecutar consulta
                        Obj_Comando.CommandText = Mi_SQL;
                        Obj_Comando.ExecuteNonQuery();


                        // SE ACTUALIZAN LOS MONTOS 
                        // Asignar el ID de la partida, a la dependencia y el ID del proyecto o programa
                        Partida_ID = Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["PARTIDA_ID"].ToString().Trim();
                        Dependencia_ID = Datos.P_Dependencia_ID.ToString().Trim();
                        Proyecto_Programa_ID = Datos.P_Proyecto_Programa_ID.ToString().Trim();

                        // Verificar si no es nulo
                        if (Convert.IsDBNull(Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["TOTAL"]) == false)
                            Monto_Total = Convert.ToDouble(Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["TOTAL"]); // Se asigna el monto total de cada producto de la requisición
                        else
                            Monto_Total = 0;

                        // Consulta para obtener el mayor numero de asignación
                        Mi_SQL = "SELECT  MAX (" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ") ";
                        Mi_SQL += " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                        Mi_SQL += " WHERE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID;
                        Mi_SQL += " = '" + Dependencia_ID + "'";
                        Mi_SQL += " and " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Partida_ID;
                        Mi_SQL += " = '" + Partida_ID + "'";
                        Mi_SQL += " and " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID;
                        Mi_SQL += " = '" + Proyecto_Programa_ID + "'";
                        Mi_SQL += " and " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto;
                        Mi_SQL += " = YEAR(GETDATE())";

                        // Ejecutar consulta
                        Obj_Comando.CommandText = Mi_SQL;
                        Aux = Obj_Comando.ExecuteScalar();                     

                        // SE DISMINUYEN LOS PRODUCTOS
                        //Consulta para el campo comprometido
                        Mi_SQL = "SELECT " + Cat_Com_Productos.Campo_Comprometido + ", " + Cat_Com_Productos.Campo_Existencia + " ";
                        Mi_SQL += "FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " ";
                        Mi_SQL += "WHERE " + Cat_Com_Productos.Campo_Producto_ID + " = '" + 
                            Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["PRODUCTO_ID"].ToString().Trim() + "'";
                        //Ejecutar consulta
                        Dt_Aux = new DataTable();
                        Obj_Comando.CommandText = Mi_SQL;
                        Obj_Adaptador.SelectCommand = Obj_Comando;
                        Obj_Adaptador.Fill(Dt_Aux);

                        //Verificar si la consulta arrojo resultado
                        if (Dt_Aux.Rows.Count > 0)
                        {
                            //Asignar los valores de los montos
                            if (Convert.IsDBNull(Dt_Aux.Rows[0][0]) == false)
                                Cantidad_Comprometida = Convert.ToInt32(Dt_Aux.Rows[0][0]);
                            else
                                Cantidad_Comprometida = 0;

                            if (Convert.IsDBNull(Dt_Aux.Rows[0][1]) == false)
                                Cantidad_Existente = Convert.ToInt32(Dt_Aux.Rows[0][1]);
                            else
                                Cantidad_Existente = 0;
                        }
                        //VALIDACION PARA NO ENTREGAR MAS DE LO DISPONIBLE Y EXISTENCIA Y NO PERMITIR NEGATIVOS
                        //if ((Cantidad_Comprometida != 0) & (Cantidad_Existente != 0))
                        //if ((Cantidad_Comprometida > 0) & (Cantidad_Existente > 0))
                        //int Ctd_Entregar = int.Parse(Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["CANTIDAD_A_ENTREGAR"].ToString());
                        if (Cantidad_Comprometida >= Cantidad_A_Entregar && Cantidad_Existente >= Cantidad_A_Entregar && Cantidad_Existente - Cantidad_A_Entregar > -1)
                        {
                            //Realizar los calculos de los montos
                            Cantidad_Comprometida = Cantidad_Comprometida - Cantidad_A_Entregar;
                            Cantidad_Existente = Cantidad_Existente - Cantidad_A_Entregar;

                            //Consulta para modificar las cantidades en la base de datos
                            Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " ";
                            Mi_SQL += "SET " + Cat_Com_Productos.Campo_Comprometido + " = " + Cantidad_Comprometida.ToString().Trim() + ", ";
                            Mi_SQL += Cat_Com_Productos.Campo_Existencia + " = " + Cantidad_Existente.ToString().Trim() + " ";
                            Mi_SQL += "WHERE " + Cat_Com_Productos.Campo_Producto_ID + " = '" + Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["PRODUCTO_ID"].ToString().Trim() + "'";

                            //Ejecutar consulta
                            Obj_Comando.CommandText = Mi_SQL;
                            Obj_Comando.ExecuteNonQuery();
                        }
                        else
                        {
                            Datos.P_Mensaje = "Verifique kardex de producto: [" +
                            Datos.P_Dt_Productos_Requisicion.Rows[Cont_Elementos]["PRODUCTO_ID"].ToString().Trim() + "]";
                            Obj_Transaccion.Rollback();
                            Obj_Conexion.Close();
                            return (0);
                        }
                    }
                }
                //Ejecutar transaccion
                Obj_Transaccion.Commit();

                //Entregar resultado
                return Datos.P_No_Orden_Salida;
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }
                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Conexion.Close();
            }
            
        }


        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Alta_Poliza
        /// DESCRIPCION:            Método utilizado consultar la información utilizada para mostrar la orden de salida
        /// PARAMETROS :            
        /// CREO       :            Salvador Hernández Ramírez
        /// FECHA_CREO :            24/Junio/2011  
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        private static String Alta_Poliza(String No_Salida, String No_Requisicion, Double Monto_Total, SqlCommand P_Cmd)
        {
            SqlDataAdapter Da = new SqlDataAdapter();
            DataTable Dt_Auxiliar = new DataTable();
            DataTable Dt_Cuentas_Almacen = new DataTable();
            DataTable Dt_Cuenta = new DataTable();
            String Mensaje = "";
            String Mi_SQL = "";
            String Tipo_Poliza_ID = "";
            Boolean Almacen_General = true;

            Mi_SQL = "SELECT * FROM " + Cat_Alm_Parametros_Cuentas.Tabla_Cat_Alm_Parametros_Cuentas;
            P_Cmd.CommandText = Mi_SQL;
            Da.SelectCommand = P_Cmd;
            Dt_Cuentas_Almacen = new DataTable();
            Da.Fill(Dt_Cuentas_Almacen);

            Mi_SQL = "SELECT DISTINCT(" + Ope_Com_Req_Producto.Campo_Tipo + ") FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
            Mi_SQL += " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion;
            P_Cmd.CommandText = Mi_SQL;
            Da.SelectCommand = P_Cmd;
            Dt_Auxiliar = new DataTable();
            Da.Fill(Dt_Auxiliar);

            if (Dt_Auxiliar.Rows.Count == 1 && Dt_Auxiliar.Rows[0][Ope_Com_Req_Producto.Campo_Tipo].ToString().Trim() == "PRODUCTO")
            {
                Mi_SQL = "SELECT DISTINCT(PROD." + Cat_Com_Productos.Campo_Almacen_General;
                Mi_SQL += ") FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PROD";
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PROD";
                Mi_SQL += " ON REQ_PROD." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = PROD." + Cat_Com_Productos.Campo_Producto_ID;
                Mi_SQL += " WHERE REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion;
                P_Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = P_Cmd;
                Dt_Auxiliar = new DataTable();
                Da.Fill(Dt_Auxiliar);

                if (Dt_Auxiliar.Rows.Count == 1 && Dt_Auxiliar.Rows[0][Cat_Com_Productos.Campo_Almacen_General].ToString().Trim() == "NO")
                {
                    Almacen_General = false;
                }
            }

            Mi_SQL = "SELECT " + Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID;
            Mi_SQL += " FROM " + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas;
            Mi_SQL += " WHERE " + Cat_Con_Tipo_Polizas.Campo_Descripcion + " LIKE '%DIARIO%'";
            P_Cmd.CommandText = Mi_SQL;
            Tipo_Poliza_ID = P_Cmd.ExecuteScalar().ToString().Trim();

            DataTable Dt_Detalles_Poliza = new DataTable();
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("MOMENTO_FINAL", typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("Nombre_Cuenta", typeof(System.String));
            Dt_Detalles_Poliza.AcceptChanges();

            Mi_SQL = "SELECT REQ_PROD." + Ope_Com_Req_Producto.Campo_Partida_ID;
            Mi_SQL += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID;
            Mi_SQL += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID;
            Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
            Mi_SQL += " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PROD";
            Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ";
            Mi_SQL += " ON REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL += " WHERE REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion;
            P_Cmd.CommandText = Mi_SQL;
            Da.SelectCommand = P_Cmd;
            Dt_Auxiliar = new DataTable();
            Da.Fill(Dt_Auxiliar);

            if (Dt_Auxiliar.Rows.Count > 0)
            {
                Mi_SQL = "SELECT PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID;
                Mi_SQL += ", FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS CLAVE_FTE_FINANCIAMIENTO";
                Mi_SQL += ", FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FTE_FINANCIAMIENTO";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID;
                Mi_SQL += ", AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Clave + " AS CLAVE_AREA_FUNCIONAL";
                Mi_SQL += ", AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS AREA_FUNCIONAL";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID;
                Mi_SQL += ", PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS CLAVE_PROYECTO_PROGRAMA";
                Mi_SQL += ", PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS PROYECTO_PROGRAMA";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID;
                Mi_SQL += ", DEP." + Cat_Dependencias.Campo_Clave + " AS CLAVE_DEPENDENCIA";
                Mi_SQL += ", DEP." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;
                Mi_SQL += ", PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS CLAVE_PARTIDA";
                Mi_SQL += ", PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID;
                Mi_SQL += ", CUENTAS." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " AS CUENTA";
                Mi_SQL += " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTOS";

                Mi_SQL += " LEFT OUTER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FTE_FIN";
                Mi_SQL += " ON FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " AREA_FUN";
                Mi_SQL += " ON AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " PROY_PROG";
                Mi_SQL += " ON PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP";
                Mi_SQL += " ON DEP." + Cat_Dependencias.Campo_Dependencia_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDAS";
                Mi_SQL += " ON PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTAS";
                Mi_SQL += " ON CUENTAS." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID;
                Mi_SQL += " WHERE PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dt_Auxiliar.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim() + "'";
                Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dt_Auxiliar.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim() + "'";
                Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dt_Auxiliar.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim() + "'";
                Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dt_Auxiliar.Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString().Trim() + "'";
                Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = YEAR(GETDATE())";
                P_Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = P_Cmd;
                Dt_Cuenta = new DataTable();
                Da.Fill(Dt_Cuenta);
            }


            if (Dt_Cuenta.Rows.Count > 0 && !String.IsNullOrEmpty(Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID].ToString().Trim()))
            {
                DataRow row = Dt_Detalles_Poliza.NewRow();
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID].ToString().Trim();
                row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Dt_Cuenta.Rows[0]["CUENTA"].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = No_Requisicion;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Monto_Total;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0.0;
                row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID].ToString().Trim();
                row["Nombre_Cuenta"] = Dt_Cuenta.Rows[0]["CUENTA"].ToString().Trim();
                Dt_Detalles_Poliza.Rows.Add(row);
                Dt_Detalles_Poliza.AcceptChanges();

                if (Dt_Cuentas_Almacen.Rows.Count > 0)
                {
                    Mi_SQL = "SELECT * FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                    Mi_SQL += " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = ";
                    Mi_SQL += Almacen_General ?
                            Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_General_ID].ToString().Trim() :
                            Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_Papeleria_ID].ToString().Trim();

                    P_Cmd.CommandText = Mi_SQL;
                    Da.SelectCommand = P_Cmd;
                    Dt_Auxiliar = new DataTable();
                    Da.Fill(Dt_Auxiliar);

                    if (Dt_Auxiliar.Rows.Count > 0)
                    {
                        row = Dt_Detalles_Poliza.NewRow();
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();
                        row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                        row["Nombre_Cuenta"] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = No_Requisicion;
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0.0;
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = Monto_Total;
                        row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID].ToString().Trim();
                        Dt_Detalles_Poliza.Rows.Add(row);
                        Dt_Detalles_Poliza.AcceptChanges();


                        Cls_Ope_Con_Polizas_Negocio Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio();

                        Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = Tipo_Poliza_ID;
                        Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now);
                        Rs_Alta_Ope_Con_Polizas.P_Fecha_Poliza = DateTime.Now;
                        Rs_Alta_Ope_Con_Polizas.P_Concepto = "Salida " + No_Salida + ",RQ-" + No_Requisicion.Trim() + " STOCK,Almacen ";
                        Rs_Alta_Ope_Con_Polizas.P_Concepto += Almacen_General ? "General " : "Papeleria ";
                        Rs_Alta_Ope_Con_Polizas.P_Total_Debe = Monto_Total; //monto total con iva de productos
                        Rs_Alta_Ope_Con_Polizas.P_Total_Haber = Monto_Total;
                        Rs_Alta_Ope_Con_Polizas.P_No_Partida = 2;
                        Rs_Alta_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                        Rs_Alta_Ope_Con_Polizas.P_Dt_Detalles_Polizas = Dt_Detalles_Poliza;
                        Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID;
                        Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Autorizo = Cls_Sessiones.Empleado_ID;
                        Rs_Alta_Ope_Con_Polizas.P_Prefijo = "";
                        Rs_Alta_Ope_Con_Polizas.P_Cmmd = P_Cmd;
                        string[] Datos_Poliza = Rs_Alta_Ope_Con_Polizas.Alta_Poliza();
                        Mensaje = "Alta_Exitosa";
                    }
                    else
                    {
                        Mensaje = "No se encontraron los datos de la cuenta contable ";
                        Mensaje += Almacen_General ? "General " : "Papeleria ";
                        Mensaje += "de Almacen.";
                    }
                }
                else
                {
                    Mensaje = "No se han asignado las cuentas contables General ó Papeleria para Almacen.";
                }
            }
            else
            {
                Mi_SQL = "SELECT " + Ope_Com_Requisiciones.Campo_Codigo_Programatico;
                Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + No_Requisicion;
                P_Cmd.CommandText = Mi_SQL;
                Mensaje = "No se encontro la cuenta contable del gasto en el presupuesto aprobado para este año. Codigo Programatico: ";
                Mensaje += P_Cmd.ExecuteScalar().ToString().Trim();
            }
            return Mensaje;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Informacion_General_OS
        ///DESCRIPCIÓN:          Método donde se consulta la información general de la orden de salida que se genero
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           24/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Informacion_General_OS(Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Datos)
        {
            // Declaración de variables
            String Mi_SQL = null;
            DataTable Dt_Cabecera = new DataTable();

            Mi_SQL = "SELECT " + "SALIDAS." + Alm_Com_Salidas.Campo_No_Salida + " as NO_ORDEN_SALIDA"; 
            Mi_SQL += ",(select DEPENDENCIAS."+ Cat_Dependencias.Campo_Nombre + " from ";
            Mi_SQL += Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS "; 
            Mi_SQL += " where SALIDAS."+ Alm_Com_Salidas.Campo_Dependencia_ID + " = DEPENDENCIAS." ;
            Mi_SQL = Mi_SQL+ Cat_Dependencias.Campo_Dependencia_ID + ")as UNIDAD_RESPONSABLE";

            Mi_SQL += ",(select distinct (FINANCIAMIENTO."+ Cat_SAP_Fuente_Financiamiento.Campo_Descripcion +")";
            Mi_SQL += " from " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FINANCIAMIENTO "; 
            Mi_SQL += "  where FINANCIAMIENTO."+ Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
            Mi_SQL += " = (select distinct(REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID + ") from ";
            Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PRODUCTO "; 
            Mi_SQL += " where  REQ_PRODUCTO." +  Ope_Com_Req_Producto.Campo_Requisicion_ID + " = ";
            Mi_SQL += " SALIDAS." + Alm_Com_Salidas.Campo_Requisicion_ID + "))as F_FINANCIAMIENTO" ;
            
            Mi_SQL += ",(select distinct (PROY_PROGRAMAS."+ Cat_Com_Proyectos_Programas.Campo_Descripcion + ")" ;
            Mi_SQL += " from " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + " PROY_PROGRAMAS "; 
            Mi_SQL += "  where PROY_PROGRAMAS."+ Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID;
            Mi_SQL += " =(select distinct (REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID + ") from ";
            Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PRODUCTO "; 
            Mi_SQL += " where  REQ_PRODUCTO." +  Ope_Com_Req_Producto.Campo_Requisicion_ID + " = ";
            Mi_SQL += " SALIDAS." +Alm_Com_Salidas.Campo_Requisicion_ID + "))as PROGRAMA" ;

            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio;
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion;
            Mi_SQL += ", SALIDAS." + Alm_Com_Salidas.Campo_Usuario_Creo + " as ENTREGO ";
            
            Mi_SQL += ", (select EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + " +' '+";
            Mi_SQL += " EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + " +' '+";
            Mi_SQL += " EMPLEADOS." + Cat_Empleados.Campo_Nombre;
            Mi_SQL += " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS ";
            Mi_SQL += " where EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + " = ";
            Mi_SQL += " SALIDAS." + Alm_Com_Salidas.Campo_Empleado_Solicito_ID + ") as RECIBIO";
            
            Mi_SQL += " FROM " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas+ " SALIDAS ";
            Mi_SQL += ", " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones+ " REQUISICIONES ";
            Mi_SQL += " where REQUISICIONES." +  Ope_Com_Requisiciones.Campo_Requisicion_ID + " = ";
            Mi_SQL += " SALIDAS." + Alm_Com_Salidas.Campo_Requisicion_ID+ "";
            Mi_SQL += " AND SALIDAS." + Alm_Com_Salidas.Campo_No_Salida+ " = ";
            Mi_SQL += Datos.P_No_Orden_Salida;

            Dt_Cabecera = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Cabecera;
        }



        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Detalles_Orden_Salida
        ///DESCRIPCIÓN:          Método donde se consultan los detalles de la orden de salida que se genero
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           24/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Detalles_Orden_Salida(Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Datos)
        {
            // Declaración de variables
            String Mi_SQL = null;
            DataTable Dt_Detalles = new DataTable();

            Mi_SQL = "SELECT SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " as NO_ORDEN_SALIDA"; 
            Mi_SQL += ",(select PRODUCTOS."+ Cat_Com_Productos.Campo_Clave+ " from ";
            Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS "; 
            Mi_SQL += " where SALIDAS_DETALLES."+ Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = PRODUCTOS." ;
            Mi_SQL += Cat_Com_Productos.Campo_Producto_ID + ")as CLAVE";

            Mi_SQL += ",(select PRODUCTOS."+ Cat_Com_Productos.Campo_Nombre+ " +' '+ ";
            Mi_SQL += " PRODUCTOS." + Cat_Com_Productos.Campo_Descripcion+ " from " ;
            Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS "; 
            Mi_SQL += " where SALIDAS_DETALLES."+ Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = PRODUCTOS." ;
            Mi_SQL += Cat_Com_Productos.Campo_Producto_ID + ")as PRODUCTO";

            Mi_SQL += ",(select REQ_PRODUCTOS."+ Ope_Com_Req_Producto.Campo_Cantidad+ " from ";
            Mi_SQL +=  Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto+ " REQ_PRODUCTOS " ;
            Mi_SQL += " where  REQ_PRODUCTOS." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = "; 
            Mi_SQL += " (select SALIDAS."+ Alm_Com_Salidas.Campo_Requisicion_ID+ " from ";
            Mi_SQL +=  Alm_Com_Salidas.Tabla_Alm_Com_Salidas + " SALIDAS " ;
            Mi_SQL += " where  SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = "; 
            Mi_SQL +=  " SALIDAS." + Alm_Com_Salidas.Campo_No_Salida + ")";
            Mi_SQL += " and REQ_PRODUCTOS."+ Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = ";
            Mi_SQL += " SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + ") as CANTIDAD_SOLICITADA ";

            Mi_SQL += ",SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Cantidad + " as CANTIDAD_ENTREGADA"; 
            
            Mi_SQL += ",(select UNIDADES."+ Cat_Com_Unidades.Campo_Abreviatura+ " from ";
            Mi_SQL +=  Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " UNIDADES " ;
            Mi_SQL += " where  UNIDADES." + Cat_Com_Unidades.Campo_Unidad_ID+ " = "; 
            Mi_SQL += " (select PRODUCTOS."+ Cat_Com_Productos.Campo_Unidad_ID + " from ";
            Mi_SQL +=  Cat_Com_Productos.Tabla_Cat_Com_Productos+ " PRODUCTOS " ;
            Mi_SQL += " where  SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = ";
            Mi_SQL += " PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID + ")) as UNIDADES";

            Mi_SQL += ",SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Costo + " as PRECIO"; 
            Mi_SQL += ",SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Subtotal + "";          
            Mi_SQL += ",SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_IVA + "";
            Mi_SQL += ",SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Importe + " as TOTAL"; 
          
            Mi_SQL += " FROM " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + " SALIDAS_DETALLES"; 
            Mi_SQL += " WHERE SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = ";
            Mi_SQL += Datos.P_No_Orden_Salida;

            Dt_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Detalles;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Rechazar_Requisicion
        ///DESCRIPCIÓN          : Rechaza la requisicion
        ///PARAMETROS           :  
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 19/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Rechazar_Requisicion(Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Datos)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Consultamos el maximo ID de la observacion y sacamos el consecutivo
                String Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Com_Req_Observaciones.Campo_Observacion_ID + ") + 1, 1) AS CONSECUTIVO FROM " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones;
                Datos.P_Observaciones_ID = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0]["CONSECUTIVO"].ToString();

                //Consultamos el maximo ID del historico requisicion y sacamos el consecutivo
                Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Com_Historial_Req.Campo_No_Historial + ") + 1, 1) AS CONSECUTIVO FROM " + Ope_Com_Historial_Req.Tabla_Ope_Com_Historial_Req;
                Datos.P_Historial_ID = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0]["CONSECUTIVO"].ToString();

                //Insertar un registro en las observaciones de la requisicion
                Mi_SQL = "INSERT INTO " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones +
                    " (" + Ope_Com_Req_Observaciones.Campo_Observacion_ID +
                    ", " + Ope_Com_Req_Observaciones.Campo_Requisicion_ID +
                    ", " + Ope_Com_Req_Observaciones.Campo_Comentario +
                    ", " + Ope_Com_Req_Observaciones.Campo_Estatus +                    
                    ", " + Ope_Com_Req_Observaciones.Campo_Usuario_Creo +
                    ", " + Ope_Com_Req_Observaciones.Campo_Fecha_Creo +
                    ") VALUES (" + Datos.P_Observaciones_ID + "," +
                     Datos.P_No_Requisicion + ", '" +
                     Datos.P_Comentarios + "', '" +
                     Datos.P_Estatus + "', '" +                     
                     Cls_Sessiones.Nombre_Empleado + "'," +
                     "GETDATE() )";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Insertar un registro en el seguimiento del contrarecibo
                Mi_SQL = "INSERT INTO " + Ope_Com_Historial_Req.Tabla_Ope_Com_Historial_Req +
                    " (" + Ope_Com_Historial_Req.Campo_No_Historial +
                    ", " + Ope_Com_Historial_Req.Campo_Estatus +
                    ", " + Ope_Com_Historial_Req.Campo_Fecha +
                    ", " + Ope_Com_Historial_Req.Campo_Empleado +
                    ", " + Ope_Com_Historial_Req.Campo_No_Requisicion +
                    ", " + Ope_Com_Historial_Req.Campo_Usuario_Creo +
                    ", " + Ope_Com_Historial_Req.Campo_Fecha_Creo +
                    ") VALUES (" + Datos.P_Historial_ID + ",'" +
                     Datos.P_Estatus + "',"+
                     "GETDATE(), '" +
                     Cls_Sessiones.Nombre_Empleado + "'," +
                     Datos.P_No_Requisicion + ",'" +
                     Cls_Sessiones.Nombre_Empleado + "'," +
                     "GETDATE() )";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Actualizar estatus en la requisicion
                Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " SET " + Ope_Com_Requisiciones.Campo_Estatus + " = 'EN CONSTRUCCION'";
                Mi_SQL += "," + Ope_Com_Requisiciones.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "'";
                Mi_SQL += "," + Ope_Com_Requisiciones.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Datos.P_No_Requisicion;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
                Mensaje_Error = Datos.P_No_Requisicion;

            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo autorizar el contrarecibo";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }//fin de Autorizar Cheque

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Reporte_Orden_Salida_Stock
        ///DESCRIPCIÓN          : Consulta para el reporte de la orden de salida de Stock
        ///PARAMETROS           : Datos: variable de la capa de negocios
        ///CREO                 : Noe Mosqueda Valadez
        ///FECHA_CREO           : 21/Febrero/2013 10:20
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataSet Reporte_Orden_Salida_Stock(Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DataTable Dt_Cabecera = new DataTable(); //Tabla para la cabecera
            DataTable Dt_Detalles = new DataTable(); //Tabla para los detalles
            DataSet Ds_Resultado = new DataSet(); //Dataset para el resultado
            DataTable Dt_Aux = new DataTable(); //tabla auxiliar para las consultas

            try
            {
                //Consulta para la cabecera
                Mi_SQL = "SELECT ALM_COM_SALIDAS.NO_SALIDA, CUENTA = '', DESCRIPCION_CUENTA = '', " +
                    "(Cat_Empleados.NOMBRE + ' ' + ISNULL(Cat_Empleados.APELLIDO_PATERNO, '') + ' ' + ISNULL(Cat_Empleados.APELLIDO_MATERNO, '')) AS ELABORO, " +
                    "(Cat_Empleados_2.NOMBRE + ' ' + ISNULL(Cat_Empleados_2.APELLIDO_PATERNO, '') + ' ' + ISNULL(Cat_Empleados_2.APELLIDO_MATERNO, '')) AS RECIBIO, " +
                    "OBSERVACIONES = '' FROM ALM_COM_SALIDAS " +
                    "INNER JOIN Cat_Empleados ON ALM_COM_SALIDAS.EMPLEADO_ALMACEN_ID = Cat_Empleados.EMPLEADO_ID " +
                    "INNER JOIN Cat_Empleados Cat_Empleados_2 ON ALM_COM_SALIDAS.EMPLEADO_SOLICITO_ID = Cat_Empleados_2.EMPLEADO_ID " +
                    "WHERE ALM_COM_SALIDAS.NO_SALIDA = " + Datos.P_No_Orden_Salida.ToString().Trim() + " ";

                //Ejecutar consulta
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar la tabla a la cabecera
                Dt_Cabecera = Dt_Aux.Copy();
                Dt_Cabecera.TableName = "Cabecera";

                //Consulta para los detalles de la orden de salida
                Mi_SQL = "SELECT ALM_COM_SALIDAS_DETALLES.NO_SALIDA, CAT_COM_PRODUCTOS.CLAVE, CAT_COM_PRODUCTOS.NOMBRE, CAT_COM_PRODUCTOS.DESCRIPCION AS DESCRIPCION_PRODUCTO, " +
                    "CAT_COM_UNIDADES.ABREVIATURA AS UNIDAD, ALM_COM_SALIDAS_DETALLES.CANTIDAD, ALM_COM_SALIDAS_DETALLES.COSTO, ALM_COM_SALIDAS_DETALLES.COSTO_PROMEDIO, " +
                    "ALM_COM_SALIDAS_DETALLES.IMPORTE, ALM_COM_SALIDAS_DETALLES.SUBTOTAL, ALM_COM_SALIDAS_DETALLES.IVA " +
                    "FROM ALM_COM_SALIDAS_DETALLES " +
                    "INNER JOIN CAT_COM_PRODUCTOS ON ALM_COM_SALIDAS_DETALLES.PRODUCTO_ID = CAT_COM_PRODUCTOS.PRODUCTO_ID " +
                    "LEFT JOIN CAT_COM_UNIDADES ON CAT_COM_PRODUCTOS.UNIDAD_ID = CAT_COM_UNIDADES.UNIDAD_ID " +
                    "WHERE ALM_COM_SALIDAS_DETALLES.NO_SALIDA = " + Datos.P_No_Orden_Salida.ToString().Trim() + " ";

                //Ejecutar la consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar la tabla de los detalles
                Dt_Detalles = Dt_Aux.Copy();
                Dt_Detalles.TableName = "Detalles";

                //Colocar las tablas en el dataset
                Ds_Resultado.Tables.Add(Dt_Cabecera);
                Ds_Resultado.Tables.Add(Dt_Detalles);

                //Entregar resultado
                return Ds_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Observaciones
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para consultar Observaciones 
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: David Herrera Rincón
        ///FECHA_CREO: 27/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Observaciones(Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Datos)
        {
            String Mi_SQL = "SELECT " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones + "." + Ope_Com_Req_Observaciones.Campo_Observacion_ID +
                            ", " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones + "." + Ope_Com_Req_Observaciones.Campo_Comentario +
                            ", " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones + "." + Ope_Com_Req_Observaciones.Campo_Estatus +
                            "," + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones + "." + Ope_Com_Req_Observaciones.Campo_Fecha_Creo +
                            ", " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones + "." + Ope_Com_Req_Observaciones.Campo_Usuario_Creo +
                            " FROM " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones +
                            " INNER JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                            " ON " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones + "." + Ope_Com_Req_Observaciones.Campo_Requisicion_ID +
                            " = " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                            " WHERE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = '" + Datos.P_No_Requisicion + "' " +
                            " ORDER BY " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones + "." + Ope_Com_Req_Observaciones.Campo_Observacion_ID + " ASC";
            DataTable Dt_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Detalles;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: LLenar_Combo
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para consultar las dependencias o el usuario 
        ///PARAMETROS:  1.- Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: David Herrera Rincón
        ///FECHA_CREO: 27/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Llenar_Combo(Cls_Ope_Com_Alm_Requisiciones_Stock_Negocio Datos, String Consulta)
        {
            // Declaración de variables
            String Mi_SQL = null;
            DataTable Dt_Requisiciones = new DataTable();

            Mi_SQL = "SELECT DISTINCT";

            if (Consulta == "DEPENDENCIA")
            {
                Mi_SQL += " DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + " as UNIDAD_RESPONSABLE_ID";
                Mi_SQL += ", DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " as UNIDAD_RESPONSABLE";                
            }
            else if (Consulta == "SOLICITO")
            {
                Mi_SQL += " REQUISICIONES." + Ope_Com_Requisiciones.Campo_Empleado_Generacion_ID + " as EMPLEADO_GENERACION_ID";
                Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Usuario_Creo + " as SOLICITO";   
            }
            
            Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICIONES ";
            Mi_SQL += " JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
            Mi_SQL += " ON REQUISICIONES." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
            Mi_SQL += " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID;
            Mi_SQL += " JOIN " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQUISICION_PRODUCTO ";
            Mi_SQL += " ON REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = REQUISICION_PRODUCTO." + Ope_Com_Req_Producto.Campo_Requisicion_ID;
            Mi_SQL += " JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS ";
            Mi_SQL += " ON REQUISICION_PRODUCTO." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID;
            Mi_SQL += " WHERE  REQUISICIONES." + Ope_Com_Requisiciones.Campo_Tipo + "= '" + "STOCK'";
            Mi_SQL += " and  ( REQUISICIONES." + Ope_Com_Requisiciones.Campo_Estatus + "= '" + "ALMACEN'"; // Nota en esta parte debe ir Surtida
            Mi_SQL += " or  REQUISICIONES." + Ope_Com_Requisiciones.Campo_Estatus + "= '" + "PARCIAL')"; // Nota en esta parte debe ir Surtida

            if (Datos.P_Dependencia_ID != null)
            {
                Mi_SQL += " and  REQUISICIONES." + Ope_Com_Requisiciones.Campo_Dependencia_ID + "= '" + Datos.P_Dependencia_ID + "'";
            }

            Dt_Requisiciones = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Requisiciones;
        }

    }
}