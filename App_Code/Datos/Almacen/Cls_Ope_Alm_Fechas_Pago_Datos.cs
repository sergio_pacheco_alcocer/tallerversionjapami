﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Ope_Alm_Fechas_Pago.Negocio;
using JAPAMI.Sessiones;


/// <summary>
/// Summary description for Cls_Ope_Alm_Fechas_Pago_Datos
/// </summary>
namespace JAPAMI.Ope_Alm_Fechas_Pago.Datos
{
    public class Cls_Ope_Alm_Fechas_Pago_Datos
    {
        public Cls_Ope_Alm_Fechas_Pago_Datos()
        {
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Fechas_Pago
        ///DESCRIPCIÓN: Consulta las fechas de los pagos
        ///PARAMETROS:  1.- Cls_Ope_Alm_Fechas_Pago_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  31/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Fechas_Pago(Cls_Ope_Alm_Fechas_Pago_Negocio Clase_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                //Creamos la consulta
                Mi_SQL = "SELECT * ";
                Mi_SQL += " FROM " + Ope_Alm_Fechas_Pagos.Tabla_Ope_Alm_Fechas_Pagos;
                //Validamos los filtros por año
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Anio_No_Fecha_Pago))
                {
                    Mi_SQL += " WHERE YEAR(" + Ope_Alm_Fechas_Pagos.Campo_Fecha_Pago + ") = " + Clase_Negocio.P_Anio_No_Fecha_Pago;
                }

                //Colocar el orden
                Mi_SQL += " ORDER BY " + Ope_Alm_Fechas_Pagos.Campo_Fecha_Limite_Recepcion + ", " + Ope_Alm_Fechas_Pagos.Campo_Fecha_Pago + " ";

                //regresamos los valores
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las fechas. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Agregar_Fechas
        ///DESCRIPCIÓN          : Inserta las fechas
        ///PARAMETROS           :  
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 31/Enero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Agregar_Fechas(Cls_Ope_Alm_Fechas_Pago_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Consultamos el consecutivo
                String Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Alm_Fechas_Pagos.Campo_Anio_No_Fecha_Pago + ") + 1, 1) AS CONSECUTIVO FROM " + Ope_Alm_Fechas_Pagos.Tabla_Ope_Alm_Fechas_Pagos;
                //Asignamos el valor
                Clase_Negocio.P_Anio_No_Fecha_Pago = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0]["CONSECUTIVO"].ToString();

                //Insertar un registro de las fechas
                Mi_SQL = "INSERT INTO " + Ope_Alm_Fechas_Pagos.Tabla_Ope_Alm_Fechas_Pagos +
                    " (" + Ope_Alm_Fechas_Pagos.Campo_Anio_No_Fecha_Pago +
                    ", " + Ope_Alm_Fechas_Pagos.Campo_Fecha_Limite_Recepcion +
                    ", " + Ope_Alm_Fechas_Pagos.Campo_Fecha_Pago;
                if (Clase_Negocio.P_Comentarios != null)
                {
                    Mi_SQL = Mi_SQL + ", " + Ope_Alm_Fechas_Pagos.Campo_Comentarios ;
                }

                Mi_SQL = Mi_SQL + ", " + Ope_Alm_Fechas_Pagos.Campo_Usuario_Creo +
               ", " + Ope_Alm_Fechas_Pagos.Campo_Fecha_Creo +
               ") VALUES (" +
                Clase_Negocio.P_Anio_No_Fecha_Pago + ",'" +
                DateTime.Parse(Clase_Negocio.P_Fecha_Recepcion).ToString("dd/MM/yyyy") + "','" +
                DateTime.Parse(Clase_Negocio.P_Fecha_Pago).ToString("dd/MM/yyyy") + "','";
                if (Clase_Negocio.P_Comentarios != null)
                {
                    Mi_SQL = Mi_SQL + Clase_Negocio.P_Comentarios + "','";
                }
                    Mi_SQL = Mi_SQL + Cls_Sessiones.Nombre_Empleado + "'," +
                     "GETDATE() )";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Trans.Commit();
                Mensaje_Error = Clase_Negocio.P_Fecha_Pago;

            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo agregar la fecha";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Actualizar_Fecha
        ///DESCRIPCIÓN          : Actualizamos el registro
        ///PARAMETROS           :  
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 31/Enero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Actualizar_Fecha(Cls_Ope_Alm_Fechas_Pago_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Actualizar las fechas
                String Mi_SQL = "UPDATE " + Ope_Alm_Fechas_Pagos.Tabla_Ope_Alm_Fechas_Pagos;
                Mi_SQL = Mi_SQL + " SET " + Ope_Alm_Fechas_Pagos.Campo_Fecha_Limite_Recepcion + " = '" + Clase_Negocio.P_Fecha_Recepcion + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Alm_Fechas_Pagos.Campo_Fecha_Pago + " = '" + Clase_Negocio.P_Fecha_Pago + "'";
                if (Clase_Negocio.P_Comentarios != null)
                {
                    Mi_SQL = Mi_SQL + ", " + Ope_Alm_Fechas_Pagos.Campo_Comentarios + " = '" + Clase_Negocio.P_Comentarios + "'";
                }
                Mi_SQL = Mi_SQL + ", " + Ope_Alm_Fechas_Pagos.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Alm_Fechas_Pagos.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Alm_Fechas_Pagos.Campo_Anio_No_Fecha_Pago + " = " + Clase_Negocio.P_Anio_No_Fecha_Pago;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Trans.Commit();
                Mensaje_Error = "Rechazado";
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo actualizar la fecha";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Ultimo
        ///DESCRIPCIÓN: Consulta las ultimas fechas de los pagos
        ///PARAMETROS:  1.- Cls_Ope_Alm_Fechas_Pago_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  31/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Ultimo(Cls_Ope_Alm_Fechas_Pago_Negocio Clase_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                //Creamos la consulta
                Mi_SQL = "SELECT * ";
                Mi_SQL += " FROM " + Ope_Alm_Fechas_Pagos.Tabla_Ope_Alm_Fechas_Pagos;
                Mi_SQL += " WHERE " + Ope_Alm_Fechas_Pagos.Campo_Anio_No_Fecha_Pago + " IN";
                Mi_SQL += " (SELECT MAX(" + Ope_Alm_Fechas_Pagos.Campo_Anio_No_Fecha_Pago + ") FROM " + Ope_Alm_Fechas_Pagos.Tabla_Ope_Alm_Fechas_Pagos + ")";
                //regresamos los valores
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las  ultimas fechas. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCION: Eliminar_Fecha
        ///DESCRIPCION: ELiminar una fecha
        ///PARAMETROS:  Datos: Instancia de la capa de negocios
        ///CREO:        Noe Mosqueda Valadez
        ///FECHA_CREO:  23/Abril/2013 17:54
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Eliminar_Fecha(Cls_Ope_Alm_Fechas_Pago_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL = null;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        

            try
            {
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos

                //Asignar consulta
                Mi_SQL = "DELETE FROM " + Ope_Alm_Fechas_Pagos.Tabla_Ope_Alm_Fechas_Pagos + 
                        " WHERE " + Ope_Alm_Fechas_Pagos.Campo_Anio_No_Fecha_Pago + " = " + Datos.P_Anio_No_Fecha_Pago;

                //Ejecutar consulta
                Comando_SQL.CommandText = Mi_SQL;
                Comando_SQL.ExecuteNonQuery();

                //Ejecutar transaccion
                Transaccion_SQL.Commit();
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            }
        }
    }
}