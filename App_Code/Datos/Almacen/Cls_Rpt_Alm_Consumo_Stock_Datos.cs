﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Reporte_Alm_Consumo_Stock.Negocio;


/// <summary>
/// Summary description for Cls_Rpt_Com_Consumo_Stock
/// </summary>
namespace JAPAMI.Reporte_Alm_Consumo_Stock.Datos
{
    public class Cls_Rpt_Alm_Consumo_Stock_Datos
    {
        public Cls_Rpt_Alm_Consumo_Stock_Datos()
        {
        }
        #region METODOS
        ///*******************************************************************************
        /// NOMBRE DEL METODO:     Consulta_Productos
        /// DESCRIPCION:            Realizar la consulta los productos
        /// PARAMETROS :            
        /// CREO       :            Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO :            09/Octubre/2012 01:54pm
        /// MODIFICO          :     
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION:     
        ///*******************************************************************************/
        public static DataTable Consulta_Productos()
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;

            try
            {
                //Asignar consulta para los Familias
                Mi_SQL = "SELECT " + Cat_Com_Productos.Campo_Producto_ID + ",";
                Mi_SQL = Mi_SQL + Cat_Com_Productos.Campo_Clave + "+ ' ' + " + Cat_Com_Productos.Campo_Nombre + " + ' ' + " + Cat_Com_Productos.Campo_Descripcion + " AS PRODUCTO" ;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Productos.Campo_Stock + " = 'SI'";
                //Ordenar
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Com_Productos.Campo_Nombre;
                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DEL METODO:     Consulta_Gerencias
        /// DESCRIPCION:            Realizar la consulta de las gerenciar registradas en el sistema
        /// PARAMETROS :            
        /// CREO       :            Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO :            09/Octubre/2012 01:54pm
        /// MODIFICO          :     
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION:     
        ///*******************************************************************************/
        public static DataTable Consulta_Gerencias()
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            try
            {
                //Asignar consulta 
                Mi_SQL = "SELECT " + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Grupos_Dependencias.Campo_Clave + " + '-' + " + Cat_Grupos_Dependencias.Campo_Nombre + " AS GERENCIA ";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias;
                //Ordenar
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Grupos_Dependencias.Campo_Nombre;
                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DEL METODO : Consulta_Dependencias
        /// DESCRIPCION       : Realizar la consulta de las gerenciar registradas en el sistema
        /// PARAMETROS        : Datos: Variable que se usa para cominicarse con la clase de negocios        
        /// CREO              : Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO        : 09/Octubre/2012 04:42pm
        /// MODIFICO          :     
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION:     
        ///*******************************************************************************/
        public static DataTable Consulta_Dependencias(Cls_Rpt_Alm_Consumo_Stock_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            try
            {
                //Asignar consulta 
                Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Dependencias.Campo_Clave + " + '-' + " + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA ";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias ;
                if(!String.IsNullOrEmpty(Datos.P_Grupo_Dependencia_ID))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Dependencias.Campo_Grupo_Dependencia_ID + " = '" + Datos.P_Grupo_Dependencia_ID + "' ";
                }
                //Ordenar
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Dependencias.Campo_Nombre;
                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DEL METODO : Consultar_Productos_Busqueda
        /// DESCRIPCION       : Realizar la consulta de las gerenciar registradas en el sistema
        /// PARAMETROS        : Datos: Variable que se usa para cominicarse con la clase de negocios        
        /// CREO              : Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO        : 09/Octubre/2012 04:42pm
        /// MODIFICO          :     
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION:     
        ///*******************************************************************************/
        public static DataTable Consultar_Productos_Busqueda(Cls_Rpt_Alm_Consumo_Stock_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            try
            {
                //Asignar consulta 
                Mi_SQL = "SELECT " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." +  Cat_Com_Productos.Campo_Producto_ID + " AS ID, ";
                Mi_SQL = Mi_SQL + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Clave + ", ";
                Mi_SQL = Mi_SQL + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Nombre + ", ";
                Mi_SQL = Mi_SQL + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Descripcion + ", ";
                Mi_SQL = Mi_SQL + " ISNULL(" + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Modelo + ", '') AS MODELO, "; 
                Mi_SQL = Mi_SQL + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + "." + Cat_Com_Unidades.Campo_Nombre + " AS UNIDAD, ";
                Mi_SQL = Mi_SQL + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Costo;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " LEFT OUTER JOIN " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades;
                Mi_SQL = Mi_SQL + " ON " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Unidad_ID + " = " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + "." + Cat_Com_Unidades.Campo_Unidad_ID;
                Mi_SQL = Mi_SQL + " WHERE " +  Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Stock + " = 'SI' ";
                if(!string.IsNullOrEmpty(Datos.P_Nombre_Producto))
                {
                    Mi_SQL = Mi_SQL + " AND " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Nombre + " LIKE UPPER('%" + Datos.P_Nombre_Producto.Trim() + "%')";
                }
                if(!String.IsNullOrEmpty(Datos.P_Clave_Producto))
                {
                    Mi_SQL = Mi_SQL + " AND " +  Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Clave + " = " + Datos.P_Clave_Producto;
                }
                if(!String.IsNullOrEmpty(Datos.P_Categoria_Producto))
                {
                    Mi_SQL  = Mi_SQL + " AND " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Categoria_ID + " = '" + Datos.P_Categoria_Producto + "'";
                }
                //Ordenar
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Nombre;
                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DEL METODO : Consulta_Categorias
        /// DESCRIPCION       : Realizar la consulta de las categorias registradas en el sistema
        /// PARAMETROS        : Datos: Variable que se usa para cominicarse con la clase de negocios        
        /// CREO              : Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO        : 09/Octubre/2012 04:42pm
        /// MODIFICO          :     
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION:     
        ///*******************************************************************************/
        public static DataTable Consulta_Categorias(Cls_Rpt_Alm_Consumo_Stock_Negocio Negocio)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            try
            {
                //Asignar consulta 
                Mi_SQL = " Select CATEGORIA_ID, NOMBRE FROM CAT_COM_CATEGORIA_PRODUCTOS ";
                //Ordenar
                Mi_SQL = Mi_SQL + " ORDER BY NOMBRE ";
                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

         ///*******************************************************************************
        /// NOMBRE DEL METODO : Consulta_Reporte_Producto_Entregado_Mes_Especifico
        /// DESCRIPCION       : Consulta las salidas de un producto especifico en un mes determinado,
        ///                     muestra informacion de la fecha en que se hizo la salida del produto,
        ///                     la dependencia que lo creo, la gerencia a la que pertenece y el usuario
        ///                     que recibio el producto
        /// PARAMETROS        : Datos: Variable que se usa para cominicarse con la clase de negocios        
        /// CREO              : Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO        : 11/Octubre/2012 10:24pm
        /// MODIFICO          :     
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION:     
        ///*******************************************************************************/
        public static DataTable Consulta_Reporte_Producto_Entregado_Mes_Especifico(Cls_Rpt_Alm_Consumo_Stock_Negocio Negocio)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            try
            {
                //Asignar consulta 
                Mi_SQL = " SELECT " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." +  Cat_Grupos_Dependencias.Campo_Nombre + " AS GERENCIA, ";
                Mi_SQL = Mi_SQL + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS UNIDAD_RESPONSABLE, ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas.Tabla_Alm_Com_Salidas+ "." + Alm_Com_Salidas.Campo_Fecha_Creo + " AS FECHA, ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida + ", ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_Cantidad + ", ";
                Mi_SQL = Mi_SQL + " CONVERT(INT," + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles +"." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + ") AS CLAVE_PRODUCTO, ";
                Mi_SQL = Mi_SQL + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + " + ' ' + " ;
                         Mi_SQL = Mi_SQL + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + " + ' ' + ";
                         Mi_SQL = Mi_SQL + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + " AS  NOMBRE ";
                Mi_SQL = Mi_SQL + " FROM " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + " JOIN " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + " ON ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida + " = " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " ON ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Empleado_Solicito_ID + " = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " ON " ;
                Mi_SQL = Mi_SQL + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + " JOIN " +  Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + " ON " ;
                Mi_SQL = Mi_SQL + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID + " = " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID;
                Mi_SQL = Mi_SQL + " WHERE MONTH(" + Alm_Com_Salidas.Tabla_Alm_Com_Salidas  + "." + Alm_Com_Salidas.Campo_Fecha_Creo + ") = " + Negocio.P_Mes;
                Mi_SQL = Mi_SQL + " AND YEAR(" + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Fecha_Creo + ") = " + Negocio.P_Anio;
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                if(!String.IsNullOrEmpty(Negocio.P_Grupo_Dependencia_ID))
                {
                    Mi_SQL = Mi_SQL + " AND " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + " = '" + Negocio.P_Grupo_Dependencia_ID + "'";
                }
                if(!String.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                {
                    Mi_SQL = Mi_SQL + " AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID + "'";
                }
                    //Ordenar
                Mi_SQL = Mi_SQL + " ORDER BY GERENCIA, UNIDAD_RESPONSABLE ";
                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
         ///*******************************************************************************
        /// NOMBRE DEL METODO : Consulta_Reporte_Salidas_Anual
        /// DESCRIPCION       : Realizar la consulta que obtiene la cantidad de salidas que tiene por mes 
        ///                     un productos, contemplado todos los mese;
        /// PARAMETROS        : Datos: Variable que se usa para cominicarse con la clase de negocios        
        /// CREO              : Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO        : 11/Octubre/2012 04:42pm
        /// MODIFICO          :     
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION:     
        ///*******************************************************************************/
        public static DataTable Consulta_Reporte_Salidas_Anual(Cls_Rpt_Alm_Consumo_Stock_Negocio Negocio)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            try
            {
                //Asignar consulta 1
                Mi_SQL = " SELECT " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + " AS Gerencia, ";
                Mi_SQL = Mi_SQL + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS Unidad_Responsable, ";
                Mi_SQL = Mi_SQL + " ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ")= 1 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "),0) AS Ene, ";
                Mi_SQL = Mi_SQL + " ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "," + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 2 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "),0) AS Feb, ";
                Mi_SQL = Mi_SQL + " ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "," + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 3 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "),0) AS Mar, ";
                Mi_SQL = Mi_SQL + " ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "," + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 4 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "),0) AS Abr, ";
                Mi_SQL = Mi_SQL + " ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "," + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 5 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "),0) AS May, ";
                Mi_SQL = Mi_SQL + " ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "," + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 6 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "),0) AS Jun, ";
                Mi_SQL = Mi_SQL + " ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "," + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 7 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "),0) AS Jul, ";
                Mi_SQL = Mi_SQL + " ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "," + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 8 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "),0) AS Ags, ";
                Mi_SQL = Mi_SQL + " ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "," + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 9 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "),0) AS Sep, ";
                Mi_SQL = Mi_SQL + " ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "," + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 10 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "),0) AS Oct, ";
                Mi_SQL = Mi_SQL + " ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "," + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 11 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "),0) AS Nov, ";
                Mi_SQL = Mi_SQL + " ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "," + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 12 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "),0) AS Dic, ";
                Mi_SQL = Mi_SQL + " ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + "= '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "),0) AS Total, ";
                Mi_SQL = Mi_SQL + "(CONVERT(decimal(18,2),ROUND (ISNULL((SELECT  SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "," + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + "= " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "),0) / ";
                Mi_SQL = Mi_SQL + " ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                Mi_SQL = Mi_SQL + "), 0) * 100, 2))) AS PorcentajeS ";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " JOIN " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " ON " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + " ON ";
                Mi_SQL = Mi_SQL + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID + " = " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID;
                Mi_SQL = Mi_SQL + " WHERE YEAR(" + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Fecha_Creo + ") = " + Negocio.P_Anio;
                if (!String.IsNullOrEmpty(Negocio.P_Grupo_Dependencia_ID))
                { Mi_SQL = Mi_SQL + " AND " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + " = '" + Negocio.P_Grupo_Dependencia_ID + "'"; }
                if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                { Mi_SQL = Mi_SQL + " AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID + "'"; }
                Mi_SQL = Mi_SQL + " GROUP BY " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ", ";
                Mi_SQL = Mi_SQL + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre;

                //Unir consulta 1 con consulta 2
                Mi_SQL = Mi_SQL + " UNION ALL ";

                //Asignar consulta 2
                Mi_SQL = Mi_SQL + " SELECT DISTINCT '' AS Gerencia, 'Totales' AS Unidad_Responsable, ";
                Mi_SQL = Mi_SQL + "  ISNULL(( SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 1 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " ),0)AS Ene,";
                Mi_SQL = Mi_SQL + "  ISNULL(( SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 2 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " ),0)AS Feb,";
                Mi_SQL = Mi_SQL + "  ISNULL(( SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 3 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " ),0)AS Mar,";
                Mi_SQL = Mi_SQL + "  ISNULL(( SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 4 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " ),0)AS Abr,";
                Mi_SQL = Mi_SQL + "  ISNULL(( SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 5 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " ),0)AS May,";
                Mi_SQL = Mi_SQL + "  ISNULL(( SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 6 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " ),0)AS Jun,";
                Mi_SQL = Mi_SQL + "  ISNULL(( SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 7 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " ),0)AS Jul,";
                Mi_SQL = Mi_SQL + "  ISNULL(( SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 8 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " ),0)AS Ags,";
                Mi_SQL = Mi_SQL + "  ISNULL(( SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 9 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " ),0)AS Sep,";
                Mi_SQL = Mi_SQL + "  ISNULL(( SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 10 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " ),0)AS Oct,";
                Mi_SQL = Mi_SQL + "  ISNULL(( SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 11 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " ),0)AS Nov,";
                Mi_SQL = Mi_SQL + "  ISNULL(( SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = 12 AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " ),0)AS Dic,";
                Mi_SQL = Mi_SQL + "  ISNULL(( SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " ),0)AS Total,";
                Mi_SQL = Mi_SQL + " 100.00 as PorcentajeS ";
                Mi_SQL = Mi_SQL + " FROM " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE YEAR(" + Alm_Com_Salidas.Campo_Fecha_Creo + ") = " + Negocio.P_Anio;
                                   

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DEL METODO : Consulta_Dependencias
        /// DESCRIPCION       : Consulta la cantidad total del prodcuto entregado en cierto mes,
        ///                     El costo monetario de ese producto x la cantidad consumida en ese mes sin iva 
        ///                     la cantidad de ese producto entregado hasta el mes seleccionado y 
        ///                     la diferencia de consumo entre el mes actual y el mas anterior.
        /// PARAMETROS        : Datos: Variable que se usa para cominicarse con la clase de negocios        
        /// CREO              : Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO        : 15/Octubre/2012 10:27am
        /// MODIFICO          :     
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION:     
        ///*******************************************************************************/
        public static DataTable Consulta_Detalle_Salida_Mes_Especifico(Cls_Rpt_Alm_Consumo_Stock_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            try
            {
                Int32 Mes_Anterior = 0;
                Mes_Anterior = Convert.ToInt32(Datos.P_Mes) - 1;
                //Asignar consulta 
                Mi_SQL = "SELECT " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + " AS GERENCIA, "; 
                Mi_SQL = Mi_SQL + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS UNIDAD_RESPONSABLE, ";
                Mi_SQL = Mi_SQL + "ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ")= " + Datos.P_Mes;
                    Mi_SQL = Mi_SQL + " AND YEAR(" + Alm_Com_Salidas.Campo_Fecha_Creo + ")= " + Datos.P_Anio;
                    Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Datos.P_Producto_ID + "'";
                    Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                    Mi_SQL = Mi_SQL + "),0) AS 'Entregados en " + Datos.P_Nombre_Mes + "', ";
                Mi_SQL = Mi_SQL + "ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Costo + " * " + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ")= " + Datos.P_Mes;
                    Mi_SQL = Mi_SQL + " AND YEAR(" + Alm_Com_Salidas.Campo_Fecha_Creo + ")= " + Datos.P_Anio;
                    Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Datos.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                    Mi_SQL = Mi_SQL + "),0) AS 'Costo de " + Datos.P_Nombre_Mes + "', ";
                Mi_SQL = Mi_SQL + "ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE "+ Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ")<= " + Datos.P_Mes;
                    Mi_SQL = Mi_SQL + " AND YEAR(" + Alm_Com_Salidas.Campo_Fecha_Creo + ")= " + Datos.P_Anio;
                    Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + "= '" + Datos.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                    Mi_SQL = Mi_SQL + "),0) AS 'Entregados hasta " + Datos.P_Nombre_Mes + "', ";
                Mi_SQL = Mi_SQL + "(ISNULL((SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ")= " + Datos.P_Mes;
                    Mi_SQL = Mi_SQL + " AND YEAR(" + Alm_Com_Salidas.Campo_Fecha_Creo + ")= " + Datos.P_Anio;
                    Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Datos.P_Producto_ID + "' ";
                    Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                    Mi_SQL = Mi_SQL + "),0) - ISNULL((";
                    Mi_SQL = Mi_SQL + " SELECT SUM(" + Alm_Com_Salidas_Detalles.Campo_Cantidad + ") FROM ";
                    Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + ", " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                    Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                    if (Mes_Anterior < 1)
                    {
                        Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ")= 12";
                        Mi_SQL = Mi_SQL + " AND YEAR(" + Alm_Com_Salidas.Campo_Fecha_Creo + ")= " + (Convert.ToInt32(Datos.P_Anio) - 1);
                    }
                    else 
                    {
                        Mi_SQL = Mi_SQL + " AND MONTH(" + Alm_Com_Salidas.Campo_Fecha_Creo + ")= " + Mes_Anterior;
                        Mi_SQL = Mi_SQL + " AND YEAR(" + Alm_Com_Salidas.Campo_Fecha_Creo + ")= " + Datos.P_Anio;
                    }
                    Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = '" + Datos.P_Producto_ID + "'";
                    Mi_SQL = Mi_SQL + " AND " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                    Mi_SQL = Mi_SQL + "),0)) AS DIFERENCIA, "+ Datos.P_Anio+" AS 'AÑO'";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " JOIN " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " ON " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias;
                Mi_SQL = Mi_SQL + " ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID + " = " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID;
                if (!String.IsNullOrEmpty(Datos.P_Grupo_Dependencia_ID)) 
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + " = '" + Datos.P_Grupo_Dependencia_ID + "' ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Dependencia_ID)) 
                {
                    Mi_SQL = Mi_SQL + " AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + "= '" + Datos.P_Dependencia_ID + "' ";
                } 
                Mi_SQL = Mi_SQL + " GROUP BY " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ", ";
                Mi_SQL = Mi_SQL + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre;
                //Ordenar
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre;
                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        
        #endregion
    }
}
