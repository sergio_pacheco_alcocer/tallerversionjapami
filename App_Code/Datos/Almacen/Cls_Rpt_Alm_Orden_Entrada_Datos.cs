﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Rpt_Alm_Orden_Entrada.Negocio;

namespace JAPAMI.Rpt_Alm_Orden_Entrada.Datos
{
    public class Cls_Rpt_Alm_Orden_Entrada_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Ordenes_Entrada
        ///DESCRIPCIÓN         : Realiza la consulta en la Base de Datos para las Ordenes
        ///                      de Entrada.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        public static DataTable Consultar_Ordenes_Entrada(Cls_Rpt_Alm_Orden_Entrada_Negocio Negocio)
        {
            String Mi_SQL = String.Empty;
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT CONVERT(INT, ENTRADAS." + Alm_Com_Entradas.Campo_No_Entrada;
                Mi_SQL += ") AS NO_ENTRADA, REQ." + Ope_Com_Requisiciones.Campo_No_Orden_Compra;
                Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_Folio;
                Mi_SQL += ", ENTRADAS." + Alm_Com_Entradas.Campo_Fecha;
                Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_Usuario_Creo;
                Mi_SQL += ", ENTRADAS." + Alm_Com_Entradas.Campo_Total;
                Mi_SQL += " FROM " + Alm_Com_Entradas.Tabla_Alm_Com_Entradas + " ENTRADAS";

                Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ";
                Mi_SQL += " ON ENTRADAS." + Alm_Com_Entradas.Campo_No_Requisicion + " = REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS";
                Mi_SQL += " ON ENTRADAS." + Alm_Com_Entradas.Campo_Empleado_Almacen_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID;

                if (!String.IsNullOrEmpty(Negocio.P_Orden_Entrada))
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += "ENTRADAS." + Alm_Com_Entradas.Campo_No_Entrada + " = " + Negocio.P_Orden_Entrada;
                }
                if (!String.IsNullOrEmpty(Negocio.P_Requisicion_ID))
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += "ENTRADAS." + Alm_Com_Entradas.Campo_No_Requisicion + " = " + Negocio.P_Requisicion_ID;
                }
                if (Negocio.P_Fecha_Inicio.Year > 1999 && Negocio.P_Fecha_Fin.Year > 1999)
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += "ENTRADAS." + Alm_Com_Entradas.Campo_Fecha + " BETWEEN CONVERT(DATETIME, '" + String.Format("{0:dd/MM/yyyy}", Negocio.P_Fecha_Inicio) + "  00:00:00',103) ";
                    Mi_SQL += " AND CONVERT(DATETIME, '" + String.Format("{0:dd/MM/yyyy}", Negocio.P_Fecha_Fin) + "  23:59:59',103) ";
                }
                if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += "REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID + " = " + Negocio.P_Dependencia_ID;
                }
                if (!String.IsNullOrEmpty(Negocio.P_Orden_Compra))
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += "REQ." + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " = " + Negocio.P_Orden_Compra;
                }
                Mi_SQL += " ORDER BY " + Alm_Com_Entradas.Campo_No_Entrada + " DESC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Detalles_Orden_Entrada
        ///DESCRIPCIÓN         : Consulta los de talles de una orden de compra.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        public static DataSet Detalles_Orden_Entrada(Cls_Rpt_Alm_Orden_Entrada_Negocio Negocio)
        {
            String Mi_SQL = String.Empty;
            DataSet Ds_Orden_Entrada = new DataSet();
            Int64 No_Orden_Compra = new Int64();
            Object Obj = new Object();
            String Facturas = String.Empty;

            try
            {
                Mi_SQL = "SELECT REQ." + Ope_Com_Requisiciones.Campo_No_Orden_Compra;
                Mi_SQL += " FROM " + Alm_Com_Entradas.Tabla_Alm_Com_Entradas;
                Mi_SQL += " ENTRADAS LEFT OUTER JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " REQ ON REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = ENTRADAS." + Alm_Com_Entradas.Campo_No_Requisicion;
                Mi_SQL += " WHERE ENTRADAS." + Alm_Com_Entradas.Campo_No_Entrada + " = " + Negocio.P_Orden_Entrada;
                Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                Int64.TryParse(Obj.ToString(), out No_Orden_Compra);

                Mi_SQL = "SELECT CONVERT(INT, ENTRADAS." + Alm_Com_Entradas.Campo_No_Entrada;
                Mi_SQL += ") AS NO_ENTRADA, REQ." + Ope_Com_Requisiciones.Campo_No_Orden_Compra;
                Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_Folio;
                Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_Usuario_Creo;
                Mi_SQL += " AS SOLICITO, REQ." + Ope_Com_Requisiciones.Campo_Fecha_Creo;
                Mi_SQL += " AS FECHA_SOLICITO, ENTRADAS." + Alm_Com_Entradas.Campo_Usuario_Creo;
                Mi_SQL += " AS RECIBIO, ENTRADAS." + Alm_Com_Entradas.Campo_Fecha;
                Mi_SQL += " AS FECHA_RECIBIO, ENTRADAS." + Alm_Com_Entradas.Campo_Total;
                Mi_SQL += ", ENTRADAS." + Alm_Com_Entradas.Campo_Subtotal;
                Mi_SQL += ", ENTRADAS." + Alm_Com_Entradas.Campo_IVA;
                Mi_SQL += ", ENTRADAS." + Alm_Com_Entradas.Campo_Estatus;
                Mi_SQL += ", COMENT." + Alm_Com_Entradas_Comentarios.Campo_Comentario;
                Mi_SQL += " FROM " + Alm_Com_Entradas.Tabla_Alm_Com_Entradas;
                Mi_SQL += " ENTRADAS LEFT OUTER JOIN " + Alm_Com_Entradas_Comentarios.Tabla_Alm_Com_Entradas_Comentarios;
                Mi_SQL += " COMENT ON COMENT." + Alm_Com_Entradas_Comentarios.Campo_No_Entrada + " = ENTRADAS." + Alm_Com_Entradas.Campo_No_Entrada;
                Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " REQ ON REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = ENTRADAS." + Alm_Com_Entradas.Campo_No_Requisicion;
                Mi_SQL += " WHERE ENTRADAS." + Alm_Com_Entradas.Campo_No_Entrada + " = " + Negocio.P_Orden_Entrada;

                Mi_SQL += " SELECT DET." + Alm_Com_Entradas_Detalles.Campo_Producto_ID;
                Mi_SQL += ", PROD." + Cat_Com_Productos.Campo_Nombre;
                Mi_SQL += ", UNID." + Cat_Com_Unidades.Campo_Abreviatura;
                Mi_SQL += ", DET." + Alm_Com_Entradas_Detalles.Campo_Cantidad;
                Mi_SQL += ", DET." + Alm_Com_Entradas_Detalles.Campo_Importe;
                Mi_SQL += ", DET." + Alm_Com_Entradas_Detalles.Campo_Precio_U_SIN_IVA;
                Mi_SQL += ", PROD." + Cat_Com_Productos.Campo_Clave;
                Mi_SQL += " FROM " + Alm_Com_Entradas_Detalles.Tabla_Alm_Com_Entradas_Detalles;
                Mi_SQL += " DET LEFT OUTER JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL += " PROD ON PROD." + Cat_Com_Productos.Campo_Producto_ID + " = DET." + Alm_Com_Entradas_Detalles.Campo_Producto_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades;
                Mi_SQL += " UNID ON UNID." + Cat_Com_Unidades.Campo_Unidad_ID + " = PROD." + Cat_Com_Productos.Campo_Unidad_ID;                
                Mi_SQL += " WHERE " + Alm_Com_Entradas_Detalles.Campo_No_Entrada + " = " + Negocio.P_Orden_Entrada;

                Mi_SQL += " SELECT * FROM " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas;
                Mi_SQL += " WHERE " + Ope_Alm_Registro_Facturas.Campo_No_Orden_Compra + " = " + No_Orden_Compra.ToString();
                Mi_SQL += " AND " + Ope_Alm_Registro_Facturas.Campo_No_Entrada + " = " + Negocio.P_Orden_Entrada;

                Ds_Orden_Entrada = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                if (Ds_Orden_Entrada.Tables[2].Rows.Count > 0)
                {
                    //En caso que la factura ya tenga la relacion con el no_entrada
                    Facturas = Ds_Orden_Entrada.Tables[2].Rows[Ds_Orden_Entrada.Tables[2].Rows.Count - 1][Ope_Alm_Registro_Facturas.Campo_Factura_Proveedor].ToString().Trim();
                    
                }
                else 
                {
                    Mi_SQL += " SELECT * FROM " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas;
                    Mi_SQL += " WHERE " + Ope_Alm_Registro_Facturas.Campo_No_Orden_Compra + " = " + No_Orden_Compra.ToString();
                    Ds_Orden_Entrada = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    foreach (DataRow Dr_Row in Ds_Orden_Entrada.Tables[3].Rows)
                    {
                        Facturas += Dr_Row[Ope_Alm_Registro_Facturas.Campo_Factura_Proveedor].ToString() + ", ";
                    }
                    Int16 Ultimo = (Int16)Facturas.LastIndexOf(",");
                    Facturas = Facturas.Substring(0, Ultimo);
                }


                Ds_Orden_Entrada.Tables[0].Columns.Add("FACTURAS", typeof(System.String));
                if (Ds_Orden_Entrada.Tables[0].Rows.Count > 0)
                {
                    Ds_Orden_Entrada.Tables[0].Rows[0]["FACTURAS"] = Facturas;
                }

                Ds_Orden_Entrada.Tables[0].TableName = "Cabecera";
                Ds_Orden_Entrada.Tables[1].TableName = "Detalles";
                Ds_Orden_Entrada.Tables[2].TableName = "Facturas";
                Ds_Orden_Entrada.AcceptChanges();

                return Ds_Orden_Entrada;
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
        }
    }
}
