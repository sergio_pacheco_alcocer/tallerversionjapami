﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Parametros_Almacen_Cuentas.Negocio;

namespace JAPAMI.Parametros_Almacen_Cuentas.Datos
{
    public class Cls_Cat_Alm_Parametros_Cuentas_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Cuentas
        ///DESCRIPCIÓN          : Obtiene registros de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 1.Parametros. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Cuentas(Cls_Cat_Alm_Parametros_Cuentas_Negocio Parametros)
        {
            //StringBuilder Mi_SQL = new StringBuilder();
            DataSet Ds_Datos = new DataSet();
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Da_SQL = new SqlDataAdapter();
            DataSet Ds_SQL = new DataSet();
            DataTable Dt_Psp = new DataTable();
            if (Parametros.P_Cmmd != null)
            {
                Cmmd = Parametros.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("SELECT * FROM " + Cat_Alm_Parametros_Cuentas.Tabla_Cat_Alm_Parametros_Cuentas);
                if (!String.IsNullOrEmpty(Parametros.P_Cta_Cont_Alm_General_ID))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_General_ID + " = '" + Parametros.P_Cta_Cont_Alm_General_ID + "'");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Cta_Cont_Alm_Papeleria_ID))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_Papeleria_ID + " = '" + Parametros.P_Cta_Cont_Alm_Papeleria_ID + "'");
                }
                if (!String.IsNullOrEmpty(Mi_SQL.ToString()))
                {
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Datos = Ds_SQL.Tables[0];
                }
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Cuentas
        ///DESCRIPCIÓN          : Modifica una Cuenta en la base de datos.
        ///PARAMETROS           : Parametros: Contiene el registro que sera modificado.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 12/Enero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Modificar_Cuentas(Cls_Cat_Alm_Parametros_Cuentas_Negocio Parametros)
        {
            StringBuilder Mi_SQL;

            try
            {
                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("DELETE FROM " + Cat_Alm_Parametros_Cuentas.Tabla_Cat_Alm_Parametros_Cuentas);
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("INSERT INTO " + Cat_Alm_Parametros_Cuentas.Tabla_Cat_Alm_Parametros_Cuentas);
                Mi_SQL.Append(" (" + Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_General_ID);
                Mi_SQL.Append(", " + Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_Papeleria_ID);
                Mi_SQL.Append(", " + Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Pendiente);
                Mi_SQL.Append(", " + Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Acreditable);
                Mi_SQL.Append(", " + Cat_Alm_Parametros_Cuentas.Campo_Fecha_Creo);
                Mi_SQL.Append(", " + Cat_Alm_Parametros_Cuentas.Campo_Usuario_Creo);
                Mi_SQL.Append(") VALUES (");
                Mi_SQL.Append(" '" + Parametros.P_Cta_Cont_Alm_General_ID);
                Mi_SQL.Append("', '" + Parametros.P_Cta_Cont_Alm_Papeleria_ID);
                Mi_SQL.Append("', '" + Parametros.P_Cta_IVA_Pendiente);
                Mi_SQL.Append("', '" + Parametros.P_Cta_IVA_Acreditable);
                Mi_SQL.Append("', GETDATE()");
                Mi_SQL.Append(" , '" + Cls_Sessiones.Nombre_Empleado + "')");
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar actualizar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Parametros_Poliza
        ///DESCRIPCIÓN          : consulta para obtener los parametros para la poliza de nomina
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 20/Abril/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static DataTable Obtener_Parametros_Poliza(Cls_Cat_Alm_Parametros_Cuentas_Negocio Parametros)
        {
            StringBuilder Mi_Sql = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Da_SQL = new SqlDataAdapter();
            DataSet Ds_SQL = new DataSet(); 
            DataTable Dt_Datos = new DataTable();
            DataTable Dt_Psp = new DataTable();
            if (Parametros.P_Cmmd != null)
            {
                Cmmd = Parametros.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_Sql.Append("SELECT * FROM " + Cat_Nom_Parametros_Poliza.Tabla_Cat_Nom_Parametros_Poliza);
                Cmmd.CommandText = Mi_Sql.ToString().Trim();
                Da_SQL.SelectCommand = Cmmd;
                Da_SQL.Fill(Ds_SQL);
                Dt_Datos = Ds_SQL.Tables[0];
                //Da_Parametros = new SqlDataAdapter(Comando)
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
            return Dt_Datos;
        }

    }
}
