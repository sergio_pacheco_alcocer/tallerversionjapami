﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Cancelar_Salidas.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;
using System.Data.SqlClient;
using JAPAMI.Polizas.Negocios;

namespace JAPAMI.Cancelar_Salidas.Datos
{
    public class Cls_Ope_Alm_Cancelar_Salidas_Datos
    {
        public Cls_Ope_Alm_Cancelar_Salidas_Datos()
        {

        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Salidas
        ///DESCRIPCIÓN: Consulta las salidas del almacen
        ///PARAMETROS:  1.- Cls_Ope_Alm_Cancelar_Salidas_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  26/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Salidas(Cls_Ope_Alm_Cancelar_Salidas_Negocio Clase_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                //Creamos la consulta
                Mi_SQL = "SELECT DISTINCT O_SALIDA." + Alm_Com_Salidas.Campo_No_Salida + " as NO_ORDEN_SALIDA ";                
                Mi_SQL += ",REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + "";
                Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Estatus + "";
                Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Tipo + " as TIPO_REQUISICION";
                Mi_SQL += ", DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " as DEPENDENCIA";
                Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Codigo_Programatico + "";
                Mi_SQL += ", O_SALIDA." + Alm_Com_Salidas.Campo_Usuario_Creo + " as EMPLEADO_SURTIO";
                Mi_SQL += ", O_SALIDA." + Alm_Com_Salidas.Campo_Fecha_Creo + " as FECHA_SURTIDO";
                Mi_SQL += ", O_SALIDA." + Alm_Com_Salidas.Campo_Subtotal + "";
                Mi_SQL += ", O_SALIDA." + Alm_Com_Salidas.Campo_IVA + "";
                Mi_SQL += ", O_SALIDA." + Alm_Com_Salidas.Campo_Total + "";
                Mi_SQL += ", O_SALIDA. " + Alm_Com_Salidas.Campo_Estatus + " AS ESTATUS_SALIDA ";
                Mi_SQL += " FROM " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + " O_SALIDA";

                Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICIONES";
                Mi_SQL += " ON O_SALIDA." + Alm_Com_Salidas.Campo_Requisicion_ID;
                Mi_SQL += " = REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID;


                Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
                Mi_SQL += " ON REQUISICIONES." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
                Mi_SQL += " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID;



                Mi_SQL += " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS";
                Mi_SQL += " ON REQUISICIONES." + Ope_Com_Requisiciones.Campo_Empleado_Surtido_ID;
                Mi_SQL += " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID;


                Mi_SQL += " LEFT OUTER JOIN " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + " SALIDA_DETALLES";
                Mi_SQL += " ON SALIDA_DETALLES." + Alm_Com_Salidas_Detalles.Campo_No_Salida;
                Mi_SQL += " = O_SALIDA." + Alm_Com_Salidas.Campo_No_Salida;

                Mi_SQL += " WHERE O_SALIDA." + Alm_Com_Salidas.Campo_Estatus + " = 'GENERADA'";

                //Validamos los filtros por No de Contrarecibo
                if (!String.IsNullOrEmpty(Clase_Negocio.P_No_Salida))
                {
                    Mi_SQL += " AND  O_SALIDA." + Alm_Com_Salidas.Campo_No_Salida + " like '%" + Clase_Negocio.P_No_Salida + "%'";
                }
                //Validamos los filtros por fechas
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Fecha_Inicio) && !String.IsNullOrEmpty(Clase_Negocio.P_Fecha_Fin))
                {
                    Clase_Negocio.P_Fecha_Inicio = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Clase_Negocio.P_Fecha_Inicio));
                    Clase_Negocio.P_Fecha_Fin = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Clase_Negocio.P_Fecha_Fin));

                    Mi_SQL += " AND ((O_SALIDA." + Alm_Com_Salidas.Campo_Fecha + " BETWEEN '" + Clase_Negocio.P_Fecha_Inicio + " 00:00:00' AND ";
                    Mi_SQL += " '" + Clase_Negocio.P_Fecha_Fin + " 23:59:00') ";
                    Mi_SQL += " OR (O_SALIDA." + Alm_Com_Salidas.Campo_Fecha_Creo + " BETWEEN '" + Clase_Negocio.P_Fecha_Inicio + " 00:00:00' AND ";
                    Mi_SQL += " '" + Clase_Negocio.P_Fecha_Fin + " 23:59:00'))";
                }
                Mi_SQL += " ORDER BY O_SALIDA." + Alm_Com_Salidas.Campo_No_Salida;
                //regresamos los valores
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las entradas. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Salidas_Detalles
        ///DESCRIPCIÓN: Consulta los detalles de la salida
        ///PARAMETROS:  1.- Cls_Ope_Alm_Cancelar_Salidas_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  26/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Salidas_Detalles(Cls_Ope_Alm_Cancelar_Salidas_Negocio Clase_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                //Creamos la consulta
                Mi_SQL = "SELECT DETALLES.*, " ;
                Mi_SQL = Mi_SQL + "(SELECT " + Cat_Com_Productos.Campo_Nombre + " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Productos.Campo_Producto_ID + "= DETALLES." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + ") AS PRODUCTO,";
                Mi_SQL = Mi_SQL + "(SELECT " + Cat_Com_Productos.Campo_Descripcion + " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Productos.Campo_Producto_ID + "= DETALLES." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + ") AS DESCRIPCION,";
                Mi_SQL = Mi_SQL + "(SELECT " + Cat_Com_Unidades.Campo_Abreviatura + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID + "= ";
                Mi_SQL = Mi_SQL + "(SELECT " + Cat_Com_Productos.Campo_Unidad_ID + " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Productos.Campo_Producto_ID + "= DETALLES." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + ")) AS UNIDAD,";
                Mi_SQL = Mi_SQL + "(SELECT " + Alm_Com_Salidas.Campo_Requisicion_ID + " FROM " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas.Campo_No_Salida + "= " +  Clase_Negocio.P_No_Salida + ") AS NO_REQUISICION,";
                Mi_SQL = Mi_SQL + "(SELECT " + Ope_Com_Req_Producto.Campo_Cantidad + " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + "= ";
                Mi_SQL = Mi_SQL + "(SELECT " + Alm_Com_Salidas.Campo_Requisicion_ID + " FROM " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas.Campo_No_Salida + "= " +  Clase_Negocio.P_No_Salida + ") ";
                Mi_SQL = Mi_SQL + " AND " + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = DETALLES." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + ") AS CANTIDAD_SOLICITADA";
                Mi_SQL = Mi_SQL + " FROM "  +Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + " DETALLES ";
                Mi_SQL = Mi_SQL + " WHERE " + Alm_Com_Salidas_Detalles.Campo_No_Salida + "= " + Clase_Negocio.P_No_Salida;
                //regresamos los valores
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los detalles de entrada. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cancelar_Salidas
        ///DESCRIPCIÓN          : Cancela las salidas del almacen
        ///PARAMETROS           :  
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 26/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Cancelar_Salidas(Cls_Ope_Alm_Cancelar_Salidas_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            Double Total = 0.0;
            String Requisicion_ID = "";
            String No_Orden_Compra = "";
            String Tipo = String.Empty;
            String Estatus = String.Empty;

            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();

            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            String Mi_SQL = "";
            DataTable Dt_Req = new DataTable();
            try
            {
                if (Clase_Negocio.P_No_Requisicion != String.Empty)
                {
                    //Consultamos la requisicion
                    Mi_SQL = "SELECT * FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Clase_Negocio.P_No_Requisicion;
                    Dt_Req = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }             
                //Actualizamos la salida
                Mi_SQL = "UPDATE " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                Mi_SQL += " SET " + Alm_Com_Salidas.Campo_Estatus + " = '" + Clase_Negocio.P_Estatus + "', ";
                Mi_SQL += Alm_Com_Salidas.Campo_Observaciones + " = '" + Clase_Negocio.P_Comentario + "', ";
                Mi_SQL += Alm_Com_Salidas.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "', ";
                Mi_SQL += Alm_Com_Salidas.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL += "WHERE " + Alm_Com_Salidas.Campo_No_Salida + " = " + Clase_Negocio.P_No_Salida;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Validamos que no venga en null la tabla de requisicion
                if (Dt_Req != null && Dt_Req.Rows.Count > 0)
                {
                    Requisicion_ID = Dt_Req.Rows[0][Ope_Com_Requisiciones.Campo_Requisicion_ID].ToString().Trim();
                    No_Orden_Compra = Dt_Req.Rows[0][Ope_Com_Requisiciones.Campo_No_Orden_Compra].ToString().Trim();
                    Tipo = Dt_Req.Rows[0][Ope_Com_Requisiciones.Campo_Tipo].ToString().Trim();
                    Estatus = Dt_Req.Rows[0][Ope_Com_Requisiciones.Campo_Estatus].ToString().Trim();
                }

                //ACTUALIZAMOS OC SU ESTATUS
                if (Tipo == "STOCK")
                {
                    Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                    Mi_SQL += " SET " + Ope_Com_Requisiciones.Campo_Estatus;
                    Mi_SQL += " = 'ALMACEN' ";
                    Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Requisicion_ID;
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                if ((Tipo == "TRANSITORIA"))
                {
                    // Si el estatus es diferente de surtida parcial lo modificamos
                    // en caso de que sea igual lo conservamos para poder seguir haciendo entradas.
                    if (Estatus != "SURTIDA_PARCIAL")
                    {
                        Mi_SQL = "UPDATE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra;
                        Mi_SQL += " SET " + Ope_Com_Ordenes_Compra.Campo_Estatus;
                        Mi_SQL += " = 'SURTIDA' ";
                        Mi_SQL += " WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = " + No_Orden_Compra;
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                        Mi_SQL += " SET " + Ope_Com_Requisiciones.Campo_Estatus;
                        Mi_SQL += " = 'SURTIDA' ";
                        Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Requisicion_ID;
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                }



                //Validamos que tenga productos el detalles
                if (Clase_Negocio.P_Dt_Detalles != null && Clase_Negocio.P_Dt_Detalles.Rows.Count > 0)
                {
                    foreach (DataRow Dr_Renglon in Clase_Negocio.P_Dt_Detalles.Rows)
                    {
                        Total += Double.Parse(Dr_Renglon["IMPORTE"].ToString().Trim());

                        Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " SET ";
                        Mi_SQL += Cat_Com_Productos.Campo_Existencia + " = " + Cat_Com_Productos.Campo_Existencia + " + " + Dr_Renglon["CANTIDAD"].ToString() + ", ";
                        if (Tipo == "STOCK")
                        {
                            Mi_SQL += Cat_Com_Productos.Campo_Comprometido + " = " + Cat_Com_Productos.Campo_Comprometido + " + " + Dr_Renglon["CANTIDAD"].ToString() + ", ";
                        }
                        Mi_SQL += Cat_Com_Productos.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "', ";
                        Mi_SQL += Cat_Com_Productos.Campo_Fecha_Modifico + " = GETDATE() ";
                        Mi_SQL += "WHERE " + Cat_Com_Productos.Campo_Producto_ID + " = '" + Dr_Renglon[Alm_Com_Entradas_Detalles.Campo_Producto_ID].ToString() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        if (Requisicion_ID != String.Empty)
                        {
                            Mi_SQL = "UPDATE " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " SET ";
                            Mi_SQL += Ope_Com_Req_Producto.Campo_Cantidad_Entregada + " = " + Ope_Com_Req_Producto.Campo_Cantidad_Entregada + " - " + Dr_Renglon["CANTIDAD"].ToString();
                            Mi_SQL += " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + Requisicion_ID;
                            Mi_SQL += " AND " + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = '" + Dr_Renglon[Alm_Com_Entradas_Detalles.Campo_Producto_ID].ToString() + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                        }
                    }
                }


                String Alta_poliza = Alta_Poliza(Clase_Negocio.P_No_Salida, No_Orden_Compra, Requisicion_ID, Total, Cmd);
                if (Alta_poliza != "Alta_Exitosa")
                {
                    throw new Exception(Alta_poliza);
                }

                Trans.Commit();

                Mensaje_Error = Clase_Negocio.P_No_Salida;
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo cancelar la entrada";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }

        
        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Alta_Poliza
        /// DESCRIPCION:            Método utilizado consultar la información utilizada para mostrar la orden de salida
        /// PARAMETROS :            
        /// CREO       :            Salvador Hernández Ramírez
        /// FECHA_CREO :            24/Junio/2011  
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        private static String Alta_Poliza(String No_Salida, String No_Orden_Compra, String No_Requisicion, Double Monto_Total, SqlCommand P_Cmd)
        {
            SqlDataAdapter Da = new SqlDataAdapter();
            DataTable Dt_Auxiliar = new DataTable();
            DataTable Dt_Cuentas_Almacen = new DataTable();
            DataTable Dt_Cuenta = new DataTable();
            String Mensaje = "";
            String Mi_SQL = "";
            String Tipo_Poliza_ID = "";
            Boolean Almacen_General = true;

            Mi_SQL = "SELECT * FROM " + Cat_Alm_Parametros_Cuentas.Tabla_Cat_Alm_Parametros_Cuentas;
            P_Cmd.CommandText = Mi_SQL;
            Da.SelectCommand = P_Cmd;
            Dt_Cuentas_Almacen = new DataTable();
            Da.Fill(Dt_Cuentas_Almacen);

            if (No_Requisicion != String.Empty)
            {
                Mi_SQL = "SELECT DISTINCT(" + Ope_Com_Req_Producto.Campo_Tipo + ") FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                Mi_SQL += " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion;
                P_Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = P_Cmd;
                Dt_Auxiliar = new DataTable();
                Da.Fill(Dt_Auxiliar);
            }


            if (Dt_Auxiliar.Rows.Count == 1 && Dt_Auxiliar.Rows[0][Ope_Com_Req_Producto.Campo_Tipo].ToString().Trim() == "PRODUCTO")
            {
                Mi_SQL = "SELECT DISTINCT(PROD." + Cat_Com_Productos.Campo_Almacen_General;
                Mi_SQL += ") FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PROD";
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PROD";
                Mi_SQL += " ON REQ_PROD." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = PROD." + Cat_Com_Productos.Campo_Producto_ID;
                Mi_SQL += " WHERE REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion;
                P_Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = P_Cmd;
                Dt_Auxiliar = new DataTable();
                Da.Fill(Dt_Auxiliar);

                if (Dt_Auxiliar.Rows.Count == 1 && Dt_Auxiliar.Rows[0][Cat_Com_Productos.Campo_Almacen_General].ToString().Trim() == "NO")
                {
                    Almacen_General = false;
                }
            }
            else
            {
                Mi_SQL = "SELECT DISTINCT(" + Cat_Com_Productos.Campo_Almacen_General;
                Mi_SQL += ") FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL += " WHERE " + Cat_Com_Productos.Campo_Producto_ID;
                Mi_SQL += " IN (SELECT " + Alm_Com_Salidas_Detalles.Campo_Producto_ID;
                Mi_SQL += " FROM " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles;
                Mi_SQL += " WHERE " + Alm_Com_Salidas_Detalles.Campo_No_Salida + "=" + No_Salida + ")";
                P_Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = P_Cmd;
                Dt_Auxiliar = new DataTable();
                Da.Fill(Dt_Auxiliar);

                if (Dt_Auxiliar.Rows.Count == 1 && Dt_Auxiliar.Rows[0][Cat_Com_Productos.Campo_Almacen_General].ToString().Trim() == "NO")
                {
                    Almacen_General = false;
                }
            }

            Mi_SQL = "SELECT " + Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID;
            Mi_SQL += " FROM " + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas;
            Mi_SQL += " WHERE " + Cat_Con_Tipo_Polizas.Campo_Descripcion + " LIKE '%DIARIO%'";
            P_Cmd.CommandText = Mi_SQL;
            Tipo_Poliza_ID = P_Cmd.ExecuteScalar().ToString().Trim();

            DataTable Dt_Detalles_Poliza = new DataTable();
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("MOMENTO_FINAL", typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("Nombre_Cuenta", typeof(System.String));
            Dt_Detalles_Poliza.AcceptChanges();
            if (No_Requisicion != String.Empty)
            {
                Mi_SQL = "SELECT REQ_PROD." + Ope_Com_Req_Producto.Campo_Partida_ID;
                Mi_SQL += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID;
                Mi_SQL += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID;
                Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
                Mi_SQL += " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PROD";
                Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ";
                Mi_SQL += " ON REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID;
                Mi_SQL += " WHERE REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion;
                P_Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = P_Cmd;
                Dt_Auxiliar = new DataTable();
                Da.Fill(Dt_Auxiliar);
            }
            else
            {
                Mi_SQL = "SELECT " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas +"." + Alm_Com_Salidas.Campo_Dependencia_ID;
                Mi_SQL += ", " + Cat_Com_Productos.Tabla_Cat_Com_Productos+ "." + Cat_Com_Productos.Campo_Partida_ID;
                Mi_SQL += " FROM  " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + " " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                 Mi_SQL += " LEFT OUTER JOIN " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + " " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles;
                Mi_SQL += " ON " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_No_Salida;
                Mi_SQL += "=  "+ Alm_Com_Salidas.Tabla_Alm_Com_Salidas + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL += " ON " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Producto_ID;
                Mi_SQL += "=" + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_Producto_ID;
                Mi_SQL += " WHERE " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas  + "." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL += "=" + No_Salida;
                P_Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = P_Cmd;
                Dt_Auxiliar = new DataTable();
                Da.Fill(Dt_Auxiliar);

                Mi_SQL = "SELECT " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AS " + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID;
                Mi_SQL += ", " +Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID;
                Mi_SQL += ", " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID;
                Mi_SQL += ", "+ Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID;
                Mi_SQL += ", " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;
                Mi_SQL += " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado;
                Mi_SQL += " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID;
                Mi_SQL += "='" + Dt_Auxiliar.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID].ToString().Trim() + "'";
                Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;
                Mi_SQL += "='" + Dt_Auxiliar.Rows[0][Cat_Com_Productos.Campo_Partida_ID].ToString().Trim() + "'";
                Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio;
                Mi_SQL += "= YEAR(GETDATE())";
                P_Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = P_Cmd;
                Dt_Auxiliar = new DataTable();
                Da.Fill(Dt_Auxiliar);
            }

            if (Dt_Auxiliar.Rows.Count > 0)
            {
                Mi_SQL = "SELECT PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID;
                Mi_SQL += ", FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS CLAVE_FTE_FINANCIAMIENTO";
                Mi_SQL += ", FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FTE_FINANCIAMIENTO";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID;
                Mi_SQL += ", AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Clave + " AS CLAVE_AREA_FUNCIONAL";
                Mi_SQL += ", AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS AREA_FUNCIONAL";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID;
                Mi_SQL += ", PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS CLAVE_PROYECTO_PROGRAMA";
                Mi_SQL += ", PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS PROYECTO_PROGRAMA";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID;
                Mi_SQL += ", DEP." + Cat_Dependencias.Campo_Clave + " AS CLAVE_DEPENDENCIA";
                Mi_SQL += ", DEP." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;
                Mi_SQL += ", PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS CLAVE_PARTIDA";
                Mi_SQL += ", PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA";
                Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID;
                Mi_SQL += ", CUENTAS." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " AS CUENTA";
                Mi_SQL += " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTOS";

                Mi_SQL += " LEFT OUTER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FTE_FIN";
                Mi_SQL += " ON FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " AREA_FUN";
                Mi_SQL += " ON AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " PROY_PROG";
                Mi_SQL += " ON PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP";
                Mi_SQL += " ON DEP." + Cat_Dependencias.Campo_Dependencia_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDAS";
                Mi_SQL += " ON PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTAS";
                Mi_SQL += " ON CUENTAS." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID;
                Mi_SQL += " WHERE PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dt_Auxiliar.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim() + "'";
                Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dt_Auxiliar.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim() + "'";
                Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dt_Auxiliar.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim() + "'";
                Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dt_Auxiliar.Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString().Trim() + "'";
                Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = YEAR(GETDATE())";
                P_Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = P_Cmd;
                Dt_Cuenta = new DataTable();
                Da.Fill(Dt_Cuenta);
            }


            if (Dt_Cuenta.Rows.Count > 0 && !String.IsNullOrEmpty(Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID].ToString().Trim()))
            {
                DataRow row = Dt_Detalles_Poliza.NewRow();
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID].ToString().Trim();
                row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Dt_Cuenta.Rows[0]["CUENTA"].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "Cancelacion de  Salida " + No_Salida;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0.0;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Monto_Total;
                row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID].ToString().Trim();
                row["Nombre_Cuenta"] = Dt_Cuenta.Rows[0]["CUENTA"].ToString().Trim();
                Dt_Detalles_Poliza.Rows.Add(row);
                Dt_Detalles_Poliza.AcceptChanges();

                if (Dt_Cuentas_Almacen.Rows.Count > 0)
                {
                    Mi_SQL = "SELECT * FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                    Mi_SQL += " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = ";
                    Mi_SQL += Almacen_General ?
                            Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_General_ID].ToString().Trim() :
                            Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_Papeleria_ID].ToString().Trim();

                    P_Cmd.CommandText = Mi_SQL;
                    Da.SelectCommand = P_Cmd;
                    Dt_Auxiliar = new DataTable();
                    Da.Fill(Dt_Auxiliar);

                    if (Dt_Auxiliar.Rows.Count > 0)
                    {
                        row = Dt_Detalles_Poliza.NewRow();
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();
                        row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                        row["Nombre_Cuenta"] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "Cancelacion de  Salida " + No_Salida;
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = Monto_Total;
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0.0;
                        row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID].ToString().Trim();
                        Dt_Detalles_Poliza.Rows.Add(row);
                        Dt_Detalles_Poliza.AcceptChanges();


                        Cls_Ope_Con_Polizas_Negocio Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio();

                        Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = Tipo_Poliza_ID;
                        Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now);
                        Rs_Alta_Ope_Con_Polizas.P_Fecha_Poliza = DateTime.Now;
                        Rs_Alta_Ope_Con_Polizas.P_Concepto = "Cancelar Salida " + Int64.Parse(No_Salida);
                        if(No_Orden_Compra != String.Empty)
                        {
                            Rs_Alta_Ope_Con_Polizas.P_Concepto += ", OC-" + No_Orden_Compra ;
                        }
                        if(No_Requisicion != String.Empty)
                        {
                            Rs_Alta_Ope_Con_Polizas.P_Concepto += ", REQ-" + No_Requisicion.Trim();
                        }
                         Rs_Alta_Ope_Con_Polizas.P_Concepto += ", Almacen ";

                        //Concatenamos si es almacen general o papeleria
                        Rs_Alta_Ope_Con_Polizas.P_Concepto += Almacen_General ? "General " : "Papeleria ";
                        Rs_Alta_Ope_Con_Polizas.P_Total_Debe = Monto_Total; //monto total con iva de productos
                        Rs_Alta_Ope_Con_Polizas.P_Total_Haber = Monto_Total;
                        Rs_Alta_Ope_Con_Polizas.P_No_Partida = 2;
                        Rs_Alta_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                        Rs_Alta_Ope_Con_Polizas.P_Dt_Detalles_Polizas = Dt_Detalles_Poliza;
                        Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID;
                        Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Autorizo = Cls_Sessiones.Empleado_ID;
                        Rs_Alta_Ope_Con_Polizas.P_Prefijo = "";
                        Rs_Alta_Ope_Con_Polizas.P_Cmmd = P_Cmd;
                        string[] Datos_Poliza = Rs_Alta_Ope_Con_Polizas.Alta_Poliza();
                        Mensaje = "Alta_Exitosa";
                    }
                    else
                    {
                        Mensaje = "No se encontraron los datos de la cuenta contable ";
                        Mensaje += Almacen_General ? "General " : "Papeleria ";
                        Mensaje += "de Almacen.";
                    }
                }
                else
                {
                    Mensaje = "No se han asignado las cuentas contables General ó Papeleria para Almacen.";
                }
            }
            else
            {
                Mi_SQL = "SELECT " + Ope_Com_Requisiciones.Campo_Codigo_Programatico;
                Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + No_Requisicion;
                P_Cmd.CommandText = Mi_SQL;
                Mensaje = "No se encontro la cuenta contable del gasto en el presupuesto aprobado para este año. Codigo Programatico: ";
                Mensaje += P_Cmd.ExecuteScalar().ToString().Trim();
            }
            return Mensaje;
        }

    }
}
