﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Almacen.Contrarecibos.Negocio;
using JAPAMI.Solicitud_Pagos.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Alm_Contrarecibos_Datos
/// </summary>
namespace JAPAMI.Almacen.Contrarecibos.Datos
{
    public class Cls_Ope_Alm_Contrarecibos_Datos
    {
        public Cls_Ope_Alm_Contrarecibos_Datos()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region (Metodos)
        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Alta_Contrarecibo
        /// DESCRIPCION:            Guardar el contrarecibo con los datos de las facturas o modificaciones de las mismas
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            28/Enero/2013 11:12 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static string Alta_Contrarecibo(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            String Mensaje = "";
            String Mi_SQL = String.Empty;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            Object aux = new Object(); //Variable auxiliar 
            Int64 No_Contra_Recibo = 0; //variable para el numero de contrarecibo
            int Cont_Elementos = 0; //variable para el contador
            Int64 Factura_ID = 0; //variable para el ID de la factura
            String No_Solicitud_Pago = String.Empty; //variable para la solicitud de pago
            Int64 No_Reserva = 0; //variable para consultar el numero de la reserva de la orden de compra
            string Partida_ID = string.Empty; //variable para el ID de la partida
            string Fuente_Financiamiento_ID = string.Empty; //Variable para el ID de la fuente de financiamiento
            string Proyecto_Programa_ID = string.Empty; //variable para el ID del proyecto programa
            DataTable Dt_Datos_Proveedor = new DataTable(); //tabla para los datos del proveedor
            string Tipo_Solicitud_Pago_ID = string.Empty; //variable para el ID de la solicitud de pago
            double Saldo_Reserva = 0; //variable apra el saldo de la reserva
            DataTable Dt_Reserva_Detalles = new DataTable(); //tabla para los detalles de la reserva
            Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago_Negocio = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //variable para la capa de negocios de la solicitud de pago

            try
            {   
                //Consultar los datos del proveedor
                Mi_SQL = "SELECT " + Cat_Com_Proveedores.Campo_Proveedor_ID + ", " + Cat_Com_Proveedores.Campo_Nombre + ", " + Cat_Com_Proveedores.Campo_Compañia + ", " +
                    Cat_Com_Proveedores.Campo_RFC + ", " + Cat_Com_Proveedores.Campo_CURP + " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " " +
                    "WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "' ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                da.SelectCommand = Cmd;
                da.Fill(Dt_Datos_Proveedor);

                //Consulta para la reserva de la orden de compra
                Mi_SQL = "SELECT " + Ope_Com_Ordenes_Compra.Campo_No_Reserva + " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " " +
                    "WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                aux = Cmd.ExecuteScalar();

                //Colocar el numero de la reserva
                No_Reserva = Convert.ToInt64(aux);

                //Consultar el tipo de la solicitud de pago de la reserva
                Mi_SQL = "SELECT " + Ope_Psp_Reservas.Campo_Tipo_Solicitud_Pago_ID + " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " " +
                    "WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                //Ejecutar la consulta
                Cmd.CommandText = Mi_SQL;
                aux = Cmd.ExecuteScalar();

                //verificar si el dato es numo
                if (aux != null)
                {
                    if (aux != DBNull.Value)
                    {
                        Tipo_Solicitud_Pago_ID = aux.ToString().Trim();
                    }
                    else
                    {
                        Tipo_Solicitud_Pago_ID = "00004";
                    }
                }
                else
                {
                    Tipo_Solicitud_Pago_ID = "00004";
                }

                //Consultar los ID de la Fuente de financiamiento, proyecto programa, partida
                Mi_SQL = "SELECT " + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + ", " + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + ", " +
                    Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ", " + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + ", " +
                    Ope_Psp_Reservas_Detalles.Campo_Capitulo_ID + ", " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " " +
                    "FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " " +
                    "WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                da.Fill(Dt_Reserva_Detalles);

                //verificar si la consulta arrojo resultado
                if (Dt_Reserva_Detalles.Rows.Count > 0)
                {
                    //Colocar los datos de la fuente de financiamiento, proyecto programa, etc
                    Partida_ID = Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString().Trim();
                    Fuente_Financiamiento_ID = Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                    Proyecto_Programa_ID = Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                }
                else
                {
                    Trans.Rollback();
                    throw new Exception("La requisicion no contiene datos.");
                }
                
                //verificar si el proveedor tiene datos
                if (Dt_Datos_Proveedor.Rows.Count > 0)
                {
                    //Consulta para el numero de contrarecibo
                    Mi_SQL = "SELECT MAX(" + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + ") FROM " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    aux = Cmd.ExecuteScalar();

                    //Verificar si no es nulo
                    if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                    {
                        No_Contra_Recibo = Convert.ToInt64(aux) + 1;
                    }
                    else
                    {
                        No_Contra_Recibo = 1;
                    }

                    //Asignar consulta para el contrarecibo
                    Mi_SQL = "INSERT INTO " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " (" + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + ", " +
                        Ope_Alm_Contrarecibos.Campo_Empleado_Recibido_ID + ", " + Ope_Alm_Contrarecibos.Campo_Proveedor_ID + ", " +
                        Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion + ", " + Ope_Alm_Contrarecibos.Campo_Fecha_Pago + ", " + Ope_Alm_Contrarecibos.Campo_Importe_Total + ", " +
                        Ope_Alm_Contrarecibos.Campo_Usuario_Creo + ", " + Ope_Alm_Contrarecibos.Campo_Fecha_Creo + ", " + Ope_Alm_Contrarecibos.Campo_Estatus + ", " +
                        Ope_Alm_Contrarecibos.Campo_Tipo + ") VALUES(" + No_Contra_Recibo.ToString().Trim() + ", '" + Datos.P_Usuario_ID + "', '" + Datos.P_Proveedor_ID + "', " +
                        "'" + Datos.P_Fecha_Recepcion + "', '" + Datos.P_Fecha_Pago + "', " + Datos.P_Importe_Total.ToString().Trim() + ", '" + Datos.P_Usuario + "', GETDATE(), " +
                        "'GENERADO', 'COMPRAS')";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Establecer relacion de contrarecibo y orden de compra
                    Mi_SQL = " UPDATE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " SET " + Ope_Com_Ordenes_Compra.Campo_No_Factura_Interno + " = " + No_Contra_Recibo;
                    Mi_SQL += " WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.ToString().Trim();
                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Consulta para el numero de solicitud de pago
                    Mi_SQL = "SELECT MAX(" + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ") FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    aux = Cmd.ExecuteScalar();

                    //verificar si es nulo
                    if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                    {
                        //Verificar si no es -1
                        if (Convert.ToInt32(aux) > -1)
                        {
                            No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(aux) + 1);
                        }
                        else
                        {
                            No_Solicitud_Pago = "0000000001";
                        }
                    }
                    else
                    {
                        No_Solicitud_Pago = "0000000001";
                    }

                    //Asignar consulta para la solicitud de pago
                    Mi_SQL = "INSERT INTO " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " (" + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", " +
                        Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", " + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", " +
                        Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", " + Ope_Con_Solicitud_Pagos.Campo_Monto + ", " + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", " +
                        Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ", " + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", " +
                        Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + ") VALUES('" + No_Solicitud_Pago + "', '" + Tipo_Solicitud_Pago_ID + "', " + 
                        "'CONTRARECIBO NO " + No_Contra_Recibo.ToString().Trim() + "', GETDATE(), " + Datos.P_Importe_Total.ToString().Trim() + ", 'PRE-DOCUMENTADO', " +
                        "'" + Datos.P_Usuario + "', GETDATE(), " + No_Reserva.ToString().Trim() + ", '" + Datos.P_Proveedor_ID + "') ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    foreach (DataRow Dr_Renglon in Datos.P_Dt_Facturas_Eliminadas.Rows)
                    {
                        Mi_SQL = "DELETE FROM " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + " WHERE " + Ope_Alm_Registro_Facturas.Campo_Factura_ID + " = " + Dr_Renglon["FACTURA_ID"].ToString().Trim();
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        Mi_SQL = "DELETE FROM " + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + " WHERE " + Ope_Alm_Facturas_Electronicas.Campo_Factura_Id + " = " + Dr_Renglon["FACTURA_ID"].ToString().Trim();
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    //Consultar el saldo de la reserva
                    Mi_SQL = "SELECT " + Ope_Psp_Reservas.Campo_Saldo + " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " " +
                        "WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    aux = Cmd.ExecuteScalar();

                    //Asignar valor del saldo de la reserva
                    if (aux != null)
                    {
                        if (aux != DBNull.Value)
                        {
                            if (string.IsNullOrEmpty(aux.ToString().Trim()) == false)
                            {
                                Saldo_Reserva = Convert.ToDouble(aux);
                            }
                        }
                    }

                    //Recalcular el saldo de la reserva
                    Saldo_Reserva = Saldo_Reserva - Datos.P_Importe_Total;

                    //Quitarle el saldo a la reserva
                    Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " " +
                        "SET " + Ope_Psp_Reservas.Campo_Saldo + " = " + Saldo_Reserva.ToString().Trim() + " " +
                        "WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Ciclo para el barrido de los detalles de la reserva
                    for (Cont_Elementos = 0; Cont_Elementos < Dt_Reserva_Detalles.Rows.Count; Cont_Elementos++)
                    {
                        Saldo_Reserva = 0;
                        Saldo_Reserva = Convert.ToDouble(Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Saldo]) - Datos.P_Importe_Total;
                        //Quitarle el saldo a la reserva
                        Mi_SQL = "UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " " +
                            "SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Saldo_Reserva.ToString().Trim() + " " +
                            "WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                        //Ejecutar consulta
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                    

                    //Ciclo para el barrido de la tabla de las facturas
                    for (Cont_Elementos = 0; Cont_Elementos < Datos.P_Dt_Facturas.Rows.Count; Cont_Elementos++)
                    {
                        //Limpiar la variable del SQL
                        Mi_SQL = "";

                        //Verificar el tipo (alta, baja, cambio)
                        switch (Datos.P_Dt_Facturas.Rows[Cont_Elementos]["Estatus"].ToString().Trim())
                        {
                            case "ALTA": //Consulta para darlo de alta
                                //COnuslta para un nuevo ID de la factura
                                Mi_SQL = "SELECT MAX(" + Ope_Alm_Registro_Facturas.Campo_Factura_ID + ") FROM " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + " ";

                                //Ejecutar consulta
                                Cmd.CommandText = Mi_SQL;
                                aux = Cmd.ExecuteScalar();

                                //Verificar si no es nulo
                                if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                                {
                                    Factura_ID = Convert.ToInt64(aux) + 1;
                                }
                                else
                                {
                                    Factura_ID = 1;
                                }

                                //COnsulta para la insercion de las facturas
                                Mi_SQL = "INSERT INTO " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + " (" + Ope_Alm_Registro_Facturas.Campo_Factura_ID + ", " +
                                    Ope_Alm_Registro_Facturas.Campo_Factura_Proveedor + ", " + Ope_Alm_Registro_Facturas.Campo_No_Contra_Recibo + ", " +
                                    Ope_Alm_Registro_Facturas.Campo_Importe_Factura + ", " + Ope_Alm_Registro_Facturas.Campo_Fecha_Factura + ", " +
                                    Ope_Alm_Registro_Facturas.Campo_No_Orden_Compra + ", " + Ope_Alm_Registro_Facturas.Campo_Usuario_Creo + ", " +
                                    Ope_Alm_Registro_Facturas.Campo_Fecha_Creo + ", " + Ope_Alm_Registro_Facturas.Campo_IVA_Factura + ", " + Ope_Alm_Registro_Facturas.Campo_Total_Factura + ") " +
                                    "VALUES(" + Factura_ID.ToString().Trim() + ", '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "', " +
                                    No_Contra_Recibo.ToString().Trim() + ", " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IMPORTE"].ToString().Trim() + ", " +
                                    "'" + String.Format("{0:yyyyMMdd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "', " + Datos.P_No_Orden_Compra + ", " +
                                    "'" + Datos.P_Usuario + "', GETDATE(), " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IVA_FACTURA"].ToString().Trim() + ", " +
                                    Datos.P_Dt_Facturas.Rows[Cont_Elementos]["TOTAL_FACTURA"].ToString().Trim() + ") ";

                                //Verificar si la cadena SQL no es nula
                                if (String.IsNullOrEmpty(Mi_SQL) == false)
                                {
                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                }

                                //Registrar Facturas Electronicas
                                if (!String.IsNullOrEmpty(Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim()))
                                {
                                    Mi_SQL = "INSERT INTO " + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + "( ";
                                    Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Factura_Id;
                                    Mi_SQL += "," + Ope_Alm_Facturas_Electronicas.Campo_Ruta;
                                    Mi_SQL += "," + Ope_Alm_Facturas_Electronicas.Campo_Descripcion;
                                    Mi_SQL += "," + Ope_Alm_Facturas_Electronicas.Campo_Usuario_Creo;
                                    Mi_SQL += "," + Ope_Alm_Facturas_Electronicas.Campo_Fecha_Creo + " )";
                                    Mi_SQL += " VALUES( ";
                                    Mi_SQL += Factura_ID.ToString().Trim();
                                    Mi_SQL += ",'" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim();
                                    Mi_SQL += "','" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim();
                                    Mi_SQL += "','" + Datos.P_Usuario + "',GETDATE() )";
                                    //Verificar si la cadena SQL no es nula
                                    if (String.IsNullOrEmpty(Mi_SQL) == false)
                                    {
                                        //Ejecutar consulta
                                        Cmd.CommandText = Mi_SQL;
                                        Cmd.ExecuteNonQuery();
                                    }
                                }
                                break;

                            case "BAJA":
                                Mi_SQL = "DELETE FROM " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + " " +
                                    "WHERE " + Ope_Alm_Registro_Facturas.Campo_Factura_ID + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();

                                //Verificar si la cadena SQL no es nula
                                if (String.IsNullOrEmpty(Mi_SQL) == false)
                                {
                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                }
                                break;

                            case "CAMBIO":
                                Mi_SQL = "UPDATE " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas;
                                Mi_SQL += " SET " + Ope_Alm_Registro_Facturas.Campo_No_Contra_Recibo + " = " + No_Contra_Recibo.ToString().Trim();
                                Mi_SQL += " WHERE " + Ope_Alm_Registro_Facturas.Campo_Factura_ID + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                                Cmd.CommandText = Mi_SQL;
                                Cmd.ExecuteNonQuery();

                                Mi_SQL = "SELECT " + Ope_Alm_Facturas_Electronicas.Campo_Factura_Id + " FROM " + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas;
                                Mi_SQL += " WHERE " + Ope_Alm_Facturas_Electronicas.Campo_Factura_Id + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                                Cmd.CommandText = Mi_SQL;
                                aux = Cmd.ExecuteScalar();

                                //verificar si es nulo
                                if (aux == null || String.IsNullOrEmpty(aux.ToString().Trim()))
                                {
                                    Mi_SQL = "INSERT INTO " + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + "( ";
                                    Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Factura_Id + ", ";
                                    Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Ruta + ", ";
                                    Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Descripcion + ", ";
                                    Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Usuario_Creo + ", ";
                                    Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Fecha_Creo + " )";
                                    Mi_SQL += " VALUES( ";
                                    Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim() + " , '";
                                    Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim() + "' , '";
                                    Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim() + "' , '";
                                    Mi_SQL += Datos.P_Usuario + "' , GETDATE())";
                                }
                                else
                                {
                                    Mi_SQL = "UPDATE " + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + " SET ";
                                    Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Ruta + " = '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim() + "', ";
                                    Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Descripcion + " = '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim() + "', ";
                                    Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario + "', ";
                                    Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Fecha_Modifico + " = GETDATE() ";
                                    Mi_SQL += " WHERE " + Ope_Alm_Facturas_Electronicas.Campo_Factura_Id + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                                }
                                Cmd.CommandText = Mi_SQL;
                                Cmd.ExecuteNonQuery();

                                break;

                            default:
                                Mi_SQL = "UPDATE " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + " " +
                                    "SET " + Ope_Alm_Registro_Facturas.Campo_No_Contra_Recibo + " = " + No_Contra_Recibo.ToString().Trim() + " " +
                                    "WHERE " + Ope_Alm_Registro_Facturas.Campo_Factura_ID + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();

                                //Verificar si la cadena SQL no es nula
                                if (String.IsNullOrEmpty(Mi_SQL) == false)
                                {
                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                }
                                break;
                        }

                        //Agregar los detalles de la solicitud de pago
                        Mi_SQL = "INSERT INTO " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " (" +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Factura + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Usuario_Creo + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Creo + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Nombre_Proveedor_Factura + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Documento + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_RFC + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_CURP + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Retencion_IVA + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_IEPS + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISH + ") " +
                            " VALUES('" + No_Solicitud_Pago + "', '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "', " +
                            "'" + String.Format("{0:yyyyMMdd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "', " +
                            Datos.P_Dt_Facturas.Rows[Cont_Elementos]["TOTAL_FACTURA"].ToString().Trim() + ", '" + Datos.P_Usuario + "', GETDATE(), '" + Datos.P_Proveedor + "', " +
                            "'DOCUMENTO', '" + Partida_ID + "', '" + Fuente_Financiamiento_ID + "', '" + Proyecto_Programa_ID + "', " +
                            Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IVA_FACTURA"].ToString().Trim() + ", " +
                            "'" + Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_RFC].ToString().Trim() + "', " +
                            "'" + Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_CURP].ToString().Trim() + "', 'OTROS', 0, 0, 0, 0, 0) ";

                        //Ejecutar consulta
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    //Agregar la solicitud de pago al contrarecibo
                    Mi_SQL = "UPDATE " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " SET " + Ope_Alm_Contrarecibos.Campo_No_Solicitud_Pago + " = " +
                        "'" + No_Solicitud_Pago + "' WHERE " + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = " + No_Contra_Recibo.ToString().Trim() + " ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Ejecutar transaccion
                    Trans.Commit();

                    //Entregar el numero de contrarecibo
                    return No_Contra_Recibo.ToString().Trim() + "," + No_Solicitud_Pago.ToString().Trim();
                }
                else
                {
                    Trans.Rollback();
                    throw new Exception("El proveedor no tiene datos.");
                }
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje); // Se indica el mensaje 
            }
            finally
            {
                Cn.Close();
            }
        }
        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Alta_Contrarecibo
        /// DESCRIPCION:            Guardar el contrarecibo con los datos de las facturas o modificaciones de las mismas
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            28/Enero/2013 11:12 
        /// MODIFICO          :     Jesus Toledo Rodriguez
        /// FECHA_MODIFICO    :     15/Abril/2013 09:45
        /// CAUSA_MODIFICACION:     Adaptar a Tablas de Taller Mecanico
        ///*******************************************************************************/
        public static string Alta_Contrarecibo_Taller(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            String Mensaje = "";
            String Mi_SQL = String.Empty;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            Object aux; //Variable auxiliar 
            Int64 No_Contra_Recibo = 0; //variable para el numero de contrarecibo
            int Cont_Elementos = 0; //variable para el contador
            Int64 Factura_ID = 0; //variable para el ID de la factura
            String No_Solicitud_Pago = String.Empty; //variable para la solicitud de pago
            Int64 No_Reserva = 0; //variable para consultar el numero de la reserva de la orden de compra
            DataTable Dt_Datos_Proveedor = new DataTable(); //tabla para los datos del proveedor
            string Tipo_Solicitud_Pago_ID = string.Empty; //variable para el tipo de la solicitud de pago
            DataTable Dt_Reserva_Detalles = new DataTable(); //Tabla para los detalles de la reserva
            string Partida_ID = string.Empty; //variable para el ID de la partida
            string Fuente_Financiamiento_ID = string.Empty; //Variable para el ID de la fuente de financiamiento
            string Proyecto_Programa_ID = string.Empty; //variable para el ID del proyecto programa
            double Saldo_Reserva = 0; //Variable para el saldo de la reserva

            try
            {
                //Consultar los datos del proveedor
                Mi_SQL = "SELECT " + Cat_Com_Proveedores.Campo_Proveedor_ID + ", " + Cat_Com_Proveedores.Campo_Nombre + ", " + Cat_Com_Proveedores.Campo_Compañia + ", " +
                    Cat_Com_Proveedores.Campo_RFC + ", " + Cat_Com_Proveedores.Campo_CURP + " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " " +
                    "WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "' ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                da.SelectCommand = Cmd;
                da.Fill(Dt_Datos_Proveedor);

                //Consulta para la reserva de la orden de compra
                Mi_SQL = "SELECT " + Ope_Tal_Ordenes_Compra.Campo_No_Reserva
                    + " FROM " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra
                    + " WHERE " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra
                    + " = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                aux = Cmd.ExecuteScalar();

                //Colocar el numero de la reserva
                No_Reserva = Convert.ToInt64(aux);

                //Consultar el tipo de la solicitud de pago de la reserva
                Mi_SQL = "SELECT " + Ope_Psp_Reservas.Campo_Tipo_Solicitud_Pago_ID + " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " " +
                    "WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                //Ejecutar la consulta
                Cmd.CommandText = Mi_SQL;
                aux = Cmd.ExecuteScalar();

                //verificar si el dato es numo
                if (aux != null)
                {
                    if (aux != DBNull.Value)
                    {
                        Tipo_Solicitud_Pago_ID = aux.ToString().Trim();
                    }
                    else
                    {
                        Tipo_Solicitud_Pago_ID = "00004";
                    }
                }
                else
                {
                    Tipo_Solicitud_Pago_ID = "00004";
                }

                //Consultar los ID de la Fuente de financiamiento, proyecto programa, partida
                Mi_SQL = "SELECT " + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + ", " + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + ", " +
                    Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ", " + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + ", " +
                    Ope_Psp_Reservas_Detalles.Campo_Capitulo_ID + ", " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " " +
                    "FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " " +
                    "WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                da.Fill(Dt_Reserva_Detalles);

                //verificar si la consulta arrojo resultado
                if (Dt_Reserva_Detalles.Rows.Count > 0)
                {
                    //Colocar los datos de la fuente de financiamiento, proyecto programa, etc
                    Partida_ID = Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString().Trim();
                    Fuente_Financiamiento_ID = Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                    Proyecto_Programa_ID = Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                }
                else
                {
                    Trans.Rollback();
                    throw new Exception("La requisicion no contiene datos.");
                }

                //verificar si el proveedor tiene datos
                if (Dt_Datos_Proveedor.Rows.Count > 0)
                {
                    //Consulta para el numero de contrarecibo
                    Mi_SQL = "SELECT MAX(" + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + ") FROM " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos;

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    aux = Cmd.ExecuteScalar();

                    //Verificar si no es nulo
                    if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                    {
                        No_Contra_Recibo = Convert.ToInt64(aux) + 1;
                    }
                    else
                    {
                        No_Contra_Recibo = 1;
                    }

                    //Asignar consulta para el contrarecibo
                    Mi_SQL = "INSERT INTO " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "(" +
                        Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + "," + Ope_Alm_Contrarecibos.Campo_Empleado_Recibido_ID +
                        "," + Ope_Alm_Contrarecibos.Campo_Proveedor_ID + "," + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion + "," + Ope_Alm_Contrarecibos.Campo_Fecha_Pago + "," + Ope_Alm_Contrarecibos.Campo_Importe_Total +
                        "," + Ope_Alm_Contrarecibos.Campo_Usuario_Creo + "," + Ope_Alm_Contrarecibos.Campo_Fecha_Creo + "," + Ope_Alm_Contrarecibos.Campo_Estatus + "," + Ope_Alm_Contrarecibos.Campo_Tipo + ") VALUES(" + No_Contra_Recibo.ToString().Trim() +
                        ",'" + Datos.P_Usuario_ID + "'," + "'" + Datos.P_Proveedor_ID + "','" + Datos.P_Fecha_Recepcion + "'," + "'" + Datos.P_Fecha_Pago + "'," + Datos.P_Importe_Total.ToString().Trim() + "," +
                        "'" + Datos.P_Usuario + "',GETDATE(), 'GENERADO','TALLER')";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Establecer relacion de contrarecibo y orden de compra
                    Mi_SQL = " UPDATE " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " SET " + Ope_Tal_Ordenes_Compra.Campo_No_Factura_Interno + " = " + No_Contra_Recibo;
                    Mi_SQL += " WHERE " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.ToString().Trim();
                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Consulta para el numero de solicitud de pago
                    Mi_SQL = "SELECT MAX(" + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ") FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos;

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    aux = Cmd.ExecuteScalar();

                    //verificar si es nulo
                    if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                    {
                        //Verificar si no es -1
                        if (Convert.ToInt32(aux) > -1)
                        {
                            No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(aux) + 1);
                        }
                        else
                        {
                            No_Solicitud_Pago = "0000000001";
                        }
                    }
                    else
                    {
                        No_Solicitud_Pago = "0000000001";
                    }

                    //Asignar consulta para la solicitud de pago
                    Mi_SQL = "INSERT INTO " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos
                        + " (" + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ","
                        + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ","
                        + Ope_Con_Solicitud_Pagos.Campo_Concepto + ","
                        + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ","
                        + Ope_Con_Solicitud_Pagos.Campo_Monto + ","
                        + Ope_Con_Solicitud_Pagos.Campo_Estatus + ","
                        + Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", "
                        + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ","
                        + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", " + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID+ ") " +
                        "VALUES('" + No_Solicitud_Pago + "','00004','CONTRARECIBO NO " + No_Contra_Recibo.ToString().Trim() + "',GETDATE()," +
                        Datos.P_Importe_Total.ToString().Trim() + ",'PRE-DOCUMENTADO','" + Datos.P_Usuario + "',GETDATE(), " + No_Reserva.ToString().Trim() + ", '" + Datos.P_Proveedor_ID +"')";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    foreach (DataRow Dr_Renglon in Datos.P_Dt_Facturas_Eliminadas.Rows)
                    {
                        Mi_SQL = "DELETE FROM " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas + " WHERE " + Ope_Tal_Registro_Facturas.Campo_Factura_ID + " = " + Dr_Renglon["FACTURA_ID"].ToString().Trim();
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        Mi_SQL = "DELETE FROM " + Ope_Tal_Facturas_Electronicas.Tabla_Ope_Tal_Facturas_Electronicas + " WHERE " + Ope_Tal_Facturas_Electronicas.Campo_Factura_Id + " = " + Dr_Renglon["FACTURA_ID"].ToString().Trim();
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    //Consultar el saldo de la reserva
                    Mi_SQL = "SELECT " + Ope_Psp_Reservas.Campo_Saldo + " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " " +
                        "WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    aux = Cmd.ExecuteScalar();

                    //Asignar valor del saldo de la reserva
                    if (aux != null)
                    {
                        if (aux != DBNull.Value)
                        {
                            if (string.IsNullOrEmpty(aux.ToString().Trim()) == false)
                            {
                                Saldo_Reserva = Convert.ToDouble(aux);
                            }
                        }
                    }

                    //Recalcular el saldo de la reserva
                    Saldo_Reserva = Saldo_Reserva - Datos.P_Importe_Total;

                    //Quitarle el saldo a la reserva
                    Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " " +
                        "SET " + Ope_Psp_Reservas.Campo_Saldo + " = " + Saldo_Reserva.ToString().Trim() + " " +
                        "WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Ciclo para el barrido de los detalles de la reserva
                    for (Cont_Elementos = 0; Cont_Elementos < Dt_Reserva_Detalles.Rows.Count; Cont_Elementos++)
                    {
                        Saldo_Reserva = 0;
                        Saldo_Reserva = Convert.ToDouble(Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Saldo]) - Datos.P_Importe_Total;
                        //Quitarle el saldo a la reserva
                        Mi_SQL = "UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " " +
                            "SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Saldo_Reserva.ToString().Trim() + " " +
                            "WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                        //Ejecutar consulta
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }


                    //Ciclo para el barrido de la tabla
                    for (Cont_Elementos = 0; Cont_Elementos < Datos.P_Dt_Facturas.Rows.Count; Cont_Elementos++)
                    {
                        //Limpiar la variable del SQL
                        Mi_SQL = "";

                        //Verificar el tipo (alta, baja, cambio)
                        switch (Datos.P_Dt_Facturas.Rows[Cont_Elementos]["Estatus"].ToString().Trim())
                        {
                            case "ALTA": //Consulta para darlo de alta
                                //COnuslta para un nuevo ID de la factura
                                Mi_SQL = "SELECT MAX( " + Ope_Tal_Registro_Facturas.Campo_Factura_ID + ") FROM " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas;

                                //Ejecutar consulta
                                Cmd.CommandText = Mi_SQL;
                                aux = Cmd.ExecuteScalar();

                                //Verificar si no es nulo
                                if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                                {
                                    Factura_ID = Convert.ToInt64(aux) + 1;
                                }
                                else
                                {
                                    Factura_ID = 1;
                                }

                                Mi_SQL = "INSERT INTO " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas
                                    + "(" + Ope_Tal_Registro_Facturas.Campo_Factura_ID + ","
                                    + Ope_Tal_Registro_Facturas.Campo_Factura_Proveedor + ","
                                    + Ope_Tal_Registro_Facturas.Campo_No_Contra_Recibo + ","
                                    + Ope_Tal_Registro_Facturas.Campo_Importe_Factura + ","
                                    + Ope_Tal_Registro_Facturas.Campo_Fecha_Factura + ", "
                                    + Ope_Tal_Registro_Facturas.Campo_No_Orden_Compra + ", "
                                    + Ope_Tal_Registro_Facturas.Campo_Usuario_Creo + ","
                                    + Ope_Tal_Registro_Facturas.Campo_Fecha_Creo + ", " + Ope_Tal_Registro_Facturas.Campo_IVA_Factura + ", " 
                                    + Ope_Tal_Registro_Facturas.Campo_Total_Factura + ") VALUES(" + Factura_ID.ToString().Trim() + "," +
                                    "'" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "',"
                                    + No_Contra_Recibo.ToString().Trim() + ","
                                    + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IMPORTE"].ToString().Trim() + "," +
                                    "'" + String.Format("{0:yyyyMMdd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "'," + Datos.P_No_Orden_Compra + "," +
                                    "'" + Datos.P_Usuario + "',GETDATE(), " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IVA_FACTURA"].ToString().Trim()+ ", " + 
                                    Datos.P_Dt_Facturas.Rows[Cont_Elementos]["TOTAL_FACTURA"].ToString().Trim() + ")";

                                //Verificar si la cadena SQL no es nula
                                if (String.IsNullOrEmpty(Mi_SQL) == false)
                                {
                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                }

                                //Registrar Facturas Electronicas
                                if (!String.IsNullOrEmpty(Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim()))
                                {
                                    Mi_SQL = "INSERT INTO " + Ope_Tal_Facturas_Electronicas.Tabla_Ope_Tal_Facturas_Electronicas + "( ";
                                    Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Factura_Id;
                                    Mi_SQL += "," + Ope_Tal_Facturas_Electronicas.Campo_Ruta;
                                    Mi_SQL += "," + Ope_Tal_Facturas_Electronicas.Campo_Descripcion;
                                    Mi_SQL += "," + Ope_Tal_Facturas_Electronicas.Campo_Tipo;
                                    Mi_SQL += "," + Ope_Tal_Facturas_Electronicas.Campo_Usuario_Creo;
                                    Mi_SQL += "," + Ope_Tal_Facturas_Electronicas.Campo_Fecha_Creo + " )";
                                    Mi_SQL += " VALUES( ";
                                    Mi_SQL += Factura_ID.ToString().Trim();
                                    Mi_SQL += ",'" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim();
                                    Mi_SQL += "','" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim();
                                    Mi_SQL += "','TALLER'";
                                    Mi_SQL += ",'" + Datos.P_Usuario + "',GETDATE() )";
                                    //Verificar si la cadena SQL no es nula
                                    if (String.IsNullOrEmpty(Mi_SQL) == false)
                                    {
                                        //Ejecutar consulta
                                        Cmd.CommandText = Mi_SQL;
                                        Cmd.ExecuteNonQuery();
                                    }
                                }
                                break;

                            case "BAJA":
                                Mi_SQL = "DELETE FROM " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas
                                    + " WHERE " + Ope_Tal_Registro_Facturas.Campo_Factura_ID + " = "
                                    + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                                //Verificar si la cadena SQL no es nula
                                if (String.IsNullOrEmpty(Mi_SQL) == false)
                                {
                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                }
                                break;

                            case "CAMBIO":
                                Mi_SQL = "UPDATE " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas;
                                Mi_SQL += " SET " + Ope_Tal_Registro_Facturas.Campo_No_Contra_Recibo + " = " + No_Contra_Recibo.ToString().Trim();
                                Mi_SQL += " WHERE " + Ope_Tal_Registro_Facturas.Campo_Factura_ID + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                                Cmd.CommandText = Mi_SQL;
                                Cmd.ExecuteNonQuery();

                                Mi_SQL = "SELECT " + Ope_Tal_Facturas_Electronicas.Campo_Factura_Id + " FROM " + Ope_Tal_Facturas_Electronicas.Tabla_Ope_Tal_Facturas_Electronicas;
                                Mi_SQL += " WHERE " + Ope_Tal_Facturas_Electronicas.Campo_Factura_Id + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                                Cmd.CommandText = Mi_SQL;
                                aux = Cmd.ExecuteScalar();

                                //verificar si es nulo
                                if (aux == null || String.IsNullOrEmpty(aux.ToString().Trim()))
                                {
                                    Mi_SQL = "INSERT INTO " + Ope_Tal_Facturas_Electronicas.Tabla_Ope_Tal_Facturas_Electronicas + "( ";
                                    Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Factura_Id + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Ruta + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Descripcion + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Tipo + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Usuario_Creo + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Fecha_Creo + " )";
                                    Mi_SQL += " VALUES( ";
                                    Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim() + " , '";
                                    Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim() + "' , '";
                                    Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim() + "' , 'TALLER', '";
                                    Mi_SQL += Datos.P_Usuario + "' , GETDATE() )";
                                }
                                else
                                {
                                    Mi_SQL = "UPDATE " + Ope_Tal_Facturas_Electronicas.Tabla_Ope_Tal_Facturas_Electronicas + " SET ";
                                    Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Ruta + " = '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim() + "', ";
                                    Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Descripcion + " = '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim() + "', ";
                                    Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario + "', ";
                                    Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Fecha_Modifico + " = GETDATE() ";
                                    Mi_SQL += " WHERE " + Ope_Tal_Facturas_Electronicas.Campo_Factura_Id + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                                }
                                Cmd.CommandText = Mi_SQL;
                                Cmd.ExecuteNonQuery();
                                break;

                            default:
                                Mi_SQL = "UPDATE " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas
                                    + " SET " + Ope_Tal_Registro_Facturas.Campo_No_Contra_Recibo + " = " + No_Contra_Recibo.ToString().Trim() + " " +
                                    "WHERE " + Ope_Tal_Registro_Facturas.Campo_Factura_ID + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                                //Verificar si la cadena SQL no es nula
                                if (String.IsNullOrEmpty(Mi_SQL) == false)
                                {
                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                }
                                break;
                        }

                        //Agregar los detalles de la solicitud de pago
                        Mi_SQL = "INSERT INTO " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles
                            + "( " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago
                            + "," + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura
                            + "," + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Factura
                            + "," + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura
                            + "," + Ope_Con_Solicitud_Pagos_Detalles.Campo_Usuario_Creo
                            + "," + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Creo + ", "
                            + Ope_Con_Solicitud_Pagos_Detalles.Campo_Nombre_Proveedor_Factura + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Documento + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_RFC + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_CURP + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Retencion_IVA + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_IEPS + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISH + ") VALUES('" + No_Solicitud_Pago + "'," +
                            "'" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "'," +
                            "'" + String.Format("{0:yyyyMMdd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "'," +
                            Datos.P_Dt_Facturas.Rows[Cont_Elementos]["TOTAL_FACTURA"].ToString().Trim() + ",'" + Datos.P_Usuario + "',GETDATE()," +
                            "'" + Datos.P_Proveedor + "', 'DOCUMENTO', '" + Partida_ID + "', '" + Fuente_Financiamiento_ID + "', '" + Proyecto_Programa_ID + "', " +
                            Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IVA_FACTURA"].ToString().Trim() + ", " +
                            "'" + Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_RFC].ToString().Trim() + "', " +
                            "'" + Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_CURP].ToString().Trim() + "', 'OTROS', 0, 0, 0, 0, 0) ";

                        //Ejecutar consulta
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    //Agregar la solicitud de pago al contrarecibo
                    Mi_SQL = "UPDATE " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " SET " + Ope_Alm_Contrarecibos.Campo_No_Solicitud_Pago + " = " +
                        "'" + No_Solicitud_Pago + "' WHERE " + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = " + No_Contra_Recibo.ToString().Trim() + " ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Ejecutar transaccion
                    Trans.Commit();

                    //Entregar el numero de contrarecibo
                    return No_Contra_Recibo.ToString().Trim() + "," + No_Solicitud_Pago.ToString().Trim();
                }
                else
                {
                    Trans.Rollback();
                    throw new Exception("El proveedor no tiene datos.");
                }
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje); // Se indica el mensaje 
            }
            finally
            {
                Cn.Close();
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Alta_Contrarecibo_Taller_Externo
        /// DESCRIPCION:            Guardar el contrarecibo con los datos de las facturas o modificaciones de las mismas
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            28/Enero/2013 11:12 
        /// MODIFICO          :     Jesus Toledo Rodriguez
        /// FECHA_MODIFICO    :     15/Abril/2013 09:45
        /// CAUSA_MODIFICACION:     Adaptar a Tablas de Taller Mecanico
        ///*******************************************************************************/
        public static string Alta_Contrarecibo_Taller_Externo(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            String Mensaje = "";
            String Mi_SQL = String.Empty;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            Object aux; //Variable auxiliar 
            Int64 No_Contra_Recibo = 0; //variable para el numero de contrarecibo
            int Cont_Elementos = 0; //variable para el contador
            Int64 Factura_ID = 0; //variable para el ID de la factura
            String No_Solicitud_Pago = String.Empty; //variable para la solicitud de pago
            Int64 No_Reserva = 0; //variable para consultar el numero de la reserva de la orden de compra
            String No_Servicio = "";
            String Tipo_Servicio = "";
            String No_Asignacion = ""; 
            DataTable Dt_Aux = new DataTable();
            DataTable Dt_Datos_Proveedor = new DataTable(); //tabla para los datos del proveedor
            string Tipo_Solicitud_Pago_ID = string.Empty; //Variable para el ID del tipo de la solicitud de pago
            DataTable Dt_Reserva_Detalles = new DataTable(); //tabla para los detalles de la reserva
            string Partida_ID = string.Empty; //variable para el ID de la partida
            string Fuente_Financiamiento_ID = string.Empty; //variable para el ID de la fuente de financiamiento
            string Proyecto_Programa_ID = string.Empty; //variable para el ID del proyecto programa
            double Saldo_Reserva = 0; //variable para el saldo de la reserva

            try
            {
                //Consultar los datos del proveedor
                Mi_SQL = "SELECT " + Cat_Com_Proveedores.Campo_Proveedor_ID + ", " + Cat_Com_Proveedores.Campo_Nombre + ", " + Cat_Com_Proveedores.Campo_Compañia + ", " +
                    Cat_Com_Proveedores.Campo_RFC + ", " + Cat_Com_Proveedores.Campo_CURP + " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " " +
                    "WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "' ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                da.SelectCommand = Cmd;
                da.Fill(Dt_Datos_Proveedor);

                //Consulta para la reserva de la orden de compra
                Mi_SQL = "SELECT " + Ope_Tal_Facturas_Servicio.Campo_No_Reserva
                    + " FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio
                    + " WHERE " + Ope_Tal_Facturas_Servicio.Campo_Folio_Solicitud
                    + " = '" + Datos.P_No_Orden_Compra.ToString().Trim() + "' ";
                    if (Datos.P_Tipo_OC == "TALLER_EXTERNO")
                        Mi_SQL += " AND " + Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio + " like 'SERVICIO%'";
                    else
                        Mi_SQL += " AND " + Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio + " = '" + Datos.P_Tipo_OC.ToString().Trim().Replace(" ","_") + "' ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                aux = Cmd.ExecuteScalar();

                //Colocar el numero de la reserva
                No_Reserva = Convert.ToInt64(aux);

                //Consultar el tipo de la solicitud de pago de la reserva
                Mi_SQL = "SELECT " + Ope_Psp_Reservas.Campo_Tipo_Solicitud_Pago_ID + " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " " +
                    "WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                //Ejecutar la consulta
                Cmd.CommandText = Mi_SQL;
                aux = Cmd.ExecuteScalar();

                //verificar si el dato es numo
                if (aux != null)
                {
                    if (aux != DBNull.Value)
                    {
                        Tipo_Solicitud_Pago_ID = aux.ToString().Trim();
                    }
                    else
                    {
                        Tipo_Solicitud_Pago_ID = "00004";
                    }
                }
                else
                {
                    Tipo_Solicitud_Pago_ID = "00004";
                }

                //Consultar los ID de la Fuente de financiamiento, proyecto programa, partida
                Mi_SQL = "SELECT " + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + ", " + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + ", " +
                    Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ", " + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + ", " +
                    Ope_Psp_Reservas_Detalles.Campo_Capitulo_ID + ", " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " " +
                    "FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " " +
                    "WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                da.Fill(Dt_Reserva_Detalles);

                //verificar si la consulta arrojo resultado
                if (Dt_Reserva_Detalles.Rows.Count > 0)
                {
                    //Colocar los datos de la fuente de financiamiento, proyecto programa, etc
                    Partida_ID = Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString().Trim();
                    Fuente_Financiamiento_ID = Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                    Proyecto_Programa_ID = Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                }
                else
                {
                    Trans.Rollback();
                    throw new Exception("La requisicion no contiene datos.");
                }


                //Verificar si el proveedor tiene datos
                if (Dt_Datos_Proveedor.Rows.Count > 0)
                {
                    //Consulta para el numero de contrarecibo
                    Mi_SQL = "SELECT MAX(" + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + ") FROM " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos;

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    aux = Cmd.ExecuteScalar();

                    //Verificar si no es nulo
                    if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                    {
                        No_Contra_Recibo = Convert.ToInt64(aux) + 1;
                    }
                    else
                    {
                        No_Contra_Recibo = 1;
                    }

                    //Asignar consulta para el contrarecibo
                    Mi_SQL = "INSERT INTO " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "(" +
                        Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + "," + Ope_Alm_Contrarecibos.Campo_Empleado_Recibido_ID +
                        "," + Ope_Alm_Contrarecibos.Campo_Proveedor_ID + "," + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion + "," + Ope_Alm_Contrarecibos.Campo_Fecha_Pago + "," + Ope_Alm_Contrarecibos.Campo_Importe_Total +
                        "," + Ope_Alm_Contrarecibos.Campo_Usuario_Creo + "," + Ope_Alm_Contrarecibos.Campo_Fecha_Creo + "," + Ope_Alm_Contrarecibos.Campo_Estatus + "," + Ope_Alm_Contrarecibos.Campo_Tipo + ") VALUES(" + No_Contra_Recibo.ToString().Trim() +
                        ",'" + Datos.P_Usuario_ID + "'," + "'" + Datos.P_Proveedor_ID + "','" + Datos.P_Fecha_Recepcion + "'," + "'" + Datos.P_Fecha_Pago + "'," + Datos.P_Importe_Total.ToString().Trim() + "," +
                        "'" + Datos.P_Usuario + "',GETDATE(), 'GENERADO','TALLER_EXTERNO')";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Obtener Numero Servicio y Tipo
                    //Mi_SQL = " SELECT " + Vw_Tal_Servicios.Campo_No_servicio + ","
                    //    + Vw_Tal_Servicios.Campo_Tipo_Servicio
                    //    + " FROM " + Vw_Tal_Servicios.Vista_Vw_Tal_Servicios
                    //    + " WHERE CAST(" + Vw_Tal_Servicios.Campo_Folio + " AS INT) = " + Datos.P_No_Orden_Compra;
                    //Cmd.CommandText = Mi_SQL;
                    //da.SelectCommand = Cmd;
                    //da.Fill(Dt_Aux);
                    //if (Dt_Aux.Rows.Count > 0)
                    //{
                    //    No_Servicio = Dt_Aux.Rows[0][Vw_Tal_Servicios.Campo_No_servicio].ToString();
                    //    Tipo_Servicio = Dt_Aux.Rows[0][Vw_Tal_Servicios.Campo_Tipo_Servicio].ToString();
                    //}
                    ////Obtener Numero de Asinacion de Proveedor
                    //Mi_SQL = " SELECT " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + ","
                    //    + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio
                    //    + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;
                    //Mi_SQL += " WHERE " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = " + No_Servicio.Trim();
                    //Mi_SQL += " AND " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = '" + Tipo_Servicio.Trim() + "'";
                    //Dt_Aux = new DataTable();
                    //Cmd.CommandText = Mi_SQL;
                    //da.SelectCommand = Cmd;
                    //da.Fill(Dt_Aux);
                    //if (Dt_Aux.Rows.Count > 0)
                    //{
                    //    No_Asignacion = Dt_Aux.Rows[0][Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion].ToString();
                    //}
                    ////Establecer relacion de contrarecibo y orden de compra
                    //Mi_SQL = " UPDATE " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " SET " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Contra_Recibo + " = " + No_Contra_Recibo;
                    //Mi_SQL += " WHERE " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = " + No_Servicio.Trim();
                    //Mi_SQL += " AND " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = '" + Tipo_Servicio.Trim() + "'";
                    ////Ejecutar consulta
                    //Cmd.CommandText = Mi_SQL;
                    //Cmd.ExecuteNonQuery();

                    //Consulta para el numero de solicitud de pago
                    Mi_SQL = "SELECT MAX(" + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ") FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos;

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    aux = Cmd.ExecuteScalar();

                    //verificar si es nulo
                    if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                    {
                        //Verificar si no es -1
                        if (Convert.ToInt32(aux) > -1)
                        {
                            No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(aux) + 1);
                        }
                        else
                        {
                            No_Solicitud_Pago = "0000000001";
                        }
                    }
                    else
                    {
                        No_Solicitud_Pago = "0000000001";
                    }

                    //Asignar consulta para la solicitud de pago
                    Mi_SQL = "INSERT INTO " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos
                        + " (" + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ","
                        + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ","
                        + Ope_Con_Solicitud_Pagos.Campo_Concepto + ","
                        + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ","
                        + Ope_Con_Solicitud_Pagos.Campo_Monto + ","
                        + Ope_Con_Solicitud_Pagos.Campo_Estatus + ","
                        + Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", "
                        + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ","
                        + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", " + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + ") " +
                        "VALUES('" + No_Solicitud_Pago + "','" + Tipo_Solicitud_Pago_ID + "','CONTRARECIBO NO " + No_Contra_Recibo.ToString().Trim() + "',GETDATE()," +
                        Datos.P_Importe_Total.ToString().Trim() + ",'PRE-DOCUMENTADO','" + Datos.P_Usuario + "',GETDATE(), " + No_Reserva.ToString().Trim() + ", " + 
                        "'" + Datos.P_Proveedor_ID + "') ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    foreach (DataRow Dr_Renglon in Datos.P_Dt_Facturas_Eliminadas.Rows)
                    {
                        Mi_SQL = "DELETE FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio + " WHERE " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + " = " + Dr_Renglon["FACTURA_ID"].ToString().Trim();
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    //Consultar el saldo de la reserva
                    Mi_SQL = "SELECT " + Ope_Psp_Reservas.Campo_Saldo + " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " " +
                        "WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    aux = Cmd.ExecuteScalar();

                    //Asignar valor del saldo de la reserva
                    if (aux != null)
                    {
                        if (aux != DBNull.Value)
                        {
                            if (string.IsNullOrEmpty(aux.ToString().Trim()) == false)
                            {
                                Saldo_Reserva = Convert.ToDouble(aux);
                            }
                        }
                    }

                    //Recalcular el saldo de la reserva
                    Saldo_Reserva = Saldo_Reserva - Datos.P_Importe_Total;

                    //Quitarle el saldo a la reserva
                    Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " " +
                        "SET " + Ope_Psp_Reservas.Campo_Saldo + " = " + Saldo_Reserva.ToString().Trim() + " " +
                        "WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Ciclo para el barrido de los detalles de la reserva
                    for (Cont_Elementos = 0; Cont_Elementos < Dt_Reserva_Detalles.Rows.Count; Cont_Elementos++)
                    {
                        Saldo_Reserva = 0;
                        Saldo_Reserva = Convert.ToDouble(Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Saldo]) - Datos.P_Importe_Total;
                        //Quitarle el saldo a la reserva
                        Mi_SQL = "UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " " +
                            "SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Saldo_Reserva.ToString().Trim() + " " +
                            "WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + No_Reserva.ToString().Trim() + " ";

                        //Ejecutar consulta
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    //Ciclo para el barrido de la tabla
                    for (Cont_Elementos = 0; Cont_Elementos < Datos.P_Dt_Facturas.Rows.Count; Cont_Elementos++)
                    {
                        //Limpiar la variable del SQL
                        Mi_SQL = "";

                        //Verificar el tipo (alta, baja, cambio)
                        switch (Datos.P_Dt_Facturas.Rows[Cont_Elementos]["Estatus"].ToString().Trim())
                        {
                            case "ALTA": //Consulta para darlo de alta
                                //COnuslta para un nuevo ID de la factura
                                Mi_SQL = "SELECT MAX( " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + ") FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio;

                                //Ejecutar consulta
                                Cmd.CommandText = Mi_SQL;
                                aux = Cmd.ExecuteScalar();

                                //Verificar si no es nulo
                                if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                                {
                                    Factura_ID = Convert.ToInt64(aux) + 1;
                                }
                                else
                                {
                                    Factura_ID = 1;
                                }

                                Mi_SQL = "INSERT INTO " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio
                                    + "(" + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + ","
                                    + Ope_Tal_Facturas_Servicio.Campo_No_Asignacion + ","
                                    + Ope_Tal_Facturas_Servicio.Campo_No_Factura + ","
                                    + Ope_Tal_Facturas_Servicio.Campo_No_Servicio + ","
                                    + Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio + ", "
                                    + Ope_Tal_Facturas_Servicio.Campo_Factura + ","
                                    + Ope_Tal_Facturas_Servicio.Campo_Monto_Factura + ", "
                                    + Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo + ", "
                                    + Ope_Tal_Facturas_Servicio.Campo_Fecha_Factura + ","
                                    + Ope_Tal_Facturas_Servicio.Campo_Ruta_Archivo + ", "
                                    + Ope_Tal_Facturas_Servicio.Campo_Comentarios + ","
                                    + Ope_Tal_Facturas_Servicio.Campo_Usuario_Creo + ","
                                    + Ope_Tal_Facturas_Servicio.Campo_Fecha_Creo + ", " + Ope_Tal_Facturas_Servicio.Campo_IVA_Factura + ", " +
                                    Ope_Tal_Facturas_Servicio.Campo_Total_Factura + ") VALUES(" + Factura_ID.ToString().Trim() + "," + No_Asignacion + "," + 
                                    Cont_Elementos + 1 + "," + No_Servicio + ",'" + Tipo_Servicio + "'," +
                                    "'" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "',"
                                    + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IMPORTE"].ToString().Trim() + "," + No_Contra_Recibo.ToString().Trim() + "," +
                                    "'" + String.Format("{0:yyyyMMdd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "', '"
                                    + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"] + "', '"
                                    + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"] + "', '" +
                                    Datos.P_Usuario + "',GETDATE(), " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IVA_FACTURA"].ToString().Trim() + " , " + 
                                    Datos.P_Dt_Facturas.Rows[Cont_Elementos]["TOTAL_FACTURA"].ToString().Trim() + ") ";

                                //Verificar si la cadena SQL no es nula
                                if (String.IsNullOrEmpty(Mi_SQL) == false)
                                {
                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                }
                                break;

                            case "BAJA":
                                Mi_SQL = "DELETE FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio
                                    + " WHERE " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + " = "
                                    + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                                //Verificar si la cadena SQL no es nula
                                if (String.IsNullOrEmpty(Mi_SQL) == false)
                                {
                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                }
                                break;

                            case "CAMBIO":
                                Mi_SQL = "SELECT " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + " FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio;
                                Mi_SQL += " WHERE " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                                Cmd.CommandText = Mi_SQL;
                                aux = Cmd.ExecuteScalar();

                                //verificar si es nulo
                                if (aux == null || String.IsNullOrEmpty(aux.ToString().Trim()))
                                {
                                    //COnuslta para un nuevo ID de la factura
                                    Mi_SQL = "SELECT MAX( " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + ") FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio;
                                    Cmd.CommandText = Mi_SQL;
                                    aux = Cmd.ExecuteScalar();

                                    Factura_ID = String.IsNullOrEmpty(aux.ToString().Trim()) ? 1 : Convert.ToInt64(aux) + 1;

                                    Mi_SQL = "INSERT INTO " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio + "(";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_No_Asignacion + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_No_Factura + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_No_Servicio + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Factura + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Monto_Factura + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Fecha_Factura + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Ruta_Archivo + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Comentarios + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Usuario_Creo + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Fecha_Creo + ", " + Ope_Tal_Facturas_Servicio.Campo_IVA_Factura + ", " + 
                                    Ope_Tal_Facturas_Servicio.Campo_Total_Factura + ") VALUES(";
                                    Mi_SQL += Factura_ID.ToString().Trim() + ", ";
                                    Mi_SQL += No_Asignacion + ", ";
                                    Mi_SQL += Cont_Elementos + 1 + ", ";
                                    Mi_SQL += No_Servicio + ", '";
                                    Mi_SQL += Tipo_Servicio + "', '";
                                    Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "', ";
                                    Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IMPORTE"].ToString().Trim() + ", ";
                                    Mi_SQL += No_Contra_Recibo.ToString().Trim() + ", '";
                                    Mi_SQL += String.Format("{0:yyyyMMdd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "', '";
                                    Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"] + "', '";
                                    Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"] + "', '";
                                    Mi_SQL += Datos.P_Usuario + "',GETDATE(), " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IVA_FACTURA"].ToString().Trim() + ", " +
                                    Datos.P_Dt_Facturas.Rows[Cont_Elementos]["TOTAL_FACTURA"].ToString().Trim() + ") ";
                                }
                                else
                                {
                                    Mi_SQL = "UPDATE " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio + " SET ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Ruta_Archivo + " = '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim() + "', ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Comentarios + " = '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim() + "', ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo + " = " + No_Contra_Recibo + ", ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario + "', ";
                                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Fecha_Modifico + " = GETDATE() ";
                                    Mi_SQL += " WHERE " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                                }
                                Cmd.CommandText = Mi_SQL;
                                Cmd.ExecuteNonQuery();
                                break;

                            default:
                                Mi_SQL = "UPDATE " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio
                                    + " SET " + Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo + " = " + No_Contra_Recibo.ToString().Trim() + " " +
                                    "WHERE " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                                //Verificar si la cadena SQL no es nula
                                if (String.IsNullOrEmpty(Mi_SQL) == false)
                                {
                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                }
                                break;
                        }


                        //Agregar los detalles de la solicitud de pago
                        Mi_SQL = "INSERT INTO " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles
                            + "( " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago
                            + "," + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura
                            + "," + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Factura
                            + "," + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura
                            + "," + Ope_Con_Solicitud_Pagos_Detalles.Campo_Usuario_Creo
                            + "," + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Creo + ", "
                            + Ope_Con_Solicitud_Pagos_Detalles.Campo_Nombre_Proveedor_Factura + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Documento + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_RFC + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_CURP + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_Retencion_IVA + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula + ", " +
                            Ope_Con_Solicitud_Pagos_Detalles.Campo_IEPS + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISH + ") VALUES('" + No_Solicitud_Pago + "'," +
                            "'" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "'," +
                            "'" + String.Format("{0:yyyyMMdd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "'," +
                            Datos.P_Dt_Facturas.Rows[Cont_Elementos]["TOTAL_FACTURA"].ToString().Trim() + ",'" + Datos.P_Usuario + "',GETDATE()," +
                            "'" + Datos.P_Proveedor + "', 'DOCUMENTO', '" + Partida_ID + "', '" + Fuente_Financiamiento_ID + "', '" + Proyecto_Programa_ID + "', " +
                            Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IVA_FACTURA"].ToString().Trim() + ", " +
                            "'" + Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_RFC].ToString().Trim() + "', " +
                            "'" + Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_CURP].ToString().Trim() + "', 'OTROS', 0, 0, 0, 0, 0)";

                        //Ejecutar consulta
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    //Agregar la solicitud de pago al contrarecibo
                    Mi_SQL = "UPDATE " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " SET " + Ope_Alm_Contrarecibos.Campo_No_Solicitud_Pago + " = " +
                        "'" + No_Solicitud_Pago + "' WHERE " + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = " + No_Contra_Recibo.ToString().Trim() + " ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Ejecutar transaccion
                    Trans.Commit();

                    //Entregar el numero de contrarecibo
                    return No_Contra_Recibo.ToString().Trim() + "," + No_Solicitud_Pago.ToString().Trim();
                }
                else
                {
                    Trans.Rollback();
                    throw new Exception("El proveedor no tiene datos.");
                }
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje); // Se indica el mensaje 
            }
            finally
            {
                Cn.Close();
            }
        }
        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Modificar_Contrarecibo
        /// DESCRIPCION:            Modificar un contrarecibo existente, asi como las facturas quer incluye
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            28/Enero/2013 11:39 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static void Modificar_Contrarecibo(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            String Mensaje = "";
            String Mi_SQL = String.Empty;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            Object aux; //Variable auxiliar 
            int Cont_Elementos = 0; //variable para el contador
            Int64 Factura_ID = 0; //variable para el ID de la factura

            try
            {
                //Asignar consulta
                Mi_SQL = "UPDATE OPE_ALM_CONTRARECIBOS SET FECHA_PAGO = '" + String.Format("{0:yyyy/MM/dd}", Datos.P_Fecha_Pago) + "', " + " ESTATUS = 'GENERADO', TIPO = 'COMPRAS'," +
                    "IMPORTE_TOTAL = " + Datos.P_Importe_Total.ToString().Trim() + ", USUARIO_MODIFICO = '" + Datos.P_Usuario + "', FECHA_MODIFICO = GETDATE() " +
                    "WHERE NO_CONTRA_RECIBO = " + Datos.P_No_Contra_Recibo.ToString().Trim();

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                //Borrar Facturas
                foreach (DataRow Dr_Renglon in Datos.P_Dt_Facturas_Eliminadas.Rows)
                {

                    Mi_SQL = "DELETE FROM " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + " WHERE " + Ope_Alm_Registro_Facturas.Campo_Factura_ID + " = " + Dr_Renglon["FACTURA_ID"].ToString().Trim();
                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Mi_SQL = "DELETE FROM " + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + " WHERE " + Ope_Alm_Facturas_Electronicas.Campo_Factura_Id + " = " + Dr_Renglon["FACTURA_ID"].ToString().Trim();
                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                //Ciclo para el barrido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Datos.P_Dt_Facturas.Rows.Count; Cont_Elementos++)
                {
                    //Verificar el tipo (alta, baja, cambio)
                    switch (Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ESTATUS"].ToString().Trim())
                    {
                        case "ALTA": //Consulta para darlo de alta
                            //COnuslta para un nuevo ID de la factura
                            Mi_SQL = "SELECT MAX(FACTURA_ID) FROM OPE_ALM_REGISTRO_FACTURAS";

                            //Ejecutar consulta
                            Cmd.CommandText = Mi_SQL;
                            aux = Cmd.ExecuteScalar();

                            //Verificar si no es nulo
                            if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                            {
                                Factura_ID = Convert.ToInt64(aux) + 1;
                            }
                            else
                            {
                                Factura_ID = 1;
                            }

                            Mi_SQL = "INSERT INTO OPE_ALM_REGISTRO_FACTURAS (FACTURA_ID, NO_FACTURA_PROVEEDOR, NO_CONTRA_RECIBO, IMPORTE_FACTURA, FECHA_FACTURA,NO_ORDEN_COMPRA," +
                                "USUARIO_CREO, FECHA_CREO) VALUES(" + Factura_ID.ToString().Trim() + "," +
                                "'" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "'," + Datos.P_No_Contra_Recibo.ToString().Trim() + "," +
                                Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IMPORTE"].ToString().Trim() + "," +
                                "'" + String.Format("{0:yyyy/dd/MM}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "'," + Datos.P_No_Orden_Compra + "," +
                                "'" + Datos.P_Usuario + "',GETDATE())";
                            //Verificar si la cadena SQL no es nula
                            if (String.IsNullOrEmpty(Mi_SQL) == false)
                            {
                                //Ejecutar consulta
                                Cmd.CommandText = Mi_SQL;
                                Cmd.ExecuteNonQuery();
                            }
                            //Registrar Facturas Electronicas
                            if (!String.IsNullOrEmpty(Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim()))
                            {
                                Mi_SQL = "INSERT INTO " + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + "( ";
                                Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Factura_Id;
                                Mi_SQL += "," + Ope_Alm_Facturas_Electronicas.Campo_Ruta;
                                Mi_SQL += "," + Ope_Alm_Facturas_Electronicas.Campo_Descripcion;
                                Mi_SQL += "," + Ope_Alm_Facturas_Electronicas.Campo_Usuario_Creo;
                                Mi_SQL += "," + Ope_Alm_Facturas_Electronicas.Campo_Fecha_Creo + " )";
                                Mi_SQL += " VALUES( ";
                                Mi_SQL += Factura_ID.ToString().Trim();
                                Mi_SQL += ",'" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim();
                                Mi_SQL += "','" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim();
                                Mi_SQL += "','" + Datos.P_Usuario + "',GETDATE() )";
                                //Verificar si la cadena SQL no es nula
                                if (String.IsNullOrEmpty(Mi_SQL) == false)
                                {
                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                }
                            }
                            break;

                        case "BAJA":
                            Mi_SQL = "DELETE FROM OPE_ALM_REGISTRO_FACTURAS WHERE FACTURA_ID = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                            //Verificar si la cadena SQL no es nula
                            if (String.IsNullOrEmpty(Mi_SQL) == false)
                            {
                                //Ejecutar consulta
                                Cmd.CommandText = Mi_SQL;
                                Cmd.ExecuteNonQuery();
                            }
                            break;

                        case "CAMBIO":

                            Mi_SQL = "UPDATE " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas;
                            Mi_SQL += " SET " + Ope_Alm_Registro_Facturas.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo.ToString().Trim();
                            Mi_SQL += " WHERE " + Ope_Alm_Registro_Facturas.Campo_Factura_ID + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();

                            Mi_SQL = "SELECT " + Ope_Alm_Facturas_Electronicas.Campo_Factura_Id + " FROM " + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas;
                            Mi_SQL += " WHERE " + Ope_Alm_Facturas_Electronicas.Campo_Factura_Id + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                            Cmd.CommandText = Mi_SQL;
                            aux = Cmd.ExecuteScalar();

                            //verificar si es nulo
                            if (aux == null || String.IsNullOrEmpty(aux.ToString().Trim()))
                            {
                                Mi_SQL = "INSERT INTO " + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + "( ";
                                Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Factura_Id + ", ";
                                Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Ruta + ", ";
                                Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Descripcion + ", ";
                                Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Usuario_Creo + ", ";
                                Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Fecha_Creo + " )";
                                Mi_SQL += " VALUES( ";
                                Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim() + " , '";
                                Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim() + "' , '";
                                Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim() + "' , '";
                                Mi_SQL += Datos.P_Usuario + "' , GETDATE() )";
                            }
                            else
                            {
                                Mi_SQL = "UPDATE " + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + " SET ";
                                Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Ruta + " = '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim() + "', ";
                                Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Descripcion + " = '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim() + "', ";
                                Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario + "', ";
                                Mi_SQL += Ope_Alm_Facturas_Electronicas.Campo_Fecha_Modifico + " = GETDATE() ";
                                Mi_SQL += " WHERE " + Ope_Alm_Facturas_Electronicas.Campo_Factura_Id + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                            }
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();

                            break;

                        default:
                            break;
                    }
                }

                //Ejecutar transaccion 
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje); // Se indica el mensaje 
            }
            finally
            {
                Cn.Close();
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Modificar_Contrarecibo_Taller
        /// DESCRIPCION:            Modificar un contrarecibo existente, asi como las facturas quer incluye
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            28/Enero/2013 11:39 
        /// MODIFICO          :     Jesus Toledo Rdz
        /// FECHA_MODIFICO    :     15/Abril/2013
        /// CAUSA_MODIFICACION:     Adecuar a Tablas de Taller Mecanico
        ///*******************************************************************************/
        public static void Modificar_Contrarecibo_Taller(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            String Mensaje = "";
            String Mi_SQL = String.Empty;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            Object aux; //Variable auxiliar 
            int Cont_Elementos = 0; //variable para el contador
            Int64 Factura_ID = 0; //variable para el ID de la factura

            try
            {
                //Asignar consulta
                Mi_SQL = "UPDATE " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " SET " + Ope_Alm_Contrarecibos.Campo_Fecha_Pago + " = '" + String.Format("{0:yyyy/MM/dd}", Datos.P_Fecha_Pago) + "', " +
                    "IMPORTE_TOTAL = " + Datos.P_Importe_Total.ToString().Trim() + ", " + Ope_Alm_Contrarecibos.Campo_Estatus + " = 'GENERADO'" + ", " + Ope_Alm_Contrarecibos.Campo_Tipo + " = 'TALLER'," + Ope_Alm_Contrarecibos.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario + "'," + Ope_Alm_Contrarecibos.Campo_Fecha_Modifico + " = GETDATE() " +
                    "WHERE " + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo.ToString().Trim();

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                //Borrar Facturas
                foreach (DataRow Dr_Renglon in Datos.P_Dt_Facturas_Eliminadas.Rows)
                {

                    Mi_SQL = "DELETE FROM " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas + " WHERE " + Ope_Tal_Registro_Facturas.Campo_Factura_ID + " = " + Dr_Renglon["FACTURA_ID"].ToString().Trim();
                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                foreach (DataRow Dr_Renglon in Datos.P_Dt_Facturas_Eliminadas.Rows)
                {

                    Mi_SQL = "DELETE FROM " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + " WHERE " + Ope_Alm_Registro_Facturas.Campo_Factura_ID + " = " + Dr_Renglon["FACTURA_ID"].ToString().Trim();
                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Mi_SQL = "DELETE FROM " + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + " WHERE " + Ope_Alm_Facturas_Electronicas.Campo_Factura_Id + " = " + Dr_Renglon["FACTURA_ID"].ToString().Trim();
                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                //Ciclo para el barrido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Datos.P_Dt_Facturas.Rows.Count; Cont_Elementos++)
                {
                    //Verificar el tipo (alta, baja, cambio)
                    switch (Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ESTATUS"].ToString().Trim())
                    {
                        case "ALTA": //Consulta para darlo de alta                                            

                            Mi_SQL = "";
                            //COnuslta para un nuevo ID de la factura
                            Mi_SQL = "SELECT MAX(" + Ope_Tal_Registro_Facturas.Campo_Factura_ID + ") FROM " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas;
                            //Ejecutar consulta
                            Cmd.CommandText = Mi_SQL;
                            aux = Cmd.ExecuteScalar();

                            //Verificar si no es nulo
                            if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                            {
                                Factura_ID = Convert.ToInt64(aux) + 1;
                            }
                            else
                            {
                                Factura_ID = 1;
                            }

                            Mi_SQL = "INSERT INTO " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas + "("
                                + Ope_Tal_Registro_Facturas.Campo_Factura_ID
                                + "," + Ope_Tal_Registro_Facturas.Campo_Factura_Proveedor
                                + "," + Ope_Tal_Registro_Facturas.Campo_No_Contra_Recibo
                                + "," + Ope_Tal_Registro_Facturas.Campo_Importe_Factura
                                + "," + Ope_Tal_Registro_Facturas.Campo_Fecha_Factura
                                + "," + Ope_Tal_Registro_Facturas.Campo_No_Orden_Compra
                                + "," + Ope_Tal_Registro_Facturas.Campo_Usuario_Creo
                                + "," + Ope_Tal_Registro_Facturas.Campo_Fecha_Creo + ") VALUES(" + Factura_ID.ToString().Trim() + "," +
                                "'" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "'," + Datos.P_No_Contra_Recibo.ToString().Trim() + "," +
                                Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IMPORTE"].ToString().Trim() + "," +
                                "'" + String.Format("{0:yyyy/MM/dd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "'," + Datos.P_No_Orden_Compra + "," +
                                "'" + Datos.P_Usuario + "',GETDATE())";

                            //Verificar si la cadena SQL no es nula
                            if (String.IsNullOrEmpty(Mi_SQL) == false)
                            {
                                //Ejecutar consulta
                                Cmd.CommandText = Mi_SQL;
                                Cmd.ExecuteNonQuery();
                            }
                            //Registrar Facturas Electronicas
                            if (!String.IsNullOrEmpty(Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim()))
                            {
                                Mi_SQL = "INSERT INTO " + Ope_Tal_Facturas_Electronicas.Tabla_Ope_Tal_Facturas_Electronicas + "( ";
                                Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Factura_Id;
                                Mi_SQL += "," + Ope_Tal_Facturas_Electronicas.Campo_Ruta;
                                Mi_SQL += "," + Ope_Tal_Facturas_Electronicas.Campo_Descripcion;
                                Mi_SQL += "," + Ope_Tal_Facturas_Electronicas.Campo_Tipo;
                                Mi_SQL += "," + Ope_Tal_Facturas_Electronicas.Campo_Usuario_Creo;
                                Mi_SQL += "," + Ope_Tal_Facturas_Electronicas.Campo_Fecha_Creo + " )";
                                Mi_SQL += " VALUES( ";
                                Mi_SQL += Factura_ID.ToString().Trim();
                                Mi_SQL += ",'" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim();
                                Mi_SQL += "','" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim();
                                Mi_SQL += "','TALLER'";
                                Mi_SQL += ",'" + Datos.P_Usuario + "',GETDATE() )";
                                //Verificar si la cadena SQL no es nula
                                if (String.IsNullOrEmpty(Mi_SQL) == false)
                                {
                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                }
                            }
                            break;

                        case "CAMBIO":

                            Mi_SQL = "UPDATE " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas;
                            Mi_SQL += " SET " + Ope_Tal_Registro_Facturas.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo.ToString().Trim();
                            Mi_SQL += " WHERE " + Ope_Tal_Registro_Facturas.Campo_Factura_ID + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();

                            Mi_SQL = "SELECT " + Ope_Tal_Facturas_Electronicas.Campo_Factura_Id + " FROM " + Ope_Tal_Facturas_Electronicas.Tabla_Ope_Tal_Facturas_Electronicas;
                            Mi_SQL += " WHERE " + Ope_Tal_Facturas_Electronicas.Campo_Factura_Id + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                            Cmd.CommandText = Mi_SQL;
                            aux = Cmd.ExecuteScalar();

                            //verificar si es nulo
                            if (aux == null || String.IsNullOrEmpty(aux.ToString().Trim()))
                            {
                                Mi_SQL = "INSERT INTO " + Ope_Tal_Facturas_Electronicas.Tabla_Ope_Tal_Facturas_Electronicas + "( ";
                                Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Factura_Id + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Ruta + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Descripcion + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Tipo + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Usuario_Creo + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Fecha_Creo + " )";
                                Mi_SQL += " VALUES( ";
                                Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim() + " , '";
                                Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim() + "' , '";
                                Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim() + "' , 'TALLER', '";
                                Mi_SQL += Datos.P_Usuario + "' , GETDATE() )";
                            }
                            else
                            {
                                Mi_SQL = "UPDATE " + Ope_Tal_Facturas_Electronicas.Tabla_Ope_Tal_Facturas_Electronicas + " SET ";
                                Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Ruta + " = '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim() + "', ";
                                Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Descripcion + " = '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim() + "', ";
                                Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario + "', ";
                                Mi_SQL += Ope_Tal_Facturas_Electronicas.Campo_Fecha_Modifico + " = GETDATE() ";
                                Mi_SQL += " WHERE " + Ope_Tal_Facturas_Electronicas.Campo_Factura_Id + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                            }
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();

                            break;

                        default:
                            break;
                    }
                }

                //Ejecutar transaccion 
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje); // Se indica el mensaje 
            }
            finally
            {
                Cn.Close();
            }
        }
        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Modificar_Contrarecibo_Taller
        /// DESCRIPCION:            Modificar un contrarecibo existente, asi como las facturas quer incluye
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            28/Enero/2013 11:39 
        /// MODIFICO          :     Jesus Toledo Rdz
        /// FECHA_MODIFICO    :     15/Abril/2013
        /// CAUSA_MODIFICACION:     Adecuar a Tablas de Taller Mecanico
        ///*******************************************************************************/
        public static void Modificar_Contrarecibo_Taller_Externo(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            String Mensaje = "";
            String Mi_SQL = String.Empty;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            Object aux; //Variable auxiliar 
            int Cont_Elementos = 0; //variable para el contador
            Int64 Factura_ID = 0; //variable para el ID de la factura
            String No_Servicio = "";
            String Tipo_Servicio = "";
            String No_Asignacion = "";
            DataTable Dt_Aux = new DataTable();
            try
            {
                //Asignar consulta
                Mi_SQL = "UPDATE " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " SET " + Ope_Alm_Contrarecibos.Campo_Fecha_Pago + " = '" + String.Format("{0:yyyy/MM/dd}", Datos.P_Fecha_Pago) + "', " +
                    "IMPORTE_TOTAL = " + Datos.P_Importe_Total.ToString().Trim() + ", " + Ope_Alm_Contrarecibos.Campo_Estatus + " = 'GENERADO'," + Ope_Alm_Contrarecibos.Campo_Tipo + " = 'TALLER', " + Ope_Alm_Contrarecibos.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario + "'," + Ope_Alm_Contrarecibos.Campo_Fecha_Modifico + " = GETDATE() " +
                    "WHERE " + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo.ToString().Trim();

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                //Borrar Facturas
                foreach (DataRow Dr_Renglon in Datos.P_Dt_Facturas_Eliminadas.Rows)
                {

                    Mi_SQL = "DELETE FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio + " WHERE " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + " = " + Dr_Renglon["FACTURA_ID"].ToString().Trim();
                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                //Obtener Numero Servicio y Tipo
                Mi_SQL = " SELECT " + Vw_Tal_Servicios.Campo_No_servicio + ","
                    + Vw_Tal_Servicios.Campo_Tipo_Servicio
                    + " FROM " + Vw_Tal_Servicios.Vista_Vw_Tal_Servicios
                    + " WHERE CAST(" + Vw_Tal_Servicios.Campo_Folio + " AS INT) = " + Datos.P_No_Orden_Compra;
                Cmd.CommandText = Mi_SQL;
                da.SelectCommand = Cmd;
                da.Fill(Dt_Aux);
                if (Dt_Aux.Rows.Count > 0)
                {
                    No_Servicio = Dt_Aux.Rows[0][Vw_Tal_Servicios.Campo_No_servicio].ToString();
                    Tipo_Servicio = Dt_Aux.Rows[0][Vw_Tal_Servicios.Campo_Tipo_Servicio].ToString();
                }
                //Obtener Numero de Asinacion de Proveedor
                Mi_SQL = " SELECT " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + ","
                    + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio
                    + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;
                Mi_SQL += " WHERE " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = " + No_Servicio.Trim();
                Mi_SQL += " AND " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = '" + Tipo_Servicio.Trim() + "'";
                Dt_Aux = new DataTable();
                Cmd.CommandText = Mi_SQL;
                da.SelectCommand = Cmd;
                da.Fill(Dt_Aux);
                if (Dt_Aux.Rows.Count > 0)
                {
                    No_Asignacion = Dt_Aux.Rows[0][Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion].ToString();
                }

                foreach (DataRow Dr_Renglon in Datos.P_Dt_Facturas_Eliminadas.Rows)
                {
                    Mi_SQL = "DELETE FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio + " WHERE " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + " = " + Dr_Renglon["FACTURA_ID"].ToString().Trim();
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                //Ciclo para el barrido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Datos.P_Dt_Facturas.Rows.Count; Cont_Elementos++)
                {
                    //Verificar el tipo (alta, baja, cambio)
                    switch (Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ESTATUS"].ToString().Trim())
                    {
                        case "ALTA": //Consulta para darlo de alta
                            //COnuslta para un nuevo ID de la factura
                            Mi_SQL = "SELECT MAX( " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + ") FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio;
                            Cmd.CommandText = Mi_SQL;
                            aux = Cmd.ExecuteScalar();

                            Factura_ID = String.IsNullOrEmpty(aux.ToString().Trim()) ? 1 : Convert.ToInt64(aux) + 1;

                            Mi_SQL = "INSERT INTO " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio
                                + "(" + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + ","
                                + Ope_Tal_Facturas_Servicio.Campo_No_Asignacion + ","
                                + Ope_Tal_Facturas_Servicio.Campo_No_Factura + ","
                                + Ope_Tal_Facturas_Servicio.Campo_No_Servicio + ","
                                + Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio + ", "
                                + Ope_Tal_Facturas_Servicio.Campo_Factura + ","
                                + Ope_Tal_Facturas_Servicio.Campo_Monto_Factura + ", "
                                + Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo + ", "
                                + Ope_Tal_Facturas_Servicio.Campo_Fecha_Factura + ","
                                + Ope_Tal_Facturas_Servicio.Campo_Ruta_Archivo + ", "
                                + Ope_Tal_Facturas_Servicio.Campo_Comentarios + ","
                                + Ope_Tal_Facturas_Servicio.Campo_Usuario_Creo + ","
                                + Ope_Tal_Facturas_Servicio.Campo_Fecha_Creo
                                + ") VALUES(" + Factura_ID.ToString().Trim() + "," + No_Asignacion + "," + Cont_Elementos + 1 + "," + No_Servicio + ",'" + Tipo_Servicio + "'," +
                                "'" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "',"
                                + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IMPORTE"].ToString().Trim() + "," + Datos.P_No_Contra_Recibo.ToString().Trim() + "," +
                                "'" + String.Format("{0:yyyyMMdd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "', '"
                                + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"] + "', '"
                                + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"] + "', '" +
                                Datos.P_Usuario + "',GETDATE())";
                            //Verificar si la cadena SQL no es nula
                            if (String.IsNullOrEmpty(Mi_SQL) == false)
                            {
                                //Ejecutar consulta
                                Cmd.CommandText = Mi_SQL;
                                Cmd.ExecuteNonQuery();
                            }
                            break;

                        case "CAMBIO":
                            Mi_SQL = "SELECT " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + " FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio;
                            Mi_SQL += " WHERE " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                            Cmd.CommandText = Mi_SQL;
                            aux = Cmd.ExecuteScalar();

                            //verificar si es nulo
                            if (aux == null || String.IsNullOrEmpty(aux.ToString().Trim()))
                            {
                                //COnuslta para un nuevo ID de la factura
                                Mi_SQL = "SELECT MAX( " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + ") FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio;
                                Cmd.CommandText = Mi_SQL;
                                aux = Cmd.ExecuteScalar();

                                Factura_ID = String.IsNullOrEmpty(aux.ToString().Trim()) ? 1 : Convert.ToInt64(aux) + 1;

                                Mi_SQL = "INSERT INTO " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio + "(";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_No_Asignacion + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_No_Factura + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_No_Servicio + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Factura + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Monto_Factura + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Fecha_Factura + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Ruta_Archivo + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Comentarios + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Usuario_Creo + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Fecha_Creo + ") VALUES(";
                                Mi_SQL += Factura_ID.ToString().Trim() + ", ";
                                Mi_SQL += No_Asignacion + ", ";
                                Mi_SQL += Cont_Elementos + 1 + ", ";
                                Mi_SQL += No_Servicio + ", '";
                                Mi_SQL += Tipo_Servicio + "', '";
                                Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "', ";
                                Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IMPORTE"].ToString().Trim() + ", ";
                                Mi_SQL += Datos.P_No_Contra_Recibo.ToString().Trim() + ", '";
                                Mi_SQL += String.Format("{0:yyyyMMdd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "', '";
                                Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"] + "', '";
                                Mi_SQL += Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"] + "', '";
                                Mi_SQL += Datos.P_Usuario + "',GETDATE())";
                            }
                            else
                            {
                                Mi_SQL = "UPDATE " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio + " SET ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Ruta_Archivo + " = '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ARCHIVO_XML"].ToString().Trim() + "', ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Comentarios + " = '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["DESCRIPCION"].ToString().Trim() + "', ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo + ", ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario + "', ";
                                Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Fecha_Modifico + " = GETDATE() ";
                                Mi_SQL += " WHERE " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + " = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                            }
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            break;
                    }
                }

                //Ejecutar transaccion 
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje); // Se indica el mensaje 
            }
            finally
            {
                Cn.Close();
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Contrarecibos
        /// DESCRIPCION:            Consultar los contrarecibos
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            28/Enero/2013 18:59 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Contrarecibos(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //Tabla para el resultado

            try
            {
                    //Asignar consulta
                Mi_SQL = "SELECT NO_ORDEN_COMPRA,OC,NO_CONTRA_RECIBO,FECHA_RECEPCION,FECHA_PAGO,PROVEEDOR NOMBRE_PROVEEDOR,IMPORTE_TOTAL TOTAL,TIPO,ESTATUS FROM( ";
                Mi_SQL = Mi_SQL + " SELECT CAST(OPE_COM_ORDENES_COMPRA.NO_ORDEN_COMPRA AS CHAR) NO_ORDEN_COMPRA, OPE_COM_ORDENES_COMPRA.FOLIO AS OC, OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO NO_CONTRA_RECIBO, " +
                        "OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION FECHA_RECEPCION, OPE_ALM_CONTRARECIBOS.FECHA_PAGO FECHA_PAGO, CAT_COM_PROVEEDORES.NOMBRE AS PROVEEDOR, OPE_ALM_CONTRARECIBOS.IMPORTE_TOTAL IMPORTE_TOTAL,OPE_ALM_CONTRARECIBOS.ESTATUS,'COMPRAS' TIPO " +
                        " FROM OPE_COM_ORDENES_COMPRA " +
                        "INNER JOIN CAT_COM_PROVEEDORES ON OPE_COM_ORDENES_COMPRA.PROVEEDOR_ID = CAT_COM_PROVEEDORES.PROVEEDOR_ID " +
                        "LEFT JOIN OPE_ALM_CONTRARECIBOS ON OPE_COM_ORDENES_COMPRA.NO_CONTRA_RECIBO = OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO " +
                        "WHERE (OPE_COM_ORDENES_COMPRA.ESTATUS IN ('SURTIDA_PARCIAL','SURTIDA_REGISTRO','SURTIDA_FACTURA','SURTIDA_DATOS','SURTIDA','PARCIAL','SURTIDA','COMPLETA','SURTIDA / RESGUARDO-CUSTODIA') " +
                        "OR OPE_COM_ORDENES_COMPRA.ESTATUS = 'SURTIDA_FACTURA') AND OPE_COM_ORDENES_COMPRA.TIPO_ARTICULO IN ('PRODUCTO')";
                    //Verificar si se tiene el numero de contrarecibo
                    if (Datos.P_No_Contra_Recibo > 0 || Datos.P_No_Orden_Compra != "")
                    {
                        //Verificar si fue busqueda de contrarecibo u orden compra
                        if (Datos.P_No_Contra_Recibo > 0)
                        {
                            Mi_SQL += "AND OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                        }
                        else
                        {
                            Mi_SQL += "AND OPE_COM_ORDENES_COMPRA.NO_ORDEN_COMPRA = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";
                        }
                    }
                    else
                    {
                        //Verificar los filtros
                        //Proveedor                        
                        if (String.IsNullOrEmpty(Datos.P_Proveedor_ID) == false)
                        {
                            Mi_SQL += "AND OPE_COM_ORDENES_COMPRA.PROVEEDOR_ID = '" + Datos.P_Proveedor_ID + "' ";
                        }
                        //Estatus
                        if (String.IsNullOrEmpty(Datos.P_Estatus) == false)
                        {
                            //Filtro de Estatus incluir estatus nulos
                            if (Datos.P_Incluir_Estatus_Pendientes)
                                Mi_SQL += "AND (OPE_ALM_CONTRARECIBOS.ESTATUS IN ('" + Datos.P_Estatus + "') OR OPE_ALM_CONTRARECIBOS.ESTATUS IS NULL ) ";
                            else
                                Mi_SQL += "AND OPE_ALM_CONTRARECIBOS.ESTATUS IN ('" + Datos.P_Estatus + "') ";
                        }

                        //Rango de fechas
                        if (Datos.P_Fecha_Inicio != "01/01/1900")
                        {
                            //Fecha final
                            if (Datos.P_Fecha_Fin != "01/01/1900")
                            {
                                Mi_SQL += "AND OPE_COM_ORDENES_COMPRA.FECHA_ENTREGA >= '" + Datos.P_Fecha_Inicio + " 00:00:00' " +
                                    "AND OPE_COM_ORDENES_COMPRA.FECHA_ENTREGA <= '" + Datos.P_Fecha_Fin + " 23:59:59' ";
                            }
                            else
                            {
                                Mi_SQL += " AND OPE_COM_ORDENES_COMPRA.FECHA_ENTREGA >= '" + Datos.P_Fecha_Inicio + " 00:00:00' " +
                                    "AND OPE_COM_ORDENES_COMPRA.FECHA_ENTREGA <= '" + String.Format("{0:dd/MM/yyyy}", DateTime.Now) + " 23:59:59' ";
                            }
                        }
                    }
                //Tipo de contrarecibo
                if (String.IsNullOrEmpty(Datos.P_Tipo_OC) == false)
                    Mi_SQL += "AND OPE_ALM_CONTRARECIBOS.TIPO IN ('" + Datos.P_Tipo_OC + "') ";


                Mi_SQL += " UNION";


                Mi_SQL = Mi_SQL + " SELECT CAST(OPE_COM_ORDENES_COMPRA.NO_ORDEN_COMPRA AS CHAR) NO_ORDEN_COMPRA, OPE_COM_ORDENES_COMPRA.FOLIO AS OC, OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO NO_CONTRA_RECIBO, " +
                        "OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION FECHA_RECEPCION, OPE_ALM_CONTRARECIBOS.FECHA_PAGO FECHA_PAGO, CAT_COM_PROVEEDORES.NOMBRE AS PROVEEDOR, OPE_ALM_CONTRARECIBOS.IMPORTE_TOTAL IMPORTE_TOTAL,OPE_ALM_CONTRARECIBOS.ESTATUS,'SERVICIOS GENERALES' TIPO " +
                        " FROM OPE_COM_ORDENES_COMPRA " +
                        "INNER JOIN CAT_COM_PROVEEDORES ON OPE_COM_ORDENES_COMPRA.PROVEEDOR_ID = CAT_COM_PROVEEDORES.PROVEEDOR_ID " +
                        "LEFT JOIN OPE_ALM_CONTRARECIBOS ON OPE_COM_ORDENES_COMPRA.NO_CONTRA_RECIBO = OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO " +
                        "WHERE OPE_COM_ORDENES_COMPRA.ESTATUS IN ('FACTURA REGISTRADA') AND OPE_COM_ORDENES_COMPRA.TIPO_ARTICULO IN ('SERVICIO')";
                //Verificar si se tiene el numero de contrarecibo
                if (Datos.P_No_Contra_Recibo > 0 || Datos.P_No_Orden_Compra != "")
                {
                    //Verificar si fue busqueda de contrarecibo u orden compra
                    if (Datos.P_No_Contra_Recibo > 0)
                    {
                        Mi_SQL += "AND OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                    }
                    else
                    {
                        Mi_SQL += "AND OPE_COM_ORDENES_COMPRA.NO_ORDEN_COMPRA = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";
                    }
                }
                else
                {
                    //Verificar los filtros
                    //Proveedor                        
                    if (String.IsNullOrEmpty(Datos.P_Proveedor_ID) == false)
                    {
                        Mi_SQL += "AND OPE_COM_ORDENES_COMPRA.PROVEEDOR_ID = '" + Datos.P_Proveedor_ID + "' ";
                    }
                    //Estatus
                    if (String.IsNullOrEmpty(Datos.P_Estatus) == false)
                    {
                        //Filtro de Estatus incluir estatus nulos
                        if (Datos.P_Incluir_Estatus_Pendientes)
                            Mi_SQL += "AND (OPE_ALM_CONTRARECIBOS.ESTATUS IN ('" + Datos.P_Estatus + "') OR OPE_ALM_CONTRARECIBOS.ESTATUS IS NULL ) ";
                        else
                            Mi_SQL += "AND OPE_ALM_CONTRARECIBOS.ESTATUS IN ('" + Datos.P_Estatus + "') ";
                    }

                    //Rango de fechas
                    if (Datos.P_Fecha_Inicio != "01/01/1900")
                    {
                        //Fecha final
                        if (Datos.P_Fecha_Fin != "01/01/1900")
                        {
                            Mi_SQL += "AND OPE_COM_ORDENES_COMPRA.FECHA_ENTREGA >= '" + Datos.P_Fecha_Inicio + " 00:00:00' " +
                                "AND OPE_COM_ORDENES_COMPRA.FECHA_ENTREGA <= '" + Datos.P_Fecha_Fin + " 23:59:59' ";
                        }
                        else
                        {
                            Mi_SQL += " AND OPE_COM_ORDENES_COMPRA.FECHA_ENTREGA >= '" + Datos.P_Fecha_Inicio + " 00:00:00' " +
                                "AND OPE_COM_ORDENES_COMPRA.FECHA_ENTREGA <= '" + String.Format("{0:dd/MM/yyyy}", DateTime.Now) + " 23:59:59' ";
                        }
                    }
                }
                //Tipo de contrarecibo
                if (String.IsNullOrEmpty(Datos.P_Tipo_OC) == false)
                    Mi_SQL += "AND OPE_ALM_CONTRARECIBOS.TIPO IN ('" + Datos.P_Tipo_OC + "') ";


                    //Union con Ordenes de Compra de Taller Mecanico
                    Mi_SQL += " UNION ";

                    Mi_SQL += " SELECT CAST(ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " AS CHAR) NO_ORDEN_COMPRA";
                        Mi_SQL += " ,ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Folio + " OC";
                        Mi_SQL += " ,CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " NO_CONTRA_RECIBO";
                        Mi_SQL += " ,CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion + " FECHA_RECEPCION";
                        Mi_SQL += " ,CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Pago + " FECHA_PAGO";
                        Mi_SQL += " ,PROVEEDORES." + Cat_Com_Proveedores.Campo_Compañia + " PROVEEDOR";
                        Mi_SQL += " ,CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Importe_Total + " IMPORTE_TOTAL";
                        Mi_SQL += " ,CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Estatus + " ESTATUS";
                        Mi_SQL += " ,'TALLER' TIPO";
                        Mi_SQL += " FROM " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " ORDEN_COMPRA";
                        //Conjuncion con tablas Proveedores y Contrarecibos
                        Mi_SQL += " INNER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDORES ON ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Proveedor_ID + " = PROVEEDORES." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                        Mi_SQL += " LEFT JOIN " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " CONTRARECIBOS ON ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_No_Factura_Interno + " = CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo;
                        Mi_SQL += " WHERE( ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Estatus + " IN ('SURTIDA') OR ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Estatus + " = 'SURTIDA_FACTURA')";
                        //Filtros Para consulta de OC de Taller Mecanico
                        //Verificar si se tiene el numero de contrarecibo
                        if (Datos.P_No_Contra_Recibo > 0 || Datos.P_No_Orden_Compra != "")
                        {
                            //Verificar si fue busqueda de contrarecibo u orden compra
                            if (Datos.P_No_Contra_Recibo > 0)
                                Mi_SQL += "AND CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                            else
                                Mi_SQL += "AND ORDEN_COMPRA. " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";
                        }
                        else
                        {
                            //Verificar los filtros
                            //Proveedor
                            if (String.IsNullOrEmpty(Datos.P_Proveedor_ID) == false)
                                Mi_SQL += "AND ORDEN_COMPRA. " + Ope_Tal_Ordenes_Compra.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "' ";                            
                            //Estatus
                            if (String.IsNullOrEmpty(Datos.P_Estatus) == false)
                                //Filtro de Estatus incluir estatus nulos
                                if (Datos.P_Incluir_Estatus_Pendientes)
                                    Mi_SQL += "AND (CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Estatus + " IN ('" + Datos.P_Estatus + "') OR CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Estatus + " IS NULL ) ";
                                else
                                    Mi_SQL += "AND CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Estatus + " IN ('" + Datos.P_Estatus + "') ";

                            //Rango de fechas
                            if (Datos.P_Fecha_Inicio != "01/01/1900")
                            {
                                //Fecha final
                                if (Datos.P_Fecha_Fin != "01/01/1900")
                                {
                                    Mi_SQL += " AND ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Entrega + " >= '" + Datos.P_Fecha_Inicio + " 00:00:00' " +
                                        "AND ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Entrega + " <= '" + Datos.P_Fecha_Fin + " 23:59:59' ";
                                }
                                else
                                {
                                    Mi_SQL += " AND ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Entrega + " >= '" + Datos.P_Fecha_Inicio + " 00:00:00' " +
                                        "AND ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Entrega + " <= '" + String.Format("{0:dd/MM/yyyy}", DateTime.Now) + " 23:59:59' ";
                                }
                            }
                        }
                        if (String.IsNullOrEmpty(Datos.P_Tipo_OC) == false)
                                Mi_SQL += "AND CONTRARECIBOS. " + Ope_Alm_Contrarecibos.Campo_Tipo + " IN ('" + Datos.P_Tipo_OC + "') ";
                        //Union con Ordenes de Compra de Taller Mecanico (Servicios Externos)
                        Mi_SQL += " UNION ";
                        Mi_SQL += " SELECT CAST(FAC." + Ope_Tal_Facturas_Servicio.Campo_Folio_Solicitud + " AS CHAR) NO_ORDEN_COMPRA";
                        //Mi_SQL += "SELECT '0' NO_ORDEN_COMPRA";
                        Mi_SQL += " ,'OS-' + CAST(fac." + Ope_Tal_Facturas_Servicio.Campo_Folio_Solicitud + " AS CHAR) OC";
                        Mi_SQL += " ,c." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " NO_CONTRA_RECIBO";
                        Mi_SQL += " ,c." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion + " FECHA_RECEPCION";
                        Mi_SQL += " ,c." + Ope_Alm_Contrarecibos.Campo_Fecha_Pago + " FECHA_PAGO";
                        Mi_SQL += " ,p." + Cat_Com_Proveedores.Campo_Compañia + " PROVEEDOR";
                        Mi_SQL += " ,c." + Ope_Alm_Contrarecibos.Campo_Importe_Total + " IMPORTE_TOTAL";
                        Mi_SQL += " ,c." + Ope_Alm_Contrarecibos.Campo_Estatus + " ESTATUS";
                        Mi_SQL += " ,CASE fac." + Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio +"  when 'SERVICIO_PREVENTIVO' THEN 'TALLER_EXTERNO' when 'SERVICIO_CORRECTIVO' THEN 'TALLER_EXTERNO' when 'TARJETAS_GASOLINA' THEN 'TARJETAS GASOLINA' END  TIPO";
                        Mi_SQL += " FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio + " fac";
                        //Conjuncion con tablas Proveedores y Contrarecibos
                        Mi_SQL += " INNER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " p ON fac." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + " = p." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                        Mi_SQL += " LEFT JOIN " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " c ON fac." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Contra_Recibo + " = c." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo;
                        //Mi_SQL += " JOIN " + Vw_Tal_Servicios.Vista_Vw_Tal_Servicios + " v ON a." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = v." + Vw_Tal_Servicios.Campo_No_servicio + " AND a." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = v." + Vw_Tal_Servicios.Campo_Tipo_Servicio;
                        if(Datos.P_No_Contra_Recibo <= 0)
                            Mi_SQL += " WHERE fac." + Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo + " is null";
                        else
                            Mi_SQL += " WHERE fac." + Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo + " is not null ";
                        //Filtros Para consulta de OC de Taller Mecanico
                        //Verificar si se tiene el numero de contrarecibo
                        if (Datos.P_No_Contra_Recibo >0)
                        {
                            Mi_SQL += " AND fac." + Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo.ToString().Trim();
                        }
                        if (Datos.P_No_Orden_Compra != "")
                        {
                            //Verificar si fue busqueda de contrarecibo u orden compra
                            Mi_SQL += " AND fac. " + Ope_Tal_Facturas_Servicio.Campo_Folio_Solicitud + " like '%" + Datos.P_No_Orden_Compra.ToString().Trim() + "%' ";
                        }
                        else
                        {
                            //Verificar los filtros
                            //Proveedor
                            if (String.IsNullOrEmpty(Datos.P_Proveedor_ID) == false)
                                Mi_SQL += " AND fac. " + Ope_Tal_Facturas_Servicio.Campo_Proveedor_Id + " = '" + Datos.P_Proveedor_ID + "' ";                            
                            //Estatus
                            if (String.IsNullOrEmpty(Datos.P_Estatus) == false)
                                //Filtro de Estatus incluir estatus nulos
                                if (Datos.P_Incluir_Estatus_Pendientes)
                                    Mi_SQL += " AND (c." + Ope_Alm_Contrarecibos.Campo_Estatus + " IN ('" + Datos.P_Estatus + "') OR c." + Ope_Alm_Contrarecibos.Campo_Estatus + " IS NULL ) ";
                                else
                                    Mi_SQL += " AND c." + Ope_Alm_Contrarecibos.Campo_Estatus + " IN ('" + Datos.P_Estatus + "') ";

                            //Rango de fechas
                            if (Datos.P_Fecha_Inicio != "01/01/1900")
                            {
                                //Fecha final
                                if (Datos.P_Fecha_Fin != "01/01/1900")
                                {
                                    Mi_SQL += " AND fac." + Ope_Tal_Facturas_Servicio.Campo_Fecha_Creo + " >= '" + Datos.P_Fecha_Inicio + " 00:00:00' " +
                                        " AND fac." + Ope_Tal_Facturas_Servicio.Campo_Fecha_Creo + " <= '" + Datos.P_Fecha_Fin + " 23:59:59' ";
                                }
                                else
                                {
                                    Mi_SQL += " AND fac." + Ope_Tal_Facturas_Servicio.Campo_Fecha_Creo + " >= '" + Datos.P_Fecha_Inicio + " 00:00:00' " +
                                        " AND fac." + Ope_Tal_Facturas_Servicio.Campo_Fecha_Creo + " <= '" + String.Format("{0:dd/MM/yyyy}", DateTime.Now) + " 23:59:59' ";
                                }
                            }
                        }
                        if (String.IsNullOrEmpty(Datos.P_Tipo_OC) == false)
                                Mi_SQL += " AND c. " + Ope_Alm_Contrarecibos.Campo_Tipo + " IN ('" + Datos.P_Tipo_OC + "') ";
                        //Orden de la consulta
                        Mi_SQL += ") TABLA_COMPUESTA";// " ORDER BY NO_CONTRA_RECIBO,NO_ORDEN_COMPRA";
                    
                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Proveedores
        /// DESCRIPCION:            Consultar los proveedores
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            30/Enero/2013 11:50 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Proveedores(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DataTable Dt_Proveedores = new DataTable(); //tabla para el resultado de la consulta

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT PROVEEDOR_ID, COMPANIA AS PROVEEDOR FROM CAT_COM_PROVEEDORES " +
                    "WHERE NOMBRE COLLATE SQL_Latin1_General_CP850_CI_AI LIKE '%" + Datos.P_Busqueda + "%' " +
                    "OR COMPANIA COLLATE SQL_Latin1_General_CP850_CI_AI LIKE '%" + Datos.P_Busqueda + "%' " +
                    "ORDER BY COMPANIA ";

                //Ejecutar consulta
                Dt_Proveedores = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Proveedores;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }


        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Datos_Contrarecibo_Compras
        /// DESCRIPCION:            Consultar los datos de los contrarecibos
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Susana Trigueros Armenta  
        /// FECHA_CREO :            30/Enero/2013 14:00 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataSet Consulta_Datos_Contrarecibo_Compras(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            DataTable Dt_Cabecera = new DataTable(); //tabla para los datos de la cabecera
            DataTable Dt_Detalles = new DataTable(); //Tabla para los detalles
            DataTable Dt_Aux = new DataTable(); //tabla auxiliar para las consultas
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataSet Ds_Resultado = new DataSet(); //dataset para el resultado

            try
            {
                //Consulta para los datos de la cabecera
                Mi_SQL = "SELECT " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + ", " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Estatus + ", " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion + ", " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Tipo + ", " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Fecha_Pago + ", " +
                    Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR, " +
                    Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + ", " +
                    Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + ", " +
                    Ope_Alm_Contrarecibos.Campo_No_Solicitud_Pago + " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " " +
                    "LEFT JOIN " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " ON " +
                    Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Factura_Interno + " = " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " " +
                    "INNER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ON " +
                    Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + " = " +
                    Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + " ";

                //Verificar si hay un contrarecibo
                if (Datos.P_No_Contra_Recibo > 0)
                {
                    if (Mi_SQL.Contains("WHERE")) Mi_SQL += " AND "; else Mi_SQL += " WHERE ";
                    Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Factura_Interno +
                        " = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                }

                //Verificar si hay una orden de compra
                if (Datos.P_No_Orden_Compra != "")
                {
                    if (Mi_SQL.Contains("WHERE")) Mi_SQL += " AND "; else Mi_SQL += " WHERE ";
                    Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra +
                        " = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";
                }

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar los datos a la tabla de la cabecera
                Dt_Cabecera = Dt_Aux.Copy();

                //Consulta para los detalles del contrarecibo
                Mi_SQL = "SELECT " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_Factura_ID + ", " +
                    Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_Factura_Proveedor + ", " +
                    Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_Importe_Factura + " AS IMPORTE, " +
                    "ISNULL(" + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + "." + Ope_Alm_Facturas_Electronicas.Campo_Ruta + ", '') AS ARCHIVO_XML, " +
                    "ESTATUS = 'ORIGINAL', " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_Fecha_Factura + ", " +
                    "ISNULL(" + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + "." + Ope_Alm_Facturas_Electronicas.Campo_Descripcion + ", '') AS DESCRIPCION, " +
                    "'" + Datos.P_Tipo_OC + "' AS TIPO_ORDEN_COMPRA, " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_IVA_Factura + ", " +
                    Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_Total_Factura + " " +
                    "FROM " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + " " +
                    "LEFT JOIN " + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + " ON " +
                    Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_Factura_ID + " = " +
                    Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + "." + Ope_Alm_Facturas_Electronicas.Campo_Factura_Id + " ";

                //Verificar si hay un contrarecibo
                if (Datos.P_No_Contra_Recibo > 0)
                {
                    if (Mi_SQL.Contains("WHERE")) Mi_SQL += " AND "; else Mi_SQL += " WHERE ";
                    Mi_SQL += Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_No_Contra_Recibo +
                        " =  " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                }

                //Verificar si hay una orden de compra
                if (Datos.P_No_Orden_Compra != "")
                {
                    if (Mi_SQL.Contains("WHERE")) Mi_SQL += " AND "; else Mi_SQL += " WHERE ";
                    Mi_SQL += Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_No_Orden_Compra +
                        " = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";
                }

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar los datos a la tabla de los detalles
                Dt_Detalles = Dt_Aux.Copy();

                //Colocat tablas en el dataset:
                Dt_Cabecera.TableName = "Cabecera";
                Dt_Detalles.TableName = "Detalles";
                Ds_Resultado.Tables.Add(Dt_Cabecera);
                Ds_Resultado.Tables.Add(Dt_Detalles);

                //Entregar resultado
                return Ds_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Datos_Contrarecibo
        /// DESCRIPCION:            Consultar los datos de los contrarecibos
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            30/Enero/2013 14:00 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataSet Consulta_Datos_Contrarecibo(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            DataTable Dt_Cabecera = new DataTable(); //tabla para los datos de la cabecera
            DataTable Dt_Detalles = new DataTable(); //Tabla para los detalles
            DataTable Dt_Aux = new DataTable(); //tabla auxiliar para las consultas
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataSet Ds_Resultado = new DataSet(); //dataset para el resultado
            
            try
            {
                //Consulta para los datos de la cabecera
                Mi_SQL = "SELECT " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + ", " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Estatus + ", " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion + ", " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Tipo + ", " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Fecha_Pago + ", " +
                    Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR, " +
                    Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + ", " +
                    Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + ", " +
                    Ope_Alm_Contrarecibos.Campo_No_Solicitud_Pago + " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " " +
                    "LEFT JOIN " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " ON " +
                    Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Factura_Interno + " = " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " " +
                    "INNER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ON " +
                    Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + " = " +
                    Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + " ";

                //Verificar si hay un contrarecibo
                if (Datos.P_No_Contra_Recibo > 0)
                {
                    if (Mi_SQL.Contains("WHERE")) Mi_SQL += " AND "; else Mi_SQL += " WHERE ";
                    Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Factura_Interno +
                        " = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                }

                //Verificar si hay una orden de compra
                if (Datos.P_No_Orden_Compra != "")
                {
                    if (Mi_SQL.Contains("WHERE")) Mi_SQL += " AND "; else Mi_SQL += " WHERE ";
                    Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra +
                        " = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";
                }

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar los datos a la tabla de la cabecera
                Dt_Cabecera = Dt_Aux.Copy();

                //Consulta para los detalles del contrarecibo
                Mi_SQL = "SELECT " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_Factura_ID + ", " +
                    Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_Factura_Proveedor + ", " +
                    Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_Importe_Factura + " AS IMPORTE, " +
                    "ISNULL(" + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + "." + Ope_Alm_Facturas_Electronicas.Campo_Ruta + ", '') AS ARCHIVO_XML, " +
                    "ESTATUS = 'ORIGINAL', " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_Fecha_Factura + ", " +
                    "ISNULL(" + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + "." + Ope_Alm_Facturas_Electronicas.Campo_Descripcion + ", '') AS DESCRIPCION, " +
                    "'" + Datos.P_Tipo_OC + "' AS TIPO_ORDEN_COMPRA, " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_IVA_Factura + ", " +
                    Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_Total_Factura + " " +
                    "FROM " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + " " +
                    "LEFT JOIN " + Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + " ON " +
                    Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_Factura_ID + " = " +
                    Ope_Alm_Facturas_Electronicas.Tabla_Ope_Alm_Facturas_Electronicas + "." + Ope_Alm_Facturas_Electronicas.Campo_Factura_Id + " ";

                //Verificar si hay un contrarecibo
                if (Datos.P_No_Contra_Recibo > 0)
                {
                    if (Mi_SQL.Contains("WHERE")) Mi_SQL += " AND "; else Mi_SQL += " WHERE ";
                    Mi_SQL += Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_No_Contra_Recibo +
                        " =  " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                }

                //Verificar si hay una orden de compra
                if (Datos.P_No_Orden_Compra != "")
                {
                    if (Mi_SQL.Contains("WHERE")) Mi_SQL += " AND "; else Mi_SQL += " WHERE ";
                    Mi_SQL += Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + "." + Ope_Alm_Registro_Facturas.Campo_No_Orden_Compra +
                        " = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";
                }                   

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar los datos a la tabla de los detalles
                Dt_Detalles = Dt_Aux.Copy();

                //Colocat tablas en el dataset:
                Dt_Cabecera.TableName = "Cabecera";
                Dt_Detalles.TableName = "Detalles";
                Ds_Resultado.Tables.Add(Dt_Cabecera);
                Ds_Resultado.Tables.Add(Dt_Detalles);

                //Entregar resultado
                return Ds_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Datos_Contrarecibo
        /// DESCRIPCION:            Consultar los datos de los contrarecibos
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            30/Enero/2013 14:00 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataSet Consulta_Datos_Contrarecibo_Taller(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            DataTable Dt_Cabecera = new DataTable(); //tabla para los datos de la cabecera
            DataTable Dt_Detalles = new DataTable(); //Tabla para los detalles
            DataTable Dt_Aux = new DataTable(); //tabla auxiliar para las consultas
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataSet Ds_Resultado = new DataSet(); //dataset para el resultado

            try
            {
                //Consulta para los datos de la cabecera
                Mi_SQL = "SELECT CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo;
                Mi_SQL += ",CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Estatus;
                Mi_SQL += ",CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion;
                Mi_SQL += ",CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Pago;
                Mi_SQL += ",PROVEEDORES." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR";
                Mi_SQL += ",ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Proveedor_ID;
                Mi_SQL += ",ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + ", ";
                Mi_SQL += "CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Solicitud_Pago + " FROM " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " ORDEN_COMPRA";
                Mi_SQL += " LEFT JOIN " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " CONTRARECIBOS ON ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_No_Factura_Interno + " = CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo;
                Mi_SQL += " INNER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDORES ON ORDEN_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + " = PROVEEDORES." + Cat_Com_Proveedores.Campo_Proveedor_ID;

                //Verificar si hay un contrarecibo
                if (Datos.P_No_Contra_Recibo > 0)
                {
                    Mi_SQL += " WHERE ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_No_Factura_Interno + " = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                }

                //Verificar si hay una orden de compra
                if (Datos.P_No_Orden_Compra != "")
                {
                    if (Mi_SQL.Contains("WHERE")) Mi_SQL += " AND "; else Mi_SQL += " WHERE ";
                        Mi_SQL += " ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";
                }

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar los datos a la tabla de la cabecera
                Dt_Cabecera = Dt_Aux.Copy();

                //Consulta para los detalles del contrarecibo
                Mi_SQL = "SELECT FACTURAS." + Ope_Tal_Registro_Facturas.Campo_Factura_ID;
                Mi_SQL += ",FACTURAS." + Ope_Tal_Registro_Facturas.Campo_Factura_Proveedor;
                Mi_SQL += ",FACTURAS." + Ope_Tal_Registro_Facturas.Campo_Total_Factura + " AS IMPORTE ";
                Mi_SQL += ",FACTURAS_ELECTRONICAS." + Ope_Tal_Facturas_Electronicas.Campo_Ruta + " AS ARCHIVO_XML, 'ORIGINAL' AS ESTATUS";
                Mi_SQL += ",FACTURAS." + Ope_Alm_Registro_Facturas.Campo_Fecha_Factura;
                Mi_SQL += ",FACTURAS_ELECTRONICAS." + Ope_Tal_Facturas_Electronicas.Campo_Descripcion + " AS DESCRIPCION, 'TALLER' AS TIPO_ORDEN_COMPRA, ";
                Mi_SQL += "FACTURAS." + Ope_Tal_Registro_Facturas.Campo_IVA_Factura + ", FACTURAS." + Ope_Tal_Registro_Facturas.Campo_Total_Factura;
                Mi_SQL += " FROM " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas + " FACTURAS";
                Mi_SQL += " LEFT JOIN " + Ope_Tal_Facturas_Electronicas.Tabla_Ope_Tal_Facturas_Electronicas + " FACTURAS_ELECTRONICAS";
                Mi_SQL += " ON (FACTURAS. " + Ope_Tal_Registro_Facturas.Campo_Factura_ID + " = FACTURAS_ELECTRONICAS." + Ope_Tal_Facturas_Electronicas.Campo_Factura_Id;
                Mi_SQL += " AND FACTURAS_ELECTRONICAS." + Ope_Tal_Facturas_Electronicas.Campo_Tipo + " = 'TALLER')";
                
                //Verificar si hay un contrarecibo
                if (Datos.P_No_Contra_Recibo > 0)
                {
                    if (Mi_SQL.Contains("WHERE")) Mi_SQL += " AND "; else Mi_SQL += " WHERE ";
                    Mi_SQL += " FACTURAS." + Ope_Alm_Registro_Facturas.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                }

                //Verificar si hay una orden de compra
                if (Datos.P_No_Orden_Compra != "")
                {
                    if (Mi_SQL.Contains("WHERE")) Mi_SQL += " AND "; else Mi_SQL += " WHERE ";
                    Mi_SQL += " FACTURAS." + Ope_Alm_Registro_Facturas.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";
                }

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar los datos a la tabla de los detalles
                Dt_Detalles = Dt_Aux.Copy();

                //Colocat tablas en el dataset:
                Dt_Cabecera.TableName = "Cabecera";
                Dt_Detalles.TableName = "Detalles";
                Ds_Resultado.Tables.Add(Dt_Cabecera);
                Ds_Resultado.Tables.Add(Dt_Detalles);

                //Entregar resultado
                return Ds_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Datos_Contrarecibo
        /// DESCRIPCION:            Consultar los datos de los contrarecibos
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            30/Enero/2013 14:00 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataSet Consulta_Datos_Contrarecibo_Taller_Externo(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            DataTable Dt_Cabecera = new DataTable(); //tabla para los datos de la cabecera
            DataTable Dt_Detalles = new DataTable(); //Tabla para los detalles
            DataTable Dt_Aux = new DataTable(); //tabla auxiliar para las consultas
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataSet Ds_Resultado = new DataSet(); //dataset para el resultado

            try
            {
                //Consulta para los datos de la cabecera
                Mi_SQL = "SELECT CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo;
                Mi_SQL += ",CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Estatus;
                Mi_SQL += ",CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion;
                Mi_SQL += ",CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Pago;
                Mi_SQL += ",PROVEEDORES." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR";
                Mi_SQL += ",ORDEN_COMPRA." + Ope_Tal_Facturas_Servicio.Campo_Proveedor_Id;
                Mi_SQL += ",ORDEN_COMPRA." + Ope_Tal_Facturas_Servicio.Campo_Folio_Solicitud + " NO_ORDEN_COMPRA, ";
                Mi_SQL += "CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Solicitud_Pago + " FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio + " ORDEN_COMPRA";
                Mi_SQL += " LEFT JOIN " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " CONTRARECIBOS ON ORDEN_COMPRA." + Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo + " = CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo;
                Mi_SQL += " INNER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDORES ON ORDEN_COMPRA." + Ope_Tal_Facturas_Servicio.Campo_Proveedor_Id + " = PROVEEDORES." + Cat_Com_Proveedores.Campo_Proveedor_ID;

                //Verificar si hay un contrarecibo
                if (Datos.P_No_Contra_Recibo > 0)
                {
                    Mi_SQL += " WHERE ORDEN_COMPRA." + Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                }

                //Verificar si hay una orden de compra
                if (Datos.P_No_Orden_Compra != "")
                {
                    if (Mi_SQL.Contains("WHERE")) Mi_SQL += " AND "; else Mi_SQL += " WHERE ";
                    Mi_SQL += Ope_Tal_Facturas_Servicio.Campo_Folio_Solicitud + " = '" + Datos.P_No_Orden_Compra.ToString().Trim() + "' ";
                }

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar los datos a la tabla de la cabecera
                Dt_Cabecera = Dt_Aux.Copy();

                //Consulta para los detalles del contrarecibo
                Mi_SQL = "SELECT FACTURAS." + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + " FACTURA_ID";
                Mi_SQL += ",FACTURAS." + Ope_Tal_Facturas_Servicio.Campo_Factura + " NO_FACTURA_PROVEEDOR";
                Mi_SQL += ",FACTURAS." + Ope_Tal_Facturas_Servicio.Campo_Total_Factura + " AS IMPORTE ";
                Mi_SQL += ",FACTURAS." + Ope_Tal_Facturas_Servicio.Campo_Ruta_Archivo + " AS ARCHIVO_XML, 'ORIGINAL' AS ESTATUS";
                Mi_SQL += ",FACTURAS." + Ope_Tal_Facturas_Servicio.Campo_Fecha_Factura;
                Mi_SQL += ",FACTURAS." + Ope_Tal_Facturas_Servicio.Campo_Comentarios + " AS DESCRIPCION, 'TALLER_EXTERNO' AS TIPO_ORDEN_COMPRA, ";
                Mi_SQL += "FACTURAS." + Ope_Tal_Facturas_Servicio.Campo_IVA_Factura + ", FACTURAS." + Ope_Tal_Facturas_Servicio.Campo_Total_Factura;
                Mi_SQL += " FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio + " FACTURAS";
                //Mi_SQL += " JOIN " + Vw_Tal_Servicios.Vista_Vw_Tal_Servicios + " v ON FACTURAS." + Ope_Tal_Facturas_Servicio.Campo_No_Servicio + " = v." + Vw_Tal_Servicios.Campo_No_servicio + " AND FACTURAS." + Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio + " = v." + Vw_Tal_Servicios.Campo_Tipo_Servicio;

                //Verificar si hay un contrarecibo
                if (Datos.P_No_Contra_Recibo > 0)
                {
                    if (Mi_SQL.Contains("WHERE")) Mi_SQL += " AND "; else Mi_SQL += " WHERE ";
                    Mi_SQL += " FACTURAS." + Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                }

                //Verificar si hay una orden de compra
                if (Datos.P_No_Orden_Compra != "")
                {
                    if (Mi_SQL.Contains("WHERE")) Mi_SQL += " AND "; else Mi_SQL += " WHERE ";
                    Mi_SQL += " FACTURAS." + Ope_Tal_Facturas_Servicio.Campo_Folio_Solicitud + " = '" + Datos.P_No_Orden_Compra.ToString().Trim() + "' ";
                }

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar los datos a la tabla de los detalles
                Dt_Detalles = Dt_Aux.Copy();

                //Colocat tablas en el dataset:
                Dt_Cabecera.TableName = "Cabecera";
                Dt_Detalles.TableName = "Detalles";
                Ds_Resultado.Tables.Add(Dt_Cabecera.Copy());
                Ds_Resultado.Tables.Add(Dt_Detalles.Copy());
                Ds_Resultado.AcceptChanges();
                //Entregar resultado
                return Ds_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Proximo_No_Contrarecibo
        /// DESCRIPCION:            Consultar el proximo numero de contrarecibo
        /// PARAMETROS :            
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            31/Enero/2013 19:26 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static Int64 Proximo_No_Contrarecibo()
        {
            //Declaracion de variables
            Int64 No_Contrarecibo = 0; //Variable para el numero de contrarecibo
            String Mi_SQL = String.Empty; //variable para las consultas
            Object Aux; //Variable auxiliar para las consultas

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT MAX(NO_CONTRA_RECIBO) FROM OPE_ALM_CONTRARECIBOS";

                //Ejecutar consulta
                Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                //Verificatr si no es nulo
                if (String.IsNullOrEmpty(Aux.ToString().Trim()) == false)
                {
                    No_Contrarecibo = Convert.ToInt64(Aux) + 1;
                }
                else
                {
                    No_Contrarecibo = 1;
                }

                //Entregar resultado
                return No_Contrarecibo;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Reporte_Contrarecibo
        /// DESCRIPCION:            Consulta para el llenado del reporte del contrarecibo
        /// PARAMETROS :            Datos: variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            05/Febrero/2013 12:00 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataSet Consulta_Reporte_Contrarecibo(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataSet Ds_Resultado = new DataSet(); //Dataset para el resultado
            DataTable Dt_Aux = new DataTable(); //tabla auxiliar para las consultas
            DataTable Dt_Cabecera = new DataTable(); //tabla para la cabecera del reporte
            DataTable Dt_Detalles = new DataTable(); //tabla para los detalles

            try
            {
                //Consulta para la cabecera
                Mi_SQL = "SELECT OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO, OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION, OPE_ALM_CONTRARECIBOS.FECHA_PAGO, " +
                    "CAT_COM_PROVEEDORES.NOMBRE AS PROVEEDOR, " +
                    "(Cat_Empleados.APELLIDO_PATERNO + ' ' + ISNULL(Cat_Empleados.APELLIDO_MATERNO, '') + ' ' + Cat_Empleados.NOMBRE) AS RECIBIO, " +
                    "OPE_ALM_CONTRARECIBOS.IMPORTE_TOTAL FROM OPE_ALM_CONTRARECIBOS " +
                    "INNER JOIN CAT_COM_PROVEEDORES ON OPE_ALM_CONTRARECIBOS.PROVEEDOR_ID = CAT_COM_PROVEEDORES.PROVEEDOR_ID " +
                    "INNER JOIN Cat_Empleados ON OPE_ALM_CONTRARECIBOS.EMPLEADO_RECIBIO_ID = Cat_Empleados.EMPLEADO_ID " +
                    "WHERE OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";

                //Ejecutar consulta
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar la tabla con estructrua y datos
                Dt_Cabecera = Dt_Aux.Copy();

                //Consulta para los detalles
                Mi_SQL = "SELECT NO_CONTRA_RECIBO, NO_FACTURA_PROVEEDOR, FECHA_FACTURA, IMPORTE_FACTURA FROM OPE_ALM_REGISTRO_FACTURAS " +
                    "WHERE NO_CONTRA_RECIBO = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar la tabla con estructura y datos
                Dt_Detalles = Dt_Aux.Copy();

                //Agregar las tablas al dataset
                Dt_Cabecera.TableName = "Cabecera";
                Ds_Resultado.Tables.Add(Dt_Cabecera);
                Dt_Detalles.TableName = "Detalles";
                Ds_Resultado.Tables.Add(Dt_Detalles);

                //Entregar resultado
                return Ds_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Reporte_Contrarecibo
        /// DESCRIPCION:            Consulta para el llenado del reporte del contrarecibo
        /// PARAMETROS :            Datos: variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            05/Febrero/2013 12:00 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataSet Consulta_Reporte_Contrarecibo_Taller(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataSet Ds_Resultado = new DataSet(); //Dataset para el resultado
            DataTable Dt_Aux = new DataTable(); //tabla auxiliar para las consultas
            DataTable Dt_Cabecera = new DataTable(); //tabla para la cabecera del reporte
            DataTable Dt_Detalles = new DataTable(); //tabla para los detalles

            try
            {
                //Consulta para la cabecera
                Mi_SQL = "SELECT cr." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo 
                    + ", cr." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion 
                    + ", cr." + Ope_Alm_Contrarecibos.Campo_Fecha_Pago 
                    + ", pr." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR, " 
                    + "( em." + Cat_Empleados.Campo_Apellido_Paterno + " + ' ' + " + " ISNULL(em."+Cat_Empleados.Campo_Apellido_Materno +", '') + ' ' + em." + Cat_Empleados.Campo_Nombre + ") AS RECIBIO, cr." +
                    Ope_Alm_Contrarecibos.Campo_Importe_Total + " FROM " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos +
                    " cr INNER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " pr ON cr." + Ope_Alm_Contrarecibos.Campo_Proveedor_ID + " = pr." + Cat_Com_Proveedores.Campo_Proveedor_ID +
                    " INNER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " em ON cr." + Ope_Alm_Contrarecibos.Campo_Empleado_Recibido_ID + " = em." + Cat_Empleados.Campo_Empleado_ID +
                    " WHERE cr." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";

                //Ejecutar consulta
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar la tabla con estructrua y datos
                Dt_Cabecera = Dt_Aux.Copy();

                //Consulta para los detalles
                Mi_SQL = "SELECT " 
                    + Ope_Tal_Registro_Facturas.Campo_No_Contra_Recibo
                    + "," + Ope_Tal_Registro_Facturas.Campo_Factura_Proveedor
                    + "," + Ope_Tal_Registro_Facturas.Campo_Fecha_Factura
                    + "," + Ope_Tal_Registro_Facturas.Campo_Importe_Factura
                    + " FROM " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas
                    + " WHERE " + Ope_Tal_Registro_Facturas.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar la tabla con estructura y datos
                Dt_Detalles = Dt_Aux.Copy();

                //Agregar las tablas al dataset
                Dt_Cabecera.TableName = "Cabecera";
                Ds_Resultado.Tables.Add(Dt_Cabecera);
                Dt_Detalles.TableName = "Detalles";
                Ds_Resultado.Tables.Add(Dt_Detalles);

                //Entregar resultado
                return Ds_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Reporte_Contrarecibo
        /// DESCRIPCION:            Consulta para el llenado del reporte del contrarecibo
        /// PARAMETROS :            Datos: variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            05/Febrero/2013 12:00 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataSet Consulta_Reporte_Contrarecibo_Taller_Externo(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataSet Ds_Resultado = new DataSet(); //Dataset para el resultado
            DataTable Dt_Aux = new DataTable(); //tabla auxiliar para las consultas
            DataTable Dt_Cabecera = new DataTable(); //tabla para la cabecera del reporte
            DataTable Dt_Detalles = new DataTable(); //tabla para los detalles

            try
            {
                //Consulta para la cabecera
                Mi_SQL = "SELECT cr." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo
                    + ", cr." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion
                    + ", cr." + Ope_Alm_Contrarecibos.Campo_Fecha_Pago
                    + ", pr." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR, "
                    + "( em." + Cat_Empleados.Campo_Apellido_Paterno + " + ' ' + " + " ISNULL(em." + Cat_Empleados.Campo_Apellido_Materno + ", '') + ' ' + em." + Cat_Empleados.Campo_Nombre + ") AS RECIBIO, cr." +
                    Ope_Alm_Contrarecibos.Campo_Importe_Total + " FROM " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos +
                    " cr INNER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " pr ON cr." + Ope_Alm_Contrarecibos.Campo_Proveedor_ID + " = pr." + Cat_Com_Proveedores.Campo_Proveedor_ID +
                    " INNER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " em ON cr." + Ope_Alm_Contrarecibos.Campo_Empleado_Recibido_ID + " = em." + Cat_Empleados.Campo_Empleado_ID +
                    " WHERE cr." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";

                //Ejecutar consulta
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar la tabla con estructrua y datos
                Dt_Cabecera = Dt_Aux.Copy();

                //Consulta para los detalles
                Mi_SQL = "SELECT "
                    + Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo
                    + "," + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + " NO_FACTURA_PROVEEDOR"
                    + "," + Ope_Tal_Facturas_Servicio.Campo_Fecha_Factura + " FECHA_FACTURA"
                    + "," + Ope_Tal_Facturas_Servicio.Campo_Monto_Factura + " IMPORTE_FACTURA"
                    + " FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio
                    + " WHERE " + Ope_Tal_Facturas_Servicio.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar la tabla con estructura y datos
                Dt_Detalles = Dt_Aux.Copy();

                //Agregar las tablas al dataset
                Dt_Cabecera.TableName = "Cabecera";
                Ds_Resultado.Tables.Add(Dt_Cabecera);
                Dt_Detalles.TableName = "Detalles";
                Ds_Resultado.Tables.Add(Dt_Detalles);

                //Entregar resultado
                return Ds_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        public static void Cancelar_Contrarecibo(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            String Mensaje = "";
            String Mi_SQL = String.Empty;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                //Asignar consulta para la cancelación del contrarecibo
                Mi_SQL = "UPDATE " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " SET " + Ope_Alm_Contrarecibos.Campo_Estatus + " = 'CANCELADO', " +
                    Ope_Alm_Contrarecibos.Campo_Fecha_Cancelacion + " = '" + Datos.P_Fecha_Cancelacion + "', " +
                    Ope_Alm_Contrarecibos.Campo_Motivo_Cancelacion + " = '" + Datos.P_Motivo_Cancelacion + "', " +
                    Ope_Alm_Contrarecibos.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario + "', " +
                    Ope_Alm_Contrarecibos.Campo_Fecha_Modifico + " = GETDATE() " +
                    "WHERE " + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Consulta para la cancelacion de la solicitud de pago

                //Ejecutar transaccion
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje); // Se indica el mensaje 
            }
            finally
            {
                Cn.Close();
            }
        }
        
        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Solo_Contrarecibos
        /// DESCRIPCION:            Consultar las ordenes de compra que ya tienen un contrarecibo asignado
        /// PARAMETROS :            Datos: variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            16/Febrero/2013 12:00 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Solo_Contrarecibos(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //Tabla para el resultado
            Boolean Where_Utilizado = false; //variable que indica si la clausula where ya ha sido utilizada

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + ", " +
                    Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Folio + " AS OC, " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + ", " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion + ", " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Fecha_Pago + ", " +
                    Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR, " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Importe_Total + ", " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Estatus + " FROM " +
                    Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " INNER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ON " +
                    Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + " = " +
                    Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + " INNER JOIN " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " ON " +
                    Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Factura_Interno + " = " +
                    Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " ";

                //Verificar si se tiene el numero de contrarecibo
                if (Datos.P_No_Contra_Recibo > 0 || Datos.P_No_Orden_Compra != "")
                {
                    //Verificar si hay contrarecibo
                    if (Datos.P_No_Contra_Recibo > 0)
                    {
                        Mi_SQL += "WHERE " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = " + 
                            Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = " +
                            Datos.P_No_Orden_Compra.ToString().Trim() + " ";
                    }
                }
                else
                {
                    //Verificar los filtros
                    //Proveedor
                    if (String.IsNullOrEmpty(Datos.P_Proveedor_ID) == false)
                    {
                        Where_Utilizado = true;

                        Mi_SQL += "WHERE " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Proveedor_ID + " = " +
                            "'" + Datos.P_Proveedor_ID + "' ";
                    }

                    //Estatus
                    if (String.IsNullOrEmpty(Datos.P_Estatus) == false)
                    {
                        //Verificar si la clausula where ya ha sido utilizado
                        if (Where_Utilizado == true)
                        {
                            Mi_SQL += "AND ";
                        }
                        else
                        {
                            Mi_SQL += "WHERE ";
                        }

                        //Resto de la consulta
                        Mi_SQL += Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Estatus + " = '" + Datos.P_Estatus + "' ";
                    }

                    //Rango de fechas
                    if (Datos.P_Fecha_Inicio != "01/01/1900")
                    {
                        //Verificar si la clausula where ya ha sido utilizado
                        if (Where_Utilizado == true)
                        {
                            Mi_SQL += "AND ";
                        }
                        else
                        {
                            Mi_SQL += "WHERE ";
                        }

                        //Fecha final
                        if (Datos.P_Fecha_Fin != "01/01/1900")
                        {
                            Mi_SQL += Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion + " >= " +
                                string.Format("{0:yyyy/MM/dd}", Datos.P_Fecha_Inicio) + " 00:00:00' AND " +
                                Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion + " <= " +
                                string.Format("{0:yyyy/MM/dd}", Datos.P_Fecha_Fin) + " 23:59:59' ";
                        }
                        else
                        {
                            Mi_SQL += Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion + " >= " +
                                string.Format("{0:yyyy/MM/dd}", Datos.P_Fecha_Inicio) + " 00:00:00' AND " +
                                Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion + " <= " +
                                string.Format("{0:yyyy/MM/dd}", DateTime.Now) + " 23:59:59' ";
                        }
                    }

                    //Orden de la consulta
                    Mi_SQL += "ORDER BY " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + "." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " ";
                }

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Fecha_Pago
        /// DESCRIPCION:            Consultar la fecha de pago para el contrarecibo
        /// PARAMETROS :            Datos: variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            01/Marzo/2013 14:35 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DateTime Consulta_Fecha_Pago(Cls_Ope_Alm_Contrarecibos_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DateTime Resultado = DateTime.Today; //Variable para el resultado
            Object aux; //Variable auxiliar para las consultas

            try
            {
                //Consulta para la proxima fecha de pago
                Mi_SQL = "SELECT " + Ope_Alm_Fechas_Pagos.Campo_Fecha_Pago + " FROM " + Ope_Alm_Fechas_Pagos.Tabla_Ope_Alm_Fechas_Pagos + " " +
                    "WHERE " + Ope_Alm_Fechas_Pagos.Campo_Fecha_Limite_Recepcion + " >= CONVERT(DATETIME, '" + string.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Recepcion_dt) +
                    "', 103) ORDER BY " + Ope_Alm_Fechas_Pagos.Campo_Fecha_Limite_Recepcion + " ASC ";

                //Ejecutar consulta
                aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                //Verificar si no es nulo
                if (aux != null)
                {
                    //Convertir la fecha
                    Resultado = Convert.ToDateTime(aux);
                }

                //Entregar el resultado
                return Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
        #endregion
    }
}