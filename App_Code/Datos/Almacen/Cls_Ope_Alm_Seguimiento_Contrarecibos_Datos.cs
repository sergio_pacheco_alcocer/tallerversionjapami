﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Seguimiento_Contrarecibos.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;
using JAPAMI.Administrar_Requisiciones.Negocios;
using System.Data.SqlClient;
using JAPAMI.Manejo_Presupuesto.Datos;

namespace JAPAMI.Seguimiento_Contrarecibos.Datos
{
    public class Cls_Alm_Seguimiento_Contrarecibos_Datos
    {
        public Cls_Alm_Seguimiento_Contrarecibos_Datos()
        {

        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Contra_Recibos
        ///DESCRIPCIÓN: Consulta los contrarecibos registrados
        ///PARAMETROS:  1.- Cls_Ope_Alm_Autorizacion_Compras_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  28/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Contra_Recibos(Cls_Alm_Seguimiento_Contrarecibos_Negocio Clase_Seguimiento_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                //Creamos la consulta
                Mi_SQL = "SELECT CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + ", CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Pago;
                Mi_SQL += ", CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion;
                Mi_SQL += ", ORDENES." + Ope_Com_Ordenes_Compra.Campo_Nombre_Proveedor + ", ORDENES." + Ope_Com_Ordenes_Compra.Campo_Total;
                Mi_SQL += ", ORDENES." + Ope_Com_Ordenes_Compra.Campo_Folio + ", CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Estatus;
                Mi_SQL += " FROM " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " CONTRARECIBOS, " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " ORDENES";
                Mi_SQL += " WHERE CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = ";
                Mi_SQL += "ORDENES." + Ope_Com_Ordenes_Compra.Campo_No_Factura_Interno;
                //Validamos los filtros por No de Contrarecibo
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_No_Contra_Recibo))
                {
                    Mi_SQL += " AND CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = " + Clase_Seguimiento_Negocio.P_No_Contra_Recibo;
                }
                //Validamos los filtros por fechas
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Fecha_Inicio) && !String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Fecha_Fin))
                {
                    Clase_Seguimiento_Negocio.P_Fecha_Inicio = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Clase_Seguimiento_Negocio.P_Fecha_Inicio));
                    Clase_Seguimiento_Negocio.P_Fecha_Fin = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Clase_Seguimiento_Negocio.P_Fecha_Fin));

                    Mi_SQL += " AND ((CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Pago + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Inicio + " 00:00:00' AND ";
                    Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Fin + " 23:59:00') ";
                    Mi_SQL += " OR (CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Creo + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Inicio + " 00:00:00' AND ";
                    Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Fin + " 23:59:00'))";
                }
                //regresamos los valores
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Registro_Facturas
        ///DESCRIPCIÓN: Consulta los registro de facturas que se crearon
        ///PARAMETROS:  1.- Cls_Alm_Seguimiento_Contrarecibos_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  29/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Registro_Facturas(Cls_Alm_Seguimiento_Contrarecibos_Negocio Clase_Seguimiento_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                //Creamos la consulta
                Mi_SQL = "SELECT FACTURAS." + Ope_Alm_Registro_Facturas.Campo_Factura_ID + ", FACTURAS." + Ope_Alm_Registro_Facturas.Campo_Fecha_Factura + ", FACTURAS." + Ope_Alm_Registro_Facturas.Campo_Importe_Factura;
                Mi_SQL += " FROM " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + " FACTURAS";
                Mi_SQL += " WHERE FACTURAS." + Ope_Alm_Registro_Facturas.Campo_No_Orden_Compra + " = " + Clase_Seguimiento_Negocio.P_No_Orden_Compra;
                Mi_SQL += " AND FACTURAS." + Ope_Alm_Registro_Facturas.Campo_No_Contra_Recibo + " = " + Clase_Seguimiento_Negocio.P_No_Contra_Recibo;

                //regresamos los valores
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Seguimiento
        ///DESCRIPCIÓN: Consulta los registro del seguimiento del contrarecibo
        ///PARAMETROS:  1.- Cls_Alm_Seguimiento_Contrarecibos_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  29/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Seguimiento(Cls_Alm_Seguimiento_Contrarecibos_Negocio Clase_Seguimiento_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                //Creamos la consulta
                Mi_SQL = "SELECT (isnull(EMPLEADO." + Cat_Empleados.Campo_Nombre + ",'')+' '+isnull(EMPLEADO." + Cat_Empleados.Campo_Apellido_Paterno + ",'')+' '+isnull(EMPLEADO." + Cat_Empleados.Campo_Apellido_Materno + ",'')) AS EMPLEADO";
                Mi_SQL += ", CONTRARECIBO." + Ope_Alm_Seguimiento_Contrarecibo.Campo_Estatus_Entrada + ", CONTRARECIBO." + Ope_Alm_Seguimiento_Contrarecibo.Campo_Estatus_Salida;
                Mi_SQL += ", CONTRARECIBO." + Ope_Alm_Seguimiento_Contrarecibo.Campo_Fecha_Entrada + ", CONTRARECIBO." + Ope_Alm_Seguimiento_Contrarecibo.Campo_Fecha_Salida;
                Mi_SQL += " FROM " + Ope_Alm_Seguimiento_Contrarecibo.Tabla_Ope_Alm_Seguimiento_Contrarecibo + " CONTRARECIBO, " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO";
                Mi_SQL += " WHERE CONTRARECIBO." + Ope_Alm_Seguimiento_Contrarecibo.Campo_Empleado_Recibido_ID + " = ";
                Mi_SQL += "EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + " AND CONTRARECIBO." + Ope_Alm_Seguimiento_Contrarecibo.Campo_No_Contra_Recibo + " = " + Clase_Seguimiento_Negocio.P_No_Contra_Recibo;
                Mi_SQL += " ORDER BY CONTRARECIBO." + Ope_Alm_Seguimiento_Contrarecibo.Campo_Fecha_Salida + " ASC";
                //regresamos los valores
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }
    }
}
