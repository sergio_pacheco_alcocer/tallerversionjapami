﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Bitacora_Eventos;
using JAPAMI.Constantes;
using JAPAMI.Parametros_Almacen.Negocio;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Text;
///malo compu///
namespace JAPAMI.Parametros_Almacen.Datos
{
    public class Cls_Cat_Alm_Parametros_Datos
    {
        
        #region Metodos        

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Parametros
        ///DESCRIPCIÓN: Consulta las imagenes  
        ///PARAMETROS:  1.- Cls_Cat_Alm_Parametros_Negocio
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Parametros(Cls_Cat_Alm_Parametros_Negocio Negocio)
        {
            String Mi_SQL = "SELECT PARAMETROS." + Cat_Com_Parametros.Campo_Dependencia_ID_Almacen + " AS ID, DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + " AS CLAVE,"
                          + " DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS NOMBRE , 'Dependencia Almacen' AS DESCRIPCION"
                          + " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + " PARAMETROS, " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS"
                          + " WHERE PARAMETROS." + Cat_Com_Parametros.Campo_Dependencia_ID_Almacen + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID
                          + " UNION"
                          + " SELECT PARAMETROS." + Cat_Com_Parametros.Campo_Programa_Almacen + " AS ID, PROYECTOS." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS CLAVE,"
                          + " PROYECTOS." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS NOMBRE , 'Programa Almacen' AS DESCRIPCION"
                          + " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + " PARAMETROS, " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " PROYECTOS"
                          + " WHERE PARAMETROS." + Cat_Com_Parametros.Campo_Programa_Almacen + " = PROYECTOS." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
            //Sentencia que ejecuta el query
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            return Data_Table;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta
        ///DESCRIPCIÓN: Agregamos los parametros 
        ///PARAMETROS:  1.- Cls_Cat_Alm_Parametros_Negocio
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Alta(Cls_Cat_Alm_Parametros_Negocio Negocio)
        {
            DataTable Dt_Temp= new DataTable();
            try
            {
                String Mi_SQL = "SELECT * FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros;
                Dt_Temp = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                if (Dt_Temp.Rows.Count > 0)
                {
                    Negocio.P_Parametros_ID = Dt_Temp.Rows[0][Cat_Com_Parametros.Campo_Parametro_ID].ToString();
                    Modificar(Negocio);
                }
                else
                {
                    Mi_SQL = "INSERT INTO " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + " ("
                                  + Cat_Com_Parametros.Campo_Parametro_ID + ", "
                                  + Cat_Com_Parametros.Campo_Dependencia_ID_Almacen + ", "
                                  + Cat_Com_Parametros.Campo_Programa_Almacen + " VALUES('',";
                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                        Mi_SQL += "'" + Negocio.P_Dependencia_ID + "'";
                    else
                        Mi_SQL += "NULL";
                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID))
                        Mi_SQL += ", '" + Negocio.P_Programa_ID + "'";
                    else
                        Mi_SQL += ", NULL";
                    Mi_SQL += ")";

                    //Sentencia que ejecuta el query
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar
        ///DESCRIPCIÓN: Modificamos los parametros
        ///PARAMETROS:  1.- Cls_Cat_Alm_Parametros_Negocio
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Febrero/2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Modificar(Cls_Cat_Alm_Parametros_Negocio Negocio)
        {
            try
            {
                //Declaracion de variables
                String Mi_SQL = String.Empty;
                DataTable Dt_Temp= new DataTable();

                Mi_SQL = "SELECT * FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros;
                Dt_Temp = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                //Asignamos el valor                
                Negocio.P_Parametros_ID = Dt_Temp.Rows[0][Cat_Com_Parametros.Campo_Parametro_ID].ToString();
                
                Mi_SQL = "UPDATE " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + " SET ";
                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                        Mi_SQL += Cat_Com_Parametros.Campo_Dependencia_ID_Almacen + " = '" + Negocio.P_Dependencia_ID + "'";
                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID))
                        Mi_SQL += Cat_Com_Parametros.Campo_Programa_Almacen + " = '" + Negocio.P_Programa_ID + "'";
                    Mi_SQL += " WHERE " + Cat_Com_Parametros.Campo_Parametro_ID + " = '" + Negocio.P_Parametros_ID + "'";
                //Sentencia que ejecuta el query 
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Dependencias
        ///DESCRIPCIÓN: Consulta las Dependencias
        ///PARAMETROS:  1.- Cls_Cat_Alm_Parametros_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  15/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Dependencias(Cls_Cat_Alm_Parametros_Negocio Negocio)
        {
            try
            {
                StringBuilder Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_Dependencias.Campo_Dependencia_ID + ",");
                Mi_SQL.Append(Cat_Dependencias.Campo_Clave + " + ' ' + " + Cat_Dependencias.Campo_Nombre);
                Mi_SQL.Append(" AS DESCRIPCION");
                Mi_SQL.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                if (!String.IsNullOrEmpty(Negocio.P_Dependencia))
                    Mi_SQL.Append(" WHERE " + Cat_Dependencias.Campo_Clave + " LIKE '%" + Negocio.P_Dependencia.Trim() + "%'");

                DataTable Dt_Clasificador_Economico = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                return Dt_Clasificador_Economico;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las  ultimas fechas. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Proyectos
        ///DESCRIPCIÓN: Consulta los proyectos
        ///PARAMETROS:  1.- Cls_Cat_Alm_Parametros_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  15/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Proyectos(Cls_Cat_Alm_Parametros_Negocio Negocio)
        {
            try
            {
                StringBuilder Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ",");
                Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + " + Cat_Sap_Proyectos_Programas.Campo_Nombre);
                Mi_SQL.Append(" AS DESCRIPCION");
                Mi_SQL.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                if (!String.IsNullOrEmpty(Negocio.P_Programa))
                    Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Clave + " LIKE '%" + Negocio.P_Programa.Trim() + "%'");

                DataTable Dt_Clasificador_Economico = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                return Dt_Clasificador_Economico;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las  ultimas fechas. Error: [" + Ex.Message + "]");
            }
        }     
        #endregion        
    }
}
