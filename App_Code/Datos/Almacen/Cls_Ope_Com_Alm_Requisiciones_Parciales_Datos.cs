﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Requisiciones_Parciales.Negocio;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Bitacora_Eventos;
using JAPAMI.Generar_Requisicion.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;

/// <summary>
/// Summary description for Cls_Ope_Com_Alm_Requisiciones_Parciales_Datos
/// </summary>
/// 

namespace JAPAMI.Requisiciones_Parciales.Datos
{
    public class Cls_Ope_Com_Alm_Requisiciones_Parciales_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Requisiciones_Parciales
        ///DESCRIPCIÓN:          Método utilizado para consultar las requisiciones parciales de stock de almacén
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           27/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Requisiciones_Parciales(Cls_Ope_Com_Alm_Requisiciones_Parciales_Negocio Datos)
        {
            // Declaración de variables
            String Mi_SQL = null;
            DataTable Dt_Requisiciones = new DataTable();

            Mi_SQL = "SELECT " + "REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + ""; // NO_REQUISICION
            Mi_SQL += ", DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " as UNIDAD_RESPONSABLE";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio + "";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " as FECHA";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Total + " as MONTO";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Estatus + " as ESTATUS";
            Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICIONES ";
            Mi_SQL += " JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
            Mi_SQL += " ON REQUISICIONES." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
            Mi_SQL += " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID;
            Mi_SQL += " WHERE  REQUISICIONES." + Ope_Com_Requisiciones.Campo_Tipo + "= '" + "STOCK'";
            Mi_SQL += " and  REQUISICIONES." + Ope_Com_Requisiciones.Campo_Estatus + " IN ('PARCIAL','ALMACEN')";

            if (Datos.P_No_Requisicion != null)
            {
                Mi_SQL += "AND REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " like '%" + Datos.P_No_Requisicion + "%'";
            }

            //if (Datos.P_Dependencia != null)
            //{
            //    Mi_SQL += " and  REQUISICIONES." + Ope_Com_Requisiciones.Campo_Dependencia_ID + "= '" + Datos.P_Dependencia + "'";
            //}

            if ((Datos.P_Fecha_Inicial != null) && (Datos.P_Fecha_Final != null))
            {   
                
                //Mi_SQL += " AND TO_DATE(TO_CHAR(" + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion+ ",'DD/MM/YY')) BETWEEN '" + Datos.P_Fecha_Inicial + "'" +
                //" AND '" + Datos.P_Fecha_Final + "'" ;

                Mi_SQL += " AND CONVERT(DATETIME,CAST(" + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " AS VARCHAR(20))) " +
                        " >= '" + Datos.P_Fecha_Inicial + "' AND " +
                        " CONVERT(DATETIME,CAST(" + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " AS VARCHAR(20))) " +
                        " <= '" + Datos.P_Fecha_Final + "' ";


            }

            Mi_SQL += " order by REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID;

            Dt_Requisiciones = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Requisiciones;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Detalles_Requisiciones
        ///DESCRIPCIÓN:          Método utilizado para consultar los detalles de la requisicion
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           27/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Detalles_Requisicion(Cls_Ope_Com_Alm_Requisiciones_Parciales_Negocio Datos)
        {
            // Declaración de variables
            String Mi_SQL = null;
            DataTable Dt_Requisicion = new DataTable();

            Mi_SQL = "SELECT " + "REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + ""; //NO_REQUISICION
            Mi_SQL += ", DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " as UNIDAD_RESPONSABLE";
            Mi_SQL += ", DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + " as UNIDAD_RESPONSABLE_ID";

            Mi_SQL += ",( SELECT AREAS." + Cat_Areas.Campo_Nombre + " FROM " + Cat_Areas.Tabla_Cat_Areas + " AREAS ";
            Mi_SQL += " WHERE AREAS."+  Cat_Areas.Campo_Area_ID + " = REQUISICIONES." + Ope_Com_Requisiciones.Campo_Area_ID;
            Mi_SQL = Mi_SQL  + " AND REQUISICIONES."+ Ope_Com_Requisiciones.Campo_Requisicion_ID + " = '" + Datos.P_No_Requisicion + "' ) as AREA ";
            Mi_SQL += ",( SELECT PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDA ";
            Mi_SQL += " WHERE PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = REQUISICIONES." + Ope_Com_Requisiciones.Campo_Partida_ID;
            Mi_SQL += " AND REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = '" + Datos.P_No_Requisicion + "' ) as PARTIDA ";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio + "";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " as FECHA";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Justificacion_Compra + " as COMENTARIOS";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_IVA + "";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Subtotal + "";
            Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Total + "";
            Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICIONES ";
            Mi_SQL += " JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
            Mi_SQL += " ON REQUISICIONES." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
            Mi_SQL += " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID;
            Mi_SQL += " WHERE  REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + "= '" + Datos.P_No_Requisicion + "'";

            Dt_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Requisicion;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Productos_Requisicion_Parcial
        ///DESCRIPCIÓN:          Método utilizado para consultar los productos de las requisiciones parciales de stock de almacén
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           27/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Productos_Requisicion_Parcial(Cls_Ope_Com_Alm_Requisiciones_Parciales_Negocio Datos) // ESTE ES EL METODO
        {
            // Declaración de variables
            String Mi_SQL = null;
            DataTable Dt_Detalles = new DataTable();

            Mi_SQL += " SELECT REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " as PRODUCTO_ID ";
            Mi_SQL += ",(select PRODUCTOS." + Cat_Com_Productos.Campo_Clave + " from ";
            Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS ";
            Mi_SQL += " where REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = PRODUCTOS.";
            Mi_SQL += Cat_Com_Productos.Campo_Producto_ID + ")as CLAVE ";

            Mi_SQL += ",(select UNIDADES." + Cat_Com_Unidades.Campo_Abreviatura + " from ";
            Mi_SQL += Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " UNIDADES ";
            Mi_SQL += " where  UNIDADES." + Cat_Com_Unidades.Campo_Unidad_ID + " = ";
            Mi_SQL += " (select PRODUCTOS." + Cat_Com_Productos.Campo_Unidad_ID + " from ";
            Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS ";
            Mi_SQL += " where  REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = ";
            Mi_SQL += " PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID + ")) as UNIDADES ";

            Mi_SQL += ",(select PRODUCTOS." + Cat_Com_Productos.Campo_Nombre + " from ";
            Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS ";
            Mi_SQL += " where REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = PRODUCTOS.";
            Mi_SQL += Cat_Com_Productos.Campo_Producto_ID + ")as PRODUCTO ";

            Mi_SQL += ",(select PRODUCTOS." + Cat_Com_Productos.Campo_Descripcion + " from ";
            Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS ";
            Mi_SQL += " where REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = PRODUCTOS.";
            Mi_SQL += Cat_Com_Productos.Campo_Producto_ID + ")as DESCRIPCION ";

            Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Cantidad + " as CANTIDAD_SOLICITADA  "; // Cantidad Solicitada
            Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Cantidad_Entregada + " ";               // Cantidad Entregada
            Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Precio_Unitario + " as PRECIO ";           
            Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Partida_ID+ " ";
            Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Porcentaje_IVA + " ";
            
            Mi_SQL += " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PRODUCTO";
            Mi_SQL += " WHERE REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + Datos.P_No_Requisicion.Trim();
           
            Dt_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Detalles;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Productos_Requisicion_Parcial
        ///DESCRIPCIÓN:          Método utilizado para consultar los productos de las requisiciones parciales de stock de almacén
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           27/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Pragrama_Financiamiento(Cls_Ope_Com_Alm_Requisiciones_Parciales_Negocio Datos)
        {
            DataTable Dt_Consulta = new DataTable();
            String Mi_SQL = null;

            Mi_SQL = " SELECT DISTINCT" + " PROYECTOS_PROGRAMAS." + Cat_Com_Proyectos_Programas.Campo_Descripcion + " as PROYECTO_PROGRAMA ";
            Mi_SQL += ", PROYECTOS_PROGRAMAS." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + " ";
            Mi_SQL += ", FINANCIAMIENTO." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " as FINANCIAMIENTO ";
            Mi_SQL += " FROM " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + " PROYECTOS_PROGRAMAS ";
            Mi_SQL += ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PRODUCTO ";
            Mi_SQL += ", " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FINANCIAMIENTO ";
            Mi_SQL += " WHERE REQ_PRODUCTO. " + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID + " = ";
            Mi_SQL += " PROYECTOS_PROGRAMAS. " + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + " ";
            Mi_SQL += " AND REQ_PRODUCTO. " + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID + " = ";
            Mi_SQL += " FINANCIAMIENTO. " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " ";
            Mi_SQL += " AND REQ_PRODUCTO. " + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = ";
            Mi_SQL += " '" + Datos.P_No_Requisicion + "' ";

            Dt_Consulta = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Consulta;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_DataTable
        ///DESCRIPCIÓN:          Método utilizado para consultar loas dependencias y las áreas
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           18/Febrero/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_DataTable(Cls_Ope_Com_Alm_Requisiciones_Parciales_Negocio Datos)
        {
            // Declaración de Variables
            String Mi_SQL = null;
            DataSet Ds_Consulta = null;
            DataTable Dt_consulta = new DataTable();

            try
            {
                if (Datos.P_Tipo_Data_Table.Equals("DEPENDENCIAS"))
                {
                    Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + " AS DEPENDENCIA_ID, " + Cat_Dependencias.Campo_Nombre + " AS NOMBRE";
                    Mi_SQL += " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
                }
                else if (Datos.P_Tipo_Data_Table.Equals("AREAS"))
                {
                    Mi_SQL = "SELECT " + Cat_Areas.Campo_Area_ID + " AS AREA_ID, " + Cat_Areas.Campo_Nombre + " AS NOMBRE";
                    Mi_SQL += " FROM " + Cat_Areas.Tabla_Cat_Areas;
                }
                
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Consulta = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Consulta != null)
                {
                    Dt_consulta= Ds_Consulta.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_consulta;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Liberar_Requisiciones
        ///DESCRIPCIÓN:          Método utilizado para liberar las requisiciones seleccionadas por el usuario
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           27/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Cancelar_Requisicion(Cls_Ope_Com_Alm_Requisiciones_Parciales_Negocio Datos)
        {
            // Declaración de Variables
            String Mi_SQL = null;
            
            Int32 Cantidad_Comprometida = 0;
            Int32 Cantidad_Disponible = 0;
            Int32 Cantidad_Productos_Cancelar = 0;
            // Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mensaje = String.Empty; //Variable para el mensaje de error
            Object Aux; // Variable auxiliar para las consultas
            SqlDataAdapter Obj_Adaptador; //Adapatador para el llenado de las tablas

            DataTable Dt_Productos_A_Cancelar = new DataTable();
            DataTable Dt_Aux;


            DataTable Dt_Requisicion = new DataTable();
            String Dependencia_ID = "";
            String Partida_ID = "";
            String Proyecto_ID = "";
            String FF = "";
            String Num_Reserva = "";
            Double Monto_Anterior = 0;
            String Cargo = "";
            String Abono = "";

            // Validacion
            if (Datos.P_Estatus == "PARCIAL")
            {
                Datos.P_Estatus = "LIBERADA";
            }
            else if (Datos.P_Estatus == "ALMACEN")
            {
                Datos.P_Estatus = "CANCELADA";
            }

            try
            {
                if (Datos.P_Dt_Productos_Cancelar.Rows.Count > 0) // Si la tabla tiene valores
                {
                    Dt_Productos_A_Cancelar = Datos.P_Dt_Productos_Cancelar;

                    Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                    Obj_Comando = new SqlCommand();
                    Obj_Adaptador = new SqlDataAdapter();
                    Obj_Conexion.Open();
                    Obj_Transaccion = Obj_Conexion.BeginTransaction();
                    Obj_Comando.Transaction = Obj_Transaccion;
                    Obj_Comando.Connection = Obj_Conexion;

                    //** ACTUALIZAR REQUISICIÓN A LIBERADA
                    // Consulta para la actualizacion del estatus de la requisicion 
                    Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " ";
                    Mi_SQL += "SET " + Ope_Com_Requisiciones.Campo_Estatus + " ='" + Datos.P_Estatus + "'";
                    Mi_SQL += "WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Datos.P_No_Requisicion + " ";

                    //String No_Requisicion = Convert.ToString(Datos.P_No_Requisicion);
                    //Se registra  el update en la bitacora
                    //Cls_Bitacora.Alta_Bitacora(Cls_Sessiones.Empleado_ID, Ope_Bitacora.Accion_Modificar, "Frm_Alm_Com_Orden_Salida.aspx", No_Requisicion, Mi_SQL);

                    //Ejecutar consulta
                    Obj_Comando.CommandText = Mi_SQL;
                    Obj_Comando.ExecuteNonQuery();


                    // Se Guarda el Historial de la requisición
                    Cls_Ope_Com_Requisiciones_Negocio Requisiciones = new Cls_Ope_Com_Requisiciones_Negocio();
                    Requisiciones.Registrar_Historial(Datos.P_Estatus, Datos.P_No_Requisicion);

                    Dt_Requisicion = Consultar_Codigo_Programatico_RQ(Datos.P_No_Requisicion, Obj_Comando);
                    Partida_ID = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim();
                    Proyecto_ID = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim();
                    Dependencia_ID = Dt_Requisicion.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
                    FF = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                    Num_Reserva = Dt_Requisicion.Rows[0]["NUM_RESERVA"].ToString().Trim();
                    Monto_Anterior = double.Parse(Dt_Requisicion.Rows[0]["TOTAL"].ToString().Trim());
                    Cargo = Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE;
                    Abono = Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO;

                    //Construimos el datatable
                    DataTable Dt_Detalles = new DataTable();
                    Dt_Detalles.Columns.Add("FUENTE_FINANCIAMIENTO_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("PROGRAMA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("DEPENDENCIA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("PARTIDA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("ANIO", typeof(System.String));
                    Dt_Detalles.Columns.Add("IMPORTE", typeof(System.String));
                    DataRow Fila_Nueva = Dt_Detalles.NewRow();

                    Fila_Nueva["FUENTE_FINANCIAMIENTO_ID"] = FF;
                    Fila_Nueva["PROGRAMA_ID"] = Proyecto_ID;
                    Fila_Nueva["DEPENDENCIA_ID"] = Dependencia_ID;
                    Fila_Nueva["PARTIDA_ID"] = Partida_ID;
                    Fila_Nueva["ANIO"] = DateTime.Now.Year;
                    Fila_Nueva["IMPORTE"] = Monto_Anterior;
                    Dt_Detalles.Rows.Add(Fila_Nueva);
                    Dt_Detalles.AcceptChanges();

                    if (Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO, Dt_Detalles, Obj_Comando) < 1)
                    {
                        Obj_Transaccion.Rollback();
                        throw new Exception("No se realizo el movimiento presupuestal.");
                    }


                    if (Dt_Productos_A_Cancelar.Rows.Count > 0) // Si la tabla contiene productos
                    {
                        for (int i = 0; i < Dt_Productos_A_Cancelar.Rows.Count; i++) // For para el recorrido de los productos por cada requisición
                        {
                            //** LIBERAR PRODUCTOS
                            //Consulta para el obtener el comprometido y el disponible
                            Mi_SQL = "SELECT " + Cat_Com_Productos.Campo_Comprometido + ", " + Cat_Com_Productos.Campo_Disponible + " ";
                            Mi_SQL += "FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " ";
                            Mi_SQL += "WHERE " + Cat_Com_Productos.Campo_Producto_ID + " = '";
                            Mi_SQL += Dt_Productos_A_Cancelar.Rows[i]["PRODUCTO_ID"].ToString().Trim() + "'";

                            //Ejecutar consulta
                            Dt_Aux = new DataTable();
                            Obj_Comando.CommandText = Mi_SQL;
                            Obj_Adaptador.SelectCommand = Obj_Comando;
                            Obj_Adaptador.Fill(Dt_Aux);

                            // Verificar si la consulta arrojo resultado
                            if (Dt_Aux.Rows.Count > 0)
                            {
                                //Asignar los valores de los montos
                                if (Convert.IsDBNull(Dt_Aux.Rows[0][0]) == false)
                                    Cantidad_Comprometida = Convert.ToInt32(Dt_Aux.Rows[0][0]);
                                else
                                    Cantidad_Comprometida = 0;

                                if (Convert.IsDBNull(Dt_Aux.Rows[0][1]) == false)
                                    Cantidad_Disponible = Convert.ToInt32(Dt_Aux.Rows[0][1]);
                                else
                                    Cantidad_Disponible = 0;
                            }

                            if (Dt_Productos_A_Cancelar.Rows[i]["CANTIDAD_CANCELAR"].ToString().Trim() != "") // Si 
                                Cantidad_Productos_Cancelar = Convert.ToInt32(Dt_Productos_A_Cancelar.Rows[i]["CANTIDAD_CANCELAR"].ToString().Trim());

                            if (Cantidad_Comprometida != 0)
                            {
                                // Se asigna el valor a las cantidades
                                Cantidad_Comprometida = Cantidad_Comprometida - Cantidad_Productos_Cancelar;
                                Cantidad_Disponible = Cantidad_Disponible + Cantidad_Productos_Cancelar;

                                //Consulta para modificar las cantidades en la base de datos
                                Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " ";
                                Mi_SQL += "SET " + Cat_Com_Productos.Campo_Comprometido + " = " + Cantidad_Comprometida.ToString().Trim() + ", ";
                                Mi_SQL += Cat_Com_Productos.Campo_Disponible + " = " + Cantidad_Disponible.ToString().Trim() + " ";
                                Mi_SQL += "WHERE " + Cat_Com_Productos.Campo_Producto_ID + " = '" + Dt_Productos_A_Cancelar.Rows[i]["PRODUCTO_ID"].ToString().Trim() + "'";

                                //String Producto_ID = "" + Dt_Requisiciones_Detalles.Rows[Cont_Elementos][3].ToString().Trim();
                                // Se registra  el Update en la bitacora
                                //Cls_Bitacora.Alta_Bitacora(Cls_Sessiones.Empleado_ID, Ope_Bitacora.Accion_Modificar, "Frm_Alm_Com_Orden_Salida.aspx", Producto_ID, Mi_SQL);

                                //Ejecutar consulta
                                Obj_Comando.CommandText = Mi_SQL;
                                Obj_Comando.ExecuteNonQuery();
                            }
                        }
                    }
                }

                //Ejecutar transaccion
                Obj_Transaccion.Commit();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }
                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Productos_Requisicion
        ///DESCRIPCIÓN:          Se consultan los productos de una requisición determinada
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           27/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Productos_Requisicion( String No_Requisicion)
        {
            // Declaración de Variables
            String Mi_SQL = null;
            DataTable Dt_Productos_Requisicion = new DataTable();

            Mi_SQL += " SELECT distinct SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " ";
            Mi_SQL += ", SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Costo + "";
            Mi_SQL += ", SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Cantidad + " as CANTIDAD_ENTREGADA";

            Mi_SQL += ",(select REQ_PRODUCTOS." + Ope_Com_Req_Producto.Campo_Cantidad + " from ";
            Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PRODUCTOS ";
            Mi_SQL += " where  REQ_PRODUCTOS." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = ";
            Mi_SQL += " (select SALIDAS." + Alm_Com_Salidas.Campo_Requisicion_ID + " from ";
            Mi_SQL += Alm_Com_Salidas.Tabla_Alm_Com_Salidas + " SALIDAS ";
            Mi_SQL += " where  SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = ";
            Mi_SQL += " SALIDAS." + Alm_Com_Salidas.Campo_No_Salida + ")";
            Mi_SQL += " and REQ_PRODUCTOS." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = ";
            Mi_SQL += " SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + ") as CANTIDAD_SOLICITADA ";

            Mi_SQL += ",(select REQ_PRODUCTOS." + Ope_Com_Req_Producto.Campo_Partida_ID + " from ";
            Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PRODUCTOS ";
            Mi_SQL += " where  REQ_PRODUCTOS." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = ";
            Mi_SQL += " (select SALIDAS." + Alm_Com_Salidas.Campo_Requisicion_ID + " from ";
            Mi_SQL += Alm_Com_Salidas.Tabla_Alm_Com_Salidas + " SALIDAS ";
            Mi_SQL += " where  SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = ";
            Mi_SQL += " SALIDAS." + Alm_Com_Salidas.Campo_No_Salida + ")";
            Mi_SQL += " and REQ_PRODUCTOS." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = ";
            Mi_SQL += " SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + ") as PARTIDA_ID ";

            Mi_SQL += ",(select REQ_PRODUCTOS." + Ope_Com_Req_Producto.Campo_Porcentaje_IVA + " from ";
            Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PRODUCTOS ";
            Mi_SQL += " where  REQ_PRODUCTOS." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = ";
            Mi_SQL += " (select SALIDAS." + Alm_Com_Salidas.Campo_Requisicion_ID + " from ";
            Mi_SQL += Alm_Com_Salidas.Tabla_Alm_Com_Salidas + " SALIDAS ";
            Mi_SQL += " where  SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = ";
            Mi_SQL += " SALIDAS." + Alm_Com_Salidas.Campo_No_Salida + ")";
            Mi_SQL += " and REQ_PRODUCTOS." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = ";
            Mi_SQL += " SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + ") as PORCENTAJE_IVA ";

            Mi_SQL += ",(select REQ_PRODUCTOS." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID + " from ";
            Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PRODUCTOS ";
            Mi_SQL += " where  REQ_PRODUCTOS." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = ";
            Mi_SQL += " (select SALIDAS." + Alm_Com_Salidas.Campo_Requisicion_ID + " from ";
            Mi_SQL += Alm_Com_Salidas.Tabla_Alm_Com_Salidas + " SALIDAS ";
            Mi_SQL += " where  SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = ";
            Mi_SQL += " SALIDAS." + Alm_Com_Salidas.Campo_No_Salida + ")";
            Mi_SQL += " and REQ_PRODUCTOS." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = ";
            Mi_SQL += " SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + ") as PROYECTO_PROGRAMA_ID ";

            Mi_SQL += " FROM " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + " SALIDAS_DETALLES ";
            Mi_SQL += " JOIN " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + " SALIDAS ";
            Mi_SQL += " ON SALIDAS." + Alm_Com_Salidas.Campo_No_Salida + " = ";
            Mi_SQL += " SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_No_Salida + "";
            Mi_SQL += " WHERE SALIDAS." + Alm_Com_Salidas.Campo_Requisicion_ID + " = " + No_Requisicion.Trim();

            Dt_Productos_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Productos_Requisicion;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Dependencia_ID
        ///DESCRIPCIÓN:          Método utilizado para consultar la Dependencia_ID
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           27/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Consultar_Dependencia_ID( String No_Requisicion)
        {
             // Declaración de Variables
            String Mi_SQL = null;
            SqlCommand Obj_Comando = new SqlCommand();
            DataTable Dt_Dependencia = new DataTable();
            String Dependencia_ID = "";

            Mi_SQL = " SELECT REQUISICIONES." + Ope_Com_Requisiciones.Campo_Dependencia_ID + "";
            Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "  REQUISICIONES";
            Mi_SQL += " WHERE REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = ";
            Mi_SQL += No_Requisicion.Trim();

            Dt_Dependencia = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            Dependencia_ID = "" + Dt_Dependencia.Rows[0][0].ToString();

            return Dependencia_ID.Trim();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Codigo_Programatico_RQ
        ///DESCRIPCIÓN          : Metodo que ejecuta la sentencia SQL para modificar una requisicion
        ///PARAMETROS           :
        ///CREO                 : Susana Trigueros Armenta
        ///FECHA_CREO           : 10/Noviembre/2010 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Codigo_Programatico_RQ(String Requisicion_ID, SqlCommand P_Comando)
        {
            String Mi_SQL = "";
            DataTable Dt_Requisicion = new DataTable();

            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;

            try
            {
                if (P_Comando != null)
                {
                    Cmd = P_Comando;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Transaction = Trans;
                    Cmd.Connection = Cn;
                }

                Mi_SQL = "SELECT REQ_DET." + Ope_Com_Req_Producto.Campo_Partida_ID +
                                 ",REQ_DET." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID +
                                 ", (SELECT " + Ope_Com_Requisiciones.Campo_Total +
                                 " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                                 " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                                 "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS TOTAL" +
                                 ", (SELECT " + Ope_Com_Requisiciones.Campo_Total_Cotizado +
                                 " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                                 " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                                 "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS TOTAL_COTIZADO" +
                                 ", (SELECT " + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                                 " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                                 " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                                 "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS DEPENDENCIA_ID" +
                                 ", REQ_DET." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID +
                                 ", (SELECT NUM_RESERVA" +
                                 " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                                 " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                                 "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS NUM_RESERVA" +
                                 " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_DET" +
                                 " WHERE REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + "='" + Requisicion_ID + "'";

                Cmd.CommandText = Mi_SQL;
                SqlDataAdapter Da_Datos = new SqlDataAdapter(Cmd);
                DataSet Ds_Datos = new DataSet();
                Da_Datos.Fill(Ds_Datos);

                if (Ds_Datos != null && Ds_Datos.Tables.Count > 0)
                {
                    Dt_Requisicion = Ds_Datos.Tables[0];
                }
                if (P_Comando == null)
                {
                    Trans.Commit();//aceptamos los cambios
                }
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                String Mensaje = "Error:  [" + Ex.Message + "]";
                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                if (P_Comando == null)
                {
                    Cn.Close();
                    Cmd = null;
                    Cn = null;
                    Trans = null;
                }
            }
            return Dt_Requisicion;
        }

    }
}