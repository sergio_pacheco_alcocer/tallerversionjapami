﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Salidas_S_Registro.Negocio;
using System.Data;
using JAPAMI.Constantes;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;
using JAPAMI.Polizas.Negocios;

/// <summary>
/// Summary description for Cls_Ope_Alm_Salidas_Sin_Registro_Datos
/// </summary>
namespace JAPAMI.Salidas_S_Registro.Datos
{
    public class Cls_Ope_Alm_Salidas_Sin_Registro_Datos
    {
        public Cls_Ope_Alm_Salidas_Sin_Registro_Datos()
        {

        }


         ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Informacion_General_OS
        ///DESCRIPCIÓN:          Método donde se consulta la información general de la orden de salida que se genero
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           24/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Informacion_General_OS(Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Clase_Negocio)
        {
            String Mi_SQL = null;

            //Consultamos los datos del Codigo Programatico
            Mi_SQL = "SELECT PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID;
            Mi_SQL += ", FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS CLAVE_FTE_FINANCIAMIENTO";
            Mi_SQL += ", FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FTE_FINANCIAMIENTO";
            Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID;
            Mi_SQL += ", AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Clave + " AS CLAVE_AREA_FUNCIONAL";
            Mi_SQL += ", AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS AREA_FUNCIONAL";
            Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID;
            Mi_SQL += ", PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS CLAVE_PROYECTO_PROGRAMA";
            Mi_SQL += ", PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS PROYECTO_PROGRAMA";
            Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID;
            Mi_SQL += ", DEP." + Cat_Dependencias.Campo_Clave + " AS CLAVE_DEPENDENCIA";
            Mi_SQL += ", DEP." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
            Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;
            Mi_SQL += ", PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS CLAVE_PARTIDA";
            Mi_SQL += ", PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA";
            Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID;
            Mi_SQL += ", CUENTAS." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " AS CUENTA";
            Mi_SQL += " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTOS";

            Mi_SQL += " LEFT OUTER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FTE_FIN";
            Mi_SQL += " ON FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID;

            Mi_SQL += " LEFT OUTER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " AREA_FUN";
            Mi_SQL += " ON AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID;

            Mi_SQL += " LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " PROY_PROG";
            Mi_SQL += " ON PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID;

            Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP";
            Mi_SQL += " ON DEP." + Cat_Dependencias.Campo_Dependencia_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID;

            Mi_SQL += " LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDAS";
            Mi_SQL += " ON PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;

            Mi_SQL += " LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTAS";
            Mi_SQL += " ON CUENTAS." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID;
            Mi_SQL += " WHERE PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Clase_Negocio.P_Partida_ID.Trim() + "'";
            Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '00001'";
            Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + " = '00001'";
            Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Clase_Negocio.P_Unidad_R_ID.Trim() + "'";
            Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = YEAR(GETDATE())";
            DataTable Dt_Codigo_Programatico = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            DataTable Dt_Cabecera = new DataTable();

            Mi_SQL = "SELECT " + "SALIDAS." + Alm_Com_Salidas.Campo_No_Salida + " as NO_ORDEN_SALIDA";
            Mi_SQL += ",(select DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " from ";
            Mi_SQL += Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ";
            Mi_SQL += " where DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + " ='";
            Mi_SQL += Dt_Codigo_Programatico.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID].ToString().Trim() + "')as UNIDAD_RESPONSABLE";

            Mi_SQL += ",(select distinct (FINANCIAMIENTO." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ")";
            Mi_SQL += " from " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FINANCIAMIENTO ";
            Mi_SQL += "  where FINANCIAMIENTO." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
            Mi_SQL += " = '00001')as F_FINANCIAMIENTO";

            Mi_SQL += ",(select distinct (PROY_PROGRAMAS." + Cat_Com_Proyectos_Programas.Campo_Descripcion + ")";
            Mi_SQL += " from " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + " PROY_PROGRAMAS ";
            Mi_SQL += "  where PROY_PROGRAMAS." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID;
            Mi_SQL += " = '" + Dt_Codigo_Programatico.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID].ToString().Trim() + "'";
            Mi_SQL += ")as PROGRAMA";

            Mi_SQL += ", NULL AS FOLIO ";
            Mi_SQL += ", NULL AS " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion;
            Mi_SQL += ", SALIDAS." + Alm_Com_Salidas.Campo_Usuario_Creo + " as ENTREGO ";

            Mi_SQL += ", (select EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + " +' '+";
            Mi_SQL += " EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + " +' '+";
            Mi_SQL += " EMPLEADOS." + Cat_Empleados.Campo_Nombre;
            Mi_SQL += " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS ";
            Mi_SQL += " where EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + " = '";
            Mi_SQL += Clase_Negocio.P_Empleado_Recibio_ID + "') as RECIBIO";
            Mi_SQL += ", SALIDAS." + Alm_Com_Salidas.Campo_Fecha_Creo + " as FECHA_CREO ";
            Mi_SQL += " FROM " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + " SALIDAS ";
            Mi_SQL += " WHERE ";
            Mi_SQL += " SALIDAS." + Alm_Com_Salidas.Campo_No_Salida + " = ";
            Mi_SQL += Clase_Negocio.P_No_Orden_Salida;

            Dt_Cabecera = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Cabecera;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Detalles_Orden_Salida
        ///DESCRIPCIÓN:          Método donde se consultan los detalles de la orden de salida que se genero
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           24/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Detalles_Orden_Salida(Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Datos)
        {            
            // Declaración de variables
            String Mi_SQL = null;
            DataTable Dt_Detalles = new DataTable();

            Mi_SQL = "SELECT SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " as NO_ORDEN_SALIDA";
            Mi_SQL += ",(select PRODUCTOS." + Cat_Com_Productos.Campo_Clave + " from ";
            Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS ";
            Mi_SQL += " where SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = PRODUCTOS.";
            Mi_SQL += Cat_Com_Productos.Campo_Producto_ID + ")as CLAVE";

            Mi_SQL += ",(select PRODUCTOS." + Cat_Com_Productos.Campo_Nombre + " +' '+ ";
            Mi_SQL += " PRODUCTOS." + Cat_Com_Productos.Campo_Descripcion + " from ";
            Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS ";
            Mi_SQL += " where SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = PRODUCTOS.";
            Mi_SQL += Cat_Com_Productos.Campo_Producto_ID + ")as PRODUCTO";

            Mi_SQL += ",SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Cantidad + " as CANTIDAD_SOLICITADA ";

            Mi_SQL += ",SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Cantidad + " as CANTIDAD_ENTREGADA";

            Mi_SQL += ",(select UNIDADES." + Cat_Com_Unidades.Campo_Abreviatura + " from ";
            Mi_SQL += Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " UNIDADES ";
            Mi_SQL += " where  UNIDADES." + Cat_Com_Unidades.Campo_Unidad_ID + " = ";
            Mi_SQL += " (select PRODUCTOS." + Cat_Com_Productos.Campo_Unidad_ID + " from ";
            Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS ";
            Mi_SQL += " where  SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " = ";
            Mi_SQL += " PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID + ")) as UNIDADES";

            Mi_SQL += ",SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Costo + " as PRECIO";
            Mi_SQL += ",SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Subtotal + "";
            Mi_SQL += ",SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_IVA + "";
            Mi_SQL += ",SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_Importe + " as TOTAL";

            Mi_SQL += " FROM " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + " SALIDAS_DETALLES";
            Mi_SQL += " WHERE SALIDAS_DETALLES." + Alm_Com_Salidas_Detalles.Campo_No_Salida + " = ";
            Mi_SQL += Datos.P_No_Orden_Salida;

            Dt_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Detalles;
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Insertar_Salida
        /// 	DESCRIPCIÓN: Inserta la salida del almacen 
        /// 	PARÁMETROS:Clase Negocio 
        /// 	CREO: Susana Trigueros Armenta
        /// 	FECHA_CREO: 6-JUN-13
        /// 	MODIFICÓ:
        /// 	FECHA_MODIFICÓ:
        /// 	CAUSA_MODIFICACIÓN:
        ///*******************************************************************************************************
        public static Int64 Insertar_Salida(Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Clase_Negocio)
        {
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            Object Aux; // Variable auxiliar para las consultas
            String Mensaje = String.Empty; //Variable para el mensaje de error
            DataTable Dt_Aux = new DataTable(); //Tabla auxiliar para las consultas
            SqlDataAdapter Obj_Adaptador; //Adapatador para el llenado de las tablas
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Comando = new SqlCommand();
            Obj_Adaptador = new SqlDataAdapter();
            Obj_Conexion.Open();
            Obj_Transaccion = Obj_Conexion.BeginTransaction();
            Obj_Comando.Transaction = Obj_Transaccion;
            Obj_Comando.Connection = Obj_Conexion;
            try
            {               

                //Asignar consulta para el Maximo ID
                Mi_SQL = "SELECT ISNULL(MAX(" + Alm_Com_Salidas.Campo_No_Salida + "), 0) FROM " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Aux = Obj_Comando.ExecuteScalar();

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                    Clase_Negocio.P_No_Orden_Salida = Convert.ToInt64(Aux) + 1;
                else
                    Clase_Negocio.P_No_Orden_Salida = 1;

          
            // Consulta para dar de alta la salida
            Mi_SQL = "INSERT INTO " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + " (" + Alm_Com_Salidas.Campo_No_Salida + ", ";
            Mi_SQL += Alm_Com_Salidas.Campo_Dependencia_ID + ", ";
            Mi_SQL += Alm_Com_Salidas.Campo_Empleado_Solicito_ID  + ", ";
            Mi_SQL += Alm_Com_Salidas.Campo_Usuario_Creo + ", ";
            Mi_SQL += Alm_Com_Salidas.Campo_Fecha_Creo + ", " + Alm_Com_Salidas.Campo_Empleado_Almacen_ID + ", ";
            Mi_SQL += Alm_Com_Salidas.Campo_Subtotal + " , " + Alm_Com_Salidas.Campo_IVA + ", " + Alm_Com_Salidas.Campo_Total;
            if(Clase_Negocio.P_Observaciones != null)
            {
                Mi_SQL = Mi_SQL + ", OBSERVACIONES ";
            }

            Mi_SQL += ") VALUES(" + Clase_Negocio.P_No_Orden_Salida + ", ";
            Mi_SQL += "'" + Clase_Negocio.P_Unidad_R_ID + "', '" + Clase_Negocio.P_Empleado_Recibio_ID + "', ";                        
            Mi_SQL += "'" + Cls_Sessiones.Nombre_Empleado + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID.Trim() + "', ";
            Mi_SQL += Clase_Negocio.P_Subtotal + ", " + Clase_Negocio.P_IVA + ", " + Clase_Negocio.P_Total;
            if (Clase_Negocio.P_Observaciones != null)
            {
                Mi_SQL += ", '" + Clase_Negocio.P_Observaciones + "'";
            }
            Mi_SQL += ")";
            //Ejecutar consulta
            Obj_Comando.CommandText = Mi_SQL;
            Obj_Comando.ExecuteNonQuery();

                //Insertamos los detalles
            for (int Cont_Elementos = 0; Cont_Elementos < Clase_Negocio.P_Dt_Productos.Rows.Count; Cont_Elementos++)
            {
                //Consulta para dar de alta los detalles de la salida
                Mi_SQL = "INSERT INTO " + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + " (" + Alm_Com_Salidas_Detalles.Campo_No_Salida + ", ";
                Mi_SQL += Alm_Com_Salidas_Detalles.Campo_Producto_ID + ", " + Alm_Com_Salidas_Detalles.Campo_Cantidad + ", ";
                Mi_SQL += Alm_Com_Salidas_Detalles.Campo_Costo + ", " + Alm_Com_Salidas_Detalles.Campo_Costo_Promedio + ", ";
                Mi_SQL += Alm_Com_Salidas_Detalles.Campo_Subtotal + ", " + Alm_Com_Salidas_Detalles.Campo_IVA + ", ";
                Mi_SQL += Alm_Com_Salidas_Detalles.Campo_Importe + ") VALUES(" + Clase_Negocio.P_No_Orden_Salida + ", ";
                Mi_SQL += "'" + Clase_Negocio.P_Dt_Productos.Rows[Cont_Elementos]["Producto_ID"].ToString().Trim() + "', ";
                Mi_SQL += Clase_Negocio.P_Dt_Productos.Rows[Cont_Elementos]["Cantidad"].ToString().Trim() + ", ";
                Mi_SQL += Clase_Negocio.P_Dt_Productos.Rows[Cont_Elementos]["Costo_Promedio"].ToString().Trim() + ", ";
                Mi_SQL += Clase_Negocio.P_Dt_Productos.Rows[Cont_Elementos]["Costo_Promedio"].ToString().Trim() + ", ";
                Mi_SQL += Clase_Negocio.P_Dt_Productos.Rows[Cont_Elementos]["Importe"].ToString().Trim() + ", ";
                Mi_SQL += Clase_Negocio.P_Dt_Productos.Rows[Cont_Elementos]["Monto_IVA"].ToString().Trim() + ", ";
                Mi_SQL += Clase_Negocio.P_Dt_Productos.Rows[Cont_Elementos]["Monto_Total"].ToString().Trim() + ")";
                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();
                //actualizamos existencia de cada producto
                Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL = Mi_SQL + " SET " + Cat_Com_Productos.Campo_Existencia;
                Mi_SQL = Mi_SQL + "=" + Cat_Com_Productos.Campo_Existencia + " - ";
                Mi_SQL = Mi_SQL + Clase_Negocio.P_Dt_Productos.Rows[Cont_Elementos]["Cantidad"].ToString().Trim() + ",";
                Mi_SQL = Mi_SQL + Cat_Com_Productos.Campo_Disponible;
                Mi_SQL = Mi_SQL + "=" + Cat_Com_Productos.Campo_Disponible + " - ";
                Mi_SQL = Mi_SQL + Clase_Negocio.P_Dt_Productos.Rows[Cont_Elementos]["Cantidad"].ToString().Trim();
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Productos.Campo_Producto_ID + "=";
                Mi_SQL = Mi_SQL + "'" + Clase_Negocio.P_Dt_Productos.Rows[Cont_Elementos]["Producto_ID"].ToString().Trim() + "'";
                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();              
                
            }
            //Damos de alta la poliza
            String Alta_poliza = Alta_Poliza(Clase_Negocio.P_No_Orden_Salida.ToString(), Double.Parse(Clase_Negocio.P_Total), Obj_Comando,Clase_Negocio);
            if (Alta_poliza != "Alta_Exitosa")
            {
                throw new Exception(Alta_poliza);
            }


            //Ejecutar transaccion
            Obj_Transaccion.Commit();

            //Entregar resultado
            return Clase_Negocio.P_No_Orden_Salida;
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }
                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Conexion.Close();                
            }
           
           
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Alta_Poliza
        /// DESCRIPCION:            Método utilizado consultar la información utilizada para mostrar la orden de salida
        /// PARAMETROS :            
        /// CREO       :            Salvador Hernández Ramírez
        /// FECHA_CREO :            24/Junio/2011  
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        private static String Alta_Poliza(String No_Salida, Double Monto_Total, SqlCommand P_Cmd,Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Clase_Negocio)
        {
            SqlDataAdapter Da = new SqlDataAdapter();
            DataTable Dt_Auxiliar = new DataTable();
            DataTable Dt_Cuentas_Almacen = new DataTable();
            DataTable Dt_Cuenta = new DataTable();
            String Mensaje = "";
            String Mi_SQL = "";
            String Tipo_Poliza_ID = "";
            Boolean Almacen_General = true;

            Mi_SQL = "SELECT * FROM " + Cat_Alm_Parametros_Cuentas.Tabla_Cat_Alm_Parametros_Cuentas;
            P_Cmd.CommandText = Mi_SQL;
            Da.SelectCommand = P_Cmd;
            Dt_Cuentas_Almacen = new DataTable();
            Da.Fill(Dt_Cuentas_Almacen);

            //Mi_SQL = "SELECT DISTINCT(" + Ope_Com_Req_Producto.Campo_Tipo + ") FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
            //Mi_SQL += " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion;
            //P_Cmd.CommandText = Mi_SQL;
            //Da.SelectCommand = P_Cmd;
            DataTable Dt_Tipo_Almacen = new DataTable();
           
            
                Mi_SQL = "SELECT " + Cat_Com_Productos.Campo_Almacen_General;
                Mi_SQL += " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL += " WHERE " + Cat_Com_Productos.Campo_Producto_ID + " = '" + Clase_Negocio.P_Dt_Productos.Rows[0]["Producto_ID"].ToString().Trim() + "'";
                P_Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = P_Cmd;
                Dt_Tipo_Almacen = new DataTable();
                Da.Fill(Dt_Tipo_Almacen);

                if (Dt_Tipo_Almacen.Rows.Count == 1 && Dt_Tipo_Almacen.Rows[0][Cat_Com_Productos.Campo_Almacen_General].ToString().Trim() == "NO")
                {
                    Almacen_General = false;
                }            

            Mi_SQL = "SELECT " + Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID;
            Mi_SQL += " FROM " + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas;
            Mi_SQL += " WHERE " + Cat_Con_Tipo_Polizas.Campo_Descripcion + " LIKE '%DIARIO%'";
            P_Cmd.CommandText = Mi_SQL;
            Tipo_Poliza_ID = P_Cmd.ExecuteScalar().ToString().Trim();

            DataTable Dt_Detalles_Poliza = new DataTable();
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("MOMENTO_FINAL", typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("Nombre_Cuenta", typeof(System.String));
            Dt_Detalles_Poliza.AcceptChanges();
           
            Mi_SQL = "SELECT PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID;
            Mi_SQL += ", FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS CLAVE_FTE_FINANCIAMIENTO";
            Mi_SQL += ", FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FTE_FINANCIAMIENTO";
            Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID;
            Mi_SQL += ", AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Clave + " AS CLAVE_AREA_FUNCIONAL";
            Mi_SQL += ", AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS AREA_FUNCIONAL";
            Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID;
            Mi_SQL += ", PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS CLAVE_PROYECTO_PROGRAMA";
            Mi_SQL += ", PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS PROYECTO_PROGRAMA";
            Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID;
            Mi_SQL += ", DEP." + Cat_Dependencias.Campo_Clave + " AS CLAVE_DEPENDENCIA";
            Mi_SQL += ", DEP." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
            Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;
            Mi_SQL += ", PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS CLAVE_PARTIDA";
            Mi_SQL += ", PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA";
            Mi_SQL += ", PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID;
            Mi_SQL += ", CUENTAS." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " AS CUENTA";
            Mi_SQL += " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTOS";

            Mi_SQL += " LEFT OUTER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FTE_FIN";
            Mi_SQL += " ON FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID;

            Mi_SQL += " LEFT OUTER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " AREA_FUN";
            Mi_SQL += " ON AREA_FUN." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID;

            Mi_SQL += " LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " PROY_PROG";
            Mi_SQL += " ON PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID;

            Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP";
            Mi_SQL += " ON DEP." + Cat_Dependencias.Campo_Dependencia_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID;

            Mi_SQL += " LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDAS";
            Mi_SQL += " ON PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;

            Mi_SQL += " LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTAS";
            Mi_SQL += " ON CUENTAS." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID;
            Mi_SQL += " WHERE PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Clase_Negocio.P_Partida_ID.Trim() + "'";
            Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '00001'";
            Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + " = '00001'";
            Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Clase_Negocio.P_Unidad_R_ID.Trim() + "'";
            Mi_SQL += " AND PRESUPUESTOS." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = YEAR(GETDATE())";
            P_Cmd.CommandText = Mi_SQL;
            Da.SelectCommand = P_Cmd;
            Dt_Cuenta = new DataTable();
            Da.Fill(Dt_Cuenta);            


            if (Dt_Cuenta.Rows.Count > 0 && !String.IsNullOrEmpty(Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID].ToString().Trim()))
            {
                DataRow row = Dt_Detalles_Poliza.NewRow();
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID].ToString().Trim();
                row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Dt_Cuenta.Rows[0]["CUENTA"].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "No. Salida " + Clase_Negocio.P_No_Orden_Salida;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Clase_Negocio.P_Total;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0.0;
                row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID].ToString().Trim();
                row["Nombre_Cuenta"] = Dt_Cuenta.Rows[0]["CUENTA"].ToString().Trim();
                Dt_Detalles_Poliza.Rows.Add(row);
                Dt_Detalles_Poliza.AcceptChanges();

                if (Dt_Cuentas_Almacen.Rows.Count > 0)
                {
                    Mi_SQL = "SELECT * FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                    Mi_SQL += " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = ";
                    Mi_SQL += Almacen_General ?
                            Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_General_ID].ToString().Trim() :
                            Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_Papeleria_ID].ToString().Trim();

                    P_Cmd.CommandText = Mi_SQL;
                    Da.SelectCommand = P_Cmd;
                    Dt_Auxiliar = new DataTable();
                    Da.Fill(Dt_Auxiliar);

                    if (Dt_Auxiliar.Rows.Count > 0)
                    {
                        row = Dt_Detalles_Poliza.NewRow();
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();
                        row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                        row["Nombre_Cuenta"] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "No. Salida " + Clase_Negocio.P_No_Orden_Salida; ;
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0.0;
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = Clase_Negocio.P_Total;
                        row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Dt_Cuenta.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID].ToString().Trim();
                        Dt_Detalles_Poliza.Rows.Add(row);
                        Dt_Detalles_Poliza.AcceptChanges();


                        Cls_Ope_Con_Polizas_Negocio Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio();

                        Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = Tipo_Poliza_ID;
                        Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now);
                        Rs_Alta_Ope_Con_Polizas.P_Fecha_Poliza = DateTime.Now;
                        Rs_Alta_Ope_Con_Polizas.P_Concepto = "Salida " + Clase_Negocio.P_No_Orden_Salida + " STOCK,Almacen ";
                        Rs_Alta_Ope_Con_Polizas.P_Concepto += Almacen_General ? "General " : "Papeleria ";
                        Rs_Alta_Ope_Con_Polizas.P_Total_Debe = Double.Parse(Clase_Negocio.P_Total); //monto total con iva de productos
                        Rs_Alta_Ope_Con_Polizas.P_Total_Haber = Double.Parse(Clase_Negocio.P_Total);
                        Rs_Alta_Ope_Con_Polizas.P_No_Partida = 2;
                        Rs_Alta_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                        Rs_Alta_Ope_Con_Polizas.P_Dt_Detalles_Polizas = Dt_Detalles_Poliza;
                        Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID;
                        Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Autorizo = Cls_Sessiones.Empleado_ID;
                        Rs_Alta_Ope_Con_Polizas.P_Prefijo = "";
                        Rs_Alta_Ope_Con_Polizas.P_Cmmd = P_Cmd;
                        string[] Datos_Poliza = Rs_Alta_Ope_Con_Polizas.Alta_Poliza();
                        Mensaje = "Alta_Exitosa";
                    }
                    else
                    {
                        Mensaje = "No se encontraron los datos de la cuenta contable ";
                        Mensaje += Almacen_General ? "General " : "Papeleria ";
                        Mensaje += "de Almacen.";
                    }
                }
                else
                {
                    Mensaje = "No se han asignado las cuentas contables General ó Papeleria para Almacen.";
                }
            }
            else
            {
                Mensaje = "No se tiene cuenta contable para la partida";
            }
            return Mensaje;
        }


        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_UR
        /// 	DESCRIPCIÓN: Consulta los datos de la UR
        /// 	PARÁMETROS:
        /// 		1. Datos: Indica que registro se desea consultar a la base de datos
        /// 	CREO: Susana Trigueros Armenta
        /// 	FECHA_CREO: 31-MAY-13
        /// 	MODIFICÓ:
        /// 	FECHA_MODIFICÓ:
        /// 	CAUSA_MODIFICACIÓN:
        ///*******************************************************************************************************
  
        public static DataTable Consulta_UR(Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Clase_Negocio)
        {
            String Mi_SQL = " SELECT " + Cat_Dependencias.Campo_Dependencia_ID;
            Mi_SQL = Mi_SQL + ", " + Cat_Dependencias.Campo_Nombre;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Dependencias.Campo_Estatus;
            Mi_SQL = Mi_SQL + " ='ACTIVO'";
            if (Clase_Negocio.P_Clave_UR != null)
            {
                Mi_SQL = Mi_SQL + " AND ( UPPER(" + Cat_Dependencias.Campo_Clave + ") LIKE UPPER('%" + Clase_Negocio.P_Clave_UR + "%')";
                Mi_SQL = Mi_SQL + " OR UPPER(" + Cat_Dependencias.Campo_Nombre + ") LIKE UPPER('%" + Clase_Negocio.P_Clave_UR + "%'))";

            }
            Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Dependencias.Campo_Nombre;
            DataTable Dt_UR = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];            
            return Dt_UR;
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_Partida
        /// 	DESCRIPCIÓN: Consulta los datos de la UR
        /// 	PARÁMETROS:
        /// 		1. Datos: Indica que registro se desea consultar a la base de datos
        /// 	CREO: Susana Trigueros Armenta
        /// 	FECHA_CREO: 31-MAY-13
        /// 	MODIFICÓ:
        /// 	FECHA_MODIFICÓ:
        /// 	CAUSA_MODIFICACIÓN:
        ///*******************************************************************************************************
        public static DataTable Consulta_Partida(Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Clase_Negocio)
        {
            String Mi_SQL = " SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "," + Cat_Sap_Partidas_Especificas.Campo_Clave;
            Mi_SQL = Mi_SQL + " +' '+ " + Cat_Sap_Partidas_Especificas.Campo_Nombre;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Estatus;
            Mi_SQL = Mi_SQL + " ='ACTIVO'";
            if (Clase_Negocio.P_Clave_Partida != null)
            {
                Mi_SQL = Mi_SQL + " AND ( UPPER(" + Cat_Sap_Partidas_Especificas.Campo_Clave + ") LIKE UPPER('%" + Clase_Negocio.P_Clave_Partida + "%')";
                Mi_SQL = Mi_SQL + " OR UPPER(" + Cat_Sap_Partidas_Especificas.Campo_Nombre + ") LIKE UPPER('%" + Clase_Negocio.P_Clave_Partida + "%'))";

            }
            Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Sap_Partidas_Especificas.Campo_Nombre;
            DataTable Dt_Partidas = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Partidas;
        }
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_Empleado
        /// 	DESCRIPCIÓN: Consulta los datos de la UR
        /// 	PARÁMETROS:
        /// 		1. Datos: Indica que registro se desea consultar a la base de datos
        /// 	CREO: Susana Trigueros Armenta
        /// 	FECHA_CREO: 31-MAY-13
        /// 	MODIFICÓ:
        /// 	FECHA_MODIFICÓ:
        /// 	CAUSA_MODIFICACIÓN:
        ///*******************************************************************************************************
        public static DataTable Consulta_Empleado(Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Clase_Negocio)
        {
            String Mi_SQL = " SELECT " + Cat_Empleados.Campo_Empleado_ID;
            Mi_SQL = Mi_SQL + ", " + Cat_Empleados.Campo_Apellido_Paterno;
            Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Apellido_Materno;
            Mi_SQL = Mi_SQL + "+' '+ " + Cat_Empleados.Campo_Nombre;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Empleados.Campo_Estatus;
            Mi_SQL = Mi_SQL + " ='ACTIVO'";
            if (Clase_Negocio.P_Empleado != null)
            {
                Mi_SQL = Mi_SQL + " AND ( UPPER(" + Cat_Empleados.Campo_No_Empleado + ") LIKE UPPER('%" + Clase_Negocio.P_Empleado + "%')";
                Mi_SQL = Mi_SQL + " OR UPPER(" + Cat_Empleados.Campo_Nombre + ") LIKE UPPER('%" + Clase_Negocio.P_Empleado + "%')";
                Mi_SQL = Mi_SQL + " OR UPPER(" + Cat_Empleados.Campo_Apellido_Paterno + ") LIKE UPPER('%" + Clase_Negocio.P_Empleado + "%')";
                Mi_SQL = Mi_SQL + " OR UPPER(" + Cat_Empleados.Campo_Apellido_Materno + ") LIKE UPPER('%" + Clase_Negocio.P_Empleado + "%'))";
            }
            Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Empleados.Campo_Apellido_Paterno + " ASC";
            DataTable Dt_Empleados = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Empleados;
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_Empleado
        /// 	DESCRIPCIÓN: Consulta los datos de la UR
        /// 	PARÁMETROS:
        /// 		1. Datos: Indica que registro se desea consultar a la base de datos
        /// 	CREO: Susana Trigueros Armenta
        /// 	FECHA_CREO: 31-MAY-13
        /// 	MODIFICÓ:
        /// 	FECHA_MODIFICÓ:
        /// 	CAUSA_MODIFICACIÓN:
        ///*******************************************************************************************************
        public static DataTable Consulta_Producto(Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Clase_Negocio)
        {
            String Mi_SQL = " SELECT " + Cat_Com_Productos.Campo_Producto_ID;
            Mi_SQL = Mi_SQL + ", " + Cat_Com_Productos.Campo_Clave;
            Mi_SQL = Mi_SQL + "+' '+" + Cat_Com_Productos.Campo_Nombre;
            Mi_SQL = Mi_SQL + "+' '+" + Cat_Com_Productos.Campo_Descripcion + " AS NOMBRE";
            Mi_SQL = Mi_SQL + ", " + Cat_Com_Productos.Campo_Existencia;
            Mi_SQL = Mi_SQL + ", " + Cat_Com_Productos.Campo_Disponible;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Productos.Campo_Estatus;
            Mi_SQL = Mi_SQL + " ='ACTIVO'";
            if (Clase_Negocio.P_Producto != null)
            {
                Mi_SQL = Mi_SQL + " AND ( UPPER(" + Cat_Com_Productos.Campo_Nombre + ") LIKE UPPER('%" + Clase_Negocio.P_Producto + "%')";
                Mi_SQL = Mi_SQL + " OR UPPER(" + Cat_Com_Productos.Campo_Clave + ") LIKE UPPER('%" + Clase_Negocio.P_Producto + "%')";
                Mi_SQL = Mi_SQL + " OR UPPER(" + Cat_Com_Productos.Campo_REF_JAPAMI + ") LIKE UPPER('%" + Clase_Negocio.P_Producto + "%'))";               
            }
            if (Clase_Negocio.P_Partida_ID != null)
            {
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Productos.Campo_Partida_ID + "='" + Clase_Negocio.P_Partida_ID.Trim() + "'";
            }
            Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Com_Productos.Campo_Nombre + " ASC";
            if (Clase_Negocio.P_Producto_ID != null)
            {
                Mi_SQL = " SELECT PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID;
                Mi_SQL = Mi_SQL + ", PRODUCTOS." + Cat_Com_Productos.Campo_Clave;
                Mi_SQL = Mi_SQL + "+' '+ PRODUCTOS." + Cat_Com_Productos.Campo_Nombre;
                Mi_SQL = Mi_SQL + "+' '+ PRODUCTOS." + Cat_Com_Productos.Campo_Descripcion;
                Mi_SQL = Mi_SQL + " AS NOMBRE , UNIDAD." + Cat_Com_Unidades.Campo_Abreviatura + " AS UNIDAD";
                Mi_SQL = Mi_SQL + ", PRODUCTOS." + Cat_Com_Productos.Campo_Costo_Promedio;
                Mi_SQL = Mi_SQL + ", NULL AS IMPORTE";
                Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Com_Impuestos.Campo_Porcentaje_Impuesto;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Impuestos.Campo_Impuesto_ID;
                Mi_SQL = Mi_SQL + "= PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_ID;
                Mi_SQL = Mi_SQL + ") AS PORCENTAJE_IVA";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS";
                Mi_SQL = Mi_SQL + " JOIN " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " UNIDAD ";
                Mi_SQL = Mi_SQL + " ON PRODUCTOS." + Cat_Com_Productos.Campo_Unidad_ID;
                Mi_SQL = Mi_SQL + "= UNIDAD." + Cat_Com_Unidades.Campo_Unidad_ID;
                Mi_SQL = Mi_SQL + " WHERE PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID;
                Mi_SQL = Mi_SQL + "='" + Clase_Negocio.P_Producto_ID.Trim() + "'";
                               
            }
            
            DataTable Dt_Productos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Productos;
        }
    }
}