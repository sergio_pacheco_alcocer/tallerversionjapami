﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using System.Text;
using JAPAMI.Alerta_Pto_Reorden.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Alm_Alerta_Punto_Reorden_Datos
/// </summary>
/// 
namespace JAPAMI.Alerta_Pto_Reorden.Datos
{
    public class Cls_Ope_Alm_Alerta_Punto_Reorden_Datos
    {

        public static DataTable Consultar_Productos_Reorden()
        {
            StringBuilder My_SQL = new StringBuilder();
            My_SQL.Append("SELECT " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Clave + " ,");
            My_SQL.Append(Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Nombre + " +' '+ ");
            My_SQL.Append( Cat_Com_Productos.Campo_Descripcion + " AS NOMBRE, ");
            My_SQL.Append(Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Existencia + ", ");
            My_SQL.Append(Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Reorden + ", ");
            My_SQL.Append(Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Almacen_General + ", ");
            My_SQL.Append("(SELECT " + Cat_Com_Unidades.Campo_Nombre);
            My_SQL.Append(" FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades);
            My_SQL.Append(" WHERE " + Cat_Com_Unidades.Campo_Unidad_ID);
            My_SQL.Append("=" + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Unidad_ID + ") AS UNIDAD");
            My_SQL.Append(" FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos);
            My_SQL.Append(" WHERE " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Existencia);
            My_SQL.Append(" <=" + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Reorden);
            My_SQL.Append(" AND " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Stock + "='SI'");


            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, My_SQL.ToString()).Tables[0];
        }

    }
}