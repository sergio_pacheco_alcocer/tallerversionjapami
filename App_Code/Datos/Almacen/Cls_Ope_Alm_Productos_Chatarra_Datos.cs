﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Productos_Chatarra.Negocio;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;


/// <summary>
/// Summary description for Cls_Ope_Alm_Productos_Chatarra_Datos
/// </summary>
namespace JAPAMI.Productos_Chatarra.Datos
{
    public class Cls_Ope_Alm_Productos_Chatarra_Datos
    {
        public Cls_Ope_Alm_Productos_Chatarra_Datos()
        {
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Productos
        ///DESCRIPCION:             Consultar los productos con filtros
        ///PARAMETROS:              Datos: Variable de la capa de negocios
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              21/Marzo/2012 13:59
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Productos(Cls_Ope_Alm_Productos_Chatarra_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL; //variable para la consulta
            DataTable Dt_Productos = new DataTable(); //Variable para la consulta de los productos
            DataTable Dt_Resultado = new DataTable(); //Variable para el resultado de la consulta
            DataTable Dt_Resguardos = new DataTable(); //Variable para la consulta de los resguardos
            Boolean Where_Utilizado = false; //Variable que indica si la clausula where ya ha sido utilizada
            int Cont_Elementos; //Variable para el contador
            DataRow Renglon; //renglon para el llenado de la tabla
            int Cont_Resguardos; //variable para el contador de los resguardos
            int Cont_Columnas; //variable para el contador de las columnas
            
            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT Cat_Com_Productos.Producto_ID, Cat_Com_Productos.Clave, Cat_Com_Productos.Ref_JAPAMI, Cat_Com_Productos.Nombre, " +
                    "(Cat_SAP_Partida_Generica.Clave + ' ' + Cat_SAP_Partida_Generica.Descripcion) AS Partida_Generica, " +
                    "(Cat_SAP_Partidas_Especificas.Clave + ' ' + Cat_SAP_Partidas_Especificas.Descripcion) AS Partida_Especifica, " +
                    "Cat_Com_Productos.Descripcion, Cat_Com_Impuestos.Nombre AS Impuesto_1, Cat_Com_Impuestos_2.Nombre AS Impuesto_2, Cat_Com_Productos.Costo, " +
                    "Cat_Com_Productos.Costo_Promedio, Cat_Com_Productos.P_Estatus, Cat_Com_Productos.Existencia, Cat_Com_Productos.Comprometido, " +
                    "Cat_Com_Productos.Disponible, Cat_Com_Productos.Maximo, Cat_Com_Productos.Minimo, Cat_Com_Productos.Reorden, Cat_Com_Productos.Ubicacion, " +
                    "Cat_Com_Productos.Tipo, Cat_Com_Productos.Stock, " +
                    "('PG: ' + Cat_SAP_Partida_Generica.Clave + ' ' + Cat_SAP_Partida_Generica.Descripcion + '<br />' + " +
                    "'PE: ' + Cat_SAP_Partidas_Especificas.Clave + ' ' + Cat_SAP_Partidas_Especificas.Descripcion) AS Partida, " +
                    "(Cat_Com_Productos.Nombre + ' ' + Cat_Com_Productos.Descripcion) AS Nombre_Completo, " +
                    "Cat_Com_Unidades.Nombre AS Unidad, Resguardo = 0, Resguardante = '' " +
                    "FROM Cat_Com_Productos " +
                    "LEFT JOIN Cat_Com_Impuestos ON Cat_Com_Productos.Impuesto_ID = Cat_Com_Impuestos.Impuesto_ID " +
                    "LEFT JOIN Cat_Com_Impuestos Cat_Com_Impuestos_2 ON Cat_Com_Productos.Impuesto_2_ID = Cat_Com_Impuestos_2.Impuesto_ID " +
                    "LEFT JOIN Cat_SAP_Partidas_Especificas ON Cat_Com_Productos.Partida_ID = Cat_SAP_Partidas_Especificas.Partida_ID " +
                    "LEFT JOIN Cat_SAP_Partida_Generica ON Cat_SAP_Partidas_Especificas.Partida_Generica_ID = Cat_SAP_Partida_Generica.Partida_Generica_ID " +
                    "LEFT JOIN Cat_Com_Unidades ON Cat_Com_Productos.Unidad_ID = Cat_Com_Unidades.Unidad_ID ";

                //Verificar el tipo de la consulta
                switch (Datos.P_Tipo_Consulta)
                {
                    case "Producto_ID":
                        Mi_SQL += "WHERE Cat_Com_Productos.Producto_ID = '" + Datos.P_Producto_ID.Trim() + "' ";
                        break;

                    case "Clave":
                        Mi_SQL += "WHERE Cat_Com_Productos.Clave = '" + Datos.P_Clave.Trim() + "' ";
                        break;

                    case "Ref_JAPAMI":
                        Mi_SQL += "WHERE Cat_Com_Productos.Ref_JAPAMI = " + Datos.P_Ref_JAPAMI.ToString().Trim() + " ";
                        break;

                    case "Nombre":
                        Mi_SQL += "WHERE Cat_Com_Productos.Nombre LIKE '%" + Datos.P_Nombre.Trim() + "%'";
                        break;

                    default:
                        //Verificar si se ha seleccionado una unidad
                        if (Datos.P_Unidad_ID != null && Datos.P_Unidad_ID != "" && Datos.P_Unidad_ID != String.Empty)
                        {
                            //Verificar si es la opcion nula
                            if (Datos.P_Unidad_ID == "NULO")
                            {
                                Mi_SQL += "WHERE Cat_Com_Productos.Unidad_ID IS NULL ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE Cat_Com_Productos.Unidad_ID = '" + Datos.P_Unidad_ID.Trim() + "' ";
                            }

                            Where_Utilizado = true;
                        }

                        //Verificar si se ha seleccionado un impuesto
                        if (Datos.P_Impuesto_ID != null && Datos.P_Impuesto_ID != "" && Datos.P_Impuesto_ID != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Resto del filtro
                            //Verificar si es la opcion nula
                            if (Datos.P_Impuesto_ID == "NULO")
                            {
                                Mi_SQL += "(Cat_Com_Productos.Impuesto_ID IS NULL AND Cat_Com_Productos.Impuesto_2_ID IS NULL ";
                            }
                            else
                            {
                                Mi_SQL += "(Cat_Com_Productos.Impuesto_ID = '" + Datos.P_Impuesto_ID + "' " +
                                    "OR Cat_Com_Productos.Impuesto_2_ID = '" + Datos.P_Impuesto_ID + "') ";
                            }
                        }

                        //Verificar si se ha seleccionado un estatus
                        if (Datos.P_Estatus != null && Datos.P_Estatus != "" && Datos.P_Estatus != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Resto del filtro
                            Mi_SQL += "Cat_Com_Productos.P_Estatus = '" + Datos.P_Estatus.Trim() + "' ";
                        }

                        //Verificar si se ha seleccionado un tipo
                        if (Datos.P_Tipo != null && Datos.P_Tipo != "" && Datos.P_Tipo != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Resto del filtro
                            //Verificar si es la opcion nula
                            if (Datos.P_Tipo == "NULO")
                            {
                                Mi_SQL += "Cat_Com_Productos.Tipo IS NULL ";
                            }
                            else
                            {
                                Mi_SQL += "Cat_Com_Productos.Tipo = '" + Datos.P_Tipo.Trim() + "' ";
                            }
                        }

                        //Verificar si se ha seleccionado el stock
                        if (Datos.P_Stock != null && Datos.P_Stock != "" && Datos.P_Stock != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Resto del filtro
                            Mi_SQL = "Cat_Com_Productos.Stock = '" + Datos.P_Stock.Trim() + "' ";
                        }

                        //Verificar si se ha seleccionado una partida generica
                        if (Datos.P_Partida_Generica_ID != null && Datos.P_Partida_Generica_ID != "" && Datos.P_Partida_Generica_ID != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Verificar si es la opcion nula
                            if (Datos.P_Partida_Generica_ID == "NULO")
                            {
                                Mi_SQL += "Cat_Com_Productos.Partida_ID IS NULL ";
                            }
                            else
                            {
                                //Verificar si se ha seleccionado una partida especifica
                                if (Datos.P_Partida_Especifica_ID != null && Datos.P_Partida_Especifica_ID != "" && Datos.P_Partida_Especifica_ID != String.Empty)
                                {
                                    Mi_SQL += "Cat_Com_Productos.Partida_ID = '" + Datos.P_Partida_Especifica_ID.Trim() + "' ";
                                }
                                else
                                {
                                    Mi_SQL += "Cat_Com_Partidas_Especificas.Partida_Generica_ID = '" + Datos.P_Partida_Generica_ID.Trim() + "' ";
                                }
                            }
                        }
                        break;
                }

                //Verificar si es consulta con los resguardos
                if (Datos.P_Resguardos == true)
                {
                    //Ejecutar consulta de los productos
                    Mi_SQL = Mi_SQL + "WHERE Cat_Com_Productos.Disponible > 0 ";
                    Dt_Productos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                    //Clonar la tabla
                    Dt_Resultado = Dt_Productos.Clone();

                    //verificar si la consulta arrojo resultado
                    if (Dt_Productos.Rows.Count > 0)
                    {
                        //Ciclo para el barrido de la tabla
                        for (Cont_Elementos = 0; Cont_Elementos < Dt_Productos.Rows.Count; Cont_Elementos++)
                        {
                            //Asignar consulta para los resguardos
                            Mi_SQL = "SELECT (Cat_Empleados.Nombre +  ' ' + Cat_Empleados.Apellido_Paterno + ' ' + ISNULL(Cat_Empleados.Apellido_Materno, '')) AS Resguardante, " +
                                "Ope_Pat_Bienes_Resguardos.Bien_Resguardo_ID FROM Ope_Pat_Bienes_Resguardos " +
                                "INNER JOIN Cat_Empleados ON Ope_Pat_Bienes_Resguardos.Empleado_Resguardo_ID = Cat_Empleados.Empleado_ID " +
                                "WHERE Ope_Pat_Bienes_Resguardos.Bien_ID = '" + Dt_Productos.Rows[Cont_Elementos]["Producto_ID"].ToString().Trim() + "' ";

                            //Ejecutar consulta
                            Dt_Resguardos.Clear();
                            Dt_Resguardos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                            //verificar si la consulta arrojo resultados
                            if (Dt_Resguardos.Rows.Count > 0)
                            {
                                //Ciclo para el barrido de la tabla de los resguardos
                                for (Cont_Resguardos = 0; Cont_Resguardos < Dt_Resguardos.Rows.Count; Cont_Resguardos++)
                                {
                                    //Instanciar renglon
                                    Renglon = Dt_Resultado.NewRow();

                                    //Ciclo para el llenado de las columnas
                                    for (Cont_Columnas = 0; Cont_Columnas < Dt_Resultado.Columns.Count; Cont_Columnas++)
                                    {
                                        //Verificar la columna
                                        switch (Dt_Resultado.Columns[Cont_Columnas].ColumnName.ToUpper())
                                        {
                                            case "RESGUARDO":
                                                Renglon[Cont_Columnas] = Dt_Resguardos.Rows[Cont_Resguardos]["Bien_Resguardo_ID"];
                                                break;

                                            case "RESGUARDANTE":
                                                Renglon[Cont_Columnas] = Dt_Resguardos.Rows[Cont_Resguardos]["Resguardante"];
                                                break;

                                            default:
                                                //Resto de las columnas
                                                Renglon[Cont_Columnas] = Dt_Productos.Rows[Cont_Elementos][Cont_Columnas];
                                                break;
                                        }
                                    }

                                    //Colocar el renglon en la tabla
                                    Dt_Resultado.Rows.Add(Renglon);
                                }
                            }
                        }
                    }
                }
                else
                {
                    Mi_SQL = Mi_SQL + "WHERE Cat_Com_Productos.Disponible > 0 ";
                    //Ejecutar consulta
                    Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Impuestos
        ///DESCRIPCION:             Consultar los impuestos
        ///PARAMETROS:              Datos: Variable de la capa de negocios
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              28/Marzo/2012 11:59
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static Double Consulta_Impuestos(Cls_Ope_Alm_Productos_Chatarra_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            Double Impuesto = 0;  //Variable para el impuesto
            Object aux; //Variable auxiliar para las consultas

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT Porcentaje_Impuesto FROM Cat_Com_Impuestos WHERE Impuesto_ID = '" + Datos.P_Impuesto_ID + "' ";

                //Ejecutar consulta
                aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                //verificar si la consulta arrojo resultados
                if (aux != null)
                {
                    Impuesto = Convert.ToDouble(aux);
                }

                //Entregar resultado
                return Impuesto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Entrada_Chatarra
        ///DESCRIPCION:             Darle entrada a los productos chatarra
        ///PARAMETROS:              Datos: Variable de la capa de negocios
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              28/Marzo/2012 12:31
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static Int64 Entrada_Chatarra(Cls_Ope_Alm_Productos_Chatarra_Negocio Datos)
        {
            //Declaracion de variables 
            Int64 No_Salida = 0; //variable para el numero de la salida
            String Mensaje = "";
            String Mi_SQL = String.Empty;
            Object Aux;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
                        
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            
            Cmd.Transaction = Trans;
            
            SqlDataAdapter Da = new SqlDataAdapter(); // Adaptador para el llenado de los datatable
            DataTable Dt_Aux = new DataTable(); // Tabla auxiliar para las consultas
            int Existencia = 0; //Variable para actualizar la existencia del producto
            int Disponible = 0; //Variable para actualizar la disponibilidad del producto

            try
            {
                //verificar si es de resguardo
                if (Datos.P_Resguardos == false)
                {
                    //Consulta para el numero de la orden de salida
                    Mi_SQL = "SELECT MAX(No_Salida) FROM Alm_Com_Salidas ";
                    Cmd.CommandText = Mi_SQL;
                    Aux = Cmd.ExecuteScalar();

                    //Verificar si no es nulo
                    if (Aux != null)
                    {
                        //verificar si no es dbnull
                        if (Aux != DBNull.Value)
                        {
                            No_Salida = Convert.ToInt64(Aux) + 1;
                        }
                        else
                        {
                            No_Salida = 1;
                        }
                    }
                    else
                    {
                        No_Salida = 1;
                    }

                    //COnsulta para la salida
                    Mi_SQL = "INSERT INTO ALM_COM_SALIDAS (NO_SALIDA, EMPLEADO_SOLICITO_ID, USUARIO_CREO, FECHA_CREO, EMPLEADO_ALMACEN_ID, FECHA, SUBTOTAL, IVA, "
                        + "TOTAL, CONTABILIZADO, ESTATUS) VALUES(" + No_Salida.ToString().Trim() + ",'" + Cls_Sessiones.Empleado_ID + "','" + Cls_Sessiones.Nombre_Empleado + "',"
                        + "GETDATE(),'" + Cls_Sessiones.Empleado_ID + "',GETDATE()," + Datos.P_Subtotal.ToString().Trim() + "," + Datos.P_IVA.ToString().Trim() + ","
                        + Datos.P_Total.ToString().Trim() + ",'NO','GENERADA')";

                    //Ejecutar la consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Consulta para los detalles
                    Mi_SQL = "INSERT INTO ALM_COM_SALIDAS_DETALLES (NO_SALIDA, PRODUCTO_ID, CANTIDAD, COSTO, COSTO_PROMEDIO, IMPORTE, SUBTOTAL, IVA) " +
                        "VALUES(" + No_Salida.ToString().Trim() + ",'" + Datos.P_Producto_ID + "'," + Datos.P_Cantidad.ToString().Trim() + "," +
                        Datos.P_Costo.ToString().Trim() + "," + Datos.P_Costo_Promedio.ToString().Trim() + "," + Datos.P_Importe.ToString().Trim() + "," +
                        Datos.P_Subtotal.ToString().Trim() + "," + Datos.P_IVA.ToString().Trim() + ")";

                    //Ejecutar la consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Consulta para la cantidad de productos
                    Mi_SQL = "SELECT * FROM Cat_Com_Productos WHERE Producto_ID = '" + Datos.P_Producto_ID + "' ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Da.SelectCommand = Cmd;
                    Dt_Aux.Clear();
                    Da.Fill(Dt_Aux);

                    //Colocar la existencia y el disponible
                    if (Dt_Aux.Rows[0]["Existencia"] != DBNull.Value)
                    {
                        Existencia = Convert.ToInt32(Dt_Aux.Rows[0]["Existencia"]);
                    }

                    if (Dt_Aux.Rows[0]["Disponible"] != DBNull.Value)
                    {
                        Disponible = Convert.ToInt32(Dt_Aux.Rows[0]["Disponible"]);
                    }

                    //Calcular las nuevas existencias y disponibles
                    Existencia = Existencia - Datos.P_Cantidad;
                    Disponible = Disponible - Datos.P_Cantidad;

                    //Actualizar la base de datos de los productos
                    Mi_SQL = "UPDATE Cat_Com_Productos SET Existencia = " + Existencia.ToString().Trim() + ", " +
                        "Disponible = " + Disponible.ToString().Trim() + ", " +
                        "Usuario_Modifico = '" + Cls_Sessiones.Nombre_Empleado + "', Fecha_Modifico = GETDATE() " + 
                        "WHERE Producto_ID = '" + Datos.P_Producto_ID + "' ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Consulta para la salida de chatarra
                    Mi_SQL = "INSERT INTO Ope_Alm_Salidas_Chatarra (Producto_ID, No_Salida, Cantidad, Usuario_Creo, Fecha_Creo) " +
                        "VALUES('" + Datos.P_Producto_ID + "'," + No_Salida.ToString().Trim() + "," + Datos.P_Cantidad.ToString().Trim() + "," +
                        "'" + Cls_Sessiones.Nombre_Empleado + "',GETDATE())";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                else
                {
                    //Consulta para cancelar el resguardo
                    Mi_SQL = "UPDATE OPE_PAT_BIENES_RESGUARDOS SET USUARIO_MODIFICO = '" + Cls_Sessiones.Nombre_Empleado + "', FECHA_MODIFICO = GETDATE(), ESTATUS = 'CANCELADO' " +
                        "WHERE Bien_Resguardo_ID = " + Datos.P_No_Resguardo.ToString().Trim() + " ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                //Consulta para los productos chatarra
                Mi_SQL = "SELECT * FROM Ope_Alm_Productos_Chatarra WHERE Producto_ID = '" + Datos.P_Producto_ID + "' ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                Dt_Aux.Clear();
                Da.SelectCommand = Cmd;
                Da.Fill(Dt_Aux);

                //verificar si la consulta arrojo resultados
                if (Dt_Aux.Rows.Count > 0)
                {
                    //Colocar las cantidades
                    Existencia = Convert.ToInt32(Dt_Aux.Rows[0]["Cantidad"]);
                    Existencia = Existencia + Datos.P_Cantidad;

                    //Consulta para actualizar las cantidades de los productos
                    Mi_SQL = "UPDATE Ope_Alm_Productos_Chatarra SET Cantidad = " + Existencia.ToString().Trim() + " " +
                        "WHERE Producto_ID = '" + Datos.P_Producto_ID + "' ";

                    //Ejecutar la consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                else
                {
                    //Consulta para los datos de los productos
                    Mi_SQL = "SELECT * FROM Cat_Com_Productos WHERE Producto_ID = '" + Datos.P_Producto_ID + "' ";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Da.SelectCommand = Cmd;
                    Dt_Aux.Clear();
                    Da.Fill(Dt_Aux);

                    //Consulta para ingresar el producto chatarra
                    Mi_SQL = "INSERT INTO Ope_Alm_Productos_Chatarra (Producto_ID, Partida_ID, Unidad_ID, Impuesto_ID, Impuesto_2_ID, Clave, Ref_JAPAMI, Nombre, Descripcion, " +
                        "Costo, Costo_Promedio, Cantidad, Ubicacion, Tipo, Stock, Comentarios, Usuario_Creo, Fecha_Creo) VALUES('" + Datos.P_Producto_ID + "',";

                    //Verificar si hay partida
                    if (Dt_Aux.Rows[0]["Partida_ID"] == DBNull.Value)
                    {
                        Mi_SQL = Mi_SQL + "NULL,";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "'" + Dt_Aux.Rows[0]["Partida_ID"].ToString().Trim() + "',";
                    }

                    //verificar si hay unidad
                    if (Dt_Aux.Rows[0]["Unidad_ID"] == DBNull.Value)
                    {
                        Mi_SQL = Mi_SQL + "NULL,";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "'" + Dt_Aux.Rows[0]["Unidad_ID"].ToString().Trim() + "',";
                    }

                    //verificar si hay impuesto
                    if (Dt_Aux.Rows[0]["Impuesto_ID"] == DBNull.Value)
                    {
                        Mi_SQL = Mi_SQL + "NULL,";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "'" + Dt_Aux.Rows[0]["Impuesto_ID"].ToString().Trim() + "',";
                    }

                    //Verificar si hay otro impuesto
                    if (Dt_Aux.Rows[0]["Impuesto_2_ID"] == DBNull.Value)
                    {
                        Mi_SQL = Mi_SQL + "NULL,";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "'" + Dt_Aux.Rows[0]["Impuesto_2_ID"].ToString().Trim() + "',";
                    }

                    //Clave
                    if (Dt_Aux.Rows[0]["Clave"] == DBNull.Value)
                    {
                        Mi_SQL = Mi_SQL + "'',";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "'" + Dt_Aux.Rows[0]["Clave"].ToString().Trim() + "',";
                    }

                    //Ref japami
                    if (Dt_Aux.Rows[0]["Ref_JAPAMI"] == DBNull.Value)
                    {
                        Mi_SQL = Mi_SQL + "0,";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + Dt_Aux.Rows[0]["Ref_JAPAMI"].ToString().Trim() + ",";
                    }

                    //Nombre
                    if (Dt_Aux.Rows[0]["Nombre"] == DBNull.Value)
                    {
                        Mi_SQL = Mi_SQL + "'',";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "'" + Dt_Aux.Rows[0]["Nombre"].ToString().Trim() + "',";
                    }

                    //Descripcion
                    if (Dt_Aux.Rows[0]["Descripcion"] == DBNull.Value)
                    {
                        Mi_SQL = Mi_SQL + "'',";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "'" + Dt_Aux.Rows[0]["Descripcion"].ToString().Trim() + "',";
                    }

                    //Costo
                    if (Dt_Aux.Rows[0]["Costo"] == DBNull.Value)
                    {
                        Mi_SQL = Mi_SQL + "0,";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + Dt_Aux.Rows[0]["Costo"].ToString().Trim() + ",";
                    }

                    //Costo promedio
                    if (Dt_Aux.Rows[0]["Costo_Promedio"] == DBNull.Value)
                    {
                        Mi_SQL = Mi_SQL + "0,";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + Dt_Aux.Rows[0]["Costo_Promedio"].ToString().Trim() + ",";
                    }

                    //Cantidad
                    Mi_SQL = Mi_SQL +  Existencia.ToString().Trim() + ",";

                    //Ubicacion
                    if (Dt_Aux.Rows[0]["Ubicacion"] == DBNull.Value)
                    {
                        Mi_SQL = Mi_SQL + "'',";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "'" + Dt_Aux.Rows[0]["Ubicacion"].ToString().Trim() + "',";
                    }

                    //Tipo
                    if (Dt_Aux.Rows[0]["Tipo"] == DBNull.Value)
                    {
                        Mi_SQL = Mi_SQL + "'',";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "'" + Dt_Aux.Rows[0]["Tipo"].ToString().Trim() + "',";
                    }

                    //Stock
                    if (Dt_Aux.Rows[0]["Stock"] == DBNull.Value)
                    {
                        Mi_SQL = Mi_SQL + "'',";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "'" + Dt_Aux.Rows[0]["Stock"].ToString().Trim() + "',";
                    }

                    //Comentarios
                    if (Dt_Aux.Rows[0]["Comentarios"] == DBNull.Value)
                    {
                        Mi_SQL = Mi_SQL + "'',";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "'" + Dt_Aux.Rows[0]["Comentarios"].ToString().Trim() + "',";
                    }

                    Mi_SQL = Mi_SQL + "'" + Cls_Sessiones.Nombre_Empleado + "', GETDATE())";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();                         
                }
                
                //Ejecutar transaccion
                Trans.Commit();

                //Entregar el numero de la salida
                return No_Salida;
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar Modificar el Bien Mueble. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Productos_Simple
        ///DESCRIPCION:             Consultar los datos de un producto particular
        ///PARAMETROS:              Datos: Variable de la capa de negocios
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              28/Marzo/2012 13:47
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Productos_Simple(Cls_Ope_Alm_Productos_Chatarra_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para la consulta
            DataTable Dt_Productos = new DataTable(); //Tabla para el resultado de la consulta

            try
            {
                //Consulta para los productos
                Mi_SQL = "SELECT * FROM Cat_Com_Productos WHERE Producto_ID = '" + Datos.P_Producto_ID + "'"; 

                //Ejecutar consulta
                Dt_Productos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Productos;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Reporte_Productos_Chatarra
        ///DESCRIPCION:             Consultar los productos chatarra
        ///PARAMETROS:              Datos: Variable de la capa de negocios
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              28/Marzo/2012 19:00
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Reporte_Productos_Chatarra(Cls_Ope_Alm_Productos_Chatarra_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DataTable Dt_Productos_Chatarra = new DataTable(); //tabla para el resultado de la consulta
            Boolean Where_Utilizado = false; //Variable que indica si la clausula where ya ha sido utilizada

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT Ope_Alm_Productos_Chatarra.Producto_ID, Ope_Alm_Productos_Chatarra.Clave, Ope_Alm_Productos_Chatarra.Ref_JAPAMI, " +
                    "Ope_Alm_Productos_Chatarra.Nombre, (Cat_SAP_Partida_Generica.Clave + ' ' + Cat_SAP_Partida_Generica.Descripcion) AS Partida_Generica, " +
                    "(Cat_SAP_Partidas_Especificas.Clave + ' ' + Cat_SAP_Partidas_Especificas.Descripcion) AS Partida_Especifica, " +
                    "Ope_Alm_Productos_Chatarra.Descripcion, Cat_Com_Impuestos.Nombre AS Impuesto_1, Cat_Com_Impuestos_2.Nombre AS Impuesto_2, " +
                    "Ope_Alm_Productos_Chatarra.Costo, Ope_Alm_Productos_Chatarra.Costo_Promedio, Ope_Alm_Productos_Chatarra.Cantidad, " + 
                    "Ope_Alm_Productos_Chatarra.Ubicacion, Ope_Alm_Productos_Chatarra.Tipo, Ope_Alm_Productos_Chatarra.Stock, " +
                    "('PG: ' + Cat_SAP_Partida_Generica.Clave + ' ' + Cat_SAP_Partida_Generica.Descripcion + '<br />' + " +
                    "'PE: ' + Cat_SAP_Partidas_Especificas.Clave + ' ' + Cat_SAP_Partidas_Especificas.Descripcion) AS Partida, " +
                    "(Ope_Alm_Productos_Chatarra.Nombre + ' ' + Ope_Alm_Productos_Chatarra.Descripcion) AS Nombre_Completo, " +
                    "Cat_Com_Unidades.Nombre AS Unidad, No_Salida = 0, Fecha = '01/01/1900' FROM Ope_Alm_Productos_Chatarra " +
                    "LEFT JOIN Cat_Com_Impuestos ON Ope_Alm_Productos_Chatarra.Impuesto_ID = Cat_Com_Impuestos.Impuesto_ID " +
                    "LEFT JOIN Cat_Com_Impuestos Cat_Com_Impuestos_2 ON Ope_Alm_Productos_Chatarra.Impuesto_2_ID = Cat_Com_Impuestos_2.Impuesto_ID " +
                    "LEFT JOIN Cat_SAP_Partidas_Especificas ON Ope_Alm_Productos_Chatarra.Partida_ID = Cat_SAP_Partidas_Especificas.Partida_ID " +
                    "LEFT JOIN Cat_SAP_Partida_Generica ON Cat_SAP_Partidas_Especificas.Partida_Generica_ID = Cat_SAP_Partida_Generica.Partida_Generica_ID " +
                    "LEFT JOIN Cat_Com_Unidades ON Ope_Alm_Productos_Chatarra.Unidad_ID = Cat_Com_Unidades.Unidad_ID ";

                //Verificar el tipo de la consulta
                switch (Datos.P_Tipo_Consulta)
                {
                    case "Producto_ID":
                        Mi_SQL += "WHERE Ope_Alm_Productos_Chatarra.Producto_ID = '" + Datos.P_Producto_ID.Trim() + "' ";
                        break;

                    case "Clave":
                        Mi_SQL += "WHERE Ope_Alm_Productos_Chatarra.Clave = '" + Datos.P_Clave.Trim() + "' ";
                        break;

                    case "Ref_JAPAMI":
                        Mi_SQL += "WHERE Ope_Alm_Productos_Chatarra.Ref_JAPAMI = " + Datos.P_Ref_JAPAMI.ToString().Trim() + " ";
                        break;

                    case "Nombre":
                        Mi_SQL += "WHERE Ope_Alm_Productos_Chatarra.Nombre LIKE '%" + Datos.P_Nombre.Trim() + "%'";
                        break;

                    default:
                        //Verificar si se ha seleccionado una unidad
                        if (Datos.P_Unidad_ID != null && Datos.P_Unidad_ID != "" && Datos.P_Unidad_ID != String.Empty)
                        {
                            //Verificar si es la opcion nula
                            if (Datos.P_Unidad_ID == "NULO")
                            {
                                Mi_SQL += "WHERE Ope_Alm_Productos_Chatarra.Unidad_ID IS NULL ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE Ope_Alm_Productos_Chatarra.Unidad_ID = '" + Datos.P_Unidad_ID.Trim() + "' ";
                            }

                            Where_Utilizado = true;
                        }

                        //Verificar si se ha seleccionado un impuesto
                        if (Datos.P_Impuesto_ID != null && Datos.P_Impuesto_ID != "" && Datos.P_Impuesto_ID != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Resto del filtro
                            //Verificar si es la opcion nula
                            if (Datos.P_Impuesto_ID == "NULO")
                            {
                                Mi_SQL += "(Ope_Alm_Productos_Chatarra.Impuesto_ID IS NULL AND Ope_Alm_Productos_Chatarra.Impuesto_2_ID IS NULL ";
                            }
                            else
                            {
                                Mi_SQL += "(Ope_Alm_Productos_Chatarra.Impuesto_ID = '" + Datos.P_Impuesto_ID + "' " +
                                    "OR Ope_Alm_Productos_Chatarra.Impuesto_2_ID = '" + Datos.P_Impuesto_ID + "') ";
                            }
                        }

                        //Verificar si se ha seleccionado un tipo
                        if (Datos.P_Tipo != null && Datos.P_Tipo != "" && Datos.P_Tipo != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Resto del filtro
                            //Verificar si es la opcion nula
                            if (Datos.P_Tipo == "NULO")
                            {
                                Mi_SQL += "Ope_Alm_Productos_Chatarra.Tipo IS NULL ";
                            }
                            else
                            {
                                Mi_SQL += "Ope_Alm_Productos_Chatarra.Tipo = '" + Datos.P_Tipo.Trim() + "' ";
                            }
                        }

                        //Verificar si se ha seleccionado el stock
                        if (Datos.P_Stock != null && Datos.P_Stock != "" && Datos.P_Stock != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Resto del filtro
                            Mi_SQL = "Ope_Alm_Productos_Chatarra.Stock = '" + Datos.P_Stock.Trim() + "' ";
                        }

                        //Verificar si se ha seleccionado una partida generica
                        if (Datos.P_Partida_Generica_ID != null && Datos.P_Partida_Generica_ID != "" && Datos.P_Partida_Generica_ID != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Verificar si es la opcion nula
                            if (Datos.P_Partida_Generica_ID == "NULO")
                            {
                                Mi_SQL += "Ope_Alm_Productos_Chatarra.Partida_ID IS NULL ";
                            }
                            else
                            {
                                //Verificar si se ha seleccionado una partida especifica
                                if (Datos.P_Partida_Especifica_ID != null && Datos.P_Partida_Especifica_ID != "" && Datos.P_Partida_Especifica_ID != String.Empty)
                                {
                                    Mi_SQL += "Ope_Alm_Productos_Chatarra.Partida_ID = '" + Datos.P_Partida_Especifica_ID.Trim() + "' ";
                                }
                                else
                                {
                                    Mi_SQL += "Ope_Alm_Productos_Chatarra.Partida_Generica_ID = '" + Datos.P_Partida_Generica_ID.Trim() + "' ";
                                }
                            }
                        }
                        break;
                }

                //Ejecutar consulta
                Dt_Productos_Chatarra = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Productos_Chatarra;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Reporte_Productos_Chatarra_Detalles
        ///DESCRIPCION:             Consultar los productos chatarra a detalle con las salidas
        ///PARAMETROS:              Datos: Variable de la capa de negocios
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              28/Marzo/2012 16:24
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Reporte_Productos_Chatarra_Detalles(Cls_Ope_Alm_Productos_Chatarra_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado de la consulta
            Boolean Where_Utilizado = false; //variable que indica si la clausula where ya ha sido utilizada

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT Ope_Alm_Productos_Chatarra.Producto_ID, Ope_Alm_Productos_Chatarra.Clave, Ope_Alm_Productos_Chatarra.Ref_JAPAMI, " +
                    "Ope_Alm_Productos_Chatarra.Nombre, (Cat_SAP_Partida_Generica.Clave + ' ' + Cat_SAP_Partida_Generica.Descripcion) AS Partida_Generica, " +
                    "(Cat_SAP_Partidas_Especificas.Clave + ' ' + Cat_SAP_Partidas_Especificas.Descripcion) AS Partida_Especifica, " +
                    "Ope_Alm_Productos_Chatarra.Descripcion, Cat_Com_Impuestos.Nombre AS Impuesto_1, Cat_Com_Impuestos_2.Nombre AS Impuesto_2, " +
                    "Ope_Alm_Productos_Chatarra.Costo, Ope_Alm_Productos_Chatarra.Costo_Promedio, Ope_Alm_Salidas_Chatarra.Cantidad, " +
                    "Ope_Alm_Productos_Chatarra.Ubicacion, Ope_Alm_Productos_Chatarra.Tipo, Ope_Alm_Productos_Chatarra.Stock, " +
                    "('PG: ' + Cat_SAP_Partida_Generica.Clave + ' ' + Cat_SAP_Partida_Generica.Descripcion + '<br />' + " +
                    "'PE: ' + Cat_SAP_Partidas_Especificas.Clave + ' ' + Cat_SAP_Partidas_Especificas.Descripcion) AS Partida, " +
                    "(Ope_Alm_Productos_Chatarra.Nombre + ' ' + Ope_Alm_Productos_Chatarra.Descripcion) AS Nombre_Completo, Cat_Com_Unidades.Nombre AS Unidad, " +
                    "Ope_Alm_Salidas_Chatarra.No_Salida, Alm_Com_Salidas.Fecha " +
                    "FROM Ope_Alm_Productos_Chatarra " +
                    "INNER JOIN Ope_Alm_Salidas_Chatarra ON Ope_Alm_Productos_Chatarra.Producto_ID = Ope_Alm_Salidas_Chatarra.Producto_ID " +
                    "INNER JOIN Alm_Com_Salidas ON Ope_Alm_Salidas_Chatarra.No_Salida = Alm_Com_Salidas.No_Salida " +
                    "LEFT JOIN Cat_Com_Impuestos ON Ope_Alm_Productos_Chatarra.Impuesto_ID = Cat_Com_Impuestos.Impuesto_ID " +
                    "LEFT JOIN Cat_Com_Impuestos Cat_Com_Impuestos_2 ON Ope_Alm_Productos_Chatarra.Impuesto_2_ID = Cat_Com_Impuestos_2.Impuesto_ID " +
                    "LEFT JOIN Cat_SAP_Partidas_Especificas ON Ope_Alm_Productos_Chatarra.Partida_ID = Cat_SAP_Partidas_Especificas.Partida_ID " +
                    "LEFT JOIN Cat_SAP_Partida_Generica ON Cat_SAP_Partidas_Especificas.Partida_Generica_ID = Cat_SAP_Partida_Generica.Partida_Generica_ID " +
                    "LEFT JOIN Cat_Com_Unidades ON Ope_Alm_Productos_Chatarra.Unidad_ID = Cat_Com_Unidades.Unidad_ID ";

                //Verificar el tipo de la consulta
                switch (Datos.P_Tipo_Consulta)
                {
                    case "Producto_ID":
                        Mi_SQL += "WHERE Ope_Alm_Productos_Chatarra.Producto_ID = '" + Datos.P_Producto_ID.Trim() + "' ";
                        break;

                    case "Clave":
                        Mi_SQL += "WHERE Ope_Alm_Productos_Chatarra.Clave = '" + Datos.P_Clave.Trim() + "' ";
                        break;

                    case "Ref_JAPAMI":
                        Mi_SQL += "WHERE Ope_Alm_Productos_Chatarra.Ref_JAPAMI = " + Datos.P_Ref_JAPAMI.ToString().Trim() + " ";
                        break;

                    case "Nombre":
                        Mi_SQL += "WHERE Ope_Alm_Productos_Chatarra.Nombre LIKE '%" + Datos.P_Nombre.Trim() + "%'";
                        break;

                    default:
                        //Verificar si se ha seleccionado una unidad
                        if (Datos.P_Unidad_ID != null && Datos.P_Unidad_ID != "" && Datos.P_Unidad_ID != String.Empty)
                        {
                            //Verificar si es la opcion nula
                            if (Datos.P_Unidad_ID == "NULO")
                            {
                                Mi_SQL += "WHERE Ope_Alm_Productos_Chatarra.Unidad_ID IS NULL ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE Ope_Alm_Productos_Chatarra.Unidad_ID = '" + Datos.P_Unidad_ID.Trim() + "' ";
                            }

                            Where_Utilizado = true;
                        }

                        //Verificar si se ha seleccionado un impuesto
                        if (Datos.P_Impuesto_ID != null && Datos.P_Impuesto_ID != "" && Datos.P_Impuesto_ID != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Resto del filtro
                            //Verificar si es la opcion nula
                            if (Datos.P_Impuesto_ID == "NULO")
                            {
                                Mi_SQL += "(Ope_Alm_Productos_Chatarra.Impuesto_ID IS NULL AND Ope_Alm_Productos_Chatarra.Impuesto_2_ID IS NULL ";
                            }
                            else
                            {
                                Mi_SQL += "(Ope_Alm_Productos_Chatarra.Impuesto_ID = '" + Datos.P_Impuesto_ID + "' " +
                                    "OR Ope_Alm_Productos_Chatarra.Impuesto_2_ID = '" + Datos.P_Impuesto_ID + "') ";
                            }
                        }

                        //Verificar si se ha seleccionado un tipo
                        if (Datos.P_Tipo != null && Datos.P_Tipo != "" && Datos.P_Tipo != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Resto del filtro
                            //Verificar si es la opcion nula
                            if (Datos.P_Tipo == "NULO")
                            {
                                Mi_SQL += "Ope_Alm_Productos_Chatarra.Tipo IS NULL ";
                            }
                            else
                            {
                                Mi_SQL += "Ope_Alm_Productos_Chatarra.Tipo = '" + Datos.P_Tipo.Trim() + "' ";
                            }
                        }

                        //Verificar si se ha seleccionado el stock
                        if (Datos.P_Stock != null && Datos.P_Stock != "" && Datos.P_Stock != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Resto del filtro
                            Mi_SQL = "Ope_Alm_Productos_Chatarra.Stock = '" + Datos.P_Stock.Trim() + "' ";
                        }

                        //Verificar si se ha seleccionado una partida generica
                        if (Datos.P_Partida_Generica_ID != null && Datos.P_Partida_Generica_ID != "" && Datos.P_Partida_Generica_ID != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Verificar si es la opcion nula
                            if (Datos.P_Partida_Generica_ID == "NULO")
                            {
                                Mi_SQL += "Ope_Alm_Productos_Chatarra.Partida_ID IS NULL ";
                            }
                            else
                            {
                                //Verificar si se ha seleccionado una partida especifica
                                if (Datos.P_Partida_Especifica_ID != null && Datos.P_Partida_Especifica_ID != "" && Datos.P_Partida_Especifica_ID != String.Empty)
                                {
                                    Mi_SQL += "Ope_Alm_Productos_Chatarra.Partida_ID = '" + Datos.P_Partida_Especifica_ID.Trim() + "' ";
                                }
                                else
                                {
                                    Mi_SQL += "Ope_Alm_Productos_Chatarra.Partida_Generica_ID = '" + Datos.P_Partida_Generica_ID.Trim() + "' ";
                                }
                            }
                        }

                        //Verificar si se ha seleccionado fecha
                        if (Datos.P_Fecha_Inicio != null)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            Mi_SQL += "Alm_Com_Salidas.Fecha >= '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Inicio) + " 00:00:00' ";

                            //Verificar si la fecha fin esta seleccionada
                            if (Datos.P_Fecha_Fin != null)
                            {
                                Mi_SQL += " AND Alm_Com_Salidas.Fecha <= '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Fin) + " 23:59:59' ";
                            }
                        }
                        break;
                }

                //Ejecutar consulta 
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
    }
}