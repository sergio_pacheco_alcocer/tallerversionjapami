﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Ope_Alm_Autorizacion_Compras.Negocio;
using JAPAMI.Sessiones;


/// <summary>
/// Summary description for Cls_Ope_Alm_Autorizacion_Compras_Datos
/// </summary>
namespace JAPAMI.Ope_Alm_Autorizacion_Compras.Datos
{
    public class Cls_Ope_Alm_Autorizacion_Compras_Datos
    {
        public Cls_Ope_Alm_Autorizacion_Compras_Datos()
        {
        }        
        
        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Contra_Recibos
        ///DESCRIPCIÓN: Consulta los contrarecibos registrados
        ///PARAMETROS:  1.- Cls_Ope_Alm_Autorizacion_Compras_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  28/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Contra_Recibos(Cls_Ope_Alm_Autorizacion_Compras_Negocio Clase_Seguimiento_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                //Creamos la consulta
                Mi_SQL = "SELECT CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + ", CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Pago;
                Mi_SQL += ", CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion;
                Mi_SQL += ", ORDENES." + Ope_Com_Ordenes_Compra.Campo_Nombre_Proveedor + ", ORDENES." + Ope_Com_Ordenes_Compra.Campo_Total;
                Mi_SQL += " FROM " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " CONTRARECIBOS, " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " ORDENES";
                Mi_SQL += ", " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICIONES";
                Mi_SQL += " WHERE CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = ";
                Mi_SQL += "ORDENES." + Ope_Com_Ordenes_Compra.Campo_No_Factura_Interno + " AND ORDENES." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = ";
                Mi_SQL += "REQUISICIONES." + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " AND CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Estatus + " = 'GENERADO'";
                Mi_SQL += " AND REQUISICIONES." + Ope_Com_Requisiciones.Campo_Tipo_Articulo + " = 'PRODUCTO'";
                //Validamos los filtros por No de Contrarecibo
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_No_Contra_Recibo))
                {
                    Mi_SQL += " AND CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = "  + Clase_Seguimiento_Negocio.P_No_Contra_Recibo;
                }         
                //Validamos los filtros por fechas
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Fecha_Inicio) && !String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Fecha_Fin))
                {
                    Clase_Seguimiento_Negocio.P_Fecha_Inicio = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Clase_Seguimiento_Negocio.P_Fecha_Inicio));
                    Clase_Seguimiento_Negocio.P_Fecha_Fin= string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Clase_Seguimiento_Negocio.P_Fecha_Fin));

                    Mi_SQL += " AND ((CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Pago + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Inicio + " 00:00:00' AND ";
                    Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Fin + " 23:59:00') ";
                    Mi_SQL += " OR (CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Creo + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Inicio + " 00:00:00' AND ";
                    Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Fin + " 23:59:00'))";                    
                }
                //regresamos los valores
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los contrarecibos. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Autorizar_Contra_Recibo
        ///DESCRIPCIÓN          : Autoriza el contrarecibo para que pase al siguiente filtro
        ///PARAMETROS           :  
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 28/Enero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Autorizar_Contra_Recibo(Cls_Ope_Alm_Autorizacion_Compras_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Consultamos los datos del contrarecibo
                String Mi_SQL = "SELECT * FROM " + Ope_Alm_Seguimiento_Contrarecibo.Tabla_Ope_Alm_Seguimiento_Contrarecibo;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Alm_Seguimiento_Contrarecibo.Campo_No_Contra_Recibo + " = " + Clase_Negocio.P_No_Contra_Recibo;
                    Mi_SQL = Mi_SQL + " AND " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Estatus_Salida + " = 'GENERADO'";
                DataTable Dt_Contra_Recibo = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Insertar un registro en el seguimiento del contrarecibo
                Mi_SQL = "INSERT INTO " + Ope_Alm_Seguimiento_Contrarecibo.Tabla_Ope_Alm_Seguimiento_Contrarecibo +
                    " (" + Ope_Alm_Seguimiento_Contrarecibo.Campo_No_Contra_Recibo +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Empleado_Recibido_ID +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Estatus_Entrada +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Fecha_Entrada +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Estatus_Salida +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Fecha_Salida +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Usuario_Creo +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Fecha_Creo +
                    ") VALUES (" + Clase_Negocio.P_No_Contra_Recibo + ",'" +
                     Cls_Sessiones.Empleado_ID + "','" +
                     Clase_Negocio.P_Estatus_Salida.Trim() + "','" +
                     DateTime.Today.ToString("dd/MM/yyyy") + "'," +
                     "'AUTORIZADO_COMPRAS',GETDATE(),'" +
                     Cls_Sessiones.Nombre_Empleado + "'," +
                     "GETDATE() )";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Actualizar estatus en la clase de cheques 
                Mi_SQL = "UPDATE " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos;
                Mi_SQL = Mi_SQL + " SET " + Ope_Alm_Contrarecibos.Campo_Estatus + " = 'AUTORIZADO_COMPRAS'";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = " + Clase_Negocio.P_No_Contra_Recibo;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
                Mensaje_Error = Clase_Negocio.P_No_Contra_Recibo;
                    
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo autorizar el contrarecibo";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }//fin de Autorizar Cheque
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Rechazar_Contra_Recibo
        ///DESCRIPCIÓN          : Rechaza el contrarecibo
        ///PARAMETROS           :  
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 28/Enero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Rechazar_Contra_Recibo(Cls_Ope_Alm_Autorizacion_Compras_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Consultamos los datos del contrarecibo
                String Mi_SQL = "SELECT * FROM " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = " + Clase_Negocio.P_No_Contra_Recibo;
                DataTable Dt_Contra_Recibo = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Insertar un registro en el seguimiento del contrarecibo
                Mi_SQL = "INSERT INTO " + Ope_Alm_Seguimiento_Contrarecibo.Tabla_Ope_Alm_Seguimiento_Contrarecibo +
                    " (" + Ope_Alm_Seguimiento_Contrarecibo.Campo_No_Contra_Recibo +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Empleado_Recibido_ID +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Estatus_Entrada +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Fecha_Entrada +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Estatus_Salida +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Fecha_Salida +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Usuario_Creo +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Fecha_Creo +
                    ", " + Ope_Alm_Seguimiento_Contrarecibo.Campo_Comentarios +
                    ") VALUES (" + Clase_Negocio.P_No_Contra_Recibo + ",'" +
                     Cls_Sessiones.Empleado_ID + "','" +
                     Dt_Contra_Recibo.Rows[0][Ope_Alm_Contrarecibos.Campo_Estatus].ToString().Trim() + "','" +
                     DateTime.Parse(Dt_Contra_Recibo.Rows[0][Ope_Alm_Contrarecibos.Campo_Fecha_Pago].ToString()).ToString("dd/MM/yyyy") + "'," +
                     "'RECHAZADO_COMPRAS',GETDATE(),'" +
                     Cls_Sessiones.Nombre_Empleado + "'," +
                     "GETDATE(),'" + Clase_Negocio.P_Comentarios + "')";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Actualizar estatus en la clase de CONTRARECIBOS
                Mi_SQL = "UPDATE " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos;
                Mi_SQL = Mi_SQL + " SET " + Ope_Alm_Contrarecibos.Campo_Estatus + " = 'RECHAZADO_COMPRAS'";
                Mi_SQL = Mi_SQL + ", " + Ope_Alm_Contrarecibos.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Alm_Contrarecibos.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + " = " + Clase_Negocio.P_No_Contra_Recibo;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
               
                Trans.Commit();
                Mensaje_Error = "Rechazado";
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo rechazar el contrarecibo";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }//fin de Rechazar_Cheque
    }
}