﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using System.Data.SqlClient;
using JAPAMI.Sessiones;
using JAPAMI.Bitacora_Eventos;
using System.Collections;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Almacen_Registrar_Factura.Negocio;
using JAPAMI.Generar_Requisicion.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Alm_Registrar_Factura_Datos
/// </summary>
/// 
namespace JAPAMI.Almacen_Registrar_Factura.Datos
{
    public class Cls_Ope_Alm_Registrar_Factura_Datos
    {

        #region (Variables Locales)

        #endregion

        #region (Variables Publicas)

        #endregion

        #region (Metodos)

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Ordenes_Compra
        /// DESCRIPCION:            Método utilizado para consultar las ordenes de compra que se encuentren en estatus "SURTIDA"
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene la información para realizar la consulta
        ///                         
        /// CREO       :            David Herrera Rincon
        /// FECHA_CREO :            22/Enero/2013  
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Ordenes_Compra(Cls_Ope_Alm_Registrar_Factura_Negocio Datos)
        {
            String Mi_SQL = String.Empty; //Variable para las consultas

            try
            {
                // Asignar consulta
                Mi_SQL = "SELECT " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + ", "; 
                Mi_SQL += Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR, "; 
                Mi_SQL += Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ";
                Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo + " AS FECHA, ";
                Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Folio + ", "; 
                Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Estatus + ", ";
                Mi_SQL += "(select REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio + " FROM ";
                Mi_SQL += Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICIONES ";
                Mi_SQL += " WHERE REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = ";
                Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Lista_Requisiciones + ") AS NO_REQUISICION, ";

                Mi_SQL += "(select REQUISICIONES." + Ope_Com_Requisiciones.Campo_Listado_Almacen+ " FROM ";
                Mi_SQL += Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICIONES ";
                Mi_SQL += " WHERE REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = ";
                Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Lista_Requisiciones + ") LISTADO_ALMACEN, ";

                Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Total + ", ";
                Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Tipo_Articulo + "";
                Mi_SQL += " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + ", " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ";
                Mi_SQL += "WHERE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID;
                Mi_SQL += " = " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + " ";
                Mi_SQL += "AND " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Estatus + " IN('SURTIDA','SURTIDA_PARCIAL')";
                Mi_SQL += " AND " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Factura_Interno + " is null "; // Cuando no tiene asignado un numero de contra recibo,
                //Validamos los filtros
                if (Datos.P_No_Orden_Compra != null)
                {
                    Mi_SQL += "AND " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " like '%" + Datos.P_No_Orden_Compra + "%'";
                }

                if (Datos.P_No_Requisicion!= null)
                {
                    Mi_SQL += "AND " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Lista_Requisiciones + " like '%" + Datos.P_No_Requisicion+ "%'";
                }

                if (Datos.P_Proveedor_ID != null)
                {
                    Mi_SQL += "AND " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "'";
                }

                if ((Datos.P_Fecha_Inicio_B != null) && (Datos.P_Fecha_Fin_B != null))
                {
                    Mi_SQL += " AND " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo + " BETWEEN '" + Datos.P_Fecha_Inicio_B + "'" +
                  " AND '" + Datos.P_Fecha_Fin_B + "'";
                }

                Mi_SQL += " order by " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra; 

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Documentos_Soporte
        /// DESCRIPCION:            Método utilizado para consultar los Documentos de Soporte de la tabla "CAT_COM_DOCUMENTOS"
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos para realizar la consulta
        ///                         
        /// CREO       :            David Herrera Rincon
        /// FECHA_CREO :            22/Enero/2013  
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Documentos_Soporte(Cls_Ope_Alm_Registrar_Factura_Negocio Datos)
        {
            //Declaracion de variables
            DataTable Dt_Doc_Soporte = new DataTable();
            String Mi_SQL = null;
            DataSet Ds_Documentos_S = null;
            //Realizamos la consulta
            Mi_SQL = " SELECT " + "DOCUMENTOS_S." + Cat_Com_Documentos.Campo_Documento_ID+ "";
            Mi_SQL += ", DOCUMENTOS_S." + Cat_Com_Documentos.Campo_Nombre+ "";
            Mi_SQL += ", DOCUMENTOS_S." + Cat_Com_Documentos.Campo_Comentarios + " as DESCRIPCION";
            Mi_SQL += " FROM " + Cat_Com_Documentos.Tabla_Cat_Com_Documentos + " DOCUMENTOS_S";
            Mi_SQL += " WHERE  DOCUMENTOS_S." + Cat_Com_Documentos.Campo_Tipo + " = '" + "SOPORTE'";
            //Validamos los filtros
            if ((Datos.P_Documento_ID != null))
            {
                Mi_SQL += " AND  DOCUMENTOS_S." + Cat_Com_Documentos.Campo_Documento_ID + "= '" + Datos.P_Documento_ID +"'";
            }

            Mi_SQL += " ORDER BY " + Cat_Com_Documentos.Campo_Nombre;
            //Hacemos la consulta y entregamos el valor
            Ds_Documentos_S = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            Dt_Doc_Soporte = Ds_Documentos_S.Tables[0];
           
            return Dt_Doc_Soporte;
        }


        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Proveedores
        /// DESCRIPCION:            Consultar los datos de los proveedores
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene 
        ///                         los datos para la busqueda
        /// CREO       :            David Herrera Rincon
        /// FECHA_CREO :            22/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Proveedores(Cls_Ope_Alm_Registrar_Factura_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable apra las consultas

            try
            {
                //Consulta
                Mi_SQL = " SELECT DISTINCT " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ";
                Mi_SQL += Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Compañia + " ";
                Mi_SQL += " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + ", " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra;
                Mi_SQL += " WHERE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID;
                Mi_SQL += " = " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + " ";
                Mi_SQL += " and " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Estatus + " = 'SURTIDA' ";
                Mi_SQL += "ORDER BY " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Compañia; // Ordenamiento

                // Resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///******************************************************************************* // Este se ocupa en la clase "ContraREcibo_Datos"
        /// NOMBRE DE LA CLASE:     Guardar_Registro_Factura
        /// DESCRIPCION:            Se guardaa la información que forma parte de la factura
        /// PARAMETROS :                                 
        /// CREO       :            David Herrera Rincón  
        /// FECHA_CREO :            22/Enero/2013 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static void Guardar_Registro_Factura(Cls_Ope_Alm_Registrar_Factura_Negocio Datos)
        {
            String Mi_SQL = String.Empty;
            Object Aux = new Object();
            String Estatus = "SURTIDA";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                foreach (DataRow Dr_Producto in Datos.P_Dt_Productos_OC.Rows)
                {
                    Mi_SQL = "INSERT INTO " + Ope_Com_Ordenes_Compra_Productos.Tabla_Ope_Com_Ordenes_Compra_Productos + " (";
                    Mi_SQL += Ope_Com_Ordenes_Compra_Productos.Campo_No_Orden_Compra + ", ";
                    Mi_SQL += Ope_Com_Ordenes_Compra_Productos.Campo_Producto_Id + ", ";
                    Mi_SQL += Ope_Com_Ordenes_Compra_Productos.Campo_Resguardo + ", ";
                    Mi_SQL += Ope_Com_Ordenes_Compra_Productos.Campo_Custodia + ", ";
                    Mi_SQL += Ope_Com_Ordenes_Compra_Productos.Campo_Recibo + ", ";
                    Mi_SQL += Ope_Com_Ordenes_Compra_Productos.Campo_Unidad + ", ";
                    Mi_SQL += Ope_Com_Ordenes_Compra_Productos.Campo_Totalidad + ", ";
                    Mi_SQL += Ope_Com_Ordenes_Compra_Productos.Campo_Usuario_Creo + ", ";
                    Mi_SQL += Ope_Com_Ordenes_Compra_Productos.Campo_Fecha_Creo + ") ";
                    Mi_SQL += "VALUES( ";
                    Mi_SQL += Datos.P_No_Orden_Compra + ", '";
                    Mi_SQL += Dr_Producto["PRODUCTO_ID"].ToString().Trim() + "', '";
                    Mi_SQL += Dr_Producto["RESGUARDO"].ToString().Trim() + "', '";
                    Mi_SQL += Dr_Producto["CUSTODIA"].ToString().Trim() + "', '";
                    Mi_SQL += Dr_Producto["RECIBO"].ToString().Trim() + "', '";
                    Mi_SQL += Dr_Producto["UNIDAD"].ToString().Trim() + "', '";
                    Mi_SQL += Dr_Producto["TOTALIDAD"].ToString().Trim() + "', '";
                    Mi_SQL += Datos.P_Usuario_Creo + "', ";
                    Mi_SQL += " GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Mi_SQL = " UPDATE " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                    Mi_SQL += " SET " + Ope_Com_Req_Producto.Campo_Resguardado + " ='";
                    Mi_SQL += (Dr_Producto["RESGUARDO"].ToString().Trim() == "SI" || Dr_Producto["CUSTODIA"].ToString().Trim() == "SI") ? "SI" : "NO";
                    Mi_SQL += "' WHERE " + Ope_Com_Req_Producto.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.Trim();
                    Mi_SQL += " and " + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = '" + Dr_Producto["PRODUCTO_ID"].ToString().Trim() + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    if (!String.IsNullOrEmpty(Dr_Producto["RESGUARDO"].ToString()) ||
                        !String.IsNullOrEmpty(Dr_Producto["CUSTODIA"].ToString()) &&
                        Estatus == "SURTIDA")
                    {
                        Estatus = "SURTIDA_FACTURA";
                    }
                }

                Mi_SQL = "SELECT " + Ope_Com_Ordenes_Compra.Campo_Estatus + " FROM ";
                Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra;
                Mi_SQL += " WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.Trim();
                Cmd.CommandText = Mi_SQL;
                Aux = Cmd.ExecuteScalar();

                if (Aux.ToString().Trim() == "SURTIDA_REGISTRO")
                {
                    Mi_SQL = "UPDATE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " SET ";
                    Mi_SQL += Ope_Com_Ordenes_Compra.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "', ";
                    Mi_SQL += Ope_Com_Ordenes_Compra.Campo_Fecha_Modifico + " = GETDATE(), ";
                    Mi_SQL += Ope_Com_Ordenes_Compra.Campo_Estatus + " = '" + Estatus + "' ";
                    Mi_SQL += " WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.Trim();
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                    Mi_SQL += " SET " + Ope_Com_Requisiciones.Campo_Estatus + "='" + Estatus + "'";
                    Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Usuario_Modifico + "= '" + Cls_Sessiones.Nombre_Empleado + "'";
                    Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Fecha_Modifico + "= GETDATE()";
                    Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " =" + Datos.P_No_Orden_Compra.Trim();
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                Trans.Commit();
            }
            catch (Exception Ex)
            {
                Trans.Rollback();
                throw new Exception(Ex.Message);
            }
            finally
            {
                Cn.Close();
            }
        }
        
        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Montos_Orden_Compra
        /// DESCRIPCION:            Se obtienen los montos de la orden de compra seleccionada por el usuario
        /// PARAMETROS :                                 
        /// CREO       :            David Herrera Rincon  
        /// FECHA_CREO :            22/Enero/2013
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Montos_Orden_Compra(Cls_Ope_Alm_Registrar_Factura_Negocio Datos)
        {
            String Mi_SQL = String.Empty; //Variable para las consultas

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + ", " + Ope_Com_Ordenes_Compra.Campo_Subtotal + ", ";
                Mi_SQL += Ope_Com_Ordenes_Compra.Campo_Total_IEPS + ", " + Ope_Com_Ordenes_Compra.Campo_Total_IVA + ", " + Ope_Com_Ordenes_Compra.Campo_Total + " ";
                Mi_SQL += "FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " ";
                Mi_SQL += "WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra;

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        } 

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Montos_Orden_Compra
        /// DESCRIPCION:            Consulta los montos de la orden de compra seleccionada por el usuario
        /// PARAMETROS :            
        /// CREO       :            David Herrera Rincon
        /// FECHA_CREO :            23/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Montos_Orden_Compra(Cls_Ope_Alm_Registrar_Factura_Negocio Datos)
        {
            String Mi_SQL = "";
            
            try
            {
                // Consulta
                Mi_SQL = "SELECT " + " ORDEN_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Subtotal + " ";
                Mi_SQL += ", ORDEN_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Total_IVA + "";
                Mi_SQL += ", ORDEN_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Total + " ";
                Mi_SQL += " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " ORDEN_COMPRA";
                Mi_SQL += " WHERE  ORDEN_COMPRA." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = ";
                Mi_SQL += Datos.P_No_Orden_Compra + "";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0]; //  Entregar resultado
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }


        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Productos_Orden_Compra
        /// DESCRIPCION:            Consulta los productos de la orden de compra seleccionada por el usuarioa
        /// PARAMETROS :            
        /// CREO       :            David Herrera Rincon
        /// FECHA_CREO :            23/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Productos_Orden_Compra(Cls_Ope_Alm_Registrar_Factura_Negocio Datos)
        {
            String Mi_SQL = String.Empty; // Variable para las consultas

            try
            {
                // Consulta
                Mi_SQL = "SELECT  REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " as PRODUCTO_ID";
                Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Cantidad + "";
                Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado + " as PRECIO_U ";
                Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Subtota_Cotizado + " as PRECIO_AC ";
                Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + "";
                Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Dependencia_ID + "";
                Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Partida_ID + "";
                Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID + "";

                Mi_SQL += ",(select PRODUCTOS." + Cat_Com_Productos.Campo_Clave + " from ";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS ";
                Mi_SQL += " where REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = PRODUCTOS.";
                Mi_SQL += Cat_Com_Productos.Campo_Producto_ID + ")as CLAVE";

                if (Datos.P_Tipo_Articulo == "PRODUCTO")
                {
                    Mi_SQL += ",(select PRODUCTOS." + Cat_Com_Productos.Campo_Nombre + " from ";
                    Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS ";
                    Mi_SQL += " where REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = PRODUCTOS.";
                    Mi_SQL += Cat_Com_Productos.Campo_Producto_ID + ")as NOMBRE";

                    Mi_SQL += ",(select PRODUCTOS." + Cat_Com_Productos.Campo_Descripcion + " from ";
                    Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS ";
                    Mi_SQL += " where REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = PRODUCTOS.";
                    Mi_SQL += Cat_Com_Productos.Campo_Producto_ID + ")as DESCRIPCION";
                }
                else if (Datos.P_Tipo_Articulo == "SERVICIO")
                {
                    Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio + " AS  NOMBRE ";
                    Mi_SQL += ", REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Nombre_Giro + " AS  DESCRIPCION";
                }

                Mi_SQL += ", ( SELECT " + Cat_Com_Unidades.Campo_Abreviatura + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " WHERE ";
                Mi_SQL += Cat_Com_Unidades.Campo_Unidad_ID + " = ( SELECT " + Cat_Com_Unidades.Campo_Unidad_ID + "  FROM ";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " WHERE " + Cat_Com_Productos.Campo_Producto_ID + " = ";
                Mi_SQL += Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " )) AS UNIDAD ";

                Mi_SQL += " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PRODUCTO, ";
                Mi_SQL += Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICIONES ";
                
                Mi_SQL += " WHERE  REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = ";
                Mi_SQL += " REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + "";
                Mi_SQL += " AND  REQ_PRODUCTO." + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " = ";
                Mi_SQL += Datos.P_No_Orden_Compra;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0]; //  Entregar resultado
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        #endregion
    }
}
