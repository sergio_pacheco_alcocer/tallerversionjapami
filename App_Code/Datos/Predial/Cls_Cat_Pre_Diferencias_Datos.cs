﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Catalogo_Diferencias.Negocio;

/// <summary>
/// Summary description for Cls_Cat_Pre_Diferencias_Datos
/// </summary>
/// 

namespace JAPAMI.Catalogo_Diferencias.Datos{
    public class Cls_Cat_Pre_Diferencias_Datos {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Diferencia
        ///DESCRIPCIÓN: Da de alta en la Base de Datos una nueva Diferencia
        ///PARAMETROS:     
        ///             1. Diferencia.  Instancia de la Clase de Negocio de Cls_Cat_Pre_Diferencias_Negocio
        ///                             con los datos de la Diferencia que va a ser dado de Alta.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///
        ///FECHA_CREO: 31/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void Alta_Diferencia(Cls_Cat_Pre_Diferencias_Negocio Diferencia){
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {
                String Diferencia_ID = Obtener_ID_Consecutivo(Cat_Pre_Diferencias.Tabla_Cat_Pre_Diferencias, Cat_Pre_Diferencias.Campo_Diferencia_ID, 5);
                String Mi_SQL = "INSERT INTO " + Cat_Pre_Diferencias.Tabla_Cat_Pre_Diferencias;
                Mi_SQL = Mi_SQL + " (" + Cat_Pre_Diferencias.Campo_Diferencia_ID + ", " + Cat_Pre_Diferencias.Campo_Identificador;
                Mi_SQL = Mi_SQL + ", " + Cat_Pre_Diferencias.Campo_Descripcion + ", " + Cat_Pre_Diferencias.Campo_Estatus;
                Mi_SQL = Mi_SQL + ", " + Cat_Pre_Diferencias.Campo_Usuario_Creo + ", " + Cat_Pre_Diferencias.Campo_Fecha_Creo + ")";
                Mi_SQL = Mi_SQL + " VALUES ('" + Diferencia_ID + "', '" + Diferencia.P_Identificador + "'";
                Mi_SQL = Mi_SQL + ",'" + Diferencia.P_Descripcion + "'";
                Mi_SQL = Mi_SQL + ",'" + Diferencia.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ",'" + Diferencia.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE()";
                Mi_SQL = Mi_SQL + ")";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                if (Diferencia.P_Diferencias_Tasas != null && Diferencia.P_Diferencias_Tasas.Rows.Count > 0){
                    String Diferencia_Tasa_ID = Obtener_ID_Consecutivo(Cat_Pre_Diferencias_Tasas.Tabla_Cat_Pre_Diferencias_Tasas, Cat_Pre_Diferencias_Tasas.Campo_Diferencia_Tasa_ID, 5);
                    for (int cnt = 0; cnt < Diferencia.P_Diferencias_Tasas.Rows.Count; cnt++){
                        Mi_SQL = "INSERT INTO " + Cat_Pre_Diferencias_Tasas.Tabla_Cat_Pre_Diferencias_Tasas;
                        Mi_SQL = Mi_SQL + " (" + Cat_Pre_Diferencias_Tasas.Campo_Diferencia_Tasa_ID + ", " + Cat_Pre_Diferencias_Tasas.Campo_Diferencia_ID;
                        Mi_SQL = Mi_SQL + ", " + Cat_Pre_Diferencias_Tasas.Campo_Año + ", " + Cat_Pre_Diferencias_Tasas.Campo_Tasa;
                        Mi_SQL = Mi_SQL + ", " + Cat_Pre_Diferencias_Tasas.Campo_Tasa_Anual + ", " + Cat_Pre_Diferencias_Tasas.Campo_Tasa_Bimestral;
                        Mi_SQL = Mi_SQL + ", " + Cat_Pre_Diferencias_Tasas.Campo_Usuario_Creo + ", " + Cat_Pre_Diferencias_Tasas.Campo_Fecha_Creo + ")";
                        Mi_SQL = Mi_SQL + " VALUES ('" + Diferencia_Tasa_ID + "', '" + Diferencia_ID + "', " + Diferencia.P_Diferencias_Tasas.Rows[cnt][1].ToString() + ", " + Diferencia.P_Diferencias_Tasas.Rows[cnt][2].ToString();
                        Mi_SQL = Mi_SQL + ", '" + Diferencia.P_Diferencias_Tasas.Rows[cnt][3].ToString() + "','" + Diferencia.P_Diferencias_Tasas.Rows[cnt][4].ToString() + "'";
                        Mi_SQL = Mi_SQL + ",'" + Diferencia.P_Usuario + "', GETDATE()";
                        Mi_SQL = Mi_SQL + ")";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        Diferencia_Tasa_ID = Convertir_A_Formato_ID(Convert.ToInt32(Diferencia_Tasa_ID) + 1, 5);
                    }
                }
                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar dar de Alta un Registro de Diferencia. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }finally {
                 Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Diferencia
        ///DESCRIPCIÓN: Actualiza en la Base de Datos una Diferencia
        ///PARAMETROS:     
        ///             1. Diferencia.  Instancia de la Clase de Negocio de Diferencias 
        ///                             con los datos de la Diferencia que va a ser Actualizada.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 31/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void Modificar_Diferencia(Cls_Cat_Pre_Diferencias_Negocio Diferencia){
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {              
                Cls_Cat_Pre_Diferencias_Negocio Diferencia_Tmp = Consultar_Datos_Diferencia(Diferencia);
                String Mi_SQL = "UPDATE " + Cat_Pre_Diferencias.Tabla_Cat_Pre_Diferencias + " SET " + Cat_Pre_Diferencias.Campo_Identificador + " = '" + Diferencia.P_Identificador + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Diferencias.Campo_Estatus + " = '" + Diferencia.P_Estatus + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Diferencias.Campo_Descripcion + " = '" + Diferencia.P_Descripcion + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Diferencias.Campo_Usuario_Modifico + " = '" + Diferencia.P_Usuario + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Diferencias.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Diferencias.Campo_Diferencia_ID + " = '" + Diferencia.P_Diferencia_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Diferencia_Tmp = Obtener_Diferencias_Tasas_Eliminados(Diferencia_Tmp, Diferencia);
                for (int cnt = 0; cnt < Diferencia_Tmp.P_Diferencias_Tasas.Rows.Count; cnt++) {
                    Mi_SQL = "DELETE FROM " + Cat_Pre_Diferencias_Tasas.Tabla_Cat_Pre_Diferencias_Tasas;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Diferencias_Tasas.Campo_Diferencia_Tasa_ID + " = '" + Diferencia_Tmp.P_Diferencias_Tasas.Rows[cnt][0].ToString() + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                if (Diferencia.P_Diferencias_Tasas!= null && Diferencia.P_Diferencias_Tasas.Rows.Count > 0){
                    String Diferencia_Tasa_ID = Obtener_ID_Consecutivo(Cat_Pre_Diferencias_Tasas.Tabla_Cat_Pre_Diferencias_Tasas, Cat_Pre_Diferencias_Tasas.Campo_Diferencia_Tasa_ID, 5);
                    for (int cnt = 0; cnt < Diferencia.P_Diferencias_Tasas.Rows.Count; cnt++){
                        if (Diferencia.P_Diferencias_Tasas.Rows[cnt][0].ToString().Trim().Equals("")){
                            Mi_SQL = "INSERT INTO " + Cat_Pre_Diferencias_Tasas.Tabla_Cat_Pre_Diferencias_Tasas;
                            Mi_SQL = Mi_SQL + " (" + Cat_Pre_Diferencias_Tasas.Campo_Diferencia_Tasa_ID + ", " + Cat_Pre_Diferencias_Tasas.Campo_Diferencia_ID;
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Diferencias_Tasas.Campo_Año + ", " + Cat_Pre_Diferencias_Tasas.Campo_Tasa;
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Diferencias_Tasas.Campo_Tasa_Anual + ", " + Cat_Pre_Diferencias_Tasas.Campo_Tasa_Bimestral;
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Diferencias_Tasas.Campo_Usuario_Creo + ", " + Cat_Pre_Diferencias_Tasas.Campo_Fecha_Creo + ")";
                            Mi_SQL = Mi_SQL + " VALUES ('" + Diferencia_Tasa_ID + "', '" + Diferencia.P_Diferencia_ID + "', " + Diferencia.P_Diferencias_Tasas.Rows[cnt][1].ToString() + ", " + Diferencia.P_Diferencias_Tasas.Rows[cnt][2].ToString();
                            Mi_SQL = Mi_SQL + "," + Diferencia.P_Diferencias_Tasas.Rows[cnt][3].ToString() + ", " + Diferencia.P_Diferencias_Tasas.Rows[cnt][4].ToString();
                            Mi_SQL = Mi_SQL + ",'" + Diferencia.P_Usuario + "', GETDATE()";
                            Mi_SQL = Mi_SQL + ")";
                            Diferencia_Tasa_ID = Convertir_A_Formato_ID(Convert.ToInt32(Diferencia_Tasa_ID) + 1, 5);
                        } else {
                            Mi_SQL = "UPDATE " + Cat_Pre_Diferencias_Tasas.Tabla_Cat_Pre_Diferencias_Tasas + " SET " + Cat_Pre_Diferencias_Tasas.Campo_Año + " = " + Diferencia.P_Diferencias_Tasas.Rows[cnt][1].ToString().Trim();
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Diferencias_Tasas.Campo_Tasa + " = " + Diferencia.P_Diferencias_Tasas.Rows[cnt][2].ToString().Trim();
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Diferencias_Tasas.Campo_Tasa_Anual + " = " + Diferencia.P_Diferencias_Tasas.Rows[cnt][3].ToString().Trim();
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Diferencias_Tasas.Campo_Tasa_Bimestral + " = " + Diferencia.P_Diferencias_Tasas.Rows[cnt][4].ToString().Trim();
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Diferencias_Tasas.Campo_Usuario_Modifico + " = '" + Diferencia.P_Usuario +"'";
                            Mi_SQL = Mi_SQL + "," + Cat_Pre_Diferencias_Tasas.Campo_Fecha_Modifico + " = GETDATE()";
                            Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Diferencias_Tasas.Campo_Diferencia_Tasa_ID + " = '" + Diferencia.P_Diferencias_Tasas.Rows[cnt][0].ToString().Trim() + "'";
                        }
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                }
                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar modificar un Registro de Diferencias. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Diferencias_Tasas_Eliminados
        ///DESCRIPCIÓN: Obtiene la lista de las Diferencias Tasas que fueron dados de alta en la Actualizacion de la
        ///             Diferencia
        ///PARAMETROS:     
        ///             1. Actuales.        Diferencias que se usa para saber los Diferencia_Tasas que estan en 
        ///                                 la Base de Datos antes de la Modificacion.
        ///             2. Actualizados.    Diferencias que se usa para saber los Diferencia_Tasas que estaran en 
        ///                                 la Base de Datos despues de la Modificacion.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 31/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private static Cls_Cat_Pre_Diferencias_Negocio Obtener_Diferencias_Tasas_Eliminados(Cls_Cat_Pre_Diferencias_Negocio Actuales, Cls_Cat_Pre_Diferencias_Negocio Actualizados){
            Cls_Cat_Pre_Diferencias_Negocio Diferencia = new Cls_Cat_Pre_Diferencias_Negocio();
            DataTable tabla = new DataTable();
            tabla.Columns.Add("DIFERENCIA_TASA_ID", Type.GetType("System.String"));
            tabla.Columns.Add("ANIO", Type.GetType("System.String"));
            tabla.Columns.Add("TASA", Type.GetType("System.String"));
            tabla.Columns.Add("TASA_ANUAL", Type.GetType("System.String"));
            tabla.Columns.Add("TASA_BIMESTRAL", Type.GetType("System.String"));
            for (int cnt1 = 0; cnt1 < Actuales.P_Diferencias_Tasas.Rows.Count; cnt1++) {
                bool eliminar = true;
                for (int cnt2 = 0; cnt2 < Actualizados.P_Diferencias_Tasas.Rows.Count; cnt2++) {
                    if (!Actualizados.P_Diferencias_Tasas.Rows[cnt2][0].ToString().Equals("")) {
                        if (Actuales.P_Diferencias_Tasas.Rows[cnt1][0].ToString().Equals(Actualizados.P_Diferencias_Tasas.Rows[cnt2][0].ToString())) {
                            eliminar = false;
                            break;
                        }
                    }
                }
                if (eliminar) {
                    DataRow fila = tabla.NewRow();
                    fila["DIFERENCIA_TASA_ID"] = Actuales.P_Diferencias_Tasas.Rows[cnt1][0].ToString();
                    fila["ANIO"] = Actuales.P_Diferencias_Tasas.Rows[cnt1][1].ToString();
                    fila["TASA"] = Actuales.P_Diferencias_Tasas.Rows[cnt1][2].ToString();
                    fila["TASA_ANUAL"] = Actuales.P_Diferencias_Tasas.Rows[cnt1][3].ToString();
                    fila["TASA_BIMESTRAL"] = Actuales.P_Diferencias_Tasas.Rows[cnt1][4].ToString();
                    tabla.Rows.Add(fila);
                }
            }
            Diferencia.P_Diferencias_Tasas = tabla;
            return Diferencia;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Diferencias
        ///DESCRIPCIÓN: Obtiene todas las Diferencias que estan dadas de alta en la Base de Datos
        ///PARAMETROS:   
        ///             1.  Diferencia.   Parametro de donde se sacara si habra o no un filtro de busqueda, en este
        ///                             caso el filtro es el Identificador.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 31/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Diferencias(Cls_Cat_Pre_Diferencias_Negocio Diferencia){
            DataTable Tabla = new DataTable();
            try{
                String Mi_SQL = "SELECT " + Cat_Pre_Diferencias.Campo_Diferencia_ID + " AS DIFERENCIA_ID, " + Cat_Pre_Diferencias.Campo_Identificador + " AS IDENTIFICADOR";
                Mi_SQL = Mi_SQL + ","+ Cat_Pre_Diferencias.Campo_Estatus +" AS ESTATUS";
                Mi_SQL = Mi_SQL +  " FROM " + Cat_Pre_Diferencias.Tabla_Cat_Pre_Diferencias;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Diferencias.Campo_Identificador + " LIKE '%" + Diferencia.P_Identificador + "%' ";
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Pre_Diferencias.Campo_Diferencia_ID;
                DataSet dataset = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataset != null) {
                    Tabla = dataset.Tables[0];
                }
            }catch(Exception Ex){
                String Mensaje = "Error al intentar consultar los registros de Diferencias. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Tabla;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Datos_Diferencia
        ///DESCRIPCIÓN: Obtiene a detalle una Diferencia.
        ///PARAMETROS:   
        ///             1. P_Diferencia.   Diferencia que se va ver a Detalle.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 31/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static Cls_Cat_Pre_Diferencias_Negocio Consultar_Datos_Diferencia(Cls_Cat_Pre_Diferencias_Negocio P_Diferencia){
            String Mi_SQL = "SELECT " + Cat_Pre_Diferencias.Campo_Identificador + ", " + Cat_Pre_Diferencias.Campo_Estatus;
            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Diferencias.Campo_Descripcion + " FROM " + Cat_Pre_Diferencias.Tabla_Cat_Pre_Diferencias;
            Mi_SQL = Mi_SQL +" WHERE " + Cat_Pre_Diferencias.Campo_Diferencia_ID + " = '" + P_Diferencia.P_Diferencia_ID + "'";
            Cls_Cat_Pre_Diferencias_Negocio R_Diferencia = new Cls_Cat_Pre_Diferencias_Negocio();
            SqlDataReader Data_Reader;
            try{
                Data_Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                R_Diferencia.P_Diferencia_ID = P_Diferencia.P_Diferencia_ID;
                while (Data_Reader.Read()){
                    R_Diferencia.P_Identificador = Data_Reader[Cat_Pre_Diferencias.Campo_Identificador].ToString();
                    R_Diferencia.P_Estatus = Data_Reader[Cat_Pre_Diferencias.Campo_Estatus].ToString();
                    R_Diferencia.P_Descripcion = Data_Reader[Cat_Pre_Diferencias.Campo_Descripcion].ToString();
                }
                Data_Reader.Close();
                Mi_SQL = "SELECT " + Cat_Pre_Diferencias_Tasas.Campo_Diferencia_Tasa_ID + " AS DIFERENCIA_TASA_ID, " + Cat_Pre_Diferencias_Tasas.Campo_Año + " AS ANIO";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Diferencias_Tasas.Campo_Tasa + " AS TASA";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Diferencias_Tasas.Campo_Tasa_Anual + " AS TASA_ANUAL";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Diferencias_Tasas.Campo_Tasa_Bimestral + " AS TASA_BIMESTRAL";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Pre_Diferencias_Tasas.Tabla_Cat_Pre_Diferencias_Tasas;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Diferencias_Tasas.Campo_Diferencia_ID + " = '" + P_Diferencia.P_Diferencia_ID + "'";
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Pre_Diferencias_Tasas.Campo_Diferencia_Tasa_ID;
                DataSet dataset = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataset == null){
                    R_Diferencia.P_Diferencias_Tasas = new DataTable();
                }else{
                    R_Diferencia.P_Diferencias_Tasas = dataset.Tables[0];
                }
            }catch (Exception Ex){
                String Mensaje = "Error al intentar consultar el registro de Diferencias. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return R_Diferencia;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Eliminar_Diferencia
        ///DESCRIPCIÓN: Elimina una Diferencia
        ///PARAMETROS:   
        ///             1. Diferencia.   Diferencia que se va eliminar.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 01/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void Eliminar_Diferencia(Cls_Cat_Pre_Diferencias_Negocio Diferencia){
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {                    
                String Mi_SQL = "DELETE FROM " + Cat_Pre_Diferencias_Tasas.Tabla_Cat_Pre_Diferencias_Tasas;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Diferencias_Tasas.Campo_Diferencia_ID + " = '" + Diferencia.P_Diferencia_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Mi_SQL = "DELETE FROM " + Cat_Pre_Diferencias.Tabla_Cat_Pre_Diferencias;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Diferencias.Campo_Diferencia_ID + " = '" + Diferencia.P_Diferencia_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
            } catch (SqlException Ex) {
                if (Ex.Number == 547){
                    Mensaje = "No se puede eliminar el registro, ya que está relacionado con datos. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar eliminar el registro de Diferencia. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje);
            }
            catch (Exception Ex) {
                Mensaje = "Error al intentar eliminar el registro de Diferencia. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARÁMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID) {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            } catch (SqlException Ex) {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
        ///PARÁMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID) {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++) {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }

    }
}