﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Catalogo_Cuentas_Predial.Negocio;

/// <summary>
/// Summary description for Cls_Cat_Pre_Cuentas_Predial_Datos
/// </summary>
namespace JAPAMI.Catalogo_Cuentas_Predial.Datos
{
    public class Cls_Cat_Pre_Cuentas_Predial_Datos
    {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Cuenta
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nueva cuenta
        ///PARAMETROS           : Cuenta, instancia de Cls_Cat_Pre_Cuentas_Datos
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 06/Diciembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static Boolean Alta_Cuenta(Cls_Cat_Pre_Cuentas_Predial_Negocio Cuenta) {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Boolean Alta;

            Alta = false;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try{
                String Cuenta_Predial_ID = Obtener_ID_Consecutivo(Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas, Cat_Pre_Cuentas_Predial.Campo_Cuenta_Predial_ID, 10);
                String Mi_SQL = "INSERT INTO " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas + "(";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Predial_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Predial + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Calle_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Propietario_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Copropietario_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Estado_Predio_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Tipo_Predio_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Uso_Suelo_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Impuesto_ID_Predial + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuota_Minima_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Origen + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Estatus + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_No_Exterior + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_No_Interior + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Superficie_Construida + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Superficie_Total + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Clave_Catastral + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Valor_Fiscal + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Efectos + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Periodo_Corriente + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuota_Anual + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Porcentaje_Exencion + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuota_Fija + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Termino_Exencion + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Fecha_Avaluo + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Usuario_Creo + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Fecha_Creo + ")";
                Mi_SQL = Mi_SQL + " VALUES ('";
                Mi_SQL = Mi_SQL + Cuenta_Predial_ID + "', '";
                Mi_SQL = Mi_SQL + Cuenta.P_Cuenta_Predial + "', ";
                Mi_SQL = Mi_SQL + Cuenta.P_Calle_ID + ", ";
                Mi_SQL = Mi_SQL + Cuenta.P_Propietario_ID + ", ";
                Mi_SQL = Mi_SQL + Cuenta.P_Copropietario_ID + ", ";
                Mi_SQL = Mi_SQL + Cuenta.P_Estado_Predio_ID + ", ";
                Mi_SQL = Mi_SQL + Cuenta.P_Tipo_Predio_ID + ", ";
                Mi_SQL = Mi_SQL + Cuenta.P_Uso_Suelo_ID + ", ";
                Mi_SQL = Mi_SQL + Cuenta.P_Impuesto_ID_Predial + ", ";
                Mi_SQL = Mi_SQL + Cuenta.P_Cuota_Minima_ID + ", '";
                Mi_SQL = Mi_SQL + Cuenta.P_Cuenta_Origen + "', '";
                Mi_SQL = Mi_SQL + Cuenta.P_Estatus + "', '";
                Mi_SQL = Mi_SQL + Cuenta.P_No_Exterior + "', '";
                Mi_SQL = Mi_SQL + Cuenta.P_No_Interior + "', ";
                Mi_SQL = Mi_SQL + Cuenta.P_Superficie_Construida + ", ";
                Mi_SQL = Mi_SQL + Cuenta.P_Superficie_Total + ", '";
                Mi_SQL = Mi_SQL + Cuenta.P_Clave_Catastral + "', ";
                Mi_SQL = Mi_SQL + Cuenta.P_Valor_Fiscal + ", '";
                Mi_SQL = Mi_SQL + Cuenta.P_Efectos + "', '";
                Mi_SQL = Mi_SQL + Cuenta.P_Periodo_Corriente + "', ";
                Mi_SQL = Mi_SQL + Cuenta.P_Cuota_Anual + ", ";
                Mi_SQL = Mi_SQL + Cuenta.P_Porcentaje_Exencion + ", '";
                Mi_SQL = Mi_SQL + Cuenta.P_Cuota_Fija + "', '";
                Mi_SQL = Mi_SQL + Cuenta.P_Termino_Exencion.ToString("dd/MM/yyyy") + "', '";
                Mi_SQL = Mi_SQL + Cuenta.P_Fecha_Avaluo.ToString("dd/MM/yyyy") + "', '";
                Mi_SQL = Mi_SQL + Cuenta.P_Usuario + "', GETDATE())";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
                Alta = true;
            }catch(SqlException Ex){
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar dar de Alta la Cuenta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            } finally {
                if (Cn.State == ConnectionState.Open) {
                    Cn.Close();
                }
            }
            return Alta;
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Cuenta
        ///DESCRIPCIÓN          : Actualiza en la Base de Datos una Cuenta de acuerdo a los parámetros indicados en la interfaz
        ///PARAMETROS           : Cuenta, instancia de Cls_Cat_Pre_Cuentas_Predial_Negocio
        ///CREO                 : Antonio Salvador Benavides Guarddado
        ///FECHA_CREO           : 07/Diciembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static Boolean Modificar_Cuenta(Cls_Cat_Pre_Cuentas_Predial_Negocio Cuenta){
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Boolean Modificar;

            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Modificar = false;
            try{   
                String Mi_SQL = "UPDATE " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas;
                Mi_SQL = Mi_SQL + " SET " + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Predial + " = '" + Cuenta.P_Cuenta_Predial + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Calle_ID + " = '" + Cuenta.P_Calle_ID + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Propietario_ID + " = '" + Cuenta.P_Propietario_ID + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Copropietario_ID + " = '" + Cuenta.P_Copropietario_ID + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Estado_Predio_ID + " = '" + Cuenta.P_Estado_Predio_ID + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Tipo_Predio_ID + " = '" + Cuenta.P_Tipo_Predio_ID + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Uso_Suelo_ID + " = '" + Cuenta.P_Uso_Suelo_ID + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Impuesto_ID_Predial + " = '" + Cuenta.P_Impuesto_ID_Predial + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuota_Minima_ID + " = '" + Cuenta.P_Cuota_Minima_ID + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Origen + " = '" + Cuenta.P_Cuenta_Origen + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Estatus + " = '" + Cuenta.P_Estatus + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_No_Exterior + " = '" + Cuenta.P_No_Exterior + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_No_Interior + " = '" + Cuenta.P_No_Interior + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Superficie_Construida + " = " + Cuenta.P_Superficie_Construida + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Superficie_Total + " = " + Cuenta.P_Superficie_Total + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Clave_Catastral + " = '" + Cuenta.P_Clave_Catastral + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Valor_Fiscal + " = " + Cuenta.P_Valor_Fiscal + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Efectos + " = '" + Cuenta.P_Efectos + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Periodo_Corriente + " = '" + Cuenta.P_Periodo_Corriente + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuota_Anual + " = " + Cuenta.P_Cuota_Anual + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Porcentaje_Exencion + " = " + Cuenta.P_Porcentaje_Exencion + ", ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuota_Fija + " = '" + Cuenta.P_Cuota_Fija + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Termino_Exencion + " = '" + Cuenta.P_Termino_Exencion + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Fecha_Avaluo + " = '" + Cuenta.P_Fecha_Avaluo + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Usuario_Modifico + " = '" + Cuenta.P_Usuario + "', ";
                Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Predial_ID + " = '" + Cuenta.P_Cuenta_Predial_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
                Modificar = true;
             } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar modificar la Cuenta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
            return Modificar;
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Cuentas
        ///DESCRIPCIÓN          : Obtiene las cuentas de acuerdo a los filtros establecidos en la interfaz
        ///PARAMETROS           : Cuenta, instancia de Cls_Cat_Pre_Cuentas_Predial_Negocio
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 07/Diciembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Cuentas(Cls_Cat_Pre_Cuentas_Predial_Negocio Cuenta)
        {
            DataTable Tabla = new DataTable();
            String Mi_SQL;
            String Mi_SQL_Campos_Foraneos = "";
            try{
                if (Cuenta.P_Incluir_Campos_Foraneos)
                {
                    Mi_SQL_Campos_Foraneos = Mi_SQL_Campos_Foraneos + "(SELECT " + Cat_Pre_Calles.Campo_Nombre + " FROM " + Cat_Pre_Calles.Tabla_Cat_Pre_Calles + " WHERE " + Cat_Pre_Calles.Campo_Calle_ID + " = " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas + "." + Cat_Pre_Cuentas_Predial.Campo_Calle_ID + ") AS NOMBRE_CALLE, ";
                    Mi_SQL_Campos_Foraneos = Mi_SQL_Campos_Foraneos + "(SELECT " + Cat_Ate_Colonias.Campo_Nombre + " FROM " + Cat_Ate_Colonias.Tabla_Cat_Ate_Colonias + " WHERE " + Cat_Ate_Colonias.Campo_Colonia_ID + " = (SELECT " + Cat_Pre_Calles_Colonias.Campo_Colonia_ID + " FROM " + Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias + " WHERE " + Cat_Pre_Calles_Colonias.Campo_Calle_ID + " = " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas + "." + Cat_Pre_Cuentas_Predial.Campo_Calle_ID + ")) AS NOMBRE_COLONIA, ";
                    Mi_SQL_Campos_Foraneos = Mi_SQL_Campos_Foraneos + "(SELECT " + Cat_Pre_Contribuyentes.Campo_Apellido_Paterno + " + ' ' +" + Cat_Pre_Contribuyentes.Campo_Apellido_Materno + " + ', ' + " + Cat_Pre_Contribuyentes.Campo_Nombre + " FROM " + Cat_Pre_Contribuyentes.Tabla_Cat_Pre_Contribuyentes + " WHERE " + Cat_Pre_Contribuyentes.Campo_Contribuyente_ID + " = " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas + "." + Cat_Pre_Cuentas_Predial.Campo_Propietario_ID + ") AS NOMBRE_PROPIETARIO, ";
                    Mi_SQL_Campos_Foraneos = Mi_SQL_Campos_Foraneos + "(SELECT " + Cat_Pre_Contribuyentes.Campo_Apellido_Paterno + " + ' ' +" + Cat_Pre_Contribuyentes.Campo_Apellido_Materno + " + ', ' + " + Cat_Pre_Contribuyentes.Campo_Nombre + " FROM " + Cat_Pre_Contribuyentes.Tabla_Cat_Pre_Contribuyentes + " WHERE " + Cat_Pre_Contribuyentes.Campo_Contribuyente_ID + " = " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas + "." + Cat_Pre_Cuentas_Predial.Campo_Copropietario_ID + ") AS NOMBRE_COPROPIETARIO, ";
                    Mi_SQL_Campos_Foraneos = Mi_SQL_Campos_Foraneos + "(SELECT " + Cat_Pre_Estados_Predio.Campo_Descripcion + " FROM " + Cat_Pre_Estados_Predio.Tabla_Cat_Pre_Estados_Predio + " WHERE " + Cat_Pre_Estados_Predio.Campo_Estado_Predio_ID + " = " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas + "." + Cat_Pre_Cuentas_Predial.Campo_Estado_Predio_ID + ") AS DESCRIPCION_ESTADO_PREDIO, ";
                    Mi_SQL_Campos_Foraneos = Mi_SQL_Campos_Foraneos + "(SELECT " + Cat_Pre_Tipos_Predio.Campo_Descripcion + " FROM " + Cat_Pre_Tipos_Predio.Tabla_Cat_Pre_Tipos_Predio + " WHERE " + Cat_Pre_Tipos_Predio.Campo_Tipo_Predio_ID + " = " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas + "." + Cat_Pre_Cuentas_Predial.Campo_Tipo_Predio_ID + ") AS DESCRIPCION_TIPO_PREDIO, ";
                    Mi_SQL_Campos_Foraneos = Mi_SQL_Campos_Foraneos + "(SELECT " + Cat_Pre_Uso_Suelo.Campo_Descripcion + " FROM " + Cat_Pre_Uso_Suelo.Tabla_Cat_Pre_Uso_Suelo + " WHERE " + Cat_Pre_Uso_Suelo.Campo_Uso_Suelo_ID + " = " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas + "." + Cat_Pre_Cuentas_Predial.Campo_Uso_Suelo_ID + ") AS DESCRIPCION_USO_SUELO, ";
                    Mi_SQL_Campos_Foraneos = Mi_SQL_Campos_Foraneos + "(SELECT " + Cat_Pre_Conceptos_Imp_Predia.Campo_Tasa + " FROM " + Cat_Pre_Conceptos_Imp_Predia.Tabla_Cat_Pre_Conceptos_Imp_Predia + " WHERE " + Cat_Pre_Conceptos_Imp_Predia.Campo_Impuesto_ID_Predial + " = " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas + "." + Cat_Pre_Cuentas_Predial.Campo_Impuesto_ID_Predial + ") AS IMPUESTO_PREDIAL, ";
                    Mi_SQL_Campos_Foraneos = Mi_SQL_Campos_Foraneos + "(SELECT " + Cat_Pre_Cuotas_Minimas.Campo_Cuota + " FROM " + Cat_Pre_Cuotas_Minimas.Tabla_Cat_Pre_Cuotas_Minimas + " WHERE " + Cat_Pre_Cuotas_Minimas.Campo_Cuota_Minima_ID + " = " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas + "." + Cat_Pre_Cuentas_Predial.Campo_Cuota_Minima_ID + ") AS CUOTA_MINIMA, ";
                }
                if (Cuenta.P_Campos_Dinamicos != null && Cuenta.P_Campos_Dinamicos != "")
                {
                    Mi_SQL = "SELECT " + Mi_SQL_Campos_Foraneos + Cuenta.P_Campos_Dinamicos;
                }
                else
                {
                    Mi_SQL = "SELECT " + Mi_SQL_Campos_Foraneos;
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Predial_ID + " AS CUENTA_PREDIAL_ID, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Predial + " AS CUENTA_PREDIAL, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Calle_ID + " AS CALLE_ID, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Propietario_ID + " AS PROPIETARIO_ID, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Copropietario_ID + " AS COPROPIETARIO_ID, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Estado_Predio_ID + " AS ESTADO_PREDIO_ID, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Tipo_Predio_ID + " AS TIPO_PREDIO_ID, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Uso_Suelo_ID + " AS USO_SUELO_ID, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Impuesto_ID_Predial + " AS IMPUESTO_ID_PREDIAL, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuota_Minima_ID + " AS CUOTA_MINIMA_ID, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Origen + " AS CUENTA_ORIGEN, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Estatus + " AS ESTATUS, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_No_Exterior + " AS NO_EXTERIOR, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_No_Interior + " AS NO_INTERIOR, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Superficie_Construida + " AS SUPERFICIE_CONSTRUIDA, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Superficie_Total + " AS SUPERFICIE_TOTAL, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Clave_Catastral + " AS CLAVE_CATASTRAL, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Valor_Fiscal + " AS VALOR_FISCAL, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Efectos + " AS EFECTOS, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Periodo_Corriente + " AS PERIODO_CORRIENTE, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuota_Anual + " AS CUOTA_ANUAL, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Porcentaje_Exencion + " AS PORCENTAJE_EXENCION, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuota_Fija + " AS CUOTA_FIJA, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Termino_Exencion + " AS TERMINO_EXENCION, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Fecha_Avaluo + " AS FECHA_AVALUO";
                }
                Mi_SQL = Mi_SQL + " FROM " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas;
                if (Cuenta.P_Filtros_Dinamicos != null && Cuenta.P_Filtros_Dinamicos != "")
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Cuenta.P_Filtros_Dinamicos;
                }
                else
                {
                    Mi_SQL = Mi_SQL + " WHERE ";
                    if (Cuenta.P_Cuenta_Predial_ID != null && Cuenta.P_Cuenta_Predial_ID != "")
                    {
                        Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Predial_ID + " = '" + Cuenta.P_Cuenta_Predial_ID + "' AND ";
                    }
                    if (Cuenta.P_Cuenta_Predial != null && Cuenta.P_Cuenta_Predial != "")
                    {
                        Mi_SQL = Mi_SQL + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Predial + " = '" + Cuenta.P_Cuenta_Predial + "' AND ";
                    }
                    if (Mi_SQL.EndsWith(" AND "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 5);
                    }
                    if (Mi_SQL.EndsWith(" WHERE "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 7);
                    }
                }
                if (Cuenta.P_Agrupar_Dinamico != null && Cuenta.P_Agrupar_Dinamico != "")
                {
                    Mi_SQL = Mi_SQL + " GROUP BY " + Cuenta.P_Agrupar_Dinamico;
                }
                if (Cuenta.P_Ordenar_Dinamico != null && Cuenta.P_Ordenar_Dinamico != "")
                {
                    Mi_SQL = Mi_SQL + " ORDER BY " + Cuenta.P_Ordenar_Dinamico;
                }
                DataSet dataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataSet != null){
                    Tabla = dataSet.Tables[0];
                }
            }catch(Exception Ex){
                String Mensaje = "Error al intentar consultar los registros de la Cuentas. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Tabla;    
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar_Cuenta
        ///DESCRIPCIÓN          : Elimina una Cuenta según el id indicado
        ///PARAMETROS           : Cuenta, instancia de Cls_Cat_Pre_Cuentas_Predial_Negocio
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 07/Diciembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static Boolean Eliminar_Cuenta(Cls_Cat_Pre_Cuentas_Predial_Negocio Cuenta){
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Boolean Eliminar;

            Eliminar = false;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {   
                String Mi_SQL = "DELETE FROM " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Predial_ID + " = '" + Cuenta.P_Cuenta_Predial_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
                Eliminar = true;
            } catch (SqlException Ex){
                Trans.Rollback();
                if (Ex.Number == 547) {
                    Mensaje = "No se puede eliminar el registro, ya que está relacionado con datos";
                } else {
                    Mensaje = "Error al intentar eliminar la Cuenta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje);
            } catch (Exception Ex) {
                Mensaje = "Error al intentar eliminar la Cuenta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos" //"Error general en la base de datos"
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
            return Eliminar;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARÁMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID) {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals("")) {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            } catch (SqlException Ex) {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
        ///PARÁMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID) {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++) {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }
    }
}