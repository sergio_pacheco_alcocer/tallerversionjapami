﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Catalogo_Calles.Negocio;

/// <summary>
/// Summary description for Cls_Cat_Pre_Calles_Datos
/// </summary>

namespace JAPAMI.Catalogo_Calles.Datos{

    public class Cls_Cat_Pre_Calles_Datos{

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Calle
        ///DESCRIPCIÓN: Da de alta en la Base de Datos una nueva Calle
        ///PARAMETROS:     
        ///             1. Calle.    Instancia de la Clase de Negocio de Calles con los datos 
        ///                          de la Calle que va a ser dada de Alta.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void Alta_Calle(Cls_Cat_Pre_Calles_Negocio Calle){
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {
                String Calle_ID = Obtener_ID_Consecutivo(Cat_Pre_Calles.Tabla_Cat_Pre_Calles, Cat_Pre_Calles.Campo_Calle_ID, 5);
                String Mi_SQL = "INSERT INTO " + Cat_Pre_Calles.Tabla_Cat_Pre_Calles;
                Mi_SQL = Mi_SQL + " (" + Cat_Pre_Calles.Campo_Calle_ID + ", " + Cat_Pre_Calles.Campo_Nombre;
                Mi_SQL = Mi_SQL + ", " + Cat_Pre_Calles.Campo_Tipo_Calle;
                Mi_SQL = Mi_SQL + ", " + Cat_Pre_Calles.Campo_Usuario_Creo + ", " + Cat_Pre_Calles.Campo_Fecha_Creo;
                Mi_SQL = Mi_SQL + " ) VALUES (";
                Mi_SQL = Mi_SQL + "'" + Calle_ID + "', '" + Calle.P_Nombre + "'";
                Mi_SQL = Mi_SQL + ",'" + Calle.P_Tipo_Calle + "'";
                Mi_SQL = Mi_SQL + ",'" + Calle.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", CONVERT(VARCHAR(10), GETDATE(), 103)";
                Mi_SQL = Mi_SQL + ")";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                if (Calle.P_Calles_Colonias != null && Calle.P_Calles_Colonias.Rows.Count > 0){
                    String Calle_Colonia_ID = Obtener_ID_Consecutivo(Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias, Cat_Pre_Calles_Colonias.Campo_Calle_Colonia_ID, 10);
                    for (int cnt = 0; cnt < Calle.P_Calles_Colonias.Rows.Count; cnt++){
                        Mi_SQL = "INSERT INTO " + Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias;
                        Mi_SQL = Mi_SQL + " (" + Cat_Pre_Calles_Colonias.Campo_Calle_Colonia_ID + ", " + Cat_Pre_Calles_Colonias.Campo_Calle_ID;
                        Mi_SQL = Mi_SQL + ", " + Cat_Pre_Calles_Colonias.Campo_Colonia_ID + ", " + Cat_Pre_Calles_Colonias.Campo_Comentarios;
                        Mi_SQL = Mi_SQL + ", " + Cat_Pre_Calles_Colonias.Campo_Usuario_Creo + ", " + Cat_Pre_Calles_Colonias.Campo_Fecha_Creo + ")";
                        Mi_SQL = Mi_SQL + " VALUES ('" + Calle_Colonia_ID + "', '" + Calle_ID + "', '" + Calle.P_Calles_Colonias.Rows[cnt][1].ToString() + "', '" + Calle.P_Calles_Colonias.Rows[cnt][3].ToString()+"'";
                        Mi_SQL = Mi_SQL + ",'" + Calle.P_Usuario + "', CONVERT(VARCHAR(10), GETDATE(), 103)";
                        Mi_SQL = Mi_SQL + ")";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        Calle_Colonia_ID = Convertir_A_Formato_ID(Convert.ToInt32(Calle_Colonia_ID) + 1, 10);
                    }
                }
                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar dar de Alta una Calle. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }finally {
                 Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Calle
        ///DESCRIPCIÓN: Actualiza en la Base de Datos una Calle
        ///PARAMETROS:     
        ///             1. Calle.   Instancia de la Clase de Negocio de Calles con 
        ///                         los datos de la Calle que va a ser Actualizada.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 18/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void Modificar_Calle(Cls_Cat_Pre_Calles_Negocio Calle){
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {
                Cls_Cat_Pre_Calles_Negocio Calle_Tmp = Consultar_Datos_Calle(Calle);
                String Mi_SQL = "UPDATE " + Cat_Pre_Calles.Tabla_Cat_Pre_Calles + " SET " + Cat_Pre_Calles.Campo_Nombre + " = '" + Calle.P_Nombre + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Calles.Campo_Tipo_Calle + " = '" + Calle.P_Tipo_Calle + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Calles.Campo_Usuario_Modifico + " = '" + Calle.P_Usuario + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Calles.Campo_Fecha_Modifico + " = CONVERT(VARCHAR(10), GETDATE(), 103)";
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Calles.Campo_Calle_ID + " = '" + Calle.P_Calle_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Calle_Tmp = Obtener_Calles_Colonias_Eliminadas(Calle_Tmp, Calle);
                for (int cnt = 0; cnt < Calle_Tmp.P_Calles_Colonias.Rows.Count; cnt++) {
                    Mi_SQL = "DELETE FROM " + Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Calles_Colonias.Campo_Calle_Colonia_ID + " = '" + Calle_Tmp.P_Calles_Colonias.Rows[cnt][0].ToString() + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                if (Calle.P_Calles_Colonias!= null && Calle.P_Calles_Colonias.Rows.Count > 0){
                    String Calle_Colonia_ID = Obtener_ID_Consecutivo(Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias, Cat_Pre_Calles_Colonias.Campo_Calle_Colonia_ID, 10);
                    for (int cnt = 0; cnt < Calle.P_Calles_Colonias.Rows.Count; cnt++){
                        if (Calle.P_Calles_Colonias.Rows[cnt][0].ToString().Trim().Equals("")){
                            Mi_SQL = "INSERT INTO " + Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias;
                            Mi_SQL = Mi_SQL + " (" + Cat_Pre_Calles_Colonias.Campo_Calle_Colonia_ID + ", " + Cat_Pre_Calles_Colonias.Campo_Calle_ID;
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Calles_Colonias.Campo_Colonia_ID + ", " + Cat_Pre_Calles_Colonias.Campo_Comentarios;
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Calles_Colonias.Campo_Usuario_Creo + ", " + Cat_Pre_Calles_Colonias.Campo_Fecha_Creo + ")";
                            Mi_SQL = Mi_SQL + " VALUES ('" + Calle_Colonia_ID + "', '" + Calle.P_Calle_ID + "', '" + Calle.P_Calles_Colonias.Rows[cnt][1].ToString() + "', '" + Calle.P_Calles_Colonias.Rows[cnt][3].ToString() + "'";
                            Mi_SQL = Mi_SQL + ",'" + Calle.P_Usuario + "', CONVERT(VARCHAR(10), GETDATE(), 103)";
                            Mi_SQL = Mi_SQL + ")";
                            Calle_Colonia_ID = Convertir_A_Formato_ID(Convert.ToInt32(Calle_Colonia_ID) + 1, 10);
                        } else {
                            Mi_SQL = "UPDATE " + Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias + " SET " + Cat_Pre_Calles_Colonias.Campo_Colonia_ID + " = '" + Calle.P_Calles_Colonias.Rows[cnt][1].ToString().Trim() + "'";
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Calles_Colonias.Campo_Comentarios + " = '" + Calle.P_Calles_Colonias.Rows[cnt][3].ToString().Trim() + "'";
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Calles_Colonias.Campo_Usuario_Modifico + " = '" + Calle.P_Usuario + "'";
                            Mi_SQL = Mi_SQL + "," + Cat_Pre_Calles_Colonias.Campo_Fecha_Modifico + " = CONVERT(VARCHAR(10), GETDATE(), 103)";
                            Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Calles_Colonias.Campo_Calle_Colonia_ID + " = '" + Calle.P_Calles_Colonias.Rows[cnt][0].ToString().Trim() + "'";
                        }
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                }
                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar modificar una calle. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Calles_Colonias_Eliminadas
        ///DESCRIPCIÓN: Obtiene la lista de las Calles_Colonias que fueron dados de baja 
        ///             en la Actualizacion de las Calles.
        ///PARAMETROS:     
        ///             1. Actuales.        Calles que se usa para saber los Calles_Colonias que estan en 
        ///                                 la Base de Datos antes de la Modificacion.
        ///             2. Actualizados.    Calles que se usa para saber los Calles_Colonias que estaran en 
        ///                                 la Base de Datos despues de la Modificacion.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 18/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private static Cls_Cat_Pre_Calles_Negocio Obtener_Calles_Colonias_Eliminadas(Cls_Cat_Pre_Calles_Negocio Actuales, Cls_Cat_Pre_Calles_Negocio Actualizados){
            Cls_Cat_Pre_Calles_Negocio Calle = new Cls_Cat_Pre_Calles_Negocio();
            DataTable tabla = new DataTable();
            tabla.Columns.Add("CALLE_COLONIA_ID", Type.GetType("System.String"));
            tabla.Columns.Add("CALLE_ID", Type.GetType("System.String"));
            tabla.Columns.Add("COLONIA_ID", Type.GetType("System.String"));
            for (int cnt1 = 0; cnt1 < Actuales.P_Calles_Colonias.Rows.Count; cnt1++) {
                bool eliminar = true;
                for (int cnt2 = 0; cnt2 < Actualizados.P_Calles_Colonias.Rows.Count; cnt2++) {
                    if (!Actualizados.P_Calles_Colonias.Rows[cnt2][0].ToString().Equals("")){
                        if (Actuales.P_Calles_Colonias.Rows[cnt1][0].ToString().Equals(Actualizados.P_Calles_Colonias.Rows[cnt2][0].ToString())){
                            eliminar = false;
                            break;
                        }
                    }
                }
                if (eliminar) {
                    DataRow fila = tabla.NewRow();
                    fila["CALLE_COLONIA_ID"] = Actuales.P_Calles_Colonias.Rows[cnt1][0].ToString();
                    fila["CALLE_ID"] = Actuales.P_Calles_Colonias.Rows[cnt1][1].ToString();
                    fila["COLONIA_ID"] = Actuales.P_Calles_Colonias.Rows[cnt1][2].ToString();
                    tabla.Rows.Add(fila);
                }
            }
            Calle.P_Calles_Colonias = tabla;
            return Calle;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Calles
        ///DESCRIPCIÓN: Obtiene todos las Calles que estan dados de alta en la Base de Datos
        ///PARAMETROS:      
        ///             1.  Calle.    Parametro de donde se sacara si habra o no un filtro 
        ///                           de busqueda, en este caso el filtro es el Identificador.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Septiembre/2010 
        ///MODIFICO             : Antonio Salvador Benavides Guardado
        ///FECHA_MODIFICO       : 16/Diciembre/2010
        ///CAUSA_MODIFICACIÓN   : Adecuar funcionalidad para hacer más dinámica la cosnulta de calles dentro ordenes de variación
        ///*******************************************************************************
        public static DataTable Consultar_Calles(Cls_Cat_Pre_Calles_Negocio Calle){
            DataTable Tabla = new DataTable();
            String Mi_SQL;
            String Mi_SQL_Campos_Foraneos = "";

            try
            {
                if (Calle.P_Incluir_Campos_Foraneos)
                {
                    Mi_SQL_Campos_Foraneos = Mi_SQL_Campos_Foraneos + "(SELECT " + Cat_Ate_Colonias.Campo_Nombre + " FROM " + Cat_Ate_Colonias.Tabla_Cat_Ate_Colonias + " WHERE " + Cat_Ate_Colonias.Campo_Colonia_ID + " = " + Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias + "." + Cat_Pre_Calles_Colonias.Campo_Colonia_ID + " AND " + Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias + "." + Cat_Pre_Calles_Colonias.Campo_Calle_ID + " = " + Cat_Pre_Calles.Tabla_Cat_Pre_Calles + "." + Cat_Pre_Calles.Campo_Calle_ID + ") AS COLONIA";
                }
                if (Calle.P_Campos_Dinamicos != null && Calle.P_Campos_Dinamicos != "")
                {
                    Mi_SQL = "SELECT " + Mi_SQL_Campos_Foraneos + Calle.P_Campos_Dinamicos;
                }
                else
                {
                    Mi_SQL = "SELECT " + Mi_SQL_Campos_Foraneos;
                    Mi_SQL = Mi_SQL + Cat_Pre_Calles.Campo_Calle_ID + " AS CALLE_ID, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Calles.Campo_Nombre + " AS NOMBRE, ";
                    Mi_SQL = Mi_SQL + Cat_Pre_Calles.Campo_Tipo_Calle + " AS TIPO_CALLE";
                }
                Mi_SQL = Mi_SQL + " FROM " + Cat_Pre_Calles.Tabla_Cat_Pre_Calles;
                if (Calle.P_Filtros_Dinamicos != null && Calle.P_Filtros_Dinamicos != "")
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Calle.P_Filtros_Dinamicos;
                }
                else
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Calles.Campo_Nombre + " LIKE '%" + Calle.P_Nombre + "%' ";
                    //DESCOMENTAR EL SIGUIENTE BLOQUE IF SI SE AGREGAN FILTROS EN ESTA SECCIÓN
                    //if (Mi_SQL.EndsWith(" AND "))
                    //{
                    //    Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 5);
                    //}
                    //DESCOMENTAR EL SIGUIENTE BLOQUE WHERE SI SE QUITA EL CAMPO CONCEPTO_PREDIAL_ID DE LA LÍNEA DEL WHERE
                    //if (Mi_SQL.EndsWith(" WHERE "))
                    //{
                    //    Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 7);
                    //}
                }
                if (Calle.P_Agrupar_Dinamico != null && Calle.P_Agrupar_Dinamico != "")
                {
                    Mi_SQL = Mi_SQL + " GROUP BY " + Calle.P_Agrupar_Dinamico;
                }
                if (Calle.P_Ordenar_Dinamico != null && Calle.P_Ordenar_Dinamico != "")
                {
                    Mi_SQL = Mi_SQL + " ORDER BY " + Calle.P_Ordenar_Dinamico;
                }
                else
                {
                    Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Pre_Calles.Campo_Nombre;
                }
                DataSet dataset = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataset != null) {
                    Tabla = dataset.Tables[0];
                }
            } catch (Exception Ex) {
                String Mensaje = "Error al intentar consultar las Calles. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Tabla;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Datos_Calle
        ///DESCRIPCIÓN: Obtiene a detalle una Calle.
        ///PARAMETROS:   
        ///             1. P_Calle.   Calle que se va ver a Detalle.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static Cls_Cat_Pre_Calles_Negocio Consultar_Datos_Calle(Cls_Cat_Pre_Calles_Negocio P_Calle){
            String Mi_SQL = "SELECT " + Cat_Pre_Calles.Campo_Nombre + ", " + Cat_Pre_Calles.Campo_Tipo_Calle;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Pre_Calles.Tabla_Cat_Pre_Calles;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Calles.Campo_Calle_ID + " = '" + P_Calle.P_Calle_ID + "'";
            Cls_Cat_Pre_Calles_Negocio R_Calle = new Cls_Cat_Pre_Calles_Negocio();
            SqlDataReader Data_Reader;
            try{
                Data_Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                R_Calle.P_Calle_ID = P_Calle.P_Calle_ID;
                while (Data_Reader.Read()){
                    R_Calle.P_Nombre = Data_Reader[Cat_Pre_Calles.Campo_Nombre].ToString();
                    R_Calle.P_Tipo_Calle = Data_Reader[Cat_Pre_Calles.Campo_Tipo_Calle].ToString();
                }
                Data_Reader.Close();
                Mi_SQL = "SELECT " + Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias + "." + Cat_Pre_Calles_Colonias.Campo_Calle_Colonia_ID + " AS CALLE_COLONIA_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias + "." + Cat_Pre_Calles_Colonias.Campo_Colonia_ID + " AS COLONIA_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Ate_Colonias.Tabla_Cat_Ate_Colonias + "." + Cat_Ate_Colonias.Campo_Nombre + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias + "." + Cat_Pre_Calles_Colonias.Campo_Comentarios + " AS COMENTARIOS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias + ", " + Cat_Ate_Colonias.Tabla_Cat_Ate_Colonias;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias + "." + Cat_Pre_Calles_Colonias.Campo_Calle_ID;
                Mi_SQL = Mi_SQL + " = '" + P_Calle.P_Calle_ID + "' ";
                Mi_SQL = Mi_SQL + " AND " + Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias + "." + Cat_Pre_Calles_Colonias.Campo_Colonia_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Ate_Colonias.Tabla_Cat_Ate_Colonias + "." + Cat_Ate_Colonias.Campo_Colonia_ID;
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Ate_Colonias.Tabla_Cat_Ate_Colonias + "." + Cat_Ate_Colonias.Campo_Nombre;
                DataSet dataset = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataset == null){
                    R_Calle.P_Calles_Colonias = new DataTable();
                }else{
                    R_Calle.P_Calles_Colonias = dataset.Tables[0];
                }
            } catch (Exception Ex) {
                String Mensaje = "Error al intentar consultar la Calle. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return R_Calle;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Eliminar_Calle
        ///DESCRIPCIÓN: Elimina una Calle
        ///PARAMETROS:   
        ///             1. Calle.   Calle que se va eliminar.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 19/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void Eliminar_Calle(Cls_Cat_Pre_Calles_Negocio Calle){
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {
                String Mi_SQL = "DELETE FROM " + Cat_Pre_Calles_Colonias.Tabla_Cat_Pre_Calles_Colonias;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Calles_Colonias.Campo_Calle_ID + " = '" + Calle.P_Calle_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Mi_SQL = "DELETE FROM " + Cat_Pre_Calles.Tabla_Cat_Pre_Calles;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Calles.Campo_Calle_ID + " = '" + Calle.P_Calle_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
            } catch (SqlException Ex) {
                if (Ex.Number == 547) {
                    Mensaje = "No se puede eliminar el registro, ya que está relacionado con datos. Error: [" + Ex.Message + "]"; ;
                } else {
                    Mensaje = "Error al intentar eliminar la Calle. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje);
            } catch (Exception Ex) {
                Mensaje = "Error al intentar eliminar la Calle. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Colonias_Almacenadas
        ///DESCRIPCIÓN: Obtiene todas las Colonias que estan dadas de alta en la Base de Datos
        ///PARAMETROS:      
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 21/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Colonias(){
            DataTable Tabla = new DataTable();
            String Mi_SQL = "SELECT " + Cat_Ate_Colonias.Campo_Colonia_ID + ", " + Cat_Ate_Colonias.Campo_Nombre + "";
            Mi_SQL = Mi_SQL + " FROM " + Cat_Ate_Colonias.Tabla_Cat_Ate_Colonias;
            Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Ate_Colonias.Campo_Nombre;
            try{
                DataSet dataset = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataset != null){
                    Tabla = dataset.Tables[0];
                }
            } catch (Exception Ex) {
                String Mensaje = "Error al intentar consultar las Colonias. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Tabla;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARÁMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID) {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals("")) {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            } catch (SqlException Ex) {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
        ///PARÁMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID) {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++) {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Nombre
        ///DESCRIPCIÓN: Obtiene el nombre de la Calle solicitada.
        ///PARAMETROS:   
        ///             1. Calle.   Nombre que se va ver a Detalle.
        ///CREO: José Alfredo García Pichardo.
        ///FECHA_CREO: 20/Julio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Nombre(Cls_Cat_Pre_Calles_Negocio Calle) //Busqueda
        {

            DataTable Tabla = new DataTable();
            try
            {
                String Mi_SQL = "SELECT C." + Cat_Pre_Calles.Campo_Calle_ID;
                Mi_SQL = Mi_SQL + ", CC." + Cat_Ate_Colonias.Campo_Nombre + " AS COLONIA";
                Mi_SQL = Mi_SQL + ", CC." + Cat_Ate_Colonias.Campo_Colonia_ID;
                Mi_SQL = Mi_SQL + ", C." + Cat_Pre_Calles.Campo_Clave;
                Mi_SQL = Mi_SQL + ", C." + Cat_Pre_Calles.Campo_Estatus;
                Mi_SQL = Mi_SQL + ", C." + Cat_Pre_Calles.Campo_Nombre;
                Mi_SQL = Mi_SQL + ", ISNULL(C." + Cat_Pre_Calles.Campo_Comentarios + ", ' ') AS COMENTARIOS";
                Mi_SQL = Mi_SQL + " FROM  " + Cat_Pre_Calles.Tabla_Cat_Pre_Calles;
                Mi_SQL = Mi_SQL + " C LEFT JOIN " + Cat_Ate_Colonias.Tabla_Cat_Ate_Colonias;
                Mi_SQL = Mi_SQL + " CC ON " + "C." + Cat_Pre_Calles.Campo_Colonia_ID + " = CC." + Cat_Ate_Colonias.Campo_Colonia_ID;


                if (Calle.P_Nombre_Calle != null)
                {
                    Mi_SQL = Mi_SQL + " WHERE C." + Cat_Pre_Calles.Campo_Nombre + " LIKE '%" + Calle.P_Nombre_Calle + "%' ";
                    Mi_SQL = Mi_SQL + " ORDER BY C." + Cat_Pre_Calles.Campo_Nombre;
                }
                else if (Calle.P_Nombre_Colonia != null)
                {
                    Mi_SQL = Mi_SQL + " WHERE CC." + Cat_Ate_Colonias.Campo_Nombre + " LIKE '%" + Calle.P_Nombre_Colonia + "%' ";
                    Mi_SQL = Mi_SQL + " ORDER BY CC." + Cat_Ate_Colonias.Campo_Nombre;
                }
                else
                {
                    Mi_SQL = Mi_SQL + " WHERE C." + Cat_Ate_Colonias.Campo_Clave + " = '" + Calle.P_Clave + "' ";
                    Mi_SQL = Mi_SQL + " ORDER BY C." + Cat_Ate_Colonias.Campo_Clave;
                }
                DataSet dataset = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataset != null)
                {
                    Tabla = dataset.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de Calles. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Tabla;
        }

    }
}