﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Operacion_Predial_Constancias.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Ope_Pre_Constancias_Datos
/// </summary>

namespace JAPAMI.Operacion_Predial_Constancias.Datos
{

    public class Cls_Ope_Pre_Constancias_Datos
    {
        #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Constancia
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro de Constancia
        ///PARAMETROS           : 1. Constancia.   Instancia de la Clase de Negocio de Cls_Ope_Pre_Constancias_Negocio
        ///                                 con los datos de Constancias que va a ser dado de Alta.
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 29/Junio/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Boolean Alta_Constancia(Cls_Ope_Pre_Constancias_Negocio Constancia)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Boolean Alta = false;

            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            String No_Constancia = Obtener_ID_Consecutivo(Ope_Pre_Constancias.Tabla_Ope_Pre_Constancias, Ope_Pre_Constancias.Campo_No_Constancia, 5);
            try
            {
                String Mi_SQL = "INSERT INTO " + Ope_Pre_Constancias.Tabla_Ope_Pre_Constancias + " (";
                Mi_SQL += Ope_Pre_Constancias.Campo_No_Constancia + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Tipo_Constancia_ID + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Cuenta_Predial_ID + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Propietario_ID + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Realizo + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Confronto + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Documento_ID + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Folio + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_No_Recibo + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Fecha + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Periodo_Año + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Periodo_Bimestre + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Estatus + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Observaciones + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Leyenda_Certificacion + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Usuario_Creo + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Fecha_Creo + ") ";
                Mi_SQL += "VALUES ('";
                Mi_SQL += No_Constancia + "', '";
                Mi_SQL += Constancia.P_Tipo_Constancia_ID + "', '";
                Mi_SQL += Constancia.P_Cuenta_Predial_ID + "', '";
                Mi_SQL += Constancia.P_Propietario_ID + "', '";
                Mi_SQL += Constancia.P_Realizo + "', '";
                Mi_SQL += Constancia.P_Confronto + "', '";
                Mi_SQL += Constancia.P_Documento_ID + "', '";
                Mi_SQL += Constancia.P_Folio + "', '";
                Mi_SQL += Constancia.P_No_Recibo + "', '";
                Mi_SQL += Constancia.P_Fecha.ToString() + "', ";
                Mi_SQL += Constancia.P_Periodo_Año + ", ";
                Mi_SQL += Constancia.P_Periodo_Bimestre + ", '";
                Mi_SQL += Constancia.P_Estatus + "', '";
                Mi_SQL += Constancia.P_Observaciones + "', '";
                Mi_SQL += Constancia.P_Leyenda_Certificacion + "', '";
                Mi_SQL += Constancia.P_Usuario + "', GETDATE())";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
                Alta = true;
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta un Registro de Constancia. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
            return Alta;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Constancia
        ///DESCRIPCIÓN          : Modifica en la Base de Datos el registro indicado del Constancia
        ///PARAMETROS          : 1. Constancia.   Instancia de la Clase de Negocio de Cls_Ope_Pre_Constancias_Negocio
        ///                                 con los datos del Constancias que va a ser Modificado.
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 29/Junio/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Boolean Modificar_Constancia(Cls_Ope_Pre_Constancias_Negocio Constancia)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Boolean Actualizar = false;

            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                String Mi_SQL = "UPDATE " + Ope_Pre_Constancias.Tabla_Ope_Pre_Constancias + " SET ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Tipo_Constancia_ID + " = '" + Constancia.P_Tipo_Constancia_ID + "', ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Cuenta_Predial_ID + " = '" + Constancia.P_Cuenta_Predial_ID + "', ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Propietario_ID + " = '" + Constancia.P_Propietario_ID + "', ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Realizo + " = '" + Constancia.P_Realizo + "', ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Confronto + " = '" + Constancia.P_Confronto + "', ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Documento_ID + " = '" + Constancia.P_Documento_ID + "', ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Folio + " = '" + Constancia.P_Folio + "', ";
                Mi_SQL += Ope_Pre_Constancias.Campo_No_Recibo + " = '" + Constancia.P_Realizo + "', ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Fecha + " = '" + Constancia.P_Fecha.ToString() + "', ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Periodo_Año + " = " + Constancia.P_Periodo_Año + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Periodo_Bimestre + " = " + Constancia.P_Periodo_Bimestre + ", ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Estatus + " = '" + Constancia.P_Estatus + "', ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Observaciones + " = '" + Constancia.P_Observaciones + "', ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Leyenda_Certificacion + " = '" + Constancia.P_Leyenda_Certificacion + "', ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Usuario_Modifico + " = '" + Constancia.P_Usuario + "', ";
                Mi_SQL += Ope_Pre_Constancias.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL += "WHERE " + Ope_Pre_Constancias.Campo_No_Constancia + " = '" + Constancia.P_No_Constancia + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
                Actualizar = true;
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar modificar un Registro de Constancia. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
            return Actualizar;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Constancias
        ///DESCRIPCIÓN          : Obtiene todos los Constancia que estan dados de alta en la base de datos
        ///PARAMETROS           : 1. Constancia.   Instancia de la Clase de Negocio de Cls_Ope_Pre_Constancias_Negocio
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 29/Junio/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Constancias(Cls_Ope_Pre_Constancias_Negocio Constancia)
        {
            DataTable Dt_Constancias = new DataTable();
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT ";
                if (Constancia.P_Campos_Foraneos)
                {
                    Mi_SQL += "(SELECT " + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Predial + " FROM " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas + " WHERE " + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Predial_ID + " = " + Ope_Pre_Constancias.Tabla_Ope_Pre_Constancias + "." + Ope_Pre_Constancias.Campo_Cuenta_Predial_ID + ") AS Cuenta_Predial, ";
                    Mi_SQL += "(SELECT " + Cat_Pre_Contribuyentes.Campo_Nombre + " + " + Cat_Pre_Contribuyentes.Campo_Apellido_Paterno + " + " + Cat_Pre_Contribuyentes.Campo_Apellido_Materno + " FROM " + Cat_Pre_Contribuyentes.Tabla_Cat_Pre_Contribuyentes + " WHERE " + Cat_Pre_Contribuyentes.Campo_Contribuyente_ID + " IN(SELECT " + Cat_Pre_Propietarios.Campo_Contribuyente_ID + " FROM " + Cat_Pre_Propietarios.Tabla_Cat_Pre_Propietarios + " WHERE " + Cat_Pre_Propietarios.Campo_Propietario_ID + " = " + Ope_Pre_Constancias.Campo_Propietario_ID + ")) AS Nombre_Propietario, ";
                    Mi_SQL += "(SELECT " + Cat_Pre_Contribuyentes.Campo_Domicilio + " FROM " + Cat_Pre_Contribuyentes.Tabla_Cat_Pre_Contribuyentes + " WHERE " + Cat_Pre_Contribuyentes.Campo_Contribuyente_ID + " IN(SELECT " + Cat_Pre_Propietarios.Campo_Contribuyente_ID + " FROM " + Cat_Pre_Propietarios.Tabla_Cat_Pre_Propietarios + " WHERE " + Cat_Pre_Propietarios.Campo_Propietario_ID + " = " + Ope_Pre_Constancias.Campo_Propietario_ID + ")) AS Domicilio_Propietario, ";
                    Mi_SQL += "(SELECT " + Cat_Pre_Contribuyentes.Campo_RFC + " FROM " + Cat_Pre_Contribuyentes.Tabla_Cat_Pre_Contribuyentes + " WHERE " + Cat_Pre_Contribuyentes.Campo_Contribuyente_ID + " IN(SELECT " + Cat_Pre_Propietarios.Campo_Contribuyente_ID + " FROM " + Cat_Pre_Propietarios.Tabla_Cat_Pre_Propietarios + " WHERE " + Cat_Pre_Propietarios.Campo_Propietario_ID + " = " + Ope_Pre_Constancias.Campo_Propietario_ID + ")) AS RFC_Propietario, ";
                    Mi_SQL += "(SELECT " + Cat_Empleados.Campo_Nombre + " + " + Cat_Empleados.Campo_Apellido_Paterno + " + " + Cat_Empleados.Campo_Apellido_Materno + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " WHERE " + Cat_Empleados.Campo_Empleado_ID + " = " + Ope_Pre_Constancias.Campo_Realizo + " ) AS Nombre_Realizo, ";
                    Mi_SQL += "(SELECT " + Cat_Empleados.Campo_Nombre + " + " + Cat_Empleados.Campo_Apellido_Paterno + " + " + Cat_Empleados.Campo_Apellido_Materno + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " WHERE " + Cat_Empleados.Campo_Empleado_ID + " = " + Ope_Pre_Constancias.Campo_Confronto + " ) AS Nombre_Confronto, ";
                    Mi_SQL += "(SELECT " + Cat_Pre_Tipos_Documento.Campo_Nombre_Documento + " FROM " + Cat_Pre_Tipos_Documento.Tabla_Cat_Pre_Tipos_Documento + " WHERE " + Cat_Pre_Tipos_Documento.Campo_Documento_ID + " = " + Ope_Pre_Constancias.Campo_Documento_ID + ") AS Nombre_Documento, ";
                }
                if (Constancia.P_Campos_Dinamicos != null && Constancia.P_Campos_Dinamicos != "")
                {
                    Mi_SQL += Constancia.P_Campos_Dinamicos;
                }
                else
                {
                    Mi_SQL += Ope_Pre_Constancias.Campo_No_Constancia + " AS No_Constancia, ";
                    Mi_SQL += Ope_Pre_Constancias.Campo_Tipo_Constancia_ID + " AS Tipo_Constancia_ID, ";
                    Mi_SQL += Ope_Pre_Constancias.Campo_Cuenta_Predial_ID + " AS Cuenta_Predial_ID, ";
                    Mi_SQL += Ope_Pre_Constancias.Campo_Propietario_ID + " AS Propietario_ID, ";
                    Mi_SQL += Ope_Pre_Constancias.Campo_Realizo + " AS Realizo, ";
                    Mi_SQL += Ope_Pre_Constancias.Campo_Confronto + " AS Confronto, ";
                    Mi_SQL += Ope_Pre_Constancias.Campo_Documento_ID + " AS Documento_ID, ";
                    Mi_SQL += Ope_Pre_Constancias.Campo_Folio + " AS Foli, ";
                    Mi_SQL += Ope_Pre_Constancias.Campo_No_Recibo + " AS No_Recibo, ";
                    Mi_SQL += Ope_Pre_Constancias.Campo_Fecha + " AS Fecha, ";
                    Mi_SQL += Ope_Pre_Constancias.Campo_Periodo_Año + " AS Periodo_Año, ";
                    Mi_SQL += Ope_Pre_Constancias.Campo_Periodo_Bimestre + " AS Periodo_Bimestre, ";
                    Mi_SQL += Ope_Pre_Constancias.Campo_Estatus + " AS P_Estatus, ";
                    Mi_SQL += Ope_Pre_Constancias.Campo_Observaciones + " AS Observaciones,";
                    Mi_SQL += Ope_Pre_Constancias.Campo_Leyenda_Certificacion + " AS Leyenda_Certificacion";
                }
                Mi_SQL += " FROM " + Ope_Pre_Constancias.Tabla_Ope_Pre_Constancias;
                if (Constancia.P_Filtros_Dinamicos != null && Constancia.P_Filtros_Dinamicos != "")
                {
                    Mi_SQL += " WHERE " + Constancia.P_Filtros_Dinamicos;
                }
                else
                {
                    Mi_SQL += " WHERE " + Ope_Pre_Constancias.Campo_Folio + " LIKE '%" + Constancia.P_Folio + "%'";
                    //if (Mi_SQL.EndsWith(" AND "))
                    //{
                    //    Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 5);
                    //}
                    //DESCOMENTAR EL SIGUIENTE BLOQUE WHERE SI SE QUITA EL CAMPO CONCEPTO_PREDIAL_ID DE LA LÍNEA DEL WHERE
                    //if (Mi_SQL.EndsWith(" WHERE "))
                    //{
                    //    Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 7);
                    //}
                }
                if (Constancia.P_Agrupar_Dinamico != null && Constancia.P_Agrupar_Dinamico != "")
                {
                    Mi_SQL += " GROUP BY " + Constancia.P_Agrupar_Dinamico;
                }
                if (Constancia.P_Ordenar_Dinamico != null && Constancia.P_Ordenar_Dinamico != "")
                {
                    Mi_SQL += " ORDER BY " + Constancia.P_Ordenar_Dinamico;
                }
                else
                {
                    Mi_SQL += " ORDER BY " + Ope_Pre_Constancias.Campo_Folio;
                }

                DataSet Ds_Constancias = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Constancias != null)
                {
                    Dt_Constancias = Ds_Constancias.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de Constancia. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Constancias;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : Antonio Salvador Benavides Guardado
        ///FECHA_MODIFICO       : 26/Octubre/2010
        ///CAUSA_MODIFICACIÓN   : Estandarizar variables usadas
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID)
        {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try
            {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            }
            catch (SqlException Ex)
            {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
        ///PARAMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : Antonio Salvador Benavides Guardado
        ///FECHA_MODIFICO       : 26/Octubre/2010
        ///CAUSA_MODIFICACIÓN   : Estandarizar variables usadas
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
        {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
            {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }

        #endregion

    }

    
}