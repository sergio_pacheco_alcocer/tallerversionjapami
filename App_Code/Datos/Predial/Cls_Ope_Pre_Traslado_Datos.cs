﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Operacion_Predial_Traslado.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Ope_Pre_Traslado_Dominio_Datos
/// </summary>

namespace JAPAMI.Operacion_Predial_Traslado.Datos
{

    public class Cls_Ope_Pre_Traslado_Datos
    {
        #region Metodos
        
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_DataTable
            ///DESCRIPCIÓN: Hace una consulta a la Base de Datos y obtiene los datos en un
            ///             DataTable.
            ///PARAMETROS:     
            ///             1.  Traslado_Dominio.   Contiene la propiedad para conocer que tipo de
            ///                                     consulta se quiere ejecutar.
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 09/Noviembre/2010 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            public static DataTable Consultar_DataTable(Cls_Ope_Pre_Traslado_Negocio Traslado_Dominio){
                DataTable Dt_Traslado_Dominio = new DataTable();
                String Mi_SQL = null;
                try{
                    DataSet Ds_Traslado_Dominio = null;
                    if (Traslado_Dominio.P_Tipo_DataTable.Equals("LISTAR_CONTRARECIBOS")) {
                        Boolean Primer_Filtro = true;
                        Mi_SQL = "SELECT " + Ope_Pre_Contrarecibos.Campo_No_Contrarecibo + " AS NO_CONTRARECIBO, ";
                        Mi_SQL = Mi_SQL + Ope_Pre_Contrarecibos.Campo_Cuenta_Predial_ID + " AS CUENTA_PREDIAL_ID, ";
                        Mi_SQL = Mi_SQL + "(SELECT " + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Predial + " FROM " + Cat_Pre_Cuentas_Predial.Tabla_Cat_Pre_Cuentas + " WHERE " + Cat_Pre_Cuentas_Predial.Campo_Cuenta_Predial_ID + " = " + Ope_Pre_Contrarecibos.Tabla_Ope_Pre_Contrarecibos + "." + Ope_Pre_Contrarecibos.Campo_Cuenta_Predial_ID + ") AS CUENTA_PREDIAL, ";
                        Mi_SQL = Mi_SQL + Ope_Pre_Contrarecibos.Campo_No_Escritura + " AS NO_ESCRITURA, ";
                        Mi_SQL = Mi_SQL + Ope_Pre_Contrarecibos.Campo_Fecha_Escritura + " AS FECHA_ESCRITURA, ";
                        Mi_SQL = Mi_SQL + Ope_Pre_Contrarecibos.Campo_Fecha_Liberacion + " AS FECHA_LIBERACION, ";
                        Mi_SQL = Mi_SQL + Ope_Pre_Contrarecibos.Campo_Fecha_Pago + " AS FECHA_PAGO, ";
                        Mi_SQL = Mi_SQL + Ope_Pre_Contrarecibos.Campo_Estatus + " AS ESTATUS";
                        Mi_SQL = Mi_SQL + " FROM " + Ope_Pre_Contrarecibos.Tabla_Ope_Pre_Contrarecibos;
                        if (Traslado_Dominio.P_Cuenta_Predial_ID != null && Traslado_Dominio.P_Cuenta_Predial_ID.Trim().Length > 0)
                        {
                            Mi_SQL = Mi_SQL + " WHERE " + Ope_Pre_Contrarecibos.Campo_Cuenta_Predial_ID + " = '" + Traslado_Dominio.P_Cuenta_Predial_ID + "'";
                            Primer_Filtro = false;
                        }
                        if (Traslado_Dominio.P_No_Contrarecibo != null && Traslado_Dominio.P_No_Contrarecibo.Trim().Length > 0)
                        {
                            if (Primer_Filtro) {
                                Mi_SQL = Mi_SQL + " WHERE " + Ope_Pre_Contrarecibos.Campo_No_Contrarecibo + " = '" + Convertir_A_Formato_ID(Convert.ToInt32(Traslado_Dominio.P_No_Contrarecibo), 10) + "'";
                                Primer_Filtro = false;
                            } else {
                                Mi_SQL = Mi_SQL + " AND " + Ope_Pre_Contrarecibos.Campo_No_Contrarecibo + " = '" + Convertir_A_Formato_ID(Convert.ToInt32(Traslado_Dominio.P_No_Contrarecibo), 10) + "'";
                            }
                        }
                        if (Traslado_Dominio.P_Buscar_Fecha_Escritura) {
                            if (Primer_Filtro) {
                                Mi_SQL = Mi_SQL + " WHERE " + Ope_Pre_Contrarecibos.Campo_Fecha_Escritura + " = '" + String.Format("{0:dd/MM/yyyy}",Traslado_Dominio.P_Fecha_Escritura) + "'";
                                Primer_Filtro = false;
                            } else {
                                Mi_SQL = Mi_SQL + " AND " + Ope_Pre_Contrarecibos.Campo_Fecha_Escritura + " = '" + String.Format("{0:dd/MM/yyyy}", Traslado_Dominio.P_Fecha_Escritura) + "'";
                            }
                        }
                        if (Traslado_Dominio.P_Buscar_Fecha_Liberacion) {
                            if (Primer_Filtro) {
                                Mi_SQL = Mi_SQL + " WHERE " + Ope_Pre_Contrarecibos.Campo_Fecha_Liberacion + " = '" + String.Format("{0:dd/MM/yyyy}", Traslado_Dominio.P_Fecha_Liberacion) + "'";
                                Primer_Filtro = false;
                            } else {
                                Mi_SQL = Mi_SQL + " AND " + Ope_Pre_Contrarecibos.Campo_Fecha_Liberacion + " = '" + String.Format("{0:dd/MM/yyyy}", Traslado_Dominio.P_Fecha_Liberacion) + "'";
                            }
                        }
                        if (Traslado_Dominio.P_Listado_ID!= null && Traslado_Dominio.P_Listado_ID.Trim().Length > 0){
                            if (Primer_Filtro) {
                                Mi_SQL = Mi_SQL + " WHERE " + Ope_Pre_Contrarecibos.Campo_Listado_ID + " = '" + Convertir_A_Formato_ID(Convert.ToInt32(Traslado_Dominio.P_Listado_ID), 10) + "'";
                                Primer_Filtro = false;
                            } else {
                                Mi_SQL = Mi_SQL + " AND " + Ope_Pre_Contrarecibos.Campo_Listado_ID + " = '" + Convertir_A_Formato_ID(Convert.ToInt32(Traslado_Dominio.P_Listado_ID), 10) + "'";
                            }
                        }
                        if (Traslado_Dominio.P_Buscar_Fecha_Generacion) {
                            if (Primer_Filtro) {
                                Mi_SQL = Mi_SQL + " WHERE " + Ope_Pre_Contrarecibos.Campo_Listado_ID + " IN (SELECT " + Ope_Pre_Listados.Campo_Listado_ID + " FROM  " + Ope_Pre_Listados.Tabla_Ope_Pre_Listados;
                                Mi_SQL = Mi_SQL + " WHERE " + Ope_Pre_Listados.Campo_Fecha_Generacion + " >= '" + String.Format("{0:dd/MM/yyyy}", Traslado_Dominio.P_Fecha_Generacion) + "'";
                                Mi_SQL = Mi_SQL + " AND " + Ope_Pre_Listados.Campo_Fecha_Generacion + " < '" + String.Format("{0:dd/MM/yyyy}", (Traslado_Dominio.P_Fecha_Generacion).AddDays(1).Date) + "')";
                                Primer_Filtro = false;
                            } else {
                                Mi_SQL = Mi_SQL + " AND " + Ope_Pre_Contrarecibos.Campo_Listado_ID + " IN (SELECT " + Ope_Pre_Listados.Campo_Listado_ID + " FROM  " + Ope_Pre_Listados.Tabla_Ope_Pre_Listados;
                                Mi_SQL = Mi_SQL + " WHERE " + Ope_Pre_Listados.Campo_Fecha_Generacion + " >= '" + String.Format("{0:dd/MM/yyyy}", Traslado_Dominio.P_Fecha_Generacion) + "'";
                                Mi_SQL = Mi_SQL + " AND " + Ope_Pre_Listados.Campo_Fecha_Generacion + " < '" + String.Format("{0:dd/MM/yyyy}", (Traslado_Dominio.P_Fecha_Generacion).AddDays(1).Date) + "')";
                            }
                        }
                        if (Traslado_Dominio.P_Notario_ID != null  && Traslado_Dominio.P_Notario_ID.Trim().Length > 0) {
                            if (Primer_Filtro) {
                                Mi_SQL = Mi_SQL + " WHERE " + Ope_Pre_Contrarecibos.Campo_Notario_ID + " = '" + Traslado_Dominio.P_Notario_ID.Trim() + "'";
                                Primer_Filtro = false;
                            } else {
                                Mi_SQL = Mi_SQL + " AND " + Ope_Pre_Contrarecibos.Campo_Notario_ID + " = '" + Traslado_Dominio.P_Notario_ID.Trim() + "'";
                            }
                        }
                        if (Traslado_Dominio.P_Con_Cuenta_Predial) {
                            if (Primer_Filtro) {
                                Mi_SQL = Mi_SQL + " WHERE NOT " + Ope_Pre_Contrarecibos.Campo_Cuenta_Predial_ID + " IS NULL";
                                Primer_Filtro = false;
                            }
                            else {
                                Mi_SQL = Mi_SQL + " AND NOT " + Ope_Pre_Contrarecibos.Campo_Cuenta_Predial_ID + " IS NULL";
                            }
                        }
                        Mi_SQL = Mi_SQL + " ORDER BY " + Ope_Pre_Contrarecibos.Campo_No_Contrarecibo + " DESC ";
                    } else if(Traslado_Dominio.P_Tipo_DataTable.Equals("LISTAR_NOTARIOS")){
                        Mi_SQL = "SELECT " + Cat_Pre_Notarios.Campo_Notario_ID + " AS NOTARIO_ID, " + Cat_Pre_Notarios.Campo_Apellido_Paterno;
                        Mi_SQL = Mi_SQL + " +' '+ " + Cat_Pre_Notarios.Campo_Apellido_Materno + " +' '+ " + Cat_Pre_Notarios.Campo_Nombre + " AS NOMBRE";
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Pre_Notarios.Tabla_Cat_Pre_Notarios;
                    }
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Traslado_Dominio = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Traslado_Dominio != null) {
                        Dt_Traslado_Dominio = Ds_Traslado_Dominio.Tables[0];
                    }
                }catch(Exception Ex){
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Traslado_Dominio;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Modificar_Contrarecibo
            ///DESCRIPCIÓN: Se actualiza de un Contrarecibo la Cuenta_Predial.
            ///PARÁMETROS:     
            ///             1.  Traslado_Dominio.   Contiene las propiedades (Cuenta_Predial y 
            ///                                     No_Contrarecibo) para actualizar el contrarecibo.
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Noviembre/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            public static void Modificar_Contrarecibo(Cls_Ope_Pre_Traslado_Negocio Traslado_Dominio) {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try {
                    String Mi_SQL = "UPDATE " + Ope_Pre_Contrarecibos.Tabla_Ope_Pre_Contrarecibos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Pre_Contrarecibos.Campo_Cuenta_Predial_ID + " = '" + Traslado_Dominio.P_Cuenta_Predial_ID + "'";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Pre_Contrarecibos.Campo_No_Contrarecibo + " = '" + Traslado_Dominio.P_No_Contrarecibo + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Trans.Commit();
                } catch (SqlException Ex) {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152) {
                        Mensaje = "Existen datos demasiado extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 2627) {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    } else if (Ex.Number == 547) {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 515) {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar modificar el Registro. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }            
            }
        
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
            ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
            ///PARÁMETROS:     
            ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
            ///             2. Longitud_ID. Longitud que tendra el ID. 
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Marzo/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID) {
                String Retornar = "";
                String Dato = "" + Dato_ID;
                for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++) {
                    Retornar = Retornar + "0";
                }
                Retornar = Retornar + Dato;
                return Retornar;
            }

        #endregion

    }

    
}