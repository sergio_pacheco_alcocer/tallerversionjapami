﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Catalogo_Gastos_Ejecucion.Negocio;

/// <summary>
/// Summary description for Cls_Cat_Pre_Gastos_Ejecucion_Datos
/// </summary>

namespace JAPAMI.Catalogo_Gastos_Ejecucion.Datos{

    public class Cls_Cat_Pre_Gastos_Ejecucion_Datos {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Gasto_Ejecucion
        ///DESCRIPCIÓN: Da de alta en la Base de Datos un nuevo Gasto de Ejecucion
        ///PARAMETROS:     
        ///             1. Gasto_Ejecucion. Instancia de la Clase de Negocio de 
        ///                                     Cls_Cat_Pre_Gastos_Ejecucion_Negocio
        ///                                     con los datos del Gasto Ejecucion que 
        ///                                     va a ser dado de Alta.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 06/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void Alta_Gasto_Ejecucion(Cls_Cat_Pre_Gastos_Ejecucion_Negocio Gasto_Ejecucion){
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {
                String Gasto_Ejecucion_ID = Obtener_ID_Consecutivo(Cat_Pre_Gastos_Ejecucion.Tabla_Cat_Pre_Gastos_Ejecucion, Cat_Pre_Gastos_Ejecucion.Campo_Gasto_Ejecucion_ID, 5);
                String Mi_SQL = "INSERT INTO " + Cat_Pre_Gastos_Ejecucion.Tabla_Cat_Pre_Gastos_Ejecucion;
                Mi_SQL = Mi_SQL + " (" + Cat_Pre_Gastos_Ejecucion.Campo_Gasto_Ejecucion_ID + ", " + Cat_Pre_Gastos_Ejecucion.Campo_Identificador;
                Mi_SQL = Mi_SQL + ", " + Cat_Pre_Gastos_Ejecucion.Campo_Descripcion + ", " + Cat_Pre_Gastos_Ejecucion.Campo_Estatus;
                Mi_SQL = Mi_SQL + ", " + Cat_Pre_Gastos_Ejecucion.Campo_Usuario_Creo + ", " + Cat_Pre_Gastos_Ejecucion.Campo_Fecha_Creo + ")";
                Mi_SQL = Mi_SQL + " VALUES ('" + Gasto_Ejecucion_ID + "', '" + Gasto_Ejecucion.P_Identificador + "'";
                Mi_SQL = Mi_SQL + ",'" + Gasto_Ejecucion.P_Descripcion + "'";
                Mi_SQL = Mi_SQL + ",'" + Gasto_Ejecucion.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ",'" + Gasto_Ejecucion.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE()";
                Mi_SQL = Mi_SQL + ")";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                if (Gasto_Ejecucion.P_Gastos_Tasas != null && Gasto_Ejecucion.P_Gastos_Tasas.Rows.Count > 0){
                    String Gasto_Ejecucion_Tasa_ID = Obtener_ID_Consecutivo(Cat_Pre_Gastos_Ejec_Tasas.Tabla_Cat_Pre_Gastos_Ejec_Tasas, Cat_Pre_Gastos_Ejec_Tasas.Campo_Gasto_Ejecucion_Tasa_ID, 5);
                    for (int cnt = 0; cnt < Gasto_Ejecucion.P_Gastos_Tasas.Rows.Count; cnt++){
                        Mi_SQL = "INSERT INTO " + Cat_Pre_Gastos_Ejec_Tasas.Tabla_Cat_Pre_Gastos_Ejec_Tasas;
                        Mi_SQL = Mi_SQL + " (" + Cat_Pre_Gastos_Ejec_Tasas.Campo_Gasto_Ejecucion_Tasa_ID + ", " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Gasto_Ejecucion_ID;
                        Mi_SQL = Mi_SQL + ", " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Año + ", " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Tasa;
                        Mi_SQL = Mi_SQL + ", " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Usuario_Creo + ", " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Fecha_Creo + ")";
                        Mi_SQL = Mi_SQL + " VALUES ('" + Gasto_Ejecucion_Tasa_ID + "', '" + Gasto_Ejecucion_ID + "', " + Gasto_Ejecucion.P_Gastos_Tasas.Rows[cnt][1].ToString();
                        Mi_SQL = Mi_SQL + ", " + Gasto_Ejecucion.P_Gastos_Tasas.Rows[cnt][2].ToString();
                        Mi_SQL = Mi_SQL + ",'" + Gasto_Ejecucion.P_Usuario + "', GETDATE()";
                        Mi_SQL = Mi_SQL + ")";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        Gasto_Ejecucion_Tasa_ID = Convertir_A_Formato_ID(Convert.ToInt32(Gasto_Ejecucion_Tasa_ID) + 1, 5);
                    }
                }
                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]"; 
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]"; 
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]"; 
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]"; 
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]"; 
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]"; 
                } else {
                    Mensaje = "Error al intentar dar de Alta un Registro de Gasto de Ejecución. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }finally {
                 Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Gasto_Ejecucion
        ///DESCRIPCIÓN: Actualiza en la Base de Datos un Gasto de Ejecución
        ///PARAMETROS:     
        ///             1. Gasto_Ejecucion.     Instancia de la Clase de Negocio de Gasto de 
        ///                                     Ejecución con los datos del Gastos de Ejecución 
        ///                                     que va a ser Actualizada.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 06/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void Modificar_Gasto_Ejecucion(Cls_Cat_Pre_Gastos_Ejecucion_Negocio Gasto_Ejecucion){
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {      
                Cls_Cat_Pre_Gastos_Ejecucion_Negocio Gasto_Ejecucion_Tmp = Consultar_Datos_Gasto_Ejecucion(Gasto_Ejecucion);
                String Mi_SQL = "UPDATE " + Cat_Pre_Gastos_Ejecucion.Tabla_Cat_Pre_Gastos_Ejecucion + " SET " + Cat_Pre_Gastos_Ejecucion.Campo_Identificador + " = '" + Gasto_Ejecucion.P_Identificador + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Gastos_Ejecucion.Campo_Estatus + " = '" + Gasto_Ejecucion.P_Estatus + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Gastos_Ejecucion.Campo_Descripcion + " = '" + Gasto_Ejecucion.P_Descripcion + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Gastos_Ejecucion.Campo_Usuario_Modifico + " = '" + Gasto_Ejecucion.P_Usuario + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Gastos_Ejecucion.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Gastos_Ejecucion.Campo_Gasto_Ejecucion_ID + " = '" + Gasto_Ejecucion.P_Gastos_Ejecucion_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Gasto_Ejecucion_Tmp = Obtener_Gastos_Ejecucion_Tasas_Eliminados(Gasto_Ejecucion_Tmp, Gasto_Ejecucion);
                for (int cnt = 0; cnt < Gasto_Ejecucion_Tmp.P_Gastos_Tasas.Rows.Count; cnt++){
                    Mi_SQL = "DELETE FROM " + Cat_Pre_Gastos_Ejec_Tasas.Tabla_Cat_Pre_Gastos_Ejec_Tasas;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Gasto_Ejecucion_Tasa_ID + " = '" + Gasto_Ejecucion_Tmp.P_Gastos_Tasas.Rows[cnt][0].ToString() + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                if (Gasto_Ejecucion.P_Gastos_Tasas != null && Gasto_Ejecucion.P_Gastos_Tasas.Rows.Count > 0) {
                    String Gasto_Ejecucion_Tasa_ID = Obtener_ID_Consecutivo(Cat_Pre_Gastos_Ejec_Tasas.Tabla_Cat_Pre_Gastos_Ejec_Tasas, Cat_Pre_Gastos_Ejec_Tasas.Campo_Gasto_Ejecucion_Tasa_ID, 5);
                    for (int cnt = 0; cnt < Gasto_Ejecucion.P_Gastos_Tasas.Rows.Count; cnt++){
                        if (Gasto_Ejecucion.P_Gastos_Tasas.Rows[cnt][0].ToString().Trim().Equals("")){
                            Mi_SQL = "INSERT INTO " + Cat_Pre_Gastos_Ejec_Tasas.Tabla_Cat_Pre_Gastos_Ejec_Tasas;
                            Mi_SQL = Mi_SQL + " (" + Cat_Pre_Gastos_Ejec_Tasas.Campo_Gasto_Ejecucion_Tasa_ID + ", " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Gasto_Ejecucion_ID;
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Año + ", " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Tasa;
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Usuario_Creo + ", " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Fecha_Creo + ")";
                            Mi_SQL = Mi_SQL + " VALUES ('" + Gasto_Ejecucion_Tasa_ID + "', '" + Gasto_Ejecucion.P_Gastos_Ejecucion_ID + "', " + Gasto_Ejecucion.P_Gastos_Tasas.Rows[cnt][1].ToString();
                            Mi_SQL = Mi_SQL + ", " + Gasto_Ejecucion.P_Gastos_Tasas.Rows[cnt][2].ToString();
                            Mi_SQL = Mi_SQL + ",'" + Gasto_Ejecucion.P_Usuario + "', GETDATE()";
                            Mi_SQL = Mi_SQL + ")";
                            Gasto_Ejecucion_Tasa_ID = Convertir_A_Formato_ID(Convert.ToInt32(Gasto_Ejecucion_Tasa_ID) + 1, 5);
                        }else{
                            Mi_SQL = "UPDATE " + Cat_Pre_Gastos_Ejec_Tasas.Tabla_Cat_Pre_Gastos_Ejec_Tasas + " SET " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Año + " = " + Gasto_Ejecucion.P_Gastos_Tasas.Rows[cnt][1].ToString().Trim();
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Tasa + " = " + Gasto_Ejecucion.P_Gastos_Tasas.Rows[cnt][2].ToString().Trim();
                            Mi_SQL = Mi_SQL + ", " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Usuario_Modifico + " = '" + Gasto_Ejecucion.P_Usuario + "'" ;
                            Mi_SQL = Mi_SQL + "," + Cat_Pre_Gastos_Ejec_Tasas.Campo_Fecha_Modifico + " = GETDATE()";
                            Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Gasto_Ejecucion_Tasa_ID + " = '" + Gasto_Ejecucion.P_Gastos_Tasas.Rows[cnt][0].ToString().Trim() + "'";
                        }
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                }
                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]"; 
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]"; 
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]"; 
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]"; 
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]"; 
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]"; 
                } else {
                    Mensaje = "Error al intentar modificar un Registro de Gasto de Ejecución. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Gastos_Ejecucion_Tasas_Eliminados
        ///DESCRIPCIÓN: Obtiene la lista de los Gasto de Ejecución Tasas que fueron dados de alta en la Actualizacion de las
        ///             Gasto de Ejecución
        ///PARAMETROS:     
        ///             1. Actuales.        Gasto de Ejecución que se usa para saber los Gastos de Ejecución Tasas que estan en 
        ///                                 la Base de Datos antes de la Modificacion.
        ///             2. Actualizados.    Gasto de Ejecución que se usa para saber los Gastos de Ejecución Tasas que estaran en 
        ///                                 la Base de Datos despues de la Modificacion.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 06/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private static Cls_Cat_Pre_Gastos_Ejecucion_Negocio Obtener_Gastos_Ejecucion_Tasas_Eliminados(Cls_Cat_Pre_Gastos_Ejecucion_Negocio Actuales, Cls_Cat_Pre_Gastos_Ejecucion_Negocio Actualizados){
            Cls_Cat_Pre_Gastos_Ejecucion_Negocio Gasto_Ejecucion = new Cls_Cat_Pre_Gastos_Ejecucion_Negocio();
            DataTable tabla = new DataTable();
            tabla.Columns.Add("GASTO_EJECUCION_TASA_ID", Type.GetType("System.String"));
            tabla.Columns.Add("ANIO", Type.GetType("System.String"));
            tabla.Columns.Add("TASA", Type.GetType("System.String"));
            for (int cnt1 = 0; cnt1 < Actuales.P_Gastos_Tasas.Rows.Count; cnt1++){
                bool eliminar = true;
                for (int cnt2 = 0; cnt2 < Actualizados.P_Gastos_Tasas.Rows.Count; cnt2++) {
                    if (!Actualizados.P_Gastos_Tasas.Rows[cnt2][0].ToString().Equals("")) {
                        if (Actuales.P_Gastos_Tasas.Rows[cnt1][0].ToString().Equals(Actualizados.P_Gastos_Tasas.Rows[cnt2][0].ToString())){
                            eliminar = false;
                            break;
                        }
                    }
                }
                if (eliminar) {
                    DataRow fila = tabla.NewRow();
                    fila["GASTO_EJECUCION_TASA_ID"] = Actuales.P_Gastos_Tasas.Rows[cnt1][0].ToString();
                    fila["ANIO"] = Actuales.P_Gastos_Tasas.Rows[cnt1][1].ToString();
                    fila["TASA"] = Actuales.P_Gastos_Tasas.Rows[cnt1][2].ToString();
                    tabla.Rows.Add(fila);
                }
            }
            Gasto_Ejecucion.P_Gastos_Tasas = tabla;
            return Gasto_Ejecucion;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Gastos_Ejecucion
        ///DESCRIPCIÓN: Obtiene todas los Gasto de Ejecucion que estan dadas de alta en 
        ///             la Base de Datos
        ///PARAMETROS:   
        ///             1.  Gasto_Ejecucion.    Parametro de donde se sacara si habra o 
        ///                                     no un filtro de busqueda, en este caso 
        ///                                     el filtro es el Identificador.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 06/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Gastos_Ejecucion(Cls_Cat_Pre_Gastos_Ejecucion_Negocio Gasto_Ejecucion){
            DataTable Tabla = new DataTable();
            try {                
                String Mi_SQL = "SELECT " + Cat_Pre_Gastos_Ejecucion.Campo_Gasto_Ejecucion_ID + " AS GASTO_EJECUCION_ID, " + Cat_Pre_Gastos_Ejecucion.Campo_Identificador + " AS IDENTIFICADOR";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Gastos_Ejecucion.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Pre_Gastos_Ejecucion.Tabla_Cat_Pre_Gastos_Ejecucion;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Gastos_Ejecucion.Campo_Identificador + " LIKE '%" + Gasto_Ejecucion.P_Identificador + "%' ";
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Pre_Gastos_Ejecucion.Campo_Gasto_Ejecucion_ID;
                DataSet dataset = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataset != null) {
                    Tabla = dataset.Tables[0];
                }
            }catch(Exception Ex){
                String Mensaje = "Error al intentar consultar los registros de Gasto de Ejecución. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Tabla;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Datos_Gasto_Ejecucion
        ///DESCRIPCIÓN: Obtiene a detalle una Gasto de Ejecución.
        ///PARAMETROS:   
        ///             1. P_Gasto_Ejecucion.    Gasto de Ejecución que se va ver a Detalle.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 06/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static Cls_Cat_Pre_Gastos_Ejecucion_Negocio Consultar_Datos_Gasto_Ejecucion(Cls_Cat_Pre_Gastos_Ejecucion_Negocio P_Gasto_Ejecucion){
            String Mi_SQL = "SELECT " + Cat_Pre_Gastos_Ejecucion.Campo_Identificador + ", " + Cat_Pre_Gastos_Ejecucion.Campo_Estatus + ", " + Cat_Pre_Gastos_Ejecucion.Campo_Descripcion;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Pre_Gastos_Ejecucion.Tabla_Cat_Pre_Gastos_Ejecucion;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Gastos_Ejecucion.Campo_Gasto_Ejecucion_ID + " = '" + P_Gasto_Ejecucion.P_Gastos_Ejecucion_ID+ "'";
            Cls_Cat_Pre_Gastos_Ejecucion_Negocio R_Gasto_Ejecucion = new Cls_Cat_Pre_Gastos_Ejecucion_Negocio();
            SqlDataReader Data_Reader;
            try{
                Data_Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                R_Gasto_Ejecucion.P_Gastos_Ejecucion_ID = P_Gasto_Ejecucion.P_Gastos_Ejecucion_ID;
                while (Data_Reader.Read()){
                    R_Gasto_Ejecucion.P_Identificador = Data_Reader[Cat_Pre_Gastos_Ejecucion.Campo_Identificador].ToString();
                    R_Gasto_Ejecucion.P_Estatus = Data_Reader[Cat_Pre_Gastos_Ejecucion.Campo_Estatus].ToString();
                    R_Gasto_Ejecucion.P_Descripcion = Data_Reader[Cat_Pre_Gastos_Ejecucion.Campo_Descripcion].ToString();
                }
                Data_Reader.Close();
                Mi_SQL = "SELECT " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Gasto_Ejecucion_Tasa_ID + " AS GASTO_EJECUCION_TASA_ID, " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Año + " AS ANIO";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Gastos_Ejec_Tasas.Campo_Tasa + " AS TASA";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Pre_Gastos_Ejec_Tasas.Tabla_Cat_Pre_Gastos_Ejec_Tasas;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Gasto_Ejecucion_ID + " = '" + P_Gasto_Ejecucion.P_Gastos_Ejecucion_ID + "'";
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Gasto_Ejecucion_Tasa_ID;
                DataSet dataset = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataset == null){
                    R_Gasto_Ejecucion.P_Gastos_Tasas = new DataTable();
                }else{
                    R_Gasto_Ejecucion.P_Gastos_Tasas = dataset.Tables[0];
                }
            }catch (Exception Ex){
                String Mensaje = "Error al intentar consultar el registro de Gasto de Ejecución. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return R_Gasto_Ejecucion;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Eliminar_Gasto_Ejecucion
        ///DESCRIPCIÓN: Elimina un Gasto Ejecucion
        ///PARAMETROS:   
        ///             1. Gasto_Ejecucion.   Gasto de Ejecucion que se va eliminar.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 06/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void Eliminar_Gasto_Ejecucion(Cls_Cat_Pre_Gastos_Ejecucion_Negocio Gasto_Ejecucion){
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {                   
                String Mi_SQL = "DELETE FROM " + Cat_Pre_Gastos_Ejec_Tasas.Tabla_Cat_Pre_Gastos_Ejec_Tasas;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Gastos_Ejec_Tasas.Campo_Gasto_Ejecucion_ID + " = '" + Gasto_Ejecucion.P_Gastos_Ejecucion_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Mi_SQL = "DELETE FROM " + Cat_Pre_Gastos_Ejecucion.Tabla_Cat_Pre_Gastos_Ejecucion;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Gastos_Ejecucion.Campo_Gasto_Ejecucion_ID + " = '" + Gasto_Ejecucion.P_Gastos_Ejecucion_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
            } catch (SqlException Ex) {
                if (Ex.Number == 547){
                    Mensaje = "No se puede eliminar el registro, ya que está relacionado con datos. Error: [" + Ex.Message + "]"; 
                } else {
                    Mensaje = "Error al intentar eliminar el registro de Gasto de Ejecución. Error: [" + Ex.Message + "]"; 
                }
                throw new Exception(Mensaje);
            } catch (Exception Ex) {
                Mensaje = "Error al intentar eliminar el registro de Gasto de Ejecución. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARÁMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID) {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals("")) {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            } catch (SqlException Ex) {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
        ///PARÁMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID) {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++) {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }

    }

}