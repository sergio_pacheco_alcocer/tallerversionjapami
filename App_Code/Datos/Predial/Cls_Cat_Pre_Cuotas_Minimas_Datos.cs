﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Catalogo_Cuotas_Minimas.Negocio;

/// <summary>
/// Summary description for Cls_Cat_Pre_Cuotas_Minimas_Datos
/// </summary>
/// 

namespace JAPAMI.Catalogo_Cuotas_Minimas.Datos{
    public class Cls_Cat_Pre_Cuotas_Minimas_Datos
    {
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Cuota_Minima
        ///DESCRIPCIÓN: Da de alta en la Base de Datos una nueva Cuota Minima
        ///PARAMETROS:     
        ///             1. Cuota_Minima.    Instancia de la Clase de Negocio de Cls_Cat_Pre_Cuotas_Minimas_Negocio
        ///                                 con los datos de la Cuota Minima que va a ser dada de Alta.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 31/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void Alta_Cuota_Minima(Cls_Cat_Pre_Cuotas_Minimas_Negocio Cuota_Minima){
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {
                String Cuota_Minima_ID = Obtener_ID_Consecutivo(Cat_Pre_Cuotas_Minimas.Tabla_Cat_Pre_Cuotas_Minimas, Cat_Pre_Cuotas_Minimas.Campo_Cuota_Minima_ID, 5);
                String Mi_SQL = "INSERT INTO " + Cat_Pre_Cuotas_Minimas.Tabla_Cat_Pre_Cuotas_Minimas;
                Mi_SQL = Mi_SQL + " (" + Cat_Pre_Cuotas_Minimas.Campo_Cuota_Minima_ID + ", " + Cat_Pre_Cuotas_Minimas.Campo_Año;
                Mi_SQL = Mi_SQL + ", " + Cat_Pre_Cuotas_Minimas.Campo_Cuota;
                Mi_SQL = Mi_SQL + ", " + Cat_Pre_Cuotas_Minimas.Campo_Usuario_Creo + ", " + Cat_Pre_Cuotas_Minimas.Campo_Fecha_Creo + ")";
                Mi_SQL = Mi_SQL + " VALUES ('" + Cuota_Minima_ID + "', " + Cuota_Minima.P_Anio;
                Mi_SQL = Mi_SQL + ", " + Cuota_Minima.P_Cuota;
                Mi_SQL = Mi_SQL + ",'" + Cuota_Minima.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE()";
                Mi_SQL = Mi_SQL + ")";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar dar de Alta un Registro de Cuota Minima. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }finally {
                if (Cn.State == ConnectionState.Open) {
                    Cn.Close();
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Cuota_Minima
        ///DESCRIPCIÓN: Actualiza en la Base de Datos una Cuota Minima
        ///PARAMETROS:     
        ///             1. Cuota_Minima.  Instancia de la Clase de Negocio de Cuotas Minimas con los datos 
        ///                              del Registro que va a ser Actualizado.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 31/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void Modificar_Cuota_Minima(Cls_Cat_Pre_Cuotas_Minimas_Negocio Cuota_Minima){
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {               
                String Mi_SQL = "UPDATE " + Cat_Pre_Cuotas_Minimas.Tabla_Cat_Pre_Cuotas_Minimas + " SET " + Cat_Pre_Cuotas_Minimas.Campo_Año + " = " + Cuota_Minima.P_Anio;
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Cuotas_Minimas.Campo_Cuota + " = " + Cuota_Minima.P_Cuota;
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Cuotas_Minimas.Campo_Usuario_Modifico + " = '" + Cuota_Minima.P_Usuario + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Pre_Cuotas_Minimas.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Cuotas_Minimas.Campo_Cuota_Minima_ID + " = '" + Cuota_Minima.P_Cuota_Minima_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar modificar un Registro de Cuota Minima. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Cuotas_Minimas
        ///DESCRIPCIÓN: Obtiene todas las Cuotas Minimas que estan dadas de alta en la Base de Datos
        ///PARAMETROS:   
        ///             1.  Cuota_Minima.   Parametro de donde se sacara si habra o no un filtro de busqueda, en este
        ///                                 caso el filtro es el año.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 31/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Cuotas_Minimas(Cls_Cat_Pre_Cuotas_Minimas_Negocio Cuota_Minima){
            DataTable Tabla = new DataTable();
            try{
                String Mi_SQL = "SELECT " + Cat_Pre_Cuotas_Minimas.Campo_Cuota_Minima_ID + " AS CUOTA_MINIMA_ID, " + Cat_Pre_Cuotas_Minimas.Campo_Año + " AS ANIO";
                Mi_SQL = Mi_SQL + ","+ Cat_Pre_Cuotas_Minimas.Campo_Cuota +" AS CUOTA";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Pre_Cuotas_Minimas.Tabla_Cat_Pre_Cuotas_Minimas;
                if (!Cuota_Minima.P_Anio.Trim().Equals("")) {
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Cuotas_Minimas.Campo_Año + " = " + Cuota_Minima.P_Anio;        
                }
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Pre_Cuotas_Minimas.Campo_Cuota_Minima_ID;
                DataSet dataset = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataset != null) {
                    Tabla = dataset.Tables[0];
                }
            }catch(Exception Ex){
                String Mensaje = "Error al intentar consultar los registros de Cuotas Minimas. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Tabla;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Datos_Cuota_Minima
        ///DESCRIPCIÓN: Obtiene a detalle una Cuota Minima.
        ///PARAMETROS:   
        ///             1. P_Cuota_Minima.   Cuota Minima que se va ver a Detalle.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 31/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static Cls_Cat_Pre_Cuotas_Minimas_Negocio Consultar_Datos_Cuota_Minima(Cls_Cat_Pre_Cuotas_Minimas_Negocio P_Cuota_Minima){
            String Mi_SQL = "SELECT " + Cat_Pre_Cuotas_Minimas.Campo_Año + ", " + Cat_Pre_Cuotas_Minimas.Campo_Cuota;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Pre_Cuotas_Minimas.Tabla_Cat_Pre_Cuotas_Minimas;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Cuotas_Minimas.Campo_Cuota_Minima_ID + " = '" + P_Cuota_Minima.P_Cuota_Minima_ID + "'";
            Cls_Cat_Pre_Cuotas_Minimas_Negocio R_Cuota_Minima = new Cls_Cat_Pre_Cuotas_Minimas_Negocio();
            SqlDataReader Data_Reader;
            try{
                Data_Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                R_Cuota_Minima.P_Cuota_Minima_ID = P_Cuota_Minima.P_Cuota_Minima_ID;
                while (Data_Reader.Read()){
                    R_Cuota_Minima.P_Anio = Data_Reader[Cat_Pre_Cuotas_Minimas.Campo_Año].ToString();
                    R_Cuota_Minima.P_Cuota = Convert.ToDouble(Data_Reader[Cat_Pre_Cuotas_Minimas.Campo_Cuota]);
                }
                Data_Reader.Close();
            }catch (Exception Ex){
                String Mensaje = "Error al intentar consultar el registro de Cuota Minima. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return R_Cuota_Minima;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Eliminar_Cuota_Minima
        ///DESCRIPCIÓN: Elimina una Cuota Minima de la Base de Datos.
        ///PARAMETROS:   
        ///             1. Cuota_Minima.   Registro que se va a eliminar.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 31/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static void Eliminar_Cuota_Minima(Cls_Cat_Pre_Cuotas_Minimas_Negocio Cuota_Minima){
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {               
                String Mi_SQL = "DELETE FROM " + Cat_Pre_Cuotas_Minimas.Tabla_Cat_Pre_Cuotas_Minimas;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Pre_Cuotas_Minimas.Campo_Cuota_Minima_ID + " = '" + Cuota_Minima.P_Cuota_Minima_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
            } catch (SqlException Ex) {
                if (Ex.Number == 547){
                    Mensaje = "No se puede eliminar el registro, ya que está relacionado con datos. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar eliminar el registro de Cuota Minima. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje);
            } catch (Exception Ex) {
                Mensaje = "Error al intentar eliminar el registro de Cuota Minima. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARÁMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID) {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals("")) {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            } catch (SqlException Ex) {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
        ///PARÁMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID) {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++) {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }

    }
}