﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Multialmacenes.Negocio;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;


/// <summary>
/// Summary description for Cls_Cat_Alm_Multialmacenes_Datos
/// </summary>
namespace JAPAMI.Multialmacenes.Datos
{
    public class Cls_Cat_Alm_Multialmacenes_Datos
    {

        ///*******************************************************************************
        /// METODOS
        ///*******************************************************************************
        #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Multialmacenes
        ///DESCRIPCIÓN: Metodo que consulta los multialmacenes
        ///PARAMETROS: 1.- Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocios objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 6/Diciembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Multialmacenes(Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocios)
        {
            String Mi_SQL = "SELECT " + Cat_Alm_Multialmacenes.Tabla_Cat_Alm_Multialmacenes + "." + Cat_Alm_Multialmacenes.Campo_Almacen_ID;
            Mi_SQL = Mi_SQL + ", " + Cat_Alm_Multialmacenes.Tabla_Cat_Alm_Multialmacenes + "." + Cat_Alm_Multialmacenes.Campo_Nombre;
            Mi_SQL = Mi_SQL + ", " + Cat_Alm_Multialmacenes.Tabla_Cat_Alm_Multialmacenes + "." + Cat_Alm_Multialmacenes.Campo_Descripcion;
            Mi_SQL = Mi_SQL + ", " + Cat_Alm_Multialmacenes.Tabla_Cat_Alm_Multialmacenes + "." + Cat_Alm_Multialmacenes.Campo_Responsable_ID;
            Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Empleados.Campo_Nombre;
            Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno;
            Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Apellido_Materno;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Empleados.Campo_Empleado_ID;
            Mi_SQL = Mi_SQL + " =" + Cat_Alm_Multialmacenes.Tabla_Cat_Alm_Multialmacenes;
            Mi_SQL = Mi_SQL + "." + Cat_Alm_Multialmacenes.Campo_Responsable_ID + ") RESPONSABLE";
            Mi_SQL = Mi_SQL + " FROM " + Cat_Alm_Multialmacenes.Tabla_Cat_Alm_Multialmacenes;

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Empleado
        ///DESCRIPCIÓN: Metodo que consulta los empleados 
        ///PARAMETROS: 1.- Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocios objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 6/Diciembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Empleado(Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocios)
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT " + Cat_Empleados.Campo_Empleado_ID;
            Mi_SQL = Mi_SQL + ", " + Cat_Empleados.Campo_No_Empleado;
            Mi_SQL = Mi_SQL + ", " + Cat_Empleados.Campo_Nombre;
            Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno;
            Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Apellido_Materno  + " AS NOMBRE";
            Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Empleados.Campo_No_Empleado + " IS NOT NULL";
            Mi_SQL = Mi_SQL + " AND " + Cat_Empleados.Campo_Estatus + " ='ACTIVO' ";

            if (Clase_Negocios.P_No_Empleado != null)
            {

                Mi_SQL = Mi_SQL + " AND " + Cat_Empleados.Campo_No_Empleado;
                Mi_SQL = Mi_SQL + "='" + Clase_Negocios.P_No_Empleado.Trim() + "'";
            }

            if (Clase_Negocios.P_Nombre_Empleado != null)
            {
                Mi_SQL = Mi_SQL + " AND UPPER(" + Cat_Empleados.Campo_Nombre;
                Mi_SQL = Mi_SQL + ") LIKE UPPER('%" +Clase_Negocios.P_Nombre_Empleado.Trim() +"%')";
            }

            if (Clase_Negocios.P_Apellido_Paterno != null)
            {
                Mi_SQL = Mi_SQL + " AND UPPER(" + Cat_Empleados.Campo_Apellido_Paterno;
                Mi_SQL = Mi_SQL + ") LIKE UPPER('%" + Clase_Negocios.P_Apellido_Paterno.Trim() + "%')";
            }

            if (Clase_Negocios.P_Apellido_Materno != null)
            {
                Mi_SQL = Mi_SQL + " AND UPPER(" + Cat_Empleados.Campo_Apellido_Materno;
                Mi_SQL = Mi_SQL + ") LIKE UPPER('%" + Clase_Negocios.P_Apellido_Materno.Trim()+"%')";
            }

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Multialmacen
        ///DESCRIPCIÓN: Metodo que da de alta un Almacen 
        ///PARAMETROS: 1.- Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocios objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 6/Diciembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Alta_Multialmacen(Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocios)
        {
            String Mi_SQL ="";
            String Msj="";
            try{
            int Almacen_ID = Obtener_Consecutivo(Cat_Alm_Multialmacenes.Campo_Almacen_ID, Cat_Alm_Multialmacenes.Tabla_Cat_Alm_Multialmacenes);
            Mi_SQL = "INSERT INTO " + Cat_Alm_Multialmacenes.Tabla_Cat_Alm_Multialmacenes;
            Mi_SQL = Mi_SQL + " (" + Cat_Alm_Multialmacenes.Campo_Almacen_ID;
            Mi_SQL = Mi_SQL + ", " + Cat_Alm_Multialmacenes.Campo_Nombre;
            Mi_SQL = Mi_SQL + ", " + Cat_Alm_Multialmacenes.Campo_Descripcion;
            Mi_SQL = Mi_SQL + ", " + Cat_Alm_Multialmacenes.Campo_Fecha_Creo;
            Mi_SQL = Mi_SQL + ", " + Cat_Alm_Multialmacenes.Campo_Usuario_Creo;
            Mi_SQL = Mi_SQL + ", " + Cat_Alm_Multialmacenes.Campo_Responsable_ID;
            Mi_SQL = Mi_SQL + ") VALUES(" + Almacen_ID;
            Mi_SQL = Mi_SQL + ",'" + Clase_Negocios.P_Nombre.Trim() + "'";
            Mi_SQL = Mi_SQL + ",'" + Clase_Negocios.P_Descripcion.Trim() + "'";
            Mi_SQL = Mi_SQL + ",GETDATE()";
            Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
            Mi_SQL = Mi_SQL + ",'" + Clase_Negocios.P_Responsable_ID.Trim() +"')";

            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            Msj = "Se creo satisfactoriamente el almacen " + Almacen_ID;
            }catch(Exception EX)
            {
                Msj = "No se pudo guardar el registro: " +EX.Message;
            }
            return Msj;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Multialmacen
        ///DESCRIPCIÓN: Metodo que da de alta un Almacen 
        ///PARAMETROS: 1.- Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocios objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 6/Diciembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Modificar_Multialmacenes(Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocios)
        {

            String Msj = "";
            String Mi_SQL = "";
            try
            {
            Mi_SQL = "UPDATE " + Cat_Alm_Multialmacenes.Tabla_Cat_Alm_Multialmacenes;
            Mi_SQL = Mi_SQL + " SET " + Cat_Alm_Multialmacenes.Campo_Nombre;
            Mi_SQL = Mi_SQL + " ='" + Clase_Negocios.P_Nombre.Trim() + "'";
            Mi_SQL = Mi_SQL + ", " + Cat_Alm_Multialmacenes.Campo_Descripcion;
            Mi_SQL = Mi_SQL + "='" + Clase_Negocios.P_Descripcion.Trim() + "'";
            Mi_SQL = Mi_SQL + ", " + Cat_Alm_Multialmacenes.Campo_Responsable_ID;
            Mi_SQL = Mi_SQL + "='" + Clase_Negocios.P_Responsable_ID + "'";
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Alm_Multialmacenes.Campo_Almacen_ID;
            Mi_SQL = Mi_SQL + "='" + Clase_Negocios.P_Almacen_ID +"'";

            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            Msj = "Se Modifico satisfactoriamente el almacen " + Clase_Negocios.P_Almacen_ID;
            }catch(Exception EX)
            {
                Msj = "No se pudo guardar el registro: " +EX.Message;
            }

            return Msj;
        }
    
                ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-P_Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }

        #endregion


    }
}//Fin del namespace 