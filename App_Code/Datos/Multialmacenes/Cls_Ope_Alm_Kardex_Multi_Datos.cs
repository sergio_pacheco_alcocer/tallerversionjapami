﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Kardex_Multialmacen.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Alm_Kardex_Multi_Datos
/// </summary>
namespace JAPAMI.Kardex_Multialmacen.Datos
{
    public class Cls_Ope_Alm_Kardex_Multi_Datos
    {

        public static DataTable Consultar_Almacenes()
        {
            String Mi_SQL = "SELECT " + Cat_Alm_Multialmacenes.Campo_Almacen_ID;
            Mi_SQL = Mi_SQL + ", " + Cat_Alm_Multialmacenes.Campo_Nombre;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Alm_Multialmacenes.Tabla_Cat_Alm_Multialmacenes;
            Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Alm_Multialmacenes.Campo_Nombre;

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        public static DataTable Consultar_Productos(Cls_Ope_Alm_Kardex_Multi_Negocio Clase_Negocios)
        {
            String Mi_SQL = "";
            Mi_SQL = Mi_SQL + "SELECT " +Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Clave;
            Mi_SQL = Mi_SQL + ", " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Producto_ID;
            Mi_SQL = Mi_SQL + ", " +Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Nombre;
            Mi_SQL = Mi_SQL + ", " +Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Descripcion;
            Mi_SQL = Mi_SQL + ", " +Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Estatus;
            Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Com_Unidades.Campo_Nombre;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID;
            Mi_SQL = Mi_SQL + " =" +Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Unidad_ID + ") AS UNIDAD";
            Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Com_Marcas.Campo_Nombre;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Marcas.Campo_Marca_ID;
            Mi_SQL = Mi_SQL + " = " +Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Marca_ID + ") AS MARCA";
            Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Com_Modelos.Campo_Nombre;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Modelos.Tabla_Cat_Com_Modelos;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Modelos.Campo_Modelo_ID;
            Mi_SQL = Mi_SQL + "= " +Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Modelo + ") AS MODELO";
            Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
            Mi_SQL = Mi_SQL + " JOIN " + Ope_Alm_Productos_Multi.Tabla_Ope_Alm_Productos_Multi;
            Mi_SQL = Mi_SQL + " ON " + Ope_Alm_Productos_Multi.Tabla_Ope_Alm_Productos_Multi + "." + Ope_Alm_Productos_Multi.Campo_Producto_ID;
            Mi_SQL = Mi_SQL + " = " +Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Producto_ID;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Alm_Productos_Multi.Tabla_Ope_Alm_Productos_Multi + "." + Ope_Alm_Productos_Multi.Campo_Almacen_ID;
            Mi_SQL = Mi_SQL + "=" + Clase_Negocios.P_Almacen_ID.Trim();

            //Verificar si se tiene que consultar a todos los productos
            if (Clase_Negocios.P_Todos_Productos == false)
            {
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Clave;
                Mi_SQL = Mi_SQL + "=" + Clase_Negocios.P_Clave;
            }

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        public static DataTable Consultar_Traspasos(Cls_Ope_Alm_Kardex_Multi_Negocio Clase_Negocios)
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT " + Ope_Alm_Traspasos.Campo_Fecha_Creo;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Traspasos.Campo_Cantidad;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Traspasos.Campo_No_Traspaso;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Traspasos.Campo_No_Requisicion;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Traspasos.Campo_Producto_ID;
            Mi_SQL = Mi_SQL + " FROM " + Ope_Alm_Traspasos.Tabla_Ope_Alm_Traspasos;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Alm_Traspasos.Campo_Almacen_ID;
            Mi_SQL = Mi_SQL + "=" + Clase_Negocios.P_Almacen_ID;
            if (Clase_Negocios.P_Clave != null)
            {
                Mi_SQL = Mi_SQL + " AND " + Ope_Alm_Traspasos.Campo_Producto_ID;
                Mi_SQL = Mi_SQL + " =(SELECT " + Cat_Com_Productos.Campo_Producto_ID;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Productos.Campo_Clave;
                Mi_SQL = Mi_SQL + "='" + Clase_Negocios.P_Clave + "')";
            }
            Mi_SQL = Mi_SQL + " AND " + Ope_Alm_Traspasos.Campo_Fecha_Creo;
            Mi_SQL = Mi_SQL + " BETWEEN '" + Clase_Negocios.P_Fecha_Inicio + "' ";
            Mi_SQL = Mi_SQL + " AND '" + Clase_Negocios.P_Fecha_Final + "' ";
            Mi_SQL = Mi_SQL + " ORDER BY " + Ope_Alm_Traspasos.Campo_Fecha_Creo;

            
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        public static DataTable Consultar_Salidas(Cls_Ope_Alm_Kardex_Multi_Negocio Clase_Negocios)
        {
            String Mi_SQL = "SELECT " + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi + "." + Ope_Alm_Salidas_Multi.Campo_Fecha_Hora;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Det_Multi.Tabla_Ope_Alm_Salidas_Det_Multi + "." + Ope_Alm_Salidas_Det_Multi.Campo_Producto_ID;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Det_Multi.Tabla_Ope_Alm_Salidas_Det_Multi + "." + Ope_Alm_Salidas_Det_Multi.Campo_Cantidad;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Det_Multi.Tabla_Ope_Alm_Salidas_Det_Multi + "." + Ope_Alm_Salidas_Det_Multi.Campo_No_Salida;
            Mi_SQL = Mi_SQL + " FROM " + Ope_Alm_Salidas_Det_Multi.Tabla_Ope_Alm_Salidas_Det_Multi;
            Mi_SQL = Mi_SQL + " JOIN " + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi;
            Mi_SQL = Mi_SQL + " ON " + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi + "." + Ope_Alm_Salidas_Multi.Campo_No_Salida;
            Mi_SQL = Mi_SQL + "=" + Ope_Alm_Salidas_Det_Multi.Tabla_Ope_Alm_Salidas_Det_Multi + "." + Ope_Alm_Salidas_Det_Multi.Campo_No_Salida;
                 
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi + "." + Ope_Alm_Salidas_Multi.Campo_Almacen_ID;
            Mi_SQL = Mi_SQL + "=" + Clase_Negocios.P_Almacen_ID;
            if (Clase_Negocios.P_Clave != null)
            {
                Mi_SQL = Mi_SQL + " AND " + Ope_Alm_Salidas_Det_Multi.Tabla_Ope_Alm_Salidas_Det_Multi + "." + Ope_Alm_Salidas_Det_Multi.Campo_Producto_ID;
                Mi_SQL = Mi_SQL + "=(SELECT " + Cat_Com_Productos.Campo_Producto_ID;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Productos.Campo_Clave;
                Mi_SQL = Mi_SQL + "='" + Clase_Negocios.P_Clave + "')";
            }
            Mi_SQL = Mi_SQL + " AND " + Ope_Alm_Salidas_Det_Multi.Tabla_Ope_Alm_Salidas_Det_Multi + "." + Ope_Alm_Salidas_Det_Multi.Campo_Fecha_Creo;
            Mi_SQL = Mi_SQL + " BETWEEN '" + Clase_Negocios.P_Fecha_Inicio + "' ";
            Mi_SQL = Mi_SQL + " AND '" +  Clase_Negocios.P_Fecha_Final + "' ";
            Mi_SQL = Mi_SQL + " ORDER BY " + Ope_Alm_Salidas_Det_Multi.Tabla_Ope_Alm_Salidas_Det_Multi + "." + Ope_Alm_Salidas_Det_Multi.Campo_Fecha_Creo;


            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }



    }
}