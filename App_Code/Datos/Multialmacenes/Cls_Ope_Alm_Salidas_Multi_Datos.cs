﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Salidas_Multialmacenes.Negocios;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;

/// <summary>
/// Summary description for Cls_Ope_Alm_Salidas_Multi_Datos
/// </summary>
/// 
namespace JAPAMI.Salidas_Multialmacenes.Datos
{

    public class Cls_Ope_Alm_Salidas_Multi_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Salidas
        ///DESCRIPCIÓN: Metodo que consulta las salida existentes
        ///PARAMETROS: 1.- Cls_Ope_Alm_Salidas_Multi_Negocio Clase_Negocios objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 22/Diciembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Salidas(Cls_Ope_Alm_Salidas_Multi_Negocio Clase_Negocios)
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT " + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi + "." + Ope_Alm_Salidas_Multi.Campo_No_Salida;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi + "." + Ope_Alm_Salidas_Multi.Campo_Fecha_Hora;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi + "." + Ope_Alm_Salidas_Multi.Campo_IVA;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi + "." + Ope_Alm_Salidas_Multi.Campo_Total;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi + "." + Ope_Alm_Salidas_Multi.Campo_Subtotal;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi + "." + Ope_Alm_Salidas_Multi.Campo_Empleado_Recibio_ID;
            Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Empleados.Campo_Nombre;
            Mi_SQL = Mi_SQL + " +' '+ " + Cat_Empleados.Campo_Apellido_Paterno;
            Mi_SQL = Mi_SQL + " +' '+ " + Cat_Empleados.Campo_Apellido_Materno;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Empleados.Campo_Empleado_ID;
            Mi_SQL = Mi_SQL + "=" + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi + "." + Ope_Alm_Salidas_Multi.Campo_Empleado_Recibio_ID + ") AS EMPLEADO_RECIBIO" ;
            Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Empleados.Campo_Nombre;
            Mi_SQL = Mi_SQL + " +' '+ " + Cat_Empleados.Campo_Apellido_Paterno;
            Mi_SQL = Mi_SQL + " +' '+ " + Cat_Empleados.Campo_Apellido_Materno;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Empleados.Campo_Empleado_ID;
            Mi_SQL = Mi_SQL + "=" + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi + "." + Ope_Alm_Salidas_Multi.Campo_Empleado_Almacen_ID + ") AS EMPLEADO_ALMACEN";
            Mi_SQL = Mi_SQL + " FROM " + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi + "." + Ope_Alm_Salidas_Multi.Campo_Almacen_ID ;
            Mi_SQL = Mi_SQL + " =" + Clase_Negocios.P_Almacen_ID;
            Mi_SQL = Mi_SQL + " OR (" + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi + "." + Ope_Alm_Salidas_Multi.Campo_Empleado_Almacen_ID;
            Mi_SQL = Mi_SQL + " = '" + Cls_Sessiones.Empleado_ID + "')";

            if (Clase_Negocios.P_No_Salida != null)
            {
                Mi_SQL = Mi_SQL + " AND " + Ope_Alm_Salidas_Multi.Campo_No_Salida;
                Mi_SQL = Mi_SQL + "='" + Clase_Negocios.P_No_Salida.Trim() +"'";
            }
            Mi_SQL = Mi_SQL + " ORDER BY " + Ope_Alm_Salidas_Multi.Campo_Fecha_Creo + " DESC ";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Detalle_Salidas
        ///DESCRIPCIÓN: Metodo que consulta las salida existentes
        ///PARAMETROS: 1.- Cls_Ope_Alm_Salidas_Multi_Negocio Clase_Negocios objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 22/Diciembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Detalle_Salidas(Cls_Ope_Alm_Salidas_Multi_Negocio Clase_Negocios)
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID +
                         ", PRODUCTOS." + Cat_Com_Productos.Campo_Clave +
                         ", PRODUCTOS." + Cat_Com_Productos.Campo_Nombre + " AS PRODUCTO_NOMBRE" +
                         ", PRODUCTOS." + Cat_Com_Productos.Campo_Descripcion +
                         ", NULL AS EXISTENCIA" +
                         ", (SELECT " + Cat_Com_Unidades.Campo_Nombre +
                         " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades +
                         " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID +
                         "=PRODUCTOS." + Cat_Com_Productos.Campo_Unidad_ID + ") AS UNIDAD" +
                         ", MULTIALMACENES." + Ope_Alm_Salidas_Det_Multi.Campo_Cantidad +
                         ", MULTIALMACENES." + Ope_Alm_Salidas_Det_Multi.Campo_Total + " AS IMPORTE" +
                         ", MULTIALMACENES." + Ope_Alm_Salidas_Det_Multi.Campo_IVA + " AS Monto_IVA" +
                         ", PRODUCTOS." + Cat_Com_Productos.Campo_Reorden +
                         ", PRODUCTOS." + Cat_Com_Productos.Campo_Costo + " AS PRECIO_UNITARIO" +
                         " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS" +
                         " JOIN " + Cat_Com_Impuestos.Tabla_Cat_Impuestos + " IMPUESTOS" +
                         " ON IMPUESTOS." + Cat_Com_Impuestos.Campo_Impuesto_ID + "= PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_ID +
                         " JOIN " + Ope_Alm_Salidas_Det_Multi.Tabla_Ope_Alm_Salidas_Det_Multi + " MULTIALMACENES" +
                         " ON MULTIALMACENES." + Ope_Alm_Salidas_Det_Multi.Campo_Producto_ID +
                         "= PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID +
                         " WHERE PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID + " IN (SELECT " +
                         Ope_Alm_Salidas_Det_Multi.Campo_Producto_ID + " FROM " + Ope_Alm_Salidas_Det_Multi.Tabla_Ope_Alm_Salidas_Det_Multi +
                         " WHERE " + Ope_Alm_Salidas_Det_Multi.Campo_No_Salida + "=" + Clase_Negocios.P_No_Salida.Trim() +")";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Productos
        ///DESCRIPCIÓN: Metodo que consulta los productos existentes
        ///PARAMETROS: 1.- Cls_Ope_Alm_Salidas_Multi_Negocio Clase_Negocios objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 22/Diciembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Productos(Cls_Ope_Alm_Salidas_Multi_Negocio Clase_Negocios)
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT PRODUCTO." + Cat_Com_Productos.Campo_Clave;
            Mi_SQL = Mi_SQL + ", PRODUCTO." + Cat_Com_Productos.Campo_Producto_ID;
            Mi_SQL = Mi_SQL + " AS PRODUCTO_SERVICIO_ID, PRODUCTO." + Cat_Com_Productos.Campo_Nombre;
            Mi_SQL = Mi_SQL + " AS PRODUCTO_SERVICIO, PRODUCTO." + Cat_Com_Productos.Campo_Descripcion;
            Mi_SQL = Mi_SQL + " AS DESCRIPCION, (SELECT " + Cat_Com_Unidades.Campo_Nombre;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID;
            Mi_SQL = Mi_SQL + "=PRODUCTO." + Cat_Com_Productos.Campo_Unidad_ID + ") AS UNIDAD";
            Mi_SQL = Mi_SQL + ", PRODUCTO." + Cat_Com_Productos.Campo_Costo;
            Mi_SQL = Mi_SQL + " AS PRECIO_UNITARIO, MULTIALMACEN." + Cat_Com_Productos.Campo_Existencia + " FROM ";
            Mi_SQL = Mi_SQL + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTO ";
            Mi_SQL = Mi_SQL + " JOIN " + Ope_Alm_Productos_Multi.Tabla_Ope_Alm_Productos_Multi + " MULTIALMACEN";
            Mi_SQL = Mi_SQL + " ON MULTIALMACEN." + Ope_Alm_Productos_Multi.Campo_Producto_ID;
            Mi_SQL = Mi_SQL + "= PRODUCTO." + Cat_Com_Productos.Campo_Producto_ID;
            Mi_SQL = Mi_SQL + " WHERE MULTIALMACEN." + Cat_Alm_Multialmacenes.Campo_Almacen_ID;
            Mi_SQL = Mi_SQL + "=" + Clase_Negocios.P_Almacen_ID;

            if (Clase_Negocios.P_Nombre_Producto != null)
            {
                Mi_SQL = Mi_SQL + " AND UPPER(PRODUCTO." + Cat_Com_Productos.Campo_Nombre;
                Mi_SQL = Mi_SQL + ") LIKE UPPER('%" + Clase_Negocios.P_Nombre_Producto + "%')";
            }
            Mi_SQL = Mi_SQL + " ORDER BY PRODUCTO." + Cat_Com_Productos.Campo_Nombre;

            if (Clase_Negocios.P_Producto_ID != null)
            {

                Mi_SQL = "SELECT PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID +
                         ", PRODUCTOS." + Cat_Com_Productos.Campo_Clave +
                         ", PRODUCTOS." + Cat_Com_Productos.Campo_Nombre + " AS PRODUCTO_NOMBRE" +
                         ", PRODUCTOS." + Cat_Com_Productos.Campo_Descripcion +
                         ", (SELECT " + Cat_Com_Unidades.Campo_Nombre +
                         " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades +
                         " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID +
                         "=PRODUCTOS." + Cat_Com_Productos.Campo_Unidad_ID + ") AS UNIDAD" +
                         ", MULTIALMACENES." + Ope_Alm_Productos_Multi.Campo_Existencia +
                         ", PRODUCTOS." + Cat_Com_Productos.Campo_Reorden +
                         ", PRODUCTOS." + Cat_Com_Productos.Campo_Maximo + "-" +
                         " PRODUCTOS." + Cat_Com_Productos.Campo_Disponible + " AS CANTIDAD" +
                         ", PRODUCTOS." + Cat_Com_Productos.Campo_Costo + " AS PRECIO_UNITARIO" +
                         ", PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_ID +
                         ", PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_2_ID +
                         ",(SELECT " + Cat_Com_Impuestos.Campo_Nombre +
                         " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                         " WHERE PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_ID + "=" +
                         Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS TIPO_IMPUESTO_1 " +
                         ",(SELECT " + Cat_Com_Impuestos.Campo_Nombre +
                         " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                         " WHERE PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_2_ID + "=" +
                         Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS TIPO_IMPUESTO_2 " +
                         ",(SELECT " + Cat_Com_Impuestos.Campo_Porcentaje_Impuesto +
                         " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                         " WHERE PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_ID + "=" +
                         Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS IMPUESTO_PORCENTAJE_1 " +
                         ",(SELECT " + Cat_Com_Impuestos.Campo_Porcentaje_Impuesto +
                         " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                         " WHERE PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_2_ID + "=" +
                         Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS IMPUESTO_PORCENTAJE_2 " +
                         " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS" +
                         " JOIN " + Cat_Com_Impuestos.Tabla_Cat_Impuestos + " IMPUESTOS" +
                         " ON IMPUESTOS." + Cat_Com_Impuestos.Campo_Impuesto_ID + "= PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_ID +
                         " JOIN " + Ope_Alm_Productos_Multi.Tabla_Ope_Alm_Productos_Multi + " MULTIALMACENES"+
                         " ON MULTIALMACENES." + Ope_Alm_Productos_Multi.Campo_Producto_ID +
                         "= PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID +
                         " WHERE PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID + "='" +
                         Clase_Negocios.P_Producto_ID + "'";

            }

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Empleado
        ///DESCRIPCIÓN: Metodo que consulta los empleados 
        ///PARAMETROS: 1.- Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocios objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 6/Diciembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Empleado(Cls_Ope_Alm_Salidas_Multi_Negocio Clase_Negocios)
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT " + Cat_Empleados.Campo_Empleado_ID;
            Mi_SQL = Mi_SQL + ", " + Cat_Empleados.Campo_No_Empleado;
            Mi_SQL = Mi_SQL + ", " + Cat_Empleados.Campo_Nombre;
            Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno;
            Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + " AS NOMBRE";
            Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Empleados.Campo_No_Empleado + " IS NOT NULL";

            if (Clase_Negocios.P_No_Empleado != null)
            {

                Mi_SQL = Mi_SQL + " AND " + Cat_Empleados.Campo_No_Empleado;
                Mi_SQL = Mi_SQL + "='" + Clase_Negocios.P_No_Empleado.Trim() + "'";
            }

            if (Clase_Negocios.P_Nombre_Empleado != null)
            {
                Mi_SQL = Mi_SQL + " AND UPPER(" + Cat_Empleados.Campo_Nombre;
                Mi_SQL = Mi_SQL + ") LIKE UPPER('%" + Clase_Negocios.P_Nombre_Empleado.Trim() + "%')";
            }

            if (Clase_Negocios.P_Apellido_Paterno != null)
            {
                Mi_SQL = Mi_SQL + " AND UPPER(" + Cat_Empleados.Campo_Apellido_Paterno;
                Mi_SQL = Mi_SQL + ") LIKE UPPER('%" + Clase_Negocios.P_Apellido_Paterno.Trim() + "%')";
            }

            if (Clase_Negocios.P_Apellido_Materno != null)
            {
                Mi_SQL = Mi_SQL + " AND UPPER(" + Cat_Empleados.Campo_Apellido_Materno;
                Mi_SQL = Mi_SQL + ") LIKE UPPER('%" + Clase_Negocios.P_Apellido_Materno.Trim() + "%')";
            }

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Salidas
        ///DESCRIPCIÓN: Metodo que consulta los empleados 
        ///PARAMETROS: 1.- Cls_Cat_Alm_Multialmacenes_Negocio Clase_Negocios objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 6/Diciembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Alta_Salidas(Cls_Ope_Alm_Salidas_Multi_Negocio Clase_Negocios)
        {
            String Mi_SQL = "";
            String Msj="";
            try{
            int No_Salida = Obtener_Consecutivo(Ope_Alm_Salidas_Multi.Campo_No_Salida,Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi);
            Mi_SQL = "INSERT INTO " + Ope_Alm_Salidas_Multi.Tabla_Ope_Alm_Salidas_Multi;
            Mi_SQL = Mi_SQL + " (" + Ope_Alm_Salidas_Multi.Campo_No_Salida;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Campo_Almacen_ID;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Campo_Estatus;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Campo_Empleado_Recibio_ID;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Campo_Empleado_Almacen_ID;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Campo_Fecha_Hora;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Campo_IVA;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Campo_Subtotal;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Campo_Total;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Campo_Fecha_Creo;
            Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Multi.Campo_Usuario_Modifico;
            Mi_SQL = Mi_SQL + ") VALUES(";
            Mi_SQL = Mi_SQL + No_Salida;
            Mi_SQL = Mi_SQL + "," + Clase_Negocios.P_Almacen_ID;
            Mi_SQL = Mi_SQL + ",'GENERADA'";
            Mi_SQL = Mi_SQL + ",'" + Clase_Negocios.P_Empleado_Recibio_ID + "'";
            Mi_SQL = Mi_SQL + ",'" + Clase_Negocios.P_Empleado_Almacen_ID + "'";
            Mi_SQL = Mi_SQL + ",GETDATE()";
            Mi_SQL = Mi_SQL + ", " + Clase_Negocios.P_IVA;
            Mi_SQL = Mi_SQL + ", " + Clase_Negocios.P_Subtotal;
            Mi_SQL = Mi_SQL + ", " + Clase_Negocios.P_Total;
            Mi_SQL = Mi_SQL + ",GETDATE()";
            Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "')";
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            if (Clase_Negocios.P_Dt_Productos.Rows.Count > 0)
            {
                for (int i = 0; i < Clase_Negocios.P_Dt_Productos.Rows.Count; i++)
                {
                    double Subtotal = double.Parse(Clase_Negocios.P_Dt_Productos.Rows[i]["Importe"].ToString().Trim()) - double.Parse(Clase_Negocios.P_Dt_Productos.Rows[i]["Monto_IVA"].ToString().Trim());
                    Mi_SQL = "INSERT INTO " + Ope_Alm_Salidas_Det_Multi.Tabla_Ope_Alm_Salidas_Det_Multi;
                    Mi_SQL = Mi_SQL + "(" + Ope_Alm_Salidas_Det_Multi.Campo_No_Salida;
                    Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Det_Multi.Campo_Producto_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Det_Multi.Campo_Cantidad;
                    Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Det_Multi.Campo_Precio_Unitario;
                    Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Det_Multi.Campo_IVA;
                    Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Det_Multi.Campo_Subtotal;
                    Mi_SQL = Mi_SQL + ", " + Ope_Alm_Salidas_Det_Multi.Campo_Total;
                    Mi_SQL = Mi_SQL + ") VALUES(";
                    Mi_SQL = Mi_SQL + No_Salida;
                    Mi_SQL = Mi_SQL + ",'" + Clase_Negocios.P_Dt_Productos.Rows[i]["Producto_ID"].ToString().Trim() + "'";
                    Mi_SQL = Mi_SQL + ",'" + Clase_Negocios.P_Dt_Productos.Rows[i]["Cantidad"].ToString().Trim() + "'";
                    Mi_SQL = Mi_SQL + "," + Clase_Negocios.P_Dt_Productos.Rows[i]["Precio_Unitario"].ToString().Trim();
                    Mi_SQL = Mi_SQL + "," + Clase_Negocios.P_Dt_Productos.Rows[i]["Monto_IVA"].ToString().Trim();
                    Mi_SQL = Mi_SQL + "," + Subtotal.ToString();
                    Mi_SQL = Mi_SQL + "," + Clase_Negocios.P_Dt_Productos.Rows[i]["Importe"].ToString().Trim() + ")";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                    //Disminuimos la existencia 
                    Mi_SQL = "UPDATE " + Ope_Alm_Productos_Multi.Tabla_Ope_Alm_Productos_Multi;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Alm_Productos_Multi.Campo_Existencia;
                    Mi_SQL = Mi_SQL + "=" + Ope_Alm_Productos_Multi.Campo_Existencia;
                    Mi_SQL = Mi_SQL + "- " + Clase_Negocios.P_Dt_Productos.Rows[i]["Cantidad"].ToString().Trim();
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Alm_Productos_Multi.Campo_Almacen_ID;
                    Mi_SQL = Mi_SQL + "=" + Clase_Negocios.P_Almacen_ID;
                    Mi_SQL = Mi_SQL + " AND " + Ope_Alm_Productos_Multi.Campo_Producto_ID;
                    Mi_SQL = Mi_SQL + "='" + Clase_Negocios.P_Dt_Productos.Rows[i]["Producto_ID"].ToString().Trim() + "'";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                Msj = "Se creo la Salida " + No_Salida;
            }
            }catch(Exception EX)
            {
                Msj = EX.Message;

            }

            return Msj;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-P_Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }

        public static DataTable Consultar_Almacen_Logueado()
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT * FROM " + Cat_Alm_Multialmacenes.Tabla_Cat_Alm_Multialmacenes;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Alm_Multialmacenes.Campo_Almacen_ID;
            Mi_SQL = Mi_SQL + "= (SELECT " + Cat_Alm_Multialmacenes.Campo_Almacen_ID;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Alm_Multialmacenes.Tabla_Cat_Alm_Multialmacenes;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Alm_Multialmacenes.Campo_Responsable_ID;
            Mi_SQL = Mi_SQL + "='" + Cls_Sessiones.Empleado_ID + "')";
 
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }
    }
}//Fin del namespace