﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Incapacidades.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Text;

namespace JAPAMI.Incapacidades.Datos
{
    public class Cls_Ope_Nom_Incapacidades_Datos
    {
        #region(Métodos Operación)
        /// ********************************************************************************************************************
        /// NOMBRE: Alta_Incapacidad
        /// 
        /// DESCRIPCIÓN: Ejecuta el alta de una Incapacidad.
        /// 
        /// PARÁMETROS: Datos: Información a guardar en la base de datos.
        /// 
        /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
        /// FECHA CREÓ: 06/Abril/2011 16:59 pm.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACION:
        /// ********************************************************************************************************************
        public static Boolean Alta_Incapacidad(Cls_Ope_Nom_Incapacidades_Negocio Datos)
        {
            StringBuilder Mi_SQL =new StringBuilder();//Obtiene la cadena de inserción hacía la base de datos
            String Mensaje = ""; //Obtiene la descripción del error ocurrido durante la ejecución de Mi_SQL
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Object No_Incapacidad; //Variable auxiliar
            Boolean Operacion_Completa = false;

            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            //Esta inserción se realiza sin el Ayudante de SQL y con el BeginTrans y Commit para proteger la información
            //el ayudante de SQL solo debe usarse cuando solo se afecte una tabla o para movimientos que NO son críticos
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                //Consulta para el ID de la region
                Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Nom_Incapacidades.Campo_No_Incapacidad + "), '0000000000') FROM ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades);

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL.ToString();
                No_Incapacidad = Cmd.ExecuteScalar();

                //Verificar si no es nulo
                if (!(No_Incapacidad is Nullable))
                {
                    Datos.P_No_Incapacidad = string.Format("{0:0000000000}", (Convert.ToInt32(No_Incapacidad) + 1));
                }
                else
                {
                    Datos.P_No_Incapacidad = "0000000001";
                }

                Mi_SQL = new StringBuilder();

                Mi_SQL.Append("INSERT INTO " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades);
                Mi_SQL.Append(" (" + Ope_Nom_Incapacidades.Campo_No_Incapacidad + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Empleado_ID + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Dependencia_ID + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Estatus + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Tipo_Incapacidad + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Aplica_Pago_Cuarto_Dia + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Porcentaje_Incapacidad + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Extencion_Incapacidad + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Fecha_Inicio + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Fecha_Fin + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Comentario + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Nomina_ID + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_No_Nomina + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Usuario_Creo + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Fecha_Creo + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Dias_Incapacidad + ") VALUES(");
                Mi_SQL.Append("'" + Datos.P_No_Incapacidad + "', ");
                Mi_SQL.Append("'" + Datos.P_Empleado_ID + "', ");
                Mi_SQL.Append("'" + Datos.P_Dependencia_ID + "', ");
                Mi_SQL.Append("'" + Datos.P_Estatus + "', ");
                Mi_SQL.Append("'" + Datos.P_Tipo_Incapacidad + "', ");
                Mi_SQL.Append("'" + Datos.P_Aplica_Pago_Cuarto_Dia + "', ");
                Mi_SQL.Append(Datos.P_Porcentaje_Incapacidad + ", ");
                Mi_SQL.Append("'" + Datos.P_Extencion_Incapacidad + "', ");
                Mi_SQL.Append("'" + Datos.P_Fecha_Inicio_Incapacidad + "', ");
                Mi_SQL.Append("'" + Datos.P_Fecha_Fin_Incapacidad + "', ");
                Mi_SQL.Append("'" + Datos.P_Comentarios + "', ");
                Mi_SQL.Append("'" + Datos.P_Nomina_ID + "', ");
                Mi_SQL.Append(Datos.P_No_Nomina + ", ");
                Mi_SQL.Append("'" + Datos.P_Usuario_Creo + "', ");
                Mi_SQL.Append("GETDATE(), " + Datos.P_Dias_Incapacidad + ")");

                //Ejecutar la consulta
                Cmd.CommandText = Mi_SQL.ToString();
                Cmd.ExecuteNonQuery();

                //Ejecutar transaccion
                Trans.Commit();
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number.ToString().Equals("8152"))
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                }
                else if (Ex.Number.ToString().Equals("2627"))
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos";
                    }
                }
                else if (Ex.Number.ToString().Equals("547"))
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla";
                }
                else if (Ex.Number.ToString().Equals("515"))
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar";
                }
                else
                {
                    Mensaje = Ex.Message; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            catch (DBConcurrencyException Ex)
            {
                //Indicamos el mensaje 
                throw new Exception("Lo siento, los datos fueron actualizados por otro Rol. Error: [" + Ex.Message + "]");

            }
            catch (Exception Ex)
            {
                //Indicamos el mensaje 
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Operacion_Completa;
        }
        /// ********************************************************************************************************************
        /// NOMBRE: Modificar_Incapacidad
        /// 
        /// DESCRIPCIÓN: Ejecuta la modificación de una Incapacidad.
        /// 
        /// PARÁMETROS: Datos: Información a actualizar en la base de datos.
        /// 
        /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
        /// FECHA CREÓ: 06/Abril/2011 17:11 pm.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACION:
        /// ********************************************************************************************************************
        public static Boolean Modificar_Incapacidad(Cls_Ope_Nom_Incapacidades_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Obtiene la cadena de inserción hacía la base de datos
            String Mensaje = ""; //Obtiene la descripción del error ocurrido durante la ejecución de Mi_SQL
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Boolean Operacion_Completa = false;

            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            //Esta inserción se realiza sin el Ayudante de SQL y con el BeginTrans y Commit para proteger la información
            //el ayudante de SQL solo debe usarse cuando solo se afecte una tabla o para movimientos que NO son críticos
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                Mi_SQL.Append("UPDATE " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + " SET ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Estatus + "='" + Datos.P_Estatus + "', ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Tipo_Incapacidad + "='" + Datos.P_Tipo_Incapacidad + "', ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Aplica_Pago_Cuarto_Dia + "='" + Datos.P_Aplica_Pago_Cuarto_Dia + "', ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Porcentaje_Incapacidad + "=" + Datos.P_Porcentaje_Incapacidad + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Extencion_Incapacidad + "='" + Datos.P_Extencion_Incapacidad + "', ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Fecha_Inicio + "='" + Datos.P_Fecha_Inicio_Incapacidad + "', ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Fecha_Fin + "='" + Datos.P_Fecha_Fin_Incapacidad + "', ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Comentario + "='" + Datos.P_Comentarios + "', ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Nomina_ID + "='" + Datos.P_Nomina_ID + "', ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_No_Nomina + "=" + Datos.P_No_Nomina + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Dias_Incapacidad + "=" + Datos.P_Dias_Incapacidad + ", ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Modifico + "', ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Fecha_Modifico + "=GETDATE()");
                Mi_SQL.Append(" WHERE ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_No_Incapacidad + "='" + Datos.P_No_Incapacidad + "'");

                //Ejecutar la consulta
                Cmd.CommandText = Mi_SQL.ToString();
                Cmd.ExecuteNonQuery();

                //Ejecutar transaccion
                Trans.Commit();
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number.ToString().Equals("8152"))
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                }
                else if (Ex.Number.ToString().Equals("2627"))
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos";
                    }
                }
                else if (Ex.Number.ToString().Equals("547"))
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla";
                }
                else if (Ex.Number.ToString().Equals("515"))
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar";
                }
                else
                {
                    Mensaje = Ex.Message; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            catch (DBConcurrencyException Ex)
            {
                //Indicamos el mensaje 
                throw new Exception("Lo siento, los datos fueron actualizados por otro Rol. Error: [" + Ex.Message + "]");

            }
            catch (Exception Ex)
            {
                //Indicamos el mensaje 
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Operacion_Completa;
        }
        /// ********************************************************************************************************************
        /// NOMBRE: Eliminar_Incapacidad
        /// 
        /// DESCRIPCIÓN: Ejecuta la baja de una Incapacidad.
        /// 
        /// PARÁMETROS: Datos: Información a actualizar en la base de datos.
        /// 
        /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
        /// FECHA CREÓ: 06/Abril/2011 17:11 pm.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACION:
        /// ********************************************************************************************************************
        public static Boolean Eliminar_Incapacidad(Cls_Ope_Nom_Incapacidades_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Obtiene la cadena de inserción hacía la base de datos
            String Mensaje = ""; //Obtiene la descripción del error ocurrido durante la ejecución de Mi_SQL
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Boolean Operacion_Completa = false;

            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            //Esta inserción se realiza sin el Ayudante de SQL y con el BeginTrans y Commit para proteger la información
            //el ayudante de SQL solo debe usarse cuando solo se afecte una tabla o para movimientos que NO son críticos
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                Mi_SQL.Append("DELETE FROM " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + " WHERE ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_No_Incapacidad + "='" + Datos.P_No_Incapacidad + "'");

                //Ejecutar la consulta
                Cmd.CommandText = Mi_SQL.ToString();
                Cmd.ExecuteNonQuery();

                //Ejecutar transaccion
                Trans.Commit();
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number.ToString().Equals("8152"))
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                }
                else if (Ex.Number.ToString().Equals("2627"))
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos";
                    }
                }
                else if (Ex.Number.ToString().Equals("547"))
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla";
                }
                else if (Ex.Number.ToString().Equals("515"))
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar";
                }
                else
                {
                    Mensaje = Ex.Message; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            catch (DBConcurrencyException Ex)
            {
                //Indicamos el mensaje 
                throw new Exception("Lo siento, los datos fueron actualizados por otro Rol. Error: [" + Ex.Message + "]");

            }
            catch (Exception Ex)
            {
                //Indicamos el mensaje 
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Operacion_Completa;
        }
        /// ********************************************************************************************************************
        /// NOMBRE: Modificar_Incapacidad
        /// 
        /// DESCRIPCIÓN: Ejecuta la modificación de una Incapacidad.
        /// 
        /// PARÁMETROS: Datos: Información a actualizar en la base de datos.
        /// 
        /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
        /// FECHA CREÓ: 06/Abril/2011 17:11 pm.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACION:
        /// ********************************************************************************************************************
        public static Boolean Cambiar_Estatus_Incapacidad(Cls_Ope_Nom_Incapacidades_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Obtiene la cadena de inserción hacía la base de datos
            String Mensaje = ""; //Obtiene la descripción del error ocurrido durante la ejecución de Mi_SQL
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Boolean Operacion_Completa = false;

            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            //Esta inserción se realiza sin el Ayudante de SQL y con el BeginTrans y Commit para proteger la información
            //el ayudante de SQL solo debe usarse cuando solo se afecte una tabla o para movimientos que NO son críticos
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                Mi_SQL.Append("UPDATE " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + " SET ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Estatus + "='" + Datos.P_Estatus + "', ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Modifico + "', ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_Fecha_Modifico + "=GETDATE()");
                Mi_SQL.Append(" WHERE ");
                Mi_SQL.Append(Ope_Nom_Incapacidades.Campo_No_Incapacidad + "='" + Datos.P_No_Incapacidad + "'");

                //Ejecutar la consulta
                Cmd.CommandText = Mi_SQL.ToString();
                Cmd.ExecuteNonQuery();

                //Ejecutar transaccion
                Trans.Commit();
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number.ToString().Equals("8152"))
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                }
                else if (Ex.Number.ToString().Equals("2627"))
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos";
                    }
                }
                else if (Ex.Number.ToString().Equals("547"))
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla";
                }
                else if (Ex.Number.ToString().Equals("515"))
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar";
                }
                else
                {
                    Mensaje = Ex.Message; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            catch (DBConcurrencyException Ex)
            {
                //Indicamos el mensaje 
                throw new Exception("Lo siento, los datos fueron actualizados por otro Rol. Error: [" + Ex.Message + "]");

            }
            catch (Exception Ex)
            {
                //Indicamos el mensaje 
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Operacion_Completa;
        }
        #endregion

        #region(Métodos Consulta)
        /// ********************************************************************************************************************
        /// NOMBRE: Consultar_Incapacidades
        /// 
        /// DESCRIPCIÓN: Consulta las incapacidades que existen registradas actualmente en el sistema.
        /// 
        /// PARÁMETROS: Datos: Información a actualizar en la base de datos.
        /// 
        /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
        /// FECHA CREÓ: 06/Abril/2011 17:11 pm.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACION:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Incapacidades(Cls_Ope_Nom_Incapacidades_Negocio Datos)
        {
            DataTable Dt_Incapacidades = null;//Variable que almacenara una lista de las antiguedades que evaluaran los sindicatos.
            String Mi_SQL = "";//Variable que almacenara la consulta.
            try
            {
                Mi_SQL = "SELECT " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + ".*, " +
                                Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA, " +
                                " (" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + "+' '+" +
                                Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + "+' '+ " +
                                Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + ") As Nombre" +
                            " FROM " +
                                Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + ", " + Cat_Dependencias.Tabla_Cat_Dependencias +
                                ", " + Cat_Empleados.Tabla_Cat_Empleados +
                            " WHERE (" +
                                Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_Dependencia_ID +
                            "=" +
                                Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + ") AND " +
                            "(" + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_Empleado_ID + "=" +
                            Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + ")";

                if (!string.IsNullOrEmpty(Datos.P_No_Incapacidad))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_No_Incapacidad + "='" + Datos.P_No_Incapacidad + "'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_No_Incapacidad + "='" + Datos.P_No_Incapacidad + "'";
                    }
                }

                if (!string.IsNullOrEmpty(Datos.P_Empleado_ID))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_Empleado_ID + "='" + Datos.P_Empleado_ID + "'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_Empleado_ID + "='" + Datos.P_Empleado_ID + "'";
                    }
                }

                if (!string.IsNullOrEmpty(Datos.P_Dependencia_ID))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_Dependencia_ID + "='" + Datos.P_Dependencia_ID + "'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_Dependencia_ID + "='" + Datos.P_Dependencia_ID + "'";
                    }
                }

                if (!string.IsNullOrEmpty(Datos.P_Estatus))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_Estatus + "='" + Datos.P_Estatus + "'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_Estatus + "='" + Datos.P_Estatus + "'";
                    }
                }

                if (!string.IsNullOrEmpty(Datos.P_Nomina_ID))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_Nomina_ID + "='" + Datos.P_Nomina_ID + "'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_Nomina_ID + "='" + Datos.P_Nomina_ID + "'";
                    }
                }

                if (Datos.P_No_Nomina is Int32 && Datos.P_No_Nomina > 0)
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_No_Nomina + "=" + Datos.P_No_Nomina;
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_No_Nomina + "=" + Datos.P_No_Nomina;
                    }
                }

                if (!string.IsNullOrEmpty(Datos.P_Fecha_Inicio_Incapacidad) && !string.IsNullOrEmpty(Datos.P_Fecha_Fin_Incapacidad))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_Fecha_Inicio + " BETWEEN TO_DATE ('" + Datos.P_Fecha_Inicio_Incapacidad + " 00:00:00', 'DD-MM-YYYY HH24:MI:SS')" +
                            " AND TO_DATE ('" + Datos.P_Fecha_Fin_Incapacidad + " 23:59:00', 'DD-MM-YYYY HH24:MI:SS')";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_Fecha_Inicio + " BETWEEN TO_DATE ('" + Datos.P_Fecha_Inicio_Incapacidad + " 00:00:00', 'DD-MM-YYYY HH24:MI:SS')" +
                            " AND TO_DATE ('" + Datos.P_Fecha_Fin_Incapacidad + " 23:59:00', 'DD-MM-YYYY HH24:MI:SS')";
                    }
                }

                if (!string.IsNullOrEmpty(Datos.P_Fecha_Inicio_Incapacidad) && !string.IsNullOrEmpty(Datos.P_Fecha_Fin_Incapacidad))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_Fecha_Fin + " BETWEEN TO_DATE ('" + Datos.P_Fecha_Inicio_Incapacidad + " 00:00:00', 'DD-MM-YYYY HH24:MI:SS')" +
                            " AND TO_DATE ('" + Datos.P_Fecha_Fin_Incapacidad + " 23:59:00', 'DD-MM-YYYY HH24:MI:SS')";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Nom_Incapacidades.Tabla_Ope_Nom_Incapacidades + "." + Ope_Nom_Incapacidades.Campo_Fecha_Fin + " BETWEEN TO_DATE ('" + Datos.P_Fecha_Inicio_Incapacidad + " 00:00:00', 'DD-MM-YYYY HH24:MI:SS')" +
                            " AND TO_DATE ('" + Datos.P_Fecha_Fin_Incapacidad + " 23:59:00', 'DD-MM-YYYY HH24:MI:SS')";
                    }
                }

                Dt_Incapacidades = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar las Incapacidades. Error: [" + Ex.Message + "]");
            }
            return Dt_Incapacidades;
        }
        #endregion
    }
}
