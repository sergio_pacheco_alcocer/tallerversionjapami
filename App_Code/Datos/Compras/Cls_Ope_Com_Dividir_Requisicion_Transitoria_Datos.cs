﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Ope_Com_Dividir_Requisicion_Transitoria.Negocios;
using JAPAMI.Generar_Requisicion.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;

/// <summary>
/// Summary description for Cls_Ope_Com_Dividir_Requisicion_Transitoria
/// </summary>
namespace JAPAMI.Ope_Com_Dividir_Requisicion_Transitoria.Datos
{
    public class Cls_Ope_Com_Dividir_Requisicion_Transitoria_Datos
    {
        public Cls_Ope_Com_Dividir_Requisicion_Transitoria_Datos()
        {
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consulta_Requisicion_Transitoria //Consulta_Listado_Almacen
        ///DESCRIPCIÓN          : Consulta las requisiciones que han sido distribuidas 
        ///                       a los cotizadores
        ///PARAMETROS           :  
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 04/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consulta_Requisicion_Transitoria(Cls_Ope_Com_Dividir_Requisicion_Transitoria_Negocio Clase_Negocio)
        {
            String Mi_SQL = "SELECT " + Ope_Com_Requisiciones.Campo_Folio;
            Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL = Mi_SQL + ", REPLACE(CONVERT(VARCHAR(11), " + Ope_Com_Requisiciones.Campo_Fecha_Creo + ",106),' ','/') AS FECHA_CREO";
            Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Tipo;
            Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Estatus;
            Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Total;
            Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones+ " ";
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Requisiciones.Campo_Estatus + "='FILTRADA'";
            Mi_SQL = Mi_SQL + " AND " + Ope_Com_Requisiciones.Campo_Tipo + " = 'TRANSITORIA'";
            Mi_SQL = Mi_SQL + " AND " + Ope_Com_Requisiciones.Campo_Requisicion_Origen_ID + " IS NULL "; 
            if (Clase_Negocio.P_No_Requisicion_ID!= null)
            {
                Mi_SQL = "SELECT * FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Clase_Negocio.P_No_Requisicion_ID;
            }
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consulta_Detalle_Requisicion_Transitoria //Consulta_Listado_Almacen
        ///DESCRIPCIÓN          : Consulta los productos de la requsisicin que no han sido divididos
        ///                       en una requisa 
        ///PARAMETROS           :  
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 04/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consulta_Detalle_Requisicion_Transitoria(Cls_Ope_Com_Dividir_Requisicion_Transitoria_Negocio Clase_Negocio)
        {
            String Mi_SQL = String.Empty;
            if (Clase_Negocio.P_Tipo_Articulo.Equals("PRODUCTO"))
            {
                Mi_SQL = "SELECT " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " AS PRODUCTO_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Clave;
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Nombre + " AS PRODUCTO_NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Descripcion;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Partida_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Precio_Unitario + " AS PRECIO_UNITARIO ";
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID;
                Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Sap_Concepto.Campo_Clave + " +' '+ " + Cat_Sap_Concepto.Campo_Descripcion + " FROM ";
                Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
                Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
                Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
                Mi_SQL = Mi_SQL + "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
                Mi_SQL = Mi_SQL + "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM ";
                Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = ";
                Mi_SQL = Mi_SQL + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Partida_ID + "))) AS CONCEPTO";
                Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Sap_Concepto.Campo_Concepto_ID + " FROM ";
                Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
                Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
                Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
                Mi_SQL = Mi_SQL + "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
                Mi_SQL = Mi_SQL + "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM ";
                Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = ";
                Mi_SQL = Mi_SQL + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Partida_ID + "))) AS CONCEPTO_ID";
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Tipo;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Cantidad;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Monto_Total + " AS COSTO_COMPRA ";
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Importe + " AS IMPORTE ";
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Monto_IVA;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Monto_IEPS;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Porcentaje_IVA;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Porcentaje_IEPS;
                Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                Mi_SQL = Mi_SQL + " JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL = Mi_SQL + " ON " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Producto_ID;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = '" + Clase_Negocio.P_No_Requisicion_ID + "'";
                Mi_SQL = Mi_SQL + " AND " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Dividida + " IS NULL ";
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Nombre;
            }
            else 
            {
                Mi_SQL = "SELECT " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " AS PRODUCTO_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + "." + Cat_Com_Servicios.Campo_Clave;
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + "." + Cat_Com_Servicios.Campo_Nombre + " AS PRODUCTO_NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + "." + Cat_Com_Servicios.Campo_Comentarios + " AS DESCRIPCION";
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Partida_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Precio_Unitario + " AS PRECIO_UNITARIO ";
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID;
                Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Sap_Concepto.Campo_Clave + " +' '+ " + Cat_Sap_Concepto.Campo_Descripcion + " FROM ";
                Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
                Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
                Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
                Mi_SQL = Mi_SQL + "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
                Mi_SQL = Mi_SQL + "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM ";
                Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = ";
                Mi_SQL = Mi_SQL + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + "." + Cat_Com_Servicios.Campo_Partida_ID + "))) AS CONCEPTO";
                Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Sap_Concepto.Campo_Concepto_ID + " FROM ";
                Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
                Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
                Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
                Mi_SQL = Mi_SQL + "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
                Mi_SQL = Mi_SQL + "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM ";
                Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = ";
                Mi_SQL = Mi_SQL + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + "." + Cat_Com_Servicios.Campo_Partida_ID + "))) AS CONCEPTO_ID";
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Tipo;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Cantidad;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Monto_Total + " AS COSTO_COMPRA ";
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Importe + " AS IMPORTE ";
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Monto_IVA;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Monto_IEPS;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Porcentaje_IVA;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Porcentaje_IEPS;
                Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                Mi_SQL = Mi_SQL + " JOIN " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios;
                Mi_SQL = Mi_SQL + " ON " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + "." + Cat_Com_Servicios.Campo_Servicio_ID;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = '" + Clase_Negocio.P_No_Requisicion_ID + "'";
                Mi_SQL = Mi_SQL + " AND " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Dividida + " IS NULL ";
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + "." + Cat_Com_Servicios.Campo_Nombre;
            }
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Requisiciones_Hijas
        ///DESCRIPCIÓN          : Consulta las requisiciones que han sido creadas de la requisicion padre
        ///PARAMETROS           :  
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 05/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Requisiciones_Hijas(Cls_Ope_Com_Dividir_Requisicion_Transitoria_Negocio Clase_Negocio)
        {
            //Consultamos las requisiciones listado 
            String Mi_SQL = "SELECT " + Ope_Com_Requisiciones.Campo_Folio;
            Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Total;
            Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_Origen_ID;
            Mi_SQL = Mi_SQL + " = " + Clase_Negocio.P_No_Requisicion_ID;
            
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Convertir_Requisicion_Transitoria
        ///DESCRIPCIÓN          : Genera la nueva requisicion transitoria 
        ///PARAMETROS           :  
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 05/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Convertir_Requisicion_Transitoria(Cls_Ope_Com_Dividir_Requisicion_Transitoria_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            String Id_Requisicion = "";
            //INSERTAR LA REQUISICION   
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Consultamos los datos de la requisición de origen
                String Mi_SQL = "SELECT * FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Clase_Negocio.P_No_Requisicion_ID; 
                DataTable Dt_Parametros = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                String Dependencia_ID= Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString();
                String Partida_ID = Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Partida_ID].ToString();
                String Programa_ID = Clase_Negocio.P_Dt_Productos.Rows[0]["Proyecto_Programa_ID"].ToString();
                String Fuente_Financiamiento_ID = Clase_Negocio.P_Dt_Productos.Rows[0]["Fuente_Financiamiento_ID"].ToString();
                //Consulta para obtener la reserva
                DataTable Dt_Reserva_Requisicion_Origen = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Reservas_De_Requisicion(Clase_Negocio.P_No_Requisicion_ID);
                String Reserva_Requisicion_Origen = Dt_Reserva_Requisicion_Origen.Rows[0][Ope_Psp_Reservas.Campo_No_Reserva].ToString();
                //Insertamos la nueva requisicion 
                String Empleado_Autorizo = "";
                Empleado_Autorizo = Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Autorizacion_ID].ToString().Trim();
                //Generamos el id de la requisiciion 
                //Insertar la nueva requisicion
                Id_Requisicion = Obtener_Consecutivo(Ope_Com_Requisiciones.Campo_Requisicion_ID, Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones).ToString();
                Mi_SQL = "INSERT INTO " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                    " (" + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Folio +
                    ", " + Ope_Com_Requisiciones.Campo_Estatus +
                    ", " + Ope_Com_Requisiciones.Campo_Tipo +
                    ", " + Ope_Com_Requisiciones.Campo_Fase +
                    ", " + Ope_Com_Requisiciones.Campo_Usuario_Creo +
                    ", " + Ope_Com_Requisiciones.Campo_Fecha_Creo +
                    ", " + Ope_Com_Requisiciones.Campo_Empleado_Filtrado_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Fecha_Filtrado +
                    ", " + Ope_Com_Requisiciones.Campo_Tipo_Articulo +
                    ", " + Ope_Com_Requisiciones.Campo_Empleado_Construccion_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Fecha_Construccion +
                    ", " + Ope_Com_Requisiciones.Campo_Empleado_Generacion_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Fecha_Generacion +
                    ", " + Ope_Com_Requisiciones.Campo_Empleado_Autorizacion_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion +
                    ", " + Ope_Com_Requisiciones.Campo_Codigo_Programatico +
                    ", " + Ope_Com_Requisiciones.Campo_Partida_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Justificacion_Compra +
                    ", " + Ope_Com_Requisiciones.Campo_Consolidada +
                    ", " + Ope_Com_Requisiciones.Campo_Cotizador_ID +
                    ", " + Ope_Com_Requisiciones.Campo_Verificaion_Entrega +
                    ", " + Ope_Com_Requisiciones.Campo_Requisicion_Origen_ID +
                    ") VALUES ('" + Id_Requisicion + "','" +
                    Dependencia_ID + "','" +
                    "RQ-" + Id_Requisicion + "','" +
                    Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Estatus].ToString() + "','" +
                    "TRANSITORIA','" +
                    "REQUISICION','" +
                    Cls_Sessiones.Nombre_Empleado + "',GETDATE()," +
                    "'" + Cls_Sessiones.Empleado_ID + "',GETDATE(), '" +
                    Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Tipo_Articulo].ToString().Trim() + "'," +
                    "'" + Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Construccion_ID].ToString() + "',";
                //Obtener la fecha de construccion
                    Mi_SQL = Mi_SQL + "(SELECT " + Ope_Com_Requisiciones.Campo_Fecha_Construccion + " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Requisicion_ID].ToString() + "),";
                Mi_SQL = Mi_SQL + "'" + Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Generacion_ID].ToString() + "',";
                //Obtenemos la fecha de generacion
                Mi_SQL = Mi_SQL + "(SELECT " + Ope_Com_Requisiciones.Campo_Fecha_Generacion + " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Requisicion_ID].ToString() + "),";
                Mi_SQL = Mi_SQL + "'" + Empleado_Autorizo + "',";
                //Obtenemos la fecha de autorizacions
                Mi_SQL = Mi_SQL + "(SELECT " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Requisicion_ID].ToString() + "),";
                Mi_SQL = Mi_SQL + "'" + Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Codigo_Programatico].ToString() + "','" +
                    Clase_Negocio.P_Dt_Productos.Rows[0]["Partida_ID"].ToString().Trim() + "', '" +
                    Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Justificacion_Compra].ToString() + "','" +
                    Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Consolidada].ToString() + "','" +
                    Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Cotizador_ID].ToString() + "','" +
                    Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Verificaion_Entrega].ToString() + "'," +
                    Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Requisicion_ID].ToString() + ") ";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                
                //Ahora recorremos el data de los productos del listado y los pasamos a la requisicion
                //Calculamos el IEPS, IVA y Subtotal de la requisicion de acuerdo a los productos que le pertenecen a esta
                //Variable que almacena la suma de todos los valores del IVA que tiene cada producto del detalle
                double IVA_Acumulado = 0;
                //Variable que almacena la suma de todos los valores del IEPS que tiene cada producto del detalle
                double IEPS_Acumulado = 0;
                //Variable que almacena la suma del costo compra sin tomar en cuenta el aumento por impuestos
                double Subtotal = 0;
                double Total = 0;
                double Subtotal_Producto = 0;
                double Total_Producto = 0;
                double IVA_Producto = 0;
                double IEPS_Producto = 0;
                if (Clase_Negocio.P_Dt_Productos.Rows.Count != 0)
                {
                    for (int i = 0; i < Clase_Negocio.P_Dt_Productos.Rows.Count; i++)
                    {
                        //obtenemos el Subtotal ya que el costo compra solo es el precio unitario sin la multiplicacion de la cantidad
                        Subtotal_Producto = double.Parse(Clase_Negocio.P_Dt_Productos.Rows[i]["Precio_Unitario"].ToString()) * int.Parse(Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Listado_Detalle.Campo_Cantidad].ToString());
                        IVA_Producto = double.Parse(Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Listado_Detalle.Campo_Monto_IVA].ToString());
                        IEPS_Producto = double.Parse(Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Listado_Detalle.Campo_Monto_IEPS].ToString());
                        Total_Producto = Subtotal_Producto + IVA_Producto + IEPS_Producto;
                        Mi_SQL = "INSERT INTO ";
                        Mi_SQL = Mi_SQL + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                        Mi_SQL = Mi_SQL + " (" + Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Requisicion_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Prod_Serv_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Partida_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Cantidad;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Fecha_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Precio_Unitario;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Monto_IVA;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Monto_IEPS;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Porcentaje_IVA;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Porcentaje_IEPS;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Importe;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Monto_Total;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Tipo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Clave;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Nombre_Giro;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Giro_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID;
                        Mi_SQL = Mi_SQL + " ) VALUES ";
                        Mi_SQL = Mi_SQL + "('" + Obtener_Consecutivo(Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID, Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto);
                        Mi_SQL = Mi_SQL + "','" + Id_Requisicion;
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Producto_ID"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Partida_ID"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "', '" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Listado_Detalle.Campo_Cantidad].ToString();
                        Mi_SQL = Mi_SQL + "','" + Cls_Sessiones.Nombre_Empleado;
                        Mi_SQL = Mi_SQL + "',GETDATE()";
                        Mi_SQL = Mi_SQL + ",'" + Clase_Negocio.P_Dt_Productos.Rows[i]["Precio_Unitario"].ToString();
                        Mi_SQL = Mi_SQL + "','" + IVA_Producto;
                        Mi_SQL = Mi_SQL + "','" + IEPS_Producto;
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Listado_Detalle.Campo_Porcentaje_IVA].ToString();
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Listado_Detalle.Campo_Porcentaje_IEPS].ToString();
                        Mi_SQL = Mi_SQL + "','" + Subtotal_Producto;
                        Mi_SQL = Mi_SQL + "','" + Total_Producto;
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Tipo"];
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Clave"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Concepto"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Concepto_ID"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Producto_Nombre"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "' +' '+'" + Clase_Negocio.P_Dt_Productos.Rows[i]["Descripcion"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "', '" + Clase_Negocio.P_Dt_Productos.Rows[i]["Proyecto_Programa_ID"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "', '" + Clase_Negocio.P_Dt_Productos.Rows[i]["Fuente_Financiamiento_ID"].ToString() + "') ";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        //Actualizar el campo dividido del producto en el detalle de la requisicion 
                        Mi_SQL = "UPDATE " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto ;
                        Mi_SQL = Mi_SQL + " SET " + Ope_Com_Req_Producto.Campo_Dividida + " = 'SI'";
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = '" + Clase_Negocio.P_Dt_Productos.Rows[i]["Producto_ID"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + " AND " + Ope_Com_Req_Producto.Campo_Requisicion_ID  + " = " + Clase_Negocio.P_No_Requisicion_ID;
                        Mi_SQL = Mi_SQL + " AND " + Ope_Com_Req_Producto.Campo_Tipo + " = '" + Clase_Negocio.P_Dt_Productos.Rows[i]["Tipo"].ToString().Trim() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        IVA_Acumulado = IVA_Acumulado + IVA_Producto;
                        IEPS_Acumulado = IEPS_Acumulado + IEPS_Producto;
                        //Como el Costo Compra es el precio unitario sinla cantidad se multiplica el presio unitario sin impuesto por la cantidad para obtener el Subtotal
                        Subtotal = Subtotal + Subtotal_Producto;
                        Total = Total + Total_Producto;
                        //REgresamos a valores ceros los acumulados
                        IVA_Producto = 0;
                        IEPS_Producto = 0;
                        Subtotal_Producto = 0;
                        Total_Producto = 0;
                    }
                    //Actualizamos la requisicion con los nuevos valores de IVA, IEPs y subtotal 
                    Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                            " SET " + Ope_Com_Requisiciones.Campo_IVA + " ='" + IVA_Acumulado.ToString() +
                            "', " + Ope_Com_Requisiciones.Campo_IEPS + "='" + IEPS_Acumulado.ToString() +
                            "', " + Ope_Com_Requisiciones.Campo_Subtotal + "='" + Subtotal.ToString() +
                            "', " + Ope_Com_Requisiciones.Campo_Total + "='" + Total.ToString() +
                            "' WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + "='" + Id_Requisicion + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //En caso de existir comentarios se agregan a  la requiza
                    if (!String.IsNullOrEmpty(Clase_Negocio.P_Comentarios))
                    {
                        Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Com_Req_Observaciones.Campo_Observacion_ID + "),'0') ";
                        Mi_SQL = Mi_SQL + "FROM " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones;
                        Cmd.CommandText = Mi_SQL;
                        Object Asunto_ID = Cmd.ExecuteScalar();
                        String Consecutivo;
                        if (Convert.IsDBNull(Asunto_ID))                        
                            Consecutivo = "1";
                        else
                            Consecutivo = string.Format("{0:0}", Convert.ToInt32(Asunto_ID) + 1);                        

                        Mi_SQL = "INSERT INTO " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones +
                            " (" + Ope_Com_Req_Observaciones.Campo_Observacion_ID +
                            ", " + Ope_Com_Req_Observaciones.Campo_Requisicion_ID +
                            ", " + Ope_Com_Req_Observaciones.Campo_Comentario +
                            ", " + Ope_Com_Req_Observaciones.Campo_Estatus +
                            ", " + Ope_Com_Req_Observaciones.Campo_Usuario_Creo +
                            ", " + Ope_Com_Req_Observaciones.Campo_Fecha_Creo +
                            ") VALUES (" +
                            Consecutivo + ",'" +
                            Id_Requisicion + "','" +
                            Clase_Negocio.P_Comentarios + "','" +
                            Dt_Parametros.Rows[0][Ope_Com_Requisiciones.Campo_Estatus].ToString()+ "','" +
                            Cls_Sessiones.Nombre_Empleado + "',GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    //Restamos al total obtenido de la Reserva de la requisicion de  origen o requisicion padre el total de la nueva requisicion
                    Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Psp_Reservas.Campo_Saldo + " = " + Ope_Psp_Reservas.Campo_Saldo  + " - " + Total; 
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva  + " = "  + Reserva_Requisicion_Origen;
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Registrar el Movimiento 
                    String Cargo = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
                    String Abono = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido; ;
                    Mi_SQL = "INSERT INTO " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos;
                    Mi_SQL = Mi_SQL + " (" + Ope_Psp_Registro_Movimientos.Campo_No_Reserva;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Cargo;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Abono;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Importe;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Fecha;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Usuario;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Usuario_Creo;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Fecha_Creo;
                    Mi_SQL = Mi_SQL + ") VALUES(";
                    Mi_SQL = Mi_SQL + Reserva_Requisicion_Origen;
                    Mi_SQL = Mi_SQL + ",'" + Cargo + "'";
                    Mi_SQL = Mi_SQL + ",'" + Abono + "'";
                    Mi_SQL = Mi_SQL + ",'" + Total.ToString() + "'";
                    Mi_SQL = Mi_SQL + ",GETDATE()";
                    Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
                    Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
                    Mi_SQL = Mi_SQL + ",GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Generamos la nueva reserva 
                    //Crear Reserva
                    int No_Reserva_Requisicion_Nueva = Obtener_Consecutivo(Ope_Psp_Reservas.Campo_No_Reserva, Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                        Mi_SQL = "INSERT INTO " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                        Mi_SQL = Mi_SQL + "(" + Ope_Psp_Reservas.Campo_No_Reserva;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Concepto;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fecha;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Dependencia_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fte_Financimiento_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Proyecto_Programa_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Partida_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Capitulo_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Anio;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Importe_Inicial;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Saldo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Fecha_Creo;
                        Mi_SQL = Mi_SQL + ") VALUES(";
                        Mi_SQL = Mi_SQL + No_Reserva_Requisicion_Nueva;
                        Mi_SQL = Mi_SQL + ",'" + "RQ-" + Id_Requisicion + "'";
                        Mi_SQL = Mi_SQL + ",GETDATE()";
                        Mi_SQL = Mi_SQL + ",'" + Dependencia_ID.Trim() + "'";
                        Mi_SQL = Mi_SQL + ",'" + Fuente_Financiamiento_ID.Trim() + "'";
                        Mi_SQL = Mi_SQL + ",'" + Programa_ID.Trim() + "'";
                        Mi_SQL = Mi_SQL + ",'" + Partida_ID.Trim() + "'";
                        Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID;
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto;
                        Mi_SQL = Mi_SQL + " JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas;
                        Mi_SQL = Mi_SQL + " ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID;
                        Mi_SQL = Mi_SQL + "=" + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID;
                        Mi_SQL = Mi_SQL + " JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
                        Mi_SQL = Mi_SQL + " ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID;
                        Mi_SQL = Mi_SQL + "=" + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID;
                        Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
                        Mi_SQL = Mi_SQL + "='" + Partida_ID.Trim() + "')";
                        Mi_SQL = Mi_SQL + ", " + DateTime.Now.Year.ToString();
                        Mi_SQL = Mi_SQL + ", " +  Convert.ToDouble(Total).ToString();
                        Mi_SQL = Mi_SQL + ", " +  Convert.ToDouble(Total).ToString();
                        Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
                        Mi_SQL = Mi_SQL + ",GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    //Actualizamos la requisicion nueva asignandole la reserva creada
                        Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                            " SET NUM_RESERVA = " + No_Reserva_Requisicion_Nueva +
                            " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Id_Requisicion;
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    //Registramos el movimiento de la nueva reserva 
                         Cargo = Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido;
                         Abono = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible;
                        Mi_SQL = "INSERT INTO " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos;
                        Mi_SQL = Mi_SQL + " (" + Ope_Psp_Registro_Movimientos.Campo_No_Reserva;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Cargo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Abono;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Importe;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Fecha;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Usuario;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Psp_Registro_Movimientos.Campo_Fecha_Creo;
                        Mi_SQL = Mi_SQL + ") VALUES(";
                        Mi_SQL = Mi_SQL + No_Reserva_Requisicion_Nueva;
                        Mi_SQL = Mi_SQL + ",'" + Cargo + "'";
                        Mi_SQL = Mi_SQL + ",'" + Abono + "'";
                        Mi_SQL = Mi_SQL + ",'" + Total.ToString() + "'";
                        Mi_SQL = Mi_SQL + ",GETDATE()";
                        Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
                        Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado + "'";
                        Mi_SQL = Mi_SQL + ",GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        Trans.Commit();     //Se Autoriza la transaccion

                        //REALIZAMOS EL INSERT EN LA TABLA DE HISTORIAL DE ESTATUS DE REQUISIONES 
                        Cls_Ope_Com_Requisiciones_Negocio Requisicion = new Cls_Ope_Com_Requisiciones_Negocio();
                        Requisicion.Registrar_Historial("DIVIDIDA", Id_Requisicion);
                        Requisicion.Registrar_Historial("FILTRADA", Id_Requisicion);
                        Mensaje_Error = Id_Requisicion;
                }//fin del if
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo guardar requisición";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }//fin de Convertir_Requisicion_Transitoria
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT  ISNULL(MAX (" + Campo_ID + "),0) FROM " + Tabla + " WITH(NOLOCK) ";
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Requisiciones_Hijas
        ///DESCRIPCIÓN          : Consulta las requisiciones que han sido creadas de la requisicion padre
        ///PARAMETROS           :  
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 05/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static int  Modificar_Requisicion_Original(Cls_Ope_Com_Dividir_Requisicion_Transitoria_Negocio Clase_Negocio)
        {
            int Filas_Modificadas = 0;
            //Consultamos las requisiciones listado 
            String Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
            Mi_SQL = Mi_SQL + " SET " + Ope_Com_Requisiciones.Campo_Estatus + " = 'DIVIDIDA'";
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL = Mi_SQL + " = " + Clase_Negocio.P_No_Requisicion_ID;

            Filas_Modificadas = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            Cls_Ope_Com_Requisiciones_Negocio Requisicion = new Cls_Ope_Com_Requisiciones_Negocio();
            Requisicion.Registrar_Historial("DIVIDIDA", Clase_Negocio.P_No_Requisicion_ID);
            return Filas_Modificadas;
        }
    }
}
