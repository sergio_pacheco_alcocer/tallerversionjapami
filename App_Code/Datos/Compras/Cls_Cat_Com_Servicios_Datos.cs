﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Catalogo_Compras_Servicios.Negocio;

namespace JAPAMI.Catalogo_Compras_Servicios.Datos
{
    public class Cls_Cat_Com_Servicios_Datos
    {

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Alta_Servicios
        ///DESCRIPCIÓN: Realiza una inscercion de un nuevo registro en la tabla de Servicios
        ///PARAMETROS: Datos: Objeto de la clase negocio
        ///CREO: jtoledo
        ///FECHA_CREO: 02/03/2011 11:13:04 a.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static void Alta_Servicios(Cls_Cat_Com_Servicios_Negocio Datos)
        {
            //Declaraion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL;
            String Clave_SQL;
            Object ID_Consecutivo;
            Object Ultima_Clave;

            //Inicializacion de variables
            Mi_SQL = String.Empty;
            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;
                        
                Mi_SQL = "SELECT ISNULL(MAX (" + Cat_Com_Servicios.Campo_Servicio_ID + "),'00000') " +
                         " FROM " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios;

                ID_Consecutivo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                
                if (!Convert.IsDBNull(ID_Consecutivo))      //Si la consulta no regresa nulo, obtener el nuevo ID
                {
                    Datos.P_Servicio_ID = string.Format("{0:00000}", Convert.ToInt32(ID_Consecutivo) + 1);
                    Datos.P_Clave =int.Parse(Datos.P_Servicio_ID).ToString();
                }

                        //Query para el alta del nuevo Servicio
                Mi_SQL = "INSERT INTO " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + " ( ";
                Mi_SQL += Cat_Com_Servicios.Campo_Servicio_ID + ",";
                Mi_SQL += Cat_Com_Servicios.Campo_Clave + ",";
                Mi_SQL += Cat_Com_Servicios.Campo_Nombre + ",";
                Mi_SQL += Cat_Com_Servicios.Campo_Costo + ",";
                Mi_SQL += Cat_Com_Servicios.Campo_Impuesto_ID + ",";
                Mi_SQL += Cat_Com_Servicios.Campo_Partida_ID + ",";
                Mi_SQL += Cat_Com_Servicios.Campo_Comentarios + ",";
                Mi_SQL += Cat_Com_Servicios.Campo_Cotizador_ID + ",";
                Mi_SQL += Cat_Com_Servicios.Campo_Usuario_Creo + ",";
                Mi_SQL += Cat_Com_Servicios.Campo_Fecha_Creo + " )";

                Mi_SQL += " VALUES( '";
                Mi_SQL += Datos.P_Servicio_ID + "','";
                Mi_SQL += Datos.P_Clave + "','";
                Mi_SQL += Datos.P_Nombre + "', ";
                Mi_SQL += Datos.P_Costo + ",'";
                Mi_SQL += Datos.P_Impuesto_ID + "','";
                Mi_SQL += Datos.P_Partida_ID + "','";
                Mi_SQL += Datos.P_Comentarios + "','";
                Mi_SQL += Datos.P_Cotizador_ID + "','";
                Mi_SQL += Datos.P_Usuario_Creo + "', GETDATE() )";

                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                String Mensaje = "Error: ";
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "No existe un registro relacionado con esta operacion";
                        break;
                    case "923":
                        Mensaje = "Consulta SQL";
                        break;
                    case "12170":
                        Mensaje = "Conexion con el Servidor";
                        break;
                    default:
                        Mensaje = Ex.Message;
                        break;
                }
                throw new Exception(Mensaje + "[" + Ex.ToString() + "]");
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;

            }
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Consulta_Servicios
        ///DESCRIPCIÓN: Realizar la consulta de la lista de servicios filtrada por el nombre del servicio
        ///PARAMETROS: Datos: Objeto de la clase negocio
        ///CREO: jtoledo
        ///FECHA_CREO: 02/04/2011 12:07:35 p.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************        
        public static DataTable Consulta_Servicios(Cls_Cat_Com_Servicios_Negocio Datos)
        {
            DataTable Dt_Temporal = null;
            String Mi_SQL;
            String Filtros_SQL = String.Empty;

            try
            {
                Mi_SQL = "SELECT " +
                         Cat_Com_Servicios.Campo_Servicio_ID + ", " +
                         Cat_Com_Servicios.Campo_Clave + ", " +
                         Cat_Com_Servicios.Campo_Nombre + ", " +
                         Cat_Com_Servicios.Campo_Costo + ", " +
                         Cat_Com_Servicios.Campo_Impuesto_ID + ", " +
                         Cat_Com_Servicios.Campo_Partida_ID + ", " +
                         Cat_Com_Servicios.Campo_Comentarios + ", " +
                         Cat_Com_Servicios.Campo_Cotizador_ID + ", " +

                         Cat_Com_Servicios.Campo_Usuario_Creo + ", " +
                         Cat_Com_Servicios.Campo_Fecha_Creo +

                    " FROM " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios;

                if (Datos.P_Nombre != null && Datos.P_Nombre != "")
                {
                    Filtros_SQL = " WHERE upper(" + Cat_Com_Servicios.Campo_Nombre + ") LIKE upper('%" +
                    Datos.P_Nombre + "%')";
                }
                if (Datos.P_Clave != null && Datos.P_Clave != "")
                {
                    if (Filtros_SQL.Length > 0)
                    Filtros_SQL += " OR upper(" + Cat_Com_Servicios.Campo_Clave + ") LIKE upper('" +
                    Datos.P_Clave + "%')";
                    else
                        Filtros_SQL += " WHERE upper(" + Cat_Com_Servicios.Campo_Clave + ") LIKE upper('" +
                        Datos.P_Clave + "%')";
                }
                Dt_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL + Filtros_SQL).Tables[0];
                return Dt_Temporal;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Baja_Servicios
        ///DESCRIPCIÓN: Ejecuta sentencia para dar de baja un servicio de la base de datos
        ///PARAMETROS: Datos: Objeto de la clase negocio
        ///CREO: jtoledo
        ///FECHA_CREO: 02/04/2011 11:38:06 a.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************        
        internal static void Baja_Servicios(Cls_Cat_Com_Servicios_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Asignar consulta para la baja
                Mi_SQL = "DELETE FROM " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + " ";
                Mi_SQL = Mi_SQL + "WHERE " + Cat_Com_Servicios.Campo_Servicio_ID + " = '" + Datos.P_Servicio_ID + "'";

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Ejecutar transaccion
                Obj_Transaccion.Commit();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Modificar_Servicio
        ///DESCRIPCIÓN: Ejecuta la sentencia para modificar un registro de un Servicio y lo guarda en la base de datos
        ///PARAMETROS: Datos: Objeto de la clase negocio
        ///CREO: jtoledo
        ///FECHA_CREO: 02/04/2011 12:05:38 p.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************        
        public static void Modificar_Servicio(Cls_Cat_Com_Servicios_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty; //Variable para el mensaje de error
            String Clave_SQL;
            Object Ultima_Clave;

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Si P_Clave esta vacia, generar nueva clave
                if (Datos.P_Clave == "")
                {
                    //Obtener la última clave (PARTIDAID + S + Consecutivo)
                    Clave_SQL = "SELECT * FROM (SELECT " + Cat_Com_Servicios.Campo_Clave;
                    Clave_SQL = Clave_SQL + " FROM " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios;
                    Clave_SQL = Clave_SQL + " WHERE " + Cat_Com_Servicios.Campo_Clave + " LIKE '" + Datos.P_Clave_Partida + "S%'";
                    Clave_SQL = Clave_SQL + " ORDER BY " + Cat_Com_Servicios.Campo_Clave + " DESC";
                    Clave_SQL = Clave_SQL + ") WHERE ROWNUM = 1";
                    Ultima_Clave = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Clave_SQL);
                    if (Ultima_Clave != null)    // Si en la base de datos se encontró una clave con la partida proporcionada,
                    {                                       // sumar 1 al consecutivo para obtener la nueva clave
                        Ultima_Clave = Ultima_Clave.ToString().Substring(Ultima_Clave.ToString().Length - 4);  //Ultimos 4 caracteres del dato obtenido
                        Datos.P_Clave = Datos.P_Clave_Partida + "S" + String.Format("{0:0000}", Convert.ToInt32(Ultima_Clave) + 1);
                    }
                    else                    // Si no se encontraron datos, asignar el primer para la partida
                    {
                        Datos.P_Clave = Datos.P_Clave_Partida + "S0001";
                    }
                }

                //Asignar consulta para la modificacion
                Mi_SQL = "UPDATE " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios;
                Mi_SQL = Mi_SQL + " SET " + Cat_Com_Servicios.Campo_Nombre + " = '" + Datos.P_Nombre + "', ";
                Mi_SQL = Mi_SQL + Cat_Com_Servicios.Campo_Comentarios + " = '" + Datos.P_Comentarios + "', ";
                Mi_SQL = Mi_SQL + Cat_Com_Servicios.Campo_Cotizador_ID + " = '" + Datos.P_Cotizador_ID + "', ";
                Mi_SQL = Mi_SQL + Cat_Com_Servicios.Campo_Costo + " = '" + Datos.P_Costo + "', ";
                Mi_SQL = Mi_SQL + Cat_Com_Servicios.Campo_Clave + " = '" + Datos.P_Clave + "', ";
                Mi_SQL = Mi_SQL + Cat_Com_Servicios.Campo_Partida_ID + " = '" + Datos.P_Partida_ID + "', ";
                Mi_SQL = Mi_SQL + Cat_Com_Servicios.Campo_Impuesto_ID + " = '" + Datos.P_Impuesto_ID + "', ";
                Mi_SQL = Mi_SQL + Cat_Com_Servicios.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ";
                Mi_SQL = Mi_SQL + Cat_Com_Servicios.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL = Mi_SQL + "WHERE " + Cat_Com_Servicios.Campo_Servicio_ID + " = '" + Datos.P_Servicio_ID + "'";

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Ejecutar transaccion
                Obj_Transaccion.Commit();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }
    }
}