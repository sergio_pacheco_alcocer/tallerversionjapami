﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Productos_Almacen.Negocio;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;

/// <summary>
/// Summary description for Cls_Rpt_Alm_Productos_Datos
/// </summary>
namespace JAPAMI.Productos_Almacen.Datos
{
    public class Cls_Rpt_Alm_Productos_Datos
    {
        public Cls_Rpt_Alm_Productos_Datos()
        {
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Unidades
        ///DESCRIPCION:             Consulta de las unidades
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              21/Marzo/2012 10:41
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Unidades()
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataTable Dt_Unidades = new DataTable(); //Tabla para el resultado de la consulta

            try
            {
                //Asignar Consulta
                Mi_SQL = "SELECT Unidad_ID, Nombre FROM Cat_Com_Unidades ORDER BY Nombre";

                //Ejecutar consulta
                Dt_Unidades = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado 
                return Dt_Unidades;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Impuestos
        ///DESCRIPCION:             Consulta de los impuestos
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              21/Marzo/2012 10:49
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Impuestos()
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //Variable para la consulta
            DataTable Dt_Impuestos = new DataTable(); //Tabla para el resultado

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT Impuesto_ID, Nombre FROM Cat_Com_Impuestos ORDER BY Nombre ";

                //Ejecutatr consulta
                Dt_Impuestos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado 
                return Dt_Impuestos;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Tipos
        ///DESCRIPCION:             Consultar los tipos de Productos
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              21/Marzo/2012 11:22
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Tipos()
        {
            //Declaracion de variables 
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataTable Dt_Tipos = new DataTable(); //Tabla para el resultado

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT DISTINCT ISNULL(Tipo, 'NULO') AS Tipo FROM Cat_Com_Productos ORDER BY Tipo ";

                //Ejecutar consulta
                Dt_Tipos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado 
                return Dt_Tipos;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Partidas_Genericas
        ///DESCRIPCION:             Consultar las partidas genericas
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              21/Marzo/2012 16:54
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Partidas_Genericas()
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para la consulta
            DataTable Dt_Partidas_Genericas = new DataTable(); //Tabla para el resultado

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT Partida_Generica_ID, (Clave + ' ' + Descripcion) AS Nombre FROM Cat_Sap_partida_Generica ORDER BY Descripcion ";

                //Ejecutar consulta
                Dt_Partidas_Genericas = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Partidas_Genericas;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Partidas_Especificas
        ///DESCRIPCION:             Consultar las partidas especificas
        ///PARAMETROS:              Datos: Variable de la capa de negocios
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              21/Marzo/2012 17:22
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Partidas_Especificas(Cls_Rpt_Alm_Productos_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para la consulta
            DataTable Dt_Partidas_Especificas = new DataTable(); //Tabla para el resultado

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT Partida_ID, (Clave + ' ' + Descripcion) AS Nombre FROM Cat_Sap_Partidas_Especificas " +
                    "WHERE Partida_Generica_ID = '" + Datos.P_Partida_Generica_ID.Trim() + "' " +
                    "ORDER BY Descripcion ";

                //Ejecutar consulta
                Dt_Partidas_Especificas = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Partidas_Especificas;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Productos
        ///DESCRIPCION:             Consultar los productos con filtros
        ///PARAMETROS:              Datos: Variable de la capa de negocios
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              21/Marzo/2012 13:59
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Productos(Cls_Rpt_Alm_Productos_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL; //variable para la consulta
            DataTable Dt_Productos = new DataTable(); //Variable para el resultado de la consulta
            Boolean Where_Utilizado = false; //Variable que indica si la clausula where ya ha sido utilizada

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT Cat_Com_Productos.Producto_ID, Cat_Com_Productos.Clave, Cat_Com_Productos.Ref_JAPAMI, Cat_Com_Productos.Nombre, " +
                    "(Cat_SAP_Partida_Generica.Clave + ' ' + Cat_SAP_Partida_Generica.Descripcion) AS Partida_Generica, " +
                    "(Cat_SAP_Partidas_Especificas.Clave + ' ' + Cat_SAP_Partidas_Especificas.Descripcion) AS Partida_Especifica, " +
                    "Cat_Com_Productos.Descripcion, Cat_Com_Impuestos.Nombre AS Impuesto_1, Cat_Com_Impuestos_2.Nombre AS Impuesto_2, Cat_Com_Productos.Costo, " +
                    "Cat_Com_Productos.Costo_Promedio, Cat_Com_Productos.P_Estatus, Cat_Com_Productos.Existencia, Cat_Com_Productos.Comprometido, " +
                    "Cat_Com_Productos.Disponible, Cat_Com_Productos.Maximo, Cat_Com_Productos.Minimo, Cat_Com_Productos.Reorden, Cat_Com_Productos.Ubicacion, " +
                    "Cat_Com_Productos.Tipo, Cat_Com_Productos.Stock, " +
                    "('PG: ' + Cat_SAP_Partida_Generica.Clave + ' ' + Cat_SAP_Partida_Generica.Descripcion + '<br />' + " +
                    "'PE: ' + Cat_SAP_Partidas_Especificas.Clave + ' ' + Cat_SAP_Partidas_Especificas.Descripcion) AS Partida, " +
                    "(Cat_Com_Productos.Nombre + ' ' + Cat_Com_Productos.Descripcion) AS Nombre_Completo, " +
                    "Cat_Com_Unidades.Nombre AS Unidad " +
                    "FROM Cat_Com_Productos " +
                    "LEFT JOIN Cat_Com_Impuestos ON Cat_Com_Productos.Impuesto_ID = Cat_Com_Impuestos.Impuesto_ID " +
                    "LEFT JOIN Cat_Com_Impuestos Cat_Com_Impuestos_2 ON Cat_Com_Productos.Impuesto_2_ID = Cat_Com_Impuestos_2.Impuesto_ID " +
                    "LEFT JOIN Cat_SAP_Partidas_Especificas ON Cat_Com_Productos.Partida_ID = Cat_SAP_Partidas_Especificas.Partida_ID " +
                    "LEFT JOIN Cat_SAP_Partida_Generica ON Cat_SAP_Partidas_Especificas.Partida_Generica_ID = Cat_SAP_Partida_Generica.Partida_Generica_ID " +
                    "LEFT JOIN Cat_Com_Unidades ON Cat_Com_Productos.Unidad_ID = Cat_Com_Unidades.Unidad_ID ";

                //Verificar el tipo de la consulta
                switch (Datos.P_Tipo_Consulta)
                {
                    case "Producto_ID":
                        Mi_SQL += "WHERE Cat_Com_Productos.Producto_ID = '" + Datos.P_Producto_ID.Trim() + "' ";
                        break;

                    case "Clave":
                        Mi_SQL += "WHERE Cat_Com_Productos.Clave = '" + Datos.P_Clave.Trim() + "' ";
                        break;

                    case "Ref_JAPAMI":
                        Mi_SQL += "WHERE Cat_Com_Productos.Ref_JAPAMI = " + Datos.P_Ref_JAPAMI.ToString().Trim() + " ";
                        break;

                    case "Nombre":
                        Mi_SQL += "WHERE Cat_Com_Productos.Nombre LIKE '%" + Datos.P_Nombre.Trim() + "%'";
                        break;

                    default:
                        //Verificar si se ha seleccionado una unidad
                        if (Datos.P_Unidad_ID != null && Datos.P_Unidad_ID != "" && Datos.P_Unidad_ID != String.Empty)
                        {
                            //Verificar si es la opcion nula
                            if (Datos.P_Unidad_ID == "NULO")
                            {
                                Mi_SQL += "WHERE Cat_Com_Productos.Unidad_ID IS NULL ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE Cat_Com_Productos.Unidad_ID = '" + Datos.P_Unidad_ID.Trim() + "' ";
                            }

                            Where_Utilizado = true;
                        }

                        //Verificar si se ha seleccionado un impuesto
                        if (Datos.P_Impuesto_ID != null && Datos.P_Impuesto_ID != "" && Datos.P_Impuesto_ID != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Resto del filtro
                            //Verificar si es la opcion nula
                            if (Datos.P_Impuesto_ID == "NULO")
                            {
                                Mi_SQL += "(Cat_Com_Productos.Impuesto_ID IS NULL AND Cat_Com_Productos.Impuesto_2_ID IS NULL) ";                            
                            }
                            else
                            {
                                Mi_SQL += "(Cat_Com_Productos.Impuesto_ID = '" + Datos.P_Impuesto_ID + "' " +
                                    "OR Cat_Com_Productos.Impuesto_2_ID = '" + Datos.P_Impuesto_ID + "') ";
                            }
                        }

                        //Verificar si se ha seleccionado un estatus
                        if (Datos.P_Estatus != null && Datos.P_Estatus != "" && Datos.P_Estatus != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Resto del filtro
                            Mi_SQL += "Cat_Com_Productos.P_Estatus = '" + Datos.P_Estatus.Trim() + "' ";
                        }

                        //Verificar si se ha seleccionado un tipo
                        if (Datos.P_Tipo != null && Datos.P_Tipo != "" && Datos.P_Tipo != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Resto del filtro
                            //Verificar si es la opcion nula
                            if (Datos.P_Tipo == "NULO")
                            {
                                Mi_SQL += "Cat_Com_Productos.Tipo IS NULL ";
                            }
                            else
                            {
                                Mi_SQL += "Cat_Com_Productos.Tipo = '" + Datos.P_Tipo.Trim() + "' ";
                            }
                        }

                        //Verificar si se ha seleccionado el stock
                        if (Datos.P_Stock != null && Datos.P_Stock != "" && Datos.P_Stock != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Resto del filtro
                            Mi_SQL = "Cat_Com_Productos.Stock = '" + Datos.P_Stock.Trim() + "' ";
                        }

                        //Verificar si se ha seleccionado una partida generica
                        if (Datos.P_Partida_Generica_ID != null && Datos.P_Partida_Generica_ID != "" && Datos.P_Partida_Generica_ID != String.Empty)
                        {
                            //verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";

                                Where_Utilizado = true;
                            }

                            //Verificar si es la opcion nula
                            if (Datos.P_Partida_Generica_ID == "NULO")
                            {
                                Mi_SQL += "Cat_Com_Productos.Partida_ID IS NULL ";
                            }
                            else
                            {
                                //Verificar si se ha seleccionado una partida especifica
                                if (Datos.P_Partida_Especifica_ID != null && Datos.P_Partida_Especifica_ID != "" && Datos.P_Partida_Especifica_ID != String.Empty)
                                {
                                    Mi_SQL += "Cat_Com_Productos.Partida_ID = '" + Datos.P_Partida_Especifica_ID.Trim() + "' ";
                                }
                                else
                                {
                                    Mi_SQL += "Cat_Com_Partidas_Especificas.Partida_Generica_ID = '" + Datos.P_Partida_Generica_ID.Trim() + "' ";
                                }
                            }
                        }
                        break;
                }

                //Ejecutar consulta
                Dt_Productos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Productos;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
    }
}