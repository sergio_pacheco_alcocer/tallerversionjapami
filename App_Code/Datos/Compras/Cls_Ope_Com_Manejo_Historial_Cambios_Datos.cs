﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Compras.Manejo_Historial_Cambios.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Ope_Pat_Manejo_Historial_Cambios_Datos
/// </summary>
/// 

namespace JAPAMI.Compras.Manejo_Historial_Cambios.Datos
{
    public class Cls_Ope_Com_Manejo_Historial_Cambios_Datos
    {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_DataTable
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                         1.  Parametros.  Contiene los parametros que se van a utilizar para
        ///                                             hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 23/Enero/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Cambios(Cls_Ope_Com_Manejo_Historial_Cambios_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Registros = null;
            DataTable Dt_Registros = new DataTable();
            try
            {
                Mi_SQL = "SELECT HISTORICO." + Ope_Com_Historial_Cambios.Campo_No_Registro + " AS NUMERO_REGISTRO";
                Mi_SQL += ", HISTORICO." + Ope_Com_Historial_Cambios.Campo_No_Cambio + " AS NUMERO_CAMBIO";
                Mi_SQL += ", HISTORICO." + Ope_Com_Historial_Cambios.Campo_Fecha_Hora_Cambio + " AS FECHA_CAMBIO";
                Mi_SQL += ", (EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + " + ' - ' + EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + " + ' ' + EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + " + ' ' + EMPLEADOS." + Cat_Empleados.Campo_Nombre + ") AS EMPLEADO_CAMBIO";
                Mi_SQL += ", (HISTORICO." + Ope_Com_Historial_Cambios.Campo_No_Requisicion + ") AS NO_REQUISICION";
                Mi_SQL += ", (REQUISICIONES." + Ope_Com_Requisiciones.Campo_No_Orden_Compra + ") AS ORDEN_SERVICIO";
                Mi_SQL += ", HIS_DET." + Ope_Com_Hist_Cam_Det.Campo_Campo + " AS CAMPO";
                Mi_SQL += ", HIS_DET." + Ope_Com_Hist_Cam_Det.Campo_Valor_Anterior + " AS VALOR_ANTERIOR";
                Mi_SQL += ", HIS_DET." + Ope_Com_Hist_Cam_Det.Campo_Valor_Nuevo + " AS VALOR_NUEVO";
                Mi_SQL += " FROM " + Ope_Com_Historial_Cambios.Tabla_Ope_Com_Historial_Cambios + " HISTORICO";
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS ON HISTORICO." + Ope_Pat_Historial_Cambios.Campo_Empleado_Cambio_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + "";
                Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Hist_Cam_Det.Tabla_Ope_Com_Hist_Cam_Det + " HIS_DET ON HISTORICO." + Ope_Com_Historial_Cambios.Campo_No_Registro + " = HIS_DET." + Ope_Com_Hist_Cam_Det.Campo_No_Registro + "";
                Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICIONES ON HISTORICO." + Ope_Com_Historial_Cambios.Campo_No_Requisicion + " = REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + "";
                Mi_SQL += " WHERE HISTORICO." + Ope_Com_Historial_Cambios.Campo_No_Requisicion + " = '" + Parametros.P_No_Requisicion + "'";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Registros = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Registros != null)
                {
                    Dt_Registros = Ds_Registros.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Registros;
        }
    }
}