﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Almacen_Recepcion_Material.Negocio;
using JAPAMI.Constantes;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;
using JAPAMI.Bitacora_Eventos;
using JAPAMI.Generar_Requisicion.Negocio;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Polizas.Negocios;
using System.Globalization;

/// <summary>
/// Summary description for Cls_Alm_Com_Recepcion_Material_Datos
/// </summary>
namespace JAPAMI.Almacen_Recepcion_Material.Datos
{
    public class Cls_Alm_Com_Recepcion_Material_Datos
    {
        public Cls_Alm_Com_Recepcion_Material_Datos()
        {
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Proveedores
        /// DESCRIPCION:            Consultar los datos de los proveedores
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene 
        ///                         los datos para la busqueda
        /// CREO       :            Salvador Hernández Ramírez
        /// FECHA_CREO :            26/Febrero/2011 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consultar_IVA_Producto(Cls_Alm_Com_Recepcion_Material_Negocio Datos)
        {

            // Declaracion de variables
            String Mi_SQL = String.Empty;
            try
            {
                //Asignar consulta
                Mi_SQL = " SELECT " + Cat_Com_Impuestos.Campo_Porcentaje_Impuesto;
                Mi_SQL += " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos;
                Mi_SQL += " WHERE " + Cat_Com_Impuestos.Campo_Impuesto_ID + " = ";
                Mi_SQL += "(SELECT " + Cat_Com_Productos.Campo_Impuesto_ID + " FROM ";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " WHERE ";
                Mi_SQL += Cat_Com_Productos.Campo_Producto_ID + "='" +  String.Format("{0:0000000000}", Convert.ToInt64(Datos.P_Producto.Trim())) + "')";

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Proveedores
        /// DESCRIPCION:            Consultar los datos de los proveedores
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene 
        ///                         los datos para la busqueda
        /// CREO       :            Salvador Hernández Ramírez
        /// FECHA_CREO :            26/Febrero/2011 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Proveedores(Cls_Alm_Com_Recepcion_Material_Negocio Datos)
        {
            // Declaracion de variables
            String Mi_SQL = String.Empty;
            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT DISTINCT " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + " ";
                Mi_SQL += " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + ", " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra;
                Mi_SQL += " WHERE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID;
                Mi_SQL += " = " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + " ";
                Mi_SQL += " and " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Estatus + " IN ('PROVEEDOR_ENTERADO','SURTIDA_PARCIAL') ";
                Mi_SQL += "ORDER BY " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre; //Ordenamiento

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Ordenes_Compra
        /// DESCRIPCION:            Método utilizado para consultar las ordenes de compra que se encuentren en estatus "COMPRA"
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene 
        ///                         los datos para la busqueda
        /// CREO       :            Salvador Hernandez Ramirez
        /// FECHA_CREO :            28/Febrero/2010  
        /// MODIFICO          :     Jennyfer Ivonne Ceja Lemus
        /// FECHA_MODIFICO    :     01/Noviembre/2012
        /// CAUSA_MODIFICACION:     Se agrego el filtro Almacen General
        ///*******************************************************************************/
        public static DataTable Consulta_Ordenes_Compra(Cls_Alm_Com_Recepcion_Material_Negocio Datos)
        {
            String Mi_SQL = String.Empty;
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT DISTINCT ORDEN." + Ope_Com_Ordenes_Compra.Campo_Folio;
                Mi_SQL += ",REQ." + Ope_Com_Requisiciones.Campo_Folio + " AS FOLIO_REQ";
                Mi_SQL += ",PROV." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR";
                Mi_SQL += ",ORDEN." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo + " AS FECHA";
                Mi_SQL += ",ORDEN." + Ope_Com_Ordenes_Compra.Campo_Total;
                Mi_SQL += ",ORDEN." + Ope_Com_Ordenes_Compra.Campo_Estatus;
                Mi_SQL += ",ORDEN." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                Mi_SQL += ",REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID;
                Mi_SQL += ",ISNULL(REQ." + Ope_Com_Requisiciones.Campo_Listado_Almacen + ", 'NO') AS LISTADO_ALMACEN";
                Mi_SQL += ",REQ." + Ope_Com_Requisiciones.Campo_Usuario_Creo;
                Mi_SQL += ",DEP." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                Mi_SQL += ",REQ." + Ope_Com_Requisiciones.Campo_Justificacion_Compra;
                Mi_SQL += " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " ORDEN ";
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROV ";
                Mi_SQL += "ON ORDEN." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + " = PROV." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ ";
                Mi_SQL += "ON ORDEN." + Ope_Com_Ordenes_Compra.Campo_Lista_Requisiciones + " = REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PROD ";
                Mi_SQL += "ON REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PROD ";
                Mi_SQL += "ON PROD." + Cat_Com_Productos.Campo_Producto_ID + " = REQ_PROD." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP ";
                Mi_SQL += "ON REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID + " = DEP." + Cat_Dependencias.Campo_Dependencia_ID;
                if (!String.IsNullOrEmpty(Datos.P_Producto))
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += " (UPPER(REQ_PROD." + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio + ") LIKE UPPER('%" + Datos.P_Producto + "%')";
                    Mi_SQL += " OR UPPER(REQ_PROD." + Ope_Com_Req_Producto.Campo_Clave + ") LIKE UPPER('%" + Datos.P_Producto + "%'))";
                }
                if (!String.IsNullOrEmpty(Datos.P_Estatus))
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += " ORDEN." + Ope_Com_Ordenes_Compra.Campo_Estatus + " IN (" + Datos.P_Estatus + ")";
                }
                if (Datos.P_No_Orden_Compra != null)
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += " ORDEN." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " LIKE '%" + Datos.P_No_Orden_Compra + "%' ";
                }
                if (Datos.P_No_Requisicion != null)
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += " REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " LIKE '%" + Datos.P_No_Requisicion + "%' ";
                }
                if (Datos.P_Proveedor_ID != null)
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += " ORDEN." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "'";
                }
                if ((Datos.P_Fecha_Inicio_B != null) && (Datos.P_Fecha_Fin_B != null))
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += " ORDEN." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo;
                    Mi_SQL += " BETWEEN '" + Datos.P_Fecha_Inicio_B + "'" + " AND '" + Datos.P_Fecha_Fin_B + "'";
                }
                if (!String.IsNullOrEmpty(Datos.P_Almacen_General))
                {
                    Mi_SQL += Entro_Where ? " AND " : " WHERE "; Entro_Where = true;
                    Mi_SQL += " PROD." + Cat_Com_Productos.Campo_Almacen_General + " IN (" + Datos.P_Almacen_General + ") ";
                }
                Mi_SQL += " ORDER BY ORDEN." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Servicios_OC
        /// DESCRIPCION:            Consultar los servicios de una orden de compra
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene 
        ///                         el numero de orden de compra
        /// CREO       :            Salvador Hernández Ramírez
        /// FECHA_CREO :            19/Julio/2011 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Servicios_OC(Cls_Alm_Com_Recepcion_Material_Negocio Datos)
        {
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataTable Dt_Servicios = new DataTable(); //Tabla para los servicios de la orden de compra

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Clave + ", ";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Nombre + " as PRODUCTO, ";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Descripcion + ", ";
                Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Importe + " as COSTO_REAL, ";
                Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Monto_Total + " as MONTO ";
                Mi_SQL += " FROM ";
                Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + ", ";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL += "WHERE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                Mi_SQL += " = " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_No_Orden_Compra + " ";
                Mi_SQL += "AND " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID;
                Mi_SQL += " = " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Producto_ID + " ";
                Mi_SQL += " AND " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                Mi_SQL += " = " + Datos.P_No_Orden_Compra + " ";

                //Ejecutar consulta
                Dt_Servicios = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Servicios;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Orden_Compra_Detalles
        /// DESCRIPCION:            Consultar los detalles de una orden de compra
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene 
        ///                         el numero de orden de compra
        /// CREO       :            Noe Mosqueda Valadez
        /// FECHA_CREO :            30/Diciembre/2010 13:52 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Orden_Compra_Detalles(Cls_Alm_Com_Recepcion_Material_Negocio Datos)
        {
            // Declarar variables
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataTable Dt_Detalles_Orden_Compra = new DataTable(); //Tabla para el resultado
            DataTable Dt_Productos = new DataTable(); //Tabla para los productos de la orden de compra
            DataTable Dt_Servicios = new DataTable(); //Tabla para los servicios de la orden de compra
            DataRow Renglon; //Renglon para el llenado de al tabla
            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Producto_ID + ", ";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Clave + ", ";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Nombre + " as PRODUCTO, ";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Descripcion + ", ";
                Mi_SQL += " ( SELECT " + Cat_Com_Modelos.Campo_Nombre + " FROM " + Cat_Com_Modelos.Tabla_Cat_Com_Modelos + " WHERE ";
                Mi_SQL += Cat_Com_Modelos.Campo_Modelo_ID + " = " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Modelo_ID + ")  as MODELO ,";
                Mi_SQL += " ( SELECT " + Cat_Com_Marcas.Campo_Nombre + " FROM " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " WHERE ";
                Mi_SQL += Cat_Com_Marcas.Campo_Marca_ID + " = " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Marca_ID + ")  as MARCA ,";
                Mi_SQL += "( SELECT " + Cat_Com_Unidades.Campo_Nombre + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " WHERE ";
                Mi_SQL += Cat_Com_Unidades.Campo_Unidad_ID + " = " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Unidad_ID + ")  as UNIDAD,";
                Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Cantidad + ", ";
                //                Mi_SQL += Cat_Com_Unidades.Tabla_Cat_Com_Unidades + "." + Cat_Com_Unidades.Campo_Nombre + " as UNIDAD, ";
                Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado + " as COSTO_REAL, ";
                Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado + " as MONTO, ";
                Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + ".CANTIDAD_RECIBIDA, ";
                Mi_SQL += "TIPO = 'PRODUCTO', ";
                Mi_SQL += "SELECCIONADA = 'SI' FROM ";
                Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + ", ";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL += " WHERE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                Mi_SQL += " = " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_No_Orden_Compra + " ";
                Mi_SQL += "AND " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID;
                Mi_SQL += " = " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Producto_ID + " ";
                //Mi_SQL += "AND " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Unidad_ID;
                //Mi_SQL += " = " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + "." + Cat_Com_Unidades.Campo_Unidad_ID + " ";
                Mi_SQL += " AND " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                Mi_SQL += " = " + Datos.P_No_Orden_Compra + " ";

                //Ejecutar consulta
                Dt_Productos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Clonar tabla
                Dt_Detalles_Orden_Compra = Dt_Productos.Clone();
                Dt_Detalles_Orden_Compra.TableName = "Detalles_Orden_Compra";

                //Ciclo para el llenado de la tabla del resultado con los productos y servicios de la orden de compra
                for (int Cont_Elementos = 0; Cont_Elementos < Dt_Productos.Rows.Count; Cont_Elementos++)
                {
                    //Instanciar renglon e ingresarlo a al tabla
                    Renglon = Dt_Productos.Rows[Cont_Elementos];
                    Dt_Detalles_Orden_Compra.ImportRow(Renglon);
                }
                //Entregar el resultado
                return Dt_Detalles_Orden_Compra;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        /////*******************************************************************************
        ///// NOMBRE DE LA CLASE:     Consultar_Productos_Resguardados
        ///// DESCRIPCION:            Metodo utilizado para consultar los productos de las ordenes de compra resuardadas
        ///// PARAMETROS :            Datos: Variable de la capa de negocios 
        /////                         
        ///// CREO       :            Salvador Hernández Ramírez
        ///// FECHA_CREO :            10/Febrero/2011
        ///// MODIFICO          :
        ///// FECHA_MODIFICO    :
        ///// CAUSA_MODIFICACION:
        /////*******************************************************************************/
        //public static DataTable Consultar_Productos_Resguardados(Cls_Alm_Com_Recepcion_Material_Negocio Datos)
        //{
        //    DataTable Dt_Productos_Resguardados = new DataTable();

        //    //Declaracion de variables
        //    String Mensaje = "";
        //    String Mi_SQL = String.Empty;
        //    SqlConnection Cn = new SqlConnection();
        //    SqlCommand Cmd = new SqlCommand();
        //    SqlTransaction Trans;
        //    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
        //    Cn.Open();
        //    Trans = Cn.BeginTransaction();
        //    Cmd.Connection = Cn;
        //    Cmd.Transaction = Trans;
        //    SqlDataAdapter Da = new SqlDataAdapter(); //Adaptador para el llenado de los datatable

        //    try
        //    {
        //        //Ciclo para el barrido de la tabla de las ordenes de compra
        //        for (int Cont_Elementos = 0; Cont_Elementos < Datos.P_Dt_Ordenes_Compra.Rows.Count; Cont_Elementos++)
        //        {
        //            //Asignar consulta para los detalles de la orden de compra
        //            Mi_SQL = "SELECT  Req_Producto." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " ";
        //            Mi_SQL += ", Req_Producto." + Ope_Com_Req_Producto.Campo_Cantidad + " ";
        //            Mi_SQL += ", Req_Producto." + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio + " as PRODUCTO";
        //            Mi_SQL += ", Req_Producto." + Ope_Com_Req_Producto.Campo_Nombre_Proveedor + " as PROVEEDOR";
        //            Mi_SQL += ", Req_Producto." + Ope_Com_Req_Producto.Campo_Importe + " as PRECIO";
        //            Mi_SQL += ", Req_Producto." + Ope_Com_Req_Producto.Campo_Monto_Total + " as TOTAL";
        //            Mi_SQL += ", Req_Producto." + Ope_Com_Req_Producto.Campo_Clave + " ";
        //            Mi_SQL += ", Req_Producto." + Ope_Com_Req_Producto.Campo_No_Orden_Compra + " as ORDEN_COMPRA";
        //            Mi_SQL += ", Facturas_Proveedor." + Ope_Com_Facturas_Proveedores.Campo_No_Factura_Proveedor + " as NO_FACTURA";
        //            Mi_SQL += ", Facturas_Proveedor." + Ope_Com_Facturas_Proveedores.Campo_Fecha_Recepcion + " ";
        //            Mi_SQL += ", Facturas_Proveedor." + Ope_Com_Facturas_Proveedores.Campo_SubTotal_Sin_Impuesto + " as SUBTOTAL ";
        //            Mi_SQL += ", Facturas_Proveedor." + Ope_Com_Facturas_Proveedores.Campo_IVA + " ";
        //            Mi_SQL += ", Facturas_Proveedor." + Ope_Com_Facturas_Proveedores.Campo_IEPS + " ";
        //            Mi_SQL += ", Facturas_Proveedor." + Ope_Com_Facturas_Proveedores.Campo_Total + " as TOTAL_FACTURA";
        //            Mi_SQL += ", Marcas." + Cat_Com_Marcas.Campo_Nombre + " as MARCA";
        //            Mi_SQL += ", Modelos." + Cat_Com_Modelos.Campo_Nombre + " as MODELO";
        //            Mi_SQL += ", Unidades." + Cat_Com_Unidades.Campo_Nombre + " as UNIDAD";
        //            Mi_SQL += ", Empleado." + Cat_Empleados.Campo_Nombre + "  +' '+";
        //            Mi_SQL += " Empleado." + Cat_Empleados.Campo_Apellido_Paterno + "  +' '+";
        //            Mi_SQL += " Empleado." + Cat_Empleados.Campo_Apellido_Materno + "  as EMPLEADO_RESGUARDO";

        //            Mi_SQL += " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " Req_Producto ";
        //            Mi_SQL += " join " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " Ordenes_Compra ";
        //            Mi_SQL += " on Req_Producto." + Ope_Com_Req_Producto.Campo_No_Orden_Compra + " ";
        //            Mi_SQL += " = Ordenes_Compra." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " ";
        //            Mi_SQL += " join " + Ope_Com_Facturas_Proveedores.Tabla_Ope_Com_Facturas_Proveedores + " Facturas_Proveedor ";
        //            Mi_SQL += " on Facturas_Proveedor." + Ope_Com_Facturas_Proveedores.Campo_No_Factura_Interno + " ";
        //            Mi_SQL += " = Ordenes_Compra." + Ope_Com_Ordenes_Compra.Campo_No_Factura_Interno + " ";
        //            Mi_SQL += " join " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " Productos";
        //            Mi_SQL += " on Req_Producto." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " ";
        //            Mi_SQL += " = Productos." + Cat_Com_Productos.Campo_Producto_ID + " ";
        //            Mi_SQL += " join " + Cat_Com_Modelos.Tabla_Cat_Com_Modelos + " Modelos";
        //            Mi_SQL += " on Modelos." + Cat_Com_Modelos.Campo_Modelo_ID + " ";
        //            Mi_SQL += " = Productos." + Cat_Com_Productos.Campo_Modelo_ID + " ";
        //            Mi_SQL += " join " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " Marcas";
        //            Mi_SQL += " on Marcas." + Cat_Com_Marcas.Campo_Marca_ID + " ";
        //            Mi_SQL += " = Productos." + Cat_Com_Productos.Campo_Marca_ID + " ";
        //            Mi_SQL += " join " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " Unidades";
        //            Mi_SQL += " on Unidades." + Cat_Com_Unidades.Campo_Unidad_ID + " ";
        //            Mi_SQL += " = Productos." + Cat_Com_Productos.Campo_Unidad_ID + " ";
        //            Mi_SQL += " join " + Cat_Empleados.Tabla_Cat_Empleados + " Empleado";
        //            Mi_SQL += " on Ordenes_Compra." + Ope_Com_Ordenes_Compra.Campo_Usuario_Id_Resguardo + " ";
        //            Mi_SQL += " = Empleado." + Cat_Empleados.Campo_Empleado_ID;
        //            Mi_SQL += " WHERE Req_Producto." + Ope_Com_Req_Producto.Campo_No_Orden_Compra + " = " + Datos.P_Dt_Ordenes_Compra.Rows[Cont_Elementos][Ope_Com_Req_Producto.Campo_No_Orden_Compra].ToString().Trim() + " ";

        //            //Ejecutar consulta
        //            Cmd.CommandText = Mi_SQL;
        //            Da.SelectCommand = Cmd;
        //            Da.Fill(Dt_Productos_Resguardados); // Se llena la tabla con la consulta
        //        }
        //    }         
        //    catch (SqlException Ex)
        //    {
        //        Trans.Rollback();
        //        //variable para el mensaje 
        //        //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
        //        if (Ex.Number == 8152)
        //        {
        //            Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
        //        }
        //        else if (Ex.Number == 2627)
        //        {
        //            if (Ex.Message.IndexOf("PRIMARY") != -1)
        //            {
        //                Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
        //            }
        //            else if (Ex.Message.IndexOf("UNIQUE") != -1)
        //            {
        //                Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
        //            }
        //            else
        //            {
        //                Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
        //            }
        //        }
        //        else if (Ex.Number == 547)
        //        {
        //            Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
        //        }
        //        else if (Ex.Number == 515)
        //        {
        //            Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
        //        }
        //        else
        //        {
        //            Mensaje = "Error al intentar consultar productos resguardados. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
        //        }
        //        //Indicamos el mensaje 
        //        throw new Exception(Mensaje);
        //    }
        //    finally
        //    {
        //        Cn.Close();
        //    }

        //    return Dt_Productos_Resguardados; // Retorna la tabla
        //}

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Montos_Orden_Compra
        /// DESCRIPCION:            Se obtienen los montos de la orden de compra seleccionada por el usuario
        /// PARAMETROS :                                 
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            24/Febrero/2010 
        /// MODIFICO          :     Se implemtento el método "Alta_Bitacora"
        /// FECHA_MODIFICO    :     03/Marzo/2011
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Montos_Orden_Compra(Cls_Alm_Com_Recepcion_Material_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //Variable para las consultas

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + ", " + Ope_Com_Ordenes_Compra.Campo_Subtotal + ", ";
                Mi_SQL += Ope_Com_Ordenes_Compra.Campo_Total_IEPS + ", " + Ope_Com_Ordenes_Compra.Campo_Total_IVA + ", " + Ope_Com_Ordenes_Compra.Campo_Total + " ";
                Mi_SQL += "FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " ";
                Mi_SQL += "WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra;

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Actualizar_Orden_Compra
        /// DESCRIPCION:            Se actualizan las ordenes de compra al estatus "SURTIDA"
        ///                         y se guardan los comentarios
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene 
        ///                         los numero de orden de compra
        /// CREO       :            Salvador Hernández Ramírez
        /// FECHA_CREO :            25/Febrero/2010 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static String Actualizar_Orden_Compra(Cls_Alm_Com_Recepcion_Material_Negocio Datos)
        {
            DataTable DataTable_Temporal = null;
            String Mi_SQL;
            String Mensaje = "";
            DataTable_Temporal = Datos.P_Dt_Ordenes_Compra;
            String No_Entrada = "";
            Double Subtotal = 0.0;
            Double Monto_Total = 0.0;
            Double IVA_ENTRADA = 0.0;
            Double Precio_C_IVA = 0.0;
            Double Precio_S_IVA = 0.0;
            Double Cantidad = 0.0;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Object Aux = new Object();
            SqlDataAdapter Da = new SqlDataAdapter();
            bool bandera = false;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Double Porcentaje_Impuesto = 0;
            string Listado_Almacen = "";
            try
            {
                Mi_SQL = "UPDATE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra;
                Mi_SQL += " SET " + Ope_Com_Ordenes_Compra.Campo_Estatus + "='" + Datos.P_Estatus.Trim() + "'";
                Mi_SQL += " WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " =" + Datos.P_No_Orden_Compra;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                String Fecha_Sustio = DateTime.Now.ToString("dd/MM/yyyy").ToUpper();

                Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " SET " + Ope_Com_Requisiciones.Campo_Estatus + "='" + Datos.P_Estatus.Trim() + "'";
                Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Fecha_Surtido + "= '" + Fecha_Sustio + "'";
                Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Empleado_Surtido_ID + "= '" + Cls_Sessiones.Empleado_ID + "'";
                Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " =" + Datos.P_No_Orden_Compra;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery(); // Se ejecuta la operación


                // METODO UTILIZADO PARA consultar EL NUMERO DE REQUISICIÓN
                String No_Requisicion = "";
                Object No_Req = null;

                Mi_SQL = " SELECT " + Ope_Com_Requisiciones.Campo_Requisicion_ID;
                Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Listado_Almacen;
                Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Tipo;
                Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.Trim();

                //Ejecutar consulta
                DataTable Dt_Auxiliar = new DataTable();
                Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = Cmd;
                Dt_Auxiliar = new DataTable();
                Da.Fill(Dt_Auxiliar);
                No_Req = Dt_Auxiliar.Rows[0][0].ToString();

                Listado_Almacen = Dt_Auxiliar.Rows[0][1].ToString();


                //Verificar si no es nulo
                if (No_Req != null && Convert.IsDBNull(No_Req) == false)
                    No_Requisicion = Convert.ToString(No_Req);
                else
                    No_Requisicion = "";

                // Se Guarda el Historial de la requisición
                Cls_Ope_Com_Requisiciones_Negocio Requisiciones = new Cls_Ope_Com_Requisiciones_Negocio();
                Requisiciones.Registrar_Historial(Datos.P_Estatus.Trim(), No_Requisicion, Cmd);
                Monto_Total = 0.0;
                double auxSubtotal = 0;
                double auxtotal = 0;
                DataTable Dt_Imp = new DataTable();
                //Actualizamos el campo de Cantidad REcibida en Ope_com_req_productos
                foreach (DataRow Dr_Producto in Datos.P_Dt_Productos.Rows)
                {
                    //Consultar el IVA
                    Mi_SQL = " SELECT " + Cat_Com_Impuestos.Campo_Porcentaje_Impuesto;
                    Mi_SQL += " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos;
                    Mi_SQL += " WHERE " + Cat_Com_Impuestos.Campo_Impuesto_ID + " = ";
                    Mi_SQL += "(SELECT " + Cat_Com_Productos.Campo_Impuesto_ID + " FROM ";
                    Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + " WHERE ";
                    Mi_SQL += Cat_Com_Productos.Campo_Producto_ID + "='" + String.Format("{0:0000000000}", Convert.ToInt64(Dr_Producto["CLAVE"].ToString().Trim())) + "')";

                    //Ejecutar consulta
                    Dt_Imp = new DataTable();
                    Cmd.CommandText = Mi_SQL;
                    Da.SelectCommand = Cmd;
                    Dt_Imp = new DataTable();
                    Da.Fill(Dt_Imp);
                    Porcentaje_Impuesto = double.Parse(Dt_Imp.Rows[0][0].ToString()) * 0.01;


                    Cantidad = Double.Parse(Dr_Producto["CANTIDAD_A_RECIBIR"].ToString().Trim());
                    Precio_C_IVA = Double.Parse(Dr_Producto["MONTO"].ToString().Trim());
                    Precio_S_IVA = Double.Parse(Dr_Producto["COSTO_REAL"].ToString().Trim());

                    Mi_SQL = "UPDATE " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                    Mi_SQL += " SET " + Ope_Com_Req_Producto.Campo_Cantidad_Recibida;
                    Mi_SQL += " = " + Ope_Com_Req_Producto.Campo_Cantidad_Recibida + " + " + Dr_Producto["CANTIDAD_A_RECIBIR"].ToString().Trim();
                    Mi_SQL += " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + "=" + No_Requisicion;
                    Mi_SQL += " AND " + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + "='" + String.Format("{0:0000000000}", Convert.ToInt64(Dr_Producto["CLAVE"].ToString().Trim())) + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    auxSubtotal = Cantidad * Precio_S_IVA;
                    if (Porcentaje_Impuesto != 0)
                    {
                        auxtotal = ((Precio_S_IVA * Porcentaje_Impuesto) + Precio_S_IVA) * Cantidad;
                    }
                    else
                    {
                        auxtotal = auxSubtotal;
                    }

                    Subtotal = Subtotal + auxSubtotal;
                    Monto_Total = Monto_Total + auxtotal;

                    Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                    Mi_SQL += " SET " + Cat_Com_Productos.Campo_Existencia;
                    Mi_SQL += " = " + Cat_Com_Productos.Campo_Existencia + " + " + Dr_Producto["CANTIDAD_A_RECIBIR"].ToString().Trim();
                    if (Listado_Almacen.Trim() == "SI")
                    {
                        Mi_SQL += ", " + Cat_Com_Productos.Campo_Disponible;
                        Mi_SQL += " = " + Cat_Com_Productos.Campo_Disponible + " + " + Dr_Producto["CANTIDAD_A_RECIBIR"].ToString().Trim();
                    }
                    Mi_SQL += " WHERE " + Cat_Com_Productos.Campo_Producto_ID + "='" + String.Format("{0:0000000000}", Convert.ToInt64(Dr_Producto["CLAVE"].ToString().Trim())) + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                if (Subtotal.ToString().Contains("."))
                    if (Subtotal.ToString().Substring(Subtotal.ToString().IndexOf("."), Subtotal.ToString().Length - Subtotal.ToString().IndexOf(".")).Length >= 3)
                        Subtotal = double.Parse(Subtotal.ToString().Substring(0, Subtotal.ToString().IndexOf(".") + 3));

                if (Monto_Total.ToString().Contains("."))
                    if (Monto_Total.ToString().Substring(Monto_Total.ToString().IndexOf("."), Monto_Total.ToString().Length - Monto_Total.ToString().IndexOf(".")).Length >= 3)
                        Monto_Total = double.Parse(Monto_Total.ToString().Substring(0, Monto_Total.ToString().IndexOf(".") + 3));

                IVA_ENTRADA = Monto_Total - Subtotal;

                if (IVA_ENTRADA.ToString().Contains("."))
                    if (IVA_ENTRADA.ToString().Substring(IVA_ENTRADA.ToString().IndexOf("."), IVA_ENTRADA.ToString().Length - IVA_ENTRADA.ToString().IndexOf(".")).Length >= 3)
                        IVA_ENTRADA = double.Parse(IVA_ENTRADA.ToString().Substring(0, IVA_ENTRADA.ToString().IndexOf(".") + 3));


                No_Entrada = Obtener_Id_Consecutivo(Alm_Com_Entradas.Campo_No_Entrada, Alm_Com_Entradas.Tabla_Alm_Com_Entradas, Cmd);

                //Insertamos el Registro de Entrada
                Mi_SQL = "INSERT INTO " + Alm_Com_Entradas.Tabla_Alm_Com_Entradas + "(" +
                Alm_Com_Entradas.Campo_No_Entrada + ", " +
                Alm_Com_Entradas.Campo_Fecha + ", " +
                Alm_Com_Entradas.Campo_Total + ", " +
                Alm_Com_Entradas.Campo_Estatus + ", " +
                Alm_Com_Entradas.Campo_Usuario_Creo + ", " +
                Alm_Com_Entradas.Campo_Fecha_Creo + ", " +
                Alm_Com_Entradas.Campo_Empleado_Almacen_ID + ", " +
                Alm_Com_Entradas.Campo_No_Requisicion + ", " +
                Alm_Com_Entradas.Campo_Subtotal + ", " +
                Alm_Com_Entradas.Campo_IVA +
                ") VALUES(" +
                "'" + No_Entrada.Trim() + "'," +
                "GETDATE()," +
                Datos.P_Dt_Facturas_Proveedores.Rows[0]["TOTAL_FACTURA"].ToString().Trim() + "," +
                "'GENERADA'," +
                "'" + Cls_Sessiones.Nombre_Empleado.Trim() + "', " +
                "GETDATE()," +
                "'" + Cls_Sessiones.Empleado_ID.Trim() + "', " +
                No_Requisicion + "," +
                Datos.P_Dt_Facturas_Proveedores.Rows[0]["IMPORTE"].ToString().Trim() + ",";
                Mi_SQL = Mi_SQL + Datos.P_Dt_Facturas_Proveedores.Rows[0]["IVA_FACTURA"].ToString().Trim() + ")";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                //Insertamos los detalles
                for (int i = 0; i < Datos.P_Dt_Productos.Rows.Count; i++)
                {
                    Mi_SQL = "INSERT INTO " + Alm_Com_Entradas_Detalles.Tabla_Alm_Com_Entradas_Detalles;
                    Mi_SQL += "(" + Alm_Com_Entradas_Detalles.Campo_No_Entrada + ", ";
                    Mi_SQL += Alm_Com_Entradas_Detalles.Campo_Producto_ID + ",";
                    Mi_SQL += Alm_Com_Entradas_Detalles.Campo_Cantidad + ",";
                    Mi_SQL += Alm_Com_Entradas_Detalles.Campo_Importe + ",";
                    Mi_SQL += Alm_Com_Entradas_Detalles.Campo_Precio_U_SIN_IVA + ")";
                    Mi_SQL += " VALUES ('" + No_Entrada.Trim() + "'";
                    Mi_SQL += ", '" + String.Format("{0:0000000000}", Convert.ToInt64(Datos.P_Dt_Productos.Rows[i]["CLAVE"].ToString().Trim())) + "'";
                    Mi_SQL += ", " + Datos.P_Dt_Productos.Rows[i]["CANTIDAD_A_RECIBIR"].ToString().Trim();
                    Mi_SQL += ", " + Double.Parse(Datos.P_Dt_Productos.Rows[i]["CANTIDAD_A_RECIBIR"].ToString().Trim()) * Double.Parse(Datos.P_Dt_Productos.Rows[i]["COSTO_REAL"].ToString().Trim());
                    Mi_SQL += ", " + Datos.P_Dt_Productos.Rows[i]["COSTO_REAL"].ToString().Trim() + ")";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }//fin del for

                Double Importe = 0;
                Double IVA = 0;
                Double Total = 0;

                // SE GUARDAN LAS FACTURAS
                if (Datos.P_Dt_Facturas_Proveedores != null)
                {
                    Int64 Factura_ID = 1;

                    //Asignar consulta para el maximo Marbete de la tabla OPE_COM_DET_DOC_SOPORTE
                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Alm_Registro_Facturas.Campo_Factura_ID + "), 0) ";
                    Mi_SQL += "FROM " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas;
                    Cmd.CommandText = Mi_SQL;
                    Aux = Cmd.ExecuteScalar();

                    //Verificar si no es nulo
                    if (Aux != null && !Convert.IsDBNull(Aux))
                        Factura_ID = Convert.ToInt64(Aux.ToString()) + 1;

                    foreach (DataRow Dr_Factura in Datos.P_Dt_Facturas_Proveedores.Rows)
                    {
                        Importe += Double.Parse(Dr_Factura["IMPORTE"].ToString().Trim());
                        IVA += Double.Parse(Dr_Factura["IVA_FACTURA"].ToString().Trim());
                        Total += Double.Parse(Dr_Factura["TOTAL_FACTURA"].ToString().Trim());

                        //Asignar consulta para ingresar la factura
                        Mi_SQL = "INSERT INTO " + Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas + " (";
                        Mi_SQL += Ope_Alm_Registro_Facturas.Campo_Factura_ID + ", ";
                        Mi_SQL += Ope_Alm_Registro_Facturas.Campo_Factura_Proveedor + ", ";
                        Mi_SQL += Ope_Alm_Registro_Facturas.Campo_No_Contra_Recibo + ", ";
                        Mi_SQL += Ope_Alm_Registro_Facturas.Campo_Importe_Factura + ", ";
                        Mi_SQL += Ope_Alm_Registro_Facturas.Campo_Fecha_Factura + ", ";
                        Mi_SQL += Ope_Alm_Registro_Facturas.Campo_Usuario_Creo + ", ";
                        Mi_SQL += Ope_Alm_Registro_Facturas.Campo_Fecha_Creo + ", ";
                        Mi_SQL += Ope_Alm_Registro_Facturas.Campo_No_Orden_Compra + ", ";
                        Mi_SQL += Ope_Alm_Registro_Facturas.Campo_IVA_Factura + ", ";
                        Mi_SQL += Ope_Alm_Registro_Facturas.Campo_Total_Factura + ", "; 
                        Mi_SQL += Ope_Alm_Registro_Facturas.Campo_No_Entrada + ") ";
                        Mi_SQL += "VALUES(" + Factura_ID + ", '";
                        Mi_SQL += Dr_Factura["NO_FACTURA"].ToString().Trim() + "', ";
                        Mi_SQL += "NULL, ";
                        Mi_SQL += Dr_Factura["IMPORTE"].ToString().Trim() + ", '";
                        Mi_SQL += String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Dr_Factura["FECHA"].ToString().Trim())) + "', '";
                        Mi_SQL += Cls_Sessiones.Nombre_Empleado + "', ";
                        Mi_SQL += " GETDATE(), ";
                        Mi_SQL += Datos.P_No_Orden_Compra + ", ";
                        Mi_SQL += Dr_Factura["IVA_FACTURA"].ToString().Trim() + ", ";
                        Mi_SQL += Dr_Factura["TOTAL_FACTURA"].ToString().Trim() + ", '";
                        Mi_SQL += No_Entrada.Trim() + "')";

                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        Factura_ID = Factura_ID + 1;
                    }
                }

                String Alta_poliza = Alta_Poliza(No_Entrada, Datos.P_No_Orden_Compra, No_Requisicion, Importe, IVA, Total, Cmd);
                if (Alta_poliza != "Alta_Exitosa")
                {
                    bandera = true;
                    throw new Exception("Asignar cuenta contable al Proveedor, no se puede completar la operación");

                }
                // Ene sta parte se insertan los comentarios en la tabla OPE_COM_COMENT_ORDEN_COMP
                if (!String.IsNullOrEmpty(Datos.P_Observaciones))
                {
                    Mi_SQL = "INSERT INTO " + Alm_Com_Entradas_Comentarios.Tabla_Alm_Com_Entradas_Comentarios +
                    " (" + Alm_Com_Entradas_Comentarios.Campo_No_Entrada +
                    ", " + Alm_Com_Entradas_Comentarios.Campo_Estatus +
                    ", " + Alm_Com_Entradas_Comentarios.Campo_Comentario +
                    ", " + Alm_Com_Entradas_Comentarios.Campo_Fecha +
                    ", " + Alm_Com_Entradas_Comentarios.Campo_Usuario_Creo +
                    ", " + Alm_Com_Entradas_Comentarios.Campo_Fecha_Creo +
                    ") VALUES (" +
                    Convert.ToInt64(No_Entrada) + ", '" +
                    Datos.P_Estatus.Trim() + "', '" +
                    Datos.P_Observaciones + "', '" +
                    String.Format("{0:dd/MM/yyyy HH:mm:ss}", Datos.P_Fecha_Factura) + "', '" +
                    Cls_Sessiones.Nombre_Empleado + "', GETDATE())";

                    // Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery(); // Se ejecutan las operaciónes


                    String No_Comentario = Obtener_Id_Consecutivo(Ope_Com_Coment_orden_Comp.Campo_No_Comentario, Ope_Com_Coment_orden_Comp.Tabla_Ope_Com_Ordenes_Compra);

                    Mi_SQL = "INSERT INTO " + Ope_Com_Coment_orden_Comp.Tabla_Ope_Com_Ordenes_Compra +
                    " (" + Ope_Com_Coment_orden_Comp.Campo_No_Comentario +
                    ", " + Ope_Com_Coment_orden_Comp.Campo_No_Orden_Compra +
                    ", " + Ope_Com_Coment_orden_Comp.Campo_Comentario +
                    ", " + Ope_Com_Coment_orden_Comp.Campo_Estatus +
                    ", " + Ope_Com_Coment_orden_Comp.Campo_Usuario_Creo +
                    ", " + Ope_Com_Coment_orden_Comp.Campo_Fecha_Creo +
                    ") VALUES ('" +
                    No_Comentario + "'," +
                    Datos.P_No_Orden_Compra + ",'" +
                    Datos.P_Observaciones + "','" +
                    Datos.P_Estatus.Trim() + "','" +
                    Cls_Sessiones.Nombre_Empleado + "', GETDATE())";

                    // Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery(); // Se ejecutan las operaciónes
                }//fin del if

                // SE GUARDAN LOS DOCUMENTOS SOPORTE
                if (Datos.P_Dt_Documentos != null)
                {
                    Int64 No_Marbete = 0;
                    //Asignar consulta para el maximo Marbete de la tabla OPE_COM_DET_DOC_SOPORTE
                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Com_Det_Doc_Soporte.Campo_Marbete + "), 0) ";
                    Mi_SQL += "FROM " + Ope_Com_Det_Doc_Soporte.Tabla_Ope_Com_Det_Doc_Soporte;

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Aux = Cmd.ExecuteScalar();

                    //Verificar si no es nulo
                    if (Aux != null && !Convert.IsDBNull(Aux))
                        Int64.TryParse(Aux.ToString(), out No_Marbete);

                    No_Marbete++;

                    foreach (DataRow Dr_Documento in Datos.P_Dt_Documentos.Rows)
                    {
                        //Asignar consulta para ingresar la factura
                        Mi_SQL = "INSERT INTO " + Ope_Com_Det_Doc_Soporte.Tabla_Ope_Com_Det_Doc_Soporte + " (";
                        Mi_SQL += Ope_Com_Det_Doc_Soporte.Campo_Documento_ID + ", ";
                        Mi_SQL += Ope_Com_Det_Doc_Soporte.Campo_No_Factura_Interno + ", ";
                        Mi_SQL += Ope_Com_Det_Doc_Soporte.Campo_Usuario_Creo + ", ";
                        Mi_SQL += Ope_Com_Det_Doc_Soporte.Campo_Fecha_Creo + ", ";
                        Mi_SQL += Ope_Com_Det_Doc_Soporte.Campo_Marbete + ") ";
                        Mi_SQL += "VALUES('" + Dr_Documento["DOCUMENTO_ID"].ToString().Trim() + "', ";
                        Mi_SQL += No_Entrada + ", '";
                        Mi_SQL += Cls_Sessiones.Nombre_Empleado + "', ";
                        Mi_SQL += " GETDATE(), ";
                        Mi_SQL += No_Marbete + ")";

                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery(); // Se ejecuta la operación
                        No_Marbete++; // Se incrementa
                    }
                }

                Trans.Commit(); // Se ejecuta la transacciones
                Mensaje = "OE-" + Convert.ToInt32(No_Entrada.Trim());
            }
            catch (Exception Ex)
            {

                Trans.Rollback();
                if (bandera)
                {
                    Mensaje = "Asignar cuenta contable al Proveedor, no se puede completar la operación";
                }
                else
                    Mensaje = "Error al intentar realizar las  transacción. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje;
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Alta_Poliza
        /// DESCRIPCION:            Método utilizado consultar la información utilizada para mostrar la orden de salida
        /// PARAMETROS :            
        /// CREO       :            Salvador Hernández Ramírez
        /// FECHA_CREO :            24/Junio/2011  
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        private static String Alta_Poliza(String No_Entrada, String No_Orden_Compra, String No_Requisicion, Double Monto, Double IVA, Double Monto_Total, SqlCommand P_Cmd)
        {
            SqlDataAdapter Da = new SqlDataAdapter();
            DataTable Dt_Auxiliar = new DataTable();
            DataTable Dt_Cuentas_Almacen = new DataTable();
            DataTable Dt_Cuenta = new DataTable();
            String Mensaje = "";
            String Mi_SQL = "";
            String Tipo_Poliza_ID = "";
            Boolean Almacen_General = true;

            Mi_SQL = "SELECT * FROM " + Cat_Alm_Parametros_Cuentas.Tabla_Cat_Alm_Parametros_Cuentas;
            P_Cmd.CommandText = Mi_SQL;
            Da.SelectCommand = P_Cmd;
            Dt_Cuentas_Almacen = new DataTable();
            Da.Fill(Dt_Cuentas_Almacen);

            Mi_SQL = "SELECT DISTINCT(" + Ope_Com_Req_Producto.Campo_Tipo + ") FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
            Mi_SQL += " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion;
            P_Cmd.CommandText = Mi_SQL;
            Da.SelectCommand = P_Cmd;
            Dt_Auxiliar = new DataTable();
            Da.Fill(Dt_Auxiliar);

            if (Dt_Auxiliar.Rows.Count == 1 && Dt_Auxiliar.Rows[0][Ope_Com_Req_Producto.Campo_Tipo].ToString().Trim() == "PRODUCTO")
            {
                Mi_SQL = "SELECT DISTINCT(PROD." + Cat_Com_Productos.Campo_Almacen_General;
                Mi_SQL += ") FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PROD";
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PROD";
                Mi_SQL += " ON REQ_PROD." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = PROD." + Cat_Com_Productos.Campo_Producto_ID;
                Mi_SQL += " WHERE REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion;
                P_Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = P_Cmd;
                Dt_Auxiliar = new DataTable();
                Da.Fill(Dt_Auxiliar);

                if (Dt_Auxiliar.Rows.Count == 1 && Dt_Auxiliar.Rows[0][Cat_Com_Productos.Campo_Almacen_General].ToString().Trim() == "NO")
                {
                    Almacen_General = false;
                }
            }

            Mi_SQL = "SELECT " + Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID;
            Mi_SQL += " FROM " + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas;
            Mi_SQL += " WHERE " + Cat_Con_Tipo_Polizas.Campo_Descripcion + " LIKE '%DIARIO%'";
            P_Cmd.CommandText = Mi_SQL;
            Tipo_Poliza_ID = P_Cmd.ExecuteScalar().ToString().Trim();

            DataTable Dt_Detalles_Poliza = new DataTable();
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("MOMENTO_FINAL", typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("Nombre_Cuenta", typeof(System.String));
            Dt_Detalles_Poliza.AcceptChanges();

            Mi_SQL = "SELECT REQ_PROD." + Ope_Com_Req_Producto.Campo_Partida_ID;
            Mi_SQL += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID;
            Mi_SQL += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID;
            Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
            Mi_SQL += " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PROD";
            Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ";
            Mi_SQL += " ON REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL += " WHERE REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion;
            P_Cmd.CommandText = Mi_SQL;
            Da.SelectCommand = P_Cmd;
            Dt_Cuenta = new DataTable();
            Da.Fill(Dt_Cuenta);

            if (Dt_Cuenta.Rows.Count > 0)
            {
                if (Dt_Cuentas_Almacen.Rows.Count > 0)
                {
                    Mi_SQL = "SELECT * FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                    Mi_SQL += " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = ";
                    Mi_SQL += Almacen_General ?
                            Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_General_ID].ToString().Trim() :
                            Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_Papeleria_ID].ToString().Trim();

                    P_Cmd.CommandText = Mi_SQL;
                    Da.SelectCommand = P_Cmd;
                    Dt_Auxiliar = new DataTable();
                    Da.Fill(Dt_Auxiliar);

                    if (Dt_Auxiliar.Rows.Count > 0)
                    {
                        DataRow row = Dt_Detalles_Poliza.NewRow();

                        row = Dt_Detalles_Poliza.NewRow();
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();
                        row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                        row["Nombre_Cuenta"] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = No_Requisicion;
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = Monto;
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0.0;
                        row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Dt_Cuenta.Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim();
                        Dt_Detalles_Poliza.Rows.Add(row);
                        Dt_Detalles_Poliza.AcceptChanges();


                        Mi_SQL = "SELECT * FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                        Mi_SQL += " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '112900002%' ";

                        P_Cmd.CommandText = Mi_SQL;
                        Da.SelectCommand = P_Cmd;
                        Dt_Auxiliar = new DataTable();
                        Da.Fill(Dt_Auxiliar);

                        if (Dt_Auxiliar.Rows.Count > 0)
                        {
                            row = Dt_Detalles_Poliza.NewRow();

                            row = Dt_Detalles_Poliza.NewRow();
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();
                            row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                            row["Nombre_Cuenta"] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = No_Requisicion;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = IVA;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0.0;
                            row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                            row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Dt_Cuenta.Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString().Trim();
                            row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim();
                            row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim();
                            Dt_Detalles_Poliza.Rows.Add(row);
                            Dt_Detalles_Poliza.AcceptChanges();

                            Mi_SQL = "SELECT ORDEN_COM." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID;
                            Mi_SQL += ", PROV." + Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID;
                            Mi_SQL += ", CUENTAS." + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                            Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ";
                            Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " ORDEN_COM";
                            Mi_SQL += " ON REQ." + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " = ORDEN_COM." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                            Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROV";
                            Mi_SQL += " ON ORDEN_COM." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + " = PROV." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                            Mi_SQL += " LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTAS";
                            Mi_SQL += " ON PROV." + Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID + " = CUENTAS." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                            Mi_SQL += " WHERE REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + No_Requisicion;
                            P_Cmd.CommandText = Mi_SQL;
                            Da.SelectCommand = P_Cmd;
                            P_Cmd.CommandText = Mi_SQL;
                            Da.SelectCommand = P_Cmd;
                            Dt_Auxiliar = new DataTable();
                            Da.Fill(Dt_Auxiliar);

                            if (Dt_Auxiliar.Rows.Count > 0)
                            {
                                row = Dt_Detalles_Poliza.NewRow();
                                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 3;
                                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Auxiliar.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID].ToString().Trim();
                                row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                                row["Nombre_Cuenta"] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = No_Requisicion;
                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0.0;
                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Monto_Total;
                                row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                                row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Dt_Cuenta.Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString().Trim();
                                row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim();
                                row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim();
                                Dt_Detalles_Poliza.Rows.Add(row);
                                Dt_Detalles_Poliza.AcceptChanges();


                                Cls_Ope_Con_Polizas_Negocio Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio();

                                Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = Tipo_Poliza_ID;
                                Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now);
                                Rs_Alta_Ope_Con_Polizas.P_Fecha_Poliza = DateTime.Now;
                                Rs_Alta_Ope_Con_Polizas.P_Concepto = "Entrada " + Int64.Parse(No_Entrada) + ", OC-" + No_Orden_Compra + ", REQ-" + No_Requisicion.Trim() + ", Almacen ";
                                //Concatenamos si es almacen general o papeleria
                                Rs_Alta_Ope_Con_Polizas.P_Concepto += Almacen_General ? "General " : "Papeleria ";
                                Rs_Alta_Ope_Con_Polizas.P_Total_Debe = Monto_Total; //monto total con iva de productos
                                Rs_Alta_Ope_Con_Polizas.P_Total_Haber = Monto_Total;
                                Rs_Alta_Ope_Con_Polizas.P_No_Partida = 3;
                                Rs_Alta_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                                Rs_Alta_Ope_Con_Polizas.P_Dt_Detalles_Polizas = Dt_Detalles_Poliza;
                                Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID;
                                Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Autorizo = Cls_Sessiones.Empleado_ID;
                                Rs_Alta_Ope_Con_Polizas.P_Prefijo = "";
                                Rs_Alta_Ope_Con_Polizas.P_Cmmd = P_Cmd;
                                string[] Datos_Poliza = Rs_Alta_Ope_Con_Polizas.Alta_Poliza();
                                Mensaje = "Alta_Exitosa";
                            }
                        }
                    }
                    else
                    {
                        Mensaje = "No se encontraron los datos de la cuenta contable ";
                        Mensaje += Almacen_General ? "General " : "Papeleria ";
                        Mensaje += "de Almacen.";
                    }
                }
                else
                {
                    Mensaje = "No se han asignado las cuentas contables General ó Papeleria para Almacen.";
                }
            }
            else
            {
                Mi_SQL = "SELECT " + Ope_Com_Requisiciones.Campo_Codigo_Programatico;
                Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + No_Requisicion;
                P_Cmd.CommandText = Mi_SQL;
                Mensaje = "No se encontro la cuenta contable del gasto en el presupuesto aprobado para este año. Codigo Programatico: ";
                Mensaje += P_Cmd.ExecuteScalar().ToString().Trim();
            }
            return Mensaje;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Id_Consecutivo
        ///DESCRIPCIÓN: crea una sentencia sql para insertar un inventario en la base de datos
        ///PARAMETROS: 1.-Campo_ID, nombre del campo de la tabla al cual se quiere sacar el ultimo valor
        ///            2.-Tabla, nombre de la tabla que se va a consultar
        ///CREO: Salvador Hernández Ramírez
        ///FECHA_CREO: 07/Enero/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static String Obtener_Id_Consecutivo(String Campo_ID, String Tabla)
        {
            String Consecutivo = "";
            String Mi_SQL;
            Object Obj;

            try
            {
                Mi_SQL = "SELECT ISNULL(MAX (" + Campo_ID + "),'0000000000') ";
                Mi_SQL += "FROM " + Tabla;
                Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                if (Convert.IsDBNull(Obj))
                {
                    Consecutivo = "0000000001";
                }
                else
                {
                    Consecutivo = string.Format("{0:0000000000}", Convert.ToInt32(Obj) + 1);
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Consecutivo;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Id_Consecutivo
        ///DESCRIPCIÓN: crea una sentencia sql para insertar un inventario en la base de datos
        ///PARAMETROS: 1.-Campo_ID, nombre del campo de la tabla al cual se quiere sacar el ultimo valor
        ///            2.-Tabla, nombre de la tabla que se va a consultar
        ///CREO: Salvador Hernández Ramírez
        ///FECHA_CREO: 07/Enero/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static String Obtener_Id_Consecutivo(String Campo_ID, String Tabla, SqlCommand Cmd)
        {
            String Consecutivo = "";
            String Mi_SQL;
            Object Obj;

            try
            {
                Mi_SQL = "SELECT ISNULL(MAX (" + Campo_ID + "),'0000000000') ";
                Mi_SQL += "FROM " + Tabla;
                Cmd.CommandText = Mi_SQL;
                Obj = Cmd.ExecuteScalar();
                //Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                if (Convert.IsDBNull(Obj))
                {
                    Consecutivo = "0000000001";
                }
                else
                {
                    Consecutivo = string.Format("{0:0000000000}", Convert.ToInt32(Obj) + 1);
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Consecutivo;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:    Consultar_Datos_Orden_Compra
        ///DESCRIPCIÓN:             Método utilizado para consultar los comentarios y el numero de reserva
        ///PARAMETROS:   
        ///CREO:                    Salvador Hernández Ramírez
        ///FECHA_CREO:              19/Abril/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Datos_Orden_Compra(Cls_Alm_Com_Recepcion_Material_Negocio Datos)
        {
            // Declaracion de Variables
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataTable Dt_Datos_Ordenes_Compra = new DataTable();

            try
            {
                Mi_SQL = "SELECT " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + ", ";
                Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Comentarios + ", ";
                Mi_SQL += Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Reserva + ", ";
                Mi_SQL += "(select " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " FROM ";
                Mi_SQL += Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "";
                Mi_SQL += " WHERE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = ";
                Mi_SQL += Ope_Com_Requisiciones.Campo_No_Orden_Compra + ") as REQUISICION ";
                Mi_SQL += " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra;
                Mi_SQL += " WHERE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                Mi_SQL += " = " + Datos.P_No_Orden_Compra.Trim();

                Dt_Datos_Ordenes_Compra = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Datos_Ordenes_Compra;
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:    Consultar_Datos_Orden_Compra
        ///DESCRIPCIÓN:             Método utilizado para consultar los comentarios y el numero de reserva
        ///PARAMETROS:              Clase de negocio con no de orden de compra y no requisición
        ///CREO:                    Gustavo Angeles C.
        ///FECHA_CREO:              25/jul/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Productos_Servicios_Orden_Compra(Cls_Alm_Com_Recepcion_Material_Negocio Datos)
        {
            // Declaracion de Variables
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataTable Dt_Productos_Servicos = new DataTable();
            try
            {
                Mi_SQL = "SELECT * FROM " +
                Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto +
                " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = '" + Datos.P_Busqueda.Trim() + "' AND " +
                Ope_Com_Req_Producto.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.Trim();
                Dt_Productos_Servicos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Productos_Servicos;
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
        }

         ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:    Consultar_Permisos_Empleados
        ///DESCRIPCIÓN:             Metodo que se utiliza para consultar los permisos de los empleados del almacen
        ///PARAMETROS:              Clase de negocio con no de orden de compra y no requisición
        ///CREO:                    Susana Trigueros Armenta
        ///FECHA_CREO:              19/SEP/13
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Permisos_Empleados(Cls_Alm_Com_Recepcion_Material_Negocio Datos)
        {
            String Mi_SQL = "SELECT * FROM " + Ope_Alm_Permisos_Facturas.Tabla_Ope_Alm_Permisos_Facturas;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Alm_Permisos_Facturas.Campo_Empleado_ID + "='" + Datos.P_Empleado_Almacen_ID + "'";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:    Consultar_Datos_Orden_Compra
        ///DESCRIPCIÓN:             Método utilizado para consultar los comentarios y el numero de reserva
        ///PARAMETROS:              Clase de negocio con no de orden de compra y no requisición
        ///CREO:                    Gustavo Angeles C.
        ///FECHA_CREO:              25/jul/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataSet Consultar_Orden_Entrada(Cls_Alm_Com_Recepcion_Material_Negocio Datos)
        {
            // Declaracion de Variables
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataSet Ds_Orden_Entrada = new DataSet();
            DataTable Dt_Cabecera = new DataTable();
            DataTable Dt_Detalles = new DataTable();
            try
            {
                Mi_SQL = "SELECT CONVERT(INT, ENTRADAS." + Alm_Com_Entradas.Campo_No_Entrada;
                Mi_SQL += ") AS NO_ENTRADA, ENTRADAS." + Alm_Com_Entradas.Campo_Fecha;
                Mi_SQL += ", ENTRADAS." + Alm_Com_Entradas.Campo_Total;
                Mi_SQL += ", COMENT." + Alm_Com_Entradas_Comentarios.Campo_Comentario;
                Mi_SQL += " AS COMENTARIO, ENTRADAS." + Alm_Com_Entradas.Campo_Usuario_Creo;
                Mi_SQL += ", ENTRADAS." + Alm_Com_Entradas.Campo_No_Requisicion;
                Mi_SQL += ", ENTRADAS." + Alm_Com_Entradas.Campo_Estatus;
                Mi_SQL += " FROM " + Alm_Com_Entradas.Tabla_Alm_Com_Entradas;
                Mi_SQL += " ENTRADAS LEFT OUTER JOIN " + Alm_Com_Entradas_Comentarios.Tabla_Alm_Com_Entradas_Comentarios;
                Mi_SQL += " COMENT ON COMENT." + Alm_Com_Entradas_Comentarios.Campo_No_Entrada + " = ENTRADAS." + Alm_Com_Entradas.Campo_No_Entrada;
                Mi_SQL += " WHERE ENTRADAS." + Alm_Com_Entradas.Campo_No_Entrada + " = " + Datos.P_No_Entrada;

                Dt_Cabecera = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                Mi_SQL = "SELECT DET." + Alm_Com_Entradas_Detalles.Campo_Producto_ID;
                Mi_SQL += ", PROD." + Cat_Com_Productos.Campo_Nombre;
                Mi_SQL += ", UNID." + Cat_Com_Unidades.Campo_Abreviatura;
                Mi_SQL += ", DET." + Alm_Com_Entradas_Detalles.Campo_Cantidad;
                Mi_SQL += ", DET." + Alm_Com_Entradas_Detalles.Campo_Importe;
                Mi_SQL += ", DET." + Alm_Com_Entradas_Detalles.Campo_Precio_U_SIN_IVA;
                Mi_SQL += ", PROD." + Cat_Com_Productos.Campo_Clave;
                Mi_SQL += " FROM " + Alm_Com_Entradas_Detalles.Tabla_Alm_Com_Entradas_Detalles;
                Mi_SQL += " DET LEFT OUTER JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL += " PROD ON PROD." + Cat_Com_Productos.Campo_Producto_ID + " = DET." + Alm_Com_Entradas_Detalles.Campo_Producto_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades;
                Mi_SQL += " UNID ON UNID." + Cat_Com_Unidades.Campo_Unidad_ID + " = PROD." + Cat_Com_Productos.Campo_Unidad_ID;
                Mi_SQL += " WHERE " + Alm_Com_Entradas_Detalles.Campo_No_Entrada + " = " + Datos.P_No_Entrada;

                Dt_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                Dt_Cabecera.TableName = "Cabecera";
                Dt_Detalles.TableName = "Detalles";

                Ds_Orden_Entrada.Tables.Add(Dt_Cabecera.Copy());
                Ds_Orden_Entrada.Tables.Add(Dt_Detalles.Copy());

                return Ds_Orden_Entrada;
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
        }

    }
}