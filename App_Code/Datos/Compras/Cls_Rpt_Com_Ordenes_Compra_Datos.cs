﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;
using JAPAMI.Reporte_Ordenes_Compra.Negocio;

/// <summary>
/// Summary description for Cls_Rpt_Com_Ordenes_Compra_Datos
/// </summary>
namespace JAPAMI.Reporte_Ordenes_Compra.Datos
{
    public class Cls_Rpt_Com_Ordenes_Compra_Datos
    {
        public Cls_Rpt_Com_Ordenes_Compra_Datos()
        {
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Productos
        ///DESCRIPCION:             Consultar los productos de acuerdo a un criterio de busqueda por aproximacion
        ///PARAMETROS:              Datos: Variable de la capa de negocios
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              31/Marzo/2012 10:48
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Productos(Cls_Rpt_Com_Ordenes_Compra_Negocio Datos)
        {
            //Declaracion de variables 
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT Producto_ID, (Nombre + ' ' + ISNULL(Descripcion, '')) AS Nombre_Completo FROM Cat_COm_Productos " +
                    "WHERE Nombre LIKE '%" + Datos.P_Busqueda + "%' OR Descripcion LIKE '%" + Datos.P_Busqueda + "%' " +
                    "ORDER BY Nombre_Completo ";

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Proveedores
        ///DESCRIPCION:             Consultar los proveedores de acuerdo a un criterio de busqueda por aproximacion
        ///PARAMETROS:              Datos: Variable de la capa de negocios
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              31/Marzo/2012 11:16
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Proveedores(Cls_Rpt_Com_Ordenes_Compra_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            DataTable Dt_Resultado = new DataTable(); //Tabla para el resultado

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT Cat_Com_Proveedores.Proveedor_ID, (Cat_Com_Proveedores.Nombre + ' Cuenta: ' + Cat_Con_Cuentas_Contables.Cuenta) AS Proveedor " +
                    "FROM Cat_Com_Proveedores " +
                    "LEFT JOIN Cat_Con_Cuentas_Contables ON Cat_Com_Proveedores.Cuenta_Contable_ID = Cat_Con_Cuentas_Contables.Cuenta_Contable_ID ";

                //verificar el tipo de consulta
                if (Datos.P_Tipo_Consulta == "Proveedor")
                {
                    //Nombre del proveedor
                    Mi_SQL += "WHERE Cat_Com_Proveedores.Nombre LIKE '%" + Datos.P_Busqueda + "%' " +
                        "OR Cat_Com_Proveedores.Compania LIKE '%" + Datos.P_Busqueda + "%' " + 
                        "OR Cat_COm_Proveedores.Contacto LIKE '%" + Datos.P_Busqueda + "%' ";
                }
                else
                {
                    //Cuenta del proveedor
                    Mi_SQL += "WHERE Cat_Con_Cuentas_Contables.Cuenta LIKE '%" + Datos.P_Busqueda + "%' ";
                }
                
                //Orden de la consulta
                Mi_SQL += "ORDER BY Cat_Com_Proveedores.Nombre  ";

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Ordenes_Compra
        ///DESCRIPCION:             Consultar las ordenes de compra por proveedor o por producto
        ///PARAMETROS:              Datos: Variable de la capa de negocios
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              09/Abril/2012 12:43
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Ordenes_Compra(Cls_Rpt_Com_Ordenes_Compra_Negocio Datos)
        {
            //Declaracion de variables 
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado
            Boolean Where_Utilizado = false; //variable que indica si la clausual where ya ha sido utilizada

            try
            {
                //verificar el tipo de la consulta
                if (Datos.P_Tipo_Consulta == "Proveedor")
                {
                    //Asignar consulta general
                    Mi_SQL = "SELECT No_Orden_Compra, P_Estatus, Nombre_Proveedor, Subtotal, Total_IEPS, Total_IVA, Total, Folio, Tipo_Articulo, " +
                        "Clave = '', Producto = '', Cantidad = 0 " +
                        "FROM Ope_Com_Ordenes_Compra ";

                    //Verificar si se tiene un estatus
                    if (Datos.P_Estatus != null && Datos.P_Estatus != "" && Datos.P_Estatus != String.Empty)
                    {
                        Mi_SQL += "WHERE P_Estatus = '" + Datos.P_Estatus + "' ";
                        Where_Utilizado = true;
                    }

                    //verificar si tiene un proveedor asignado
                    if (Datos.P_Proveedor_ID != null && Datos.P_Proveedor_ID != "" && Datos.P_Proveedor_ID != String.Empty)
                    {
                        //verificar si ya se ha utilizado la clusula where
                        if (Where_Utilizado == true)
                        {
                            Mi_SQL += "AND ";
                        }
                        else
                        {
                            Mi_SQL += "WHERE ";
                            Where_Utilizado = true;
                        }

                        //Resto de la consulta
                        Mi_SQL += "Proveedor_ID = '" + Datos.P_Proveedor_ID + "' ";
                    }

                    //Verifixcar si hay rango de fechas
                    if (Datos.P_Fecha_Inicio != null && Datos.P_Fecha_Inicio != new DateTime(1,1,1))
                    {
                        //verificar si ya se ha utilizado la clusula where
                        if (Where_Utilizado == true)
                        {
                            Mi_SQL += "AND ";
                        }
                        else
                        {
                            Mi_SQL += "WHERE ";
                            Where_Utilizado = true;
                        }

                        Mi_SQL += "Fecha_Entrega >= '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Inicio) + " 00:00:00' ";

                        //verificar si se ha asignado la fecha final
                        if (Datos.P_Fecha_Fin != null)
                        {
                            Mi_SQL += "Fecha_Entrega <= '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Fin) + " 23:59:59 '";
                        }
                    }
                }
                else
                {
                    //Asignar consulta de los detalles
                    Mi_SQL = "SELECT Ope_Com_Ordenes_Compra.No_Orden_Compra, Ope_Com_Ordenes_Compra.P_Estatus, " +
                        "Ope_Com_Ordenes_Compra.Nombre_Proveedor, Ope_Com_Ordenes_Compra.Folio, " +
                        "Ope_Com_Ordenes_Compra.Tipo_Articulo, Cat_Com_Productos.Clave, " +
                        "(Cat_Com_Productos.Nombre +  ' ' + ISNULL(Cat_Com_Productos.Descripcion, '')) AS Producto, Ope_Com_Req_Producto.Cantidad, " +
                        "Ope_Com_Req_Producto.Monto AS Subtotal, Ope_Com_Req_Producto.Monto_IVA AS Total_IVA, Ope_Com_Req_Producto.Monto_IEPS AS Total_IEPS, " +
                        "Ope_Com_Req_Producto.Monto_Total AS Total " +
                        "FROM Ope_Com_Ordenes_Compra " +
                        "INNER JOIN Ope_Com_Req_Producto ON Ope_Com_Ordenes_Compra.No_Orden_Compra = Ope_Com_Req_Producto.No_Orden_Compra " +
                        "INNER JOIN Cat_Com_Productos ON Ope_Com_Req_Producto.Prod_Serv_ID = Cat_Com_Productos.Producto_ID ";

                    //Verificar el tipo de consulta de los detalles
                    switch (Datos.P_Tipo_Consulta_Detalles)
                    {
                        case "Producto_ID":
                            Mi_SQL += "WHERE Cat_Com_Productos.Producto_ID = '" + Datos.P_Producto_ID.Trim() + "' ";
                            break;

                        case "Clave":
                            Mi_SQL += "WHERE Cat_Com_Productos.Clave = '" + Datos.P_Clave.Trim() + "' ";
                            break;

                        case "Ref_JAPAMI":
                            Mi_SQL += "WHERE Cat_Com_Productos.Ref_JAPAMI = " + Datos.P_Ref_JAPAMI.ToString().Trim() + " ";
                            break;

                        case "Nombre":
                            Mi_SQL += "WHERE Cat_Com_Productos.Nombre LIKE '%" + Datos.P_Nombre.Trim() + "%'";
                            break;

                        default:
                            //Verificar si se ha seleccionado una unidad
                            if (Datos.P_Unidad_ID != null && Datos.P_Unidad_ID != "" && Datos.P_Unidad_ID != String.Empty)
                            {
                                //Verificar si es la opcion nula
                                if (Datos.P_Unidad_ID == "NULO")
                                {
                                    Mi_SQL += "WHERE Cat_Com_Productos.Unidad_ID IS NULL ";
                                }
                                else
                                {
                                    Mi_SQL += "WHERE Cat_Com_Productos.Unidad_ID = '" + Datos.P_Unidad_ID.Trim() + "' ";
                                }

                                Where_Utilizado = true;
                            }

                            //Verificar si se ha seleccionado un impuesto
                            if (Datos.P_Impuesto_ID != null && Datos.P_Impuesto_ID != "" && Datos.P_Impuesto_ID != String.Empty)
                            {
                                //verificar si la clausula where ya ha sido utilizada
                                if (Where_Utilizado == true)
                                {
                                    Mi_SQL += "AND ";
                                }
                                else
                                {
                                    Mi_SQL += "WHERE ";

                                    Where_Utilizado = true;
                                }

                                //Resto del filtro
                                //Verificar si es la opcion nula
                                if (Datos.P_Impuesto_ID == "NULO")
                                {
                                    Mi_SQL += "(Cat_Com_Productos.Impuesto_ID IS NULL AND Cat_Com_Productos.Impuesto_2_ID IS NULL ";
                                }
                                else
                                {
                                    Mi_SQL += "(Cat_Com_Productos.Impuesto_ID = '" + Datos.P_Impuesto_ID + "' " +
                                        "OR Cat_Com_Productos.Impuesto_2_ID = '" + Datos.P_Impuesto_ID + "') ";
                                }
                            }

                            //Verificar si se ha seleccionado un estatus
                            if (Datos.P_Estatus != null && Datos.P_Estatus != "" && Datos.P_Estatus != String.Empty)
                            {
                                //verificar si la clausula where ya ha sido utilizada
                                if (Where_Utilizado == true)
                                {
                                    Mi_SQL += "AND ";
                                }
                                else
                                {
                                    Mi_SQL += "WHERE ";

                                    Where_Utilizado = true;
                                }

                                //Resto del filtro
                                Mi_SQL += "Cat_Com_Productos.P_Estatus = '" + Datos.P_Estatus.Trim() + "' ";
                            }

                            //Verificar si se ha seleccionado un tipo
                            if (Datos.P_Tipo != null && Datos.P_Tipo != "" && Datos.P_Tipo != String.Empty)
                            {
                                //verificar si la clausula where ya ha sido utilizada
                                if (Where_Utilizado == true)
                                {
                                    Mi_SQL += "AND ";
                                }
                                else
                                {
                                    Mi_SQL += "WHERE ";

                                    Where_Utilizado = true;
                                }

                                //Resto del filtro
                                //Verificar si es la opcion nula
                                if (Datos.P_Tipo == "NULO")
                                {
                                    Mi_SQL += "Cat_Com_Productos.Tipo IS NULL ";
                                }
                                else
                                {
                                    Mi_SQL += "Cat_Com_Productos.Tipo = '" + Datos.P_Tipo.Trim() + "' ";
                                }
                            }

                            //Verificar si se ha seleccionado el stock
                            if (Datos.P_Stock != null && Datos.P_Stock != "" && Datos.P_Stock != String.Empty)
                            {
                                //verificar si la clausula where ya ha sido utilizada
                                if (Where_Utilizado == true)
                                {
                                    Mi_SQL += "AND ";
                                }
                                else
                                {
                                    Mi_SQL += "WHERE ";

                                    Where_Utilizado = true;
                                }

                                //Resto del filtro
                                Mi_SQL = "Cat_Com_Productos.Stock = '" + Datos.P_Stock.Trim() + "' ";
                            }

                            //Verificar si se ha seleccionado una partida generica
                            if (Datos.P_Partida_Generica_ID != null && Datos.P_Partida_Generica_ID != "" && Datos.P_Partida_Generica_ID != String.Empty)
                            {
                                //verificar si la clausula where ya ha sido utilizada
                                if (Where_Utilizado == true)
                                {
                                    Mi_SQL += "AND ";
                                }
                                else
                                {
                                    Mi_SQL += "WHERE ";

                                    Where_Utilizado = true;
                                }

                                //Verificar si es la opcion nula
                                if (Datos.P_Partida_Generica_ID == "NULO")
                                {
                                    Mi_SQL += "Cat_Com_Productos.Partida_ID IS NULL ";
                                }
                                else
                                {
                                    //Verificar si se ha seleccionado una partida especifica
                                    if (Datos.P_Partida_Especifica_ID != null && Datos.P_Partida_Especifica_ID != "" && Datos.P_Partida_Especifica_ID != String.Empty)
                                    {
                                        Mi_SQL += "Cat_Com_Productos.Partida_ID = '" + Datos.P_Partida_Especifica_ID.Trim() + "' ";
                                    }
                                    else
                                    {
                                        Mi_SQL += "Cat_Com_Partidas_Especificas.Partida_Generica_ID = '" + Datos.P_Partida_Generica_ID.Trim() + "' ";
                                    }
                                }
                            }
                            break;

                    }
                }
                
                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
    }
}