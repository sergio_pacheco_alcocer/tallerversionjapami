﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Generar_Requisicion.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;
using JAPAMI.Administrar_Requisiciones.Negocios;
using System.Data.SqlClient;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Filtrar_Cotizaciones.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Com_Filtrar_Cotizaciones_Datos
/// </summary>
/// 
namespace JAPAMI.Filtrar_Cotizaciones.Datos
{
	public class Cls_Ope_Com_Filtrar_Cotizaciones_Datos
{
		//
		// TODO: Add constructor logic here
		//

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consultar_Requisiciones
    ///DESCRIPCIÓN: crea una sentencia sql para conultar una Requisa en la base de datos
    ///PARAMETROS: 1.-Clase de Negocio
    ///            2.-Usuario que crea la requisa
    ///CREO: Silvia Morales Portuhondo
    ///FECHA_CREO: Noviembre/2010 
    ///MODIFICO:Gustavo Angeles Cruz
    ///FECHA_MODIFICO: 25/Ene/2011
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    public static DataTable Consultar_Requisiciones2(Cls_Ope_Com_Filtrar_Cotizaciones_Negocio Requisicion_Negocio)
    {
        //Requisicion_Negocio.P_Fecha_Inicial = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Inicial)) + " 00:00:00";
        //Requisicion_Negocio.P_Fecha_Final = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Final)) + " 23:59:59";
        if (Requisicion_Negocio.P_Estatus == "CONST ,GENERADA, REVISAR")
        {
            Requisicion_Negocio.P_Estatus = "EN CONSTRUCCION,GENERADA,REVISAR,RECHAZADA";
        }
        Requisicion_Negocio.P_Estatus = Requisicion_Negocio.P_Estatus.Replace(",", "','");
        //Requisicion_Negocio.P_Tipo = Requisicion_Negocio.P_Tipo.Replace(",", "','");
        String Mi_Sql = "";
        Mi_Sql =
        "SELECT " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + ".*, " +
            "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
            " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " +
            Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Dependencia_ID +
            ") NOMBRE_DEPENDENCIA " +
        " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
        " WHERE " +
        Ope_Com_Requisiciones.Campo_Estatus + " IN ('" + Requisicion_Negocio.P_Estatus + "')";
        //" AND " + Ope_Com_Requisiciones.Campo_Tipo + " IN ('" + Requisicion_Negocio.P_Tipo + "')";
        //if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Dependencia_ID) && Requisicion_Negocio.P_Dependencia_ID != "0")
        //{
        //    Mi_Sql += " AND " + Ope_Com_Requisiciones.Campo_Dependencia_ID +
        //    " = '" + Requisicion_Negocio.P_Dependencia_ID + "'";
        //}
        //if (Requisicion_Negocio.P_Estatus == "GENERADA")
        //{
        //    Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Generacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
        //        "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Generacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
        //}
        //else if (Requisicion_Negocio.P_Estatus == "CANCELADA")
        //{
        //    Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Cancelada + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
        //        "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Cancelada + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
        //}
        //else if (Requisicion_Negocio.P_Estatus == "EN CONSTRUCCION")
        //{
        //    Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Construccion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
        //        "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Construccion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
        //}
        //else if (Requisicion_Negocio.P_Estatus == "AUTORIZADA")
        //{
        //    Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
        //        "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
        //}
        //else if (Requisicion_Negocio.P_Estatus == "CONFIRMADA")
        //{
        //    Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Confirmacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
        //        "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Confirmacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
        //}
        //else if (Requisicion_Negocio.P_Estatus == "COTIZADA")
        //{
        //    Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
        //        "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
        //}
        //else if (Requisicion_Negocio.P_Estatus == "REVISAR")
        //{
        //    Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
        //        "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
        //}
        //else if (Requisicion_Negocio.P_Estatus == "COMPRA")
        //{
        //    Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
        //        "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
        //}
        //else
        //{
        //    Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Creo + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
        //        "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Creo + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
        //}
        //if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Requisicion_ID))
        //{
        //    Mi_Sql +=
        //    " AND " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
        //    " = " + Requisicion_Negocio.P_Requisicion_ID;
        //}
        if (Requisicion_Negocio.P_Cotizador_ID != null && Requisicion_Negocio.P_Cotizador_ID != "0")
        {
            Mi_Sql +=
            " AND " + Ope_Com_Requisiciones.Campo_Cotizador_ID +
            " = '" + Requisicion_Negocio.P_Cotizador_ID + "'";
        }


        Mi_Sql = Mi_Sql + " ORDER BY " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " DESC";
        DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
        if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0)
        {
            return (Data_Set.Tables[0]);
        }
        else
        {
            return null;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Consultar_Requisiciones
    ///DESCRIPCIÓN: Metodo que consulta las requisiciones listas para ser distribuidas a los cotizadores
    ///PARAMETROS: 1.- Cls_Ope_Com_Definir_Cotizadores_Negocio Clase_Negocios, objeto de la clase de negocios
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/JULIO/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public static DataTable Consultar_Requisiciones(Cls_Ope_Com_Filtrar_Cotizaciones_Negocio Clase_Negocio)
    {
        String Mi_SQL = "SELECT REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID;
        Mi_SQL = Mi_SQL + ", REQ." + Ope_Com_Requisiciones.Campo_Folio;
        Mi_SQL = Mi_SQL + ", DEP." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
        Mi_SQL = Mi_SQL + ", REQ." + Ope_Com_Requisiciones.Campo_Total;
        Mi_SQL = Mi_SQL + ", REQ." + Ope_Com_Requisiciones.Campo_Tipo_Articulo;
        Mi_SQL = Mi_SQL + ", REQ." + Ope_Com_Requisiciones.Campo_Estatus;
        Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Sap_Concepto.Campo_Clave + " +' '+ " + Cat_Sap_Concepto.Campo_Descripcion + " FROM ";
        Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
        Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
        Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
        Mi_SQL = Mi_SQL + "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
        Mi_SQL = Mi_SQL + "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
        Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = REQ." + Ope_Com_Requisiciones.Campo_Partida_ID + "))) AS CONCEPTO";
        Mi_SQL = Mi_SQL + ", REQ." + Ope_Com_Requisiciones.Campo_Alerta;
        Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ";
        Mi_SQL = Mi_SQL + " JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP";
        Mi_SQL = Mi_SQL + " ON DEP." + Cat_Dependencias.Campo_Dependencia_ID + "=";
        Mi_SQL = Mi_SQL + " REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
        Mi_SQL = Mi_SQL + " WHERE REQ." + Ope_Com_Requisiciones.Campo_Tipo + "='TRANSITORIA'";
        //Mi_SQL = Mi_SQL + " AND REQ." + Ope_Com_Requisiciones.Campo_Cotizador_ID + "='" + Cls_Sessiones.Empleado_ID +"'";
        if (Clase_Negocio.P_Cotizador_ID != null && Clase_Negocio.P_Cotizador_ID != "0")
        {
            Mi_SQL = Mi_SQL + " AND REQ." + Ope_Com_Requisiciones.Campo_Cotizador_ID + "='" + Cls_Sessiones.Empleado_ID + "'";
        }
        if(!String.IsNullOrEmpty(Clase_Negocio.P_Requisicion))
            Mi_SQL = Mi_SQL + " AND REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + "='" + Clase_Negocio.P_Requisicion + "'";

        Mi_SQL = Mi_SQL + " AND REQ." + Ope_Com_Requisiciones.Campo_Estatus + " IN ('PROVEEDOR','FILTRADA','COTIZADA-RECHAZADA')";
        Mi_SQL = Mi_SQL + " ORDER BY REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID;


        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Consultar_Requisiciones
    ///DESCRIPCIÓN: Metodo que consulta las requisiciones listas para ser distribuidas a los cotizadores
    ///PARAMETROS: 1.- Cls_Ope_Com_Definir_Cotizadores_Negocio Clase_Negocios, objeto de la clase de negocios
    ///CREO: Luis Daniel Guzmán Malagón
    ///FECHA_CREO: 01/Octubre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public static DataTable Consultar_Requisiciones_Busqueda(Cls_Ope_Com_Filtrar_Cotizaciones_Negocio Clase_Negocio)
    {
        String Mi_SQL = "SELECT REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID;
        Mi_SQL = Mi_SQL + ", REQ." + Ope_Com_Requisiciones.Campo_Folio;
        Mi_SQL = Mi_SQL + ", DEP." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
        Mi_SQL = Mi_SQL + ", REQ." + Ope_Com_Requisiciones.Campo_Total;
        Mi_SQL = Mi_SQL + ", REQ." + Ope_Com_Requisiciones.Campo_Tipo_Articulo;
        Mi_SQL = Mi_SQL + ", REQ." + Ope_Com_Requisiciones.Campo_Estatus;
        Mi_SQL = Mi_SQL + ", REQ." + Ope_Com_Requisiciones.Campo_Fecha_Creo;
        Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Sap_Concepto.Campo_Clave + " +' '+ " + Cat_Sap_Concepto.Campo_Descripcion + " FROM ";
        Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
        Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
        Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
        Mi_SQL = Mi_SQL + "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
        Mi_SQL = Mi_SQL + "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
        Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = REQ." + Ope_Com_Requisiciones.Campo_Partida_ID + "))) AS CONCEPTO";
        Mi_SQL = Mi_SQL + ", REQ." + Ope_Com_Requisiciones.Campo_Alerta;
        Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ";
        Mi_SQL = Mi_SQL + " JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP";
        Mi_SQL = Mi_SQL + " ON DEP." + Cat_Dependencias.Campo_Dependencia_ID + "=";
        Mi_SQL = Mi_SQL + " REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
        Mi_SQL = Mi_SQL + " WHERE REQ." + Ope_Com_Requisiciones.Campo_Tipo + "='TRANSITORIA'";
        //Mi_SQL = Mi_SQL + " AND REQ." + Ope_Com_Requisiciones.Campo_Cotizador_ID + "='" + Cls_Sessiones.Empleado_ID +"'";
        //if (Clase_Negocio.P_Cotizador_ID != null && Clase_Negocio.P_Cotizador_ID != "0")
        //{
            Mi_SQL = Mi_SQL + " AND REQ." + Ope_Com_Requisiciones.Campo_Cotizador_ID + "='" + Cls_Sessiones.Empleado_ID + "'";
        //}
        if (!String.IsNullOrEmpty(Clase_Negocio.P_Requisicion))
            Mi_SQL = Mi_SQL + " AND REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + "='" + Clase_Negocio.P_Requisicion + "'";

        if (!String.IsNullOrEmpty(Clase_Negocio.P_Fecha_Inicial))
        {
            if (!String.IsNullOrEmpty(Clase_Negocio.P_Fecha_Final))
                Mi_SQL += " AND REQ." + Ope_Com_Requisiciones.Campo_Fecha_Creo + " BETWEEN '" + Clase_Negocio.P_Fecha_Inicial + "' AND '" + Clase_Negocio.P_Fecha_Final+"'";
            else
                Mi_SQL += " AND REQ." + Ope_Com_Requisiciones.Campo_Fecha_Creo + " BETWEEN '" + Clase_Negocio.P_Fecha_Inicial + "' AND GETDATE()";
        }
        else
        {
            if (!String.IsNullOrEmpty(Clase_Negocio.P_Fecha_Final))
                Mi_SQL += " AND REQ." + Ope_Com_Requisiciones.Campo_Fecha_Creo + " < '" + Clase_Negocio.P_Fecha_Final+"'";
        }

        if(!String.IsNullOrEmpty(Clase_Negocio.P_Estatus))
            Mi_SQL = Mi_SQL + " AND REQ." + Ope_Com_Requisiciones.Campo_Estatus + " = '"+ Clase_Negocio.P_Estatus +"'";
        else
            Mi_SQL = Mi_SQL + " AND REQ." + Ope_Com_Requisiciones.Campo_Estatus + " IN ('PROVEEDOR','FILTRADA','COTIZADA-RECHAZADA')";

        
        
        Mi_SQL = Mi_SQL + " ORDER BY REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID;


        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
    }

    
        
    public static void Modificar_Estatus(Cls_Ope_Com_Filtrar_Cotizaciones_Negocio Clase_Negocio)
    {
        String Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " SET ";
        Mi_SQL += Ope_Com_Requisiciones.Campo_Estatus + "  = '" + Clase_Negocio.P_Estatus +"'";
        Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +" = '"+Clase_Negocio.P_Requisicion+"'";

        SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
    }

	}
}
