﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Archivos_Cotizaciones.Negocio;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;

/// <summary>
/// Summary description for Cls_Ope_Archivos_Cotizaciones_Datos
/// </summary>
namespace JAPAMI.Archivos_Cotizaciones.Datos
{
    public class Cls_Ope_Archivos_Cotizaciones_Datos
    {
        public Cls_Ope_Archivos_Cotizaciones_Datos()
        {
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Archivos
        ///DESCRIPCION:             hacer la consulta de los archivos de una cotizacion
        ///PARAMETROS:              Datos: Variable de la capa de negocio
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              09/Marzo/2012 13:39
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Archivos(Cls_Ope_Archivos_Cotizaciones_Negocio Datos)
        {
            //Declaracion de variables
            DataTable Dt_Resultado = new DataTable(); //tabla para la consulta
            String Mi_SQL = String.Empty; //variable para la consulta

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT Ope_Com_Cotizaciones_Archivos.No_Archivo, Ope_Com_Cotizaciones_Archivos.Proveedor_ID, Ope_Com_Cotizaciones_Archivos.Nombre_Archivo, "+ 
                    "Ope_Com_Cotizaciones_Archivos.Descripcion, Cat_Com_Proveedores.Nombre AS Proveedor, Ope_Com_Requisiciones.Folio AS Folio, Original = 'SI' " +
                    "FROM Ope_Com_Cotizaciones_Archivos " +
                    "INNER JOIN Ope_Com_Requisiciones ON Ope_Com_Cotizaciones_Archivos.No_Requisicion = Ope_Com_Requisiciones.No_Requisicion " +
                    "INNER JOIN Cat_Com_Proveedores ON Ope_Com_Cotizaciones_Archivos.Proveedor_ID = Cat_Com_Proveedores.Proveedor_ID " +
                    "WHERE Ope_Com_Cotizaciones_Archivos.No_Requisicion = " + Datos.P_No_Requisicion.ToString().Trim();

                //Ejecutar la consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        public static void Agregar_Archivos_Cotizacion(Cls_Ope_Archivos_Cotizaciones_Negocio Datos)
        {
            String Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
            String Mi_SQL = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            int Cont_Elementos; //variable para el contador
            Object aux; //variable auxiliar para las consultas
            int No_Archivo; //variable para el numero del archivo

            //Abrir la conexion
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Boolean Ejecutar_Commit = false;
            try
            {
                //Verificar si la tabla no es nula
                if (Datos.P_Dt_Archivos != null)
                {
                    //Verificar si la tabla tiene elementos
                    if (Datos.P_Dt_Archivos.Rows.Count > 0)
                    {
                        //Ciclo para el barrido de la tabla de los elementos a agregar
                        for (Cont_Elementos = 0; Cont_Elementos < Datos.P_Dt_Archivos.Rows.Count; Cont_Elementos++)
                        {
                            //Verificar que el renglon no haya sido eliminado
                            if (Datos.P_Dt_Archivos.Rows[Cont_Elementos].RowState != DataRowState.Deleted)
                            {
                                //Verificar si el archivo es nuevo
                                if (Datos.P_Dt_Archivos.Rows[Cont_Elementos]["Original"].ToString().Trim() == "NO")
                                {
                                    //Consulta para el numero del archivo
                                    Mi_SQL = "SELECT ISNULL(MAX(No_Archivo), 0) FROM Ope_Com_Cotizaciones_Archivos ";

                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    aux = Cmd.ExecuteScalar();

                                    //obtener el ID
                                    No_Archivo = Convert.ToInt32(aux) + 1;

                                    //Construir la consulta
                                    Mi_SQL = "INSERT INTO Ope_Com_Cotizaciones_Archivos (No_Archivo, No_Requisicion, Proveedor_ID, Nombre_Archivo, Descripcion, Usuario_Creo, Fecha_Creo) "+ 
                                        "VALUES(" + No_Archivo.ToString().Trim() + "," + Datos.P_No_Requisicion.ToString().Trim() + "," +
                                        "'" + Datos.P_Dt_Archivos.Rows[Cont_Elementos]["Proveedor_ID"].ToString().Trim() + "'," +
                                        "'" + Datos.P_Dt_Archivos.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim() + "'," +
                                        "'" + Datos.P_Dt_Archivos.Rows[Cont_Elementos]["Descripcion"].ToString().Trim() + "'," +
                                        "'" + Usuario_Creo + "',GETDATE())";

                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                    Ejecutar_Commit = true;
                                }
                            }
                        }
                    }
                }

                        //Verificar si la tabla no es nula
                        if (Datos.P_Dt_Archivos_Eliminados != null)
                        {
                            //Ciclo para el barrido de la tabla de los elementos a eliminar
                            for (Cont_Elementos = 0; Cont_Elementos < Datos.P_Dt_Archivos_Eliminados.Rows.Count; Cont_Elementos++)
                            {
                                //Verificar que el renglon no haya sido eliminado
                                if (Datos.P_Dt_Archivos_Eliminados.Rows[Cont_Elementos].RowState != DataRowState.Deleted)
                                {
                                    //verificar que el renglon sea original
                                    if (Datos.P_Dt_Archivos_Eliminados.Rows[Cont_Elementos]["Original"].ToString().Trim() == "SI")
                                    {
                                        //COnsulta para eliminar el registro del archivo
                                        Mi_SQL = "DELETE FROM Ope_Com_Cotizaciones_Archivos " +
                                            "WHERE No_Archivo = " + Datos.P_Dt_Archivos_Eliminados.Rows[Cont_Elementos]["No_Archivo"].ToString().Trim();

                                        //Ejecutar consulta
                                        Cmd.CommandText = Mi_SQL;
                                        Cmd.ExecuteNonQuery();
                                        Ejecutar_Commit = true;
                                    }
                                }
                            }
                        }

                        //Ejecutar transaccion
                        if(Ejecutar_Commit)Trans.Commit();
            }
            catch (SqlException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Información: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Los datos fueron actualizados por otro Usuario. Información: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Información: " + Ex.Message);
            }
            finally
            {
                Cn.Close();
            }
        }

        public static void Eliminar_Archivos_Cotizaciones(Cls_Ope_Archivos_Cotizaciones_Negocio Datos)
        {
            String Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
            String Mi_SQL = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;

            //Abrir la conexion
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                //Consulta para eliminar la informacion de los archivos en la base de datos
                Mi_SQL = "DELETE FROM Ope_Com_Cotizaciones_Archivos WHERE No_Requisicion = " + Datos.P_No_Requisicion.ToString().Trim();

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Ejecutar transaccion
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Información: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Los datos fueron actualizados por otro Usuario. Información: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Información: " + Ex.Message);
            }
            finally
            {
                Cn.Close();
            }
        }
    }
}