﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Cancelar_Ordenes.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Generar_Requisicion.Datos;
using JAPAMI.Manejo_Presupuesto.Datos;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Ope_Com_Cancelar_Ordenes_Compra_Datos
/// </summary>

namespace JAPAMI.Cancelar_Ordenes.Datos
{
    public class Cls_Ope_Com_Cancelar_Ordenes_Compra_Datos
    {

        public static DataTable Consultar_Ordenes_Compra(Cls_Ope_Com_Cancelar_Ordenes_Compra_Negocio Clase_Negocio)
        {
            String Mi_SQL = "";

            Mi_SQL = "SELECT ORDEN." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
            Mi_SQL += ", ORDEN." + Ope_Com_Ordenes_Compra.Campo_Folio;
            Mi_SQL += ", ORDEN." + Ope_Com_Ordenes_Compra.Campo_Estatus;
            Mi_SQL += ", ORDEN." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo;
            Mi_SQL += ", ORDEN." + Ope_Com_Ordenes_Compra.Campo_Total;
            Mi_SQL += ", ORDEN." + Ope_Com_Ordenes_Compra.Campo_Tipo_Articulo;
            Mi_SQL += ", ORDEN." + Ope_Com_Ordenes_Compra.Campo_Lista_Requisiciones;

            Mi_SQL += " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " ORDEN ";
            Mi_SQL += " WHERE ORDEN." + Ope_Com_Ordenes_Compra.Campo_Lista_Requisiciones + " IN ";
            Mi_SQL += "(SELECT " + Ope_Com_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
            Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Cotizador_ID;
            Mi_SQL += "='" + Cls_Sessiones.Empleado_ID.Trim() + "' ";
            Mi_SQL += " AND " + Ope_Com_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL += "=ORDEN." + Ope_Com_Ordenes_Compra.Campo_Lista_Requisiciones+") ";
                
            if (Clase_Negocio.P_Estatus != null)
            {
                Mi_SQL += " AND ORDEN." + Ope_Com_Ordenes_Compra.Campo_Estatus;
                Mi_SQL += " IN ('" + Clase_Negocio.P_Estatus.Trim() + "')";
            }
            else
            {
                Mi_SQL += " AND ORDEN." + Ope_Com_Ordenes_Compra.Campo_Estatus;
                Mi_SQL += " IN ('RECHAZADA','GENERADA','AUTORIZADA', 'AUTORIZADA_COMPRAS','AUTORIZADA_GERENCIA','CANCELACION PARCIAL','CANCELACION TOTAL','PROVEEDOR_ENTERADO','FACTURA REGISTRADA')";
                 
            }
            if (Clase_Negocio.P_No_Requisicio != null)
            {
                Mi_SQL += " AND ORDEN." + Ope_Com_Ordenes_Compra.Campo_Lista_Requisiciones + "=" + Clase_Negocio.P_No_Requisicio.Trim();
            }


            if (Clase_Negocio.P_Folio_Busqueda != null)
            {
                Mi_SQL += " AND UPPER(ORDEN." + Ope_Com_Ordenes_Compra.Campo_Folio;
                Mi_SQL += ") LIKE ('%" +  Clase_Negocio.P_Folio_Busqueda.Trim() +"%')";

            }

            if (Clase_Negocio.P_Fecha_Inicio != null)
            {
                Mi_SQL += " AND ORDEN." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo;
                Mi_SQL += " BETWEEN '" + Clase_Negocio.P_Fecha_Inicio + "'";
                Mi_SQL += " AND '" + Clase_Negocio.P_Fecha_Fin + "'";

            }

            Mi_SQL += " ORDER BY ORDEN." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;

            if(Clase_Negocio.P_No_Orden_Compra != null)
            {
                Mi_SQL = "SELECT ORDEN." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                Mi_SQL += ", ORDEN." + Ope_Com_Ordenes_Compra.Campo_No_Reserva;
                Mi_SQL += ", " + Ope_Com_Ordenes_Compra.Campo_Fecha_Cancelacion;
                Mi_SQL += ", (SELECT " + Cat_Com_Proveedores.Campo_Nombre;
                Mi_SQL += " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores;
                Mi_SQL += " WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL += "= ORDEN." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + ") AS PROVEEDOR";
                Mi_SQL += ", ORDEN." + Ope_Com_Ordenes_Compra.Campo_Estatus;
                Mi_SQL += ", ORDEN." + Ope_Com_Ordenes_Compra.Campo_Fecha_Cancelacion;
                Mi_SQL += ", ORDEN." +Ope_Com_Ordenes_Compra.Campo_Motivo_Cancelacion;
                Mi_SQL += ", ORDEN." + Ope_Com_Ordenes_Compra.Campo_Subtotal;
                Mi_SQL += ", ORDEN." + Ope_Com_Ordenes_Compra.Campo_Total_IVA;
                Mi_SQL += ", ORDEN." + Ope_Com_Ordenes_Compra.Campo_Total;
                Mi_SQL += ", (SELECT " + Ope_Com_Requisiciones.Campo_Requisicion_ID;
                Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_No_Orden_Compra;
                Mi_SQL += "= ORDEN." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + ") AS " + Ope_Com_Requisiciones.Campo_Requisicion_ID;
                Mi_SQL += ", (SELECT " + Ope_Com_Requisiciones.Campo_Listado_Almacen;
                Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_No_Orden_Compra;
                Mi_SQL += "= ORDEN." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + ") AS " + Ope_Com_Requisiciones.Campo_Listado_Almacen;
                Mi_SQL += ", (SELECT " + Ope_Com_Requisiciones.Campo_Folio;
                Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_No_Orden_Compra;
                Mi_SQL += "= ORDEN." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + ") AS FOLIO_REQUISICION ";
                Mi_SQL += ", (SELECT " + Ope_Com_Requisiciones.Campo_Codigo_Programatico;
                Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_No_Orden_Compra;
                Mi_SQL += "= ORDEN." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + ") AS " + Ope_Com_Requisiciones.Campo_Codigo_Programatico;
                Mi_SQL += ", (SELECT " + Ope_Com_Requisiciones.Campo_Justificacion_Compra;
                Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_No_Orden_Compra;
                Mi_SQL += "= ORDEN." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + ") AS " +Ope_Com_Requisiciones.Campo_Justificacion_Compra;
                Mi_SQL += " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " ORDEN ";
                Mi_SQL += " WHERE ORDEN." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                Mi_SQL += " ='" + Clase_Negocio.P_No_Orden_Compra.Trim() + "'";


            }
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Productos_Servicios
        ///DESCRIPCIÓN: Metodo que Consulta los detalles de la Requisicion seleccionada, ya sea Producto o servicio.
        ///PARAMETROS: 1.- Cls_Ope_Com_Definir_Cotizadores_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Productos_Servicios(Cls_Ope_Com_Cancelar_Ordenes_Compra_Negocio Clase_Negocio)
        {
            String Mi_SQL = "";
            DataTable Dt_Productos = new DataTable();

            if (Clase_Negocio.P_Estatus != "CANCELACION PARCIAL" && Clase_Negocio.P_Estatus != "CANCELACION TOTAL")
            {
                //Con sultamos primero el tipo de producto de la compra 
                Mi_SQL += "SELECT " + Ope_Com_Req_Producto.Campo_Tipo;
                Mi_SQL += " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                Mi_SQL += " WHERE " + Ope_Com_Req_Producto.Campo_No_Orden_Compra;
                Mi_SQL += "='" + Clase_Negocio.P_No_Orden_Compra.Trim() + "'";
                Mi_SQL += " GROUP BY " + Ope_Com_Req_Producto.Campo_Tipo;
                DataTable Dt_Tipo_Producto = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];


                if (Dt_Tipo_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Tipo].ToString().Trim() != String.Empty)
                {

                    switch (Dt_Tipo_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Tipo].ToString().Trim())
                    {

                        case "PRODUCTO":
                            Mi_SQL = "SELECT PRODUCTO." + Cat_Com_Productos.Campo_Clave +
                                    ", PRODUCTO." + Cat_Com_Productos.Campo_Nombre +
                                    "+' '+ PRODUCTO." + Cat_Com_Productos.Campo_Descripcion + " AS " + Cat_Com_Productos.Campo_Nombre +
                                    ", DET_REQ." + Ope_Com_Req_Producto.Campo_Cantidad +
                                    ", DET_REQ." + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado +
                                    ", DET_REQ." + Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado +
                                    ", DET_REQ." + Ope_Com_Req_Producto.Campo_IVA_Cotizado +
                                    ", DET_REQ." + Ope_Com_Req_Producto.Campo_Subtota_Cotizado +
                                    ", DET_REQ." + Ope_Com_Req_Producto.Campo_Total_Cotizado +
                                    " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " DET_REQ" +
                                    " JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTO" +
                                    " ON PRODUCTO." + Cat_Com_Productos.Campo_Producto_ID + " =" +
                                    " DET_REQ." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID +
                                    " WHERE DET_REQ." + Ope_Com_Req_Producto.Campo_No_Orden_Compra +
                                    "='" + Clase_Negocio.P_No_Orden_Compra + "'";
                            break;
                        case "SERVICIO":
                            Mi_SQL = "SELECT SERVICIO." + Cat_Com_Servicios.Campo_Clave +
                                    ", SERVICIO." + Cat_Com_Servicios.Campo_Nombre +
                                    ", DET_REQ." + Ope_Com_Req_Producto.Campo_Cantidad +
                                    ", DET_REQ." + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado +
                                    ", DET_REQ." + Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado +
                                    ", DET_REQ." + Ope_Com_Req_Producto.Campo_IVA_Cotizado +
                                    ", DET_REQ." + Ope_Com_Req_Producto.Campo_Subtota_Cotizado +
                                    ", DET_REQ." + Ope_Com_Req_Producto.Campo_Total_Cotizado +
                                    " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " DET_REQ" +
                                    " JOIN " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + " SERVICIO" +
                                    " ON SERVICIO." + Cat_Com_Servicios.Campo_Servicio_ID + " =" +
                                    " DET_REQ." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID +
                                    " WHERE DET_REQ." + Ope_Com_Req_Producto.Campo_No_Orden_Compra +
                                    "='" + Clase_Negocio.P_No_Orden_Compra + "'";

                            break;

                    }
                    Dt_Productos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }//Fin del IF
            }//FIN DEL IF 
            return Dt_Productos;

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Liberar_Presupuesto_Cancelacion_Total
        ///DESCRIPCIÓN: Metodo que libera el presupúesto de una orden de compra 
        ///PARAMETROS: 1.- Cls_Ope_Com_Cancelar_Ordenes_Compra_Negocio Clase_Negocio
        ///CREO:
        ///FECHA_CREO: 10/OCT/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Liberar_Presupuesto_Cancelacion_Total(Cls_Ope_Com_Cancelar_Ordenes_Compra_Negocio Clase_Negocio)
        {
            String Partida_ID = "";
            String Proyecto_ID = "";
            String FF = "";
            double Monto_Total_Cotizado = 0;
            String Mi_SQL = "";
            String Mensaje="";

            try{

            //Consultamos los campos para afectar el presupuesto 
            Mi_SQL = "SELECT " + Ope_Com_Req_Producto.Campo_Partida_ID;
            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID;
            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID;
            Mi_SQL += " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
            Mi_SQL += " WHERE " + Ope_Com_Req_Producto.Campo_No_Orden_Compra;
            Mi_SQL += "='" + Clase_Negocio.P_No_Orden_Compra.Trim() + "'";
            Mi_SQL += " GROUP BY " + Ope_Com_Req_Producto.Campo_Partida_ID;
            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID;
            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID;
            

            DataTable Dt_Partidas_Productos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Obtenemos la Partida, Programa y Fuente de dinanciamiento correspondiente al Producto o servicio
                Partida_ID = Dt_Partidas_Productos.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString();
                Proyecto_ID = Dt_Partidas_Productos.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString();
                FF = Dt_Partidas_Productos.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString();
                Monto_Total_Cotizado = double.Parse(Clase_Negocio.P_Monto_Total);

                //liberamos el presupuesto que era para este listado
                Mi_SQL = "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                                " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible +
                                " =" + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " + " + Monto_Total_Cotizado +
                                "," + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido +
                                "=" + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " - " + Monto_Total_Cotizado +
                                " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                                "='" + Partida_ID + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                                "='" + Proyecto_ID + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
                                "='" + FF + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio +
                                " = (SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
                                " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                                " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                                "='" + Partida_ID + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                                "='" + Proyecto_ID + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                                "= TO_CHAR(GETDATE(),'YYYY'))" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                                "= TO_CHAR(GETDATE(),'YYYY')" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
                                "=(SELECT " + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                                " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                                " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + "=" +
                                "(SELECT " + Ope_Com_Ordenes_Compra.Campo_No_Requisicion +
                                " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra +
                                " WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra +
                                "='" + Clase_Negocio.P_No_Orden_Compra + "' GROUP BY " +
                                Ope_Com_Ordenes_Compra.Campo_No_Requisicion + ") GROUP BY " + Ope_Com_Requisiciones.Campo_Dependencia_ID + ")";

                //Sentencia que ejecuta el query
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                Mensaje = "Se cancelo exitosamente la Orden de Compra con su requisicion";
            }catch(Exception Ex)
            {
                Mensaje = Ex.Message;
            }

            return Mensaje;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Liberar_Presupuesto_Cancelacion_Total
        ///DESCRIPCIÓN: Metodo que libera el presupúesto de una orden de compra 
        ///PARAMETROS: 1.- Cls_Ope_Com_Cancelar_Ordenes_Compra_Negocio Clase_Negocio
        ///CREO:
        ///FECHA_CREO: 10/OCT/2011
        ///MODIFICO: Jennyfer Ceja
        ///FECHA_MODIFICO:31-Agosto-2012
        ///CAUSA_MODIFICACIÓN: Realizacion del cargo y del abono 
        ///*******************************************************************************
        public static String Liberar_Presupuesto_Cancelacion_Parcial(Cls_Ope_Com_Cancelar_Ordenes_Compra_Negocio Clase_Negocio, SqlCommand Cmd)
        {
            String Mi_SQL = "";
            String Mensaje_Error = "";
            SqlDataAdapter Da = new SqlDataAdapter();

            DataTable Dt_Requisicion = Consultar_Codigo_Programatico_RQ(Clase_Negocio, Cmd);
            Double Monto_Cotizado = double.Parse(Dt_Requisicion.Rows[0]["TOTAL_COTIZADO"].ToString());
            Double Monto_Anterior = double.Parse(Dt_Requisicion.Rows[0]["TOTAL"].ToString());
            Double Diferencia = 0;
            String No_Reserva = Dt_Requisicion.Rows[0]["NUM_RESERVA"].ToString().Trim();

            Mi_SQL = "SELECT RES." + Ope_Psp_Reservas.Campo_Fte_Financimiento_ID + " AS FUENTE_FINANCIAMIENTO_ID, ";
            Mi_SQL += "RES." + Ope_Psp_Reservas.Campo_Proyecto_Programa_ID + " AS PROGRAMA_ID, ";
            Mi_SQL += "RES." + Ope_Psp_Reservas.Campo_Dependencia_ID + ", ";
            Mi_SQL += "RES." + Ope_Psp_Reservas.Campo_Partida_ID + ", ";
            Mi_SQL += "RES." + Ope_Psp_Reservas.Campo_Anio + ", ";
            Mi_SQL += "RES." + Ope_Psp_Reservas.Campo_Importe_Inicial + " AS IMPORTE ";
            Mi_SQL += "FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " RES ";
            Mi_SQL += "WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva;
            Cmd.CommandText = Mi_SQL;
            Da.SelectCommand = Cmd;
            DataTable Dt_Detalles = new DataTable();
            Da.Fill(Dt_Detalles);

            if (Dt_Detalles.Rows.Count > 0)
            {
                Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual
                   (Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO,
                   Dt_Detalles, Cmd);

                // PASO 1 VERIFICAMOS CUAL DE LOS 2 MONTOS ES MAYOR SI EL COTIZADO O  EL ANTERIOR
                if (Monto_Cotizado > Monto_Anterior)
                {
                    //Obtenemos la resta
                    Diferencia = Monto_Cotizado - Monto_Anterior;
                    Dt_Detalles.Rows[0]["IMPORTE"] = Diferencia.ToString();
                    Dt_Detalles.AcceptChanges();
                    Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual
                       (Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO,
                       Dt_Detalles, Cmd);
                }
                if (Monto_Cotizado < Monto_Anterior)
                {
                    //obtener resta
                    Diferencia = Monto_Anterior - Monto_Cotizado;
                    Dt_Detalles.Rows[0]["IMPORTE"] = Diferencia.ToString();
                    Dt_Detalles.AcceptChanges();
                    Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual
                       (Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE,
                       Dt_Detalles, Cmd);
                }
                Mensaje_Error = "Se realizo la Cancelacion Parcial Satisfactoriamente";
            }
            else
            {
                Mensaje_Error = "No se pudo realizar el movimiento presupuestal.";
            }

            return Mensaje_Error;
        }

        public static void Modificar_Montos_Cotizados_Requisicion(Cls_Ope_Com_Cancelar_Ordenes_Compra_Negocio Clase_Negocio, SqlCommand Cmd)
        {
            //mODIFICAMOS LOS TOTALES COTIZADOS EN LA TABLA OPE_COM_REQUISICIONES
            String Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
            Mi_SQL += " SET " + Ope_Com_Requisiciones.Campo_Subtotal_Cotizado + "=0";
            Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Total_Cotizado + "=0";
            Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_IVA_Cotizado + "=0";
            Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_IEPS_Cotizado + "=0";
            Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
            Cmd.CommandText = Mi_SQL;
            Cmd.ExecuteNonQuery();

            //MODIFICAMOS LOS MONTOS COTIZADOS EN EL DETALLE DE PRODUCTOS DE LA REQUISICION
            Mi_SQL = "";
            Mi_SQL = "UPDATE " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
            Mi_SQL += " SET " + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado + "=0";
            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado + "=0";
            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Subtota_Cotizado + "=0";
            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Total_Cotizado + "=0";
            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Proveedor_ID + "=NULL";
            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Nombre_Proveedor + "=NULL";
            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_No_Orden_Compra + "=0";
            Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
            Cmd.CommandText = Mi_SQL;
            Cmd.ExecuteNonQuery();
        }

        public static String Modificar_Orden_Compra(Cls_Ope_Com_Cancelar_Ordenes_Compra_Negocio Clase_Negocio)
        {
            String Mi_SQL = "";
            String Mensaje = "";
            String Num_Reserva;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos

            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion = new SqlConnection();//Variable para la conexión para la base de datos   
            SqlCommand Comando = new SqlCommand();//Sirve para la ejecución de las operaciones a la base de datos
            SqlDataAdapter Da = new SqlDataAdapter();

            //VAriable que ayudara a mandar llamar el nombre del metodo para dar de alta el estatus en el segumiento de la requisicion
            Cls_Ope_Com_Requisiciones_Datos Requisicion = new Cls_Ope_Com_Requisiciones_Datos();

            Conexion.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Transaction = Transaccion;
            Comando.Connection = Conexion;

            try
            {
                Mi_SQL = "SELECT " + Alm_Com_Entradas.Campo_No_Entrada;
                Mi_SQL += " FROM " + Alm_Com_Entradas.Tabla_Alm_Com_Entradas;
                Mi_SQL += " WHERE " + Alm_Com_Entradas.Campo_No_Requisicion + " = " + Clase_Negocio.P_No_Requisicio;
                Mi_SQL += " AND " + Alm_Com_Entradas.Campo_Estatus + " NOT IN ('CANCELADA')";
                Comando.CommandText = Mi_SQL;
                Da.SelectCommand = Comando;
                DataTable Dt_Entradas = new DataTable();
                Da.Fill(Dt_Entradas);

                if (Dt_Entradas.Rows.Count > 0)
                {
                    Mensaje = "Es necesario cancelar las entradas: ";
                    foreach (DataRow Dr_Renglo in Dt_Entradas.Rows)
                    {
                        Mensaje += Int64.Parse(Dr_Renglo[Alm_Com_Entradas.Campo_No_Entrada].ToString()) + ", ";
                    }
                    Mensaje += "de la orden de compra.";
                    Clase_Negocio.P_Estatus = String.Empty;
                }

                //En caso de ser cancelacion total
                if (Clase_Negocio.P_Estatus.Trim() == "CANCELACION TOTAL")
                {

                    Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                    Mi_SQL += " SET " + Ope_Com_Requisiciones.Campo_Estatus;
                    Mi_SQL += "='CANCELADA'";
                    Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Fecha_Cancelada;
                    Mi_SQL += "=GETDATE()";
                    Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Empleado_Cancelada_ID;
                    Mi_SQL += "='" + Cls_Sessiones.Empleado_ID + "'";
                    Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID;
                    Mi_SQL += "='" + Clase_Negocio.P_No_Requisicio + "'";
                    Comando.CommandText = Mi_SQL;
                    Comando.ExecuteNonQuery();

                    //Registramos la afectacion al Estatus  de las propuestas de cotizacion ya que se cambia a aceptada, se pone como RECHAZADA
                    Mi_SQL = "UPDATE " + Ope_Com_Propuesta_Cotizacion.Tabla_Ope_Com_Propuesta_Cotizacion;
                    Mi_SQL += " SET ESTATUS='RECHAZADA' WHERE " + Ope_Com_Propuesta_Cotizacion.Campo_No_Requisicion + "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
                    Comando.CommandText = Mi_SQL;
                    Comando.ExecuteNonQuery();

                    Cls_Ope_Com_Requisiciones_Datos.Registrar_Historial("CANCELADA", Clase_Negocio.P_No_Requisicio, Comando);

                    //Modificamos el estatus de la Requisicion
                    Mi_SQL = "UPDATE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra;
                    Mi_SQL += " SET " + Ope_Com_Ordenes_Compra.Campo_Estatus;
                    Mi_SQL += "='CANCELADA'";
                    Mi_SQL += ", " + Ope_Com_Ordenes_Compra.Campo_Fecha_Cancelacion;
                    Mi_SQL += "=GETDATE()";
                    Mi_SQL += ", " + Ope_Com_Ordenes_Compra.Campo_Motivo_Cancelacion;
                    Mi_SQL += "='" + Clase_Negocio.P_Motivo_Cancelacion + "'";
                    Mi_SQL += " WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                    Mi_SQL += "='" + Clase_Negocio.P_No_Orden_Compra.Trim() + "'";
                    Comando.CommandText = Mi_SQL;
                    Comando.ExecuteNonQuery();

                    if (Clase_Negocio.P_Listado_Almacen == null || String.IsNullOrEmpty(Clase_Negocio.P_Listado_Almacen))
                    {
                        //Primero Consultamos el Numero de Reserva.
                        Mi_SQL = "SELECT NUM_RESERVA FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                        Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID;
                        Mi_SQL += " = (SELECT " + Ope_Com_Ordenes_Compra.Campo_Lista_Requisiciones;
                        Mi_SQL += " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra;
                        Mi_SQL += " WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                        Mi_SQL += "='" + Clase_Negocio.P_No_Orden_Compra.Trim() + "')";
                        Comando.CommandText = Mi_SQL;
                        Obj = Comando.ExecuteScalar();
                        Num_Reserva = Obj.ToString();

                        Mi_SQL = "SELECT RES." + Ope_Psp_Reservas.Campo_Fte_Financimiento_ID + " AS FUENTE_FINANCIAMIENTO_ID, ";
                        Mi_SQL += "RES." + Ope_Psp_Reservas.Campo_Proyecto_Programa_ID + " AS PROGRAMA_ID, ";
                        Mi_SQL += "RES." + Ope_Psp_Reservas.Campo_Dependencia_ID + ", ";
                        Mi_SQL += "RES." + Ope_Psp_Reservas.Campo_Partida_ID + ", ";
                        Mi_SQL += "RES." + Ope_Psp_Reservas.Campo_Anio + ", ";
                        Mi_SQL += "RES." + Ope_Psp_Reservas.Campo_Importe_Inicial + " AS IMPORTE ";
                        Mi_SQL += "FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " RES ";
                        Mi_SQL += "WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Num_Reserva;
                        Comando.CommandText = Mi_SQL;
                        Da.SelectCommand = Comando;
                        DataTable Dt_Detalles = new DataTable();
                        Da.Fill(Dt_Detalles);

                        if (Dt_Detalles.Rows.Count > 0)
                        {
                            //Realizamos el Movimiento
                            Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO, Dt_Detalles, Comando);
                            Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Num_Reserva, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO, double.Parse(Clase_Negocio.P_Monto_Total), "", "", "", "", Comando);

                            Mensaje = "Se realizo la Cancelacion Total Satisfactoriamente";
                        }
                    }
                    else
                    {
                        Mensaje = "Se realizo la Cancelacion Total Satisfactoriamente";
                    }
                }//Fin del if Cancelacion total

                //En caso de Ser cancelacion Parcial 
                if (Clase_Negocio.P_Estatus.Trim() == "CANCELACION PARCIAL")
                {
                    //Cuando es un listado almacen se realiza lo siguiente
                    if (Clase_Negocio.P_Listado_Almacen != null && !String.IsNullOrEmpty(Clase_Negocio.P_Listado_Almacen))
                    {
                        Mi_SQL = "";
                        Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                        Mi_SQL += " SET " + Ope_Com_Requisiciones.Campo_Estatus;
                        Mi_SQL += "='FILTRADA'";
                        Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Alerta + "='ROJA2'";
                        Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID;
                        Mi_SQL += "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
                        Comando.CommandText = Mi_SQL;
                        Comando.ExecuteNonQuery();
                        //REgistramos el cambio de estatus a Proveedor pero antes una de Reasignar_Proveedor

                        Cls_Ope_Com_Requisiciones_Datos.Registrar_Historial("R_PROVEEDOR", Clase_Negocio.P_No_Requisicio, Comando);
                        Cls_Ope_Com_Requisiciones_Datos.Registrar_Historial("FILTRADA", Clase_Negocio.P_No_Requisicio, Comando);
                        //Modificamos todas las propuestas de Cotizacion al estatus de EN CONSTRUCCION con la finalidad de poder reecotizar los productos

                        //Modificamos todas las propuestas de Cotizacion a EN CONSTRUCCION para poder modificar los montos en caso de ser necesario
                        Mi_SQL = "UPDATE " + Ope_Com_Propuesta_Cotizacion.Tabla_Ope_Com_Propuesta_Cotizacion;
                        Mi_SQL += " SET " + Ope_Com_Propuesta_Cotizacion.Campo_Estatus;
                        Mi_SQL += "='EN CONSTRUCCION'";
                        Mi_SQL += ", " + Ope_Com_Propuesta_Cotizacion.Campo_Resultado + "=NULL";
                        Mi_SQL += " WHERE " + Ope_Com_Propuesta_Cotizacion.Campo_No_Requisicion + "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
                        Comando.CommandText = Mi_SQL;
                        Comando.ExecuteNonQuery();

                        //En caso de ser listado de almacen afectar solo los montos de la requisicion
                        Modificar_Montos_Cotizados_Requisicion(Clase_Negocio, Comando);

                        //Modificamos el estatus de la Requisicion
                        Mi_SQL = "UPDATE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra;
                        Mi_SQL += " SET " + Ope_Com_Ordenes_Compra.Campo_Estatus;
                        Mi_SQL += "='CANCELADA'";
                        Mi_SQL += ", " + Ope_Com_Ordenes_Compra.Campo_Fecha_Cancelacion;
                        Mi_SQL += "=GETDATE()";
                        Mi_SQL += ", " + Ope_Com_Ordenes_Compra.Campo_Motivo_Cancelacion;
                        Mi_SQL += "='" + Clase_Negocio.P_Motivo_Cancelacion + "'";
                        Mi_SQL += " WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                        Mi_SQL += "='" + Clase_Negocio.P_No_Orden_Compra.Trim() + "'";
                        Comando.CommandText = Mi_SQL;
                        Comando.ExecuteNonQuery();
                        Mensaje = "Se realizo la Cancelacion Parcial Satisfactoriamente";
                    }
                    //VERIFICAMOS PRIMERO SI EXISTE PRESUPUESTO ESTO ES PARA CUANDO NO ES UN LISTADO DE ALMACEN 
                    if (Clase_Negocio.P_Listado_Almacen == null || String.IsNullOrEmpty(Clase_Negocio.P_Listado_Almacen))
                    {
                        if (Consultamos_Presupuesto_Existente(Clase_Negocio, Comando))
                        {
                            Mi_SQL = "";
                            Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                            Mi_SQL += " SET " + Ope_Com_Requisiciones.Campo_Estatus;
                            Mi_SQL += "='FILTRADA'";
                            Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Alerta + "='ROJA2'";
                            Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID;
                            Mi_SQL += "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
                            Comando.CommandText = Mi_SQL;
                            Comando.ExecuteNonQuery();
                            //REgistramos el cambio de estatus a Proveedor pero antes una de Reasignar_Proveedor

                            Cls_Ope_Com_Requisiciones_Datos.Registrar_Historial("R_PROVEEDOR", Clase_Negocio.P_No_Requisicio, Comando);
                            Cls_Ope_Com_Requisiciones_Datos.Registrar_Historial("FILTRADA", Clase_Negocio.P_No_Requisicio, Comando);
                            //Modificamos todas las propuestas de Cotizacion al estatus de EN CONSTRUCCION con la finalidad de poder reecotizar los productos

                            //Modificamos todas las propuestas de Cotizacion a EN CONSTRUCCION para poder modificar los montos en caso de ser necesario
                            Mi_SQL = "UPDATE " + Ope_Com_Propuesta_Cotizacion.Tabla_Ope_Com_Propuesta_Cotizacion;
                            Mi_SQL += " SET " + Ope_Com_Propuesta_Cotizacion.Campo_Estatus;
                            Mi_SQL += "='EN CONSTRUCCION'";
                            Mi_SQL += ", " + Ope_Com_Propuesta_Cotizacion.Campo_Resultado + "=NULL";
                            Mi_SQL += " WHERE " + Ope_Com_Propuesta_Cotizacion.Campo_No_Requisicion + "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
                            Comando.CommandText = Mi_SQL;
                            Comando.ExecuteNonQuery();

                            Mensaje = Liberar_Presupuesto_Cancelacion_Parcial(Clase_Negocio, Comando);

                            //Modificamos el estatus de la Requisicion
                            Mi_SQL = "UPDATE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra;
                            Mi_SQL += " SET " + Ope_Com_Ordenes_Compra.Campo_Estatus;
                            Mi_SQL += "='CANCELADA'";
                            Mi_SQL += ", " + Ope_Com_Ordenes_Compra.Campo_Fecha_Cancelacion;
                            Mi_SQL += "=GETDATE()";
                            Mi_SQL += ", " + Ope_Com_Ordenes_Compra.Campo_Motivo_Cancelacion;
                            Mi_SQL += "='" + Clase_Negocio.P_Motivo_Cancelacion + "'";
                            Mi_SQL += " WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                            Mi_SQL += "='" + Clase_Negocio.P_No_Orden_Compra.Trim() + "'";
                            Comando.CommandText = Mi_SQL;
                            Comando.ExecuteNonQuery();
                        }
                        else
                        {
                            Mensaje = "El presupuesto no permite realizar la cancelacion, es insuficiente";
                        }
                    }//fin Clase_Negocio.P_Listado_Almacen == null
                }//fin del if Cancelacion Parcial 

                Transaccion.Commit();//aceptamos los cambios
            }
            catch (Exception EX)
            {
                Mensaje = EX.Message;
                Transaccion.Rollback();
            }
            finally
            {
                Conexion.Close();
                Comando = null;
                Conexion = null;
                Transaccion = null;
            }
            return Mensaje;
        }



    ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultamos_Presupuesto_Existente
        ///DESCRIPCIÓN: Metodo que consulta si existe presupuesto para los productos seleccionados
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 25/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static bool Consultamos_Presupuesto_Existente(Cls_Ope_Com_Cancelar_Ordenes_Compra_Negocio Requisicion_Negocio, SqlCommand Cmd)
        {
            bool Existe_Presupuesto = false;
            String Mi_SQL = "";
            //Primero Obtenemos todos lo productos pertenecientes a la requisicion:

            DataTable Dt_Requisicion = Consultar_Codigo_Programatico_RQ(Requisicion_Negocio, Cmd);
            //Guardamos en la variable Monto_Restante la diferencia de Total_Cotizado y Monto_Total inicial del producto
            //Primero checamos si se va a restar o a aumentar el presupuesto
            double Monto_Cotizado = double.Parse(Dt_Requisicion.Rows[0]["TOTAL_COTIZADO"].ToString().Trim());
            double Monto_Anterior = double.Parse(Dt_Requisicion.Rows[0]["TOTAL"].ToString().Trim());
            double Diferencia = 0;
            double Disponible = 0;
            String Partida_ID = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim();
            String Proyecto_ID = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim();
            String Dependencia_ID = Dt_Requisicion.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
            String FF = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim();
            String Programa_ID = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim();
            SqlDataAdapter Da = new SqlDataAdapter();
           
            // PASO 1 VERIFICAMOS CUAL DE LOS 2 MONTOS ES MAYOR SI EL COTIZADO O  EL ANTERIOR
            if (Monto_Anterior > Monto_Cotizado)
            {
                //Obtenemos la resta
                Diferencia = Monto_Anterior - Monto_Cotizado;
                Mi_SQL = "";
                //Consultamos el Monto presupuestal para ver si es suficiente
                Mi_SQL = "SELECT * FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado +
                "  WITH(NOLOCK) WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + DateTime.Now.Year;
                if (!String.IsNullOrEmpty(Dependencia_ID))
                {
                    Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dependencia_ID + "'";
                }
                if (!String.IsNullOrEmpty(FF))
                {
                    Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + FF + "'";
                }
                if (!String.IsNullOrEmpty(Proyecto_ID))
                {
                    Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Proyecto_ID + "'";
                }
                if (!String.IsNullOrEmpty(Partida_ID))
                {
                    Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida_ID + "'";
                }
                Mi_SQL += " ORDER BY " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " ASC";

                Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = Cmd;
                DataTable Dt_Presupuestos = new DataTable();
                Da.Fill(Dt_Presupuestos);

                Disponible = double.Parse(Dt_Presupuestos.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Disponible].ToString().Trim());
                if (Disponible >= Diferencia)
                    Existe_Presupuesto = true;
                else
                    Existe_Presupuesto = false;
            }
            else
                Existe_Presupuesto = true; //Existe presupuesto ya que el monto cotizado es menor que el k se aparto 

           
            return Existe_Presupuesto;
        }

        public static DataTable Consultar_Codigo_Programatico_RQ(Cls_Ope_Com_Cancelar_Ordenes_Compra_Negocio Clase_Negocio, SqlCommand Cmd)
        {
            String Mi_SQL = "";
            SqlDataAdapter Da = new SqlDataAdapter();
            Mi_SQL = "SELECT REQ_DET." + Ope_Com_Req_Producto.Campo_Partida_ID +
                             ",REQ_DET." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID +
                             ", (SELECT " + Ope_Com_Requisiciones.Campo_Total +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS TOTAL" +
                             ", (SELECT " + Ope_Com_Requisiciones.Campo_Total_Cotizado +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS TOTAL_COTIZADO" +
                             ", (SELECT " + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS DEPENDENCIA_ID" +
                             ", REQ_DET." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID +
                             ", (SELECT NUM_RESERVA" +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS NUM_RESERVA" +
                             " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_DET" +
                             " WHERE REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + "='" +Clase_Negocio.P_No_Requisicio + "'";

            Cmd.CommandText = Mi_SQL;
            Da.SelectCommand = Cmd;
            DataTable Dt_Requisicion = new DataTable();
            Da.Fill(Dt_Requisicion);

            return Dt_Requisicion;

        }

    }//fin del class


}//fin del namespace