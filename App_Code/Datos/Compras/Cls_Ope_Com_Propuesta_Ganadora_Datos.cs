﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Propuesta_Ganadora.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Manejo_Presupuesto.Datos;
using System.Data.SqlClient;
using JAPAMI.Generar_Requisicion.Datos;
using JAPAMI.Generar_Requisicion.Negocio;


/// <summary>
/// Summary description for Cls_Ope_Com_Propuesta_Ganadora_Datos
/// </summary>

namespace JAPAMI.Propuesta_Ganadora.Datos
{
    public class Cls_Ope_Com_Propuesta_Ganadora_Datos
    {
        #region Metodos
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Productos_Servicios
        ///DESCRIPCIÓN: Metodo que Consulta los detalles de la Requisicion seleccionada, ya sea Producto o servicio.
        ///PARAMETROS: 1.- Cls_Ope_Com_Definir_Cotizadores_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Productos_Servicios(Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio)
        {
            String Mi_SQL = "";
            switch (Clase_Negocio.P_Tipo_Articulo)
            {
                case "PRODUCTO":
                    Mi_SQL = "SELECT DET." + Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID +
                             ", PRO." + Cat_Com_Productos.Campo_Producto_ID + " AS PROD_SERV_ID" +
                            ", PRO." + Cat_Com_Productos.Campo_Clave +
                            "+' '+ PRO." + Cat_Com_Productos.Campo_Nombre + " AS NOMBRE_PROD_SERV" +
                            ", PRO." + Cat_Com_Productos.Campo_Descripcion +
                            ", DET." + Ope_Com_Req_Producto.Campo_Cantidad +
                            ", DET." + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado +
                            ", DET." + Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado +
                            ", DET." + Ope_Com_Req_Producto.Campo_IVA_Cotizado +
                            ", DET." + Ope_Com_Req_Producto.Campo_IEPS_Cotizado +
                            ", DET." + Ope_Com_Req_Producto.Campo_Subtota_Cotizado +
                            ", DET." + Ope_Com_Req_Producto.Campo_Total_Cotizado +
                            ", DET." + Ope_Com_Req_Producto.Campo_Proveedor_ID +
                            ", DET." + Ope_Com_Req_Producto.Campo_Nombre_Proveedor +
                            " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " DET" +
                            " JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRO" +
                            " ON PRO." + Cat_Com_Productos.Campo_Producto_ID +
                            " = DET." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID +
                            " JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ" +
                            " ON REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                            " = DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID +
                            " WHERE REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                            " = '" + Clase_Negocio.P_No_Requisicion + "'";
                    break;
                case "SERVICIO":
                    Mi_SQL = "SELECT DET." + Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID +
                            ",SER." + Cat_Com_Servicios.Campo_Servicio_ID + " AS PROD_SERV_ID" +
                            ", SER." + Cat_Com_Servicios.Campo_Clave +
                            "+' '+  SER." + Cat_Com_Servicios.Campo_Nombre + " AS NOMBRE_PROD_SERV" +
                            ", NULL AS DESCRIPCION" +
                            ", DET." + Ope_Com_Req_Producto.Campo_Cantidad +
                            ", DET." + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado +
                            ", DET." + Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado +
                            ", DET." + Ope_Com_Req_Producto.Campo_IVA_Cotizado +
                            ", DET." + Ope_Com_Req_Producto.Campo_IEPS_Cotizado +
                            ", DET." + Ope_Com_Req_Producto.Campo_Subtota_Cotizado +
                            ", DET." + Ope_Com_Req_Producto.Campo_Total_Cotizado +
                            ", DET." + Ope_Com_Req_Producto.Campo_Proveedor_ID +
                            ", DET." + Ope_Com_Req_Producto.Campo_Nombre_Proveedor +
                            " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " DET" +
                            " JOIN " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + " SER" +
                            " ON SER." + Cat_Com_Servicios.Campo_Servicio_ID +
                            " = DET." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID +
                            " JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ" +
                            " ON REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                            " = DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID +
                            " WHERE REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                            " = '" + Clase_Negocio.P_No_Requisicion + "'";

                    break;
            }
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Requisiciones
        ///DESCRIPCIÓN: Metodo que consulta las requisiciones listas para ser distribuidas a los cotizadores
        ///PARAMETROS: 1.- Cls_Ope_Com_Definir_Cotizadores_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Requisiciones(Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio)
        {
            String Mi_SQL = "SELECT REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_Folio;
            Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_Tipo_Articulo;
            Mi_SQL += ", convert(varchar(10), REQ." + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion;
            Mi_SQL += ",103) AS FECHA";
            Mi_SQL += ",(SELECT " + Cat_Sap_Concepto.Campo_Clave + " +' '+ " + Cat_Sap_Concepto.Campo_Descripcion + " FROM ";
            Mi_SQL += Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
            Mi_SQL += Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
            Mi_SQL += Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
            Mi_SQL += "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
            Mi_SQL += "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = REQ." + Ope_Com_Requisiciones.Campo_Partida_ID + "))) AS CONCEPTO";
            Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ";
            Mi_SQL += " WHERE REQ." + Ope_Com_Requisiciones.Campo_Estatus + "='PROVEEDOR'";
            Mi_SQL += " AND REQ." + Ope_Com_Requisiciones.Campo_Cotizador_ID + "='" + Cls_Sessiones.Empleado_ID + "'";
            if (Clase_Negocio.P_No_Requisicion != null)
            {
                Mi_SQL = Mi_SQL + " AND REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Clase_Negocio.P_No_Requisicion;
            }
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Detalle_Requisicion
        ///DESCRIPCIÓN: Metodo que consulta los detalles de la requisicion seleccionada en el Grid_Requisiciones
        ///PARAMETROS: 1.- Cls_Ope_Com_Definir_Cotizadores_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Detalle_Requisicion(Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio)
        {
            String Mi_SQL = "SELECT ";
            Mi_SQL += " DEPENDENCIA." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
            Mi_SQL += ",(SELECT " + Cat_Sap_Concepto.Campo_Clave + " +' '+ " + Cat_Sap_Concepto.Campo_Descripcion + " FROM ";
            Mi_SQL += Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
            Mi_SQL += Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
            Mi_SQL += Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
            Mi_SQL += "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
            Mi_SQL += "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = REQUISICION." + Ope_Com_Requisiciones.Campo_Partida_ID + "))) AS CONCEPTO";
            Mi_SQL += ",(SELECT " + Cat_Sap_Concepto.Campo_Concepto_ID + " FROM ";
            Mi_SQL += Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
            Mi_SQL += Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
            Mi_SQL += Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
            Mi_SQL += "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
            Mi_SQL += "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = REQUISICION." + Ope_Com_Requisiciones.Campo_Partida_ID + "))) AS CONCEPTO_ID";
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_Tipo;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_Total_Cotizado;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_Subtotal_Cotizado;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_IVA_Cotizado;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_IEPS_Cotizado;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_Folio;
            Mi_SQL += ", convert(varchar(10), REQUISICION." + Ope_Com_Requisiciones.Campo_Fecha_Generacion + ",103) AS FECHA_GENERACION";
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_Estatus;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_Subtotal;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_IEPS;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_IVA;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_Total;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_Justificacion_Compra;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_Especificacion_Prod_Serv;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_Verificaion_Entrega;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_Tipo_Articulo;
            Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_Listado_Almacen;
            Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICION ";
            Mi_SQL += " JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIA ";
            Mi_SQL += " ON REQUISICION." + Ope_Com_Requisiciones.Campo_Dependencia_ID + "= DEPENDENCIA.";
            Mi_SQL += Cat_Dependencias.Campo_Dependencia_ID;
            Mi_SQL += " WHERE REQUISICION." + Ope_Com_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL += "='" + Clase_Negocio.P_No_Requisicion.Trim() + "'";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Proveedores
        ///DESCRIPCIÓN: Metodo que consulta los proveedores de la cotizacion
        ///PARAMETROS:1.- Cls_Ope_Com_Licitacion_Proveedores_Negocio Datos_Lic_Pro
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 15/Enero/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Proveedores(Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio)
        {
            String Mi_SQL = "SELECT PROPUESTA." + Ope_Com_Propuesta_Cotizacion.Campo_Proveedor_ID;
            Mi_SQL += ",(SELECT " + Cat_Com_Proveedores.Campo_Nombre;
            Mi_SQL += " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores;
            Mi_SQL += " WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID+ "=";
            Mi_SQL += " PROPUESTA." + Ope_Com_Propuesta_Cotizacion.Campo_Proveedor_ID + ")";
            Mi_SQL += " FROM " + Ope_Com_Propuesta_Cotizacion.Tabla_Ope_Com_Propuesta_Cotizacion + " PROPUESTA ";
            Mi_SQL += " WHERE PROPUESTA." + Ope_Com_Propuesta_Cotizacion.Campo_No_Requisicion + "='";
            Mi_SQL += Clase_Negocio.P_No_Requisicion + "'";
            Mi_SQL += " GROUP BY (PROPUESTA." + Ope_Com_Propuesta_Cotizacion.Campo_Proveedor_ID + ")";
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Impuesto_Producto
        ///DESCRIPCIÓN: Metodo que consulta los impuestos de los productos
        ///PARAMETROS:1.- Cls_Ope_Com_Licitacion_Proveedores_Negocio Datos_Lic_Pro
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 15/Enero/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Impuesto_Producto(Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio)
        {
            String Mi_SQL = "";
            switch (Clase_Negocio.P_Tipo_Articulo)
            {
                case "PRODUCTO":
                    Mi_SQL = "SELECT PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID +
                             ", PRODUCTOS." + Cat_Com_Productos.Campo_Clave +
                             ", PRODUCTOS." + Cat_Com_Productos.Campo_Nombre + " AS PRODUCTO_NOMBRE" +
                             ", PRODUCTOS." + Cat_Com_Productos.Campo_Costo + " AS PRECIO_UNITARIO" +
                             ", PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_ID +
                             ", PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_2_ID +
                             ",(SELECT " + Cat_Com_Impuestos.Campo_Nombre +
                             " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                             " WHERE PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_ID + "=" +
                             Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS TIPO_IMPUESTO_1 " +
                             ",(SELECT " + Cat_Com_Impuestos.Campo_Nombre +
                             " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                             " WHERE PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_2_ID + "=" +
                             Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS TIPO_IMPUESTO_2 " +
                             ",(SELECT " + Cat_Com_Impuestos.Campo_Porcentaje_Impuesto +
                             " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                             " WHERE PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_ID + "=" +
                             Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS IMPUESTO_PORCENTAJE_1 " +
                             ",(SELECT " + Cat_Com_Impuestos.Campo_Porcentaje_Impuesto +
                             " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                             " WHERE PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_2_ID + "=" +
                             Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS IMPUESTO_PORCENTAJE_2 " +
                             " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS" +
                             " JOIN " + Cat_Com_Impuestos.Tabla_Cat_Impuestos + " IMPUESTOS" +
                             " ON IMPUESTOS." + Cat_Com_Impuestos.Campo_Impuesto_ID + "= PRODUCTOS." + Cat_Com_Productos.Campo_Impuesto_ID +
                             " WHERE PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID +
                             " ='" + Clase_Negocio.P_Producto_ID + "'";
                    break;
                case "SERVICIO":
                    Mi_SQL = "SELECT SER." + Cat_Com_Servicios.Campo_Servicio_ID +
                         ", SER." + Cat_Com_Servicios.Campo_Clave +
                         ", SER." + Cat_Com_Servicios.Campo_Nombre + " AS PRODUCTO_NOMBRE" +
                         ", SER." + Cat_Com_Servicios.Campo_Costo + " AS PRECIO_UNITARIO" +
                         ", SER." + Cat_Com_Servicios.Campo_Impuesto_ID +
                         ",(SELECT " + Cat_Com_Impuestos.Campo_Nombre +
                         " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                         " WHERE SER." + Cat_Com_Servicios.Campo_Impuesto_ID + "=" +
                         Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS TIPO_IMPUESTO " +
                         ",(SELECT " + Cat_Com_Impuestos.Campo_Porcentaje_Impuesto +
                         " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                         " WHERE SER." + Cat_Com_Servicios.Campo_Impuesto_ID + "=" +
                         Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS IMPUESTO_PORCENTAJE " +
                         " FROM " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + " SER" +
                         " JOIN " + Cat_Com_Impuestos.Tabla_Cat_Impuestos + " IMPUESTOS" +
                         " ON IMPUESTOS." + Cat_Com_Impuestos.Campo_Impuesto_ID + "= SER." + Cat_Com_Servicios.Campo_Impuesto_ID +
                         " WHERE SER." + Cat_Com_Servicios.Campo_Servicio_ID +
                         " ='" + Clase_Negocio.P_Producto_ID + "'";


                    break;

            }
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Productos_Servicios
        ///DESCRIPCIÓN: Metodo que Consulta los detalles de la Requisicion seleccionada, ya sea Producto o servicio.
        ///PARAMETROS: 1.- Cls_Ope_Com_Definir_Cotizadores_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Productos_Propuesta(Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio)
        {
            String Mi_SQL = "SELECT * FROM " + Ope_Com_Propuesta_Cotizacion.Tabla_Ope_Com_Propuesta_Cotizacion;
            Mi_SQL += " WHERE " + Ope_Com_Propuesta_Cotizacion.Campo_No_Requisicion;
            Mi_SQL += "='" + Clase_Negocio.P_No_Requisicion.Trim() + "'";
            Mi_SQL += " AND " + Ope_Com_Propuesta_Cotizacion.Campo_Proveedor_ID;
            Mi_SQL += "='" + Clase_Negocio.P_Proveedor_ID.Trim() + "'";
            Mi_SQL += " AND " + Ope_Com_Propuesta_Cotizacion.Campo_Prod_Serv_ID;
            Mi_SQL += "='" + Clase_Negocio.P_Producto_ID.Trim()+"'";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Agregar_Cotizaciones
        ///DESCRIPCIÓN: Metodo para ligar la cotizacion ganadora con la Req. 
        ///PARAMETROS: 1.- Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static bool Agregar_Cotizaciones(Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio)
        {
            bool Operacion_Realizada = false;
            String Mi_SQL = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            SqlDataAdapter Da = new SqlDataAdapter();
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Consultamos la reserva de la requisicion
                Cls_Ope_Com_Requisiciones_Negocio negocio_req = new Cls_Ope_Com_Requisiciones_Negocio();
                negocio_req.P_Requisicion_ID = Clase_Negocio.P_No_Requisicion.Trim();
                int no_reserva = Cls_Ope_Com_Requisiciones_Datos.Consultar_Num_Reserva(negocio_req);
                Double Costo_Promedio = 0;
                Double Ultimo_Costo_Promedio = 0;
                DataTable Dt = new DataTable();
                //RECORREMOS LOS DETALLES DE LA LICITACION,   
                for (int i = 0; i < Clase_Negocio.P_Dt_Productos.Rows.Count; i++)
                {
                    Mi_SQL = "UPDATE " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto +
                             " SET " + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado].ToString().Trim() + "'" +
                             ", " + Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado].ToString().Trim() + "'" +
                             ", " + Ope_Com_Req_Producto.Campo_IVA_Cotizado +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IVA_Cotizado].ToString().Trim() + "'" +
                             ", " + Ope_Com_Req_Producto.Campo_IEPS_Cotizado +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IEPS_Cotizado].ToString().Trim() + "'" +
                             ", " + Ope_Com_Req_Producto.Campo_Subtota_Cotizado +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Subtota_Cotizado].ToString().Trim() + "'" +
                             ", " + Ope_Com_Req_Producto.Campo_Total_Cotizado +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Total_Cotizado].ToString().Trim() + "'" +
                             ", " + Ope_Com_Req_Producto.Campo_Proveedor_ID +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Proveedor_ID].ToString().Trim() + "'" +
                             ", " + Ope_Com_Req_Producto.Campo_Nombre_Proveedor +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Nombre_Proveedor].ToString().Trim() + "'" +
                             " WHERE " + Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID].ToString().Trim() + "'";

                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    //Modificamos el detalle de la propuesta seleccionada

                    Mi_SQL = "UPDATE " + Ope_Com_Propuesta_Cotizacion.Tabla_Ope_Com_Propuesta_Cotizacion;
                    Mi_SQL += " SET " + Ope_Com_Propuesta_Cotizacion.Campo_Resultado;
                    Mi_SQL += " ='ACEPTADA'";
                    Mi_SQL += " WHERE " + Ope_Com_Propuesta_Cotizacion.Campo_Proveedor_ID;
                    Mi_SQL += "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Proveedor_ID].ToString().Trim() + "'";
                    Mi_SQL += " AND " + Ope_Com_Propuesta_Cotizacion.Campo_No_Requisicion;
                    Mi_SQL += "='" + Clase_Negocio.P_No_Requisicion.Trim() + "'";
                    Mi_SQL += " AND " + Ope_Com_Propuesta_Cotizacion.Campo_Ope_Com_Req_Producto_ID;
                    Mi_SQL += "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Prod_Serv_ID].ToString().Trim() + "'";
                    
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    //Consultamos el costo promedio
                    Mi_SQL = "SELECT " + Cat_Com_Productos.Campo_Costo_Promedio;
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Productos.Campo_Producto_ID;
                    Mi_SQL = Mi_SQL + "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Prod_Serv_ID].ToString().Trim() + "'";
                    Cmd.CommandText = Mi_SQL;
                    Da.SelectCommand = Cmd;
                    Dt = new DataTable();
                    Da.Fill(Dt);                    
                    if (Dt.Rows.Count > 0)
                    {
                        Ultimo_Costo_Promedio = double.Parse(Dt.Rows[0][0].ToString().Trim());
                        //Calculamos Costo Promedio
                        Costo_Promedio = (double.Parse(Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado].ToString().Trim()) + Ultimo_Costo_Promedio) / 2;
                        //Actualizar los precios historicos de los productos.
                        Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                        Mi_SQL += " SET " + Cat_Com_Productos.Campo_Costo;
                        Mi_SQL += " ='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado].ToString().Trim() + "', ";
                        Mi_SQL += Cat_Com_Productos.Campo_Costo_Promedio;
                        Mi_SQL += " =" + Costo_Promedio;
                        Mi_SQL += " WHERE " + Cat_Com_Productos.Campo_Producto_ID;
                        Mi_SQL += "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Prod_Serv_ID].ToString().Trim() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                    }
                    else
                    {
                        //Actualizar los precios historicos de los servicios.
                        Mi_SQL = "UPDATE " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios;
                        Mi_SQL += " SET " + Cat_Com_Servicios.Campo_Costo;
                        Mi_SQL += " ='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado].ToString().Trim() + "' ";
                        Mi_SQL += " WHERE " + Cat_Com_Servicios.Campo_Servicio_ID;
                        Mi_SQL += "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Prod_Serv_ID].ToString().Trim() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                    //Actualizamos la reserva
                    Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas +
                    " SET " + Ope_Psp_Reservas.Campo_Proveedor_ID + " = '" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Proveedor_ID].ToString().Trim() + "', " +
                    Ope_Psp_Reservas.Campo_Beneficiario + " = '" + "P-" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Nombre_Proveedor].ToString().Trim() + "'" +
                    " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + no_reserva;
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    
                    //actualizamos el monto de la reserva
                    Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET " + Ope_Psp_Reservas.Campo_Importe_Inicial + " = " + Clase_Negocio.P_Total_Cotizado;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Saldo + " = " + Clase_Negocio.P_Total_Cotizado;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + no_reserva;
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }//fIN FOR I
                Operacion_Realizada = true;
                Trans.Commit();
            }
            catch
            {
                Operacion_Realizada = false;
            }
            finally
            {
                Cn.Close();
            }
            return Operacion_Realizada;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Modificar_Requisicion
        ///DESCRIPCIÓN: Metodo que Modifica la Requisicion seleccionada.
        ///PARAMETROS: 1.- Cls_Ope_Com_Definir_Cotizadores_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static bool Modificar_Requisicion(Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio)
        {
            bool Operacion_Realizada = false;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            String Mi_SQL = "";
            try
            {
                //Consultamos la reserva de la requisicion
                Cls_Ope_Com_Requisiciones_Negocio negocio_req = new Cls_Ope_Com_Requisiciones_Negocio();
                negocio_req.P_Requisicion_ID = Clase_Negocio.P_No_Requisicion.Trim();
                int no_reserva = Cls_Ope_Com_Requisiciones_Datos.Consultar_Num_Reserva(negocio_req);

                Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " SET " + Ope_Com_Requisiciones.Campo_Estatus + "='" + Clase_Negocio.P_Estatus + "'";
                Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_IVA_Cotizado + "='" + Clase_Negocio.P_IVA_Cotizado + "'";
                Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_IEPS_Cotizado + "='" + Clase_Negocio.P_IEPS_Cotizado + "'";
                Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Subtotal_Cotizado + "='" + Clase_Negocio.P_Subtotal_Cotizado + "'";
                Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Total_Cotizado + "='" + Clase_Negocio.P_Total_Cotizado + "'";
                Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion + "=GETDATE()";
                Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + "='" + Clase_Negocio.P_No_Requisicion + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                //RECORREMOS LOS DETALLES DE LA LICITACION,   
                for (int i = 0; i < Clase_Negocio.P_Dt_Productos.Rows.Count; i++)
                {
                    Mi_SQL = "UPDATE " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto +
                             " SET " + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado].ToString().Trim() + "'" +
                             ", " + Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado].ToString().Trim() + "'" +
                             ", " + Ope_Com_Req_Producto.Campo_IVA_Cotizado +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IVA_Cotizado].ToString().Trim() + "'" +
                             ", " + Ope_Com_Req_Producto.Campo_IEPS_Cotizado +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IEPS_Cotizado].ToString().Trim() + "'" +
                             ", " + Ope_Com_Req_Producto.Campo_Subtota_Cotizado +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Subtota_Cotizado].ToString().Trim() + "'" +
                             ", " + Ope_Com_Req_Producto.Campo_Total_Cotizado +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Total_Cotizado].ToString().Trim() + "'" +
                             ", " + Ope_Com_Req_Producto.Campo_Proveedor_ID + " = ";

                    Mi_SQL += String.IsNullOrEmpty(Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Proveedor_ID].ToString().Trim())
                        ? " NULL "
                        : "'" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Proveedor_ID].ToString().Trim() + "'";

                    Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Nombre_Proveedor + " = ";

                    Mi_SQL += String.IsNullOrEmpty(Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Proveedor_ID].ToString().Trim())
                        ? " NULL "
                        : "'" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Nombre_Proveedor].ToString().Trim() + "'";

                    Mi_SQL += " WHERE " + Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID +
                             "='" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID].ToString().Trim() + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    //actualizamos el monto de la reserva
                    Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET " + Ope_Psp_Reservas.Campo_Importe_Inicial + " = " + Clase_Negocio.P_Total_Cotizado;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Saldo + " = " + Clase_Negocio.P_Total_Cotizado;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + no_reserva;
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    //actualizamos el monto de la reserva detalles
                    Mi_SQL = "UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " SET " + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " = " + Clase_Negocio.P_Total_Cotizado;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Clase_Negocio.P_Total_Cotizado;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + "=" + no_reserva;
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Actualizamos la reserva
                    if (Clase_Negocio.P_Estatus != "POR_DIVIDIR")
                        Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Reserva_De_Compra(no_reserva, Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Proveedor_ID].ToString().Trim(), "P-" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Nombre_Proveedor].ToString().Trim());


                }


                Trans.Commit();
                Operacion_Realizada = Cls_Util.Registrar_Historial(Clase_Negocio.P_Estatus, Clase_Negocio.P_No_Requisicion);
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Operacion_Realizada = false;
            }
            finally
            {
                Cn.Close();
            }

            return Operacion_Realizada;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Parametro_Monto_Compra
        ///DESCRIPCIÓN: Metodo que consulta el parametro del monto de compra
        ///PARAMETROS:1.- Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 21/Noviembre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static Double Consultar_Parametro_Monto_Compra(Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio)
        {
            String Mi_SQL = "SELECT " + Cat_Com_Parametros.Campo_Monto_Compra;
            Object Monto_Compra;
            Double Parametro_Monto_Com = 0;
            Mi_SQL += " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros;

            Monto_Compra  =  SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
            if (Convert.IsDBNull(Monto_Compra))
            {
                Parametro_Monto_Com = 0;
            }
            else 
            {
                Parametro_Monto_Com = Convert.ToDouble(Monto_Compra);
            }
            return Parametro_Monto_Com;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Requisicion
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para modificar una requisicion y si es el caso poder confirmarla automaticamente
        ///PARAMETROS:   1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 10/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static Boolean Modificar_Requisicion_Confirmada(Cls_Ope_Com_Propuesta_Ganadora_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "";
            DataTable Dt_Requisicion = new DataTable();
            String Partida_ID = "";
            String Proyecto_ID = "";
            String Dependencia_ID = "";
            String FF = "";
            String Num_Reserva = "";
            double Monto_Anterior = 0;
            double Monto_Cotizado = 0;
            String Cargo = "";
            String Abono = "";
            Boolean Operacion_Realizada = false;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            //DE ACUERDO AL ESTATUS MODIFICAMOS LA REQUISICION
            try
            {
                //1. Primero se consulta las particularidaddes de la reserva, que es partida, dependencia, fte financ, 
                Dt_Requisicion = Consultar_Codigo_Programatico_RQ(Requisicion_Negocio);
                Partida_ID = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim();
                Proyecto_ID = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim();
                Dependencia_ID = Dt_Requisicion.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
                FF = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                Num_Reserva = Dt_Requisicion.Rows[0]["NUM_RESERVA"].ToString().Trim();
                Monto_Anterior = double.Parse(Dt_Requisicion.Rows[0]["TOTAL"].ToString().Trim());
                Monto_Cotizado = double.Parse(Requisicion_Negocio.P_Total_Cotizado);
                Cargo = "";
                Abono = "";
                Cls_Ope_Com_Requisiciones_Negocio req_datos = new Cls_Ope_Com_Requisiciones_Negocio();
                req_datos.P_Partida_ID = Partida_ID;
                DataTable dt = Cls_Ope_Com_Requisiciones_Datos.Consultar_Capitulo(req_datos);
                double Diferencia = 0;
                DataTable Dt_Detalles = new DataTable();
                Dt_Detalles.Columns.Add("FUENTE_FINANCIAMIENTO_ID", typeof(System.String));
                Dt_Detalles.Columns.Add("PROGRAMA_ID", typeof(System.String));
                Dt_Detalles.Columns.Add("DEPENDENCIA_ID", typeof(System.String));
                Dt_Detalles.Columns.Add("PARTIDA_ID", typeof(System.String));
                Dt_Detalles.Columns.Add("ANIO", typeof(System.String));
                Dt_Detalles.Columns.Add("IMPORTE", typeof(System.String));
                Dt_Detalles.Columns.Add("CAPITULO_ID", typeof(System.String));
                DataRow Fila_Nueva = Dt_Detalles.NewRow();
                Fila_Nueva["FUENTE_FINANCIAMIENTO_ID"] = FF;
                Fila_Nueva["PROGRAMA_ID"] = Proyecto_ID;
                Fila_Nueva["DEPENDENCIA_ID"] = Dependencia_ID;
                Fila_Nueva["PARTIDA_ID"] = Partida_ID;
                Fila_Nueva["ANIO"] = DateTime.Now.Year;

                Fila_Nueva["CAPITULO_ID"] = dt.Rows[0][0].ToString();

                switch (Requisicion_Negocio.P_Estatus.Trim())
                {
                    case "CONFIRMADA":

                        //Acutalizar la requisicion 
                        Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                        Mi_SQL += " SET " + Ope_Com_Requisiciones.Campo_Estatus + "='" + Requisicion_Negocio.P_Estatus + "'";
                        Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_IVA_Cotizado + "='" + Requisicion_Negocio.P_IVA_Cotizado + "'";
                        Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_IEPS_Cotizado + "='" + Requisicion_Negocio.P_IEPS_Cotizado + "'";
                        Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Subtotal_Cotizado + "='" + Requisicion_Negocio.P_Subtotal_Cotizado + "'";
                        Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Total_Cotizado + "='" + Requisicion_Negocio.P_Total_Cotizado + "'";
                        Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion + " =GETDATE() ";
                        Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Empleado_Cotizacion_ID + "='" + Requisicion_Negocio.P_Empleado_ID + "'";
                        Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + "='" + Requisicion_Negocio.P_No_Requisicion + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        Operacion_Realizada = Cls_Util.Registrar_Historial(Requisicion_Negocio.P_Estatus, Requisicion_Negocio.P_No_Requisicion);

                        //Modificamos el presupuesto                   

                        //2. Verificamos a que momento presupuestal se le hace el cargo o el abono de acuerdo a la direrencia.

                        if (Monto_Cotizado > Monto_Anterior)
                        {
                            Cargo = Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO;
                            Abono = Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE;
                            Diferencia = Monto_Cotizado - Monto_Anterior;
                        }
                        if (Monto_Cotizado < Monto_Anterior)
                        {
                            Cargo = Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE;
                            Abono = Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO;
                            Diferencia = Monto_Anterior - Monto_Cotizado;
                        }
                        Fila_Nueva["IMPORTE"] = Diferencia;
                        Dt_Detalles.Rows.Add(Fila_Nueva);
                        Dt_Detalles.AcceptChanges();
                        //Actualizamos el Presupuesto
                        int registro = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cargo, Abono, Dt_Detalles, Cmd);
                        //Registramos el movimiento presupuestal
                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Num_Reserva, Cargo, Abono, Diferencia, "", "", "", "", Cmd);


                        break;
                    case "PROVEEDOR":
                        Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                        Mi_SQL += " SET " + Ope_Com_Requisiciones.Campo_Estatus + "='" + Requisicion_Negocio.P_Estatus + "'";
                        Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_IVA_Cotizado + "='" + Requisicion_Negocio.P_IVA_Cotizado + "'";
                        Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_IEPS_Cotizado + "='" + Requisicion_Negocio.P_IEPS_Cotizado + "'";
                        Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Subtotal_Cotizado + "='" + Requisicion_Negocio.P_Subtotal_Cotizado + "'";
                        Mi_SQL += ", " + Ope_Com_Requisiciones.Campo_Total_Cotizado + "='" + Requisicion_Negocio.P_Total_Cotizado + "'";
                        Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + "='" + Requisicion_Negocio.P_No_Requisicion + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        Operacion_Realizada = Cls_Util.Registrar_Historial(Requisicion_Negocio.P_Estatus, Requisicion_Negocio.P_No_Requisicion);

                        //return Operacion_Realizada;
                        break;
                }
                //actualizamos el monto de la reserva
                Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET " + Ope_Psp_Reservas.Campo_Importe_Inicial + " = " + Requisicion_Negocio.P_Total_Cotizado;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Saldo + " = " + Requisicion_Negocio.P_Total_Cotizado;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + Num_Reserva;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                //actualizamos el monto de la reserva detalles
                Mi_SQL = "UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " SET " + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " = " + Requisicion_Negocio.P_Total_Cotizado;
                Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Requisicion_Negocio.P_Total_Cotizado;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + "=" + Num_Reserva;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Operacion_Realizada = false;
            }
            finally
            {
                Cn.Close();
            }
            return Operacion_Realizada;

        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Codigo_Programatico_RQ
        ///DESCRIPCIÓN: Metodo que obitene el codigo proramatico
        ///PARAMETROS:   1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 10/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Codigo_Programatico_RQ(Cls_Ope_Com_Propuesta_Ganadora_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "";

            Mi_SQL = "SELECT REQ_DET." + Ope_Com_Req_Producto.Campo_Partida_ID +
                             ",REQ_DET." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID +
                             ", (SELECT " + Ope_Com_Requisiciones.Campo_Total +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS TOTAL" +
                             ", (SELECT " + Ope_Com_Requisiciones.Campo_Total_Cotizado +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS TOTAL_COTIZADO" +
                             ", (SELECT " + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS DEPENDENCIA_ID" +
                             ", REQ_DET." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID +
                             ", (SELECT NUM_RESERVA" +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS NUM_RESERVA" +
                             " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_DET" +
                             " WHERE REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + "='" + Requisicion_Negocio.P_No_Requisicion + "'";
            DataTable Dt_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Requisicion;

        }

        #endregion

    }
}
