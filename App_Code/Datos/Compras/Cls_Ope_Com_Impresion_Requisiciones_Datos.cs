﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Compras.Impresion_Requisiciones.Negocio;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;

namespace JAPAMI.Compras.Impresion_Requisiciones.Datos
{

    public class Cls_Ope_Com_Impresion_Requisiciones_Datos
    {
        public Cls_Ope_Com_Impresion_Requisiciones_Datos()
        {            
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Consultar_Requisiciones
        ///DESCRIPCIÓN: Consultar los detalles de la requisicion
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 04/28/2011 04:37:00 p.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Consultar_Requisiciones_Detalles()
        {

        }
        public static DataTable Consultar_Requisiciones(Cls_Ope_Com_Impresion_Requisiciones_Negocio Datos)
        {
            //Declaracion de Variables
            String Mi_SQL = String.Empty; //Variable para las consultas

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT ";
                Mi_SQL = Mi_SQL + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + ".*, ";
                //Mi_SQL = Mi_SQL + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + ".*, ";
                Mi_SQL = Mi_SQL + "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM ";
                Mi_SQL = Mi_SQL + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + " = " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Dependencia_ID + ")";
                Mi_SQL = Mi_SQL + " AS UNIDAD_RESPONSABLE, ";


                Mi_SQL = Mi_SQL + " ( SELECT ";
                Mi_SQL = Mi_SQL + Cat_Empleados.Tabla_Cat_Empleados + ".";
                Mi_SQL = Mi_SQL + Cat_Empleados.Campo_Nombre + " +' '+ ";
                Mi_SQL = Mi_SQL + Cat_Empleados.Tabla_Cat_Empleados + ".";
                Mi_SQL = Mi_SQL + Cat_Empleados.Campo_Apellido_Paterno + " +' '+ ";
                Mi_SQL = Mi_SQL + Cat_Empleados.Tabla_Cat_Empleados + ".";
                Mi_SQL = Mi_SQL + Cat_Empleados.Campo_Apellido_Materno;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " WHERE ";
                Mi_SQL = Mi_SQL + Cat_Empleados.Tabla_Cat_Empleados + ".";
                Mi_SQL = Mi_SQL + Cat_Empleados.Campo_Empleado_ID + " = ";
                Mi_SQL = Mi_SQL + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + ".";
                Mi_SQL = Mi_SQL + Ope_Com_Requisiciones.Campo_Empleado_Autorizacion_ID + " ) AS EMPLEADO ";

                Mi_SQL = Mi_SQL + "FROM ";
                Mi_SQL = Mi_SQL + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL = Mi_SQL + " WHERE " +
                    Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + 
                    Ope_Com_Requisiciones.Campo_Estatus + " IS NOT NULL ";


                if (!String.IsNullOrEmpty(Datos.P_Requisicion_ID))
                {
                    Mi_SQL = Mi_SQL + " AND ";
                    Mi_SQL = Mi_SQL + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = '" + Datos.P_Requisicion_ID + "' ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Dependencia_ID))
                {
                    Mi_SQL = Mi_SQL + " AND ";
                    Mi_SQL = Mi_SQL + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "' ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Estatus))
                {
                    Mi_SQL = Mi_SQL + " AND ";
                    Mi_SQL = Mi_SQL + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Estatus + " = '" + Datos.P_Estatus + "' ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicial) && !String.IsNullOrEmpty(Datos.P_Fecha_Final))
                {
                    Mi_SQL = Mi_SQL + " AND TO_DATE(TO_CHAR(" + Ope_Com_Requisiciones.Campo_Fecha_Creo + ",'DD-MM-YYYY'))" +
                            " >= '" + Datos.P_Fecha_Inicial + "' AND " +
                    "TO_DATE(TO_CHAR(" + Ope_Com_Requisiciones.Campo_Fecha_Creo + ",'DD-MM-YYYY'))" +
                            " <= '" + Datos.P_Fecha_Final + "'";
                }

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Consultar_Requisiciones_Detalles
        ///DESCRIPCIÓN: Consultar los detalles de la requisicion
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 04/28/2011 04:37:00 p.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************        
        internal static DataTable Consultar_Requisiciones_Detalles(Cls_Ope_Com_Impresion_Requisiciones_Negocio Datos)
        {
            //Declaracion de Variables
            String Mi_SQL = String.Empty; //Variable para las consultas
            //Mi_SQL = "IF ((SELECT TIPO_ARTICULO FROM OPE_COM_REQUISICIONES WHERE NO_REQUISICION = '" + Datos.P_Requisicion_ID + "') = 'PRODUCTO') THEN ";
            Mi_SQL = "SELECT " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + "." + Cat_Com_Unidades.Campo_Abreviatura + " AS UNIDAD,";
            Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + ".*"; 
            Mi_SQL += " FROM ";
            Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
            Mi_SQL += " LEFT JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
            Mi_SQL += " ON " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID;
            Mi_SQL += " = ";
            Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Producto_ID;
            Mi_SQL += " LEFT JOIN " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " ON ";
            Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Unidad_ID + " = ";
            Mi_SQL += Cat_Com_Unidades.Tabla_Cat_Com_Unidades + "." + Cat_Com_Unidades.Campo_Unidad_ID;
            Mi_SQL += " WHERE " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + ".";
            Mi_SQL += Ope_Com_Req_Producto.Campo_Requisicion_ID + " = '" + Datos.P_Requisicion_ID + "' ";
            //Mi_SQL += " ELSE SELECT * FROM OPE_COM_REQ_PRODUCTO WHERE NO_REQUISICION =" + Datos.P_Requisicion_ID;
            try
            {
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
    
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Consultar_Requisiciones_Bienes
        ///DESCRIPCIÓN: Consultar los bienes de la requisicion
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 04/28/2011 04:37:00 p.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************        
        internal static DataTable Consultar_Requisiciones_Bienes(Cls_Ope_Com_Impresion_Requisiciones_Negocio Datos)
        {
            DataTable Dt_Datos = new DataTable();
            try
            {
                String Mi_SQL = "SELECT REQUISICIONES_BIENES." + Ope_Com_Req_Bienes.Campo_Bien_Mueble_ID + " AS BIEN_MUEBLE_ID";
                Mi_SQL = Mi_SQL + ", STR(BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + ") AS NUMERO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Nombre + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie + " AS SERIE";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Modelo + " AS MODELO";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Req_Bienes.Tabla_Ope_Com_Req_Bienes + " REQUISICIONES_BIENES";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " BIENES_MUEBLES";
                Mi_SQL = Mi_SQL + " ON REQUISICIONES_BIENES." + Ope_Com_Req_Bienes.Campo_Bien_Mueble_ID + " = BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID;
                Mi_SQL = Mi_SQL + " WHERE REQUISICIONES_BIENES." + Ope_Com_Req_Bienes.Campo_Requisicion_ID + " = '" + Datos.P_Requisicion_ID + "'";
                DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Datos != null)
                {
                    if (Ds_Datos.Tables.Count > 0)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
            }
            catch (SqlException Ex)
            {
                throw new Exception("Excepción al consultar los Bienes de la Requisición: [" + Ex.Message + "]");
            }
            return Dt_Datos;
        }
    
    
    
    }
}
        