﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Generar_Requisicion.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;
using JAPAMI.Administrar_Requisiciones.Negocios;
using System.Data.SqlClient;
using JAPAMI.Manejo_Presupuesto.Datos;
namespace JAPAMI.Generar_Requisicion.Datos
{
    public class Cls_Ope_Com_Requisiciones_Datos
    {
        public Cls_Ope_Com_Requisiciones_Datos()
        {

        }
        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************
        #region UTILERIAS
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),0) FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }

        public static DataTable Consultar_Columnas_De_Tabla_BD(String Nombre_Tabla)
        {
           // String Mi_Sql = "SELECT COLUMN_NAME AS COLUMNA FROM ALL_TAB_COLUMNS WHERE TABLE_NAME = '" + Nombre_Tabla + "'";
            String Mi_Sql = "SELECT ISNULL(TABLE_NAME,'') as Tabla," +
                          " ISNULL(COLUMN_NAME,'') as COLUMNA" +
                          " From INFORMATION_SCHEMA.Columns " +
                          " WHERE TABLE_SCHEMA = 'dbo' " +
                          " AND TABLE_NAME IN (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES " +
                          " WHERE TABLE_TYPE = 'BASE TABLE' " +
                          " AND TABLE_CATALOG='JAPAMI_SIAC' " +
                          " AND (TABLE_NAME = '" + Nombre_Tabla + "') " +
                          " )";


            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Capitulo
        ///DESCRIPCIÓN: Obtiene el capitulo de una partida especifica
        ///PARAMETROS: 1.-Id de la partida
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 14/Mayo/2013
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Capitulo(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            DataTable dt_capitulos = new DataTable();
            String Mi_SQL = "SELECT " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto;
            Mi_SQL = Mi_SQL + " JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica;
            Mi_SQL = Mi_SQL + " ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID;
            Mi_SQL = Mi_SQL + "=" + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID;
            Mi_SQL = Mi_SQL + " JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL = Mi_SQL + " ON " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
            Mi_SQL = Mi_SQL + "=" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
            Mi_SQL = Mi_SQL + "='" + Requisicion_Negocio.P_Partida_ID.Trim() + "'";
            dt_capitulos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return dt_capitulos;
        }

        #endregion

        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************
        #region GUARDAR REQUISICION / INSERTAR REQUISICION, GUARDAR SUS DETALLES 
        public static String Guardar_Nueva_Requisicion(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mensaje = "";
            String Fecha_Creo = DateTime.Now.ToString("dd/MM/yy").ToUpper();
            String Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
            String Mi_SQL = "";
            //INSERTAR LA REQUISICION   
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                Requisicion_Negocio.P_Requisicion_ID = "" + Obtener_Consecutivo(Ope_Com_Requisiciones.Campo_Requisicion_ID, Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones);
                String Requisicion_Num = Requisicion_Negocio.P_Requisicion_ID + "";
                Requisicion_Negocio.P_Folio = "RQ-" + Requisicion_Negocio.P_Requisicion_ID;
                Mi_SQL = "SELECT CLAVE FROM CAT_SAP_AREA_FUNCIONAL WHERE AREA_FUNCIONAL_ID = " +
                    "(SELECT AREA_FUNCIONAL_ID FROM CAT_DEPENDENCIAS WHERE DEPENDENCIA_ID = '" + Requisicion_Negocio.P_Dependencia_ID + "')";
                DataSet Ds_Area_Funcional = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Area_Funcional != null && Ds_Area_Funcional.Tables[0].Rows.Count > 0)
                {
                    String Area = Ds_Area_Funcional.Tables[0].Rows[0]["CLAVE"].ToString();
                    Requisicion_Negocio.P_Codigo_Programatico = Requisicion_Negocio.P_Codigo_Programatico.Replace("1.1.1", Area);
                }
                //insertar cuando la requisicion quedo en estatus de contruccion
                Mi_SQL = "INSERT INTO " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                " (" + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                ", " + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                ", " + Ope_Com_Requisiciones.Campo_Folio +
                ", " + Ope_Com_Requisiciones.Campo_Estatus +
                ", " + Ope_Com_Requisiciones.Campo_Codigo_Programatico +
                ", " + Ope_Com_Requisiciones.Campo_Elemento_PEP +
                ", " + Ope_Com_Requisiciones.Campo_Tipo +
                ", " + Ope_Com_Requisiciones.Campo_Fase +
                ", " + Ope_Com_Requisiciones.Campo_Subtotal +
                ", " + Ope_Com_Requisiciones.Campo_IVA +
                ", " + Ope_Com_Requisiciones.Campo_IEPS +
                ", " + Ope_Com_Requisiciones.Campo_Total +
                ", " + Ope_Com_Requisiciones.Campo_Usuario_Creo +
                ", " + Ope_Com_Requisiciones.Campo_Fecha_Creo +
                ", " + Ope_Com_Requisiciones.Campo_Justificacion_Compra +
                ", " + Ope_Com_Requisiciones.Campo_Especificacion_Prod_Serv +
                ", " + Ope_Com_Requisiciones.Campo_Verificaion_Entrega +
                ", " + Ope_Com_Requisiciones.Campo_Consolidada +
                ", " + Ope_Com_Requisiciones.Campo_Tipo_Articulo +
                ", " + Ope_Com_Requisiciones.Campo_Partida_ID +
                ", " + Ope_Com_Requisiciones.Campo_Transitoria_Stock;
                if (Requisicion_Negocio.P_Estatus == "GENERADA")
                {
                    Mi_SQL = Mi_SQL +
                             ", " + Ope_Com_Requisiciones.Campo_Empleado_Generacion_ID +
                             ", " + Ope_Com_Requisiciones.Campo_Fecha_Generacion;
                }
                else
                {
                    Mi_SQL = Mi_SQL +
                             ", " + Ope_Com_Requisiciones.Campo_Empleado_Construccion_ID +
                             ", " + Ope_Com_Requisiciones.Campo_Fecha_Construccion;
                }
                Mi_SQL = Mi_SQL +
                ") VALUES (" +
                Requisicion_Negocio.P_Requisicion_ID + ",'" +
                Requisicion_Negocio.P_Dependencia_ID + "','" +
                Requisicion_Negocio.P_Folio + "','" +
                Requisicion_Negocio.P_Estatus + "','" +
                Requisicion_Negocio.P_Codigo_Programatico + "','" +
                Requisicion_Negocio.P_Elemento_PEP + "','" +
                Requisicion_Negocio.P_Tipo + "','" +
                Requisicion_Negocio.P_Fase + "'," +
                Requisicion_Negocio.P_Subtotal + ", " +
                Requisicion_Negocio.P_IVA + ", " +
                Requisicion_Negocio.P_IEPS + ", " +
                Requisicion_Negocio.P_Total + ",'" +
                Usuario_Creo + "','" +
                Fecha_Creo + "','" +
                Requisicion_Negocio.P_Justificacion_Compra + "','" +
                Requisicion_Negocio.P_Especificacion_Productos + "','" +
                Requisicion_Negocio.P_Verificacion_Entrega + "','NO','" +
                Requisicion_Negocio.P_Tipo_Articulo + "','" +
                Requisicion_Negocio.P_Partida_ID + "','" +
                Requisicion_Negocio.P_Transitoria_Stock + "','" +
                Cls_Sessiones.Empleado_ID + "','" +
                Fecha_Creo + "')";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                //insertar los productos que se hayn seleccionado 
                //para la requisa validando q si hay productos agregados
                //String Mi_SQL = "";
                int Consecutivo_Productos_Requisa =
                    Obtener_Consecutivo(Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID, Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto);
                foreach (DataRow Renglon in Requisicion_Negocio.P_Dt_Productos_Servicios.Rows)
                {
                    Mi_SQL = "INSERT INTO " +
                        Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto +
                        " (" + Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID +
                        ", " + Ope_Com_Req_Producto.Campo_Requisicion_ID +
                        ", " + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID +
                        ", " + Ope_Com_Req_Producto.Campo_Prod_Serv_ID +
                        ", " + Ope_Com_Req_Producto.Campo_Clave +
                        ", " + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio +
                        ", " + Ope_Com_Req_Producto.Campo_Partida_ID +
                        ", " + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID +
                        ", " + Ope_Com_Req_Producto.Campo_Cantidad +
                        ", " + Ope_Com_Req_Producto.Campo_Tipo +
                        ", " + Ope_Com_Req_Producto.Campo_Giro_ID +
                        ", " + Ope_Com_Req_Producto.Campo_Nombre_Giro +
                        ", " + Ope_Com_Req_Producto.Campo_Usuario_Creo +
                        ", " + Ope_Com_Req_Producto.Campo_Fecha_Creo +
                        ", " + Ope_Com_Req_Producto.Campo_Precio_Unitario +
                        ", " + Ope_Com_Req_Producto.Campo_Importe +
                        ", " + Ope_Com_Req_Producto.Campo_Monto_IVA +
                        ", " + Ope_Com_Req_Producto.Campo_Monto_IEPS +
                        ", " + Ope_Com_Req_Producto.Campo_Porcentaje_IVA +
                        ", " + Ope_Com_Req_Producto.Campo_Porcentaje_IEPS +
                        ", " + Ope_Com_Req_Producto.Campo_Monto_Total;
                    if (Renglon["Especificacion_Producto"].ToString().Trim() != String.Empty)
                    {
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Especificacion_Producto;
                    }

                    Mi_SQL = Mi_SQL + " ) VALUES " +
                    "(" + Consecutivo_Productos_Requisa +
                    "," + Requisicion_Negocio.P_Requisicion_ID +
                    ",'" + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() +
                    "','" + Renglon["Prod_Serv_ID"].ToString().Trim() +
                    "','" + Renglon["Clave"].ToString().Trim() +
                    "','" + Renglon["Nombre_Producto_Servicio"].ToString().Trim() +
                    "','" + Renglon["Partida_ID"].ToString().Trim() +
                    "','" + Renglon["Proyecto_Programa_ID"].ToString().Trim() +
                    "', " + Renglon["Cantidad"].ToString().Trim() +
                    ", '" + Renglon["Tipo"].ToString().Trim() +
                    "','" + Renglon["Concepto_ID"].ToString().Trim() +
                    "','" + Renglon["Nombre_Concepto"].ToString().Trim() +
                    "','" + Cls_Sessiones.Nombre_Empleado +
                    "', " + "GETDATE()" +
                    ", " + Renglon["Precio_Unitario"].ToString().Trim() +
                    ", " + Renglon["Monto"].ToString().Trim() +
                    ", " + Renglon["Monto_IVA"].ToString().Trim() +
                    ", " + Renglon["Monto_IEPS"].ToString().Trim() +
                    ", " + Renglon["Porcentaje_IVA"].ToString().Trim() +
                    ", " + Renglon["Porcentaje_IEPS"].ToString().Trim() +
                    ", " + Renglon["Monto_Total"].ToString().Trim();
                    if (Renglon["Especificacion_Producto"].ToString().Trim() != String.Empty)
                    {
                        Mi_SQL = Mi_SQL + ",'" + Renglon["Especificacion_Producto"].ToString().Trim() + "'";
                    }
                    Mi_SQL = Mi_SQL + ")";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Consecutivo_Productos_Requisa++;
                }

                foreach (DataRow Renglon in Requisicion_Negocio.P_Dt_Bienes.Rows)
                {
                    Mi_SQL = "INSERT INTO " + Ope_Com_Req_Bienes.Tabla_Ope_Com_Req_Bienes +
                        " (" + Ope_Com_Req_Bienes.Campo_Requisicion_ID +
                        ", " + Ope_Com_Req_Bienes.Campo_Bien_Mueble_ID +
                        ", " + Ope_Com_Req_Bienes.Campo_Usuario_Creo +
                        ", " + Ope_Com_Req_Bienes.Campo_Fecha_Creo + " ) VALUES " +
                        "(" + Requisicion_Negocio.P_Requisicion_ID +
                        ",'" + Renglon["BIEN_MUEBLE_ID"].ToString().Trim() +
                        "','" + Cls_Sessiones.Nombre_Empleado +
                        "', " + "GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }


                //Guardar Comentarios
                if (Requisicion_Negocio.P_Comentarios.Length > 0)
                {
                    String Consecutivo = "";
                    Object Asunto_ID; //Obtiene el ID con la cual se guardo los datos en la base de datos

                    Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Com_Req_Observaciones.Campo_Observacion_ID + "),'0') ";
                    Mi_SQL = Mi_SQL + "FROM " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones;
                    Cmd.CommandText = Mi_SQL;
                    Asunto_ID = Cmd.ExecuteScalar();

                    if (Convert.IsDBNull(Asunto_ID))
                    {
                        Consecutivo = "1";
                    }
                    else
                    {
                        Consecutivo = string.Format("{0:0}", Convert.ToInt32(Asunto_ID) + 1);
                    }

                    Mi_SQL = "INSERT INTO " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones +
                        " (" + Ope_Com_Req_Observaciones.Campo_Observacion_ID +
                        ", " + Ope_Com_Req_Observaciones.Campo_Requisicion_ID +
                        ", " + Ope_Com_Req_Observaciones.Campo_Comentario +
                        ", " + Ope_Com_Req_Observaciones.Campo_Estatus +
                        ", " + Ope_Com_Req_Observaciones.Campo_Usuario_Creo +
                        ", " + Ope_Com_Req_Observaciones.Campo_Fecha_Creo +
                        ") VALUES (" +
                        Consecutivo + ",'" +
                        Requisicion_Negocio.P_Requisicion_ID + "','" +
                        Requisicion_Negocio.P_Comentarios + "','" +
                        Requisicion_Negocio.P_Estatus + "','" +
                        Cls_Sessiones.Nombre_Empleado + "',GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                //Si la requisición es de STOCK se comprometen los productos
                if (Requisicion_Negocio.P_Tipo == "STOCK")
                {

                    DataTable Dt_Productos_Almacen = null;
                    // String Mi_SQL = "";
                    Mi_SQL =
                    "SELECT " + Cat_Com_Productos.Campo_Producto_ID + "," + Cat_Com_Productos.Campo_Existencia + "," +
                    Cat_Com_Productos.Campo_Disponible + "," + Cat_Com_Productos.Campo_Comprometido + "," +
                    Cat_Com_Productos.Campo_Nombre + "," + Cat_Com_Productos.Campo_Comprometido + "," +
                    Cat_Com_Productos.Campo_Descripcion + "," + Cat_Com_Productos.Campo_Comprometido +
                    " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                    " WHERE " + Cat_Com_Productos.Campo_Stock + " = 'SI'";
                    DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0)
                    {
                        Dt_Productos_Almacen = Data_Set.Tables[0];
                    }
                    if (Dt_Productos_Almacen != null)
                    {
                        int Disponible_Stock = 0;
                        int Comprometido_Stock = 0;
                        int Cantidad = 0;
                        String Producto_ID = "";
                        foreach (DataRow Producto_Requisicion in Requisicion_Negocio.P_Dt_Productos_Servicios.Rows)
                        {
                            Cantidad = int.Parse(Producto_Requisicion["CANTIDAD"].ToString().Trim());
                            Producto_ID = Producto_Requisicion["PROD_SERV_ID"].ToString().Trim();
                            DataRow[] Dr_Productos_Almacen = Dt_Productos_Almacen.Select("PRODUCTO_ID ='" + Producto_ID + "'");
                            Disponible_Stock = int.Parse(Dr_Productos_Almacen[0]["DISPONIBLE"].ToString().Trim());
                            Comprometido_Stock = int.Parse(Dr_Productos_Almacen[0]["COMPROMETIDO"].ToString().Trim());
                            if (Disponible_Stock >= Cantidad)
                            {
                                //Compromete
                                Disponible_Stock = Disponible_Stock - Cantidad;
                                Comprometido_Stock = Comprometido_Stock + Cantidad;
                                Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                                    " SET " + Cat_Com_Productos.Campo_Comprometido +
                                    " = " + Comprometido_Stock + ", " +
                                    Cat_Com_Productos.Campo_Disponible +
                                    " = " + Disponible_Stock + "WHERE " +
                                    Cat_Com_Productos.Campo_Producto_ID + " = '" +
                                    Producto_ID + "'";
                                Cmd.CommandText = Mi_SQL;
                                Cmd.ExecuteNonQuery();
                                Mensaje = "EXITO";
                            }
                            else
                            {
                                //No compromete y manda mensaje
                                Mensaje = "CANTIDAD INSUFICIENTE DEL PRODUCTO " + Dr_Productos_Almacen[0]["NOMBRE"].ToString().Trim() + ", " +
                                Dr_Productos_Almacen[0]["DESCRIPCION"].ToString().Trim() + ", DEBIDO A QUE SE COMPROMETIO EN UNA REQUISICION" +
                                " ANTERIOR A LA DE USTED. DISPONIBLE[" + Disponible_Stock + "]";
                            }
                        }
                    }
                }
                else
                {
                    Mensaje = "EXITO";
                }
                if (Mensaje == "EXITO")
                {
                    //consultamos el capitulo de la partida

                    DataTable dt = Cls_Ope_Com_Requisiciones_Datos.Consultar_Capitulo(Requisicion_Negocio);

                    //Creamos el datatable del Codifo Programatico
                    //Construimos el datatable
                    DataTable Dt_Detalles = new DataTable();
                    Dt_Detalles.Columns.Add("FUENTE_FINANCIAMIENTO_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("PROGRAMA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("DEPENDENCIA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("PARTIDA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("ANIO", typeof(System.String));
                    Dt_Detalles.Columns.Add("IMPORTE", typeof(System.String));
                    Dt_Detalles.Columns.Add("CAPITULO_ID", typeof(System.String));
                    DataRow Fila_Nueva = Dt_Detalles.NewRow();
                    Fila_Nueva["FUENTE_FINANCIAMIENTO_ID"] = Requisicion_Negocio.P_Fuente_Financiamiento;
                    Fila_Nueva["PROGRAMA_ID"] = Requisicion_Negocio.P_Proyecto_Programa_ID;
                    Fila_Nueva["DEPENDENCIA_ID"] = Requisicion_Negocio.P_Dependencia_ID;
                    Fila_Nueva["PARTIDA_ID"] = Requisicion_Negocio.P_Partida_ID;
                    Fila_Nueva["ANIO"] = DateTime.Now.Year;
                    Fila_Nueva["IMPORTE"] = Requisicion_Negocio.P_Total;
                    Fila_Nueva["CAPITULO_ID"] = dt.Rows[0][0].ToString();
                    Dt_Detalles.Rows.Add(Fila_Nueva);
                    Dt_Detalles.AcceptChanges();
                    //Crear Reserva


                    String No_Reserva = Cls_Ope_Psp_Manejo_Presupuesto.Crear_Reserva
                        (Requisicion_Negocio.P_Dependencia_ID, "GENERADA", "",
                        Requisicion_Negocio.P_Fuente_Financiamiento,
                        Requisicion_Negocio.P_Proyecto_Programa_ID,
                        Requisicion_Negocio.P_Folio,
                        DateTime.Now.Year.ToString(),
                        Convert.ToDouble(Requisicion_Negocio.P_Total), "", "", "00007", Dt_Detalles, "RECURSO ASIGNADO", Cmd).ToString();
                    int Registros = 0;
                    //Comprometer presupuesto partida           
                    if (Requisicion_Negocio.P_Tipo == "TRANSITORIA")
                    {
                        Registros = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Dt_Detalles, Cmd);
                        Cls_Ope_Psp_Manejo_Presupuesto.
                            Registro_Movimiento_Presupuestal(
                                No_Reserva,
                                Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO,
                                Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE,
                                Convert.ToDouble(Requisicion_Negocio.P_Total),
                                "",
                                "",
                                "",
                                "", Cmd);
                    }
                    else
                    {

                        Registros = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Dt_Detalles, Cmd);
                        //Registrar movimiento presupuestal
                        Cls_Ope_Psp_Manejo_Presupuesto.
                            Registro_Movimiento_Presupuestal(
                                No_Reserva,
                                Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO,
                                Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE,
                                Convert.ToDouble(Requisicion_Negocio.P_Total),
                                "",
                                "",
                                "",
                                "", Cmd);
                    }
                    //Actualizar reserva en requisicion               
                    Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                        " SET NUM_RESERVA = " + No_Reserva +
                        " WHERE " +
                        Ope_Com_Requisiciones.Campo_Folio + " ='" + Requisicion_Negocio.P_Folio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Mensaje = "Requisición registrada con el folio: " + Requisicion_Negocio.P_Requisicion_ID + " / Número de reserva: " + No_Reserva;
                    Registrar_Historial(Requisicion_Negocio.P_Estatus, Requisicion_Negocio.P_Requisicion_ID);
                    Trans.Commit();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje = "No se pudo guardar requisición, consulte a su administrador";
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje;
        }

        private String Comprometer_Disponible_Stock(DataTable Dt_Productos_Requisicion) 
        {
            String Mensaje = "";
            DataTable Dt_Productos_Almacen = null;
            String Mi_SQL = "";
            Mi_SQL =
            "SELECT " + Cat_Com_Productos.Campo_Producto_ID + "," + Cat_Com_Productos.Campo_Existencia + "," +
            Cat_Com_Productos.Campo_Disponible + "," + Cat_Com_Productos.Campo_Comprometido +
            " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
            " WHERE " + Cat_Com_Productos.Campo_Stock + " = 'SI'";
            DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0)
            {
                Dt_Productos_Almacen = Data_Set.Tables[0];
            }
            if (Dt_Productos_Almacen != null) 
            {
                int Disponible_Stock = 0;
                int Comprometido_Stock = 0;
                int Cantidad = 0;
                String Producto_ID = "";
                foreach (DataRow Producto_Requisicion in Dt_Productos_Requisicion.Rows)
                {
                    Cantidad = int.Parse(Producto_Requisicion["CANTIDAD"].ToString().Trim());
                    Producto_ID = Producto_Requisicion["PRODUCTO_ID"].ToString().Trim();
                    DataRow[] Dr_Productos_Almacen = Dt_Productos_Almacen.Select("PRODUCTO_ID ='" + Producto_Requisicion["PROD_SERV_ID"].ToString().Trim() + "'");
                    Disponible_Stock = int.Parse( Dr_Productos_Almacen[0]["DISPONIBLE"].ToString().Trim());
                    Comprometido_Stock = int.Parse(Dr_Productos_Almacen[0]["COMPROMETIDO"].ToString().Trim());
                    if (Disponible_Stock >= Cantidad)
                    {
                        //Compromete
                        Disponible_Stock = Disponible_Stock - Cantidad;
                        Comprometido_Stock = Comprometido_Stock + Cantidad;
                        Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                            " SET " + Cat_Com_Productos.Campo_Comprometido +
                            " = " + Comprometido_Stock + ", " +
                            Cat_Com_Productos.Campo_Disponible +
                            " = " + Disponible_Stock + "WHERE " +
                            Cat_Com_Productos.Campo_Producto_ID + " = '" +
                            Producto_ID + "'";
                        //Cmd.CommandText = Mi_SQL;
                        //Cmd.ExecuteNonQuery();
                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        Mensaje = "EXITO";
                    }
                    else
                    {
                        //No compromete y manda mensaje
                        Mensaje = "CANTIDAD INSUFICIENTE DEL PRODUCTO " + Producto_Requisicion["NOMBRE"].ToString().Trim() + ", " +
                        Producto_Requisicion["DESCRIPCION"].ToString().Trim() + ", DEBIDO A QUE SE COMPROMETIO EN UNA REQUISICION" +
                        " ANTERIOR A LA DE USTED. DISPONIBLE[" + Disponible_Stock + "]";                        
                    }
                }
            }
            return Mensaje;
        }
        public static String Proceso_Insertar_Requisicion(
            Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            if (Cls_Sessiones.Nombre_Empleado != null && Cls_Sessiones.Area_ID_Empleado != null)
            {
                return Guardar_Nueva_Requisicion(Requisicion_Negocio);
            }
            else
            {
                return "No se guardó la requisición, reinicie sesión";
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Insertar_Requisicion
        ///DESCRIPCIÓN: crea una sentencia sql para insertar una Requisa en la base de datos
        ///PARAMETROS: 1.-Clase de Negocio
        ///            2.-Usuario que crea la requisa
        ///CREO: Silvia Morales Portuhondo
        ///FECHA_CREO: Noviembre/2010 
        ///MODIFICO:Gustavo Angeles Cruz
        ///FECHA_MODIFICO: 25/Ene/2011
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************            
        private static String Insertar_Requisicion(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio) {
            String Fecha_Creo = DateTime.Now.ToString("dd/MM/yy").ToUpper();
            String Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
            String Mi_SQL = "";
            //INSERTAR LA REQUISICION            
            Requisicion_Negocio.P_Requisicion_ID = "" + Obtener_Consecutivo(Ope_Com_Requisiciones.Campo_Requisicion_ID, Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones);
            String Requisicion_Num = Requisicion_Negocio.P_Requisicion_ID + "";
            Requisicion_Negocio.P_Folio = "RQ-" + Requisicion_Negocio.P_Requisicion_ID;
            //Requisicion_Negocio.P_Codigo_Programatico = "";
            Mi_SQL = "SELECT CLAVE FROM CAT_SAP_AREA_FUNCIONAL WHERE AREA_FUNCIONAL_ID = " +
                "(SELECT AREA_FUNCIONAL_ID FROM CAT_DEPENDENCIAS WHERE DEPENDENCIA_ID = '" + Requisicion_Negocio.P_Dependencia_ID + "')";
            DataSet Ds_Area_Funcional = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            if (Ds_Area_Funcional != null && Ds_Area_Funcional.Tables[0].Rows.Count > 0)
            {
                String Area = Ds_Area_Funcional.Tables[0].Rows[0]["CLAVE"].ToString();
                Requisicion_Negocio.P_Codigo_Programatico = Requisicion_Negocio.P_Codigo_Programatico.Replace("area",Area);
            }
            //insertar cuando la requisicion quedo en estatus de contruccion
                Mi_SQL = "INSERT INTO " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                " (" + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                ", " + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                ", " + Ope_Com_Requisiciones.Campo_Area_ID + 
                ", " + Ope_Com_Requisiciones.Campo_Folio + 
                ", " + Ope_Com_Requisiciones.Campo_Estatus +
                ", " + Ope_Com_Requisiciones.Campo_Codigo_Programatico +
                ", " + Ope_Com_Requisiciones.Campo_Elemento_PEP +
                ", " + Ope_Com_Requisiciones.Campo_Tipo + 
                ", " + Ope_Com_Requisiciones.Campo_Fase +
                ", " + Ope_Com_Requisiciones.Campo_Subtotal + 
                ", " + Ope_Com_Requisiciones.Campo_IVA + 
                ", " + Ope_Com_Requisiciones.Campo_IEPS +
                ", " + Ope_Com_Requisiciones.Campo_Total + 
                ", " + Ope_Com_Requisiciones.Campo_Usuario_Creo + 
                ", " + Ope_Com_Requisiciones.Campo_Fecha_Creo + 
                ", " + Ope_Com_Requisiciones.Campo_Justificacion_Compra +
                ", " + Ope_Com_Requisiciones.Campo_Especificacion_Prod_Serv + 
                ", " + Ope_Com_Requisiciones.Campo_Verificaion_Entrega +
                ", " + Ope_Com_Requisiciones.Campo_Consolidada +
                ", " + Ope_Com_Requisiciones.Campo_Tipo_Articulo +
                ", " + Ope_Com_Requisiciones.Campo_Partida_ID;
                if (Requisicion_Negocio.P_Estatus == "GENERADA")
                {
                    Mi_SQL = Mi_SQL + 
                             ", " + Ope_Com_Requisiciones.Campo_Empleado_Generacion_ID +
                             ", " + Ope_Com_Requisiciones.Campo_Fecha_Generacion;
                }
                else
                {
                    Mi_SQL = Mi_SQL + 
                             ", " + Ope_Com_Requisiciones.Campo_Empleado_Construccion_ID +
                             ", " + Ope_Com_Requisiciones.Campo_Fecha_Construccion;
                }
                Mi_SQL = Mi_SQL +
                ") VALUES (" +
                Requisicion_Negocio.P_Requisicion_ID + ",'" +
                Requisicion_Negocio.P_Dependencia_ID + "','" +
                Cls_Sessiones.Area_ID_Empleado.Trim() + "','" +
                Requisicion_Negocio.P_Folio + "','" + 
                Requisicion_Negocio.P_Estatus + "','" +
                Requisicion_Negocio.P_Codigo_Programatico + "','" +
                Requisicion_Negocio.P_Elemento_PEP + "','" +
                Requisicion_Negocio.P_Tipo + "','" + 
                Requisicion_Negocio.P_Fase + "'," +
                Requisicion_Negocio.P_Subtotal + ", " + 
                Requisicion_Negocio.P_IVA + ", " +
                Requisicion_Negocio.P_IEPS + ", " + 
                Requisicion_Negocio.P_Total + ",'" + 
                Usuario_Creo + "','" + 
                Fecha_Creo + "','" + 
                Requisicion_Negocio.P_Justificacion_Compra + "','" + 
                Requisicion_Negocio.P_Especificacion_Productos + "','" + 
                Requisicion_Negocio.P_Verificacion_Entrega + "','NO','" + 
                Requisicion_Negocio.P_Tipo_Articulo + "','" +
                Requisicion_Negocio.P_Partida_ID + "','" +
                Cls_Sessiones.Empleado_ID + "','" + 
                Fecha_Creo + "')";
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            return Requisicion_Num;
        }

        private static void Guarda_Productos_O_Servicios_Requisicion(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            //insertar los productos que se hayn seleccionado 
            //para la requisa validando q si hay productos agregados
            String Mi_SQL = "";
            foreach (DataRow Renglon in Requisicion_Negocio.P_Dt_Productos_Servicios.Rows)
            {
                Mi_SQL = "INSERT INTO " +
                    Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto +
                    " (" + Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID +
                    ", " + Ope_Com_Req_Producto.Campo_Requisicion_ID +
                    ", " + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID +
                    ", " + Ope_Com_Req_Producto.Campo_Prod_Serv_ID +
                    ", " + Ope_Com_Req_Producto.Campo_Clave +
                    ", " + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio +
                    ", " + Ope_Com_Req_Producto.Campo_Partida_ID +
                    ", " + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID +
                    ", " + Ope_Com_Req_Producto.Campo_Cantidad +
                    ", " + Ope_Com_Req_Producto.Campo_Tipo +
                    ", " + Ope_Com_Req_Producto.Campo_Giro_ID +
                    ", " + Ope_Com_Req_Producto.Campo_Nombre_Giro +
                    ", " + Ope_Com_Req_Producto.Campo_Usuario_Creo +
                    ", " + Ope_Com_Req_Producto.Campo_Fecha_Creo +
                    ", " + Ope_Com_Req_Producto.Campo_Precio_Unitario +
                    ", " + Ope_Com_Req_Producto.Campo_Importe +
                    ", " + Ope_Com_Req_Producto.Campo_Monto_IVA +
                    ", " + Ope_Com_Req_Producto.Campo_Monto_IEPS +
                    ", " + Ope_Com_Req_Producto.Campo_Porcentaje_IVA +
                    ", " + Ope_Com_Req_Producto.Campo_Porcentaje_IEPS +
                    ", " + Ope_Com_Req_Producto.Campo_Monto_Total + " ) VALUES " +
                    "(" + Obtener_Consecutivo(Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID, Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto) +
                    "," + Requisicion_Negocio.P_Requisicion_ID +
                    ",'" + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() +
                    "','" + Renglon["Prod_Serv_ID"].ToString().Trim() +
                    "','" + Renglon["Clave"].ToString().Trim() +
                    "','" + Renglon["Nombre_Producto_Servicio"].ToString().Trim() +
                    "','" + Renglon["Partida_ID"].ToString().Trim() +
                    "','" + Renglon["Proyecto_Programa_ID"].ToString().Trim() +
                    "', " + Renglon["Cantidad"].ToString().Trim() +
                    //", '" + Requisicion_Negocio.P_Tipo_Articulo +
                    ", '" + Renglon["Tipo"].ToString().Trim() +
                    "','" + Renglon["Concepto_ID"].ToString().Trim() +
                    "','" + Renglon["Nombre_Concepto"].ToString().Trim() +
                    "','" + Cls_Sessiones.Nombre_Empleado +
                    "', " + "GETDATE()" +
                    ", " + Renglon["Precio_Unitario"].ToString().Trim() +
                    ", " + Renglon["Monto"].ToString().Trim() +
                    ", " + Renglon["Monto_IVA"].ToString().Trim() +
                    ", " + Renglon["Monto_IEPS"].ToString().Trim() +
                    ", " + Renglon["Porcentaje_IVA"].ToString().Trim() +
                    ", " + Renglon["Porcentaje_IEPS"].ToString().Trim() +
                    ", " + Renglon["Monto_Total"].ToString().Trim() + ")";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            //Guardar Comentarios
            if (Requisicion_Negocio.P_Comentarios.Length > 0)
            {
                Cls_Ope_Com_Administrar_Requisiciones_Negocio Administrar_Requisicion = 
                    new Cls_Ope_Com_Administrar_Requisiciones_Negocio();
                Administrar_Requisicion.P_Requisicion_ID = Requisicion_Negocio.P_Requisicion_ID;
                Administrar_Requisicion.P_Comentario = Requisicion_Negocio.P_Comentarios;
                Administrar_Requisicion.P_Estatus = Requisicion_Negocio.P_Estatus;
                Administrar_Requisicion.P_Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                Administrar_Requisicion.Alta_Observaciones();
            }
        }

        #endregion

        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************
        #region MODIFICAR REQUISICION
       
        public static String Proceso_Actualizar_Requisicion(
        Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mensaje = "";
            if (Requisicion_Negocio.P_Listado_Almacen != null &&
                Requisicion_Negocio.P_Listado_Almacen == "SI" &&
                Requisicion_Negocio.P_Estatus == "CANCELADA")
            {
                Mensaje = Cancelar_Requisición_De_Listado(Requisicion_Negocio.P_Requisicion_ID, Requisicion_Negocio.P_Comentarios);
            }
            else
            {
                Mensaje = Actualizar_Requisicion_De_Unidad_Responsable(Requisicion_Negocio);
            }
            return Mensaje;
        }
        private static String Cancelar_Requisición_De_Listado(String No_Requisicion, String Comentarios) 
        {
            String Mi_SQL = "";
            String Mensaje = "EXITO";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                " SET " + Ope_Com_Requisiciones.Campo_Estatus + " = 'CANCELADA' WHERE " +
                Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + No_Requisicion;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();                
                Mi_SQL = "UPDATE " + Ope_Com_Listado.Tabla_Ope_Com_Listado + " SET " + Ope_Com_Listado.Campo_Estatus + " = 'AUTORIZADA' WHERE " +
                    Ope_Com_Listado.Campo_Listado_ID + " = (SELECT DISTINCT(" + Ope_Com_Listado_Detalle.Campo_No_Listado_ID + ")" +
                    " FROM " + Ope_Com_Listado_Detalle.Tabla_Ope_Com_Listado_Detalle + " WHERE " + Ope_Com_Listado_Detalle.Campo_No_Requisicion +
                    " = " + No_Requisicion + ")";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Mi_SQL = "UPDATE " + Ope_Com_Listado_Detalle.Tabla_Ope_Com_Listado_Detalle + " SET " +
                    Ope_Com_Listado_Detalle.Campo_No_Requisicion + " = NULL WHERE " +
                    Ope_Com_Listado_Detalle.Campo_No_Requisicion + " = " + No_Requisicion;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                //Guardar Comentarios
                if (Comentarios.Length > 0)
                {
                    Cls_Ope_Com_Administrar_Requisiciones_Negocio Administrar_Requisicion =
                        new Cls_Ope_Com_Administrar_Requisiciones_Negocio();
                    Administrar_Requisicion.P_Requisicion_ID = No_Requisicion;
                    Administrar_Requisicion.P_Comentario = Comentarios;
                    Administrar_Requisicion.P_Estatus = "CANCELADA";
                    Administrar_Requisicion.P_Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                    Administrar_Requisicion.Alta_Observaciones();
                }
                Trans.Commit();
                Registrar_Historial("CANCELADA", No_Requisicion);
            }
            catch (Exception ex)
            {
                Trans.Rollback();
                Mensaje = ex.ToString();               
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje;
        }
        public static String Cancelar_Requisicion(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mensaje = "";
            String Fecha_Creo = DateTime.Now.ToString("dd/MM/yy").ToUpper();
            String Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
            if (Requisicion_Negocio.P_Tipo == "STOCK")
            { 

            }
            else if (Requisicion_Negocio.P_Tipo == "TRANSITORIA")
            {
 
            }
            return Mensaje;
        }
        public static String Actualizar_Requisicion_De_Unidad_Responsable(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mensaje = "";
            String Fecha_Creo = DateTime.Now.ToString("dd/MM/yy").ToUpper();
            String Usuario_Creo = Cls_Sessiones.Nombre_Empleado;           
            String Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
            String Mi_SQL = "";
            Double Diferencia = 0;
            Double Diferencia_Total = 0;

        
            //Obtenemos el total anterior de la requisicion 
            Mi_SQL = "SELECT " + Ope_Com_Requisiciones.Campo_Total;
            Mi_SQL += " FROM "  + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones ;
            Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = '" + Requisicion_Negocio.P_Requisicion_ID + "'";
            DataTable Dt_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            Mi_SQL = "SELECT * ";
            Mi_SQL += " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
            Mi_SQL += " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = '" + Requisicion_Negocio.P_Requisicion_ID + "'";
            DataTable Dt_Req_Productos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            //Monto de la requisicion a guardar
            Double Monto_Actual = double.Parse(Requisicion_Negocio.P_Total);
            // MOnto totla de la requisicion antes de actualizar
            Double Monto_Anterior = double.Parse(Dt_Requisicion.Rows[0]["TOTAL"].ToString());
            String Cargo = "";
            String Abono = "";
            if (Monto_Actual > Monto_Anterior)
            {
                Diferencia = Monto_Actual - Monto_Anterior;
                Diferencia_Total = Diferencia;
                Cargo = Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO;
                Abono = Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE;
            }
            else 
            {
                Diferencia = Monto_Anterior - Monto_Actual;
                Diferencia_Total = Diferencia;
                Cargo = Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE;
                Abono = Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO;
            }

            //ACTUALIZAR LA REQUISICION
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            SqlDataAdapter adapter = new SqlDataAdapter();

            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                " SET " +
                Ope_Com_Requisiciones.Campo_Estatus + " = '" + Requisicion_Negocio.P_Estatus + "', " +
                Ope_Com_Requisiciones.Campo_Subtotal + " = " + Requisicion_Negocio.P_Subtotal + ", " +
                Ope_Com_Requisiciones.Campo_IVA + " = " + Requisicion_Negocio.P_IVA + ", " +
                Ope_Com_Requisiciones.Campo_IEPS + " = " + Requisicion_Negocio.P_IEPS + ", " +
                Ope_Com_Requisiciones.Campo_Total + " = " + Requisicion_Negocio.P_Total + ", " +
                Ope_Com_Requisiciones.Campo_Usuario_Modifico + " ='" + Usuario + "', " +
                Ope_Com_Requisiciones.Campo_Fecha_Modifico + " = GETDATE(), " +
                Ope_Com_Requisiciones.Campo_Justificacion_Compra + " ='" + Requisicion_Negocio.P_Justificacion_Compra + "'," +
                Ope_Com_Requisiciones.Campo_Especificacion_Prod_Serv + " ='" + Requisicion_Negocio.P_Especificacion_Productos + "'," +
                Ope_Com_Requisiciones.Campo_Verificaion_Entrega + "='" + Requisicion_Negocio.P_Verificacion_Entrega + "'";
                if (Requisicion_Negocio.P_Estatus == "GENERADA")
                {
                    Mi_SQL = Mi_SQL + "," +
                    Ope_Com_Requisiciones.Campo_Empleado_Generacion_ID + "='" + Cls_Sessiones.Empleado_ID.ToString() + "'," +
                    Ope_Com_Requisiciones.Campo_Fecha_Generacion + "=GETDATE()";
                }
                else if (Requisicion_Negocio.P_Estatus == "CANCELADA")
                {
                    Mi_SQL = Mi_SQL + "," +
                    Ope_Com_Requisiciones.Campo_Empleado_Cancelada_ID + "='" + Cls_Sessiones.Empleado_ID.ToString() + "'," +
                    Ope_Com_Requisiciones.Campo_Fecha_Cancelada + "=GETDATE()";
                    Mensaje = "EXITO";

                }

                Mi_SQL = Mi_SQL +
                    " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = '" + Requisicion_Negocio.P_Requisicion_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                if (Requisicion_Negocio.P_Estatus != "CANCELADA")
                {
                    Mi_SQL = "DELETE " + Ope_Com_Propuesta_Cotizacion.Tabla_Ope_Com_Propuesta_Cotizacion +
                        " WHERE " + Ope_Com_Propuesta_Cotizacion.Campo_No_Requisicion +
                        " = " + Requisicion_Negocio.P_Requisicion_ID;
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    //Se borran los detalles, productos o servicios
                    Mi_SQL = "DELETE " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto +
                        " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID +
                        " = " + Requisicion_Negocio.P_Requisicion_ID;
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Se modifica la reserva
                    //actualizamos el monto de la reserva
                    Mi_SQL = "UPDATE " +Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "  SET "+ Ope_Psp_Reservas.Campo_Importe_Inicial + " = " + Monto_Actual;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Saldo + " = " + Monto_Actual;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "= (SELECT " + Ope_Com_Requisiciones.Campo_Num_Reserva + " FROM ";
                    Mi_SQL = Mi_SQL + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + "=" + Requisicion_Negocio.P_Requisicion_ID.Trim() + ")";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    //actualizamos el monto de la reserva detalles
                    Mi_SQL = "UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "  SET " + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " = " + Monto_Actual;
                    Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Monto_Actual;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + "= (SELECT " + Ope_Com_Requisiciones.Campo_Num_Reserva + " FROM ";
                    Mi_SQL = Mi_SQL + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + "=" + Requisicion_Negocio.P_Requisicion_ID.Trim() + ")";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                }
                //Ejecutar transaccion 
                //Trans.Commit();
                //if (Cn == null)
                //{
                //    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                //    Cn.Open();
                //}
                //Trans = Cn.BeginTransaction();
                //Cmd.Connection = Cn;
                //Cmd.Transaction = Trans;

                //Se guardan los detalles, productps o sevicios
                if (Requisicion_Negocio.P_Estatus != "CANCELADA")
                {
                    //Obttenemos el consecutivo del producto
                    Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID + "),0) FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                    Cmd.CommandText = Mi_SQL;
                    object Consecutivo_Productos_Requisa = Cmd.ExecuteScalar();
                    Int64 Cont_Consecutivo = 1;
                    if (Consecutivo_Productos_Requisa != null)
                    {
                        Cont_Consecutivo = Int64.Parse(Consecutivo_Productos_Requisa.ToString()) + 1;
                    }

                    //Obtener_Consecutivo(Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID, Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto);
                    foreach (DataRow Renglon in Requisicion_Negocio.P_Dt_Productos_Servicios.Rows)
                    {
                        Mi_SQL = "INSERT INTO " +
                            Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto +
                            " (" + Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID +
                            ", " + Ope_Com_Req_Producto.Campo_Requisicion_ID +
                            ", " + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID +
                            ", " + Ope_Com_Req_Producto.Campo_Prod_Serv_ID +
                            ", " + Ope_Com_Req_Producto.Campo_Clave +
                            ", " + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio +
                            ", " + Ope_Com_Req_Producto.Campo_Partida_ID +
                            ", " + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID +
                            ", " + Ope_Com_Req_Producto.Campo_Cantidad +
                            ", " + Ope_Com_Req_Producto.Campo_Tipo +
                            ", " + Ope_Com_Req_Producto.Campo_Giro_ID +
                            ", " + Ope_Com_Req_Producto.Campo_Nombre_Giro +
                            ", " + Ope_Com_Req_Producto.Campo_Usuario_Creo +
                            ", " + Ope_Com_Req_Producto.Campo_Fecha_Creo +
                            ", " + Ope_Com_Req_Producto.Campo_Precio_Unitario +
                            ", " + Ope_Com_Req_Producto.Campo_Importe +
                            ", " + Ope_Com_Req_Producto.Campo_Monto_IVA +
                            ", " + Ope_Com_Req_Producto.Campo_Monto_IEPS +
                            ", " + Ope_Com_Req_Producto.Campo_Porcentaje_IVA +
                            ", " + Ope_Com_Req_Producto.Campo_Porcentaje_IEPS +
                            ", " + Ope_Com_Req_Producto.Campo_Monto_Total;
                        if (Renglon["Especificacion_Producto"].ToString().Trim() != String.Empty)
                        {
                            Mi_SQL = Mi_SQL + ", " + Ope_Com_Req_Producto.Campo_Especificacion_Producto;
                        }
                        Mi_SQL = Mi_SQL + " ) VALUES " +
                        "(" + Cont_Consecutivo +
                        "," + Requisicion_Negocio.P_Requisicion_ID +
                        ",'" + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() +
                        "','" + Renglon["Prod_Serv_ID"].ToString().Trim() +
                        "','" + Renglon["Clave"].ToString().Trim() +
                        "','" + Renglon["Nombre_Producto_Servicio"].ToString().Trim() +
                        "','" + Renglon["Partida_ID"].ToString().Trim() +
                        "','" + Renglon["Proyecto_Programa_ID"].ToString().Trim() +
                        "', " + Renglon["Cantidad"].ToString().Trim() +
                            //", '" + Requisicion_Negocio.P_Tipo_Articulo +
                        ", '" + Renglon["Tipo"].ToString().Trim() +
                        "','" + Renglon["Concepto_ID"].ToString().Trim() +
                        "','" + Renglon["Nombre_Concepto"].ToString().Trim() +
                        "','" + Cls_Sessiones.Nombre_Empleado +
                        "', " + "GETDATE()" +
                        ", " + Renglon["Precio_Unitario"].ToString().Trim() +
                        ", " + Renglon["Monto"].ToString().Trim() +
                        ", " + Renglon["Monto_IVA"].ToString().Trim() +
                        ", " + Renglon["Monto_IEPS"].ToString().Trim() +
                        ", " + Renglon["Porcentaje_IVA"].ToString().Trim() +
                        ", " + Renglon["Porcentaje_IEPS"].ToString().Trim() +
                        ", " + Renglon["Monto_Total"].ToString().Trim();
                        if (Renglon["Especificacion_Producto"].ToString().Trim() != String.Empty)
                        {
                            Mi_SQL = Mi_SQL + ",'" + Renglon["Especificacion_Producto"].ToString().Trim() + "'";
                        }
                        Mi_SQL = Mi_SQL + ")";

                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        Cont_Consecutivo++;
                    }


                    Mi_SQL = "DELETE FROM " + Ope_Com_Req_Bienes.Tabla_Ope_Com_Req_Bienes + " WHERE " + Ope_Com_Req_Bienes.Campo_Requisicion_ID + " = '" + Requisicion_Negocio.P_Requisicion_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    foreach (DataRow Renglon in Requisicion_Negocio.P_Dt_Bienes.Rows)
                    {
                        Mi_SQL = "INSERT INTO " + Ope_Com_Req_Bienes.Tabla_Ope_Com_Req_Bienes +
                            " (" + Ope_Com_Req_Bienes.Campo_Requisicion_ID +
                            ", " + Ope_Com_Req_Bienes.Campo_Bien_Mueble_ID +
                            ", " + Ope_Com_Req_Bienes.Campo_Usuario_Creo +
                            ", " + Ope_Com_Req_Bienes.Campo_Fecha_Creo + " ) VALUES " +
                            "(" + Requisicion_Negocio.P_Requisicion_ID +
                            ",'" + Renglon["BIEN_MUEBLE_ID"].ToString().Trim() +
                            "','" + Cls_Sessiones.Nombre_Empleado +
                            "', " + "GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    //Si la requisición es de STOCK se comprometen los productos
                    if (Requisicion_Negocio.P_Tipo == "STOCK")
                    {
                        DataTable Dt_Productos_Almacen = null;
                        // String Mi_SQL = "";
                        //descomprometer                                         
                        Double Cantidad = 0;
                        String Producto_ID = "";
                        int resultado = 0;
                        //foreach (DataRow Producto_Requisicion in Requisicion_Negocio.P_Dt_Productos_Servicios_Aux.Rows)
                        foreach (DataRow Producto_Requisicion in Dt_Req_Productos.Rows)
                        {
                            Cantidad = Double.Parse(Producto_Requisicion["CANTIDAD"].ToString().Trim());
                            Producto_ID = Producto_Requisicion["PROD_SERV_ID"].ToString().Trim();
                            //desCompromete                            
                            Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                                " SET " + Cat_Com_Productos.Campo_Comprometido +
                                " = " + Cat_Com_Productos.Campo_Comprometido + " - " + Cantidad + ", " +
                                Cat_Com_Productos.Campo_Disponible +
                                " = " + Cat_Com_Productos.Campo_Disponible + " + " +
                                Cantidad + " WHERE " +
                                Cat_Com_Productos.Campo_Producto_ID + " = '" +
                                Producto_ID + "'";
                            Cmd.CommandText = Mi_SQL;
                            resultado = Cmd.ExecuteNonQuery();
                            //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        }

                        Mensaje = "EXITO";
                        if (Requisicion_Negocio.P_Estatus != "CANCELADA")
                        {
                            //comprometer           
                            Cantidad = 0;
                            Producto_ID = "";
                            foreach (DataRow Producto_Requisicion in Requisicion_Negocio.P_Dt_Productos_Servicios.Rows)
                            {
                                Cantidad = Double.Parse(Producto_Requisicion["CANTIDAD"].ToString().Trim());
                                Producto_ID = Producto_Requisicion["PROD_SERV_ID"].ToString().Trim();
                                //Compromete
                                Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                                " SET " + Cat_Com_Productos.Campo_Comprometido +
                                " = " + Cat_Com_Productos.Campo_Comprometido + " + " + Cantidad + ", " +
                                Cat_Com_Productos.Campo_Disponible +
                                " = " + Cat_Com_Productos.Campo_Disponible + " - " +
                                Cantidad + "WHERE " +
                                Cat_Com_Productos.Campo_Producto_ID + " = '" +
                                Producto_ID + "'";
                                Cmd.CommandText = Mi_SQL;
                                resultado = Cmd.ExecuteNonQuery();

                                if (resultado > 0)
                                {
                                    Mensaje = "EXITO";
                                }
                                else
                                {
                                    //No compromete y manda mensaje
                                    Mensaje = "CANTIDAD INSUFICIENTE DEL PRODUCTO ";// + Dr_Productos_Almacen[0]["NOMBRE"].ToString().Trim() + ", " +
                                    //Dr_Productos_Almacen[0]["DESCRIPCION"].ToString().Trim() + ", DEBIDO A QUE SE COMPROMETIO EN UNA REQUISICION" +
                                    //" ANTERIOR A LA DE USTED.";
                                }
                            }

                        }//fin de if cancelar stock
                    }
                    else
                    {
                        Mensaje = "EXITO";
                    }
                }
                if (Mensaje == "EXITO")
                {
                    
                    //Mensaje += //"-" + Requisicion_Negocio.P_Requisicion_ID;
                    Registrar_Historial(Requisicion_Negocio.P_Estatus, Requisicion_Negocio.P_Requisicion_ID);
                    //Guardar Comentarios
                    if (Requisicion_Negocio.P_Comentarios.Length > 0)
                    {
                        Cls_Ope_Com_Administrar_Requisiciones_Negocio Administrar_Requisicion =
                            new Cls_Ope_Com_Administrar_Requisiciones_Negocio();
                        Administrar_Requisicion.P_Requisicion_ID = Requisicion_Negocio.P_Requisicion_ID;
                        Administrar_Requisicion.P_Comentario = Requisicion_Negocio.P_Comentarios;
                        Administrar_Requisicion.P_Estatus = Requisicion_Negocio.P_Estatus;
                        Administrar_Requisicion.P_Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                        Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Com_Req_Observaciones.Campo_Observacion_ID + "),'0') ";
                        Mi_SQL = Mi_SQL + "FROM " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones;
                        Cmd.CommandText = Mi_SQL;
                        object observaciones = Cmd.ExecuteScalar();
                        String Consecutivo = observaciones.ToString();
                        Mi_SQL = "INSERT INTO " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones +
                        " (" + Ope_Com_Req_Observaciones.Campo_Observacion_ID +
                        ", " + Ope_Com_Req_Observaciones.Campo_Requisicion_ID +
                        ", " + Ope_Com_Req_Observaciones.Campo_Comentario +
                        ", " + Ope_Com_Req_Observaciones.Campo_Estatus +
                        ", " + Ope_Com_Req_Observaciones.Campo_Usuario_Creo +
                        ", " + Ope_Com_Req_Observaciones.Campo_Fecha_Creo +
                        ") VALUES (" +
                                        //"SECUENCIA_OBSERVACION_REQ_ID.NEXTVAL,'" +
                            Consecutivo + ",'" +
                        Administrar_Requisicion.P_Requisicion_ID + "','" +
                        Administrar_Requisicion.P_Comentario + "','" +
                        Administrar_Requisicion.P_Estatus + "','" +
                        Cls_Sessiones.Nombre_Empleado + "',GETDATE())";
                       
                        Cmd.CommandText = Mi_SQL;
                        int result = Cmd.ExecuteNonQuery();
                    }
                    if (Requisicion_Negocio.P_Tipo == "STOCK" && Requisicion_Negocio.P_Estatus=="CANCELADA")
                    {
                        DataTable Dt_Productos_Almacen = null;
                        // String Mi_SQL = "";
                        //descomprometer  cuando es cancelada                                     
                        Double Cantidad = 0;
                        String Producto_ID = "";
                        int resultado = 0;
                        //foreach (DataRow Producto_Requisicion in Requisicion_Negocio.P_Dt_Productos_Servicios_Aux.Rows)
                        foreach (DataRow Producto_Requisicion in Dt_Req_Productos.Rows)
                        {
                            Cantidad = Double.Parse(Producto_Requisicion["CANTIDAD"].ToString().Trim());
                            Producto_ID = Producto_Requisicion["PROD_SERV_ID"].ToString().Trim();
                            //desCompromete                            
                            Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                                " SET " + Cat_Com_Productos.Campo_Comprometido +
                                " = " + Cat_Com_Productos.Campo_Comprometido + " - " + Cantidad + ", " +
                                Cat_Com_Productos.Campo_Disponible +
                                " = " + Cat_Com_Productos.Campo_Disponible + " + " +
                                Cantidad + " WHERE " +
                                Cat_Com_Productos.Campo_Producto_ID + " = '" +
                                Producto_ID + "'";
                            Cmd.CommandText = Mi_SQL;
                            resultado = Cmd.ExecuteNonQuery();
                            //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        }
                    }
                    //Comprometer presupuesto partida    
                    Mi_SQL = "SELECT NUM_RESERVA FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                    " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Requisicion_Negocio.P_Requisicion_ID.Trim();
                    Cmd.CommandText = Mi_SQL;
                    object reserva = Cmd.ExecuteScalar();
                    String No_Reserva = reserva.ToString();
                    //Construimos el datatable
                    DataTable Dt_Detalles = new DataTable();
                    Dt_Detalles.Columns.Add("FUENTE_FINANCIAMIENTO_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("PROGRAMA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("DEPENDENCIA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("PARTIDA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("ANIO", typeof(System.String));
                    Dt_Detalles.Columns.Add("IMPORTE", typeof(System.String));
                    DataRow Fila_Nueva = Dt_Detalles.NewRow();
                    Fila_Nueva["FUENTE_FINANCIAMIENTO_ID"] = Requisicion_Negocio.P_Fuente_Financiamiento;
                    Fila_Nueva["PROGRAMA_ID"] = Requisicion_Negocio.P_Proyecto_Programa_ID;
                    Fila_Nueva["DEPENDENCIA_ID"] = Requisicion_Negocio.P_Dependencia_ID;
                    Fila_Nueva["PARTIDA_ID"] = Requisicion_Negocio.P_Partida_ID;
                    Fila_Nueva["ANIO"] = DateTime.Now.Year;
                    Fila_Nueva["IMPORTE"] = Diferencia_Total;
                    Dt_Detalles.Rows.Add(Fila_Nueva);
                    Dt_Detalles.AcceptChanges();
                    int Registros = 0;
                    if (Requisicion_Negocio.P_Tipo == "TRANSITORIA")
                    {
                        if (Requisicion_Negocio.P_Estatus == "CANCELADA")
                        {
                            Registros = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(
                                Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE,
                                Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, 
                                Dt_Detalles, Cmd);

                      
                            //Registrar movimiento presupuestal
                            Cls_Ope_Psp_Manejo_Presupuesto.
                                Registro_Movimiento_Presupuestal(
                                    No_Reserva,
                                    Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE,
                                    Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO,
                                    Convert.ToDouble(Monto_Actual),
                                    "",
                                    "",
                                    "",
                                    "",Cmd);
                            //Actualizamos estatus de la reserva
                            Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas +
                                " SET " + Ope_Psp_Reservas.Campo_Estatus +
                                " = 'CANCELADA'" +
                                " WHERE " +
                                Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva;
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();

                        }
                        else
                        {
                            //Verificamos si el monto ha cambiado o no 
                            if (Diferencia_Total > 0) 
                            {
                                //Si el total cambio entonces regresamos el total anterior de la requisiscion 
                                //al presupuesto y  tomamos el nuevo total del presupuesto modificado
                                Fila_Nueva["IMPORTE"] = Diferencia_Total.ToString().Trim();
                                Registros = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(
                                Cargo,
                                Abono,
                                Dt_Detalles, Cmd);
                              
                               //Registrar movimiento presupuestal
                               Cls_Ope_Psp_Manejo_Presupuesto.
                                    Registro_Movimiento_Presupuestal(
                                        No_Reserva,
                                        Cargo,
                                        Abono,
                                        Convert.ToDouble(Diferencia_Total),
                                        "",
                                        "",
                                        "",
                                        "",Cmd);                             
                            }
                            if (Diferencia_Total == 0)
                            {
                                Registros = 1;
                            }
                        }
                    }
                    else
                    {
                        if (Requisicion_Negocio.P_Estatus == "CANCELADA")
                        {

                            Fila_Nueva["IMPORTE"] = Requisicion_Negocio.P_Total;
                            Registros = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(
                            Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE,
                            Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO,
                            Dt_Detalles, Cmd);   

                         
                            //Registrar movimiento presupuestal
                            Cls_Ope_Psp_Manejo_Presupuesto.
                                Registro_Movimiento_Presupuestal(
                                    No_Reserva,
                                    Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE,
                                    Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO,
                                    Convert.ToDouble(Requisicion_Negocio.P_Total),
                                    "",
                                    "",
                                    "",
                                    "",Cmd);
                            //Actualizamos estatus de la reserva
                            Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas +
                                " SET " + Ope_Psp_Reservas.Campo_Estatus +
                                " = 'CANCELADA'" +
                                " WHERE " +
                                Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva;
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            //Si el total anterior es diferente al total actual
                            if (Diferencia_Total > 0)
                            {
                                //Se craga al dismponible el total anterior
                                Fila_Nueva["IMPORTE"] = Diferencia_Total.ToString().Trim();
                                Registros = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(
                                Cargo,
                                Abono,
                                Dt_Detalles, Cmd);   

                                //Registrar movimiento presupuestal
                                Cls_Ope_Psp_Manejo_Presupuesto.
                                    Registro_Movimiento_Presupuestal(
                                        No_Reserva,
                                        Cargo,
                                        Abono,
                                        Convert.ToDouble(Diferencia_Total),
                                        "",
                                        "",
                                        "",
                                        "",Cmd);
                            }
                            if (Diferencia_Total == 0)
                            {
                                Registros = 1;
                            }
                        }
                    }
                    if (Registros > 0)
                    {
                        Trans.Commit();
                    }
                    else
                    {
                        new Exception();
                        Mensaje = "SIN ACTUALIZACION";
                    }
                }
                else
                {
                    Mensaje = "SIN ACTUALIZACION";
                }
            }
            catch (Exception ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                Mensaje = ex.ToString();
                throw new Exception(ex.ToString());
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje;
        }

        private static void Actualizar_Requisicion(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
            String Mi_SQL = "";
            //ACTUALIZAR LA REQUISICION
            Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
            " SET " + 
            Ope_Com_Requisiciones.Campo_Estatus + " = '" + Requisicion_Negocio.P_Estatus + "', " +
            Ope_Com_Requisiciones.Campo_Subtotal + " = " + Requisicion_Negocio.P_Subtotal + ", " +
            Ope_Com_Requisiciones.Campo_IVA + " = " + Requisicion_Negocio.P_IVA + ", " +
            Ope_Com_Requisiciones.Campo_IEPS + " = " + Requisicion_Negocio.P_IEPS + ", " +
            Ope_Com_Requisiciones.Campo_Total + " = " + Requisicion_Negocio.P_Total + ", " +
            Ope_Com_Requisiciones.Campo_Usuario_Modifico + " ='" + Usuario + "', " +
            Ope_Com_Requisiciones.Campo_Fecha_Modifico + " = GETDATE(), " +
            Ope_Com_Requisiciones.Campo_Justificacion_Compra + " ='" + Requisicion_Negocio.P_Justificacion_Compra + "'," +
            Ope_Com_Requisiciones.Campo_Especificacion_Prod_Serv + " ='" + Requisicion_Negocio.P_Especificacion_Productos + "'," +
            Ope_Com_Requisiciones.Campo_Verificaion_Entrega + "='" + Requisicion_Negocio.P_Verificacion_Entrega + "'";
            if (Requisicion_Negocio.P_Estatus == "GENERADA")
            {
                Mi_SQL = Mi_SQL + "," +
                Ope_Com_Requisiciones.Campo_Empleado_Generacion_ID + "='" + Cls_Sessiones.Empleado_ID.ToString() + "'," +
                Ope_Com_Requisiciones.Campo_Fecha_Generacion + "=GETDATE()";
            }
            else if (Requisicion_Negocio.P_Estatus == "CANCELADA")
            {
                Mi_SQL = Mi_SQL + "," +
                Ope_Com_Requisiciones.Campo_Empleado_Cancelada_ID + "='" + Cls_Sessiones.Empleado_ID.ToString() + "'," +
                Ope_Com_Requisiciones.Campo_Fecha_Cancelada + "=GETDATE()";
            }

            Mi_SQL = Mi_SQL +
                " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = '" + Requisicion_Negocio.P_Requisicion_ID + "'";
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        }

        public static bool Registrar_Historial(String Estatus, String No_Requisicion) 
        {
            No_Requisicion = No_Requisicion.Replace("RQ-","");
            bool Resultado = false;
            try
            {
                int Consecutivo =
                    Obtener_Consecutivo(Ope_Com_Historial_Req.Campo_No_Historial, Ope_Com_Historial_Req.Tabla_Ope_Com_Historial_Req);
                String Mi_SQL = "";
                Mi_SQL = "INSERT INTO " + Ope_Com_Historial_Req.Tabla_Ope_Com_Historial_Req + " (" +
                    Ope_Com_Historial_Req.Campo_No_Historial + "," +
                    Ope_Com_Historial_Req.Campo_No_Requisicion + "," +
                    Ope_Com_Historial_Req.Campo_Estatus + "," +
                    Ope_Com_Historial_Req.Campo_Fecha + "," +
                    Ope_Com_Historial_Req.Campo_Empleado + "," +
                    Ope_Com_Historial_Req.Campo_Usuario_Creo + "," +
                    Ope_Com_Historial_Req.Campo_Fecha_Creo + ") VALUES (" +
                    Consecutivo + ", " +
                    No_Requisicion + ", '" +
                    Estatus + "', " +
                    "GETDATE(), '" +
                    Cls_Sessiones.Nombre_Empleado + "', '" +
                    Cls_Sessiones.Nombre_Empleado + "', " +
                    "GETDATE())";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                Resultado = true;
            }
            catch (Exception Ex) 
            {
                String Str_Ex = Ex.ToString();
                Resultado = false;
            }
            return Resultado;
        }

        public static bool Registrar_Historial(String Estatus, String No_Requisicion, SqlCommand Cmd)
        {
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion = new SqlConnection();//Variable para la conexión para la base de datos   
            SqlCommand Comando = new SqlCommand();//Sirve para la ejecución de las operaciones a la base de datos
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos

            if (Cmd != null)
            {
                Comando = Cmd;
            }
            else
            {
                Conexion.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Conexion.Open();
                Transaccion = Conexion.BeginTransaction();
                Comando.Transaction = Transaccion;
                Comando.Connection = Conexion;
            }

            No_Requisicion = No_Requisicion.Replace("RQ-", "");
            bool Resultado = false;
            try
            {
                Double Consecutivo = new Double();

                String Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Com_Historial_Req.Campo_No_Historial + "),0) FROM " + Ope_Com_Historial_Req.Tabla_Ope_Com_Historial_Req;
                Comando.CommandText = Mi_SQL;
                Obj = Comando.ExecuteScalar();
                Double.TryParse(Obj.ToString(), out Consecutivo);
                Consecutivo++;

                Mi_SQL = "INSERT INTO " + Ope_Com_Historial_Req.Tabla_Ope_Com_Historial_Req + " (" +
                    Ope_Com_Historial_Req.Campo_No_Historial + "," +
                    Ope_Com_Historial_Req.Campo_No_Requisicion + "," +
                    Ope_Com_Historial_Req.Campo_Estatus + "," +
                    Ope_Com_Historial_Req.Campo_Fecha + "," +
                    Ope_Com_Historial_Req.Campo_Empleado + "," +
                    Ope_Com_Historial_Req.Campo_Usuario_Creo + "," +
                    Ope_Com_Historial_Req.Campo_Fecha_Creo + ") VALUES (" +
                    Consecutivo + ", " +
                    No_Requisicion + ", '" +
                    Estatus + "', " +
                    "GETDATE(), '" +
                    Cls_Sessiones.Nombre_Empleado + "', '" +
                    Cls_Sessiones.Nombre_Empleado + "', " +
                    "GETDATE())";

                Comando.CommandText = Mi_SQL;
                Comando.ExecuteNonQuery();
                Resultado = true;

                if (Cmd == null)
                {
                    Transaccion.Commit();//aceptamos los cambios
                }
            }
            catch (Exception Ex)
            {
                String Str_Ex = Ex.ToString();
                Resultado = false;
                if (Cmd == null)
                {
                    Transaccion.Rollback();
                }
                throw new Exception(Ex.ToString());
            }
            finally
            {
                if (Cmd == null)
                {
                    Conexion.Close();
                    Comando = null;
                    Conexion = null;
                    Transaccion = null;
                }
            }
            return Resultado;
        }

        public static DataTable Consultar_Historial_Requisicion(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            try
            {
                String Mi_SQL = "";
                Mi_SQL =
                "SELECT * FROM " + Ope_Com_Historial_Req.Tabla_Ope_Com_Historial_Req +
                " WHERE " + Ope_Com_Historial_Req.Campo_No_Requisicion + " = " +
                Requisicion_Negocio.P_Requisicion_ID +
                " ORDER BY " + Ope_Com_Historial_Req.Campo_Fecha + " ASC";
                DataTable Data_Table =
                    SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Data_Table;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static bool Verificar_Rango_Caja_Chica(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            bool Respuesta = false;
            double Monto_Inicio = 0;
            double Monto_Fin = 0;
            String Mi_Sql = "";
            Mi_Sql = "SELECT " +
                Cat_Com_Monto_Proceso_Compra.Campo_Monto_Compra_Directa_Ini + ", " +
                Cat_Com_Monto_Proceso_Compra.Campo_Monto_Compra_Directa_Fin +
                " FROM " +
                Cat_Com_Monto_Proceso_Compra.Tabla_Cat_Com_Monto_Proceso_Compra +
                " WHERE " +
                Cat_Com_Monto_Proceso_Compra.Campo_Tipo + " = '" + Requisicion_Negocio.P_Tipo_Articulo + "'";
            DataSet _DSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            DataTable Dt_Tmp = null;
            if (_DSet != null && _DSet.Tables.Count > 0 && _DSet.Tables[0].Rows.Count > 0)
            {
                Dt_Tmp = _DSet.Tables[0];
                Monto_Inicio = Convert.ToDouble(Dt_Tmp.Rows[0]["MONTO_COMPRA_DIRECTA_INI"].ToString().Trim());
                Monto_Fin = Convert.ToDouble(Dt_Tmp.Rows[0]["MONTO_COMPRA_DIRECTA_FIN"].ToString().Trim());
                double Total = Convert.ToDouble(Requisicion_Negocio.P_Total);
                Respuesta = Total >= Monto_Inicio && Total <= Monto_Fin ? true : false;
            }
            return Respuesta;
        }

        public static int Asignar_Reserva(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio) 
        {
            int Registros = 0;
            try
            {
                String Mi_Sql = "";
                Mi_Sql = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                    " SET NO_RESERVA ='" + Requisicion_Negocio.P_No_Reserva + "'," +
                    Ope_Com_Requisiciones.Campo_Estatus + " = '" + Requisicion_Negocio.P_Estatus + "'" + 
                    " WHERE " +
                    Ope_Com_Requisiciones.Campo_Folio + " ='" + Requisicion_Negocio.P_Folio + "'";
                Registros = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);                                
            }
            catch(Exception Ex)
            {
                Ex.ToString();
                Registros = 0;
            }
            if (Registros > 0)
                Registrar_Historial("ALMACEN", Requisicion_Negocio.P_Folio);
            return Registros;
        }
        public static int Rechaza_Contabilidad(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            int Registros = 0;
            try
            {
                String Mi_Sql = "";
                Mi_Sql = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                    " SET " + Ope_Com_Requisiciones.Campo_Estatus + " = '" + Requisicion_Negocio.P_Estatus + "'," +
                    " ALERTA ='ROJA'" +
                    " WHERE " +
                    Ope_Com_Requisiciones.Campo_Folio + " ='" + Requisicion_Negocio.P_Folio + "'";
                Registros = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                if (Registros > 0)
                {
                    Cls_Ope_Com_Administrar_Requisiciones_Negocio Admin_Req = new Cls_Ope_Com_Administrar_Requisiciones_Negocio();
                    Admin_Req.P_Requisicion_ID = Requisicion_Negocio.P_Requisicion_ID;
                    Admin_Req.P_Comentario = Requisicion_Negocio.P_Comentarios;
                    Admin_Req.P_Estatus = Requisicion_Negocio.P_Estatus;
                    Admin_Req.Alta_Observaciones();
                }
            }
            catch (Exception Ex)
            {
                Ex.ToString();
                Registros = 0;
            }
            if (Registros > 0)
                Registrar_Historial(Requisicion_Negocio.P_Estatus, Requisicion_Negocio.P_Folio);
            return Registros;
        }

        public static String Modificar_Cant_Prod_Req(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;

            String Mi_SQL = "";
            String Mensaje = "";
            DataTable Dt_Aux = new DataTable();
            Double Monto_Actual = 0;
            Double Monto_Anterior = 0;
            Int64 Num_Reserva = 0;

            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                DataSet Ds_Requisicion = Consultar_Requisicion(Requisicion_Negocio);

                Double.TryParse(Requisicion_Negocio.P_Total, out Monto_Actual);
                Double.TryParse(Ds_Requisicion.Tables["Dt_Requisiciones"].Rows[0][Ope_Com_Requisiciones.Campo_Total].ToString(), out Monto_Anterior);
                Int64.TryParse(Ds_Requisicion.Tables["Dt_Requisiciones"].Rows[0][Ope_Com_Requisiciones.Campo_Num_Reserva].ToString(), out Num_Reserva);

                Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " SET ";
                Mi_SQL += Ope_Com_Requisiciones.Campo_Subtotal + " = " + Requisicion_Negocio.P_Subtotal + ", ";
                Mi_SQL += Ope_Com_Requisiciones.Campo_IVA + " = " + Requisicion_Negocio.P_IVA + ", ";
                Mi_SQL += Ope_Com_Requisiciones.Campo_IEPS + " = " + Requisicion_Negocio.P_IEPS + ", ";
                Mi_SQL += Ope_Com_Requisiciones.Campo_Total + " = " + Requisicion_Negocio.P_Total + ", ";
                Mi_SQL += Ope_Com_Requisiciones.Campo_Usuario_Modifico + " ='" + Cls_Sessiones.Nombre_Empleado + "', ";
                Mi_SQL += Ope_Com_Requisiciones.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Requisicion_Negocio.P_Requisicion_ID;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Registrar_Historial(Requisicion_Negocio.P_Estatus, Requisicion_Negocio.P_Requisicion_ID, Cmd);

                foreach (DataRow Renglon in Requisicion_Negocio.P_Dt_Productos_Servicios.Rows)
                {
                    Mi_SQL = "UPDATE " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " SET ";
                    Mi_SQL += Ope_Com_Req_Producto.Campo_Cantidad + " = " + Renglon["Cantidad"].ToString().Trim();
                    Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Importe + " = " + Renglon["Monto"].ToString().Trim();
                    Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Monto_IVA + " = " + Renglon["Monto_IVA"].ToString().Trim();
                    Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Monto_IEPS + " = " + Renglon["Monto_IEPS"].ToString().Trim();
                    Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Monto_Total + " = " + Renglon["Monto_Total"].ToString().Trim();
                    Mi_SQL += " WHERE " + Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID + " = '" + Renglon["Ope_Com_Req_Producto_ID"].ToString().Trim() + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                DataTable Dt_Detalles = new DataTable();
                Dt_Detalles.Columns.Add("FUENTE_FINANCIAMIENTO_ID", typeof(System.String));
                Dt_Detalles.Columns.Add("PROGRAMA_ID", typeof(System.String));
                Dt_Detalles.Columns.Add("DEPENDENCIA_ID", typeof(System.String));
                Dt_Detalles.Columns.Add("PARTIDA_ID", typeof(System.String));
                Dt_Detalles.Columns.Add("ANIO", typeof(System.String));
                Dt_Detalles.Columns.Add("IMPORTE", typeof(System.String));

                DataRow Fila_Nueva = Dt_Detalles.NewRow();
                Fila_Nueva["FUENTE_FINANCIAMIENTO_ID"] = Ds_Requisicion.Tables["Dt_Detalles"].Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                Fila_Nueva["PROGRAMA_ID"] = Ds_Requisicion.Tables["Dt_Detalles"].Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim();
                Fila_Nueva["DEPENDENCIA_ID"] = Ds_Requisicion.Tables["Dt_Detalles"].Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString().Trim();
                Fila_Nueva["PARTIDA_ID"] = Ds_Requisicion.Tables["Dt_Detalles"].Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim();
                Fila_Nueva["ANIO"] = DateTime.Now.Year;
                Fila_Nueva["IMPORTE"] = Monto_Anterior.ToString();
                Dt_Detalles.Rows.Add(Fila_Nueva);
                Dt_Detalles.AcceptChanges();

                Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Dt_Detalles, Cmd);
                Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(
                        Num_Reserva.ToString(),
                        Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE,
                        Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO,
                        Convert.ToDouble(Requisicion_Negocio.P_Total),
                        "",
                        "",
                        "",
                        "",
                        Cmd);

                Dt_Detalles.Rows[0]["IMPORTE"] = Monto_Actual.ToString();
                Dt_Detalles.AcceptChanges();

                Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Dt_Detalles, Cmd);
                Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(
                        Num_Reserva.ToString(),
                        Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO,
                        Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE,
                        Convert.ToDouble(Requisicion_Negocio.P_Total),
                        "",
                        "",
                        "",
                        "",
                        Cmd);

                Trans.Commit();
                Mensaje = "Actualización Exitosa";
            }
            catch (Exception ex)
            {
                Trans.Rollback();
                Mensaje = ex.ToString();
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje;
        }

        #endregion

        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************
        #region CONSULTAS / FTE FINANCIAMIENTO, PROYECTOS, PARTIDAS, PRESUPUESTOS
       
        public static DataTable Consultar_Fuentes_Financiamiento(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            try
            {
                String Mi_SQL = "";
                Mi_SQL =
                "SELECT FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + "," +
                " FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+" +
                " FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion +
                " FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FUENTE" +
                " JOIN " + Cat_SAP_Det_Fte_Dependencia.Tabla_Cat_SAP_Det_Fte_Financiamiento_Dependencia + " DETALLE" +
                " ON FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = " +
                " DETALLE." + Cat_SAP_Det_Fte_Dependencia.Campo_Fuente_Financiamiento_ID +
                " WHERE DETALLE." + Cat_SAP_Det_Fte_Dependencia.Campo_Dependencia_ID + " = " +
                "'" + Requisicion_Negocio.P_Dependencia_ID + "'" +
                " AND FUENTE." + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID + " IN " +
                 "(SELECT " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID +
                " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado +
                " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = " +
                "'" + Requisicion_Negocio.P_Dependencia_ID + "'" +
                " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio +
                " =YEAR(GETDATE()))" +
                " ORDER BY " + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " ASC";
                DataTable Data_Table =
                    SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Data_Table;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static DataTable Consultar_Proyectos_Programas(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "";
            Mi_SQL =
            "SELECT PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + "," +
            " PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Clave + " +' '+" +
            " PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Nombre +
            " FROM " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + " PROGRAMA" +
            " JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " DETALLE" +
            " ON PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + " = " +
            " DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID +
            " WHERE DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = " +
            "'" + Requisicion_Negocio.P_Dependencia_ID + "'" +
            " AND DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio +"="+ DateTime.Now.Year + 
            " GROUP BY PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID +
            ", PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Clave +
            ", PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Nombre +
            " ORDER BY PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Nombre + " ASC ";            

            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        //CONSULTAR PARTIDAS DE UN PROGRAMA
        public static DataTable Consultar_Partidas_De_Un_Programa(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio) 
        {
            String Mi_SQL = "SELECT PARTIDA." + Cat_Com_Partidas.Campo_Partida_ID + ", " +
            " PARTIDA." + Cat_Com_Partidas.Campo_Clave + " +' '+" +
            " PARTIDA." + Cat_Com_Partidas.Campo_Nombre +
            " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas + " PARTIDA" +
            " JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " PARTIDA_GENERICA" +
            " ON PARTIDA_GENERICA." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + "=" +
            " PARTIDA." + Cat_Com_Partidas.Campo_Partida_Generica_ID +
            " JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " CONCEPTO" +
            " ON CONCEPTO." + Cat_Sap_Concepto.Campo_Concepto_ID + "= PARTIDA_GENERICA." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID +
            " JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " CAPITULO" +
            " ON CAPITULO." + Cat_SAP_Capitulos.Campo_Capitulo_ID + " =CONCEPTO." + Cat_Sap_Concepto.Campo_Capitulo_ID +
            " JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " DETALLE" +
            " ON PARTIDA." + Cat_Com_Partidas.Campo_Partida_ID + " = " +
            " DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID +
            " WHERE DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = " +
            "'" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
            " AND DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " =" +
            "'" + Requisicion_Negocio.P_Dependencia_ID + "'" +
            " AND DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " =" +
            " '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +
            " AND DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio +
            "=" + DateTime.Today.Year +
            " AND CAPITULO." + Cat_SAP_Capitulos.Campo_Capitulo_ID + "!='00001'"; 

            if (Requisicion_Negocio.P_Tipo == "STOCK")
            {
                Mi_SQL = Mi_SQL + " AND " + " PARTIDA." + Cat_Com_Partidas.Campo_Partida_ID + " IN " +
                    "(SELECT DISTINCT(" + Cat_Com_Productos.Campo_Partida_ID + ") FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                    " WHERE " + Cat_Com_Productos.Campo_Stock + " = 'SI')";
            }
            Mi_SQL = Mi_SQL + " GROUP BY PARTIDA." + Cat_Com_Partidas.Campo_Partida_ID + ",  PARTIDA." + Cat_Com_Partidas.Campo_Clave +
                ", PARTIDA." + Cat_Com_Partidas.Campo_Nombre + ",DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID +
                ", DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID +
                ", DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio;

            Mi_SQL = Mi_SQL + " ORDER BY PARTIDA." + Cat_Com_Partidas.Campo_Clave + " ASC";
            
            DataTable Data_Table = 
                SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }


        #endregion



        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************
        #region MANEJO DE PRODUCTOS STOCK / COMPROMETER, DESCOMPROMETER
       
        public static int Comprometer_Productos_Stock(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            int Registros_Afectados = 0;
            try
            {
                String Mensaje = "";
                DataTable Dt_Productos_Almacen = null;
                String Mi_SQL = "";
                Mi_SQL =
                "SELECT " + Cat_Com_Productos.Campo_Producto_ID + "," + Cat_Com_Productos.Campo_Existencia + "," +
                Cat_Com_Productos.Campo_Disponible + "," + Cat_Com_Productos.Campo_Comprometido +
                " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                " WHERE " + Cat_Com_Productos.Campo_Stock + " = 'SI'";
                DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0)
                {
                    Dt_Productos_Almacen = Data_Set.Tables[0];
                }
                if (Dt_Productos_Almacen != null)
                {
                    int Disponible_Stock = 0;
                    int Comprometido_Stock = 0;
                    int Cantidad = 0;
                    String Producto_ID = "";
                    foreach (DataRow Producto_Requisicion in Requisicion_Negocio.P_Dt_Productos_Servicios.Rows)
                    {
                        Cantidad = int.Parse(Producto_Requisicion["CANTIDAD"].ToString().Trim());
                        Producto_ID = Producto_Requisicion["PROD_SERV_ID"].ToString().Trim();
                        DataRow[] Dr_Productos_Almacen = Dt_Productos_Almacen.Select("PRODUCTO_ID ='" + Producto_ID + "'");
                        Disponible_Stock = int.Parse(Dr_Productos_Almacen[0]["DISPONIBLE"].ToString().Trim());
                        Comprometido_Stock = int.Parse(Dr_Productos_Almacen[0]["COMPROMETIDO"].ToString().Trim());
                        //Compromete
                        Disponible_Stock = Disponible_Stock - Cantidad;
                        Comprometido_Stock = Comprometido_Stock + Cantidad;
                        Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                            " SET " + Cat_Com_Productos.Campo_Comprometido +
                            " = " + Comprometido_Stock + ", " +
                            Cat_Com_Productos.Campo_Disponible +
                            " = " + Disponible_Stock + "WHERE " +
                            Cat_Com_Productos.Campo_Producto_ID + " = '" +
                            Producto_ID + "'";
                        Registros_Afectados = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                }
            }
            catch (Exception Ex)
            {
                String Str = Ex.ToString();
                throw new Exception(Str);
            }
            return Registros_Afectados;
        }

        #endregion

        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************
        #region BUSQUEDA DE PRODUCTOS Y DETALLES

        public static DataTable Consultar_Poducto_Por_ID(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "SELECT " + Cat_Com_Productos.Tabla_Cat_Com_Productos +".*, ISNULL(" +
            Cat_Com_Productos.Campo_Nombre + ", '')  +', '+ ISNULL(" + Cat_Com_Productos.Campo_Descripcion +
            ", '') AS NOMBRE_DESCRIPCION" +

            ", (SELECT " + Cat_Com_Unidades.Campo_Abreviatura + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " WHERE " +
                Cat_Com_Unidades.Campo_Unidad_ID + " = " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + 
                Cat_Com_Productos.Campo_Unidad_ID + ") AS UNIDAD " +

            " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
            " WHERE " + Cat_Com_Productos.Campo_Producto_ID +
            " IN (" + Requisicion_Negocio.P_Producto_ID + ")";
            DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0)
            {
                return (Data_Set.Tables[0]);
            }
            else
            {
                return null;
            }
        }

        //BUSQUEDA EN MODAL POPUP DE UN PRODUCTO
        public static DataTable Consultar_Productos(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "";
            String Stock = "";
            if (Requisicion_Negocio.P_Tipo == "STOCK" || Requisicion_Negocio.P_Tipo == "TRANSITORIA_STOCK" || Requisicion_Negocio.P_Transitoria_Stock == "SI")
            {
                Stock = "SI";
            }
            else
            {
                Stock = "NO";
            }
            Mi_SQL = "SELECT " + Cat_Com_Productos.Campo_Producto_ID + " AS ID, " +
            Cat_Com_Productos.Tabla_Cat_Com_Productos + ".*, " +
            Cat_Com_Productos.Campo_Nombre + "  +'; '+ " + Cat_Com_Productos.Campo_Descripcion + " AS NOMBRE_DESCRIPCION " +

            ",(SELECT ABREVIATURA FROM CAT_COM_UNIDADES WHERE UNIDAD_ID = CAT_COM_PRODUCTOS.UNIDAD_ID) AS UNIDAD" +
            ",(SELECT NOMBRE FROM CAT_COM_MODELOS WHERE MODELO_ID = CAT_COM_PRODUCTOS.MODELO_ID) AS MODELO" +

            " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
            if (Requisicion_Negocio.P_Transitoria_Stock == "SI")
            {
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Productos.Campo_Stock + " is not null ";
            }else
            {
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Productos.Campo_Stock + " = '" + Stock + "'";
            }
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Productos.Campo_Estatus + " = 'ACTIVO'";
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Partida_ID))
            {
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Productos.Campo_Partida_ID +
                    " = '" + Requisicion_Negocio.P_Partida_ID + "'";
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Nombre_Producto_Servicio))
            {
                Mi_SQL = Mi_SQL + " AND UPPER(" + Cat_Com_Productos.Campo_Nombre +
                    ") LIKE UPPER('%" + Requisicion_Negocio.P_Nombre_Producto_Servicio + "%') ";
            }
            if (!String.IsNullOrEmpty(Requisicion_Negocio.P_Producto_ID)) 
            {
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Productos.Campo_Producto_ID +
                    " = " + Requisicion_Negocio.P_Producto_ID;
            }
            Mi_SQL = Mi_SQL + " ORDER BY " +
                    Cat_Com_Productos.Campo_Nombre + " ASC";
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }

        public static DataSet Consultar_Impuesto(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "SELECT " + Cat_Com_Impuestos.Campo_Nombre + ", " +
                Cat_Com_Impuestos.Campo_Porcentaje_Impuesto + " FROM " +
                Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                " WHERE " + Cat_Com_Impuestos.Campo_Impuesto_ID + " = '" + Requisicion_Negocio.P_Impuesto_ID + "'";
            DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            return Data_Set;
        }


        #endregion

        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************                               
        #region CONSULTA REQUISICIONES
        public static bool Proceso_Filtrar(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            bool Actualizado = false;
            try
            {
                String Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                    " SET " + 
                    Ope_Com_Requisiciones.Campo_Estatus + " = '" + Requisicion_Negocio.P_Estatus + "'," +
                    Ope_Com_Requisiciones.Campo_Fecha_Filtrado + " = GETDATE()" +
                    " WHERE " +
                    Ope_Com_Requisiciones.Campo_Folio + " = '" + Requisicion_Negocio.P_Folio + "'";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                Actualizado = true;
            }
            catch (Exception Ex)
            {
                String Str = Ex.ToString();
                Actualizado = false;
            }
            return Actualizado;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Requisiciones
        ///DESCRIPCIÓN: crea una sentencia sql para conultar una Requisa en la base de datos
        ///PARAMETROS: 1.-Clase de Negocio
        ///            2.-Usuario que crea la requisa
        ///CREO: Silvia Morales Portuhondo
        ///FECHA_CREO: Noviembre/2010 
        ///MODIFICO:Gustavo Angeles Cruz
        ///FECHA_MODIFICO: 25/Ene/2011
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        public static DataTable Consultar_Requisiciones(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            Requisicion_Negocio.P_Fecha_Inicial = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Inicial)) + " 00:00:00";
            Requisicion_Negocio.P_Fecha_Final = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Final)) + " 23:59:59";
            
            if (Requisicion_Negocio.P_Estatus == "CONST ,GENERADA, REVISAR")
            {
                Requisicion_Negocio.P_Estatus = "EN CONSTRUCCION,GENERADA,REVISAR,RECHAZADA";
            }
            Requisicion_Negocio.P_Estatus = Requisicion_Negocio.P_Estatus.Replace(",","','");
            if (Requisicion_Negocio.P_Estatus == "TODAS")
            {
                Requisicion_Negocio.P_Estatus = null;
            }
            Requisicion_Negocio.P_Tipo = Requisicion_Negocio.P_Tipo.Replace(",","','");
            String Mi_Sql = "";
            Mi_Sql =
            "SELECT " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + ".*, " +
                "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " +
                Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                ") NOMBRE_DEPENDENCIA " +
            " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
            " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +" IS NOT NULL";
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Estatus))
            {
                Mi_Sql += " AND "+ Ope_Com_Requisiciones.Campo_Estatus + " IN ('" + Requisicion_Negocio.P_Estatus + "')";
            }
            Mi_Sql +=" AND " + Ope_Com_Requisiciones.Campo_Tipo + " IN ('" + Requisicion_Negocio.P_Tipo + "')";
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Dependencia_ID) && Requisicion_Negocio.P_Dependencia_ID != "0")
            {
                Mi_Sql += " AND " + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                " IN ('" + Requisicion_Negocio.P_Dependencia_ID + "')";
            }
            if (Requisicion_Negocio.P_Estatus == "GENERADA")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Generacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + 
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Generacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else if (Requisicion_Negocio.P_Estatus == "CANCELADA")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Cancelada + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + 
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Cancelada + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else if (Requisicion_Negocio.P_Estatus == "EN CONSTRUCCION")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Construccion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + 
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Construccion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else if (Requisicion_Negocio.P_Estatus == "AUTORIZADA")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + 
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else if (Requisicion_Negocio.P_Estatus == "CONFIRMADA")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Confirmacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + 
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Confirmacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else if (Requisicion_Negocio.P_Estatus == "COTIZADA")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + 
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else if (Requisicion_Negocio.P_Estatus == "REVISAR")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + 
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else if (Requisicion_Negocio.P_Estatus == "COMPRA")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + 
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else 
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Creo + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + 
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Creo + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Requisicion_ID))
            {
                Mi_Sql +=
                " AND " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                " = " + Requisicion_Negocio.P_Requisicion_ID;
            }
            if (Requisicion_Negocio.P_Cotizador_ID != null && Requisicion_Negocio.P_Cotizador_ID != "0")
            {
                Mi_Sql +=
                " AND " + Ope_Com_Requisiciones.Campo_Cotizador_ID +
                " = '" + Requisicion_Negocio.P_Cotizador_ID + "'";
            }

            
            Mi_Sql = Mi_Sql + " ORDER BY " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " DESC";
            DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0)
            {
                return (Data_Set.Tables[0]);
            }
            else
            {
                return null;
            }
        }

        public static DataTable Consultar_Requisiciones_Generales(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            Requisicion_Negocio.P_Fecha_Inicial = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Inicial));
            Requisicion_Negocio.P_Fecha_Final = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Final));
            if (Requisicion_Negocio.P_Tipo == "TODOS")
            {
                Requisicion_Negocio.P_Tipo = "STOCK','TRANSITORIA";
            }
            String Mi_Sql = "";
            Mi_Sql =
            "SELECT " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + ".*, " +
                "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " +
                Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                ") NOMBRE_DEPENDENCIA " +
            " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
            " WHERE " +
            Ope_Com_Requisiciones.Campo_Tipo + " IN ('" + Requisicion_Negocio.P_Tipo + "')" +
            " AND " + Ope_Com_Requisiciones.Campo_Fecha_Creo +
                        " between '" + Requisicion_Negocio.P_Fecha_Inicial + " 00:00:00' AND " +

                        " '" + Requisicion_Negocio.P_Fecha_Final + " 23:59:00'";
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Dependencia_ID) && Requisicion_Negocio.P_Dependencia_ID != "0")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Dependencia_ID + " = " +
                    "'" + Requisicion_Negocio.P_Dependencia_ID + "'";
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Requisicion_ID))
            {
                Mi_Sql +=
                " AND " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                " = " + Requisicion_Negocio.P_Requisicion_ID;
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Estatus))
            {
                Mi_Sql +=
                " AND " + Ope_Com_Requisiciones.Campo_Estatus +
                " IN ('" + Requisicion_Negocio.P_Estatus + "')";
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Cotizador_ID))
            {
                Mi_Sql +=
                    " AND " + Ope_Com_Requisiciones.Campo_Cotizador_ID +
                    " = '" + Requisicion_Negocio.P_Cotizador_ID + "' ";
            }
            Mi_Sql = Mi_Sql + " ORDER BY " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " DESC";
            DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0)
            {
                return (Data_Set.Tables[0]);
            }
            else
            {
                return null;
            }
        }

        //
        public static DataTable Consultar_Requisiciones_En_Web(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            //Requisicion_Negocio.P_Fecha_Inicial = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Inicial));
            //Requisicion_Negocio.P_Fecha_Final = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Final));
            String Mi_Sql = "";
            Mi_Sql =
            "SELECT " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + ".*, " +
                "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " +
                Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                ") NOMBRE_DEPENDENCIA " +
            " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
            " WHERE " +
            Ope_Com_Requisiciones.Campo_Tipo + " IN ('" + Requisicion_Negocio.P_Tipo + "')" +
             " AND " + Ope_Com_Requisiciones.Campo_Estatus + " IN (" + Requisicion_Negocio.P_Estatus + ")";
            if (!String.IsNullOrEmpty(Requisicion_Negocio.P_Folio))
            {
                Mi_Sql += " And " + Ope_Com_Requisiciones.Campo_Folio + " like '%" + Requisicion_Negocio.P_Folio + "%'";
            }

            Mi_Sql += " ORDER BY " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " DESC";
            DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0)
            {
                return (Data_Set.Tables[0]);
            }
            else
            {
                return null;
            }
        }
        public static DataTable Consultar_Requisicion_Por_ID(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_Sql = "";
            Mi_Sql =
            "SELECT " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + ".*, " +
                "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " +
                Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                ") NOMBRE_DEPENDENCIA " +
            " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
            " WHERE " +
            Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Requisicion_Negocio.P_Requisicion_ID;
            DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0)
            {
                return (Data_Set.Tables[0]);
            }
            else
            {
                return null;
            } 
        }

        public static DataSet Consultar_Requisicion(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_Sql = "";
            Mi_Sql = "SELECT * FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
            Mi_Sql += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Requisicion_Negocio.P_Requisicion_ID;

            Mi_Sql += " SELECT * FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
            Mi_Sql += " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + Requisicion_Negocio.P_Requisicion_ID;

            Mi_Sql += " SELECT REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
            Mi_Sql += ", DEP." + Cat_Dependencias.Campo_Nombre;
            Mi_Sql += " AS DEPENDENCIA, REQ_PROD." + Ope_Com_Req_Producto.Campo_Partida_ID;
            Mi_Sql += ", PART." + Cat_Sap_Partidas_Especificas.Campo_Nombre;
            Mi_Sql += " AS PARTIDA, REQ_PROD." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID;
            Mi_Sql += ", PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Nombre;
            Mi_Sql += " AS PRYECTO_PROGRAMA, REQ_PROD." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID;
            Mi_Sql += ", FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion;
            Mi_Sql += " AS FTE_FINANCIAMIENTO FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ ";

            Mi_Sql += " LEFT OUTER JOIN " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PROD ";
            Mi_Sql += "ON REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID;

            Mi_Sql += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP ";
            Mi_Sql += "ON REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID + " = DEP." + Cat_Dependencias.Campo_Dependencia_ID;

            Mi_Sql += " LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PART ";
            Mi_Sql += "ON REQ_PROD." + Ope_Com_Req_Producto.Campo_Partida_ID + " = PART." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID;

            Mi_Sql += " LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " PROY_PROG ";
            Mi_Sql += "ON REQ_PROD." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID + " = PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;

            Mi_Sql += " LEFT OUTER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FTE_FIN ";
            Mi_Sql += "ON REQ_PROD." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID + " = FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;

            Mi_Sql += " WHERE REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Requisicion_Negocio.P_Requisicion_ID;
            Mi_Sql += " GROUP BY ";
            Mi_Sql += " REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
            Mi_Sql += ", DEP." + Cat_Dependencias.Campo_Nombre;
            Mi_Sql += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Partida_ID;
            Mi_Sql += ", PART." + Cat_Sap_Partidas_Especificas.Campo_Nombre;
            Mi_Sql += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID;
            Mi_Sql += ", PROY_PROG." + Cat_Sap_Proyectos_Programas.Campo_Nombre;
            Mi_Sql += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID;
            Mi_Sql += ", FTE_FIN." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion;

            DataSet Data_Set = new DataSet();
            Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Data_Set.Tables[0].TableName = "Dt_Requisiciones";
            Data_Set.Tables[1].TableName = "Dt_Req_Producto";
            Data_Set.Tables[2].TableName = "Dt_Detalles";

            return Data_Set;
        }

        //Consulta los detalles de la requisición
        public static DataTable Consultar_Productos_Servicios_Requisiciones(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "";
            if (Requisicion_Negocio.P_Tipo_Articulo != null && Requisicion_Negocio.P_Tipo_Articulo.Trim() == "PRODUCTO")
            {
                Mi_SQL = "SELECT " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + ".*," +
                "(SELECT " + Cat_Com_Unidades.Campo_Abreviatura + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " WHERE " +
                Cat_Com_Unidades.Campo_Unidad_ID + " = (SELECT " + Cat_Com_Productos.Campo_Unidad_ID + " FROM " +
                Cat_Com_Productos.Tabla_Cat_Com_Productos + " WHERE " + Cat_Com_Productos.Campo_Producto_ID + " = " +
                Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + ")) AS UNIDAD ";
            }
            else 
            {
                Mi_SQL = "SELECT " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + ".*," +
                "'S/U' AS UNIDAD ";
            }
             Mi_SQL = Mi_SQL + "FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto +
                " WHERE " + 
                Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + "." +
                Ope_Com_Req_Producto.Campo_Requisicion_ID + 
                " = '" + Requisicion_Negocio.P_Requisicion_ID + "'";
            DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            DataTable _DataTable = null;
            if (_DataSet != null && _DataSet.Tables.Count > 0 && _DataSet.Tables[0].Rows.Count > 0)
            {
                _DataTable = _DataSet.Tables[0];
            }
            return _DataTable;
        }


        public static DataTable Consultar_Bienes_Requisicion(Cls_Ope_Com_Requisiciones_Negocio Requisiciones_Negocio) {
            DataTable Dt_Datos = new DataTable();
            try {
                String Mi_SQL = "SELECT REQUISICIONES_BIENES." + Ope_Com_Req_Bienes.Campo_Bien_Mueble_ID + " AS BIEN_MUEBLE_ID";
                Mi_SQL = Mi_SQL + ", STR(BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + ") AS NUMERO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Nombre + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie + " AS NUMERO_SERIE";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Modelo + " AS MODELO";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Req_Bienes.Tabla_Ope_Com_Req_Bienes + " REQUISICIONES_BIENES";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " BIENES_MUEBLES";
                Mi_SQL = Mi_SQL + " ON REQUISICIONES_BIENES." + Ope_Com_Req_Bienes.Campo_Bien_Mueble_ID + " = BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID;
                Mi_SQL = Mi_SQL + " WHERE REQUISICIONES_BIENES." + Ope_Com_Req_Bienes.Campo_Requisicion_ID + " = '" + Requisiciones_Negocio.P_Requisicion_ID + "'";
                DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Datos != null)
                {
                    if (Ds_Datos.Tables.Count > 0)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
            } catch (SqlException Ex) {
                throw new Exception("Excepción al consultar los Bienes de la Requisición: [" + Ex.Message + "]"); 
            }
            return Dt_Datos;
        }

        public static DataTable Consultar_Bienes_Muebles(Cls_Ope_Com_Requisiciones_Negocio Requisiciones_Negocio) {
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try {
                String Mi_SQL = "SELECT BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + " AS BIEN_MUEBLE_ID";
                Mi_SQL = Mi_SQL + ", STR(BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + ") AS NUMERO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Nombre + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie + " AS NUMERO_SERIE";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Modelo + " AS MODELO";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " BIENES_MUEBLES";
                if (!String.IsNullOrEmpty(Requisiciones_Negocio.P_Busq_Nombre)) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Entro_Where = true; Mi_SQL += " WHERE "; }
                    Mi_SQL = Mi_SQL + " BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Nombre + " LIKE '%" + Requisiciones_Negocio.P_Busq_Nombre.Trim() + "%'";
                }
                if (!String.IsNullOrEmpty(Requisiciones_Negocio.P_Busq_Modelo)) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Entro_Where = true; Mi_SQL += " WHERE "; }
                    Mi_SQL = Mi_SQL + " BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Modelo + " LIKE '%" + Requisiciones_Negocio.P_Busq_Modelo.Trim() + "%'";
                }
                if (!String.IsNullOrEmpty(Requisiciones_Negocio.P_Busq_Serie)) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Entro_Where = true; Mi_SQL += " WHERE "; }
                    Mi_SQL = Mi_SQL + " BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie + " LIKE '%" + Requisiciones_Negocio.P_Busq_Serie.Trim() + "%'";
                }
                if (!String.IsNullOrEmpty(Requisiciones_Negocio.P_Busq_Inventario)) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Entro_Where = true; Mi_SQL += " WHERE "; }
                    Mi_SQL = Mi_SQL + " BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " = '" + Requisiciones_Negocio.P_Busq_Inventario.Trim() + "'";
                }
                DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Datos != null) {
                    if (Ds_Datos.Tables.Count > 0) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
            } catch (SqlException Ex) {
                throw new Exception("Excepción al consultar los Bienes de la Requisición: [" + Ex.Message + "]"); 
            }
            return Dt_Datos;
        }
        

        #endregion

        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************                               
        #region SERVICIOS

        public static DataTable Consultar_Servicio_Por_ID(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "SELECT " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + ".*, " +  
                Cat_Com_Servicios.Campo_Nombre + 
                " AS NOMBRE_DESCRIPCION, 'S/U' AS UNIDAD " +
                " FROM " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios +

              

                " WHERE " + Cat_Com_Servicios.Campo_Servicio_ID +
                " IN (" + Requisicion_Negocio.P_Producto_ID + ")";//el Producto ID de Negocio sirve para pasar el Servicio_ID
            DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            if (_DataSet != null && _DataSet.Tables[0].Rows.Count > 0)
            {
                return (_DataSet.Tables[0]);
            }
            else
            {
                return null;
            }
        }
        
        public static DataTable Consultar_Servicios(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "SELECT " +
                Cat_Com_Servicios.Campo_Clave + ", " +
                Cat_Com_Servicios.Campo_Servicio_ID + " AS ID, " + 
                Cat_Com_Servicios.Tabla_Cat_Com_Servicios + ".*, '0' AS DISPONIBLE, 'SRV' AS MODELO" +
                " FROM " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios +
                " WHERE " + 
                Cat_Com_Partidas.Campo_Partida_ID + " = '" + Requisicion_Negocio.P_Partida_ID + "'";

            if (Requisicion_Negocio.P_Nombre_Producto_Servicio != null)
            {
                Mi_SQL = Mi_SQL + " AND UPPER (" + Cat_Com_Servicios.Campo_Nombre +
                    ") LIKE UPPER ('%" + Requisicion_Negocio.P_Nombre_Producto_Servicio + "%')";
            }
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }

        public static int Consultar_Num_Reserva(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            int Num_Reserva = 0;
            try
            {
                String Mi_Sql = "";
                Mi_Sql = "SELECT NUM_RESERVA FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                    " WHERE " +
                    Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Requisicion_Negocio.P_Requisicion_ID.Trim();
                Object Objeto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                Num_Reserva = int.Parse(Objeto.ToString());
            }
            catch (Exception Ex)
            {
                Ex.ToString();
            }
            return Num_Reserva;
        }
        #endregion

        //Metodo por checar
        //OBTINE PARTIDAS ESPECIFICAS CON PRESPUESTOS A PARTIR DE LA DEPENDENCIA Y EL PROYECTO
        public static DataTable Consultar_Presupuesto_Partidas(Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "";

            Mi_SQL =
            "SELECT " +
            Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ", " +
                "(SELECT " + Cat_Com_Partidas.Campo_Nombre + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " +
                Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") NOMBRE, " +

                "(SELECT " + Cat_Com_Partidas.Campo_Clave + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " +
                Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") CLAVE, " +

                "(SELECT " + Cat_Com_Partidas.Campo_Clave + " +' '+" +
                Cat_Com_Partidas.Campo_Nombre + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " +
                Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") CLAVE_NOMBRE, " +

                //con esto obtengo el giro de la partida
                //"(SELECT " + Cat_Com_Partidas.Campo_Giro_ID + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                //" WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " +
                //Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                //Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") GIRO_ID, " +

            Cat_Com_Dep_Presupuesto.Campo_Monto_Presupuestal + ", " +
            Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " MONTO_DISPONIBLE, " +
            Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + ", " +
            Cat_Com_Dep_Presupuesto.Campo_Monto_Ejercido + ", " +
            Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto + ", " +
            Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ", " +
            Cat_Com_Dep_Presupuesto.Campo_Fecha_Creo +
                //" TO_CHAR(FECHA_CREO ,'DD/MM/YY') FECHA_CREO" + 
            " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
            " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
            " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
            " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
            " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
            " IN (" + Requisicion_Negocio.P_Partida_ID + ")" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
            " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + " = " +
                "(SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
                " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                " WHERE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                            Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                            " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                            Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
                            " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
                            " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +

                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                            " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +

                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                            " IN (" + Requisicion_Negocio.P_Partida_ID + "))";

            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Cotizadores
        ///DESCRIPCIÓN          : Consulta los Cotizadores
        ///PARAMETROS           : Objeto de la clase Cls_Cat_Psp_SubNivel_Presupuesto_Negocio
        ///CREO                 : Jennyfer Ivonne Cceja Lemus
        ///FECHA_CREO           : 16/Septiembre/2012 06:44 pm
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Cotizadores(Cls_Ope_Com_Requisiciones_Negocio Negocio)
        {
            String MiSql = String.Empty;

            try
            {

                MiSql = "SELECT " + Cat_Com_Cotizadores.Campo_Empleado_ID+ ", ";
                MiSql += Cat_Com_Cotizadores.Campo_Nombre_Completo  ;
                MiSql += " FROM " + Cat_Com_Cotizadores.Tabla_Cat_Com_Cotizadores;
                MiSql += " ORDER BY NOMBRE_COMPLETO ";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }

    }
}