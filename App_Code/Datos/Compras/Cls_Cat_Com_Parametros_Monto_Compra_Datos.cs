﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Parametros_Monto_Compra.Negocios;

/// <summary>
/// Summary description for Cls_Cat_Com_Parametros_Monto_Compra
/// </summary>
namespace JAPAMI.Parametros_Monto_Compra.Datos
{
    public class Cls_Cat_Com_Parametros_Monto_Compra_Datos
    {
        public Cls_Cat_Com_Parametros_Monto_Compra_Datos()
        {
        }
        #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Monto_Compra
        ///DESCRIPCIÓN          :Consulta los parametros registrados en la base de datos
        ///PARAMETROS           :  
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 09/Noviembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************

        public static DataTable Consultar_Monto_Compra()
        {
            try
            {
                String Mi_SQL = "SELECT * FROM  " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros;
                //Sentencia que ejecuta el query
                DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Data_Table;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Parametro_Monto_Compra
        ///DESCRIPCIÓN          : Da de alta el parametro monto de compra, en caso de que no exista
        ///                       ninguna fila en la tabla Cat_Com_Parametros.
        ///PARAMETROS           :  1.-  objeto de Cls_Cat_Com_Parametros_Monto_Compra
        ///                         para obrener los datos de la capa de negocios
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 09/Noviembre/2012 12:56 pm
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************

        public static Boolean Alta_Parametro_Monto_Compra(Cls_Cat_Com_Parametros_Monto_Compra_Negocio Parametro_Negocio)
        {
            Boolean Operacion = false;
            try
            {
                String Mi_SQL = String.Empty;
                object Parametro_ID;
                String Consecutivo = "";
                //Obtener el ID Consecutivo
                Mi_SQL = "SELECT ISNULL(MAX(" + Cat_Com_Parametros.Campo_Parametro_ID + "), '00000') FROM " + 
                          Cat_Com_Parametros.Tabla_Cat_Com_Parametros;
                Parametro_ID = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                //Verificar si no es nulo
                if (Convert.IsDBNull(Parametro_ID) == false)
                    Consecutivo = String.Format("{0:00000}", Convert.ToInt32(Parametro_ID) + 1);
                else
                    Consecutivo = "00001";
                    
                Mi_SQL = "INSERT INTO " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros+
                         "(" + Cat_Com_Parametros.Campo_Parametro_ID + ", " + Cat_Com_Parametros.Campo_Monto_Compra + ") VALUES ('" +
                          Consecutivo  + "', " + Parametro_Negocio.P_Monto_Compra + " ) ";
                //Sentencia que ejecuta el query
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                return Operacion = true;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Parametro_Monto_Compra
        ///DESCRIPCIÓN          : Actualiza el valor del parametro
        ///PARAMETROS           :  1.-  objeto de Cls_Cat_Com_Parametros_Monto_Compra
        ///                         para obrener los datos de la capa de negocios
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 09/Noviembre/2012 01:07 pm
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************

        public static Boolean Modificar_Parametro_Monto_Compra(Cls_Cat_Com_Parametros_Monto_Compra_Negocio Parametro_Negocio)
        {
            Boolean Operacion = false; // Variable que informara el estatus de la operacion 
            try
            {
                String Mi_SQL = "UPDATE " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros +
                    " SET " + Cat_Com_Parametros.Campo_Monto_Compra + " = " + Parametro_Negocio.P_Monto_Compra;
                //Sentencia que ejecuta el query
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                return Operacion = true;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        #endregion
    }
}
