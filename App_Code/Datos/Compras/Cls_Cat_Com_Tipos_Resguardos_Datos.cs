﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Cat_Tipos_Resguardos.Negocio;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;

/// <summary>
/// Summary description for Cls_Cat_Com_Tipos_Resguardos_Datos
/// </summary>
namespace JAPAMI.Cat_Tipos_Resguardos.Datos
{
    public class Cls_Cat_Com_Tipos_Resguardos_Datos
    {
	    public Cls_Cat_Com_Tipos_Resguardos_Datos()
	    {
         
	    }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Tipo_Resguardo
        ///DESCRIPCIÓN: Inserta un registro de un nuevo Resguardo
        ///PARAMETROS: El objeto Tipos_Resguardos_Negocio que se encarga de la comunicacion con la clase de Negocio
        ///CREO: Jennyfer Ivonne Ceja Lemus 
        ///FECHA_CREO: 05/julio/2012 10:25 a.m.
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Alta_Tipo_Resguardo(Cls_Cat_Com_Tipos_Resguardos_Negocio Tipos_Resguardos_Negocio)
        {
            String Mi_SQL = String.Empty;

            try
            {
                //Query para la insercion
                //FALTA EL ID
                Int32 ID = Obtener_Consecutivo(Cat_Com_Tipos_Resguardo.Campo_Tipo_Resguardo_ID, Cat_Com_Tipos_Resguardo.Tabla_Cat_Com_Tipos_Resguardo); 

                Mi_SQL = "INSERT INTO " + Cat_Com_Tipos_Resguardo.Tabla_Cat_Com_Tipos_Resguardo + "(";
                Mi_SQL = Mi_SQL + Cat_Com_Tipos_Resguardo.Campo_Tipo_Resguardo_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Com_Tipos_Resguardo.Campo_Estatus + ", ";
                Mi_SQL = Mi_SQL + Cat_Com_Tipos_Resguardo.Campo_Nombre + ", ";
                Mi_SQL = Mi_SQL + Cat_Com_Tipos_Resguardo.Campo_Descripcion + ", ";
                Mi_SQL = Mi_SQL + Cat_Com_Tipos_Resguardo.Campo_Fecha_Creo + ", ";
                Mi_SQL = Mi_SQL + Cat_Com_Tipos_Resguardo.Campo_Usuario_Creo + ")";
                Mi_SQL = Mi_SQL + "VALUES("+ ID +", '" + Tipos_Resguardos_Negocio.P_Estatus + "', ";
                Mi_SQL = Mi_SQL + "'" + Tipos_Resguardos_Negocio.P_Nombre + "', ";
                Mi_SQL = Mi_SQL + "'" + Tipos_Resguardos_Negocio.P_Descripcion + "', ";
                Mi_SQL = Mi_SQL + "GETDATE(), '" + Cls_Sessiones.Nombre_Empleado + "' )";
                
                //Ejecutar Query
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Tipo_Resguardo
        ///DESCRIPCIÓN: Modifica un registro de Tipo Resguardo
        ///PARAMETROS: El objeto Tipos_Resguardos_Negocio que se encarga de la comunicacion con la clase de Negocio
        ///CREO: Jennyfer Ivonne Ceja Lemus 
        ///FECHA_CREO: 05/julio/2012 11:07 a.m.
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Modificar_Tipo_Resguardo(Cls_Cat_Com_Tipos_Resguardos_Negocio Tipos_Resguardos_Negocio) 
        {
            String Mi_SQL = String.Empty;
            
            try
            {
                //Query para al actualizacion del Resguardo
                Mi_SQL = "UPDATE " +Cat_Com_Tipos_Resguardo.Tabla_Cat_Com_Tipos_Resguardo + " SET ";
                Mi_SQL = Mi_SQL + Cat_Com_Tipos_Resguardo.Campo_Estatus + " = '" + Tipos_Resguardos_Negocio.P_Estatus + "', ";
                Mi_SQL = Mi_SQL + Cat_Com_Tipos_Resguardo.Campo_Nombre + " = '" + Tipos_Resguardos_Negocio.P_Nombre + "', ";
                Mi_SQL = Mi_SQL + Cat_Com_Tipos_Resguardo.Campo_Descripcion + " = '" + Tipos_Resguardos_Negocio.P_Descripcion + "', ";
                Mi_SQL = Mi_SQL + Cat_Com_Tipos_Resguardo.Campo_Fecha_Modifico + " = GETDATE(), ";
                Mi_SQL = Mi_SQL + Cat_Com_Tipos_Resguardo.Campo_Usuario_Modifico + "= '" + Cls_Sessiones.Nombre_Empleado + "' ";
                Mi_SQL = Mi_SQL + "WHERE " + Cat_Com_Tipos_Resguardo.Campo_Tipo_Resguardo_ID + " = " + Tipos_Resguardos_Negocio.P_Tipo_Resguardo_ID;
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Tipo_Resguardo
        ///DESCRIPCIÓN: Consulta un Tipo resguardo de acuerdo al ID
        ///PARAMETROS: El objeto Tipos_Resguardos_Negocio que se encarga de la comunicacion con la clase de Negocio
        ///CREO: Jennyfer Ivonne Ceja Lemus 
        ///FECHA_CREO: 05/julio/2012 11:30 a.m.
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Tipo_Resguardo(Cls_Cat_Com_Tipos_Resguardos_Negocio Tipos_Resguardos_Negocio) 
        {
            String Mi_SQL = String.Empty;

            try
            {
                //Query para consultar el resguardo seleccionado
                Mi_SQL = "SELECT * FROM " + Cat_Com_Tipos_Resguardo.Tabla_Cat_Com_Tipos_Resguardo;
                if ((!Tipos_Resguardos_Negocio.P_Nombre.Equals("")) && Tipos_Resguardos_Negocio.P_Nombre!=String.Empty)
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Tipos_Resguardo.Campo_Nombre + " LIKE '%" + Tipos_Resguardos_Negocio.P_Nombre + "%'";
                }
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Eliminar_Tipo_Resguardo
        ///DESCRIPCIÓN: Elimina uu registro de Tipo Resguardo
        ///PARAMETROS: El objeto Tipos_Resguardos_Negocio que se encarga de la comunicacion con la clase de Negocio
        ///CREO: Jennyfer Ivonne Ceja Lemus 
        ///FECHA_CREO: 05/julio/2012 11:55 a.m.
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Eliminar_Tipo_Resguardo(Cls_Cat_Com_Tipos_Resguardos_Negocio Tipos_Resguardos_Negocio)
        {
            String Mi_SQL = String.Empty;

            try
            {
                //QUERY Para eliminar un resguardo
                Mi_SQL = "DELETE FROM " + Cat_Com_Tipos_Resguardo.Tabla_Cat_Com_Tipos_Resguardo;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Tipos_Resguardo.Campo_Tipo_Resguardo_ID +" = " + Tipos_Resguardos_Negocio.P_Tipo_Resguardo_ID;

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Buscar_Tipo_Resguardo
        ///DESCRIPCIÓN: Busca alguna coincidencia dentro de la caja de texto
        ///PARAMETROS:El objeto Tipos_Resguardos_Negocio que se encarga de la comunicacion con la clase de Negocio
        ///CREO: Jennyfer
        ///FECHA_CREO: 06/JULIO/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Buscar_Tipo_Resguardo(Cls_Cat_Com_Tipos_Resguardos_Negocio Tipos_Resguardos_Negocio)
        {
            String Mi_SQL = String.Empty;
            try
            {
                Mi_SQL = "SELECT * FROM " + Cat_Com_Tipos_Resguardo.Tabla_Cat_Com_Tipos_Resguardo;
                
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Jennifer
        ///FECHA_CREO: 5/JULIO/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }


    }//fin de la clase
}//fin de namespace
