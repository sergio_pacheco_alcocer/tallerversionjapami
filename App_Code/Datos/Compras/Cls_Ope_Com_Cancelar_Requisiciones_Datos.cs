﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
using System.Data;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Cancelar_Requisiciones.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Manejo_Presupuesto.Datos;

namespace JAPAMI.Cancelar_Requisiciones.Datos
{
    public class Cls_Ope_Com_Cancelar_Requisiciones_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Requisiciones
        ///DESCRIPCIÓN          : Obtiene registros de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 1.Parametros. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Requisiciones(Cls_Ope_Com_Cancelar_Requisiciones_Negocio Parametros)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            DataSet Ds_Datos = new DataSet();
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            
            try
            {
                Mi_SQL.Append("SELECT * FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones);
                if (!String.IsNullOrEmpty(Parametros.P_Requisicion_ID))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Com_Requisiciones.Campo_Requisicion_ID + " = '" + Parametros.P_Requisicion_ID + "'");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Tipo))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Com_Requisiciones.Campo_Tipo + " = '" + Parametros.P_Tipo + "'");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Cotizador_ID))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Com_Requisiciones.Campo_Cotizador_ID + " = '" + Parametros.P_Cotizador_ID.Trim() + "'");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Tipo_Articulo))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(" LTRIM(RTRIM(" + Ope_Com_Requisiciones.Campo_Tipo_Articulo + ")) = '" + Parametros.P_Tipo_Articulo.Trim() + "'");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Com_Requisiciones.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Folio))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append("UPPER(" + Ope_Com_Requisiciones.Campo_Folio + ") LIKE UPPER('%" + Parametros.P_Folio + "%')");
                }
                if (!String.IsNullOrEmpty(Mi_SQL.ToString()))
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }
                if (Ds_Datos != null && Ds_Datos.Tables.Count > -1)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Proveedores_Requisicion
        ///DESCRIPCIÓN          : Obtiene registros de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 1.Parametros. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Proveedores_Requisicion(Cls_Ope_Com_Cancelar_Requisiciones_Negocio Parametros)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            DataSet Ds_Datos = new DataSet();
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL.Append("SELECT DISTINCT(REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Proveedor_ID);
                Mi_SQL.Append("), PROVEEDORES." + Cat_Com_Proveedores.Campo_Nombre);
                Mi_SQL.Append(" FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PRODUCTO");
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDORES");
                Mi_SQL.Append(" ON REQ_PRODUCTO." + Cat_Com_Proveedores.Campo_Proveedor_ID + " = PROVEEDORES." + Cat_Com_Proveedores.Campo_Proveedor_ID);

                if (!String.IsNullOrEmpty(Parametros.P_Requisicion_ID))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Com_Req_Producto.Campo_Requisicion_ID + " = '" + Parametros.P_Requisicion_ID + "'");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Tipo))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Com_Req_Producto.Campo_Tipo + " = '" + Parametros.P_Tipo + "'");
                }
                if (!String.IsNullOrEmpty(Mi_SQL.ToString()))
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }
                if (Ds_Datos != null && Ds_Datos.Tables.Count > -1)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Productos_Requisicion
        ///DESCRIPCIÓN          : Obtiene registros de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 1.Parametros. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Productos_Requisicion(Cls_Ope_Com_Cancelar_Requisiciones_Negocio Parametros)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            DataSet Ds_Datos = new DataSet();
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL.Append("SELECT * FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto);
                if (!String.IsNullOrEmpty(Parametros.P_Requisicion_ID))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Com_Req_Producto.Campo_Requisicion_ID + " = '" + Parametros.P_Requisicion_ID + "'");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Tipo))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Com_Req_Producto.Campo_Tipo + " = '" + Parametros.P_Tipo + "'");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Proveedor_ID))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append(Ope_Com_Req_Producto.Campo_Proveedor_ID + " = '" + Parametros.P_Proveedor_ID + "'");
                }
                if (!String.IsNullOrEmpty(Mi_SQL.ToString()))
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }
                if (Ds_Datos != null && Ds_Datos.Tables.Count > -1)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Requisicion
        ///DESCRIPCIÓN          : Modifica una Requisicion en la base de datos.
        ///PARAMETROS           : Parametros: Contiene el registro que sera modificado.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 12/Enero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Modificar_Requisicion(Cls_Ope_Com_Cancelar_Requisiciones_Negocio Parametros)
        {
            StringBuilder Mi_SQL;
            DataTable Dt_Requisicion = new DataTable();
            String Partida_ID = "";
            String Proyecto_ID = "";
            String Dependencia_ID = "";
            String FF = "";
            String Num_Reserva = "";
            Double Monto_Anterior = 0;
            String Cargo = "";
            String Abono = "";

            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                Int32 Consecutivo = Obtener_Consecutivo(Ope_Com_Historial_Req.Campo_No_Historial, Ope_Com_Historial_Req.Tabla_Ope_Com_Historial_Req);
                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("INSERT INTO " + Ope_Com_Historial_Req.Tabla_Ope_Com_Historial_Req);
                Mi_SQL.Append(" (" + Ope_Com_Historial_Req.Campo_No_Historial);
                Mi_SQL.Append(", " + Ope_Com_Historial_Req.Campo_Estatus);
                Mi_SQL.Append(", " + Ope_Com_Historial_Req.Campo_Fecha);
                Mi_SQL.Append(", " + Ope_Com_Historial_Req.Campo_Empleado);
                Mi_SQL.Append(", " + Ope_Com_Historial_Req.Campo_No_Requisicion);
                Mi_SQL.Append(", " + Ope_Com_Historial_Req.Campo_Fecha_Creo);
                Mi_SQL.Append(", " + Ope_Com_Historial_Req.Campo_Usuario_Creo);
                Mi_SQL.Append(") VALUES ( " + Consecutivo);
                Mi_SQL.Append(" , '" + Parametros.P_Estatus);
                Mi_SQL.Append("', GETDATE()");
                Mi_SQL.Append(" , '" + Cls_Sessiones.Nombre_Empleado);
                Mi_SQL.Append("',  " + Parametros.P_Requisicion_ID);
                Mi_SQL.Append(" , GETDATE()");
                Mi_SQL.Append(" , '" + Cls_Sessiones.Nombre_Empleado + "')");
                Cmd.CommandText = Mi_SQL.ToString();
                Cmd.ExecuteNonQuery();

                Consecutivo = Obtener_Consecutivo(Ope_Com_Req_Observaciones.Campo_Observacion_ID, Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones);
                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("INSERT INTO " + Ope_Com_Req_Observaciones.Tabla_Ope_Com_Req_Observaciones);
                Mi_SQL.Append(" (" + Ope_Com_Req_Observaciones.Campo_Observacion_ID);
                Mi_SQL.Append(", " + Ope_Com_Req_Observaciones.Campo_Requisicion_ID);
                Mi_SQL.Append(", " + Ope_Com_Req_Observaciones.Campo_Comentario);
                Mi_SQL.Append(", " + Ope_Com_Req_Observaciones.Campo_Estatus);
                Mi_SQL.Append(", " + Ope_Com_Req_Observaciones.Campo_Fecha_Creo);
                Mi_SQL.Append(", " + Ope_Com_Req_Observaciones.Campo_Usuario_Creo);
                Mi_SQL.Append(") VALUES ( " + Consecutivo);
                Mi_SQL.Append(" , " + Parametros.P_Requisicion_ID);
                Mi_SQL.Append(" , '" + Parametros.P_Comentarios);
                Mi_SQL.Append("', '" + Parametros.P_Estatus);
                Mi_SQL.Append("', GETDATE()");
                Mi_SQL.Append(" , '" + Cls_Sessiones.Nombre_Empleado + "')");
                Cmd.CommandText = Mi_SQL.ToString();
                Cmd.ExecuteNonQuery();

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " SET ");
                if (!String.IsNullOrEmpty(Parametros.P_Cotizador_ID))
                { Mi_SQL.Append(Ope_Com_Requisiciones.Campo_Cotizador_ID + " = '" + Parametros.P_Cotizador_ID + "', "); }
                if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                { Mi_SQL.Append(Ope_Com_Requisiciones.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "', "); }
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                { Mi_SQL.Append(Ope_Com_Requisiciones.Campo_Estatus + " = '" + Parametros.P_Estatus + "', "); }
                if (!String.IsNullOrEmpty(Parametros.P_Tipo))
                { Mi_SQL.Append(Ope_Com_Requisiciones.Campo_Tipo + " = '" + Parametros.P_Tipo + "', "); }
                if (!String.IsNullOrEmpty(Parametros.P_Tipo_Articulo))
                { Mi_SQL.Append(Ope_Com_Requisiciones.Campo_Tipo_Articulo + " = '" + Parametros.P_Tipo_Articulo + "', "); }
                if (Parametros.P_Estatus != "CANCELADA")
                {
                    Mi_SQL.Append(Ope_Com_Requisiciones.Campo_Cotizador_ID + " =NULL, ");
                }
                
                Mi_SQL.Append(Parametros.P_Estatus == "CANCELADA" ? Ope_Com_Requisiciones.Campo_Fecha_Cancelada : Ope_Com_Requisiciones.Campo_Fecha_Rechazo);
                Mi_SQL.Append(" = GETDATE(), ");
                Mi_SQL.Append(Parametros.P_Estatus == "CANCELADA" ? Ope_Com_Requisiciones.Campo_Empleado_Cancelada_ID : Ope_Com_Requisiciones.Campo_Empleado_Rechazo_ID);
                Mi_SQL.Append(" = '" + Cls_Sessiones.Empleado_ID + "', ");
                
                Mi_SQL.Append(Ope_Com_Requisiciones.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "', ");
                Mi_SQL.Append(Ope_Com_Requisiciones.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = '" + Parametros.P_Requisicion_ID + "'");
                Cmd.CommandText = Mi_SQL.ToString();
                Cmd.ExecuteNonQuery();

                if (Parametros.P_Estatus == "CANCELADA")
                {
                    Dt_Requisicion = Consultar_Codigo_Programatico_RQ(Parametros.P_Requisicion_ID, Cmd);
                    Partida_ID = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim();
                    Proyecto_ID = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim();
                    Dependencia_ID = Dt_Requisicion.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
                    FF = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                    Num_Reserva = Dt_Requisicion.Rows[0]["NUM_RESERVA"].ToString().Trim();
                    Monto_Anterior = double.Parse(Dt_Requisicion.Rows[0]["TOTAL"].ToString().Trim());
                    Cargo = Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE;
                    Abono = Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO;

                    //Construimos el datatable
                    DataTable Dt_Detalles = new DataTable();
                    Dt_Detalles.Columns.Add("FUENTE_FINANCIAMIENTO_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("PROGRAMA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("DEPENDENCIA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("PARTIDA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("ANIO", typeof(System.String));
                    Dt_Detalles.Columns.Add("IMPORTE", typeof(System.String));
                    DataRow Fila_Nueva = Dt_Detalles.NewRow();

                    Fila_Nueva["FUENTE_FINANCIAMIENTO_ID"] = FF;
                    Fila_Nueva["PROGRAMA_ID"] = Proyecto_ID;
                    Fila_Nueva["DEPENDENCIA_ID"] = Dependencia_ID;
                    Fila_Nueva["PARTIDA_ID"] = Partida_ID;
                    Fila_Nueva["ANIO"] = DateTime.Now.Year;
                    Fila_Nueva["IMPORTE"] = Monto_Anterior;
                    Dt_Detalles.Rows.Add(Fila_Nueva);
                    Dt_Detalles.AcceptChanges();

                    if (Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Dt_Detalles, Cmd) > 0)
                    {
                        Trans.Commit();
                    }
                    else
                    {
                        throw new Exception("No se realizo el movimiento presupuestal.");
                    }
                }
                else
                {
                    Trans.Commit();
                }
            }
            catch (Exception Ex)
            {
                Trans.Rollback();
                String Mensaje = "Error al intentar actualizar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
                Cmd = null;
                Cn = null;
                Trans = null;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Dividir_Requisicion
        ///DESCRIPCIÓN          : Modifica una Requisicion en la base de datos.
        ///PARAMETROS           : Parametros: Contiene el registro que sera modificado.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 12/Enero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Crear_Requisicion(Cls_Ope_Com_Cancelar_Requisiciones_Negocio Clase_Negocio)
        {
            String Mi_SQL;
            DataTable Dt_Requisicion = new DataTable();
            String Partida_ID = "";
            String Proyecto_ID = "";
            String Dependencia_ID = "";
            String FF = "";
            String Num_Reserva = "";
            Double Monto_Anterior = 0;
            String Cargo = "";
            String Abono = "";
            String Lista_Requisiciones = "";
            int query_afectado = 0;
            Double Monto_Total = 0;
            Double Importe = 0;
            Double Monto_IVA = 0;
            Double Total_Cotizado = 0;
            Double Subtotal_Cotizado = 0;
            Double IVA_Cotizado = 0;

             
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            SqlDataAdapter Adapter = new SqlDataAdapter();
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            //Seleccionamos el numero de Proveedores
            Mi_SQL = "SELECT COUNT(DISTINCT(" + Ope_Com_Req_Producto.Campo_Proveedor_ID + "))";
            Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID;
            Mi_SQL = Mi_SQL + " = " + Clase_Negocio.P_Requisicion_ID;
            Cmd.CommandText = Mi_SQL;
            object Proveedores = Cmd.ExecuteScalar();
            int num_Proveedores = int.Parse(Proveedores.ToString());
            DataSet ds = new DataSet();
            //Obtenemos el datatable de los proveedores. 
            Mi_SQL = "SELECT DISTINCT* FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID;
            Mi_SQL = Mi_SQL + " IN (SELECT " + Ope_Com_Req_Producto.Campo_Proveedor_ID;
            Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + "=" + Clase_Negocio.P_Requisicion_ID + ")";
            Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
            Adapter.SelectCommand = Cmd;
            Adapter.Fill(ds);
            DataTable Dt_Proveedores = ds.Tables[0];
            //obtenemos el datatable de la requisicion
            ds = new DataSet();
            Mi_SQL = "SELECT * FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + "=" + Clase_Negocio.P_Requisicion_ID;
            Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
            Adapter.SelectCommand = Cmd;
            Adapter.Fill(ds);            
            DataTable Dt_Requisicion_Padre = ds.Tables[0];
            DataTable Dt_Detalle_Producto = new DataTable();
            //aSIGMANOS LOS VALORES DE LA DEPENDENCIA_ID QUE SE USARA PARA LA RESERVA
            Dependencia_ID = Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString().Trim();

         
            //obtenemos el datatable de los detalles de la requisicion de productos
            ds = new DataSet();
            Mi_SQL = "SELECT * FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + "=" + Clase_Negocio.P_Requisicion_ID;
            Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
            Adapter.SelectCommand = Cmd;
            Adapter.Fill(ds);
            DataTable Dt_Productos = ds.Tables[0];
            //OBTENEMOS LOS DATOS DE LOS DETALLES DEL CODIGO PROGRAMATICO PARA CREAR LA RESERVA
            Partida_ID = Dt_Productos.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim();
            FF = Dt_Productos.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim();
            Proyecto_ID = Dt_Productos.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim();


            //CREAMOS EL DATATABLE PARA LA RESERVA
            DataTable Dt_Detalles = new DataTable();
            

            try
            {
                Int64 Id_Requisicion = Int64.Parse( Obtener_Consecutivo(Ope_Com_Requisiciones.Campo_Requisicion_ID, Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones).ToString());
                
                Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID + "),0) FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                Cmd.CommandText = Mi_SQL;
                Object Obj = Cmd.ExecuteScalar();
                Int64 Ope_Com_Req_Producto_ID = (Convert.ToInt64(Obj) + 1);

                for (int i = 0; i < num_Proveedores; i++)
                {
                    //obtenemos primero los totales. 
                    Monto_Total = Double.Parse(Dt_Productos.Compute("Sum(MONTO_TOTAL)", "PROVEEDOR_ID = " + Dt_Proveedores.Rows[i][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim()).ToString());
                    Importe = Double.Parse(Dt_Productos.Compute("Sum(MONTO)", "PROVEEDOR_ID = " + Dt_Proveedores.Rows[i][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim()).ToString());
                    Monto_IVA = Double.Parse(Dt_Productos.Compute("Sum(MONTO_IVA)", "PROVEEDOR_ID = " + Dt_Proveedores.Rows[i][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim()).ToString());
                    Total_Cotizado = Double.Parse(Dt_Productos.Compute("Sum(TOTAL_COTIZADO)", "PROVEEDOR_ID = " + Dt_Proveedores.Rows[i][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim()).ToString());
                    Subtotal_Cotizado = Double.Parse(Dt_Productos.Compute("Sum(SUBTOTAL_COTIZADO)", "PROVEEDOR_ID = " + Dt_Proveedores.Rows[i][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim()).ToString());
                    IVA_Cotizado = Double.Parse(Dt_Productos.Compute("Sum(IVA_COTIZADO)", "PROVEEDOR_ID = " + Dt_Proveedores.Rows[i][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim()).ToString());


                    //Creamos el datatable del Codigo Programatico
                    //Construimos el datatable
                    Dt_Detalles = new DataTable();
                    Dt_Detalles.Columns.Add("FUENTE_FINANCIAMIENTO_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("PROGRAMA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("DEPENDENCIA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("PARTIDA_ID", typeof(System.String));
                    Dt_Detalles.Columns.Add("ANIO", typeof(System.String));
                    Dt_Detalles.Columns.Add("IMPORTE", typeof(System.String));
                    Dt_Detalles.Columns.Add("CAPITULO_ID", typeof(System.String));
                    DataRow Fila_Nueva = Dt_Detalles.NewRow();

                    Fila_Nueva = Dt_Detalles.NewRow();
                    Fila_Nueva["FUENTE_FINANCIAMIENTO_ID"] = FF;
                    Fila_Nueva["PROGRAMA_ID"] = Proyecto_ID;
                    Fila_Nueva["DEPENDENCIA_ID"] = Dependencia_ID;
                    Fila_Nueva["PARTIDA_ID"] = Partida_ID;
                    Fila_Nueva["ANIO"] = DateTime.Now.Year;
                    Fila_Nueva["IMPORTE"] = Total_Cotizado;

                    //consultamos el capitulo
                    Mi_SQL = "SELECT " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID;
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto;
                    Mi_SQL = Mi_SQL + " JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica;
                    Mi_SQL = Mi_SQL + " ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID;
                    Mi_SQL = Mi_SQL + "=" + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID;
                    Mi_SQL = Mi_SQL + " JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
                    Mi_SQL = Mi_SQL + " ON " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
                    Mi_SQL = Mi_SQL + "=" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
                    Mi_SQL = Mi_SQL + "='" + Partida_ID.Trim() + "'";
                    Cmd.CommandText = Mi_SQL;
                    object capitulo = Cmd.ExecuteScalar();

                    Fila_Nueva["CAPITULO_ID"] = capitulo.ToString().Trim();
                    Dt_Detalles.Rows.Add(Fila_Nueva);
                    Dt_Detalles.AcceptChanges();
                    //Creamos la de la req creada Reserva
                    String No_Reserva = Cls_Ope_Psp_Manejo_Presupuesto.Crear_Reserva
                        (Dependencia_ID, "GENERADA", "",
                        FF,
                        Proyecto_ID,
                        "RQ-" + Id_Requisicion,
                        DateTime.Now.Year.ToString(),
                        Total_Cotizado, "", "", "00007", Dt_Detalles, "RECURSO ASIGNADO", Cmd).ToString();
                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(
                        No_Reserva,
                        Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO,
                        Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE,
                        Total_Cotizado,
                        Dt_Proveedores.Rows[i][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim(),
                        "",
                        "",
                        "", Cmd);

                    Mi_SQL = "INSERT INTO " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                        " (" + Ope_Com_Requisiciones.Campo_Requisicion_ID;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Usuario_Creo].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Codigo_Programatico;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Dependencia_ID;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Folio].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Folio;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Estatus].ToString().Trim() != String.Empty)    
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Estatus;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Tipo].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Tipo;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fase].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Fase;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Usuario_Creo].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Usuario_Creo;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Creo].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Fecha_Creo;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Filtrado_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Empleado_Filtrado_ID;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Filtrado].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Fecha_Filtrado;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Tipo_Articulo].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Tipo_Articulo;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Construccion_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Empleado_Construccion_ID;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Construccion].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Fecha_Construccion;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Generacion_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Empleado_Generacion_ID;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Generacion].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Fecha_Generacion;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Autorizacion_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Empleado_Autorizacion_ID;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Autorizacion].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Cotizacion_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Empleado_Cotizacion_ID;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Cotizacion].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Listado_Almacen].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Listado_Almacen;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Partida_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Partida_ID;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Justificacion_Compra].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Justificacion_Compra;
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Requisicion_Origen_ID;                   
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Cotizador_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Cotizador_ID;
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Transitoria_Stock].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Transitoria_Stock;
                    Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Total;
                    Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Subtotal;
                    Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_IVA;
                    Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Total_Cotizado;
                    Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Subtotal_Cotizado;
                    Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_IVA_Cotizado;
                    Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Num_Reserva;

                    Mi_SQL = Mi_SQL + ") VALUES ('" + Id_Requisicion + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Codigo_Programatico].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Codigo_Programatico].ToString().Trim() + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Codigo_Programatico].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString().Trim() + "',";
                    Mi_SQL = Mi_SQL + "'RQ-" + Id_Requisicion + "',";
                    Mi_SQL = Mi_SQL + "'CONFIRMADA',";
                    Mi_SQL = Mi_SQL + "'TRANSITORIA',";
                    Mi_SQL = Mi_SQL + "'REQUISICION',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Usuario_Creo].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Usuario_Creo].ToString().Trim() + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Creo].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + String.Format("{0:dd/MM/yyyy HH:mm:ss}", Convert.ToDateTime(Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Creo])) + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Filtrado_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Filtrado_ID].ToString().Trim() + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Filtrado].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" +String.Format("{0:dd/MM/yyyy HH:mm:ss}", Convert.ToDateTime(Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Filtrado])) + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Tipo_Articulo].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Tipo_Articulo].ToString().Trim() + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Construccion_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Construccion_ID].ToString().Trim() + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Construccion].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + String.Format("{0:dd/MM/yyyy HH:mm:ss}", Convert.ToDateTime(Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Construccion])) + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Generacion_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Generacion_ID].ToString().Trim() + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Generacion].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + String.Format("{0:dd/MM/yyyy HH:mm:ss}", Convert.ToDateTime(Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Generacion])) + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Autorizacion_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Autorizacion_ID].ToString().Trim() + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Autorizacion].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + String.Format("{0:dd/MM/yyyy HH:mm:ss}", Convert.ToDateTime(Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Autorizacion])) + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Cotizacion_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Empleado_Cotizacion_ID].ToString().Trim() + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Cotizacion].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + String.Format("{0:dd/MM/yyyy HH:mm:ss}", Convert.ToDateTime(Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Cotizacion])) + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Listado_Almacen].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Listado_Almacen].ToString().Trim() + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Partida_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Partida_ID].ToString().Trim() + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Justificacion_Compra].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Justificacion_Compra].ToString().Trim() + "',";

                    Mi_SQL = Mi_SQL + Clase_Negocio.P_Requisicion_ID + ",";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Cotizador_ID].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Cotizador_ID].ToString().Trim() + "',";
                    if (Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Transitoria_Stock].ToString().Trim() != String.Empty)
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisicion_Padre.Rows[0][Ope_Com_Requisiciones.Campo_Transitoria_Stock].ToString().Trim() + "',";

                    Mi_SQL = Mi_SQL + Monto_Total ;
                    Mi_SQL = Mi_SQL + ", " + Importe;
                    Mi_SQL = Mi_SQL + ", " + Monto_IVA;
                    Mi_SQL = Mi_SQL + ", " + Total_Cotizado;
                    Mi_SQL = Mi_SQL + ", " + Subtotal_Cotizado;
                    Mi_SQL = Mi_SQL + ", " + IVA_Cotizado;
                    Mi_SQL = Mi_SQL + ", " + No_Reserva;
                    Mi_SQL = Mi_SQL + ")";

                    Cmd.CommandText = Mi_SQL;
                    query_afectado = Cmd.ExecuteNonQuery();

                    Mi_SQL = "SELECT * FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Req_Producto.Campo_Proveedor_ID;
                    Mi_SQL = Mi_SQL + " ='" + Dt_Proveedores.Rows[i][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim() + "'";
                    Mi_SQL = Mi_SQL + " AND " + Ope_Com_Req_Producto.Campo_Requisicion_ID;
                    Mi_SQL = Mi_SQL + "=" + Clase_Negocio.P_Requisicion_ID;
                    Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Adapter.SelectCommand = Cmd;
                    ds = new DataSet();
                    Adapter.Fill(ds);
                    Clase_Negocio.P_Dt_Productos = new DataTable();
                    Clase_Negocio.P_Dt_Productos = ds.Tables[0];
                    

                    //Insertamos los detalles de la Requisicion

                    for (int y = 0; y < Clase_Negocio.P_Dt_Productos.Rows.Count; y++)
                    {                      

                        //obtenemos los datos del detalle
                        ds = new DataSet();
                        Mi_SQL = "SELECT * FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + "=" + Clase_Negocio.P_Requisicion_ID;
                        Mi_SQL = Mi_SQL + " AND " + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + "='" + Clase_Negocio.P_Dt_Productos.Rows[y][Ope_Com_Req_Producto.Campo_Prod_Serv_ID].ToString().Trim() +"'";
                        Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                        Adapter.SelectCommand = Cmd;
                        Adapter.Fill(ds);
                        Dt_Detalle_Producto = ds.Tables[0];

                        Mi_SQL = "INSERT INTO ";
                        Mi_SQL += Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
                        Mi_SQL += " (" + Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Requisicion_ID;
                        Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Prod_Serv_ID;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Partida_ID;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Cantidad].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Cantidad;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Usuario_Creo].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Usuario_Creo;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Fecha_Creo].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Fecha_Creo;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Precio_Unitario].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Precio_Unitario;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Monto_IVA].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Monto_IVA;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Monto_IEPS].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Monto_IEPS;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Porcentaje_IVA].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Porcentaje_IVA;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Porcentaje_IEPS].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Porcentaje_IEPS;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Importe].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Importe;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Monto_Total].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Monto_Total;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Tipo].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Tipo;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Clave].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Clave;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Nombre_Giro].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Nombre_Giro;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Giro_ID].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Giro_ID;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Proveedor_ID].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Proveedor_ID;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Nombre_Proveedor].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Nombre_Proveedor;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Subtota_Cotizado].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Subtota_Cotizado;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_IVA_Cotizado].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_IVA_Cotizado;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_IEPS_Cotizado].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_IEPS_Cotizado;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Total_Cotizado].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Total_Cotizado;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Empleado_Cotizador_ID].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Empleado_Cotizador_ID;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Especificacion_Producto].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Especificacion_Producto;
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim() != String.Empty)
                            Mi_SQL += ", " + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID;

                        Mi_SQL += " ) VALUES ";
                        Mi_SQL += "('" + Ope_Com_Req_Producto_ID;
                        Mi_SQL += "','" + Id_Requisicion + "'";                      
                        Mi_SQL += ",'" + Clase_Negocio.P_Dt_Productos.Rows[y][Ope_Com_Req_Producto.Campo_Prod_Serv_ID].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim()+"',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Cantidad].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Cantidad].ToString().Trim() + ",";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Usuario_Creo].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Usuario_Creo].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Fecha_Creo].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" +String.Format("{0:dd/MM/yyyy HH:mm:ss}", Convert.ToDateTime( Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Fecha_Creo])) + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Precio_Unitario].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Precio_Unitario].ToString().Trim() + ",";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Monto_IVA].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Monto_IVA].ToString().Trim() + ",";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Monto_IEPS].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Monto_IEPS].ToString().Trim() + ",";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Porcentaje_IVA].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Porcentaje_IVA].ToString().Trim() + ",";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Porcentaje_IEPS].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Porcentaje_IEPS].ToString().Trim() + ",";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Importe].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Importe].ToString().Trim() + ",";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Monto_Total].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Monto_Total].ToString().Trim() + ",";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Tipo].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Tipo].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Clave].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Clave].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Nombre_Giro].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Nombre_Giro].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Giro_ID].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Giro_ID].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Proveedor_ID].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Proveedor_ID].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Nombre_Proveedor].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Nombre_Proveedor].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Subtota_Cotizado].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Subtota_Cotizado].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_IVA_Cotizado].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_IVA_Cotizado].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_IEPS_Cotizado].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_IEPS_Cotizado].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Total_Cotizado].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Total_Cotizado].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Empleado_Cotizador_ID].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Empleado_Cotizador_ID].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado].ToString().Trim() + "',";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + "'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim() + "'";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Especificacion_Producto].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + ",'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Especificacion_Producto].ToString().Trim() + "'";
                        if (Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim() != String.Empty)
                            Mi_SQL = Mi_SQL + ",'" + Dt_Detalle_Producto.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ")";
                        Cmd.CommandText = Mi_SQL;
                        query_afectado = Cmd.ExecuteNonQuery();
                        //aumentamos el consecutivo de la tabla Ope_Com_Req_Producto
                        Ope_Com_Req_Producto_ID = Ope_Com_Req_Producto_ID + 1;
                    }
                    //aumentamos el consecutivo de la requisicion
                    Id_Requisicion = Id_Requisicion + 1;
                    if (i != num_Proveedores - 1)
                    {
                        Lista_Requisiciones = Lista_Requisiciones + Id_Requisicion + ",";
                    }
                    else
                    {
                        Lista_Requisiciones = Lista_Requisiciones + Id_Requisicion + "";
                    }
                
                //Seleccionamos el numero del Historial
                Mi_SQL = "SELECT MAX(" + Ope_Com_Historial_Req.Campo_No_Historial + ")";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Historial_Req.Tabla_Ope_Com_Historial_Req;               
                Cmd.CommandText = Mi_SQL;
                object historico = Cmd.ExecuteScalar();
                Double num_historico = Double.Parse(historico.ToString());
                num_historico = num_historico + 1;
                //Insertamos el historico de la requisicion 

                Mi_SQL = "INSERT INTO " + Ope_Com_Historial_Req.Tabla_Ope_Com_Historial_Req + " (" +
                   Ope_Com_Historial_Req.Campo_No_Historial + "," +
                   Ope_Com_Historial_Req.Campo_No_Requisicion + "," +
                   Ope_Com_Historial_Req.Campo_Estatus + "," +
                   Ope_Com_Historial_Req.Campo_Fecha + "," +
                   Ope_Com_Historial_Req.Campo_Empleado + "," +
                   Ope_Com_Historial_Req.Campo_Usuario_Creo + "," +
                   Ope_Com_Historial_Req.Campo_Fecha_Creo + ") VALUES (" +
                   num_historico + ", " +
                   Id_Requisicion + ", 'DIVIDIDA', " +
                   "GETDATE(), '" +
                   Cls_Sessiones.Nombre_Empleado + "', '" +
                   Cls_Sessiones.Nombre_Empleado + "', " +
                   "GETDATE())";
                Cmd.CommandText = Mi_SQL;
                query_afectado = Cmd.ExecuteNonQuery();
             

                }//fin del for del proveedor. 



                //Actualizamos el estatus de la req padre. 
                Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL = Mi_SQL + " SET " + Ope_Com_Requisiciones.Campo_Estatus;
                Mi_SQL = Mi_SQL + "='DIVIDIDA CERRADA'";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID;
                Mi_SQL = Mi_SQL + " = " + Clase_Negocio.P_Requisicion_ID;
                Cmd.CommandText = Mi_SQL;
                query_afectado = Cmd.ExecuteNonQuery();

                //Actualizamos el estatus de la reserva de la req padre. 
                Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                Mi_SQL = Mi_SQL + " SET " + Ope_Psp_Reservas.Campo_Estatus;
                Mi_SQL = Mi_SQL + "='DIVIDIDA'";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva;
                Mi_SQL = Mi_SQL + " = (SELECT " + Ope_Com_Requisiciones.Campo_Num_Reserva;
                Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Clase_Negocio.P_Requisicion_ID + ")";
                Cmd.CommandText = Mi_SQL;
                query_afectado = Cmd.ExecuteNonQuery();
                

                 //Ejecutar transaccion
                Trans.Commit();
                //Actualizamos los totales
                   
                return Lista_Requisiciones;
            }
            catch (Exception Ex)
            {
                Trans.Rollback();
                String Mensaje = "Error al intentar actualizar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
                Cmd = null;
                Cn = null;
                Trans = null;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Codigo_Programatico_RQ
        ///DESCRIPCIÓN          : Metodo que ejecuta la sentencia SQL para modificar una requisicion
        ///PARAMETROS           :
        ///CREO                 : Susana Trigueros Armenta
        ///FECHA_CREO           : 10/Noviembre/2010 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Codigo_Programatico_RQ(String Requisicion_ID, SqlCommand P_Comando)
        {
            String Mi_SQL = "";
            DataTable Dt_Requisicion = new DataTable();

            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;

            try
            {
                if (P_Comando != null)
                {
                    Cmd = P_Comando;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Transaction = Trans;
                    Cmd.Connection = Cn;
                }

                Mi_SQL = "SELECT REQ_DET." + Ope_Com_Req_Producto.Campo_Partida_ID +
                                 ",REQ_DET." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID +
                                 ", (SELECT " + Ope_Com_Requisiciones.Campo_Total +
                                 " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                                 " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                                 "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS TOTAL" +
                                 ", (SELECT " + Ope_Com_Requisiciones.Campo_Total_Cotizado +
                                 " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                                 " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                                 "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS TOTAL_COTIZADO" +
                                 ", (SELECT " + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                                 " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                                 " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                                 "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS DEPENDENCIA_ID" +
                                 ", REQ_DET." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID +
                                 ", (SELECT NUM_RESERVA" +
                                 " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                                 " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                                 "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS NUM_RESERVA" +
                                 " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_DET" +
                                 " WHERE REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + "='" + Requisicion_ID + "'";

                Cmd.CommandText = Mi_SQL;
                SqlDataAdapter Da_Datos = new SqlDataAdapter(Cmd);
                DataSet Ds_Datos = new DataSet();
                Da_Datos.Fill(Ds_Datos);

                if (Ds_Datos != null && Ds_Datos.Tables.Count > 0)
                {
                    Dt_Requisicion = Ds_Datos.Tables[0];
                }
                if (P_Comando == null)
                {
                    Trans.Commit();//aceptamos los cambios
                }
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                String Mensaje = "Error:  [" + Ex.Message + "]";
                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                if (P_Comando == null)
                {
                    Cn.Close();
                    Cmd = null;
                    Cn = null;
                    Trans = null;
                }
            }
            return Dt_Requisicion;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Consecutivo
        ///DESCRIPCIÓN          : Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS           : 1.-Campo del cual se obtendra el consecutivo
        ///                       2.-Nombre de la tabla
        ///CREO                 : Gustavo Angeles Cruz
        ///FECHA_CREO           : 10/Enero/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'0') FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }
    }
}
