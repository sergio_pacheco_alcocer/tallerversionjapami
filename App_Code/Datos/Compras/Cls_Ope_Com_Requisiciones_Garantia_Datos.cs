﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Requisiciones_Garantia.Negocio;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Text;
using JAPAMI.Constantes;

/// <summary>
/// Summary description for Cls_Ope_Com_Requisiciones_Garantia_Datos
/// </summary>
/// 
namespace JAPAMI.Requisiciones_Garantia.Datos{
    public class Cls_Ope_Com_Requisiciones_Garantia_Datos   {

        #region Metodos

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Consultar_Dependencias
            /// DESCRIPCIÓN: Consultar_Dependencias
            /// RETORNA: 
            /// CREO: Francisco A. Gallardo Castañeda
            /// FECHA_CREO: Mayo 2013
            /// MODIFICO:
            /// FECHA_MODIFICO
            /// CAUSA_MODIFICACIÓN   
            /// *******************************************************************************/
            internal static DataTable Consultar_Dependencias(Cls_Ope_Com_Requisiciones_Garantia_Negocio Parametros)
            {
                DataTable Dt_Resultado = new DataTable();
                DataSet Ds_Resultado = new DataSet();
                try
                {
                    StringBuilder Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + " AS DEPENDENCIA_ID");
                    Mi_SQL.Append(", LTRIM(RTRIM(DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + ")) AS CLAVE_DEPENDENCIA");
                    Mi_SQL.Append(", LTRIM(RTRIM(DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + ")) AS NOMBRE_DEPENDENCIA");
                    Mi_SQL.Append(", LTRIM(RTRIM(DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + ")) + ' - ' + LTRIM(RTRIM(DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + ")) AS CLAVE_NOMBRE_DEPENDENCIA");
                    Mi_SQL.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS");
                    if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                        Mi_SQL.Append(" WHERE DEPENDENCIAS." + Cat_Dependencias.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')");
                    Mi_SQL.Append("ORDER BY CLAVE_NOMBRE_DEPENDENCIA");
                    Ds_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString().Trim());
                    if (Ds_Resultado != null)
                        if (Ds_Resultado.Tables.Count > 0)
                            Dt_Resultado = Ds_Resultado.Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception(Ex.Message);
                }
                return Dt_Resultado;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Consultar_Dependencias_Empleado
            /// DESCRIPCIÓN: Consultar_Dependencias_Empleado
            /// RETORNA: 
            /// CREO: Francisco A. Gallardo Castañeda
            /// FECHA_CREO: Mayo 2013
            /// MODIFICO:
            /// FECHA_MODIFICO
            /// CAUSA_MODIFICACIÓN   
            /// *******************************************************************************/
            internal static DataTable Consultar_Dependencias_Empleado(Cls_Ope_Com_Requisiciones_Garantia_Negocio Parametros)
            {
                DataTable Dt_Resultado = new DataTable();
                DataSet Ds_Resultado = new DataSet();
                Boolean Entro_Where = false;
                try
                {
                    StringBuilder Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + " AS DEPENDENCIA_ID");
                    Mi_SQL.Append(", LTRIM(RTRIM(DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + ")) AS CLAVE_DEPENDENCIA");
                    Mi_SQL.Append(", LTRIM(RTRIM(DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + ")) AS NOMBRE_DEPENDENCIA");
                    Mi_SQL.Append(", LTRIM(RTRIM(DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + ")) + ' - ' + LTRIM(RTRIM(DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + ")) AS CLAVE_NOMBRE_DEPENDENCIA");
                    Mi_SQL.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS");
                    Mi_SQL.Append(" JOIN " + Cat_Det_Empleado_UR.Tabla_Cat_Det_Empleado_UR + " DEP_EMPLEADO");
                    Mi_SQL.Append(" ON DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + " = DEP_EMPLEADO." + Cat_Det_Empleado_UR.Campo_Dependencia_ID);
                    if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                    {
                        if (!Entro_Where) { Mi_SQL.Append(" WHERE "); Entro_Where = true; } else Mi_SQL.Append(" AND ");
                        Mi_SQL.Append("DEPENDENCIAS." + Cat_Dependencias.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')");
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Empleado_ID))
                    {
                        if (!Entro_Where) { Mi_SQL.Append(" WHERE "); Entro_Where = true; } else Mi_SQL.Append(" AND ");
                        Mi_SQL.Append("DEP_EMPLEADO." + Cat_Det_Empleado_UR.Campo_Empleado_ID + " IN ('" + Parametros.P_Empleado_ID + "')");
                    }
                    Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE_DEPENDENCIA");
                    Ds_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString().Trim());
                    if (Ds_Resultado != null)
                        if (Ds_Resultado.Tables.Count > 0)
                            Dt_Resultado = Ds_Resultado.Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception(Ex.Message);
                }
                return Dt_Resultado;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Consultar_Requisiciones
            /// DESCRIPCIÓN: Consultar_Requisiciones
            /// RETORNA: 
            /// CREO: Francisco A. Gallardo Castañeda
            /// FECHA_CREO: Mayo 2013
            /// MODIFICO:
            /// FECHA_MODIFICO
            /// CAUSA_MODIFICACIÓN   
            /// *******************************************************************************/
            internal static DataTable Consultar_Requisiciones(Cls_Ope_Com_Requisiciones_Garantia_Negocio Parametros)
            {
                DataTable Dt_Resultado = new DataTable();
                DataSet Ds_Resultado = new DataSet();
                Boolean Entro_Where = false;
                try
                {
                    StringBuilder Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT " + Ope_Com_Requisiciones.Campo_Requisicion_ID + "");
                    Mi_SQL.Append(", " + Ope_Com_Requisiciones.Campo_Partida_ID + "");
                    Mi_SQL.Append(" FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "");
                    Mi_SQL.Append(" WHERE " + Ope_Com_Requisiciones.Campo_Tipo_Articulo + " = 'SERVICIO'"); Entro_Where = true;
                    if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                    {
                        if (!Entro_Where) { Mi_SQL.Append(" WHERE "); Entro_Where = true; } else Mi_SQL.Append(" AND ");
                        Mi_SQL.Append(Ope_Com_Requisiciones.Campo_Dependencia_ID + " IN ('" + Parametros.P_Dependencia_ID.Trim() + "')");
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_No_Requisicion))
                    {
                        if (!Entro_Where) { Mi_SQL.Append(" WHERE "); Entro_Where = true; } else Mi_SQL.Append(" AND ");
                        Mi_SQL.Append(Ope_Com_Requisiciones.Campo_Requisicion_ID + " IN ('" + Parametros.P_No_Requisicion.Trim() + "')");
                    }
                    Ds_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString().Trim());
                    if (Ds_Resultado != null)
                        if (Ds_Resultado.Tables.Count > 0)
                            Dt_Resultado = Ds_Resultado.Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception(Ex.Message);
                }
                return Dt_Resultado;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Consultar_Servicios_Requisicion
            /// DESCRIPCIÓN: Consultar_Servicios_Requisicion
            /// RETORNA: 
            /// CREO: Francisco A. Gallardo Castañeda
            /// FECHA_CREO: Mayo 2013
            /// MODIFICO:
            /// FECHA_MODIFICO
            /// CAUSA_MODIFICACIÓN   
            /// *******************************************************************************/
            internal static DataTable Consultar_Servicios_Requisicion(Cls_Ope_Com_Requisiciones_Garantia_Negocio Parametros)
            {
                DataTable Dt_Resultado = new DataTable();
                DataSet Ds_Resultado = new DataSet();
                Boolean Entro_Where = false;
                try
                {
                    StringBuilder Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT REQ_SERVICIOS." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " AS SERVICIO_ID");
                    Mi_SQL.Append(", SERVICIOS." + Cat_Com_Servicios.Campo_Clave + " AS CLAVE_SERVICIO");
                    Mi_SQL.Append(", SERVICIOS." + Cat_Com_Servicios.Campo_Nombre + " AS NOMBRE");
                    Mi_SQL.Append(", REQ_SERVICIOS." + Ope_Com_Req_Producto.Campo_Cantidad + " AS CANTIDAD");
                    Mi_SQL.Append(", REQ_SERVICIOS." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " AS NO_REQUISICION");
                    Mi_SQL.Append(", REQ_SERVICIOS." + Ope_Com_Req_Producto.Campo_Especificacion_Producto + " AS ESPECIFICACION_ADICIONAL");
                    Mi_SQL.Append(" FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_SERVICIOS");
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + " SERVICIOS ON REQ_SERVICIOS." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = SERVICIOS." + Cat_Com_Servicios.Campo_Servicio_ID + "");
                    Mi_SQL.Append(" WHERE REQ_SERVICIOS." + Ope_Com_Req_Producto.Campo_Tipo + " = 'SERVICIO'"); Entro_Where = true;
                    if (!String.IsNullOrEmpty(Parametros.P_No_Requisicion))
                    {
                        if (!Entro_Where) { Mi_SQL.Append(" WHERE "); Entro_Where = true; } else Mi_SQL.Append(" AND ");
                        Mi_SQL.Append("REQ_SERVICIOS." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " IN ('" + Parametros.P_No_Requisicion.Trim() + "')");
                    }
                    Ds_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString().Trim());
                    if (Ds_Resultado != null)
                        if (Ds_Resultado.Tables.Count > 0)
                            Dt_Resultado = Ds_Resultado.Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception(Ex.Message);
                }
                return Dt_Resultado;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Consultar_Bienes_Requisicion
            /// DESCRIPCIÓN: Consultar_Bienes_Requisicion
            /// RETORNA: 
            /// CREO: Francisco A. Gallardo Castañeda
            /// FECHA_CREO: Mayo 2013
            /// MODIFICO:
            /// FECHA_MODIFICO
            /// CAUSA_MODIFICACIÓN   
            /// *******************************************************************************/
            internal static DataTable Consultar_Bienes_Requisicion(Cls_Ope_Com_Requisiciones_Garantia_Negocio Requisiciones_Negocio)
            {
                DataTable Dt_Datos = new DataTable();
                try
                {
                    String Mi_SQL = "SELECT REQUISICIONES_BIENES." + Ope_Com_Req_Bienes.Campo_Bien_Mueble_ID + " AS BIEN_MUEBLE_ID";
                    Mi_SQL = Mi_SQL + ", STR(BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + ") AS NUMERO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Nombre + " AS NOMBRE";
                    Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie + " AS NUMERO_SERIE";
                    Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Modelo + " AS MODELO";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Req_Bienes.Tabla_Ope_Com_Req_Bienes + " REQUISICIONES_BIENES";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " BIENES_MUEBLES";
                    Mi_SQL = Mi_SQL + " ON REQUISICIONES_BIENES." + Ope_Com_Req_Bienes.Campo_Bien_Mueble_ID + " = BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID;
                    Mi_SQL = Mi_SQL + " WHERE REQUISICIONES_BIENES." + Ope_Com_Req_Bienes.Campo_Requisicion_ID + " = '" + Requisiciones_Negocio.P_No_Requisicion + "'";
                    DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Ds_Datos != null)
                    {
                        if (Ds_Datos.Tables.Count > 0)
                        {
                            Dt_Datos = Ds_Datos.Tables[0];
                        }
                    }
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Excepción al consultar los Bienes de la Requisición: [" + Ex.Message + "]");
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Alta_Requisicion_Garantia
            /// DESCRIPCIÓN: Alta_Requisicion_Garantia
            /// RETORNA: 
            /// CREO: Francisco A. Gallardo Castañeda
            /// FECHA_CREO: Mayo 2013
            /// MODIFICO:
            /// FECHA_MODIFICO
            /// CAUSA_MODIFICACIÓN   
            /// *******************************************************************************/
            internal static Cls_Ope_Com_Requisiciones_Garantia_Negocio Alta_Requisicion_Garantia(Cls_Ope_Com_Requisiciones_Garantia_Negocio Parametros)
            {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try
                {
                    Parametros.P_No_Requisicion = Convert.ToInt64(Obtener_ID_Consecutivo(Ope_Com_Requisiciones_Garantia.Tabla_Ope_Com_Requisiciones_Garantia, Ope_Com_Requisiciones_Garantia.Campo_No_Requisicion, 10)).ToString();
                    Int64 No_Registro = Convert.ToInt64(Obtener_ID_Consecutivo(Ope_Com_Historial_Req_Garantia.Tabla_Ope_Com_Historial_Req_Garantia, Ope_Com_Historial_Req_Garantia.Campo_No_Registro, 10));
                    String Mi_SQL = "INSERT INTO " + Ope_Com_Requisiciones_Garantia.Tabla_Ope_Com_Requisiciones_Garantia + "";
                    Mi_SQL += " (" + Ope_Com_Requisiciones_Garantia.Campo_No_Requisicion;
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Folio_Requisicion;
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Estatus;
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Fecha_Elaboracion;
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Dependencia_ID;
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Requisicion_Ref;
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Justificacion;
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Usuario_Creo;
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Fecha_Creo;
                    if (Parametros.P_Estatus.Trim().Equals("EN CONSTRUCCION"))
                        Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Empleado_Construccion_ID;
                    if (Parametros.P_Estatus.Trim().Equals("GENERADA"))
                        Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Empleado_Genero_ID;
                    Mi_SQL += ") VALUES ";
                    Mi_SQL += " ('" + Parametros.P_No_Requisicion + "'";
                    Mi_SQL += ", 'RQG-" + Parametros.P_No_Requisicion + "'";
                    Mi_SQL += ", '" + Parametros.P_Estatus + "'";
                    Mi_SQL += ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Elaboracion) + "'";
                    Mi_SQL += ", '" + Parametros.P_Dependencia_ID + "'";
                    Mi_SQL += ", '" + Parametros.P_No_Requisicion_Referencia + "'";
                    Mi_SQL += ", '" + Parametros.P_Justificacion + "'";
                    Mi_SQL += ", '" + Parametros.P_Nombre_Usuario + "'";
                    Mi_SQL += ", GETDATE()";
                    Mi_SQL += ", '" + Parametros.P_Usuario_ID + "'";
                    Mi_SQL += ")";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Mi_SQL = "INSERT INTO " + Ope_Com_Historial_Req_Garantia.Tabla_Ope_Com_Historial_Req_Garantia + "";
                    Mi_SQL += " (" + Ope_Com_Historial_Req_Garantia.Campo_No_Registro;
                    Mi_SQL += ", " + Ope_Com_Historial_Req_Garantia.Campo_No_Requisicion;
                    Mi_SQL += ", " + Ope_Com_Historial_Req_Garantia.Campo_Estatus;
                    Mi_SQL += ", " + Ope_Com_Historial_Req_Garantia.Campo_Empleado_ID;
                    Mi_SQL += ", " + Ope_Com_Historial_Req_Garantia.Campo_Usuario_Creo;
                    Mi_SQL += ", " + Ope_Com_Historial_Req_Garantia.Campo_Fecha_Creo;
                    Mi_SQL += ") VALUES ";
                    Mi_SQL += " ('" + No_Registro + "'";
                    Mi_SQL += ", '" + Parametros.P_No_Requisicion + "'";
                    Mi_SQL += ", '" + Parametros.P_Estatus + "'";
                    Mi_SQL += ", '" + Parametros.P_Usuario_ID + "'";
                    Mi_SQL += ", '" + Parametros.P_Nombre_Usuario + "'";
                    Mi_SQL += ", GETDATE()";
                    Mi_SQL += ")";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Trans.Commit();

                }
                catch (SqlException Ex)
                {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152)
                    {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 2627)
                    {
                        if (Ex.Message.IndexOf("PRIMARY") != -1)
                        {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        }
                        else if (Ex.Message.IndexOf("UNIQUE") != -1)
                        {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        }
                        else
                        {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    }
                    else if (Ex.Number == 547)
                    {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 515)
                    {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error al intentar dar de Alta un Color. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                }
                finally
                {
                    Cn.Close();
                }
                return Parametros;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Alta_Requisicion_Garantia
            /// DESCRIPCIÓN: Alta_Requisicion_Garantia
            /// RETORNA: 
            /// CREO: Francisco A. Gallardo Castañeda
            /// FECHA_CREO: Mayo 2013
            /// MODIFICO:
            /// FECHA_MODIFICO
            /// CAUSA_MODIFICACIÓN   
            /// *******************************************************************************/
            internal static void Modifica_Requisicion_Garantia(Cls_Ope_Com_Requisiciones_Garantia_Negocio Parametros)
            {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try
                {
                    Int64 No_Registro = Convert.ToInt64(Obtener_ID_Consecutivo(Ope_Com_Historial_Req_Garantia.Tabla_Ope_Com_Historial_Req_Garantia, Ope_Com_Historial_Req_Garantia.Campo_No_Registro, 10));
                    Int64 No_Registro_Comentario = Convert.ToInt64(Obtener_ID_Consecutivo(Ope_Com_Req_Gar_Comentarios.Tabla_Ope_Com_Req_Gar_Comentarios, Ope_Com_Req_Gar_Comentarios.Campo_No_Registro, 10));
                    String Mi_SQL = "UPDATE " + Ope_Com_Requisiciones_Garantia.Tabla_Ope_Com_Requisiciones_Garantia + "";
                    Mi_SQL += " SET " + Ope_Com_Requisiciones_Garantia.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Justificacion + " = '" + Parametros.P_Justificacion + "'";
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Ultimo_Comentario + " = '" + Parametros.P_Comentarios + "'";
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Proveedor_ID + " = '" + Parametros.P_Proveedor + "'";
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Diagnostico + " = '" + Parametros.P_Diagnositco + "'";
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Resultado + " = '" + Parametros.P_Resultado + "'";
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Usuario_Modifico + " = '" + Parametros.P_Nombre_Usuario + "'";
                    Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Fecha_Modifico + " = GETDATE()"; ;
                    if (Parametros.P_Estatus.Trim().Equals("EN CONSTRUCCION"))
                        Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Empleado_Construccion_ID + " = '" + Parametros.P_Usuario_ID + "'";
                    if (Parametros.P_Estatus.Trim().Equals("GENERADA"))
                        Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Empleado_Genero_ID + " = '" + Parametros.P_Usuario_ID + "'";
                    if (Parametros.P_Estatus.Trim().Equals("AUTORIZADA"))
                        Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Empleado_Autorizo_ID + " = '" + Parametros.P_Usuario_ID + "'";
                    if (Parametros.P_Estatus.Trim().Equals("CANCELADA"))
                        Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Empleado_Cancelo_ID + " = '" + Parametros.P_Usuario_ID + "'";
                    if (Parametros.P_Estatus.Trim().Equals("RECHAZADA"))
                        Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Empleado_Rechazo_ID + " = '" + Parametros.P_Usuario_ID + "'";
                    if (Parametros.P_Estatus.Trim().Equals("ENVIAR A PROVEEDOR"))
                        Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Empleado_Termino_ID + " = '" + Parametros.P_Usuario_ID + "'";
                    if (Parametros.P_Estatus.Trim().Equals("TERMINADA"))
                        Mi_SQL += ", " + Ope_Com_Requisiciones_Garantia.Campo_Empleado_Termino_ID + " = '" + Parametros.P_Usuario_ID + "'";
                    Mi_SQL += " WHERE " + Ope_Com_Requisiciones_Garantia.Campo_No_Requisicion + " = '" + Parametros.P_No_Requisicion + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Mi_SQL = "INSERT INTO " + Ope_Com_Historial_Req_Garantia.Tabla_Ope_Com_Historial_Req_Garantia + "";
                    Mi_SQL += " (" + Ope_Com_Historial_Req_Garantia.Campo_No_Registro;
                    Mi_SQL += ", " + Ope_Com_Historial_Req_Garantia.Campo_No_Requisicion;
                    Mi_SQL += ", " + Ope_Com_Historial_Req_Garantia.Campo_Estatus;
                    Mi_SQL += ", " + Ope_Com_Historial_Req_Garantia.Campo_Empleado_ID;
                    Mi_SQL += ", " + Ope_Com_Historial_Req_Garantia.Campo_Usuario_Creo;
                    Mi_SQL += ", " + Ope_Com_Historial_Req_Garantia.Campo_Fecha_Creo;
                    Mi_SQL += ") VALUES ";
                    Mi_SQL += " ('" + No_Registro + "'";
                    Mi_SQL += ", '" + Parametros.P_No_Requisicion + "'";
                    Mi_SQL += ", '" + Parametros.P_Estatus + "'";
                    Mi_SQL += ", '" + Parametros.P_Usuario_ID + "'";
                    Mi_SQL += ", '" + Parametros.P_Nombre_Usuario + "'";
                    Mi_SQL += ", GETDATE()";
                    Mi_SQL += ")";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    if (!String.IsNullOrEmpty(Parametros.P_Comentarios)) {
                        Mi_SQL = "INSERT INTO " + Ope_Com_Req_Gar_Comentarios.Tabla_Ope_Com_Req_Gar_Comentarios + "";
                        Mi_SQL += " (" + Ope_Com_Req_Gar_Comentarios.Campo_No_Registro;
                        Mi_SQL += ", " + Ope_Com_Req_Gar_Comentarios.Campo_No_Requisicion;
                        Mi_SQL += ", " + Ope_Com_Req_Gar_Comentarios.Campo_Estatus;
                        Mi_SQL += ", " + Ope_Com_Req_Gar_Comentarios.Campo_Comentario;
                        Mi_SQL += ", " + Ope_Com_Req_Gar_Comentarios.Campo_Empleado_Comento_ID;
                        Mi_SQL += ", " + Ope_Com_Req_Gar_Comentarios.Campo_Usuario_Creo;
                        Mi_SQL += ", " + Ope_Com_Req_Gar_Comentarios.Campo_Fecha_Creo;
                        Mi_SQL += ") VALUES ";
                        Mi_SQL += " ('" + No_Registro_Comentario + "'";
                        Mi_SQL += ", '" + Parametros.P_No_Requisicion + "'";
                        Mi_SQL += ", '" + Parametros.P_Estatus + "'";
                        Mi_SQL += ", '" + Parametros.P_Comentarios + "'";
                        Mi_SQL += ", '" + Parametros.P_Usuario_ID + "'";
                        Mi_SQL += ", '" + Parametros.P_Nombre_Usuario + "'";
                        Mi_SQL += ", GETDATE()";
                        Mi_SQL += ")";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                    Trans.Commit();

                }
                catch (SqlException Ex)
                {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152)
                    {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 2627)
                    {
                        if (Ex.Message.IndexOf("PRIMARY") != -1)
                        {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        }
                        else if (Ex.Message.IndexOf("UNIQUE") != -1)
                        {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        }
                        else
                        {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    }
                    else if (Ex.Number == 547)
                    {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 515)
                    {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error al intentar dar de Alta un Color. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                }
                finally
                {
                    Cn.Close();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
            ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
            ///PARAMETROS:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Marzo/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID)
            {
                String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
                try
                {
                    String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                    Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                    {
                        Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                    }
                }
                catch (SqlException Ex)
                {
                    new Exception(Ex.Message);
                }
                return Id;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
            ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
            ///PARAMETROS:     
            ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
            ///             2. Longitud_ID. Longitud que tendra el ID. 
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Marzo/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
            {
                String Retornar = "";
                String Dato = "" + Dato_ID;
                for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
                {
                    Retornar = Retornar + "0";
                }
                Retornar = Retornar + Dato;
                return Retornar;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Consultar_Requisiciones_Garantia
            /// DESCRIPCIÓN: Consultar_Requisiciones_Garantia
            /// RETORNA: 
            /// CREO: Francisco A. Gallardo Castañeda
            /// FECHA_CREO: Mayo 2013
            /// MODIFICO:
            /// FECHA_MODIFICO
            /// CAUSA_MODIFICACIÓN   
            /// *******************************************************************************/
            internal static DataTable Consultar_Requisiciones_Garantia(Cls_Ope_Com_Requisiciones_Garantia_Negocio Parametros)
            {
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try
                {
                    String Mi_SQL = "SELECT REQ_GARANTIA." + Ope_Com_Requisiciones_Garantia.Campo_No_Requisicion + " AS NO_REQUISICION";
                    Mi_SQL = Mi_SQL + ", REQ_GARANTIA." + Ope_Com_Requisiciones_Garantia.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                    Mi_SQL = Mi_SQL + ", REQ_GARANTIA." + Ope_Com_Requisiciones_Garantia.Campo_Folio_Requisicion + " AS FOLIO";
                    Mi_SQL = Mi_SQL + ", REQ_GARANTIA." + Ope_Com_Requisiciones_Garantia.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL = Mi_SQL + ", (LTRIM(RTRIM(DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "))+' - '+ LTRIM(RTRIM(DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + "))) AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio + " AS REQUISICION_REFERENCIADA";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Requisiciones_Garantia.Tabla_Ope_Com_Requisiciones_Garantia + " REQ_GARANTIA";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON REQ_GARANTIA." + Ope_Com_Requisiciones_Garantia.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICIONES ON REQ_GARANTIA." + Ope_Com_Requisiciones_Garantia.Campo_Requisicion_Ref + " = REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + "";
                    if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID)) {
                        if (!Entro_Where) { Mi_SQL += " WHERE "; Entro_Where = true; } else { Mi_SQL += " AND "; }
                        Mi_SQL += "REQ_GARANTIA." + Ope_Com_Requisiciones_Garantia.Campo_Dependencia_ID + " IN ('" + Parametros.P_Dependencia_ID + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                    {
                        if (!Entro_Where) { Mi_SQL += " WHERE "; Entro_Where = true; } else { Mi_SQL += " AND "; }
                        Mi_SQL += "REQ_GARANTIA." + Ope_Com_Requisiciones_Garantia.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')"; ;
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Folio))
                    {
                        if (!Entro_Where) { Mi_SQL += " WHERE "; Entro_Where = true; } else { Mi_SQL += " AND "; }
                        Mi_SQL += "REQ_GARANTIA." + Ope_Com_Requisiciones_Garantia.Campo_Folio_Requisicion + " LIKE '%" + Parametros.P_Folio + "%'";
                    }
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Inicial).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))
                    {
                        if (!Entro_Where) { Mi_SQL += " WHERE "; Entro_Where = true; } else { Mi_SQL += " AND "; }
                        Mi_SQL += "REQ_GARANTIA." + Ope_Com_Requisiciones_Garantia.Campo_Fecha_Elaboracion + " >= '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Inicial) + "'";
                    }
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Final).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))
                    {
                        if (!Entro_Where) { Mi_SQL += " WHERE "; Entro_Where = true; } else { Mi_SQL += " AND "; }
                        Mi_SQL += "REQ_GARANTIA." + Ope_Com_Requisiciones_Garantia.Campo_Fecha_Elaboracion + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Final.AddDays(1)) + "'";
                    }
                    DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Ds_Datos != null)
                    {
                        if (Ds_Datos.Tables.Count > 0)
                        {
                            Dt_Datos = Ds_Datos.Tables[0];
                        }
                    }
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Excepción al consultar los Bienes de la Requisición: [" + Ex.Message + "]");
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Consultar_Proveedores_Requisiciones_Garantia
            /// DESCRIPCIÓN: Consultar_Proveedores_Requisiciones_Garantia
            /// RETORNA: 
            /// CREO: Francisco A. Gallardo Castañeda
            /// FECHA_CREO: Mayo 2013
            /// MODIFICO:
            /// FECHA_MODIFICO
            /// CAUSA_MODIFICACIÓN   
            /// *******************************************************************************/
            internal static DataTable Consultar_Proveedores_Requisiciones_Garantia(Cls_Ope_Com_Requisiciones_Garantia_Negocio Parametros)
            {
                DataTable Dt_Datos = new DataTable();
                try
                {
                    String Mi_SQL = "SELECT PROPUESTA." + Ope_Com_Propuesta_Cotizacion.Campo_Proveedor_ID + " AS PROVEEDOR_ID";
                    Mi_SQL = Mi_SQL + ", (LTRIM(RTRIM(PROVEEDORES." + Cat_Com_Proveedores.Campo_Compañia + ")) + ' / ' + LTRIM(RTRIM(PROVEEDORES." + Cat_Com_Proveedores.Campo_Nombre + "))) AS PROVEEDOR";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Propuesta_Cotizacion.Tabla_Ope_Com_Propuesta_Cotizacion + " PROPUESTA";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDORES ON PROPUESTA." + Ope_Com_Propuesta_Cotizacion.Campo_Proveedor_ID + " = PROVEEDORES." + Cat_Com_Proveedores.Campo_Proveedor_ID + "";
                    Mi_SQL = Mi_SQL + " WHERE PROPUESTA." + Ope_Com_Propuesta_Cotizacion.Campo_No_Requisicion + " = '" + Parametros.P_No_Requisicion_Referencia.Trim() + "' AND PROPUESTA." + Ope_Com_Propuesta_Cotizacion.Campo_Resultado + " = 'ACEPTADA'";
                    DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Ds_Datos != null)
                    {
                        if (Ds_Datos.Tables.Count > 0)
                        {
                            Dt_Datos = Ds_Datos.Tables[0];
                        }
                    }
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Excepción al consultar los Bienes de la Requisición: [" + Ex.Message + "]");
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Consultar_Detalle_Requisicion_Garantia
            /// DESCRIPCIÓN: Consultar_Detalle_Requisicion_Garantia
            /// RETORNA: 
            /// CREO: Francisco A. Gallardo Castañeda
            /// FECHA_CREO: Mayo 2013
            /// MODIFICO:
            /// FECHA_MODIFICO
            /// CAUSA_MODIFICACIÓN   
            /// *******************************************************************************/
            internal static Cls_Ope_Com_Requisiciones_Garantia_Negocio Consultar_Detalle_Requisicion_Garantia(Cls_Ope_Com_Requisiciones_Garantia_Negocio Parametros)
            {
                String Mi_SQL = null;
                Cls_Ope_Com_Requisiciones_Garantia_Negocio Obj_Cargado = new Cls_Ope_Com_Requisiciones_Garantia_Negocio();
                try
                {
                    Mi_SQL = "SELECT * FROM " + Ope_Com_Requisiciones_Garantia.Tabla_Ope_Com_Requisiciones_Garantia + " WHERE " + Ope_Com_Requisiciones_Garantia.Campo_No_Requisicion + " = '" + Parametros.P_No_Requisicion + "'";
                    SqlDataReader Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    while (Reader.Read())
                    {
                        Obj_Cargado.P_No_Requisicion = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones_Garantia.Campo_No_Requisicion].ToString())) ? Reader[Ope_Com_Requisiciones_Garantia.Campo_No_Requisicion].ToString() : String.Empty;
                        Obj_Cargado.P_Folio = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones_Garantia.Campo_Folio_Requisicion].ToString())) ? Reader[Ope_Com_Requisiciones_Garantia.Campo_Folio_Requisicion].ToString() : String.Empty;
                        Obj_Cargado.P_Fecha_Elaboracion = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones_Garantia.Campo_Fecha_Elaboracion].ToString())) ? Convert.ToDateTime(Reader[Ope_Com_Requisiciones_Garantia.Campo_Fecha_Elaboracion]) : new DateTime();
                        Obj_Cargado.P_Estatus = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones_Garantia.Campo_Estatus].ToString())) ? Reader[Ope_Com_Requisiciones_Garantia.Campo_Estatus].ToString() : String.Empty;
                        Obj_Cargado.P_Dependencia_ID = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones_Garantia.Campo_Dependencia_ID].ToString())) ? Reader[Ope_Com_Requisiciones_Garantia.Campo_Dependencia_ID].ToString() : String.Empty;
                        Obj_Cargado.P_No_Requisicion_Referencia = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones_Garantia.Campo_Requisicion_Ref].ToString())) ? Reader[Ope_Com_Requisiciones_Garantia.Campo_Requisicion_Ref].ToString() : String.Empty;
                        Obj_Cargado.P_Justificacion = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones_Garantia.Campo_Justificacion].ToString())) ? Reader[Ope_Com_Requisiciones_Garantia.Campo_Justificacion].ToString() : String.Empty;
                        Obj_Cargado.P_Proveedor = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones_Garantia.Campo_Proveedor_ID].ToString())) ? Reader[Ope_Com_Requisiciones_Garantia.Campo_Proveedor_ID].ToString() : String.Empty;
                        Obj_Cargado.P_Resultado = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones_Garantia.Campo_Resultado].ToString())) ? Reader[Ope_Com_Requisiciones_Garantia.Campo_Resultado].ToString() : String.Empty;
                        Obj_Cargado.P_Diagnositco = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones_Garantia.Campo_Diagnostico].ToString())) ? Reader[Ope_Com_Requisiciones_Garantia.Campo_Diagnostico].ToString() : String.Empty;
                    }
                    Reader.Close();
                    if (!String.IsNullOrEmpty(Obj_Cargado.P_No_Requisicion))
                    {
                        Mi_SQL = "SELECT COMENT." + Ope_Com_Req_Gar_Comentarios.Campo_No_Registro + " AS NO_REGISTRO";
                        Mi_SQL += ", COMENT." + Ope_Com_Req_Gar_Comentarios.Campo_Estatus + " AS ESTATUS";
                        Mi_SQL += ", COMENT." + Ope_Com_Req_Gar_Comentarios.Campo_Comentario + " AS COMENTARIO";
                        Mi_SQL += ", COMENT." + Ope_Com_Req_Gar_Comentarios.Campo_Usuario_Creo + " AS USUARIO_COMENTO";
                        Mi_SQL += ", COMENT." + Ope_Com_Req_Gar_Comentarios.Campo_Fecha_Creo + " AS FECHA_COMENTO";
                        Mi_SQL += " FROM " + Ope_Com_Req_Gar_Comentarios.Tabla_Ope_Com_Req_Gar_Comentarios + " COMENT";
                        Mi_SQL += " WHERE COMENT." + Ope_Com_Req_Gar_Comentarios.Campo_No_Requisicion + " = '" + Obj_Cargado.P_No_Requisicion + "'";
                        Mi_SQL += " ORDER BY NO_REGISTRO DESC";
                        DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Datos != null)
                        {
                            if (Ds_Datos.Tables.Count > 0)
                            {
                                Obj_Cargado.P_Dt_Comentarios = Ds_Datos.Tables[0];
                            }
                        }
                        Ds_Datos = null;
                    }
                    if (!String.IsNullOrEmpty(Obj_Cargado.P_No_Requisicion))
                    {
                        Mi_SQL = "SELECT HIST." + Ope_Com_Historial_Req_Garantia.Campo_No_Registro + " AS NO_REGISTRO";
                        Mi_SQL += ", HIST." + Ope_Com_Historial_Req_Garantia.Campo_Estatus + " AS ESTATUS";
                        Mi_SQL += ", HIST." + Ope_Com_Historial_Req_Garantia.Campo_Usuario_Creo + " AS USUARIO_CREO";
                        Mi_SQL += ", HIST." + Ope_Com_Historial_Req_Garantia.Campo_Fecha_Creo + " AS FECHA_CREO";
                        Mi_SQL += " FROM " + Ope_Com_Historial_Req_Garantia.Tabla_Ope_Com_Historial_Req_Garantia + " HIST";
                        Mi_SQL += " WHERE HIST." + Ope_Com_Historial_Req_Garantia.Campo_No_Requisicion + "= '" + Obj_Cargado.P_No_Requisicion + "'";
                        Mi_SQL += " ORDER BY NO_REGISTRO ASC;";
                        DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Datos != null)
                        {
                            if (Ds_Datos.Tables.Count > 0)
                            {
                                Obj_Cargado.P_Dt_Historial = Ds_Datos.Tables[0];
                            }
                        }
                        Ds_Datos = null;
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Obj_Cargado;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCIÓN: Consultar_Cabecera_Reporte_Requisiciones_Garantia
            /// DESCRIPCIÓN: Consultar_Proveedores_Requisiciones_Garantia
            /// RETORNA: 
            /// CREO: Francisco A. Gallardo Castañeda
            /// FECHA_CREO: Mayo 2013
            /// MODIFICO:
            /// FECHA_MODIFICO
            /// CAUSA_MODIFICACIÓN   
            /// *******************************************************************************/
            internal static DataTable Consultar_Cabecera_Reporte_Requisiciones_Garantia(Cls_Ope_Com_Requisiciones_Garantia_Negocio Parametros)
            {
                DataTable Dt_Datos = new DataTable();
                try
                {
                    String Mi_SQL = "SELECT REQ_GAR." + Ope_Com_Requisiciones_Garantia.Campo_No_Requisicion + " AS NO_REQUISICION";
                    Mi_SQL += ", REQ_GAR." + Ope_Com_Requisiciones_Garantia.Campo_Folio_Requisicion + " AS FOLIO_REQUISICION";
                    Mi_SQL += ", REQ_GAR." + Ope_Com_Requisiciones_Garantia.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                    Mi_SQL += ", REQ_GAR." + Ope_Com_Requisiciones_Garantia.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL += ", REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio + " AS FOLIO_REQ_REF";
                    Mi_SQL += ", (LTRIM(RTRIM(DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + ")) +' - '+ LTRIM(RTRIM(DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + "))) AS DEPENDENCIA";
                    Mi_SQL += ", (LTRIM(RTRIM(PROVEEDORES." + Cat_Com_Proveedores.Campo_Compañia + ")) +' / '+ LTRIM(RTRIM(PROVEEDORES." + Cat_Com_Proveedores.Campo_Nombre + "))) AS PROVEEDOR";
                    Mi_SQL += ", ISNULL(REQ_GAR." + Ope_Com_Requisiciones_Garantia.Campo_Resultado + ", '- - -') AS RESULTADO";
                    Mi_SQL += ", ISNULL(REQ_GAR." + Ope_Com_Requisiciones_Garantia.Campo_Diagnostico + ", '- - -') AS DIAGNOSTICO";
                    Mi_SQL += ", REQ_GAR." + Ope_Com_Requisiciones_Garantia.Campo_Justificacion + " AS JUSTIFICACION";
                    Mi_SQL += ", (SELECT (Cat_Empleados." + Cat_Empleados.Campo_Nombre + " + ' ' + Cat_Empleados." + Cat_Empleados.Campo_Apellido_Paterno + " + ' ' + Cat_Empleados." + Cat_Empleados.Campo_Apellido_Materno + ") FROM " + Cat_Empleados.Tabla_Cat_Empleados + " WHERE Cat_Empleados." + Cat_Empleados.Campo_Empleado_ID + " = REQ_GAR." + Ope_Com_Requisiciones_Garantia.Campo_Empleado_Genero_ID + ") AS EMPLEADO_GENERO";
                    Mi_SQL += ", (SELECT (Cat_Empleados." + Cat_Empleados.Campo_Nombre + " + ' ' + Cat_Empleados." + Cat_Empleados.Campo_Apellido_Paterno + " + ' ' + Cat_Empleados." + Cat_Empleados.Campo_Apellido_Materno + ") FROM " + Cat_Empleados.Tabla_Cat_Empleados + " WHERE Cat_Empleados." + Cat_Empleados.Campo_Empleado_ID + " = REQ_GAR." + Ope_Com_Requisiciones_Garantia.Campo_Empleado_Autorizo_ID + ") AS EMPLEADO_AUTORIZO";
                    Mi_SQL += ", (SELECT (Cat_Empleados." + Cat_Empleados.Campo_Nombre + " + ' ' + Cat_Empleados." + Cat_Empleados.Campo_Apellido_Paterno + " + ' ' + Cat_Empleados." + Cat_Empleados.Campo_Apellido_Materno + ") FROM " + Cat_Empleados.Tabla_Cat_Empleados + " WHERE Cat_Empleados." + Cat_Empleados.Campo_Empleado_ID + " = REQ_GAR." + Ope_Com_Requisiciones_Garantia.Campo_Empleado_Termino_ID + ") AS EMPLEADO_TERMINO";
                    Mi_SQL += " FROM " + Ope_Com_Requisiciones_Garantia.Tabla_Ope_Com_Requisiciones_Garantia + " REQ_GAR";
                    Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICIONES ON REQ_GAR." + Ope_Com_Requisiciones_Garantia.Campo_Requisicion_Ref + " = REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + "";
                    Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON REQ_GAR." + Ope_Com_Requisiciones_Garantia.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Propuesta_Cotizacion.Tabla_Ope_Com_Propuesta_Cotizacion + " PROPUESTA ON REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = PROPUESTA." + Ope_Com_Propuesta_Cotizacion.Campo_No_Requisicion + " AND PROPUESTA." + Ope_Com_Propuesta_Cotizacion.Campo_Resultado + " = 'ACEPTADA'";
                    Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDORES ON PROPUESTA." + Ope_Com_Propuesta_Cotizacion.Campo_Proveedor_ID + " = PROVEEDORES." + Cat_Com_Proveedores.Campo_Proveedor_ID + "";
                    Mi_SQL += " WHERE REQ_GAR." + Ope_Com_Requisiciones_Garantia.Campo_No_Requisicion + " = '" + Parametros.P_No_Requisicion + "'";
                    DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Ds_Datos != null)
                    {
                        if (Ds_Datos.Tables.Count > 0)
                        {
                            Dt_Datos = Ds_Datos.Tables[0];
                        }
                    }
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Excepción al consultar los Bienes de la Requisición: [" + Ex.Message + "]");
                }
                return Dt_Datos;
            }

        #endregion Metodos

    }
}