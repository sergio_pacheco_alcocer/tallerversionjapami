﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JAPAMI.Seguimiento_Reservas.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Com_Seguimiento_Reservas_Datos
/// </summary>
/// 
namespace JAPAMI.Seguimiento_Reservas.Datos
{
    public class Cls_Ope_Com_Seguimiento_Reservas_Datos
    {

        public static DataTable Consultar_Proveedor(Cls_Ope_Com_Seguimiento_Reservas_Negocio Clase_Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            Mi_SQL.Append("SELECT " + Cat_Com_Proveedores.Campo_Proveedor_ID);
            Mi_SQL.Append(",CAST( CAST(" + Cat_Com_Proveedores.Campo_Proveedor_ID + " AS INT) AS VARCHAR) +' '+ " + Cat_Com_Proveedores.Campo_Nombre);
            Mi_SQL.Append(" FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores);
            Mi_SQL.Append(" WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + " IS NOT NULL");
            if (Clase_Negocio.P_Razon_Social != null)
                Mi_SQL.Append(" AND UPPER(" + Cat_Com_Proveedores.Campo_Nombre + ") LIKE UPPER('%" + Clase_Negocio.P_Razon_Social + "%')");
            if(Clase_Negocio.P_Proveedor_ID != null)
                Mi_SQL.Append(" AND " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + String.Format("{0:0000000000}", Convert.ToInt32(Clase_Negocio.P_Proveedor_ID)) + "'");

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
        }

        public static DataTable Consultar_Empleado(Cls_Ope_Com_Seguimiento_Reservas_Negocio Clase_Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            Mi_SQL.Append("SELECT " + Cat_Empleados.Campo_Empleado_ID);
            Mi_SQL.Append(", CAST( CAST(" + Cat_Empleados.Campo_No_Empleado + " AS INT) AS VARCHAR) +' '+ " + Cat_Empleados.Campo_Apellido_Paterno);
            Mi_SQL.Append(" + ' '+ " + Cat_Empleados.Campo_Apellido_Materno + " +' '+ " + Cat_Empleados.Campo_Nombre);
            Mi_SQL.Append(" FROM " + Cat_Empleados.Tabla_Cat_Empleados);
            Mi_SQL.Append(" WHERE " + Cat_Empleados.Campo_Empleado_ID + " IS NOT NULL");
            if (!String.IsNullOrEmpty(Clase_Negocio.P_No_Empledo))
            {
                Mi_SQL.Append(" AND " + Cat_Empleados.Campo_No_Empleado + "='"+Clase_Negocio.P_No_Empledo+"'");
            }
            else
            {
                Mi_SQL.Append(" AND UPPER(" + Cat_Empleados.Campo_Nombre + ") LIKE UPPER('%" + Clase_Negocio.P_Nombre_Empleado + "%')");
                Mi_SQL.Append(" OR  UPPER(" + Cat_Empleados.Campo_Apellido_Paterno + ") LIKE UPPER('%" + Clase_Negocio.P_Nombre_Empleado + "%')");
                Mi_SQL.Append(" OR  UPPER(" + Cat_Empleados.Campo_Apellido_Materno + ") LIKE UPPER('%" + Clase_Negocio.P_Nombre_Empleado + "%')");
                Mi_SQL.Append(" ORDER BY " + Cat_Empleados.Campo_Apellido_Paterno);
            
            }
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
        }



    }
}//fin del namespace