﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Catalogo_Compras_Parametros.Negocio;
using JAPAMI.Constantes;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;

/// <summary>
/// Summary description for Cls_Cat_Com_Parametros_Datos
/// </summary>
namespace JAPAMI.Catalogo_Compras_Parametros.Datos
{
    public class Cls_Cat_Com_Parametros_Datos
    {
        public Cls_Cat_Com_Parametros_Datos()
        {
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Alta_Parametros
        /// DESCRIPCION:            Dar de Alta un nuevo Parametro a la base de datos
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos a ingresar
        /// CREO       :            José Antonio López Hernández
        /// FECHA_CREO :            07/Enero/2011 12:27 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static void Alta_Parametros(Cls_Cat_Com_Parametros_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            Object Aux; //Variable auxiliar para las consultas
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Consultas para el ID
                Mi_SQL = "SELECT ISNULL(MAX(" + Cat_Com_Parametros.Campo_Parametro_ID + "), '00000') FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros;

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Aux = Obj_Comando.ExecuteScalar();

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                    Datos.P_Parametro_ID = String.Format("{0:00000}", Convert.ToInt32(Aux) + 1);
                else
                    Datos.P_Parametro_ID = "00001";

                //Asignar consulta para la insercion
                Mi_SQL = "INSERT INTO " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + " (" + Cat_Com_Parametros.Campo_Parametro_ID + ",";
                Mi_SQL = Mi_SQL + Cat_Com_Parametros.Campo_Cantidad_Sal_Min_Resguardo + ",";
                Mi_SQL = Mi_SQL + Cat_Com_Parametros.Campo_Plazo_Surtir_Orden_Compra + ",";
                Mi_SQL = Mi_SQL + Cat_Com_Parametros.Campo_Usuario_Creo + "," + Cat_Com_Parametros.Campo_Fecha_Creo + ") ";
                Mi_SQL = Mi_SQL + "VALUES('" + Datos.P_Parametro_ID + "','" + Datos.P_Salario_Minimo_Resguardado + "','";
                Mi_SQL = Mi_SQL + "'" + Datos.P_Usuario + "',GETDATE())";

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Ejecutar transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Cambio_Parametros
        /// DESCRIPCION:            Modificar un Parametro existente de la base de datos
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos a modificar
        /// CREO       :            José Antonio López Hernández
        /// FECHA_CREO :            07/Enero/2011 12:32 
        /// MODIFICO          :     Roberto González
        /// FECHA_MODIFICO    :     07/Febrero/2011
        /// CAUSA_MODIFICACION:     Cambio a parámetro estático para el campo ID en la consulta para que sólo 
        ///                         modifique el primer registro
        /// MODIFICO          :     Fernando Gonzalez
        /// FECHA_MODIFICO    :     25/abril/2012
        /// CAUSA_MODIFICACION:     Se agrego la actualizacion del campo Rol_Proveedor_ID del catalogo de roles 

        ///*******************************************************************************/
        public static void Cambio_Parametros(Cls_Cat_Com_Parametros_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Asignar consulta para la modificacion
                Mi_SQL = "UPDATE " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + " SET ";
                Mi_SQL = Mi_SQL + Cat_Com_Parametros.Campo_Cantidad_Sal_Min_Resguardo + " = '" + Datos.P_Salario_Minimo_Resguardado + "',";
                Mi_SQL = Mi_SQL + Cat_Com_Parametros.Campo_Plazo_Surtir_Orden_Compra + " = '" + Datos.P_Plazo_Surtir_Orden_Compra + "',";
                Mi_SQL = Mi_SQL + Cat_Com_Parametros.Campo_Rol_Proveedor_ID + " = '" + Datos.P_Rol_Proveedor_ID + "',";
                Mi_SQL = Mi_SQL + Cat_Com_Parametros.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario + "', ";
                Mi_SQL = Mi_SQL + Cat_Com_Parametros.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL = Mi_SQL + "WHERE " + Cat_Com_Parametros.Campo_Parametro_ID + " = '00001'";

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Ejecutar transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consultar_Generico
        /// DESCRIPCION:            Realizar la consulta del listado de Partida Generica
        /// PARAMETROS :            
        /// CREO       :            Jacqueline Ramírez Sierra
        /// FECHA_CREO :            12/Marzo/2011 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consultar_Generico(Cls_Cat_Com_Parametros_Negocio Parametros)
        {
            String Mi_SQL = "SELECT " + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID +
                            ", " + Cat_SAP_Partida_Generica.Campo_Descripcion +
                            " FROM " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica;
            //if (Parametros. != null)
            //{
            //    Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Giros.Campo_Giro_ID +
            //             " ='" + Cotizadores.P_Giro_ID + "'";
            //}


            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consultar_Especifico
        /// DESCRIPCION:            Realizar la consulta del listado de Partida Especifica
        /// PARAMETROS :            
        /// CREO       :            Jacqueline Ramírez Sierra
        /// FECHA_CREO :            12/Marzo/2011 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consultar_Especificas(Cls_Cat_Com_Parametros_Negocio Parametros)
        {
            String Mi_SQL = "SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID +
                            ", " + Cat_Sap_Partidas_Especificas.Campo_Descripcion +
                            " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas +
                            " WHERE " + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID + "='" + Parametros.P_Partida_Generica_ID + "'";
            //if (Parametros. != null)
            //{
            //    Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Giros.Campo_Giro_ID +
            //             " ='" + Cotizadores.P_Giro_ID + "'";
            //}


            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Parametros
        /// DESCRIPCION:            Realizar la consulta de los Parametros por criterio de busqueda o por un ID
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos a modificar
        /// CREO       :            José Antonio López Hernández
        /// FECHA_CREO :            07/Enero/2011 12:34
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consultar_Combo(Cls_Cat_Com_Parametros_Negocio Parametros)
        {
            String Mi_SQL = "SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID +
                            //", " + Cat_Sap_Partidas_Especificas.Campo_Descripcion +
                            " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + 
                            " WHERE " + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID + "='" + Parametros.P_Partida_Generica_ID + "'" ;
 
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }
        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Parametros
        /// DESCRIPCION:            Realizar la consulta de los Parametros por criterio de busqueda o por un ID
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos a modificar
        /// CREO       :            José Antonio López Hernández
        /// FECHA_CREO :            07/Enero/2011 12:34
        /// MODIFICO          :     Fernando Gonzalez   
        /// FECHA_MODIFICO    :     25/Abril/2012
        /// CAUSA_MODIFICACION:     consultar tambien el campo Rol_Proveedor_ID
        ///*******************************************************************************/
        public static DataTable Consulta_Parametros(Cls_Cat_Com_Parametros_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;

            try
            {
                //Asignar consulta para los Parametros
                Mi_SQL = "SELECT " + Cat_Com_Parametros.Campo_Parametro_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Com_Parametros.Campo_Cantidad_Sal_Min_Resguardo + ", ";
                Mi_SQL = Mi_SQL + Cat_Com_Parametros.Campo_Plazo_Surtir_Orden_Compra + ", ";
                Mi_SQL = Mi_SQL + Cat_Com_Parametros.Campo_Rol_Proveedor_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Com_Parametros.Campo_Tipo_Solicitud_Pago_ID;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros;
                //Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Parametros.Campo_Parametro_ID + " = '00001'";

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consultar_Roles
        /// DESCRIPCION:            Realizar la consulta del Catalogo de roles proveedor
        /// PARAMETROS :            Parametros: es un tipo de dato de la tabal Cat_Parametros
        /// CREO       :            Fernando Gonzalez
        /// FECHA_CREO :            25/Abril/2012 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consultar_Roles(Cls_Cat_Com_Parametros_Negocio Parametros)
        {
            String Mi_SQL;
            
            try
            {
                Mi_SQL = "SELECT " + Apl_Cat_Roles.Campo_Rol_ID +
                              "," + Apl_Cat_Roles.Campo_Nombre +
                            " FROM " + Apl_Cat_Roles.Tabla_Apl_Cat_Roles;
                if (Parametros.P_Rol_Proveedor_ID != null)
                    Mi_SQL += " WHERE " + Apl_Cat_Roles.Campo_Rol_ID + " ='" + Parametros.P_Rol_Proveedor_ID + "'";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consultar_Tipo_Solicitud
        /// DESCRIPCION:            Realiza la consulta del catalogo de solicitudes de pago
        /// PARAMETROS :            Parametros: es un tipo de dato de la tabla Cat_Parametros
        /// CREO       :            Susana Trigueros Armenta
        /// FECHA_CREO :            13/Mayo/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consultar_Tipo_Solicitud(Cls_Cat_Com_Parametros_Negocio Parametros)
        {
            String Mi_SQL;

            try
            {
                Mi_SQL = "SELECT " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID +
                         "," + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion +
                         " FROM " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago +
                         " WHERE " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Estatus + " = 'ACTIVO'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }

        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Actualizar_Rol_Proveedor
        /// DESCRIPCION:            actualiza el Rol_ID de todos los proveedores
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos a modificar
        /// CREO       :            José Antonio López Hernández
        /// FECHA_CREO :            07/Enero/2011 12:32 
        /// MODIFICO          :     Fernando Gonzalez
        /// FECHA_MODIFICO    :     25/Abril/2012
        /// CAUSA_MODIFICACION:     Actualizar el campo Rol_Proveedor_ID
                          

        ///*******************************************************************************/
        public static void Actualizar_Rol_Proveedor(Cls_Cat_Com_Parametros_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Asignar consulta para la modificacion
                Mi_SQL = "UPDATE " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " SET ";
                Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Rol_ID + " = '" + Datos.P_Rol_Proveedor_ID + "',";
                Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario + "', ";
                Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Fecha_Modifico + " = GETDATE() ";

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Ejecutar transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }

    }
}