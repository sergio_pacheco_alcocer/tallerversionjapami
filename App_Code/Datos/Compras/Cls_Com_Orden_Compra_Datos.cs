﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Rpt_Orden_Compra.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;


/// <summary>
/// Summary description for Cls_Com_Orden_Compra_Datos
/// </summary>
/// 
namespace JAPAMI.Rpt_Orden_Compra.Datos
{

    public class Cls_Com_Orden_Compra_Datos
    {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Ordenes_Compra
        ///DESCRIPCIÓN: Consultar_Ordenes_Compra
        ///PARAMETROS:  
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 2/Septiembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataSet Consultar_Ordenes_Compra(Cls_Com_Orden_Compra_Negocio Datos_Negocio)
        {
            String Mi_SQL = "";

            switch (Datos_Negocio.P_Tipo.Trim())
            {
                case "COMPRA DIRECTA":
                    Mi_SQL = "SELECT OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Folio +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Estatus +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Tipo_Proceso +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Tipo_Articulo +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Nombre_Proveedor +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Resguardada +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Recibo_Transitorio +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_No_Reserva +
                        ", TO_CHAR(OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo +
                        ",'DD/MON/YYYY') AS " + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo +
                        ", TO_CHAR(OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Fecha_Entrega +
                        ",'DD/MON/YYYY') AS " + Ope_Com_Ordenes_Compra.Campo_Fecha_Entrega +
                        ", NULL AS FOLIO_TIPO_COMPRA" +
                        ", OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio + " AS FOLIO_REQUISICION" +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Cantidad +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Total_Cotizado +
                        " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " OPE_COM_ORDENES_COMPRA " +
                        ", " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " OPE_COM_REQUISICIONES" +
                        ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " OPE_COM_REQ_PRODUCTO" +
                        " WHERE OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra +
                        "= OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_No_Orden_Compra +
                        " AND OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                        "= OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Requisicion_ID;

                    break;
                case "COTIZACION":
                    Mi_SQL = "SELECT OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Folio +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Estatus +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Tipo_Proceso +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Tipo_Articulo +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Nombre_Proveedor +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Resguardada +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Recibo_Transitorio +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_No_Reserva +
                        ", TO_CHAR(OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo +
                        ",'DD/MON/YYYY') AS " + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo +
                        ", TO_CHAR(OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Fecha_Entrega +
                        ",'DD/MON/YYYY') AS " + Ope_Com_Ordenes_Compra.Campo_Fecha_Entrega +
                        ", OPE_COM_COTIZACION." + Ope_Com_Cotizaciones.Campo_Folio + " AS FOLIO_TIPO_COMPRA" +
                        ", OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio + " AS FOLIO_REQUISICION" +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Cantidad +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Total_Cotizado +
                        " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " OPE_COM_ORDENES_COMPRA " +
                        ", " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " OPE_COM_REQUISICIONES" +
                        ", " + Ope_Com_Cotizaciones.Tabla_Ope_Com_Cotizaciones + " OPE_COM_COTIZACION" +
                        ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " OPE_COM_REQ_PRODUCTO" +
                        " WHERE OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra +
                        "= OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_No_Orden_Compra +
                        " AND OPE_COM_COTIZACION." + Ope_Com_Cotizaciones.Campo_No_Cotizacion +
                        "= OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_No_Cotizacion +
                        " AND OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                        "= OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Requisicion_ID;
                    break;
                case "COMITE COMPRAS":
                    Mi_SQL = "SELECT OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Folio +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Estatus +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Tipo_Proceso +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Tipo_Articulo +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Nombre_Proveedor +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Resguardada +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Recibo_Transitorio +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_No_Reserva +
                        ", TO_CHAR(OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo +
                        ",'DD/MON/YYYY') AS " + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo +
                        ", TO_CHAR(OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Fecha_Entrega +
                        ",'DD/MON/YYYY') AS " + Ope_Com_Ordenes_Compra.Campo_Fecha_Entrega +
                        ", OPE_COM_COMITE_COMPRA." + Ope_Com_Comite_Compras.Campo_Folio + " AS FOLIO_TIPO_COMPRA" +
                        ", OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio + " AS FOLIO_REQUISICION" +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Cantidad +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Total_Cotizado +
                        " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " OPE_COM_ORDENES_COMPRA " +
                        ", " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " OPE_COM_REQUISICIONES" +
                        ", " + Ope_Com_Comite_Compras.Tabla_Ope_Com_Comite_Compras + " OPE_COM_COMITE_COMPRA" +
                        ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " OPE_COM_REQ_PRODUCTO" +
                        " WHERE OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra +
                        "= OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_No_Orden_Compra +
                        " AND OPE_COM_COMITE_COMPRA." + Ope_Com_Comite_Compras.Campo_No_Comite_Compras +
                        "= OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_No_Comite_Compras +
                        " AND OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                        "= OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Requisicion_ID;

                    break;
                case "LICITACION":
                    Mi_SQL = "SELECT OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Folio +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Estatus +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Tipo_Proceso +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Tipo_Articulo +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Nombre_Proveedor +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Resguardada +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Recibo_Transitorio +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_No_Reserva +
                        ", TO_CHAR(OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo +
                        ",'DD/MON/YYYY') AS " + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo +
                        ", TO_CHAR(OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Fecha_Entrega +
                        ",'DD/MON/YYYY') AS " + Ope_Com_Ordenes_Compra.Campo_Fecha_Entrega +
                        ", OPE_COM_LICITACION." + Ope_Com_Licitaciones.Campo_Folio + " AS FOLIO_TIPO_COMPRA" +
                        ", OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio + " AS FOLIO_REQUISICION" +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Cantidad +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Total_Cotizado +
                        " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " OPE_COM_ORDENES_COMPRA " +
                        ", " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " OPE_COM_REQUISICIONES" +
                        ", " + Ope_Com_Licitaciones.Tabla_Ope_Com_Licitaciones + " OPE_COM_LICITACION" +
                        ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " OPE_COM_REQ_PRODUCTO" +
                        " WHERE OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra +
                        "= OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_No_Orden_Compra +
                        " AND OPE_COM_LICITACION." + Ope_Com_Licitaciones.Campo_No_Licitacion +
                        "= OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_No_Licitacion +
                        " AND OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                        "= OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Requisicion_ID;
                    break;
            }

            if (Datos_Negocio.P_Tipo != null)
            {
                Mi_SQL = Mi_SQL + " AND OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Tipo_Proceso +
                    "='" + Datos_Negocio.P_Tipo + "'";
            }
            if (Datos_Negocio.P_Estatus != null)
            {
                Mi_SQL = Mi_SQL + " AND OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Estatus +
                    "='" + Datos_Negocio.P_Estatus + "'";
            }

            Mi_SQL = Mi_SQL + " AND OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo +
                     " BETWEEN TO_DATE ('" + Datos_Negocio.P_Fecha_Inicial + " 00:00:00', 'DD-MM-YYYY HH24:MI:SS')" +
                     " AND TO_DATE ('" + Datos_Negocio.P_Fecha_Final + " 23:59:00', 'DD-MM-YYYY HH24:MI:SS')";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Ordenes_Servicio
        ///DESCRIPCIÓN: Consultar_Ordenes_Servicio
        ///PARAMETROS:  
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 06/jun/2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Ordenes_Servicio(Cls_Com_Orden_Compra_Negocio Datos_Negocio)
        {
            DataTable Dt_Datos = new DataTable();
            try
            {
                String Mi_SQL = "SELECT OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Folio +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Estatus +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Tipo_Proceso +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Tipo_Articulo +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Nombre_Proveedor +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Resguardada +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Recibo_Transitorio +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_No_Reserva +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo +
                        ", OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Fecha_Entrega +
                        ", NULL AS FOLIO_TIPO_COMPRA" +
                        ", OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_Folio + " AS FOLIO_REQUISICION" +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Cantidad +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado +
                        ", OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Total_Cotizado +
                        ", LTRIM(RTRIM(CAT_DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + ")) + ' - ' + LTRIM(RTRIM(CAT_DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + ")) AS DEPENDENCIA" +
                        " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " OPE_COM_ORDENES_COMPRA " +
                        ", " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " OPE_COM_REQUISICIONES" +
                        ", " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " OPE_COM_REQ_PRODUCTO" +
                        ", " + Cat_Dependencias.Tabla_Cat_Dependencias + " CAT_DEPENDENCIAS" +
                        " WHERE OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra +
                        "= OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_No_Orden_Compra +
                        " AND OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                        "= OPE_COM_REQ_PRODUCTO." + Ope_Com_Req_Producto.Campo_Requisicion_ID +
                        " AND OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                        "= CAT_DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID;

                if (Datos_Negocio.P_Tipo_Articulo != null)
                {
                    Mi_SQL = Mi_SQL + " AND OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Tipo_Articulo + " = '" + Datos_Negocio.P_Tipo_Articulo + "'";
                }
                if (Datos_Negocio.P_Proveedor_ID != null)
                {
                    Mi_SQL = Mi_SQL + " AND OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + " = '" + Datos_Negocio.P_Proveedor_ID + "'";
                }
                if (Datos_Negocio.P_Dependencia_ID != null)
                {
                    Mi_SQL = Mi_SQL + " AND OPE_COM_REQUISICIONES." + Ope_Com_Requisiciones.Campo_Dependencia_ID + " = '" + Datos_Negocio.P_Dependencia_ID + "'";
                }
                if (Datos_Negocio.P_Estatus != null)
                {
                    Mi_SQL = Mi_SQL + " AND OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Estatus + " IN ('" + Datos_Negocio.P_Estatus + "')";
                }
                if (!String.Format("{0:ddMMyyyy}", Datos_Negocio.P_Filtro_Fecha_Inicial).Equals(String.Format("{0:ddMMyyyy}", new DateTime())))
                {

                    Mi_SQL = Mi_SQL + " AND OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo + " >= '" + String.Format("{0:dd/MM/yyyy}", Datos_Negocio.P_Filtro_Fecha_Inicial) + "'";
                }
                if (!String.Format("{0:ddMMyyyy}", Datos_Negocio.P_Filtro_Fecha_Final).Equals(String.Format("{0:ddMMyyyy}", new DateTime())))
                {
                    Mi_SQL = Mi_SQL + " AND OPE_COM_ORDENES_COMPRA." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo + " < '" + String.Format("{0:dd/MM/yyyy}", Datos_Negocio.P_Filtro_Fecha_Final.AddDays(1)) + "'";
                }

                DataSet Ds_Datos= SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Datos != null) {
                    if (Ds_Datos.Tables.Count > 0) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Dependencias
        ///DESCRIPCIÓN: Consultar_Dependencias
        ///PARAMETROS:  
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 06/jun/2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Dependencias(Cls_Com_Orden_Compra_Negocio Datos_Negocio) {
            DataTable Dt_Datos = new DataTable();
            try
            {
                String Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + " AS DEPENDENCIA_ID, " + Cat_Dependencias.Campo_Clave + " +'-'+ " + Cat_Dependencias.Campo_Nombre + " AS NOMBRE, " + Cat_Dependencias.Campo_Grupo_Dependencia_ID + " AS GERENCIA_ID";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
                if (!String.IsNullOrEmpty(Datos_Negocio.P_Estatus))
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Dependencias.Campo_Estatus + " = '" + Datos_Negocio.P_Estatus + "'";

                DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Datos != null)
                {
                    if (Ds_Datos.Tables.Count > 0)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

    }
}