﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Actualizar_Proveedores.Negocio;


/// <summary>
/// Summary description for Cls_Cat_Com_Actializar_Proveedores
/// </summary>
namespace JAPAMI.Actualizar_Proveedores.Datos
{
    public class Cls_Cat_Com_Actializar_Proveedores_Datos
    {
        public Cls_Cat_Com_Actializar_Proveedores_Datos()
        {
        }
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consultar_proveedor
        /// 	DESCRIPCION:    Consultara un proveedor de acuerdo a su no_patron y a su nombre comercial
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Jennyfer Ivonne Ceja Lemus
        /// 	FECHA_CREO:     11/Septiembre/2012 09:34
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        ///
        public static DataTable Consultar_proveedor(Cls_Cat_Com_Actualizar_Proveedores_Negocio Datos) 
        {
            StringBuilder MiSql = new StringBuilder();
            try
            {
                MiSql.Append("SELECT " + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_No_Padron+ ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Nombre + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Compañia + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_RFC + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Contacto + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Estatus + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Direccion + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Colonia + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Ciudad + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Estado + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_CP + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Telefono_1 + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Telefono_2 + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Nextel + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Fax + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Tipo_Fiscal +  ", " );
                MiSql.Append(Cat_Com_Proveedores.Campo_Fecha_Actualizacion);
                MiSql.Append(" FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores);
                MiSql.Append(" WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "' ");

                if (!String.IsNullOrEmpty(Datos.P_Nombre)) 
                {
                    MiSql.Append(" AND " + Cat_Com_Proveedores.Campo_Nombre + " = '" + Datos.P_Nombre + "' ");
                }

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consultar_Detalles_Partidas
        /// 	DESCRIPCION:    Consultara las partidas genericas de un proveedor
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Jennyfer Ivonne Ceja Lemus
        /// 	FECHA_CREO:     11/Septiembre/2012 11:26
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        ///
        public static DataTable Consultar_Detalles_Partidas(Cls_Cat_Com_Actualizar_Proveedores_Negocio Datos)
        {
            StringBuilder MiSql = new StringBuilder();
            try
            {
                MiSql.Append("SELECT " + Cat_Com_Det_Part_Prov.Tabla_Cat_Com_Det_Part_Prov + "." + Cat_Com_Det_Part_Prov.Campo_Proveedor_ID+ ", ");
                MiSql.Append(Cat_Com_Det_Part_Prov.Tabla_Cat_Com_Det_Part_Prov + "." + Cat_Com_Det_Part_Prov.Campo_Partida_Generica_ID + ", ");
                MiSql.Append(Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Clave);              
                MiSql.Append(" FROM " + Cat_Com_Det_Part_Prov.Tabla_Cat_Com_Det_Part_Prov + " JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica);
                MiSql.Append(" ON " +  Cat_Com_Det_Part_Prov.Tabla_Cat_Com_Det_Part_Prov + "." +  Cat_Com_Det_Part_Prov.Campo_Partida_Generica_ID + " = " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                MiSql.Append(" WHERE " + Cat_Com_Det_Part_Prov.Tabla_Cat_Com_Det_Part_Prov + "." +  Cat_Com_Det_Part_Prov.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "' ");

                if (!String.IsNullOrEmpty(Datos.P_Partida_Clave))
                {
                    MiSql.Append(" AND " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Clave + " = '" + Datos.P_Partida_Clave + "' ");
                }

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }
          ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Obtener_Datos_Parida
        /// 	DESCRIPCION:    Consultar los datos de la partida a insertar
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Jennyfer Ivonne Ceja Lemus
        /// 	FECHA_CREO:     11/Septiembre/2012 11:55
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        ///
        public static DataTable Obtener_Datos_Parida(Cls_Cat_Com_Actualizar_Proveedores_Negocio Datos)
        {
            StringBuilder MiSql = new StringBuilder();
            try
            {

                MiSql.Append("SELECT " + Cat_SAP_Partida_Generica.Campo_Clave+ ", ");
                MiSql.Append(Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID+ ", ");
                MiSql.Append(Cat_SAP_Partida_Generica.Campo_Concepto_ID);              
                MiSql.Append(" FROM " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica);
                MiSql.Append(" WHERE " + Cat_SAP_Partida_Generica.Campo_Clave + " = '" + Datos.P_Partida_Clave + "' ");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }
    
      ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consultar_Conceptos_Detalles
        /// 	DESCRIPCION:    Consulta los conceptos registrados en el sistema asociado a un proveedor especifico
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Jennyfer Ivonne Ceja Lemus
        /// 	FECHA_CREO:     11/Septiembre/2012 12:24
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        ///
        public static DataTable Consultar_Conceptos_Detalles(Cls_Cat_Com_Actualizar_Proveedores_Negocio Datos)
        {
            StringBuilder MiSql = new StringBuilder();
            try
            {
                MiSql.Append("SELECT " + Cat_Com_Giro_Proveedor.Campo_Proveedor_ID + ", ");
                MiSql.Append(Cat_Com_Giro_Proveedor.Campo_Giro_ID);              
                MiSql.Append(" FROM " + Cat_Com_Giro_Proveedor.Tabla_Cat_Com_Giro_Proveedor);
                MiSql.Append(" WHERE " + Cat_Com_Giro_Proveedor.Campo_Proveedor_ID+ " = '" + Datos.P_Proveedor_ID + "' ");

                if (!String.IsNullOrEmpty(Datos.P_Concepto_ID))
                {
                    MiSql.Append(" AND " + Cat_Com_Giro_Proveedor.Campo_Giro_ID + " = '" + Datos.P_Concepto_ID + "' ");
                }

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }
        ///****************************************************************************************
        /// NOMBRE DE LA FUNCION:  Alta_Proveedor
        /// DESCRIPCION :          1.Consulta el ultimo ID dado de alta para poder ingresar el siguiente
        ///                        2. Da de Alta el Proveedor en la BD con los datos proporcionados por el usuario
        /// PARAMETROS  :          Datos: Variable que contiene los datos que serán insertados en la base de datos
        /// CREO        :          Susana Trigueros Armenta 
        /// FECHA_CREO  :          10/Nov/2011
        /// MODIFICO          : Jennyfer Ivonne Ceja Lemus
        /// FECHA_MODIFICO    : 12/Sept/2012
        /// CAUSA_MODIFICACION: Adaptar el metodo para que funcione para una carga masiva de proveedores
        ///****************************************************************************************/
        public static String Alta_Proveedor(Cls_Cat_Com_Actualizar_Proveedores_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            Object Aux; //Variable auxiliar para las consultas
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {

                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;
                DataTable dt_Insertar  = Datos.P_Dt_Insertar;
                DataTable dt_Actualizar  = Datos.P_Dt_Acualizar;
                DataTable dt_Historia_Fechas = new DataTable();
                Int32 Contador = 0;

                if (dt_Insertar != null && dt_Insertar.Rows.Count > 0)
                {
                    foreach (DataRow Registro in dt_Insertar.Rows)//For para insertar proveedores
                    {
                        Contador++;
                        //Asignar consulta para el maximo ID
                        //Mi_SQL = "SELECT ISNULL(MAX(" + Cat_Com_Proveedores.Campo_Proveedor_ID + "), '0000000000') FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores;

                        ////Ejecutar consulta
                        //Obj_Comando.CommandText = Mi_SQL;
                        //Aux = Obj_Comando.ExecuteScalar();

                        //Verificar si no es nulo
                        //if (Convert.IsDBNull(Aux) == false)
                        //    Datos.P_Proveedor_ID = String.Format("{0:0000000000}", Convert.ToInt32(Aux) + 1);
                        //else
                        //    Datos.P_Proveedor_ID = "0000000001";


                        //Asignar consulta para la insercion
                        Mi_SQL = "INSERT INTO " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores;
                        Mi_SQL = Mi_SQL + " (" + Cat_Com_Proveedores.Campo_Proveedor_ID;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_No_Padron;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Fecha_Registro;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Nombre;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Compañia;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Representante_Legal;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Contacto;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_RFC;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Estatus;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Tipo_Fiscal;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Direccion;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Colonia;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Ciudad;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Estado;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_CP;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Telefono_1;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Telefono_2;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Nextel;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Fax;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Correo_Electronico;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Tipo_Pago;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Dias_Credito;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Forma_Pago;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Comentarios;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Fecha_Creo;
                        if (!String.IsNullOrEmpty(Registro["FECHA_ACTUALIZACION"].ToString())) //
                        {
                            Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Fecha_Actualizacion;
                            
                        }
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Rol_ID;


                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Usuario;
                        if (!String.IsNullOrEmpty(Registro["PORCENTAJE_ANTICIPO"].ToString()))//
                        {
                            Mi_SQL = Mi_SQL + ", PORCENTAJE_ANTICIPO";
                        }
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Provisional;
                        Mi_SQL = Mi_SQL + ") ";
                        Mi_SQL = Mi_SQL + "VALUES('" + Registro["PROVEEDOR_ID"].ToString() + "',";
                        Mi_SQL = Mi_SQL + Registro["PROVEEDOR_ID"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + "GETDATE(), '";
                        Mi_SQL = Mi_SQL + Registro["RAZON_SOCIAL"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["NOMBRE_COMERCIAL"].ToString() + "','";
                        Mi_SQL = Mi_SQL + Registro["REPRESENTANTE_LEGAL"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["CONTACTO"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["RFC"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["ESTATUS"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["TIPO_FISCAL"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["DIRECCION"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["COLONIA"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["CIUDAD"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["ESTADO"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Registro["CODIGO_POSTAL"].ToString() + ", '";
                        Mi_SQL = Mi_SQL + Registro["TELEFONO1"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["TELEFONO2"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["NEXTEL"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["FAX"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["E_MAIL"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["TIPO_PAGO"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Registro["DIAS_CREDITO"].ToString() + ", '";
                        Mi_SQL = Mi_SQL + Registro["FORMA_PAGO"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Registro["COMENTARIOS"].ToString() + "', '";
                        Mi_SQL = Mi_SQL + Cls_Sessiones.Nombre_Empleado + "', GETDATE()";
                        if (!String.IsNullOrEmpty(Registro["FECHA_ACTUALIZACION"].ToString())) //Cambiar
                        {
                            Mi_SQL = Mi_SQL + ",'" + Registro["FECHA_ACTUALIZACION"].ToString() + "'";
                        }
                        Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Com_Parametros.Campo_Rol_Proveedor_ID;
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + ")";
                        Mi_SQL = Mi_SQL + ", '" + Int32.Parse(Registro["PROVEEDOR_ID"].ToString()) + "'";
                        if (!String.IsNullOrEmpty(Registro["PORCENTAJE_ANTICIPO"].ToString())) //Cambiar
                        {
                            Mi_SQL = Mi_SQL + ", " + Registro["PORCENTAJE_ANTICIPO"].ToString();
                        }
                        Mi_SQL = Mi_SQL + ", 'NO'";
                        Mi_SQL = Mi_SQL + ")";
                        //Ejecutar consulta
                        Obj_Comando.CommandText = Mi_SQL;
                        Obj_Comando.ExecuteNonQuery();
                        if (!String.IsNullOrEmpty(Registro["FECHA_ACTUALIZACION"].ToString())) //Cambiar
                        {
                            //Si existe una fecha de actualizacion  agregarla al historial de Actualizaciones
                            Mi_SQL = "";
                            Mi_SQL = " INSERT INTO " + Ope_Com_His_Autor_Prov.Tabla_Ope_Com_His_Autor_Prov;
                            Mi_SQL = Mi_SQL + "(" + Ope_Com_His_Autor_Prov.Campo_Historial_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Com_His_Autor_Prov.Campo_Fecha_Actualizacion;
                            Mi_SQL = Mi_SQL + ", " + Ope_Com_His_Autor_Prov.Campo_Proveedor_ID;
                            Mi_SQL = Mi_SQL + ", " + Cat_Com_Giro_Proveedor.Campo_Fecha_Creo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Com_His_Autor_Prov.Campo_Usuario_Creo + ")";
                            Mi_SQL = Mi_SQL + " VALUES(" + Obtener_Consecutivo(Ope_Com_His_Autor_Prov.Campo_Historial_ID, Ope_Com_His_Autor_Prov.Tabla_Ope_Com_His_Autor_Prov);
                            Mi_SQL = Mi_SQL + ",GETDATE()";
                            Mi_SQL = Mi_SQL + ",'" + Registro["PROVEEDOR_ID"].ToString();
                            Mi_SQL = Mi_SQL + "','" + Registro["FECHA_ACTUALIZACION"].ToString() + "','" + Cls_Sessiones.Nombre_Empleado.Trim() + "')";


                            //Ejecutar consulta
                            Obj_Comando.CommandText = Mi_SQL;
                            Obj_Comando.ExecuteNonQuery();
                        }
                    }//Fin forEach Insertar
                }//Fin if Dt_Insertar 
                if (dt_Actualizar != null && dt_Actualizar.Rows.Count > 0)
                {
                    foreach (DataRow Registro in dt_Actualizar.Rows)//For para insertar proveedores
                    {
                        dt_Historia_Fechas = null;
                        if (!String.IsNullOrEmpty(Registro["ID_ANTERIOR"].ToString())) 
                        {
                            //Elimina los conceptos del provedor provisional que dejara de ser provisional
                            Mi_SQL = Baja_Detalle_Conceptos_Proveedor(Registro["ID_ANTERIOR"].ToString());
                            //Ejecutar consulta
                            Obj_Comando.CommandText = Mi_SQL;
                            Obj_Comando.ExecuteNonQuery();

                            //Elimna las partidas del proveedor provisional que dejara de ser provisional
                            Mi_SQL = Baja_Detalle_Partida_Proveedor(Registro["ID_ANTERIOR"].ToString());
                            //Ejecutar consulta
                            Obj_Comando.CommandText = Mi_SQL;
                            Obj_Comando.ExecuteNonQuery();

                            Mi_SQL = "SELECT " + Ope_Com_His_Autor_Prov.Campo_Historial_ID + ", ";
                            
                            Mi_SQL = Mi_SQL + "CONVERT(VARCHAR," + Ope_Com_His_Autor_Prov.Campo_Fecha_Actualizacion + ",120) AS FECHA_ACTUALIZACION , ";
                            Mi_SQL = Mi_SQL + Ope_Com_His_Autor_Prov.Campo_Proveedor_ID + ", ";
                            Mi_SQL = Mi_SQL + "CONVERT(VARCHAR," + Ope_Com_His_Autor_Prov.Campo_Fecha_Creo + ",120) AS FECHA_CREO, ";
                            Mi_SQL = Mi_SQL + Ope_Com_His_Autor_Prov.Campo_Usuario_Creo + ", ";
                            Mi_SQL = Mi_SQL + "CONVERT(VARCHAR," + Ope_Com_His_Autor_Prov.Campo_Fecha_Modifico + ",120) AS FECHA_MODIFICO, ";
                            Mi_SQL = Mi_SQL + Ope_Com_His_Autor_Prov.Campo_Usuario_Modifico;
                            Mi_SQL = Mi_SQL + " FROM " + Ope_Com_His_Autor_Prov.Tabla_Ope_Com_His_Autor_Prov;
                            Mi_SQL = Mi_SQL + " WITH(NOLOCK) ";
                            Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_His_Autor_Prov.Campo_Proveedor_ID + " = '" + Registro["ID_ANTERIOR"].ToString() + "' ";

                            dt_Historia_Fechas = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                            //Actualiza el nuevo ID del proveedor en el historial de actualizacions 
                            Mi_SQL = Borrar_His_Act_Prov(Registro["ID_ANTERIOR"].ToString());
                            //Ejecutar consulta
                            Obj_Comando.CommandText = Mi_SQL;
                            Obj_Comando.ExecuteNonQuery();

                            
                               
                        }

                        //Asignar consulta para modificar los datos del proveedor
                        Mi_SQL = "UPDATE " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ";
                        Mi_SQL = Mi_SQL + "SET ";
                        if (!String.IsNullOrEmpty(Registro["ID_ANTERIOR"].ToString()))
                        {
                            Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Registro["PROVEEDOR_ID"].ToString() + "', ";
                            Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_No_Padron + " = " + Registro["PROVEEDOR_ID"].ToString() + ", ";
                        }
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Nombre + " = '" + Registro["RAZON_SOCIAL"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Compañia + " = '" + Registro["NOMBRE_COMERCIAL"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Representante_Legal + "='" + Registro["REPRESENTANTE_LEGAL"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Contacto + " = '" + Registro["CONTACTO"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_RFC + " = '" + Registro["RFC"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Estatus + " = '" + Registro["ESTATUS"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Tipo_Fiscal + "='" + Registro["TIPO_FISCAL"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Direccion + " = '" + Registro["DIRECCION"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Colonia + " = '" + Registro["COLONIA"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Ciudad + " = '" + Registro["CIUDAD"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Estado + " = '" + Registro["ESTADO"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_CP + " = " + Registro["CODIGO_POSTAL"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Telefono_1 + " = '" + Registro["TELEFONO1"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Telefono_2 + " = '" + Registro["TELEFONO2"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Nextel + " = '" + Registro["NEXTEL"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Fax + " = '" + Registro["FAX"].ToString() + "', ";

                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Tipo_Pago + " = '" + Registro["TIPO_PAGO"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Dias_Credito + " = " + Registro["DIAS_CREDITO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Forma_Pago + " = '" + Registro["FORMA_PAGO"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Correo_Electronico + " = '" + Registro["E_MAIL"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Comentarios + " = '" + Registro["COMENTARIOS"].ToString() + "', ";
                        if (!String.IsNullOrEmpty(Registro["FECHA_ACTUALIZACION"].ToString()))
                        {
                            Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Fecha_Actualizacion + "='" + Registro["FECHA_ACTUALIZACION"].ToString() + "', ";
                            
                        }
                        if (!String.IsNullOrEmpty(Registro["PORCENTAJE_ANTICIPO"].ToString()))
                        {
                            Mi_SQL = Mi_SQL + " PORCENTAJE_ANTICIPO=" + Registro["PORCENTAJE_ANTICIPO"].ToString() + ",";
                        }
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "', ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Fecha_Modifico + " = GETDATE(), ";
                        Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Provisional + " = 'NO' ";
                        if (!String.IsNullOrEmpty(Registro["ID_ANTERIOR"].ToString()))
                        {
                            Mi_SQL = Mi_SQL + "WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Registro["ID_ANTERIOR"].ToString() + "'";
                        }
                        else 
                        {
                            Mi_SQL = Mi_SQL + "WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Registro["PROVEEDOR_ID"].ToString() + "'";
                        }
                        
                        //Ejecutar consulta
                        Obj_Comando.CommandText = Mi_SQL;
                        Obj_Comando.ExecuteNonQuery();
                        
                        Mi_SQL = "";
                        if (dt_Historia_Fechas != null && dt_Historia_Fechas.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt_Historia_Fechas.Rows.Count; i++)
                            {
                                Mi_SQL = " INSERT INTO " + Ope_Com_His_Autor_Prov.Tabla_Ope_Com_His_Autor_Prov;
                                Mi_SQL = Mi_SQL + "(" + Ope_Com_His_Autor_Prov.Campo_Historial_ID;
                                Mi_SQL = Mi_SQL + ", " + Ope_Com_His_Autor_Prov.Campo_Fecha_Actualizacion;
                                Mi_SQL = Mi_SQL + ", " + Ope_Com_His_Autor_Prov.Campo_Proveedor_ID;
                                Mi_SQL = Mi_SQL + ", " + Ope_Com_His_Autor_Prov.Campo_Fecha_Creo;
                                Mi_SQL = Mi_SQL + ", " + Ope_Com_His_Autor_Prov.Campo_Usuario_Creo;
                                if (!String.IsNullOrEmpty(dt_Historia_Fechas.Rows[i][Ope_Com_His_Autor_Prov.Campo_Usuario_Modifico].ToString()))
                                {
                                    Mi_SQL = Mi_SQL + ", " + Ope_Com_His_Autor_Prov.Campo_Usuario_Modifico;
                                    Mi_SQL = Mi_SQL + ", " + Ope_Com_His_Autor_Prov.Campo_Fecha_Modifico;
                                }
                                Mi_SQL = Mi_SQL + ")";
                                Mi_SQL = Mi_SQL + " VALUES(" + dt_Historia_Fechas.Rows[i][Ope_Com_His_Autor_Prov.Campo_Historial_ID].ToString();
                                Mi_SQL = Mi_SQL + ",'" + dt_Historia_Fechas.Rows[i][Ope_Com_His_Autor_Prov.Campo_Fecha_Actualizacion].ToString().Trim();
                                Mi_SQL = Mi_SQL + "','" + Registro["PROVEEDOR_ID"].ToString();
                                Mi_SQL = Mi_SQL + "', '" + dt_Historia_Fechas.Rows[i][Ope_Com_His_Autor_Prov.Campo_Fecha_Creo].ToString();
                                Mi_SQL = Mi_SQL + "', '" + dt_Historia_Fechas.Rows[i][Ope_Com_His_Autor_Prov.Campo_Usuario_Creo].ToString() + "' ";
                                if (!String.IsNullOrEmpty(dt_Historia_Fechas.Rows[i][Ope_Com_His_Autor_Prov.Campo_Usuario_Modifico].ToString()))
                                {
                                    Mi_SQL = Mi_SQL + ", '" + dt_Historia_Fechas.Rows[i][Ope_Com_His_Autor_Prov.Campo_Usuario_Modifico].ToString();
                                    Mi_SQL = Mi_SQL + "', '" + dt_Historia_Fechas.Rows[i][Ope_Com_His_Autor_Prov.Campo_Fecha_Modifico].ToString() + "'";
                                }
                                Mi_SQL = Mi_SQL + " )";
                                //Ejecutar consulta 
                                Obj_Comando.CommandText = Mi_SQL;
                                Obj_Comando.ExecuteNonQuery();

                            }//Fin del FOR

                        }//Fin del IF
                        

                        if (!String.IsNullOrEmpty(Registro["FECHA_ACTUALIZACION"].ToString())) //Cambiar
                        {
                            //Si existe una fecha de actualizacion  agregarla al historial de Actualizaciones
                            Mi_SQL = "";
                            Mi_SQL = " INSERT INTO " + Ope_Com_His_Autor_Prov.Tabla_Ope_Com_His_Autor_Prov;
                            Mi_SQL = Mi_SQL + "(" + Ope_Com_His_Autor_Prov.Campo_Historial_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Com_His_Autor_Prov.Campo_Fecha_Actualizacion;
                            Mi_SQL = Mi_SQL + ", " + Ope_Com_His_Autor_Prov.Campo_Proveedor_ID;
                            Mi_SQL = Mi_SQL + ", " + Cat_Com_Giro_Proveedor.Campo_Fecha_Creo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Com_His_Autor_Prov.Campo_Usuario_Creo + ")";
                            Mi_SQL = Mi_SQL + " VALUES(" + Obtener_Consecutivo(Ope_Com_His_Autor_Prov.Campo_Historial_ID, Ope_Com_His_Autor_Prov.Tabla_Ope_Com_His_Autor_Prov);
                            Mi_SQL = Mi_SQL + ",GETDATE()";
                            Mi_SQL = Mi_SQL + ",'" + Registro["PROVEEDOR_ID"].ToString();
                            Mi_SQL = Mi_SQL + "','" + Registro["FECHA_ACTUALIZACION"].ToString() + "','" + Cls_Sessiones.Nombre_Empleado.Trim() + "')";


                            //Ejecutar consulta
                            Obj_Comando.CommandText = Mi_SQL;
                            Obj_Comando.ExecuteNonQuery();

                                                    }
                    }//fin  del for
                }//Fin del if dtActualizar


                //Ejecutar transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
                Mensaje = "Se dieron de alta exitosamente a los proveedores";

               
                //Damos de Alta los detalles del Proveedor el Concepto
                if (Datos.P_Dt_Conceptos != null)
                   Alta_Detalle_Conceptos_Proveedor(Datos);
                //Damos de Alta los detalles del Proveedor las partidas
                if (Datos.P_Dt_Partidas!= null)
                    Alta_Detalle_Partidas(Datos);
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
            return Mensaje;
        }
        ///****************************************************************************************
        /// NOMBRE DE LA FUNCION:  Alta_Detalle_Conceptos_Proveedor
        /// DESCRIPCION :          Se crean los conceptos que se le asignaron al Proveedor
        /// PARAMETROS  :          Datos: Variable que contiene los datos para la busqueda
        /// CREO        :          Susana Trigueros Armenta
        /// FECHA_CREO  :         9/Nov/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///****************************************************************************************/
        public static void Alta_Detalle_Conceptos_Proveedor(Cls_Cat_Com_Actualizar_Proveedores_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty; //Variable para el mensaje de error
            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;
                //Primero eliminamos los que ya esten dados de alta
                //Mi_SQL = " DELETE " + Cat_Com_Giro_Proveedor.Tabla_Cat_Com_Giro_Proveedor;
                //Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Giro_Proveedor.Campo_Proveedor_ID;
                //Mi_SQL = Mi_SQL + "='" + Datos.P_Proveedor_ID.Trim() + "'";
                ////Ejecutar consulta
                //Obj_Comando.CommandText = Mi_SQL;
                //Obj_Comando.ExecuteNonQuery();


                Mi_SQL = "";
                if (Datos.P_Dt_Conceptos != null)
                {
                    for (int i = 0; i < Datos.P_Dt_Conceptos.Rows.Count; i++)
                    {
                        Mi_SQL = " INSERT INTO " + Cat_Com_Giro_Proveedor.Tabla_Cat_Com_Giro_Proveedor;
                        Mi_SQL = Mi_SQL + "(" + Cat_Com_Giro_Proveedor.Campo_Proveedor_ID;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Giro_Proveedor.Campo_Giro_ID;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Giro_Proveedor.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Giro_Proveedor.Campo_Fecha_Creo + ")";
                        Mi_SQL = Mi_SQL + " VALUES('" + Datos.P_Dt_Conceptos.Rows[i]["Proveedor_ID"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "','" + Datos.P_Dt_Conceptos.Rows[i]["Concepto_ID"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "','" + Cls_Sessiones.Nombre_Empleado;
                        Mi_SQL = Mi_SQL + "',GETDATE())";


                        //Ejecutar consulta
                        Obj_Comando.CommandText = Mi_SQL;
                        Obj_Comando.ExecuteNonQuery();

                    }
                }//Fin del IF
                //Ejecutar transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();

            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }

        ///****************************************************************************************
        /// NOMBRE DE LA FUNCION:  Alta_Detalle_Partidas
        /// DESCRIPCION :          Consultar los proveedores de acuerdo al ID del proveedor proporcionado por el usuario
        /// PARAMETROS  :          Datos: Variable que contiene los datos para la busqueda
        /// CREO        :          Susana Trigueros Armenta
        /// FECHA_CREO  :         7/Septiembre/2011
        /// MODIFICO          : Jennyfer Ivonne Ceja Lemus
        /// FECHA_MODIFICO    : 12/Septiembre/2012
        /// CAUSA_MODIFICACION: MOdificacion para generar una carga masiva
        ///****************************************************************************************/
        public static void Alta_Detalle_Partidas(Cls_Cat_Com_Actualizar_Proveedores_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty; //Variable para el mensaje de error
            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;
                //Primero eliminamos los que ya esten dados de alta
                //Mi_SQL = " DELETE " + Cat_Com_Det_Part_Prov.Tabla_Cat_Com_Det_Part_Prov;
                //Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Det_Part_Prov.Campo_Proveedor_ID;
                //Mi_SQL = Mi_SQL + "='" + Datos.P_Proveedor_ID.Trim() + "'";
                ////Ejecutar consulta
                //Obj_Comando.CommandText = Mi_SQL;
                //Obj_Comando.ExecuteNonQuery();


                Mi_SQL = "";
                if (Datos.P_Dt_Partidas != null)
                {
                    for (int i = 0; i < Datos.P_Dt_Partidas.Rows.Count; i++)
                    {
                        Mi_SQL = " INSERT INTO " + Cat_Com_Det_Part_Prov.Tabla_Cat_Com_Det_Part_Prov;
                        Mi_SQL = Mi_SQL + "(" + Cat_Com_Det_Part_Prov.Campo_Proveedor_ID;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Det_Part_Prov.Campo_Partida_Generica_ID;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Det_Part_Prov.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Det_Part_Prov.Campo_Fecha_Creo + ")";
                        Mi_SQL = Mi_SQL + " VALUES('" + Datos.P_Dt_Partidas.Rows[i]["Proveedor_ID"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "','" + Datos.P_Dt_Partidas.Rows[i]["Partida_ID"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "','" + Cls_Sessiones.Nombre_Empleado;
                        Mi_SQL = Mi_SQL + "',GETDATE())";


                        //Ejecutar consulta
                        Obj_Comando.CommandText = Mi_SQL;
                        Obj_Comando.ExecuteNonQuery();

                    }//Fin del FOR
                }//Fin del IF
                //Ejecutar transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla + " WITH(NOLOCK) ";
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consultar_Proveedor_Datos
        /// 	DESCRIPCION:    Consultara si un proveedor existe de acuerdo a sus datos excepto al ID o numero de patron
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Jennyfer Ivonne Ceja Lemus
        /// 	FECHA_CREO:     11/Septiembre/2012 09:34
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        ///
        public static DataTable Consultar_Proveedor_Datos(Cls_Cat_Com_Actualizar_Proveedores_Negocio Datos)
        {
            StringBuilder MiSql = new StringBuilder();
            try
            {
                MiSql.Append("SELECT " + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_No_Padron + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Nombre + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Compañia + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_RFC + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Contacto + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Estatus + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Direccion + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Colonia + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Ciudad + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Estado + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_CP + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Telefono_1 + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Telefono_2 + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Nextel + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Fax + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Tipo_Fiscal + ", ");
                MiSql.Append(Cat_Com_Proveedores.Campo_Fecha_Actualizacion);
                MiSql.Append(" FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores);
                //MiSql.Append(" WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "' ");

                if ((!String.IsNullOrEmpty(Datos.P_Nombre)) && MiSql.ToString().Contains("WHERE")) 
                {
                    MiSql.Append(" AND " + Cat_Com_Proveedores.Campo_Nombre + " = '" + Datos.P_Nombre + "' ");
                }
                else
                {
                    MiSql.Append(" WHERE " + Cat_Com_Proveedores.Campo_Nombre + " = '" + Datos.P_Nombre + "' ");
                }

                if ((!String.IsNullOrEmpty(Datos.P_Compania)) && MiSql.ToString().Contains("WHERE"))
                {
                    MiSql.Append(" AND " + Cat_Com_Proveedores.Campo_Compañia + " = '" + Datos.P_Compania + "' ");
                }
                else
                {
                    MiSql.Append(" WHERE " + Cat_Com_Proveedores.Campo_Compañia + " = '" + Datos.P_Compania + "' ");
                }

                if ((!String.IsNullOrEmpty(Datos.P_RFC)) && MiSql.ToString().Contains("WHERE"))
                {
                    MiSql.Append(" AND " + Cat_Com_Proveedores.Campo_RFC + " = '" + Datos.P_RFC + "' ");
                }
                else
                {
                    MiSql.Append(" WHERE " + Cat_Com_Proveedores.Campo_RFC + " = '" + Datos.P_RFC + "' ");
                }

                if ((!String.IsNullOrEmpty(Datos.P_Contacto)) && MiSql.ToString().Contains("WHERE"))
                {
                    MiSql.Append(" AND " + Cat_Com_Proveedores.Campo_Contacto + " = '" + Datos.P_Contacto + "' ");
                }
                else
                {
                    MiSql.Append(" WHERE " + Cat_Com_Proveedores.Campo_Contacto + " = '" + Datos.P_Contacto + "' ");
                }


                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }
        ///****************************************************************************************
        /// NOMBRE DE LA FUNCION:  Baja_Detalle_Partida_Proveedor
        /// DESCRIPCION :          elimina las partidas de un proveedor
        /// PARAMETROS  :          Datos: Variable que contiene los datos para la busqueda
        /// CREO        :          Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO  :         02/Octubre/2012 10:14
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///****************************************************************************************/
        public static String Baja_Detalle_Partida_Proveedor(String Proveedor_ID )
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            try
            {
                //Primero eliminamos los que ya esten dados de alta
                Mi_SQL = " DELETE " + Cat_Com_Det_Part_Prov.Tabla_Cat_Com_Det_Part_Prov;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Det_Part_Prov.Campo_Proveedor_ID;
                Mi_SQL = Mi_SQL + "='" + Proveedor_ID.Trim() + "'";
            }
            catch (Exception Ex)
            {
                throw new Exception("Error generar la Elimiacion de Partida-Proveedor  Error[" + Ex.Message + "]");
            }
            return Mi_SQL;

        }
        ///****************************************************************************************
        /// NOMBRE DE LA FUNCION:  Baja_Detalle_Conceptos_Proveedor
        /// DESCRIPCION :          Genera la consulta que dara de baja los conceptos de un proveedor
        /// PARAMETROS  :          Datos: Variable que contiene los datos para la busqueda
        /// CREO        :          Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO  :         02/Octubre/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///****************************************************************************************/
        public static String Baja_Detalle_Conceptos_Proveedor(String Proveedor_ID)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            try
            {
                //Primero eliminamos los que ya esten dados de alta
                Mi_SQL = " DELETE " + Cat_Com_Giro_Proveedor.Tabla_Cat_Com_Giro_Proveedor;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Giro_Proveedor.Campo_Proveedor_ID;
                Mi_SQL = Mi_SQL + "='" + Proveedor_ID.Trim() + "'";
            }
            catch (Exception Ex)
            {
                throw new Exception("Error generar la Elimiacion de Concepto-Proveedor  Error[" + Ex.Message + "]");
            }
            return Mi_SQL;
        }
        ///****************************************************************************************
        /// NOMBRE DE LA FUNCION:  Borrar_His_Act_Prov
        /// DESCRIPCION :          Genera la consulta que borra el historial de un proveedor
        /// PARAMETROS  :          Datos: Variable que contiene los datos para la busqueda
        /// CREO        :          Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO  :         02/Octubre/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///****************************************************************************************/
        public static String Borrar_His_Act_Prov(String Proveedor_ID_Anterior)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            try
            {
                //Actualizacion del Id del Proveedor
                Mi_SQL = " DELETE " + Ope_Com_His_Autor_Prov.Tabla_Ope_Com_His_Autor_Prov;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_His_Autor_Prov.Campo_Proveedor_ID + " = '" + Proveedor_ID_Anterior.Trim() + "'";
            }
            catch (Exception Ex)
            {
                throw new Exception("Error generar la Elimiacion de Concepto-Proveedor  Error[" + Ex.Message + "]");
            }
            return Mi_SQL;
        }
        
        
    }
}
