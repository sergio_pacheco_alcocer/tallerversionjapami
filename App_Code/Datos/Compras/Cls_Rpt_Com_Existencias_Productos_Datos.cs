﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Rpt_Com_Existencias_productos.Negocio;

/// <summary>
/// Summary description for Cls_Rpt_Com_Existencias_Productos_Datos
/// </summary>
namespace JAPAMI.Rpt_Com_Existencias_productos.Datos
{
    public class Cls_Rpt_Com_Existencias_Productos_Datos
    {
        public Cls_Rpt_Com_Existencias_Productos_Datos()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Partidas_Especificas
        ///DESCRIPCIÓN: Consulta las partidas especificas registradas en el sistema
        ///PARAMETROS:  1.- Clase_Negocio, Objeto de la clase de negocios para obener los datos
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 18/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Partidas_Especificas(Cls_Rpt_Com_Existencias_Productos_Negocio Clase_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                Mi_SQL = "SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", ";
                Mi_SQL += Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + UPPER(" + Cat_Sap_Partidas_Especificas.Campo_Nombre + ") AS NOMBRE ";
                Mi_SQL += " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
                if(!String.IsNullOrEmpty(Clase_Negocio.P_Clave_Partida))
                {
                    Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Clave + " LIKE '%" + Clase_Negocio.P_Clave_Partida + "%' "; 
                }
                Mi_SQL += " ORDER BY " + Cat_Sap_Partidas_Especificas.Campo_Clave;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }
        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Productos
        ///DESCRIPCIÓN: Consulta las partidas especificas registradas en el sistema
        ///PARAMETROS:  1.- Clase_Negocio, Objeto de la clase de negocios para obener los datos
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 18/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Productos(Cls_Rpt_Com_Existencias_Productos_Negocio Clase_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                Mi_SQL = "SELECT " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Clave+ ", ";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Nombre + ",";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Descripcion + ", ";
                Mi_SQL += Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' +  ";
                Mi_SQL +=" UPPER(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ") AS PARTIDA, ";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Existencia + ", ";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Comprometido + ", ";
                Mi_SQL += Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Disponible;
                Mi_SQL += " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
                Mi_SQL += " ON " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Partida_ID + " = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Partida_ID))
                {
                    Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = '" + Clase_Negocio.P_Partida_ID + "' ";
                }
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Nombre_Producto)) 
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Nombre + " LIKE UPPER('%" + Clase_Negocio.P_Nombre_Producto + "%')";
                    }
                    else 
                    {
                        Mi_SQL += " WHERE " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Nombre + " LIKE UPPER('%" + Clase_Negocio.P_Nombre_Producto + "%')";
                    }
                }
                if(!String.IsNullOrEmpty(Clase_Negocio.P_Alamacen_General) && Clase_Negocio.P_Alamacen_General.Equals("SI"))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Almacen_General + " = 'SI' ";
                    }
                    else 
                    {
                        Mi_SQL += " WHERE " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Almacen_General + " = 'SI' ";
                    }
                }
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Alamacen_General) && Clase_Negocio.P_Alamacen_General.Equals("NO"))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND (" + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Almacen_General + " IS NULL ";
                        Mi_SQL += " OR " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Almacen_General + " = 'NO' )";
                    }
                    else 
                    {
                        Mi_SQL += " WHERE (" + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Almacen_General + " IS NULL ";
                        Mi_SQL += " OR " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Almacen_General + " = 'NO' )";
                    }
                }
                Mi_SQL += " ORDER BY " + Cat_Com_Productos.Tabla_Cat_Com_Productos +"." + Cat_Com_Productos.Campo_Nombre;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }
        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Clave_Partida_Especifica
        ///DESCRIPCIÓN: Consulta la clave de una partida
        ///PARAMETROS:  1.- Clase_Negocio, Objeto de la clase de negocios para obener los datos
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 18/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Clave_Partida_Especifica(Cls_Rpt_Com_Existencias_Productos_Negocio Clase_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                Mi_SQL = "SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", ";
                Mi_SQL += Cat_Sap_Partidas_Especificas.Campo_Clave;
                Mi_SQL += " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Partida_ID))
                {
                    Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = '" + Clase_Negocio.P_Partida_ID + "' ";
                }

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }
    }
}
