﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Seguimiento_Requisicion.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;
using JAPAMI.Administrar_Requisiciones.Negocios;
using System.Data.SqlClient;
using JAPAMI.Manejo_Presupuesto.Datos;

namespace JAPAMI.Seguimiento_Requisicion.Datos
{
    public class Cls_Ope_Com_Seguimiento_Requisiciones_Datos
    {
        public Cls_Ope_Com_Seguimiento_Requisiciones_Datos()
        {

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Historial_Requisicion
        ///DESCRIPCIÓN          : Consulta el historial de las requisisiones
        ///PARAMETROS           : Objeto de la clase Cls_Ope_Com_Seguimiento_Requisiciones_Negocio
        ///CREO                 : Jennyfer Ivonne Cceja Lemus
        ///FECHA_CREO           : 16/Septiembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Historial_Requisicion(Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Requisicion_Negocio)
        {
            try
            {
                String Mi_SQL = "";
                Mi_SQL =
                "SELECT * FROM " + Ope_Com_Historial_Req.Tabla_Ope_Com_Historial_Req +
                " WHERE " + Ope_Com_Historial_Req.Campo_No_Requisicion + " = " +
                Requisicion_Negocio.P_Requisicion_ID +
                " ORDER BY " + Ope_Com_Historial_Req.Campo_Fecha + " ASC";
                DataTable Data_Table =
                    SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Data_Table;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Requisiciones
        ///DESCRIPCIÓN: crea una sentencia sql para conultar una Requisa en la base de datos
        ///PARAMETROS: 1.-Clase de Negocio
        ///            2.-Usuario que crea la requisa
        ///CREO: Silvia Morales Portuhondo
        ///FECHA_CREO: Noviembre/2010 
        ///MODIFICO:Gustavo Angeles Cruz
        ///FECHA_MODIFICO: 25/Ene/2011
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        public static DataTable Consultar_Requisiciones(Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Requisicion_Negocio)
        {
            Requisicion_Negocio.P_Fecha_Inicial = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Inicial)) + " 00:00:00";
            Requisicion_Negocio.P_Fecha_Final = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Final)) + " 23:59:59";
            if (Requisicion_Negocio.P_Estatus == "CONST ,GENERADA, REVISAR")
            {
                Requisicion_Negocio.P_Estatus = "EN CONSTRUCCION,GENERADA,REVISAR,RECHAZADA";
            }
            Requisicion_Negocio.P_Estatus = Requisicion_Negocio.P_Estatus.Replace(",", "','");
            Requisicion_Negocio.P_Tipo = Requisicion_Negocio.P_Tipo.Replace(",", "','");
            String Mi_Sql = "";
            Mi_Sql =
            "SELECT " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + ".*, " +
                "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " +
                Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                ") NOMBRE_DEPENDENCIA " +
            " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
            " WHERE " +
            Ope_Com_Requisiciones.Campo_Estatus + " IN ('" + Requisicion_Negocio.P_Estatus + "')" +
            " AND " + Ope_Com_Requisiciones.Campo_Tipo + " IN ('" + Requisicion_Negocio.P_Tipo + "')";
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Dependencia_ID) && Requisicion_Negocio.P_Dependencia_ID != "0")
            {
                Mi_Sql += " AND " + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                " = '" + Requisicion_Negocio.P_Dependencia_ID + "'";
            }
            if (Requisicion_Negocio.P_Estatus == "GENERADA")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Generacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Generacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else if (Requisicion_Negocio.P_Estatus == "CANCELADA")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Cancelada + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Cancelada + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else if (Requisicion_Negocio.P_Estatus == "EN CONSTRUCCION")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Construccion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Construccion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else if (Requisicion_Negocio.P_Estatus == "AUTORIZADA")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else if (Requisicion_Negocio.P_Estatus == "CONFIRMADA")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Confirmacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Confirmacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else if (Requisicion_Negocio.P_Estatus == "COTIZADA")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else if (Requisicion_Negocio.P_Estatus == "REVISAR")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Autorizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else if (Requisicion_Negocio.P_Estatus == "COMPRA")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
                    "' AND " + Ope_Com_Requisiciones.Campo_Fecha_Cotizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            else
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Fecha_Creo + " >= '" + Requisicion_Negocio.P_Fecha_Inicial +
                    "' AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Fecha_Creo + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Requisicion_ID))
            {
                Mi_Sql +=
                " AND " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                " = " + Requisicion_Negocio.P_Requisicion_ID;
            }
            if (Requisicion_Negocio.P_Cotizador_ID != null && Requisicion_Negocio.P_Cotizador_ID != "0")
            {
                Mi_Sql +=
                " AND " + Ope_Com_Requisiciones.Campo_Cotizador_ID +
                " = '" + Requisicion_Negocio.P_Cotizador_ID + "'";
            }


            Mi_Sql = Mi_Sql + " ORDER BY " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " DESC";
            DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0)
            {
                return (Data_Set.Tables[0]);
            }
            else
            {
                return null;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Requisiciones_Generales
        ///DESCRIPCIÓN          : Consulta las requisisiones generales
        ///PARAMETROS           : Objeto de la clase Cls_Ope_Com_Seguimiento_Requisiciones_Negocio
        ///CREO                 : Jennyfer Ivonne Cceja Lemus
        ///FECHA_CREO           : 16/Septiembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Requisiciones_Generales(Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Requisicion_Negocio)
        {
            Requisicion_Negocio.P_Fecha_Inicial = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Inicial));
            Requisicion_Negocio.P_Fecha_Final = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Final));
            if (Requisicion_Negocio.P_Tipo == "TODOS")
            {
                Requisicion_Negocio.P_Tipo = "STOCK','TRANSITORIA";
            }
            String Mi_Sql = "";
            Mi_Sql =
            "SELECT " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + ".*, " +
            "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
            " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " +
            Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Dependencia_ID +
            ") NOMBRE_DEPENDENCIA,"
            + " (SELECT " + Cat_Com_Cotizadores.Campo_Nombre_Completo + " FROM " + Cat_Com_Cotizadores.Tabla_Cat_Com_Cotizadores +
            " WHERE " + Cat_Com_Cotizadores.Campo_Empleado_ID + " = " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." +
            Ope_Com_Requisiciones.Campo_Cotizador_ID + ") COTIZADOR," +
            " ISNULL(" + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Folio + ",'') AS OC" +
            " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " LEFT JOIN " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra +
            " ON " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " = " +
            Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra +
            " WHERE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Tipo_Articulo + " = '" + Requisicion_Negocio.P_Tipo_Articulo + "' AND " +
            Ope_Com_Requisiciones.Campo_Tipo + " IN ('" + Requisicion_Negocio.P_Tipo + "')" +
            " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Fecha_Creo +
            " between '" + Requisicion_Negocio.P_Fecha_Inicial + " 00:00:00' AND " +
            " '" + Requisicion_Negocio.P_Fecha_Final + " 23:59:00'";

            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Dependencia_ID) && Requisicion_Negocio.P_Dependencia_ID != "0")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Dependencia_ID + " IN (" +
                     "'" + Requisicion_Negocio.P_Dependencia_ID + "')";
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Requisicion_ID))
            {
                Mi_Sql +=
                " AND " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                " = " + Requisicion_Negocio.P_Requisicion_ID;
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Producto))
            {
                Mi_Sql +=
                " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                " IN (SELECT " + Ope_Com_Req_Producto.Campo_Requisicion_ID +
                " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto +
                " WHERE (" + Ope_Com_Req_Producto.Campo_Clave + " LIKE '%" + Requisicion_Negocio.P_Producto.Trim() + "%'" +
                " OR UPPER(" + Ope_Com_Req_Producto.Campo_Nombre_Producto_Servicio + ") LIKE UPPER('%" + Requisicion_Negocio.P_Producto.Trim() + "%')))";

            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Estatus))
            {
                Mi_Sql +=
                " AND " + Ope_Com_Requisiciones.Campo_Estatus +
                " IN ('" + Requisicion_Negocio.P_Estatus + "')";
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Cotizador_ID))
            {
                Mi_Sql +=
                    " AND " + Ope_Com_Requisiciones.Campo_Cotizador_ID +
                    " = '" + Requisicion_Negocio.P_Cotizador_ID + "' ";
            }
            Mi_Sql = Mi_Sql + " ORDER BY " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " DESC";
            DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0)
            {
                return (Data_Set.Tables[0]);
            }
            else
            {
                return null;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Requisiciones_Generales
        ///DESCRIPCIÓN          : Consulta las requisisiones generales
        ///PARAMETROS           : Objeto de la clase Cls_Ope_Com_Seguimiento_Requisiciones_Negocio
        ///CREO                 : Jennyfer Ivonne Cceja Lemus
        ///FECHA_CREO           : 16/Septiembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Requisiciones_Fechas_Bienes(Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Requisicion_Negocio)
        {
            //Requisicion_Negocio.P_Fecha_Inicial = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Inicial));
            //Requisicion_Negocio.P_Fecha_Final = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Final));
            if (Requisicion_Negocio.P_Tipo == "TODOS")
            {
                Requisicion_Negocio.P_Tipo = "STOCK','TRANSITORIA";
            }
            String Mi_Sql = "";
            Mi_Sql =
            "SELECT " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + ".*, " +
                "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " +
                Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                ") NOMBRE_DEPENDENCIA, '' ESTATUS_BIEN,"
                + " (SELECT " + Cat_Com_Cotizadores.Campo_Nombre_Completo + " FROM " + Cat_Com_Cotizadores.Tabla_Cat_Com_Cotizadores +
                " WHERE " + Cat_Com_Cotizadores.Campo_Empleado_ID + " = " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." +
                Ope_Com_Requisiciones.Campo_Cotizador_ID + ") COTIZADOR," +
                " ISNULL(" + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_Folio + ",'') AS OC" +
            " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " LEFT JOIN " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra +
            " ON " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " = " +
            Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + "." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra +
            " WHERE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Tipo_Articulo + " = '" + Requisicion_Negocio.P_Tipo_Articulo + "' AND " +
            Ope_Com_Requisiciones.Campo_Tipo + " IN ('" + Requisicion_Negocio.P_Tipo + "')" +
            " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Fecha_Creo +
                        " between '" + Requisicion_Negocio.P_Fecha_Inicial + " 00:00:00' AND " +

                        " '" + Requisicion_Negocio.P_Fecha_Final + " 23:59:00'" + " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " IS NOT NULL"; ;
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Dependencia_ID) && Requisicion_Negocio.P_Dependencia_ID != "0")
            {
                Mi_Sql = Mi_Sql + " AND " + Ope_Com_Requisiciones.Campo_Dependencia_ID + " IN (" +
                     "'" + Requisicion_Negocio.P_Dependencia_ID + "')";
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Requisicion_ID))
            {
                Mi_Sql +=
                " AND " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                " = " + Requisicion_Negocio.P_Requisicion_ID;
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_No_Orden_Compra))
            {
                Mi_Sql +=
                " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_No_Orden_Compra +
                " = " + Requisicion_Negocio.P_No_Orden_Compra;
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Estatus))
            {
                if (Requisicion_Negocio.P_Estatus == "ENTRADA")
                {
                    Mi_Sql +=
                        " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Fecha_Entrada +
                        " is not null ";
                    Mi_Sql +=
                        " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Fecha_Entrega +
                        " is null ";
                }
                if (Requisicion_Negocio.P_Estatus == "ENTREGADO")
                {
                    Mi_Sql +=
                        " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Fecha_Entrega +
                        " is not null ";
                }
                if (Requisicion_Negocio.P_Estatus == "RECIBIDO")
                {
                    Mi_Sql +=
                        " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Fecha_Recepcion +
                        " is not null ";
                    Mi_Sql +=
                        " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Fecha_Entrada +
                        " is null ";
                    Mi_Sql +=
                        " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Fecha_Entrega +
                        " is null ";
                    Mi_Sql +=
                        " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Fecha_Envio +
                        " is null ";
                }
                if (Requisicion_Negocio.P_Estatus == "ENVIADO")
                {
                    Mi_Sql +=
                        " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Fecha_Envio +
                        " is not null ";
                    Mi_Sql +=
                        " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Fecha_Entrada +
                        " is null ";
                    Mi_Sql +=
                        " AND " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + "." + Ope_Com_Requisiciones.Campo_Fecha_Entrega +
                        " is null ";
                }
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Cotizador_ID))
            {
                Mi_Sql +=
                    " AND " + Ope_Com_Requisiciones.Campo_Cotizador_ID +
                    " = '" + Requisicion_Negocio.P_Cotizador_ID + "' ";
            }
            Mi_Sql = Mi_Sql + " ORDER BY " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " DESC";
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql).Tables[0];
            foreach (DataRow Dt_row in Data_Table.Rows)
            {
                if (!String.IsNullOrEmpty(Dt_row[Ope_Com_Requisiciones.Campo_Empleado_Recepcion].ToString()))
                    Dt_row["ESTATUS_BIEN"] = "RECEPCIÓN";
                if (!String.IsNullOrEmpty(Dt_row[Ope_Com_Requisiciones.Campo_Empleado_Envio].ToString()))
                    Dt_row["ESTATUS_BIEN"] = "ENVIO";
                if (!String.IsNullOrEmpty(Dt_row[Ope_Com_Requisiciones.Campo_Empleado_Entrada].ToString()))
                    Dt_row["ESTATUS_BIEN"] = "ENTRADA";
                if (!String.IsNullOrEmpty(Dt_row[Ope_Com_Requisiciones.Campo_Empleado_Entrega].ToString()))
                    Dt_row["ESTATUS_BIEN"] = "ENTREGA";
                Dt_row.AcceptChanges();
            }
            return Data_Table;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Bienes_Requisiciones
        ///DESCRIPCIÓN          : Consulta bienes en las requisisiones de servicio
        ///PARAMETROS           : Objeto de la clase Cls_Ope_Com_Seguimiento_Requisiciones_Negocio
        ///CREO                 : Jesus Toledo Rdz
        ///FECHA_CREO           : 27/Junio/2013 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Bienes_Requisiciones(Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_Sql = "SELECT rb." + Ope_Com_Req_Bienes.Campo_Bien_Mueble_ID + " BIEN_MUEBLE_ID, rb." + Ope_Com_Req_Bienes.Campo_Requisicion_ID + " NO_REQUISICION";
            Mi_Sql += ", '' AS ESTATUS";
            Mi_Sql += ",rq." + Ope_Com_Requisiciones.Campo_Folio + " FOLIO";
            Mi_Sql += ",rq." + Ope_Com_Requisiciones.Campo_Dependencia_ID + " DEPENDENCIA_ID";
            Mi_Sql += ",rq." + Ope_Com_Requisiciones.Campo_Fecha_Entrada;
            Mi_Sql += ",rq." + Ope_Com_Requisiciones.Campo_Fecha_Entrega;
            Mi_Sql += ",rq." + Ope_Com_Requisiciones.Campo_Fecha_Envio;
            Mi_Sql += ",rq." + Ope_Com_Requisiciones.Campo_Fecha_Recepcion;
            Mi_Sql += ",rq." + Ope_Com_Requisiciones.Campo_Empleado_Entrada;
            Mi_Sql += ",rq." + Ope_Com_Requisiciones.Campo_Empleado_Entrega;
            Mi_Sql += ",rq." + Ope_Com_Requisiciones.Campo_Empleado_Envio;
            Mi_Sql += ",rq." + Ope_Com_Requisiciones.Campo_Empleado_Recepcion;
            Mi_Sql += ",CONVERT(char,rq." + Ope_Com_Requisiciones.Campo_No_Orden_Compra + ") NO_ORDEN_COMPRA";
            Mi_Sql += ",rq." + Ope_Com_Requisiciones.Campo_Estatus + " ESTATUS_RQ";
            Mi_Sql += ",bm." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie + " NUMERO_SERIE";
            Mi_Sql += ",bm." + Ope_Pat_Bienes_Muebles.Campo_Nombre + " NOMBRE";
            Mi_Sql += ",CONVERT(char,bm." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + ") NUMERO_INVENTARIO";
            Mi_Sql += ",bm." + Ope_Pat_Bienes_Muebles.Campo_Modelo + " MODELO";
            Mi_Sql += ",(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = rq." +
                Ope_Com_Requisiciones.Campo_Dependencia_ID +
                ") NOMBRE_DEPENDENCIA ";
            Mi_Sql += ",(SELECT " + Ope_Com_Ordenes_Compra.Campo_Nombre_Proveedor + " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra +
                " WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = rq." +
                Ope_Com_Requisiciones.Campo_No_Orden_Compra +
                ") NOMBRE_PROVEEDOR ";
            Mi_Sql += ", oc." + Ope_Com_Ordenes_Compra.Campo_Total + " AS IMPORTE_SERVICIO";
            Mi_Sql += ", oc." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo + " AS FECHA_ORDEN_SERVICIO";
            Mi_Sql += " FROM " + Ope_Com_Req_Bienes.Tabla_Ope_Com_Req_Bienes + " rb ";
            Mi_Sql += " LEFT JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " rq on rb." + Ope_Com_Req_Bienes.Campo_Requisicion_ID + " = rq." + Ope_Com_Requisiciones.Campo_Requisicion_ID;
            Mi_Sql += " LEFT JOIN " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " bm on rb." + Ope_Com_Requisiciones.Campo_Bien_Mueble_ID + " = bm." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID;
            Mi_Sql += " LEFT JOIN " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " oc on rq." + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " = oc." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;

            Mi_Sql += " WHERE " + "rq." + Ope_Com_Requisiciones.Campo_Tipo_Articulo + " = '" + Requisicion_Negocio.P_Tipo_Articulo + "' AND rq." +
            Ope_Com_Requisiciones.Campo_Tipo + " IN ('" + Requisicion_Negocio.P_Tipo + "') AND " + "rq." + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " IS NOT NULL";
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Dependencia_ID) && Requisicion_Negocio.P_Dependencia_ID != "0")
            {
                Mi_Sql = Mi_Sql + " AND rq." + Ope_Com_Requisiciones.Campo_Dependencia_ID + " IN (" +
                     "'" + Requisicion_Negocio.P_Dependencia_ID + "')";
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Requisicion_ID))
            {
                Mi_Sql +=
                " AND rq." + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                " = " + Requisicion_Negocio.P_Requisicion_ID;
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_No_Orden_Compra))
            {
                Mi_Sql +=
                " AND rq." + Ope_Com_Requisiciones.Campo_No_Orden_Compra +
                " = " + Requisicion_Negocio.P_No_Orden_Compra;
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Estatus_Bien))
            {
                if (Requisicion_Negocio.P_Estatus == "ENTRADA")
                {
                    Mi_Sql +=
                        " AND rq." + Ope_Com_Requisiciones.Campo_Fecha_Entrada +
                        " is not null ";
                    Mi_Sql +=
                        " AND rq." + Ope_Com_Requisiciones.Campo_Fecha_Entrega +
                        " is null ";
                }
                if (Requisicion_Negocio.P_Estatus == "ENTREGADO")
                {
                    Mi_Sql +=
                        " AND rq." + Ope_Com_Requisiciones.Campo_Fecha_Entrega +
                        " is not null ";
                }
                if (Requisicion_Negocio.P_Estatus == "RECIBIDO")
                {
                    Mi_Sql +=
                        " AND rq." + Ope_Com_Requisiciones.Campo_Fecha_Recepcion +
                        " is not null ";
                    Mi_Sql +=
                        " AND rq." + Ope_Com_Requisiciones.Campo_Fecha_Entrada +
                        " is null ";
                    Mi_Sql +=
                        " AND rq." + Ope_Com_Requisiciones.Campo_Fecha_Entrega +
                        " is null ";
                    Mi_Sql +=
                        " AND rq." + Ope_Com_Requisiciones.Campo_Fecha_Envio +
                        " is null ";
                }
                if (Requisicion_Negocio.P_Estatus == "ENVIADO")
                {
                    Mi_Sql +=
                        " AND rq." + Ope_Com_Requisiciones.Campo_Fecha_Envio +
                        " is not null ";
                    Mi_Sql +=
                        " AND rq." + Ope_Com_Requisiciones.Campo_Fecha_Entrada +
                        " is null ";
                    Mi_Sql +=
                        " AND rq." + Ope_Com_Requisiciones.Campo_Fecha_Entrega +
                        " is null ";
                }
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Cotizador_ID))
            {
                Mi_Sql +=
                    " AND rq." + Ope_Com_Requisiciones.Campo_Cotizador_ID +
                    " = '" + Requisicion_Negocio.P_Cotizador_ID + "' ";
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_No_Serie))
            {
                Mi_Sql +=
                    " AND bm." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie +
                    " LIKE '%" + Requisicion_Negocio.P_No_Serie + "%' ";
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Modelo))
            {
                Mi_Sql +=
                    " AND bm." + Ope_Pat_Bienes_Muebles.Campo_Modelo +
                    " LIKE '%" + Requisicion_Negocio.P_Modelo + "%' ";
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_No_Inventario))
            {
                Mi_Sql +=
                    " AND bm." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario +
                    " = '" + Requisicion_Negocio.P_No_Inventario + "' ";
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Nombre_Bien))
            {
                Mi_Sql +=
                    " AND bm." + Ope_Pat_Bienes_Muebles.Campo_Nombre +
                    " LIKE '%" + Requisicion_Negocio.P_Nombre_Bien + "%' ";
            }
            if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Bien_Id))
            {
                Mi_Sql +=
                    " AND bm." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID +
                    " in ('" + Requisicion_Negocio.P_Bien_Id + "') ";
            }
            Mi_Sql = Mi_Sql + " ORDER BY " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " DESC";
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql).Tables[0];
            foreach (DataRow Dt_row in Data_Table.Rows)
            {
                if (!String.IsNullOrEmpty(Dt_row[Ope_Com_Requisiciones.Campo_Empleado_Recepcion].ToString()))
                    Dt_row["ESTATUS"] = "RECEPCIÓN";
                if (!String.IsNullOrEmpty(Dt_row[Ope_Com_Requisiciones.Campo_Empleado_Envio].ToString()))
                    Dt_row["ESTATUS"] = "ENVIO";
                if (!String.IsNullOrEmpty(Dt_row[Ope_Com_Requisiciones.Campo_Empleado_Entrada].ToString()))
                    Dt_row["ESTATUS"] = "ENTRADA";
                if (!String.IsNullOrEmpty(Dt_row[Ope_Com_Requisiciones.Campo_Empleado_Entrega].ToString()))
                    Dt_row["ESTATUS"] = "ENTREGA";
                Dt_row.AcceptChanges();
            }
            return Data_Table;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Cotizadores
        ///DESCRIPCIÓN          : Consulta los Cotizadores
        ///PARAMETROS           : Objeto de la clase Cls_Ope_Com_Seguimiento_Requisiciones_Negocio
        ///CREO                 : Jennyfer Ivonne Cceja Lemus
        ///FECHA_CREO           : 16/Septiembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Cotizadores(Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Negocio)
        {
            String MiSql = String.Empty;

            try
            {

                MiSql = "SELECT " + Cat_Com_Cotizadores.Campo_Empleado_ID + ", ";
                MiSql += Cat_Com_Cotizadores.Campo_Nombre_Completo;
                MiSql += " FROM " + Cat_Com_Cotizadores.Tabla_Cat_Com_Cotizadores;
                if (!String.IsNullOrEmpty(Negocio.P_Tipo)) MiSql += " WHERE " + Cat_Com_Cotizadores.Campo_Tipo + " = '" + Negocio.P_Tipo + "'";
                MiSql += " ORDER BY NOMBRE_COMPLETO ";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Proveedor
        ///DESCRIPCIÓN          : Consulta los proveedores
        ///PARAMETROS           : Objeto de la clase Cls_Ope_Com_Seguimiento_Requisiciones_Negocio
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 07/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Proveedor(Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Negocio)
        {
            String MiSql = String.Empty;

            try
            {

                MiSql = "SELECT " + Ope_Com_Ordenes_Compra.Campo_Nombre_Proveedor;
                MiSql += " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra;
                MiSql += " WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Negocio.P_No_Orden_Compra;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Entradas
        ///DESCRIPCIÓN          : Consulta las entradas
        ///PARAMETROS           : Objeto de la clase Cls_Ope_Com_Seguimiento_Requisiciones_Negocio
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 07/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Entradas(Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Negocio)
        {
            String MiSql = String.Empty;

            try
            {

                MiSql = "SELECT CAST(" + Alm_Com_Entradas.Campo_No_Entrada + " AS INT) AS NO_ENTRADA, " + Alm_Com_Entradas.Campo_Fecha;
                MiSql += ", " + Alm_Com_Entradas.Campo_Total + ", " + Alm_Com_Entradas.Campo_Estatus;
                MiSql += " FROM " + Alm_Com_Entradas.Tabla_Alm_Com_Entradas;
                MiSql += " WHERE " + Alm_Com_Entradas.Campo_No_Requisicion + " = " + Negocio.P_Requisicion_ID;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Salidas
        ///DESCRIPCIÓN          : Consulta las salidas
        ///PARAMETROS           : Objeto de la clase Cls_Ope_Com_Seguimiento_Requisiciones_Negocio
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 07/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Salidas(Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Negocio)
        {
            String MiSql = String.Empty;

            try
            {

                MiSql = "SELECT CAST(" + Alm_Com_Salidas.Campo_No_Salida + " AS INT) AS NO_SALIDA, " + Alm_Com_Salidas.Campo_Fecha;
                MiSql += ", " + Alm_Com_Salidas.Campo_Total + ", " + Alm_Com_Salidas.Campo_Estatus;
                MiSql += " FROM " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas;
                MiSql += " WHERE " + Alm_Com_Salidas.Campo_Requisicion_ID + " = " + Negocio.P_Requisicion_ID;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Contrarecibos
        ///DESCRIPCIÓN          : Consulta los Contrarecibos
        ///PARAMETROS           : Objeto de la clase Cls_Ope_Com_Seguimiento_Requisiciones_Negocio
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 07/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Contra_Recibos(Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Negocio)
        {
            String MiSql = String.Empty;

            try
            {

                MiSql = "SELECT CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo + ", CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_Fecha_Pago;
                MiSql += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ JOIN " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " ORDENES";
                MiSql += " ON REQ." + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " = ORDENES." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                MiSql += " JOIN " + Ope_Alm_Contrarecibos.Tabla_Ope_Alm_Contrarecibos + " CONTRARECIBOS";
                MiSql += " ON ORDENES." + Ope_Com_Ordenes_Compra.Campo_No_Factura_Interno + " = CONTRARECIBOS." + Ope_Alm_Contrarecibos.Campo_No_Contra_Recibo;
                MiSql += " WHERE REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Negocio.P_Requisicion_ID;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Insertar_Fechas
        ///DESCRIPCIÓN          : Insertar fechas del seguimiento de la Rq
        ///PARAMETROS           : Objeto de la clase Cls_Ope_Com_Seguimiento_Requisiciones_Negocio
        ///CREO                 : Jesus Toledo Rdz
        ///FECHA_CREO           : 20/Junio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Insertar_Fechas(Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Datos)
        {
            String Mi_SQL = "";
            String Mensaje = "";
            SqlTransaction Trans = null;
            SqlCommand Cmd = new SqlCommand();
            SqlConnection Cn = new SqlConnection();
            DataTable Dt_Parametros = new DataTable();
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            try
            {
                if (Datos.P_Cmmd != null)
                {
                    Cmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                //Actualiza los datos de la tabla Asignacion de Proveedor donde estan los proveedores
                //para este servicio Externo con su estatus y su costo
                Mi_SQL = "UPDATE " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " SET ";
                if (Datos.P_Fecha_Envio != new DateTime())
                    Mi_SQL = Mi_SQL + Ope_Com_Requisiciones.Campo_Fecha_Envio + " = '" + string.Format("{0:dd/MM/yyyy hh:mm:ss}", Datos.P_Fecha_Envio) + "'";
                else
                    Mi_SQL = Mi_SQL + Ope_Com_Requisiciones.Campo_Fecha_Envio + " = null";
                if (Datos.P_Fecha_Entrega != new DateTime())
                    Mi_SQL = Mi_SQL + " , " + Ope_Com_Requisiciones.Campo_Fecha_Entrega + " = '" + string.Format("{0:dd/MM/yyyy hh:mm:ss}", Datos.P_Fecha_Entrega) + "'";
                else
                    Mi_SQL = Mi_SQL + " , " + Ope_Com_Requisiciones.Campo_Fecha_Entrega + " = null";
                if (Datos.P_Fecha_Entrada != new DateTime())
                    Mi_SQL = Mi_SQL + " , " + Ope_Com_Requisiciones.Campo_Fecha_Entrada + " = '" + string.Format("{0:dd/MM/yyyy hh:mm:ss}", Datos.P_Fecha_Entrada) + "'";
                else
                    Mi_SQL = Mi_SQL + " , " + Ope_Com_Requisiciones.Campo_Fecha_Entrada + " = null";
                if (Datos.P_Fecha_Recepcion != new DateTime())
                    Mi_SQL = Mi_SQL + " , " + Ope_Com_Requisiciones.Campo_Fecha_Recepcion + " = '" + string.Format("{0:dd/MM/yyyy hh:mm:ss}", Datos.P_Fecha_Recepcion) + "'";
                else
                    Mi_SQL = Mi_SQL + " , " + Ope_Com_Requisiciones.Campo_Fecha_Recepcion + " = null";
                if (!string.IsNullOrEmpty(Datos.P_Empleado_Recepcion))
                    Mi_SQL = Mi_SQL + " , " + Ope_Com_Requisiciones.Campo_Empleado_Recepcion + " = '" + Datos.P_Empleado_Recepcion + "'";
                else
                    Mi_SQL = Mi_SQL + " , " + Ope_Com_Requisiciones.Campo_Empleado_Recepcion + " = null";
                if (!string.IsNullOrEmpty(Datos.P_Empleado_Envio))
                    Mi_SQL = Mi_SQL + " , " + Ope_Com_Requisiciones.Campo_Empleado_Envio + " = '" + Datos.P_Empleado_Envio + "'";
                else
                    Mi_SQL = Mi_SQL + " , " + Ope_Com_Requisiciones.Campo_Empleado_Envio + " = null";
                if (!string.IsNullOrEmpty(Datos.P_Empleado_Entrada))
                    Mi_SQL = Mi_SQL + " , " + Ope_Com_Requisiciones.Campo_Empleado_Entrada + " = '" + Datos.P_Empleado_Entrada + "'";
                else
                    Mi_SQL = Mi_SQL + " , " + Ope_Com_Requisiciones.Campo_Empleado_Entrada + " = null";
                if (!string.IsNullOrEmpty(Datos.P_Empleado_Entrega))
                    Mi_SQL = Mi_SQL + " , " + Ope_Com_Requisiciones.Campo_Empleado_Entrega + " = '" + Datos.P_Empleado_Entrega + "'";
                else
                    Mi_SQL = Mi_SQL + " , " + Ope_Com_Requisiciones.Campo_Empleado_Entrega + " = null";


                Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = '" + Datos.P_Requisicion_ID + "'";
                //Se ejecuta la operacion
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //SE CARGAN LOS CAMBIOS QUE SE REALIZARÓN EN EL BIEN
                if (Datos.P_Dt_Cambios != null)
                {
                    if (Datos.P_Dt_Cambios.Rows.Count > 0)
                    {
                        Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Com_Historial_Cambios.Tabla_Ope_Com_Historial_Cambios, Ope_Com_Historial_Cambios.Campo_No_Registro, 10));
                        Int32 No_Cambio = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Com_Historial_Cambios.Tabla_Ope_Com_Historial_Cambios, Ope_Com_Historial_Cambios.Campo_No_Cambio, 10, Ope_Com_Historial_Cambios.Campo_No_Requisicion, Datos.P_Requisicion_ID, "", ""));
                        Int32 No_Detalle = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Com_Hist_Cam_Det.Tabla_Ope_Com_Hist_Cam_Det, Ope_Com_Hist_Cam_Det.Campo_No_Detalle, 10));
                        Mi_SQL = "INSERT INTO " + Ope_Com_Historial_Cambios.Tabla_Ope_Com_Historial_Cambios +
                                " ( " + Ope_Com_Historial_Cambios.Campo_No_Registro + ", " + Ope_Pat_Historial_Cambios.Campo_No_Cambio + ", " + Ope_Com_Historial_Cambios.Campo_Fecha_Hora_Cambio +
                                ", " + Ope_Com_Historial_Cambios.Campo_Empleado_Cambio_ID + ", " + Ope_Com_Historial_Cambios.Campo_No_Requisicion + ", " + Ope_Com_Historial_Cambios.Campo_Usuario_Creo + ", " + Ope_Com_Historial_Cambios.Campo_Fecha_Creo + ") " +
                                "VALUES ( '" + No_Registro + "','" + No_Cambio + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', '" + Datos.P_Requisicion_ID.Trim() + "', '" + Cls_Sessiones.Nombre_Empleado + "', GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        for (Int32 Contador = 0; Contador < (Datos.P_Dt_Cambios.Rows.Count); Contador++)
                        {
                            Mi_SQL = "INSERT INTO " + Ope_Com_Hist_Cam_Det.Tabla_Ope_Com_Hist_Cam_Det +
                                     " ( " + Ope_Com_Hist_Cam_Det.Campo_No_Detalle + ", " + Ope_Com_Hist_Cam_Det.Campo_No_Registro + ", " + Ope_Com_Hist_Cam_Det.Campo_Campo +
                                     ", " + Ope_Com_Hist_Cam_Det.Campo_Valor_Anterior + ", " + Ope_Com_Hist_Cam_Det.Campo_Valor_Nuevo + ") " +
                                     "VALUES ( '" + No_Detalle + "','" + No_Registro + "', '" + Datos.P_Dt_Cambios.Rows[Contador]["Campo"].ToString() +
                                     "', '" + Datos.P_Dt_Cambios.Rows[Contador]["Valor_Anterior"].ToString() + "', '" + Datos.P_Dt_Cambios.Rows[Contador]["Valor_Nuevo"].ToString() + "')";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            No_Detalle = No_Detalle + 1;
                        }
                    }
                }

                //Transaccion
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Detalles_Fechas_Requisicion
        ///DESCRIPCIÓN          : Obtener_Detalles_Fechas_Requisicion
        ///PARAMETROS           : Objeto de la clase Cls_Ope_Com_Seguimiento_Requisiciones_Negocio
        ///CREO                 : Jesus Toledo Rdz
        ///FECHA_CREO           : 20/Junio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Obtener_Detalles_Fechas_Requisicion(Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Datos)
        {
            Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Obj_Retornar = new Cls_Ope_Com_Seguimiento_Requisiciones_Negocio();
            String Mi_SQL = null;
            try
            {
                Mi_SQL = "SELECT " + Ope_Com_Requisiciones.Campo_Fecha_Recepcion;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Fecha_Envio;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Fecha_Entrada;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Fecha_Entrega;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Empleado_Recepcion;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Empleado_Envio;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Empleado_Entrada;
                Mi_SQL = Mi_SQL + ", " + Ope_Com_Requisiciones.Campo_Empleado_Entrega;
                Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = '" + Datos.P_Requisicion_ID + "'";
                SqlDataReader Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                while (Reader.Read())
                {
                    Obj_Retornar.P_Fecha_Recepcion = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones.Campo_Fecha_Recepcion].ToString())) ? Convert.ToDateTime(Reader[Ope_Com_Requisiciones.Campo_Fecha_Recepcion]) : new DateTime();
                    Obj_Retornar.P_Fecha_Envio = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones.Campo_Fecha_Envio].ToString())) ? Convert.ToDateTime(Reader[Ope_Com_Requisiciones.Campo_Fecha_Envio]) : new DateTime();
                    Obj_Retornar.P_Fecha_Entrada = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones.Campo_Fecha_Entrada].ToString())) ? Convert.ToDateTime(Reader[Ope_Com_Requisiciones.Campo_Fecha_Entrada]) : new DateTime();
                    Obj_Retornar.P_Fecha_Entrega = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones.Campo_Fecha_Entrega].ToString())) ? Convert.ToDateTime(Reader[Ope_Com_Requisiciones.Campo_Fecha_Entrega]) : new DateTime();
                    Obj_Retornar.P_Empleado_Recepcion = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones.Campo_Empleado_Recepcion].ToString())) ? Reader[Ope_Com_Requisiciones.Campo_Empleado_Recepcion].ToString() : "";
                    Obj_Retornar.P_Empleado_Envio = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones.Campo_Empleado_Envio].ToString())) ? Reader[Ope_Com_Requisiciones.Campo_Empleado_Envio].ToString() : "";
                    Obj_Retornar.P_Empleado_Entrada = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones.Campo_Empleado_Entrada].ToString())) ? Reader[Ope_Com_Requisiciones.Campo_Empleado_Entrada].ToString() : "";
                    Obj_Retornar.P_Empleado_Entrega = (!String.IsNullOrEmpty(Reader[Ope_Com_Requisiciones.Campo_Empleado_Entrega].ToString())) ? Reader[Ope_Com_Requisiciones.Campo_Empleado_Entrega].ToString() : "";
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Obj_Retornar;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARÁMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID)
        {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try
            {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            }
            catch (SqlException Ex)
            {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
        ///PARÁMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
        {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
            {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARÁMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID, String Campo_Condicion, String Valor_Condicion, String Campo_Condicion_2, String Valor_Condicion_2)
        {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try
            {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla + " WHERE " + Campo_Condicion + " = '" + Valor_Condicion + "'";
                if (!String.IsNullOrEmpty(Campo_Condicion_2) && !String.IsNullOrEmpty(Valor_Condicion_2))
                {
                    Mi_SQL += " AND " + Campo_Condicion_2 + " = '" + Valor_Condicion_2 + "'";
                }
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            }
            catch (SqlException Ex)
            {
                new Exception(Ex.Message);
            }
            return Id;
        }
    }
}
