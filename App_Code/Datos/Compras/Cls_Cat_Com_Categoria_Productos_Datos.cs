﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Categoria_Productos.Negocio;

/// <summary>
/// Summary description for Cls_Cat_Com_Categoria_Productos_Datos
/// </summary>
namespace JAPAMI.Categoria_Productos.Datos
{
    public class Cls_Cat_Com_Categoria_Productos_Datos
    {
        public Cls_Cat_Com_Categoria_Productos_Datos()
        {
        }

        #region (Metodos)
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consulta_Categoria_Productos
        /// 	DESCRIPCION:    Consultar las categorias de los productos
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Noe Mosqueda Valadez
        /// 	FECHA_CREO:     02/Mayo/2012 19:00
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        public static DataTable Consulta_Categoria_Productos(Cls_Cat_Com_Categoria_Productos_Negocio Datos)
        {
            //Declaracion de variables
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado
            String Mi_SQL = String.Empty; //Variable para las consutlas
            Boolean Where_Utilizado = false; //variable que indica si la clausula where ya ha sido utilizada

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT Categoria_ID, Nombre, Estatus, Descripcion FROM Cat_Com_Categoria_Productos ";
                
                //Verificar si se tiene el ID
                if (String.IsNullOrEmpty(Datos.P_Categoria_ID) == false)
                {
                    Mi_SQL += "WHERE Categoria_ID = '" + Datos.P_Categoria_ID + "' ";
                }
                else
                {
                    //verificar los otros criterios
                    if (String.IsNullOrEmpty(Datos.P_Busqueda) == false)
                    {
                        Mi_SQL += "WHERE Nombre LIKE '%" + Datos.P_Busqueda  + "%' ";
                        Mi_SQL += "OR Descripcion LIKE '%" + Datos.P_Busqueda + "%' ";

                        Where_Utilizado = true;
                    }

                    if (String.IsNullOrEmpty(Datos.P_Estatus) == false)
                    {
                        //verificar si la clausula where ya ha sido utilizada
                        if (Where_Utilizado == false)
                        {
                            Mi_SQL += "WHERE ";
                            Where_Utilizado = true;
                        }
                        else
                        {
                            Mi_SQL += "AND ";
                        }

                        //Resto de la consulta
                        Mi_SQL += " Estatus = '" + Datos.P_Estatus + "' ";
                    }
                }

                //Colocar el orden
                Mi_SQL += "ORDER BY Nombre";

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Alta_Categoria_Productos
        /// 	DESCRIPCION:    Dar de alta la categoria de los productos
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Noe Mosqueda Valadez
        /// 	FECHA_CREO:     03/Mayo/2012 12:45
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        public static void Alta_Categoria_Productos(Cls_Cat_Com_Categoria_Productos_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            Object Aux; //Variable auxiliar para las consultas
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Consulta para el ID de la categoria
                Mi_SQL = "SELECT MAX(Categoria_ID) FROM Cat_Com_Categoria_Productos ";

                //ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Aux = Obj_Comando.ExecuteScalar();

                //Verificar si tiene datos
                if (Aux != DBNull.Value)
                {
                    Datos.P_Categoria_ID = String.Format("{0:00000}", Convert.ToInt32(Aux) + 1);
                }
                else
                {
                    Datos.P_Categoria_ID = "00001";
                }

                //Consulta para la insercion de la categoria
                Mi_SQL = "INSERT INTO CAT_COM_CATEGORIA_PRODUCTOS (CATEGORIA_ID, NOMBRE, ESTATUS, DESCRIPCION, USUARIO_CREO, FECHA_CREO) " + 
                    "VALUES('" + Datos.P_Categoria_ID + "','" + Datos.P_Nombre + "','" + Datos.P_Estatus + "','" + Datos.P_Descripcion + "'," +
                    "'" + Datos.P_Usuario + "',GETDATE()) ";

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Ejecutar transaccion
                Obj_Transaccion.Commit();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Baja_Categoria_Productos
        /// 	DESCRIPCION:    Dar de baja la categoria de los productos
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Noe Mosqueda Valadez
        /// 	FECHA_CREO:     03/Mayo/2012 13:20
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        public static void Baja_Categoria_Productos(Cls_Cat_Com_Categoria_Productos_Negocio Datos)
        {            
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Asignar consulta
                Mi_SQL = "DELETE FROM Cat_Com_Categoria_Productos WHERE Categoria_ID = '" + Datos.P_Categoria_ID + "' ";

                //Ejecutar la consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //ejecutar la transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Cambio_Categoria_Productos
        /// 	DESCRIPCION:    Modificar la categoria de los productos
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Noe Mosqueda Valadez
        /// 	FECHA_CREO:     03/Mayo/2012 13:50
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        public static void Cambio_Categoria_Productos(Cls_Cat_Com_Categoria_Productos_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Asignar consulta para la modificacion
                Mi_SQL = "UPDATE CAT_COM_CATEGORIA_PRODUCTOS SET NOMBRE = '" + Datos.P_Nombre + "', ESTATUS = '" + Datos.P_Estatus + "', " +
                    "DESCRIPCION = '" + Datos.P_Descripcion + "', USUARIO_MODIFICO = '" + Datos.P_Usuario + "', FECHA_MODIFICO =GETDATE() " +
                    "WHERE Categoria_ID = '" + Datos.P_Categoria_ID + "' ";

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Ejecutar transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }
        #endregion
    }
}