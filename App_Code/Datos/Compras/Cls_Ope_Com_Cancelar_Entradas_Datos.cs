﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Cancelar_Entradas.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;
using System.Data.SqlClient;
using JAPAMI.Polizas.Negocios;

namespace JAPAMI.Cancelar_Entradas.Datos
{
    public class Cls_Ope_Com_Cancelar_Entradas_Datos
    {
        public Cls_Ope_Com_Cancelar_Entradas_Datos()
        {

        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Entradas
        ///DESCRIPCIÓN: Consulta las entradas
        ///PARAMETROS:  1.- Cls_Ope_Com_Cancelar_Entradas_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  05/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Entradas(Cls_Ope_Com_Cancelar_Entradas_Negocio Clase_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                //Creamos la consulta
                Mi_SQL = "SELECT CAST(ENTRADA." + Alm_Com_Entradas.Campo_No_Entrada + " AS INT) AS NO_ENTRADA, ORDENES." + Ope_Com_Ordenes_Compra.Campo_Folio;
                Mi_SQL += ", REQUISICION." + Ope_Com_Requisiciones.Campo_Folio + " AS REQ, ENTRADA." + Alm_Com_Entradas.Campo_Fecha + "";
                Mi_SQL += ", ENTRADA." + Alm_Com_Entradas.Campo_Estatus + ", ENTRADA." + Alm_Com_Entradas.Campo_Total;
                Mi_SQL += ", ENTRADA." + Alm_Com_Entradas.Campo_IVA + ", ENTRADA." + Alm_Com_Entradas.Campo_Subtotal;
                Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQUISICION, " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " ORDENES";
                Mi_SQL += ", " + Alm_Com_Entradas.Tabla_Alm_Com_Entradas + " ENTRADA";
                Mi_SQL += " WHERE REQUISICION." + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " = ";
                Mi_SQL += "ORDENES." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " AND ORDENES." + Ope_Com_Ordenes_Compra.Campo_Lista_Requisiciones + " = ";
                Mi_SQL += "ENTRADA." + Alm_Com_Entradas.Campo_No_Requisicion + " AND ENTRADA." + Alm_Com_Entradas.Campo_Estatus + " = 'GENERADA'";
                //Validamos los filtros por No de Contrarecibo
                if (!String.IsNullOrEmpty(Clase_Negocio.P_No_Entrada))
                {
                    Mi_SQL += " AND ENTRADA." + Alm_Com_Entradas.Campo_No_Entrada + " = " + Clase_Negocio.P_No_Entrada;
                }
                //Validamos los filtros por fechas
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Fecha_Inicio) && !String.IsNullOrEmpty(Clase_Negocio.P_Fecha_Fin))
                {
                    Clase_Negocio.P_Fecha_Inicio = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Clase_Negocio.P_Fecha_Inicio));
                    Clase_Negocio.P_Fecha_Fin = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Clase_Negocio.P_Fecha_Fin));

                    Mi_SQL += " AND ((ENTRADA." + Alm_Com_Entradas.Campo_Fecha + " BETWEEN '" + Clase_Negocio.P_Fecha_Inicio + " 00:00:00' AND ";
                    Mi_SQL += " '" + Clase_Negocio.P_Fecha_Fin + " 23:59:00') ";
                    Mi_SQL += " OR (ENTRADA." + Alm_Com_Entradas.Campo_Fecha_Creo + " BETWEEN '" + Clase_Negocio.P_Fecha_Inicio + " 00:00:00' AND ";
                    Mi_SQL += " '" + Clase_Negocio.P_Fecha_Fin + " 23:59:00'))";
                }
                Mi_SQL += " ORDER BY ENTRADA." + Alm_Com_Entradas.Campo_No_Entrada;
                //regresamos los valores
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las entradas. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Entradas_Detalles
        ///DESCRIPCIÓN: Consulta los detalles de la entrada
        ///PARAMETROS:  1.- Cls_Ope_Com_Cancelar_Entradas_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  05/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Entradas_Detalles(Cls_Ope_Com_Cancelar_Entradas_Negocio Clase_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                //Creamos la consulta
                Mi_SQL = "SELECT DETALLES.*, PRODUCTOS." + Cat_Com_Productos.Campo_Nombre + ", ENTRADAS." + Alm_Com_Entradas.Campo_Total + " AS Total";
                Mi_SQL += ", (DETALLES." + Alm_Com_Entradas_Detalles.Campo_Cantidad + " * DETALLES." + Alm_Com_Entradas_Detalles.Campo_Precio_U_SIN_IVA + ") AS Subtotal";
                Mi_SQL += " FROM " + Alm_Com_Entradas_Detalles.Tabla_Alm_Com_Entradas_Detalles + " DETALLES, " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PRODUCTOS";
                Mi_SQL += " LEFT OUTER JOIN " + Alm_Com_Entradas.Tabla_Alm_Com_Entradas + " ENTRADAS." + Alm_Com_Entradas.Campo_No_Entrada;
                Mi_SQL += " ON DETALLES." + Alm_Com_Entradas_Detalles.Campo_No_Entrada + " = ENTRADAS.";
                Mi_SQL += " WHERE DETALLES." + Alm_Com_Entradas_Detalles.Campo_Producto_ID + " = PRODUCTOS." + Cat_Com_Productos.Campo_Producto_ID;
                Mi_SQL += " AND DETALLES." + Alm_Com_Entradas_Detalles.Campo_No_Entrada + " = " + Clase_Negocio.P_No_Entrada;
                Mi_SQL += " AND ENTRADAS." + Alm_Com_Entradas.Campo_No_Entrada + " = " + Clase_Negocio.P_No_Entrada;

                //regresamos los valores
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los detalles de entrada. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cancelar_Entrada
        ///DESCRIPCIÓN          : Cancela la entrada
        ///PARAMETROS           :  
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 28/Enero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Cancelar_Entrada(Cls_Ope_Com_Cancelar_Entradas_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            SqlDataAdapter Da = new SqlDataAdapter();

            String No_Orden_Compra = String.Empty;
            Double Monto = 0.0;
            Double Monto_Total = 0.0;
            Double Existencia = 0.0;
            Double Cantidad = 0.0;
            Object Aux = new Object();

            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                //Consultamos la requisicion
                String Mi_SQL = "SELECT * FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + Clase_Negocio.P_No_Requisicion;
                Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = Cmd;
                DataTable Dt_Req = new DataTable();
                Da.Fill(Dt_Req);

                //Insertar un registro en la tabla de comentarios de entradas
                 Mi_SQL = "INSERT INTO " + Alm_Com_Entradas_Comentarios.Tabla_Alm_Com_Entradas_Comentarios +
                    " (" + Alm_Com_Entradas_Comentarios.Campo_No_Entrada +
                    ", " + Alm_Com_Entradas_Comentarios.Campo_Estatus +
                    ", " + Alm_Com_Entradas_Comentarios.Campo_Comentario +
                    ", " + Alm_Com_Entradas_Comentarios.Campo_Fecha +                    
                    ", " + Alm_Com_Entradas_Comentarios.Campo_Usuario_Creo +
                    ", " + Alm_Com_Entradas_Comentarios.Campo_Fecha_Creo +
                    ") VALUES ('" + Clase_Negocio.P_No_Entrada + "','" +
                     Clase_Negocio.P_Estatus + "','" +
                     Clase_Negocio.P_Comentario + "', GETDATE(), '" +
                     Cls_Sessiones.Nombre_Empleado + "'," +
                     "GETDATE() )";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Actualizamos la entrada  
                Mi_SQL = "UPDATE " + Alm_Com_Entradas.Tabla_Alm_Com_Entradas;
                Mi_SQL += " SET " + Alm_Com_Entradas.Campo_Estatus + " = '" + Clase_Negocio.P_Estatus + "', ";
                Mi_SQL += Alm_Com_Entradas.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "', ";
                Mi_SQL += Alm_Com_Entradas.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL += "WHERE " + Alm_Com_Entradas.Campo_No_Entrada + " = " + Clase_Negocio.P_No_Entrada;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Validamos que no venga en null la tabla de requisicion
                if ((Dt_Req != null) && (Dt_Req.Rows.Count > 0))
                {
                    Boolean Listado_Almacen = Dt_Req.Rows[0][Ope_Com_Requisiciones.Campo_Listado_Almacen].ToString().Trim() == "SI" ? true : false;
                    No_Orden_Compra = Dt_Req.Rows[0][Ope_Com_Requisiciones.Campo_No_Orden_Compra].ToString().Trim();

                    foreach (DataRow Dr_Producto in Clase_Negocio.P_Dt_Detalles.Rows)
                    {
                        Existencia = 0.0;
                        Cantidad = 0.0;
                        Mi_SQL = "SELECT " + Cat_Com_Productos.Campo_Existencia;
                        Mi_SQL += " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                        Mi_SQL += " WHERE " + Cat_Com_Productos.Campo_Producto_ID + " = '" + Dr_Producto[Alm_Com_Entradas_Detalles.Campo_Producto_ID].ToString() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Aux = Cmd.ExecuteScalar();

                        Double.TryParse(Aux.ToString(), out Existencia);
                        Double.TryParse(Dr_Producto[Alm_Com_Entradas_Detalles.Campo_Cantidad].ToString().Trim(), out Cantidad);

                        if (Existencia - Cantidad < 0)
                        {
                            //Trans.Rollback();
                            throw new Exception("Verificar la Existencia del producto " + Convert.ToInt64(Dr_Producto[Alm_Com_Entradas_Detalles.Campo_Producto_ID].ToString()));
                        }


                        Monto += Double.Parse(Dr_Producto[Alm_Com_Entradas_Detalles.Campo_Importe].ToString().Trim());

                        Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " SET ";
                        if (Listado_Almacen)
                        {
                            Mi_SQL += Cat_Com_Productos.Campo_Disponible + " = " + Cat_Com_Productos.Campo_Disponible + " - " + Cantidad + ", ";
                        }
                        Mi_SQL += Cat_Com_Productos.Campo_Existencia + " = " + Cat_Com_Productos.Campo_Existencia + " - " + Cantidad + ", ";
                        Mi_SQL += Cat_Com_Productos.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "', ";
                        Mi_SQL += Cat_Com_Productos.Campo_Fecha_Modifico + " = GETDATE() ";
                        Mi_SQL += "WHERE " + Cat_Com_Productos.Campo_Producto_ID + " = '" + Dr_Producto[Alm_Com_Entradas_Detalles.Campo_Producto_ID].ToString() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        Mi_SQL = "UPDATE " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " SET ";
                        Mi_SQL += Ope_Com_Req_Producto.Campo_Cantidad_Recibida + " = " + Ope_Com_Req_Producto.Campo_Cantidad_Recibida + " - " + Cantidad + ", ";
                        Mi_SQL += Ope_Com_Req_Producto.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "', ";
                        Mi_SQL += Ope_Com_Req_Producto.Campo_Fecha_Modifico + " = GETDATE() ";
                        Mi_SQL += "WHERE " + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = '" + Dr_Producto[Alm_Com_Entradas_Detalles.Campo_Producto_ID].ToString() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    Mi_SQL = "SELECT * FROM " + Alm_Com_Entradas.Tabla_Alm_Com_Entradas;
                    Mi_SQL += " WHERE " + Alm_Com_Entradas.Campo_No_Requisicion + " = " + Clase_Negocio.P_No_Requisicion;
                    Mi_SQL += " AND " + Alm_Com_Entradas.Campo_Estatus + " NOT IN ('CANCELADA')";
                    Cmd.CommandText = Mi_SQL;
                    Da.SelectCommand = Cmd;
                    DataTable Dt_Entradas = new DataTable();
                    Da.Fill(Dt_Entradas);

                    if (Dt_Entradas.Rows.Count < 1)
                    {
                        Mi_SQL = "UPDATE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra;
                        Mi_SQL += " SET " + Ope_Com_Ordenes_Compra.Campo_Estatus + " = 'PROVEEDOR_ENTERADO', ";
                        Mi_SQL += Ope_Com_Ordenes_Compra.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "', ";
                        Mi_SQL += Ope_Com_Ordenes_Compra.Campo_Fecha_Modifico + " = GETDATE() ";
                        Mi_SQL += "WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Dt_Req.Rows[0][Ope_Com_Requisiciones.Campo_No_Orden_Compra];
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                }

                Mi_SQL = "SELECT REQ_DET." + Ope_Com_Req_Producto.Campo_Partida_ID +
                             ",REQ_DET." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID +
                             ", (SELECT " + Ope_Com_Requisiciones.Campo_Total +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS TOTAL" +
                             ", (SELECT " + Ope_Com_Requisiciones.Campo_Total_Cotizado +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS TOTAL_COTIZADO" +
                             ", (SELECT " + Ope_Com_Requisiciones.Campo_Dependencia_ID +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS DEPENDENCIA_ID" +
                             ", REQ_DET." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID +
                             ", (SELECT NUM_RESERVA" +
                             " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + ") AS NUM_RESERVA" +
                             " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_DET" +
                             " WHERE REQ_DET." + Ope_Com_Req_Producto.Campo_Requisicion_ID + "='" + Clase_Negocio.P_No_Requisicion.Trim() + "'";

                Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = Cmd;
                DataTable Dt_Reserva = new DataTable();
                Da.Fill(Dt_Reserva);

                //Creamos el datatable del Codifo Programatico
                //Construimos el datatable
                DataTable Dt_Detalles = new DataTable();
                Dt_Detalles.Columns.Add("FUENTE_FINANCIAMIENTO_ID", typeof(System.String));
                Dt_Detalles.Columns.Add("PROGRAMA_ID", typeof(System.String));
                Dt_Detalles.Columns.Add("DEPENDENCIA_ID", typeof(System.String));
                Dt_Detalles.Columns.Add("PARTIDA_ID", typeof(System.String));
                Dt_Detalles.Columns.Add("ANIO", typeof(System.String));
                Dt_Detalles.Columns.Add("IMPORTE", typeof(System.String));
                DataRow Fila_Nueva = Dt_Detalles.NewRow();
                Fila_Nueva["FUENTE_FINANCIAMIENTO_ID"] = Dt_Reserva.Rows[0]["FUENTE_FINANCIAMIENTO_ID"]; //Requisicion_Negocio.P_Fuente_Financiamiento;
                Fila_Nueva["PROGRAMA_ID"] = Dt_Reserva.Rows[0]["PROYECTO_PROGRAMA_ID"]; //Requisicion_Negocio.P_Proyecto_Programa_ID;
                Fila_Nueva["DEPENDENCIA_ID"] = Dt_Reserva.Rows[0]["DEPENDENCIA_ID"];//Requisicion_Negocio.P_Dependencia_ID;
                Fila_Nueva["PARTIDA_ID"] = Dt_Reserva.Rows[0]["PARTIDA_ID"];//Requisicion_Negocio.P_Partida_ID;
                Fila_Nueva["ANIO"] = DateTime.Now.Year;
                Fila_Nueva["IMPORTE"] = Clase_Negocio.P_Total; ;//Requisicion_Negocio.P_Total;
                Dt_Detalles.Rows.Add(Fila_Nueva);
                Dt_Detalles.AcceptChanges();
                //Crear Reserva

                String No_Reserva = Dt_Req.Rows[0][Ope_Com_Requisiciones.Campo_Num_Reserva].ToString().Trim(); 
                
                int Registros = 0;
                
                //Comprometer presupuesto partida           
                if (Dt_Req.Rows[0][Ope_Com_Requisiciones.Campo_Tipo].ToString().Trim() == "TRANSITORIA")
                {
                    Registros = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO, Dt_Detalles, Cmd);
                    Cls_Ope_Psp_Manejo_Presupuesto.
                        Registro_Movimiento_Presupuestal(
                            No_Reserva,
                            Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO,
                            Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO,
                            Convert.ToDouble(Clase_Negocio.P_Total),
                            "",
                            "",
                            "",
                            "",
                            Cmd);
                }

                Mi_SQL = "SELECT " + Alm_Com_Entradas.Campo_Total + " FROM " + Alm_Com_Entradas.Tabla_Alm_Com_Entradas;
                Mi_SQL += " WHERE " + Alm_Com_Entradas.Campo_No_Entrada + " = " + Clase_Negocio.P_No_Entrada;
                Cmd.CommandText = Mi_SQL;
                Aux = Cmd.ExecuteScalar();

                Double.TryParse(Aux.ToString(), out Monto_Total);

                String Alta_poliza = Alta_Poliza(Clase_Negocio.P_No_Entrada, No_Orden_Compra, Clase_Negocio.P_No_Requisicion.Trim(), Monto, Monto_Total - Monto, Monto_Total, Cmd);
                if (Alta_poliza != "Alta_Exitosa")
                {
                    //Trans.Rollback();
                    throw new Exception(Alta_poliza);
                }


                Mensaje_Error = Clase_Negocio.P_No_Entrada;
                Trans.Commit();
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo cancelar la entrada";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Alta_Poliza
        /// DESCRIPCION:            Método utilizado consultar la información utilizada para mostrar la orden de salida
        /// PARAMETROS :            
        /// CREO       :            Salvador Hernández Ramírez
        /// FECHA_CREO :            24/Junio/2011  
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        private static String Alta_Poliza(String No_Entrada, String No_Orden_Compra, String No_Requisicion, Double Monto, Double IVA, Double Monto_Total, SqlCommand P_Cmd)
        {
            SqlDataAdapter Da = new SqlDataAdapter();
            DataTable Dt_Auxiliar = new DataTable();
            DataTable Dt_Cuentas_Almacen = new DataTable();
            DataTable Dt_Cuenta = new DataTable();
            String Mensaje = "";
            String Mi_SQL = "";
            String Tipo_Poliza_ID = "";
            Boolean Almacen_General = true;

            Mi_SQL = "SELECT * FROM " + Cat_Alm_Parametros_Cuentas.Tabla_Cat_Alm_Parametros_Cuentas;
            P_Cmd.CommandText = Mi_SQL;
            Da.SelectCommand = P_Cmd;
            Dt_Cuentas_Almacen = new DataTable();
            Da.Fill(Dt_Cuentas_Almacen);

            Mi_SQL = "SELECT DISTINCT(" + Ope_Com_Req_Producto.Campo_Tipo + ") FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto;
            Mi_SQL += " WHERE " + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion;
            P_Cmd.CommandText = Mi_SQL;
            Da.SelectCommand = P_Cmd;
            Dt_Auxiliar = new DataTable();
            Da.Fill(Dt_Auxiliar);

            if (Dt_Auxiliar.Rows.Count == 1 && Dt_Auxiliar.Rows[0][Ope_Com_Req_Producto.Campo_Tipo].ToString().Trim() == "PRODUCTO")
            {
                Mi_SQL = "SELECT DISTINCT(PROD." + Cat_Com_Productos.Campo_Almacen_General;
                Mi_SQL += ") FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PROD";
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PROD";
                Mi_SQL += " ON REQ_PROD." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = PROD." + Cat_Com_Productos.Campo_Producto_ID;
                Mi_SQL += " WHERE REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion;
                P_Cmd.CommandText = Mi_SQL;
                Da.SelectCommand = P_Cmd;
                Dt_Auxiliar = new DataTable();
                Da.Fill(Dt_Auxiliar);

                if (Dt_Auxiliar.Rows.Count == 1 && Dt_Auxiliar.Rows[0][Cat_Com_Productos.Campo_Almacen_General].ToString().Trim() == "NO")
                {
                    Almacen_General = false;
                }
            }

            Mi_SQL = "SELECT " + Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID;
            Mi_SQL += " FROM " + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas;
            Mi_SQL += " WHERE " + Cat_Con_Tipo_Polizas.Campo_Descripcion + " LIKE '%DIARIO%'";
            P_Cmd.CommandText = Mi_SQL;
            Tipo_Poliza_ID = P_Cmd.ExecuteScalar().ToString().Trim();

            DataTable Dt_Detalles_Poliza = new DataTable();
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("MOMENTO_FINAL", typeof(System.String));
            Dt_Detalles_Poliza.Columns.Add("Nombre_Cuenta", typeof(System.String));
            Dt_Detalles_Poliza.AcceptChanges();

            Mi_SQL = "SELECT REQ_PROD." + Ope_Com_Req_Producto.Campo_Partida_ID;
            Mi_SQL += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID;
            Mi_SQL += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID;
            Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID;
            Mi_SQL += " FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PROD";
            Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ";
            Mi_SQL += " ON REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL += " WHERE REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion;
            P_Cmd.CommandText = Mi_SQL;
            Da.SelectCommand = P_Cmd;
            Dt_Cuenta = new DataTable();
            Da.Fill(Dt_Cuenta);

            if (Dt_Cuenta.Rows.Count > 0)
            {
                if (Dt_Cuentas_Almacen.Rows.Count > 0)
                {
                    Mi_SQL = "SELECT * FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                    Mi_SQL += " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = ";
                    Mi_SQL += Almacen_General ?
                            Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_General_ID].ToString().Trim() :
                            Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_Papeleria_ID].ToString().Trim();

                    P_Cmd.CommandText = Mi_SQL;
                    Da.SelectCommand = P_Cmd;
                    Dt_Auxiliar = new DataTable();
                    Da.Fill(Dt_Auxiliar);

                    if (Dt_Auxiliar.Rows.Count > 0)
                    {
                        DataRow row = Dt_Detalles_Poliza.NewRow();

                        row = Dt_Detalles_Poliza.NewRow();
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                        row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();
                        row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                        row["Nombre_Cuenta"] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = No_Requisicion;
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0.0;
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = Monto;
                        row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Dt_Cuenta.Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim();
                        row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim();
                        Dt_Detalles_Poliza.Rows.Add(row);
                        Dt_Detalles_Poliza.AcceptChanges();

                        Mi_SQL = "SELECT * FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                        Mi_SQL += " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = ";
                        Mi_SQL += Dt_Cuentas_Almacen.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Pendiente].ToString().Trim();

                        P_Cmd.CommandText = Mi_SQL;
                        Da.SelectCommand = P_Cmd;
                        Dt_Auxiliar = new DataTable();
                        Da.Fill(Dt_Auxiliar);

                        if (Dt_Auxiliar.Rows.Count > 0)
                        {
                            row = Dt_Detalles_Poliza.NewRow();

                            row = Dt_Detalles_Poliza.NewRow();
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();
                            row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                            row["Nombre_Cuenta"] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = No_Requisicion;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0.0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = IVA;
                            row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                            row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Dt_Cuenta.Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString().Trim();
                            row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim();
                            row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim();
                            Dt_Detalles_Poliza.Rows.Add(row);
                            Dt_Detalles_Poliza.AcceptChanges();

                            Mi_SQL = "SELECT ORDEN_COM." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID;
                            Mi_SQL += ", PROV." + Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID;
                            Mi_SQL += ", CUENTAS." + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                            Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ";
                            Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " ORDEN_COM";
                            Mi_SQL += " ON REQ." + Ope_Com_Requisiciones.Campo_No_Orden_Compra + " = ORDEN_COM." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
                            Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROV";
                            Mi_SQL += " ON ORDEN_COM." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + " = PROV." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                            Mi_SQL += " LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTAS";
                            Mi_SQL += " ON PROV." + Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID + " = CUENTAS." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                            Mi_SQL += " WHERE REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + No_Requisicion;
                            P_Cmd.CommandText = Mi_SQL;
                            Da.SelectCommand = P_Cmd;
                            P_Cmd.CommandText = Mi_SQL;
                            Da.SelectCommand = P_Cmd;
                            Dt_Auxiliar = new DataTable();
                            Da.Fill(Dt_Auxiliar);

                            if (Dt_Auxiliar.Rows.Count > 0)
                            {
                                row = Dt_Detalles_Poliza.NewRow();
                                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 3;
                                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Auxiliar.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID].ToString().Trim();
                                row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                                row["Nombre_Cuenta"] = Dt_Auxiliar.Rows[0][Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString().Trim();
                                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = No_Requisicion;
                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Monto_Total;
                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0.0;
                                row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                                row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = Dt_Cuenta.Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString().Trim();
                                row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim();
                                row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = Dt_Cuenta.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim();
                                Dt_Detalles_Poliza.Rows.Add(row);
                                Dt_Detalles_Poliza.AcceptChanges();


                                Cls_Ope_Con_Polizas_Negocio Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio();

                                Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = Tipo_Poliza_ID;
                                Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now);
                                Rs_Alta_Ope_Con_Polizas.P_Fecha_Poliza = DateTime.Now;
                                Rs_Alta_Ope_Con_Polizas.P_Concepto = "Cancelar Entrada " + Int64.Parse(No_Entrada) + ", OC-" + No_Orden_Compra + ", RQ-" + No_Requisicion.Trim() + ", Almacen ";
                                //Concatenamos si es almacen general o papeleria
                                Rs_Alta_Ope_Con_Polizas.P_Concepto += Almacen_General ? "General " : "Papeleria ";
                                Rs_Alta_Ope_Con_Polizas.P_Total_Debe = Monto_Total; //monto total con iva de productos
                                Rs_Alta_Ope_Con_Polizas.P_Total_Haber = Monto_Total;
                                Rs_Alta_Ope_Con_Polizas.P_No_Partida = 3;
                                Rs_Alta_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                                Rs_Alta_Ope_Con_Polizas.P_Dt_Detalles_Polizas = Dt_Detalles_Poliza;
                                Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID;
                                Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Autorizo = Cls_Sessiones.Empleado_ID;
                                Rs_Alta_Ope_Con_Polizas.P_Prefijo = "";
                                Rs_Alta_Ope_Con_Polizas.P_Cmmd = P_Cmd;
                                string[] Datos_Poliza = Rs_Alta_Ope_Con_Polizas.Alta_Poliza();
                                Mensaje = "Alta_Exitosa";
                            }
                        }
                        else
                        {
                            Mensaje = "No se encontraron los datos de la cuenta contable para el IVA pendiente de acreditar.";
                        }
                    }
                    else
                    {
                        Mensaje = "No se encontraron los datos de la cuenta contable ";
                        Mensaje += Almacen_General ? "General " : "Papeleria ";
                        Mensaje += "de Almacen.";
                    }
                }
                else
                {
                    Mensaje = "No se han asignado las cuentas contables General ó Papeleria para Almacen.";
                }
            }
            else
            {
                Mi_SQL = "SELECT " + Ope_Com_Requisiciones.Campo_Codigo_Programatico;
                Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
                Mi_SQL += " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + No_Requisicion;
                P_Cmd.CommandText = Mi_SQL;
                Mensaje = "No se encontro la cuenta contable del gasto en el presupuesto aprobado para este año. Codigo Programatico: ";
                Mensaje += P_Cmd.ExecuteScalar().ToString().Trim();
            }
            return Mensaje;
        }
    }
}
