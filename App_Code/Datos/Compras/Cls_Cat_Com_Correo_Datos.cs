﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Bitacora_Eventos;
using JAPAMI.Constantes;
using JAPAMI.Correo.Negocio;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Text;
///malo compu///
namespace JAPAMI.Correo.Datos
{
    public class Cls_Cat_Com_Correo_Datos
    {
        
        #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Correo
        ///DESCRIPCIÓN: Consulta el correo  
        ///PARAMETROS:  1.- Cls_Cat_Com_Correo_Negocio
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Correo(Cls_Cat_Com_Correo_Negocio Correo)
        {
            String Mi_SQL = "SELECT USUARIO_CORREO, CORREO_SALIENTE, PASSWORD_CORREO, SERVIDOR_CORREO FROM APL_PARAMETROS ";
            //Sentencia que ejecuta el query
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            
            return Data_Table;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Correo
        ///DESCRIPCIÓN: Agregamos el correo  
        ///PARAMETROS:  1.- Cls_Cat_Com_Correo_Negocio
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Alta_Correo(Cls_Cat_Com_Correo_Negocio Correo)
        {
            try
            {

                String Mi_SQL = "INSERT INTO APL_PARAMETROS (USUARIO_CORREO, CORREO_SALIENTE, PASSWORD_CORREO, SERVIDOR_CORREO) VALUES"
                              + "('Departamento Compras', '" + Correo.P_Correo + "', '" + Correo.P_Password + "', '" + Correo.P_Direccion_IP + "')";
            //Sentencia que ejecuta el query
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Correo
        ///DESCRIPCIÓN: Modificamos el correo
        ///PARAMETROS:  1.- Cls_Cat_Com_Correo_Negocio
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Modificar_Correo(Cls_Cat_Com_Correo_Negocio Correo)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
                 //Ejecutar consulta

            Mi_SQL = "UPDATE APL_PARAMETROS SET "
                   + "CORREO_SALIENTE = '" + Correo.P_Correo + "', "
                   + "PASSWORD_CORREO = '" + Correo.P_Password + "', "
                   + "SERVIDOR_CORREO = '" + Correo.P_Direccion_IP + "'";
            //Sentencia que ejecuta el query 
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Imagenes
        ///DESCRIPCIÓN: Consulta las imagenes  
        ///PARAMETROS:  1.- Cls_Cat_Com_Correo_Negocio
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 17/Enero/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Imagenes(Cls_Cat_Com_Correo_Negocio Correo)
        {
            String Mi_SQL = "SELECT 'Jefe Compras' AS Firma, Firma_JEFE_Compras_IMG AS Ruta"
                          + " FROM CAT_COM_PARAMETROS"
                          + " UNION"
                          + " SELECT 'Gerente Administrativo' AS Firma, Firma_GERENTE_ADMIN_IMG AS Ruta"
                          + " FROM CAT_COM_PARAMETROS";
            //Sentencia que ejecuta el query
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            return Data_Table;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Imagen
        ///DESCRIPCIÓN: Agregamos la imagen 
        ///PARAMETROS:  1.- Cls_Cat_Com_Correo_Negocio
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 17/Enero/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Alta_Imagen(Cls_Cat_Com_Correo_Negocio Correo)
        {
            try
            {

                String Mi_SQL = "INSERT INTO CAT_COM_PARAMETROS (PARAMETRO_ID, Firma_JEFE_Compras_IMG, Firma_GERENTE_ADMIN_IMG) VALUES('00001',";
                if (!String.IsNullOrEmpty(Correo.P_Ruta_Compras))
                    Mi_SQL += "'" + Correo.P_Ruta_Compras + "',";
                else
                    Mi_SQL += "'',";
                if (!String.IsNullOrEmpty(Correo.P_Ruta_Gerencia))
                    Mi_SQL += "'" + Correo.P_Ruta_Gerencia + "'";
                else
                    Mi_SQL += "''";
                Mi_SQL += ")";

                //Sentencia que ejecuta el query
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Imagen
        ///DESCRIPCIÓN: Modificamos la imagen
        ///PARAMETROS:  1.- Cls_Cat_Com_Correo_Negocio
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 17/Enero/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Modificar_Imagen(Cls_Cat_Com_Correo_Negocio Correo)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            //Ejecutar consulta

            Mi_SQL = "UPDATE CAT_COM_PARAMETROS SET "
                   + Correo.P_Campo + " = '" + Correo.P_Ruta_Compras + "'";                   
            //Sentencia que ejecuta el query 
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        }
        #endregion        
    }
}
