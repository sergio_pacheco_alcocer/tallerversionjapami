﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Rpt_Com_Pedidos_Proproductos.Negocios;


/// <summary>
/// Summary description for Cls_Rpt_Com_Pedidos_Productos
/// </summary>
/// 
namespace JAPAMI.Rpt_Com_Pedidos_Proproductos.Datos
{
    public class Cls_Rpt_Com_Pedidos_Productos_Datos
    {
        public Cls_Rpt_Com_Pedidos_Productos_Datos()
        {

        }
        #region METODOS
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:Consultar_Unidad_Responsable
        /// DESCRIPCION         :Realizar la consulta de la unidad responsable a la que pertenece el usuario
        /// PARAMETROS          :            
        /// CREO                :Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO          :16/Octubre/2012 05:48pm
        /// MODIFICO            :     
        /// FECHA_MODIFICO      : 
        /// CAUSA_MODIFICACION  :     
        ///*******************************************************************************/
        public static DataTable Consultar_Unidad_Responsable(Cls_Rpt_Com_Pedidos_Productos_Negocio Datos_Negocio)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;

            try
            {
                //Consulta para encontrar una dependencia
                Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + ",";
                Mi_SQL += Cat_Dependencias.Campo_Clave + "+ ' ' + " + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                Mi_SQL += " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
                Mi_SQL += " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Datos_Negocio.P_Dependencia_ID + "'";
                //Ordenar
                Mi_SQL += " ORDER BY " + Cat_Dependencias.Campo_Nombre;
                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Gerencias
        ///DESCRIPCIÓN  : Obtiene la partdas que tienen presupuesto
        ///PARAMETROS   : 1.-Variable de la clase de negocios
        ///            
        ///CREO         :Susana Trigueros
        ///FECHA_CREO   : 11/Octubre/2012
        ///MODIFICO     :
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        //CONSULTAR PARTIDAS DE UN PROGRAMA
        public static DataTable Consultar_Gerencias(Cls_Rpt_Com_Pedidos_Productos_Negocio Reporte_Negocio)
        {
            try
            {
                String Mi_SQL = "SELECT " + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + ", " +
               " " + Cat_Grupos_Dependencias.Campo_Clave + " +' '+" +
               " " + Cat_Grupos_Dependencias.Campo_Nombre + " AS GERENCIA " +
               " FROM " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias;                            

                DataTable Data_Table =
                    SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Data_Table;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Partidas_De_Un_Programa
        ///DESCRIPCIÓN  : Obtiene la partdas que tienen presupuesto
        ///PARAMETROS   : 1.-Variable de la clase de negocios
        ///            
        ///CREO         :Susana Trigueros
        ///FECHA_CREO   : 11/Octubre/2012
        ///MODIFICO     :
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        //CONSULTAR PARTIDAS DE UN PROGRAMA
        public static DataTable Consultar_Partidas_De_Un_Programa(Cls_Rpt_Com_Pedidos_Productos_Negocio Reporte_Negocio)
        {
            try 
            {
                String Mi_SQL = "SELECT PARTIDA." + Cat_Com_Partidas.Campo_Partida_ID + ", " +
               " PARTIDA." + Cat_Com_Partidas.Campo_Clave + " +' '+" +
               " PARTIDA." + Cat_Com_Partidas.Campo_Nombre + " AS PARTIDA " +
               " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas + " PARTIDA" +
               " JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " PARTIDA_GENERICA" +
               " ON PARTIDA_GENERICA." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + "=" +
               " PARTIDA." + Cat_Com_Partidas.Campo_Partida_Generica_ID +
               " JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " CONCEPTO" +
               " ON CONCEPTO." + Cat_Sap_Concepto.Campo_Concepto_ID + "= PARTIDA_GENERICA." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID +
               " JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " CAPITULO" +
               " ON CAPITULO." + Cat_SAP_Capitulos.Campo_Capitulo_ID + " =CONCEPTO." + Cat_Sap_Concepto.Campo_Capitulo_ID +
               " JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " DETALLE" +
               " ON PARTIDA." + Cat_Com_Partidas.Campo_Partida_ID + " = " +
               " DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID +
               " WHERE DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " IN( " +
                   " SELECT " + Cat_SAP_Det_Prog_Dependencia.Campo_Proyecto_Programa_ID + " FROM " +
                   Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia +
                   " WHERE " + Cat_SAP_Det_Prog_Dependencia.Campo_Dependencia_ID + " = '" + Reporte_Negocio.P_Dependencia_ID + "') " +
               " AND DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " =" +
               "'" + Reporte_Negocio.P_Dependencia_ID + "'" +
               " AND DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " IN(" +
                   " SELECT " + Cat_SAP_Det_Fte_Dependencia.Campo_Fuente_Financiamiento_ID + " FROM " +
                   Cat_SAP_Det_Fte_Dependencia.Tabla_Cat_SAP_Det_Fte_Financiamiento_Dependencia +
                   " WHERE " + Cat_SAP_Det_Fte_Dependencia.Campo_Dependencia_ID + " = '" + Reporte_Negocio.P_Dependencia_ID + "')" +
    //" AND DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio +
    //           "=" + DateTime.Today.Year +           
               " AND CAPITULO." + Cat_SAP_Capitulos.Campo_Capitulo_ID + "!='00001'";
                    ////Condicion para traer las  partidas de los prosuctos que son de STOCK
                    //Mi_SQL += " AND " + " PARTIDA." + Cat_Com_Partidas.Campo_Partida_ID + " IN " +
                    //        "(SELECT DISTINCT(" + Cat_Com_Productos.Campo_Partida_ID + ") FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                    //        " WHERE " + Cat_Com_Productos.Campo_Stock + " = 'SI')";
                //Fin Condición
                Mi_SQL += " GROUP BY PARTIDA." + Cat_Com_Partidas.Campo_Partida_ID + ",  PARTIDA." + Cat_Com_Partidas.Campo_Clave +
                    ", PARTIDA." + Cat_Com_Partidas.Campo_Nombre + ",DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID +
                    ", DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID;
                    //", DETALLE." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio;

                Mi_SQL += " ORDER BY PARTIDA." + Cat_Com_Partidas.Campo_Clave + " ASC";

                DataTable Data_Table =
                    SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Data_Table;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:Consultar_Productos_Por_Partida
        /// DESCRIPCION         :Obtiene los productos registrados en el sistema, segun la partida 
        ///                      a la que pertenescan y el usuarioo seleccione
        /// PARAMETROS          :            
        /// CREO                :Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO          :16/Octubre/2012 05:48pm
        /// MODIFICO            :     
        /// FECHA_MODIFICO      : 
        /// CAUSA_MODIFICACION  :     
        ///*******************************************************************************/
        public static DataTable Consultar_Productos_Por_Partida(Cls_Rpt_Com_Pedidos_Productos_Negocio Datos_Negocio)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;

            try
            {
                //Consulta para encontrar una dependencia
                Mi_SQL = "SELECT " + Cat_Com_Productos.Campo_Producto_ID + ",ISNULL(";
                Mi_SQL += Cat_Com_Productos.Campo_Clave + " + " + Cat_Com_Productos.Campo_Nombre + " + ' ' + " + Cat_Com_Productos.Campo_Descripcion + ", 'DATOS DEL PRODUCTO NO DISPONIBLES') AS PRODUCTO ";
                Mi_SQL += " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                Mi_SQL += " WHERE " + Cat_Com_Productos.Campo_Partida_ID + " = '" + Datos_Negocio.P_Partida_Especifica_ID + "'";
                Mi_SQL += " AND " + Cat_Com_Productos.Campo_Estatus + " = 'ACTIVO'";
                //Ordenar
                Mi_SQL += " ORDER BY " + Cat_Com_Productos.Campo_Nombre;
                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:Consultar_Pedido_Productos
        /// DESCRIPCION         :Consulta los pedidos de una unidad responsables segun los filtros seleccionados
        ///                     :por el usuario
        /// PARAMETROS          :            
        /// CREO                :Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO          :17/Octubre/2012 12:33pm
        /// MODIFICO            :     
        /// FECHA_MODIFICO      : 
        /// CAUSA_MODIFICACION  :     
        ///*******************************************************************************/
        public static DataTable Consultar_Pedido_Productos(Cls_Rpt_Com_Pedidos_Productos_Negocio Datos_Negocio)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;
            Boolean Entro_Where = false;

            try
            {
                //Consulta para encontrar una dependencia
                Mi_SQL = "SELECT REQ." + Ope_Com_Requisiciones.Campo_Folio;
                Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_No_Orden_Compra;
                Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_Estatus;
                Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_Tipo;
                Mi_SQL += ", PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA";
                Mi_SQL += ", GER." + Cat_Grupos_Dependencias.Campo_Clave + "+ ' ' + GER." + Cat_Grupos_Dependencias.Campo_Nombre + " AS GERENCIA";
                Mi_SQL += ", (SELECT " + Cat_Dependencias.Campo_Clave + "+ ' ' +" + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + "= REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID +") AS DEPARTAMENTO";
                Mi_SQL += ", PROD." + Cat_Com_Productos.Campo_Clave + " + ' ' + PROD." + Cat_Com_Productos.Campo_Nombre + " + ' - ' + PROD." + Cat_Com_Productos.Campo_Descripcion + " AS PRODUCTO";
                Mi_SQL += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Cantidad;
                Mi_SQL += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Importe;
                Mi_SQL += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Monto_Total;
                Mi_SQL += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado;
                Mi_SQL += ", REQ_PROD." + Ope_Com_Req_Producto.Campo_Total_Cotizado;
                Mi_SQL += ", SALIDAS." + Alm_Com_Salidas.Campo_No_Salida;
                Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_Fecha_Creo;
                Mi_SQL += ", REQ." + Ope_Com_Requisiciones.Campo_Usuario_Creo;
                Mi_SQL += ", PROV." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR";
                Mi_SQL += ", EMPLEADOS." + Cat_Empleados.Campo_Nombre + " + ' ' + EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + " + ' ' + EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + " AS RECIBIO";

                Mi_SQL += " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " REQ";

                Mi_SQL += " LEFT OUTER JOIN " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " REQ_PROD";
                Mi_SQL += " ON REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = REQ_PROD." + Ope_Com_Req_Producto.Campo_Requisicion_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROV";
                Mi_SQL += " ON PROV." + Cat_Com_Proveedores.Campo_Proveedor_ID + " = REQ_PROD." + Ope_Com_Req_Producto.Campo_Proveedor_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " PROD";
                Mi_SQL += " ON REQ_PROD." + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " = PROD." + Cat_Com_Productos.Campo_Producto_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDAS";
                Mi_SQL += " ON REQ_PROD." + Ope_Com_Req_Producto.Campo_Partida_ID + " = PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Alm_Com_Salidas.Tabla_Alm_Com_Salidas + " SALIDAS";
                Mi_SQL += " ON REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = SALIDAS." + Alm_Com_Salidas.Campo_Requisicion_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS";
                Mi_SQL += " ON EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + " = SALIDAS." + Alm_Com_Salidas.Campo_Empleado_Solicito_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP";
                Mi_SQL += " ON DEP." + Cat_Dependencias.Campo_Dependencia_ID + " = REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID;

                Mi_SQL += " LEFT OUTER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + " GER";
                Mi_SQL += " ON GER." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + " = DEP." + Cat_Dependencias.Campo_Grupo_Dependencia_ID;

                if (!String.IsNullOrEmpty(Datos_Negocio.P_Dependencia_ID))
                {
                    Mi_SQL += Entro_Where ? " AND REQ." : " WHERE REQ."; Entro_Where = true;
                    Mi_SQL += Ope_Com_Requisiciones.Campo_Dependencia_ID + " = '" + Datos_Negocio.P_Dependencia_ID + "'";
                }
                if (!String.IsNullOrEmpty(Datos_Negocio.P_Gerencia_ID))
                {
                    Mi_SQL += Entro_Where ? " AND REQ." : " WHERE REQ."; Entro_Where = true;
                    Mi_SQL += Ope_Com_Requisiciones.Campo_Dependencia_ID + " IN (SELECT " + Cat_Dependencias.Campo_Dependencia_ID + "  FROM ";
                    Mi_SQL += Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Grupo_Dependencia_ID;
                    Mi_SQL += "='" + Datos_Negocio.P_Gerencia_ID + "')";
                }
                if (!String.IsNullOrEmpty(Datos_Negocio.P_Partida_Especifica_ID))
                {
                    Mi_SQL += Entro_Where ? " AND REQ_PROD." : " WHERE REQ_PROD."; Entro_Where = true;
                    Mi_SQL += Ope_Com_Req_Producto.Campo_Partida_ID + " = '" + Datos_Negocio.P_Partida_Especifica_ID + "'";
                }
                if (!String.IsNullOrEmpty(Datos_Negocio.P_Producto_ID))
                {
                    Mi_SQL += Entro_Where ? " AND PROD." : " WHERE PROD."; Entro_Where = true;
                    Mi_SQL += Cat_Com_Productos.Campo_Producto_ID + " = '" + Datos_Negocio.P_Producto_ID + "'";
                }
                if (!String.IsNullOrEmpty(Datos_Negocio.P_Fecha_Inicio) && !String.IsNullOrEmpty(Datos_Negocio.P_Fecha_Fin)) 
                {
                    //Cambia de formato la fecha
                    Datos_Negocio.P_Fecha_Inicio = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Datos_Negocio.P_Fecha_Inicio)) + " 00:00:00";
                    Datos_Negocio.P_Fecha_Fin = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Datos_Negocio.P_Fecha_Fin)) + " 23:59:59";

                    Mi_SQL += Entro_Where ? " AND REQ." : " WHERE REQ."; Entro_Where = true;
                    Mi_SQL += Ope_Com_Requisiciones.Campo_Fecha_Generacion + " >= '" + Datos_Negocio.P_Fecha_Inicio +
                             "' AND REQ." + Ope_Com_Requisiciones.Campo_Fecha_Generacion + " <= '" + Datos_Negocio.P_Fecha_Fin + "'";		
                }
                if (!String.IsNullOrEmpty(Datos_Negocio.P_Estatus))
                {
                    Mi_SQL += Entro_Where ? " AND PROD." : " WHERE PROD."; Entro_Where = true;
                    Mi_SQL += Cat_Com_Productos.Campo_Stock + " IN ('" + Datos_Negocio.P_Estatus + "')";
                }
                if (!String.IsNullOrEmpty(Datos_Negocio.P_Producto))
                {
                    Mi_SQL += Entro_Where ? " AND (PROD." : " WHERE (PROD."; Entro_Where = true;
                    Mi_SQL += Cat_Com_Productos.Campo_Clave + " = '" + Datos_Negocio.P_Producto.Trim() + "'";
                    Mi_SQL += " OR UPPER(PROD." + Cat_Com_Productos.Campo_Nombre + ") LIKE UPPER('%" + Datos_Negocio.P_Producto.Trim() + "%'))";
                }

                if (!String.IsNullOrEmpty(Datos_Negocio.P_Proveedor))
                {
                    Mi_SQL += Entro_Where ? " AND (PROV." : " WHERE (PROV."; Entro_Where = true;
                    Mi_SQL += Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Datos_Negocio.P_Proveedor.Trim() + "'";
                    Mi_SQL += " OR UPPER(PROV." + Cat_Com_Proveedores.Campo_Nombre + ") LIKE UPPER('%" + Datos_Negocio.P_Proveedor.Trim() + "%')";
                    Mi_SQL += " OR UPPER(PROV." + Cat_Com_Proveedores.Campo_Compañia + ") LIKE UPPER('%" + Datos_Negocio.P_Proveedor.Trim() + "%'))";
                }

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:Consultar_Unidades_Responsables
        /// DESCRIPCION         :Realizar la consulta de las unidades responsables 
        /// PARAMETROS          :            
        /// CREO                :David Herrera rincon
        /// FECHA_CREO          :14/Enero/2013
        /// MODIFICO            :     
        /// FECHA_MODIFICO      : 
        /// CAUSA_MODIFICACION  :     
        ///*******************************************************************************/
        public static DataTable Consultar_Unidades_Responsables(Cls_Rpt_Com_Pedidos_Productos_Negocio Datos_Negocio)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;

            try
            {
                //Consulta para encontrar una dependencia
                Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + ",";
                Mi_SQL += Cat_Dependencias.Campo_Clave + "+ ' ' + " + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                Mi_SQL += " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;                
                //Ordenar
                if (Datos_Negocio.P_Gerencia_ID != null)
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Dependencias.Campo_Grupo_Dependencia_ID + " ='" + Datos_Negocio.P_Gerencia_ID.Trim() + "'";
                Mi_SQL += " ORDER BY " + Cat_Dependencias.Campo_Nombre;
                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        #endregion
    }
}
