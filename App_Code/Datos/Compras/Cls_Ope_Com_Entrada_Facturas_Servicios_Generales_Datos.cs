﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Ope_Com_Entrada_Facturas_Servicios_Generales.Negocio;
using System.Data;
using System.Text;
using JAPAMI.Constantes;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;

/// <summary>
/// Summary description for Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Datos
/// </summary>
/// 
namespace JAPAMI.Ope_Com_Entrada_Facturas_Servicios_Generales.Datos
{
    public class Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Ordenes_Servicio
        ///DESCRIPCIÓN         : Consultar_Ordenes_Servicio
        ///PROPIEDADES         : Parametros. Objeto de la Clase de Negocio
        ///CREO                : Francisco Antonio Gallardo Catañeda
        ///FECHA_CREO          : 21/Agosto/2013
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        public static DataTable Consultar_Ordenes_Servicio(Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Negocio Parametros)
        {
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                StringBuilder Mi_Sql = new StringBuilder();
                Mi_Sql.Append("SELECT OC." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " AS NO_ORDEN_COMPRA");
                Mi_Sql.Append(", OC." + Ope_Com_Ordenes_Compra.Campo_Folio + " AS FOLIO");
                Mi_Sql.Append(", OC." + Ope_Com_Ordenes_Compra.Campo_Nombre_Proveedor + " AS PROVEEDOR");
                Mi_Sql.Append(", OC." + Ope_Com_Ordenes_Compra.Campo_Total + " AS TOTAL");
                Mi_Sql.Append(", OC." + Ope_Com_Ordenes_Compra.Campo_Estatus + " AS ESTATUS");
                Mi_Sql.Append(", OC." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo + " AS FECHA");
                Mi_Sql.Append(" FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " OC");
                if (!String.IsNullOrEmpty(Parametros.P_Proveedor_ID))
                {
                    if (Entro_Where) { Mi_Sql.Append(" AND "); } else { Mi_Sql.Append(" WHERE "); Entro_Where = true; }
                    Mi_Sql.Append(Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + " IN ('" + Parametros.P_Proveedor_ID + "')");
                }
                if (!String.IsNullOrEmpty(Parametros.P_No_Orden_Compra))
                {
                    if (Entro_Where) { Mi_Sql.Append(" AND "); } else { Mi_Sql.Append(" WHERE "); Entro_Where = true; }
                    Mi_Sql.Append(Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " IN ('" + Parametros.P_No_Orden_Compra + "')");
                }
                if (!String.Format("{0:ddMMyyyy}", new DateTime()).Equals(String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Inicio)))
                {
                    if (Entro_Where) { Mi_Sql.Append(" AND "); } else { Mi_Sql.Append(" WHERE "); Entro_Where = true; }
                    Mi_Sql.Append(Ope_Com_Ordenes_Compra.Campo_Fecha_Creo + " >= '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Inicio) + "'");
                }
                if (!String.Format("{0:ddMMyyyy}", new DateTime()).Equals(String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Final)))
                {
                    if (Entro_Where) { Mi_Sql.Append(" AND "); } else { Mi_Sql.Append(" WHERE "); Entro_Where = true; }
                    Mi_Sql.Append(Ope_Com_Ordenes_Compra.Campo_Fecha_Creo + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Final.AddDays(1)) + "'");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    if (Entro_Where) { Mi_Sql.Append(" AND "); } else { Mi_Sql.Append(" WHERE "); Entro_Where = true; }
                    Mi_Sql.Append(Ope_Com_Ordenes_Compra.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')");
                }
                if (Entro_Where) { Mi_Sql.Append(" AND "); } else { Mi_Sql.Append(" WHERE "); Entro_Where = true; }
                Mi_Sql.Append(Ope_Com_Ordenes_Compra.Campo_Tipo_Articulo + " = 'SERVICIO'");
                Mi_Sql.Append(" ORDER BY FECHA DESC");
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                if (Ds_Datos != null) if (Ds_Datos.Tables.Count > 0) Dt_Datos = Ds_Datos.Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Proveedores
        ///DESCRIPCIÓN         : Consultar_Proveedores
        ///PROPIEDADES         : Parametros. Objeto de la Clase de Negocio
        ///CREO                : Francisco Antonio Gallardo Catañeda
        ///FECHA_CREO          : 22/Agosto/2013
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        public static DataTable Consultar_Proveedores(Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Negocio Parametros)
        {
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            try
            {
                StringBuilder Mi_Sql = new StringBuilder();
                Mi_Sql.Append("SELECT DISTINCT(OC." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + ") AS PROVEEDOR_ID");
                Mi_Sql.Append(", UPPER(PRO." + Cat_Com_Proveedores.Campo_Nombre + ") AS NOMBRE_PROVEEDOR");
                Mi_Sql.Append(" FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " OC");
                Mi_Sql.Append(" INNER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PRO ON PRO." + Cat_Com_Proveedores.Campo_Proveedor_ID + " = OC." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + "");
                Mi_Sql.Append(" WHERE OC." + Ope_Com_Ordenes_Compra.Campo_Tipo_Articulo + " = 'SERVICIO' AND OC." + Ope_Com_Ordenes_Compra.Campo_Estatus + " = 'PROVEEDOR_ENTERADO'");
                Mi_Sql.Append(" ORDER BY NOMBRE_PROVEEDOR ASC");
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                if (Ds_Datos != null) if (Ds_Datos.Tables.Count > 0) Dt_Datos = Ds_Datos.Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Detalles_Orden_Servicio
        ///DESCRIPCIÓN         : Consultar_Detalles_Orden_Servicio
        ///PROPIEDADES         : Parametros. Objeto de la Clase de Negocio
        ///CREO                : Francisco Antonio Gallardo Catañeda
        ///FECHA_CREO          : 22/Agosto/2013
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        public static DataTable Consultar_Detalles_Orden_Servicio(Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Negocio Parametros)
        {
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                StringBuilder Mi_Sql = new StringBuilder();
                Mi_Sql.Append("SELECT OC." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " AS NO_ORDEN_COMPRA");
                Mi_Sql.Append(", OC." + Ope_Com_Ordenes_Compra.Campo_Folio + " AS FOLIO");
                Mi_Sql.Append(", OC." + Ope_Com_Ordenes_Compra.Campo_Fecha_Creo + " AS FECHA_CREO");
                Mi_Sql.Append(", RQ." + Ope_Com_Requisiciones.Campo_Folio + " AS FOLIO_RQ");
                Mi_Sql.Append(", UPPER(OC." + Ope_Com_Ordenes_Compra.Campo_Usuario_Creo + ") AS USUARIO_CREO");
                Mi_Sql.Append(", LTRIM(RTRIM(DEP." + Cat_Dependencias.Campo_Clave + ")) + ' - ' + LTRIM(RTRIM(DEP." + Cat_Dependencias.Campo_Nombre + ")) AS DEPENDENCIA");
                Mi_Sql.Append(", UPPER(LTRIM(RTRIM(PRO." + Cat_Com_Proveedores.Campo_Nombre + "))) AS PROVEEDOR");
                Mi_Sql.Append(", UPPER(LTRIM(RTRIM(RQ." + Ope_Com_Requisiciones.Campo_Justificacion_Compra + "))) AS JUSTIFICACION");
                Mi_Sql.Append(", OC." + Ope_Com_Ordenes_Compra.Campo_Subtotal + " AS SUBTOTAL");
                Mi_Sql.Append(", OC." + Ope_Com_Ordenes_Compra.Campo_Total_IVA + " AS IVA");
                Mi_Sql.Append(", OC." + Ope_Com_Ordenes_Compra.Campo_Total + " AS TOTAL");
                Mi_Sql.Append(" FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra + " OC");
                Mi_Sql.Append(" INNER JOIN " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " RQP ON RQP." + Ope_Com_Req_Producto.Campo_No_Orden_Compra + " = OC." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + "");
                Mi_Sql.Append(" INNER JOIN " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " RQ ON RQP." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " = RQ." + Ope_Com_Req_Producto.Campo_Requisicion_ID + "");
                Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP ON DEP." + Cat_Dependencias.Campo_Dependencia_ID + " = RQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID + "");
                Mi_Sql.Append(" INNER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PRO ON PRO." + Cat_Com_Proveedores.Campo_Proveedor_ID + " = OC." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + "");
                if (!String.IsNullOrEmpty(Parametros.P_No_Orden_Compra))
                {
                    if (Entro_Where) { Mi_Sql.Append(" AND "); } else { Mi_Sql.Append(" WHERE "); Entro_Where = true; }
                    Mi_Sql.Append("OC." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " IN ('" + Parametros.P_No_Orden_Compra + "')");
                }
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                if (Ds_Datos != null) if (Ds_Datos.Tables.Count > 0) Dt_Datos = Ds_Datos.Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Facturas
        ///DESCRIPCIÓN         : Alta_Facturas
        ///PROPIEDADES         : Parametros. Objeto de la Clase de Negocio
        ///CREO                : Francisco Antonio Gallardo Catañeda
        ///FECHA_CREO          : 22/Agosto/2013
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        public static void Alta_Facturas(Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Negocio Parametros)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Int32 Factura_ID = Obtener_ID_Consecutivo(Ope_Alm_Registro_Facturas.Campo_Factura_ID, Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas);
            try
            {
                foreach (DataRow Fila_Actual in Parametros.P_Dt_Datos.Rows)
                {
                    Mi_Sql.Append("INSERT INTO "+Ope_Alm_Registro_Facturas.Tabla_Ope_Alm_Registro_Facturas);
                    Mi_Sql.Append("( " + Ope_Alm_Registro_Facturas.Campo_Factura_ID);
                    Mi_Sql.Append(", " + Ope_Alm_Registro_Facturas.Campo_Factura_Proveedor);
                    Mi_Sql.Append(", " + Ope_Alm_Registro_Facturas.Campo_Importe_Factura);
                    Mi_Sql.Append(", " + Ope_Alm_Registro_Facturas.Campo_Fecha_Factura);
                    Mi_Sql.Append(", " + Ope_Alm_Registro_Facturas.Campo_Usuario_Creo);
                    Mi_Sql.Append(", " + Ope_Alm_Registro_Facturas.Campo_Fecha_Creo);
                    Mi_Sql.Append(", " + Ope_Alm_Registro_Facturas.Campo_No_Orden_Compra);
                    Mi_Sql.Append(", " + Ope_Alm_Registro_Facturas.Campo_IVA_Factura);
                    Mi_Sql.Append(", " + Ope_Alm_Registro_Facturas.Campo_Total_Factura);
                    Mi_Sql.Append(") VALUES (");
                    Mi_Sql.Append("'" + Factura_ID + "'");
                    Mi_Sql.Append(", '" + Fila_Actual["NUMERO_FACTURA"].ToString().Trim() + "'");
                    Mi_Sql.Append(", '" + Convert.ToDouble(Fila_Actual["IMPORTE_FACTURA"]) + "'");
                    Mi_Sql.Append(", '" + String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Fila_Actual["FECHA_FACTURA"])) + "'");
                    Mi_Sql.Append(", '" + Parametros.P_Usuario.Trim() + "'");
                    Mi_Sql.Append(", GETDATE()");
                    Mi_Sql.Append(", '" + Parametros.P_No_Orden_Compra.Trim() + "'");
                    Mi_Sql.Append(", '" + Convert.ToDouble(Fila_Actual["IVA_FACTURA"]) + "'");
                    Mi_Sql.Append(", '" + Convert.ToDouble(Fila_Actual["TOTAL_FACTURA"]) + "'");
                    Mi_Sql.Append(")");
                    Cmd.CommandText = Mi_Sql.ToString();
                    Cmd.ExecuteNonQuery();
                    Mi_Sql.Length = 0;
                    Factura_ID = Factura_ID + 1;
                }
                Mi_Sql.Append("UPDATE " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra);
                Mi_Sql.Append(" SET " + Ope_Com_Ordenes_Compra.Campo_Estatus + " = 'FACTURA REGISTRADA'");
                Mi_Sql.Append(" WHERE " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = '" + Parametros.P_No_Orden_Compra.Trim() + "'");
                Cmd.CommandText = Mi_Sql.ToString();
                Cmd.ExecuteNonQuery();
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static Int32 Obtener_ID_Consecutivo(String Campo, String Tabla)
        {
            Int32 Identificador = 1;
            try
            {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Identificador = Convert.ToInt32(Obj_Temp) + 1;
                }
            }
            catch (SqlException Ex)
            {
                new Exception(Ex.Message);
            }
            return Identificador;
        }

    }
}
