﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Archivos_Ordenes_Compra.Negocios;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;


/// <summary>
/// Summary description for Cls_Ope_Archivos_Ordenes_Compra_Datos
/// </summary>
namespace JAPAMI.Archivos_Ordenes_Compra.Datos
{
    public class Cls_Ope_Archivos_Ordenes_Compra_Datos
    {
        public Cls_Ope_Archivos_Ordenes_Compra_Datos()
        {
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Archivos
        ///DESCRIPCION:             hacer la consulta de los archivos de una orden de compra
        ///PARAMETROS:              Datos: Variable de la capa de negocio
        ///CREO:                    David Herrera Rincon
        ///FECHA_CREO:              12/Enero/2013
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Archivos(Cls_Ope_Archivos_Ordenes_Compra_Negocio Datos)
        {
            //Declaracion de variables
            DataTable Dt_Resultado = new DataTable(); //tabla para la consulta
            String Mi_SQL = String.Empty; //variable para la consulta

            try
            {
                //Ejecutar la consulta
                Mi_SQL = "SELECT OPE_COM_ORDENES_COMPRA_ARCHIVOS.No_Archivo, OPE_COM_ORDENES_COMPRA_ARCHIVOS.NO_ORDEN_COMPRA, OPE_COM_ORDENES_COMPRA_ARCHIVOS.Nombre_Archivo,"
                       + " OPE_COM_ORDENES_COMPRA_ARCHIVOS.Descripcion, LTRIM(RTRIM( OPE_COM_ORDENES_COMPRA.FOLIO)) AS Folio, Original = 'SI', OPE_COM_ORDENES_COMPRA.USUARIO_CREO" 
                       + " FROM OPE_COM_ORDENES_COMPRA_ARCHIVOS INNER JOIN OPE_COM_ORDENES_COMPRA ON OPE_COM_ORDENES_COMPRA_ARCHIVOS.NO_ORDEN_COMPRA = OPE_COM_ORDENES_COMPRA.NO_ORDEN_COMPRA" 
                       + " WHERE OPE_COM_ORDENES_COMPRA_ARCHIVOS.NO_ORDEN_COMPRA = " + Datos.P_No_Orden_Compra.ToString().Trim();

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Alta_Archivos_Onden_Compra
        ///DESCRIPCION:             Dar de alta en la base de datos los nombres de los archivos a ingresar en la base de datos
        ///PARAMETROS:              Datos: Variable de la capa de negocio
        ///CREO:                    David Herrera Rincon
        ///FECHA_CREO:              12/Enero/2013
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static void Alta_Archivos_Onden_Compra(Cls_Ope_Archivos_Ordenes_Compra_Negocio Datos)
        {
            String Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
            String Mi_SQL = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            int Cont_Elementos; //variable para el contador
            Object aux; //variable auxiliar para las consultas
            int No_Archivo; //variable para el numero del archivo
            
            //Abrir la conexion
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                //Verificar si la tabla no es nula
                if (Datos.P_Dt_Archivos != null)
                {
                    //Verificar si la tabla tiene elementos
                    if (Datos.P_Dt_Archivos.Rows.Count > 0)
                    {
                        //Ciclo para el barrido de la tabla de los elementos a agregar
                        for (Cont_Elementos = 0; Cont_Elementos < Datos.P_Dt_Archivos.Rows.Count; Cont_Elementos++)
                        {
                            //Verificar que el renglon no haya sido eliminado
                            if (Datos.P_Dt_Archivos.Rows[Cont_Elementos].RowState != DataRowState.Deleted)
                            {
                                //Verificar si el archivo es nuevo
                                if (Datos.P_Dt_Archivos.Rows[Cont_Elementos]["Original"].ToString().Trim().Equals("NO"))
                                {
                                    //Consulta para el numero del archivo
                                    Mi_SQL = "SELECT ISNULL(MAX(No_Archivo), 0) FROM OPE_COM_ORDENES_COMPRA_ARCHIVOS ";

                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    aux = Cmd.ExecuteScalar();

                                    //obtener el ID
                                    No_Archivo = Convert.ToInt32(aux) + 1;

                                    //Construir la consulta
                                    Mi_SQL = "INSERT INTO OPE_COM_ORDENES_COMPRA_ARCHIVOS (No_Archivo, NO_ORDEN_COMPRA, Nombre_Archivo, Descripcion, Usuario_Creo, Fecha_Creo) " +
                                        "VALUES(" + No_Archivo.ToString().Trim() + "," + Datos.P_No_Orden_Compra.ToString().Trim() + ",'" +
                                        Datos.P_Dt_Archivos.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim() + "','" +
                                        Datos.P_Dt_Archivos.Rows[Cont_Elementos]["Descripcion"].ToString().Trim() + "','" + Usuario_Creo + "',GETDATE())";

                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                }
                            }
                        }

                        //Verificar si la tabla no es nula
                        if (Datos.P_Dt_Archivos_Eliminados != null)
                        {
                            //Ciclo para el barrido de la tabla de los elementos a eliminar
                            for (Cont_Elementos = 0; Cont_Elementos < Datos.P_Dt_Archivos_Eliminados.Rows.Count; Cont_Elementos++)
                            {
                                //Verificar que el renglon no haya sido eliminado
                                if (Datos.P_Dt_Archivos_Eliminados.Rows[Cont_Elementos].RowState != DataRowState.Deleted)
                                {
                                    //verificar que el renglon sea original
                                    if (Datos.P_Dt_Archivos_Eliminados.Rows[Cont_Elementos]["Original"].ToString().Trim() == "SI")
                                    {
                                        //COnsulta para eliminar el registro del archivo
                                        Mi_SQL = "DELETE FROM OPE_COM_ORDENES_COMPRA_ARCHIVOS " +
                                            "WHERE No_Archivo = " + Datos.P_Dt_Archivos_Eliminados.Rows[Cont_Elementos]["No_Archivo"].ToString().Trim();

                                        //Ejecutar consulta
                                        Cmd.CommandText = Mi_SQL;
                                        Cmd.ExecuteNonQuery();
                                    }
                                }
                            }
                        }
                        //Ejecutar transaccion
                        Trans.Commit();
                    }
                }
            }
            catch (SqlException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Información: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Los datos fueron actualizados por otro Usuario. Información: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Información: " + Ex.Message);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Eliminar_Archivos_Orden_Compra
        ///DESCRIPCION:             Eliminar los archivos de la Orden de Compra
        ///PARAMETROS:              Datos: Variable de la capa de negocio
        ///CREO:                    David Herrera Rincon
        ///FECHA_CREO:              12/Enero/2013
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static void Eliminar_Archivos_Orden_Compra(Cls_Ope_Archivos_Ordenes_Compra_Negocio Datos)
        {
            String Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
            String Mi_SQL = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;

            //Abrir la conexion
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                //Consulta para eliminar los archivos de esta requisicion
                Mi_SQL = "DELETE FROM OPE_COM_ORDENES_COMPRA_ARCHIVOS WHERE NO_ORDEN_COMPRA = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                
                //Ejecutar transaccion
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Información: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Los datos fueron actualizados por otro Usuario. Información: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Información: " + Ex.Message);
            }
            finally
            {
                Cn.Close();
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Datos_Archivo
        ///DESCRIPCION:             Consulta todos los datos de un archivo
        ///PARAMETROS:              Datos: Variable de la capa de negocio
        ///CREO:                    David Herrera Rincon
        ///FECHA_CREO:              12/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Datos_Archivo(Cls_Ope_Archivos_Ordenes_Compra_Negocio Datos)
        {
            //Declaracion de variables
            DataTable Dt_Resultado = new DataTable(); //tabla para la consulta
            String Mi_SQL = String.Empty; //variable para la consulta

            try
            {
                //Ejecutar la consulta
                Mi_SQL = "SELECT * FROM OPE_COM_ORDENES_COMPRA_ARCHIVOS " +
                        "WHERE No_Archivo = " + Datos.P_No_Archivo.ToString().Trim();

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

    }
}