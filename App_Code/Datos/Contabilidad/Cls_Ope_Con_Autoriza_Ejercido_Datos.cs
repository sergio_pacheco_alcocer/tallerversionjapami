﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Autoriza_Ejercido.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Con_Autoriza_Ejercido_Datos
/// </summary>

namespace JAPAMI.Autoriza_Ejercido.Datos
{
    public class Cls_Ope_Con_Autoriza_Ejercido_Datos
    {
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Solicitudes_SinEjercer
        /// DESCRIPCION : Consulta las solicitudes de pago que estan En estatus PORPAGAR de autorizar o rechazar 
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Solicitudes_SinEjercer(Cls_Ope_Con_Autoriza_Ejercido_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT Solicitud." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_No_Poliza;
                Mi_SQL += ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", " + Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID;
                Mi_SQL += ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Monto + ", Tipo." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + " as Tipo_Pago FROM ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos+" Solicitud, "+ Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago +" Tipo, ";
                Mi_SQL += Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " Reserva ";
                Mi_SQL += " WHERE solicitud." + Ope_Con_Solicitud_Pagos.Campo_Estatus  + " = 'PORPAGAR' AND Solicitud."+ Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID +" = Tipo."+ Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID ;
                Mi_SQL += " AND solicitud." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = Reserva." + Ope_Psp_Reservas.Campo_No_Reserva+" AND Reserva."+Ope_Psp_Reservas.Campo_Recurso + " ='" +Datos.P_Tipo_Recurso +"'";
                Mi_SQL += " ORDER BY " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cambiar_Estatus_Solicitud_Pago
        /// DESCRIPCION : Autoriza o rechaza la solicitud de pago 
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Cambiar_Estatus_Solicitud_Pago(Cls_Ope_Con_Autoriza_Ejercido_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            try
            {
                //Da de Alta los datos del Cierre Mensual con los datos proporcionados por el usuario
                Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Comentario_Ejercido + "='" + Datos.P_Comentario + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Estatus + "='" + Datos.P_Estatus + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Modifico + "',";
                if (!String.IsNullOrEmpty(Datos.P_Usuario_Recibio_Ejercido))
                {
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Ejercido + "='" + Datos.P_Usuario_Recibio_Ejercido + "', ";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Ejercido + "=GETDATE() ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Usuario_Autorizo_Ejercido))
                {
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Ejercido + "='" + Datos.P_Usuario_Autorizo_Ejercido + "', ";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Autoriza_Rechaza_Ejercido + "= GETDATE() ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Usuario_Recibio_Pagado))
                {
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Pagado + "='" + Datos.P_Usuario_Recibio_Pagado + "', ";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Pagado + "= GETDATE() ,";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Forma_Pago + "='" + Datos.P_Forma_Pago + "',";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Cuenta_Banco_Pago_ID + "='" + Datos.P_Cuenta_Banco_Pago + "' ";

                }
                Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Datos.P_No_Solicitud_Pago + "'";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();

                //Verificar si hay comando
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Solicitud_Pago
        /// DESCRIPCION : Consulta las solicitudes de pago que estan pendientes de autorizar o rechazar 
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Solicitud_Pago(Cls_Ope_Con_Autoriza_Ejercido_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            SqlDataAdapter Da_SQL = new SqlDataAdapter();
            DataSet Ds_SQL = new DataSet();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT " + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + "," + Ope_Con_Solicitud_Pagos.Campo_Estatus + "," + Ope_Con_Solicitud_Pagos.Campo_No_Poliza + "," + Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + ", ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Monto + ", " + Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + "," + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + "," + Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales + " FROM ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos;
                Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'";
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Da_SQL.SelectCommand = Cmmd;
                Da_SQL.Fill(Ds_SQL);
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Ds_SQL.Tables[0];
                //return OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }

            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Solicitud_Pago_Completa
        /// DESCRIPCION : Consulta las solicitudes de pago  
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Solicitud_Pago_Completa(Cls_Ope_Con_Autoriza_Ejercido_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT pagos.*,proveedor." + Cat_Com_Proveedores.Campo_Compañia + ",( empleado." + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno + " ) as nombre_empleado FROM ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " Pagos ";
                Mi_SQL += " left outer join " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " proveedor on pagos." + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + "= proveedor." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL += " left outer join " + Cat_Empleados.Tabla_Cat_Empleados + " empleado on pagos." + Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + "= empleado." + Cat_Empleados.Campo_Empleado_ID;
                Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        
    }
}

