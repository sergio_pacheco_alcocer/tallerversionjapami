﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Ope_Con_Servicios_Generales.Negocio;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using System.Data.SqlClient;
using JAPAMI.Manejo_Presupuesto.Datos;

/// <summary>
/// Summary description for Cls_Ope_Con_Servicios_Generales_Datos
/// </summary>
/// 
namespace JAPAMI.Ope_Con_Servicios_Generales.Datos
{
    public class Cls_Ope_Con_Servicios_Generales_Datos
    {
        #region Metodos

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Fuentes_Financiamiento
        /// DESCRIPCION :Consultar las Ftes de Financiamiento que cuenten con presupuesto 
        ///              de cierta partida
        /// PARAMETROS  : Parametros. Objeto con las propiedades cargadas para realizar
        ///               la Actividad.
        /// CREO        : Francisco Gallardo
        /// FECHA_CREO  : 29/Agosto/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Fuentes_Financiamiento(Cls_Ope_Con_Servicios_Generales_Negocio Parametros)
        {
            DataTable Dt_Datos = new DataTable();
            DataSet Ds_Datos = null;
            try
            {
                StringBuilder Mi_Sql = new StringBuilder();
                Mi_Sql.Append("SELECT DISTINCT (PA." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ") AS FTE_FINANCIAMIENTO_ID");
                Mi_Sql.Append(", RTRIM(LTRIM(ISNULL(FF." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ",''))) +' - '+ RTRIM(LTRIM(ISNULL(FF." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ",''))) AS FTE_FINANCIAMIENTO");
                Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PA");
                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FF ON PA." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = FF." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + "");
                Mi_Sql.Append(" WHERE FF." + Cat_SAP_Fuente_Financiamiento.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')");
                Mi_Sql.Append(" AND PA." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Parametros.P_Anio);
                Mi_Sql.Append(" ORDER BY FTE_FINANCIAMIENTO");
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                if (Ds_Datos != null) if (Ds_Datos.Tables.Count > 0) Dt_Datos = Ds_Datos.Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Consultar_Dependencias_Partida..." + Ex.Message);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Dependencias_Partida
        /// DESCRIPCION :Consultar las Unidades Responsables que cuenten con presupuesto 
        ///              de cierta partida
        /// PARAMETROS  : Parametros. Objeto con las propiedades cargadas para realizar
        ///               la Actividad.
        /// CREO        : Francisco Gallardo
        /// FECHA_CREO  : 24/Agosto/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Dependencias_Partida(Cls_Ope_Con_Servicios_Generales_Negocio Parametros)
        {
            DataTable Dt_Datos = new DataTable();
            DataSet Ds_Datos = null;
            try
            {
                StringBuilder Mi_Sql = new StringBuilder();
                Mi_Sql.Append("SELECT DISTINCT (PA." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ") AS DEPENDENCIA_ID");
                Mi_Sql.Append(", RTRIM(LTRIM(ISNULL(DEP." + Cat_Dependencias.Campo_Nombre + ",''))) AS DEPENDENCIA");
                Mi_Sql.Append(", RTRIM(LTRIM(ISNULL(DEP." + Cat_Dependencias.Campo_Clave + ",''))) + ' - ' + RTRIM(LTRIM(ISNULL(DEP." + Cat_Dependencias.Campo_Nombre + ",''))) AS CLAVE_DESCRIPCION_DEPENDENCIA");
                Mi_Sql.Append(", RTRIM(LTRIM(ISNULL(PP." + Cat_Sap_Proyectos_Programas.Campo_Clave + ",''))) AS CLAVE_PROGRAMA");
                Mi_Sql.Append(", RTRIM(LTRIM(ISNULL(PP." + Cat_Sap_Proyectos_Programas.Campo_Nombre + ",''))) AS NOMBRE_PROGRAMA");
                Mi_Sql.Append(", RTRIM(LTRIM(ISNULL(PP." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ",''))) AS PROGRAMA_ID");
                Mi_Sql.Append(", RTRIM(LTRIM(ISNULL(PA." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + ",''))) AS CAPITULO_ID");
                Mi_Sql.Append(", RTRIM(LTRIM(ISNULL(PP." + Cat_Sap_Proyectos_Programas.Campo_Clave + ",''))) + ' - ' + RTRIM(LTRIM(ISNULL(PP." + Cat_Sap_Proyectos_Programas.Campo_Nombre + ",''))) AS CLAVE_DESCRIPCION_PROGRAMA");
                Mi_Sql.Append(", PA." + Parametros.P_Mes + " AS DISPONIBLE");
                Mi_Sql.Append(", 0.0 AS MONTO");
                Mi_Sql.Append(", RTRIM(LTRIM(ISNULL(PE." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", ''))) AS PARTIDA_ID");
                Mi_Sql.Append(", RTRIM(LTRIM(ISNULL(PE." + Cat_Sap_Partidas_Especificas.Campo_Clave + ", ''))) AS CLAVE_PARTIDA");
                Mi_Sql.Append(", RTRIM(LTRIM(ISNULL(PE." + Cat_Sap_Partidas_Especificas.Campo_Clave + ", ''))) + ' - ' + RTRIM(LTRIM(ISNULL(PE." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ", ''))) AS CLAVE_DESCRIPCION_PARTIDA");
                Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PA");
                Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP ON PA." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = DEP." + Cat_Dependencias.Campo_Dependencia_ID + "");
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " PP ON PA." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = PP." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + "");
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PE ON PA." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = PE." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "");
                Mi_Sql.Append(" WHERE PA." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " IN ('" + Parametros.P_Partida_ID + "')");
                Mi_Sql.Append(" AND PA." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Parametros.P_Anio);
                Mi_Sql.Append(" ORDER BY DEPENDENCIA");
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                if (Ds_Datos != null) if (Ds_Datos.Tables.Count > 0) Dt_Datos = Ds_Datos.Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Consultar_Dependencias_Partida..." + Ex.Message);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Dependencias_Partida
        /// DESCRIPCION :Consultar las Unidades Responsables que cuenten con presupuesto 
        ///              de cierta partida
        /// PARAMETROS  : Parametros. Objeto con las propiedades cargadas para realizar
        ///               la Actividad.
        /// CREO        : Francisco Gallardo
        /// FECHA_CREO  : 24/Agosto/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Proveedores(Cls_Ope_Con_Servicios_Generales_Negocio Parametros)
        {
            DataTable Dt_Datos = new DataTable();
            DataSet Ds_Datos = null;
            Boolean Entro_Where = false;
            try
            {
                StringBuilder Mi_Sql = new StringBuilder();
                Mi_Sql.Append("SELECT PRO." + Cat_Com_Proveedores.Campo_Proveedor_ID + "  AS PROVEEDOR_ID");
                Mi_Sql.Append(", PRO." + Cat_Com_Proveedores.Campo_Nombre + "  AS NOMBRE_PROVEEDOR");
                Mi_Sql.Append(", CC." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " AS CUENTA_CONTABLE_ID");
                Mi_Sql.Append(", CC." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " AS DESCRIPCION_CUENTA");
                Mi_Sql.Append(", LTRIM(RTRIM(CC." + Cat_Con_Cuentas_Contables.Campo_Cuenta + ")) + ' - ' + LTRIM(RTRIM(CC." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ")) AS CUENTA_DESCRIPCION");
                Mi_Sql.Append(" FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PRO");
                Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CC ON PRO." + Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID + " = CC." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "");
                if (!String.IsNullOrEmpty(Parametros.P_Proveedor_ID))
                {
                    if (Entro_Where) { Mi_Sql.Append(" AND "); } else { Mi_Sql.Append(" WHERE "); Entro_Where = true; }
                    Mi_Sql.Append(Cat_Com_Proveedores.Campo_Proveedor_ID + " IN ('" + Parametros.P_Proveedor_ID + "')");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    if (Entro_Where) { Mi_Sql.Append(" AND "); } else { Mi_Sql.Append(" WHERE "); Entro_Where = true; }
                    Mi_Sql.Append(Cat_Com_Proveedores.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Aprox_Proveedor))
                {
                    if (Entro_Where) { Mi_Sql.Append(" AND "); } else { Mi_Sql.Append(" WHERE "); Entro_Where = true; }
                    Mi_Sql.Append("(");
                    Mi_Sql.Append(Cat_Com_Proveedores.Campo_Nombre + " LIKE '%" + Parametros.P_Aprox_Proveedor + "%'");
                    Mi_Sql.Append(" OR ");
                    Mi_Sql.Append(Cat_Com_Proveedores.Campo_Compañia + " LIKE '%" + Parametros.P_Aprox_Proveedor + "%'");
                    Mi_Sql.Append(")");
                }
                Mi_Sql.Append(" ORDER BY NOMBRE_PROVEEDOR ASC");

                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                if (Ds_Datos != null) if (Ds_Datos.Tables.Count > 0) Dt_Datos = Ds_Datos.Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Consultar_Dependencias_Partida..." + Ex.Message);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Partidas_Gasto
        /// DESCRIPCION :Consultar las Partidas de Gastos
        /// PARAMETROS  : Parametros. Objeto con las propiedades cargadas para realizar
        ///               la Actividad.
        /// CREO        : Francisco Gallardo
        /// FECHA_CREO  : 24/Agosto/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Partida_Gasto(Cls_Ope_Con_Servicios_Generales_Negocio Parametros)
        {
            DataTable Dt_Datos = new DataTable();
            DataSet Ds_Datos = null;
            Boolean Entro_Where = false;
            try
            {
                StringBuilder Mi_Sql = new StringBuilder();
                Mi_Sql.Append("SELECT " + Cat_Con_Cuentas_Gastos_Detalles.Campo_Partida_ID + " AS PARTIDA_ID");
                Mi_Sql.Append(" FROM " + Cat_Con_Cuentas_Gastos_Detalles.Tabla_Cat_Con_Cuentas_Gastos_Detalles + " CGD");
                if (!String.IsNullOrEmpty(Parametros.P_Cuenta_Gasto_ID))
                {
                    if (Entro_Where) { Mi_Sql.Append(" AND "); } else { Mi_Sql.Append(" WHERE "); Entro_Where = true; }
                    Mi_Sql.Append(Cat_Con_Cuentas_Gastos_Detalles.Campo_Id_Gasto + " IN ('" + Parametros.P_Cuenta_Gasto_ID + "')");
                }

                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                if (Ds_Datos != null) if (Ds_Datos.Tables.Count > 0) Dt_Datos = Ds_Datos.Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Consultar_Dependencias_Partida..." + Ex.Message);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Reserva_Solicitud_Pago
        /// DESCRIPCION :Da de Alta los Registros
        /// PARAMETROS  : Parametros. Objeto con las propiedades cargadas para realizar
        ///               la Actividad.
        /// CREO        : Francisco Gallardo
        /// FECHA_CREO  : 29/Agosto/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static String Alta_Reserva_Solicitud_Pago(Cls_Ope_Con_Servicios_Generales_Negocio Parametros)
        {
            String Reserva_Solicitud = String.Empty;
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            StringBuilder Mi_SQL = new StringBuilder();
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                ///Se crea la Reserva al Presupuesto
                Int32 No_Reserva = Cls_Ope_Psp_Manejo_Presupuesto.Crear_Reserva("", "GENERADA", "P-" + Parametros.P_Aprox_Proveedor.Trim(), "", "", Parametros.P_Concepto, DateTime.Now.Year.ToString(), Parametros.P_Total, Parametros.P_Proveedor_ID, "", Parametros.P_Tipo_Solicitud_ID, Parametros.P_Dt_Detalles_Reserva, "RECURSO ASIGNADO", Cmd);

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_SQL.Append(" SET " + Ope_Psp_Reservas.Campo_Tipo_Reserva + " = 'UNICA'");
                Mi_SQL.Append(", " + Ope_Psp_Reservas.Campo_Saldo + " = '0.0'");
                Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + No_Reserva + "'");

                Cmd.CommandText = Mi_SQL.ToString();
                Cmd.ExecuteNonQuery();

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                Mi_SQL.Append(" SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = '0.0'");
                Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = '" + No_Reserva + "'");

                Cmd.CommandText = Mi_SQL.ToString();
                Cmd.ExecuteNonQuery();

                ///Se hace la Afectación Presupuestal
                Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Parametros.P_Dt_Detalles_Presupuesto, Cmd);
                Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimientos_Presupuestales(No_Reserva.ToString().Trim(), Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Parametros.P_Total, "", "", "", "", Cmd);

                ///Creación de Solicitud de Pago

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "),'0000000000')");
                Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);

                Cmd.CommandText = Mi_SQL.ToString().Trim();
                Object No_Solicitud_Pago = Cmd.ExecuteScalar();
                String No_Solicitud = "";
                if (Convert.IsDBNull(No_Solicitud_Pago))
                {
                    No_Solicitud = "0000000001";
                }
                else
                {
                    No_Solicitud = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud_Pago) + 1);
                }

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("INSERT INTO " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" (" + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ")");
                Mi_SQL.Append(" VALUES ('" + No_Solicitud + "'");
                Mi_SQL.Append(", '" + Parametros.P_Tipo_Solicitud_ID + "'");
                Mi_SQL.Append(", " + Convert.ToInt32(No_Reserva));
                Mi_SQL.Append(", '" + Parametros.P_Concepto.Trim() + "'");
                Mi_SQL.Append(", GETDATE()");
                Mi_SQL.Append(", " + Parametros.P_Total);
                Mi_SQL.Append(", '" + Parametros.P_Proveedor_ID + "'");
                Mi_SQL.Append(", 'PRE-DOCUMENTADO'");
                Mi_SQL.Append(", 'SI'");
                Mi_SQL.Append(", '" + Parametros.P_Usuario_Nombre + "'");
                Mi_SQL.Append(", GETDATE())");

                Cmd.CommandText = Mi_SQL.ToString();
                Cmd.ExecuteNonQuery();

                foreach (DataRow Dr in Parametros.P_Dt_Detalles_Solicitud_Pago.Rows)
                {
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("INSERT INTO " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                        Mi_SQL.Append(" (" + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Factura + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Documento + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID + ",");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Dependencia_ID + ",");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_IEPS + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Nombre_Proveedor_Factura + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Retencion_IVA + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISH + ",");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_RFC + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_CURP + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Usuario_Creo + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Creo + ")");
                        Mi_SQL.Append(" VALUES ('" + No_Solicitud + "'");
                        Mi_SQL.Append(", '" + Parametros.P_No_Factura.Trim() + "'");
                        Mi_SQL.Append(", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Factura) + "'");
                        Mi_SQL.Append(", '" + Convert.ToDouble(Dr["IMPORTE"].ToString().Trim()) + "'");
                        Mi_SQL.Append(", 'DOCUMENTO'");
                        Mi_SQL.Append(", '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                        Mi_SQL.Append(", '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                        Mi_SQL.Append(", '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                        Mi_SQL.Append(", '" + Dr["DEPENDENCIA_ID"].ToString().Trim() + "'");
                        Mi_SQL.Append(", '" + Convert.ToDouble(Dr["IVA"].ToString().Trim()) + "'");
                        Mi_SQL.Append(", '0.0'");
                        Mi_SQL.Append(", '" + Parametros.P_Aprox_Proveedor + "'");
                        Mi_SQL.Append(", 'OTROS'");
                        Mi_SQL.Append(", '0.0'");
                        Mi_SQL.Append(", '0.0'");
                        Mi_SQL.Append(", '0.0'");
                        Mi_SQL.Append(", '0.0'");
                        Mi_SQL.Append(", ''");
                        Mi_SQL.Append(", ''");
                        Mi_SQL.Append(", '" + Parametros.P_Usuario_Nombre + "'");
                        Mi_SQL.Append(", GETDATE())");

                        Cmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Cmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                }

                Trans.Commit();
                Parametros.P_No_Reserva = No_Reserva;
                Parametros.P_No_Solicitud_Pago = No_Solicitud;
                Reserva_Solicitud = "Se ha creado la Reserva no. " + No_Reserva.ToString().Trim() + " y la Solicitud de Pago no. " + No_Solicitud;
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
            return Reserva_Solicitud;
        }

        #endregion Metodos

    }
}
