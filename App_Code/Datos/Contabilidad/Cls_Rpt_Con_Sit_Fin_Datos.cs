﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Negocio;

namespace JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Datos
{
    public class Cls_Rpt_Con_Sit_Fin_Datos
    {
        #region(Métodos)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Meses_Cerrados
        /// DESCRIPCION: Consulta los meses que se encuentran cerrados
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO : 22-Febrero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Meses_Cerrados(Cls_Rpt_Con_Sit_Fin_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT  distinct " + Ope_Con_Cierre_Mensual.Campo_Mes_Anio);
                Mi_SQL.Append(" FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual);
                Mi_SQL.Append(" WHERE " + Ope_Con_Cierre_Mensual.Campo_Mes_Anio + " LIKE '%" + Datos.P_Año + "'");
                Mi_SQL.Append(" ORDER BY " + Ope_Con_Cierre_Mensual.Campo_Mes_Anio + " asc");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Anios
        /// DESCRIPCION: Consulta los meses que se encuentran cerrados
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : Sergio Manuel Gallardo
        /// FECHA_CREO : 22-Febrero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Anios(Cls_Rpt_Con_Sit_Fin_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT  distinct " + Ope_Con_Cierre_Mensual_Gral.Campo_Anio);
                Mi_SQL.Append(" FROM " + Ope_Con_Cierre_Mensual_Gral.Tabla_Ope_Con_Cierre_Mensual_Gral);
                Mi_SQL.Append(" ORDER BY " + Ope_Con_Cierre_Mensual_Gral.Campo_Anio + " asc");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Situacion_Financiera
        /// DESCRIPCION: Consulta la situacion financiera de las cuentas que comiencen con 1,2,3
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO : 22-Febrero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Situacion_Financiera(Cls_Rpt_Con_Sit_Fin_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta +
                        ", Cuentas." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ",");
                Mi_SQL.Append(Ope_Con_Cierre_Mensual.Campo_Saldo_Final + ", " + Ope_Con_Cierre_Mensual.Campo_Saldo_Inicial +",");
                Mi_SQL.Append(Ope_Con_Cierre_Mensual.Campo_Total_Haber + "," + Ope_Con_Cierre_Mensual.Campo_Total_Debe);
                Mi_SQL.Append(" FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + " Cierre," +
                        Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " Cuentas");
                Mi_SQL.Append(" WHERE Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "=Cierre." +
                        Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " AND (Cierre." +
                        Ope_Con_Cierre_Mensual.Campo_Mes_Anio + "='" + Datos.P_Mes_Año + "')");
                Mi_SQL.Append(" AND " + "(Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro1 + "%'");

                if (!string.IsNullOrEmpty(Datos.P_Filtro2))
                {
                    Mi_SQL.Append(" OR Cuentas." +
                            Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro2 + "%' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro3))
                {
                    Mi_SQL.Append(" OR Cuentas." +
                        Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro3 + "%' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro4))
                {
                    Mi_SQL.Append(" OR Cuentas." +
                        Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro4 + "%' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro5))
                {
                    Mi_SQL.Append(" OR Cuentas." +
                            Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro5 + "%' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro6))
                {
                    Mi_SQL.Append(" OR Cuentas." +
                            Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro6 + "%' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro7))
                {
                    Mi_SQL.Append(" OR Cuentas." +
                            Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro7 + "%' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro8))
                {
                    Mi_SQL.Append(" OR Cuentas." +
                            Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro8 + "%' ");
                }
                Mi_SQL.Append(") ORDER BY " + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                //Mi_SQL.Append(" AND Cuentas." + Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta + "='00004'");
                //Mi_SQL.Append(" ORDER BY Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Situacion_Financiera_Acumulado
        /// DESCRIPCION: Consulta la situacion financiera de las cuentas que comiencen con 1,2,3
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : sERGIO mANUEL gALALRDO
        /// FECHA_CREO : 22-Febrero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Situacion_Financiera_Acumulado(Cls_Rpt_Con_Sit_Fin_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta +
                        ", Cuentas." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ",");
                Mi_SQL.Append(Ope_Con_Cierre_Mensual.Campo_Saldo_Final + ", " + Ope_Con_Cierre_Mensual.Campo_Saldo_Inicial + ",");
                Mi_SQL.Append(Ope_Con_Cierre_Mensual.Campo_Total_Haber + "," + Ope_Con_Cierre_Mensual.Campo_Total_Debe);
                Mi_SQL.Append(" FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + " Cierre," +
                        Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " Cuentas");
                Mi_SQL.Append(" WHERE Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "=Cierre." +
                        Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " AND (Cierre." +
                        Ope_Con_Cierre_Mensual.Campo_Mes_Anio + "='" + Datos.P_Mes_Año + "' OR " +
                        Ope_Con_Cierre_Mensual.Campo_Mes_Anio + "=" + Datos.P_Mes_Año_Ant + ")");
                Mi_SQL.Append(" AND " + "(Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro1 + "%'");

                if (!string.IsNullOrEmpty(Datos.P_Filtro2))
                {
                    Mi_SQL.Append(" OR Cuentas." +
                            Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro2 + "%' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro3))
                {
                    Mi_SQL.Append(" OR Cuentas." +
                        Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro3 + "%' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro4))
                {
                    Mi_SQL.Append(" OR Cuentas." +
                        Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro4 + "%' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro5))
                {
                    Mi_SQL.Append(" OR Cuentas." +
                            Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro5 + "%' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro6))
                {
                    Mi_SQL.Append(" OR Cuentas." +
                            Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro6 + "%' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro7))
                {
                    Mi_SQL.Append(" OR Cuentas." +
                            Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro7 + "%' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro8))
                {
                    Mi_SQL.Append(" OR Cuentas." +
                            Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro8 + "%' ");
                }
                Mi_SQL.Append(") ORDER BY " + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                //Mi_SQL.Append(" AND Cuentas." + Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta + "='00004'");
                //Mi_SQL.Append(" ORDER BY Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Cuentas
        /// DESCRIPCION: Consulta las cuentas que comiencen con 1,2,3
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO : 22-Febrero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Cuentas(Cls_Rpt_Con_Sit_Fin_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta +
                        ", " + Cat_Con_Cuentas_Contables.Campo_Descripcion + ", " + Cat_Con_Cuentas_Contables.Campo_Nivel_ID);
                Mi_SQL.Append(" FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                Mi_SQL.Append(" WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro1 + "%'");
                  
                if (!string.IsNullOrEmpty(Datos.P_Filtro2))
                {
                    Mi_SQL.Append(" OR " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro2 + "%'");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro3))
                {
                    Mi_SQL.Append(" OR " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro3 + "%'");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro4))
                {
                    Mi_SQL.Append(" OR " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro4 + "%'");
                }
                Mi_SQL.Append(" ORDER BY " + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                //Mi_SQL.Append(" AND Cuentas." + Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta + "='00004'");
                //Mi_SQL.Append(" ORDER BY Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Tercer_Nivel
        /// DESCRIPCION: Consulta las cuentas que comiencen con 1,2,3
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO : 22-Febrero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Cuentas_Tercer_Nivel(Cls_Rpt_Con_Sit_Fin_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta +
                        ", " + Cat_Con_Cuentas_Contables.Campo_Descripcion);
                Mi_SQL.Append(" FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                Mi_SQL.Append(" WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro1 + "%000000'");

                if (!string.IsNullOrEmpty(Datos.P_Filtro2))
                {
                    Mi_SQL.Append(" OR " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro2 + "%000000'");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro3))
                {
                    Mi_SQL.Append(" OR " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro3 + "%000000'");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro4))
                {
                    Mi_SQL.Append(" OR " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro4 + "%000000'");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro5))
                {
                    Mi_SQL.Append(" OR " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro5 + "%000000' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro6))
                {
                    Mi_SQL.Append(" OR " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro6 + "%000000' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro7))
                {
                    Mi_SQL.Append(" OR " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro7 + "%000000' ");
                }
                if (!string.IsNullOrEmpty(Datos.P_Filtro8))
                {
                    Mi_SQL.Append(" OR " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Datos.P_Filtro8 + "%000000' ");
                }
                Mi_SQL.Append(" ORDER BY " + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                //Mi_SQL.Append(" AND Cuentas." + Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta + "='00004'");
                //Mi_SQL.Append(" ORDER BY Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        #endregion
        #region"REPORTES 2"
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Situacion_Financiera
        /// DESCRIPCION: Consulta la situacion financiera de las cuentas que comiencen con 1,2,3
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO : 22-Febrero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Situacion_Financiera_Activo(Cls_Rpt_Con_Sit_Fin_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variabivole que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta +
                        ", Cuentas." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ",");
                Mi_SQL.Append(Ope_Con_Cierre_Mensual.Campo_Total_Debe + ",");
                Mi_SQL.Append(Ope_Con_Cierre_Mensual.Campo_Total_Haber + ",");
                Mi_SQL.Append(Ope_Con_Cierre_Mensual.Campo_Saldo_Inicial + ",");
                Mi_SQL.Append(Ope_Con_Cierre_Mensual.Campo_Saldo_Final);
                Mi_SQL.Append(" FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + " Cierre," +
                        Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " Cuentas");
                Mi_SQL.Append(" WHERE Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "=Cierre." +
                        Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " AND Cierre." +
                        Ope_Con_Cierre_Mensual.Campo_Mes_Anio + "='" + Datos.P_Mes_Año + "'");
                if (Datos.P_filtro == "1")
                {
                    Mi_SQL.Append(" AND " + "Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '1%'");
                }
                else
                {
                    Mi_SQL.Append(" AND " + "Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '2%'");
                }
                Mi_SQL.Append(" ORDER BY " + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Situacion_Financiera
        /// DESCRIPCION: Consulta la situacion financiera de las cuentas que comiencen con 1,2,3
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO : 22-Febrero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Situacion_Financiera_Activo_Acumulado(Cls_Rpt_Con_Sit_Fin_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variabivole que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta +
                        ", Cuentas." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ",");
                Mi_SQL.Append(" SUM("+Ope_Con_Cierre_Mensual.Campo_Total_Debe + ")AS TOTAL_DEBE, SUM(");
                Mi_SQL.Append(Ope_Con_Cierre_Mensual.Campo_Total_Haber + ") AS TOTAL_HABER, SUM(");
                Mi_SQL.Append(Ope_Con_Cierre_Mensual.Campo_Saldo_Inicial + ")AS SALDO_INICIAL, SUM(");
                Mi_SQL.Append(Ope_Con_Cierre_Mensual.Campo_Saldo_Final+")AS SALDO_FINAL");
                Mi_SQL.Append(" FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + " Cierre," +
                        Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " Cuentas");
                Mi_SQL.Append(" WHERE Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "=Cierre." +
                        Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " AND (Cierre." +
                        Ope_Con_Cierre_Mensual.Campo_Mes_Anio + "='" + Datos.P_Mes_Año + "'");
                Mi_SQL.Append(" OR Cierre." + Ope_Con_Cierre_Mensual.Campo_Mes_Anio + "=" + Datos.P_Mes_Año_Ant + ")");
                if (Datos.P_filtro == "1")
                {
                    Mi_SQL.Append(" AND " + "Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '1%'");
                }
                else
                {
                    Mi_SQL.Append(" AND " + "Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '2%'");
                }
                Mi_SQL.Append(" GROUP BY Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta + ",Cuentas." + Cat_Con_Cuentas_Contables.Campo_Descripcion);
                Mi_SQL.Append(" ORDER BY Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Situacion_Financiera
        /// DESCRIPCION: Consulta la situacion financiera de las cuentas que comiencen con 1,2,3
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO : 22-Febrero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Situacion_Financiera2(Cls_Rpt_Con_Sit_Fin_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta +
                        ", Cuentas." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ",");
                Mi_SQL.Append(Ope_Con_Cierre_Mensual.Campo_Saldo_Final);
                Mi_SQL.Append(" FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + " Cierre," +
                        Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " Cuentas");
                Mi_SQL.Append(" WHERE Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "=Cierre." +
                        Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " AND Cierre." +
                        Ope_Con_Cierre_Mensual.Campo_Mes_Anio + "='" + Datos.P_Mes_Año + "'");
                Mi_SQL.Append(" AND " + "(Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '1%' OR Cuentas." +
                        Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '2%' OR Cuentas." +
                        Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '3%' )");
                Mi_SQL.Append(" ORDER BY " + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                //Mi_SQL.Append(" AND Cuentas." + Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta + "='00004'");
                //Mi_SQL.Append(" ORDER BY Cuentas." + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Cuentas
        /// DESCRIPCION: Consulta las cuentas que comiencen con 1,2,3
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO : 22-Febrero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Cuentas2(Cls_Rpt_Con_Sit_Fin_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta +
                        ", " + Cat_Con_Cuentas_Contables.Campo_Descripcion + ", " + Cat_Con_Cuentas_Contables.Campo_Nivel_ID);
                Mi_SQL.Append(" FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                Mi_SQL.Append(" WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '1%00000' OR " +
                        Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '2%00000' OR " +
                        Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '3%00000'");
                Mi_SQL.Append(" ORDER BY " + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Movimientos_Contables
        /// DESCRIPCION: Consulta los movimientos contables de la cuenta
        /// PARAMETROS :
        /// CREO       : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO : 25/Abril/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Movimientos_Contables(Cls_Rpt_Con_Sit_Fin_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                //SELECT POLIZA.FECHA,poliza.no_poliza, tipo.descripcion, cuenta.cuenta, cuenta.descripcion,
                //poliza.cuenta_contable_id,poliza.concepto,poliza.debe,poliza.haber
                //FROM ope_con_polizas_detalles  POLIZA
                //INNER JOIN cat_con_cuentas_contables CUENTA ON poliza.cuenta_contable_id=CUENTA.cuenta_contable_id
                //INNER JOIN cat_con_tipo_polizas TIPO ON poliza.tipo_poliza_id=tipo.tipo_poliza_id
                //WHERE poliza.mes_ano='0112'

                Mi_SQL.Append("SELECT POLIZA." + Ope_Con_Polizas_Detalles.Campo_Fecha + ", POLIZA." + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", TIPO." + Cat_Con_Tipo_Polizas.Campo_Descripcion);
                Mi_SQL.Append(", CUENTA." + Cat_Con_Cuentas_Contables.Campo_Cuenta +", CUENTA."+ Cat_Con_Cuentas_Contables.Campo_Descripcion +", POLIZA."+ Ope_Con_Polizas_Detalles.Campo_Concepto);
                Mi_SQL.Append(", POLIZA." + Ope_Con_Polizas_Detalles.Campo_Debe + ", POLIZA." + Ope_Con_Polizas_Detalles.Campo_Haber);
                Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +" POLIZA INNER JOIN ");
                Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTA ON POLIZA." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " =CUENTA." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append( " INNER JOIN "+Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas + " TIPO ON POLIZA." + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + " =TIPO." + Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID);
                Mi_SQL.Append(" WHERE POLIZA." + Ope_Con_Polizas_Detalles.Campo_Mes_Ano +" ='"+ Datos.P_Mes_Año+"'");
                Mi_SQL.Append(" ORDER BY POLIZA." + Ope_Con_Polizas_Detalles.Campo_Fecha + ", POLIZA." + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", POLIZA." + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        #endregion
    }
}
