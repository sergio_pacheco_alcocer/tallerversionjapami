﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Autoriza_Solicitud_Pago.Negocio;
using JAPAMI.Sessiones;

/// <summary>
/// Summary description for Cls_Ope_Con_Autoriza_Solicitud_Pago_Datos
/// </summary>

namespace JAPAMI.Autoriza_Solicitud_Pago.Datos
{
    public class Cls_Ope_Con_Autoriza_Solicitud_Pago_Datos
    {
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Solicitudes_SinAutotizar
        /// DESCRIPCION : Consulta las solicitudes de pago que estan pendientes de autorizar o rechazar 
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Solicitudes_SinAutotizar(Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            SqlDataAdapter Da_SQL = new SqlDataAdapter();
            DataSet Ds_SQL = new DataSet();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL = "SELECT " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago;
                Mi_SQL += ", " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva;
                Mi_SQL += ", " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID;
                Mi_SQL += ", " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto;
                Mi_SQL += ", " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus;
                Mi_SQL += ", " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Monto;
                Mi_SQL += ", " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Poliza;
                Mi_SQL += ", " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID;
                Mi_SQL += ", " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Mes_Ano;
                Mi_SQL += ", " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + "." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + " as Tipo_Pago , ";
                Mi_SQL += Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + "." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion_Finanzas + " as Tipo_Pago_Finanzas , ";
                Mi_SQL+=Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Compañia + " AS Proveedor ,";
                Mi_SQL+="( " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + " +' '+ ";
                Mi_SQL+=Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + " +' '+ ";
                Mi_SQL+=Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ") as Empleado ";
                Mi_SQL+=" FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                Mi_SQL += " inner join " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " ON " +
                                Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + "=" +
                                Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva;
                Mi_SQL += " inner join " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + " ON " +
                                Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Tipo_Solicitud_Pago_ID + "=" +
                                Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + "." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID;
                Mi_SQL +=" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores;
                Mi_SQL +=" ON " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID;
                Mi_SQL +=" = " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL +=" LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados;
                Mi_SQL +=" ON " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Empleado_ID;
               Mi_SQL +=" = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID;
               Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias ;
               Mi_SQL += " ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID ;
               Mi_SQL += " = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID;
                if (!String.IsNullOrEmpty(Datos.P_Dependencia_ID))
                {
                    Mi_SQL += " WHERE (" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas+"." + Ope_Psp_Reservas.Campo_Dependencia_ID + " IN (";
                    Mi_SQL += "SELECT " + Cat_Dependencias.Campo_Dependencia_ID +
                       " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                       " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " ='" + Cls_Sessiones.Dependencia_ID_Empleado + "'" +
                       " UNION ALL " +
                       "SELECT " + Cat_Dependencias.Campo_Dependencia_ID +
                       " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                       " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " IN (SELECT " +
                       Cat_Det_Empleado_UR.Campo_Dependencia_ID + " FROM " + Cat_Det_Empleado_UR.Tabla_Cat_Det_Empleado_UR +
                       " WHERE " + Cat_Det_Empleado_UR.Campo_Empleado_ID + " ='" + Cls_Sessiones.Empleado_ID.Trim() + "'))" +
                       " OR " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID + " IS NULL)";
                }
                if (!String.IsNullOrEmpty(Datos.P_Grupo_Dependencia))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Cat_Dependencias.Tabla_Cat_Dependencias  + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID + "='" + Datos.P_Grupo_Dependencia + "'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID + "='" + Datos.P_Grupo_Dependencia + "'";
                    }
                    
                }

                if (!String.IsNullOrEmpty(Datos.P_No_Solicitud_Pago))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'";
                    }
                }
                else
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus +
                            " = 'PENDIENTE'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus +
                            " = 'PENDIENTE'";
                    }
                }
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Recurso))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Recurso + " = '" + Datos.P_Tipo_Recurso + "'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_Recurso + " = '" + Datos.P_Tipo_Recurso + "'";
                    }
                }
                Mi_SQL += " ORDER BY " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ASC";
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Da_SQL.SelectCommand = Cmmd;
                Da_SQL.Fill(Ds_SQL);
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Ds_SQL.Tables[0];
               // return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd  == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cambiar_Estatus_Solicitud_Pago
        /// DESCRIPCION : Autoriza o rechaza la solicitud de pago 
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Cambiar_Estatus_Solicitud_Pago(Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Datos)
        {
            String Mi_SQL;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;

            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Modifica el estatus de la solicitud de pago
                Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Comentarios_Jefe_Area + "='" + Datos.P_Comentario + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Estatus + "='" + Datos.P_Estatus + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Jefe_Area + "='" + Datos.P_Empleado_ID_Jefe + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Modifico + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + "= GETDATE(), ";
                if(!String.IsNullOrEmpty(Datos.P_Usuario_Autorizo)){
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Area + "='" + Datos.P_Usuario_Autorizo + "', ";
                }
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Jefe + "= GETDATE() " + " WHERE ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Datos.P_No_Solicitud_Pago + "'";
                Cmmd.CommandText = Mi_SQL.ToString();
                Cmmd.ExecuteNonQuery();
                //SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }

        ///***************************************************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Solicitudes
        /// DESCRIPCION         : Consulta las solicitudes de pago 
        /// PARAMETROS          : Autoriza_Solicitud_Negocio: Recibe los datos proporcionados por el usuario.
        /// CREO                : Leslie Gonzalez Vazquez
        /// FECHA_CREO          : 21/Diciembre/2011
        /// MODIFICO            :
        /// FECHA_MODIFICO      :
        /// CAUSA_MODIFICACION  :
        ///***************************************************************************************************************
        public static DataTable Consulta_Solicitudes(Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Autoriza_Solicitud_Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                Mi_SQL.Append(Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + "." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + " AS Tipo_Pago ,");
                Mi_SQL.Append(Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + "." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion_Finanzas + " AS Tipo_Pago_Finanzas ,");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores  + "." + Cat_Com_Proveedores.Campo_Compañia + " AS Proveedor ,");
                Mi_SQL.Append("( " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + " +' '+ ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + " +' '+ ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ") as Empleado ");
                Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" INNER JOIN " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago);
                Mi_SQL.Append(" ON " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID);
                Mi_SQL.Append(" = " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + "." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID);
                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_SQL.Append(" ON " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_SQL.Append(" = " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores);
                Mi_SQL.Append(" ON " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID);
                Mi_SQL.Append(" = " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados);
                Mi_SQL.Append(" ON " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Empleado_ID);
                Mi_SQL.Append(" = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);
                if (!String.IsNullOrEmpty(Autoriza_Solicitud_Negocio.P_Tipo_Recurso))
                {
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Recurso + "='" + Autoriza_Solicitud_Negocio.P_Tipo_Recurso + "'");
                }                
                if (!String.IsNullOrEmpty(Autoriza_Solicitud_Negocio.P_Estatus))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus);
                        Mi_SQL.Append(" = '" + Autoriza_Solicitud_Negocio.P_Estatus + "'");
                        if (!String.IsNullOrEmpty(Autoriza_Solicitud_Negocio.P_No_Solicitud_Pago))
                        {
                            Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago);
                            Mi_SQL.Append(" = '" + Autoriza_Solicitud_Negocio.P_No_Solicitud_Pago + "'");
                        }
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus);
                        Mi_SQL.Append(" = '" + Autoriza_Solicitud_Negocio.P_Estatus + "'");
                        if (!String.IsNullOrEmpty(Autoriza_Solicitud_Negocio.P_No_Solicitud_Pago))
                        {
                            Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago);
                            Mi_SQL.Append(" = '" + Autoriza_Solicitud_Negocio.P_No_Solicitud_Pago + "'");
                        }
                    }
                }
                Mi_SQL.Append(" ORDER BY " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ASC");


                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Estatus_Recepcion_Documentos
        /// DESCRIPCION         : Consulta para cambiar el estatus de la recepcion de documentos
        /// PARAMETROS          : Autoriza_Solicitud_Negocio: Recibe los datos proporcionados por el usuario.
        /// CREO                : Leslie Gonzalez Vazquez
        /// FECHA_CREO          : 22/Diciembre/2011
        /// MODIFICO            :
        /// FECHA_MODIFICO      :
        /// CAUSA_MODIFICACION  :
        ///*******************************************************************************
        public static void Modificar_Estatus_Recepcion_Documentos(Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Autoriza_Solicitud_Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_Comentarios_Recepcion_Doc + " = '" + Autoriza_Solicitud_Negocio.P_Comentario + "', ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Documentos + " = '" + Autoriza_Solicitud_Negocio.P_Documentos + "', ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Estatus + " = '" + Autoriza_Solicitud_Negocio.P_Estatus + "', ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + " = '" + Autoriza_Solicitud_Negocio.P_Usuario_Modifico + "', ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + " = GETDATE(), ");
                if (!String.IsNullOrEmpty(Autoriza_Solicitud_Negocio.P_Usuario_Recibio_Doc_Fisica))
                {
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Doc_Fisica + " = '" + Autoriza_Solicitud_Negocio.P_Usuario_Recibio_Doc_Fisica + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Doc_Fisica + " = GETDATE() ");
                }
                if (!String.IsNullOrEmpty(Autoriza_Solicitud_Negocio.P_Usuario_Autorizo_Documentos))
                {
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Documentos + " = '" + Autoriza_Solicitud_Negocio.P_Usuario_Autorizo_Documentos + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio + " = '" + Autoriza_Solicitud_Negocio.P_Usuario_Modifico + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recepcion_Documentos + " = GETDATE() ");
                }
                if (!String.IsNullOrEmpty(Autoriza_Solicitud_Negocio.P_Usuario_Recibio_Contabilidad))
                {
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Contabilidad + " = '" + Autoriza_Solicitud_Negocio.P_Usuario_Recibio_Contabilidad + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Contabilidad + " = GETDATE() ");
                }
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Autoriza_Solicitud_Negocio.P_No_Solicitud_Pago + "'");

                SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///***************************************************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Detalles
        /// DESCRIPCION         : Consulta los detalles de las solicitudes
        /// PARAMETROS          : Autoriza_Solicitud_Negocio: Recibe los datos proporcionados por el usuario.
        /// CREO                : Leslie Gonzalez Vazquez
        /// FECHA_CREO          : 21/Diciembre/2011
        /// MODIFICO            :
        /// FECHA_MODIFICO      :
        /// CAUSA_MODIFICACION  :
        ///***************************************************************************************************************
        public static DataTable Consulta_Detalles(Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Autoriza_Solicitud_Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            SqlDataAdapter Da_SQL = new SqlDataAdapter();
            DataSet Ds_SQL = new DataSet();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (Autoriza_Solicitud_Negocio.P_Cmmd != null)
            {
                Cmmd = Autoriza_Solicitud_Negocio.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Jefe + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Comentarios_Jefe_Area + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Recepcion_Documentos + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Comentarios_Recepcion_Doc + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Comentarios_Contabilidad + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Poliza + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Doc_Fisica + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Doc_Fisica + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Documentos + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Contabilidad + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Contabilidad + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Contabilidad + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Ejercido + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Ejercido + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Ejercido + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Pagado + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Pagado + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Autoriza_Rechaza_Ejercido+ ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + " AS CONCEPTO_RESERVA, ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario+", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Fecha_Creo  + " AS FECHA_PAGO, ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Usuario_Creo +" AS USUARIO_PAGO ");
                Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_SQL.Append(" ON " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva);
                Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos);
                Mi_SQL.Append(" ON " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Pago );
                Mi_SQL.Append(" = " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Pago + " and " +Ope_Con_Pagos.Tabla_Ope_Con_Pagos+"."+ Ope_Con_Pagos.Campo_Estatus +"='PAGADO'");
                if (!String.IsNullOrEmpty(Autoriza_Solicitud_Negocio.P_No_Solicitud_Pago))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago);
                        Mi_SQL.Append(" = '" + Autoriza_Solicitud_Negocio.P_No_Solicitud_Pago + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago);
                        Mi_SQL.Append(" = '" + Autoriza_Solicitud_Negocio.P_No_Solicitud_Pago + "'");
                    }
                }
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Da_SQL.SelectCommand = Cmmd;
                Da_SQL.Fill(Ds_SQL);

                if (Autoriza_Solicitud_Negocio.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Ds_SQL.Tables[0];
                //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///***************************************************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Documentos
        /// DESCRIPCION         : Consulta los documentos de la solicitud
        /// PARAMETROS          : 
        /// CREO                : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO          : 02/Marzo/2012
        /// MODIFICO            :
        /// FECHA_MODIFICO      :
        /// CAUSA_MODIFICACION  :
        ///***************************************************************************************************************
        public static DataTable Consulta_Documentos(Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Autoriza_Solicitud_Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append("SELECT DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID +
                    ",PARTIDAS." + Cat_Sap_Partidas_Especificas.Campo_Descripcion + " AS PARTIDA");
                Mi_SQL.Append(", DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura +
                    ", DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Ruta +
                    ", DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Archivo);
                Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " DETALLES " +
                    ", " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDAS");
                Mi_SQL.Append(" WHERE DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID + "=PARTIDAS." +
                    Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " AND DETALLES." +
                    Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + "='" +
                    Autoriza_Solicitud_Negocio.P_No_Solicitud_Pago + "'");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
    }
}

