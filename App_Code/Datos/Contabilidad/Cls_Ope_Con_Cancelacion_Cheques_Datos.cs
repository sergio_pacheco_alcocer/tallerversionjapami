﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Ope_Con_Cancelacion_Cheques.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Cheque.Negocio;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Parametros_Almacen_Cuentas.Negocio;
using JAPAMI.Tipo_Solicitud_Pagos.Negocios;

/// <summary>
/// Summary description for Cls_Ope_Con_Estatus_Cheques_Datos
/// </summary>
namespace JAPAMI.Ope_Con_Cancelacion_Cheques.Datos
{
    public class Cls_Ope_Con_Cancelacion_Cheques_Datos
    {
        public Cls_Ope_Con_Cancelacion_Cheques_Datos()
        {
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Tipos_Beneficiarios
        ///DESCRIPCIÓN: Consulta los tipos de beneficiarios que pueden existir para un cheque
        ///PARAMETROS:  
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 13/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Tipos_Beneficiarios()
        {
            String Mi_SQL = string.Empty;
            try
            {
                Mi_SQL = "SELECT " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ";
                Mi_SQL += "UPPER(" + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + ") AS DESCRIPCION ";
                Mi_SQL += " FROM " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago;
                Mi_SQL += " ORDER BY " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Beneficiarios
        ///DESCRIPCIÓN: Consulta a los posibles empleados beneficiarios
        ///PARAMETROS:  1.- Objeto de la clase de negocios para obtener datos
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 14/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Beneficiarios(Cls_Ope_Con_Cancelacion_Cheques_Negocio Clase_Seguimiento_Negocio)
        {
            String Mi_SQL = string.Empty;

            try
            {
                Mi_SQL = "SELECT DISTINCT " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Beneficiario_Pago;
                Mi_SQL += " FROM " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " JOIN " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos;
                Mi_SQL += " ON " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Solicitud_Pago + " = " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago;
                Mi_SQL += " WHERE " + Ope_Con_Pagos.Campo_Beneficiario_Pago + " LIKE UPPER('%" + Clase_Seguimiento_Negocio.P_Beneficiario + "%') ";
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Tipo_Beneficiario))
                {
                    Mi_SQL += " AND " + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Clase_Seguimiento_Negocio.P_Tipo_Beneficiario + "'";
                }
                Mi_SQL += " ORDER BY " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Beneficiario_Pago;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Cheques_Listado
        ///DESCRIPCIÓN: Consulta los cheques registrados
        ///PARAMETROS:  1.- Cls_Ope_Con_Cancelacion_Cheques_Negocio
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 15/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Cheques_Listado(Cls_Ope_Con_Cancelacion_Cheques_Negocio Clase_Seguimiento_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                Mi_SQL = "SELECT " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_No_Cheque;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Beneficiario;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Concepto;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Monto + " AS  Importe ";
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques +"."+ Ope_Con_Cheques.Campo_Estatus + " AS ESTATUS ";
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Folio;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Fecha + " AS FECHA_EMISION";
                Mi_SQL += " FROM " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + " JOIN " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos;
                Mi_SQL += " ON " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_No_Pago + " = " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Pago;
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Folio_Cheque))
                {
                    Mi_SQL += " WHERE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Folio + " = " + Clase_Seguimiento_Negocio.P_Folio_Cheque;
                }
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Estatus_Entrada))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Estatus + " <> '" + Clase_Seguimiento_Negocio.P_Estatus_Entrada+ "'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Estatus + " <> '" + Clase_Seguimiento_Negocio.P_Estatus_Entrada + "'";
                    }
                }
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Tipo_Beneficiario))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Clase_Seguimiento_Negocio.P_Tipo_Beneficiario + "'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Clase_Seguimiento_Negocio.P_Tipo_Beneficiario + "'";
                    }
                }
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Beneficiario))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Beneficiario + " = '" + Clase_Seguimiento_Negocio.P_Beneficiario + "'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Beneficiario + " = '" + Clase_Seguimiento_Negocio.P_Beneficiario + "'";
                    }
                }
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Fecha_Emision_Inicio) && !String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Fecha_Emision_Fin))
                {
                    Clase_Seguimiento_Negocio.P_Fecha_Emision_Inicio = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Clase_Seguimiento_Negocio.P_Fecha_Emision_Inicio));
                    Clase_Seguimiento_Negocio.P_Fecha_Emision_Fin= string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Clase_Seguimiento_Negocio.P_Fecha_Emision_Fin));
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND ((" + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Fecha + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Inicio + " 00:00:00' AND ";
                        Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Fin + " 23:59:00') ";
                        Mi_SQL += " OR (" + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Fecha_Creo + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Inicio + " 00:00:00' AND ";
                        Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Fin+ " 23:59:00'))";
                    }
                    else
                    {
                        Mi_SQL += " WHERE ((" + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Fecha + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Inicio + " 00:00:00' AND ";
                        Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Fin + " 23:59:00') ";
                        Mi_SQL += " OR (" + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Fecha_Creo + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Inicio + " 00:00:00' AND ";
                        Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Fin + " 23:59:00'))";
                    }
                }

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cancelar_Cheque
        ///DESCRIPCIÓN          : Cancela el cheque
        ///PARAMETROS           :  
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 10/Enero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///********************************************************************************
        public static String Cancelar_Cheque(Cls_Ope_Con_Cancelacion_Cheques_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Consultamos los datos del cheque
                String Mi_SQL = "SELECT * FROM " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Con_Cheques.Campo_No_Cheque + " = " + Clase_Negocio.P_No_Cheque_ID;
                DataTable Dt_Datos_Cheque = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Insertar un registro en el seguimiento del cheque
                Mi_SQL = "INSERT INTO " + Ope_Con_Seguimiento_Cheques.Tabla_Ope_Con_Seguimiento_Cheques +
                    " (" + Ope_Con_Seguimiento_Cheques.Campo_No_Cheque +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Empleado_Recibio_ID +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Estatus_Entrada +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Fecha_Entrada +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Estatus_Salida +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Fecha_Salida +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Usuario_Creo +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Fecha_Creo +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Comentarios +
                    ") VALUES (" + Clase_Negocio.P_No_Cheque_ID + ",'" +
                     Cls_Sessiones.Empleado_ID + "','" +
                     Dt_Datos_Cheque.Rows[0][Ope_Con_Cheques.Campo_Estatus].ToString() + "', " +
                     "( SELECT " + Ope_Con_Cheques.Campo_Fecha + " FROM " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques +
                        " WHERE " + Ope_Con_Cheques.Campo_No_Cheque + " = " + Clase_Negocio.P_No_Cheque_ID + "), " +
                     " '" + Clase_Negocio.P_Estatus_Salida + "', " +
                     " GETDATE(),'" +
                     Cls_Sessiones.Empleado_ID + "', " +
                     " GETDATE()" + 
                     ", '" + Clase_Negocio.P_Comentarios + "')";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Actualizar estatus en la clase de cheques 
                Mi_SQL = "UPDATE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques;
                Mi_SQL = Mi_SQL + " SET " + Ope_Con_Cheques.Campo_Estatus + " = '" + Clase_Negocio.P_Estatus_Salida + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Con_Cheques.Campo_Usuario_Modifico + "= '" + Cls_Sessiones.Nombre_Empleado + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Con_Cheques.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Con_Cheques.Campo_No_Cheque + " = " + Clase_Negocio.P_No_Cheque_ID;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Actualizar el estatus en la tabla de ope_con_pagos
                Mi_SQL = "UPDATE " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos;
                Mi_SQL = Mi_SQL + " SET " + Ope_Con_Pagos.Campo_Estatus + " = '" + Clase_Negocio.P_Estatus_Salida + "'";
                Mi_SQL = Mi_SQL+ ", " + Ope_Con_Pagos.Campo_Motivo_Cancelacion + " = '" + Clase_Negocio.P_Comentarios + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Con_Pagos.Campo_Usuario_Modifico + "= '" + Cls_Sessiones.Nombre_Empleado + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Con_Pagos.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL = Mi_SQL + " WHERE  " + Ope_Con_Pagos.Campo_No_Pago + " = '" + Dt_Datos_Cheque.Rows[0][Ope_Con_Cheques.Campo_No_Pago].ToString() + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
                Mensaje_Error = "Cancelado";
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo cancelar el cheque";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }//fin de Rechazar_Cheque
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Rechazar_Cheque_Masiva
        ///DESCRIPCIÓN          : Cancela el cheque
        ///PARAMETROS           :  
        ///CREO                 : Sergio Manuel Galardo Andrade
        ///FECHA_CREO           : 12/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///********************************************************************************
        public static String Rechazar_Cheque_Masiva(Cls_Ope_Con_Cancelacion_Cheques_Negocio Clase_Negocio)
        {
            Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
            String No_Solicitud = "";
            String Proveedor = "";
            Int32 Contador = 0;
            String Servicios_Generales = "";
            Boolean Insertar = false;
            String Mensaje_Error = "";
            String Resultado = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            String Tipo_Solicitud_Pago = "";
            DataTable Dt_Datos_Cheques = new DataTable();
            DataTable Dt_Temporal = new DataTable();
            DataTable Dt_Datos_Solicitudes = new DataTable();
            DataTable Dt_Solicitud_Pagos = new DataTable();
            SqlDataAdapter Dt_Sql = new SqlDataAdapter();
            DataSet Ds_Sql = new DataSet();
            String Mi_SQL = string.Empty; 
            Cls_Ope_Con_Cheques_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Cheques_Negocio(); //Variable de coneción hacia la capa de datos
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            
            try
            {
                foreach (DataRow Fila_Cheques in Clase_Negocio.P_Dt_Cheques.Rows)
                {
                    Mi_SQL = "";
                    //Consultamos los datos del cheque
                    Mi_SQL = "SELECT * FROM " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Con_Cheques.Campo_No_Cheque + " = " + Fila_Cheques["NO_CHEQUE"].ToString();
                    Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Dt_Sql.SelectCommand = Cmd;
                    Dt_Sql.Fill(Ds_Sql);
                    Dt_Datos_Cheques = Ds_Sql.Tables[0];
                        //DataTable Dt_Datos_Cheque = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                    //Insertar un registro en el seguimiento del cheque
                    Mi_SQL = "INSERT INTO " + Ope_Con_Seguimiento_Cheques.Tabla_Ope_Con_Seguimiento_Cheques +
                        " (" + Ope_Con_Seguimiento_Cheques.Campo_No_Cheque +
                        ", " + Ope_Con_Seguimiento_Cheques.Campo_Empleado_Recibio_ID +
                        ", " + Ope_Con_Seguimiento_Cheques.Campo_Estatus_Entrada +
                        ", " + Ope_Con_Seguimiento_Cheques.Campo_Estatus_Salida +
                        ", " + Ope_Con_Seguimiento_Cheques.Campo_Usuario_Creo +
                        ", " + Ope_Con_Seguimiento_Cheques.Campo_Fecha_Creo +
                        ", " + Ope_Con_Seguimiento_Cheques.Campo_Comentarios +
                        ", " + Ope_Con_Seguimiento_Cheques.Campo_Fecha_Entrada +
                        ") VALUES (" + Fila_Cheques["NO_CHEQUE"].ToString() + ",'" +
                         Cls_Sessiones.Empleado_ID + "','" + Clase_Negocio.P_Estatus_Entrada + "', '" + Clase_Negocio.P_Estatus_Entrada + "', '" +
                         Cls_Sessiones.Empleado_ID + "', " +
                         " GETDATE()" +
                         ", '" + Clase_Negocio.P_Comentarios + "', GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Actualizar estatus en la clase de cheques 
                    Mi_SQL = "UPDATE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Con_Cheques.Campo_Estatus + " = '" + Clase_Negocio.P_Estatus_Entrada + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Con_Cheques.Campo_Usuario_Modifico + "= '" + Cls_Sessiones.Nombre_Empleado + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Con_Cheques.Campo_Fecha_Modifico + " = GETDATE() ";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Con_Cheques.Campo_No_Cheque + " = " + Fila_Cheques["NO_CHEQUE"].ToString();
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Actualizar el estatus en la tabla de ope_con_pagos
                    Mi_SQL = "UPDATE " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Con_Pagos.Campo_Estatus + " = '" + Clase_Negocio.P_Estatus_Entrada + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Con_Pagos.Campo_Motivo_Cancelacion + " = '" + Clase_Negocio.P_Comentarios + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Con_Pagos.Campo_Usuario_Modifico + "= '" + Cls_Sessiones.Nombre_Empleado + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Con_Pagos.Campo_Fecha_Modifico + " = GETDATE() ";
                    Mi_SQL = Mi_SQL + " WHERE  " + Ope_Con_Pagos.Campo_No_Pago + " = '" + Dt_Datos_Cheques.Rows[0][Ope_Con_Cheques.Campo_No_Pago].ToString() + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    //se limpia el dataset
                    Ds_Sql = new DataSet();
                    //Consultar las solicitudes que se pagaron con ese cheque 
                    Mi_SQL = "SELECT * FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Pago + " = '" + Dt_Datos_Cheques.Rows[0][Ope_Con_Cheques.Campo_No_Pago].ToString().Trim()+"'";
                    Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Dt_Sql.SelectCommand = Cmd;
                    Dt_Sql.Fill(Ds_Sql);
                    Dt_Datos_Solicitudes = Ds_Sql.Tables[0];
                    if(Dt_Datos_Solicitudes.Rows.Count>0)
                    {
                        if (!String.IsNullOrEmpty(Dt_Datos_Solicitudes.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales].ToString()))
                        {
                            Servicios_Generales = Dt_Datos_Solicitudes.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales].ToString();
                        }
                        else
                        {
                            Servicios_Generales = "NO";
                        }
                        Tipo_Solicitud_Pago = Dt_Datos_Solicitudes.Rows[0][Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID].ToString();
                        if (Dt_Temporal.Rows.Count <= 0 && Dt_Temporal.Columns.Count <= 0)
                        {
                            Dt_Temporal.Columns.Add("No_Solicitud", typeof(System.String));
                            Dt_Temporal.Columns.Add("Proveedor", typeof(System.String));
                            Dt_Temporal.Columns.Add("Banco_Pago", typeof(System.String));
                            Dt_Temporal.Columns.Add("Monto", typeof(System.Decimal));
                        }
                        foreach (DataRow Registro_Solicitud in Dt_Datos_Solicitudes.Rows)
                        {
                            No_Solicitud = Registro_Solicitud["No_Solicitud_Pago"].ToString();
                            Proveedor = Registro_Solicitud["Proveedor_ID"].ToString();
                            Insertar = false;
                            if (Contador == 0)
                            {
                                Contador++;
                                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud;
                                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Estatus = "PAGADO";
                                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmd;
                                Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Solicitudes_Autorizadas();
                                if (Dt_Solicitud_Pagos.Rows.Count > 0)
                                {
                                    DataRow row = Dt_Temporal.NewRow(); //Crea un nuevo registro a la tabla
                                    //Asigna los valores al nuevo registro creado a la tabla
                                    row["No_Solicitud"] = Registro_Solicitud["No_Solicitud_Pago"].ToString();
                                    row["Proveedor"] = Registro_Solicitud["Proveedor_ID"].ToString();
                                    row["Banco_Pago"] = Dt_Solicitud_Pagos.Rows[0]["Banco_Pago"].ToString();
                                    row["Monto"] = Registro_Solicitud["Monto"].ToString();
                                    Dt_Temporal.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Temporal.AcceptChanges();
                                }
                            }
                            else
                            {
                                Contador++;
                                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud;
                                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Estatus = "PAGADO";
                                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmd;
                                Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Solicitudes_Autorizadas();
                                if (Dt_Solicitud_Pagos.Rows.Count > 0)
                                {
                                    foreach (DataRow Fila_Solicitud in Dt_Temporal.Rows)
                                    {
                                        if (Fila_Solicitud["Proveedor"].ToString() == Proveedor && Fila_Solicitud["Banco_Pago"].ToString() == Dt_Solicitud_Pagos.Rows[0]["Banco_Pago"].ToString())
                                        {
                                            Insertar = true;
                                        }
                                    }
                                    if (Insertar)
                                    {
                                        DataRow row = Dt_Temporal.NewRow(); //Crea un nuevo registro a la tabla
                                        //Asigna los valores al nuevo registro creado a la tabla
                                        row["No_Solicitud"] = Registro_Solicitud["No_Solicitud_Pago"].ToString();
                                        row["Proveedor"] = Registro_Solicitud["Proveedor_ID"].ToString();
                                        row["Banco_Pago"] = Dt_Solicitud_Pagos.Rows[0]["Banco_Pago"].ToString();
                                        row["Monto"] = Registro_Solicitud["Monto"].ToString();
                                        Dt_Temporal.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                        Dt_Temporal.AcceptChanges();
                                    }
                                }
                            }
                        }
                    }
                    if (Dt_Temporal.Rows.Count > 0)
                    {
                        DataTable Dt_Tipo = new DataTable();
                        Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Tipo_Solicitud_Pago;
                        Rs_Consulta.P_Cmmd = Cmd;
                        Dt_Tipo = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos();
                        if (Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().ToUpper() != "FINIQUITO" || Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().Substring(0, 4).ToUpper() != "FINI")
                        {
                            if (Servicios_Generales == "NO")
                            {
                                Resultado = Alta_Cheque_Varias_Solicitudes(Dt_Temporal, Cmd);
                            }
                            else
                            {
                                Resultado = Alta_Cheque_Varias_Solicitudes_Servicios_Generales(Dt_Temporal, Cmd);
                            }
                        }
                        else
                        {
                            Resultado = Alta_Cheque_Varias_Solicitudes_Finiquito(Dt_Temporal, Cmd);
                        }
                    }
                }
                if (Resultado == "SI")
                {
                    Trans.Commit();
                    Mensaje_Error = "Cancelado";
                }
                else
                {
                    Trans.Rollback();
                    Mensaje_Error = "No se pudo cancelar el cheque";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo cancelar el cheque";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Cheque_Varias_Solicitudes
        ///DESCRIPCIÓN          : Cancela el cheque
        ///PARAMETROS           :  
        ///CREO                 : Sergio Manuel Galardo Andrade
        ///FECHA_CREO           : 12/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///********************************************************************************
        public static String Alta_Cheque_Varias_Solicitudes(DataTable Dt_Solicitudes, SqlCommand P_Cmmd)
        {
            Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexion con la capa de datos
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de coneción hacia la capa de datos
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
            //Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
            DataTable Dt_Cuentas_Iva = new DataTable();
            DataTable Dt_Consulta_Folio = new DataTable();
            DataTable Dt_Banco_Cuenta_Contable = new DataTable();
            DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            Decimal Monto_Cedular = 0;
            Decimal Monto_ISR = 0;
            Decimal Monto_Total = 0;
            String No_Cheque = "";
            String Cheque = "";
            String CTA_IVA_Acreditable = "";
            String CTA_IVA_Pendiente_Acreditar = "";
            Decimal Total_Iva_por_Acreditar = 0;
            DataTable Dt_Solicitud_Pagos = new DataTable();
            String Cuenta_Contable_Banco_ID = "";
            String Banco_ID = "";
            String Beneficiario = "";
            String Resultado = "SI";
            String Cuenta_Contable_Proveedor_ID = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //se optiene la cuenta contable del banco con la que se va a realizar el pago
                Banco_ID = Dt_Solicitudes.Rows[0]["Banco_pago"].ToString();
                Rs_Ope_Con_Cheques.P_Banco_ID = Dt_Solicitudes.Rows[0]["Banco_pago"].ToString();
                Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Cheques.Consulta_Cuenta_Contable_Banco();
                foreach (DataRow Renglon in Dt_Banco_Cuenta_Contable.Rows)
                {
                    Cuenta_Contable_Banco_ID = Renglon[Cat_Nom_Bancos.Campo_Cuenta_Contable_ID].ToString();
                }
                foreach (DataRow Renglon in Dt_Solicitudes.Rows)
                {
                    Dt_Solicitud_Pagos = new DataTable();
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Renglon["No_Solicitud"].ToString();
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                    if (Dt_Solicitud_Pagos.Rows.Count > 0)
                    {
                        Cuenta_Contable_Proveedor_ID = Dt_Solicitud_Pagos.Rows[0]["Pro_Cuenta_Proveedor"].ToString();
                        Beneficiario = Dt_Solicitud_Pagos.Rows[0][Ope_Psp_Reservas.Campo_Beneficiario].ToString();
                        foreach (DataRow Registro in Dt_Solicitud_Pagos.Rows)
                        {
                            Monto_Cedular = Monto_Cedular + Convert.ToDecimal(Registro["MONTO_CEDULAR"].ToString());
                            Monto_ISR = Monto_ISR + Convert.ToDecimal(Registro["MONTO_ISR"].ToString());
                        }
                    }
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = Renglon["No_Solicitud"].ToString();
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                    Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
                    if (Dt_Datos_Solicitud.Rows.Count > 0)
                    {
                        foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
                        {
                            if (!String.IsNullOrEmpty(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString()))
                            {
                                Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString());
                            }
                        }
                    }
                    Monto_Total = Monto_Total + Convert.ToDecimal(Dt_Solicitud_Pagos.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Monto].ToString());
                }
                #region "Partidas Poliza
                //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add("BENEFICIARIO_ID", typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));
                DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                //Agrega el abono del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Banco_ID;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Monto_Total - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";

                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                row = Dt_Partidas_Polizas.NewRow();
                //Agrega el cargo del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Proveedor_ID;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] =  Monto_Total - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "CANCELACION";
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();

                //Se agrega la partida de iva pendiente de acreditar
                Rs_Parametros_Iva.P_Cmmd = Cmmd;
                Dt_Cuentas_Iva = Rs_Parametros_Iva.Consultar_Cuentas();
                if (Dt_Cuentas_Iva.Rows.Count > 0)
                {
                    if (Total_Iva_por_Acreditar > 0)
                    {
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            CTA_IVA_Acreditable = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Acreditable].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(CTA_IVA_Acreditable))
                        {
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 3;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Acreditable;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Iva_por_Acreditar;
                            row["Beneficiario_ID"] = "";
                            row["Tipo_Beneficiario"] = "";
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            CTA_IVA_Pendiente_Acreditar = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Pendiente].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(CTA_IVA_Pendiente_Acreditar))
                        {
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 4;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Pendiente_Acreditar;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Iva_por_Acreditar;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            row["Beneficiario_ID"] = "";
                            row["Tipo_Beneficiario"] = "";
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                }
                #endregion
                if (Cuenta_Contable_Banco_ID != "" && Cuenta_Contable_Proveedor_ID != "" && CTA_IVA_Acreditable != "" && CTA_IVA_Pendiente_Acreditar != "")
                {
                    Rs_Ope_Con_Cheques.P_Dt_Solicitudes_Masivas = Dt_Solicitudes;
                    Rs_Ope_Con_Cheques.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                    Rs_Ope_Con_Cheques.P_Monto = Monto_Total.ToString();
                    Rs_Ope_Con_Cheques.P_Monto_Transferencia = Convert.ToString(Monto_Total - (Monto_Cedular + Monto_ISR) + Total_Iva_por_Acreditar);
                    Rs_Ope_Con_Cheques.P_No_Partidas = "4";
                    Rs_Ope_Con_Cheques.P_Estatus = "EJERCIDO";
                    Rs_Ope_Con_Cheques.P_Fecha_Pago = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                    Rs_Ope_Con_Cheques.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now).ToString();
                    Rs_Ope_Con_Cheques.P_Banco_ID = Banco_ID;
                    //Rs_Ope_Con_Cheques.P_No_Cheque = No_Cheque;
                    Rs_Ope_Con_Cheques.P_Fecha_Creo = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                    Rs_Ope_Con_Cheques.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                    Rs_Ope_Con_Cheques.P_IVA = Total_Iva_por_Acreditar;
                    Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                    Cheque = Rs_Ope_Con_Cheques.Cancelacion_Masiva_Cheque();
                    Resultado = Cheque;
                }
                else
                {
                    if (Cuenta_Contable_Banco_ID == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        //Lbl_Mensaje_Error.Visible = true;
                        //Img_Error.Visible = true;
                        //Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    if (Cuenta_Contable_Proveedor_ID == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        //Lbl_Mensaje_Error.Visible = true;
                        //Img_Error.Visible = true;
                        //Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    if (CTA_IVA_Acreditable == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        //Lbl_Mensaje_Error.Visible = true;
                        //Img_Error.Visible = true;
                        //Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    if (CTA_IVA_Pendiente_Acreditar == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        //Lbl_Mensaje_Error.Visible = true;
                        //Img_Error.Visible = true;
                        //Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    Resultado = "NO";
                }
            }
            catch (Exception ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //Lbl_Mensaje_Error.Visible = true;
                //Img_Error.Visible = true;
                //Lbl_Mensaje_Error.Text = ex.Message.ToString();
                Resultado = "NO";
            }
            return Resultado;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Cheque_Varias_Solicitudes_Servicios_Generales
        ///DESCRIPCIÓN          : Cancela el cheque
        ///PARAMETROS           :  
        ///CREO                 : Sergio Manuel Galardo Andrade
        ///FECHA_CREO           : 04/Septiembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///********************************************************************************
        public static String Alta_Cheque_Varias_Solicitudes_Servicios_Generales(DataTable Dt_Solicitudes, SqlCommand P_Cmmd)
        {
            Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexion con la capa de datos
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de coneción hacia la capa de datos
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
            DataTable Dt_Cuentas_Iva = new DataTable();
            DataTable Dt_Consulta_Folio = new DataTable();
            DataTable Dt_Banco_Cuenta_Contable = new DataTable();
            DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            Decimal Monto_Total = 0;
            String Cheque = "";
            String CTA_IVA_Acreditable = "";
            String CTA_IVA_Pendiente_Acreditar = "";
            Decimal Total_Iva_por_Acreditar = 0;
            DataTable Dt_Solicitud_Pagos = new DataTable();
            String Cuenta_Contable_Banco_ID = "";
            String Banco_ID = "";
            String Beneficiario = "";
            String Resultado = "SI";
            String Cuenta_Contable_Proveedor_ID = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //se optiene la cuenta contable del banco con la que se va a realizar el pago
                Banco_ID = Dt_Solicitudes.Rows[0]["Banco_pago"].ToString();
                Rs_Ope_Con_Cheques.P_Banco_ID = Dt_Solicitudes.Rows[0]["Banco_pago"].ToString();
                Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Cheques.Consulta_Cuenta_Contable_Banco();
                foreach (DataRow Renglon in Dt_Banco_Cuenta_Contable.Rows)
                {
                    Cuenta_Contable_Banco_ID = Renglon[Cat_Nom_Bancos.Campo_Cuenta_Contable_ID].ToString();
                }
                foreach (DataRow Renglon in Dt_Solicitudes.Rows)
                {
                    Dt_Solicitud_Pagos = new DataTable();
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Renglon["No_Solicitud"].ToString();
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago_Servicios_Generales();
                    if (Dt_Solicitud_Pagos.Rows.Count > 0)
                    {
                        Cuenta_Contable_Proveedor_ID = Dt_Solicitud_Pagos.Rows[0]["Pro_Cuenta_Proveedor"].ToString();
                        Beneficiario = Dt_Solicitud_Pagos.Rows[0][Ope_Psp_Reservas.Campo_Beneficiario].ToString();
                    }
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = Renglon["No_Solicitud"].ToString();
                    Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                    Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Datos_Solicitud_Pago_Servicios_Generales();
                    if (Dt_Datos_Solicitud.Rows.Count > 0)
                    {
                        foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
                        {
                            if (!String.IsNullOrEmpty(Fila_Iva["IVA_MONTO"].ToString()))
                            {
                                Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva["IVA_MONTO"].ToString());
                            }
                        }
                    }
                    Monto_Total = Monto_Total + Convert.ToDecimal(Dt_Solicitud_Pagos.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Monto].ToString());
                }
                #region "Partidas Poliza
                //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add("BENEFICIARIO_ID", typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));
                DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                //Agrega el abono del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Banco_ID;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Monto_Total;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";

                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                row = Dt_Partidas_Polizas.NewRow();
                //Agrega el cargo del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Proveedor_ID;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] =  Monto_Total;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "CANCELACION";
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();

                //Se agrega la partida de iva pendiente de acreditar
                Rs_Parametros_Iva.P_Cmmd = Cmmd;
                Dt_Cuentas_Iva = Rs_Parametros_Iva.Consultar_Cuentas();
                if (Dt_Cuentas_Iva.Rows.Count > 0)
                {
                    if (Total_Iva_por_Acreditar > 0)
                    {
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            CTA_IVA_Acreditable = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Acreditable].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(CTA_IVA_Acreditable))
                        {
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 3;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Acreditable;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Iva_por_Acreditar;
                            row["Beneficiario_ID"] = "";
                            row["Tipo_Beneficiario"] = "";
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            CTA_IVA_Pendiente_Acreditar = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Pendiente].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(CTA_IVA_Pendiente_Acreditar))
                        {
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 4;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Pendiente_Acreditar;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Iva_por_Acreditar;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            row["Beneficiario_ID"] = "";
                            row["Tipo_Beneficiario"] = "";
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                }
                #endregion
                if (Cuenta_Contable_Banco_ID != "" && Cuenta_Contable_Proveedor_ID != "" )
                {
                    if (Total_Iva_por_Acreditar > 0)
                    {
                        if (CTA_IVA_Acreditable != "" && CTA_IVA_Pendiente_Acreditar != "")
                        {
                            Rs_Ope_Con_Cheques.P_Dt_Solicitudes_Masivas = Dt_Solicitudes;
                            Rs_Ope_Con_Cheques.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                            Rs_Ope_Con_Cheques.P_Monto = Monto_Total.ToString();
                            Rs_Ope_Con_Cheques.P_Monto_Transferencia = Convert.ToString(Monto_Total + Total_Iva_por_Acreditar);
                            Rs_Ope_Con_Cheques.P_No_Partidas = "4";
                            Rs_Ope_Con_Cheques.P_Estatus = "EJERCIDO";
                            Rs_Ope_Con_Cheques.P_Fecha_Pago = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                            Rs_Ope_Con_Cheques.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now).ToString();
                            Rs_Ope_Con_Cheques.P_Banco_ID = Banco_ID;
                            //Rs_Ope_Con_Cheques.P_No_Cheque = No_Cheque;
                            Rs_Ope_Con_Cheques.P_Fecha_Creo = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                            Rs_Ope_Con_Cheques.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                            Rs_Ope_Con_Cheques.P_IVA = Total_Iva_por_Acreditar;
                            Rs_Ope_Con_Cheques.P_Servicios_Generales = "SI";
                            Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                            Cheque = Rs_Ope_Con_Cheques.Cancelacion_Masiva_Cheque();
                            Resultado = Cheque;
                        }
                        else
                        {
                            if (CTA_IVA_Acreditable == "")
                            {
                                if (P_Cmmd == null)
                                {
                                    Trans.Rollback();
                                }
                                //Lbl_Mensaje_Error.Visible = true;
                                //Img_Error.Visible = true;
                                //Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                            }
                            if (CTA_IVA_Pendiente_Acreditar == "")
                            {
                                if (P_Cmmd == null)
                                {
                                    Trans.Rollback();
                                }
                                //Lbl_Mensaje_Error.Visible = true;
                                //Img_Error.Visible = true;
                                //Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                            }
                            Resultado = "NO";
                        }
                    }
                    else
                    {
                        Rs_Ope_Con_Cheques.P_Dt_Solicitudes_Masivas = Dt_Solicitudes;
                        Rs_Ope_Con_Cheques.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                        Rs_Ope_Con_Cheques.P_Monto = Monto_Total.ToString();
                        Rs_Ope_Con_Cheques.P_Monto_Transferencia = Convert.ToString(Monto_Total + Total_Iva_por_Acreditar);
                        Rs_Ope_Con_Cheques.P_No_Partidas = "4";
                        Rs_Ope_Con_Cheques.P_Estatus = "EJERCIDO";
                        Rs_Ope_Con_Cheques.P_Fecha_Pago = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                        Rs_Ope_Con_Cheques.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now).ToString();
                        Rs_Ope_Con_Cheques.P_Banco_ID = Banco_ID;
                        //Rs_Ope_Con_Cheques.P_No_Cheque = No_Cheque;
                        Rs_Ope_Con_Cheques.P_Fecha_Creo = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                        Rs_Ope_Con_Cheques.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                        Rs_Ope_Con_Cheques.P_IVA = Total_Iva_por_Acreditar;
                        Rs_Ope_Con_Cheques.P_Servicios_Generales = "SI";
                        Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                        Cheque = Rs_Ope_Con_Cheques.Cancelacion_Masiva_Cheque();
                        Resultado = Cheque;
                    }
                }
                else
                {
                    if (Cuenta_Contable_Banco_ID == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        //Lbl_Mensaje_Error.Visible = true;
                        //Img_Error.Visible = true;
                        //Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    if (Cuenta_Contable_Proveedor_ID == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        //Lbl_Mensaje_Error.Visible = true;
                        //Img_Error.Visible = true;
                        //Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    if (CTA_IVA_Acreditable == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        //Lbl_Mensaje_Error.Visible = true;
                        //Img_Error.Visible = true;
                        //Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    if (CTA_IVA_Pendiente_Acreditar == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        //Lbl_Mensaje_Error.Visible = true;
                        //Img_Error.Visible = true;
                        //Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    Resultado = "NO";
                }
            }
            catch (Exception ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //Lbl_Mensaje_Error.Visible = true;
                //Img_Error.Visible = true;
                //Lbl_Mensaje_Error.Text = ex.Message.ToString();
                Resultado = "NO";
            }
            return Resultado;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Cheque_Varias_Solicitudes_Finiquito
        ///DESCRIPCIÓN          : Cancela el cheque
        ///PARAMETROS           :  
        ///CREO                 : Sergio Manuel Galardo Andrade
        ///FECHA_CREO           : 19/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///********************************************************************************
        public static String Alta_Cheque_Varias_Solicitudes_Finiquito(DataTable Dt_Solicitudes, SqlCommand P_Cmmd)
        {
            Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexion con la capa de datos
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de coneción hacia la capa de datos
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Cuentas_Iva = new DataTable();
            DataTable Dt_Consulta_Folio = new DataTable();
            DataTable Dt_Banco_Cuenta_Contable = new DataTable();
            DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            Decimal Monto_Total = 0;
            String Cheque = ""; 
            Decimal Total_Debe = 0;
            Decimal Total_Haber = 0;
            DataTable Dt_Deducciones_Persepciones = new DataTable();
            DataTable Dt_Cuenta_Empleado = new DataTable();
            String CTA_IVA_Acreditable = "";
            String CTA_IVA_Pendiente_Acreditar = "";
            Decimal Total_Iva_por_Acreditar = 0;
            DataTable Dt_Solicitud_Pagos = new DataTable();
            String Cuenta_Contable_Banco_ID = "";
            String Banco_ID = "";
            String Resultado = "SI";
            String Cuenta_Contable_Proveedor_ID = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //se optiene la cuenta contable del banco con la que se va a realizar el pago
                Banco_ID = Dt_Solicitudes.Rows[0]["Banco_pago"].ToString();
                Rs_Ope_Con_Cheques.P_Banco_ID = Dt_Solicitudes.Rows[0]["Banco_pago"].ToString();
                Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Cheques.Consulta_Cuenta_Contable_Banco();
                foreach (DataRow Renglon in Dt_Banco_Cuenta_Contable.Rows)
                {
                    Cuenta_Contable_Banco_ID = Renglon[Cat_Nom_Bancos.Campo_Cuenta_Contable_ID].ToString();
                }
                foreach (DataRow Renglon in Dt_Solicitudes.Rows)
                {
                    Dt_Solicitud_Pagos = new DataTable();
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Renglon["No_Solicitud"].ToString();
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                    Rs_Parametros_Iva.P_Cmmd = Cmmd;
                    Dt_Cuenta_Empleado = Rs_Parametros_Iva.Obtener_Parametros_Poliza();//se optiene la cuenta del empleado
                    if (Dt_Cuenta_Empleado.Rows.Count > 0)
                    {
                        Cuenta_Contable_Proveedor_ID = Dt_Cuenta_Empleado.Rows[0][Cat_Nom_Parametros_Poliza.Campo_Cuenta_Servicios_Profecionales_Pagar_A_CP].ToString();
                    }
                    else
                    {
                        Cuenta_Contable_Proveedor_ID = "";
                    }
                    ///Se consultan los detalles de las deducciones y persepciones del finiquito
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Dt_Solicitudes.Rows[0]["No_Solicitud"].ToString(); ;
                    Dt_Deducciones_Persepciones = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consultar_Datos_Solicitud_Finiquito();
                    if (Dt_Deducciones_Persepciones.Rows.Count > 0)
                    {
                        foreach (DataRow Fila_D_P in Dt_Deducciones_Persepciones.Rows)
                        {
                            if (Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString()) != 0)
                            {
                                Total_Debe = Total_Debe + Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString());
                            }
                            else
                            {
                                Total_Haber = Total_Haber + Convert.ToDecimal(Fila_D_P[Ope_Con_Solicitud_Finiquito_Det.Campo_Haber].ToString());
                            }
                        }
                    }
                    Monto_Total = Total_Debe - Total_Haber;
                }
                #region "Partidas Poliza
                //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add("BENEFICIARIO_ID", typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));
                DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                //Agrega el abono del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Banco_ID;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Monto_Total;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";

                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                row = Dt_Partidas_Polizas.NewRow();
                //Agrega el cargo del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Proveedor_ID;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] =  Monto_Total;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "CANCELACION";
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                #endregion
                if (Cuenta_Contable_Banco_ID != "" && Cuenta_Contable_Proveedor_ID != "")
                {
                    Rs_Ope_Con_Cheques.P_Dt_Solicitudes_Masivas = Dt_Solicitudes;
                    Rs_Ope_Con_Cheques.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                    Rs_Ope_Con_Cheques.P_Monto = Monto_Total.ToString();
                    Rs_Ope_Con_Cheques.P_Monto_Transferencia = Convert.ToString(Monto_Total);
                    Rs_Ope_Con_Cheques.P_No_Partidas = "2";
                    Rs_Ope_Con_Cheques.P_Estatus = "EJERCIDO";
                    Rs_Ope_Con_Cheques.P_Fecha_Pago = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                    Rs_Ope_Con_Cheques.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now).ToString();
                    Rs_Ope_Con_Cheques.P_Banco_ID = Banco_ID;
                    Rs_Ope_Con_Cheques.P_Fecha_Creo = String.Format("{0:dd/MMM/yyyy}", DateTime.Now).ToString();
                    Rs_Ope_Con_Cheques.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                    Rs_Ope_Con_Cheques.P_IVA = Total_Iva_por_Acreditar;
                    Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                    Cheque = Rs_Ope_Con_Cheques.Cancelacion_Masiva_Cheque_Finiquito();
                    Resultado = Cheque;
                }
                else
                {
                    if (Cuenta_Contable_Banco_ID == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        //Lbl_Mensaje_Error.Visible = true;
                        //Img_Error.Visible = true;
                        //Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    if (Cuenta_Contable_Proveedor_ID == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        //Lbl_Mensaje_Error.Visible = true;
                        //Img_Error.Visible = true;
                        //Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    if (CTA_IVA_Acreditable == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        //Lbl_Mensaje_Error.Visible = true;
                        //Img_Error.Visible = true;
                        //Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    if (CTA_IVA_Pendiente_Acreditar == "")
                    {
                        if (P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        //Lbl_Mensaje_Error.Visible = true;
                        //Img_Error.Visible = true;
                        //Lbl_Mensaje_Error.Text = "El Banco del Pago no tiene Cuenta Contable Asignada Favor de verificarlo con Contabilidad";
                    }
                    Resultado = "NO";
                }
            }
            catch (Exception ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //Lbl_Mensaje_Error.Visible = true;
                //Img_Error.Visible = true;
                //Lbl_Mensaje_Error.Text = ex.Message.ToString();
                Resultado = "NO";
            }
            return Resultado;
        }
        
    }
}