﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Cheques_Bancos.Negocio;

namespace JAPAMI.Cheques_Bancos.Datos
{
    public class Cls_Ope_Con_Cheques_Bancos_Datos
    {
        #region Alta-Modificacion-Eliminar
        /// ********************************************************************************************************************
        /// NOMBRE:      Modificar_Folio_Banco
        /// COMENTARIOS: Esta operación actualiza un registro
        /// PARÁMETROS:  Datos.- Valor de los campos a Modificar
        /// USUARIO CREÓ:Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  31/Enero/2012
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static Boolean Modificar_Folio_Banco(Cls_Ope_Con_Cheques_Bancos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion;//Variable para la conexión para la base de datos   
            SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;

            Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Comando = new SqlCommand();
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Transaction = Transaccion;
            Comando.Connection = Conexion;
            try
            {
                Mi_SQL.Append("UPDATE " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " SET ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Folio_Inicial + "=" + Datos.P_Folio_Inicial + ", ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Folio_Final + "=" + Datos.P_Folio_Final + ", ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Folio_Actual + "=" + Datos.P_Folio_Actual + ", ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Usuario_Modifico + "='" + Datos.P_Usuario + "', ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Fecha_Modifico + "=GETDATE() ");
                Mi_SQL.Append(" WHERE " + Cat_Nom_Bancos.Campo_Banco_ID + "='" + Datos.P_Banco_ID + "'");

                //Ejecutar consulta
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Conexion.Close();
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                if (Transaccion != null)
                {
                    Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }
                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Conexion.Close();
                Comando = null;
                Conexion = null;
                Transaccion = null;
            }
            return Operacion_Completa;
        }

        /// ********************************************************************************************************************
        /// NOMBRE:      Modificar_Comisiones_Bancos
        /// COMENTARIOS: Esta operación actualiza un registro
        /// PARÁMETROS:  Datos.- Valor de los campos a Modificar
        /// USUARIO CREÓ:Sergio Manuel Gallardo Andrade 
        /// FECHA CREÓ:  17/Mayo/2012
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static Boolean Modificar_Comisiones_Bancos(Cls_Ope_Con_Cheques_Bancos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion;//Variable para la conexión para la base de datos   
            SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;

            Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Comando = new SqlCommand();
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Transaction = Transaccion;
            Comando.Connection = Conexion;
            try
            {
                Mi_SQL.Append("UPDATE " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " SET ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Comision_Transferencia + "=" + Datos.P_Comision_Transferencia + ", ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_IVA_Comision + "=" + Datos.P_Iva_Comision + ", ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Usuario_Modifico + "='" + Datos.P_Usuario + "', ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Banco_Clabe + "='" + Datos.P_Banco_Clabe + "', ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Comision_Spay + "=" + Datos.P_Comision_Spay + ", ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_IVA_Spay + "=" + Datos.P_IVA_Spay + ", ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Fecha_Modifico + "=GETDATE() ");
                Mi_SQL.Append(" WHERE " + Cat_Nom_Bancos.Campo_Nombre + "='" + Datos.P_Nombre_Banco + "'");

                //Ejecutar consulta
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Conexion.Close();
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                if (Transaccion != null)
                {
                    Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }
                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Conexion.Close();
                Comando = null;
                Conexion = null;
                Transaccion = null;
            }
            return Operacion_Completa;
        }
        /// ********************************************************************************************************************
        /// NOMBRE:      Modificar_Folio_Actual
        /// COMENTARIOS: Esta operación actualiza un registro
        /// PARÁMETROS:  Datos.- Valor de los campos a Modificar
        /// USUARIO CREÓ:Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  31/Enero/2012
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static Boolean Modificar_Folio_Actual(Cls_Ope_Con_Cheques_Bancos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion;//Variable para la conexión para la base de datos   
            SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;
            DataTable Dt_Folio_Acutal = new DataTable();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            SqlDataAdapter Dt_Oracle_Consulta = new SqlDataAdapter();
            DataSet Ds_Oracle_Consulta = new DataSet();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Int32 Folio = 0;
                Dt_Folio_Acutal = Consultar_Folio_Actual(Datos);

                foreach (DataRow Registro in Dt_Folio_Acutal.Rows)
                {
                    Folio =  Convert.ToInt32(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()) + 1;
                }

                Datos.P_Folio_Actual = "" + Folio;

                Mi_SQL.Append("UPDATE " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " SET ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Folio_Actual + "=" + Datos.P_Folio_Actual + ", ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Usuario_Modifico + "='" + Datos.P_Usuario + "', ");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Fecha_Modifico + "=GETDATE() ");
                Mi_SQL.Append(" WHERE " + Cat_Nom_Bancos.Campo_Banco_ID + "='" + Datos.P_Banco_ID + "'");
                Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                
                //Ejecutar consulta
                //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Operacion_Completa = true;
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Operacion_Completa;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }
                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        #endregion

        #region Consultas
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Bancos_Like
        /// DESCRIPCION : Consulta los bancos que se parescan  
        /// PARAMETROS  :  Datos.- Valor de los campos a 
        /// CREO        : Hugo Enrique Ramirez Aguilera 
        /// FECHA_CREO  : 30/Enero/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Bancos_Like(Cls_Ope_Con_Cheques_Bancos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {

                Mi_SQL.Append("Select * ");
                Mi_SQL.Append("From " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " ");
                Mi_SQL.Append("Where " + Cat_Nom_Bancos.Campo_Nombre);
                Mi_SQL.Append(" like '" + Datos.P_Nombre_Banco + "%'");
                Mi_SQL.Append(" ORDER BY " + Cat_Nom_Bancos.Campo_Nombre + "," + Cat_Nom_Bancos.Campo_No_Cuenta);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Bancos
        /// DESCRIPCION : Consulta los bancos
        /// PARAMETROS  :  Datos.- Valor de los campos a 
        /// CREO        : Hugo Enrique Ramirez Aguilera 
        /// FECHA_CREO  : 30/Enero/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Bancos(Cls_Ope_Con_Cheques_Bancos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {

                Mi_SQL.Append("SELECT * ");
                Mi_SQL.Append("FROM " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " ");
                if (!String.IsNullOrEmpty(Datos.P_No_Cuenta_Banco))
                {
                    Mi_SQL.Append(" WHERE " + Cat_Nom_Bancos.Campo_No_Cuenta + " like '%" + Datos.P_No_Cuenta_Banco + "%'");
                }
                Mi_SQL.Append(" ORDER BY " + Cat_Nom_Bancos.Campo_Nombre + "," + Cat_Nom_Bancos.Campo_No_Cuenta);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Bancos_Existentes
        /// DESCRIPCION : Consulta el nombre de los distintos bancos que existan en la tabla
        /// PARAMETROS  :  Datos.- Valor de los campos a 
        /// CREO        : Hugo Enrique Ramirez Aguilera 
        /// FECHA_CREO  : 10/Marzo/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Bancos_Existentes(Cls_Ope_Con_Cheques_Bancos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {

                Mi_SQL.Append("Select distinct " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Nombre);
                //Se utiliza en el catalogo de comisiones bancarias 
                if (!String.IsNullOrEmpty(Datos.P_Comisiones))
                {
                    Mi_SQL.Append(", " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Comision_Transferencia+", ");
                    Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_IVA_Comision + ",");
                    Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Banco_Clabe + ",");
                    Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_No_Cuenta + ",");
                    Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Comision_Spay + ",");
                    Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_IVA_Spay);
                }
                Mi_SQL.Append(" From " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " ");
                Mi_SQL.Append(" WHERE  " + Cat_Nom_Bancos.Campo_No_Cuenta + " IS NOT NULL  ");
                if(!String.IsNullOrEmpty(Datos.P_Nombre_Banco)){
                    Mi_SQL.Append(" AND  UPPER(" + Cat_Nom_Bancos.Campo_Nombre + ") LIKE UPPER('%" + Datos.P_Nombre_Banco + "%')");
                }
                if (!String.IsNullOrEmpty(Datos.P_Comisiones))
                {
                    Mi_SQL.Append(" GROUP BY " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Nombre + ", ");
                    Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Comision_Transferencia + ", ");
                    Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_IVA_Comision + ",");
                    Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Banco_Clabe + ",");
                    Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_No_Cuenta + ",");
                    Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Comision_Spay + ",");
                    Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_IVA_Spay);
                }
                Mi_SQL.Append(" ORDER BY " + Cat_Nom_Bancos.Campo_Nombre);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Cuenta_Bancos
        /// DESCRIPCION : Consulta el nombre de los distintos bancos que existan en la tabla
        /// PARAMETROS  :  Datos.- Valor de los campos a 
        /// CREO        : Hugo Enrique Ramirez Aguilera 
        /// FECHA_CREO  : 10/Marzo/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Cuenta_Bancos(Cls_Ope_Con_Cheques_Bancos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {

                Mi_SQL.Append("Select * " );
                Mi_SQL.Append(" From " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " ");
                Mi_SQL.Append(" WHERE " + Cat_Nom_Bancos.Campo_Nombre + "='" + Datos.P_Nombre_Banco + "'");
                Mi_SQL.Append(" ORDER BY " + Cat_Nom_Bancos.Campo_No_Cuenta);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Cuenta_Bancos_con_Descripcion
        /// DESCRIPCION : Consulta el nombre de los distintos bancos que existan en la tabla
        /// PARAMETROS  :  Datos.- Valor de los campos a 
        /// CREO        : Hugo Enrique Ramirez Aguilera 
        /// FECHA_CREO  : 10/Marzo/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Cuenta_Bancos_con_Descripcion(Cls_Ope_Con_Cheques_Bancos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {

                Mi_SQL.Append("Select banco.*, (LTRIM(RTRIM(banco." + Cat_Nom_Bancos.Campo_No_Cuenta + ")) +' - '+ cuenta."+ Cat_Con_Cuentas_Contables.Campo_Descripcion+") as Cuenta");
                Mi_SQL.Append(" From " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " banco, "+Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables+" cuenta ");
                Mi_SQL.Append(" WHERE banco." + Cat_Nom_Bancos.Campo_Cuenta_Contable_ID  + "= cuenta."+ Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append(" AND banco." + Cat_Nom_Bancos.Campo_Nombre + "='" + Datos.P_Nombre_Banco + "'");
                //Mi_SQL.Append(" AND banco." + Cat_Nom_Bancos.Campo_Tipo  + "='INGRESOS'");
                //Mi_SQL.Append(" ORDER BY " + Cat_Nom_Bancos.Campo_No_Cuenta + " DESC ");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Folio_Actual
        /// DESCRIPCION : Consulta el folio actual del banco seleccionado
        /// PARAMETROS  :  Datos.- Valor de los campos a 
        /// CREO        : Hugo Enrique Ramirez Aguilera 
        /// FECHA_CREO  : 31/Enero/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Folio_Actual(Cls_Ope_Con_Cheques_Bancos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            DataTable Dt_Psp = new DataTable();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Sql = new SqlDataAdapter();
            DataSet Ds_Sql = new DataSet();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {

                Mi_SQL.Append("Select * ");
                Mi_SQL.Append("From " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " ");
                Mi_SQL.Append("Where " + Cat_Nom_Bancos.Campo_Banco_ID + "='" + Datos.P_Banco_ID +"'");
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Sql.SelectCommand = Cmmd;
                Dt_Sql.Fill(Ds_Sql);
                Dt_Psp = Ds_Sql.Tables[0];
                //return OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Dt_Psp;
                //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: [" + Ex.Message + "]");
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        #endregion
    }
}