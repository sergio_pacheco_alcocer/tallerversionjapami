﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Ope_Con_Autorizar_Cheques.Negocio;
using JAPAMI.Sessiones;


/// <summary>
/// Summary description for Cls_Ope_Con_Estatus_Cheques_Datos
/// </summary>
namespace JAPAMI.Ope_Con_Autorizar_Cheques.Datos
{
    public class Cls_Ope_Con_Estatus_Cheques_Datos
    {
        public Cls_Ope_Con_Estatus_Cheques_Datos()
        {
        }
        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Tipos_Beneficiarios
        ///DESCRIPCIÓN: Consulta los tipos de beneficiarios que pueden existir para un cheque
        ///PARAMETROS:  1.- Cls_Cat_Com_Monto_Proceso_Compra_Negocios
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 13/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Tipos_Beneficiarios()
        {
            String Mi_SQL = string.Empty;
            try
            {
                Mi_SQL = "SELECT " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ";
                Mi_SQL += "UPPER(" + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + ") AS DESCRIPCION ";
                Mi_SQL += " FROM " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago;
                Mi_SQL += " ORDER BY " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }
        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Beneficiarios
        ///DESCRIPCIÓN: Consulta a los posibles empleados beneficiarios
        ///PARAMETROS:  1.- Objeto de la clase de negocios para obtener datos
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 14/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Beneficiarios(Cls_Ope_Con_Estatus_Cheques_Negocio Clase_Seguimiento_Negocio)
        {
            String Mi_SQL = string.Empty;

            try
            {
                Mi_SQL = "SELECT DISTINCT " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Beneficiario_Pago;
                Mi_SQL += " FROM " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " JOIN " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos;
                Mi_SQL += " ON " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Solicitud_Pago + " = " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago;
                Mi_SQL += " WHERE " + Ope_Con_Pagos.Campo_Beneficiario_Pago + " LIKE UPPER('%" + Clase_Seguimiento_Negocio.P_Beneficiario + "%') ";
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Tipo_Beneficiario))
                {
                    Mi_SQL += " AND " + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Clase_Seguimiento_Negocio.P_Tipo_Beneficiario + "'";
                }
                Mi_SQL += " ORDER BY " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Beneficiario_Pago;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }
        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Cheques_Listado
        ///DESCRIPCIÓN: Consulta los cheques registrados
        ///PARAMETROS:  1.- Cls_Cat_Com_Monto_Proceso_Compra_Negocios
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 15/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Cheques_Listado(Cls_Ope_Con_Estatus_Cheques_Negocio Clase_Seguimiento_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                Mi_SQL = "SELECT " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_No_Cheque;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Beneficiario;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Concepto;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Monto + " AS  Importe ";
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Estatus + " AS ESTATUS ";
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Folio;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Fecha + " AS FECHA_EMISION";
                Mi_SQL += " FROM " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + " JOIN " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos;
                Mi_SQL += " ON " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_No_Pago + " = " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Pago;
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Folio_Cheque))
                {
                    Mi_SQL += " WHERE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Folio + " = " + Clase_Seguimiento_Negocio.P_Folio_Cheque;
                }
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Estatus_Entrada))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Estatus + " = '" + Clase_Seguimiento_Negocio.P_Estatus_Entrada+ "'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Estatus + " = '" + Clase_Seguimiento_Negocio.P_Estatus_Entrada + "'";
                    }
                }
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Tipo_Beneficiario))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Clase_Seguimiento_Negocio.P_Tipo_Beneficiario + "'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Clase_Seguimiento_Negocio.P_Tipo_Beneficiario + "'";
                    }
                }
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Beneficiario))
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Beneficiario + " = '" + Clase_Seguimiento_Negocio.P_Beneficiario + "'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Beneficiario + " = '" + Clase_Seguimiento_Negocio.P_Beneficiario + "'";
                    }
                }
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Fecha_Emision_Inicio) && !String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Fecha_Emision_Fin))
                {
                    Clase_Seguimiento_Negocio.P_Fecha_Emision_Inicio = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Clase_Seguimiento_Negocio.P_Fecha_Emision_Inicio));
                    Clase_Seguimiento_Negocio.P_Fecha_Emision_Fin= string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Clase_Seguimiento_Negocio.P_Fecha_Emision_Fin));
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND ((" + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Fecha + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Inicio + " 00:00:00' AND ";
                        Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Fin + " 23:59:00') ";
                        Mi_SQL += " OR (" + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Fecha_Creo + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Inicio + " 00:00:00' AND ";
                        Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Fin+ " 23:59:00'))";
                    }
                    else
                    {
                        Mi_SQL += " WHERE ((" + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Fecha + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Inicio + " 00:00:00' AND ";
                        Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Fin + " 23:59:00') ";
                        Mi_SQL += " OR (" + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Fecha_Creo + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Inicio + " 00:00:00' AND ";
                        Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Emision_Fin + " 23:59:00'))";
                    }
                }

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Autorizar_Cheque
        ///DESCRIPCIÓN          : Autoriza elcheque para que pase al siguiente filtro
        ///PARAMETROS           :  
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 22/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Autorizar_Cheque(Cls_Ope_Con_Estatus_Cheques_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Consultamos los datos del cheque
                String Mi_SQL = "SELECT * FROM " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Con_Cheques.Campo_No_Cheque + " = " + Clase_Negocio.P_No_Cheque_ID;
                DataTable Dt_Datos_Cheque = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                
                //Insertar un registro en el seguimiento del cheque
                Mi_SQL = "INSERT INTO " + Ope_Con_Seguimiento_Cheques.Tabla_Ope_Con_Seguimiento_Cheques +
                    " (" + Ope_Con_Seguimiento_Cheques.Campo_No_Cheque +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Empleado_Recibio_ID +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Estatus_Entrada +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Fecha_Entrada +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Estatus_Salida +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Fecha_Salida +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Usuario_Creo +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Fecha_Creo +
                    ") VALUES (" + Clase_Negocio.P_No_Cheque_ID + ",'" +
                     Cls_Sessiones.Empleado_ID + "','" +
                     Dt_Datos_Cheque.Rows[0][Ope_Con_Cheques.Campo_Estatus].ToString() + "', " +
                     "( SELECT " + Ope_Con_Cheques.Campo_Fecha + " FROM " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques +
                        " WHERE " + Ope_Con_Cheques.Campo_No_Cheque + " = " + Clase_Negocio.P_No_Cheque_ID + "), " +
                     " '" + Clase_Negocio.P_Estatus_Salida + "', " +
                     " GETDATE(),'" +
                     Cls_Sessiones.Empleado_ID + "', " +
                     " GETDATE() )";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Actualizar estatus en la clase de cheques 
                Mi_SQL = "UPDATE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques;
                Mi_SQL = Mi_SQL + " SET " + Ope_Con_Cheques.Campo_Estatus + " = '" +  Clase_Negocio.P_Estatus_Salida + "'";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Con_Cheques.Campo_No_Cheque + " = " + Clase_Negocio.P_No_Cheque_ID;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
                Mensaje_Error = Clase_Negocio.P_No_Cheque_ID;
                    
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo autorizar el cheque";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }//fin de Autorizar Cheque
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Rechazar_Cheque
        ///DESCRIPCIÓN          : Rechaza el cheque
        ///PARAMETROS           :  
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 26/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Rechazar_Cheque(Cls_Ope_Con_Estatus_Cheques_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Consultamos los datos del cheque
                String Mi_SQL = "SELECT * FROM " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Con_Cheques.Campo_No_Cheque + " = " + Clase_Negocio.P_No_Cheque_ID;
                DataTable Dt_Datos_Cheque = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Insertar un registro en el seguimiento del cheque
                Mi_SQL = "INSERT INTO " + Ope_Con_Seguimiento_Cheques.Tabla_Ope_Con_Seguimiento_Cheques +
                    " (" + Ope_Con_Seguimiento_Cheques.Campo_No_Cheque +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Empleado_Recibio_ID +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Estatus_Entrada +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Fecha_Entrada +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Estatus_Salida +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Fecha_Salida +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Usuario_Creo +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Fecha_Creo +
                    ", " + Ope_Con_Seguimiento_Cheques.Campo_Comentarios +
                    ") VALUES (" + Clase_Negocio.P_No_Cheque_ID + ",'" +
                     Cls_Sessiones.Empleado_ID + "','" +
                     Dt_Datos_Cheque.Rows[0][Ope_Con_Cheques.Campo_Estatus].ToString() + "', " +
                     "( SELECT " + Ope_Con_Cheques.Campo_Fecha + " FROM " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques +
                        " WHERE " + Ope_Con_Cheques.Campo_No_Cheque + " = " + Clase_Negocio.P_No_Cheque_ID + "), " +
                     " '" + Clase_Negocio.P_Estatus_Salida + "', " +
                     " GETDATE(),'" +
                     Cls_Sessiones.Empleado_ID + "', " +
                     " GETDATE()" + 
                     ", '" + Clase_Negocio.P_Comentarios + "')";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Actualizar estatus en la clase de cheques 
                Mi_SQL = "UPDATE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques;
                Mi_SQL = Mi_SQL + " SET " + Ope_Con_Cheques.Campo_Estatus + " = '" + Clase_Negocio.P_Estatus_Salida + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Con_Cheques.Campo_Usuario_Modifico + "= '" + Cls_Sessiones.Nombre_Empleado + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Con_Cheques.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Con_Cheques.Campo_No_Cheque + " = " + Clase_Negocio.P_No_Cheque_ID;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Actualizar el estatus en la tabla de ope_con_pagos
                Mi_SQL = "UPDATE " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos;
                Mi_SQL = Mi_SQL + " SET " + Ope_Con_Pagos.Campo_Estatus + " = '" + Clase_Negocio.P_Estatus_Salida + "'";
                Mi_SQL = Mi_SQL+ ", " + Ope_Con_Pagos.Campo_Motivo_Cancelacion + " = '" + Clase_Negocio.P_Comentarios + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Con_Pagos.Campo_Usuario_Modifico + "= '" + Cls_Sessiones.Nombre_Empleado + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Con_Pagos.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL = Mi_SQL + " WHERE  " + Ope_Con_Pagos.Campo_No_Pago + " = '" + Dt_Datos_Cheque.Rows[0][Ope_Con_Cheques.Campo_No_Pago].ToString() + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
                Mensaje_Error = "Rechazado";
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo rechazar el cheque";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }//fin de Rechazar_Cheque
    }
}