﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Generar_Reservas.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;
using JAPAMI.Administrar_Requisiciones.Negocios;
using System.Data.SqlClient;
using System.Text;

namespace JAPAMI.Generar_Reservas.Datos
{
    public class Cls_Ope_Con_Reservas_Datos
    {
        public Cls_Ope_Con_Reservas_Datos()
        {
        }
        #region CONSULTAS / FTE FINANCIAMIENTO, PROYECTOS, PARTIDAS, PRESUPUESTOS
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Fuentes_Financiamiento
        ///DESCRIPCIÓN: crea una sentencia sql para Consultar_Fuentes_Financiamiento
        ///PARAMETROS: 1.-Clase de Negocio
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 25/Ene/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        public static DataTable Consultar_Fuentes_Financiamiento(Cls_Ope_Con_Reservas_Negocio Reserva_Negocio)
        {
            try
            {
                String Mi_SQL = "";
                Mi_SQL =
                "SELECT FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + "," +
                " FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+" +
                " FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion +
                " FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FUENTE" +
                " JOIN " + Cat_SAP_Det_Fte_Dependencia.Tabla_Cat_SAP_Det_Fte_Financiamiento_Dependencia + " DETALLE" +
                " ON FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = " +
                " DETALLE." + Cat_SAP_Det_Fte_Dependencia.Campo_Fuente_Financiamiento_ID +
                " WHERE DETALLE." + Cat_SAP_Det_Fte_Dependencia.Campo_Dependencia_ID + " = " +
                "'" + Reserva_Negocio.P_Dependencia_ID + "'" +
                " ORDER BY " + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " ASC";
                //" AND FUENTE." + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID + " IN " +
                //"(SELECT " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID + 
                //" FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                //" WHERE " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + " = " +
                //"'" + Reserva_Negocio.P_Dependencia_ID + "'" + ")" +
                
                DataTable Data_Table =
                    SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Data_Table;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Proyectos_Programas
        ///DESCRIPCIÓN: crea una sentencia sql para Consultar_Proyectos_Programas
        ///PARAMETROS: 1.-Clase de Negocio
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 25/Ene/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************  
        public static DataTable Consultar_Proyectos_Programas(Cls_Ope_Con_Reservas_Negocio Reserva_Negocio)
        {
            String Mi_SQL = "";
            Mi_SQL =
            "SELECT PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + "," +
            " PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Clave + " +' '+" +
            " PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Nombre + "," +
            " PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Elemento_PEP +
            " FROM " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + " PROGRAMA" +            
            " JOIN " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia + " DETALLE" +
            " ON PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + " = " +
            " DETALLE." + Cat_SAP_Det_Prog_Dependencia.Campo_Proyecto_Programa_ID +
            " WHERE DETALLE." + Cat_SAP_Det_Prog_Dependencia.Campo_Dependencia_ID + " = " +
            "'" + Reserva_Negocio.P_Dependencia_ID + "' ORDER BY " + 
            Cat_Com_Proyectos_Programas.Campo_Descripcion + " ASC";
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Programas_Dependencia
        ///DESCRIPCIÓN: crea una sentencia sql para Consultar_Proyectos_Programas
        ///PARAMETROS: 1.-Clase de Negocio
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 19-julio-2012 
        ///MODIFICO:
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************  
        public static DataTable Consultar_Programas_Dependencia(Cls_Ope_Con_Reservas_Negocio Reserva_Negocio)
        {
            String Mi_SQL = "";
            String anio = String.Format("{0:yyyy}", DateTime.Now);
            Mi_SQL = "SELECT PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + ", PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Clave + " +' '+";
            Mi_SQL += " PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Nombre + " AS DESCRIPCION , PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Elemento_PEP;
            Mi_SQL += " FROM " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + " PROGRAMA";
            Mi_SQL += " INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTO  ON PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + "=PROGRAMA.";
            Mi_SQL += Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID;
            Mi_SQL += " INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDA  ON PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "=PRESUPUESTO.";
            Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;
            Mi_SQL += " WHERE PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + "='" + Reserva_Negocio.P_Dependencia_ID + "'";
            Mi_SQL += " AND PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Cuenta_Contable_ID + "='" + Reserva_Negocio.P_Partida_ID+ "'";
            Mi_SQL += " AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + "='" + anio + "'";
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Dependencia_Partida
        ///DESCRIPCIÓN: crea una sentencia sql para Consultar_Proyectos_Programas
        ///PARAMETROS: 1.-Clase de Negocio
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 19-julio-2012 
        ///MODIFICO:
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************  
        public static DataTable Consultar_Dependencia_Partida(Cls_Ope_Con_Reservas_Negocio Reserva_Negocio)
        {
            String Mi_SQL = "";
            String anio = String.Format("{0:yyyy}", DateTime.Now);
            Mi_SQL = "SELECT DISTINCT DEPENDENCIA." + Cat_Dependencias.Campo_Dependencia_ID + ", DEPENDENCIA." + Cat_Dependencias.Campo_Nombre+", PARTIDA."+Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
            Mi_SQL += " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIA";
            Mi_SQL += " INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTO  ON PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + "=DEPENDENCIA.";
            Mi_SQL += Cat_Dependencias.Campo_Dependencia_ID;
            Mi_SQL += " INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDA  ON PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "=PRESUPUESTO.";
            Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;
            Mi_SQL += " WHERE DEPENDENCIA." + Cat_Dependencias.Campo_Estatus + "='ACTIVO'";
            Mi_SQL += " AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + "='" + anio + "'";
            Mi_SQL += " AND PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Cuenta_Contable_ID + "='" + Reserva_Negocio.P_Partida_ID + "' ORDER BY DEPENDENCIA." + Cat_Dependencias.Campo_Nombre;
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Disponible_Partida
        ///DESCRIPCIÓN: crea una sentencia sql para Consultar_Proyectos_Programas
        ///PARAMETROS: 1.-Clase de Negocio
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 19-julio-2012 
        ///MODIFICO:
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************  
        public static DataTable Consultar_Disponible_Partida(Cls_Ope_Con_Reservas_Negocio Reserva_Negocio)
        {
            String anio=String.Format("{0:yyyy}",DateTime.Now);
            String Mi_SQL = "";
            Mi_SQL = "SELECT  PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID +" AS FUENTE";
            Mi_SQL += " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTO";
            Mi_SQL += " INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDA  ON PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + "=PARTIDA.";
            Mi_SQL += Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
            Mi_SQL += " WHERE PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + "='" + Reserva_Negocio.P_Proyecto_Programa_ID+ "'";
            Mi_SQL += " AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID  + "='" + Reserva_Negocio.P_Dependencia_ID + "'";
            Mi_SQL += " AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio   + "='"+anio+"'";
            Mi_SQL += " AND PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Cuenta_Contable_ID  + "='" + Reserva_Negocio.P_Partida_ID + "'";
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas_De_Un_Programa
        ///DESCRIPCIÓN          : crea una sentencia sql para Consultar_Partidas_De_Un_Programa
        ///PARAMETROS           : 1.-Clase de Negocio
        ///CREO                 : Gustavo Angeles Cruz
        ///FECHA_CREO           : 25/Ene/2011 
        ///MODIFICO             : Leslie Gonzalez Vazquez
        ///FECHA_MODIFICO       : 20/Enero/2012
        ///CAUSA_MODIFICACIÓN   : Modifique la cosulta para solo obtener las partidas con presupuesto aprobado
        ///*******************************************************************************  
        //CONSULTAR PARTIDAS DE UN PROGRAMA
        public static DataTable Consultar_Partidas_De_Un_Programa(Cls_Ope_Con_Reservas_Negocio Reservas_Negocio) 
        {
            StringBuilder Mi_Sql = new StringBuilder();
            Mi_Sql.Append(" SELECT DISTINCT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", (");
            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + "+ ' - ' + ");
            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ") AS NOMBRE, ");
            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible);
            Mi_Sql.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
            Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
            Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
            Mi_Sql.Append(" = '" + Reservas_Negocio.P_Anio_Presupuesto + "'");
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
            Mi_Sql.Append(" = '" + Reservas_Negocio.P_Dependencia_ID + "'");
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
            Mi_Sql.Append(" = '" + Reservas_Negocio.P_Fuente_Financiamiento + "'");
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
            Mi_Sql.Append(" = '" + Reservas_Negocio.P_Proyecto_Programa_ID + "'");
            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + " > 0");
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " > 0");

            if(!String.IsNullOrEmpty(Reservas_Negocio.P_Partida_ID))
            {
                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                Mi_Sql.Append(" = '" + Reservas_Negocio.P_Partida_ID + "'");
            }

            Mi_Sql.Append(" ORDER BY (" +Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + "+ ' - ' + ");
            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre+")");
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            return Data_Table;
        }

        //OBTINE PARTIDAS ESPECIFICAS CON PRESPUESTOS A PARTIR DE LA DEPENDENCIA Y EL PROYECTO
        public static DataTable Consultar_Presupuesto_Partidas(Cls_Ope_Con_Reservas_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "";

            Mi_SQL = 
            "SELECT " +
            Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ", " +
                "(SELECT " + Cat_Com_Partidas.Campo_Nombre + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " + 
                Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + 
                Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") NOMBRE, " +

                "(SELECT " + Cat_Com_Partidas.Campo_Clave + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " +
                Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") CLAVE, " +

                "(SELECT " + Cat_Com_Partidas.Campo_Clave + " +' '+" + 
                Cat_Com_Partidas.Campo_Nombre + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " +
                Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") CLAVE_NOMBRE, " +
            Cat_Com_Dep_Presupuesto.Campo_Monto_Presupuestal + ", " +
            Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " MONTO_DISPONIBLE, " +
            Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + ", " +
            Cat_Com_Dep_Presupuesto.Campo_Monto_Ejercido + ", " +
            Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto + ", " +
            Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ", " +
            Cat_Com_Dep_Presupuesto.Campo_Fecha_Creo + 
            //" TO_CHAR(FECHA_CREO ,'DD/MM/YY') FECHA_CREO" + 
            " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
            " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID + 
            " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + 
            " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
            " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
            " IN (" + Requisicion_Negocio.P_Partida_ID + ")" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
            " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + " = " +
                "(SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
                " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + 
                " WHERE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                            Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                            " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                            Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
                            " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID + 
                            " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +

                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                            " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +

                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                            " IN (" + Requisicion_Negocio.P_Partida_ID + "))";

            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }
        #endregion                             

        #region CONSULTA RESERVAS
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Reservas
        ///DESCRIPCIÓN: crea una sentencia sql para conultar una Requisa en la base de datos
        ///PARAMETROS: 1.-Clase de Negocio
        ///            2.-Usuario que crea la requisa
        ///CREO: Silvia Morales Portuhondo
        ///FECHA_CREO: Noviembre/2010 
        ///MODIFICO:Gustavo Angeles Cruz
        ///FECHA_MODIFICO: 25/Ene/2011
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        public static DataTable Consultar_Reservas(Cls_Ope_Con_Reservas_Negocio Reserva_Negocio)
        {

            String Mi_Sql = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            DataTable Dt_Detalle_Solicitud = new DataTable();
            if (Reserva_Negocio.P_Cmmd != null)
            {
                Cmmd = Reserva_Negocio.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_Sql = "SELECT " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ".*, " +
                "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " +
                Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID +
                ") NOMBRE_DEPENDENCIA " +
            " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                if (!string.IsNullOrEmpty(Reserva_Negocio.P_Dependencia_ID) && Reserva_Negocio.P_Dependencia_ID != "0")
                {
                    Mi_Sql += " WHERE " + Ope_Psp_Reservas.Campo_Dependencia_ID +
                    " = '" + Reserva_Negocio.P_Dependencia_ID + "'";
                    Mi_Sql += " AND " + Ope_Psp_Reservas.Campo_Fecha + "" +
                            " >= '" + Reserva_Negocio.P_Fecha_Inicial + "' AND " +
                        Ope_Psp_Reservas.Campo_Fecha + 
                            " <= '" + Reserva_Negocio.P_Fecha_Final + "'";
                }
                if (!string.IsNullOrEmpty(Reserva_Negocio.P_No_Reserva))
                {
                    if (string.IsNullOrEmpty(Reserva_Negocio.P_Dependencia_ID))
                    {
                        Mi_Sql +=
                    " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva +
                    " = " + Reserva_Negocio.P_No_Reserva;
                    }
                    else
                    {
                        Mi_Sql +=
                    " AND " + Ope_Psp_Reservas.Campo_No_Reserva +
                    " = " + Reserva_Negocio.P_No_Reserva;
                    }
                }
                Mi_Sql = Mi_Sql + " ORDER BY " + Ope_Psp_Reservas.Campo_No_Reserva + " DESC";
                Cmmd.CommandText = Mi_Sql.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                DataSet Data_Set = Ds_Oracle;
                if (Reserva_Negocio.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                //DataSet Data_Set = OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0)
                {
                    return (Data_Set.Tables[0]);
                }
                else
                {
                    return null;
                }
            }
            catch (SqlException Ex)
            {
                if (Reserva_Negocio.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Reserva_Negocio.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Reserva
        /// DESCRIPCION : Modifica la reserva seleccionado
        /// PARAMETROS  : Datos: Contiene los datos proporcionados
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 22/Noviembre/2011
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static void Modificar_Reserva(Cls_Ope_Con_Reservas_Negocio  Datos)
        {

            String Mi_SQL;  //Almacena la sentencia de modificacion.
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ";
                Mi_SQL += Ope_Psp_Reservas.Campo_Estatus + " = '" + Datos.P_Estatus + "', ";
                Mi_SQL += Ope_Psp_Reservas.Campo_Saldo + " = '0', ";
                Mi_SQL += Ope_Psp_Reservas.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Modifico + "',";
                Mi_SQL += Ope_Psp_Reservas.Campo_Fecha_Modifico + " =GETDATE()";
                Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + Datos.P_No_Reserva + "'";
                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Reservas_Detalladas
        ///DESCRIPCIÓN          : Consulta para obtenes los datos de las reservas y sus detalles
        ///PARAMETROS           : 1.-Clase de Negocio
        ///CREO                 : Leslie Gonzalez Vazquez
        ///FECHA_CREO           : 23/Enero/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************  
        public static DataTable Consultar_Reservas_Detalladas(Cls_Ope_Con_Reservas_Negocio Reservas_Negocio)
        {
             StringBuilder Mi_Sql = new StringBuilder();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            DataTable Dt_Detalle_Solicitud = new DataTable();
            if (Reservas_Negocio.P_Cmmd != null)
            {
                Cmmd = Reservas_Negocio.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_Sql.Append(" SELECT " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " AS FUENTE_FINANCIAMIENTO_ID, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " AS PROGRAMA_ID, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Capitulo_ID + " AS CAPITULO_ID, ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA, ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FUENTE_FINANCIAMIENTO, ");
                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS PROGRAMA, ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Empleado_ID + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Proveedor_ID + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Importe_Inicial + " AS TOTAL, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ", ");
                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " AS IMPORTE, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Saldo + " AS SALDO, ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " AS ANIO, ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Estatus + " AS ESTATUS ");
                Mi_Sql.Append(" FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID);
                Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                Mi_Sql.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID);
                Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID);
                Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_No_Reserva))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                        Mi_Sql.Append(" = " + Reservas_Negocio.P_No_Reserva + "");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                        Mi_Sql.Append(" = " + Reservas_Negocio.P_No_Reserva + "");
                    }
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Fecha_Inicial) && !String.IsNullOrEmpty(Reservas_Negocio.P_Fecha_Final))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Fecha);
                        Mi_Sql.Append(" BETWEEN '" + Reservas_Negocio.P_Fecha_Inicial + " 00:00:00'");
                        Mi_Sql.Append(" AND '" + Reservas_Negocio.P_Fecha_Final + " 23:59:00'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Fecha);
                        Mi_Sql.Append(" BETWEEN '" + Reservas_Negocio.P_Fecha_Inicial + " 00:00:00'");
                        Mi_Sql.Append(" AND '" + Reservas_Negocio.P_Fecha_Final + " 23:59:00'");
                    }
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Dependencia_ID))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Dependencia_ID + ")");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Dependencia_ID + ")");
                    }
                }
                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Grupo_Dependencia))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Grupo_Dependencia + ")");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Grupo_Dependencia + ")");
                    }
                }

                Mi_Sql.Append(" ORDER BY " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + " ASC");
                Cmmd.CommandText = Mi_Sql.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                DataSet Data_Set = Ds_Oracle;
                if (Reservas_Negocio.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                //DataSet Data_Set = OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0)
                {
                    return (Data_Set.Tables[0]);
                }
                else
                {
                    return null;
                }
                //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                if (Reservas_Negocio.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Reservas_Negocio.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Reservas_Nomina_Detalladas
        ///DESCRIPCIÓN          : Consulta para obtenes los datos de las reservas y sus detalles
        ///PARAMETROS           : 1.-Clase de Negocio
        ///CREO                 : Sergio Manuel Gallardo
        ///FECHA_CREO           : 30/Abril/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************  
        public static DataTable Consultar_Reservas_Nomina_Detalladas(Cls_Ope_Con_Reservas_Negocio Reservas_Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {

                Mi_Sql.Append(" SELECT " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " AS FUENTE_FINANCIAMIENTO_ID, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " AS PROGRAMA_ID, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Capitulo_ID + " AS CAPITULO_ID, ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA, ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FUENTE_FINANCIAMIENTO, ");
                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS PROGRAMA, ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Empleado_ID + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Proveedor_ID + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Importe_Inicial + " AS TOTAL, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ", ");
                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " AS IMPORTE, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Saldo + " AS SALDO, ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " AS ANIO, ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Estatus + " AS ESTATUS ");
                Mi_Sql.Append(" FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                Mi_Sql.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID);
                Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID);
                Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID);
                Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_No_Reserva))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                        Mi_Sql.Append(" = " + Reservas_Negocio.P_No_Reserva + "");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                        Mi_Sql.Append(" = " + Reservas_Negocio.P_No_Reserva + "");
                    }
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Fecha_Inicial) && !String.IsNullOrEmpty(Reservas_Negocio.P_Fecha_Final))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Fecha);
                        Mi_Sql.Append(" BETWEEN '" + Reservas_Negocio.P_Fecha_Inicial + " 00:00:00'");
                        Mi_Sql.Append(" AND '" + Reservas_Negocio.P_Fecha_Final + " 23:59:00'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Fecha);
                        Mi_Sql.Append(" BETWEEN '" + Reservas_Negocio.P_Fecha_Inicial + " 00:00:00'");
                        Mi_Sql.Append(" AND '" + Reservas_Negocio.P_Fecha_Final + " 23:59:00'");
                    }
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Dependencia_ID))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Dependencia_ID + ")");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Dependencia_ID + ")");
                    }
                }
                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Grupo_Dependencia))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Grupo_Dependencia + ")");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Grupo_Dependencia + ")");
                    }
                }

                Mi_Sql.Append(" ORDER BY " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + " ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los datos de las reservas. Error[" + Ex.Message + "]");
            }
        }
        #endregion

        #region Consulta de partidas con saldo disponible
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Reservas_Unidad_Responsable
        /// DESCRIPCION: Consulta las partidas que pertenescan a la unidad responsable buscada
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO : 28-Febrero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Reservas_Unidad_Responsable(Cls_Ope_Con_Reservas_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT  PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                Mi_SQL.Append(", PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);

                Mi_SQL.Append(",(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " +
                        Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ")+ '-' +(SELECT "
                        + Cat_Sap_Partidas_Especificas.Campo_Nombre + " FROM " +
                        Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ") Clave_Partida");

                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID);
                Mi_SQL.Append(",(SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " +
                        Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ")+ '-' +(SELECT "
                        + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " FROM " +
                        Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ") Clave_Programa");
                Mi_SQL.Append(",(SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " +
                       Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                       "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ")AS Programa");
                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                Mi_SQL.Append(",(SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " FROM " +
                       Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID +
                       "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ") Clave_Financiamiento");

                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + Datos.P_Filtro_Campo_Mes + " AS DISPONIBLE, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + Datos.P_Filtro_Campo_Mes +
                        " AS DEVENGADO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + Datos.P_Filtro_Campo_Mes + " AS EJERCIDO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + Datos.P_Filtro_Campo_Mes +
                        " AS PAGADO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe + Datos.P_Filtro_Campo_Mes +" AS APROBADO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + Datos.P_Filtro_Campo_Mes + " AS COMPROMETIDO");
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTO, ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDA, ");
                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_SQL.Append(" WHERE PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "=PRESUPUESTO." +
                        Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = " +
                        "PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + //" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 +
                        //"='" + Datos.P_Ramo_33 + "'" 
                        " AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + Datos.P_Filtro_Campo_Mes +
                        "> 0 AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID +
                        "='" + Datos.P_Dependencia_ID + "'" + " AND (PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Clave + ">='1000' AND PARTIDA.");
                //if (Datos.P_Ramo_33.ToString() != "SI")
                //{
                //    Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Campo_Clave + "< '6000' ");
                //}
                //else
                //{
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Campo_Clave + "< '7000' )");// OR PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Clave+"='1241')");
                //}
                Mi_SQL.Append(" AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + "= '" + DateTime.Now.Year + "' ");
                Mi_SQL.Append(" ORDER BY Clave_Partida");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Reservas_Unidad_Responsable_y_Partida
        /// DESCRIPCION: Consulta las partidas que de la reserva paraobtener su disponible y comprometido
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO : 27/Abril/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Reservas_Unidad_Responsable_y_Partida(Cls_Ope_Con_Reservas_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT  PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                Mi_SQL.Append(", PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);

                Mi_SQL.Append(",(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " +
                        Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ")+ '-' +(SELECT "
                        + Cat_Sap_Partidas_Especificas.Campo_Nombre + " FROM " +
                        Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ") Clave_Partida");

                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID);
                Mi_SQL.Append(",(SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " +
                        Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ")+ '-' +(SELECT "
                        + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " FROM " +
                        Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ") Clave_Programa");
                Mi_SQL.Append(",(SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " +
                       Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                       "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ")AS Programa");
                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                Mi_SQL.Append(",(SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " FROM " +
                       Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID +
                       "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ") Clave_Financiamiento");

                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + Datos.P_Filtro_Campo_Mes + " AS DISPONIBLE, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + Datos.P_Filtro_Campo_Mes +
                        " AS DEVENGADO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + Datos.P_Filtro_Campo_Mes + " AS EJERCIDO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + Datos.P_Filtro_Campo_Mes +
                        " AS PAGADO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe + Datos.P_Filtro_Campo_Mes + " AS APROBADO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + Datos.P_Filtro_Campo_Mes + " AS COMPROMETIDO");
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTO, ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDA, ");
                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_SQL.Append(" WHERE PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "=PRESUPUESTO." +
                        Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = " +
                        "PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID +
                        " AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + Datos.P_Filtro_Campo_Mes +
                        "> 0 AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID +
                        "='" + Datos.P_Dependencia_ID + "'" + " AND PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Clave + ">='2000' AND PARTIDA.");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Campo_Clave + "< '7000' ");

                Mi_SQL.Append(" AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + "= '" + DateTime.Now.Year + "' ");
                Mi_SQL.Append(" AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + "= '" + Datos.P_Proyecto_Programa_ID + "' ");
                Mi_SQL.Append(" AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + "= '" + Datos.P_Fuente_Financiamiento + "' ");
                Mi_SQL.Append(" AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + "= '" + Datos.P_Partida_ID + "' ");
                Mi_SQL.Append(" AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + "= '" + Datos.P_Capitulo_ID + "' ");
                Mi_SQL.Append(" ORDER BY Clave_Partida");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Reservas_Unidad_Responsable_y_Partida_de_Nomina la Mil
        /// DESCRIPCION: Consulta las partidas que de la reserva paraobtener su disponible y comprometido
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO : 27/Abril/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Reservas_Unidad_Responsable_y_Partida_de_Nomina(Cls_Ope_Con_Reservas_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT  PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);

                Mi_SQL.Append(",(SELECT " + Cat_Dependencias.Campo_Clave + " FROM " +
                        Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ")+ '-' +(SELECT "
                        + Cat_Dependencias.Campo_Nombre + " FROM " +
                        Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ") Clave_Dependencia");

                Mi_SQL.Append(", PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);

                Mi_SQL.Append(",(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " +
                        Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ")+ '-' +(SELECT "
                        + Cat_Sap_Partidas_Especificas.Campo_Nombre + " FROM " +
                        Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ") Clave_Partida");

                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID);
                Mi_SQL.Append(",(SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " +
                        Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ")+ '-' +(SELECT "
                        + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " FROM " +
                        Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ") Clave_Programa");
                Mi_SQL.Append(",(SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " +
                       Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                       "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ")AS Programa");
                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                Mi_SQL.Append(",(SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " FROM " +
                       Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID +
                       "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ") Clave_Financiamiento");

                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + Datos.P_Filtro_Campo_Mes + " AS DISPONIBLE, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + Datos.P_Filtro_Campo_Mes +
                        " AS DEVENGADO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + Datos.P_Filtro_Campo_Mes + " AS EJERCIDO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + Datos.P_Filtro_Campo_Mes +
                        " AS PAGADO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe + Datos.P_Filtro_Campo_Mes + " AS APROBADO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + Datos.P_Filtro_Campo_Mes + " AS COMPROMETIDO");
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTO, ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDA, ");
                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_SQL.Append(" WHERE PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "=PRESUPUESTO." +
                        Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = " +
                        "PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID +
                        " AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID +
                        "='" + Datos.P_Dependencia_ID + "'" + " AND PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Clave + ">='1000' AND PARTIDA.");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Campo_Clave + "< '2000' ");

                Mi_SQL.Append(" AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + "= '" + DateTime.Now.Year + "' ");
                Mi_SQL.Append(" AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + "= '" + Datos.P_Proyecto_Programa_ID + "' ");
                Mi_SQL.Append(" AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + "= '" + Datos.P_Fuente_Financiamiento + "' ");
                Mi_SQL.Append(" AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + "= '" + Datos.P_Partida_ID + "' ");
                Mi_SQL.Append(" AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + "= '" + Datos.P_Capitulo_ID + "' ");
                Mi_SQL.Append(" ORDER BY Clave_Partida");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        
        #endregion

        #region Actualizar Tipo Reserva y Saldos
        /// ********************************************************************************************************************
        /// NOMBRE: Modificar_Tipo_Reserva
        /// COMENTARIOS: Esta operación actualiza el tipo de reserva
        /// PARÁMETROS: Datos.- Clase de negocios
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  09/Marzo/2012
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static Boolean Modificar_Tipo_Reserva(Cls_Ope_Con_Reservas_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;

            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ");
                Mi_SQL.Append(Ope_Psp_Reservas.Campo_Tipo_Reserva + "='" + Datos.P_Tipo_Reserva + "' ");
                if(!String.IsNullOrEmpty(Datos.P_No_Deuda))
                {
                    Mi_SQL.Append(", NO_DEUDA='" + Datos.P_No_Deuda + "'");
                }
                Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + Datos.P_No_Reserva);

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                //OracleHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
            return Operacion_Completa;
        }
        /// ********************************************************************************************************************
        /// NOMBRE: Modificar_Saldos_Reserva
        /// COMENTARIOS: Esta operación actualiza el tipo de reserva
        /// PARÁMETROS: Datos.- Clase de negocios
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  09/Marzo/2012
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static void Modificar_Saldos_Reserva(Cls_Ope_Con_Reservas_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ");
                if (Datos.P_Tipo_Modificacion == "AMPLIACION")
                {
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Importe_Inicial + "=" + Ope_Psp_Reservas.Campo_Importe_Inicial +" + "+ Datos.P_Importe + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Saldo + "=" + Ope_Psp_Reservas.Campo_Saldo + " + " + Datos.P_Importe + ", ");
                }
                else
                {
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Importe_Inicial + "=" + Ope_Psp_Reservas.Campo_Importe_Inicial + " - " + Datos.P_Importe + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Saldo + "=" + Ope_Psp_Reservas.Campo_Saldo + " - " + Datos.P_Importe + ", ");
                }
                Mi_SQL.Append(Ope_Psp_Reservas.Campo_Usuario_Modifico + "='" + Cls_Sessiones.Nombre_Empleado + "', ");
                Mi_SQL.Append(Ope_Psp_Reservas.Campo_Fecha_Modifico + "= GETDATE() ");
                Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + Datos.P_No_Reserva);
                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                foreach (DataRow Partida_Reserva in Datos.P_Dt_Detalles_Reserva.Rows)
                {
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles  + " SET ");
                    if (Datos.P_Tipo_Modificacion == "AMPLIACION")
                    {
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + "=" + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " + " + Partida_Reserva["IMPORTE"].ToString() + ", ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Saldo + "=" + Ope_Psp_Reservas_Detalles.Campo_Saldo + " + " + Partida_Reserva["IMPORTE"].ToString() + ", ");
                    }
                    else
                    {
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + "=" + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " - " + Partida_Reserva["IMPORTE"].ToString() + ", ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Saldo + "=" + Ope_Psp_Reservas_Detalles.Campo_Saldo + " - " + Partida_Reserva["IMPORTE"].ToString() + ", ");
                    }
                    Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Usuario_Modifico + "='" + Cls_Sessiones.Nombre_Empleado + "', ");
                    Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fecha_Modifico + "= GETDATE() ");
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + "='" + Datos.P_No_Reserva);
                    Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Capitulo_ID + "='" + Partida_Reserva["CAPITULO_ID"].ToString());
                    Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + "='" + Partida_Reserva["FUENTE_FINANCIAMIENTO_ID"].ToString());
                    Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + "='" + Partida_Reserva["PARTIDA_ID"].ToString());
                    Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + "='" + Partida_Reserva["PROGRAMA_ID"].ToString()+"'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                }
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        #endregion
    }
}
