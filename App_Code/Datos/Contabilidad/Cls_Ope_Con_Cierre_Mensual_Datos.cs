﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Cierre_Mensual.Negocio;
using JAPAMI.Polizas.Negocios;

namespace JAPAMI.Cierre_Mensual.Datos
{
    public class Cls_Ope_Con_Cierre_Mensual_Datos
    {
        #region (Metodos Operacion)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Cierre_Mensual
            /// DESCRIPCION : Comienza la operacion del Cierre Mensual
            /// PARAMETROS  : Datos: Recibe los datos necesarios para efectuar la consulta.
            /// CREO        : Salvador L. Rea Ayala
            /// FECHA_CREO  : 30/Septiembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Cierre_Mensual(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                String Mi_SQL; //Variable para la consulta para la póliza
                try
                {
                    Mi_SQL = "SELECT " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", ";
                    Mi_SQL += Ope_Con_Polizas_Detalles.Campo_Debe + ", ";
                    Mi_SQL += Ope_Con_Polizas_Detalles.Campo_Haber;
                    Mi_SQL += " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                    Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " = '" + Datos.P_Mes_Anio + "'";
                    Mi_SQL += " ORDER BY " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " ASC";
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Cuentas_Movimientos_Mes
            /// DESCRIPCION : Consulta las cuentas 
            /// PARAMETROS  : 
            /// CREO        : Salvador L. Rea Ayala
            /// FECHA_CREO  : 30/Septiembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Cuentas_Movimientos_Mes(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                String Mi_SQL; //Variable para la consulta para la póliza
                try
                {
                    Mi_SQL = "SELECT DISTINCT " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID;
                    Mi_SQL += " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                    Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " = '" + Datos.P_Mes_Anio + "'";
                    Mi_SQL += " ORDER BY " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " ASC";
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Cuentas_Contables_Afectables
            /// DESCRIPCION : Consulta las cuentas que se pueden afectar 
            /// PARAMETROS  : 
            /// CREO        : SErgio Manuel Gallardo 
            /// FECHA_CREO  : 08/Noviembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Cuentas_Contables_Afectables(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                String Mi_SQL; //Variable para la consulta para la póliza
                try
                {
                    Mi_SQL = "SELECT  " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "," + Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta + " As Naturaleza";
                    Mi_SQL += " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables ;
                    Mi_SQL += " WHERE " + Cat_Con_Cuentas_Contables.Campo_Afectable + " = 'SI'";
                    Mi_SQL += " ORDER BY " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " ASC";
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Saldo_Inicial_Cierre_Mensual
            /// DESCRIPCION : Consulta el Saldo Inicial de la cuenta con respecto al ultimo cierre
            /// PARAMETROS  : 
            /// CREO        : Salvador L. Rea Ayala
            /// FECHA_CREO  : 3/Octubre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Saldo_Inicial_Cierre_Mensual(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                String Mi_SQL;             //Variable de Consulta para el Saldo Final del Cierre Mensual anterior
                String Mes_Anio_Anterior;  //Variable que contendra el mes anterior al cierre.
                DateTime fecha;
                try
                {
                    //se establece el mes anterior para obtener su saldo
                    if (Datos.P_Mes_Anio.Substring(0, 2) == "01")
                    {
                        //Mes_Anio_Anterior = "12" + String.Format("{0:00}", Convert.ToDouble(Datos.P_Mes_Anio.Substring(2, 2)) - 1);
                        Mes_Anio_Anterior = "13" + String.Format("{0:00}", Convert.ToDouble(Datos.P_Mes_Anio.Substring(2, 2)) - 1);
                    }
                    else
                    {
                        Mes_Anio_Anterior = String.Format("{0:00}", Convert.ToDouble(Datos.P_Mes_Anio.Substring(0, 2)) - 1) + Datos.P_Mes_Anio.Substring(2, 2);
                    }
                    //Da de Alta los datos del Cierre Mensual con los datos proporcionados por el usuario
                    Mi_SQL = "SELECT " + Ope_Con_Cierre_Mensual.Campo_Saldo_Final;
                    Mi_SQL += " FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual;
                    Mi_SQL += " WHERE " + Ope_Con_Cierre_Mensual.Campo_Mes_Anio + " = '" + Mes_Anio_Anterior + "'";
                    Mi_SQL += " AND " + Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID + "'";

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }
        #endregion

        #region (Operaciones [Alta - Actualizar - Eliminar])
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Cierre_Mensual_Alta
            /// DESCRIPCION : Consulta las cuentas que tuvieron movimientos en el mes.
            /// PARAMETROS  : 
            /// CREO        : Salvador L. Rea Ayala
            /// FECHA_CREO  : 3/Octubre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static void Cierre_Mensual_Alta(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                String Mi_SQL;             //Variable de Consulta para la Alta del Cierre Mensual
                Object Cierre_Mensual_ID; //Variable que contendrá el ID de la consulta

                try
                {
                    //Obtiene el ID del ultimo Cierre Mensual.
                    Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Con_Cierre_Mensual.Campo_Cierre_Mensual_ID + "),'00000') ";
                    Mi_SQL = Mi_SQL + "FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID + "' ";
                    Cierre_Mensual_ID = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    
                    if (Convert.IsDBNull(Cierre_Mensual_ID))
                        Datos.P_Cierre_Mensual_ID = "00001";                    
                    else
                        Datos.P_Cierre_Mensual_ID = String.Format("{0:00000}", Convert.ToInt32(Cierre_Mensual_ID) + 1);

                    //Da de Alta los datos del Cierre Mensual con los datos proporcionados por el usuario
                    Mi_SQL = "INSERT INTO " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "(";
                    Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Cierre_Mensual_ID + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Mes_Anio + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Saldo_Inicial + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Saldo_Final + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Total_Debe + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Total_Haber + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Diferencia + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Fecha_Inicio + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Fecha_Final + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Usuario_Creo + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Fecha_Creo + ") VALUES ('";
                    Mi_SQL += Datos.P_Cierre_Mensual_ID + "', '";
                    Mi_SQL += Datos.P_Cuenta_Contable_ID + "', '";
                    Mi_SQL += Datos.P_Mes_Anio + "', ";
                    Mi_SQL += Datos.P_Saldo_Inicial + ", ";
                    Mi_SQL += Datos.P_Saldo_Final + ", ";
                    Mi_SQL += Datos.P_Total_Debe + ", ";
                    Mi_SQL += Datos.P_Total_Haber + ", ";
                    Mi_SQL += Datos.P_Diferencia + ", '";
                    Mi_SQL += Datos.P_Fecha_Inicio + "', '";
                    Mi_SQL += Datos.P_Fecha_Final + "', '";
                    Mi_SQL += Datos.P_Usuario_Creo + "', GETDATE())";

                    SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Alta_Cierre_Mensual_General
            /// DESCRIPCION : realiza el cierre del mes con las cuentas.
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 07/Noviembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static void Alta_Cierre_Mensual_General(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                String Mi_SQL;             //Variable de Consulta para la Alta del Cierre Mensual
                Object Cierre_Mensual_ID; //Variable que contendrá el ID de la consulta

                try
                {
                    //Obtiene el ID del ultimo Cierre Mensual.
                    Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Con_Cierre_Mensual_Gral.Campo_Cierre_Mensual_Gral_ID   + "),'00000') ";
                    Mi_SQL = Mi_SQL + "FROM " + Ope_Con_Cierre_Mensual_Gral.Tabla_Ope_Con_Cierre_Mensual_Gral ;
                    Cierre_Mensual_ID = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Convert.IsDBNull(Cierre_Mensual_ID))
                        Datos.P_Cierre_Mensual_ID  = "00001";
                    else
                        Datos.P_Cierre_Mensual_ID = String.Format("{0:00000}", Convert.ToInt32(Cierre_Mensual_ID) + 1);

                    //Da de Alta los datos del Cierre Mensual con los datos proporcionados por el usuario
                    Mi_SQL = "INSERT INTO " + Ope_Con_Cierre_Mensual_Gral.Tabla_Ope_Con_Cierre_Mensual_Gral  + "(";
                    Mi_SQL += Ope_Con_Cierre_Mensual_Gral.Campo_Cierre_Mensual_Gral_ID   + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual_Gral.Campo_Mes   + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual_Gral.Campo_Anio   + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual_Gral.Campo_Estatus  + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual_Gral.Campo_Usuario_Creo   + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual_Gral.Campo_Fecha_Creo  + ") VALUES ('";
                    Mi_SQL += Datos.P_Cierre_Mensual_ID + "', '";
                    Mi_SQL += Datos.P_Mes  + "', '";
                    Mi_SQL += Datos.P_Anio  + "', ";
                    Mi_SQL += " 'CERRADO', '";
                    Mi_SQL += Datos.P_Usuario_Creo + "', GETDATE())";
                    SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Modifica_Cierre_Mensual_Gral
            /// DESCRIPCION : Afecta el cierre mensual de las cuentas y lo pone como afectado 
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 07/Noviembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static void Modifica_Cierre_Mensual_Gral(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                String Mi_SQL;             //Variable de Consulta para la Alta del Cierre Mensual


                try
                {
                    //Da de Alta los datos del Cierre Mensual con los datos proporcionados por el usuario
                    Mi_SQL = "UPDATE " + Ope_Con_Cierre_Mensual_Gral.Tabla_Ope_Con_Cierre_Mensual_Gral + " SET ";
                    Mi_SQL += Ope_Con_Cierre_Mensual_Gral.Campo_Estatus + "='" + Datos.P_Estatus +"',";
                    Mi_SQL += Ope_Con_Cierre_Mensual_Gral.Campo_Usuario_Modifico + "='"+ Datos.P_Usuario_Modifico +"',";
                    Mi_SQL += Ope_Con_Cierre_Mensual_Gral.Campo_Fecha_Modifico + "= GETDATE() " + " WHERE ";
                    Mi_SQL += Ope_Con_Cierre_Mensual_Gral.Campo_Mes + " ='" + Datos.P_Mes + "' AND " + Ope_Con_Cierre_Mensual_Gral.Campo_Anio + "='" + Datos.P_Anio+"'";
                    SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Limpiar_Cierre_Mensual
            /// DESCRIPCION : Elimina los registros de las cuentas que se hayan cerrado con anterioridad
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 08/Noviembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static void Limpiar_Cierre_Mensual(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                //Declaracion de variables
                String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
                SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
                SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
                SqlTransaction Transaccion_SQL = null;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        

                try
                {
                    //Verificar si el comando es externo
                    if (Datos.P_Cmmd == null)
                    {
                        if (Conexion_Base.State != ConnectionState.Open)
                        {
                            Conexion_Base.Open(); //Abre la conexión a la base de datos            
                        }
                        Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                        Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                        Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
                    }
                    else
                    {
                        Comando_SQL = Datos.P_Cmmd;
                    }

                    //Asignar consulta
                    Mi_SQL = "DELETE  FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual ;
                    Mi_SQL += " WHERE " + Ope_Con_Cierre_Mensual.Campo_Mes_Anio + "='"+ Datos.P_Mes_Anio+"'";

                    //Ejecutar consulta
                    Comando_SQL.CommandText = Mi_SQL;
                    Comando_SQL.ExecuteNonQuery();

                    //Ejecutar transaccion
                    if (Datos.P_Cmmd == null)
                    {
                        Transaccion_SQL.Commit();
                    }
                }
                catch (SqlException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    Conexion_Base.Close();
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Cierre_Mensual_Actualizar
            /// DESCRIPCION : Consulta las cuentas que tuvieron movimientos en el mes.
            /// PARAMETROS  : 
            /// CREO        : Salvador L. Rea Ayala
            /// FECHA_CREO  : 3/Octubre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            /////*******************************************************************************
            //public static void Cierre_Mensual_Actualizar(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            //{
            //    String Mi_SQL;             //Variable de Consulta para la Alta del Cierre Mensual
            //    Object Cierre_Mensual_ID; //Variable que contendrá el ID de la consulta

            //    try
            //    {
            //        //Da de Alta los datos del Cierre Mensual con los datos proporcionados por el usuario
            //        Mi_SQL = "UPDATE " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + " SET ";
            //        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Saldo_Final + " = " + Datos.P_Saldo_Final + ", ";
            //        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Total_Debe + " = " + Datos.P_Total_Debe + ", ";
            //        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Total_Haber + " = " + Datos.P_Total_Haber + ", ";
            //        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Diferencia  + " = " + Datos.P_Diferencia  + ", ";
            //        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Fecha_Modifico + " = GETDATE() , ";
            //        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Modifico+"' ";
            //        Mi_SQL += " WHERE " + Ope_Con_Cierre_Mensual.Campo_Cierre_Mensual_ID + " = '" + Datos.P_Cierre_Mensual_ID + "'";

            //        SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            //    }
            //    catch (SqlException Ex)
            //    {
            //        throw new Exception("Error: " + Ex.Message);
            //    }
            //    catch (Exception Ex)
            //    {
            //        throw new Exception("Error: " + Ex.Message);
            //    }
            //}
             //*******************************************************************************
            //NOMBRE DE LA FUNCION: Abrir_Cierre_Mensual
             //DESCRIPCION : Re-abrir el mes que esta cerrado .
             //PARAMETROS  : 
             //CREO        : Sergio Manuel Gallardo Andrade
             //FECHA_CREO  : 09/Noviembre/2011
             //MODIFICO          :
             //FECHA_MODIFICO    :
             //CAUSA_MODIFICACION:
            //*******************************************************************************
            public static void Abrir_Cierre_Mensual(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                //Declaracion de variables
                String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
                SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
                SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
                SqlTransaction Transaccion_SQL = null;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        

                try
                {
                    //Verificar si el comando es externo
                    if (Datos.P_Cmmd == null)
                    {
                        if (Conexion_Base.State != ConnectionState.Open)
                        {
                            Conexion_Base.Open(); //Abre la conexión a la base de datos            
                        }
                        Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                        Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                        Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
                    }
                    else
                    {
                        Comando_SQL = Datos.P_Cmmd;
                    }

                    //Elimina el Registro que tiene el mes cerrado para que se visualice como abierto
                    Mi_SQL = "UPDATE " + Ope_Con_Cierre_Mensual_Gral.Tabla_Ope_Con_Cierre_Mensual_Gral + " SET ";
                    Mi_SQL += Ope_Con_Cierre_Mensual_Gral.Campo_Estatus + " = 'ABIERTO'";
                    Mi_SQL += " WHERE " + Ope_Con_Cierre_Mensual_Gral.Campo_Anio + " = '" + Datos.P_Anio + "' ";
                    Mi_SQL += " AND " + Ope_Con_Cierre_Mensual_Gral.Campo_Mes + " = '" + Datos.P_Mes + "'";
                    //Mi_SQL = "DELETE FROM " + Ope_Con_Cierre_Mensual_Gral.Tabla_Ope_Con_Cierre_Mensual_Gral + " ";
                    //Mi_SQL += " WHERE " + Ope_Con_Cierre_Mensual_Gral.Campo_Anio + " = '" + Datos.P_Anio  + "' ";
                    //Mi_SQL += " AND " + Ope_Con_Cierre_Mensual_Gral.Campo_Mes + " = '" + Datos.P_Mes  + "'";
                    
                    //Ejecutar consulta
                    Comando_SQL.CommandText = Mi_SQL;
                    Comando_SQL.ExecuteNonQuery();

                    //Verificar si esxiste el comando
                    if (Datos.P_Cmmd == null)
                    {
                        //Ejecutar transaccion
                        Transaccion_SQL.Commit();
                    }
                }
                catch (SqlException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    Conexion_Base.Close();
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Cierre_Mensual_Completo
            /// DESCRIPCION : Hacer el cierre mensual completo con transaccion
            /// PARAMETROS  : Datos: variable de la capa de negocios
            /// CREO        : Noe Mosqueda Valadez
            /// FECHA_CREO  : 30/Abril/2013 15:30
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static void Cierre_Mensual_Completo(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                //Declaracion de variables
                String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
                SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
                SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
                SqlTransaction Transaccion_SQL = null;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
                DataTable Dt_Cuentas_Cierre_Mensual = new DataTable(); //Tabla para lamacenar las cuentas existentes de un determinado cierre mensual
                DataTable Dt_Movimientos = new DataTable(); //Tabla para lamacenar los movimientos de cada una de las cuentas
                DataTable Dt_Saldo_Inicial = new DataTable(); //tabla que contendra el cierre final del mes anterior
                decimal Debe = 0; //Variable apra el calculo del debe
                decimal Haber = 0; //variable para el calculo del haber
                int Cont_Elementos = 0; //Variable para el contador
                string Mes_Anio_Anterior = string.Empty; //Variable para el mes y año anterior
                SqlDataAdapter Da; //Adaptador para llenar las datatable
                DataTable Dt_Aux = new DataTable(); //tabla auxiliar para las consultas
                string Cierre_Mensual_ID = string.Empty; //Variable para el ID del cierre mensual
                object aux; //Objeto auxiliar para las consultas escalares

                try
                {                    
                    //Verificar si el comando es externo
                    if (Datos.P_Cmmd == null)
                    {
                        if (Conexion_Base.State != ConnectionState.Open)
                        {
                            Conexion_Base.Open(); //Abre la conexión a la base de datos            
                        }
                        Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                        Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                        Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
                    }
                    else
                    {
                        Comando_SQL = Datos.P_Cmmd;
                    }

                    //Instanciar el dataadpater
                    Da = new SqlDataAdapter(Comando_SQL);

                    //Construir la cadena para la consulta de las cuentas contables afectables con los totales de debe y haber calculados
                    Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ", " +
                        "Debe = (SELECT ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Debe + "), 0) " +
                        "FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + " " +
                        "WHERE " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " = '" + Datos.P_Mes_Anio + "' " +
                        "AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = " +
                        Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "), " +
                        "Haber = (SELECT ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Haber + "), 0) " +
                        "FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + " " +
                        "WHERE " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " = '" + Datos.P_Mes_Anio + "' " +
                        "AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = " +
                        Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "), " +
                        Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta + " AS Naturaleza " +
                        "FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " " +
                        "WHERE " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Afectable + " = 'SI' " +
                        "ORDER BY " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " ASC ";

                    //Ejecutar consulta
                    Comando_SQL.CommandText = Mi_SQL;
                    Da.SelectCommand = Comando_SQL;
                    Da.Fill(Dt_Movimientos);

                    //Consultar si ya se hizo previamente este cierre mensual
                    Mi_SQL = "SELECT * FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual;
                    Mi_SQL += " WHERE " + Ope_Con_Cierre_Mensual.Campo_Mes_Anio + " = '" + Datos.P_Mes_Anio + "'";
                    Mi_SQL += " ORDER BY " + Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " ASC";

                    //Ejecutar consulta
                    Comando_SQL.CommandText = Mi_SQL;
                    Da.SelectCommand = Comando_SQL;
                    Da.Fill(Dt_Cuentas_Cierre_Mensual);

                    //Verificar si hay elementos
                    if (Dt_Cuentas_Cierre_Mensual.Rows.Count > 0)
                    {
                        //Hacer la limpieza del cierre mensual
                        Limpiar_Cierre_Mensual(Datos);
                    }

                    //Ciclo para el barrido de la tabla
                    for (Cont_Elementos = 0; Cont_Elementos < Dt_Movimientos.Rows.Count; Cont_Elementos++)
                    {
                        //Construir las fechas
                        //verificar si es el cierre anual
                        if (Datos.P_Mes_Anio.Substring(0, 2) == "13")
                        {
                            Datos.P_Fecha_Inicio = "31/12/" + Datos.P_Anio;
                        }
                        else
                        {
                            Datos.P_Fecha_Inicio = string.Format("{0:dd/MM/yy}", new DateTime(Convert.ToInt32(Datos.P_Anio), Convert.ToInt32(Datos.P_Mes_Anio.Substring(0, 2)), 1));
                        }

                        Datos.P_Fecha_Final = string.Format("{0:dd/MM/yy}", DateTime.Now);

                        //Colocar la cuenta contable
                        Datos.P_Cuenta_Contable_ID = Dt_Movimientos.Rows[Cont_Elementos]["Cuenta_Contable_ID"].ToString().Trim();

                        //Construir la cadena del mes-año anterior
                        if (Datos.P_Mes_Anio.Substring(0, 2) == "01")
                        {
                            Mes_Anio_Anterior = "13" + String.Format("{0:00}", Convert.ToDouble(Datos.P_Mes_Anio.Substring(2, 2)) - 1);
                        }
                        else
                        {
                            Mes_Anio_Anterior = String.Format("{0:00}", Convert.ToDouble(Datos.P_Mes_Anio.Substring(0, 2)) - 1) + Datos.P_Mes_Anio.Substring(2, 2);
                        }

                        //Construir consulta
                        Mi_SQL = "SELECT " + Ope_Con_Cierre_Mensual.Campo_Saldo_Final;
                        Mi_SQL += " FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual;
                        Mi_SQL += " WHERE " + Ope_Con_Cierre_Mensual.Campo_Mes_Anio + " = '" + Mes_Anio_Anterior + "'";
                        Mi_SQL += " AND " + Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID + "'";

                        //Ejecutar consulta
                        Comando_SQL.CommandText = Mi_SQL;
                        Dt_Saldo_Inicial = new DataTable();
                        Da.Fill(Dt_Saldo_Inicial);

                        //Verificar si la consulta arrojo resultados
                        if (Dt_Saldo_Inicial.Rows.Count == 0)
                        {
                            Datos.P_Saldo_Inicial = "0";
                        }
                        else
                        {
                            Datos.P_Saldo_Inicial = Dt_Saldo_Inicial.Rows[0][0].ToString().Trim();
                        }

                        //Colocar los totales del debe y haber
                        Datos.P_Total_Debe = Dt_Movimientos.Rows[Cont_Elementos]["Debe"].ToString().Trim();
                        Datos.P_Total_Haber = Dt_Movimientos.Rows[Cont_Elementos]["Haber"].ToString().Trim();

                        //Obtener el saldo final verificando el tipo de cuenta
                        if (Dt_Movimientos.Rows[Cont_Elementos]["Naturaleza"].ToString().Trim().ToUpper() == "DEUDOR")
                        {
                            Datos.P_Saldo_Final = Convert.ToString((Convert.ToDecimal(Datos.P_Saldo_Inicial) + Convert.ToDecimal(Datos.P_Total_Debe) - Convert.ToDecimal(Datos.P_Total_Haber)));
                        }
                        else
                        {
                            Datos.P_Saldo_Final = Convert.ToString((Convert.ToDecimal(Datos.P_Saldo_Inicial) + Convert.ToDecimal(Datos.P_Total_Haber) - Convert.ToDecimal(Datos.P_Total_Debe)));
                        }

                        //Obtener la diferencia
                        Datos.P_Diferencia = (Convert.ToDecimal(Datos.P_Total_Debe) - Convert.ToDecimal(Datos.P_Total_Haber)).ToString();

                        //Alta del cierre mensual
                        //Obtiene el ID del ultimo Cierre Mensual.
                        Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Con_Cierre_Mensual.Campo_Cierre_Mensual_ID + "),'00000') ";
                        Mi_SQL = Mi_SQL + "FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual;
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID + "' ";

                        //Ejecutar consulta
                        Comando_SQL.CommandText = Mi_SQL;
                        aux = null;
                        aux = Comando_SQL.ExecuteScalar();
                        
                        if (Convert.IsDBNull(aux))
                            Datos.P_Cierre_Mensual_ID = "00001";
                        else
                            Datos.P_Cierre_Mensual_ID = String.Format("{0:00000}", Convert.ToInt32(aux) + 1);

                        //Da de Alta los datos del Cierre Mensual con los datos proporcionados por el usuario
                        Mi_SQL = "INSERT INTO " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "(";
                        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Cierre_Mensual_ID + ", ";
                        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + ", ";
                        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Mes_Anio + ", ";
                        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Saldo_Inicial + ", ";
                        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Saldo_Final + ", ";
                        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Total_Debe + ", ";
                        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Total_Haber + ", ";
                        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Diferencia + ", ";
                        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Fecha_Inicio + ", ";
                        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Fecha_Final + ", ";
                        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Usuario_Creo + ", ";
                        Mi_SQL += Ope_Con_Cierre_Mensual.Campo_Fecha_Creo + ") VALUES ('";
                        Mi_SQL += Datos.P_Cierre_Mensual_ID + "', '";
                        Mi_SQL += Datos.P_Cuenta_Contable_ID + "', '";
                        Mi_SQL += Datos.P_Mes_Anio + "', ";
                        Mi_SQL += Datos.P_Saldo_Inicial + ", ";
                        Mi_SQL += Datos.P_Saldo_Final + ", ";
                        Mi_SQL += Datos.P_Total_Debe + ", ";
                        Mi_SQL += Datos.P_Total_Haber + ", ";
                        Mi_SQL += Datos.P_Diferencia + ", '";
                        Mi_SQL += Datos.P_Fecha_Inicio + "', '";
                        Mi_SQL += Datos.P_Fecha_Final + "', '";
                        Mi_SQL += Datos.P_Usuario_Creo + "', GETDATE())";

                        //Ejecutar consulta
                        Comando_SQL.CommandText = Mi_SQL;
                        Comando_SQL.ExecuteNonQuery();
                    }


                    //Verificar si esxiste el comando
                    if (Datos.P_Cmmd == null)
                    {
                        //Ejecutar transaccion
                        Transaccion_SQL.Commit();
                    }
                }
                catch (SqlException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    Conexion_Base.Close();
                }
            }
        #endregion

        #region (Metodos Consulta)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cierre_Mensual_Auxiliar
            /// DESCRIPCION : Consulta el Saldo Inicial de la cuenta con respecto al ultimo cierre
            /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
            /// CREO        : Salvador L. Rea Ayala
            /// FECHA_CREO  : 18/Octubre/2011
            /// MODIFICO          :Hugo Enrique Ramírez Aguilera
            /// FECHA_MODIFICO    :20-febrero-2012
            /// CAUSA_MODIFICACION:Cuando el mes se trate de 13 se buscara todas las cuentas del año
            ///*******************************************************************************
            public static DataTable Consulta_Cierre_Mensual_Auxiliar(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                String Mi_SQL;  //Almacenara la Query de Consulta.
                try
                {
                    //Consulta los movimientos de las cuentas contables.
                    Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + ", ";
                    Mi_SQL += Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ", ";
                    Mi_SQL += Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Saldo_Final + " AS SALDO_FINAL,";
                    Mi_SQL += " TO_DATE(" +Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Mes_Anio +",'MM-YY') AS MES";
                    Mi_SQL += " FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + " RIGHT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " ON " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " = " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;

                    //  para cualdo el mes_anio = 13
                    if (Datos.P_Mes=="13")
                    {
                        String Anio = Datos.P_Mes_Anio.Substring(2, 2);
                        Mi_SQL += " WHERE  TO_DATE(" + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Mes_Anio +
                                ",'MM-YY') >= '01/01/" + Anio + "' And TO_DATE(" + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Mes_Anio +
                                ",'MM-YY') <= '01/12/" + Anio + "'";
                    }
                    else // se realiza la consulta normal
                        Mi_SQL += " WHERE " + Ope_Con_Cierre_Mensual.Campo_Mes_Anio + " = '" + Datos.P_Mes_Anio + "'";
                    
                    if (!String.IsNullOrEmpty(Datos.P_Descripcion))
                    {
                        Mi_SQL += " AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " LIKE UPPER ('%" + Datos.P_Descripcion + "%')";
                    }
                    Mi_SQL += " ORDER BY CUENTA ASC";

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cierre_Mensual
            /// DESCRIPCION : Consulta el Saldo Inicial de la cuenta con respecto al ultimo cierre
            /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
            /// CREO        : Salvador L. Rea Ayala
            /// FECHA_CREO  : 18/Octubre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Cierre_Mensual(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                String Mi_SQL;  //Almacenara la Query de Consulta.
                try
                {
                    //Consulta los movimientos de las cuentas contables.
                    Mi_SQL = "SELECT * FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual;
                    Mi_SQL += " WHERE " + Ope_Con_Cierre_Mensual.Campo_Mes_Anio + " = '" + Datos.P_Mes_Anio + "'";
                    Mi_SQL += " ORDER BY " + Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " ASC";

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Bitacora
            /// DESCRIPCION : Consulta las polizas que entraron en un mes cerrado
            /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 09/Noviembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Bitacora(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                String Mi_SQL;  //Almacenara la Query de Consulta.
                String Mes_Anio;
                DateTime fecha;
                try
                {
                    fecha = Convert.ToDateTime(String.Format("{0: dd/MMMM/yyyy}", "01/" + Datos.P_Mes + "/" + Datos.P_Anio ));
                    Mes_Anio = String.Format("{0:MMyy}",fecha);

                    //Consulta los movimientos de las cuentas contables.
                    Mi_SQL = "SELECT Cuenta." + Cat_Con_Cuentas_Contables.Campo_Cuenta + ", Bitacora." + Ope_Con_Bitacora_Polizas.Campo_Usuario_Creo + ", ";
                    Mi_SQL += " Bitacora." + Ope_Con_Bitacora_Polizas.Campo_Fecha_Creo + ", Bitacora." + Ope_Con_Bitacora_Polizas.Campo_Debe + ", ";
                    Mi_SQL += " Bitacora." + Ope_Con_Bitacora_Polizas .Campo_Haber +", Tipo."+ Cat_Con_Tipo_Polizas.Campo_Descripcion + " FROM " + Ope_Con_Bitacora_Polizas.Tabla_Ope_Con_Bitacora_Polizas +" Bitacora, ";
                    Mi_SQL += Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables +" Cuenta,"+ Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas +" Tipo WHERE Bitacora."+Ope_Con_Bitacora_Polizas.Campo_Cuenta_Contable_ID +" = Cuenta.";
                    Mi_SQL += Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID+" AND Tipo."+ Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID +" = Bitacora."+Ope_Con_Bitacora_Polizas.Campo_Tipo_Poliza_ID + " AND Bitacora."+ Ope_Con_Bitacora_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'";
                    Mi_SQL += " ORDER BY " + Ope_Con_Bitacora_Polizas.Campo_No_Bitacora  + " ASC";

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cierre_General
            /// DESCRIPCION : Consulta los meses cerrados por el usuario 
            /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 04/Noviembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Cierre_General(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                String Mi_SQL;  //Almacenara la Query de Consulta.
                try
                {
                    //Consulta los movimientos de las cuentas contables.
                    Mi_SQL = "SELECT * FROM " + Ope_Con_Cierre_Mensual_Gral.Tabla_Ope_Con_Cierre_Mensual_Gral;
                    Mi_SQL += " WHERE " + Ope_Con_Cierre_Mensual_Gral.Campo_Anio  + " = '" + Datos.P_Anio  + "'";
                    if (Datos.P_Mes != null)
                    {
                        Mi_SQL += " AND " + Ope_Con_Cierre_Mensual_Gral.Campo_Mes  + " = '" + Datos.P_Mes  + "'";
                    }
                    Mi_SQL += " ORDER BY " + Ope_Con_Cierre_Mensual_Gral.Campo_Cierre_Mensual_Gral_ID  + " ASC";

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Total_Cierre_Mensual
            /// DESCRIPCION : Consultar el total del debe y del haber de un mes/año particular
            /// PARAMETROS  : Datos: Variable de la capa de negocios
            /// CREO        : Noe Mosqueda Valadez
            /// FECHA_CREO  : 19/Mayo/2012 12:00
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Total_Cierre_Mensual(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                //Declaracion de variables
                DataTable Dt_Resultado = new DataTable(); //Tabla para el resultado
                String Mi_SQL = String.Empty; //Variable para la consutruccion de la consulta

                try
                {
                    //Asignar consulta
                    Mi_SQL = "SELECT SUM(Total_Debe) AS Total_Debe, SUM(Total_Haber) AS Total_Haber FROM Ope_Con_Cierre_Mensual " +
                        "WHERE Mes_Anio = '" + Datos.P_Mes_Anio + "'";

                    //Ejecutar consulta
                    Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                    //Entregar resultado
                    return Dt_Resultado;
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Totales_Poliza_Cierre_Mensual
            /// DESCRIPCION : Consultar los totales del debe y del haber por poliza
            /// PARAMETROS  : Datos: Variable de la capa de negocios
            /// CREO        : Noe Mosqueda Valadez
            /// FECHA_CREO  : 19/Mayo/2012 12:45
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Totales_Poliza_Cierre_Mensual(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                //Declaracion de variables
                DataTable Dt_Resultado = new DataTable(); //Tabla para el resultado
                String Mi_SQL = String.Empty; //Variable para la consutruccion de la consulta

                try
                {
                    //Asignar consulta
                    Mi_SQL = "SELECT Ope_Con_Polizas_Detalles.No_Poliza, Ope_Con_Polizas_Detalles.Tipo_Poliza_ID, Ope_Con_Polizas_Detalles.Mes_Ano, Ope_Con_Polizas.Prefijo, " +
                        "SUM(Ope_Con_Polizas_Detalles.Debe) AS Total_Debe, SUM(Ope_Con_Polizas_Detalles.Haber) AS Total_Haber FROM Ope_Con_Polizas_Detalles " +
                        "INNER JOIN Ope_Con_Polizas ON Ope_Con_Polizas_Detalles.No_Poliza = Ope_Con_Polizas.No_Poliza " +
                        "AND Ope_Con_Polizas_Detalles.Mes_Ano = Ope_Con_Polizas.Mes_Ano " +
                        "AND Ope_Con_Polizas_Detalles.Tipo_Poliza_ID = Ope_Con_Polizas.Tipo_Poliza_ID " +
                        "WHERE Ope_Con_Polizas_Detalles.Mes_Ano = '" + Datos.P_Mes_Anio + "' " +
                        "GROUP BY Ope_Con_Polizas_Detalles.No_Poliza, Ope_Con_Polizas_Detalles.Tipo_Poliza_ID, Ope_Con_Polizas_Detalles.Mes_Ano, Ope_Con_Polizas.Prefijo " +
                        "ORDER BY Ope_Con_Polizas_Detalles.No_poliza, Ope_Con_Polizas_Detalles.Tipo_Poliza_ID  ";

                    //Ejecutar consulta
                    Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                    //Entregar resultado
                    return Dt_Resultado;
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consideraciones_Cierre_Mensual
            /// DESCRIPCION : Validar que este cerrado el mes anterior o el cierre anual
            /// PARAMETROS  : Datos: Variable de la capa de negocios
            /// CREO        : Noe Mosqueda Valadez
            /// FECHA_CREO  : 09/Abril/2013 17:40
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static String Consideraciones_Cierre_Mensual(Cls_Ope_Con_Cierre_Mensual_Negocio Datos)
            {
                //Declaracion de variables
                String Resultado = String.Empty; //variable para el resultado
                String Mi_SQL = String.Empty; //variable para la consulta
                Object aux; //variable auxiliar
                string Mes_Anio_aux = String.Empty;
                int int_Aux = 0; //variable apra el calculo del año

                try
                {
                    //Verificar si es el mes de enero
                    if (Datos.P_Mes_Anio.StartsWith("01") == true)
                    {
                        //Construir la cadena del mes y el año
                        Mes_Anio_aux = "13";

                        //Obtener el año
                        int_Aux = Convert.ToInt32(Datos.P_Mes_Anio.Substring(2, 2)) - 1;
                        if (int_Aux.ToString().Trim().Length == 1)
                        {
                            Mes_Anio_aux += "0" + int_Aux.ToString().Trim();
                        }
                        else
                        {
                            Mes_Anio_aux += int_Aux.ToString().Trim();
                        }

                        //Consulta para la poliza
                        Mi_SQL = "SELECT COUNT(*) FROM OPE_CON_POLIZAS WHERE MES_ANO = '" + Mes_Anio_aux + "'";

                        //Ejecutar consulta
                        aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                        //Verificar si es mayor de cero
                        if (Convert.ToInt32(aux) == 0)
                        {
                            Resultado = "Se necesita ejecutar el cierre anual antes de cerrar el mes";
                        }
                    }
                    else
                    {
                        //Construir el mes-año anterior
                        int_Aux = Convert.ToInt32(Datos.P_Mes_Anio.Substring(0, 2)) - 1;

                        if (int_Aux.ToString().Trim().Length == 1)
                        {
                            Mes_Anio_aux = "0" + int_Aux.ToString().Trim() + Datos.P_Mes_Anio.Substring(2, 2);
                        }
                        else
                        {
                            Mes_Anio_aux = int_Aux.ToString().Trim() + Datos.P_Mes_Anio.Substring(2, 2);
                        }

                        //Consulta para el cierre anterior
                        Mi_SQL = "SELECT ISNULL(ESTATUS, 'ABIERTO') FROM OPE_CON_CIERRE_MENSUAL_GRAL WHERE ANIO = '" + Datos.P_Anio + "' " +
                            "AND MES = '" + Datos.P_Mes + "' ";

                        //Ejecutar consulta
                        aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                        //Verificar si es mayor de cero
                        if (aux !=null)
                        {
                            if (aux.ToString().Trim() == "ABIERTO")
                            {
                                Resultado = "Se necesita ejecutar el cierre mensual del mes anterior antes de cerrar el mes";
                            }
                       }
                    }
                    //Entregar resultado
                    return Resultado;
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
            }
        #endregion
    }
}