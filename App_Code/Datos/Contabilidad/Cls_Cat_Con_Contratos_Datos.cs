﻿using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Sessiones;
using JAPAMI.Cat_Contratos.Negocio;
using JAPAMI.Cat_Psp_Rubros.Datos;
using JAPAMI.Manejo_Presupuesto.Datos;

namespace JAPAMI.Cat_Contratos.Datos
{
    public class Cls_Cat_Con_Contratos_Datos
    {
        #region Metodos
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Contratos
            ///DESCRIPCIÓN          : consulta para obtener los datos de los contratos
            ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 03/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Contratos(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    Mi_Sql.Append("SELECT " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Contrato_ID + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Numero_Contrato + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Nombre_Contrato + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Fecha_Inicio + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Fecha_Fin+ ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Estatus + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Proveedor_ID + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Cuenta_Banco_Contratos_ID + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Forma_Pago + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Importe_Total + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_No_Reserva + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Anticipo_Monto + ", ");
                    Mi_Sql.Append("ISNULL(" + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Anticipo_Porcentaje + ",0) AS ANTICIPO_PORCENTAJE, ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Amortizado + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Por_Amortizar+ ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Saldo_Total + ", ");
                    Mi_Sql.Append("ISNULL(" + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Avance_Fisico + ",0) AS AVANCE_FISICO, ");
                    Mi_Sql.Append("ISNULL(" + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Avance_Financiero + ",0) AS AVANCE_FINANCIERO, ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Usuario_Creo + ", ");
                    Mi_Sql.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Convenio_ID + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Archivo + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Ruta_Archivo + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Fecha_Inicial_Original + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Fecha_Final_Original + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Motivo + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Importe_Original + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Localidad + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Modalidad + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Fianza_Garantia + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Fianza_Anticipo + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Fecha_Fianza + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Monto_Garantia + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Fecha_Finiquito + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Monto_Anticipo);
                    Mi_Sql.Append(" FROM " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores);
                    Mi_Sql.Append(" ON " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Proveedor_ID);
                    Mi_Sql.Append(" = " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID);

                    if(!String.IsNullOrEmpty(Negocio.P_Contrato_ID))
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Contrato_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Contrato_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Estatus))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Estatus);
                            Mi_Sql.Append(" = '" + Negocio.P_Estatus.Trim() + "'");
                        }
                        else 
                        {
                            Mi_Sql.Append(" WHERE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Estatus);
                            Mi_Sql.Append(" = '" + Negocio.P_Estatus.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Nom_Contrato))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Nombre_Contrato);
                            Mi_Sql.Append(" = '" + Negocio.P_Nom_Contrato.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Nombre_Contrato);
                            Mi_Sql.Append(" = '" + Negocio.P_Nom_Contrato.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Num_Contrato))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Numero_Contrato);
                            Mi_Sql.Append(" = '" + Negocio.P_Num_Contrato.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Numero_Contrato);
                            Mi_Sql.Append(" = '" + Negocio.P_Num_Contrato.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Ini) && !String.IsNullOrEmpty(Negocio.P_Fecha_Fin))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Fecha_Inicio);
                            Mi_Sql.Append(" BETWEEN '" + Negocio.P_Fecha_Ini.Trim() + " 00:00:00'");
                            Mi_Sql.Append(" AND '" + Negocio.P_Fecha_Fin.Trim() + " 23:59:00'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Fecha_Inicio);
                            Mi_Sql.Append(" BETWEEN '" + Negocio.P_Fecha_Ini.Trim() + " 00:00:00'");
                            Mi_Sql.Append(" AND '" + Negocio.P_Fecha_Fin.Trim() + " 23:59:00'");
                        }
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_No_Reserva))
                    {
                        if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" AND " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_No_Reserva);
                            Mi_Sql.Append(" = '" + Negocio.P_No_Reserva.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" WHERE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_No_Reserva);
                            Mi_Sql.Append(" = '" + Negocio.P_No_Reserva.Trim() + "'");
                        }
                    }


                    Mi_Sql.Append(" ORDER BY " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Fecha_Inicio + " DESC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los contratos. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Contratos_Detalles
            ///DESCRIPCIÓN          : consulta para obtener los datos de los detalles de los contratos 
            ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 03/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Contratos_Detalles(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    Mi_Sql.Append("SELECT ID = '', " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "." + Cat_Con_Contratos_Det.Campo_Contrato_ID + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "." + Cat_Con_Contratos_Det.Campo_Proyecto_Programa_ID + " AS PROGRAMA_ID ,");
                    Mi_Sql.Append(Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "." + Cat_Con_Contratos_Det.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "." + Cat_Con_Contratos_Det.Campo_Dependencia_ID + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "." + Cat_Con_Contratos_Det.Campo_Area_Funcional_ID + ", ");
                    Mi_Sql.Append(Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "." + Cat_Con_Contratos_Det.Campo_Partida_ID+ ", ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS CLAVE_NOMBRE_PP, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE_FF, ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS CLAVE_NOMBRE_AF, ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE_UR, ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CLAVE_NOMBRE_P, ");
                    Mi_Sql.Append(Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "." + Cat_Con_Contratos_Det.Campo_Importe);
                    Mi_Sql.Append(" FROM " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" ON " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "." + Cat_Con_Contratos_Det.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" ON " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "." + Cat_Con_Contratos_Det.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                    Mi_Sql.Append(" ON " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "." + Cat_Con_Contratos_Det.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "." + Cat_Con_Contratos_Det.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_Sql.Append(" ON " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "." + Cat_Con_Contratos_Det.Campo_Partida_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Contrato_ID.Trim()))
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "." + Cat_Con_Contratos_Det.Campo_Contrato_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Contrato_ID.Trim() + "'");
                    }

                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE_PP ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los detalles de los contratos. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Dependencias
            ///DESCRIPCIÓN          : consulta para obtener los datos de las dependencias
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 03/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Dependencias(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT DISTINCT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + ", ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + ", ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS CLAVE_NOMBRE_AF ");
                    Mi_Sql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                    Mi_Sql.Append(" ON " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_FF_ID.Trim()))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_FF_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_FF_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_PP_ID.Trim()))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_PP_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_PP_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_UR_ID.Trim()))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_UR_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_UR_ID.Trim() + "'");
                        }
                    }

                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las depencencias. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Programas
            ///DESCRIPCIÓN          : consulta para obtener los datos de los programas
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 03/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Programas(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT DISTINCT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                    
                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los programas. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Fuentes
            ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 03/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Fuentes(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_PP_ID.Trim()))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_PP_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_PP_ID.Trim() + "'");
                        }
                    }

                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas
            ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 03/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Partidas(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT DISTINCT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible);
                    Mi_Sql.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_FF_ID.Trim()))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_FF_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_FF_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_PP_ID.Trim()))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_PP_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_PP_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_UR_ID.Trim()))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_UR_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_UR_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_P_ID.Trim()))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_P_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_P_ID.Trim() + "'");
                        }
                    }

                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las partidas. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas
            ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 03/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Presupuesto(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT DISTINCT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CLAVE_NOMBRE_P, ");
                    Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE_UR, ");
                    Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + ", ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS CLAVE_NOMBRE_AF, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS CLAVE_NOMBRE_PP, ");
                    Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE_FF, ");
                    Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + ".Saldo");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID+" AND ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Estatus + "='ACTIVO'");
                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI'");
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID+ " AND ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Estatus + "='ACTIVO'");
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                    Mi_Sql.Append(" ON " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID+ " AND ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional  + "." + Cat_SAP_Area_Funcional.Campo_Estatus + "='ACTIVO'");
                    Mi_Sql.Append(" INNER JOIN " +  Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID+ " AND ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas  + "." + Cat_Sap_Partidas_Especificas.Campo_Estatus + "='ACTIVO'");
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas );
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id+ " AND ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Estatus + "='ACTIVO'");
                    Mi_Sql.Append("LEFT OUTER JOIN " + Cat_Con_Programas_Acciones.Tabla_Cat_Con_Programas_Acciones + " CON");
                    Mi_Sql.Append(" ON " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" = CON." + Cat_Con_Programas_Acciones.Campo_Proyecto_Programa_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio))
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " > 0 ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_FF_ID))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_FF_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_FF_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_PP_ID))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_PP_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_PP_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_UR_ID))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_UR_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_UR_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_P_ID))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_P_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql.Append(" = '" + Negocio.P_P_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_P))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE (" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_P.Trim().ToLower() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_P.Trim().ToUpper() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_P.Trim() + "%')");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND (" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_P.Trim().ToLower() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_P.Trim().ToUpper() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_P.Trim() + "%')");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_PP))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE (" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_PP.Trim().ToLower() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_PP.Trim().ToUpper() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_PP.Trim() + "%')");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND (" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_PP.Trim().ToLower() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_PP.Trim().ToUpper() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_PP.Trim() + "%')");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_UR))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE (" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_UR.Trim().ToLower() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_UR.Trim().ToUpper() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_UR.Trim() + "%')");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND (" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_UR.Trim().ToLower() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_UR.Trim().ToUpper() + "%'");
                            Mi_Sql.Append(" OR " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_UR.Trim() + "%')");
                        }
                    }


                    if (!String.IsNullOrEmpty(Negocio.P_FF))
                    {
                        if (!Mi_Sql.ToString().Contains("WHERE"))
                        {
                            Mi_Sql.Append(" WHERE (" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_FF.Trim().ToLower() + "%'");
                            Mi_Sql.Append(" OR " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_FF.Trim().ToUpper() + "%'");
                            Mi_Sql.Append(" OR " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_FF.Trim() + "%')");
                        }
                        else
                        {
                            Mi_Sql.Append(" AND (" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_FF.Trim().ToLower() + "%'");
                            Mi_Sql.Append(" OR " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_FF.Trim().ToUpper() + "%'");
                            Mi_Sql.Append(" OR " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+ ");
                            Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion);
                            Mi_Sql.Append(" LIKE '%" + Negocio.P_FF.Trim() + "%')");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Convenio_ID))
                    {
                        Mi_Sql.Append(" AND CON." + Cat_Con_Programas_Acciones.Campo_Convenio_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Convenio_ID + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" AND CON." + Cat_Con_Programas_Acciones.Campo_Convenio_ID+" IS NULL ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID))
                    {
                        Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_Sql.Append(" = '" + Negocio.P_Programa_ID + "'");
                    }

                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE_PP ASC");

                   return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros del presupuesto. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_contratos
            ///DESCRIPCIÓN          : consulta para guardar los datos de los contratos
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 03/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static String Alta_contratos(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
                Boolean Operacion_Completa = false;//Estado de la operacion.
                String Contrato_ID = String.Empty;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                Int32 No_Reserva;
                Int32 No_Reserva_Det;
                Double Saldo=0.00;
                DataTable Dt = new DataTable();
                DataTable Dt_Saldo = new DataTable();
                Int32 Accion_Contrato_ID;

                try
                {
                    Dt = Negocio.P_Dt_Detalles;

                    //insertamos los datos de la reserva para obtener el no de reserva
                    No_Reserva = Convert.ToInt32(Cls_Cat_Psp_Rubros_Datos.Consecutivo_ID(Ope_Psp_Reservas.Campo_No_Reserva, Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas, "10"));

                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "(");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_No_Reserva + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Estatus + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Fecha + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Usuario_Creo + ", ");
                    if (Negocio.P_Dt_Detalles != null && Negocio.P_Dt_Detalles.Rows.Count > 0)
                    {
                        Mi_SQL.Append(Ope_Psp_Reservas.Campo_Dependencia_ID + ", ");
                    }
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Anio + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Importe_Inicial + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Saldo + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Tipo_Solicitud_Pago_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Proveedor_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Tipo_Reserva + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Recurso + ") VALUES(");
                    Mi_SQL.Append(No_Reserva + ", ");
                    Mi_SQL.Append("'GENERADA', ");
                    Mi_SQL.Append("'CONTRATO [" + Negocio.P_Num_Contrato.Trim() + "]', ");
                    Mi_SQL.Append("GETDATE(), ");
                    Mi_SQL.Append("GETDATE(), ");
                    Mi_SQL.Append("'" + Negocio.P_Usuario_Creo.Trim() + "', ");
                    if (Negocio.P_Dt_Detalles != null && Negocio.P_Dt_Detalles.Rows.Count > 0)
                    {
                        Mi_SQL.Append("'" + Dt.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString().Trim() + "', ");
                    }
                    Mi_SQL.Append("'" + Negocio.P_Anio.Trim() + "', ");
                    Mi_SQL.Append(Negocio.P_Importe_Total.Trim().Replace(",", "") + ", ");
                    Mi_SQL.Append(Negocio.P_Importe_Total.Trim().Replace(",", "") + ", ");
                    Mi_SQL.Append("'C-" + Negocio.P_Busqueda_Proveedor.Trim() + "', ");
                    Mi_SQL.Append("'00014', ");
                    Mi_SQL.Append("'" + Negocio.P_Proveedor_ID + "', ");
                    Mi_SQL.Append("'ABIERTA', ");
                    Mi_SQL.Append("'RAMO 33') ");

                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();

                    //INSERTAMOS LOS DETALLES DE LA RESERVA
                    No_Reserva_Det = Convert.ToInt32(Cls_Cat_Psp_Rubros_Datos.Consecutivo_ID(Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle, Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles, "10"));

                    Mi_SQL = new StringBuilder();
                    if (Negocio.P_Dt_Detalles != null && Negocio.P_Dt_Detalles.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt.Rows)
                        {
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("INSERT INTO " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "(");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_No_Reserva + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Saldo + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Usuario_Creo + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fecha_Creo + ")");
                            Mi_SQL.Append(" VALUES(" + No_Reserva_Det + ", ");
                            Mi_SQL.Append(No_Reserva + ", ");
                            Mi_SQL.Append("'" + Dr[Cat_Sap_Partidas_Especificas.Campo_Partida_ID].ToString().Trim() + "', ");
                            Mi_SQL.Append(Dr["IMPORTE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMPORTE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append("'" + Dr[Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr["PROGRAMA_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Negocio.P_Usuario_Creo.Trim() + "', ");
                            Mi_SQL.Append("GETDATE())");

                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();

                            No_Reserva_Det++;
                        }
                    }
                    
                    //////insertamos el momento presupuestal
                    ////Mi_SQL = new StringBuilder();
                    ////Mi_SQL.Append("INSERT INTO " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos + "(");
                    ////Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_No_Reserva + ", ");
                    ////Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Cargo + ", ");
                    ////Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Abono + ", ");
                    ////Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Importe + ", ");
                    ////Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Fecha + ", ");
                    ////Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Usuario + ", ");
                    ////Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Usuario_Creo + ", ");
                    ////Mi_SQL.Append(Ope_Psp_Registro_Movimientos.Campo_Fecha_Creo + ") VALUES(");
                    ////Mi_SQL.Append(No_Reserva + ", ");
                    ////Mi_SQL.Append("'COMPROMETIDO', ");
                    ////Mi_SQL.Append("'DISPONIBLE', ");
                    ////Mi_SQL.Append(Negocio.P_Importe_Total.Trim().Replace(",", "") + ", ");
                    ////Mi_SQL.Append("SYSDATE, ");
                    ////Mi_SQL.Append("'" + Negocio.P_Usuario_Creo.Trim() + "', ");
                    ////Mi_SQL.Append("'" + Negocio.P_Usuario_Creo.Trim() + "', ");
                    ////Mi_SQL.Append("SYSDATE)");

                    ////Cmd.CommandText = Mi_SQL.ToString();
                    ////Cmd.ExecuteNonQuery();

                    //obtenemos el id del contrato
                    Contrato_ID = Cls_Cat_Psp_Rubros_Datos.Consecutivo_ID(Cat_Con_Contratos.Campo_Contrato_ID, Cat_Con_Contratos.Tabla_Cat_Con_Contratos, "10");

                    //INSERTAMOS LOS DATOS DEL CONTRATO
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "(");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Contrato_ID + ", ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Numero_Contrato + ", ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Nombre_Contrato + ", ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Inicio + ", ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Fin + ", ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Inicial_Original + ", ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Final_Original + ", ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Estatus + ", ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Proveedor_ID + ", ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Importe_Original + ", ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Importe_Total + ", ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Cuenta_Banco_Contratos_ID + ", ");
                    
                    if (!String.IsNullOrEmpty(Negocio.P_Forma_Pago.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Forma_Pago + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Anticipo_Monto.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Anticipo_Monto + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Anticipo_Porcentaje.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Anticipo_Porcentaje + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Amortizado.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Amortizado + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Por_Amortizar.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Por_Amortizar + ", ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Saldo_Total.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Saldo_Total + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Avance_Fisico.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Avance_Fisico + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Avance_Financiero.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Avance_Financiero + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Convenio_ID.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Convenio_ID+ ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Localidad.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Localidad  + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Modalidad.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Modalidad + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Monto_Fianza.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Monto_Garantia + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Fianza.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Fianza + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Finiquito.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Finiquito + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fianza_Anticipo.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fianza_Anticipo + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fianza_Garantia.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fianza_Garantia + ", ");
                    }
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_No_Reserva + ", ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Creo + ") VALUES (");
                    Mi_SQL.Append("'" + Contrato_ID.Trim() + "', ");
                    Mi_SQL.Append("'" + Negocio.P_Num_Contrato.Trim() + "', ");
                    Mi_SQL.Append("'" + Negocio.P_Nom_Contrato.Trim() + "', ");
                    Mi_SQL.Append("'" + Negocio.P_Fecha_Ini.Trim() + "', ");
                    Mi_SQL.Append("'" + Negocio.P_Fecha_Fin.Trim() + "', ");
                    Mi_SQL.Append("'" + Negocio.P_Fecha_Ini.Trim() + "', ");
                    Mi_SQL.Append("'" + Negocio.P_Fecha_Fin.Trim() + "', ");
                    Mi_SQL.Append("'" + Negocio.P_Estatus.Trim() + "', ");
                    Mi_SQL.Append("'" + Negocio.P_Proveedor_ID.Trim() + "', ");
                    Mi_SQL.Append(Negocio.P_Importe_Total.Trim().Replace(",", "") + ", ");
                    Mi_SQL.Append(Negocio.P_Importe_Total.Trim().Replace(",", "") + ",'");
                    Mi_SQL.Append(Negocio.P_Cuenta_Contrato_ID + "','");

                    if (!String.IsNullOrEmpty(Negocio.P_Forma_Pago.Trim()))
                    {
                        Mi_SQL.Append(Negocio.P_Forma_Pago.Trim() + "', ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Anticipo_Monto.Trim()))
                    {
                        Mi_SQL.Append(Negocio.P_Anticipo_Monto.Trim().Replace(",", "") + ", ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Anticipo_Porcentaje.Trim()))
                    {
                        Mi_SQL.Append(Negocio.P_Anticipo_Porcentaje.Trim().Replace(",", "") + ", ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Amortizado.Trim()))
                    {
                        Mi_SQL.Append(Negocio.P_Amortizado.Trim().Replace(",", "") + ", ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Por_Amortizar.Trim()))
                    {
                        Mi_SQL.Append(Negocio.P_Por_Amortizar.Trim().Replace(",", "") + ", ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Saldo_Total.Trim()))
                    {
                        Mi_SQL.Append(Negocio.P_Saldo_Total.Trim().Replace(",", "") + ", ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Avance_Fisico.Trim()))
                    {
                        Mi_SQL.Append(Negocio.P_Avance_Fisico.Trim().Replace(",", "") + ", ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Avance_Financiero.Trim()))
                    {
                        Mi_SQL.Append(Negocio.P_Avance_Financiero.Trim().Replace(",", "") + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Convenio_ID.Trim()))
                    {
                        Mi_SQL.Append("'"+Negocio.P_Convenio_ID.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Localidad.Trim()))
                    {
                        Mi_SQL.Append("'" + Negocio.P_Localidad.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Modalidad.Trim()))
                    {
                        Mi_SQL.Append("'" + Negocio.P_Modalidad.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Monto_Fianza.Trim()))
                    {
                        Mi_SQL.Append("" + Negocio.P_Monto_Fianza.Trim().Replace(",", "") + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Fianza.Trim()))
                    {
                        Mi_SQL.Append("'" + Negocio.P_Fecha_Fianza.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Finiquito.Trim()))
                    {
                        Mi_SQL.Append("'" + Negocio.P_Fecha_Finiquito.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fianza_Anticipo.Trim()))
                    {
                        Mi_SQL.Append("'" + Negocio.P_Fianza_Anticipo.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fianza_Garantia.Trim()))
                    {
                        Mi_SQL.Append("'" + Negocio.P_Fianza_Garantia.Trim().Replace(",", "") + "', ");
                    }
                    Mi_SQL.Append(No_Reserva.ToString().Trim() + ", ");
                    Mi_SQL.Append("'" + Negocio.P_Usuario_Creo.Trim() + "', ");
                    Mi_SQL.Append("GETDATE())");

                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();

                    //INSERTAMOS LOS DETALLES DEL CONTRATO

                    if (Negocio.P_Dt_Detalles != null && Negocio.P_Dt_Detalles.Rows.Count > 0)
                    { 
                        foreach (DataRow Dr in Dt.Rows) 
                        {
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("INSERT INTO " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "(");
                            Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Contrato_ID + ", ");
                            Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Fuente_Financiamiento_ID + ", ");
                            Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Proyecto_Programa_ID + ", ");
                            Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Area_Funcional_ID + ", ");
                            Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Dependencia_ID + ", ");
                            Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Partida_ID + ", ");
                            Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Importe + ", ");
                            Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Usuario_Creo + ", ");
                            Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Fecha_Creo + ") VALUES( ");
                            Mi_SQL.Append("'" + Contrato_ID.Trim() + "', ");
                            Mi_SQL.Append("'" + Dr[Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr["PROGRAMA_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr[Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr[Cat_Dependencias.Campo_Dependencia_ID].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr[Cat_Sap_Partidas_Especificas.Campo_Partida_ID].ToString().Trim() + "', ");
                            Mi_SQL.Append(Dr["IMPORTE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append("'" + Negocio.P_Usuario_Creo.Trim() + "', ");
                            Mi_SQL.Append("GETDATE())");

                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();

                            ////Mi_SQL = new StringBuilder();
                            ////Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                            ////Mi_SQL.Append(" SET DISPONIBLE = DISPONIBLE - " + Dr["IMPORTE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                            ////Mi_SQL.Append(" COMPROMETIDO = COMPROMETIDO + " + Dr["IMPORTE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                            ////Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Creo + "', ");
                            ////Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = SYSDATE ");
                            ////Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr[Cat_Dependencias.Campo_Dependencia_ID].ToString().Trim() + "'");
                            ////Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr[Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID].ToString().Trim() + "'");
                            ////Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr[Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id].ToString().Trim() + "'");
                            ////Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr[Cat_Sap_Partidas_Especificas.Campo_Partida_ID].ToString().Trim() + "'");
                            ////Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Negocio.P_Anio);

                            ////Cmd.CommandText = Mi_SQL.ToString();
                            ////Cmd.ExecuteNonQuery();
                        }
                    }
                    //Se da de alta las acciones del contrato 
                    if (Negocio.P_Dt_Acciones != null && Negocio.P_Dt_Acciones.Rows.Count > 0)
                    {

                        foreach (DataRow Dr_Recorrido in Negocio.P_Dt_Acciones.Rows)
                        {
                            Accion_Contrato_ID = Convert.ToInt32(Cls_Cat_Psp_Rubros_Datos.Consecutivo_ID(Cat_Con_Contratos_Acciones.Campo_Contrato_Accion_ID, Cat_Con_Contratos_Acciones.Tabla_Cat_Con_Contratos_Acciones, "10"));
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("INSERT INTO " + Cat_Con_Contratos_Acciones.Tabla_Cat_Con_Contratos_Acciones);
                            Mi_SQL.Append(" (" + Cat_Con_Contratos_Acciones.Campo_Contrato_Accion_ID + "," + Cat_Con_Contratos_Acciones.Campo_Accion_ID);
                            Mi_SQL.Append("," + Cat_Con_Contratos_Acciones.Campo_Contrato_ID + ")");
                            Mi_SQL.Append(" VALUES (" + Accion_Contrato_ID);
                            Mi_SQL.Append(",'" + Dr_Recorrido["ACCION_ID"].ToString() + "'");
                            Mi_SQL.Append(",'" + Contrato_ID + "')");
                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();
                        }
                    }
                    //Actualiza el Saldo del progrma
                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID) && !String.IsNullOrEmpty(Negocio.P_Importe_Total))
                    {
                        Dt_Saldo = Consulta_Saldo(Negocio.P_Programa_ID);
                        if (Dt_Saldo != null && Dt_Saldo.Rows.Count > 0)
                        {
                            Saldo = Convert.ToDouble(Dt_Saldo.Rows[0][0].ToString());
                            Saldo = Saldo - Convert.ToDouble(Negocio.P_Importe_Total);
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("UPDATE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                            Mi_SQL.Append(" SET Saldo = '" + Saldo.ToString() + "'");
                            Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID + "'");
                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();
                        }
                    }
                    Trans.Commit();
                    Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(No_Reserva), "PRE_COMPROMETIDO", "DISPONIBLE", Dt);
                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(No_Reserva), "PRE_COMPROMETIDO", "DISPONIBLE", Convert.ToDouble(Negocio.P_Importe_Total.Trim().Replace(",", "")), "", "", "", "");
                    Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(No_Reserva), "COMPROMETIDO", "PRE_COMPROMETIDO", Dt);
                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(No_Reserva), "COMPROMETIDO", "PRE_COMPROMETIDO", Convert.ToDouble(Negocio.P_Importe_Total.Trim().Replace(",", "")), "", "", "", "");
                    
                    Operacion_Completa = true;
                    return No_Reserva.ToString().Trim();
                }
                catch (SqlException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    Cn.Close();
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Saldo
            ///DESCRIPCIÓN          : consulta para modificar los datos de los contratos
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios
            ///CREO                 : Armando Zavala Moreno
            ///FECHA_CREO           : 12/Septiembre/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consulta_Saldo(String Programa)
            {
                StringBuilder Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT ISNULL(Saldo, 0)");
                Mi_SQL.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Programa + "'");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Contratos
            ///DESCRIPCIÓN          : consulta para modificar los datos de los contratos
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 03/Agosto/2012
            ///MODIFICO             :Sergio Manuel Gallardo Andrade    
            ///FECHA_MODIFICO       :27/Septiembre/2012
            ///CAUSA_MODIFICACIÓN   :Falta el registro de los momentos presupuestales y la reserva no se pueden generar denuevo sus detalles
            ///                       solo se deben actualizar 
            ///*******************************************************************************
            internal static Boolean Modificar_Contratos(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
                Boolean Operacion_Completa = false;//Estado de la operacion.
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                DataTable Dt_Det_Ant = new DataTable();
                DataTable Dt_Reserva = new DataTable();
                Int32 No_Reserva=0;
                Int32 No_Reserva_Det;
                Int32 Accion_Contrato_ID;
                String Estatus = String.Empty;
                DataTable Dt = new DataTable();
                Double Saldo = 0.00;//Para actualizar el saldo del programa
                DataTable Dt_Saldo = new DataTable();
                DataTable Dt_Reserva_Datos = new DataTable();
                DataTable Dt_Reserva_Datos_Detalles = new DataTable();
                try
                {
                    Dt = Negocio.P_Dt_Detalles;

                    //INSERTAMOS LOS DATOS DEL CONTRATO
                    Mi_SQL.Append("UPDATE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + " SET ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Numero_Contrato + " = '" + Negocio.P_Num_Contrato.Trim() + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Nombre_Contrato + " = '" + Negocio.P_Nom_Contrato.Trim() + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Inicio + " = '" + Negocio.P_Fecha_Ini.Trim()  + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Fin + " = '" + Negocio.P_Fecha_Fin.Trim() + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Inicial_Original + " = '" + Negocio.P_Fecha_Ini.Trim() + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Final_Original + " = '" + Negocio.P_Fecha_Fin.Trim() + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Estatus + " = '" + Negocio.P_Estatus.Trim() + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Proveedor_ID + " = '" + Negocio.P_Proveedor_ID.Trim() + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Cuenta_Banco_Contratos_ID + " = '" + Negocio.P_Cuenta_Contrato_ID + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Importe_Original + " = " + Negocio.P_Importe_Total.Trim().Replace(",", "") + ", ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Importe_Total + " = " + Negocio.P_Importe_Total.Trim().Replace(",", "") + ", ");

                    if (!String.IsNullOrEmpty(Negocio.P_Forma_Pago.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Forma_Pago + " ='" + Negocio.P_Forma_Pago.Trim() + "', ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Anticipo_Monto.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Anticipo_Monto + " = " + Negocio.P_Anticipo_Monto.Trim().Replace(",", "") + ", ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Anticipo_Porcentaje.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Anticipo_Porcentaje + " = " + Negocio.P_Anticipo_Porcentaje.Trim().Replace(",", "") + ", ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Amortizado.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Amortizado + " = " + Negocio.P_Amortizado.Trim().Replace(",", "") + ", ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Por_Amortizar.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Por_Amortizar + " = " + Negocio.P_Por_Amortizar.Trim().Replace(",", "") + ", ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Saldo_Total.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Saldo_Total + " = " + Negocio.P_Saldo_Total.Trim().Replace(",", "") + ", ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Avance_Fisico.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Avance_Fisico + " = " + Negocio.P_Avance_Fisico.Trim().Replace(",", "") + ", ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Avance_Financiero.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Avance_Financiero + " = " + Negocio.P_Avance_Financiero.Trim().Replace(",", "") + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Convenio_ID.Trim()))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Convenio_ID + " = '" + Negocio.P_Convenio_ID.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Inicial_Original))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Inicial_Original + " = '" + Negocio.P_Fecha_Inicial_Original.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Final_Original))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Final_Original+ " = '" + Negocio.P_Fecha_Final_Original.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Importe_Original))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Importe_Original + " = '" + Negocio.P_Importe_Original.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Motivo))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Motivo + " = '" + Negocio.P_Motivo.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Archivo))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Archivo + " = '" + Negocio.P_Archivo.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Ruta_Archivo))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Ruta_Archivo + " = '" + Negocio.P_Ruta_Archivo.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Localidad))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Localidad + " = '" + Negocio.P_Localidad.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Modalidad))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Modalidad + " = '" + Negocio.P_Modalidad.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Monto_Fianza))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Monto_Garantia + " = " + Negocio.P_Monto_Fianza.Trim().Replace(",", "") + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Fianza))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Fianza + " = '" + Negocio.P_Fecha_Fianza.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Finiquito))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Finiquito + " = '" + Negocio.P_Fecha_Finiquito.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fianza_Anticipo))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fianza_Anticipo + " = '" + Negocio.P_Fianza_Anticipo.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fianza_Garantia))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fianza_Garantia + " = '" + Negocio.P_Fianza_Garantia.Trim().Replace(",", "") + "', ");
                    }
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico.Trim() + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Modifico + " = GETDATE() ");
                    Mi_SQL.Append(" WHERE " + Cat_Con_Contratos_Det.Campo_Contrato_ID + " = '" + Negocio.P_Contrato_ID.Trim() + "'");

                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();

                    //obtenmos el numero de reserva del contrato
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT " + Cat_Con_Contratos.Campo_No_Reserva + " FROM " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos);
                    Mi_SQL.Append(" WHERE " + Cat_Con_Contratos_Det.Campo_Contrato_ID + " = '" + Negocio.P_Contrato_ID.Trim() + "'");

                    Dt_Reserva = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                    if (Dt_Reserva != null && Dt_Reserva.Rows.Count > 0)
                    {
                        if (Negocio.P_Estatus.Trim().Equals("CANCELADO")) { Estatus = "CANCELADA"; }
                        else if (Negocio.P_Estatus.Trim().Equals("TERMINADO") || Negocio.P_Estatus.Trim().Equals("ACTIVO")) { Estatus = "GENERADA"; }

                        No_Reserva = Convert.ToInt32(Dt_Reserva.Rows[0][Cat_Con_Contratos.Campo_No_Reserva]);

                        //Consultamos la reserva con sus detalles
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas );
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + No_Reserva + "'");
                        Dt_Reserva_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                        
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("SELECT " + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " AS FUENTE_FINANCIAMIENTO_ID ," + Ope_Psp_Reservas_Detalles.Campo_Saldo + " AS IMPORTE ," + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " AS PROGRAMA_ID ," + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + ".* FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = '" + No_Reserva + "'");
                        Dt_Reserva_Datos_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                        //modificamos los datos de la reserva
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ");
                        Mi_SQL.Append(Ope_Psp_Reservas.Campo_Estatus + " = '" + Estatus.Trim() + "', ");
                        Mi_SQL.Append(Ope_Psp_Reservas.Campo_Fecha + " = GETDATE(), ");
                        if (Negocio.P_Dt_Detalles != null && Negocio.P_Dt_Detalles.Rows.Count > 0)
                        {
                            Mi_SQL.Append(Ope_Psp_Reservas.Campo_Dependencia_ID + " = '" + Dt.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString().Trim() + "', ");
                        }
                        Mi_SQL.Append(Ope_Psp_Reservas.Campo_Anio + " = '" + Negocio.P_Anio.Trim() + "', ");
                        Mi_SQL.Append(Ope_Psp_Reservas.Campo_Importe_Inicial + " = " + Negocio.P_Importe_Total.Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Ope_Psp_Reservas.Campo_Saldo + " = " + Negocio.P_Importe_Total.Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Ope_Psp_Reservas.Campo_Beneficiario + " = 'C-" + Negocio.P_Busqueda_Proveedor.Trim() + "', ");
                        Mi_SQL.Append(Ope_Psp_Reservas.Campo_Proveedor_ID + " = '" + Negocio.P_Proveedor_ID + "'");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva);

                        Cmd.CommandText = Mi_SQL.ToString();
                        Cmd.ExecuteNonQuery();

                        //ELIMINAMOS LOS DETALLES DE LA RESERVA
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("DELETE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + No_Reserva);
                        
                        Cmd.CommandText = Mi_SQL.ToString();
                        Cmd.ExecuteNonQuery();

                        //INSERTAMOS LOS DETALLES DE LA RESERVA
                        No_Reserva_Det = Convert.ToInt32(Cls_Cat_Psp_Rubros_Datos.Consecutivo_ID(Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle, Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles, "10"));

                        if (Negocio.P_Dt_Detalles != null && Negocio.P_Dt_Detalles.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt.Rows)
                            {
                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("INSERT INTO " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "(");
                                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle + ", ");
                                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_No_Reserva + ", ");
                                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ", ");
                                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + ", ");
                                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Saldo + ", ");
                                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + ", ");
                                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + ", ");
                                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Usuario_Creo + ", ");
                                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fecha_Creo + ")");
                                Mi_SQL.Append(" VALUES(" + No_Reserva_Det + ", ");
                                Mi_SQL.Append(No_Reserva + ", ");
                                Mi_SQL.Append("'" + Dr[Cat_Sap_Partidas_Especificas.Campo_Partida_ID].ToString().Trim() + "', ");
                                Mi_SQL.Append(Dr["IMPORTE"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append(Dr["IMPORTE"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append("'" + Dr[Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Dr["PROGRAMA_ID"].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Negocio.P_Usuario_Creo.Trim() + "', ");
                                Mi_SQL.Append("GETDATE())");

                                Cmd.CommandText = Mi_SQL.ToString();
                                Cmd.ExecuteNonQuery();

                                No_Reserva_Det++;
                            }
                        }
                    }

                    if (Negocio.P_Estatus.Trim().Equals("ACTIVO") || Negocio.P_Estatus.Trim().Equals("TERMINADO"))
                    {
                        if (Negocio.P_Dt_Detalles != null && Negocio.P_Dt_Detalles.Rows.Count > 0)
                        {
                            Dt = Negocio.P_Dt_Detalles;

                            //ELIMINAMOS LOS DETALLES DEL CONTRATO PARA NO DUPLICAR DATOS
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("DELETE " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det);
                            Mi_SQL.Append(" WHERE " + Cat_Con_Contratos_Det.Campo_Contrato_ID + " = '" + Negocio.P_Contrato_ID.Trim() + "'");

                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();

                            //INSERTAMOS LOS DETALLES DEL CONTRATO
                            foreach (DataRow Dr in Dt.Rows)
                            {
                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("INSERT INTO " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "(");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Contrato_ID + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Fuente_Financiamiento_ID + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Proyecto_Programa_ID + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Area_Funcional_ID + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Dependencia_ID + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Partida_ID + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Importe + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Usuario_Creo + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Fecha_Creo + ") VALUES( ");
                                Mi_SQL.Append("'" + Negocio.P_Contrato_ID.Trim() + "', ");
                                Mi_SQL.Append("'" + Dr[Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Dr["PROGRAMA_ID"].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Dr[Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Dr[Cat_Dependencias.Campo_Dependencia_ID].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Dr[Cat_Sap_Partidas_Especificas.Campo_Partida_ID].ToString().Trim() + "', ");
                                Mi_SQL.Append(Dr["IMPORTE"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append("'" + Negocio.P_Usuario_Creo.Trim() + "', ");
                                Mi_SQL.Append("GETDATE())");

                                Cmd.CommandText = Mi_SQL.ToString();
                                Cmd.ExecuteNonQuery();
                            }
                            if (Negocio.P_Dt_Acciones != null && Negocio.P_Dt_Acciones.Rows.Count > 0)
                            {
                                //Se eliminan las acciones anteriores 
                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("DELETE " + Cat_Con_Contratos_Acciones.Tabla_Cat_Con_Contratos_Acciones);
                                Mi_SQL.Append(" WHERE " + Cat_Con_Contratos_Acciones.Campo_Contrato_ID + " = '" + Negocio.P_Contrato_ID.Trim() + "'");

                                Cmd.CommandText = Mi_SQL.ToString();
                                Cmd.ExecuteNonQuery();
                                Int32 Contador = 0;
                                //Se da de alta las acciones del contrato 
                                foreach (DataRow Dr_Recorrido in Negocio.P_Dt_Acciones.Rows)
                                {
                                    Accion_Contrato_ID = Convert.ToInt32(Cls_Cat_Psp_Rubros_Datos.Consecutivo_ID(Cat_Con_Contratos_Acciones.Campo_Contrato_Accion_ID, Cat_Con_Contratos_Acciones.Tabla_Cat_Con_Contratos_Acciones, "10"));
                                    Accion_Contrato_ID = Accion_Contrato_ID + Contador;
                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("INSERT INTO " + Cat_Con_Contratos_Acciones.Tabla_Cat_Con_Contratos_Acciones);
                                    Mi_SQL.Append(" (" + Cat_Con_Contratos_Acciones.Campo_Contrato_Accion_ID + "," + Cat_Con_Contratos_Acciones.Campo_Accion_ID);
                                    Mi_SQL.Append("," + Cat_Con_Contratos_Acciones.Campo_Contrato_ID + ")");
                                    Mi_SQL.Append(" VALUES (" + Accion_Contrato_ID);
                                    Mi_SQL.Append(",'" + Dr_Recorrido["ACCION_ID"].ToString() + "'");
                                    Mi_SQL.Append(",'" + Negocio.P_Contrato_ID.Trim() + "')");
                                    Cmd.CommandText = Mi_SQL.ToString();
                                    Cmd.ExecuteNonQuery();
                                    Contador = Contador + 1;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Negocio.P_Dt_Detalles != null && Negocio.P_Dt_Detalles.Rows.Count > 0)
                        {
                            Dt = Negocio.P_Dt_Detalles;

                            //ELIMINAMOS LOS DETALLES DEL CONTRATO PARA NO DUPLICAR DATOS
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("DELETE " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det);
                            Mi_SQL.Append(" WHERE " + Cat_Con_Contratos_Det.Campo_Contrato_ID + " = '" + Negocio.P_Contrato_ID.Trim() + "'");

                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();

                            //INSERTAMOS LOS DETALLES DEL CONTRATO
                            foreach (DataRow Dr in Dt.Rows)
                            {
                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("INSERT INTO " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + "(");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Contrato_ID + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Fuente_Financiamiento_ID + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Proyecto_Programa_ID + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Area_Funcional_ID + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Dependencia_ID + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Partida_ID + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Importe + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Usuario_Creo + ", ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Fecha_Creo + ") VALUES( ");
                                Mi_SQL.Append("'" + Negocio.P_Contrato_ID.Trim() + "', ");
                                Mi_SQL.Append("'" + Dr[Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Dr["PROGRAMA_ID"].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Dr[Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Dr[Cat_Dependencias.Campo_Dependencia_ID].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Dr[Cat_Sap_Partidas_Especificas.Campo_Partida_ID].ToString().Trim() + "', ");
                                Mi_SQL.Append(Dr["IMPORTE"].ToString().Trim().Replace(",", "") + ", ");
                                Mi_SQL.Append("'" + Negocio.P_Usuario_Creo.Trim() + "', ");
                                Mi_SQL.Append("GETDATE())");

                                Cmd.CommandText = Mi_SQL.ToString();
                                Cmd.ExecuteNonQuery();
                            }

                            //Se da de alta las acciones del contrato 
                            if (Negocio.P_Dt_Acciones != null && Negocio.P_Dt_Acciones.Rows.Count > 0)
                            {
                                //Se eliminan las acciones anteriores 
                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("DELETE " + Cat_Con_Contratos_Acciones.Tabla_Cat_Con_Contratos_Acciones);
                                Mi_SQL.Append(" WHERE " + Cat_Con_Contratos_Acciones.Campo_Contrato_ID + " = '" + Negocio.P_Contrato_ID.Trim() + "'");

                                Cmd.CommandText = Mi_SQL.ToString();
                                Cmd.ExecuteNonQuery();
                                Int32 Contador = 0;
                                foreach (DataRow Dr_Recorrido in Negocio.P_Dt_Acciones.Rows)
                                {
                                    Accion_Contrato_ID = Convert.ToInt32(Cls_Cat_Psp_Rubros_Datos.Consecutivo_ID(Cat_Con_Contratos_Acciones.Campo_Contrato_Accion_ID, Cat_Con_Contratos_Acciones.Tabla_Cat_Con_Contratos_Acciones, "10"));
                                    Accion_Contrato_ID = Accion_Contrato_ID + Contador;
                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("INSERT INTO " + Cat_Con_Contratos_Acciones.Tabla_Cat_Con_Contratos_Acciones);
                                    Mi_SQL.Append(" (" + Cat_Con_Contratos_Acciones.Campo_Contrato_Accion_ID + "," + Cat_Con_Contratos_Acciones.Campo_Accion_ID);
                                    Mi_SQL.Append("," + Cat_Con_Contratos_Acciones.Campo_Contrato_ID + ")");
                                    Mi_SQL.Append(" VALUES (" + Accion_Contrato_ID);
                                    Mi_SQL.Append(",'" + Dr_Recorrido["ACCION_ID"].ToString() + "'");
                                    Mi_SQL.Append(",'" + Negocio.P_Contrato_ID.Trim() + "')");
                                    Cmd.CommandText = Mi_SQL.ToString();
                                    Cmd.ExecuteNonQuery();
                                    Contador = Contador + 1;
                                }
                            }
                        }
                    }

                    //Actualiza el Saldo del progrma
                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID) && !String.IsNullOrEmpty(Negocio.P_Importe_Total) && !String.IsNullOrEmpty(Negocio.P_Saldo_Contrato))
                    {
                        Dt_Saldo = Consulta_Saldo(Negocio.P_Programa_ID);
                        if (Dt_Saldo != null && Dt_Saldo.Rows.Count > 0)
                        {
                            Saldo = Convert.ToDouble(Dt_Saldo.Rows[0][0].ToString())+ Convert.ToDouble(Negocio.P_Saldo_Contrato);
                            Saldo -= Convert.ToDouble(Negocio.P_Importe_Total);
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("UPDATE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                            Mi_SQL.Append(" SET Saldo = '" + Saldo.ToString() + "'");
                            Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Negocio.P_Programa_ID + "'");
                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();
                        }
                    }

                    Trans.Commit();
                    if (Dt_Reserva != null && Dt_Reserva.Rows.Count > 0)
                    {
                        if (Dt_Reserva_Datos.Rows.Count > 0 && Dt_Reserva_Datos_Detalles.Rows.Count > 0)
                        {
                            Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(No_Reserva), "PRE_COMPROMETIDO", "COMPROMETIDO", Dt_Reserva_Datos_Detalles);
                            Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(No_Reserva), "PRE_COMPROMETIDO", "COMPROMETIDO", Convert.ToDouble(Dt_Reserva_Datos.Rows[0][Ope_Psp_Reservas.Campo_Saldo]), "", "", "", "");
                            Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(No_Reserva), "DISPONIBLE", "PRE_COMPROMETIDO", Dt_Reserva_Datos_Detalles);
                            Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(No_Reserva), "DISPONIBLE", "PRE_COMPROMETIDO", Convert.ToDouble(Dt_Reserva_Datos.Rows[0][Ope_Psp_Reservas.Campo_Saldo]), "", "", "", "");
                        }
                        Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(No_Reserva), "PRE_COMPROMETIDO", "DISPONIBLE", Dt);
                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(No_Reserva), "PRE_COMPROMETIDO", "DISPONIBLE", Convert.ToDouble(Negocio.P_Importe_Total.Trim().Replace(",", "")), "", "", "", "");
                        Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(No_Reserva), "COMPROMETIDO", "PRE_COMPROMETIDO", Dt);
                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(No_Reserva), "COMPROMETIDO", "PRE_COMPROMETIDO", Convert.ToDouble(Negocio.P_Importe_Total.Trim().Replace(",", "")), "", "", "", "");
                    
                    }
                    Operacion_Completa = true;
                    return Operacion_Completa;
                }
                catch (SqlException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    Cn.Close();
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Contratos
            ///DESCRIPCIÓN          : consulta para modificar los datos de los contratos
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios
            ///CREO                 : Armando Zavala Moreno
            ///FECHA_CREO           : 14/Septiembre/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Boolean Modificar_Contratos_Ampliacion(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
                Boolean Operacion_Completa = false;//Estado de la operacion.
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                DataTable Dt_Det_Ant = new DataTable();
                DataTable Dt_Reserva = new DataTable();
                String Estatus = String.Empty;
                String Respuesta = "";
                String Ruta_Almacenar = "";
                try
                {
                    Mi_SQL.Append("UPDATE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + " SET ");

                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Ini))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Inicio + " = '" + Negocio.P_Fecha_Ini.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Fin))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Fin + " = '" + Negocio.P_Fecha_Fin.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Inicial_Original))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Inicial_Original + " = '" + Negocio.P_Fecha_Inicial_Original.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Final_Original))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Final_Original+ " = '" + Negocio.P_Fecha_Final_Original.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Importe_Original))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Importe_Original + " = '" + Negocio.P_Importe_Original.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Motivo))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Motivo + " = '" + Negocio.P_Motivo.Trim().Replace(",", "") + "', ");
                    }
                    
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico.Trim() + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Modifico + " = GETDATE() ");
                    Mi_SQL.Append(" WHERE " + Cat_Con_Contratos_Det.Campo_Contrato_ID + " = '" + Negocio.P_Contrato_ID.Trim() + "'");

                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();

                    //Se registra la Ampliacion del comtrato
                    if (!String.IsNullOrEmpty(Negocio.P_Archivo))
                    {
                        Ruta_Almacenar = Crear_Ruta(Negocio.P_Contrato_ID);
                        Ruta_Almacenar = Ruta_Almacenar + "\\";
                        Copiar_Archivo(Ruta_Almacenar, Negocio.P_Archivo);
                    }
                    //Se registra la Ampliacion del comtrato
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Cat_Con_Contratos_Ampliaciones.Tabla_Cat_Con_Contratos_Ampliaciones);
                    Mi_SQL.Append(" (" + Cat_Con_Contratos_Ampliaciones.Campo_Contrato_ID);
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Ini))
                    {
                        Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Fecha_Amp_Inicio);
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Fin))
                    {
                        Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Fecha_Amp_Fin);
                    }
                    Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Importe);
                    Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Motivo);
                    Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Archivo);
                    Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Ruta_Archivo);
                    Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Usuario_Creo);
                    Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Fecha_Creo);
                    Mi_SQL.Append(") VALUES('");
                    Mi_SQL.Append(Negocio.P_Contrato_ID.Trim() + "'");
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Ini))
                    {
                        Mi_SQL.Append(",'" + Negocio.P_Fecha_Ini.Trim() + "'");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Fin))
                    {
                        Mi_SQL.Append(",'" + Negocio.P_Fecha_Fin.Trim() + "'");
                    }
                    Mi_SQL.Append(",0");
                    Mi_SQL.Append(",'" + Negocio.P_Motivo.Trim() + "'");
                    Mi_SQL.Append(",'" + Negocio.P_Archivo + "'");
                    Mi_SQL.Append(",'" + Ruta_Almacenar + "'");
                    Mi_SQL.Append(",'" + Cls_Sessiones.Nombre_Empleado + "'");
                    Mi_SQL.Append(",GETDATE())");

                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();
                    Trans.Commit();
                    Operacion_Completa = true;
                }
                catch (SqlException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    Cn.Close();
                }
                return Operacion_Completa;
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Proveedor
            ///DESCRIPCIÓN          : consulta para obtener los datos de los proveedores
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 03/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Proveedor(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT DISTINCT " + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ");
                    Mi_Sql.Append(Cat_Com_Proveedores.Campo_Nombre);
                    Mi_Sql.Append(" FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores);
                    Mi_Sql.Append(" WHERE " + Cat_Com_Proveedores.Campo_Estatus + " = 'ACTIVO'");
                    Mi_Sql.Append(" AND " + Cat_Com_Proveedores.Campo_Tipo + " = 'CONTABILIDAD'");
                    Mi_Sql.Append(" AND " + Cat_Com_Proveedores.Campo_Cuenta_Contratista_ID + " IS NOT NULL ");

                    if (!String.IsNullOrEmpty(Negocio.P_Busqueda_Proveedor.Trim()))
                    {
                        Mi_Sql.Append(" AND (" + Cat_Com_Proveedores.Campo_Nombre + " LIKE '%" + Negocio.P_Busqueda_Proveedor.Trim().ToUpper() + "%'");
                        Mi_Sql.Append(" OR " + Cat_Com_Proveedores.Campo_Nombre + " LIKE '%" + Negocio.P_Busqueda_Proveedor.Trim().ToLower() + "%'");
                        Mi_Sql.Append(" OR " + Cat_Com_Proveedores.Campo_Nombre + " LIKE '%" + Negocio.P_Busqueda_Proveedor.Trim() + "%')");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Proveedor_ID.Trim()))
                    {
                        Mi_Sql.Append(" AND " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Negocio.P_Proveedor_ID.Trim() + "'");
                    }

                    Mi_Sql.Append(" ORDER BY " + Cat_Com_Proveedores.Campo_Nombre + " ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los proveedores. Error: [" + Ex.Message + "]");
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Guardar_Archivo_Directo
            ///DESCRIPCIÓN          : Guarda el documento en una carpeta de acuerdo al id del convenio
            ///PARAMETROS           : Negocio: conexion con la capa de negocios
            ///CREO                 : Sergio Pacheco
            ///FECHA_CREO           : 
            ///MODIFICO             : Armando Zavala Moreno
            ///FECHA_MODIFICO       : 18/Septiembre/2012
            ///CAUSA_MODIFICACIÓN   : Se adapato al modulo de Contabilidad
            ///*******************************************************************************
            public static String Guardar_Archivo_Directo(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                String Respuesta = "";
                String Ruta_Almacenar;

                if (!String.IsNullOrEmpty(Negocio.P_Archivo))
                {
                    Ruta_Almacenar = Crear_Ruta(Negocio.P_Contrato_ID);
                    Ruta_Almacenar = Ruta_Almacenar + "\\";

                    if (Copiar_Archivo(Ruta_Almacenar, Negocio.P_Archivo) == false)
                    {
                        Respuesta = "Fallo al copiar el archivo";
                        return Respuesta;
                    }

                    if (Registrar_Archivo(Negocio.P_Archivo, Negocio.P_Contrato_ID, Ruta_Almacenar) == false)
                    {
                        Respuesta = "Fallo al registrar el archivo";
                        return Respuesta;
                    }

                    Respuesta = "bien";
                }

                return Respuesta;
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Ruta
            ///DESCRIPCIÓN          : Guarda el documento en una carpeta de acuerdo al id del convenio
            ///PARAMETROS           : Negocio: conexion con la capa de negocios
            ///CREO                 : Sergio Pacheco
            ///FECHA_CREO           : 
            ///MODIFICO             : Armando Zavala Moreno
            ///FECHA_MODIFICO       : 18/Septiembre/2012
            ///CAUSA_MODIFICACIÓN   : Se adapato al modulo de Contabilidad
            ///*******************************************************************************
            private static string Crear_Ruta(String Contrato_ID)
            {
                String Ruta = "";
                DirectoryInfo Ruta_Crear;

                Ruta_Crear = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Archivos/Contabilidad/Contratos/" + Contrato_ID));
                if (!Ruta_Crear.Exists)
                {
                    Ruta_Crear.Create();
                }

                Ruta = Ruta_Crear.ToString();
                return Ruta;
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Copiar_Archivo
            ///DESCRIPCIÓN          : Guarda el documento en una carpeta de acuerdo al id del convenio
            ///PARAMETROS           : Negocio: conexion con la capa de negocios
            ///CREO                 : Sergio Pacheco
            ///FECHA_CREO           : 
            ///MODIFICO             : Armando Zavala Moreno
            ///FECHA_MODIFICO       : 18/Septiembre/2012
            ///CAUSA_MODIFICACIÓN   : Se adapato al modulo de Contabilidad
            ///*******************************************************************************
            private static Boolean Copiar_Archivo(String Ruta_Almacenar, String Nombre_Archivo)
            {
                Boolean respuesta = false;
                String ruta_definitiva;
                String ruta_local;

                try
                {
                    ruta_definitiva = Ruta_Almacenar + Nombre_Archivo;
                    ruta_local = HttpContext.Current.Server.MapPath("~/Temporal/") + Nombre_Archivo;

                    // si no existe en el archivo
                    if (!File.Exists(ruta_definitiva))
                    {
                        File.Copy(ruta_local, ruta_definitiva);

                    }
                    respuesta = true;
                }
                catch (Exception e)
                {
                    return false;
                }
                return respuesta;
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Registrar_Archivo
            ///DESCRIPCIÓN          : Guarda el documento en una carpeta de acuerdo al id del convenio
            ///PARAMETROS           : Negocio: conexion con la capa de negocios
            ///CREO                 : Sergio Pacheco
            ///FECHA_CREO           : 
            ///MODIFICO             : Armando Zavala Moreno
            ///FECHA_MODIFICO       : 18/Septiembre/2012
            ///CAUSA_MODIFICACIÓN   : Se adapato al modulo de Contabilidad
            ///*******************************************************************************
            private static Boolean Registrar_Archivo(String Archivo, String Contrato_ID, String Ruta_almacenar)
            {
                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
                Boolean Operacion_Completa = false;//Estado de la operacion.
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;

                try
                {
                    Mi_SQL.Append("UPDATE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + " SET ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Archivo + " = '" + Archivo + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Ruta_Archivo + " = '" + Ruta_almacenar + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado.ToUpper() + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Modifico + " = GETDATE() ");
                    Mi_SQL.Append(" WHERE " + Cat_Con_Contratos_Det.Campo_Contrato_ID + " = '" + Contrato_ID + "'");

                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();
                    Trans.Commit();
                    Operacion_Completa = true;
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al ejecutar los la modificación de los contratos. Error: [" + Ex.Message + "]");
                }
                return Operacion_Completa;
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Saldo_Disponible
            ///DESCRIPCIÓN          : consulta para obtener los datos de los proveedores
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 03/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static Double Saldo_Disponible(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                DataTable Dt_Consulta = new DataTable();
                Double Saldo_Disponible = 0.00;
                try
                {
                    Mi_Sql.Append("SELECT ISNULL((P." + Cat_Sap_Proyectos_Programas.Campo_Importe + "- P.");
                    Mi_Sql.Append("Saldo),0)SALDO_DISPONIBLE");
                    Mi_Sql.Append(" FROM " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + " CD");
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " P");
                    Mi_Sql.Append(" ON CD." + Cat_Con_Contratos_Det.Campo_Proyecto_Programa_ID + "=P." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);

                    if (!String.IsNullOrEmpty(Negocio.P_Contrato_ID))
                    {
                        Mi_Sql.Append(" WHERE CD." + Cat_Con_Contratos_Det.Campo_Contrato_ID + "= '" + Negocio.P_Contrato_ID.Trim() + "'");
                    }

                    Dt_Consulta = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                    Saldo_Disponible = Convert.ToDouble(Dt_Consulta.Rows[0][0]);
                    return Saldo_Disponible;
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los proveedores. Error: [" + Ex.Message + "]");
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Contratos_Saldo_Ampliacion
            ///DESCRIPCIÓN          : consulta para modificar los datos de los contratos
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios
            ///CREO                 : Armando Zavala
            ///FECHA_CREO           : 03/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static Boolean Modificar_Contratos_Saldo_Ampliacion(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
                Boolean Operacion_Completa = false;//Estado de la operacion.
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                DataTable Dt_Det_Ant = new DataTable();
                DataTable Dt_Reserva = new DataTable();
                Int32 No_Reserva;
                Double Restante = 0;
                String Estatus = String.Empty;
                DataTable Dt = new DataTable();
                Double Saldo = 0.00;//Para actualizar el saldo del programa
                DataTable Dt_Saldo = new DataTable();
                DataTable Dt_Reserva_Detalles = new DataTable();
                Double Disponible;
                Boolean Terminado = false;
                DataTable Dt_Registros_Ampliados = new DataTable();
                DataTable Dt_Contratos_Detalles = new DataTable();
                String Respuesta = "";
                String Ruta_Almacenar="";
                try
                {
                    Dt = Negocio.P_Dt_Detalles;
                    //INSERTAMOS LOS DATOS DEL CONTRATO
                    Mi_SQL.Append("UPDATE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + " SET ");

                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Ini))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Inicio + " = '" + Negocio.P_Fecha_Ini.Trim() + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Fin))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Fin + " = '" + Negocio.P_Fecha_Fin.Trim() + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Importe_Total))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Importe_Total + " = " + Negocio.P_Importe_Total.Trim().Replace(",", "") + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Saldo_Total))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Saldo_Total + " = " + Negocio.P_Saldo_Total.Trim().Replace(",", "") + ", ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Inicial_Original))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Inicial_Original + " = '" + Negocio.P_Fecha_Inicial_Original.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Final_Original))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Final_Original + " = '" + Negocio.P_Fecha_Final_Original.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Importe_Original))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Importe_Original + " = '" + Negocio.P_Importe_Original.Trim().Replace(",", "") + "', ");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Motivo))
                    {
                        Mi_SQL.Append(Cat_Con_Contratos.Campo_Motivo + " = '" + Negocio.P_Motivo.Trim().Replace(",", "") + "', ");
                    }
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico.Trim() + "', ");
                    Mi_SQL.Append(Cat_Con_Contratos.Campo_Fecha_Modifico + " = GETDATE() ");
                    Mi_SQL.Append(" WHERE " + Cat_Con_Contratos_Det.Campo_Contrato_ID + " = '" + Negocio.P_Contrato_ID.Trim() + "'");

                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();

                    if (!String.IsNullOrEmpty(Negocio.P_Archivo))
                    {
                        Ruta_Almacenar = Crear_Ruta(Negocio.P_Contrato_ID);
                        Ruta_Almacenar = Ruta_Almacenar + "\\";
                        Copiar_Archivo(Ruta_Almacenar, Negocio.P_Archivo);
                    }
                    //Se registra la Ampliacion del comtrato
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Cat_Con_Contratos_Ampliaciones.Tabla_Cat_Con_Contratos_Ampliaciones);
                    Mi_SQL.Append(" (" + Cat_Con_Contratos_Ampliaciones.Campo_Contrato_ID);
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Ini))
                    {
                        Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Fecha_Amp_Inicio);
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Fin))
                    {
                        Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Fecha_Amp_Fin);
                    }
                    Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Importe);
                    Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Motivo);
                    Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Archivo);
                    Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Ruta_Archivo);
                    Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Usuario_Creo);
                    Mi_SQL.Append(", " + Cat_Con_Contratos_Ampliaciones.Campo_Fecha_Creo);
                    Mi_SQL.Append(") VALUES('");
                    Mi_SQL.Append(Negocio.P_Contrato_ID.Trim()+"'");
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Ini))
                    {
                        Mi_SQL.Append(",'" + Negocio.P_Fecha_Ini.Trim() + "'");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Fin))
                    {
                        Mi_SQL.Append(",'" + Negocio.P_Fecha_Fin.Trim() + "'");
                    }
                    Mi_SQL.Append("," + Negocio.P_Ampliacion_Monto + "");
                    Mi_SQL.Append(",'" + Negocio.P_Motivo.Trim() + "'");
                    Mi_SQL.Append(",'" + Negocio.P_Archivo + "'");
                    Mi_SQL.Append(",'" + Ruta_Almacenar + "'");
                    Mi_SQL.Append(",'" + Cls_Sessiones.Nombre_Empleado + "'");
                    Mi_SQL.Append(",GETDATE())");

                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();


                    //obtenmos el numero de reserva del contrato
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT " + Cat_Con_Contratos.Campo_No_Reserva + " FROM " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos);
                    Mi_SQL.Append(" WHERE " + Cat_Con_Contratos_Det.Campo_Contrato_ID + " = '" + Negocio.P_Contrato_ID.Trim() + "'");

                    Dt_Reserva = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                    if (Dt_Reserva != null && Dt_Reserva.Rows.Count > 0)
                    {
                        No_Reserva = Convert.ToInt32(Dt_Reserva.Rows[0][Cat_Con_Contratos.Campo_No_Reserva]);

                        //modificamos los datos de la reserva
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ");
                        Mi_SQL.Append(Ope_Psp_Reservas.Campo_Estatus + " = 'GENERADA', ");
                        Mi_SQL.Append(Ope_Psp_Reservas.Campo_Fecha_Modifico + " = GETDATE(), ");
                        Mi_SQL.Append(Ope_Psp_Reservas.Campo_Usuario_Modifico + " = '"+Cls_Sessiones.Nombre_Empleado.ToString()+"', ");
                        //if (Negocio.P_Dt_Detalles != null && Negocio.P_Dt_Detalles.Rows.Count > 0)
                        //{
                        //    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Dependencia_ID + " = '" + Dt.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString().Trim() + "', ");
                        //}
                        Mi_SQL.Append(Ope_Psp_Reservas.Campo_Importe_Inicial + " = " + Ope_Psp_Reservas.Campo_Importe_Inicial +" + "+ Negocio.P_Ampliacion_Monto.Trim().Replace(",", "") + ", ");
                        Mi_SQL.Append(Ope_Psp_Reservas.Campo_Saldo + " = " + Ope_Psp_Reservas.Campo_Saldo + " + " + Negocio.P_Ampliacion_Monto.Trim().Replace(",", "") + "");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva);

                        Cmd.CommandText = Mi_SQL.ToString();
                        Cmd.ExecuteNonQuery();

                        if (Dt_Registros_Ampliados.Rows.Count == 0)
                        {
                            //Agrega los campos que va a contener el DataTable
                            Dt_Registros_Ampliados.Columns.Add(Cat_Con_Contratos_Det.Campo_Dependencia_ID, typeof(System.String));
                            Dt_Registros_Ampliados.Columns.Add(Cat_Con_Contratos_Det.Campo_Fuente_Financiamiento_ID, typeof(System.String));
                            Dt_Registros_Ampliados.Columns.Add(Cat_Con_Contratos_Det.Campo_Proyecto_Programa_ID, typeof(System.String));
                            Dt_Registros_Ampliados.Columns.Add(Cat_Con_Contratos_Det.Campo_Partida_ID, typeof(System.String));
                            Dt_Registros_Ampliados.Columns.Add(Cat_Con_Contratos_Det.Campo_Importe, typeof(System.String));
                        }
                        //CONSULTAR DETALLES DE LA RESERVA y LA ACTUALIZACION DEL SALDO
                #region(Momentos Presupuestales)
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("SELECT " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + ".*," + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID + ", " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio);
                        Mi_SQL.Append(" FROM  " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                        Mi_SQL.Append(" LEFT OUTER JOIN   " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " ON " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + "=");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + No_Reserva);
                        Dt_Reserva_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                        if (Dt_Reserva_Detalles.Rows.Count > 0)
                        {
                            Disponible = 0;
                            Terminado = false;
                            Restante = Convert.ToDouble(Negocio.P_Ampliacion_Monto);
                            foreach (DataRow Fila in Dt_Reserva_Detalles.Rows)
                            {
                                if (!Terminado)
                                {
                                    Disponible = 0;
                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT  " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Fila[Ope_Psp_Reservas.Campo_Dependencia_ID].ToString().Trim() + "'");
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim() + "'");
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim() + "'");
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString().Trim() + "'");
                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Fila[Ope_Psp_Reservas.Campo_Anio].ToString().Trim());
                                    Disponible = Convert.ToDouble(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                                    if (Disponible >= Restante)
                                    {
                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                        Mi_SQL.Append(" SET DISPONIBLE = DISPONIBLE - " + Restante + ", ");
                                        Mi_SQL.Append(" PRE_COMPROMETIDO = PRE_COMPROMETIDO + " + Restante + ", ");
                                        Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado.ToString()+ "', ");
                                        Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Fila[Ope_Psp_Reservas.Campo_Dependencia_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Fila[Ope_Psp_Reservas.Campo_Anio].ToString().Trim());
                                        Cmd.CommandText = Mi_SQL.ToString();
                                        Cmd.ExecuteNonQuery();

                                        //Agregamos el registro que si se afecto 
                                        DataRow row = Dt_Registros_Ampliados.NewRow(); //Crea un nuevo registro a la tabla
                                        //Asigna los valores al nuevo registro creado a la tabla
                                        row[Cat_Con_Contratos_Det.Campo_Dependencia_ID] = Fila[Ope_Psp_Reservas.Campo_Dependencia_ID].ToString().Trim();
                                        row[Cat_Con_Contratos_Det.Campo_Fuente_Financiamiento_ID] = Fila[Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                                        row[Cat_Con_Contratos_Det.Campo_Partida_ID] = Fila[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString().Trim();
                                        row[Cat_Con_Contratos_Det.Campo_Proyecto_Programa_ID] = Fila[Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                                        row[Cat_Con_Contratos_Det.Campo_Importe] = Restante;
                                        Dt_Registros_Ampliados.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                        Dt_Registros_Ampliados.AcceptChanges();

                                        //Se registra el movimiento del presupuesto
                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("INSERT INTO " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                        Mi_SQL.Append(" (" + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Cargo);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Abono);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Importe);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Fecha);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Usuario);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Usuario_Creo);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Fecha_Creo);
                                        Mi_SQL.Append(") VALUES(");
                                        Mi_SQL.Append(No_Reserva);
                                        Mi_SQL.Append(",'PRE_COMPROMETIDO'");
                                        Mi_SQL.Append(",'DISPONIBLE'");
                                        Mi_SQL.Append(",'" + Convert.ToString(Restante) + "'");
                                        Mi_SQL.Append(",GETDATE()");
                                        Mi_SQL.Append(",'" + Cls_Sessiones.Nombre_Empleado + "'");
                                        Mi_SQL.Append(",'" + Cls_Sessiones.Nombre_Empleado + "'");
                                        Mi_SQL.Append(",GETDATE())");
                                        Cmd.CommandText = Mi_SQL.ToString();
                                        Cmd.ExecuteNonQuery();


                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                        Mi_SQL.Append(" SET PRE_COMPROMETIDO = PRE_COMPROMETIDO - " + Restante + ", ");
                                        Mi_SQL.Append(" COMPROMETIDO = COMPROMETIDO + " + Restante + ", ");
                                        Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado.ToString() + "', ");
                                        Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Fila[Ope_Psp_Reservas.Campo_Dependencia_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Fila[Ope_Psp_Reservas.Campo_Anio].ToString().Trim());
                                        Cmd.CommandText = Mi_SQL.ToString();
                                        Cmd.ExecuteNonQuery();


                                        //Se registra el movimiento del presupuesto
                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("INSERT INTO " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                        Mi_SQL.Append(" (" + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Cargo);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Abono);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Importe);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Fecha);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Usuario);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Usuario_Creo);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Fecha_Creo);
                                        Mi_SQL.Append(") VALUES(");
                                        Mi_SQL.Append(No_Reserva);
                                        Mi_SQL.Append(",'COMPROMETIDO'");
                                        Mi_SQL.Append(",'PRE_COMPROMETIDO'");
                                        Mi_SQL.Append(",'" + Convert.ToString(Restante) + "'");
                                        Mi_SQL.Append(",GETDATE()");
                                        Mi_SQL.Append(",'" + Cls_Sessiones.Nombre_Empleado + "'");
                                        Mi_SQL.Append(",'" + Cls_Sessiones.Nombre_Empleado + "'");
                                        Mi_SQL.Append(",GETDATE())");
                                        Cmd.CommandText = Mi_SQL.ToString();
                                        Cmd.ExecuteNonQuery();



                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                        Mi_SQL.Append(" SET AFECTADO_" + String.Format("{0:MMMM}", DateTime.Now) + " = AFECTADO_" + String.Format("{0:MMMM}", DateTime.Now) + "+" + Restante);
                                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Fila[Ope_Psp_Reservas.Campo_Dependencia_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Fila[Ope_Psp_Reservas.Campo_Anio].ToString().Trim());
                                        Cmd.CommandText = Mi_SQL.ToString();
                                        Cmd.ExecuteNonQuery();
                                        Terminado = true;
                                    }
                                    else
                                    {


                                        Restante = Restante - Disponible;
                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                        Mi_SQL.Append(" SET DISPONIBLE = DISPONIBLE + " + Disponible + ", ");
                                        Mi_SQL.Append(" PRE_COMPROMETIDO = PRE_COMPROMETIDO - " + Disponible + ", ");
                                        Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado.ToString() + "', ");
                                        Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Fila[Ope_Psp_Reservas.Campo_Dependencia_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Fila[Ope_Psp_Reservas.Campo_Anio].ToString().Trim());
                                        Cmd.CommandText = Mi_SQL.ToString();
                                        Cmd.ExecuteNonQuery();


                                        //Agregamos el registro que si se afecto 
                                        DataRow row = Dt_Registros_Ampliados.NewRow(); //Crea un nuevo registro a la tabla
                                        //Asigna los valores al nuevo registro creado a la tabla
                                        row[Cat_Con_Contratos_Det.Campo_Dependencia_ID] = Fila[Ope_Psp_Reservas.Campo_Dependencia_ID].ToString().Trim();
                                        row[Cat_Con_Contratos_Det.Campo_Fuente_Financiamiento_ID] = Fila[Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                                        row[Cat_Con_Contratos_Det.Campo_Partida_ID] = Fila[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString().Trim();
                                        row[Cat_Con_Contratos_Det.Campo_Proyecto_Programa_ID] = Fila[Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                                        row[Cat_Con_Contratos_Det.Campo_Importe] = Disponible;
                                        Dt_Registros_Ampliados.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                        Dt_Registros_Ampliados.AcceptChanges();


                                        //Se registra el movimiento del presupuesto
                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("INSERT INTO " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                        Mi_SQL.Append(" (" + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Cargo);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Abono);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Importe);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Fecha);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Usuario);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Usuario_Creo);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Fecha_Creo);
                                        Mi_SQL.Append(") VALUES(");
                                        Mi_SQL.Append(No_Reserva);
                                        Mi_SQL.Append(",'PRE_COMPROMETIDO'");
                                        Mi_SQL.Append(",'DISPONIBLE'");
                                        Mi_SQL.Append(",'" + Convert.ToString(Disponible) + "'");
                                        Mi_SQL.Append(",GETDATE()");
                                        Mi_SQL.Append(",'" + Cls_Sessiones.Nombre_Empleado + "'");
                                        Mi_SQL.Append(",'" + Cls_Sessiones.Nombre_Empleado + "'");
                                        Mi_SQL.Append(",GETDATE())");
                                        Cmd.CommandText = Mi_SQL.ToString();
                                        Cmd.ExecuteNonQuery();

                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                        Mi_SQL.Append(" SET AFECTADO_" + String.Format("{0:MMMM}", DateTime.Now) + " = AFECTADO_" + String.Format("{0:MMMM}", DateTime.Now) + "+" + Disponible);
                                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Fila[Ope_Psp_Reservas.Campo_Dependencia_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Fila[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString().Trim() + "'");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Fila[Ope_Psp_Reservas.Campo_Anio].ToString().Trim());
                                        Cmd.CommandText = Mi_SQL.ToString();
                                        Cmd.ExecuteNonQuery();


                                        //Se registra el movimiento del presupuesto
                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("INSERT INTO " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                                        Mi_SQL.Append(" (" + Ope_Psp_Registro_Movimientos.Campo_No_Reserva);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Cargo);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Abono);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Importe);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Fecha);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Usuario);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Usuario_Creo);
                                        Mi_SQL.Append(", " + Ope_Psp_Registro_Movimientos.Campo_Fecha_Creo);
                                        Mi_SQL.Append(") VALUES(");
                                        Mi_SQL.Append(No_Reserva);
                                        Mi_SQL.Append(",'COMPROMETIDO'");
                                        Mi_SQL.Append(",'PRE_COMPROMETIDO'");
                                        Mi_SQL.Append(",'" + Convert.ToString(Disponible) + "'");
                                        Mi_SQL.Append(",GETDATE()");
                                        Mi_SQL.Append(",'" + Cls_Sessiones.Nombre_Empleado + "'");
                                        Mi_SQL.Append(",'" + Cls_Sessiones.Nombre_Empleado + "'");
                                        Mi_SQL.Append(",GETDATE())");
                                        Cmd.CommandText = Mi_SQL.ToString();
                                        Cmd.ExecuteNonQuery();

                                        if (Restante <= 0.00001)
                                        {
                                            Terminado = true;
                                        }
                                    }
                                }
                            }
                        }
                #endregion
                        //CONSULTAR LOS DETALLES DEL CONTRATO 
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("SELECT *FROM " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det);
                        Mi_SQL.Append(" WHERE " + Cat_Con_Contratos_Det.Campo_Contrato_ID + " = '" + Negocio.P_Contrato_ID.Trim() + "'");
                        Dt_Contratos_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                        if (Dt_Registros_Ampliados.Rows.Count > 0 && Dt_Contratos_Detalles.Rows.Count>0)
                        {

                            foreach (DataRow F1 in Dt_Registros_Ampliados.Rows)
                            {
                                //Actualizamos los detalles del contrato 
                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("UPDATE " + Cat_Con_Contratos_Det.Tabla_Cat_Con_Contratos_Det + " SET ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Importe + " = " + Cat_Con_Contratos_Det.Campo_Importe + "+" + F1[Cat_Con_Contratos_Det.Campo_Importe].ToString().Trim());
                                Mi_SQL.Append(" WHERE ");
                                Mi_SQL.Append(Cat_Con_Contratos_Det.Campo_Dependencia_ID + " = '" + F1[Cat_Con_Contratos_Det.Campo_Dependencia_ID].ToString().Trim() + "'");
                                Mi_SQL.Append(" AND " + Cat_Con_Contratos_Det.Campo_Fuente_Financiamiento_ID + " = '" + F1[Cat_Con_Contratos_Det.Campo_Fuente_Financiamiento_ID].ToString().Trim() + "'");
                                Mi_SQL.Append(" AND " + Cat_Con_Contratos_Det.Campo_Proyecto_Programa_ID + " = '" + F1[Cat_Con_Contratos_Det.Campo_Proyecto_Programa_ID].ToString().Trim() + "'");
                                Mi_SQL.Append(" AND " + Cat_Con_Contratos_Det.Campo_Partida_ID + " = '" + F1[Cat_Con_Contratos_Det.Campo_Partida_ID].ToString().Trim() + "'");
                                Cmd.CommandText = Mi_SQL.ToString();
                                Cmd.ExecuteNonQuery();
                            }
                        }
                    }
                    //Actualiza el Saldo del progrma
                    if (Dt_Registros_Ampliados.Rows.Count>0 && !String.IsNullOrEmpty(Negocio.P_Ampliacion_Monto) && !String.IsNullOrEmpty(Negocio.P_Importe_Total))
                    {
                        Dt_Saldo = Consulta_Saldo(Dt_Registros_Ampliados.Rows[0][Cat_Con_Contratos_Det.Campo_Proyecto_Programa_ID].ToString());
                        if (Dt_Saldo != null && Dt_Saldo.Rows.Count > 0)
                        {
                            Saldo = Convert.ToDouble(Dt_Saldo.Rows[0][0].ToString()) + Convert.ToDouble(Negocio.P_Importe_Total) - Convert.ToDouble(Negocio.P_Ampliacion_Monto);
                            Saldo -= Convert.ToDouble(Negocio.P_Importe_Total);
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("UPDATE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                            Mi_SQL.Append(" SET Saldo = '" + Saldo.ToString() + "'");
                            Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Dt_Registros_Ampliados.Rows[0][Cat_Con_Contratos_Det.Campo_Proyecto_Programa_ID].ToString() + "'");
                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();
                        }
                    }
                    Trans.Commit();
                    Operacion_Completa = true;

                    return Operacion_Completa;
                }
                catch (SqlException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    Cn.Close();
                }
            }
          ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Acciones_Programa_Convenio
            ///DESCRIPCIÓN          : consulta para obtener las acciones ligadas al programa y al convenio
            ///PARAMETROS           : 
            ///CREO                 :Sergio Manuel Gallardo Andrade
            ///FECHA_CREO           : 14/Septiembre/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Acciones_Programa_Convenio(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Cat_Con_Acciones.Tabla_Cat_Con_Acciones + "." + Cat_Con_Acciones.Campo_Accion_Id + "+' - '+");
                    Mi_Sql.Append(Cat_Con_Acciones.Tabla_Cat_Con_Acciones +"."+ Cat_Con_Acciones.Campo_Descripcion + " as DESCRIPCION ,");
                    Mi_Sql.Append(Cat_Con_Acciones.Tabla_Cat_Con_Acciones + "." + Cat_Con_Acciones.Campo_Accion_Id);
                    Mi_Sql.Append(" FROM " + Cat_Con_Acciones.Tabla_Cat_Con_Acciones);
                    Mi_Sql.Append(" LEFT OUTER JOIN "+ Cat_Con_Programas_Acciones.Tabla_Cat_Con_Programas_Acciones +" ON ");
                    Mi_Sql.Append(Cat_Con_Acciones.Tabla_Cat_Con_Acciones +"."+ Cat_Con_Acciones.Campo_Accion_Id+"="+Cat_Con_Programas_Acciones.Tabla_Cat_Con_Programas_Acciones +".");
                    Mi_Sql.Append(Cat_Con_Programas_Acciones.Campo_Accion_ID);
                    Mi_Sql.Append(" WHERE " + Cat_Con_Programas_Acciones.Tabla_Cat_Con_Programas_Acciones + "." + Cat_Con_Programas_Acciones.Campo_Proyecto_Programa_ID + " = '"+Negocio.P_Programa_ID+"'");
                    Mi_Sql.Append(" AND " + Cat_Con_Programas_Acciones.Tabla_Cat_Con_Programas_Acciones + "." + Cat_Con_Programas_Acciones.Campo_Convenio_ID + " = '" + Negocio.P_Convenio_ID+ "'");
                    if(!String.IsNullOrEmpty(Negocio.P_Accion_ID))
                    {
                        Mi_Sql.Append(" AND " + Cat_Con_Programas_Acciones.Tabla_Cat_Con_Programas_Acciones + "." + Cat_Con_Programas_Acciones.Campo_Accion_ID + " = '" + Negocio.P_Accion_ID + "'");
                    }

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los proveedores. Error: [" + Ex.Message + "]");
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Contratos_Acciones
            ///DESCRIPCIÓN          : consulta para obtener los datos de los detalles de los contratos 
            ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
            ///CREO                 : Sergio Manuel Gallardo Andrade
            ///FECHA_CREO           : 17/Septiembre/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Contratos_Acciones(Cls_Con_Cat_Contratos_Negocios Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    Mi_Sql.Append("SELECT " + Cat_Con_Acciones.Tabla_Cat_Con_Acciones + "." + Cat_Con_Acciones.Campo_Accion_Id + "+' - '+ ");
                    Mi_Sql.Append(Cat_Con_Acciones.Tabla_Cat_Con_Acciones + "." + Cat_Con_Acciones.Campo_Descripcion + " AS DESCRIPCION , ");
                    Mi_Sql.Append(Cat_Con_Acciones.Tabla_Cat_Con_Acciones + "." + Cat_Con_Acciones.Campo_Accion_Id + " FROM  ");
                    Mi_Sql.Append(Cat_Con_Contratos_Acciones.Tabla_Cat_Con_Contratos_Acciones);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Con_Acciones.Tabla_Cat_Con_Acciones + " ON " + Cat_Con_Acciones.Tabla_Cat_Con_Acciones + "." + Cat_Con_Acciones.Campo_Accion_Id + "=");
                    Mi_Sql.Append(Cat_Con_Contratos_Acciones.Tabla_Cat_Con_Contratos_Acciones + "." + Cat_Con_Contratos_Acciones.Campo_Accion_ID);
                    Mi_Sql.Append(" WHERE " + Cat_Con_Contratos_Acciones.Tabla_Cat_Con_Contratos_Acciones + "." + Cat_Con_Contratos_Acciones.Campo_Contrato_ID + "='" + Negocio.P_Contrato_ID + "'");
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los detalles de los contratos. Error: [" + Ex.Message + "]");
                }
            }
        #endregion
    }
}
