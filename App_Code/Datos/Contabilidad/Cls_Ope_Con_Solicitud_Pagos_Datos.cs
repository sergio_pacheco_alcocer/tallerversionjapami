﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Sessiones;
using JAPAMI.Polizas.Datos;
namespace JAPAMI.Solicitud_Pagos.Datos
{
    public class Cls_Ope_Con_Solicitud_Pagos_Datos
    {
        #region (Métodos Operación)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Alta_Solicitud_Pago
            /// DESCRIPCION : 1.Consulta el último No dado de alta para poder ingresar el siguiente
            ///               2. Da de Alta La Solictud de Pago en la BD con los datos 
            ///                  proporcionados por elusuario
            /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 17-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static String Alta_Solicitud_Pago(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL_2 = new StringBuilder();
                String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
                StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos
                Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
                Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
                Object Saldo;                                    //Obtiene el saldo de la cuenta contable     
                Object Total_Presupuesto=null;
                DataTable Dt_Partidas = new DataTable();     // se obtiene los detalles de la partida 
                int Registros_Actualizados = 0;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    //Consulta para la obtención del último ID dado de alta 
                    Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                    Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00003'");
                    //Mi_SQL.Append(" ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    No_Poliza = Cmmd.ExecuteScalar();
                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(No_Poliza))
                    {
                        No_Poliza = "0000000001";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                    }
                    Mi_SQL.Length = 0;
                    //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                    Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL.Append(" (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", ");
                    Mi_SQL.Append(Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + ")");
                    Mi_SQL.Append(" VALUES ('" + No_Poliza + "', '00003', '" + Mes_Anio + "',");
                    Mi_SQL.Append(" '" + String.Format("{0:dd/MM/yyyy}", DateTime.Today) + "',");
                    Mi_SQL.Append(" '" + Datos.P_Concepto + "', " + Datos.P_Monto + ", " + Datos.P_Monto + ","+Datos.P_Partida+ ", ");
                    Mi_SQL.Append("'" + Datos.P_Nombre_Usuario + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', ");
                    Mi_SQL.Append("'" + Cls_Sessiones.Empleado_ID + "')");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                    //Da de alta los detalles de la póliza
                    foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                    {
                        Mi_SQL.Length = 0;
                        //consulta el saldo de la cuenta contable
                        Mi_SQL.Append("SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - ");
                        Mi_SQL.Append(" ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                        Saldo = Cmmd.ExecuteScalar();
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                        }
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                        }

                        Mi_SQL.Length = 0;
                        //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                        Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                        Consecutivo = Cmmd.ExecuteScalar();

                        //Valida si el ID es nulo para asignarle automaticamente el primer registro
                        if (Convert.IsDBNull(Consecutivo))
                        {
                            Consecutivo = "1";
                        }
                        //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                        else
                        {
                            Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                        }
                        Mi_SQL.Length = 0;
                        //Inserta el registro del detalle de la póliza en la base de datos
                        Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Mi_SQL.Append("(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", Beneficiario_ID, Tipo_Beneficiario, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Consecutivo + ")");
                        Mi_SQL.Append(" VALUES('" + No_Poliza + "', '00003', '" + Mes_Anio + "', ");
                        Mi_SQL.Append(Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + ",");
                        Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "',");
                        Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString() + "', ");
                        Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", ");
                        Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", ");
                        Mi_SQL.Append("'" + Renglon["Beneficiario_ID"].ToString() + "','" + Renglon["Tipo_Beneficiario"].ToString() + "', ");
                        Mi_SQL.Append(Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ")");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos            
                    }
                    Mi_SQL.Length = 0;
                    // se agrega el no_poliza y el mes_ano a la solicitud de pago
                    Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " ='" + No_Poliza + "', ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00003', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos
                    
                    //Transaccion_SQL.Commit();             //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos                
                    
                    //Optenemos el total de la reserva con los impuestos que se disminuyeron
                    Mi_SQL_2.Append("SELECT " + Ope_Con_Polizas.Campo_Total_Debe);
                    Mi_SQL_2.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas );
                    Mi_SQL_2.Append(" WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " ='" + No_Poliza + "'");
                    Mi_SQL_2.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " ='00003'");
                    Mi_SQL_2.Append(" AND " + Ope_Con_Polizas.Campo_Mes_Ano + " ='" + Mes_Anio + "'");
                    Cmmd.CommandText = Mi_SQL_2.ToString(); //Realiza la ejecución de la obtención del consecutivo
                    Total_Presupuesto = Cmmd.ExecuteScalar();

                    // Total_Presupuesto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL_2.ToString());
                    if (!String.IsNullOrEmpty(Datos.P_Servicios_Generales))
                    {
                        if (Datos.P_Servicios_Generales == "SI")
                        {
                            Dt_Partidas = Consultar_detalles_partidas_de_solicitud_Servicios_Generales(Datos.P_No_Solicitud_Pago, Cmmd);
                        }
                        else
                        {
                            Dt_Partidas = Consultar_detalles_partidas_de_solicitud(Datos.P_No_Solicitud_Pago, Cmmd);
                        }
                    }
                    else
                    {
                        Dt_Partidas = Consultar_detalles_partidas_de_solicitud(Datos.P_No_Solicitud_Pago, Cmmd);
                    }
                    
                    Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza,Cmmd);
                    Registros_Actualizados=Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DEVENGADO", "COMPROMETIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal

                    if (Registros_Actualizados > 0)
                    {
                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "DEVENGADO", "COMPROMETIDO", Convert.ToDouble(Datos.P_Monto), Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                        if (Datos.P_Cmmd == null)
                        {
                            Trans.Commit();
                        }
                        return "SI";
                    }
                    else
                    {
                        if (Datos.P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        return "NO";
                    }
                }
                catch (SqlException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Alta_Poliza_Finiquito
            /// DESCRIPCION : 1.Consulta el último No dado de alta para poder ingresar el siguiente
            ///               2. Da de Alta La Solictud de Pago en la BD con los datos 
            ///                  proporcionados por elusuario
            /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 14-Agosto-2013
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static String Alta_Poliza_Finiquito(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL_2 = new StringBuilder();
                String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
                StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos
                Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
                Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
                Object Saldo;                                    //Obtiene el saldo de la cuenta contable     
                Object Total_Presupuesto = null;
                Object Reserva = null;
                DataTable Dt_Partidas = new DataTable();     // se obtiene los detalles de la partida 
                int Registros_Actualizados = 0;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    //Consulta para la obtención del último ID dado de alta 
                    Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                    Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00003'");
                    //Mi_SQL.Append(" ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    No_Poliza = Cmmd.ExecuteScalar();
                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(No_Poliza))
                    {
                        No_Poliza = "0000000001";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                    }
                    Mi_SQL.Length = 0;
                    //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                    Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL.Append(" (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", ");
                    Mi_SQL.Append(Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + ")");
                    Mi_SQL.Append(" VALUES ('" + No_Poliza + "', '00003', '" + Mes_Anio + "',");
                    Mi_SQL.Append(" '" + String.Format("{0:dd/MM/yyyy}", DateTime.Today) + "',");
                    Mi_SQL.Append(" '" + Datos.P_Concepto + "', " + Datos.P_Monto + ", " + Datos.P_Monto + "," + Datos.P_Partida + ", ");
                    Mi_SQL.Append("'" + Datos.P_Nombre_Usuario + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', ");
                    Mi_SQL.Append("'" + Cls_Sessiones.Empleado_ID + "')");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                    //Da de alta los detalles de la póliza
                    foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                    {
                        Mi_SQL.Length = 0;
                        //consulta el saldo de la cuenta contable
                        Mi_SQL.Append("SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - ");
                        Mi_SQL.Append(" ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                        Saldo = Cmmd.ExecuteScalar();
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                        }
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                        }

                        Mi_SQL.Length = 0;
                        //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                        Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                        Consecutivo = Cmmd.ExecuteScalar();

                        //Valida si el ID es nulo para asignarle automaticamente el primer registro
                        if (Convert.IsDBNull(Consecutivo))
                        {
                            Consecutivo = "1";
                        }
                        //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                        else
                        {
                            Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                        }
                        Mi_SQL.Length = 0;
                        //Inserta el registro del detalle de la póliza en la base de datos
                        Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Mi_SQL.Append("(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", Beneficiario_ID, Tipo_Beneficiario, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Consecutivo + ")");
                        Mi_SQL.Append(" VALUES('" + No_Poliza + "', '00003', '" + Mes_Anio + "', ");
                        Mi_SQL.Append(Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + ",");
                        Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "',");
                        Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString() + "', ");
                        Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", ");
                        Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", ");
                        Mi_SQL.Append("'" + Renglon["Beneficiario_ID"].ToString() + "','" + Renglon["Tipo_Beneficiario"].ToString() + "', ");
                        Mi_SQL.Append(Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ")");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos            
                    }
                    Mi_SQL.Length = 0;
                    // se agrega el no_poliza y el mes_ano a la solicitud de pago
                    Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " ='" + No_Poliza + "', ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00003', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos

                    //Transaccion_SQL.Commit();             //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos                

                    //Optenemos el total de la reserva con los impuestos que se disminuyeron
                    Mi_SQL_2.Append("SELECT " + Ope_Con_Polizas.Campo_Total_Debe);
                    Mi_SQL_2.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL_2.Append(" WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " ='" + No_Poliza + "'");
                    Mi_SQL_2.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " ='00003'");
                    Mi_SQL_2.Append(" AND " + Ope_Con_Polizas.Campo_Mes_Ano + " ='" + Mes_Anio + "'");
                    Cmmd.CommandText = Mi_SQL_2.ToString(); //Realiza la ejecución de la obtención del consecutivo
                    Total_Presupuesto = Cmmd.ExecuteScalar();

                    // Total_Presupuesto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL_2.ToString());
                    Dt_Partidas = Consultar_detalles_partidas_de_solicitud_Finiquito(Datos.P_No_Solicitud_Pago, Cmmd);
                    Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza, Cmmd);
                    Registros_Actualizados = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DEVENGADO", "COMPROMETIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal

                    if (Registros_Actualizados > 0)
                    {

                        Mi_SQL_2.Length = 0;
                        Mi_SQL_2.Append("SELECT " + Ope_Con_Solicitud_Pagos.Campo_No_Reserva);
                        Mi_SQL_2.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                        Mi_SQL_2.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Datos.P_No_Solicitud_Pago + "'");
                        Cmmd.CommandText = Mi_SQL_2.ToString(); //Realiza la ejecución de la obtención del consecutivo
                        Reserva = Cmmd.ExecuteScalar();
                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Reserva.ToString(), "DEVENGADO", "COMPROMETIDO", Convert.ToDouble(Datos.P_Monto), Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                        if (Datos.P_Cmmd == null)
                        {
                            Trans.Commit();
                        }
                        return "SI";
                    }
                    else
                    {
                        if (Datos.P_Cmmd == null)
                        {
                            Trans.Rollback();
                        }
                        return "NO";
                    }
                }
                catch (SqlException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consultar_detalles_partidas_de_solicitud
            /// DESCRIPCION : 1.Consulta el último No dado de alta para poder ingresar el siguiente
            ///               2. Da de Alta La Solictud de Pago en la BD con los datos 
            ///                  proporcionados por elusuario
            /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
            /// CREO        : Sergio Manuel gallardo Andrade
            /// FECHA_CREO  : 27-02-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consultar_detalles_partidas_de_solicitud(String no_solicitud)
            {
                StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos         
                Object No_Reserva;
                Double Importe;
                Double ISR;
                Double CEDULAR;
                Double Cap;
                Double Divo;
                Double Sefupu;
                Double Icic;
                Double Rapce;
                Double LabCC;
                Double OBS;
                Double EstPro;
                Double Anticipo;
                DataTable Dt_Reserva = new DataTable();           //Obtiene la reserva a la que se pertenece la solicitud
                DataTable Dt_Reserva_Detalles = new DataTable();     // se obtiene los detalles de la partida 
                DataTable Dt_Partidas = new DataTable();     // se obtiene los detalles de la partida 
                DataTable Dt_solicitud_Detalles = new DataTable();
                DataRow Fila;
                try
                {
                   Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT No_Reserva FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + no_solicitud + "'");
                    No_Reserva = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID);
                    Mi_SQL.Append(", CAP, DIVO, SEFUPU, ICIC, RAPCE, Lab_Control_C, OBS, Est_Y_Proy, Amortizado");
                    Mi_SQL .Append(", "+Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula +", "+ Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR+ " FROM " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + " = '" + no_solicitud + "'");
                    Dt_solicitud_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    Mi_SQL.Length = 0;
                    //consultar la reserva
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + No_Reserva + "'");
                    Dt_Reserva = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    Mi_SQL.Length = 0;
                    // consultar los detalles de la reserva
                    Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = '" + Dt_Reserva.Rows[0]["No_Reserva"].ToString() + "'");
                    Dt_Reserva_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    if (Dt_Partidas != null)
                    {
                        Dt_Partidas.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("ANIO", System.Type.GetType("System.String"));
                        foreach (DataRow Registro in Dt_Reserva_Detalles.Rows)
                        {
                            Fila = Dt_Partidas.NewRow();
                            Fila["DEPENDENCIA_ID"] = Dt_Reserva.Rows[0]["DEPENDENCIA_ID"].ToString();
                            Fila["FUENTE_FINANCIAMIENTO_ID"] = Registro["FTE_FINANCIAMIENTO_ID"].ToString();
                            Fila["PROGRAMA_ID"] = Registro["PROYECTO_PROGRAMA_ID"].ToString();
                            Importe = 0;
                            ISR = 0;
                            Cap = 0;
                            Divo = 0;
                            Sefupu = 0;
                            Icic = 0;
                            Rapce = 0;
                            LabCC = 0;
                            OBS = 0;
                            EstPro = 0;
                            Anticipo = 0;
                            CEDULAR = 0;
                            foreach (DataRow Registro_2 in Dt_solicitud_Detalles.Rows)
                            {
                                if (Registro[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString() == Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString())
                                {
                                    Importe = Importe + Convert.ToDouble(Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura].ToString());
                                     if(!String.IsNullOrEmpty(Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString()))
                                    {
                                      ISR=ISR+Convert.ToDouble(Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString());
                                      CEDULAR = CEDULAR + Convert.ToDouble(Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString());
                                     }
                                    if(!String.IsNullOrEmpty(Registro_2["CAP"].ToString()))
                                    {
                                    Cap =Cap+Convert.ToDouble(Registro_2["CAP"].ToString());
                                    Divo =Divo+Convert.ToDouble(Registro_2["DIVO"].ToString());
                                    Sefupu =Sefupu+Convert.ToDouble(Registro_2["SEFUPU"].ToString());
                                    Icic =Icic+Convert.ToDouble(Registro_2["ICIC"].ToString());
                                    Rapce =Rapce+Convert.ToDouble(Registro_2["RAPCE"].ToString());
                                    LabCC =LabCC+Convert.ToDouble(Registro_2["Lab_Control_C"].ToString());
                                    OBS =OBS+Convert.ToDouble(Registro_2["OBS"].ToString());
                                    EstPro =EstPro+Convert.ToDouble(Registro_2["Est_Y_Proy"].ToString());
                                    Anticipo = Anticipo + Convert.ToDouble(Registro_2["Amortizado"].ToString());
                                    }
                                }
                            }

                            Fila["PARTIDA_ID"] = Registro[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString();
                            Fila["IMPORTE"] = Importe + ISR + CEDULAR + Cap + Divo + Sefupu + Icic + Rapce + LabCC + OBS + EstPro + Anticipo;
                            Fila["ANIO"] = Dt_Reserva.Rows[0]["ANIO"].ToString();
                            Dt_Partidas.Rows.Add(Fila);
                            Dt_Partidas.AcceptChanges();
                        }
                    }
                    return Dt_Partidas;
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consultar_detalles_partidas_de_solicitud
            /// DESCRIPCION : 1.Consulta el último No dado de alta para poder ingresar el siguiente
            ///               2. Da de Alta La Solictud de Pago en la BD con los datos 
            ///                  proporcionados por elusuario
            /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
            /// CREO        : Sergio Manuel gallardo Andrade
            /// FECHA_CREO  : 27-02-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consultar_detalles_partidas_de_solicitud(String no_solicitud, SqlCommand P_Cmmd)
            {
                StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos         
                Object No_Reserva;
                Double Importe;
                Double ISR;
                Double CEDULAR;
                Double Cap;
                Double Divo;
                Double Sefupu;
                Double Icic;
                Double Rapce;
                Double LabCC;
                Double OBS;
                Double EstPro;
                Double Anticipo;
                DataTable Dt_Reserva = new DataTable();           //Obtiene la reserva a la que se pertenece la solicitud
                DataTable Dt_Reserva_Detalles = new DataTable();     // se obtiene los detalles de la partida 
                DataTable Dt_Partidas = new DataTable();     // se obtiene los detalles de la partida 
                DataTable Dt_solicitud_Detalles = new DataTable();
                DataRow Fila;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                SqlDataAdapter Da_SQL = new SqlDataAdapter();
                DataSet Ds_SQL = new DataSet();
                if (P_Cmmd != null)
                {
                    Cmmd = P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT No_Reserva FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + no_solicitud + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    No_Reserva = Cmmd.ExecuteScalar();
                            
                    //No_Reserva = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID);
                    Mi_SQL.Append(", CAP, DIVO, SEFUPU, ICIC, RAPCE, Lab_Control_C, OBS, Est_Y_Proy, Amortizado");
                    Mi_SQL.Append(", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR + " FROM " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + " = '" + no_solicitud + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_solicitud_Detalles = Ds_SQL.Tables[0];
                    //Dt_solicitud_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    Mi_SQL.Length = 0;
                    //consultar la reserva
                    Ds_SQL = new DataSet();
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + No_Reserva + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Reserva = Ds_SQL.Tables[0];
                    //Dt_Reserva = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    Ds_SQL = new DataSet();
                    Mi_SQL.Length = 0;
                    // consultar los detalles de la reserva
                    Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = '" + Dt_Reserva.Rows[0]["No_Reserva"].ToString() + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Reserva_Detalles = Ds_SQL.Tables[0];
                    //Dt_Reserva_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    if (Dt_Partidas != null)
                    {
                        Dt_Partidas.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("ANIO", System.Type.GetType("System.String"));
                        foreach (DataRow Registro in Dt_Reserva_Detalles.Rows)
                        {
                            Fila = Dt_Partidas.NewRow();
                            Fila["DEPENDENCIA_ID"] = Dt_Reserva.Rows[0]["DEPENDENCIA_ID"].ToString();
                            Fila["FUENTE_FINANCIAMIENTO_ID"] = Registro["FTE_FINANCIAMIENTO_ID"].ToString();
                            Fila["PROGRAMA_ID"] = Registro["PROYECTO_PROGRAMA_ID"].ToString();
                            Importe = 0;
                            ISR = 0;
                            Cap = 0;
                            Divo = 0;
                            Sefupu = 0;
                            Icic = 0;
                            Rapce = 0;
                            LabCC = 0;
                            OBS = 0;
                            EstPro = 0;
                            Anticipo = 0;
                            CEDULAR = 0;
                            foreach (DataRow Registro_2 in Dt_solicitud_Detalles.Rows)
                            {
                                if (Registro[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString() == Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString())
                                {
                                    Importe = Importe + Convert.ToDouble(Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura].ToString());
                                    if (!String.IsNullOrEmpty(Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString()))
                                    {
                                        ISR = ISR + Convert.ToDouble(Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString());
                                        CEDULAR = CEDULAR + Convert.ToDouble(Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString());
                                    }
                                    if (!String.IsNullOrEmpty(Registro_2["CAP"].ToString()))
                                    {
                                        Cap = Cap + Convert.ToDouble(Registro_2["CAP"].ToString());
                                        Divo = Divo + Convert.ToDouble(Registro_2["DIVO"].ToString());
                                        Sefupu = Sefupu + Convert.ToDouble(Registro_2["SEFUPU"].ToString());
                                        Icic = Icic + Convert.ToDouble(Registro_2["ICIC"].ToString());
                                        Rapce = Rapce + Convert.ToDouble(Registro_2["RAPCE"].ToString());
                                        LabCC = LabCC + Convert.ToDouble(Registro_2["Lab_Control_C"].ToString());
                                        OBS = OBS + Convert.ToDouble(Registro_2["OBS"].ToString());
                                        EstPro = EstPro + Convert.ToDouble(Registro_2["Est_Y_Proy"].ToString());
                                        Anticipo = Anticipo + Convert.ToDouble(Registro_2["Amortizado"].ToString());
                                    }
                                }
                            }

                            Fila["PARTIDA_ID"] = Registro[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString();
                            Fila["IMPORTE"] = Importe + ISR + CEDULAR + Cap + Divo + Sefupu + Icic + Rapce + LabCC + OBS + EstPro + Anticipo;
                            Fila["ANIO"] = Dt_Reserva.Rows[0]["ANIO"].ToString();
                            Dt_Partidas.Rows.Add(Fila);
                            Dt_Partidas.AcceptChanges();
                        }
                    }
                    if (P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    return Dt_Partidas;
                }
                catch (SqlException Ex)
                {
                    if (P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (P_Cmmd == null)
                    {
                        Cn.Close();
                    }

                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consultar_detalles_partidas_de_solicitud
            /// DESCRIPCION : 1.Consulta el último No dado de alta para poder ingresar el siguiente
            ///               2. Da de Alta La Solictud de Pago en la BD con los datos 
            ///                  proporcionados por elusuario
            /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
            /// CREO        : Sergio Manuel gallardo Andrade
            /// FECHA_CREO  : 27-02-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consultar_detalles_partidas_de_solicitud_Gastos(String no_solicitud, SqlCommand P_Cmmd)
            {
                StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos         
                Object No_Reserva;
                Double Importe;
                Double ISR;
                Double CEDULAR;
                Double Cap;
                Double Divo;
                Double Sefupu;
                Double Icic;
                Double Rapce;
                Double LabCC;
                Double OBS;
                Double EstPro;
                Double Anticipo;
                DataTable Dt_Reserva = new DataTable();           //Obtiene la reserva a la que se pertenece la solicitud
                DataTable Dt_Reserva_Detalles = new DataTable();     // se obtiene los detalles de la partida 
                DataTable Dt_Partidas = new DataTable();     // se obtiene los detalles de la partida 
                DataTable Dt_solicitud_Detalles = new DataTable();
                DataRow Fila;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                SqlDataAdapter Da_SQL = new SqlDataAdapter();
                DataSet Ds_SQL = new DataSet();
                if (P_Cmmd != null)
                {
                    Cmmd = P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT No_Reserva FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + no_solicitud + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    No_Reserva = Cmmd.ExecuteScalar();

                    //No_Reserva = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT " + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + ", " + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                    Mi_SQL.Append(" FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = '" + No_Reserva + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_solicitud_Detalles = Ds_SQL.Tables[0];
                    //Dt_solicitud_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    Mi_SQL.Length = 0;
                    //consultar la reserva
                    Ds_SQL = new DataSet();
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + No_Reserva + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Reserva = Ds_SQL.Tables[0];
                    //Dt_Reserva = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    Ds_SQL = new DataSet();
                    Mi_SQL.Length = 0;
                    // consultar los detalles de la reserva
                    Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = '" + Dt_Reserva.Rows[0]["No_Reserva"].ToString() + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Reserva_Detalles = Ds_SQL.Tables[0];
                    //Dt_Reserva_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    if (Dt_Partidas != null)
                    {
                        Dt_Partidas.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("ANIO", System.Type.GetType("System.String"));
                        foreach (DataRow Registro in Dt_Reserva_Detalles.Rows)
                        {
                            Fila = Dt_Partidas.NewRow();
                            Fila["DEPENDENCIA_ID"] = Dt_Reserva.Rows[0]["DEPENDENCIA_ID"].ToString();
                            Fila["FUENTE_FINANCIAMIENTO_ID"] = Registro["FTE_FINANCIAMIENTO_ID"].ToString();
                            Fila["PROGRAMA_ID"] = Registro["PROYECTO_PROGRAMA_ID"].ToString();
                            Importe = 0;
                            foreach (DataRow Registro_2 in Dt_solicitud_Detalles.Rows)
                            {
                                if (Registro[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString() == Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString())
                                {
                                    Importe = Importe + Convert.ToDouble(Registro_2[Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial].ToString());
                                }
                            }

                            Fila["PARTIDA_ID"] = Registro[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString();
                            Fila["IMPORTE"] = Importe; //+ ISR + CEDULAR + Cap + Divo + Sefupu + Icic + Rapce + LabCC + OBS + EstPro + Anticipo;
                            Fila["ANIO"] = Dt_Reserva.Rows[0]["ANIO"].ToString();
                            Dt_Partidas.Rows.Add(Fila);
                            Dt_Partidas.AcceptChanges();
                        }
                    }
                    if (P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    return Dt_Partidas;
                }
                catch (SqlException Ex)
                {
                    if (P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (P_Cmmd == null)
                    {
                        Cn.Close();
                    }

                }
            }
                
        ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consultar_detalles_partidas_de_solicitud_Servicios_Generales
            /// DESCRIPCION : 1.Consulta el último No dado de alta para poder ingresar el siguiente
            ///               2. Da de Alta La Solictud de Pago en la BD con los datos 
            ///                  proporcionados por elusuario
            /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
            /// CREO        : Sergio Manuel gallardo Andrade
            /// FECHA_CREO  : 03-Septiembre-2013
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consultar_detalles_partidas_de_solicitud_Servicios_Generales(String no_solicitud, SqlCommand P_Cmmd)
            {
                StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos         
                Object No_Reserva;
                Double Importe;
                DataTable Dt_Reserva = new DataTable();           //Obtiene la reserva a la que se pertenece la solicitud
                DataTable Dt_Reserva_Detalles = new DataTable();     // se obtiene los detalles de la partida 
                DataTable Dt_Partidas = new DataTable();     // se obtiene los detalles de la partida 
                DataTable Dt_solicitud_Detalles = new DataTable();
                DataRow Fila;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                SqlDataAdapter Da_SQL = new SqlDataAdapter();
                DataSet Ds_SQL = new DataSet();
                if (P_Cmmd != null)
                {
                    Cmmd = P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT No_Reserva FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + no_solicitud + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    No_Reserva = Cmmd.ExecuteScalar();

                    //No_Reserva = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT (" + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + " + "+Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva+" ) as MONTO_TOTAL, " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID);
                    Mi_SQL.Append(", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Dependencia_ID + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID+", "+ Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID);
                    Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + " = '" + no_solicitud + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_solicitud_Detalles = Ds_SQL.Tables[0];
                    //Dt_solicitud_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    Mi_SQL.Length = 0;
                    //consultar la reserva
                    Ds_SQL = new DataSet();
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + No_Reserva + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Reserva = Ds_SQL.Tables[0];
                    //Dt_Reserva = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    Ds_SQL = new DataSet();
                    Mi_SQL.Length = 0;
                    // consultar los detalles de la reserva
                    Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = '" + Dt_Reserva.Rows[0]["No_Reserva"].ToString() + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Reserva_Detalles = Ds_SQL.Tables[0];
                    //Dt_Reserva_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    if (Dt_Partidas != null)
                    {
                        Dt_Partidas.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("ANIO", System.Type.GetType("System.String"));
                        foreach (DataRow Registro in Dt_Reserva_Detalles.Rows)
                        {
                            Fila = Dt_Partidas.NewRow();
                            Fila["DEPENDENCIA_ID"] = Registro["DEPENDENCIA_ID"].ToString();
                            Fila["FUENTE_FINANCIAMIENTO_ID"] = Registro["FTE_FINANCIAMIENTO_ID"].ToString();
                            Fila["PROGRAMA_ID"] = Registro["PROYECTO_PROGRAMA_ID"].ToString();
                            Importe = 0;
                            foreach (DataRow Registro_2 in Dt_solicitud_Detalles.Rows)
                            {
                                if ((Registro[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString() == Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString()) && (Registro[Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID].ToString() == Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_Dependencia_ID].ToString()) && (Registro[Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString() == Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString()))
                                {
                                    Importe = Importe + Convert.ToDouble(Registro_2["MONTO_TOTAL"].ToString());
                                }
                            }
                            Fila["PARTIDA_ID"] = Registro[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString();
                            Fila["IMPORTE"] = Importe;
                            Fila["ANIO"] = Dt_Reserva.Rows[0]["ANIO"].ToString();
                            Dt_Partidas.Rows.Add(Fila);
                            Dt_Partidas.AcceptChanges();
                        }
                    }
                    if (P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    return Dt_Partidas;
                }
                catch (SqlException Ex)
                {
                    if (P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (P_Cmmd == null)
                    {
                        Cn.Close();
                    }

                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consultar_detalles_partidas_de_solicitud_Finiquito
            /// DESCRIPCION : 1.Consulta el último No dado de alta para poder ingresar el siguiente
            ///               2. Da de Alta La Solictud de Pago en la BD con los datos 
            ///                  proporcionados por elusuario
            /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
            /// CREO        : Sergio Manuel gallardo Andrade
            /// FECHA_CREO  : 14-Agosto-2013
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consultar_detalles_partidas_de_solicitud_Finiquito(String No_Solicitud, SqlCommand P_Cmmd)
            {
                StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos         
                DataTable Dt_Reserva = new DataTable();           //Obtiene la reserva a la que se pertenece la solicitud
                DataTable Dt_Reserva_Detalles = new DataTable();     // se obtiene los detalles de la partida 
                DataTable Dt_Partidas = new DataTable();     // se obtiene los detalles de la partida 
                DataTable Dt_solicitud_Detalles = new DataTable();
                DataRow Fila;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                SqlDataAdapter Da_SQL = new SqlDataAdapter();
                DataSet Ds_SQL = new DataSet();
                if (P_Cmmd != null)
                {
                    Cmmd = P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {

                    // consultar los detalles de la reserva
                    Mi_SQL.Append("SELECT * FROM " + Ope_Con_Solicitud_Finiquito_Det.Tabla_Ope_Con_Solicitud_Finiquito_Det);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Finiquito_Det.Campo_No_Solicitud_Pago + " = '" + No_Solicitud + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Reserva_Detalles = Ds_SQL.Tables[0];
                    if (Dt_Partidas != null)
                    {
                        Dt_Partidas.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                        Dt_Partidas.Columns.Add("ANIO", System.Type.GetType("System.String"));
                        foreach (DataRow Registro in Dt_Reserva_Detalles.Rows)
                        {
                            if (!String.IsNullOrEmpty(Registro[Ope_Con_Solicitud_Finiquito_Det.Campo_Dependencia_ID].ToString()))
                            {
                                Fila = Dt_Partidas.NewRow();
                                Fila["DEPENDENCIA_ID"] = Registro[Ope_Con_Solicitud_Finiquito_Det.Campo_Dependencia_ID].ToString();
                                Fila["FUENTE_FINANCIAMIENTO_ID"] = Registro[Ope_Con_Solicitud_Finiquito_Det.Campo_Fte_Financiamiento_ID].ToString();
                                Fila["PROGRAMA_ID"] = Registro[Ope_Con_Solicitud_Finiquito_Det.Campo_Proyecto_Programa_ID].ToString();
                                Fila["PARTIDA_ID"] = Registro[Ope_Con_Solicitud_Finiquito_Det.Campo_Partida_ID].ToString();
                                Fila["IMPORTE"] = Registro[Ope_Con_Solicitud_Finiquito_Det.Campo_Debe].ToString();
                                Fila["ANIO"] = DateTime.Now.Year;
                                Dt_Partidas.Rows.Add(Fila);
                                Dt_Partidas.AcceptChanges();
                            }
                        }
                    }
                    if (P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    return Dt_Partidas;
                }
                catch (SqlException Ex)
                {
                    if (P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (P_Cmmd == null)
                    {
                        Cn.Close();
                    }

                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Alta_Solicitud_Pago_Sin_poliza
            /// DESCRIPCION : 1.Consulta el último No dado de alta para poder ingresar el siguiente
            ///               2. Da de Alta La Solictud de Pago en la BD con los datos 
            ///                  proporcionados por elusuario
            /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 21-Diciembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static String  Alta_Solicitud_Pago_Sin_Poliza(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos
                Object No_Solicitud_Pago;                        //Obtiene el último número de registro que fue dado de alta en la base de datos
                SqlCommand Comando_SQL = new SqlCommand(); //Sirve para la ejecución de las operaciones a la base de datos
                SqlTransaction Transaccion_SQL;               //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
                SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
                String Directorio_Destino;
                DataTable Dt_Partidas = new DataTable();
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;
                try
                {
                    Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "),'0000000000')");
                    Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    No_Solicitud_Pago = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    if (Convert.IsDBNull(No_Solicitud_Pago))
                    {
                        Datos.P_No_Solicitud_Pago = "0000000001";
                    }
                    else
                    {
                        Datos.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud_Pago) + 1);
                    }

                    //Consulta el último No que fue dato de alta en la base de datos
                    Mi_SQL.Length = 0;

                    //Inserta un nuevo registro en la base de datos con los datos obtenidos por el usuario
                    Mi_SQL.Append("INSERT INTO " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" (" + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", " + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", " + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", " + Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                    //Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + ", ");
                    if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID)) {
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + ", ");
                    }
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID )) {
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID  + ", ");
                    }                    
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ")");
                    Mi_SQL.Append(" VALUES ('" + Datos.P_No_Solicitud_Pago + "', '" + Datos.P_Tipo_Solicitud_Pago_ID + "'," + Convert.ToInt32(Datos.P_No_Reserva) + ",");
                    Mi_SQL.Append(" '" + Datos.P_Concepto + "', GETDATE(), " + Datos.P_Monto + ", '");// + Datos.P_No_Factura + "',");
                    //Mi_SQL.Append(" TO_DATE('" + Datos.P_Fecha_Factura + "', 'MM/DD/YY'), '");
                    if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID))
                    {
                        Mi_SQL.Append(Datos.P_Proveedor_ID + "', '");
                    }
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID)) 
                    {
                        Mi_SQL.Append(Datos.P_Empleado_ID + "', '");
                    }
                    Mi_SQL.Append(Datos.P_Estatus + "',");
                    Mi_SQL.Append(" '" + Datos.P_Nombre_Usuario + "', GETDATE())");
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    
                    Directorio_Destino = HttpContext.Current.Server.MapPath("~") + "\\Archivo_Solicitud_Pagos\\" + Datos.P_No_Solicitud_Pago + "\\";
                    if (System.IO.Directory.Exists(Directorio_Destino) == false)
                    {
                        System.IO.Directory.CreateDirectory(Directorio_Destino);
                    }
                    foreach (DataRow Renglon in Datos.P_Dt_Detalles_Solicitud.Rows)
                    {
                        Mi_SQL.Length = 0;
                        Mi_SQL.Append("INSERT INTO " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles );
                        Mi_SQL.Append(" (" + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Factura
                                            + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura
                                            + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID +
                                              ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Documento + ",");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Archivo 
                                            + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Ruta 
                                            + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID +
                                            ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva
                                            +", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_IEPS  +
                                              ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID + ",");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Nombre_Proveedor_Factura
                                            + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion
                                            + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR +
                                            ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Retencion_IVA
                                            + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula +
                                            ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_RFC +
                                              ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_CURP +
                                              ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISH  + ",");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Usuario_Creo + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Creo + ")");
                        Mi_SQL.Append(" VALUES ('" + Datos.P_No_Solicitud_Pago + "', '" + Renglon["No_Documento"].ToString() + "', '" + String.Format("{0:dd/MM/yyyy}", Renglon["Fecha_Documento"]) + "',");
                        Mi_SQL.Append(" " + Renglon["Monto"].ToString().Replace(",","") + ",'" +
                                            Renglon["Partida_Id"].ToString() +
                                            "','" + Datos.P_Tipo_Documento + "','" +
                                            Renglon["Archivo"].ToString() + "','" );
                                            if(Renglon["Archivo"].ToString() != null && Renglon["Archivo"].ToString()!=""){
                                               Mi_SQL.Append(Directorio_Destino + Renglon["Archivo"].ToString()  + "','");
                                                 
                                            }else{
                                                Mi_SQL.Append("','");
                                            }
                                            Mi_SQL.Append(Renglon["Proyecto_Programa_Id"].ToString() + "'," +
                                            Renglon["iva"].ToString() +
                                            "," +Renglon["IEPS"].ToString() +
                                            ",'" + Renglon["Fte_Financiamiento_Id"].ToString() + "','" +
                                            Renglon["Nombre_Proveedor_Fact"].ToString() + "','" +
                                            Renglon["Operacion"].ToString() +
                                            "'," + Renglon["Retencion_ISR"].ToString() + "," +
                                            Renglon["Retencion_IVA"].ToString() +
                                            "," + Renglon["Retencion_Celula"].ToString() + ",'" +
                                            Renglon["RFC"].ToString() +
                                            "','" + Renglon["CURP"].ToString() +
                                            "'," + Renglon["ISH"].ToString() + ",'" +
                                            Datos.P_Nombre_Usuario + "', GETDATE())");// + Datos.P_No_Factura + "',");
                        Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                        if (Renglon["Archivo"].ToString() != null && Renglon["Archivo"].ToString()!="")
                        {
                            System.IO.File.Copy(Renglon["Ruta"].ToString(), Directorio_Destino + Renglon["Archivo"].ToString());
                            System.IO.File.Delete(Renglon["Ruta"].ToString());
                        }
                    }
                    Directorio_Destino = HttpContext.Current.Server.MapPath("~") + "\\Archivo_Solicitud_Pagos\\Temporal";
                    if (System.IO.Directory.Exists(Directorio_Destino) == true)
                    {
                        String[] archivos = System.IO.Directory.GetFiles(Directorio_Destino);
                        int a;
                        for (a = 0; a < archivos.Length; a++)
                        {
                            System.IO.File.Delete(a.ToString());
                        }
                    }
                    
                    Mi_SQL.Length = 0;
                    //Actualiza el saldo de la reserva
                    Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                    Mi_SQL.Append(" SET " + Ope_Psp_Reservas.Campo_Saldo + " = " + Ope_Psp_Reservas.Campo_Saldo + " - " + Datos.P_Monto );//+ ", ");
                   // Mi_SQL.Append(Ope_Psp_Reservas.Campo_Beneficiario + " = '" + Datos.P_Beneficiario + "'");
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva));
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos 
                    Transaccion_SQL.Commit();    
                    //obtenemos las partidas
                    Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Datos.P_No_Solicitud_Pago);
                    foreach (DataRow Renglon in Dt_Partidas.Rows)
                    {
                        Mi_SQL.Length = 0;
                        Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                        Mi_SQL.Append(" SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " - " + Renglon["importe"].ToString() + "");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva)+" AND " );
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = " + Renglon["PARTIDA_ID"].ToString() + " AND ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " = " + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString() + " AND ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " = " + Renglon["PROGRAMA_ID"].ToString());
                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    }
         //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos                    
                    return Datos.P_No_Solicitud_Pago;
                }
                catch (SqlException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    Conexion_Base.Close();
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Modificar_Solicitud_Pago
            /// DESCRIPCION : Modifica los datos de la Solicitud de Pago con lo que fueron 
            ///               introducidos por el usuario
            /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
            ///                       proporcionados por el usuario y van a sustituir a los datos que se
            ///                       encuentran en la base de datos
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 17-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static String Modificar_Solicitud_Pago(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                String Total_Presupuesto = "";
                Decimal Total_Poliza = 0;                         //Obtiene el monto total del debe y haber de la póliza
                String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
                StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos
                Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
                Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
                Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
                DataTable Dt_Partidas = new DataTable();
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                String Resultado = "";
                int Actualiza_Presupuesto = 0;
                int Registra_Movimiento = 0;
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_No_Poliza)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Poliza_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Mes_Ano)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Datos.P_Tipo_Solicitud_Pago_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Jefe_Area + " = '" + Datos.P_Empleado_ID_Jefe_Area + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Contabilidad + " = '" + Datos.P_Empleado_ID_Contabilidad + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Concepto)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Concepto + " = '" + Datos.P_Concepto + "', ");
                    if (Datos.P_Monto > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto + " = " + Datos.P_Monto + ", ");
                    if (!String.IsNullOrEmpty(Datos.P_No_Factura)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Factura + " = '" + Datos.P_No_Factura + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Factura)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Factura) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Jefe)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Jefe + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Jefe) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Contabi)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Contabi) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Jefe_Area + " = '" + Datos.P_Comentarios_Jefe_Area + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Contabilidad + " = '" + Datos.P_Comentarios_Contabilidad + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "', ");
                    if (Datos.P_No_Reserva > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = " + Datos.P_No_Reserva + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + " = '" + Datos.P_Nombre_Usuario + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + " = GETDATE()");
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                    Mi_SQL.Length = 0;
                    //Consulta para la obtención del último ID dado de alta 
                    Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                    Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00003'");
                    //Mi_SQL.Append(" ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    No_Poliza = Cmmd.ExecuteScalar();
                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(No_Poliza))
                    {
                        No_Poliza = "0000000001";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                    }
                    Mi_SQL.Length = 0;
                    Total_Poliza += Convert.ToDecimal(Datos.P_Monto_Anterior) + Datos.P_Monto;
                    //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                    Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL.Append(" (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", ");
                    Mi_SQL.Append(Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + ")");
                    Mi_SQL.Append(" VALUES ('" + No_Poliza + "', '00003', '" + Mes_Anio + "',");
                    Mi_SQL.Append(" '" + String.Format("{0:dd/MM/yyyy}", DateTime.Today) + "',");
                    Mi_SQL.Append(" '" + Datos.P_Concepto + "', " + Total_Poliza + ", " + Total_Poliza + ", " + Datos.P_Partida + ", ");
                    Mi_SQL.Append("'" + Datos.P_Nombre_Usuario + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', ");
                    Mi_SQL.Append("'" + Cls_Sessiones.Empleado_ID + "')");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                    //Da de alta los detalles de la póliza
                    foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                    {
                        Mi_SQL.Length = 0;
                        //consulta el saldo de la cuenta contable
                        Mi_SQL.Append("SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - ");
                        Mi_SQL.Append(" ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                        Saldo = Cmmd.ExecuteScalar();
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                        }
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                        }

                        Mi_SQL.Length = 0;
                        //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                        Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                        Consecutivo = Cmmd.ExecuteScalar();

                        //Valida si el ID es nulo para asignarle automaticamente el primer registro
                        if (Convert.IsDBNull(Consecutivo))
                        {
                            Consecutivo = "1";
                        }
                        //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                        else
                        {
                            Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                        }
                        Mi_SQL.Length = 0;
                        //Inserta el registro del detalle de la póliza en la base de datos
                        Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Mi_SQL.Append("(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", ");
                        Mi_SQL.Append("Beneficiario_ID, Tipo_Beneficiario, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Consecutivo + ")");
                        Mi_SQL.Append(" VALUES('" + No_Poliza + "', '00003', '" + Mes_Anio + "', ");
                        Mi_SQL.Append(Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + ",");
                        Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "',");
                        Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString() + "', ");
                        Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", ");
                        Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", ");
                        Mi_SQL.Append(" '" + Renglon["Beneficiario_ID"].ToString() + "', '" + Renglon["Tipo_Beneficiario"].ToString() + "', ");
                        Mi_SQL.Append(Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ")");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos            
                    }
                    //Transaccion_SQL.Commit(); //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
                    Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza, Cmmd);
                    if (!String.IsNullOrEmpty(Datos.P_Servicios_Generales))
                    {
                        if (Datos.P_Servicios_Generales == "SI")
                        {
                            Dt_Partidas = Consultar_detalles_partidas_de_solicitud_Servicios_Generales(Datos.P_No_Solicitud_Pago, Cmmd);
                        }
                        else
                        {
                            Dt_Partidas = Consultar_detalles_partidas_de_solicitud(Datos.P_No_Solicitud_Pago, Cmmd);
                        }
                    }
                    else
                    {
                        Dt_Partidas = Consultar_detalles_partidas_de_solicitud(Datos.P_No_Solicitud_Pago, Cmmd);
                    }
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Ejercido))
                    {
                        Mi_SQL.Length = 0;
                        Mi_SQL.Append("SELECT " + Ope_Con_Polizas.Campo_Total_Debe);
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " ='" + No_Poliza + "'");
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " ='00003'");
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Mes_Ano + " ='" + Mes_Anio + "'");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                        Total_Presupuesto = Cmmd.ExecuteScalar().ToString();

                        //Total_Presupuesto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL_2.ToString());
                        Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("COMPROMETIDO", "DEVENGADO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal

                        if (Actualiza_Presupuesto > 0)
                        {
                            Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva_Anterior), "COMPROMETIDO", "DEVENGADO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                            Resultado = "SI";
                        }
                        else
                        {
                            Resultado = "NO";
                        }
                        Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PRE_COMPROMETIDO", "COMPROMETIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                        if (Actualiza_Presupuesto > 0)
                        {
                            Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva_Anterior), "PRE_COMPROMETIDO", "COMPROMETIDO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                            Resultado = "SI";
                        }
                        else
                        {
                            Resultado = "NO";
                        }
                    }

                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Jefe_Area) || !String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad))
                    {
                        //Optenemos el total de la reserva con los impuestos que se disminuyeron

                        Mi_SQL.Length = 0;
                        Mi_SQL.Append("SELECT " + Ope_Con_Polizas.Campo_Total_Debe);
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " ='" + No_Poliza + "'");
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " ='00003'");
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Mes_Ano + " ='" + Mes_Anio + "'");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                        Total_Presupuesto = Cmmd.ExecuteScalar().ToString();

                        //Total_Presupuesto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL_2.ToString());
                        Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("COMPROMETIDO", "DEVENGADO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal

                        if (Actualiza_Presupuesto > 0)
                        {
                            Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva_Anterior), "COMPROMETIDO", "DEVENGADO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                            Resultado = "SI";
                        }
                        else
                        {
                            Resultado = "NO";
                        }
                        Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PRE_COMPROMETIDO", "COMPROMETIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                        if (Actualiza_Presupuesto > 0)
                        {
                            Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva_Anterior), "PRE_COMPROMETIDO", "COMPROMETIDO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                            Resultado = "SI";
                        }
                        else
                        {
                            Resultado = "NO";
                        }
                    }

                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    return Resultado;
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Modificar_Solicitud_Finiquito
            /// DESCRIPCION : Modifica los datos de la Solicitud de Pago con lo que fueron 
            ///               introducidos por el usuario
            /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
            ///                       proporcionados por el usuario y van a sustituir a los datos que se
            ///                       encuentran en la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 15-Agosto-2013
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static String Modificar_Solicitud_Finiquito(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                String Total_Presupuesto = "";
                Decimal Total_Poliza = 0;                         //Obtiene el monto total del debe y haber de la póliza
                String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
                StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos
                Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
                Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
                Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
                DataTable Dt_Partidas = new DataTable();
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                String Resultado = "";
                int Actualiza_Presupuesto = 0;
                int Registra_Movimiento = 0;
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_No_Poliza)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Poliza_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Mes_Ano)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Datos.P_Tipo_Solicitud_Pago_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Jefe_Area + " = '" + Datos.P_Empleado_ID_Jefe_Area + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Contabilidad + " = '" + Datos.P_Empleado_ID_Contabilidad + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Concepto)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Concepto + " = '" + Datos.P_Concepto + "', ");
                    if (Datos.P_Monto > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto + " = " + Datos.P_Monto + ", ");
                    if (!String.IsNullOrEmpty(Datos.P_No_Factura)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Factura + " = '" + Datos.P_No_Factura + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Factura)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Factura) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Jefe)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Jefe + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Jefe) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Contabi)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Contabi) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Jefe_Area + " = '" + Datos.P_Comentarios_Jefe_Area + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Contabilidad + " = '" + Datos.P_Comentarios_Contabilidad + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "', ");
                    if (Datos.P_No_Reserva > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = " + Datos.P_No_Reserva + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + " = '" + Datos.P_Nombre_Usuario + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + " = GETDATE()");
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    Mi_SQL.Length = 0;
                    //Consulta para la obtención del último ID dado de alta 
                    Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                    Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00003'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    No_Poliza = Cmmd.ExecuteScalar();
                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(No_Poliza))
                    {
                        No_Poliza = "0000000001";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                    }
                    Mi_SQL.Length = 0;
                    Total_Poliza += Convert.ToDecimal(Datos.P_Monto_Anterior) + Datos.P_Monto;
                    //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                    Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL.Append(" (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", ");
                    Mi_SQL.Append(Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + ")");
                    Mi_SQL.Append(" VALUES ('" + No_Poliza + "', '00003', '" + Mes_Anio + "',");
                    Mi_SQL.Append(" '" + String.Format("{0:dd/MM/yyyy}", DateTime.Today) + "',");
                    Mi_SQL.Append(" '" + Datos.P_Concepto + "', " + Datos.P_Monto_Anterior + ", " + Datos.P_Monto_Anterior + ", " + Datos.P_Partida + ", ");
                    Mi_SQL.Append("'" + Datos.P_Nombre_Usuario + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', ");
                    Mi_SQL.Append("'" + Cls_Sessiones.Empleado_ID + "')");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                    //Da de alta los detalles de la póliza
                    foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                    {
                        Mi_SQL.Length = 0;
                        //consulta el saldo de la cuenta contable
                        Mi_SQL.Append("SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - ");
                        Mi_SQL.Append(" ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                        Saldo = Cmmd.ExecuteScalar();
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                        }
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                        }
                        Mi_SQL.Length = 0;
                        //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                        Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                        Consecutivo = Cmmd.ExecuteScalar();
                        //Valida si el ID es nulo para asignarle automaticamente el primer registro
                        if (Convert.IsDBNull(Consecutivo))
                        {
                            Consecutivo = "1";
                        }
                        //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                        else
                        {
                            Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                        }
                        Mi_SQL.Length = 0;
                        //Inserta el registro del detalle de la póliza en la base de datos
                        Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Mi_SQL.Append("(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", ");
                        Mi_SQL.Append("Beneficiario_ID, Tipo_Beneficiario, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Consecutivo + ")");
                        Mi_SQL.Append(" VALUES('" + No_Poliza + "', '00003', '" + Mes_Anio + "', ");
                        Mi_SQL.Append(Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + ",");
                        Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "',");
                        Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString() + "', ");
                        Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", ");
                        Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", ");
                        Mi_SQL.Append(" '" + Renglon["Beneficiario_ID"].ToString() + "', '" + Renglon["Tipo_Beneficiario"].ToString() + "', ");
                        Mi_SQL.Append(Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ")");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos            
                    }
                    Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza, Cmmd);
                    Dt_Partidas=Consultar_detalles_partidas_de_solicitud_Finiquito(Datos.P_No_Solicitud_Pago, Cmmd);
                    //Dt_Partidas = Consultar_detalles_partidas_de_solicitud(Datos.P_No_Solicitud_Pago, Cmmd);
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Ejercido))
                    {
                        Mi_SQL.Length = 0;
                        Mi_SQL.Append("SELECT " + Ope_Con_Polizas.Campo_Total_Debe);
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " ='" + No_Poliza + "'");
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " ='00003'");
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Mes_Ano + " ='" + Mes_Anio + "'");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                        Total_Presupuesto = Cmmd.ExecuteScalar().ToString();

                        //Total_Presupuesto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL_2.ToString());
                        Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("COMPROMETIDO", "DEVENGADO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal

                        if (Actualiza_Presupuesto > 0)
                        {
                            Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva_Anterior), "COMPROMETIDO", "DEVENGADO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                            Resultado = "SI";
                        }
                        else
                        {
                            Resultado = "NO";
                        }
                        Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PRE_COMPROMETIDO", "COMPROMETIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                        if (Actualiza_Presupuesto > 0)
                        {
                            Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva_Anterior), "PRE_COMPROMETIDO", "COMPROMETIDO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                            Resultado = "SI";
                        }
                        else
                        {
                            Resultado = "NO";
                        }
                    }
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Jefe_Area) || !String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad))
                    {
                        //Optenemos el total de la reserva con los impuestos que se disminuyeron

                        Mi_SQL.Length = 0;
                        Mi_SQL.Append("SELECT " + Ope_Con_Polizas.Campo_Total_Debe);
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " ='" + No_Poliza + "'");
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " ='00003'");
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Mes_Ano + " ='" + Mes_Anio + "'");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                        Total_Presupuesto = Cmmd.ExecuteScalar().ToString();

                        //Total_Presupuesto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL_2.ToString());
                        Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("COMPROMETIDO", "DEVENGADO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal

                        if (Actualiza_Presupuesto > 0)
                        {
                            Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva_Anterior), "COMPROMETIDO", "DEVENGADO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                            Resultado = "SI";
                        }
                        else
                        {
                            Resultado = "NO";
                        }
                        Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PRE_COMPROMETIDO", "COMPROMETIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                        if (Actualiza_Presupuesto > 0)
                        {
                            Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva_Anterior), "PRE_COMPROMETIDO", "COMPROMETIDO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                            Resultado = "SI";
                        }
                        else
                        {
                            Resultado = "NO";
                        }
                    }
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    return Resultado;
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Rechaza_Solicitud_Pago
            /// DESCRIPCION : Modifica los datos de la Solicitud de Pago con lo que fueron 
            ///               introducidos por el usuario
            /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
            ///                       proporcionados por el usuario y van a sustituir a los datos que se
            ///                       encuentran en la base de datos
            /// CREO        : Sergio Manuel Gallardo A.
            /// FECHA_CREO  : 12-junio-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static String Rechaza_Solicitud_Pago(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                Double Total_Presupuesto = 0;
                StringBuilder Mi_SQL_2 = new StringBuilder();
                Decimal Total_Poliza = 0;                         //Obtiene el monto total del debe y haber de la póliza
                String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
                StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos
                Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
                Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
                Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
                DataTable Dt_Partidas = new DataTable();
                int Afectacion_Presupuestal = 0;
                int Afectacion_Presupuestal_2 = 0;
                String Resultado = " ";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_No_Poliza)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Poliza_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Mes_Ano)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Datos.P_Tipo_Solicitud_Pago_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Jefe_Area + " = '" + Datos.P_Empleado_ID_Jefe_Area + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Contabilidad + " = '" + Datos.P_Empleado_ID_Contabilidad + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Concepto)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Concepto + " = '" + Datos.P_Concepto + "', ");
                    if (Datos.P_Monto > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto + " = " + Datos.P_Monto + ", ");
                    if (!String.IsNullOrEmpty(Datos.P_No_Factura)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Factura + " = '" + Datos.P_No_Factura + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Factura)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Factura) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Jefe)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Jefe + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Jefe) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Contabi)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Contabi) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Jefe_Area + " = '" + Datos.P_Comentarios_Jefe_Area + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Contabilidad + " = '" + Datos.P_Comentarios_Contabilidad + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentario_Finanzas)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentario_Finanzas + " = '" + Datos.P_Comentario_Finanzas + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Usuario_Recibio))
                    {
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Pagado + " = '" + Datos.P_Usuario_Recibio + "', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Pagado + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Ejercido + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Contabilidad + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autoriza_Rechaza_Ejercido + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Ejercido + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Contabilidad + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Contabilidad + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Ejercido + " = '',");
                    }
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + " = '" + Datos.P_Nombre_Usuario + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + " = GETDATE()");
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                   
                    Mi_SQL.Length = 0;
                    //Consulta para la obtención del último ID dado de alta 
                    Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                    Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00003'");
                   // Mi_SQL.Append(" ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    No_Poliza = Cmmd.ExecuteScalar();
                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(No_Poliza))
                    {
                        No_Poliza = "0000000001";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                    }
                    Mi_SQL.Length = 0;
                    Total_Poliza +=Datos.P_Monto+Convert.ToDecimal(Datos.P_Monto_Anterior);
                    //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                    Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL.Append(" (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", ");
                    Mi_SQL.Append(Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + ")");
                    Mi_SQL.Append(" VALUES ('" + No_Poliza + "', '00003', '" + Mes_Anio + "',");
                    Mi_SQL.Append(" '" + String.Format("{0:dd/MM/yyyy}", DateTime.Today) + "',");
                    Mi_SQL.Append(" '" + Datos.P_Concepto_Poliza + "', " + Total_Poliza + ", " + Total_Poliza + ", "+ Datos.P_Partida+", ");
                    Mi_SQL.Append("'" + Datos.P_Nombre_Usuario + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', ");
                    Mi_SQL.Append("'" + Cls_Sessiones.Empleado_ID + "')");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                    //Da de alta los detalles de la póliza
                    foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                    {
                        Mi_SQL.Length = 0;
                        //consulta el saldo de la cuenta contable
                        Mi_SQL.Append("SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - ");
                        Mi_SQL.Append(" ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                        Saldo = Cmmd.ExecuteScalar();
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                        }
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                        }

                        Mi_SQL.Length = 0;
                        //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                        Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                        Consecutivo = Cmmd.ExecuteScalar();

                        //Valida si el ID es nulo para asignarle automaticamente el primer registro
                        if (Convert.IsDBNull(Consecutivo))
                        {
                            Consecutivo = "1";
                        }
                        //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                        else
                        {
                            Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                        }
                        Mi_SQL.Length = 0;
                        //Inserta el registro del detalle de la póliza en la base de datos
                        Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Mi_SQL.Append("(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", ");
                        Mi_SQL.Append("Beneficiario_ID, Tipo_Beneficiario, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Consecutivo + ")");
                        Mi_SQL.Append(" VALUES('" + No_Poliza + "', '00003', '" + Mes_Anio + "', ");
                        Mi_SQL.Append(Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + ",");
                        Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "',");
                        Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString() + "', ");
                        Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", ");
                        Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", ");
                        Mi_SQL.Append("'" + Renglon["Beneficiario_ID"].ToString() + "','" + Renglon["Tipo_Beneficiario"].ToString() + "', ");
                        Mi_SQL.Append(Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ")");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos            
                        Total_Presupuesto = Total_Presupuesto + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }
                   //// Transaccion_SQL.Commit(); //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
                    //Optenemos el total de la reserva con los impuestos que se disminuyeron
                    ////Mi_SQL_2.Append("SELECT " + Ope_Con_Polizas.Campo_Total_Debe);
                    ////Mi_SQL_2.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    ////Mi_SQL_2.Append(" WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " ='" + No_Poliza + "'");
                    ////Mi_SQL_2.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " ='00003'");
                    ////Mi_SQL_2.Append(" AND " + Ope_Con_Polizas.Campo_Mes_Ano + " ='" + Mes_Anio + "'");
                    ////Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                    ////Total_Presupuesto = Cmmd.ExecuteScalar();
                    
                    ///Total_Presupuesto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL_2.ToString());
                    if (!String.IsNullOrEmpty(Datos.P_Servicios_Generales))
                    {
                        if (Datos.P_Servicios_Generales.ToString() == "SI")
                        {
                            Dt_Partidas = Consultar_detalles_partidas_de_solicitud_Servicios_Generales(Datos.P_No_Solicitud_Pago, Cmmd);
                        }
                        else
                        {
                            Dt_Partidas = Consultar_detalles_partidas_de_solicitud(Datos.P_No_Solicitud_Pago, Cmmd);
                        }
                    }
                    else
                    {
                        Dt_Partidas = Consultar_detalles_partidas_de_solicitud(Datos.P_No_Solicitud_Pago, Cmmd);
                    }
                    Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza,Cmmd);
                    Afectacion_Presupuestal=Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DEVENGADO", "EJERCIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                    if (Afectacion_Presupuestal > 0)
                    {
                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "DEVENGADO", "EJERCIDO", Convert.ToDouble(Total_Presupuesto), "", "", "", "", Cmmd);//Agrega el historial del movimiento de la partida presupuestal
                        Afectacion_Presupuestal_2=Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("COMPROMETIDO", "DEVENGADO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                        if (Afectacion_Presupuestal_2 > 0)
                        {
                            Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "COMPROMETIDO", "DEVENGADO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                            Resultado = "SI";
                        }
                        else
                        {
                            Resultado = "NO";
                        }
                        
                    }
                    else
                    {
                        Resultado = "NO";
                    }
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }

                    return Resultado;
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }

                }
            }
            /// ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Rechaza_Solicitud_Finiquito
            /// DESCRIPCION : Modifica los datos de la Solicitud de Pago con lo que fueron 
            ///               introducidos por el usuario
            /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
            ///                       proporcionados por el usuario y van a sustituir a los datos que se
            ///                       encuentran en la base de datos
            /// CREO        : Sergio Manuel Gallardo A.
            /// FECHA_CREO  : 16-Agosto-2013
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static String Rechaza_Solicitud_Finiquito(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                Double Total_Presupuesto = 0;
                StringBuilder Mi_SQL_2 = new StringBuilder();
                Decimal Total_Poliza = 0;                         //Obtiene el monto total del debe y haber de la póliza
                String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
                StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos
                Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
                Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
                Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
                DataTable Dt_Partidas = new DataTable();
                int Afectacion_Presupuestal = 0;
                int Afectacion_Presupuestal_2 = 0;
                String Resultado = " ";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_No_Poliza)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Poliza_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Mes_Ano)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Datos.P_Tipo_Solicitud_Pago_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Jefe_Area + " = '" + Datos.P_Empleado_ID_Jefe_Area + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Contabilidad + " = '" + Datos.P_Empleado_ID_Contabilidad + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Concepto)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Concepto + " = '" + Datos.P_Concepto + "', ");
                    if (Datos.P_Monto > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto + " = " + Datos.P_Monto + ", ");
                    if (!String.IsNullOrEmpty(Datos.P_No_Factura)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Factura + " = '" + Datos.P_No_Factura + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Factura)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Factura) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Jefe)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Jefe + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Jefe) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Contabi)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Contabi) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Jefe_Area + " = '" + Datos.P_Comentarios_Jefe_Area + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Contabilidad + " = '" + Datos.P_Comentarios_Contabilidad + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentario_Finanzas)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentario_Finanzas + " = '" + Datos.P_Comentario_Finanzas + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Usuario_Recibio))
                    {
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Pagado + " = '" + Datos.P_Usuario_Recibio + "', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Pagado + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Ejercido + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Contabilidad + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autoriza_Rechaza_Ejercido + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Ejercido + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Contabilidad + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Contabilidad + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Ejercido + " = '',");
                    }
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + " = '" + Datos.P_Nombre_Usuario + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + " = GETDATE()");
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                    Mi_SQL.Length = 0;
                    //Consulta para la obtención del último ID dado de alta 
                    Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                    Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00003'");
                    // Mi_SQL.Append(" ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    No_Poliza = Cmmd.ExecuteScalar();
                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(No_Poliza))
                    {
                        No_Poliza = "0000000001";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                    }
                    Mi_SQL.Length = 0;
                    Total_Poliza += Datos.P_Monto + Convert.ToDecimal(Datos.P_Monto_Anterior);
                    //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                    Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL.Append(" (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", ");
                    Mi_SQL.Append(Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + ")");
                    Mi_SQL.Append(" VALUES ('" + No_Poliza + "', '00003', '" + Mes_Anio + "',");
                    Mi_SQL.Append(" '" + String.Format("{0:dd/MM/yyyy}", DateTime.Today) + "',");
                    Mi_SQL.Append(" '" + Datos.P_Concepto_Poliza + "', " + Total_Poliza + ", " + Total_Poliza + ", " + Datos.P_Partida + ", ");
                    Mi_SQL.Append("'" + Datos.P_Nombre_Usuario + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', ");
                    Mi_SQL.Append("'" + Cls_Sessiones.Empleado_ID + "')");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                    //Da de alta los detalles de la póliza
                    foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                    {
                        Mi_SQL.Length = 0;
                        //consulta el saldo de la cuenta contable
                        Mi_SQL.Append("SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - ");
                        Mi_SQL.Append(" ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                        Saldo = Cmmd.ExecuteScalar();
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                        }
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                        }

                        Mi_SQL.Length = 0;
                        //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                        Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                        Consecutivo = Cmmd.ExecuteScalar();

                        //Valida si el ID es nulo para asignarle automaticamente el primer registro
                        if (Convert.IsDBNull(Consecutivo))
                        {
                            Consecutivo = "1";
                        }
                        //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                        else
                        {
                            Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                        }
                        Mi_SQL.Length = 0;
                        //Inserta el registro del detalle de la póliza en la base de datos
                        Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Mi_SQL.Append("(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", ");
                        Mi_SQL.Append("Beneficiario_ID, Tipo_Beneficiario, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Consecutivo + ")");
                        Mi_SQL.Append(" VALUES('" + No_Poliza + "', '00003', '" + Mes_Anio + "', ");
                        Mi_SQL.Append(Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + ",");
                        Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "',");
                        Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString() + "', ");
                        Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", ");
                        Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", ");
                        Mi_SQL.Append("'" + Renglon["Beneficiario_ID"].ToString() + "','" + Renglon["Tipo_Beneficiario"].ToString() + "', ");
                        Mi_SQL.Append(Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ")");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos            
                        Total_Presupuesto = Total_Presupuesto + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }
                    ///Total_Presupuesto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL_2.ToString());
                    Dt_Partidas = Consultar_detalles_partidas_de_solicitud_Finiquito(Datos.P_No_Solicitud_Pago, Cmmd);
                    Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza, Cmmd);
                    Afectacion_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DEVENGADO", "EJERCIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                    if (Afectacion_Presupuestal > 0)
                    {
                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "DEVENGADO", "EJERCIDO", Convert.ToDouble(Total_Presupuesto), "", "", "", "", Cmmd);//Agrega el historial del movimiento de la partida presupuestal
                        Afectacion_Presupuestal_2 = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("COMPROMETIDO", "DEVENGADO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                        if (Afectacion_Presupuestal_2 > 0)
                        {
                            Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "COMPROMETIDO", "DEVENGADO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                            Resultado = "SI";
                        }
                        else
                        {
                            Resultado = "NO";
                        }

                    }
                    else
                    {
                        Resultado = "NO";
                    }
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    return Resultado;
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }

                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Modificar_Solicitud_Pago_Sin_Poliza
            /// DESCRIPCION : Modifica los datos de la Solicitud de Pago con lo que fueron 
            ///               introducidos por el usuario
            /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
            ///                       proporcionados por el usuario y van a sustituir a los datos que se
            ///                       encuentran en la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 17-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static String  Modificar_Solicitud_Pago_Sin_Poliza(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                //String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
                StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos              
                String Directorio_Destino;
                DataTable Dt_Detalles_Anteriores = new DataTable();
                Boolean Insertar = true;
                DataTable Dt_Partidas = new DataTable();
                DataTable Dt_solicitud = new DataTable();
                SqlDataAdapter Da_SQL = new SqlDataAdapter();
                DataSet Ds_SQL = new DataSet();
                
                SqlCommand Comando_SQL = new SqlCommand(); //Sirve para la ejecución de las operaciones a la base de datos
                SqlConnection Conexion_Base = new SqlConnection(); //Variable para la conexión para la base de datos        
                SqlTransaction Transaccion_SQL= null;                                                  //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        

                if (Datos.P_Cmmd != null)
                {
                    Comando_SQL = Datos.P_Cmmd;
                }
                else
                {
                    Conexion_Base.ConnectionString = Cls_Constantes.Str_Conexion;
                    Conexion_Base.Open();
                    Transaccion_SQL = Conexion_Base.BeginTransaction();
                    Comando_SQL.Connection = Transaccion_SQL.Connection;
                    Comando_SQL.Transaction = Transaccion_SQL;
                }
                try
                {
                    
                    Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_No_Poliza)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Poliza_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Mes_Ano)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Datos.P_Tipo_Solicitud_Pago_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Jefe_Area + " = '" + Datos.P_Empleado_ID_Jefe_Area + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Contabilidad + " = '" + Datos.P_Empleado_ID_Contabilidad + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Concepto)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Concepto + " = '" + Datos.P_Concepto + "', ");
                    if(Datos.P_Monto > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto + " = " + Datos.P_Monto + ", ");
                    //if (!String.IsNullOrEmpty(Datos.P_No_Factura )) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Factura + " = '" + Datos.P_No_Factura + "', ");
                    //if (!String.IsNullOrEmpty(Datos.P_Fecha_Factura)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + " = TO_DATE('" + String.Format("{0:dd/MM/yy}", Datos.P_Fecha_Factura) + "','DD/MM/YY'), ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Jefe)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Jefe + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Jefe) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Contabi)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Contabi) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Jefe_Area + " = '" + Datos.P_Comentarios_Jefe_Area + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Contabilidad + " = '" + Datos.P_Comentarios_Contabilidad + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentario_Recepcion)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Recepcion_Doc + " = '" + Datos.P_Comentario_Recepcion + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Usuario_Recibio)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio + " = '" + Datos.P_Usuario_Recibio + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_recepcion_Documentos)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recepcion_Documentos + " = '" + Datos.P_Fecha_recepcion_Documentos + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID  + " = '" + Datos.P_Empleado_ID  + "', ");
                    if (Datos.P_No_Reserva > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = " + Datos.P_No_Reserva + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + " = '" + Datos.P_Nombre_Usuario + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + " = GETDATE()");
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                   
                     Mi_SQL.Length = 0;
                    ////Actualiza el saldo de la reserva
                    //Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                    //Mi_SQL.Append(" SET " + Ope_Psp_Reservas.Campo_Saldo + " =" + Ope_Psp_Reservas.Campo_Importe_Inicial);
                    //Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva_Anterior));
                    //Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    //Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos
                    //Mi_SQL.Length = 0;
                    //Mi_SQL.Length = 0;

                    //Actualiza el saldo de la reserva
                    Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                    Mi_SQL.Append(" SET " + Ope_Psp_Reservas.Campo_Saldo + " = " + Ope_Psp_Reservas.Campo_Saldo + " + " + Datos.P_Monto_Anterior);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva_Anterior));
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos
                    Mi_SQL.Length = 0;

                    if (String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area) && String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad))
                    {
                        Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Datos.P_No_Solicitud_Pago,Comando_SQL);
                        foreach (DataRow Renglon in Dt_Partidas.Rows)
                        {
                            Mi_SQL.Length = 0;
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                            Mi_SQL.Append(" SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " + " + Renglon["importe"].ToString() + "");
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva_Anterior) + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = " + Renglon["PARTIDA_ID"].ToString() + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " = " + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString() + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " = " + Renglon["PROGRAMA_ID"].ToString());
                            Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                            Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                        }
                        Mi_SQL.Length = 0;
                        //Actualiza el saldo de la reserva
                        Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                        Mi_SQL.Append(" SET " + Ope_Psp_Reservas.Campo_Saldo + " = " + Ope_Psp_Reservas.Campo_Saldo + " - " + Datos.P_Monto);
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva_Anterior));
                        Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    }
                    if(Datos.P_Modifica_Solicitud=="S"){
                        Mi_SQL.Length = 0;
                        Mi_SQL.Append("SELECT "+Ope_Con_Solicitud_Pagos_Detalles.Campo_Archivo +" FROM " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                        //Comando_SQL.CommandText = Mi_SQL.ToString();
                        Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                        Da_SQL.SelectCommand = Comando_SQL;
                        Da_SQL.Fill(Ds_SQL);
                        Dt_Detalles_Anteriores = Ds_SQL.Tables[0];
                        // Dt_Detalles_Anteriores = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0]; 
                        Mi_SQL.Length = 0;
                        //Elimina el registro de los detalles al cual pertenece la solicitud de pago que fue seleccionado por el usuario
                        Mi_SQL.Append("DELETE FROM " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                        Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                        Directorio_Destino = HttpContext.Current.Server.MapPath("~") + "\\Archivo_Solicitud_Pagos\\" + Datos.P_No_Solicitud_Pago + "\\";
                        foreach (DataRow Renglon in Datos.P_Dt_Detalles_Solicitud.Rows)
                        {
                            Mi_SQL.Length = 0;
                            Mi_SQL.Append("INSERT INTO " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                            Mi_SQL.Append(" (" + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura + ", ");
                            Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Factura
                                                + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura
                                                + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID +
                                                  ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Documento + ",");
                            Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Archivo
                                                + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Ruta
                                                + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID +
                                                ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva
                                                + ", " +  Ope_Con_Solicitud_Pagos_Detalles.Campo_IEPS +
                                                  ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID + ",");
                            Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Nombre_Proveedor_Factura
                                                + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion
                                                + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR +
                                                ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Retencion_IVA
                                                + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula +
                                                ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_RFC +
                                                  ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_CURP +                             
                                                  ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISH + ",");
                            Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Usuario_Creo + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Creo + ")");
                            Mi_SQL.Append(" VALUES ('" + Datos.P_No_Solicitud_Pago + "', '" + Renglon["No_Documento"].ToString() + "', '" + String.Format("{0:dd/MM/yyyy}", Renglon["Fecha_Documento"]) + "',");
                            Mi_SQL.Append(" " + Renglon["Monto"].ToString().Replace(",", "") + ",'" +
                                                Renglon["Partida_Id"].ToString() +
                                                "','" + Datos.P_Tipo_Documento + "','" +
                                                Renglon["Archivo"].ToString() + "','");
                            if (Renglon["Archivo"].ToString() != null && Renglon["Archivo"].ToString() != "")
                            {
                                Mi_SQL.Append(Directorio_Destino + Renglon["Archivo"].ToString() + "','");
                            }
                            else
                            {
                                Mi_SQL.Append("','");
                            }
                            Mi_SQL.Append(Renglon["Proyecto_Programa_Id"].ToString() + "'," +
                            Renglon["iva"].ToString() +
                            "," + Renglon["IEPS"].ToString() +
                            ",'" + Renglon["Fte_Financiamiento_Id"].ToString() + "','" +
                            Renglon["Nombre_Proveedor_Fact"].ToString() + "','" +
                            Renglon["Operacion"].ToString() +
                            "'," + Renglon["Retencion_ISR"].ToString() + "," +
                            Renglon["Retencion_IVA"].ToString() +
                            "," + Renglon["Retencion_Celula"].ToString() + ",'" +
                            Renglon["RFC"].ToString() +
                            "','" + Renglon["CURP"].ToString() + "'" +
                            ",'" + Renglon["ISH"].ToString() + "','" +
                            Datos.P_Nombre_Usuario + "', GETDATE())");// + Datos.P_No_Factura + "',");
                            Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                            Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                            if (Renglon["Archivo"].ToString() != null && Renglon["Archivo"].ToString() != "")
                            {
                                foreach (DataRow x in Dt_Detalles_Anteriores.Rows)
                                {
                                    if (Renglon["Archivo"].ToString() == x["Archivo"].ToString())
                                    {
                                        Insertar = false;
                                    }
                                }
                                if (Insertar == true)
                                {
                                    System.IO.File.Copy(Renglon["Ruta"].ToString(), Directorio_Destino + Renglon["Archivo"].ToString());
                                    System.IO.File.Delete(Renglon["Ruta"].ToString());
                                }
                            }
                        }
                        Directorio_Destino = HttpContext.Current.Server.MapPath("~") + "\\Archivo_Solicitud_Pagos\\Temporal";
                        String[] archivos = System.IO.Directory.GetFiles(Directorio_Destino);
                        int a;
                        for (a = 0; a < archivos.Length; a++)
                        {
                            System.IO.File.Delete(a.ToString());
                        }
                    }
                    //Transaccion_SQL.Commit(); //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
                    if (String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area) && String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad))
                    {
                        Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Datos.P_No_Solicitud_Pago,Comando_SQL);
                        foreach (DataRow Renglon in Dt_Partidas.Rows)
                        {
                            Mi_SQL.Length = 0;
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                            Mi_SQL.Append(" SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " - " + Renglon["importe"].ToString() + "");
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva_Anterior) + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = " + Renglon["PARTIDA_ID"].ToString() + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " = " + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString() + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " = " + Renglon["PROGRAMA_ID"].ToString());
                            Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                            Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                            //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        }
                    }
                    if (String.IsNullOrEmpty(Datos.P_Modifica_Solicitud))
                    {

                         Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Datos.P_No_Solicitud_Pago,Comando_SQL);
                        foreach (DataRow Renglon in Dt_Partidas.Rows)
                        {
                            Mi_SQL.Length = 0;
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                            Mi_SQL.Append(" SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " + " + Renglon["importe"].ToString() + "");
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva_Anterior) + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = " + Renglon["PARTIDA_ID"].ToString() + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " = " + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString() + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " = " + Renglon["PROGRAMA_ID"].ToString());
                            Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                            Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                            //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        }
                    }
                    if (Datos.P_Cmmd == null)
                    {
                        Transaccion_SQL.Commit();
                    }
                    return Datos.P_No_Solicitud_Pago;
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    Conexion_Base.Close();
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Rechaza_Solicitud_Pago_Sin_Poliza
            /// DESCRIPCION : Modifica los datos de la Solicitud de Pago con lo que fueron 
            ///               introducidos por el usuario
            /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
            ///                       proporcionados por el usuario y van a sustituir a los datos que se
            ///                       encuentran en la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 17-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static String Rechaza_Solicitud_Pago_Sin_Poliza(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                //String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
                StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos    
                DataTable Dt_Detalles_Anteriores = new DataTable();
                DataTable Dt_Partidas = new DataTable();
                DataTable Dt_solicitud = new DataTable();
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {

                    Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_No_Poliza)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Poliza_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Mes_Ano)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Datos.P_Tipo_Solicitud_Pago_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Jefe_Area + " = '" + Datos.P_Empleado_ID_Jefe_Area + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Contabilidad + " = '" + Datos.P_Empleado_ID_Contabilidad + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Concepto)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Concepto + " = '" + Datos.P_Concepto + "', ");
                    if (Datos.P_Monto > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto + " = " + Datos.P_Monto + ", ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Jefe)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Jefe + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Jefe) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Contabi)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Contabi) + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Jefe_Area + " = '" + Datos.P_Comentarios_Jefe_Area + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Contabilidad + " = '" + Datos.P_Comentarios_Contabilidad + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Comentario_Recepcion)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Recepcion_Doc + " = '" + Datos.P_Comentario_Recepcion + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Usuario_Recibio)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio + " = '" + Datos.P_Usuario_Recibio + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_recepcion_Documentos)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recepcion_Documentos + " = '" + Datos.P_Fecha_recepcion_Documentos + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Empleado_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + " = '" + Datos.P_Empleado_ID + "', ");
                    if (!String.IsNullOrEmpty(Datos.P_Usuario_Recibio))
                    {
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Pagado + " = '" + Datos.P_Usuario_Recibio + "', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Pagado + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Ejercido + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Contabilidad + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autoriza_Rechaza_Ejercido + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Ejercido + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Contabilidad + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Contabilidad + " = '', ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Ejercido + " = '',");
                    }
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + " = '" + Datos.P_Nombre_Usuario + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + " = GETDATE()");
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    Mi_SQL.Length = 0;
                    ///Transaccion_SQL.Commit(); //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    return Datos.P_No_Solicitud_Pago;
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }

                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Alta_Solicitud_Pago_Factoraje
            /// DESCRIPCION         : Consulta para cambiar el estatus de la recepcion de documentos
            /// PARAMETROS          : Autoriza_Solicitud_Negocio: Recibe los datos proporcionados por el usuario.
            /// CREO                : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO          : 22/agosto/2012
            /// MODIFICO            :
            /// FECHA_MODIFICO      :
            /// CAUSA_MODIFICACION  :
            ///*******************************************************************************
            public static void Alta_Solicitud_Pago_Factoraje(Cls_Ope_Con_Solicitud_Pagos_Negocio Autoriza_Solicitud_Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder();
                try
                {
                    Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = '" + Autoriza_Solicitud_Negocio.P_Proveedor_ID + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + " = '" + Autoriza_Solicitud_Negocio.P_Nombre_Usuario + "', ");
                    //Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = '" + Autoriza_Solicitud_Negocio.P_Proveedor_ID + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + " = GETDATE() ");
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Autoriza_Solicitud_Negocio.P_No_Solicitud_Pago + "'");

                    SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Eliminar_Solicitud_Pago
            /// DESCRIPCION : Elimina la Solicitud de pago que fue seleccionada por el usuario de la BD
            /// PARAMETROS  : Datos: Obtiene que Cuenta Contable desea eliminar de la BD
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 17-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static void Eliminar_Solicitud_Pago(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene los datos de la inserción a realizar a la base de datos
                SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
                SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
                SqlTransaction Transaccion_SQL;                                                  //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;
                try
                {
                    //Elimina el registro al cual pertenece la solicitud de pago que fue seleccionado por el usuario
                    Mi_SQL.Append("DELETE FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                    Mi_SQL.Length = 0;
                    //Actualiza el saldo de la reserva
                    Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                    Mi_SQL.Append(" SET " + Ope_Psp_Reservas.Campo_Saldo + " = " + Ope_Psp_Reservas.Campo_Saldo + " + " + Datos.P_Monto);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva));
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos

                    Transaccion_SQL.Commit();                    //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
                }
                catch (SqlException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    Conexion_Base.Close();
                }
            }
        #endregion
        #region(Consultas)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consultar_Solicitud_Pago
            /// DESCRIPCION : Consulta todos los datos que se tienen registrados en la base
            ///               de datos ya sea de acuerdo a los parametros proporcionado pos el
            ///               usuario o todos
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 17-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consultar_Solicitud_Pago(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                try
                {   
                    Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + "." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + " AS Solicitud, ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario);
                    Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + ", " + Cat_Dependencias.Tabla_Cat_Dependencias + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ", " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva);
                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                    Mi_SQL.Append(" AND " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + "." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID);
                    Mi_SQL.Append(" = " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID);
                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID);
                    Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    if (!String.IsNullOrEmpty(Datos.P_No_Solicitud_Pago)) Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                    if (!String.IsNullOrEmpty(Datos.P_Estatus)) Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "'");
                    if (!String.IsNullOrEmpty(Datos.P_Dependencia_ID)) Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "'");
                    if (!String.IsNullOrEmpty(Datos.P_Dependencia_Grupo)) Mi_SQL.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID  + " = '" + Datos.P_Dependencia_Grupo + "'");
                    if (!String.IsNullOrEmpty(Datos.P_Recurso)) Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Recurso  + " = '" + Datos.P_Recurso + "'");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID)) Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Datos.P_Tipo_Solicitud_Pago_ID + "'");
                    if (Datos.P_No_Reserva > 0) Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = " + Datos.P_No_Reserva);
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicial) && !String.IsNullOrEmpty(Datos.P_Fecha_Final))
                    {
                        Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud);
                        Mi_SQL.Append(" BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00'");
                        Mi_SQL.Append(" AND '" + Datos.P_Fecha_Final + " 23:59:59'");
                    }
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consultar_Datos_Solicitud_Finiquito
            /// DESCRIPCION : Consulta todos los datos que se tienen registrados en la base
            ///               de datos ya sea de acuerdo a los parametros proporcionado pos el
            ///               usuario o todos
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 14-Agosto-2013
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consultar_Datos_Solicitud_Finiquito(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                SqlDataAdapter Da_SQL = new SqlDataAdapter();
                DataSet Ds_SQL = new DataSet();
                DataTable Dt_Psp = new DataTable();
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {   
                    Mi_SQL.Append("SELECT * FROM " + Ope_Con_Solicitud_Finiquito_Det.Tabla_Ope_Con_Solicitud_Finiquito_Det);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Finiquito_Det.Campo_No_Solicitud_Pago+"='"+Datos.P_No_Solicitud_Pago+"'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Psp = Ds_SQL.Tables[0];
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    return Dt_Psp;
                    // return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consultar_Solicitud_Pago_Por_Gurpo
            /// DESCRIPCION : Consulta todos los datos que se tienen registrados en la base
            ///               de datos ya sea de acuerdo a los parametros proporcionado pos el
            ///               usuario o todos
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        :Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 17-Agosto-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consultar_Solicitud_Pago_Por_Gurpo(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                try
                {
                    Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + ", ");
                    Mi_SQL.Append(Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + "." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + " AS Solicitud, ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario);
                    Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + ", " + Cat_Dependencias.Tabla_Cat_Dependencias + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ", " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva);
                    Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                    Mi_SQL.Append(" AND " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + "." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID);
                    Mi_SQL.Append(" = " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID);
                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID);
                    Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    if (!String.IsNullOrEmpty(Datos.P_No_Solicitud_Pago)) Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                    if (!String.IsNullOrEmpty(Datos.P_Estatus)) Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "'");
                    if (!String.IsNullOrEmpty(Datos.P_Dependencia_ID)) Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "'");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID)) Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Datos.P_Tipo_Solicitud_Pago_ID + "'");
                    if (Datos.P_No_Reserva > 0) Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = " + Datos.P_No_Reserva);
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicial) && !String.IsNullOrEmpty(Datos.P_Fecha_Final))
                    {
                        Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud);
                        Mi_SQL.Append(" BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00'");
                        Mi_SQL.Append(" AND '" + Datos.P_Fecha_Final + " 23:59:59'");
                    }
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Reservas
            /// DESCRIPCION : Consulta todas las reservas que tengan un saldo mayor a 0
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Reservas(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                try
                {
                    Mi_SQL.Append("SELECT " + Ope_Psp_Reservas.Campo_No_Reserva + ",");
                    Mi_SQL.Append(" (LTRIM(RTRIM(STR(" + Ope_Psp_Reservas.Campo_No_Reserva + ")))+'-'+" + Ope_Psp_Reservas.Campo_Concepto + ") AS Reserva");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_Saldo + " > 0");
                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Campo_Estatus + " ='GENERADA'");
                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Campo_Reserva_Directa + " ='SI'");
                    if (!String.IsNullOrEmpty(Datos.P_Dependencia_ID)) 
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Campo_Dependencia_ID + " IN (");
                        Mi_SQL.Append("SELECT " + Cat_Dependencias.Campo_Dependencia_ID + 
                        " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                        " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " ='" + Cls_Sessiones.Dependencia_ID_Empleado + "'" +
                        " UNION ALL " +
                        "SELECT " + Cat_Dependencias.Campo_Dependencia_ID +                        
                        " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                        " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " IN (SELECT " +
                        Cat_Det_Empleado_UR.Campo_Dependencia_ID + " FROM " + Cat_Det_Empleado_UR.Tabla_Cat_Det_Empleado_UR +
                        " WHERE " + Cat_Det_Empleado_UR.Campo_Empleado_ID + " ='" + Cls_Sessiones.Empleado_ID.Trim() + "'))");
                    }
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Datos_Reserva
            /// DESCRIPCION : Consulta todos los datos de la reserva que fue seleccionada por
            ///               el usuario
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Datos_Reserva(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                try
                {
                    Mi_SQL.Append("SELECT " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + " AS Reservado, ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Proveedor_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Empleado_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Tipo_Solicitud_Pago_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles  + "." + Ope_Psp_Reservas_Detalles.Campo_Saldo + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial  + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Saldo + " As Saldo_Total, ");
                    Mi_SQL.Append("(" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                    Mi_SQL.Append("+'-'+" + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave);
                    Mi_SQL.Append("+'-'+" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                    Mi_SQL.Append("+'-'+" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                    Mi_SQL.Append("+'-'+" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ") AS Codigo_Programatico, ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS Fuente_Financiamiento, ");
                    Mi_SQL.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS Area_Funcional, ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS Proyectos_Programas, ");
                    Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS Dependencia, ");
                    Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Descripcion + " AS Partida, (");
                    Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + "+"+Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Descripcion + " ) AS Nombre_Partida, ");
                    Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Cuenta_Contable_ID);
                    Mi_SQL.Append(" FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ", " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + ", ");
                    Mi_SQL.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + ", " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + ", ");
                    Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + ", " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas+","+Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles );
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID);
                    Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID);
                    Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID);
                    Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                    Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_SQL.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_SQL.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                    Mi_SQL.Append(" = " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                    Mi_SQL.Append(" AND " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva));
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Datos_Solicitud_Pago
            /// DESCRIPCION : Consulta todos los datos de la solicitud de pago que selecciono
            ///               el usuario
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 19-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Datos_Solicitud_Pago(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                SqlDataAdapter Da_SQL = new SqlDataAdapter();
                DataSet Ds_SQL = new DataSet();
                DataTable Dt_Psp = new DataTable();
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Acreedor_ID  + " as Pro_Cuenta_Acreedor, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Contratista_ID + " as Pro_Cuenta_Contratista, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Anticipo_ID + " as Pro_Cuenta_Anticipos, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Deudor_ID + " as Pro_Cuenta_Deudor, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Judicial_ID + " as Pro_Cuenta_Judicial, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Nomina_ID + " as Pro_Cuenta_Nomina, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Predial_ID + " as Pro_Cuenta_Predial, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID + " as Pro_Cuenta_Proveedor, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + " as Proveedor, ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Cuenta_Banco_Pago_ID + ", "); 
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Cuenta_Contable_ID + " as Empleado_Cuenta, (");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + "+' '+"+Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + " )as Empleado, ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " , ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo+ ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo+ ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto+ ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + ".Factoraje_Proveedor_ID, ");
                    Mi_SQL.Append(" (LTRIM(RTRIM(STR(" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                    Mi_SQL.Append(")))+'-'+" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ") AS Reserva, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + " AS Cuenta_Contable_Reserva, ");
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", 
                    Mi_SQL.Append("SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + ") AS Monto_Partida, SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR + ") AS Monto_ISR, SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula + ") AS Monto_Cedular, SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".CAP) AS Monto_CAP , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".DIVO) AS Monto_DIVO , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".SEFUPU) AS Monto_SEFUPU , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".ICIC) AS Monto_ICIC , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".RAPCE) AS Monto_RAPCE , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".Lab_Control_C) AS Monto_Lab_Control_C , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".OBS) AS Monto_OBS , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".Est_Y_Proy) AS Monto_Est_Y_Proy , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".Amortizado) AS Monto_Armonizado , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva + ") AS IVA_MONTO ");
                    Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ON  " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " ON  " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + " = ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " ON  " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " ON  " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + " = ");
                    Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                    //Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " ON  " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = ");
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    //    " AND " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID +" = ");
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " AND ");
                    Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AND ");
                    Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " AND ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " AND ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " ON  " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + " = ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ");
                    Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles+"."+ Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID+"=");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado +"."+Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'");
                    Mi_SQL.Append(" Group by "+ Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Acreedor_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Contratista_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Deudor_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Anticipo_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Judicial_ID+ ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Nomina_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Predial_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Cuenta_Banco_Pago_ID + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Cuenta_Contable_ID + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " , ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + ".Factoraje_Proveedor_ID, ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Cuenta_Contable_ID + ", ");
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Psp = Ds_SQL.Tables[0];
                                
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    return Dt_Psp;
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Datos_Solicitud_Pago_Gastos
            /// DESCRIPCION : Consulta todos los datos de la solicitud de pago que selecciono
            ///               el usuario
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Sergio Manuel Gallardo
            /// FECHA_CREO  : 19-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Datos_Solicitud_Pago_Gastos(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                SqlDataAdapter Da_SQL = new SqlDataAdapter();
                DataSet Ds_SQL = new DataSet();
                DataTable Dt_Psp = new DataTable();
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Acreedor_ID + " as Pro_Cuenta_Acreedor, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Contratista_ID + " as Pro_Cuenta_Contratista, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Anticipo_ID + " as Pro_Cuenta_Anticipos, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Deudor_ID + " as Pro_Cuenta_Deudor, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Judicial_ID + " as Pro_Cuenta_Judicial, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Nomina_ID + " as Pro_Cuenta_Nomina, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Predial_ID + " as Pro_Cuenta_Predial, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID + " as Pro_Cuenta_Proveedor, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + " as Proveedor, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Cuenta_Contable_ID + " as Empleado_Cuenta, (");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + " )as Empleado, ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " , ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + ".Factoraje_Proveedor_ID, ");
                    Mi_SQL.Append(" (LTRIM(RTRIM(STR(" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                    Mi_SQL.Append(")))+'-'+" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ") AS Reserva, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + " AS Cuenta_Contable_Reserva, ");
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", 
                    Mi_SQL.Append("SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + ") AS Monto_Partida, SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR + ") AS Monto_ISR, SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula + ") AS Monto_Cedular, SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".CAP) AS Monto_CAP , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".DIVO) AS Monto_DIVO , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".SEFUPU) AS Monto_SEFUPU , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".ICIC) AS Monto_ICIC , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".RAPCE) AS Monto_RAPCE , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".Lab_Control_C) AS Monto_Lab_Control_C , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".OBS) AS Monto_OBS , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".Est_Y_Proy) AS Monto_Est_Y_Proy , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".Amortizado) AS Monto_Armonizado , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva + ") AS IVA_MONTO ");
                    Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ON  " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " ON  " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + " = ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " ON  " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " ON  " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + " = ");
                    Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                    //Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " ON  " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = ");
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    //    " AND " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID +" = ");
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " AND ");
                    Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AND ");
                    Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " AND ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " AND ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " ON  " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + " = ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ");
                    Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID + "=");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'");
                    Mi_SQL.Append(" Group by " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Acreedor_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Contratista_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Deudor_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Anticipo_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Judicial_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Nomina_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Predial_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Cuenta_Contable_ID + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " , ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + ".Factoraje_Proveedor_ID, ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Cuenta_Contable_ID + ", ");
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Psp = Ds_SQL.Tables[0];

                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    return Dt_Psp;
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Datos_Solicitud_Pago_Servicios_Generales
            /// DESCRIPCION : Consulta todos los datos de la solicitud de pago que selecciono
            ///               el usuario
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 19-Septiembre-2013
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Datos_Solicitud_Pago_Servicios_Generales(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                SqlDataAdapter Da_SQL = new SqlDataAdapter();
                DataSet Ds_SQL = new DataSet();
                DataTable Dt_Psp = new DataTable();
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID + " as Pro_Cuenta_Proveedor, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + " as Proveedor, ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Cuenta_Contable_ID + " as Empleado_Cuenta, (");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + "+' '+"+Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + " )as Empleado, ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " , ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo+ ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo+ ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto+ ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + ".Factoraje_Proveedor_ID, ");
                    Mi_SQL.Append(" (LTRIM(RTRIM(STR(" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                    Mi_SQL.Append(")))+'-'+" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ") AS Reserva, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + " AS Cuenta_Contable_Reserva, ");
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", 
                    Mi_SQL.Append("SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + ") AS Monto_Partida, SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR + ") AS Monto_ISR, SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula + ") AS Monto_Cedular, SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".CAP) AS Monto_CAP , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".DIVO) AS Monto_DIVO , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".SEFUPU) AS Monto_SEFUPU , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".ICIC) AS Monto_ICIC , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".RAPCE) AS Monto_RAPCE , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".Lab_Control_C) AS Monto_Lab_Control_C , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".OBS) AS Monto_OBS , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".Est_Y_Proy) AS Monto_Est_Y_Proy , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".Amortizado) AS Monto_Armonizado  , SUM(");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva + ") AS IVA_MONTO ");
                    Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ON  " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " ON  " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + " = ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " ON  " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " ON  " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + " = ");
                    Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                    //Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " ON  " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = ");
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    //    " AND " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID +" = ");
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " AND ");
                    Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AND ");
                    Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " AND ");
                    Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " AND ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " ON  " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + " = ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ");
                    Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles+"."+ Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID+"=");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado +"."+Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Dependencia_ID + "=");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID + "=");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID + "=");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'");
                    Mi_SQL.Append(" Group by "+ Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Acreedor_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Contratista_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Deudor_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Anticipo_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Judicial_ID+ ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Nomina_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Predial_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Cuenta_Contable_ID + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + ", ");
                    Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " , ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + ".Factoraje_Proveedor_ID, ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Cuenta_Contable_ID + ", ");
                    //Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Psp = Ds_SQL.Tables[0];
                                
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    return Dt_Psp;
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consultar_Datos_Poliza_Solicitud
            /// DESCRIPCION : Consulta todos los datos de la solicitud de pago que selecciono
            ///               el usuario
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 19-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consultar_Datos_Poliza_Solicitud(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                SqlDataAdapter Da_SQL = new SqlDataAdapter();
                DataSet Ds_SQL = new DataSet();
                DataTable Dt_Psp = new DataTable();
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo+ ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo+ ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto+ ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Poliza);
                    Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Psp = Ds_SQL.Tables[0];
                                
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    return Dt_Psp;
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }
                }
            }
        ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cuenta_Contable_Proveedor
            /// DESCRIPCION : Consulta si el proveedor proporcionado tiene una cuenta contable
            ///               asiganada
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 23-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Cuenta_Contable_Proveedor(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                try
                {
                    Mi_SQL.Append("SELECT " + Cat_Com_Proveedores.Campo_Proveedor_ID + ", " + Cat_Com_Proveedores.Campo_Nombre + ", ");
                    Mi_SQL.Append(Cat_Com_Proveedores.Campo_Cuenta_Contable_ID);
                    Mi_SQL.Append(" FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores);
                    Mi_SQL.Append(" WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "'");
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Detalles_Solicitud
            /// DESCRIPCION : Consulta todos los datos de la solicitud de pago que selecciono
            ///               el usuario
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 05-Enero-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Detalles_Solicitud(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                SqlDataAdapter Da_SQL = new SqlDataAdapter();
                DataSet Ds_SQL = new DataSet();
                DataTable Dt_Psp = new DataTable();
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    Mi_SQL.Append("SELECT detalles.*, partida.Descripcion FROM " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " detalles," + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas+" partida");
                    Mi_SQL.Append(" WHERE detalles." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID +" = partida."+ Cat_Sap_Partidas_Especificas.Campo_Partida_ID +" AND "+ Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Psp = Ds_SQL.Tables[0];
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    return Dt_Psp;
                    //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Detalles_Solicitud_Finiquito
            /// DESCRIPCION : Consulta todos los datos de la solicitud de pago que selecciono
            ///               el usuario
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 12/Agosto/2013
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Detalles_Solicitud_Finiquito(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                SqlDataAdapter Da_SQL = new SqlDataAdapter();
                DataSet Ds_SQL = new DataSet();
                DataTable Dt_Psp = new DataTable();
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    Mi_SQL.Append("SELECT * FROM " + Ope_Con_Solicitud_Finiquito_Det.Tabla_Ope_Con_Solicitud_Finiquito_Det);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Finiquito_Det.Campo_No_Solicitud_Pago + " ='" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Dt_Psp = Ds_SQL.Tables[0];
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    return Dt_Psp;
                    //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Solicitud_Pagos_Anticipo_Estimaciones
            /// DESCRIPCION : Consulta todos los datos de la solicitud de pago que selecciono
            ///               el usuario
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 28/Sep/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Solicitud_Pagos_Anticipo_Estimaciones(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                try
                {
                    Mi_SQL.Append("SELECT DETALLE." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion + ", DETALLE." + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + ", DETALLE.");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + ", DETALLE." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva + ",(DETALLE." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + " + DETALLE.");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva + ") AS SUBTOTAL, DETALLE.Amortizado,((DETALLE." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + " + DETALLE.");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva + ")- DETALLE.Amortizado) AS TOTAL , DETALLE.Alcance_Neto, SOLICITUD.");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Pago + ", PAGO." + Ope_Con_Pagos.Campo_Fecha_Pago + ", DETALLE." + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura + ", SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + " FROM ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles+" DETALLE");
                    Mi_SQL.Append(" LEFT OUTER JOIN "+ Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos +" SOLICITUD ON SOLICITUD."+Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago+"=DETALLE."+Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " PAGO ON PAGO." + Ope_Con_Pagos.Campo_No_Pago + "=SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Pago);
                    if (!String.IsNullOrEmpty(Datos.P_No_Solicitud_Pago))
                    {
                        Mi_SQL.Append(" WHERE DETALLE."+Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago +"='"+Datos.P_No_Solicitud_Pago+"'");
                    }

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Solicitud_Detalles_Pagos_Anticipo_Estimaciones
            /// DESCRIPCION : Consulta todos los datos de la solicitud de pago que selecciono
            ///               el usuario
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 28/Sep/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Solicitud_Detalles_Pagos_Anticipo_Estimaciones(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                try
                {
                    Mi_SQL.Append("SELECT DETALLE." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Creo + ", DETALLE.ICIC, DETALLE.");
                    Mi_SQL.Append("SEFUPU, DETALLE.OBS, DETALLE.Lab_Control_C, DETALLE.");
                    Mi_SQL.Append("Est_Y_Proy, DETALLE." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Factura + ",(DETALLE.ICIC + DETALLE.");
                    Mi_SQL.Append("SEFUPU + DETALLE.OBS + DETALLE.Lab_Control_C + DETALLE.Est_Y_Proy) AS TOTAL");
                    Mi_SQL.Append(", DETALLE." + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura + " FROM ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " DETALLE");
                    //Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SOLICITUD ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "=DETALLE." + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago);
                    //Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " PAGO ON PAGO." + Ope_Con_Pagos.Campo_No_Pago + "=SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Pago);
                    if (!String.IsNullOrEmpty(Datos.P_No_Solicitud_Pago))
                    {
                        Mi_SQL.Append(" WHERE DETALLE." + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'");
                    }

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
            
        ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Parametro_ISR
            /// DESCRIPCION : consulta en la tabla de ISR el porcentaje a disminuar  a la factura del personal dependiendo del salario a obtener
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 05-Enero-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Parametro_ISR(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                try
                {
                    Mi_SQL.Append("SELECT * FROM " + Tab_Nom_ISR.Tabla_Tab_Nom_ISR + " WHERE " + Tab_Nom_ISR.Campo_Limite_Inferior + " <='"+Datos.P_Limite_Inferior+"'");
                    //Mi_SQL.Append(" ORDER BY " + Tab_Nom_ISR.Campo_Limite_Inferior + " DESC" );
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Solicitud_Pagos_con_Detalles
            /// DESCRIPCION : Consulta todos los datos de la solicitud de pago que selecciono
            ///               el usuario
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 06-Enero-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Solicitud_Pagos_con_Detalles(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                try
                {
                    Mi_SQL.Append("SELECT SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", SOLICITUD.");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto + ", SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion + " AS T_OPERACION, DETALLES.");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura + ", DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + " AS IMPORTE , RESERVA.");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_No_Reserva + " AS RESERVA , PROVEEDOR." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR , PROVEEDOR.");
                    Mi_SQL.Append(Cat_Com_Proveedores.Campo_RFC + " AS RFC_PROVEEDOR , PROVEEDOR." + Cat_Com_Proveedores.Campo_Correo_Electronico + " AS CORREO_PROVEEDOR , SOLICITUD.");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + " AS FECHA_SOLICITUD , DEPENDENCIA.");
                    Mi_SQL.Append(Cat_Dependencias.Campo_Nombre +" AS DEPENDENCIA , RESERVA." + Ope_Psp_Reservas.Campo_Importe_Inicial + ", EMPLEADO.");
                    Mi_SQL.Append(Cat_Empleados.Campo_Nombre  + " AS NOMBRE_EMPLEADO , EMPLEADO." + Cat_Empleados.Campo_Apellido_Materno + " , EMPLEADO.");
                    Mi_SQL.Append(Cat_Empleados.Campo_Apellido_Paterno  + ", EMPLEADO." + Cat_Empleados.Campo_RFC + " AS RFC_EMPLEADO, EMPLEADO.");
                    Mi_SQL.Append(Cat_Empleados.Campo_Correo_Electronico + " AS CORREO_EMPLEADO , BANCO." + Cat_Nom_Bancos.Campo_Nombre + " AS BANCO_EMPLEADO, BANCO.");
                    Mi_SQL.Append(Cat_Nom_Bancos.Campo_Banco_ID + " AS BANCO_ID_EMPLEADO , BANCO." + Cat_Nom_Bancos.Campo_No_Cuenta + " as NO_CUENTA_BANCO_EMP, ");
                    Mi_SQL.Append(" PROVEEDOR." + Cat_Com_Proveedores.Campo_Cuenta + " AS CUENTA , PROVEEDOR." + Cat_Com_Proveedores.Campo_Banco_Proveedor_ID + " AS BANCO_ID , PROVEEDOR.");
                    Mi_SQL.Append(Cat_Com_Proveedores.Campo_Clabe + " AS CLABE , PROVEEDOR." + Cat_Com_Proveedores.Campo_Banco_Proveedor + " AS BANCO ,");
                    Mi_SQL.Append("( FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                    Mi_SQL.Append("+'-'+ AREA." + Cat_SAP_Area_Funcional.Campo_Clave);
                    Mi_SQL.Append("+'-'+ PROGRAMA." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                    Mi_SQL.Append("+'-'+ DEPENDENCIA." + Cat_Dependencias.Campo_Clave);
                    Mi_SQL.Append("+'-'+ PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Clave + ") AS Codigo_Programatico, ");
                    Mi_SQL.Append("( FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                    Mi_SQL.Append("+'-'+ AREA." + Cat_SAP_Area_Funcional.Campo_Clave);
                    Mi_SQL.Append("+'-'+ PROGRAMA." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                    Mi_SQL.Append("+'-'+ DEPENDENCIA." + Cat_Dependencias.Campo_Clave + ") AS Codigo_Dependencia ,' ' as CUENTA_A_PAGAR,' ' as CUENTA_DE_PAGO ");
                    Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos+" SOLICITUD");
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " DETALLES ON  SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = DETALLES.");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " RESERVA ON  SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = RESERVA.");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_No_Reserva);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " PROGRAMA ON  DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID + " = PROGRAMA.");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIA ON  RESERVA." + Ope_Psp_Reservas.Campo_Dependencia_ID + " = DEPENDENCIA.");
                    Mi_SQL.Append(Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " AREA ON  DEPENDENCIA." + Cat_Dependencias.Campo_Area_Funcional_ID + " = AREA.");
                    Mi_SQL.Append(Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FUENTE ON  DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID + " = FUENTE.");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDA ON  DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID + " = PARTIDA.");
                    Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDOR ON  RESERVA." + Ope_Psp_Reservas.Campo_Proveedor_ID + " = PROVEEDOR.");
                    Mi_SQL.Append(Cat_Com_Proveedores.Campo_Proveedor_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO ON  RESERVA." + Ope_Psp_Reservas.Campo_Empleado_ID + " = EMPLEADO.");
                    Mi_SQL.Append(Cat_Empleados.Campo_Empleado_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO ON  EMPLEADO." + Cat_Empleados.Campo_Banco_ID + " = BANCO.");
                    Mi_SQL.Append(Cat_Nom_Bancos.Campo_Banco_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO1 ON  PROVEEDOR." + Cat_Com_Proveedores.Campo_Banco_ID + " = BANCO1.");
                    Mi_SQL.Append(Cat_Nom_Bancos.Campo_Banco_ID);
                    Mi_SQL.Append(" WHERE SOLICITUD."+ Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Incluir_Reserva
            /// DESCRIPCION : Valida si una reserva tiene que ser incluida en la consulta de las reservas para las solicitudes de pago
            /// PARAMETROS  : Datos: Variable para la capa de negocios
            /// CREO        : Noe Mosqueda Valadez
            /// FECHA_CREO  : 13/Abril/2013 14:00
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static Boolean Incluir_Reserva(Cls_Ope_Con_Solicitud_Pagos_Negocio Datos)
            {
                //Declaracion de variables
                String Mi_SQL = String.Empty; //variable para la consulta
                Object aux; //variable auxiliar
                Boolean Resultado = true; //variable que indica si se tiene que incluir la reserva

                try
                {
                    //Construir la consulta
                    Mi_SQL = "SELECT COUNT(*) FROM OPE_CON_SOLICITUD_PAGOS WHERE NO_RESERVA = '" + Datos.P_No_Reserva.ToString().Trim() + "' AND ESTATUS<>'CANCELADO'";

                    //Ejecutar consulta
                    aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                    //Verificar si hay una solicitud de pago
                    if (Convert.ToInt32(aux) > 0)
                    {
                        //Consulta para verificar si la reserva es de tipo unica
                        Mi_SQL = "SELECT TIPO_RESERVA FROM OPE_PSP_RESERVAS WHERE NO_RESERVA = '" + Datos.P_No_Reserva.ToString().Trim() + "'";
                        aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                        //Verificar si es de tipo unica
                        if (aux != null)
                        {
                            if (aux.ToString().Trim() == "UNICA")
                            {
                                Resultado = false;
                            }
                        }
                    }

                    //Entregar resultado
                    return Resultado;
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
        #endregion
    }
}