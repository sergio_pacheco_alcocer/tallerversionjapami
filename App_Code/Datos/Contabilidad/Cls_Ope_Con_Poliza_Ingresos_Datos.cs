﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Parametros_Presupuestales.Datos;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Polizas.Negocios;

namespace JAPAMI.Ope_Con_Poliza_Ingresos.Datos
{
    public class Cls_Ope_Con_Poliza_Ingresos_Datos
    {
        #region Metodos
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Poliza_Ingresos
        ///DESCRIPCIÓN          : consulta para modificar los datos de la poliza de ingresos
        ///PARAMETROS           1 Dt_Datos: datos a afectar en la poliza de ingresos
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 20/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Boolean Alta_Poliza_Ingresos(DataTable Dt_Datos,SqlCommand Cmmd, String Referencia,
            String No_Poliza, String Tipo_Poliza_ID, String Mes_Anio, String Tipo_Poliza_Ing)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
            Boolean Operacion_Completa = false;//Estado de la operacion.
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Sql = new SqlDataAdapter();
            DataSet Ds_Sql = new DataSet();
            if (Cmmd != null)
            {
                Cmd = Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Trans.Connection;
                Cmd.Transaction = Trans;
            }
            String Rubro = String.Empty;
            String Tipo = String.Empty;
            String Clase = String.Empty;
            String Concepto = String.Empty;
            String Cuenta = String.Empty;
            String Partida = String.Empty;
            DataTable Dt_Registro = new DataTable();
            Int32 Registro_Ingreso = 0;
            DateTime Fecha = DateTime.Now;
            String MES = String.Format("{0:MM}", Fecha).ToUpper();
            String Cargo = String.Empty;
            String Abono = String.Empty;
            Double Importe = 0.00;
            Int32 No_Movimiento = 0;

            //obtenemos el numero de movimiento
            //Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Psp_Registro_Mov_Ingreso.Campo_No_Movimiento + "),0)");
            //Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso);
            //Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
            //No_Movimiento = Convert.ToInt32(Cmd.ExecuteScalar());
            ////No_Movimiento = Convert.ToInt32(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString().Trim()));
            //No_Movimiento = No_Movimiento + 1;
            
            //obtenemos el no de presipuesto id por si se tiene q insertar un registro no existente
            Mi_SQL = new StringBuilder();
            Mi_SQL.Append("SELECT MAX (" + Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID + ")");
            Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
            Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
            Registro_Ingreso = Convert.ToInt32(Cmd.ExecuteScalar());
            //Registro_Ingreso = Convert.ToInt32(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));
            if (Convert.IsDBNull(Registro_Ingreso))
            {Registro_Ingreso = 1;}
            else{Registro_Ingreso = Registro_Ingreso + 1;}

            try
            {
                if (Dt_Datos != null)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(No_Poliza.Trim()) && !String.IsNullOrEmpty(Tipo_Poliza_ID.Trim()) 
                            && !String.IsNullOrEmpty(Mes_Anio.Trim()) && !String.IsNullOrEmpty(Tipo_Poliza_Ing.Trim()))
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                //validamos si la fuente de financiamiento tiene dato y la dependencia viene vacia, 
                                //el programa lo validaremos mas adelante porque puede o no venir
                                if (!String.IsNullOrEmpty(Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim()) && String.IsNullOrEmpty(Dr["DEPENDENCIA_ID"].ToString().Trim()))
                                {
                                    Cuenta = Dr[Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();

                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT " + Cat_Con_Cuentas_Contables.Campo_Partida_ID);
                                    Mi_SQL.Append(" FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                                    Mi_SQL.Append(" WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = '" + Cuenta + "'");
                                    Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                                    Concepto = (String)Cmd.ExecuteScalar();
                                   // Concepto = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT " + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID);
                                    Mi_SQL.Append(" FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Concepto + "'");
                                    Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                                    Clase = (String)Cmd.ExecuteScalar();
                                    //Clase = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT " + Cat_Psp_Clase_Ing.Campo_Tipo_ID);
                                    Mi_SQL.Append(" FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Clase + "'");
                                    Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                                    Tipo = (String)Cmd.ExecuteScalar();
                                   // Tipo = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT " + Cat_Psp_Tipo.Campo_Rubro_ID);
                                    Mi_SQL.Append(" FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                                    Mi_SQL.Append(" WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Tipo + "'");
                                    Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                                    Rubro = (String)Cmd.ExecuteScalar();
                                   // Rubro = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                                    if (Convert.ToDouble(String.IsNullOrEmpty(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim()) ? "0" : Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "")) > 0)
                                    {
                                        if(Tipo_Poliza_Ing.Trim().Equals("DEVENGADO"))
                                        {
                                            Cargo = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado.Trim();
                                            Abono = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar.Trim();
                                        }
                                        else if (Tipo_Poliza_Ing.Trim().Equals("RECAUDADO"))
                                        {
                                            Cargo = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado.Trim();
                                            Abono = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado.Trim();
                                        }
                                        
                                        Importe = Convert.ToDouble(String.IsNullOrEmpty(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim()) ? "0" : Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",",""));

                                        //Primero se consulta si existe un registro en el presupuesto de ingreso 
                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                                        if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                        {
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                                        }
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now) + " ");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Rubro.Trim() + "' ");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Tipo.Trim() + "' ");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Clase.Trim() + "' ");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Concepto.Trim() + "' ");
                                        Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                                        Dt_Sql.SelectCommand = Cmd;
                                        Dt_Sql.Fill(Ds_Sql);
                                        Dt_Registro = Ds_Sql.Tables[0];
                                        //Dt_Registro = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                                        if (Dt_Registro.Rows.Count > 0)
                                        {
                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                            if (Tipo_Poliza_Ing.Trim().Equals("DEVENGADO"))
                                            {
                                                Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + " + ");
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ", ");
                                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + " - ");
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", ""));
                                            }
                                            else if (Tipo_Poliza_Ing.Trim().Equals("RECAUDADO"))
                                            {
                                                Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + " + ");
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ", ");
                                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + " - ");
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ", ");

                                                if (Convert.ToInt32(MES) == 1)
                                                {
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Enero + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Enero + " + ");
                                                    Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", ""));
                                                }
                                                else if (Convert.ToInt32(MES) == 2)
                                                {
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Febrero + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Febrero + " + ");
                                                    Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", ""));
                                                }
                                                else if (Convert.ToInt32(MES) == 3)
                                                {
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Marzo + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Marzo + " + ");
                                                    Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", ""));
                                                }
                                                else if (Convert.ToInt32(MES) == 4)
                                                {
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Abril + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Abril + " + ");
                                                    Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", ""));
                                                }
                                                else if (Convert.ToInt32(MES) == 5)
                                                {
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Mayo + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Mayo + " + ");
                                                    Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", ""));
                                                }
                                                else if (Convert.ToInt32(MES) == 6)
                                                {
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Junio + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Junio + " + ");
                                                    Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", ""));
                                                }
                                                else if (Convert.ToInt32(MES) == 7)
                                                {
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Julio + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Julio + " + ");
                                                    Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", ""));
                                                }
                                                else if (Convert.ToInt32(MES) == 8)
                                                {
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Agosto + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Agosto + " + ");
                                                    Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", ""));
                                                }
                                                else if (Convert.ToInt32(MES) == 9)
                                                {
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Septiembre + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Septiembre + " + ");
                                                    Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", ""));
                                                }
                                                else if (Convert.ToInt32(MES) == 10)
                                                {
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Octubre + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Octubre + " + ");
                                                    Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", ""));
                                                }
                                                else if (Convert.ToInt32(MES) == 11)
                                                {
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Noviembre + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Noviembre + " + ");
                                                    Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", ""));
                                                }
                                                else if (Convert.ToInt32(MES) == 12)
                                                {
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Diciembre + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Diciembre + " + ");
                                                    Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", ""));
                                                }
                                            }

                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                                            if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                                            }
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now) + " ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Rubro.Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Tipo.Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Clase.Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Concepto.Trim() + "' ");
                                            Cmd.CommandText = Mi_SQL.ToString();
                                            Cmd.ExecuteNonQuery();
                                        }
                                        else
                                        {
                                            //Se inserta el registro ya que no existe en el presupuesto
                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("INSERT INTO " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + " (");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                                            if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                            { Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + ", ");}
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", "+Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID  + ", "+Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", " + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero  + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero  + ", "+ Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo  + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril+ ", "+Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ", "+Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto+ ", "+ Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre+ ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ", "+Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ", " + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado  + ", "+Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion  + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ", "+Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ", " + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ", " + Ope_Psp_Presupuesto_Ingresos.Campo_Saldo + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Enero + ", " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Febrero  + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Marzo  + ", " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Abril + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Mayo + ", " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Junio + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Julio + ", " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Agosto + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Septiembre + ", " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Octubre + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Noviembre + ", " + Ope_Psp_Presupuesto_Ingresos.Campo_Acumulado_Diciembre + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Creo + ", " + Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Creo + ") ");
                                            Mi_SQL.Append("VALUES (" + Registro_Ingreso + ", '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "', ");
                                            if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                            { Mi_SQL.Append("'" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "', "); }
                                            Mi_SQL.Append("'" + Rubro.Trim() + "', '" + Tipo.Trim() + "', '");
                                            Mi_SQL.Append(Clase.Trim() + "', '" + Concepto.Trim() + "', " + String.Format("{0:yyyy}", DateTime.Now) + ", ");
                                            Mi_SQL.Append("0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00,");
                                            if (Tipo_Poliza_Ing.Trim().Equals("DEVENGADO"))
                                            {
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ", 0.00, ");
                                                Mi_SQL.Append("-" + Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ", 0.00, ");
                                            }
                                            else if (Tipo_Poliza_Ing.Trim().Equals("RECAUDADO"))
                                            {
                                                Mi_SQL.Append(" 0.00, " + Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ", ");
                                                Mi_SQL.Append("-" + Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ", 0.00, ");
                                            }

                                            if (Convert.ToDouble(MES) == 1)
                                            {
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ",");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append("0.00,");
                                            }
                                            if (Convert.ToDouble(MES) == 2)
                                            {
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ",");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append("0.00,");
                                            }
                                            if (Convert.ToDouble(MES) == 3)
                                            {
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ",");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append("0.00,");
                                            }
                                            if (Convert.ToDouble(MES) == 4)
                                            {
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ",");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append("0.00,");
                                            }
                                            if (Convert.ToDouble(MES) == 5)
                                            {
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ",");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append("0.00,");
                                            }
                                            if (Convert.ToDouble(MES) == 6)
                                            {
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ",");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append("0.00,");
                                            }
                                            if (Convert.ToDouble(MES) == 7)
                                            {
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ",");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append("0.00,");
                                            }
                                            if (Convert.ToDouble(MES) == 8)
                                            {
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ",");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append("0.00,");
                                            }
                                            if (Convert.ToDouble(MES) == 9)
                                            {
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ",");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append("0.00,");
                                            }
                                            if (Convert.ToDouble(MES) == 10)
                                            {
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ",");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append("0.00,");
                                            }
                                            if (Convert.ToDouble(MES) == 11)
                                            {
                                                 Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ",");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append("0.00,");
                                            }
                                            if (Convert.ToDouble(MES) == 12)
                                            {
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ",");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append("0.00,");
                                            }
                                            Mi_SQL.Append("'"+Cls_Sessiones.Nombre_Empleado.ToString() + "',GETDATE())");
                                            Cmd.CommandText = Mi_SQL.ToString();
                                            Cmd.ExecuteNonQuery();

                                            Registro_Ingreso++;
                                        }
                                        Operacion_Completa = true;
                                    }
                                    else if (Convert.ToDouble(String.IsNullOrEmpty(Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString().Trim()) ? "0" : Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString().Trim()) > 0)
                                    {
                                        if (Tipo_Poliza_Ing.Trim().Equals("DEVENGADO"))
                                        {
                                            Cargo = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado.Trim();
                                            Abono = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado.Trim();
                                        }
                                        else if (Tipo_Poliza_Ing.Trim().Equals("RECAUDADO"))
                                        {
                                            Cargo = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar.Trim();
                                            Abono = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado.Trim();
                                        }
                                        else
                                        {
                                            break;
                                        }

                                        Importe = Convert.ToDouble(String.IsNullOrEmpty(Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString().Trim()) ? "0" : Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString().Trim().Replace(",",""));
                                        Ds_Sql = new DataSet();
                                        //Primero se consulta si existe un registro en el presupuesto de ingreso 
                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                                        if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                        {Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");}
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now) + " ");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Rubro.Trim() + "' ");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Tipo.Trim() + "' ");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Clase.Trim() + "' ");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Concepto.Trim() + "' ");
                                        Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                                        Dt_Sql.SelectCommand = Cmd;
                                        Dt_Sql.Fill(Ds_Sql);
                                        Dt_Registro = Ds_Sql.Tables[0];
                                       // Dt_Registro = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                                        if (Dt_Registro.Rows.Count > 0)
                                        {
                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                            if (Tipo_Poliza_Ing.Trim().Equals("DEVENGADO"))
                                            {
                                                Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + " - ");
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString().Trim().Replace(",", "") + ", ");
                                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + " + ");
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString().Trim().Replace(",", ""));
                                            }
                                            else if (Tipo_Poliza_Ing.Trim().Equals("RECAUDADO"))
                                            {
                                                Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + " - ");
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString().Trim().Replace(",", "") + ", ");
                                                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + " = " + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + " + ");
                                                Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString().Trim().Replace(",", ""));
                                            }
                                            
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                                            if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                            {Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");}
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now) + " ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Rubro.Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Tipo.Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Clase.Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Concepto.Trim() + "' ");
                                            Cmd.CommandText = Mi_SQL.ToString();
                                            Cmd.ExecuteNonQuery();
                                        }
                                    }

                                    //insertamos los registros de los movimientos del presupuesto

                                    Insertar_Registro_Mov_Ing(Cargo.Trim(), Abono.Trim(), Importe.ToString().Trim(),
                                        Dr[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString().Trim(), Referencia.Trim(), No_Poliza.Trim(), Tipo_Poliza_ID.Trim(),
                                        Mes_Anio.Trim(), Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim(), Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim(), Rubro.Trim(),
                                        Tipo.Trim(), Clase.Trim(), Concepto.Trim(), String.Empty, Cls_Sessiones.Nombre_Empleado.Trim(), Cmd,
                                        "","","");

                                    No_Movimiento++;
                                }
                                else if (!String.IsNullOrEmpty(Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim()) && !String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()) && !String.IsNullOrEmpty(Dr["DEPENDENCIA_ID"].ToString().Trim()))
                                {
                                    Cuenta = Dr[Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();

                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("SELECT " + Cat_Con_Cuentas_Contables.Campo_Partida_ID);
                                    Mi_SQL.Append(" FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                                    Mi_SQL.Append(" WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = '" + Cuenta + "'");
                                    Cmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                                    Partida = (String)Cmd.ExecuteScalar();
                                   // Partida = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                                    if (Convert.ToDouble(String.IsNullOrEmpty(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim()) ? "0" : Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim()) > 0)
                                    {
                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                        Mi_SQL.Append(" SET " + Dr["MOMENTO_INICIAL"].ToString().Trim() + " = " + Dr["MOMENTO_INICIAL"].ToString().Trim() + " - ");
                                        Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + ", ");
                                        Mi_SQL.Append(Dr["MOMENTO_FINAL"].ToString().Trim() + " = " + Dr["MOMENTO_FINAL"].ToString().Trim() + " + ");
                                        Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Debe].ToString().Trim().Replace(",", "") + " ");
                                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now) + " ");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida + "' ");
                                        Cmd.CommandText = Mi_SQL.ToString();
                                        Cmd.ExecuteNonQuery();

                                        Operacion_Completa = true;
                                    }
                                    else if (Convert.ToDouble(String.IsNullOrEmpty(Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString().Trim()) ? "0" : Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString().Trim()) > 0)
                                    {
                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                        Mi_SQL.Append(" SET " + Dr["MOMENTO_INICIAL"].ToString().Trim() + " = " + Dr["MOMENTO_INICIAL"].ToString().Trim() + " + ");
                                        Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString().Trim().Replace(",", "") + ", ");
                                        Mi_SQL.Append(Dr["MOMENTO_FINAL"].ToString().Trim() + " = " + Dr["MOMENTO_FINAL"].ToString().Trim() + " - ");
                                        Mi_SQL.Append(Dr[Ope_Con_Polizas_Detalles.Campo_Haber].ToString().Trim().Replace(",", "") + " ");
                                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now) + " ");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida + "' ");
                                        Cmd.CommandText = Mi_SQL.ToString();
                                        Cmd.ExecuteNonQuery(); ;
                                    }
                                }
                            }
                        }
                        if (Cmmd == null)
                        {
                            Trans.Commit();
                        }
                       Operacion_Completa = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                Trans.Rollback();
                throw new Exception("Error al ejecutar los el alta de la poliza de ingresos. Error: [" + Ex.Message + "]");
            }
            finally
            {
                if (Cmmd == null)
                {
                    Cn.Close();
                }
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Insertar_Registro_Mov_Ing
        ///DESCRIPCIÓN          : consulta para insertar los datos de los movimientos de ingresos
        ///PARAMETROS           1 Cargo: Nombre Columna de ingresos a la que se realizara el cargo (Obligatorio)
        ///                     2 Abono: Nombre Columna de ingresos a la que se realizara el abono (Obligatorio)
        ///                     3 Importe: monto que se afectara en el presupuesto de ingresos (Obligatorio)
        ///                     4 Descripcion: aqui puede ir el concepto, justificacion o descripcion del movimiento (Opcional)
        ///                     5 Referencia: aqui puede ir la referencia motivo del movimiento (Opcional)
        ///                     6 No_Poliza: no de poliza que se genero para afectar el presupuesto de ingresos (Obligatorio)
        ///                     7 Tipo_Poliza_ID: tipo de poliza que se genero para afectar el presupuesto de ingresos (Obligatorio)
        ///                     8 Mes_Anio: mes y año de la poliza que se genero para afectar el presupuesto de ingresos (Obligatorio)
        ///                     9 Fte_Financiamiento_ID: fuente de financiamiento al que se afectara el ingreso (Obligatorio)
        ///                     10 Programa_ID: programa al que se afectara el ingreso, si es que se cuenta con el nombre del programa  (Opcional)
        ///                     11 Rubro_ID: rubro al que pertenece el concepto que afectara el presupuesto(Obligatorio)
        ///                     12 Tipo_ID: tipo al que pertenece el concepto que afectara el presupuesto (Obligatorio)
        ///                     13 Clase_ID: clase al que pertenece el concepto que afectara el presupuesto (Obligatorio)
        ///                     14 Concepto_ID: concepto que afectara el presupuesto (Obligatorio)
        ///                     15 SubConcepto_ID: subconcepto que se afectara el presupuesto, si es que llega a este nivel  (Opcional)
        ///                     16 Usuario: usuario que solicito realizar la poliza (Obligatorio)
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Agosto/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************************************
        public static Boolean Insertar_Registro_Mov_Ing(String Cargo, String Abono, String Importe, String Descripcion, String Referencia,
             String No_Poliza, String Tipo_Poliza_ID, String Mes_Anio, String Fte_Financiamiento_ID, String Programa_ID, String Rubro_ID,
             String Tipo_ID, String Clase_ID, String Concepto_ID, String SubConcepto_ID, String Usuario, SqlCommand Cmmd,
             String No_Poliza_PSP, String Tipo_Poliza_PSP, String Mes_Anio_PSP)
        {

            Boolean Operacion_Completa = false;
            Int32 No_Registros = 0;
            String Mov_Inicial = Cargo.Trim();
            String Mov_Final = Abono.Trim();
            Int32 No_Movimiento = 0;
            Double Importe_Registro = 0.00;

            StringBuilder Mi_SQL = new StringBuilder();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;

            if (Cmmd != null)
            {
                Cmd = Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Trans.Connection;
                Cmd.Transaction = Trans;
            }

            try
            {
                Importe_Registro = Convert.ToDouble(String.IsNullOrEmpty(Importe) ? "0" : Importe.Replace("$", "").Replace(",", ""));

                //Obtenemos los datos del movimiento inicial y final para obtener si es de por_recaudar a recaudado o
                //de recaudado a por_recaudar, y poder meter el momento intemedio que es el devengado
                if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar) &&
                    Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado))
                {
                    Cargo = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar;
                    Abono = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado;
                    No_Registros = 2;
                }
                else if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado) &&
                    Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar))
                {
                    Cargo = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado;
                    Abono = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado;
                    No_Registros = 2;
                }
                else if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Modificado) &&
                    Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar))
                {
                    if (Importe_Registro > 0)
                    {
                        Cargo = Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion;
                        Abono = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar;
                    }
                    else 
                    {
                        Cargo = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar;
                        Abono = Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion;
                    }
                    No_Registros = 1;
                }
                else
                {
                    No_Registros = 1;
                }

                //hacemos un ciclo while para manejar el numero de registros que insertaremos
                while (No_Registros > 0)
                {
                    //obtenemos el numero de movimiento
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Psp_Registro_Mov_Ingreso.Campo_No_Movimiento + "),0)");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso);
                    Cmd.CommandText = Mi_SQL.ToString();

                    No_Movimiento = Convert.ToInt32(Cmd.ExecuteScalar());
                    No_Movimiento = No_Movimiento + 1;

                    //insertamos el registro
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Ope_Psp_Registro_Mov_Ingreso.Tabla_Ope_Psp_Registro_Mov_Ingreso + "(");
                    Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_No_Movimiento + ", ");
                    if (!String.IsNullOrEmpty(Cargo.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Cargo + ", "); }
                    if (!String.IsNullOrEmpty(Abono.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Abono + ", "); }
                    if (!String.IsNullOrEmpty(Importe.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Importe + ", "); }
                    Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Fecha + ", ");
                    if (!String.IsNullOrEmpty(Descripcion.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Descripcion + ", "); }
                    if (!String.IsNullOrEmpty(Referencia.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Referencia + ", "); }
                    if (!String.IsNullOrEmpty(No_Poliza.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_No_Poliza + ", "); }
                    if (!String.IsNullOrEmpty(Tipo_Poliza_ID.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Tipo_Poliza_ID + ", "); }
                    if (!String.IsNullOrEmpty(Mes_Anio.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Mes_Ano + ", "); }
                    if (!String.IsNullOrEmpty(Fte_Financiamiento_ID.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Fte_Financiamiento_ID + ", "); }
                    if (!String.IsNullOrEmpty(Programa_ID.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Proyecto_Programa_ID + ", "); }
                    if (!String.IsNullOrEmpty(Rubro_ID.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Rubro_ID + ", "); }
                    if (!String.IsNullOrEmpty(Tipo_ID.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Tipo_ID + ", "); }
                    if (!String.IsNullOrEmpty(Clase_ID.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Clase_Ing_ID + ", "); }
                    if (!String.IsNullOrEmpty(Concepto_ID.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Concepto_Ing_ID + ", "); }
                    if (!String.IsNullOrEmpty(SubConcepto_ID.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_SubConcepto_Ing_ID + ", "); }

                    if (!String.IsNullOrEmpty(No_Poliza_PSP.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_No_Poliza_Presupuestal + ", "); }
                    if (!String.IsNullOrEmpty(Tipo_Poliza_PSP.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Tipo_Poliza_ID_Presupuestal + ", "); }
                    if (!String.IsNullOrEmpty(Mes_Anio_PSP.Trim()))
                    { Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Mes_Anio_Presupuestal + ", "); }

                    Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Ope_Psp_Registro_Mov_Ingreso.Campo_Fecha_Creo + ") VALUES( ");
                    Mi_SQL.Append(No_Movimiento + ", ");
                    if (!String.IsNullOrEmpty(Cargo.Trim()))
                    { Mi_SQL.Append("'" + Cargo.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(Abono.Trim()))
                    { Mi_SQL.Append("'" + Abono.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(Importe.Trim()))
                    { Mi_SQL.Append(Math.Abs(Importe_Registro) + ", "); }
                    Mi_SQL.Append("GETDATE(), ");
                    if (!String.IsNullOrEmpty(Descripcion.Trim()))
                    { Mi_SQL.Append("'" + Descripcion.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(Referencia.Trim()))
                    { Mi_SQL.Append("'" + Referencia.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(No_Poliza.Trim()))
                    { Mi_SQL.Append("'" + No_Poliza.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(Tipo_Poliza_ID.Trim()))
                    { Mi_SQL.Append("'" + Tipo_Poliza_ID.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(Mes_Anio.Trim()))
                    { Mi_SQL.Append("'" + Mes_Anio.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(Fte_Financiamiento_ID.Trim()))
                    { Mi_SQL.Append("'" + Fte_Financiamiento_ID.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(Programa_ID.Trim()))
                    { Mi_SQL.Append("'" + Programa_ID.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(Rubro_ID.Trim()))
                    { Mi_SQL.Append("'" + Rubro_ID.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(Tipo_ID.Trim()))
                    { Mi_SQL.Append("'" + Tipo_ID.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(Clase_ID.Trim()))
                    { Mi_SQL.Append("'" + Clase_ID.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(Concepto_ID.Trim()))
                    { Mi_SQL.Append("'" + Concepto_ID.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(SubConcepto_ID.Trim()))
                    { Mi_SQL.Append("'" + SubConcepto_ID.Trim() + "', "); }

                    if (!String.IsNullOrEmpty(No_Poliza_PSP.Trim()))
                    { Mi_SQL.Append("'" + No_Poliza_PSP.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(Tipo_Poliza_PSP.Trim()))
                    { Mi_SQL.Append("'" + Tipo_Poliza_PSP.Trim() + "', "); }
                    if (!String.IsNullOrEmpty(Mes_Anio_PSP.Trim()))
                    { Mi_SQL.Append("'" + Mes_Anio_PSP.Trim() + "', "); }
                    
                    Mi_SQL.Append("'" + Usuario.Trim() + "', ");
                    Mi_SQL.Append("GETDATE()) ");

                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();

                    //validamos los siguientes movimientos a insertar
                    if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar) &&
                        Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado))
                    {
                        Cargo = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado;
                        Abono = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado;
                    }
                    else if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado) &&
                        Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar))
                    {
                        Cargo = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado;
                        Abono = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar;
                    }

                    No_Registros--;
                }

                if (Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (Exception Ex)
            {
                Trans.Rollback();
                throw new Exception("Error al intentar insertat los datos del movimiento de ingresos. Error: [" + Ex.Message + "]");
            }
            finally
            {
                if (Cmmd == null)
                {
                    Cn.Close();
                }
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Unificar_Conceptos
        ///DESCRIPCIÓN          : consulta para juntar los conceptos repetidos
        ///PARAMETROS           1 Dt_Datos: datos a afectar en la poliza de ingresos
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 07/Septiembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Unificar_Conceptos(DataTable Dt_Datos)
        {
            DataTable Dt_Psp_Ing = new DataTable();
            Double Importe = 0.00;
            String Fuente = String.Empty;
            String Programa = String.Empty;
            String Concepto = String.Empty;
            String SubConcepto = String.Empty;
            DataRow Fila;
            String Anio = String.Empty;
            Boolean Repetido = false;

            try
            {
                if (Dt_Datos != null)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        //creamos las columnas del datatable
                        Dt_Psp_Ing.Columns.Add("Fte_Financiamiento_ID", typeof(System.String));
                        Dt_Psp_Ing.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                        Dt_Psp_Ing.Columns.Add(Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID, typeof(System.String));
                        Dt_Psp_Ing.Columns.Add(Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID, typeof(System.String));
                        Dt_Psp_Ing.Columns.Add("Anio", typeof(System.String));
                        Dt_Psp_Ing.Columns.Add("Importe", typeof(System.String));

                        //obtnemos los primeros datos para comparar
                        Fuente = Dt_Datos.Rows[0]["Fte_Financiamiento_ID"].ToString().Trim();
                        Programa = Dt_Datos.Rows[0][Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                        Concepto = Dt_Datos.Rows[0][Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID].ToString().Trim();
                        SubConcepto = Dt_Datos.Rows[0][Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID].ToString().Trim();
                        Anio = Dt_Datos.Rows[0]["Anio"].ToString().Trim();

                        foreach (DataRow Dr in Dt_Datos.Rows)
                        {
                            //validamos si existen registros iguales
                            if (Fuente.Trim().Equals(Dr["Fte_Financiamiento_ID"].ToString().Trim()) &&
                                Programa.Trim().Equals(Dr[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim()) &&
                                Concepto.Trim().Equals(Dr[Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID].ToString().Trim()) &&
                                SubConcepto.Trim().Equals(Dr[Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID].ToString().Trim())&&
                                Anio.Trim().Equals(Dr["Anio"].ToString().Trim()))
                            {
                                //sumamos el importe
                                Importe += Convert.ToDouble(String.IsNullOrEmpty(Dr["Importe"].ToString().Trim()) ? "0" : Dr["Importe"].ToString().Trim());
                            }
                            else 
                            {
                                //insertamos los datos del registro anterior
                                if(Dt_Psp_Ing != null)
                                {
                                    if (Dt_Psp_Ing.Rows.Count > 0)
                                    {
                                        //validamos si no a sido insertado un registro ya con estos datos
                                        foreach (DataRow Dr_Psp in Dt_Psp_Ing.Rows)
                                        {
                                            if (Fuente.Trim().Equals(Dr_Psp["Fte_Financiamiento_ID"].ToString().Trim()) &&
                                                Programa.Trim().Equals(Dr_Psp[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim()) &&
                                                Concepto.Trim().Equals(Dr_Psp[Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID].ToString().Trim()) &&
                                                SubConcepto.Trim().Equals(Dr_Psp[Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID].ToString().Trim()) &&
                                                Anio.Trim().Equals(Dr_Psp["Anio"].ToString().Trim()))
                                            {
                                                Repetido = true; //si el registro ya se inserto regresamos true
                                            }
                                        }

                                        if (!Repetido) //validamos que el registro no exista
                                        {
                                            Repetido = false;
                                            //insertamos los datos del concepto
                                            Fila = Dt_Psp_Ing.NewRow();
                                            Fila["Fte_Financiamiento_ID"] = Fuente.Trim();
                                            Fila[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Programa.Trim();
                                            Fila[Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID] = Concepto.Trim();
                                            Fila[Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID] = SubConcepto.Trim();
                                            Fila["Anio"] = Anio.Trim();
                                            Fila["Importe"] = Importe.ToString().Trim();
                                            Dt_Psp_Ing.Rows.Add(Fila);
                                        }
                                    }
                                    else
                                    {
                                        Fila = Dt_Psp_Ing.NewRow();
                                        Fila["Fte_Financiamiento_ID"] = Fuente.Trim();
                                        Fila[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Programa.Trim();
                                        Fila[Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID] = Concepto.Trim();
                                        Fila[Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID] = SubConcepto.Trim();
                                        Fila["Anio"] = Anio.Trim();
                                        Fila["Importe"] = Importe.ToString().Trim();
                                        Dt_Psp_Ing.Rows.Add(Fila);
                                    }
                                }

                                //obtenemos los nuevos valores
                                Fuente = Dr["Fte_Financiamiento_ID"].ToString().Trim();
                                Programa = Dr[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                                Concepto = Dr[Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID].ToString().Trim();
                                SubConcepto = Dr[Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID].ToString().Trim();
                                Anio = Dr["Anio"].ToString().Trim();
                                Importe = Convert.ToDouble(String.IsNullOrEmpty(Dr["Importe"].ToString().Trim()) ? "0" : Dr["Importe"].ToString().Trim());
                            }
                        }

                        //insertamos el ultimo registro que se quedo guardado en las variables
                        Fila = Dt_Psp_Ing.NewRow();
                        Fila["Fte_Financiamiento_ID"] = Fuente.Trim();
                        Fila[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = Programa.Trim();
                        Fila[Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID] = Concepto.Trim();
                        Fila[Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID] = SubConcepto.Trim();
                        Fila["Anio"] = Anio.Trim();
                        Fila["Importe"] = Importe.ToString().Trim();
                        Dt_Psp_Ing.Rows.Add(Fila);
                    }
                    else
                    {
                        Dt_Psp_Ing = Dt_Datos;
                    }
                }
                else
                {
                    Dt_Psp_Ing = Dt_Datos;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error al unificar los datos de los conceptos. Error[" + e.Message + "]");
            }

            return Dt_Psp_Ing;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Columna
        ///DESCRIPCIÓN          : consulta para obtener la columna que se afectara
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 14/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static String Obtener_Columna(String Momento_Presupuestal, String Mes)
        {
            String Columna = String.Empty;

            try
            {
                if (Momento_Presupuestal.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar.Trim()))
                {
                    #region (Por Recaudar)
                    switch (Mes)
                    {
                        case "01":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero;
                            break;
                        case "02":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero;
                            break;
                        case "03":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo;
                            break;
                        case "04":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril;
                            break;
                        case "05":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo;
                            break;
                        case "06":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio;
                            break;
                        case "07":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio;
                            break;
                        case "08":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto;
                            break;
                        case "09":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre;
                            break;
                        case "10":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre;
                            break;
                        case "11":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre;
                            break;
                        case "12":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre;
                            break;
                    }
                    #endregion
                }
                else if (Momento_Presupuestal.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado.Trim()))
                {
                    #region (Devengado)
                    switch (Mes)
                    {
                        case "01":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Enero;
                            break;
                        case "02":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Febrero;
                            break;
                        case "03":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Marzo;
                            break;
                        case "04":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Abril;
                            break;
                        case "05":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Mayo;
                            break;
                        case "06":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Junio;
                            break;
                        case "07":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Julio;
                            break;
                        case "08":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Agosto;
                            break;
                        case "09":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Septiembre;
                            break;
                        case "10":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Octubre;
                            break;
                        case "11":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Noviembre;
                            break;
                        case "12":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Diciembre;
                            break;
                    }
                    #endregion
                }
                else if (Momento_Presupuestal.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado.Trim()))
                {
                    #region (Recaudado)
                    switch (Mes)
                    {
                        case "01":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Enero;
                            break;
                        case "02":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Febrero;
                            break;
                        case "03":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Marzo;
                            break;
                        case "04":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Abril;
                            break;
                        case "05":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Mayo;
                            break;
                        case "06":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Junio;
                            break;
                        case "07":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Julio;
                            break;
                        case "08":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Agosto;
                            break;
                        case "09":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Septiembre;
                            break;
                        case "10":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Octubre;
                            break;
                        case "11":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Noviembre;
                            break;
                        case "12":
                            Columna = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Diciembre;
                            break;
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener el nombre de la columna que se afectara. Error[" + ex.Message + "]");
            }
            return Columna;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Movimientos_Presupuestales
        ///DESCRIPCIÓN          : consulta para modificar los datos del presupuesto de ingresos
        ///PARAMETROS           1 Dt_Datos: datos a afectar en la poliza de ingresos
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 07/Septiembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String[] Alta_Movimientos_Presupuestales(DataTable Dt_Datos, SqlCommand Cmmd, String Referencia,
            String No_Poliza, String Tipo_Poliza_ID, String Mes_Anio, String Momento_Inicial, String Momento_Final)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Da_Datos = new SqlDataAdapter();
            DataSet Ds_Datos = new DataSet();

            if (Cmmd != null)
            {
                Cmd = Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Trans.Connection;
                Cmd.Transaction = Trans;
            }
            String Rubro = String.Empty;
            String Tipo = String.Empty;
            String Clase = String.Empty;
            String Concepto = String.Empty;
            String SubConcepto = String.Empty;
            DataTable Dt_Registro = new DataTable();
            Int32 Registro_Ingreso = 0;
            DateTime Fecha = DateTime.Now;
            String Mes = String.Format("{0:MM}", Fecha).ToUpper();
            Double Importe = 0.00;
            String Anio = String.Format("{0:yyyy}", DateTime.Now);
            String Columna_Actual = String.Empty;
            String Columna_Anterior = String.Empty;
            Boolean Actualizado = false;
            String Actual_Psp = String.Empty;
            String[] Datos_Poliza_PSP = null;
            String No_Poliza_PSP = String.Empty;
            String Tipo_Poliza_PSP = String.Empty;
            String Mes_Anio_PSP = String.Empty;


            //obtenemos el estatus de actualizacion del presupuesto por si tenemos que dar de alta algun concepto en el presupuesto
            Actualizado = Presupuesto_Actualizado(Cmd);
            if (Actualizado)
            {
                Actual_Psp = "SI";
            }
            else
            {
                Actual_Psp = "NO";
            }

            try
            {
                if (Acumular_Saldos_Fin_Mes(Cmd))
                {
                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            //verificamos que los datos no sean nulos
                            if (!String.IsNullOrEmpty(Momento_Inicial.Trim()) && !String.IsNullOrEmpty(Momento_Final.Trim()))
                            {
                                //unificamos los conceptos
                                Dt_Datos = Unificar_Conceptos(Dt_Datos);
                                Columna_Actual = Obtener_Columna(Momento_Final.Trim(), Mes.Trim());
                                Columna_Anterior = Obtener_Columna(Momento_Inicial.Trim(), Mes.Trim());

                                //obtenemos los datos de la poliza presupuestal
                                Datos_Poliza_PSP = Crear_Poliza_Presupuestal_Ingresos(Momento_Inicial.Trim(), Momento_Final.Trim(), Dt_Datos, Cmd, Referencia);

                                if (Datos_Poliza_PSP != null)
                                {
                                    if (Datos_Poliza_PSP.Length > 0)
                                    {
                                        No_Poliza_PSP = Datos_Poliza_PSP[0].Trim();
                                        Tipo_Poliza_PSP = Datos_Poliza_PSP[1].Trim();
                                        Mes_Anio_PSP = Datos_Poliza_PSP[2].Trim();
                                    }
                                }

                                foreach (DataRow Dr in Dt_Datos.Rows)
                                {
                                    //LIMPIAMOS EL IMPORTE
                                    Importe = 0.00;

                                    if (!String.IsNullOrEmpty(Dr["Fte_Financiamiento_ID"].ToString().Trim()) &&
                                        !String.IsNullOrEmpty(Dr["Concepto_Ing_ID"].ToString().Trim()))
                                    {
                                        if (!String.IsNullOrEmpty(Dr["Anio"].ToString().Trim()))
                                        { Anio = Dr["Anio"].ToString().Trim(); }

                                        if (Convert.ToDouble(String.IsNullOrEmpty(Dr["Importe"].ToString().Trim()) ? "0" : Dr["Importe"].ToString().Trim().Replace(",", "")) != 0)
                                        {
                                            //OBTENEMOS EL CONCEPTO, LA CLASE, EL TIPO Y EL RUBRO
                                            Concepto = Dr["Concepto_Ing_ID"].ToString().Trim();
                                            if (!String.IsNullOrEmpty(Dr["SubConcepto_Ing_ID"].ToString()))
                                            {
                                                SubConcepto = Dr["SubConcepto_Ing_ID"].ToString().Trim();
                                            }

                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("SELECT " + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID);
                                            Mi_SQL.Append(" FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                                            Mi_SQL.Append(" WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Concepto + "'");
                                            Cmd.CommandText = Mi_SQL.ToString().Trim();
                                            Clase = (String)Cmd.ExecuteScalar();

                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("SELECT " + Cat_Psp_Clase_Ing.Campo_Tipo_ID);
                                            Mi_SQL.Append(" FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                                            Mi_SQL.Append(" WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Clase + "'");
                                            Cmd.CommandText = Mi_SQL.ToString().Trim();
                                            Tipo = (String)Cmd.ExecuteScalar();

                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("SELECT " + Cat_Psp_Tipo.Campo_Rubro_ID);
                                            Mi_SQL.Append(" FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                                            Mi_SQL.Append(" WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Tipo + "'");
                                            Cmd.CommandText = Mi_SQL.ToString().Trim();
                                            Rubro = (String)Cmd.ExecuteScalar();

                                            Importe = Convert.ToDouble(String.IsNullOrEmpty(Dr["Importe"].ToString().Trim()) ? "0" : Dr["Importe"].ToString().Trim().Replace(",", ""));

                                            //limpiamos las variables
                                            Dt_Registro = new DataTable();
                                            Da_Datos = new SqlDataAdapter();
                                            Ds_Datos = new DataSet();

                                            //Primero se consulta si existe un registro en el presupuesto de ingreso 
                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["Fte_Financiamiento_ID"].ToString().Trim() + "' ");
                                            if (!String.IsNullOrEmpty(Dr["Proyecto_Programa_ID"].ToString().Trim()))
                                            { Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["Proyecto_Programa_ID"].ToString().Trim() + "' "); }
                                            else
                                            { Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL"); }
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = " + Anio);
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Rubro.Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Tipo.Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Clase.Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Concepto.Trim() + "' ");
                                            if (!String.IsNullOrEmpty(Dr["SubConcepto_Ing_ID"].ToString().Trim()))
                                            { Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SubConcepto_Ing_ID"].ToString().Trim() + "' "); }
                                            else
                                            { Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL"); }
                                            Cmd.CommandText = Mi_SQL.ToString().Trim();
                                            Da_Datos = new SqlDataAdapter(Cmd);
                                            Da_Datos.Fill(Ds_Datos);

                                            if (Ds_Datos != null)
                                            {
                                                Dt_Registro = Ds_Datos.Tables[0];
                                            }

                                            if (Momento_Inicial.Trim() != Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado && Momento_Final.Trim() != Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado
                                                && Momento_Inicial.Trim() != Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion && Momento_Final.Trim() != Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion
                                                && Momento_Inicial.Trim() != Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion && Momento_Final.Trim() != Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion
                                                && Momento_Inicial.Trim() != Ope_Psp_Presupuesto_Ingresos.Campo_Modificado && Momento_Final.Trim() != Ope_Psp_Presupuesto_Ingresos.Campo_Modificado)
                                            {
                                               //insertamos o modificamos los datos del presupuesto de ingresos
                                                if (Dt_Registro.Rows.Count > 0)
                                                {
                                                    Mi_SQL = new StringBuilder();
                                                    Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                                    Mi_SQL.Append(" SET " + Momento_Final.Trim() + " =  ISNULL(" + Momento_Final.Trim() + ", 0) + " + Math.Abs(Importe) + ", ");
                                                    Mi_SQL.Append(Momento_Inicial.Trim() + " =  ISNULL(" + Momento_Inicial.Trim() + ", 0) - " + Math.Abs(Importe) + ", ");
                                                    Mi_SQL.Append(Columna_Actual.Trim() + " = ISNULL(" + Columna_Actual.Trim() + ", 0) + " + Math.Abs(Importe) + ", ");
                                                    Mi_SQL.Append(Columna_Anterior.Trim() + " = ISNULL(" + Columna_Anterior.Trim() + ", 0) - " + Math.Abs(Importe) + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Modifico + " = GETDATE() ");
                                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["Fte_Financiamiento_ID"].ToString().Trim() + "' ");
                                                    if (!String.IsNullOrEmpty(Dr["Proyecto_Programa_ID"].ToString().Trim()))
                                                    { Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["Proyecto_Programa_ID"].ToString().Trim() + "' "); }
                                                    else
                                                    { Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL"); }
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = " + Anio);
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Rubro.Trim() + "' ");
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Tipo.Trim() + "' ");
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Clase.Trim() + "' ");
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Concepto.Trim() + "' ");
                                                    if (!String.IsNullOrEmpty(Dr["SubConcepto_Ing_ID"].ToString().Trim()))
                                                    { Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SubConcepto_Ing_ID"].ToString().Trim() + "' "); }
                                                    else
                                                    { Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL"); }
                                                    Cmd.CommandText = Mi_SQL.ToString();
                                                    Cmd.ExecuteNonQuery();
                                                }
                                                else
                                                {
                                                    //obtenemos el no de presupuesto id por si se tiene q insertar un registro no existente
                                                    Mi_SQL = new StringBuilder();
                                                    Mi_SQL.Append("SELECT  ISNULL(MAX (" + Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID + "),0)");
                                                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                                    Cmd.CommandText = Mi_SQL.ToString().Trim();

                                                    Registro_Ingreso = Convert.ToInt32(Cmd.ExecuteScalar());
                                                    Registro_Ingreso = Registro_Ingreso + 1;

                                                    //Se inserta el registro ya que no existe en el presupuesto
                                                    Mi_SQL = new StringBuilder();
                                                    Mi_SQL.Append("INSERT INTO " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + " (");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Enero + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Febrero + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Marzo + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Abril + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Mayo + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Junio + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Julio + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Agosto + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Septiembre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Octubre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Noviembre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Diciembre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Enero + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Febrero + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Marzo + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Abril + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Mayo + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Junio + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Julio + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Agosto + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Septiembre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Octubre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Noviembre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Diciembre + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Actualizado + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Creo + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Creo + ") ");
                                                    Mi_SQL.Append("VALUES (" + Registro_Ingreso + ", ");
                                                    Mi_SQL.Append("'" + Dr["Fte_Financiamiento_ID"].ToString().Trim() + "', ");
                                                    if (!String.IsNullOrEmpty(Dr["Proyecto_Programa_ID"].ToString().Trim()))
                                                    { Mi_SQL.Append("'" + Dr["Proyecto_Programa_ID"].ToString().Trim() + "', "); }
                                                    else { Mi_SQL.Append("NULL, "); }
                                                    Mi_SQL.Append("'" + Rubro.Trim() + "', ");
                                                    Mi_SQL.Append("'" + Tipo.Trim() + "', ");
                                                    Mi_SQL.Append("'" + Clase.Trim() + "', ");
                                                    Mi_SQL.Append("'" + Concepto.Trim() + "', ");
                                                    if (!String.IsNullOrEmpty(Dr["SubConcepto_Ing_ID"].ToString().Trim()))
                                                    { Mi_SQL.Append("'" + Dr["SubConcepto_Ing_ID"].ToString().Trim() + "', "); }
                                                    else { Mi_SQL.Append("NULL, "); }
                                                    Mi_SQL.Append(Anio + ", ");
                                                    Mi_SQL.Append("0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, ");
                                                    Mi_SQL.Append("0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, ");
                                                    Mi_SQL.Append("0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, ");
                                                    Mi_SQL.Append("0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, ");
                                                    Mi_SQL.Append("0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, ");
                                                    Mi_SQL.Append("'" + Actual_Psp.Trim() + "', ");
                                                    Mi_SQL.Append("'" + Cls_Sessiones.Nombre_Empleado.ToString() + "',GETDATE())");
                                                    Cmd.CommandText = Mi_SQL.ToString();
                                                    Cmd.ExecuteNonQuery();

                                                    Mi_SQL = new StringBuilder();
                                                    Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                                    Mi_SQL.Append(" SET " + Momento_Final.Trim() + " =  ISNULL(" + Momento_Final.Trim() + ", 0) + " + Math.Abs(Importe) + ", ");
                                                    Mi_SQL.Append(Momento_Inicial.Trim() + " =  ISNULL(" + Momento_Inicial.Trim() + ", 0) - " + Math.Abs(Importe) + ", ");
                                                    Mi_SQL.Append(Columna_Actual.Trim() + " = ISNULL(" + Columna_Actual.Trim() + ", 0) + " + Math.Abs(Importe) + ", ");
                                                    Mi_SQL.Append(Columna_Anterior.Trim() + " = ISNULL(" + Columna_Anterior.Trim() + ", 0) - " + Math.Abs(Importe) + ", ");
                                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Modifico + " = GETDATE() ");
                                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["Fte_Financiamiento_ID"].ToString().Trim() + "' ");
                                                    if (!String.IsNullOrEmpty(Dr["Proyecto_Programa_ID"].ToString().Trim()))
                                                    { Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["Proyecto_Programa_ID"].ToString().Trim() + "' "); }
                                                    else
                                                    { Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL"); }
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = " + Anio);
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Rubro.Trim() + "' ");
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Tipo.Trim() + "' ");
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Clase.Trim() + "' ");
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Concepto.Trim() + "' ");
                                                    if (!String.IsNullOrEmpty(Dr["SubConcepto_Ing_ID"].ToString().Trim()))
                                                    { Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SubConcepto_Ing_ID"].ToString().Trim() + "' "); }
                                                    else
                                                    { Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL"); }
                                                    Cmd.CommandText = Mi_SQL.ToString();
                                                    Cmd.ExecuteNonQuery();
                                                }
                                            }

                                            //insertamos los registros de los movimientos del presupuesto
                                            Insertar_Registro_Mov_Ing(Momento_Inicial.Trim(), Momento_Final.Trim(), Importe.ToString().Trim(),
                                                String.Empty, Referencia.Trim(), No_Poliza.Trim(), Tipo_Poliza_ID.Trim(),
                                                Mes_Anio.Trim(), Dr["Fte_Financiamiento_ID"].ToString().Trim(), Dr["Proyecto_Programa_ID"].ToString().Trim(), Rubro.Trim(),
                                                Tipo.Trim(), Clase.Trim(), Concepto.Trim(), SubConcepto.Trim(), Cls_Sessiones.Nombre_Empleado.Trim(), Cmd,
                                                No_Poliza_PSP, Tipo_Poliza_PSP, Mes_Anio_PSP);
                                        }
                                    }
                                }//fin foreach
                            }//fin if validar datos nulos

                            if (Cmmd == null)
                            {
                                Trans.Commit();
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Trans.Rollback();
                throw new Exception("Error al ejecutar los el alta de la poliza de ingresos. Error: [" + Ex.Message + "]");
            }
            finally
            {
                if (Cmmd == null)
                {
                    Cn.Close();
                }
            }
            return Datos_Poliza_PSP;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Acumular_Saldos_Fin_Mes
        ///DESCRIPCIÓN          : consulta para actualizar los datos del presupuesto de egresos acumulando saldos
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 14/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static Boolean Acumular_Saldos_Fin_Mes(SqlCommand Cmd)
        {
            Boolean Operacion_Completa = true;
            DataTable Dt_Parametros = new DataTable();
            DataTable Dt_Psp_Actualizado = new DataTable();
            String Anio = String.Format("{0:yyyy}", DateTime.Now);
            String Dia = String.Format("{0:dd}", DateTime.Now);
            String Mes = String.Format("{0:MM}", DateTime.Now);
            Boolean Presupuesto_Actualizado = false;
            StringBuilder Mi_SQL = new StringBuilder();
            String Mes_Anterior = String.Format("{0:00}", DateTime.Now.Month - 1);
            String Por_Recaudar_Actual = String.Empty;
            String Devengado_Actual = String.Empty;
            String Por_Recaudar_Anterior = String.Empty;
            String Devengado_Anterior = String.Empty;

            try
            {
                //CONSULTAMOS DIA PARA APLICAR EL ACUMULADO
                if (Convert.ToInt32(Dia.Trim()) < 20) //si es 1ro aplicamos el acumulado
                {
                    //validamos el mes
                    if (Convert.ToInt32(Mes.Trim()) > 1)
                    {
                        //CONSULTAMOS SI EL PRESUPUESTO YA FUE ACTUALIZADO
                        Presupuesto_Actualizado = Cls_Ope_Con_Poliza_Ingresos_Datos.Presupuesto_Actualizado(Cmd);

                        if (!Presupuesto_Actualizado)
                        {

                            //CONSULTAMOS LOS NOMBRES DE LAS COLUMNAS A MODIFICAR DE ACUERDO AL MES
                            Por_Recaudar_Actual = Obtener_Columna(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar, Mes);
                            Devengado_Actual = Obtener_Columna(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado, Mes);

                            Por_Recaudar_Anterior = Obtener_Columna(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar, Mes_Anterior);
                            Devengado_Anterior = Obtener_Columna(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado, Mes_Anterior);

                            //ACTUALIZAMOS EL PRESUPUESTO ACUMULANDO LOS SALDOS
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                            Mi_SQL.Append(" SET ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Actualizado + " = 'SI', ");
                            //MODIFICAMOS EL COMPROMETIDO
                            Mi_SQL.Append(Por_Recaudar_Actual);
                            Mi_SQL.Append(" = ISNULL(" + Por_Recaudar_Actual + ",0) + ISNULL(" + Por_Recaudar_Anterior + ", 0), ");
                            //MODIFICAMOS EL DEVENGADO
                            Mi_SQL.Append(Devengado_Actual);
                            Mi_SQL.Append(" = ISNULL(" + Devengado_Actual + ",0) + ISNULL(" + Devengado_Anterior + ", 0) ");
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = " + Anio.Trim());

                            Cmd.CommandText = Mi_SQL.ToString().Trim();
                            Cmd.ExecuteNonQuery();
                        }
                    }
                }
                else //else validación día 1ro
                {
                    //si es otro dia validamos si es 20 de cada mes para modificar el presupuesto a actualizado a NO,
                    //para cuando seal inicio de otro mes volver a actualizar el presupuesto
                    if (Convert.ToInt32(Dia) > 20)
                    {
                        Presupuesto_Actualizado = Cls_Ope_Con_Poliza_Ingresos_Datos.Presupuesto_Actualizado(Cmd);

                        if (Presupuesto_Actualizado)
                        {
                            //modificamos el presupuesto de egresos en actualizado = NO
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                            Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Actualizado + " = 'NO'");
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = " + Anio);

                            Cmd.CommandText = Mi_SQL.ToString().Trim();
                            Cmd.ExecuteNonQuery();
                        }
                    }
                }

                Operacion_Completa = true;
            }
            catch (Exception)
            {
                Operacion_Completa = false;
                throw new Exception();
            }
            return Operacion_Completa;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Presupuesto_Actualizado
        ///DESCRIPCIÓN          : consulta para actualizar los datos del presupuesto de egresos
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 14/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static Boolean Presupuesto_Actualizado(SqlCommand Cmd)
        {
            DataTable Dt_Psp_Actualizado = new DataTable();
            String Anio = String.Format("{0:yyyy}", DateTime.Now);
            Boolean Datos_Actualizados = false;
            StringBuilder Mi_SQL = new StringBuilder();
            SqlDataAdapter Da_Datos = new SqlDataAdapter();
            DataSet Ds_Datos = new DataSet();

            try
            {
                //CONSULTAMOS SI EL PRESUPUESTO YA FUE ACTUALIZADO
                Mi_SQL.Append("SELECT DISTINCT ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Actualizado + ", 'NO') AS ACTUALIZADO ");
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = " + Anio.Trim());

                Cmd.CommandText = Mi_SQL.ToString().Trim();
                Da_Datos = new SqlDataAdapter(Cmd);
                Da_Datos.Fill(Ds_Datos);

                if (Ds_Datos != null)
                {
                    Dt_Psp_Actualizado = Ds_Datos.Tables[0];
                }

                if (Dt_Psp_Actualizado != null)
                {
                    if (Dt_Psp_Actualizado.Rows.Count > 0)
                    {
                        if (Dt_Psp_Actualizado.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado].ToString().Trim().Equals("NO"))
                        {
                            Datos_Actualizados = false;
                        }
                        else
                        {
                            Datos_Actualizados = true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                Datos_Actualizados = false;
                throw new Exception();
            }
            return Datos_Actualizados;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Poliza_Presupuestal_Ingresos
        ///DESCRIPCIÓN          : consulta para crear la poliza presupuestal de ingresos
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 02/Abril/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static String[] Crear_Poliza_Presupuestal_Ingresos(String Momento_Inicial, String Momento_Final,
            DataTable Dt_Datos, SqlCommand Cmmd, String Referencia) 
        {
            Cls_Ope_Con_Polizas_Negocio Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio();
            DataTable Dt_Parametros_Psp = new DataTable();
            String ID_Inicial = String.Empty;
            String ID_Final = String.Empty;
            DataTable Dt_Detalle = new DataTable();
            DataRow Fila;
            String[] No_Poliza = null;
            Double Importe = 0.00;
            Double Importe_Debe = 0.00;
            Double Importe_Haber = 0.00;
            Int32 No_Registros = 0;
            String Mov_Inicial = Momento_Inicial.Trim();
            String Mov_Final = Momento_Final.Trim();
            String Tipo_Poliza = String.Empty;
            Int32 Contador = 0;
            DataTable Dt_Modificacion = new DataTable();

            StringBuilder Mi_SQL = new StringBuilder();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;

            if (Cmmd != null)
            {
                Cmd = Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Trans.Connection;
                Cmd.Transaction = Trans;
            }

            try
            {
                //obtenemos los datos de los parametros de presupuestos, para obtener el tipo de poliza,
                // y el id de las cuentas contables que se afectaran
                Dt_Parametros_Psp = Cls_Cat_Psp_Parametros_Presupuestales_Datos.Consultar_Parametros_Presupuestales();
                
                //Obtenemos el importe de los conceptos
                Importe = Dt_Datos.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<String>("Importe")) ? "0" : x.Field<String>("Importe")));

                //creamos las columnas del datatable para el alta de la poliza
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                Dt_Detalle.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
                Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Detalle.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
                Dt_Detalle.Columns.Add("MOMENTO_FINAL", typeof(System.String));


                //Obtenemos los datos del movimiento inicial y final para obtener si es de por_recaudar a recaudado o
                //de recaudado a por_recaudar, y poder meter el momento intemedio que es el devengado
                if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar) &&
                    Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado))
                {
                    Momento_Inicial  = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar;
                    Momento_Final  = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado;
                    No_Registros = 2;
                }
                else if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado) &&
                    Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar))
                {
                    Momento_Inicial = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado;
                    Momento_Final = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado;
                    No_Registros = 2;
                }
                else if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Modificado) &&
                    Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar))
                {
                    Momento_Inicial = Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion;
                    Momento_Final = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar;

                    Dt_Modificacion = (from x in Dt_Datos.AsEnumerable()
                                       where Convert.ToDouble(String.IsNullOrEmpty(x.Field<String>("Importe")) ? "0" : x.Field<String>("Importe")) > 0
                                        select x).AsDataView().ToTable();

                    Importe = Dt_Modificacion.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<String>("Importe")) ? "0" : x.Field<String>("Importe")));

                    No_Registros = 2;
                }
                else
                {
                    No_Registros = 1;
                }

                //obtenemos el id de la cuenda contable del movimiento
                Tipo_Poliza = Obtener_Id_Poliza_Presupuestal(Dt_Parametros_Psp, Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Ing);
                
                //hacemos un ciclo while para manejar el numero de registros que insertaremos
                while (No_Registros > 0)
                {
                    ID_Inicial = Obtener_Id_Poliza_Presupuestal(Dt_Parametros_Psp, Momento_Inicial);
                    ID_Final = Obtener_Id_Poliza_Presupuestal(Dt_Parametros_Psp, Momento_Final);

                    if (!String.IsNullOrEmpty(ID_Inicial.Trim()) && !String.IsNullOrEmpty(ID_Final.Trim())
                        && (Importe != 0))
                    {
                        //cargamos los dato de CARGO - DEBE
                        Fila = Dt_Detalle.NewRow();
                        Fila[Ope_Con_Polizas_Detalles.Campo_Partida] = ++Contador;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = ID_Inicial.Trim();
                        Fila["CUENTA"] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Concepto] = Referencia;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Debe] = Math.Abs(Importe);
                        Fila[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = String.Empty;
                        Fila["MOMENTO_INICIAL"] = String.Empty;
                        Fila["MOMENTO_FINAL"] = String.Empty;
                        Dt_Detalle.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla

                        //cargamos los dato de ABONO - HABER
                        Fila = Dt_Detalle.NewRow();
                        Fila[Ope_Con_Polizas_Detalles.Campo_Partida] = ++Contador;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = ID_Final.Trim();
                        Fila["CUENTA"] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Concepto] = Referencia;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Haber] = Math.Abs(Importe);
                        Fila[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = String.Empty;
                        Fila[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = String.Empty;
                        Fila["MOMENTO_INICIAL"] = String.Empty;
                        Fila["MOMENTO_FINAL"] = String.Empty;
                        Dt_Detalle.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                    }

                     if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar) &&
                     Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado))
                     {
                         Momento_Inicial = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado;
                         Momento_Final = Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado;
                     }
                     else if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado) &&
                     Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar))
                     {
                         Momento_Inicial = Ope_Psp_Presupuesto_Ingresos.Campo_Devengado;
                         Momento_Final = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar;
                     }
                     else if (Mov_Inicial.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Modificado) &&
                     Mov_Final.Trim().Equals(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar))
                     {
                         Momento_Inicial = Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar;
                         Momento_Final = Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion;

                         Dt_Modificacion = (from x in Dt_Datos.AsEnumerable()
                                            where Convert.ToDouble(String.IsNullOrEmpty(x.Field<String>("Importe")) ? "0" : x.Field<String>("Importe")) < 0
                                            select x).AsDataView().ToTable();

                         Importe = Dt_Modificacion.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<String>("Importe")) ? "0" : x.Field<String>("Importe")));
                     }

                    No_Registros--;
                }

                if (Dt_Detalle.Rows.Count > 0 && !String.IsNullOrEmpty(Tipo_Poliza))
                {
                    //Obtenemos el importe del debe y del haber para totalizar la poliza
                    Importe_Debe = Dt_Detalle.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<Double>(Ope_Con_Polizas_Detalles.Campo_Debe).ToString()) ? "0" : x.Field<Double>(Ope_Con_Polizas_Detalles.Campo_Debe).ToString()));
                    Importe_Haber = Dt_Detalle.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<Double>(Ope_Con_Polizas_Detalles.Campo_Haber).ToString()) ? "0" : x.Field<Double>(Ope_Con_Polizas_Detalles.Campo_Haber).ToString()));

                    Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = Tipo_Poliza;
                    Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now);
                    Rs_Alta_Ope_Con_Polizas.P_Fecha_Poliza = DateTime.Now;
                    Rs_Alta_Ope_Con_Polizas.P_Concepto = Referencia;
                    Rs_Alta_Ope_Con_Polizas.P_Total_Debe = Math.Abs(Importe_Debe);
                    Rs_Alta_Ope_Con_Polizas.P_Total_Haber = Math.Abs(Importe_Haber);
                    Rs_Alta_Ope_Con_Polizas.P_No_Partida = Contador;
                    Rs_Alta_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado.Trim();
                    Rs_Alta_Ope_Con_Polizas.P_Dt_Detalles_Polizas = Dt_Detalle;
                    Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID.Trim();
                    Rs_Alta_Ope_Con_Polizas.P_Cmmd = Cmd;
                    No_Poliza = Rs_Alta_Ope_Con_Polizas.Alta_Poliza(); //Da de alta los datos de la Póliza proporcionados por el usuario en la BD
                }


                if (Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (Exception Ex)
            {
                Trans.Rollback();
                throw new Exception(" Error al crear la poliza presupuestal de ingresos. Error[" + Ex.Message + "]");
            }
            finally
            {
                if (Cmmd == null)
                {
                    Cn.Close();
                }
            }
            return No_Poliza;
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Id_Poliza_Presupuestal
        ///DESCRIPCIÓN          : consulta para crear la poliza presupuestal de ingresos
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 02/Abril/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static String Obtener_Id_Poliza_Presupuestal(DataTable Dt_Parametros_Psp, String Columna)
        {
            String Id = String.Empty;

            try
            {
                if (Dt_Parametros_Psp != null)
                {
                    if (Dt_Parametros_Psp.Rows.Count > 0)
                    {
                        switch (Columna) 
                        {
                            //tipos de polizas ingresos y egresos
                            case Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Ing:
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Ing].ToString().Trim();
                                break;

                            //cuentas de ingresos
                            case Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado:
                                    Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Estimado].ToString().Trim();
                                break;
                            case Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion:
                                    Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Ampliacion].ToString().Trim();
                                break;
                            case Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion:
                                    Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Reduccion].ToString().Trim();
                                break;
                            case Ope_Psp_Presupuesto_Ingresos.Campo_Modificado:
                                    Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Modificado].ToString().Trim();
                                break;
                            case Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar:
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Por_Recaudar].ToString().Trim();
                                break;
                            case Ope_Psp_Presupuesto_Ingresos.Campo_Devengado:
                                    Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Devengado].ToString().Trim();
                                break;
                            case Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado:
                                Id = Dt_Parametros_Psp.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Recaudado].ToString().Trim();
                                break;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(" Error al obtener los datos poliza presupuestal de ingresos. Error[" + Ex.Message + "]");
            }

            return Id;
        }

        #endregion
    }
}


