﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Reporte_Cuentas_Contables.Negocio;

/// <summary>
/// Summary description for Cls_Rpt_Con_Cuentas_Contables_Datos
/// </summary>
namespace JAPAMI.Reporte_Cuentas_Contables.Datos
{
    public class Cls_Rpt_Con_Cuentas_Contables_Datos
    {
        public Cls_Rpt_Con_Cuentas_Contables_Datos()
        {
        }

        /// *************************************************************************************
        /// NOMBRE:              Consulta_Cuentas_Contables
        /// DESCRIPCION:         Consultar las cuentas contables respecto a diversos filtros
        /// PARÁMETROS:          Datos: Variable de la capa de negocios
        /// USUARIO CREO:        Noe Mosqueda Valadez
        /// FECHA CREO:          20/Abril/2012 11:52
        /// USUARIO MODIFICO:    
        /// FECHA MODIFICO:      
        /// CAUSA MODIFICACION:  
        /// *************************************************************************************
        public static DataTable Consulta_Cuentas_Contables(Cls_Rpt_Con_Cuentas_Contables_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //Tabla para el resultado
            Boolean Where_Utilizado = false; //variable que indica si la clausula where ya ha sido utilizada

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT Cat_Con_Cuentas_Contables.Cuenta_Contable_ID, Cat_Con_Cuentas_Contables.Cuenta, Cat_Con_Cuentas_Contables.Descripcion, " +
                    "(Cat_Con_Niveles.Descripcion + ' (' + LTRIM(RTRIM(STR(Cat_Con_Niveles.Inicio_Nivel))) + '-' + LTRIM(RTRIM(STR(Cat_Con_Niveles.Inicio_Nivel))) + ')') AS Nivel " +
                    "FROM Cat_Con_Cuentas_Contables " +
                    "INNER JOIN Cat_Con_Niveles ON Cat_Con_Cuentas_Contables.Nivel_ID = Cat_Con_Niveles.Nivel_ID ";

                //Filtros
                if (String.IsNullOrEmpty(Datos.P_Afectable) == false)
                {
                    Mi_SQL += "WHERE Cat_Con_Cuentas_Contables.Afectable = '" + Datos.P_Afectable + "' ";
                    Where_Utilizado = true;
                }

                if (String.IsNullOrEmpty(Datos.P_Dependencia_ID) == false)
                {
                    //Verificar si ya se ha utilizado la clausula where
                    if (Where_Utilizado == false)
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }
                    else
                    {
                        Mi_SQL += "AND ";
                    }

                    //Resto de la consulta
                    Mi_SQL += "Cat_Con_Cuentas_Contables.Dependencia_ID = '" + Datos.P_Dependencia_ID + "' ";
                }

                if (String.IsNullOrEmpty(Datos.P_Nivel_ID) == false)
                {
                    //Verificar si ya se ha utilizado la clausula where
                    if (Where_Utilizado == false)
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }
                    else
                    {
                        Mi_SQL += "AND ";
                    }

                    //Resto de la consulta
                    Mi_SQL += "Cat_Con_Cuentas_Contables.Nivel_ID = '" + Datos.P_Nivel_ID + "' ";
                }

                if (String.IsNullOrEmpty(Datos.P_Partida_ID) == false)
                {
                    //Verificar si ya se ha utilizado la clausula where
                    if (Where_Utilizado == false)
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }
                    else
                    {
                        Mi_SQL += "AND ";
                    }

                    //Resto de la consulta
                    Mi_SQL += "Cat_Con_Cuentas_Contables.Partida_ID = '" + Datos.P_Partida_ID + "' ";
                }

                if (String.IsNullOrEmpty(Datos.P_Tipo_Cuenta) == false)
                {
                    //Verificar si ya se ha utilizado la clausula where
                    if (Where_Utilizado == false)
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }
                    else
                    {
                        Mi_SQL += "AND ";
                    }

                    //Resto de la consulta
                    Mi_SQL += "Cat_Con_Cuentas_Contables.Tipo_Cuenta = '" + Datos.P_Tipo_Cuenta + "' ";
                }

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
    }
}