﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Reporte_Cheques.Negocio;

/// <summary>
/// Summary description for Cls_Rpt_Con_Cheques_Datos
/// </summary>
namespace JAPAMI.Reporte_Cheques.Datos
{
    public class Cls_Rpt_Con_Cheques_Datos
    {
        public Cls_Rpt_Con_Cheques_Datos()
        {
        }

        #region (Metodos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Cheques
        ///DESCRIPCION:             Consulta de los cheques con diversos filtros
        ///PARAMETROS:              Datos: Variable de la capa de negocios
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              17/Abril/2012 16:35
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Cheques(Cls_Rpt_Con_Cheques_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado de la consulta
            Boolean Where_Utilizado = false; //variable que indica si la clausula where ya ha sido utilizada

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT ABS(Ope_Con_Pagos.No_Pago) AS No_Pago, Ope_Con_Pagos.Fecha_Pago, ABS(Ope_Con_Pagos.No_Solicitud_Pago) AS No_Solicitud_Pago, " +
                    "Ope_Con_Solicitud_Pagos.Fecha_Solicitud, Ope_Con_Polizas.Prefijo, Ope_Con_Pagos.Mes_Ano, Ope_Con_Polizas.Fecha_Poliza, " +
                    "Cat_Con_Tipo_Polizas.Descripcion AS Tipo_Poliza, Cat_Nom_Bancos.Nombre AS Banco, Ope_Con_Pagos.No_Cheque, Ope_Con_Pagos.Estatus, " +
                    "Ope_Con_Solicitud_Pagos.Monto, Cat_Com_Proveedores.Nombre AS Proveedor, " +
                    "(Cat_Empleados.Apellido_Paterno + ' ' + ISNULL(Cat_Empleados.Apellido_Materno, '') + ' ' + Cat_Empleados.Nombre) AS Empleado, " +
                    "Cat_Empleados.No_Empleado, " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + " AS Concepto " +
                    "FROM Ope_Con_Pagos " +
                    "INNER JOIN Ope_Con_Solicitud_Pagos ON Ope_Con_Pagos.No_Solicitud_Pago = Ope_Con_Solicitud_Pagos.No_Solicitud_Pago " +
                    "INNER JOIN Ope_Con_Polizas ON Ope_Con_Pagos.No_Poliza = Ope_Con_Polizas.No_Poliza  and Ope_Con_Pagos.Tipo_poliza_id = Ope_Con_Polizas.Tipo_poliza_id and Ope_Con_Pagos.Mes_Ano = Ope_Con_Polizas.Mes_Ano " +
                    "INNER JOIN Cat_Con_Tipo_Polizas ON Ope_Con_Polizas.Tipo_Poliza_ID = Cat_Con_Tipo_Polizas.Tipo_Poliza_ID " +
                    "INNER JOIN Cat_Nom_Bancos ON Ope_Con_Pagos.Banco_ID = Cat_Nom_Bancos.Banco_ID " +
                    "LEFT JOIN Cat_Com_Proveedores ON Ope_Con_Solicitud_Pagos.Proveedor_ID = Cat_Com_Proveedores.Proveedor_ID " +
                    "LEFT JOIN Cat_Empleados ON Ope_Con_Solicitud_Pagos.Empleado_ID = Cat_Empleados.Empleado_ID ";

                //Verificar los filtros
                if (String.IsNullOrEmpty(Datos.P_Banco_ID) == false)
                {
                    Mi_SQL += "WHERE Ope_Con_Pagos.Banco_ID = '" + Datos.P_Banco_ID + "' ";

                    Where_Utilizado = true;
                }

                if (String.IsNullOrEmpty(Datos.P_Estatus) == false)
                {
                    //verificar si la clausula where ya ha sido utilizada
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    //Resto de la consulta
                    Mi_SQL += "Ope_Con_Pagos.Estatus = '" + Datos.P_Estatus + "' ";
                }

                if (String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Inicio) != "01/01/0001")
                {
                    //verificar si la clausula where ya ha sido utilizada
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    //Resto de la consulta
                    Mi_SQL += "Ope_Con_Pagos.Fecha_Pago >= '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Inicio) + " 00:00:00' ";

                    //verificar si la fecha de fin tiene valor
                    if (String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Fin) != "01/01/0001")
                    {
                        Mi_SQL += "AND Ope_Con_Pagos.Fecha_Pago <= '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Fin) + " 23:59:59' ";
                    }
                }

                switch (Datos.P_Tipo)
                {
                    case "Empleado":
                        //verificar si la clausula where ya ha sido utilizada
                        if (!String.IsNullOrEmpty(Datos.P_Empleado_ID))
                        {       
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";
                                Where_Utilizado = true;
                            }

                            //Resto de la consulta
                            Mi_SQL += "Ope_Con_Solicitud_Pagos.Empleado_ID = '" + Datos.P_Empleado_ID + "' ";
                        }
                        break;

                    case "Proveedor":
                        //verificar si la clausula where ya ha sido utilizada
                        if(!String.IsNullOrEmpty(Datos.P_Proveedor_ID)){
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += "AND ";
                            }
                            else
                            {
                                Mi_SQL += "WHERE ";
                                Where_Utilizado = true;
                            }

                            //Resto de la consulta
                            Mi_SQL += "Ope_Con_Solicitud_Pagos.Proveedor_ID = '" + Datos.P_Proveedor_ID + "' ";
                        }
                        break;

                    default:
                        break;
                }

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
        #endregion
    }
}