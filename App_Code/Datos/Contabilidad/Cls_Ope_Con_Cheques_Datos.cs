﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Cheque.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Polizas.Datos;
using JAPAMI.Solicitud_Pagos.Datos;
using JAPAMI.Generar_Reservas.Datos;
using JAPAMI.Solicitud_Pagos_Ingresos.Negocio;
using JAPAMI.Solicitud_Pagos.Negocio;
/// <summary>
/// Summary description for Cls_Ope_Con_Autoriza_Solicitud_Pago_Datos
/// </summary>
namespace JAPAMI.Cheque.Datos
{
    public class Cls_Ope_Con_Cheques_Datos
    {
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Solicitudes_Autorizadas
        /// DESCRIPCION : Consulta las solicitudes de pago que estan Autorizadas o pagadas
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Solicitudes_Autorizadas(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT Solicitud." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva;
                Mi_SQL += ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Concepto;
                Mi_SQL += ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud;
                Mi_SQL += ", Solicitud.MES_ANO, Solicitud." + Ope_Con_Solicitud_Pagos.Campo_No_Poliza + ", Reserva." + Ope_Psp_Reservas.Campo_Beneficiario;
                Mi_SQL += ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura;
                Mi_SQL += ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Monto + ", Tipo." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + " as Tipo_Pago";
                Mi_SQL += ", Tipo." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion_Finanzas + " as Tipo_Pago_Finanzas, Banco." + Cat_Nom_Bancos.Campo_Nombre + " as Banco";
                Mi_SQL += ", Proveedor." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", Proveedor." + Cat_Com_Proveedores.Campo_Nombre + " as Proveedor ";
                Mi_SQL += ", ('E-'+Empleado." + Cat_Empleados.Campo_Empleado_ID + ")as Empleado_id , (Empleado." + Cat_Empleados.Campo_Nombre + "+' '+Empleado." + Cat_Empleados.Campo_Apellido_Paterno + "+' '+Empleado." + Cat_Empleados.Campo_Apellido_Materno + " ) as Empleado "; 
                Mi_SQL += ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Forma_Pago + ", Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Cuenta_Banco_Pago_ID + " as Banco_Pago ";
                Mi_SQL += " FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " Solicitud ";
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + " Tipo ON Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + "= Tipo." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " Reserva ON Solicitud." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + "= Reserva." + Ope_Psp_Reservas.Campo_No_Reserva;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " Banco ON Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Cuenta_Banco_Pago_ID + "= Banco." + Cat_Nom_Bancos.Campo_Banco_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " Proveedor ON Solicitud." + Cat_Com_Proveedores.Campo_Proveedor_ID + "= Proveedor." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " Empleado ON Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + "= Empleado." + Cat_Empleados.Campo_Empleado_ID;
                Mi_SQL += " WHERE Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Transferencia + " IS NULL ";
                if (!String.IsNullOrEmpty(Datos.P_Estatus))
                {
                    Mi_SQL += " AND (solicitud." + Ope_Con_Solicitud_Pagos.Campo_Estatus + " ='" + Datos.P_Estatus + "')";
                }
                else
                {
                    Mi_SQL += " AND (solicitud." + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = 'EJERCIDO')";
                }
                if (!String.IsNullOrEmpty(Datos.P_No_Solicitud_Pago) && Datos.P_No_Solicitud_Pago != "0")
                {
                    Mi_SQL += " AND " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago +
                    " = '" + Datos.P_No_Solicitud_Pago + "'";
                    if (String.IsNullOrEmpty(Datos.P_No_Solicitud_Pago))
                    {
                        Mi_SQL += " AND " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud +
                                " >= '" + Datos.P_Fecha_Inicio + " 00:00:00' AND " +
                         Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud +
                                " <= '" + Datos.P_Fecha_Final + " 23:59:59'";
                    }
                }
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID))
                {
                    Mi_SQL +=
                    " AND Tipo." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID +
                    " = '" + Datos.P_Tipo_Solicitud_Pago_ID + "'";
                    if (String.IsNullOrEmpty(Datos.P_No_Solicitud_Pago) || Datos.P_No_Solicitud_Pago == "0")
                    {
                        Mi_SQL += " AND " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud +
                            " >= '" + Datos.P_Fecha_Inicio + " 00:00:00' AND " +
                     Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud +
                            " <= '" + Datos.P_Fecha_Final + " 23:59:59'";
                    }
                }
                if ((String.IsNullOrEmpty(Datos.P_No_Solicitud_Pago) || Datos.P_No_Solicitud_Pago == "0") && String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID) && !String.IsNullOrEmpty(Datos.P_Fecha_Inicio) && !String.IsNullOrEmpty(Datos.P_Fecha_Final))
                {
                    Mi_SQL += " AND " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud +
                        " >= '" + Datos.P_Fecha_Inicio + " 00:00:00'  AND " +
                Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud +
                        " <= '" + Datos.P_Fecha_Final + " 23:59:59'";
                }
                Mi_SQL += " ORDER BY " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ASC";
                Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                //return OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Ds_Oracle.Tables[0];
                //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Tipos_Solicitudes
        /// DESCRIPCION : Consulta los tipos de solicitudes de pago que existen
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Tipos_Solicitudes(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + " FROM  " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago;
                Mi_SQL += " WHERE " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Estatus + " ='ACTIVO'";
                Mi_SQL += " ORDER BY " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Pago
        /// DESCRIPCION : Consulta los tipos de solicitudes de pago que existen
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Pago(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT * FROM  " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos;
                Mi_SQL += " WHERE " + Ope_Con_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Pago
        /// DESCRIPCION : Consulta los tipos de solicitudes de pago que existen
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Pago_Cheque(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT PAGO.*,'0' as monto,'0' as cantidad_letra FROM  " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos;
                Mi_SQL += " PAGO WHERE " + Ope_Con_Pagos.Campo_No_Pago + "='" + Datos.P_No_Cheque + "'";
                Mi_SQL += " AND " + Ope_Con_Pagos.Campo_Estatus + "='PAGADO'";
                Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                //return OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Ds_Oracle.Tables[0];
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_Proveedor
        /// DESCRIPCION : Consulta los tipos de solicitudes de pago que existen
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Proveedor(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT " + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + "," + Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + " FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Datos.P_No_Solicitud_Pago + "'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Solicitud_Pago
        /// DESCRIPCION : Consulta los tipos de solicitudes de pago que existen
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Solicitud_Pago(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT Solicitud.*, Banco."+ Cat_Nom_Bancos.Campo_Nombre+" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos+" Solicitud ";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN  " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " Banco On Solicitud." + Ope_Con_Solicitud_Pagos.Campo_Cuenta_Banco_Pago_ID+ " =Banco.";
                Mi_SQL = Mi_SQL + Cat_Nom_Bancos.Campo_Banco_ID + " WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Datos.P_No_Solicitud_Pago + "'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_Pago
        /// DESCRIPCION : Consulta todos los datos de la solicitud de pago que selecciono
        ///               el usuario
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 25/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Datos_Pago(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Pago + ", ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Cuenta_Contable_ID + ", ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Nombre + ", ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Banco_ID + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Estatus + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Fecha_Pago + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Beneficiario_Pago + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Motivo_Cancelacion + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Cheque + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Forma_Pago + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Referencia_Transferencia_Banca + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Comentarios);
                Mi_SQL.Append(" FROM " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " ON " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Pago+"= ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Pago);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " ON " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Banco_ID  + "= ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Banco_ID);
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                Mi_SQL.Append(" AND " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Estatus + " ='PAGADO'");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Bancos
        /// DESCRIPCION : Consulta los tipos de solicitudes de pago que existen
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Bancos(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT " + Cat_Nom_Bancos.Campo_Banco_ID + ", " + Cat_Nom_Bancos.Campo_Nombre + " FROM  " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos;
                Mi_SQL += " ORDER BY " + Cat_Nom_Bancos.Campo_Banco_ID + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Cuenta_Contable_Banco
        /// DESCRIPCION : Consulta si el banco proporcionado tiene una cuenta contable
        ///               asiganada
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 23-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Cuenta_Contable_Banco(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Sql = new SqlDataAdapter();
            DataSet Ds_Sql = new DataSet();
            DataTable Dt_Psp = new DataTable();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("SELECT " + Cat_Nom_Bancos.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append(" FROM " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos);
                Mi_SQL.Append(" WHERE " + Cat_Nom_Bancos.Campo_Banco_ID + " = '" + Datos.P_Banco_ID + "'");
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Sql.SelectCommand = Cmmd;
                Dt_Sql.Fill(Ds_Sql);
                Dt_Psp = Ds_Sql.Tables[0];
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Dt_Psp;
                //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Cuenta_Proveedor_Bancario
        /// DESCRIPCION : Consulta si el banco proporcionado tiene una cuenta contable
        ///               asiganada
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 14-Junio-2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Cuenta_Proveedor_Bancario(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Sql = new SqlDataAdapter();
            DataSet Ds_Sql = new DataSet();
            DataTable Dt_Psp = new DataTable();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("SELECT " + Cat_Nom_Bancos.Campo_Cuenta_Contable_Proveedor);
                Mi_SQL.Append(" FROM " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos);
                Mi_SQL.Append(" WHERE " + Cat_Nom_Bancos.Campo_Banco_ID + " = '" + Datos.P_Banco_ID + "'");
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Sql.SelectCommand = Cmmd;
                Dt_Sql.Fill(Ds_Sql);
                Dt_Psp = Ds_Sql.Tables[0];
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Dt_Psp;
                //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Cheque
        /// DESCRIPCION : Inserta en nuevo Cheque en la BD
        /// PARAMETROS  : Datos: Contiene los datos de los filtros
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 21/Noviembre/2011
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static String Alta_Cheque(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;
            Object Compromisos_ID; //Variable que contendrá el ID de la consulta
            Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
            Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
            Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
            String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_Reserva_Detalle = new DataTable();
            DataTable Dt_Reserva = new DataTable();
            DataTable Dt_Saldos_Reserva = new DataTable();
            String Dependencia_ID = "";
            Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Rs_Solcitud_Pago_Detalle = new Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio();
            String Tipo_Reserva = "";
            DataRow Fila;
            String Afectaciones = "";
            int Afectacion_Presupuestal;
            String Servicios_Generales = "";
            Double Saldo_Reserva;
            int Registro_Presupuestal;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();            
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Busca el maximo ID de la tabla Compromisos
                Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Con_Pagos.Campo_No_Pago + "),'00000') ";
                Mi_SQL = Mi_SQL + "FROM " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos;
                Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                Compromisos_ID = Cmmd.ExecuteScalar();
                if (Convert.IsDBNull(Compromisos_ID)) //Si no existen valores en la tabla, asigna el primer valor manualmente.
                {
                    Datos.P_No_Pago = "00001";
                }
                else // Si ya existen registros, toma el valor maximo y le suma 1 para el nuevo registro.
                {
                    Datos.P_No_Pago = String.Format("{0:00000}", Convert.ToInt32(Compromisos_ID) + 1);
                }
                Mi_SQL = "INSERT INTO " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "(";
                Mi_SQL += Ope_Con_Pagos.Campo_No_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_No_Solicitud_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Banco_ID + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Fecha_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Beneficiario_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Forma_Pago + ", ";
                if (Datos.P_Tipo_Pago == "CHEQUE")
                {
                    Mi_SQL += Ope_Con_Pagos.Campo_No_Cheque + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Abono_Cuenta_Beneficiario + ", ";
                }
                else
                {
                    Mi_SQL += Ope_Con_Pagos.Campo_Referencia_Transferencia_Banca + ", ";
                }
                Mi_SQL += Ope_Con_Pagos.Campo_Estatus + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Comentarios + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Usuario_Creo + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Fecha_Creo + ") VALUES('";
                Mi_SQL += Datos.P_No_Pago + "', '";
                Mi_SQL += Datos.P_No_Solicitud_Pago + "', '";
                Mi_SQL += Datos.P_Banco_ID + "','";
                Mi_SQL += Datos.P_Fecha_Pago + "','";
                Mi_SQL += Datos.P_Beneficiario_Pago + "', '";
                Mi_SQL += Datos.P_Tipo_Pago + "',";
                if (Datos.P_Tipo_Pago == "CHEQUE")
                {
                    Mi_SQL += "'"+Datos.P_No_Cheque + "',";
                    Mi_SQL += "'" + Datos.P_Abono_Cuenta_Beneficiario + "','";
                }
                else
                {
                    Mi_SQL += "'" + Datos.P_Referencia + "', '";
                }
                Mi_SQL += Datos.P_Estatus + "', '";
                Mi_SQL += Datos.P_Comentario + "', '";
                Mi_SQL += Datos.P_Usuario_Creo + "',  GETDATE())";
                Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Estatus + "='" + Datos.P_Estatus + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Cuenta_Banco_Pago_ID + "='" + Datos.P_Banco_ID + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + "= GETDATE() " + " WHERE ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Datos.P_No_Solicitud_Pago + "'";
                Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos                     
                
                Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')" +
                " FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                " WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'" +
                " AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00002'" +
                " GROUP BY " + Ope_Con_Polizas.Campo_No_Poliza +
                " ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC";
                Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                No_Poliza = Cmmd.ExecuteScalar();
                //Valida si el ID es nulo para asignarle automaticamente el primer registro
                if (Convert.IsDBNull(No_Poliza))
                {
                    No_Poliza = "0000000001";
                }
                //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                else
                {
                    No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                }
                //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                Mi_SQL = "INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                " (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", " +
                Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", " +
                Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", " +
                Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", " +
                Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", " +
                Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + "," + Ope_Con_Polizas.Campo_Referencia + ")" +
                " VALUES ('" + No_Poliza + "', '00002', '" + Mes_Anio + "'," +
                " CONVERT(varchar, GETDATE(),103)," +
                " '" + "CHEQUE " + Datos.P_No_Pago + "', " + Datos.P_Monto_Transferencia + ", " + Datos.P_Monto_Transferencia + "," + Datos.P_No_Partidas + ", " +
                "'" + Datos.P_Usuario_Creo + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', " +
                "'" + Cls_Sessiones.Empleado_ID +"','" + Datos.P_Referencia+ "')";
                Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos                  
                
                //Da de alta los detalles de la póliza
                foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                {
                    //consulta el saldo de la cuenta contable
                    Mi_SQL = "SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - " +
                    " ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo" +
                    " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                    " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'";
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Saldo = Cmmd.ExecuteScalar();                    
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                    }
                    //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')" +
                    " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Consecutivo = Cmmd.ExecuteScalar();
                    
                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(Consecutivo))
                    {
                        Consecutivo = "1";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                    }

                    //Inserta el registro del detalle de la póliza en la base de datos
                    Mi_SQL = "INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                    "(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Consecutivo + "," + Ope_Con_Polizas_Detalles.Campo_Referencia + ")" +
                    " VALUES('" + No_Poliza + "', '00002', '" + Mes_Anio + "', " +
                    Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + "," +
                    " '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'," +
                    " '" + "CHEQUE " + Datos.P_No_Pago + "', " +
                    Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", " +
                    Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", " +
                    Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ",'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Referencia].ToString() + "')";
                    Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                }
                // se incerta el numero de poliza, el tipo de poliza y el mes_anio del pago
                Mi_SQL = "UPDATE " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " SET ";
                Mi_SQL += Ope_Con_Pagos.Campo_Mes_Ano + "='" + Mes_Anio + "',";
                Mi_SQL += Ope_Con_Pagos.Campo_Tipo_Poliza_ID + "='00002',";
                Mi_SQL += Ope_Con_Pagos.Campo_No_poliza + "='" + No_Poliza + "' WHERE ";
                Mi_SQL += Ope_Con_Pagos.Campo_No_Pago + " ='" + Datos.P_No_Pago + "'";
                Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                
                Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza,Cmmd);
                if (!String.IsNullOrEmpty(Datos.P_Servicios_Generales))
                {
                    if (Datos.P_Servicios_Generales == "SI")
                    {
                        Servicios_Generales = "SI";
                        Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud_Servicios_Generales(Datos.P_No_Solicitud_Pago, Cmmd);
                    }
                    else
                    {
                        Servicios_Generales = "NO";
                        Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Datos.P_No_Solicitud_Pago, Cmmd);
                    }
                }
                else
                {
                    Servicios_Generales = "NO";
                    Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Datos.P_No_Solicitud_Pago, Cmmd);
                }
                Afectacion_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PAGADO", "EJERCIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                if (Afectacion_Presupuestal > 0)
                {
                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "PAGADO", "EJERCIDO", (Convert.ToDouble(Datos.P_Monto_Transferencia)-Convert.ToDouble(Datos.P_IVA)), Convert.ToString(No_Poliza), "00002", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                    Afectaciones = "SI";
                }
                else
                {
                    Afectaciones = "NO";
                }
                if (Afectaciones == "SI")
                {
                    //  para las consultas de la reserva, reserva detalles 
                    Dt_Reserva = Consulta_Dependencia_Reserva(Datos);
                    Dt_Reserva_Detalle = Consulta_Reserva_Detalles(Datos);
                    if (Dt_Reserva.Rows.Count > 0)
                    {
                        if (Servicios_Generales == "NO")
                        {
                            Dependencia_ID = Dt_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Dependencia_ID].ToString();
                        }
                        Tipo_Reserva = Dt_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Tipo_Reserva].ToString();

                        if (Tipo_Reserva == "UNICA" && (Datos.P_Estatus_Comparacion == "PAGADO"))
                        {
                            Saldo_Reserva = 0;
                            if (Dt_Saldos_Reserva != null)
                            {
                                if (Dt_Saldos_Reserva.Rows.Count <= 0 && Dt_Saldos_Reserva.Columns.Count <= 0)
                                {
                                    Dt_Saldos_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                                    Dt_Saldos_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                                    Dt_Saldos_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                                    Dt_Saldos_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                                    Dt_Saldos_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                                    Dt_Saldos_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                                }
                                foreach (DataRow Registro in Dt_Reserva_Detalle.Rows)
                                {
                                    Fila = Dt_Saldos_Reserva.NewRow();
                                    if (Servicios_Generales == "NO")
                                    {
                                        Fila["DEPENDENCIA_ID"] = Dependencia_ID;
                                    }
                                    else
                                    {
                                        Fila["DEPENDENCIA_ID"] = Registro["DEPENDENCIA_ID"].ToString();
                                    }
                                    Fila["FUENTE_FINANCIAMIENTO_ID"] = Registro["FTE_FINANCIAMIENTO_ID"].ToString();
                                    Fila["PROGRAMA_ID"] = Registro["PROYECTO_PROGRAMA_ID"].ToString();
                                    Fila["PARTIDA_ID"] = Registro["PARTIDA_ID"].ToString();
                                    Fila["IMPORTE"] = Registro["SALDO"].ToString();
                                    Fila["ANIO"] = Dt_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Anio].ToString();
                                    Saldo_Reserva = Saldo_Reserva + Convert.ToDouble(Registro["SALDO"].ToString());
                                    Dt_Saldos_Reserva.Rows.Add(Fila);
                                    Dt_Saldos_Reserva.AcceptChanges();
                                }
                            }
                            if (Saldo_Reserva > 0)
                            {
                                Afectacion_Presupuestal = 0;
                                Afectacion_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DISPONIBLE", "PRE_COMPROMETIDO", Dt_Saldos_Reserva, Cmmd);
                                if (Afectacion_Presupuestal > 0)
                                {
                                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "DISPONIBLE", "PRE_COMPROMETIDO", Saldo_Reserva, "", "", "", "", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                                    Afectaciones = "SI";
                                }
                                else
                                {
                                    Afectaciones = "NO";
                                }
                            }
                            //  los saldos de la reserva  se hacen cero
                            Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ";
                            Mi_SQL += Ope_Psp_Reservas.Campo_Saldo + "= 0";
                            Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "='" + Datos.P_No_Reserva + "'";
                            Cmmd.CommandText = Mi_SQL;
                            Cmmd.ExecuteNonQuery();


                            //se actualiza el importe de la reserva 
                            Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ";
                            Mi_SQL += Ope_Psp_Reservas.Campo_Importe_Inicial + "=" + Ope_Psp_Reservas.Campo_Importe_Inicial + "-" + Saldo_Reserva;
                            Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "='" + Datos.P_No_Reserva + "'";
                            Cmmd.CommandText = Mi_SQL;
                            Cmmd.ExecuteNonQuery();
                            ///Actualizar los saldos de las partidas
                            foreach (DataRow Registro in Dt_Reserva_Detalle.Rows)
                            {
                                //se actualiza el importe de los detalles de la reserva 
                                Mi_SQL = "UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " SET ";
                                Mi_SQL += Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + "=" + Ope_Psp_Reservas.Campo_Importe_Inicial + "-" + Registro["SALDO"].ToString() + ", ";
                                Mi_SQL += Ope_Psp_Reservas_Detalles.Campo_Saldo + "=0";// +Ope_Psp_Reservas.Campo_Importe_Inicial + "-" + Registro["SALDO"].ToString();
                                Mi_SQL += " WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + "='" + Datos.P_No_Reserva + "'";
                                Mi_SQL += " AND " + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + "='" + Registro["FTE_FINANCIAMIENTO_ID"].ToString() + "'";
                                if (Servicios_Generales == "NO")
                                {
                                    Mi_SQL += " AND " + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + "='" + Dependencia_ID + "'";
                                }
                                else
                                {
                                    Mi_SQL += " AND " + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + "='" + Registro["DEPENDENCIA_ID"].ToString() + "'";
                                }
                                Mi_SQL += " AND " + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + "='" + Registro["PARTIDA_ID"].ToString() + "'";
                                Mi_SQL += " AND " + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + "='" + Registro["PROYECTO_PROGRAMA_ID"].ToString() + "'";
                                Cmmd.CommandText = Mi_SQL;
                                Cmmd.ExecuteNonQuery();

                            }
                            //SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        }
                    }
                    Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Pago + "='" + Datos.P_No_Pago + "'";
                    Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'";
                    Cmmd.CommandText = Mi_SQL;
                    Cmmd.ExecuteNonQuery();
                }
                if (Datos.P_Cmmd == null)
                {
                    if (Afectaciones == "SI")
                    {
                        Trans.Commit();
                    }
                    else
                    {
                        Trans.Rollback();
                    }
                }
                return Datos.P_No_Pago + "-" + Afectaciones;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + ex.Message, ex);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Cheque_Finiquito
        /// DESCRIPCION : Inserta en nuevo Cheque en la BD
        /// PARAMETROS  : Datos: Contiene los datos de los filtros
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 14/Agosto/2013
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static String Alta_Cheque_Finiquito(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;
            Object Compromisos_ID; //Variable que contendrá el ID de la consulta
            Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
            Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
            Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
            String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_Reserva_Detalle = new DataTable();
            DataTable Dt_Reserva = new DataTable();
            DataTable Dt_Saldos_Reserva = new DataTable();
            String Dependencia_ID = "";
            Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Rs_Solcitud_Pago_Detalle = new Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio();
            String Tipo_Reserva = "";
            DataRow Fila;
            String Afectaciones = "";
            int Afectacion_Presupuestal;
            Double Saldo_Reserva;
            int Registro_Presupuestal;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();            
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Busca el maximo ID de la tabla Compromisos
                Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Con_Pagos.Campo_No_Pago + "),'00000') ";
                Mi_SQL = Mi_SQL + "FROM " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos;
                Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                Compromisos_ID = Cmmd.ExecuteScalar();
                //Compromisos_ID = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                if (Convert.IsDBNull(Compromisos_ID)) //Si no existen valores en la tabla, asigna el primer valor manualmente.
                {
                    Datos.P_No_Pago = "00001";
                }
                else // Si ya existen registros, toma el valor maximo y le suma 1 para el nuevo registro.
                {
                    Datos.P_No_Pago = String.Format("{0:00000}", Convert.ToInt32(Compromisos_ID) + 1);
                }
                Mi_SQL = "INSERT INTO " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "(";
                Mi_SQL += Ope_Con_Pagos.Campo_No_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_No_Solicitud_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Banco_ID + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Fecha_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Beneficiario_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Forma_Pago + ", ";
                if (Datos.P_Tipo_Pago == "CHEQUE")
                {
                    Mi_SQL += Ope_Con_Pagos.Campo_No_Cheque + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Abono_Cuenta_Beneficiario + ", ";
                }
                else
                {
                    Mi_SQL += Ope_Con_Pagos.Campo_Referencia_Transferencia_Banca + ", ";
                }
                Mi_SQL += Ope_Con_Pagos.Campo_Estatus + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Comentarios + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Usuario_Creo + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Fecha_Creo + ") VALUES('";
                Mi_SQL += Datos.P_No_Pago + "', '";
                Mi_SQL += Datos.P_No_Solicitud_Pago + "', '";
                Mi_SQL += Datos.P_Banco_ID + "','";
                Mi_SQL += Datos.P_Fecha_Pago + "','";
                Mi_SQL += Datos.P_Beneficiario_Pago + "', '";
                Mi_SQL += Datos.P_Tipo_Pago + "',";
                if (Datos.P_Tipo_Pago == "CHEQUE")
                {
                    Mi_SQL += "'"+Datos.P_No_Cheque + "',";
                    Mi_SQL += "'" + Datos.P_Abono_Cuenta_Beneficiario + "','";
                }
                else
                {
                    Mi_SQL += "'" + Datos.P_Referencia + "', '";
                }
                Mi_SQL += Datos.P_Estatus + "', '";
                Mi_SQL += Datos.P_Comentario + "', '";
                Mi_SQL += Datos.P_Usuario_Creo + "',  GETDATE())";
                Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();
                    Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Estatus + "='" + Datos.P_Estatus + "',";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Cuenta_Banco_Pago_ID + "='" + Datos.P_Banco_ID + "',";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "',";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + "= GETDATE() " + " WHERE ";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Datos.P_No_Solicitud_Pago + "'";
                    Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos
                //Consulta para la obtención del último ID dado de alta 
                Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')" +
                " FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                " WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'" +
                " AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00002'" +
                " GROUP BY " + Ope_Con_Polizas.Campo_No_Poliza +
                " ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC";
                Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                No_Poliza = Cmmd.ExecuteScalar();
                //Valida si el ID es nulo para asignarle automaticamente el primer registro
                if (Convert.IsDBNull(No_Poliza))
                {
                    No_Poliza = "0000000001";
                }
                //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                else
                {
                    No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                }
                //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                Mi_SQL = "INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                " (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", " +
                Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", " +
                Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", " +
                Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", " +
                Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", " +
                Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + "," + Ope_Con_Polizas.Campo_Referencia + ")" +
                " VALUES ('" + No_Poliza + "', '00002', '" + Mes_Anio + "'," +
                " CONVERT(varchar, GETDATE(),103)," +
                " '" + "CHEQUE " + Datos.P_No_Pago + "', " + Datos.P_Monto_Transferencia + ", " + Datos.P_Monto_Transferencia + "," + Datos.P_No_Partidas + ", " +
                "'" + Datos.P_Usuario_Creo + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', " +
                "'" + Cls_Sessiones.Empleado_ID +"','" + Datos.P_Referencia+ "')";
                Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos                  
                //Da de alta los detalles de la póliza
                foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                {
                    //consulta el saldo de la cuenta contable
                    Mi_SQL = "SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - " +
                    " ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo" +
                    " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                    " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'";
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Saldo = Cmmd.ExecuteScalar();                    
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                    }
                    //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')" +
                    " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Consecutivo = Cmmd.ExecuteScalar();
                    
                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(Consecutivo))
                    {
                        Consecutivo = "1";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                    }

                    //Inserta el registro del detalle de la póliza en la base de datos
                    Mi_SQL = "INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                    "(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Consecutivo + "," + Ope_Con_Polizas_Detalles.Campo_Referencia + ")" +
                    " VALUES('" + No_Poliza + "', '00002', '" + Mes_Anio + "', " +
                    Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + "," +
                    " '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'," +
                    " '" + "CHEQUE " + Datos.P_No_Pago + "', " +
                    Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", " +
                    Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", " +
                    Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ",'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Referencia].ToString() + "')";
                    Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                }
                // se incerta el numero de poliza, el tipo de poliza y el mes_anio del pago
                Mi_SQL = "UPDATE " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " SET ";
                Mi_SQL += Ope_Con_Pagos.Campo_Mes_Ano + "='" + Mes_Anio + "',";
                Mi_SQL += Ope_Con_Pagos.Campo_Tipo_Poliza_ID + "='00002',";
                Mi_SQL += Ope_Con_Pagos.Campo_No_poliza + "='" + No_Poliza + "' WHERE ";
                Mi_SQL += Ope_Con_Pagos.Campo_No_Pago + " ='" + Datos.P_No_Pago + "'";
                Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza,Cmmd);
                Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud_Finiquito(Datos.P_No_Solicitud_Pago, Cmmd);
                Afectacion_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PAGADO", "EJERCIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                if (Afectacion_Presupuestal > 0)
                {
                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "PAGADO", "EJERCIDO",Convert.ToDouble(Datos.P_Monto_Transferencia), Convert.ToString(No_Poliza), "00002", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                    Afectaciones = "SI";
                }
                else
                {
                    Afectaciones = "NO";
                }
                    //Actualiza el impote de la partida presupuestal
                if (Afectaciones == "SI")
                {
                    Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Pago + "='" + Datos.P_No_Pago + "'";
                    Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'";
                    Cmmd.CommandText = Mi_SQL;
                    Cmmd.ExecuteNonQuery();
                }
                if (Datos.P_Cmmd == null)
                {
                    if (Afectaciones == "SI")
                    {
                        Trans.Commit();
                    }
                    else
                    {
                        Trans.Rollback();
                    }
                }
                return Datos.P_No_Pago + "-" + Afectaciones;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + ex.Message, ex);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Cheque_Masivo
        /// DESCRIPCION : Inserta en nuevo Cheque en la BD
        /// PARAMETROS  : Datos: Contiene los datos de los filtros
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 21/Junio/2013
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static String Alta_Cheque_Masivo(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;
            Object Compromisos_ID; //Variable que contendrá el ID de la consulta
            Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
            Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
            Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
            String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_Reserva_Detalle = new DataTable();
            DataTable Dt_Reserva = new DataTable();
            DataTable Dt_Solicitud_Reserva= new DataTable();
            DataTable Dt_Saldos_Reserva = new DataTable();
            String Dependencia_ID = "";
            Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Rs_Solcitud_Pago_Detalle = new Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio();
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Solicitudes= new Cls_Ope_Con_Solicitud_Pagos_Negocio();
            String Tipo_Reserva = "";
            DataRow Fila;
            String Afectaciones = "";
            int Afectacion_Presupuestal;
            Double Saldo_Reserva;
            String Servicios_Generales="";
            int Registro_Presupuestal;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Busca el maximo ID de la tabla Compromisos
                Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Con_Pagos.Campo_No_Pago + "),'00000') ";
                Mi_SQL = Mi_SQL + "FROM " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos;
                Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                Compromisos_ID = Cmmd.ExecuteScalar();
                //Compromisos_ID = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                if (Convert.IsDBNull(Compromisos_ID)) //Si no existen valores en la tabla, asigna el primer valor manualmente.
                {
                    Datos.P_No_Pago = "00001";
                }
                else // Si ya existen registros, toma el valor maximo y le suma 1 para el nuevo registro.
                {
                    Datos.P_No_Pago = String.Format("{0:00000}", Convert.ToInt32(Compromisos_ID) + 1);
                }
                Mi_SQL = "INSERT INTO " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "(";
                Mi_SQL += Ope_Con_Pagos.Campo_No_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Banco_ID + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Fecha_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Beneficiario_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Forma_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_No_Cheque + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Abono_Cuenta_Beneficiario + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Estatus + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Comentarios + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Usuario_Creo + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Fecha_Creo + ") VALUES('";
                Mi_SQL += Datos.P_No_Pago + "', '";
                Mi_SQL += Datos.P_Banco_ID + "','";
                Mi_SQL += Datos.P_Fecha_Pago + "','";
                Mi_SQL += Datos.P_Beneficiario_Pago + "', '";
                Mi_SQL += Datos.P_Tipo_Pago + "',";
                Mi_SQL += "'" + Datos.P_No_Cheque + "',";
                Mi_SQL += "'" + Datos.P_Abono_Cuenta_Beneficiario + "','";
                Mi_SQL += Datos.P_Estatus + "', '";
                Mi_SQL += Datos.P_Comentario + "', '";
                Mi_SQL += Datos.P_Usuario_Creo + "',  GETDATE())";
                Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                
                //Se actualizan los registros de las solicitudes que se van a pagar
                foreach (DataRow Renglon_Solicitud in Datos.P_Dt_Solicitudes_Masivas.Rows)
                {
                    Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Estatus + "='" + Datos.P_Estatus + "',";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Cuenta_Banco_Pago_ID + "='" + Datos.P_Banco_ID + "',";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "',";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + "= GETDATE() " + " WHERE ";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Renglon_Solicitud["No_Solicitud"].ToString() + "'";
                    Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos                     
                }
                Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')" +
                " FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                " WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'" +
                " AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00002'" +
                " GROUP BY " + Ope_Con_Polizas.Campo_No_Poliza +
                " ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC";
                Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                No_Poliza = Cmmd.ExecuteScalar();
                if (Convert.IsDBNull(No_Poliza))
                {
                    No_Poliza = "0000000001";
                }
                else
                {
                    No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                }

                //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                Mi_SQL = "INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                " (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", " +
                Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", " +
                Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", " +
                Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", " +
                Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", " +
                Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + "," + Ope_Con_Polizas.Campo_Referencia + ")" +
                " VALUES ('" + No_Poliza + "', '00002', '" + Mes_Anio + "'," +
                " CONVERT(varchar, GETDATE(),103)," +
                " '" + "CHEQUE " + Datos.P_No_Pago + "', " + Datos.P_Monto_Transferencia + ", " + Datos.P_Monto_Transferencia + "," + Datos.P_No_Partidas + ", " +
                "'" + Datos.P_Usuario_Creo + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', " +
                "'" + Cls_Sessiones.Empleado_ID + "','" + Datos.P_Referencia + "')";
                Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos                  
                //Da de alta los detalles de la póliza
                foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                {
                    //consulta el saldo de la cuenta contable
                    Mi_SQL = "SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - " +
                    " ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo" +
                    " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                    " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'";
                    //Saldo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Saldo = Cmmd.ExecuteScalar();
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                    }
                    //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')" +
                    " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Consecutivo = Cmmd.ExecuteScalar();
                    //Consecutivo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(Consecutivo))
                    {
                        Consecutivo = "1";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                    }

                    //Inserta el registro del detalle de la póliza en la base de datos
                    Mi_SQL = "INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                    "(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Consecutivo + "," + Ope_Con_Polizas_Detalles.Campo_Referencia + ")" +
                    " VALUES('" + No_Poliza + "', '00002', '" + Mes_Anio + "', " +
                    Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + "," +
                    " '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'," +
                    " '" + "CHEQUE " + Datos.P_No_Pago + "', " +
                    Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", " +
                    Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", " +
                    Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ",'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Referencia].ToString() + "')";
                    Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                // se incerta el numero de poliza, el tipo de poliza y el mes_anio del pago
                Mi_SQL = "UPDATE " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " SET ";
                Mi_SQL += Ope_Con_Pagos.Campo_Mes_Ano + "='" + Mes_Anio + "',";
                Mi_SQL += Ope_Con_Pagos.Campo_Tipo_Poliza_ID + "='00002',";
                Mi_SQL += Ope_Con_Pagos.Campo_No_poliza + "='" + No_Poliza + "' WHERE ";
                Mi_SQL += Ope_Con_Pagos.Campo_No_Pago + " ='" + Datos.P_No_Pago + "'";
                Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza, Cmmd);

                foreach (DataRow Fila_Solicitud in Datos.P_Dt_Solicitudes_Masivas.Rows)
                {
                    Afectacion_Presupuestal = 0;
                    Dt_Partidas = new DataTable();
                    Rs_Solcitud_Pago_Detalle.P_No_Solicitud_Pago = Fila_Solicitud["No_Solicitud"].ToString();
                    Rs_Solcitud_Pago_Detalle.P_Cmmd = Cmmd;
                    Dt_Partidas = Rs_Solcitud_Pago_Detalle.Consulta_Detalles_Solicitud();
                    if (!String.IsNullOrEmpty(Datos.P_Servicios_Generales))
                    {
                        if (Datos.P_Servicios_Generales == "SI")
                        {
                            Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud_Servicios_Generales(Fila_Solicitud["No_Solicitud"].ToString(), Cmmd);
                            Servicios_Generales="SI";
                        }
                        else
                        {
                            Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Fila_Solicitud["No_Solicitud"].ToString(), Cmmd);
                            Servicios_Generales="NO";
                        }
                    }
                    else
                    {
                        Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Fila_Solicitud["No_Solicitud"].ToString(), Cmmd);
                        Servicios_Generales="NO";
                    }
                    Afectacion_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PAGADO", "EJERCIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                    if (Afectacion_Presupuestal > 0)
                    {
                        Rs_Solicitudes.P_No_Solicitud_Pago=Fila_Solicitud["No_Solicitud"].ToString();
                        Rs_Solicitudes.P_Cmmd=Cmmd;
                        Dt_Solicitud_Reserva=Rs_Solicitudes.Consulta_Datos_Solicitud_Pago();
                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Dt_Solicitud_Reserva.Rows[0][Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString(), "PAGADO", "EJERCIDO", Convert.ToDouble(Dt_Solicitud_Reserva.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Monto].ToString()), Convert.ToString(No_Poliza), "00002", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                        Afectaciones = "SI";
                    }
                    else
                    {
                        Afectaciones = "NO";
                    }
                    if (Afectaciones == "SI")
                    {
                        //  para las consultas de la reserva, reserva detalles 
                        Dt_Reserva = Consulta_Dependencia_Reserva(Dt_Solicitud_Reserva.Rows[0][Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString(),Cmmd);
                        Dt_Reserva_Detalle = Consulta_Reserva_Detalles(Dt_Solicitud_Reserva.Rows[0][Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString(),Cmmd);
                        if (Dt_Reserva.Rows.Count > 0)
                        {
                            if (Servicios_Generales == "NO")
                            {
                                Dependencia_ID = Dt_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Dependencia_ID].ToString();
                            }
                            Tipo_Reserva = Dt_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Tipo_Reserva].ToString();

                            if (Tipo_Reserva == "UNICA" && (Datos.P_Estatus_Comparacion == "PAGADO"))
                            {
                                Saldo_Reserva = 0;
                                if (Dt_Saldos_Reserva != null)
                                {
                                    if (Dt_Saldos_Reserva.Rows.Count <= 0 && Dt_Saldos_Reserva.Columns.Count <= 0)
                                    {
                                        Dt_Saldos_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                                        Dt_Saldos_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                                        Dt_Saldos_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                                        Dt_Saldos_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                                        Dt_Saldos_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                                        Dt_Saldos_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                                    }
                                    foreach (DataRow Registro in Dt_Reserva_Detalle.Rows)
                                    {
                                        Fila = Dt_Saldos_Reserva.NewRow();
                                        if (Servicios_Generales == "NO")
                                        {
                                            Fila["DEPENDENCIA_ID"] = Dependencia_ID;
                                        }
                                        else
                                        {
                                            Fila["DEPENDENCIA_ID"] = Registro["DEPENDENCIA_ID"].ToString(); ;
                                        }
                                        Fila["FUENTE_FINANCIAMIENTO_ID"] = Registro["FTE_FINANCIAMIENTO_ID"].ToString();
                                        Fila["PROGRAMA_ID"] = Registro["PROYECTO_PROGRAMA_ID"].ToString();
                                        Fila["PARTIDA_ID"] = Registro["PARTIDA_ID"].ToString();
                                        Fila["IMPORTE"] = Registro["SALDO"].ToString();
                                        Fila["ANIO"] = Dt_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Anio].ToString();
                                        Saldo_Reserva = Saldo_Reserva + Convert.ToDouble(Registro["SALDO"].ToString());
                                        Dt_Saldos_Reserva.Rows.Add(Fila);
                                        Dt_Saldos_Reserva.AcceptChanges();
                                    }
                                }
                                if (Saldo_Reserva > 0)
                                {
                                    Afectacion_Presupuestal = 0;
                                    Afectacion_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DISPONIBLE", "PRE_COMPROMETIDO", Dt_Saldos_Reserva, Cmmd);
                                    if (Afectacion_Presupuestal > 0)
                                    {
                                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Dt_Solicitud_Reserva.Rows[0][Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString(), "DISPONIBLE", "PRE_COMPROMETIDO", Saldo_Reserva, "", "", "", "", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                                        Afectaciones = "SI";
                                    }
                                    else
                                    {
                                        Afectaciones = "NO";
                                    }
                                }
                                //  los saldos de la reserva  se hacen cero
                                Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ";
                                Mi_SQL += Ope_Psp_Reservas.Campo_Saldo + "= 0";
                                Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "='" + Dt_Solicitud_Reserva.Rows[0][Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString() + "'";
                                Cmmd.CommandText = Mi_SQL;
                                Cmmd.ExecuteNonQuery();

                                //se actualiza el importe de la reserva 
                                Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ";
                                Mi_SQL += Ope_Psp_Reservas.Campo_Importe_Inicial + "=" + Ope_Psp_Reservas.Campo_Importe_Inicial + "-" + Saldo_Reserva;
                                Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "='" + Dt_Solicitud_Reserva.Rows[0][Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString() + "'";
                                Cmmd.CommandText = Mi_SQL;
                                Cmmd.ExecuteNonQuery();
                                ///Actualizar los saldos de las partidas
                                foreach (DataRow Registro in Dt_Reserva_Detalle.Rows)
                                {
                                    //se actualiza el importe de los detalles de la reserva 
                                    Mi_SQL = "UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " SET ";
                                    Mi_SQL += Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + "=" + Ope_Psp_Reservas.Campo_Importe_Inicial + "-" + Registro["SALDO"].ToString()+", ";
                                    Mi_SQL += Ope_Psp_Reservas_Detalles.Campo_Saldo + "=0";// +Ope_Psp_Reservas.Campo_Importe_Inicial + "-" + Registro["SALDO"].ToString();
                                    Mi_SQL += " WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + "='" + Dt_Solicitud_Reserva.Rows[0][Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString() + "'";
                                    Mi_SQL += " AND " + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + "='" + Registro["FTE_FINANCIAMIENTO_ID"].ToString()+"'";
                                    if (Servicios_Generales == "NO")
                                    {
                                        Mi_SQL += " AND " + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + "='" + Dependencia_ID + "'";
                                    }
                                    else
                                    {
                                        Mi_SQL += " AND " + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + "='" + Registro["DEPENDENCIA_ID"].ToString() + "'";
                                    }
                                    Mi_SQL += " AND " + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + "='" + Registro["PARTIDA_ID"].ToString() + "'";
                                    Mi_SQL += " AND " + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + "='" + Registro["PROYECTO_PROGRAMA_ID"].ToString() + "'";
                                    Cmmd.CommandText = Mi_SQL;
                                    Cmmd.ExecuteNonQuery();
                                
                                }
                                //SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                            }
                        }
                        Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                        Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Pago + "='" + Datos.P_No_Pago + "'";
                        Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Fila_Solicitud["No_Solicitud"].ToString()+ "'";
                        Cmmd.CommandText = Mi_SQL;
                        Cmmd.ExecuteNonQuery();
                        //SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if(Afectaciones=="NO"){
                        break;
                    }
                }
                if (Datos.P_Cmmd == null)
                {
                    if (Afectaciones == "SI")
                    {
                        Trans.Commit();
                    }
                    else
                    {
                        Trans.Rollback();
                    }
                }
                return Datos.P_No_Pago + "-" + Afectaciones;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + ex.Message, ex);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cancelacion_Masiva_Cheque
        /// DESCRIPCION : Inserta en nuevo Cheque en la BD
        /// PARAMETROS  : Datos: Contiene los datos de los filtros
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 21/Junio/2013
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static String Cancelacion_Masiva_Cheque(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;
            Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
            Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
            Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
            String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_Reserva_Detalle = new DataTable();
            DataTable Dt_Reserva = new DataTable();
            DataTable Dt_Solicitud_Reserva= new DataTable();
            DataTable Dt_Saldos_Reserva = new DataTable();
            Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Rs_Solcitud_Pago_Detalle = new Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio();
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Solicitudes= new Cls_Ope_Con_Solicitud_Pagos_Negocio();
            String Afectaciones = "";
            int Afectacion_Presupuestal;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Se actualizan los registros de las solicitudes que se van a pagar
                foreach (DataRow Renglon_Solicitud in Datos.P_Dt_Solicitudes_Masivas.Rows)
                {
                    Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Estatus + "='" + Datos.P_Estatus + "',";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Cuenta_Banco_Pago_ID + "='" + Datos.P_Banco_ID + "',";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "',";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + "= GETDATE() " + " WHERE ";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Renglon_Solicitud["No_Solicitud"].ToString() + "'";
                    Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos
                }
                Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')" +
                " FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                " WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'" +
                " AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00003'" +
                " GROUP BY " + Ope_Con_Polizas.Campo_No_Poliza +
                " ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC";
                Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                No_Poliza = Cmmd.ExecuteScalar();
                if (Convert.IsDBNull(No_Poliza))
                {
                    No_Poliza = "0000000001";
                }
                else
                {
                    No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                }
                //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                Mi_SQL = "INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                " (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", " +
                Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", " +
                Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", " +
                Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", " +
                Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", " +
                Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + "," + Ope_Con_Polizas.Campo_Referencia + ")" +
                " VALUES ('" + No_Poliza + "', '00003', '" + Mes_Anio + "'," +
                " CONVERT(varchar, GETDATE(),103)," +
                " '" + "CHEQUE " + Datos.P_No_Pago + "', " + Datos.P_Monto_Transferencia + ", " + Datos.P_Monto_Transferencia + "," + Datos.P_No_Partidas + ", " +
                "'" + Datos.P_Usuario_Creo + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', " +
                "'" + Cls_Sessiones.Empleado_ID + "','" + Datos.P_Referencia + "')";
                Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos                  
                //Da de alta los detalles de la póliza
                foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                {
                    //consulta el saldo de la cuenta contable
                    Mi_SQL = "SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - " +
                    " ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo" +
                    " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                    " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'";
                    //Saldo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Saldo = Cmmd.ExecuteScalar();
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                    }
                    //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')" +
                    " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Consecutivo = Cmmd.ExecuteScalar();
                    //Consecutivo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(Consecutivo))
                    {
                        Consecutivo = "1";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                    }
                    //Inserta el registro del detalle de la póliza en la base de datos
                    Mi_SQL = "INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                    "(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Consecutivo + "," + Ope_Con_Polizas_Detalles.Campo_Referencia + ")" +
                    " VALUES('" + No_Poliza + "', '00003', '" + Mes_Anio + "', " +
                    Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + "," +
                    " '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'," +
                    " '" + "CHEQUE " + Datos.P_No_Pago + "', " +
                    Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", " +
                    Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", " +
                    Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ",'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Referencia].ToString() + "')";
                    Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza, Cmmd);
                foreach (DataRow Fila_Solicitud in Datos.P_Dt_Solicitudes_Masivas.Rows)
                {
                    Afectacion_Presupuestal = 0;
                    Dt_Partidas = new DataTable();
                    //Rs_Solcitud_Pago_Detalle.P_No_Solicitud_Pago = Fila_Solicitud["No_Solicitud"].ToString();
                    //Rs_Solcitud_Pago_Detalle.P_Cmmd = Cmmd;
                    //Dt_Partidas = Rs_Solcitud_Pago_Detalle.Consulta_Detalles_Solicitud();
                    if (!String.IsNullOrEmpty(Datos.P_Servicios_Generales))
                    {
                        if(Datos.P_Servicios_Generales=="SI")
                        {
                            Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud_Servicios_Generales(Fila_Solicitud["No_Solicitud"].ToString(), Cmmd);
                        }else{
                            Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Fila_Solicitud["No_Solicitud"].ToString(), Cmmd);
                        }
                    }
                    else
                    {
                        Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Fila_Solicitud["No_Solicitud"].ToString(), Cmmd);
                    }
                    Afectacion_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("EJERCIDO", "PAGADO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                    if (Afectacion_Presupuestal > 0)
                    {
                        Rs_Solicitudes.P_No_Solicitud_Pago=Fila_Solicitud["No_Solicitud"].ToString();
                        Rs_Solicitudes.P_Cmmd=Cmmd;
                        Dt_Solicitud_Reserva=Rs_Solicitudes.Consulta_Datos_Solicitud_Pago();
                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Dt_Solicitud_Reserva.Rows[0][Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString(), "EJERCIDO", "PAGADO", Convert.ToDouble(Dt_Solicitud_Reserva.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Monto].ToString()), Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                        Afectaciones = "SI";
                    }
                    else
                    {
                        Afectaciones = "NO";
                    }
                    if (Afectaciones == "SI")
                    {
                        Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                        Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Pago + "=NULL";
                        Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Fila_Solicitud["No_Solicitud"].ToString()+ "'";
                        Cmmd.CommandText = Mi_SQL;
                        Cmmd.ExecuteNonQuery();
                    }
                    if(Afectaciones=="NO"){
                        break;
                    }
                }
                if (Datos.P_Cmmd == null)
                {
                    if (Afectaciones == "SI")
                    {
                        Trans.Commit();
                    }
                    else
                    {
                        Trans.Rollback();
                    }
                }
                return  Afectaciones;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + ex.Message, ex);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cancelacion_Masiva_Cheque_Finiquito
        /// DESCRIPCION : Inserta en nuevo Cheque en la BD
        /// PARAMETROS  : Datos: Contiene los datos de los filtros
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 21/Junio/2013
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static String Cancelacion_Masiva_Cheque_Finiquito(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;
            Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
            Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
            Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
            String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_Reserva_Detalle = new DataTable();
            DataTable Dt_Reserva = new DataTable();
            DataTable Dt_Solicitud_Reserva= new DataTable();
            DataTable Dt_Saldos_Reserva = new DataTable();
            Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Rs_Solcitud_Pago_Detalle = new Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio();
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Solicitudes= new Cls_Ope_Con_Solicitud_Pagos_Negocio();
            String Afectaciones = "";
            int Afectacion_Presupuestal;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Se actualizan los registros de las solicitudes que se van a pagar
                foreach (DataRow Renglon_Solicitud in Datos.P_Dt_Solicitudes_Masivas.Rows)
                {
                    Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Estatus + "='" + Datos.P_Estatus + "',";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Cuenta_Banco_Pago_ID + "='" + Datos.P_Banco_ID + "',";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "',";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + "= GETDATE() " + " WHERE ";
                    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Renglon_Solicitud["No_Solicitud"].ToString() + "'";
                    Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos
                }
                Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')" +
                " FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                " WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'" +
                " AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00003'" +
                " GROUP BY " + Ope_Con_Polizas.Campo_No_Poliza +
                " ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC";
                Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                No_Poliza = Cmmd.ExecuteScalar();
                if (Convert.IsDBNull(No_Poliza))
                {
                    No_Poliza = "0000000001";
                }
                else
                {
                    No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                }
                //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                Mi_SQL = "INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                " (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", " +
                Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", " +
                Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", " +
                Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", " +
                Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", " +
                Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + "," + Ope_Con_Polizas.Campo_Referencia + ")" +
                " VALUES ('" + No_Poliza + "', '00003', '" + Mes_Anio + "'," +
                " CONVERT(varchar, GETDATE(),103)," +
                " '" + "CHEQUE " + Datos.P_No_Pago + "', " + Datos.P_Monto_Transferencia + ", " + Datos.P_Monto_Transferencia + "," + Datos.P_No_Partidas + ", " +
                "'" + Datos.P_Usuario_Creo + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', " +
                "'" + Cls_Sessiones.Empleado_ID + "','" + Datos.P_Referencia + "')";
                Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos                  
                //Da de alta los detalles de la póliza
                foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                {
                    //consulta el saldo de la cuenta contable
                    Mi_SQL = "SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - " +
                    " ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo" +
                    " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                    " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'";
                    //Saldo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Saldo = Cmmd.ExecuteScalar();
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                    }
                    //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')" +
                    " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Consecutivo = Cmmd.ExecuteScalar();
                    //Consecutivo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(Consecutivo))
                    {
                        Consecutivo = "1";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                    }
                    //Inserta el registro del detalle de la póliza en la base de datos
                    Mi_SQL = "INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                    "(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Consecutivo + "," + Ope_Con_Polizas_Detalles.Campo_Referencia + ")" +
                    " VALUES('" + No_Poliza + "', '00003', '" + Mes_Anio + "', " +
                    Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + "," +
                    " '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'," +
                    " '" + "CHEQUE " + Datos.P_No_Pago + "', " +
                    Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", " +
                    Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", " +
                    Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ",'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Referencia].ToString() + "')";
                    Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza, Cmmd);
                foreach (DataRow Fila_Solicitud in Datos.P_Dt_Solicitudes_Masivas.Rows)
                {
                    Afectacion_Presupuestal = 0;
                    Dt_Partidas = new DataTable();
                    Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud_Finiquito(Fila_Solicitud["No_Solicitud"].ToString(), Cmmd);
                    Afectacion_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("EJERCIDO", "PAGADO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                    if (Afectacion_Presupuestal > 0)
                    {
                        Rs_Solicitudes.P_No_Solicitud_Pago=Fila_Solicitud["No_Solicitud"].ToString();
                        Rs_Solicitudes.P_Cmmd=Cmmd;
                        Dt_Solicitud_Reserva=Rs_Solicitudes.Consulta_Datos_Solicitud_Pago();
                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Dt_Solicitud_Reserva.Rows[0][Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString(), "EJERCIDO", "PAGADO", Convert.ToDouble(Datos.P_Monto_Transferencia), Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                        Afectaciones = "SI";
                    }
                    else
                    {
                        Afectaciones = "NO";
                    }
                    if (Afectaciones == "SI")
                    {
                        Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                        Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Pago + "=NULL";
                        Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Fila_Solicitud["No_Solicitud"].ToString()+ "'";
                        Cmmd.CommandText = Mi_SQL;
                        Cmmd.ExecuteNonQuery();
                    }
                    if(Afectaciones=="NO"){
                        break;
                    }
                }
                if (Datos.P_Cmmd == null)
                {
                    if (Afectaciones == "SI")
                    {
                        Trans.Commit();
                    }
                    else
                    {
                        Trans.Rollback();
                    }
                }
                return  Afectaciones;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + ex.Message, ex);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        public static String Alta_Pago(Cls_Ope_Con_Cheques_Negocio Datos)
        {
                Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Rs_Solcitud_Pago_Detalle = new Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio();
                String Mi_SQL;
                Object Compromisos_ID; //Variable que contendrá el ID de la consulta
                Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
                Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
                Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
                String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
                DataTable Dt_Partidas = new DataTable();
                DataTable Dt_Reserva_Detalle = new DataTable();
                DataTable Dt_Solicitudes = new DataTable();
                DataTable Dt_Reserva = new DataTable();
                DataTable Dt_Saldos_Reserva = new DataTable();
                String Dependencia_ID = "";
                Double Monto;
                Decimal Monto_Transferencia = 0;
                String Tipo_Reserva = "";
                DataTable Dt_Detalle_Solicitud ;
                DataRow Fila;
                Double Saldo_Reserva;
                DataTable Dt_Deudas = new DataTable();
                int Registro_Presupuestal;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                SqlDataAdapter Dt_Sql = new SqlDataAdapter();
                DataSet Ds_Sql = new DataSet();
                int Afectacion_Presupuestal = 0;
                String Resultados = "SI";
                SqlDataAdapter Dt_Sql_Consulta = new SqlDataAdapter();
                DataSet Ds_Sql_Consulta = new DataSet();
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                } 
            try
                {
                //Busca el maximo ID de la tabla Compromisos
                foreach (DataRow Filas in Datos.P_Dt_Datos_Completos.Rows)
                {
                    //Cuenta = Convert.ToString(Fila["CUENTA_BANCO_PAGO_ID"].ToString()).Trim();
                    if (Dt_Solicitudes.Rows.Count <= 0 && Dt_Solicitudes.Columns.Count <= 0)
                    {
                        Dt_Solicitudes.Columns.Add("No_Solicitud", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Referencia", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Banco", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Proveedor", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Cuenta", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Monto", typeof(System.Double));
                        Dt_Solicitudes.Columns.Add("Reserva", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Tipo_Solicitud_Pago", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Banco_Proveedor", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Cuenta_Contable_ID_Banco", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Cuenta_Contable_Proveedor", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Monto_Cedular", typeof(System.Double));
                        Dt_Solicitudes.Columns.Add("Monto_ISR", typeof(System.Double));
                        Dt_Solicitudes.Columns.Add("Monto_Transferencia", typeof(System.Double));
                        Dt_Solicitudes.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Anticipo", typeof(System.Double));
                    }
                    DataRow ROW = Dt_Solicitudes.NewRow(); //Crea un nuevo registro a la tabla
                    if (Filas["PROVEEDOR"].ToString().Trim() == Datos.P_Dt_Datos_Agrupados.Rows[0]["PROVEEDOR"].ToString().Trim() && Filas["Cuenta"].ToString().Trim() == Datos.P_Dt_Datos_Agrupados.Rows[0]["Cuenta"].ToString().Trim() && Filas["Cuenta_Contable_ID_Banco"].ToString().Trim() == Datos.P_Dt_Datos_Agrupados.Rows[0]["Cuenta_Contable_ID_Banco"].ToString().Trim() && Filas["Cuenta_Contable_Proveedor"].ToString().Trim() == Datos.P_Dt_Datos_Agrupados.Rows[0]["Cuenta_Contable_Proveedor"].ToString().Trim())
                    {
                        ROW["No_Solicitud"] = Filas["No_Solicitud"].ToString().Trim();
                        ROW["Referencia"] = Filas["Referencia"].ToString().Trim();
                        ROW["Banco"] = Filas["Banco"].ToString().Trim();
                        ROW["cuenta"] = Filas["cuenta"].ToString().Trim();
                        ROW["Proveedor"] = Filas["Proveedor"].ToString().Trim();
                        ROW["Monto"] = Filas["Monto"].ToString().Trim();
                        ROW["Reserva"] = Filas["Reserva"].ToString().Trim();
                        ROW["Tipo_Solicitud_Pago"] = Filas["Tipo_Solicitud_Pago"].ToString().Trim();
                        ROW["Banco_Proveedor"] = Filas["Banco_Proveedor"].ToString().Trim();
                        ROW["Cuenta_Contable_ID_Banco"] = Filas["Cuenta_Contable_ID_Banco"].ToString().Trim();
                        ROW["Cuenta_Contable_Proveedor"] = Filas["Cuenta_Contable_Proveedor"].ToString().Trim();
                        ROW["Monto_Cedular"] = Filas["Monto_Cedular"].ToString().Trim();
                        ROW["Monto_ISR"] = Filas["Monto_ISR"].ToString().Trim();
                        ROW["Monto_Transferencia"] = Filas["Monto_Transferencia"].ToString().Trim();
                        ROW["Tipo_Beneficiario"] = Filas["Tipo_Beneficiario"].ToString().Trim();
                        Dt_Solicitudes.Rows.Add(ROW); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Solicitudes.AcceptChanges();
                    }
                }
                if (Dt_Solicitudes.Rows.Count > 0)
                {
                    Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Con_Pagos.Campo_No_Pago + "),'00000') ";
                    Mi_SQL = Mi_SQL + "FROM " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos;
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Compromisos_ID = Cmmd.ExecuteScalar();
                    if (Convert.IsDBNull(Compromisos_ID)) //Si no existen valores en la tabla, asigna el primer valor manualmente.
                    {
                        Datos.P_No_Pago = "00001";
                    }
                    else // Si ya existen registros, toma el valor maximo y le suma 1 para el nuevo registro.
                    {
                        Datos.P_No_Pago = String.Format("{0:00000}", Convert.ToInt32(Compromisos_ID) + 1);
                    }
                    Mi_SQL = "INSERT INTO " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "(";
                    Mi_SQL += Ope_Con_Pagos.Campo_No_Pago + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_No_Solicitud_Pago + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Banco_ID + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Fecha_Pago + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Beneficiario_Pago + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Forma_Pago + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Referencia_Transferencia_Banca + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Estatus + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Comentarios + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Usuario_Creo + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Fecha_Creo + ") VALUES('";
                    Mi_SQL += Datos.P_No_Pago + "', ' ', '";
                    Mi_SQL += Datos.P_Dt_Datos_Agrupados.Rows[0]["CUENTA"].ToString().Trim() + "','";
                    Mi_SQL += Datos.P_Fecha_Pago + "','";
                    Mi_SQL += Datos.P_Beneficiario_Pago + "', '";
                    Mi_SQL += Datos.P_Tipo_Pago + "', '";
                    Mi_SQL += Datos.P_Dt_Datos_Agrupados.Rows[0]["REFERENCIA"].ToString().Trim() + "', '";
                    Mi_SQL += Datos.P_Estatus + "', '";
                    Mi_SQL += Datos.P_Comentario + "', '";
                    Mi_SQL += Datos.P_Usuario_Creo + "',  GETDATE())";
                    Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    Monto = Convert.ToDouble(Datos.P_Dt_Datos_Agrupados.Rows[0]["Monto"].ToString()) - (Convert.ToDouble(Datos.P_Dt_Datos_Agrupados.Rows[0]["Monto_Cedular"].ToString().Trim()) + Convert.ToDouble(Datos.P_Dt_Datos_Agrupados.Rows[0]["Monto_ISR"].ToString().Trim())) + ((Convert.ToDouble(Datos.P_Monto_Iva) + Convert.ToDouble(Datos.P_Monto_Comision)));
                    //Se optiene el total de la poliza 
                    foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                    {
                        Monto_Transferencia = Monto_Transferencia + Convert.ToDecimal(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }                    
                    //Consulta para la obtención del último ID dado de alta 
                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')" +
                    " FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                    " WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'" +
                        " AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00002'";
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    No_Poliza = Cmmd.ExecuteScalar();
                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(No_Poliza))
                    {
                        No_Poliza = "0000000001";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                    }
                    Mes_Anio = String.Format("{0:MMyy}", Convert.ToDateTime(Datos.P_Dt_Datos_Agrupados.Rows[0]["Fecha_Poliza"].ToString().Trim()));
                    //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                    Mi_SQL = "INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                    " (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", " +
                    Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", " +
                    Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", " +
                    Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", " +
                    Ope_Con_Polizas.Campo_Usuario_Creo + ", " + Ope_Con_Polizas.Campo_Fecha_Creo + ", " +
                    Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + "," + Ope_Con_Polizas.Campo_Referencia + ")" +
                    " VALUES ('" + No_Poliza + "', '00002', '" + Mes_Anio + "'," +
                    " '" + String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Datos.P_Dt_Datos_Agrupados.Rows[0]["Fecha_Poliza"].ToString().Trim())) + "'," +
                    " '" + "TRANSFERENICA " + Datos.P_No_Pago + "', " + Monto_Transferencia + ", " + Monto_Transferencia + "," + Datos.P_No_Partidas + ", " +
                    "'" + Datos.P_Usuario_Creo + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', " +
                    "'" + Cls_Sessiones.Empleado_ID + "','" + Datos.P_Dt_Datos_Agrupados.Rows[0]["REFERENCIA"].ToString().Trim() + "')";
                    Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    //Da de alta los detalles de la póliza
                    foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                    {
                        //consulta el saldo de la cuenta contable
                        Mi_SQL = "SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - " +
                        " ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo" +
                        " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                        " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'";
                        Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                        Saldo = Cmmd.ExecuteScalar();
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                        }
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                        }
                        //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                        Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')" +
                        " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                        Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                        Consecutivo = Cmmd.ExecuteScalar();
                        //Valida si el ID es nulo para asignarle automaticamente el primer registro
                        if (Convert.IsDBNull(Consecutivo))
                        {
                            Consecutivo = "1";
                        }
                        //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                        else
                        {
                            Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                        }
                        //Inserta el registro del detalle de la póliza en la base de datos
                        Mi_SQL = "INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                        "(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", " +
                        Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", " +
                        Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", " +
                        Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", " +
                        Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " +
                        Ope_Con_Polizas_Detalles.Campo_Consecutivo + "," + Ope_Con_Polizas_Detalles.Campo_Referencia + ")" +
                        " VALUES('" + No_Poliza + "', '00002', '" + Mes_Anio + "', " +
                        Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + "," +
                        " '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'," +
                        " '" + "TRANSFERENCIA " + Datos.P_No_Pago + "', " +
                        Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", " +
                        Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", " +
                        Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ",'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Referencia].ToString() + "')";
                        Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                        Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    }
                    foreach (DataRow Data in Dt_Solicitudes.Rows)
                    {
                        //if ((Data["Tipo_Solicitud_Pago"].ToString() == "00001" || (Data["Tipo_Solicitud_Pago"].ToString() == "00003")) && (Datos.P_Estatus_Comparacion == "PAGADO"))
                        //{
                        //    Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                        //    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Estatus + "='PORCOMPROBAR',";
                        //    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "',";
                        //    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + "= GETDATE() " + " WHERE ";
                        //    Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Data["No_Solicitud"].ToString() + "'";
                        //    SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        //}
                        //else
                        //{
                            Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                            Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Estatus + "='" + Datos.P_Estatus + "',";
                            Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "',";
                            Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + "= GETDATE() " + " WHERE ";
                            Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Data["No_Solicitud"].ToString() + "'";
                            Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                            Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                            //}
                        // se incerta el numero de poliza, el tipo de poliza y el mes_anio del pago
                        Mi_SQL = "UPDATE " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " SET ";
                        Mi_SQL += Ope_Con_Pagos.Campo_Mes_Ano + "='" + Mes_Anio + "',";
                        Mi_SQL += Ope_Con_Pagos.Campo_Tipo_Poliza_ID + "='00002',";
                        Mi_SQL += Ope_Con_Pagos.Campo_No_poliza + "='" + No_Poliza + "' WHERE ";
                        Mi_SQL += Ope_Con_Pagos.Campo_No_Pago + " ='" + Datos.P_No_Pago + "'";
                        Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                        Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                        Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza, Cmmd);
                        //Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Data["No_Solicitud"].ToString(),Cmmd);
                       
                        Rs_Solcitud_Pago_Detalle.P_No_Solicitud_Pago = Data["No_Solicitud"].ToString();
                        Rs_Solcitud_Pago_Detalle.P_Cmmd = Cmmd;
                        Dt_Detalle_Solicitud = Rs_Solcitud_Pago_Detalle.Consulta_Detalles_Solicitud();
                        if (Dt_Detalle_Solicitud.Rows.Count > 0)
                        {
                            Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Data["No_Solicitud"].ToString(), Cmmd);
                            //if (Data["Tipo_Solicitud_Pago"].ToString() != "00001" && Data["Tipo_Solicitud_Pago"].ToString() != "00003")
                            //{
                                Afectacion_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PAGADO", "EJERCIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                                if (Afectacion_Presupuestal > 0)
                                {
                                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Data["Reserva"].ToString()), "PAGADO", "EJERCIDO", (Convert.ToDouble(Monto)) - ((Convert.ToDouble(Datos.P_Monto_Iva) + Convert.ToDouble(Datos.P_Monto_Comision))), Convert.ToString(No_Poliza), "00002", Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                                }
                                else
                                {
                                    Resultados = "NO";
                                }
                            //}
                        }
                        if (Resultados == "SI")
                        {
                            Mi_SQL = "SELECT * from " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                            Mi_SQL += " where " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + Data["Reserva"].ToString();
                            Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                            Dt_Sql.SelectCommand = Cmmd;
                            Dt_Sql.Fill(Ds_Sql);
                            Dt_Reserva = Ds_Sql.Tables[0]; Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                            Dt_Sql.SelectCommand = Cmmd;
                            Dt_Sql.Fill(Ds_Sql);
                            Dt_Reserva = Ds_Sql.Tables[0];
                            Ds_Sql = new DataSet();
                            Mi_SQL = "SELECT * from " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles;
                            Mi_SQL += " where " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + "=" + Data["Reserva"].ToString();
                            Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                            Dt_Sql.SelectCommand = Cmmd;
                            Dt_Sql.Fill(Ds_Sql);
                            Dt_Reserva_Detalle = Ds_Sql.Tables[0];

                            //Dt_Reserva_Detalle = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                            if (Dt_Reserva.Rows.Count > 0)
                            {
                                Dependencia_ID = Dt_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Dependencia_ID].ToString();
                                Tipo_Reserva = Dt_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Tipo_Reserva].ToString();

                                if (Tipo_Reserva == "UNICA" && (Datos.P_Estatus_Comparacion == "PAGADO"))
                                {
                                    Saldo_Reserva = 0;
                                    if (Dt_Saldos_Reserva != null)
                                    {
                                        if (Dt_Saldos_Reserva.Rows.Count <= 0 && Dt_Saldos_Reserva.Columns.Count <= 0)
                                        {
                                            Dt_Saldos_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                                            Dt_Saldos_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                                            Dt_Saldos_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                                            Dt_Saldos_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                                            Dt_Saldos_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                                            Dt_Saldos_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                                        }
                                        foreach (DataRow Registro in Dt_Reserva_Detalle.Rows)
                                        {
                                            Fila = Dt_Saldos_Reserva.NewRow();
                                            Fila["DEPENDENCIA_ID"] = Dependencia_ID;
                                            Fila["FUENTE_FINANCIAMIENTO_ID"] = Registro["FTE_FINANCIAMIENTO_ID"].ToString();
                                            Fila["PROGRAMA_ID"] = Registro["PROYECTO_PROGRAMA_ID"].ToString();
                                            Fila["PARTIDA_ID"] = Registro["PARTIDA_ID"].ToString();
                                            Fila["IMPORTE"] = Registro["SALDO"].ToString();
                                            Fila["ANIO"] = Dt_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Anio].ToString();
                                            Saldo_Reserva = Saldo_Reserva + Convert.ToDouble(Registro["SALDO"].ToString());
                                            Dt_Saldos_Reserva.Rows.Add(Fila);
                                            Dt_Saldos_Reserva.AcceptChanges();
                                        }
                                    }
                                    if (Saldo_Reserva > 0)
                                    {
                                        Afectacion_Presupuestal = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DISPONIBLE", "PRE_COMPROMETIDO", Dt_Saldos_Reserva, Cmmd);
                                        if (Afectacion_Presupuestal > 0)
                                        {
                                            Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Data["Reserva"].ToString()), "DISPONIBLE", "PRE_COMPROMETIDO", Saldo_Reserva, "", "", "", "", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                                        }
                                        else
                                        {
                                            Resultados = "NO";
                                        }
                                    }
                                    //  los saldos de la reserva  se hacen cero
                                    Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ";
                                    Mi_SQL += Ope_Psp_Reservas.Campo_Saldo + "= 0";
                                    Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "='" + Data["Reserva"].ToString() + "'";
                                    Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                                    //SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                                }
                            }
                            if (Resultados == "SI")
                            {
                                Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Pago + "='" + Datos.P_No_Pago + "'";
                                Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Data["No_Solicitud"].ToString() + "'";
                                Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                                //SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                            }
                        }
                    }
                }
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Datos.P_No_Pago + "-" + Resultados;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error : " + ex.Message, ex);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Dependencia_Reserva
        /// DESCRIPCION : Consulta la dependencia de la reserva
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO  : 12/Marzo/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Dependencia_Reserva(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            DataTable Dt_Psp = new DataTable();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT * from " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                Mi_SQL += " where " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + Datos.P_No_Reserva;
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                Dt_Psp = Ds_Oracle.Tables[0];
                // return OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Dt_Psp;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Dependencia_Reserva
        /// DESCRIPCION : Consulta la dependencia de la reserva
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO  : 12/Marzo/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Dependencia_Reserva(String No_Reserva,SqlCommand P_Cmmd)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            DataTable Dt_Psp = new DataTable();
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT * from " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                Mi_SQL += " where " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + No_Reserva;
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                Dt_Psp = Ds_Oracle.Tables[0];
                // return OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                if (P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Dt_Psp;
            }
            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Reserva_Detalles
        /// DESCRIPCION : Consulta la dependencia de la reserva
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO  : 12/Marzo/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Reserva_Detalles(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            DataTable Dt_Psp = new DataTable();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT * from " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles;
                Mi_SQL += " where " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + "=" + Datos.P_No_Reserva;
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                Dt_Psp = Ds_Oracle.Tables[0];
                //return OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Dt_Psp;
                //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Reserva_Detalles
        /// DESCRIPCION : Consulta la dependencia de la reserva
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO  : 12/Marzo/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Reserva_Detalles(String No_Reserva,SqlCommand P_Cmmd)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            DataTable Dt_Psp = new DataTable();
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT * from " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles;
                Mi_SQL += " where " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + "=" + No_Reserva;
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                Dt_Psp = Ds_Oracle.Tables[0];
                //return OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                if (P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Dt_Psp;
                //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Reserva_Detalles
        /// DESCRIPCION : Consulta la dependencia de la reserva
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO  : 12/Marzo/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Saldo_Presupuesto(String Dependencia_ID, String Fte_Financiamiento_ID, String Programa_ID, String Partida_ID)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT * from " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado;
                Mi_SQL += " where " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + "= '" + Dependencia_ID + "'and ";
                Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + "='" + Fte_Financiamiento_ID + "' and ";
                Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + "='" + Programa_ID + "' and ";
                Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + "='" + Partida_ID + "'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Pago
        /// DESCRIPCION : Modifica los datos del  Pago con lo que fueron 
        ///               introducidos por el usuario
        /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
        ///                       proporcionados por el usuario y van a sustituir a los datos que se
        ///                       encuentran en la base de datos
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 17-Noviembre-2011
        /// MODIFICO          :sergio Manuel Gallardo Andrade
        /// FECHA_MODIFICO    :25/nov/2011
        /// CAUSA_MODIFICACION:se adacto el metodo para la cancelacion del pago 
        ///*******************************************************************************
        public static String Modificar_Pago(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            Double Total_Poliza = 0;                         //Obtiene el monto total del debe y haber de la póliza
            String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos
            Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
            Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
            Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
            SqlCommand Comando_SQL = new SqlCommand(); //Sirve para la ejecución de las operaciones a la base de datos
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlTransaction Transaccion_SQL;                                                  //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            int AFECTACION_PRESUPUESTAL = 0; 
            DataTable Dt_Partidas= new DataTable();
            String respuesta="";
            Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Rs_Solcitud_Pago_Detalle = new Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio();
            DataTable Dt_Detalle_Solicitud = new DataTable();
            DataTable Dt_Reserva = new DataTable();
            DataTable Dt_Reserva_Detalles = new DataTable();
            DataTable Dt_Saldos_Reserva = new DataTable();
            Double Saldo_Reserva;
            String Dependencia_ID = "";
            DataRow Fila;
            String MiSQL="";
            String Tipo_Reserva = "";
            try
            {
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;

                Mi_SQL.Append("UPDATE " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos);
                Mi_SQL.Append(" SET " + Ope_Con_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Motivo_Cancelacion)) Mi_SQL.Append(Ope_Con_Pagos.Campo_Motivo_Cancelacion + " = '" + Datos.P_Motivo_Cancelacion + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Comentario)) Mi_SQL.Append(Ope_Con_Pagos.Campo_Comentarios + " = '" + Datos.P_Comentario + "', ");
                Mi_SQL.Append(Ope_Con_Pagos.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Modifico + "', ");
                Mi_SQL.Append(Ope_Con_Pagos.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + Ope_Con_Pagos.Campo_No_Pago + " = '" + Datos.P_No_Pago + "'");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                Mi_SQL.Length = 0;
                Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = 'EJERCIDO', ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Pago + " = '', ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Modifico + "', ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  


                Mi_SQL.Length = 0;
                //Consulta para la obtención del último ID dado de alta 
                Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')");
                Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00003'");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                No_Poliza = Comando_SQL.ExecuteScalar();
                //Valida si el ID es nulo para asignarle automaticamente el primer registro
                if (Convert.IsDBNull(No_Poliza))
                {
                    No_Poliza = "0000000001";
                }
                //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                else
                {
                    No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                }
                Mi_SQL.Length = 0;
                Total_Poliza += Convert.ToDouble(Datos.P_Monto);
                //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                Mi_SQL.Append(" (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", ");
                Mi_SQL.Append(Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + ")");
                Mi_SQL.Append(" VALUES ('" + No_Poliza + "', '00003', '" + Mes_Anio + "',");
                Mi_SQL.Append(" CONVERT(varchar,GETDATE(),103),");
                Mi_SQL.Append(" '" + Datos.P_Comentario + "', " + Total_Poliza + ", " + Total_Poliza + ", 4, ");
                Mi_SQL.Append("'" + Datos.P_Usuario_Modifico + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', ");
                Mi_SQL.Append("'" + Cls_Sessiones.Empleado_ID + "')");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                //Da de alta los detalles de la póliza
                foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                {
                    Mi_SQL.Length = 0;
                    //consulta el saldo de la cuenta contable
                    Mi_SQL.Append("SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - ");
                    Mi_SQL.Append(" ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'");
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                    Saldo = Comando_SQL.ExecuteScalar();
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                    }

                    Mi_SQL.Length = 0;
                    //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                    Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                    Consecutivo = Comando_SQL.ExecuteScalar();

                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(Consecutivo))
                    {
                        Consecutivo = "1";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                    }
                    Mi_SQL.Length = 0;
                    //Inserta el registro del detalle de la póliza en la base de datos
                    Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Mi_SQL.Append("(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Consecutivo + ")");
                    Mi_SQL.Append(" VALUES('" + No_Poliza + "', '00003', '" + Mes_Anio + "', ");
                    Mi_SQL.Append(Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + ",");
                    Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "',");
                    Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString() + "', ");
                    Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ")");
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos            
                }
                //Transaccion_SQL.Commit(); //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
                Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza,Comando_SQL);
                Dt_Detalle_Solicitud = new DataTable();
                Rs_Solcitud_Pago_Detalle.P_No_Solicitud_Pago = Datos.P_No_Solicitud_Pago;
                Rs_Solcitud_Pago_Detalle.P_Cmmd = Comando_SQL;
                Dt_Detalle_Solicitud = Rs_Solcitud_Pago_Detalle.Consulta_Detalles_Solicitud();
                if (Dt_Detalle_Solicitud.Rows.Count > 0)
                {
                        Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(Datos.P_No_Solicitud_Pago, Comando_SQL);
                        if (Convert.ToDouble(Datos.P_Monto) > 0)
                        {
                            AFECTACION_PRESUPUESTAL=Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("EJERCIDO", "PAGADO", Dt_Partidas, Comando_SQL); //Actualiza el impote de la partida presupuestal
                            if (AFECTACION_PRESUPUESTAL > 0)
                            {
                                Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "EJERCIDO", "PAGADO", (Total_Poliza-Convert.ToDouble(Datos.P_IVA)) , Convert.ToString(No_Poliza), "00003", Mes_Anio, "1", Comando_SQL); //Agrega el historial del movimiento de la partida presupuestal
                                respuesta = "SI";
                            }
                            else
                            {
                                respuesta = "NO";
                            }
                        }
                }
                if (respuesta == "SI")
                {
                    //  para las consultas de la reserva, reserva detalles 
                    Dt_Reserva = Consulta_Dependencia_Reserva(Datos);
                    Dt_Reserva_Detalles = Consulta_Reserva_Detalles(Datos);
                    if (Dt_Reserva.Rows.Count > 0)
                    {
                        Dependencia_ID = Dt_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Dependencia_ID].ToString();
                        Tipo_Reserva = Dt_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Tipo_Reserva].ToString();

                        if (Tipo_Reserva == "UNICA")
                        {
                            Saldo_Reserva = 0;
                            if (Dt_Saldos_Reserva != null)
                            {
                                if (Dt_Saldos_Reserva.Rows.Count <= 0 && Dt_Saldos_Reserva.Columns.Count <= 0)
                                {
                                    Dt_Saldos_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                                    Dt_Saldos_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                                    Dt_Saldos_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                                    Dt_Saldos_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                                    Dt_Saldos_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                                    Dt_Saldos_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                                }
                                foreach (DataRow Registro in Dt_Reserva_Detalles.Rows)
                                {
                                    Fila = Dt_Saldos_Reserva.NewRow();
                                    Fila["DEPENDENCIA_ID"] = Dependencia_ID;
                                    Fila["FUENTE_FINANCIAMIENTO_ID"] = Registro["FTE_FINANCIAMIENTO_ID"].ToString();
                                    Fila["PROGRAMA_ID"] = Registro["PROYECTO_PROGRAMA_ID"].ToString();
                                    Fila["PARTIDA_ID"] = Registro["PARTIDA_ID"].ToString();
                                    Fila["IMPORTE"] = Registro["SALDO"].ToString();
                                    Fila["ANIO"] = Dt_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Anio].ToString();
                                    Saldo_Reserva = Saldo_Reserva + Convert.ToDouble(Registro["SALDO"].ToString());
                                    Dt_Saldos_Reserva.Rows.Add(Fila);
                                    Dt_Saldos_Reserva.AcceptChanges();
                                }
                            }
                            if (Saldo_Reserva > 0)
                            {
                                AFECTACION_PRESUPUESTAL = 0;
                                AFECTACION_PRESUPUESTAL = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PRE_COMPROMETIDO", "DISPONIBLE", Dt_Saldos_Reserva, Comando_SQL);
                                if (AFECTACION_PRESUPUESTAL > 0)
                                {
                                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "PRE_COMPROMETIDO", "DISPONIBLE", Saldo_Reserva, "", "", "", "", Comando_SQL); //Agrega el historial del movimiento de la partida presupuestal
                                    respuesta = "SI";
                                }
                                else
                                {
                                    respuesta = "NO";
                                }
                            }
                            //  los saldos de la reserva  se hacen cero
                            MiSQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ";
                            MiSQL += Ope_Psp_Reservas.Campo_Saldo + "=" + Saldo_Reserva;
                            MiSQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "='" + Datos.P_No_Reserva + "'";
                            Comando_SQL.CommandText = MiSQL;
                            Comando_SQL.ExecuteNonQuery();
                            //OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                            // Cancela el registro del cheque 
                            MiSQL = "UPDATE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + " SET ";
                            MiSQL += Ope_Con_Cheques.Campo_Estatus + "='CANCELADO',";
                            MiSQL += Ope_Con_Cheques.Campo_Usuario_Modifico + "='"+Cls_Sessiones.Nombre_Empleado.ToString()+"',";
                            MiSQL += Ope_Con_Cheques.Campo_Fecha_Modifico + "=GETDATE()";
                            MiSQL += " WHERE " + Ope_Con_Cheques.Campo_No_Pago + "='" + Datos.P_No_Pago + "'";
                            MiSQL += " AND " + Ope_Con_Cheques.Campo_Estatus + "<>'CANCELADO'";
                            Comando_SQL.CommandText = MiSQL;
                            Comando_SQL.ExecuteNonQuery();
                        }
                    }
                }
                    if (respuesta == "SI")
                    {
                        Transaccion_SQL.Commit();
                    }
                    else
                    {
                        Transaccion_SQL.Rollback();
                    }
                return respuesta;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION: Cambiar_Estatus_Solicitud_Pago
        /// DESCRIPCION : Autoriza o rechaza la solicitud de pago 
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Cambiar_Estatus_Solicitud_Pago(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;
            try
            {
                //Da de Alta los datos del Cierre Mensual con los datos proporcionados por el usuario
                Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Comentarios_Contabilidad + "='" + Datos.P_Comentario + "',";
                //Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Contabilidad  + "='" + Datos.P_Empleado_ID_Contabilidad + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Estatus + "='" + Datos.P_Estatus + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Modifico + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + "= GETDATE() " + " WHERE ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Datos.P_No_Solicitud_Pago + "'";
                SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION: Alta_Transferencia
        /// DESCRIPCION : Agrega la informacion de la tranferencia a la solicitud de pago 
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 17/abril/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Alta_Transferencia(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;
            try
            {
                //Da de Alta los datos del Cierre Mensual con los datos proporcionados por el usuario
                Mi_SQL = "UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Transferencia + "='" + Datos.P_Transferencia + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Cuenta_Transferencia_ID + "='" + Datos.P_Cuenta_Transferencia_ID + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Modifico + "',";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Layout_Transferencia + "=GETDATE(),";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + "= GETDATE() " + " WHERE ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Datos.P_No_Solicitud_Pago + "'";
                SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_transferencia
        /// DESCRIPCION : Consulta los datos de la solicitud para la transferencia
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 16/Marzo/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Datos_transferencia(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT PROVEEDOR." + Cat_Com_Proveedores.Campo_Banco_Proveedor + " AS BANCO, CUENTA." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " AS CUENTA_DESTINO_EMPLEADO, PROVEEDOR.";
                Mi_SQL += Cat_Com_Proveedores.Campo_Cuenta + " AS CUENTA_DESTINO_PROVEEDOR , PROVEEDOR." + Cat_Com_Proveedores.Campo_Clabe + " AS CLABE, PROVEEDOR." + Cat_Com_Proveedores.Campo_Tipo_Cuenta + " AS TIPO_CUENTA, PROVEEDOR.";
                Mi_SQL += Cat_Com_Proveedores.Campo_Banco_Proveedor_ID + " AS ID_BANCO_PROVEEDOR, SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Monto + " AS IMPORTE, SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Concepto;
                Mi_SQL += " AS CONCEPTO, EMPLEADO." + Cat_Empleados.Campo_Correo_Electronico + " AS EMAIL_EMPLEADO, PROVEEDOR." + Cat_Com_Proveedores.Campo_Correo_Electronico + " AS EMAIL_PROVEEDOR,";
                Mi_SQL += " RESERVA." + Ope_Psp_Reservas.Campo_Beneficiario + " AS BENEFICIARIO, BANCO." + Cat_Nom_Bancos.Campo_No_Cuenta+", SOLICITUD."+ Ope_Con_Solicitud_Pagos.Campo_Cuenta_Banco_Pago_ID+" AS CUENTA_ORIGEN, ";
                Mi_SQL += " BANCO_EMPLEADO." + Cat_Nom_Bancos.Campo_Nombre + " As BANCO_EMPLEADO, EMPLEADO." + Cat_Empleados.Campo_No_Cuenta_Bancaria + " As CUENTA_DESTINO_EMPLEADO, EMPLEADO." + Cat_Empleados.Campo_No_Tarjeta + ",";
                Mi_SQL += "(SELECT SUM(" + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva + ") FROM " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " WHERE " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "') AS IVA";
                Mi_SQL +=" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SOLICITUD";
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + " = EMPLEADO." + Cat_Empleados.Campo_Empleado_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO_EMPLEADO ON EMPLEADO." + Cat_Empleados.Campo_Banco_ID + " = BANCO_EMPLEADO." + Cat_Nom_Bancos.Campo_Banco_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTA ON CUENTA." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = EMPLEADO." + Cat_Empleados.Campo_Cuenta_Contable_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDOR ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = PROVEEDOR." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " RESERVA ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = RESERVA." + Ope_Psp_Reservas.Campo_No_Reserva;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Cuenta_Banco_Pago_ID + " = BANCO." + Cat_Nom_Bancos.Campo_Banco_ID;
                Mi_SQL += " WHERE SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Datos_Bancarios
        /// COMENTARIOS:    Consulta las solicitudes  con numero de transferencia
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     18/Abril/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Datos_Bancarios(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                Mi_SQL.Append("Select ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + ".*, ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Nombre + " as Nombre_Banco, ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ",");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + " as Nombre_Beneficiario, ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_No_Tarjeta);

                Mi_SQL.Append(" From ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);

                Mi_SQL.Append(" left outer join ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_SQL.Append(" ON ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva);
                Mi_SQL.Append("=");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);

                Mi_SQL.Append(" left outer join ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados);
                Mi_SQL.Append(" ON ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Empleado_ID);
                Mi_SQL.Append("=");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);

                Mi_SQL.Append(" left outer join ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos);
                Mi_SQL.Append(" ON ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Banco_ID);
                Mi_SQL.Append("=");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Banco_ID);

                Mi_SQL.Append(" Where ");

                if (!String.IsNullOrEmpty(Datos.P_No_Solicitud_Pago))
                {
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "' ");
                }


                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0]; ;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Datos_Empleado
        /// DESCRIPCION : Consulta los tipos de solicitudes de pago que existen
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Datos_Empleado(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT Empleados.*, Banco." + Cat_Nom_Bancos.Campo_Nombre + " as BANCO FROM " + Cat_Empleados.Tabla_Cat_Empleados + " Empleados ";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN  " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " Banco On Empleados." + Cat_Empleados.Campo_Banco_ID + " =Banco.";
                Mi_SQL = Mi_SQL + Cat_Nom_Bancos.Campo_Banco_ID + " WHERE " + Cat_Empleados.Campo_Empleado_ID + " ='" + Datos.P_Empleado_ID_Jefe + "'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Seguimiento_Cheque
        /// DESCRIPCION : Insertar el registro en la tabla de seguimiento de cheques
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Noe Mosqueda
        /// FECHA_CREO  : 31/Mayo/2013 11:15am
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Alta_Seguimiento_Cheque(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
            Int64 No_Cheque = 0; //variable para el numero del cheque
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL = null;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            object aux; //variable para las consultas escalares

            if (Datos.P_Cmmd == null)
            {
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
            }
            else
            {
                Comando_SQL = Datos.P_Cmmd;
            }

            try
            {
                //Colnsulta para el maximo numero de cheque
                Mi_SQL = "SELECT MAX(" + Ope_Con_Cheques.Campo_No_Cheque + ") FROM " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + " ";

                //Ejecutar consulta
                Comando_SQL.CommandText = Mi_SQL;
                aux = Comando_SQL.ExecuteScalar();

                //verificar si la consulta arrojo resultados
                if (aux != null)
                {
                    if (aux != DBNull.Value)
                    {
                        No_Cheque = Convert.ToInt64(aux) + 1;
                    }
                    else
                    {
                        No_Cheque = 1;
                    }
                }
                else
                {
                    No_Cheque = 1;
                }

                //Consulta para la insercion de los datos
                Mi_SQL = "INSERT INTO " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + " (" + Ope_Con_Cheques.Campo_No_Cheque + ", " + Ope_Con_Cheques.Campo_Folio + ", " +
                    Ope_Con_Cheques.Campo_Banco_ID + ", " + Ope_Con_Cheques.Campo_No_Pago + ", " + Ope_Con_Cheques.Campo_Fecha + ", " + Ope_Con_Cheques.Campo_Concepto + ", " +
                    Ope_Con_Cheques.Campo_Monto + ", " + Ope_Con_Cheques.Campo_Monto_Letra + ", " + Ope_Con_Cheques.Campo_Beneficiario + "," +
                    Ope_Con_Cheques.Campo_Usuario_Creo + ", " + Ope_Con_Cheques.Campo_Fecha_Creo + ", " + Ope_Con_Cheques.Campo_Estatus + ") VALUES(" + No_Cheque.ToString().Trim() + ", " +
                    "'" + Datos.P_No_Cheque + "', " +
                    "'" + Datos.P_Banco_ID + "', '" + Datos.P_No_Pago + "', GETDATE(), '" + Datos.P_Concepto + "', " + Datos.P_Monto + ", '" + Datos.P_Monto_Letra + "', " +
                    "'" + Datos.P_Beneficiario_Pago + "', '" + Datos.P_Usuario_Creo + "', GETDATE(), '" + Datos.P_Estatus + "') ";

                //Ejecutar consulta
                Comando_SQL.CommandText = Mi_SQL;
                Comando_SQL.ExecuteNonQuery();

                //verificar si hay comando
                if (Datos.P_Cmmd == null)
                {
                    //Ejecutar transaccion
                    Transaccion_SQL.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            }
        }
        
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_Pago_Deudores
        /// DESCRIPCION : Consulta los tipos de solicitudes de pago que existen
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Agoto/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Datos_Pago_Deudores(Cls_Ope_Con_Cheques_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Pago + ", ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Cuenta_Contable_ID + ", ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Nombre + ", ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Banco_ID + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Estatus + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Fecha_Pago + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Beneficiario_Pago + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Motivo_Cancelacion + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Cheque + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Forma_Pago + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Referencia_Transferencia_Banca + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Comentarios);
                Mi_SQL.Append(" FROM " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos);
                Mi_SQL.Append(" LEFT OUTER JOIN " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " ON " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_No_Pago + "= ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Pago);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " ON " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Banco_ID + "= ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Banco_ID);
                Mi_SQL.Append(" WHERE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_No_Deuda + " = '" + Datos.P_No_Deuda + "'");
                Mi_SQL.Append(" AND " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Estatus + " ='PAGADO'");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
    }
}
