﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Deudores.Negocios;

namespace JAPAMI.Deudores.Datos
{
    public class Cls_Ope_Con_Deudores_Datos
    {
        public Cls_Ope_Con_Deudores_Datos()
        {
        }
        #region (Métodos Operación)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Deudor
        /// DESCRIPCION : 1.Consulta el último ID dado de alta para poder ingresar el siguiente
        ///               2. Da de Alta El Tipo de Solictud de Pago en la BD con los datos 
        ///                  proporcionados por elusuario
        /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
        /// CREO        :  Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 03-Agosto-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static String Alta_Deudor(Cls_Ope_Con_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la cadena de inserción a realizar hacia la base de datos
            Object Deuda_ID;              //Obtiene el ID que le corresponde al nuevo registro en la base de datos
            try
            {
                //Consulta el último ID que fue dato de alta en la base de datos
                Mi_SQL.Append("SELECT ISNULL(MAX(" +OPE_CON_DEUDORES.Campo_No_Deuda + "),'0000000000')");
                Mi_SQL.Append(" FROM " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores );
                Deuda_ID = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Convert.IsDBNull(Deuda_ID))
                {
                    Datos.P_No_Deuda = "0000000001";
                }
                else
                {
                    Datos.P_No_Deuda = String.Format("{0:0000000000}", Convert.ToInt32(Deuda_ID) + 1);
                }

                Mi_SQL.Length = 0;
                //Inserta un nuevo registro en la base de datos con los datos obtenidos por el usuario
                Mi_SQL.Append("INSERT INTO " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores );
                Mi_SQL.Append(" (" + OPE_CON_DEUDORES.Campo_No_Deuda + ", " + OPE_CON_DEUDORES.Campo_Deudor_ID + ", " + OPE_CON_DEUDORES.Campo_Tipo_Deudor + ",");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Tipo_Movimiento + ", " + OPE_CON_DEUDORES.Campo_Estatus + ", "+ OPE_CON_DEUDORES.Campo_Tipo_Solicitud_ID + ", ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Concepto + ", " + OPE_CON_DEUDORES.Campo_Importe + ", " + OPE_CON_DEUDORES.Campo_Saldo + ", ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Fecha_Limite + ", "+Cat_Con_Tipo_Solicitud_Pagos.Campo_Usuario_Creo + ", " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Fecha_Creo +", "+OPE_CON_DEUDORES.Campo_Dependencia_ID+ ")");
                Mi_SQL.Append(" VALUES ('" + Datos.P_No_Deuda + "', '" + Datos.P_Deudor_ID + "', '" + Datos.P_Tipo_Deudor   + "', ");
                Mi_SQL.Append("'" + Datos.P_Tipo_Movimiento + "', '" + Datos.P_Estatus + "', '" + Datos.P_Tipo_Solicitud_ID  +"', '" + Datos.P_Concepto  + "', ");
                Mi_SQL.Append(Datos.P_Importe + ","+Datos.P_Importe + ",'" + Datos.P_Fecha_Limite + "', '" + Datos.P_Nombre_Usuario + "', GETDATE(),'" + Datos.P_Dependencia_ID + "')");
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                return Datos.P_No_Deuda;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Deudor
        /// DESCRIPCION : Modifica los datos del Tipo de Solicitud de Pago con lo que fueron 
        ///               introducidos por el usuario
        /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
        ///                       proporcionados por el usuario y van a sustituir a los datos que se
        ///                       encuentran en la base de datos
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 16-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Modificar_Deudor(Cls_Ope_Con_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable a contener la modificación de los datos a la base de datos
            try
            {
                //Modifica el registro del tipo de pago seleccionado por el usuario con los nuevos valores que fueron proporcionados por el usuario
                Mi_SQL.Append("UPDATE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores);
                Mi_SQL.Append(" SET " + OPE_CON_DEUDORES.Campo_Tipo_Movimiento  + " = '" + Datos.P_Tipo_Movimiento+ "', ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Tipo_Deudor + " = '" + Datos.P_Tipo_Deudor  + "', ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Estatus + " = '" + Datos.P_Estatus + "', ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Concepto  + " = '" + Datos.P_Concepto  + "', ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Deudor_ID + " ='"+Datos.P_Deudor_ID +"', ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Importe  + " ='" + Datos.P_Importe  + "', ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Usuario_Modifico + " = '" + Datos.P_Nombre_Usuario + "', ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + OPE_CON_DEUDORES.Campo_No_Deuda  + " = '" + Datos.P_No_Deuda + "'");
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Saldo_Deudor
        /// DESCRIPCION : Modifica los datos del Tipo de Solicitud de Pago con lo que fueron 
        ///               introducidos por el usuario
        /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
        ///                       proporcionados por el usuario y van a sustituir a los datos que se
        ///                       encuentran en la base de datos
        /// CREO        :Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 16-Agosto-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Modificar_Saldo_Deudor(Cls_Ope_Con_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable a contener la modificación de los datos a la base de datos
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Modifica el registro del tipo de pago seleccionado por el usuario con los nuevos valores que fueron proporcionados por el usuario
                Mi_SQL.Append("UPDATE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores);
                Mi_SQL.Append(" SET " + OPE_CON_DEUDORES.Campo_Saldo + " = " + OPE_CON_DEUDORES.Campo_Saldo + " - " + Datos.P_Importe + ", ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Usuario_Modifico + " = '" + Datos.P_Nombre_Usuario + "', ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + OPE_CON_DEUDORES.Campo_No_Deuda  + " = '" + Datos.P_No_Deuda + "'");
                Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                //OracleHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:   Crear_Solicitud_Pago_Interna
        /// DESCRIPCION :           Crear la solicitud de pago interna para los gastos por comprobar
        /// PARAMETROS  :           Datos: Variable para la capa de negocios
        /// CREO        :           Noe Mosqueda Valadez
        /// FECHA_CREO  :           10/Julio/2013 16:00
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static string Crear_Solicitud_Pago_Interna(Cls_Ope_Con_Deudores_Negocio Datos)
        {
            //Declaracion de variables
            string Mi_SQL = string.Empty; //Variable a contener la modificación de los datos a la base de datos
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            string Tipo_Solicitud_Pago = string.Empty; //variable para el tipo de la solicitud de pago de la reserva
            object aux; //variable auxiliar para las consultas escalares
            DataTable Dt_Reserva_Detalles = new DataTable(); //tabla para los detalles de la reserva
            string Partida_ID = string.Empty; //variable para el ID de la partida
            string Fuente_Financiamiento_ID = string.Empty; //Variable para el ID de la fuente de financiamiento
            string Proyecto_Programa_ID = string.Empty; //variable para el ID del proyecto programa
            SqlDataAdapter da; //Adaptador para las consultas 
            string No_Solicitud_Pago = string.Empty; //variable para el numero de la solicitud de pago
            DataTable Dt_Datos_Proveedor = new DataTable(); //tabla para los datos del proveedor

            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            //instanciar el adaptador
            da = new SqlDataAdapter(Cmmd);

            try
            {
                //Verificar el tipo de deudor
                if (Datos.P_Tipo_Deudor == "PROVEEDOR")
                {
                    //Consultar los datos del proveedor
                    Mi_SQL = "SELECT " + Cat_Com_Proveedores.Campo_Proveedor_ID + ", " + Cat_Com_Proveedores.Campo_Nombre + ", " + Cat_Com_Proveedores.Campo_Compañia + ", " +
                        Cat_Com_Proveedores.Campo_RFC + ", " + Cat_Com_Proveedores.Campo_CURP + " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " " +
                        "WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Datos.P_Deudor_ID + "' ";

                    //Ejecutar consulta
                    Cmmd.CommandText = Mi_SQL;
                    da.SelectCommand = Cmmd;
                    da.Fill(Dt_Datos_Proveedor);
                }

                //Consulta para obtener el tipo de solicitud de pago de la reserva
                Mi_SQL = "SELECT " + Ope_Psp_Reservas.Campo_Tipo_Solicitud_Pago_ID + " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " " +
                    "WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Datos.P_No_Reserva.ToString().Trim() + " ";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                aux = Cmmd.ExecuteScalar();

                //verificar si no es nulo
                if (aux != null)
                {
                    if (aux != DBNull.Value)
                    {
                        Tipo_Solicitud_Pago = aux.ToString().Trim();
                    }
                    else
                    {
                        Tipo_Solicitud_Pago = "00004";
                    }
                }
                else
                {
                    Tipo_Solicitud_Pago = "00004";
                }

                //Consultar los ID de la Fuente de financiamiento, proyecto programa, partida
                Mi_SQL = "SELECT " + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + ", " + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + ", " +
                    Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ", " + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + ", " +
                    Ope_Psp_Reservas_Detalles.Campo_Capitulo_ID + ", " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " " +
                    "FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " " +
                    "WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + Datos.P_No_Reserva.ToString().Trim() + " ";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                da.Fill(Dt_Reserva_Detalles);

                //verificar si la consulta arrojo resultado
                if (Dt_Reserva_Detalles.Rows.Count > 0)
                {
                    //Colocar los datos de la fuente de financiamiento, proyecto programa, etc
                    Partida_ID = Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString().Trim();
                    Fuente_Financiamiento_ID = Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                    Proyecto_Programa_ID = Dt_Reserva_Detalles.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                }
                else
                {
                    Trans.Rollback();
                    throw new Exception("La requisicion no contiene datos.");
                }

                //Consulta para el numero de solicitud de pago
                Mi_SQL = "SELECT MAX(" + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ") FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " ";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                aux = Cmmd.ExecuteScalar();

                //verificar si es nulo
                if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                {
                    //Verificar si no es -1
                    if (Convert.ToInt32(aux) > -1)
                    {
                        No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(aux) + 1);
                    }
                    else
                    {
                        No_Solicitud_Pago = "0000000001";
                    }
                }
                else
                {
                    No_Solicitud_Pago = "0000000001";
                }

                //Asignar consulta para la solicitud de pago
                Mi_SQL = "INSERT INTO " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " (" + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", " +
                    Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", " + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", " +
                    Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", " + Ope_Con_Solicitud_Pagos.Campo_Monto + ", " + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", " +
                    Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ", " + Ope_Con_Solicitud_Pagos.Campo_No_Reserva;

                //Verificar el tipo de deudor
                switch (Datos.P_Tipo_Deudor)
                {
                    case "EMPLEADO":
                        Mi_SQL += ", " + Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + ") ";
                        break;

                    case "PROVEEDOR":
                        Mi_SQL += ", " + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + ") ";
                        break;

                    default:
                        Mi_SQL += ") ";
                        break;
                }

                Mi_SQL += "VALUES('" + No_Solicitud_Pago + "', '" + Tipo_Solicitud_Pago + "', '" + Datos.P_Concepto + "', GETDATE(), " + Datos.P_Importe.ToString().Trim() + ", " +
                    "'PENDIENTE', '" + Datos.P_Nombre_Usuario + "', GETDATE(), " + Datos.P_No_Reserva.ToString().Trim();

                //Verificar el tipo de deudor
                switch (Datos.P_Tipo_Deudor)
                {
                    case "EMPLEADO": 
                        Mi_SQL += ", '" + Datos.P_Deudor_ID.Substring(Datos.P_Deudor_ID.IndexOf('-')+1) + "') ";
                        break;
                    case "PROVEEDOR":
                        Mi_SQL += ", '" + Datos.P_Deudor_ID.Substring(Datos.P_Deudor_ID.IndexOf('-') + 1) + "') ";
                        break;

                    default:
                        Mi_SQL += ") ";
                        break;
                }
                
                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();
                //Agregar las facturas de la solicitud de pago en los detalles
                foreach(DataRow Fila in Datos.P_Dt_Detalles_Solicitud_Pago.Rows){
                    //Agregar los detalles de la solicitud de pago
                    Mi_SQL = "INSERT INTO " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " (" +
                        Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura + ", " +
                        Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Factura + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + ", " +
                        Ope_Con_Solicitud_Pagos_Detalles.Campo_Usuario_Creo + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Creo + ", ";

                    //Verificar el tipo de deudor
                    if (Datos.P_Tipo_Deudor == "PROVEEDOR")
                    {
                        Mi_SQL += Ope_Con_Solicitud_Pagos_Detalles.Campo_Nombre_Proveedor_Factura + ", ";
                    }

                    Mi_SQL += Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Documento + ", " +
                        Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID + ", " +
                        Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva + ", ";

                    //Verificar el tipo de deudor
                    if (Datos.P_Tipo_Deudor == "PROVEEDOR")
                    {
                        Mi_SQL += Ope_Con_Solicitud_Pagos_Detalles.Campo_RFC + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_CURP + ", ";
                    }

                    Mi_SQL += Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR + ", " +
                        Ope_Con_Solicitud_Pagos_Detalles.Campo_Retencion_IVA + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula + ", " +
                        Ope_Con_Solicitud_Pagos_Detalles.Campo_IEPS + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISH + ") " +
                        " VALUES('" + No_Solicitud_Pago + "', '" + Fila["No_Documento"].ToString() + "', '" + string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Fila["Fecha_Documento"].ToString())) + "', " +
                        Fila["Monto"].ToString() + ", '" + Datos.P_Nombre_Usuario + "', GETDATE(), ";

                    //Verificar el tipo de deudor
                    if (Datos.P_Tipo_Deudor == "PROVEEDOR")
                    {
                        Mi_SQL += "'" + Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Compañia].ToString().Trim() + "', ";
                    }


                    Mi_SQL += "'DOCUMENTO', '" + Partida_ID + "', '" + Fuente_Financiamiento_ID + "', '" + Proyecto_Programa_ID + "', 0, ";

                    //Verificar el tipo de deudor
                    if (Datos.P_Tipo_Deudor == "PROVEEDOR")
                    {
                        Mi_SQL += "'" + Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_RFC].ToString().Trim() + "', " +
                            "'" + Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_CURP].ToString().Trim() + "', ";
                    }

                    Mi_SQL += "'OTROS', 0, 0, 0, 0, 0) ";

                    //Ejecutar consulta
                    Cmmd.CommandText = Mi_SQL;
                    Cmmd.ExecuteNonQuery();
                }
                //Verificar si hay comando
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }

                //Entregar el numero de la solicitud de pago
                return No_Solicitud_Pago;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        #endregion
        #region(Consultas)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Proveedores_Deudores
        /// DESCRIPCION : Consultar los proveedores que tengan asignada una cuenta de deudor
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 02-Agosto-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Proveedores_Deudores(Cls_Ope_Con_Deudores_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos

                try
                {
                    Mi_SQL.Append("SELECT " + Cat_Com_Proveedores.Campo_Proveedor_ID + "," + Cat_Com_Proveedores.Campo_Cuenta_Contable_ID + "," + Cat_Com_Proveedores.Campo_Compañia + " AS NOMBRE FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores);
                    if (!String.IsNullOrEmpty(Datos.P_Deudor))
                    {
                        Mi_SQL.Append(" WHERE " + Cat_Com_Proveedores.Campo_Compañia + " like'%"+Datos.P_Deudor+"%'");
                        Mi_SQL.Append(" AND " + Cat_Com_Proveedores.Campo_Cuenta_Deudor_ID + " IS NOT NULL ");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Cat_Com_Proveedores.Campo_Cuenta_Deudor_ID + " IS NOT NULL ");
                    }

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Empleados_Deudores
        /// DESCRIPCION : Consultar los empleados  que tengan asignada una cuenta de deudor
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 02-Agosto-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Empleados_Deudores(Cls_Ope_Con_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos

            try
            {
                Mi_SQL.Append("SELECT " + Cat_Empleados.Campo_Empleado_ID + ",(" + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno);
                Mi_SQL.Append("+' '+"+Cat_Empleados.Campo_Apellido_Materno+ ") AS NOMBRE FROM " + Cat_Empleados.Tabla_Cat_Empleados);
                Mi_SQL.Append(" WHERE " + Cat_Empleados.Campo_Estatus  + "='ACTIVO'");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Deudores
        /// DESCRIPCION : Consultar los empleados  que tengan asignada una cuenta de deudor
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 02-Agosto-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Deudores(Cls_Ope_Con_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlDataAdapter Da_SQL = new SqlDataAdapter();
            DataSet Ds_SQL = new DataSet();
            DataTable Dt_Psp = new DataTable();

            SqlTransaction Trans = null;
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("SELECT DEUDORES.* , CASE WHEN (DEUDORES."+OPE_CON_DEUDORES.Campo_Tipo_Deudor +"= 'EMPLEADO') THEN(");
                Mi_SQL.Append(" SELECT "+Cat_Empleados.Campo_Nombre +"+' '+"+Cat_Empleados.Campo_Apellido_Paterno+"+' '+"+Cat_Empleados.Campo_Apellido_Materno+" FROM ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados +" EMPLEADO WHERE EMPLEADO."+Cat_Empleados.Campo_Empleado_ID +"= DEUDORES."+OPE_CON_DEUDORES.Campo_Deudor_ID+") ELSE ");
                Mi_SQL.Append(" PROVEEDOR." + Cat_Com_Proveedores.Campo_Compañia + " END AS NOMBRE FROM " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " DEUDORES");
                Mi_SQL.Append(" LEFT OUTER JOIN "+Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores +" PROVEEDOR ON DEUDORES."+OPE_CON_DEUDORES.Campo_Deudor_ID+"=PROVEEDOR."+Cat_Com_Proveedores.Campo_Proveedor_ID);
                Mi_SQL.Append(" WHERE DEUDORES."+OPE_CON_DEUDORES.Campo_Deudor_ID+" is not null ");
                if (!String.IsNullOrEmpty(Datos.P_Deudor_ID))
                {
                    Mi_SQL.Append(" AND DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + "='" + Datos.P_Deudor_ID + "'");
                }
                if(!String.IsNullOrEmpty(Datos.P_No_Deuda)){
                    Mi_SQL.Append(" AND DEUDORES." + OPE_CON_DEUDORES.Campo_No_Deuda + "='" + Datos.P_No_Deuda + "'");
                }
                if (!String.IsNullOrEmpty(Datos.P_Deudor))
                {
                    Mi_SQL.Append(" AND " + (" (SELECT " + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "= DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + ")") + " LIKE '%" + Datos.P_Deudor + "%'");
                    Mi_SQL.Append(" OR " + (" (SELECT " + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + "+' '+" + Cat_Empleados.Campo_Nombre  +" FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "= DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + ")") + " LIKE '%" + Datos.P_Deudor + "%'");
                }
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Final))
                {
                    Mi_SQL.Append(" AND DEUDORES." + OPE_CON_DEUDORES.Campo_Fecha_Creo + " < '" + Datos.P_Fecha_Final + "'");
                }
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicial))
                {
                    Mi_SQL.Append(" AND DEUDORES." + OPE_CON_DEUDORES.Campo_Fecha_Creo + " > '" + Datos.P_Fecha_Inicial + "'");
                }
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Da_SQL.SelectCommand = Cmmd;
                Da_SQL.Fill(Ds_SQL);
                Dt_Psp = Ds_SQL.Tables[0];

                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Dt_Psp;
                //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Deudores_Por_Deudor
        /// DESCRIPCION : Consultar los empleados  que tengan asignada una cuenta de deudor por no de deudor
        /// PARAMETROS  : 
        /// CREO        : Jorge L.Gonzalez Reyes
        /// FECHA_CREO  : 17-Agosto-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Deudores_Por_Deudor(Cls_Ope_Con_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT DEUDORES.* ");
                Mi_SQL.Append(", PAGO." + Ope_Con_Pagos.Campo_No_poliza + " AS POLIZA_PAGADO, PAGO." + Ope_Con_Pagos.Campo_Tipo_Poliza_ID + " AS TIPO_POLIZA_PAGADO, ");
                Mi_SQL.Append("PAGO." + Ope_Con_Pagos.Campo_Mes_Ano + " AS MES_ANO_PAGADO, PAGO." + Ope_Con_Pagos.Campo_Fecha_Pago + ", PAGO." + Ope_Con_Pagos.Campo_No_Solicitud_Pago);
                Mi_SQL.Append(", CASE WHEN (DEUDORES." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "= 'EMPLEADO') THEN(");
                Mi_SQL.Append(" SELECT " + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + " FROM ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "= DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + ") ELSE ");
                Mi_SQL.Append(" PROVEEDOR." + Cat_Com_Proveedores.Campo_Compañia + " END AS NOMBRE FROM " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " DEUDORES");
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDOR ON DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + "=PROVEEDOR." + Cat_Com_Proveedores.Campo_Proveedor_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " PAGO ON DEUDORES." + OPE_CON_DEUDORES.Campo_No_Pago + "=PAGO." + Ope_Con_Pagos.Campo_No_Pago);
                
                Mi_SQL.Append(" WHERE DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + "='" + Datos.P_Deudor_ID + "'");
                if (!String.IsNullOrEmpty(Datos.P_No_Deuda))
                {
                    Mi_SQL.Append(" AND DEUDORES." + OPE_CON_DEUDORES.Campo_No_Deuda + "='" + Datos.P_No_Deuda + "'");
                }
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Banco_Monto_Deudas
        /// DESCRIPCION : Consultar los empleados  que tengan asignada una cuenta de deudor por no de deudor
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 29-Agosto-2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Banco_Monto_Deudas(Cls_Ope_Con_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand(); 
            SqlDataAdapter Da_SQL = new SqlDataAdapter();
            DataSet Ds_SQL = new DataSet();
            DataTable Dt_Psp = new DataTable();
                
            SqlTransaction Trans = null;
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("SELECT DISTINCT("+OPE_CON_DEUDORES.Campo_Cuenta_Banco_Pago+"), SUM("+Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos+"."+Ope_Con_Solicitud_Pagos.Campo_Monto+") AS MONTO,");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Deudor_ID+", "+OPE_CON_DEUDORES.Campo_Tipo_Deudor+" FROM ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores +" LEFT JOIN "+Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas +" on "+ Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas +"."+Ope_Psp_Reservas.Campo_No_Deuda+" = ");
                Mi_SQL.Append( OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores+"."+OPE_CON_DEUDORES.Campo_No_Deuda );
                Mi_SQL.Append(" LEFT JOIN "+ Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos +" ON "+Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos +"."+ Ope_Con_Solicitud_Pagos.Campo_No_Reserva+"= ");
                Mi_SQL.Append( Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas +"."+Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_SQL.Append(" WHERE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_No_Deuda + " IN " + Datos.P_No_Deuda +" GROUP BY ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Cuenta_Banco_Pago + ", " + OPE_CON_DEUDORES.Campo_Deudor_ID + ", " + OPE_CON_DEUDORES.Campo_Tipo_Deudor);
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Da_SQL.SelectCommand = Cmmd;
                Da_SQL.Fill(Ds_SQL);
                Dt_Psp = Ds_SQL.Tables[0];

                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Dt_Psp;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Tipo_Solicitud_Pagos_Combo
        /// DESCRIPCION : Consulta unicamente los datos que son necesario para el llenado
        ///               de algun combo dentro del sistema
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 16-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Tipo_Solicitud_Pagos_Combo(Cls_Ope_Con_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos

            try
            {
                Mi_SQL.Append("SELECT " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + ", " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion_Finanzas);
                Mi_SQL.Append(" FROM " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago);
                Mi_SQL.Append(" WHERE " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Estatus + " = 'ACTIVO'");
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Comprobacion))
                {
                    Mi_SQL.Append(" AND TIPO_COMPROBACION = '" + Datos.P_Tipo_Comprobacion + "'");
                }
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_impresion
        /// DESCRIPCION : Consulta unicamente los datos que son necesario para el llenado
        ///               de algun combo dentro del sistema
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 22-Agosto-2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Datos_impresion(Cls_Ope_Con_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT DEUDA." + OPE_CON_DEUDORES.Campo_No_Deuda + ", DEUDA." + OPE_CON_DEUDORES.Campo_Concepto + " as MOTIVO, DEUDA." + OPE_CON_DEUDORES.Campo_Importe + " AS MONTO,");
                Mi_SQL.Append(" SOLICITUD."+ Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion +" AS TIPO_DEUDA, DEUDA."+OPE_CON_DEUDORES.Campo_Fecha_Creo+", ");
                Mi_SQL.Append(" CASE WHEN (DEUDA."+OPE_CON_DEUDORES.Campo_Tipo_Deudor+"='EMPLEADO') THEN (SELECT("+Cat_Empleados.Campo_Nombre+" +' '+ "+Cat_Empleados.Campo_Apellido_Paterno+"+' '+"+Cat_Empleados.Campo_Apellido_Materno+") AS NOMBRE");
                Mi_SQL.Append(" FROM "+Cat_Empleados.Tabla_Cat_Empleados+" WHERE "+Cat_Empleados.Campo_Empleado_ID+"=DEUDA."+OPE_CON_DEUDORES.Campo_Deudor_ID+")");
                Mi_SQL.Append(" ELSE ");
                Mi_SQL.Append("PROVEEDOR." + Cat_Com_Proveedores.Campo_Compañia + " END AS SOLICITANTE FROM " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " DEUDA");
                Mi_SQL.Append(" LEFT JOIN "+Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores+" PROVEEDOR ON PROVEEDOR."+Cat_Com_Proveedores.Campo_Proveedor_ID+"=DEUDA."+OPE_CON_DEUDORES.Campo_Deudor_ID);
                Mi_SQL.Append(" LEFT JOIN "+ Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago+" SOLICITUD ON SOLICITUD."+Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID+"=DEUDA."+OPE_CON_DEUDORES.Campo_Tipo_Solicitud_ID);
                Mi_SQL.Append(" WHERE " + OPE_CON_DEUDORES.Campo_No_Deuda + " = '"+Datos.P_No_Deuda+"'");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Deudores_Activos
        /// DESCRIPCION : Consultar los empleados  que tengan asignada una cuenta de deudor
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 02-Agosto-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Deudores_Activos(Cls_Ope_Con_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos

            try
            {
                Mi_SQL.Append("SELECT DEUDORES.* , CASE WHEN (DEUDORES." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "= 'EMPLEADO') THEN(");
                Mi_SQL.Append(" SELECT " + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + " FROM ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "= DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + ") ELSE ");
                Mi_SQL.Append(" PROVEEDOR." + Cat_Com_Proveedores.Campo_Compañia + " END AS NOMBRE FROM " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " DEUDORES");
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDOR ON DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + "=PROVEEDOR." + Cat_Com_Proveedores.Campo_Proveedor_ID);
                Mi_SQL.Append(" WHERE DEUDORES."+OPE_CON_DEUDORES.Campo_Estatus+" ='PAGADO' AND DEUDORES."+OPE_CON_DEUDORES.Campo_Saldo+">0");
                if(!String.IsNullOrEmpty(Datos.P_Deudor_ID) && !String.IsNullOrEmpty(Datos.P_Tipo_Deudor)){
                    Mi_SQL.Append(" AND DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + "='" + Datos.P_Deudor_ID + "' ");
                    Mi_SQL.Append(" AND DEUDORES." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "='" + Datos.P_Tipo_Deudor + "'");
                }
                if (!String.IsNullOrEmpty(Datos.P_No_Deuda))
                {
                    Mi_SQL.Append(" AND DEUDORES." + OPE_CON_DEUDORES.Campo_No_Deuda + "='" + Datos.P_No_Deuda + "' ");
                }
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Contables_De_Reserva
        /// DESCRIPCION : Consultar los empleados  que tengan asignada una cuenta de deudor por no de deudor
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 29-Agosto-2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Cuentas_Contables_De_Reserva(Cls_Ope_Con_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand(); 
            SqlDataAdapter Da_SQL = new SqlDataAdapter();
            DataSet Ds_SQL = new DataSet();
            DataTable Dt_Psp = new DataTable();
                
            SqlTransaction Trans = null;
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ",( ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + " )as Empleado,");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " , ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Cuenta_Contable_ID + "  as Cuenta_Deudor, "); 
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                Mi_SQL.Append(" (LTRIM(RTRIM(STR(" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_SQL.Append(")))+'-'+" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ") AS Reserva, ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + " AS Cuenta_Contable_Reserva, ");
                Mi_SQL.Append("SUM(");
                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + ") AS Monto_Partida");
                Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " ON  " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + " = ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " ON  " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " ON  " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + " = ");
                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " AND ");
                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " = ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AND ");
                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " = ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " AND ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID + " = ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " AND ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " = ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " ON  " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + " = ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ");
                //Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID + "=");
                //Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " IN " + Datos.P_No_Deuda);
                Mi_SQL.Append(" Group by " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ", ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + ", ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + ", ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " , ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Cuenta_Contable_ID + " , ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + ", ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ", ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + ",");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Da_SQL.SelectCommand = Cmmd;
                Da_SQL.Fill(Ds_SQL);
                Dt_Psp = Ds_SQL.Tables[0];

                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Dt_Psp;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Datos_Vale
        /// DESCRIPCION : Consultar los empleados  que tengan asignada una cuenta de deudor por no de deudor
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 29-Agosto-2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Datos_Vale(Cls_Ope_Con_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand(); 
            SqlDataAdapter Da_SQL = new SqlDataAdapter();
            DataSet Ds_SQL = new DataSet();
            DataTable Dt_Psp = new DataTable();
                
            SqlTransaction Trans = null;
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("SELECT VALE." + Ope_Con_Vales_Deudores.Campo_No_Vale_Deudor + " AS NO_VALE ,VALE." + Ope_Con_Vales_Deudores.Campo_Importe + ",");
                Mi_SQL.Append("(EMPLEADO." + Cat_Empleados.Campo_Nombre + " +' '+ EMPLEADO." + Cat_Empleados.Campo_Apellido_Paterno + "+' '+ EMPLEADO." + Cat_Empleados.Campo_Apellido_Materno + ") AS DEUDOR ,");
                Mi_SQL.Append(" PUESTO." + Cat_Puestos.Campo_Nombre+" AS PUESTO, EMPLEADO."+Cat_Empleados.Campo_No_Empleado+",(SELECT ("+Cat_Empleados.Campo_Nombre+"+' '+"+Cat_Empleados.Campo_Apellido_Paterno+"+' '+"+Cat_Empleados.Campo_Apellido_Materno+") FROM "+Cat_Con_Parametros.Tabla_Cat_Con_Parametros+" LEFT JOIN "+Cat_Empleados.Tabla_Cat_Empleados +" on "+Cat_Empleados.Tabla_Cat_Empleados +"."+Cat_Empleados.Campo_Empleado_ID+"="+Cat_Con_Parametros.Tabla_Cat_Con_Parametros +"."+Cat_Con_Parametros.Campo_Usuario_Autoriza_Gastos +")AS USUARIO_AUTORIZA,");
                Mi_SQL.Append(" DEUDA."+OPE_CON_DEUDORES.Campo_Fecha_Limite+", DEUDA."+OPE_CON_DEUDORES.Campo_Concepto+" AS OBSERVACIONES, ' ' AS CANTIDAD_LETRA , '' AS NO_CATORCENA, '' AS FECHA_CATORCENA ,");
                Mi_SQL.Append("( SELECT " + Cat_Empleados.Campo_No_Empleado + " FROM " + Cat_Empleados.Tabla_Cat_Empleados +" WHERE " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + "=DEUDA." + OPE_CON_DEUDORES.Campo_Deudor_ID + ")AS NO_EMPLEADO ");
                Mi_SQL.Append("FROM "+ Ope_Con_Vales_Deudores.Tabla_Ope_Con_Vales_Deudores +" VALE ");
                Mi_SQL.Append(" LEFT JOIN "+ OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores+" DEUDA  ON  VALE."+Ope_Con_Vales_Deudores.Campo_No_Deuda+"=DEUDA."+OPE_CON_DEUDORES.Campo_No_Deuda);
                Mi_SQL.Append(" LEFT JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO  ON  DEUDA." + OPE_CON_DEUDORES.Campo_Deudor_ID + "=EMPLEADO." + Cat_Empleados.Campo_Empleado_ID);
                Mi_SQL.Append(" LEFT JOIN " + Cat_Puestos.Tabla_Cat_Puestos + " PUESTO  ON  EMPLEADO." + Cat_Empleados.Campo_Puesto_ID + "=PUESTO." + Cat_Puestos.Campo_Puesto_ID);
                Mi_SQL.Append(" WHERE VALE." + Ope_Con_Vales_Deudores.Campo_No_Vale_Deudor + " = '" + Datos.P_Vale + "'");
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Da_SQL.SelectCommand = Cmmd;
                Da_SQL.Fill(Ds_SQL);
                Dt_Psp = Ds_SQL.Tables[0];

                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Dt_Psp;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        #endregion
    }
}