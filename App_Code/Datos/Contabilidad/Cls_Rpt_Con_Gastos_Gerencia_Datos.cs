﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Reporte_Gastos_Gerencia.Negocio;

/// <summary>
/// Summary description for Cls_Rpt_Con_Gastos_Gerencia_Datos
/// </summary>
namespace JAPAMI.Reporte_Gastos_Gerencia.Datos
{
    public class Cls_Rpt_Con_Gastos_Gerencia_Datos
    {
        public Cls_Rpt_Con_Gastos_Gerencia_Datos()
        {
        }

        public static DataTable Consulta_Anios_Cerrados()
        {
            //Declaracion de variables
            string Mi_SQL = string.Empty; //variable para la consulta
            DataTable Dt_Resultado = new DataTable(); //tabla para le resultado
            DataTable Dt_Aux = new DataTable(); //tabla auxiliar para las consultas
            DataRow Renglon; //Renglon para el llenado de la tabla
            int Cont_Elementos = 0; //variable para el contador

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT " + Ope_Con_Cierre_Mensual_Gral.Campo_Anio + ", COUNT(" + Ope_Con_Cierre_Mensual_Gral.Campo_Mes + ") AS Total_Meses " +
                    "FROM " + Ope_Con_Cierre_Mensual_Gral.Tabla_Ope_Con_Cierre_Mensual_Gral + " " +
                    "WHERE " + Ope_Con_Cierre_Mensual_Gral.Campo_Anio + " > 2011 " + 
                    "GROUP BY " + Ope_Con_Cierre_Mensual_Gral.Campo_Anio + " " +
                    "ORDER BY " + Ope_Con_Cierre_Mensual_Gral.Campo_Anio + " ";

                //Ejecutar consulta
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                
                //Colocar columna a la tabla
                Dt_Resultado.Columns.Add("ANIO", typeof(int));

                //Ciclo para el llenado de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Dt_Aux.Rows.Count; Cont_Elementos++)
                {
                    //Instanciar renglon
                    Renglon = Dt_Resultado.NewRow();

                    //Llenar el renglon
                    Renglon["ANIO"] = Convert.ToInt32(Dt_Aux.Rows[Cont_Elementos][Ope_Con_Cierre_Mensual_Gral.Campo_Anio]);

                    //Colocar el renglon en la tabla
                    Dt_Resultado.Rows.Add(Renglon);
                    Dt_Resultado.AcceptChanges();
                }

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static DataTable Consulta_Grupo_Gerencial( Cls_Rpt_Con_Gastos_Gerencias_Negocio Datos)
        {
            DataTable Dt_Resultado = new DataTable(); 
            String Mi_SQL = string.Empty; //variable para la consulta
            try
            {
                Mi_SQL = "SELECT " + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + ",(" + Cat_Grupos_Dependencias.Campo_Clave + "+ ' - ' +" + Cat_Grupos_Dependencias.Campo_Nombre + ") AS Nombre, "+Cat_Grupos_Dependencias.Campo_Clave_Contabilidad;
                Mi_SQL = Mi_SQL +" FROM "+ Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias;
                if(!String.IsNullOrEmpty(Datos.P_Grupo_Dependencia_ID))
                {
                    Mi_SQL=Mi_SQL+" WHERE "+Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID+" ='"+Datos.P_Grupo_Dependencia_ID+"'";
                }
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Resultado;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message, Ex);
            }
        }
        public static DataTable Consultar_Gerencias(Cls_Rpt_Con_Gastos_Gerencias_Negocio Datos)
        {
            DataTable Dt_Dependencias = new DataTable();
            String Sql = string.Empty;
            try
            {
                Sql = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + ",(" + Cat_Dependencias.Campo_Clave + "+' - '+" + Cat_Dependencias.Campo_Nombre + ") AS NOMBRE ," + Cat_Dependencias.Campo_Clave_Contabilidad;
                Sql = Sql + " FROM "+Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Estatus + "='ACTIVO' ";
                if(!String.IsNullOrEmpty(Datos.P_Grupo_Dependencia_ID))
                {
                    Sql=Sql+" AND "+Cat_Dependencias.Campo_Grupo_Dependencia_ID + "='" + Datos.P_Grupo_Dependencia_ID + "'";
                }
                Dt_Dependencias = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Sql).Tables[0];
                return Dt_Dependencias;
            }catch(Exception Ex)
            {
                throw new Exception(Ex.Message, Ex);
            }
        }
        public static DataTable Consulta_Meses_Anio(Cls_Rpt_Con_Gastos_Gerencias_Negocio Datos)
        {
            //Declaracion de variables
            string Mi_SQL = string.Empty; //variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado
            object aux; //Variable auxiliar para las consultas escalares
            int Total_Meses_Cerrados = 0; //variable para el total de los meses cerrados
            int Cont_Elementos = 0; //variable apra el contador
            DataRow Renglon; //Renglona para el llenado de la tabla

            try
            {
                //Asignar consulta para conocer el toptal de meses cerrados del año seleccionado
                Mi_SQL = "SELECT COUNT(*) AS Total FROM " + Ope_Con_Cierre_Mensual_Gral.Tabla_Ope_Con_Cierre_Mensual_Gral + " " +
                    "WHERE " + Ope_Con_Cierre_Mensual_Gral.Campo_Anio + " = '" + Datos.P_Anio.ToString().Trim() + "' " ;
                
                //Ejecutar consulta
                aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                //verificar si es nulo
                if (aux != null)
                {
                    if (aux != DBNull.Value)
                    {
                        Total_Meses_Cerrados = Convert.ToInt32(aux);
                    }
                }

                //Colocar columnas a la tabla
                Dt_Resultado.Columns.Add("MES_LETRA", typeof(string));
                Dt_Resultado.Columns.Add("MES_NUMERO", typeof(int));

                //Ciclo para la construccion de la tabla de los meses
                for (Cont_Elementos = 0; Cont_Elementos < Total_Meses_Cerrados; Cont_Elementos++)
                {
                    //Instanciar renglon de la tabla
                    Renglon = Dt_Resultado.NewRow();

                    //Seleccionar el mes
                    switch (Cont_Elementos + 1)
                    {
                        case 1:
                            Renglon["MES_LETRA"] = "Enero";
                            Renglon["MES_NUMERO"] = 1;
                            break;

                        case 2:
                            Renglon["MES_LETRA"] = "Febrero";
                            Renglon["MES_NUMERO"] = 2;
                            break;

                        case 3:
                            Renglon["MES_LETRA"] = "Marzo";
                            Renglon["MES_NUMERO"] = 3;
                            break;

                        case 4:
                            Renglon["MES_LETRA"] = "Abril";
                            Renglon["MES_NUMERO"] = 4;
                            break;

                        case 5:
                            Renglon["MES_LETRA"] = "Mayo";
                            Renglon["MES_NUMERO"] = 5;
                            break;

                        case 6:
                            Renglon["MES_LETRA"] = "Junio";
                            Renglon["MES_NUMERO"] = 6;
                            break;

                        case 7:
                            Renglon["MES_LETRA"] = "Julio";
                            Renglon["MES_NUMERO"] = 7;
                            break;

                        case 8:
                            Renglon["MES_LETRA"] = "Agosto";
                            Renglon["MES_NUMERO"] = 9;
                            break;

                        case 9:
                            Renglon["MES_LETRA"] = "Septiembre";
                            Renglon["MES_NUMERO"] = 9;
                            break;

                        case 10:
                            Renglon["MES_LETRA"] = "Octubre";
                            Renglon["MES_NUMERO"] = 10;
                            break;

                        case 11:
                            Renglon["MES_LETRA"] = "Noviembre";
                            Renglon["MES_NUMERO"] = 11;
                            break;

                        case 12:
                            Renglon["MES_LETRA"] = "Diciembre";
                            Renglon["MES_NUMERO"] = 12;
                            break;

                        default:
                            break;
                    }

                    //Colocar el renglon en la tabla
                    Dt_Resultado.Rows.Add(Renglon);
                    Dt_Resultado.AcceptChanges();
                }

                //entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
        public static DataTable Consulta_Gastos_Gerencia(Cls_Rpt_Con_Gastos_Gerencias_Negocio Datos)
        {
            //Declaracion de variables
            string Mi_SQL = string.Empty; //variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado
            DataTable Dt_Aux = new DataTable(); //Tabla auxiliar para las consultas
            DataRow Renglon; //Renglon para el llenado de la tabla
            int Cont_Elementos = 0; //variable para el contador
            DataTable Dt_Grupos_Dependencias = new DataTable(); //tabla para los grupos de las dependencias
            DataTable Dt_Dependencias = new DataTable(); //tabla para las dependencias
            int Cont_Grupos_Dependencias = 0; //Contador para el grupo de las dependencias
            int Cont_Dependencias = 0; //Contador para las dependencias
            int Cont_Meses = 0; //variable para el contador de los meses
            DataTable Dt_Grupos_Dependencias_Cuentas = new DataTable(); //Tabla con la infromacion de los grupos de las dependencias, las dependencias y las cuentas

            try
            {
                //Construir la tabla de los grupos de las dependencias y las cuentas

                //Construir la estructura de la tabla
                Dt_Resultado.Columns.Add(Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID, typeof(string));
                Dt_Resultado.Columns.Add("Grupo_Dependencia", typeof(string));
                Dt_Resultado.Columns.Add(Cat_Dependencias.Campo_Dependencia_ID, typeof(string));
                Dt_Resultado.Columns.Add("Dependencia", typeof(string));
                Dt_Resultado.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(string));
                Dt_Resultado.Columns.Add("Descripcion_Cuenta", typeof(string));
                Dt_Resultado.Columns.Add("Enero", typeof(double));
                Dt_Resultado.Columns.Add("Febrero", typeof(double));
                Dt_Resultado.Columns.Add("Marzo", typeof(double));
                Dt_Resultado.Columns.Add("Abril", typeof(double));
                Dt_Resultado.Columns.Add("Mayo", typeof(double));
                Dt_Resultado.Columns.Add("Junio", typeof(double));
                Dt_Resultado.Columns.Add("Julio", typeof(double));
                Dt_Resultado.Columns.Add("Agosto", typeof(double));
                Dt_Resultado.Columns.Add("Septiembre", typeof(double));
                Dt_Resultado.Columns.Add("Octubre", typeof(double));
                Dt_Resultado.Columns.Add("Noviembre", typeof(double));
                Dt_Resultado.Columns.Add("Diciembre", typeof(double));
                Dt_Resultado.Columns.Add("Total", typeof(double));
                Dt_Resultado.Columns.Add("Renglon_Grupo", typeof(string));
                Dt_Resultado.Columns.Add("Renglon_Dependencia", typeof(string));

                //Asignar consulta para los grupos de las dependencias
                Mi_SQL = "SELECT " + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + ", " + Cat_Grupos_Dependencias.Campo_Nombre + " AS Grupo_Dependencia " +
                    "FROM " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + " ";

                //Verificar si hay un grupo de dependencias seleccionado
                if (string.IsNullOrEmpty(Datos.P_Grupo_Dependencia_ID) == false)
                {
                    Mi_SQL += "WHERE " + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + " = '" + Datos.P_Grupo_Dependencia_ID + "' ";
                }

                //Ejecutar consulta
                Dt_Grupos_Dependencias = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Verificar si la consulta arrojo resultados
                if (Dt_Grupos_Dependencias.Rows.Count > 0)
                {
                    //Instanciar renglon para los totales del grupo e la dependencia
                    Renglon = Dt_Resultado.NewRow();

                    //Llenar el renglon
                    Renglon[Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID] = Dt_Grupos_Dependencias.Rows[Cont_Grupos_Dependencias][Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID].ToString().Trim();
                    Renglon["Grupo_Dependencia"] = Dt_Grupos_Dependencias.Rows[Cont_Grupos_Dependencias]["Grupo_Dependencia"].ToString().Trim();
                    Renglon[Cat_Dependencias.Campo_Dependencia_ID] = "";
                    Renglon["Dependencia"] = "";
                    Renglon["Descripcion_Cuenta"] = "";
                    Renglon["Renglon_Grupo"] = "SI";
                    Renglon["Renglon_Dependencia"] = "NO";

                    //Colocar los valores de los meses en cero
                    Renglon["Enero"] = 0;
                    Renglon["Febrero"] = 0;
                    Renglon["Marzo"] = 0;
                    Renglon["Abril"] = 0;
                    Renglon["Mayo"] = 0;
                    Renglon["Junio"] = 0;
                    Renglon["Julio"] = 0;
                    Renglon["Agosto"] = 0;
                    Renglon["Septiembre"] = 0;
                    Renglon["Octubre"] = 0;
                    Renglon["Noviembre"] = 0;
                    Renglon["Diciembre"] = 0;

                    //Verificar si hay mes inicial
                    if (Datos.P_Mes_Inicial > 0)
                    {
                        //Verificar si hay mes final
                        if (Datos.P_Mes_Final == 0)
                        {
                            Datos.P_Mes_Final = 12;
                        }
                    }
                    else
                    {
                        Datos.P_Mes_Inicial = 1;
                        Datos.P_Mes_Final = 12;
                    }
                    
                    //Ciclo para el llenado de los meses
                    for (Cont_Meses = Datos.P_Mes_Inicial; Cont_Meses < Datos.P_Mes_Final + 1; Cont_Meses++)
                    {
                       // Construir_Tabla_Grupos_Dependencias_Cuentas(Dt_Grupos_Dependencias.Rows[Cont_Grupos_Dependencias][Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID].ToString().Trim(), Datos.P_Dependencia_ID);
                    }
                    //Colocar el renglon en la tabla
                    Dt_Resultado.Rows.Add(Renglon);
                    Dt_Resultado.AcceptChanges();

                    //Ciclo para el barrido de la tabla de los grupos de las dependencias
                    for (Cont_Grupos_Dependencias = 0; Cont_Grupos_Dependencias < Dt_Grupos_Dependencias.Rows.Count; Cont_Grupos_Dependencias++)
                    {
                        //Consulta para las dependencias del grupo de las dependencias
                        Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + ", " + Cat_Dependencias.Campo_Nombre + " AS Dependencia " +
                            "FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " ";

                        //Verificar si se 
                        //Ciclo para el barrido de la tabla
                        for (Cont_Elementos = 0; Cont_Elementos < Dt_Aux.Rows.Count; Cont_Elementos++)
                        {
                            //Instanciar renglon
                            Renglon = Dt_Resultado.NewRow();

                            //Llenar el renglon

                            //Colocar el renglon en la tabla
                            Dt_Resultado.Rows.Add(Renglon);
                            Dt_Resultado.AcceptChanges();
                        }
                    }
                }
                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
        public static DataTable Consultar_Cuentas_Por_Grupo_y_Gernecia(Cls_Rpt_Con_Gastos_Gerencias_Negocio Datos)
        {
            DataTable Dt_Resultado = new DataTable();
            String Sql = string.Empty;
            try
            {
                Sql = "SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "," + Cat_Con_Cuentas_Contables.Campo_Cuenta + "," + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                Sql = Sql + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE'5________" + Datos.P_Clave_Grupo_Dependencia + Datos.P_Clave_Dependencia + "%' order by "+Cat_Con_Cuentas_Contables.Campo_Cuenta;
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Sql).Tables[0];
                return Dt_Resultado;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message,Ex);
            }
        }

        public static DataTable Consultar_Cuenta_Padre(Cls_Rpt_Con_Gastos_Gerencias_Negocio Datos)
        {
            DataTable Dt_Resultado = new DataTable();
            String Sql = string.Empty;
            try
            {
                Sql = "SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "," + Cat_Con_Cuentas_Contables.Campo_Cuenta + "," + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                Sql = Sql + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE'" + Datos.P_Cuenta_Padre + "%' order by "+Cat_Con_Cuentas_Contables.Campo_Cuenta;
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Sql).Tables[0];
                return Dt_Resultado;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message,Ex);
            }
        }

        public static Decimal Consultar_Monto_Por_Mes(Cls_Rpt_Con_Gastos_Gerencias_Negocio Datos)
        {
            Object Resultado = new Object();
            String Sql = string.Empty;
            try
            {
                Sql = "SELECT SUM(" + Ope_Con_Cierre_Mensual.Campo_Diferencia + ") FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + " WHERE " + Ope_Con_Cierre_Mensual.Campo_Mes_Anio + "='" + Datos.P_Mes_Anio + "'";
                Sql = Sql + " AND " + Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " IN (" + Datos.P_Cuentas_Hijo + ")";
                Resultado = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Sql);
                if (String.IsNullOrEmpty(Resultado.ToString()))
                {
                    Resultado = 0.00;
                }
                return Convert.ToDecimal(Resultado.ToString());
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message,Ex);
            }
        }
        
    }
}