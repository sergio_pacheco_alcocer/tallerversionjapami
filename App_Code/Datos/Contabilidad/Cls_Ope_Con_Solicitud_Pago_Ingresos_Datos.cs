﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Solicitud_Pagos_Ingresos.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Sessiones;
using JAPAMI.Polizas.Datos;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.Cuentas_Contables.Negocio;

namespace JAPAMI.Solicitud_Pagos_Ingresos.Datos
{
    public class Cls_Ope_Con_Solicitud_Pago_Ingresos_Datos
    {
        #region (Métodos Operación)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Solicitud_Pago
        /// DESCRIPCION : 1.Consulta el último No dado de alta para poder ingresar el siguiente
        ///               2. Da de Alta La Solictud de Pago en la BD con los datos 
        ///                  proporcionados por elusuario
        /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 17-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Alta_Solicitud_Pago(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            StringBuilder Mi_SQL_2 = new StringBuilder();
            String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos
            ///Object No_Solicitud_Pago;                        //Obtiene el último número de registro que fue dado de alta en la base de datos
            Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
            Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
            Object Saldo;                                    //Obtiene el saldo de la cuenta contable     
            Object Total_Presupuesto = null;
            //Double Importe;
            ////DataTable Dt_Reserva = new DataTable();           //Obtiene la reserva a la que se pertenece la solicitud
            ////DataTable Dt_Reserva_Detalles = new DataTable();     // se obtiene los detalles de la partida 
            DataTable Dt_Partidas = new DataTable();     // se obtiene los detalles de la partida 
            //DataTable Dt_solicitud_Detalles= new DataTable();          
            SqlCommand Comando_SQL = new SqlCommand(); //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL;               //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            //DataRow Fila;
            if (Conexion_Base.State != ConnectionState.Open)
            {
                Conexion_Base.Open(); //Abre la conexión a la base de datos            
            }
            Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
            Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
            Comando_SQL.Transaction = Transaccion_SQL;
            try
            {
                //Consulta para la obtención del último ID dado de alta 
                Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')");
                Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00002'");
                Mi_SQL.Append(" ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                No_Poliza = Comando_SQL.ExecuteScalar();
                //Valida si el ID es nulo para asignarle automaticamente el primer registro
                if (Convert.IsDBNull(No_Poliza))
                {
                    No_Poliza = "0000000001";
                }
                //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                else
                {
                    No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                }
                Mi_SQL.Length = 0;
                //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                Mi_SQL.Append(" (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", ");
                Mi_SQL.Append(Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + ")");
                Mi_SQL.Append(" VALUES ('" + No_Poliza + "', '00002', '" + Mes_Anio + "',");
                Mi_SQL.Append(" GETDATE(),");
                Mi_SQL.Append(" '" + Datos.P_Concepto + "', " + Datos.P_Monto + ", " + Datos.P_Monto + "," + Datos.P_Partida + ", ");
                Mi_SQL.Append("'" + Datos.P_Nombre_Usuario + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', ");
                Mi_SQL.Append("'" + Cls_Sessiones.Empleado_ID + "')");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                //Da de alta los detalles de la póliza
                foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                {
                    Mi_SQL.Length = 0;
                    //consulta el saldo de la cuenta contable
                    Mi_SQL.Append("SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - ");
                    Mi_SQL.Append(" ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'");
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                    Saldo = Comando_SQL.ExecuteScalar();
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                    }

                    Mi_SQL.Length = 0;
                    //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                    Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                    Consecutivo = Comando_SQL.ExecuteScalar();

                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(Consecutivo))
                    {
                        Consecutivo = "1";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                    }
                    Mi_SQL.Length = 0;
                    //Inserta el registro del detalle de la póliza en la base de datos
                    Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Mi_SQL.Append("(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Consecutivo + ")");
                    Mi_SQL.Append(" VALUES('" + No_Poliza + "', '00002', '" + Mes_Anio + "', ");
                    Mi_SQL.Append(Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + ",");
                    Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "',");
                    Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString() + "', ");
                    Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ")");
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Comando_SQL.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos            
                }
                Mi_SQL.Length = 0;
                // se agrega el no_poliza y el mes_ano a la solicitud de pago
                Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " ='" + No_Poliza + "', ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00002', ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos
                Transaccion_SQL.Commit();             //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos                
                //Optenemos el total de la reserva con los impuestos que se disminuyeron
                Mi_SQL_2.Append("SELECT " + Ope_Con_Polizas.Campo_Total_Debe);
                Mi_SQL_2.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                Mi_SQL_2.Append(" WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " ='" + No_Poliza + "'");
                Mi_SQL_2.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " ='00002'");
                Mi_SQL_2.Append(" AND " + Ope_Con_Polizas.Campo_Mes_Ano + " ='" + Mes_Anio + "'");
                Total_Presupuesto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL_2.ToString());
                Dt_Partidas = Consultar_detalles_partidas_de_solicitud(null,Datos.P_No_Solicitud_Pago);
                Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza);
                Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(Datos.P_No_Reserva), "DEVENGADO", "COMPROMETIDO", Dt_Partidas); //Actualiza el impote de la partida presupuestal
                Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "DEVENGADO", "COMPROMETIDO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(No_Poliza), "00002", Mes_Anio, "1"); //Agrega el historial del movimiento de la partida presupuestal
                Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(Datos.P_No_Reserva), "EJERCIDO", "DEVENGADO", Dt_Partidas); //Actualiza el impote de la partida presupuestal
                Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "EJERCIDO", "DEVENGADO", Convert.ToDouble(Total_Presupuesto), "", "", "", ""); //Agrega el historial del movimiento de la partida presupuestal
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_detalles_partidas_de_solicitud
        /// DESCRIPCION : 1.Consulta el último No dado de alta para poder ingresar el siguiente
        ///               2. Da de Alta La Solictud de Pago en la BD con los datos 
        ///                  proporcionados por elusuario
        /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
        /// CREO        : Sergio Manuel gallardo Andrade
        /// FECHA_CREO  : 27-02-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_detalles_partidas_de_solicitud(SqlCommand P_Cmmd, String no_solicitud)
        {
            StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos         
            Object No_Reserva;
            Double Importe;
            Double ISR;
            Double CEDULAR;
            Double IVA;
            Double Cap;
            Double Divo;
            Double Sefupu;
            Double Icic;
            Double Rapce;
            Double LabCC;
            Double OBS;
            Double EstPro;
            Double Anticipo;
            DataTable Dt_Reserva = new DataTable();           //Obtiene la reserva a la que se pertenece la solicitud
            DataTable Dt_Reserva_Detalles = new DataTable();     // se obtiene los detalles de la partida 
            DataTable Dt_Partidas = new DataTable();     // se obtiene los detalles de la partida 
            DataTable Dt_solicitud_Detalles = new DataTable();
            DataRow Fila;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Length = 0;
                Mi_SQL.Append("SELECT No_Reserva FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + no_solicitud + "'");
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                No_Reserva = Cmmd.ExecuteScalar();
                            
                //No_Reserva = OracleHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Mi_SQL.Length = 0;
                Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva);
                Mi_SQL.Append(", CAP, DIVO, SEFUPU, ICIC, RAPCE, Lab_Control_C, OBS, Est_Y_Proy, Amortizado");
                Mi_SQL.Append(", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR + " FROM " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + " = '" + no_solicitud + "'");
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                Dt_solicitud_Detalles = Ds_Oracle.Tables[0];
                                
                //Dt_solicitud_Detalles = OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                //consultar la reserva
                Ds_Oracle = new DataSet();
                Mi_SQL.Length = 0;
                Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + No_Reserva + "'");
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                Dt_Reserva = Ds_Oracle.Tables[0];
                //Dt_Reserva = OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                Mi_SQL.Length = 0;

                Ds_Oracle = new DataSet();
                // consultar los detalles de la reserva
                Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = '" + Dt_Reserva.Rows[0]["No_Reserva"].ToString() + "'");
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                Dt_Reserva_Detalles = Ds_Oracle.Tables[0];
                //Dt_Reserva_Detalles = OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                if (Dt_Partidas != null)
                {
                    Dt_Partidas.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                    Dt_Partidas.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                    Dt_Partidas.Columns.Add("ANIO", System.Type.GetType("System.String"));
                    Dt_Partidas.Columns.Add("IVA", System.Type.GetType("System.String"));
                    foreach (DataRow Registro in Dt_Reserva_Detalles.Rows)
                    {
                        Fila = Dt_Partidas.NewRow();
                        Fila["DEPENDENCIA_ID"] = Dt_Reserva.Rows[0]["DEPENDENCIA_ID"].ToString();
                        Fila["FUENTE_FINANCIAMIENTO_ID"] = Registro["FTE_FINANCIAMIENTO_ID"].ToString();
                        Fila["PROGRAMA_ID"] = Registro["PROYECTO_PROGRAMA_ID"].ToString();
                        Importe = 0;
                        ISR = 0;
                        Cap = 0;
                        Divo = 0;
                        Sefupu = 0;
                        IVA = 0;
                        Icic = 0;
                        Rapce = 0;
                        LabCC = 0;
                        OBS = 0;
                        EstPro = 0;
                        Anticipo = 0;
                        CEDULAR = 0;
                        foreach (DataRow Registro_2 in Dt_solicitud_Detalles.Rows)
                        {
                            if (Registro[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString() == Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString())
                            {
                                Importe = Importe + Convert.ToDouble(Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura].ToString());
                                if (!String.IsNullOrEmpty(Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString()))
                                {
                                    ISR = ISR + Convert.ToDouble(Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString());
                                    CEDULAR = CEDULAR + Convert.ToDouble(Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString());
                                }
                                if (!String.IsNullOrEmpty(Registro_2["CAP"].ToString()))
                                {
                                    Cap = Cap + Convert.ToDouble(Registro_2["CAP"].ToString());
                                    Divo = Divo + Convert.ToDouble(Registro_2["DIVO"].ToString());
                                    Sefupu = Sefupu + Convert.ToDouble(Registro_2["SEFUPU"].ToString());
                                    Icic = Icic + Convert.ToDouble(Registro_2["ICIC"].ToString());
                                    Rapce = Rapce + Convert.ToDouble(Registro_2["RAPCE"].ToString());
                                    LabCC = LabCC + Convert.ToDouble(Registro_2["Lab_Control_C"].ToString());
                                    OBS = OBS + Convert.ToDouble(Registro_2["OBS"].ToString());
                                    EstPro = EstPro + Convert.ToDouble(Registro_2["Est_Y_Proy"].ToString());
                                    Anticipo = Anticipo + Convert.ToDouble(Registro_2["Amortizado"].ToString());
                                }
                                IVA = IVA + Convert.ToDouble(Registro_2[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString());
                            }
                        }

                        Fila["PARTIDA_ID"] = Registro[Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString();
                        Fila["IMPORTE"] = Importe + ISR + CEDULAR + Anticipo + IVA;
                        Fila["ANIO"] = Dt_Reserva.Rows[0]["ANIO"].ToString();
                        Fila["IVA"] = IVA;
                        Dt_Partidas.Rows.Add(Fila);
                        Dt_Partidas.AcceptChanges();
                    }
                }
                return Dt_Partidas;
            }
            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Solicitud_Pago_Sin_poliza
        /// DESCRIPCION : 1.Consulta el último No dado de alta para poder ingresar el siguiente
        ///               2. Da de Alta La Solictud de Pago en la BD con los datos 
        ///                  proporcionados por elusuario
        /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 21-Diciembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static String Alta_Solicitud_Pago_Sin_Poliza(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            object No_Reserva;
            StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos
            Object No_Solicitud_Pago;                        //Obtiene el último número de registro que fue dado de alta en la base de datos
            String Directorio_Destino;
            String Descuento = "";
            String Regreso = "";
            object Total_Presupuesto;
            int Afectacion_Presupuestal = 0;
            String Resultado = "SI";
            DataTable Dt_Partidas = new DataTable();
            Object Contrato = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            String No_Poliza = "";
            String Tipo_Poliza = "";
            String Fecha_Poliza = "";
            String Temporal="";
            String Mes_Anio = "";
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "),'0000000000')");
                Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                //No_Solicitud_Pago = OracleHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                No_Solicitud_Pago = Cmmd.ExecuteScalar();
                if (Convert.IsDBNull(No_Solicitud_Pago))
                {
                    Datos.P_No_Solicitud_Pago = "0000000001";
                }
                else
                {
                    Datos.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud_Pago) + 1);
                }
                
                //Consulta el último No que fue dato de alta en la base de datos
                Mi_SQL.Length = 0;

                //Inserta un nuevo registro en la base de datos con los datos obtenidos por el usuario
                Mi_SQL.Append("INSERT INTO " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" (" + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", " + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", " + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", " + Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                //Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + ", ");
                if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID))
                {
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + ", ");
                }
                if (!String.IsNullOrEmpty(Datos.P_Empleado_ID))
                {
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + ", ");
                }
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ")");
                Mi_SQL.Append(" VALUES ('" + Datos.P_No_Solicitud_Pago + "', '" + Datos.P_Tipo_Solicitud_Pago_ID + "'," + Convert.ToInt32(Datos.P_No_Reserva) + ",");
                Mi_SQL.Append(" '" + Datos.P_Concepto + "', GETDATE(), " + Datos.P_Monto + ", '");// + Datos.P_No_Factura + "',");
                //Mi_SQL.Append(" TO_DATE('" + Datos.P_Fecha_Factura + "', 'MM/DD/YY'), '");
                if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID))
                {
                    Mi_SQL.Append(Datos.P_Proveedor_ID + "', '");
                }
                if (!String.IsNullOrEmpty(Datos.P_Empleado_ID))
                {
                    Mi_SQL.Append(Datos.P_Empleado_ID + "', '");
                }
                Mi_SQL.Append(Datos.P_Estatus + "',");
                Mi_SQL.Append(" '" + Datos.P_Nombre_Usuario + "', GETDATE())");
                Cmmd.CommandText = Mi_SQL.ToString();
                Cmmd.ExecuteNonQuery();
                //Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                //Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                Directorio_Destino = HttpContext.Current.Server.MapPath("~") + "\\Archivo_Solicitud_Pagos\\" + Datos.P_No_Solicitud_Pago + "\\";
                if (System.IO.Directory.Exists(Directorio_Destino) == false)
                {
                    System.IO.Directory.CreateDirectory(Directorio_Destino);
                }
                foreach (DataRow Renglon in Datos.P_Dt_Detalles_Solicitud.Rows)
                {
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("INSERT INTO " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                    Mi_SQL.Append(" (" + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Factura
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID +
                                          ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Documento + ",");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Archivo
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Ruta
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID +
                                        ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID +
                                          ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Nombre_Proveedor_Factura + ",");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_RFC
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_CURP
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion
                                        + ", CAP, DIVO, SEFUPU, RAPCE, ICIC, OBS, Est_Y_Proy, Lab_Control_C, Suma, Alcance_Neto, Total_Est, " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR +
                                            ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula + ", Amortizado,");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Usuario_Creo + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Creo + ")");
                    Mi_SQL.Append(" VALUES ('" + Datos.P_No_Solicitud_Pago + "', '" + Renglon["No_Documento"].ToString() + "', '" + Renglon["Fecha_Documento"].ToString() + "',");
                    Mi_SQL.Append(" " + Renglon["SubTotal"].ToString().Replace(",", "") + ",'" +
                                        Renglon["Partida_Id"].ToString() +
                                        "','" + Datos.P_Tipo_Documento + "','" +
                                        Renglon["Archivo"].ToString() + "','");
                    if (Renglon["Archivo"].ToString() != null && Renglon["Archivo"].ToString() != "")
                    {
                        Mi_SQL.Append(Directorio_Destino + Renglon["Archivo"].ToString() + "','");

                    }
                    else
                    {
                        Mi_SQL.Append("','");
                    }
                    Mi_SQL.Append(Renglon["Proyecto_Programa_Id"].ToString() + "'," +
                        Renglon["Iva"].ToString() + ", " +
                        "'" + Renglon["Fte_Financiamiento_Id"].ToString() + "','" +
                         Renglon["Nombre_Proveedor_Fact"].ToString() + "','" +
                         Renglon["RFC"].ToString() +
                        "','" + Renglon["CURP"].ToString() + "','"+
                        Renglon["Operacion"].ToString() + "'," +
                          Renglon["Divo"].ToString() + "," +
                          Renglon["Cap"].ToString() + "," +
                          Renglon["Sefupu"].ToString() + "," +
                          Renglon["Rapce"].ToString() + "," +
                          Renglon["Icic"].ToString() + "," +
                          Renglon["Obs"].ToString() + "," +
                          Renglon["EstyProy"].ToString() + "," +
                          Renglon["Lab_Control"].ToString() + "," +
                          Renglon["Suma"].ToString() + "," +
                          Renglon["Alcance_Neto"].ToString() + "," +
                          Renglon["Total"].ToString() + "," +
                          Renglon["Retencion_ISR"].ToString() + "," +
                          Renglon["Retencion_Cedular"].ToString() + "," +
                          Renglon["Amortizacion"].ToString() + ",'" +
                        Datos.P_Nombre_Usuario + "', SYSDATE )");// + Datos.P_No_Factura + "',");
                        Cmmd.CommandText = Mi_SQL.ToString();
                        Cmmd.ExecuteNonQuery();
                        //Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        //Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                        if (Renglon["Archivo"].ToString() != null && Renglon["Archivo"].ToString() != "")
                        {
                            System.IO.File.Copy(Renglon["Ruta"].ToString(), Directorio_Destino + Renglon["Archivo"].ToString());
                            System.IO.File.Delete(Renglon["Ruta"].ToString());
                        }
                    }
                    Directorio_Destino = HttpContext.Current.Server.MapPath("~") + "\\Archivo_Solicitud_Pagos\\Temporal";
                    if (System.IO.Directory.Exists(Directorio_Destino) == true)
                    {
                        String[] archivos = System.IO.Directory.GetFiles(Directorio_Destino);
                        int a;
                        for (a = 0; a < archivos.Length; a++)
                        {
                            System.IO.File.Delete(a.ToString());
                        }
                    }
                    DataTable Dt_Detalle = new DataTable();
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT detalles.*, partida.Descripcion FROM " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " detalles," + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " partida");
                    Mi_SQL.Append(" WHERE detalles." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID + " = partida." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " AND " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Dt_Oracle.SelectCommand = Cmmd;
                    Dt_Oracle.Fill(Ds_Oracle);
                    Dt_Detalle = Ds_Oracle.Tables[0];
                                
                //DataTable Dt_Detalle = OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    String Operacion = "";
                if (Dt_Detalle.Rows.Count > 0)
                    {
                        Operacion = Dt_Detalle.Rows[0]["TIPO_OPERACION"].ToString().Trim();
                    }
                if(!String.IsNullOrEmpty(Operacion))
                {
                    if (Operacion != "ANTICIPO")
                    {
                        Mi_SQL.Length = 0;
                        //Actualiza el saldo de la reserva
                        Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                        Mi_SQL.Append(" SET " + Ope_Psp_Reservas.Campo_Saldo + " = " + Ope_Psp_Reservas.Campo_Saldo + " - " + Datos.P_Monto + ", ");
                        Mi_SQL.Append(Ope_Psp_Reservas.Campo_Beneficiario + " = '" + Datos.P_Beneficiario + "'");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva));
                        Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                        // OracleHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        //obtenemos las partidas
                        Dt_Partidas = Consultar_detalles_partidas_de_solicitud(Cmmd, Datos.P_No_Solicitud_Pago);
                        foreach (DataRow Renglon in Dt_Partidas.Rows)
                        {
                            Mi_SQL.Length = 0;
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                            if (Operacion == "ESTIMACION")
                            {
                                Descuento = Convert.ToString(Convert.ToDouble(Renglon["Importe"].ToString()) - Convert.ToDouble(Renglon["IVA"].ToString()));
                                Mi_SQL.Append(" SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " - " + Descuento + "");
                            }
                            else
                            {
                                Mi_SQL.Append(" SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " - " + Renglon["Importe"].ToString() + "");
                                Descuento = "0";
                            }
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva) + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = " + Renglon["PARTIDA_ID"].ToString() + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " = " + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString() + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " = " + Renglon["PROGRAMA_ID"].ToString());
                            //OracleHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                            Cmmd.CommandText = Mi_SQL.ToString();
                            Cmmd.ExecuteNonQuery();
                        }
                        //Aumenta el avance fisico
                        if (!String.IsNullOrEmpty(Datos.P_Avance_Fisico))
                        {
                            Mi_SQL.Length = 0;
                            Mi_SQL.Append("SELECT " + Cat_Con_Contratos.Campo_Contrato_ID + " FROM " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos);
                            Mi_SQL.Append(" WHERE " + Cat_Con_Contratos.Campo_No_Reserva + "='" + Datos.P_No_Reserva + "'");
                            Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                            Contrato = Cmmd.ExecuteScalar();

                            //Contrato = OracleHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                            if (!Convert.IsDBNull(Contrato))
                            {
                                Mi_SQL.Length = 0;
                                Mi_SQL.Append("UPDATE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + " SET ");
                                Mi_SQL.Append(Cat_Con_Contratos.Campo_Avance_Fisico + "= " + Datos.P_Avance_Fisico);
                                Mi_SQL.Append(" WHERE " + Cat_Con_Contratos.Campo_Contrato_ID + "='" + Contrato + "'");
                                Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                                //OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                            }
                        }
                        Regreso = Alta_Poliza_Inversiones(Datos, Operacion);
                    }
                    else
                    {
                       Regreso=Alta_Poliza_Activo(Datos);
                    }
                }
                if (Regreso != "Falta")
                {

                    No_Poliza = Regreso.Substring(0, Regreso.IndexOf(","));
                    Temporal = Regreso.Substring(Regreso.IndexOf(",") + 1);
                    Tipo_Poliza = Temporal.Substring(0, Temporal.IndexOf(","));
                    Temporal = Temporal.Substring(Temporal.IndexOf(",") + 1);
                    Fecha_Poliza = Temporal.Substring(0, Temporal.IndexOf(","));
                    Temporal = Temporal.Substring(Temporal.IndexOf(",") + 1);
                    Mes_Anio = Temporal;
                    // se agrega el no_poliza y el mes_ano a la solicitud de pago
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " ='" + No_Poliza + "', ");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '" + Tipo_Poliza + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT " + Ope_Con_Polizas.Campo_Total_Debe);
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " ='" + No_Poliza + "'");
                    Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " ='"+Tipo_Poliza+"'");
                    Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Mes_Ano + " ='" + Mes_Anio + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                    Total_Presupuesto = Cmmd.ExecuteScalar();

                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos.Campo_No_Reserva);
                    Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " ='" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                    No_Reserva = Cmmd.ExecuteScalar();
                    
                    // Total_Presupuesto = OracleHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    Dt_Partidas = Consultar_detalles_partidas_de_solicitud(Cmmd,Datos.P_No_Solicitud_Pago);
                    if (Operacion != "ANTICIPO")
                    {
                        Afectacion_Presupuestal=Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DEVENGADO", "COMPROMETIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                        if (Afectacion_Presupuestal > 0)
                        {
                            Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(No_Reserva), "DEVENGADO", "COMPROMETIDO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(No_Poliza), Tipo_Poliza, Mes_Anio, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                        }
                        else
                        {
                            Resultado = "NO";
                        }
                        //Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(No_Reserva), "EJERCIDO", "DEVENGADO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                        //Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(No_Reserva), "EJERCIDO", "DEVENGADO", Convert.ToDouble(Total_Presupuesto), "", "", "", "", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                    }
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    return Datos.P_No_Solicitud_Pago +","+ Regreso+ "-"+Resultado;
                }
                else
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    return "Falta";
                }
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
            //catch (OracleException Ex)
            //{
            //    throw new Exception("Error: " + Ex.Message);
            //}
            //catch (DBConcurrencyException Ex)
            //{
            //    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            //}
            //catch (Exception Ex)
            //{
            //    throw new Exception("Error: " + Ex.Message);
            //}
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Poliza_Activo
        ///DESCRIPCIÓN: Se da de alta la poliza de inversiones la devengado
        ///PARÁMETROS:     
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 17/Noviembre/2012 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Alta_Poliza_Inversiones(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos, String Operacion)
        {
            Cls_Ope_Con_Polizas_Negocio Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión hacia la capa de negocios para envio de los datos a dar de alta
            DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            DataTable Dt_Datos_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Impuestos = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
            Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable Dt_Cuentas = new DataTable();
            DataTable Dt_C_Proveedor = new DataTable();
            DataTable Dt_Anticipo = new DataTable();
            DataTable Dt_Estimacion = new DataTable();
            DataTable Dt_Honorarios = new DataTable();
            DataTable Dt_Arrendamientos = new DataTable();
            DataRow Registro_Estimacion;
            DataRow Registro_Honorarios;
            DataRow Registro_Arrendamiento;
            Double Tot_Cap = 0.00;
            Double Tot_Divo = 0.00;
            Double Tot_Sefupu = 0.00;
            Double Tot_Icic = 0.00;
            Double Tot_Rapce = 0.00;
            Double Tot_LabCC = 0.00;
            Double Tot_OBS = 0.00;
            Double Tot_EstPro = 0.00;
            Double Total_Partidas = 0.00;
            Double Tot_Anticipo = 0.00;
            String Cuenta_Anticipo = "";
            String Cuenta_Cap = "";
            String Cuenta_Divo = "";
            String Cuenta_Sefupu = "";
            String Cuenta_Icic = "";
            String Cuenta_Rapce = "";
            String Cuenta_LabCC = "";
            String Cuenta_OBS = "";
            String Cuenta_EstPro = "";
            Double Total_ISR_Honorarios = 0;
            Double Total_Cedular_Honorarios = 0;
            Double Total_ISR_Arrendamientos = 0;
            Double Total_Cedular_Arrendamientos = 0;
            String Cuenta_Contable_ID_Proveedor = String.Empty;
            String Cuenta_Contrable_ID_Anticipo = String.Empty;
            String Cuenta_ISR = "0";
            String Cuenta_Cedular = "0";
            Double Monto_Solicitud = 0;
            String Cuenta_Contable_reserva = "";
            Double Monto_Total = 0;
            String No_Reserva;
            int partida = 0;
            String Estatus="";
            String Concepto_Solicitud = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            Boolean CUENTA_RESERVA = true;
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                //Agregar las partidas de los impuestos de isr y de retencion de iva si tiene honorarios la solicitud de pago
                //se crean las columnas de los datatable de honorarios y de arrendamientos
                if (Dt_Estimacion.Rows.Count == 0)
                {
                    Dt_Estimacion.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                    Dt_Estimacion.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                    Dt_Estimacion.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                    Dt_Estimacion.Columns.Add("CAP", typeof(System.String));
                    Dt_Estimacion.Columns.Add("DIVO", typeof(System.String));
                    Dt_Estimacion.Columns.Add("SEFUPU", typeof(System.String));
                    Dt_Estimacion.Columns.Add("RAPCE", typeof(System.String));
                    Dt_Estimacion.Columns.Add("OBS", typeof(System.String));
                    Dt_Estimacion.Columns.Add("Est_Y_Proy", typeof(System.String));
                    Dt_Estimacion.Columns.Add("ICIC", typeof(System.String));
                    Dt_Estimacion.Columns.Add("Lab_Control_C", typeof(System.String));
                    Dt_Estimacion.Columns.Add("Amortizado", typeof(System.String));
                }
                if (Dt_Honorarios.Rows.Count == 0)
                {
                    Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                    Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                    Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                    Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula, typeof(System.Double));
                    Dt_Honorarios.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR, typeof(System.Double));
                }
                if (Dt_Arrendamientos.Rows.Count == 0)
                {
                    Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID, typeof(System.String));
                    Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID, typeof(System.String));
                    Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                    Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula, typeof(System.Double));
                    Dt_Arrendamientos.Columns.Add(Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR, typeof(System.Double));
                }
                //Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = Datos.P_No_Solicitud_Pago;
                Dt_Datos_Solicitud = Consulta_Detalles_Solicitud(Datos);
                if (Dt_Datos_Solicitud.Rows.Count > 0)
                {
                    //se recorre el dataset de los detalles para dividir los detalles por tipo de operacion 
                    foreach (DataRow fila in Dt_Datos_Solicitud.Rows)
                    {
                        if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "ESTIMACION")
                        {
                            Registro_Estimacion = Dt_Estimacion.NewRow();
                            Registro_Estimacion[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                            Registro_Estimacion[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                            Registro_Estimacion[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                            Registro_Estimacion["CAP"] = fila["CAP"].ToString().Trim();
                            Registro_Estimacion["DIVO"] = fila["DIVO"].ToString().Trim();
                            Registro_Estimacion["SEFUPU"] = fila["SEFUPU"].ToString().Trim();
                            Registro_Estimacion["RAPCE"] = fila["RAPCE"].ToString().Trim();
                            Registro_Estimacion["OBS"] = fila["OBS"].ToString().Trim();
                            Registro_Estimacion["Est_Y_Proy"] = fila["Est_Y_Proy"].ToString().Trim();
                            Registro_Estimacion["ICIC"] = fila["ICIC"].ToString().Trim();
                            Registro_Estimacion["Lab_Control_C"] = fila["Lab_Control_C"].ToString().Trim();
                            Registro_Estimacion["Amortizado"] = fila["Amortizado"].ToString().Trim();
                            Dt_Estimacion.Rows.Add(Registro_Estimacion); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Estimacion.AcceptChanges();
                        }
                        if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "HONORARIO")
                        {
                            Registro_Honorarios = Dt_Honorarios.NewRow();
                            Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                            Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                            Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                            Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim();
                            Registro_Honorarios[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim();
                            Dt_Honorarios.Rows.Add(Registro_Honorarios); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Honorarios.AcceptChanges();
                        }
                        if (fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString().Trim() == "ARRENDAMIENTO")
                        {
                            Registro_Arrendamiento = Dt_Arrendamientos.NewRow();
                            Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString().Trim();
                            Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString().Trim();
                            Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString().Trim();
                            Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim();
                            Registro_Arrendamiento[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR] = fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim();
                            Dt_Arrendamientos.Rows.Add(Registro_Arrendamiento); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Arrendamientos.AcceptChanges();
                        }
                    }
                    if (Dt_Estimacion.Rows.Count > 0)
                    {
                        foreach (DataRow fila in Dt_Estimacion.Rows)
                        {
                            Tot_Cap += Convert.ToDouble(String.IsNullOrEmpty(fila["CAP"].ToString()) ? "0" : fila["CAP"]);
                            Tot_Divo += Convert.ToDouble(String.IsNullOrEmpty(fila["DIVO"].ToString()) ? "0" : fila["DIVO"]);
                            Tot_Sefupu += Convert.ToDouble(String.IsNullOrEmpty(fila["SEFUPU"].ToString()) ? "0" : fila["SEFUPU"]);
                            Tot_Rapce += Convert.ToDouble(String.IsNullOrEmpty(fila["RAPCE"].ToString()) ? "0" : fila["RAPCE"]);
                            Tot_Icic += Convert.ToDouble(String.IsNullOrEmpty(fila["ICIC"].ToString()) ? "0" : fila["ICIC"]);
                            Tot_OBS += Convert.ToDouble(String.IsNullOrEmpty(fila["OBS"].ToString()) ? "0" : fila["OBS"]);
                            Tot_EstPro += Convert.ToDouble(String.IsNullOrEmpty(fila["Est_Y_Proy"].ToString()) ? "0" : fila["Est_Y_Proy"]);
                            Tot_LabCC += Convert.ToDouble(String.IsNullOrEmpty(fila["Lab_Control_C"].ToString()) ? "0" : fila["Lab_Control_C"]);
                            Tot_Anticipo += Convert.ToDouble(String.IsNullOrEmpty(fila["Amortizado"].ToString()) ? "0" : fila["Amortizado"]);
                        }
                    }
                    Total_ISR_Honorarios = 0;
                    Total_Cedular_Honorarios = 0;
                    if (Dt_Honorarios.Rows.Count > 0)
                    {
                        foreach (DataRow fila in Dt_Honorarios.Rows)
                        {
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                            {
                                Total_ISR_Honorarios = Total_ISR_Honorarios + Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                            }
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                            {
                                Total_Cedular_Honorarios = Total_Cedular_Honorarios + Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                            }

                        }
                    }
                    Total_ISR_Arrendamientos = 0;
                    Total_Cedular_Arrendamientos = 0;
                    if (Dt_Arrendamientos.Rows.Count > 0)
                    {
                        foreach (DataRow fila in Dt_Arrendamientos.Rows)
                        {
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim()) > 0)
                            {
                                Total_ISR_Arrendamientos = Total_ISR_Arrendamientos + Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString().Trim());
                            }
                            if (Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim()) > 0)
                            {
                                Total_Cedular_Arrendamientos = Total_Cedular_Arrendamientos + Convert.ToDouble(fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString().Trim());
                            }
                        }
                    }
                }
                //Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Datos.P_No_Solicitud_Pago;
                Dt_Datos_Polizas = Consulta_Datos_Solicitud_Pago(Datos);
                DataRow row; //Crea un nuevo registro a la tabla
                foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
                {
                    if (!String.IsNullOrEmpty(Registro["Monto_Partida"].ToString()))
                    {
                        partida = partida + 1;
                        Total_Partidas = Convert.ToDouble(Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString());
                        Monto_Solicitud = Convert.ToDouble(Registro["Monto_Partida"].ToString());
                        if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Contratista"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "C")
                        {
                            Cuenta_Contable_ID_Proveedor = Registro["Pro_Cuenta_Contratista"].ToString();
                        }

                        if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Anticipo"].ToString()))
                        {
                            Cuenta_Contrable_ID_Anticipo = Registro["Pro_Cuenta_Anticipo"].ToString();
                        }
                        if (Operacion == "AFECTACION")
                        {
                            Monto_Solicitud = Convert.ToDouble(Registro["Monto_Partida"].ToString()) + Convert.ToDouble(Registro["Monto_Armonizado"].ToString()) + Convert.ToDouble(Registro["Monto_Cedular"].ToString()) + Convert.ToDouble(Registro["Monto_ISR"].ToString()) + Convert.ToDouble(Registro["Monto_IVA"].ToString());
                            Monto_Total = Monto_Total + Monto_Solicitud;
                        }
                        else
                        {
                            Monto_Solicitud = Convert.ToDouble(Registro["Monto_Partida"].ToString()) + Convert.ToDouble(Registro["Monto_Armonizado"].ToString()) + Convert.ToDouble(Registro["Monto_Cedular"].ToString()) + Convert.ToDouble(Registro["Monto_ISR"].ToString());
                            Monto_Total = Monto_Total + Monto_Solicitud;
                        }
                        Concepto_Solicitud = Registro["Concepto"].ToString();
                        if (!String.IsNullOrEmpty(Registro["Cuenta_Contable_Reserva"].ToString()))
                        {
                            Cuenta_Contable_reserva = Registro["Cuenta_Contable_Reserva"].ToString();
                            No_Reserva = Registro["NO_RESERVA"].ToString();
                            //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                            if (Dt_Partidas_Polizas.Rows.Count == 0)
                            {
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                                Dt_Partidas_Polizas.Columns.Add("Beneficiario_ID", typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                            }
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_reserva;
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Monto_Solicitud;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                            row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                            row["Tipo_Beneficiario"] = "PROVEEDOR";
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                        else
                        {
                            CUENTA_RESERVA = false;
                        }
                    }
                }
                if (!String.IsNullOrEmpty(Cuenta_Contable_ID_Proveedor) && !String.IsNullOrEmpty(Cuenta_Contrable_ID_Anticipo) && CUENTA_RESERVA==true)
                {
                    //se consultan los detalles de la solicitud de pago
                    //Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = Datos.P_No_Solicitud_Pago;
                    Dt_Datos_Solicitud = Consulta_Detalles_Solicitud(Datos);
                    if (Dt_Datos_Solicitud.Rows.Count > 0)
                    {
                        row = Dt_Partidas_Polizas.NewRow();
                        partida = partida + 1;
                        //Agrega el abono del registro de la póliza
                        row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                        if (!String.IsNullOrEmpty(Cuenta_Contable_ID_Proveedor))
                        {
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_ID_Proveedor;
                        }
                        row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                        row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                        row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Partidas - (Tot_Cap + Tot_Divo + Tot_EstPro + Tot_Icic + Tot_LabCC + Tot_OBS + Tot_Rapce + Tot_Sefupu + Tot_Anticipo + Total_Cedular_Arrendamientos + Total_Cedular_Honorarios + Total_ISR_Arrendamientos + Total_ISR_Honorarios);
                        row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                        row["Tipo_Beneficiario"] = "PROVEEDOR";
                        Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Partidas_Polizas.AcceptChanges();
                        // se recorre el datatable de honorarios para realizar la sumatoria de los impuestos en las variables correspondientes 
                        //y asi obtener el total de los impuestos de cada tipo
                        if (Dt_Estimacion.Rows.Count > 0)
                        {
                            if (Tot_Cap > 0)
                            {
                                // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                                Dt_Cuentas = Consulta_Datos_Parametros(ref Cmmd);
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_Cap = Dt_Cuentas.Rows[0]["CTA_CAP"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_Cap))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Cap;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Tot_Cap;
                                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                            if (Tot_Divo > 0)
                            {
                                // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                                Dt_Cuentas = Consulta_Datos_Parametros(ref Cmmd);
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_Divo = Dt_Cuentas.Rows[0]["CTA_DIVO"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_Divo))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Divo;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Tot_Divo;
                                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                            if (Tot_Rapce > 0)
                            {
                                // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                                Dt_Cuentas = Consulta_Datos_Parametros(ref Cmmd);
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_Rapce = Dt_Cuentas.Rows[0]["CTA_RAPCE"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_Rapce))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Rapce;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Tot_Rapce;
                                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                            if (Tot_Sefupu > 0)
                            {
                                // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                                Dt_Cuentas = Consulta_Datos_Parametros(ref Cmmd);
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_Sefupu = Dt_Cuentas.Rows[0]["CTA_SEFUPU"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_Sefupu))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Sefupu;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Tot_Sefupu;
                                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                            if (Tot_Icic > 0)
                            {
                                // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                                Dt_Cuentas = Consulta_Datos_Parametros(ref Cmmd);
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_Icic = Dt_Cuentas.Rows[0]["CTA_ICIC"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_Icic))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Icic;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Tot_Icic;
                                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                            if (Tot_OBS > 0)
                            {
                                // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                                Dt_Cuentas = Consulta_Datos_Parametros(ref Cmmd);
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_OBS = Dt_Cuentas.Rows[0]["CTA_OBS"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_OBS))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_OBS;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Tot_OBS;
                                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                            if (Tot_EstPro > 0)
                            {
                                Dt_Cuentas = Consulta_Datos_Parametros(ref Cmmd);
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_EstPro = Dt_Cuentas.Rows[0]["CTA_Est_Y_Proy"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_EstPro))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_EstPro;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Tot_EstPro;
                                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                            if (Tot_LabCC > 0)
                            {
                                Dt_Cuentas = Consulta_Datos_Parametros(ref Cmmd);
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_LabCC = Dt_Cuentas.Rows[0]["CTA_Lab_Control_C"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_LabCC))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_LabCC;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Tot_LabCC;
                                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                            if (Tot_Anticipo > 0)
                            {
                                if (!String.IsNullOrEmpty(Cuenta_Contrable_ID_Anticipo))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contrable_ID_Anticipo;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Tot_Anticipo;
                                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                        }
                        if (Dt_Honorarios.Rows.Count > 0)
                        {
                            if (Total_ISR_Honorarios > 0)
                            {
                                Dt_Cuentas = Consulta_Datos_Parametros(ref Cmmd);
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_ISR = Dt_Cuentas.Rows[0]["CTA_ISR"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_ISR))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_ISR;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_ISR_Honorarios;
                                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                            if (Total_Cedular_Honorarios > 0)
                            {
                                // CUENTA IMPUESTO CEDULAR 1% HONORARIOS RET consultamos el id que tiene la cuenta en el sistema
                                Dt_Cuentas = Consulta_Datos_Parametros(ref Cmmd);
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_Cedular = Dt_Cuentas.Rows[0]["CTA_Cedular"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_Cedular))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Cedular;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Cedular_Honorarios;
                                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                        }
                        // se recorre el datatable de honorarios para realizar la sumatoria de los impuestos en las variables correspondientes 
                        //y asi obtener el total de los impuestos de cada tipo
                        if (Dt_Arrendamientos.Rows.Count > 0)
                        {
                            Cuenta_Cedular = "";
                            Cuenta_ISR = "";
                            if (Total_ISR_Arrendamientos > 0)
                            {
                                Dt_Cuentas = Consulta_Datos_Parametros(ref Cmmd);
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_ISR = Dt_Cuentas.Rows[0]["CTA_ISR"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_ISR))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_ISR;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_ISR_Arrendamientos;
                                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                            if (Total_Cedular_Arrendamientos > 0)
                            {
                                Dt_Cuentas = Consulta_Datos_Parametros(ref Cmmd);
                                if (Dt_Cuentas.Rows.Count > 0)
                                {
                                    Cuenta_Cedular = Dt_Cuentas.Rows[0]["CTA_Cedular"].ToString().Trim();
                                }
                                if (!String.IsNullOrEmpty(Cuenta_Cedular))
                                {
                                    partida = partida + 1;
                                    row = Dt_Partidas_Polizas.NewRow();
                                    //Agrega el cargo del registro de la póliza
                                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Cedular;
                                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto_Solicitud;
                                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Cedular_Arrendamientos;
                                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Partidas_Polizas.AcceptChanges();
                                }
                            }
                        }
                    }
                    Rs_Alta_Ope_Con_Polizas.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
                    Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = "00003";
                    Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", Convert.ToDateTime(DateTime.Now));
                    Rs_Alta_Ope_Con_Polizas.P_Fecha_Poliza = Convert.ToDateTime(DateTime.Now);
                    Rs_Alta_Ope_Con_Polizas.P_Concepto = Concepto_Solicitud;
                    Rs_Alta_Ope_Con_Polizas.P_Total_Debe = Monto_Total;
                    Rs_Alta_Ope_Con_Polizas.P_Total_Haber = Monto_Total;
                    Rs_Alta_Ope_Con_Polizas.P_No_Partida = Convert.ToInt32(partida);
                    Rs_Alta_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Rs_Alta_Ope_Con_Polizas.P_Dt_Detalles_Polizas = Dt_Partidas_Polizas;
                    Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID;
                    Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Autorizo = Cls_Sessiones.Empleado_ID;
                    Rs_Alta_Ope_Con_Polizas.P_Usuario_Valido = Datos.P_Usuario_Valido_Poliza;
                    Rs_Alta_Ope_Con_Polizas.P_Usuario_Autorizo = Datos.P_Usuario_Autorizo_Poliza;
                    Rs_Alta_Ope_Con_Polizas.P_Cmmd = Datos.P_Cmmd;
                    string[] Datos_Poliza = Rs_Alta_Ope_Con_Polizas.Alta_Poliza(); //Da de alta los datos de la Póliza proporcionados por el usuario en la BD
                    Estatus = Datos_Poliza[0] + "," + Datos_Poliza[1] + "," + Datos_Poliza[3] + "," + Datos_Poliza[2];
                }
                else
                {
                    Estatus = "Falta";
                    throw new Exception("Error:  La partida no contiene cuenta contable ligada");
                }
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Estatus;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_Parametros
        /// DESCRIPCION : Consulta los datos almacenados
        /// PARAMETROS  : 
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 21/Septiembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Datos_Parametros(ref SqlCommand P_Cmmd)
        {
            String Mi_SQL; //Variable para almacenar la instruccion de consulta a la BD.
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            DataTable Dt_Detalle = new DataTable();
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL = "SELECT PARAMETROS.*,EMPLEADO1." + Cat_Empleados.Campo_No_Empleado + " as usuario1, EMPLEADO2." + Cat_Empleados.Campo_No_Empleado + " as usuario2 FROM " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros;
                Mi_SQL = Mi_SQL + " PARAMETROS , " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO1, " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO2 WHERE " + Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv + "= EMPLEADO1.";
                Mi_SQL = Mi_SQL + Cat_Empleados.Campo_Empleado_ID + " AND " + Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv_2 + " = EMPLEADO2." + Cat_Empleados.Campo_Empleado_ID;
               // return OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                Dt_Detalle = Ds_Oracle.Tables[0];
                if (P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Dt_Detalle;
            }
            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Poliza_Activo
        ///DESCRIPCIÓN: Inserta la poliza del activo de la obra
        ///PARÁMETROS:     
        ///CREO: sergio manuel gallardo andrade
        ///FECHA_CREO: 17/noviembre/2012 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Alta_Poliza_Activo(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            String Mi_SQL = "";
            object Cuenta_Proveedor;
            object Cuenta_Anticipo;
            object Contrato;
            Double Monto_Anticipo=0;
            Cls_Ope_Con_Polizas_Negocio Rs_Alta_Ope_Con_Polizas= new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión hacia la capa de negocios para envio de los datos a dar de alta
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            DataTable Dt_Datos_Polizas = new DataTable();
            DataTable Dt_Partidas_Polizas = new DataTable();
            String Estatus = "";
            int partida = 0;
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL = "SELECT  " + Cat_Com_Proveedores.Campo_Cuenta_Contratista_ID ;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ";
                if (Datos.P_Proveedor_ID != null && Datos.P_Proveedor_ID != "")     // Si el P_Proveedore_ID no esá vacío, filtrar por ID
                {
                    Mi_SQL = Mi_SQL + "WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "' ";
                }
                Cmmd.CommandText = Mi_SQL; //Realiza la ejecución para obtener el Saldo
                Cuenta_Proveedor = Cmmd.ExecuteScalar();

                Mi_SQL = "SELECT  " + Cat_Com_Proveedores.Campo_Cuenta_Anticipo_ID;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ";
                if (Datos.P_Proveedor_ID != null && Datos.P_Proveedor_ID != "")     // Si el P_Proveedore_ID no esá vacío, filtrar por ID
                {
                    Mi_SQL = Mi_SQL + "WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "' ";
                }
                Cmmd.CommandText = Mi_SQL; //Realiza la ejecución para obtener el Saldo
                Cuenta_Anticipo = Cmmd.ExecuteScalar();
                if (Cuenta_Proveedor != "" && Cuenta_Anticipo != "")
                {
                    DataRow row; //Crea un nuevo registro a la tabla
                    foreach (DataRow Renglon in Datos.P_Dt_Detalles_Solicitud.Rows)
                    {
                        Monto_Anticipo = Monto_Anticipo + Convert.ToDouble(Renglon["SubTotal"].ToString().Replace(",", "")) + Convert.ToDouble(Renglon["IVA"].ToString().Replace(",", ""));
                    }
                    //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                    if (Dt_Partidas_Polizas.Rows.Count == 0)
                    {
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                        Dt_Partidas_Polizas.Columns.Add("Beneficiario_ID", typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                    }
                    Mi_SQL = "";
                    Mi_SQL = "SELECT " + Cat_Con_Contratos.Campo_Numero_Contrato + " FROM " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Contratos.Campo_No_Reserva + "='" + Datos.P_No_Reserva + "'";
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecución para obtener el Saldo
                    Contrato = Cmmd.ExecuteScalar();
                    row = Dt_Partidas_Polizas.NewRow();
                    partida = partida + 1;
                    //Agrega el cargo del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Proveedor;
                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "Anticipo de la Obra No." + Contrato;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Monto_Anticipo;
                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();

                    row = Dt_Partidas_Polizas.NewRow();
                    partida = partida + 1;
                    //Agrega el abono del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Anticipo;
                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "Anticipo de la Obra No." + Contrato;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Monto_Anticipo;
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                    row["Beneficiario_ID"] = Datos.P_Proveedor_ID;
                    row["Tipo_Beneficiario"] = "PROVEEDOR";
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();

                    //Agrega los valores a pasar a la capa de negocios para ser dados de alta en la poliza
                    Rs_Alta_Ope_Con_Polizas.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
                    Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = "00003";
                    Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", Convert.ToDateTime(DateTime.Now));
                    Rs_Alta_Ope_Con_Polizas.P_Fecha_Poliza = Convert.ToDateTime(DateTime.Now);
                    Rs_Alta_Ope_Con_Polizas.P_Concepto = "Anticipo de la Obra No." + Contrato;
                    Rs_Alta_Ope_Con_Polizas.P_Total_Debe = Monto_Anticipo;
                    Rs_Alta_Ope_Con_Polizas.P_Total_Haber = Monto_Anticipo;
                    Rs_Alta_Ope_Con_Polizas.P_No_Partida = Convert.ToInt32(partida);
                    Rs_Alta_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Rs_Alta_Ope_Con_Polizas.P_Dt_Detalles_Polizas = Dt_Partidas_Polizas;
                    Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID;
                    Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Autorizo = Cls_Sessiones.Empleado_ID;
                    Rs_Alta_Ope_Con_Polizas.P_Usuario_Valido = Datos.P_Usuario_Valido_Poliza;
                    Rs_Alta_Ope_Con_Polizas.P_Usuario_Autorizo = Datos.P_Usuario_Autorizo_Poliza;
                    Rs_Alta_Ope_Con_Polizas.P_Cmmd = Datos.P_Cmmd;
                    string[] Datos_Poliza = Rs_Alta_Ope_Con_Polizas.Alta_Poliza(); //Da de alta los datos de la Póliza proporcionados por el usuario en la BD
                    Estatus = Datos_Poliza[0] + "," + Datos_Poliza[1] + "," + Datos_Poliza[3] + "," + Datos_Poliza[2];
                }
                else
                {
                    Estatus = "Falta";
                    throw new Exception("Error: la Cuenta Contable de anticipo del Contratista para poder continuar");

                }
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Estatus;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Solicitud_Pago
        /// DESCRIPCION : Modifica los datos de la Solicitud de Pago con lo que fueron 
        ///               introducidos por el usuario
        /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
        ///                       proporcionados por el usuario y van a sustituir a los datos que se
        ///                       encuentran en la base de datos
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 17-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Modificar_Solicitud_Pago(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            Object Total_Presupuesto = null;
            StringBuilder Mi_SQL_2 = new StringBuilder();
            Double Total_Poliza = 0;                         //Obtiene el monto total del debe y haber de la póliza
            String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos
            Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
            Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
            Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
            SqlCommand Comando_SQL = new SqlCommand(); //Sirve para la ejecución de las operaciones a la base de datos
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlTransaction Transaccion_SQL;                                                  //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            DataTable Dt_Partidas = new DataTable();

            try
            {
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;

                Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "', ");
                if (!String.IsNullOrEmpty(Datos.P_No_Poliza)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Poliza_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Mes_Ano)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Datos.P_Tipo_Solicitud_Pago_ID + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Jefe_Area + " = '" + Datos.P_Empleado_ID_Jefe_Area + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Contabilidad + " = '" + Datos.P_Empleado_ID_Contabilidad + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Concepto)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Concepto + " = '" + Datos.P_Concepto + "', ");
                if (Datos.P_Monto > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto + " = " + Datos.P_Monto + ", ");
                if (!String.IsNullOrEmpty(Datos.P_No_Factura)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Factura + " = '" + Datos.P_No_Factura + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Factura)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Factura) + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Jefe)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Jefe + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Jefe) + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Contabi)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Contabi) + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Comentarios_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Jefe_Area + " = '" + Datos.P_Comentarios_Jefe_Area + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Contabilidad + " = '" + Datos.P_Comentarios_Contabilidad + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "', ");
                if (Datos.P_No_Reserva > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = " + Datos.P_No_Reserva + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + " = '" + Datos.P_Nombre_Usuario + "', ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                Mi_SQL.Length = 0;
                //Actualiza el saldo de la reserva
                Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_SQL.Append(" SET " + Ope_Psp_Reservas.Campo_Saldo + " = " + Ope_Psp_Reservas.Campo_Saldo + " + " + Datos.P_Monto_Anterior);
                Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva_Anterior));
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos
                Mi_SQL.Length = 0;

                //if (String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area) && String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad))
                //{
                //    //Actualiza el saldo de la reserva
                //    Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                //    Mi_SQL.Append(" SET " + Ope_Psp_Reservas.Campo_Saldo + " = " + Ope_Psp_Reservas.Campo_Saldo + " - " + Datos.P_Monto + ", ");
                //    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Beneficiario + " = '" + Datos.P_Beneficiario + "'");
                //    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva));
                //    Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                //    Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                //}
                Mi_SQL.Length = 0;
                //Consulta para la obtención del último ID dado de alta 
                Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')");
                Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00002'");
                Mi_SQL.Append(" ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                No_Poliza = Comando_SQL.ExecuteScalar();
                //Valida si el ID es nulo para asignarle automaticamente el primer registro
                if (Convert.IsDBNull(No_Poliza))
                {
                    No_Poliza = "0000000001";
                }
                //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                else
                {
                    No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                }
                Mi_SQL.Length = 0;
                Total_Poliza += Datos.P_Monto_Anterior + Datos.P_Monto;
                //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                Mi_SQL.Append(" (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", ");
                Mi_SQL.Append(Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + ")");
                Mi_SQL.Append(" VALUES ('" + No_Poliza + "', '00002', '" + Mes_Anio + "',");
                Mi_SQL.Append(" '" + String.Format("{0:dd/MM/yyyy}", DateTime.Today) + "',");
                Mi_SQL.Append(" '" + Datos.P_Concepto + "', " + Total_Poliza + ", " + Total_Poliza + ", 2, ");
                Mi_SQL.Append("'" + Datos.P_Nombre_Usuario + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', ");
                Mi_SQL.Append("'" + Cls_Sessiones.Empleado_ID + "')");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                //Da de alta los detalles de la póliza
                foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                {
                    Mi_SQL.Length = 0;
                    //consulta el saldo de la cuenta contable
                    Mi_SQL.Append("SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - ");
                    Mi_SQL.Append(" ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'");
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                    Saldo = Comando_SQL.ExecuteScalar();
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                    }

                    Mi_SQL.Length = 0;
                    //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                    Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                    Consecutivo = Comando_SQL.ExecuteScalar();

                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(Consecutivo))
                    {
                        Consecutivo = "1";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                    }
                    Mi_SQL.Length = 0;
                    //Inserta el registro del detalle de la póliza en la base de datos
                    Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Mi_SQL.Append("(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Consecutivo + ")");
                    Mi_SQL.Append(" VALUES('" + No_Poliza + "', '00002', '" + Mes_Anio + "', ");
                    Mi_SQL.Append(Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + ",");
                    Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "',");
                    Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString() + "', ");
                    Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ")");
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos            
                }
                Transaccion_SQL.Commit(); //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
                Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza);
                Dt_Partidas = Consultar_detalles_partidas_de_solicitud( null , Datos.P_No_Solicitud_Pago);
                if (!String.IsNullOrEmpty(Datos.P_Comentarios_Ejercido))
                {
                    foreach (DataRow Renglon in Dt_Partidas.Rows)
                    {
                        Mi_SQL.Length = 0;
                        Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                        Mi_SQL.Append(" SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " + " + Renglon["importe"].ToString() + "");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva_Anterior) + " AND ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = " + Renglon["PARTIDA_ID"].ToString() + " AND ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " = " + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString() + " AND ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " = " + Renglon["PROGRAMA_ID"].ToString());
                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    }
                }
                //if (Datos.P_Monto > 0)
                //{
                //        Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(Datos.P_No_Reserva), "DEVENGADO", "COMPROMETIDO", Dt_Partidas); //Actualiza el impote de la partida presupuestal
                //        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "DEVENGADO", "COMPROMETIDO", Datos.P_Monto, Convert.ToString(No_Poliza), "00002", Mes_Anio, "1"); //Agrega el historial del movimiento de la partida presupuestal
                //}
                //if(Datos.P_Monto_Anterior > 0)
                //{

                if (!String.IsNullOrEmpty(Datos.P_Comentarios_Jefe_Area) || !String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad))
                {
                    //Optenemos el total de la reserva con los impuestos que se disminuyeron
                    Mi_SQL_2.Append("SELECT " + Ope_Con_Polizas.Campo_Total_Debe);
                    Mi_SQL_2.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                    Mi_SQL_2.Append(" WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " ='" + No_Poliza + "'");
                    Mi_SQL_2.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " ='00002'");
                    Mi_SQL_2.Append(" AND " + Ope_Con_Polizas.Campo_Mes_Ano + " ='" + Mes_Anio + "'");
                    Total_Presupuesto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL_2.ToString());
                    Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(Datos.P_No_Reserva_Anterior), "COMPROMETIDO", "DEVENGADO", Dt_Partidas); //Actualiza el impote de la partida presupuestal
                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva_Anterior), "COMPROMETIDO", "DEVENGADO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(No_Poliza), "00002", Mes_Anio, "1"); //Agrega el historial del movimiento de la partida presupuestal
                }
                //else
                //{                            
                //    Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(Datos.P_No_Reserva_Anterior), "COMPROMETIDO", "DEVENGADO", Dt_Partidas); //Actualiza el impote de la partida presupuestal
                //    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva_Anterior), "COMPROMETIDO", "DEVENGADO", Datos.P_Monto_Anterior, Convert.ToString(No_Poliza), "00002", Mes_Anio, "3"); //Agrega el historial del movimiento de la partida presupuestal
                //  }
                //}
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Rechaza_Solicitud_Pago
        /// DESCRIPCION : Modifica los datos de la Solicitud de Pago con lo que fueron 
        ///               introducidos por el usuario
        /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
        ///                       proporcionados por el usuario y van a sustituir a los datos que se
        ///                       encuentran en la base de datos
        /// CREO        : Sergio Manuel Gallardo A.
        /// FECHA_CREO  : 12-junio-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Rechaza_Solicitud_Pago(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            Object Total_Presupuesto = null;
            StringBuilder Mi_SQL_2 = new StringBuilder();
            Double Total_Poliza = 0;                         //Obtiene el monto total del debe y haber de la póliza
            String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos
            Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
            Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
            Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
            SqlCommand Comando_SQL = new SqlCommand(); //Sirve para la ejecución de las operaciones a la base de datos
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlTransaction Transaccion_SQL;                                                  //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            DataTable Dt_Partidas = new DataTable();

            try
            {
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;

                Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "', ");
                if (!String.IsNullOrEmpty(Datos.P_No_Poliza)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Poliza_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Mes_Ano)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Datos.P_Tipo_Solicitud_Pago_ID + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Jefe_Area + " = '" + Datos.P_Empleado_ID_Jefe_Area + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Contabilidad + " = '" + Datos.P_Empleado_ID_Contabilidad + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Concepto)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Concepto + " = '" + Datos.P_Concepto + "', ");
                if (Datos.P_Monto > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto + " = " + Datos.P_Monto + ", ");
                if (!String.IsNullOrEmpty(Datos.P_No_Factura)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Factura + " = '" + Datos.P_No_Factura + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Factura)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Factura) + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Jefe)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Jefe + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Jefe) + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Contabi)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Contabi) + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Comentarios_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Jefe_Area + " = '" + Datos.P_Comentarios_Jefe_Area + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Contabilidad + " = '" + Datos.P_Comentarios_Contabilidad + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Comentario_Finanzas)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentario_Finanzas + " = '" + Datos.P_Comentario_Finanzas + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Usuario_Recibio))
                {
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Pagado + " = '" + Datos.P_Usuario_Recibio + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Pagado + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Ejercido + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Contabilidad + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autoriza_Rechaza_Ejercido + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Ejercido + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Contabilidad + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Contabilidad + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Ejercido + " = '',");
                }
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + " = '" + Datos.P_Nombre_Usuario + "', ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                Mi_SQL.Length = 0;
                //Consulta para la obtención del último ID dado de alta 
                Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')");
                Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00002'");
                Mi_SQL.Append(" ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                No_Poliza = Comando_SQL.ExecuteScalar();
                //Valida si el ID es nulo para asignarle automaticamente el primer registro
                if (Convert.IsDBNull(No_Poliza))
                {
                    No_Poliza = "0000000001";
                }
                //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                else
                {
                    No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                }
                Mi_SQL.Length = 0;
                Total_Poliza += Datos.P_Monto_Anterior;
                //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                Mi_SQL.Append(" (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", ");
                Mi_SQL.Append(Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + ")");
                Mi_SQL.Append(" VALUES ('" + No_Poliza + "', '00002', '" + Mes_Anio + "',");
                Mi_SQL.Append(" '" + String.Format("{0:dd/MM/yyyy}", DateTime.Today) + "',");
                Mi_SQL.Append(" '" + Datos.P_Concepto_Poliza + "', " + Total_Poliza + ", " + Total_Poliza + ", " + Datos.P_Partida + ", ");
                Mi_SQL.Append("'" + Datos.P_Nombre_Usuario + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', ");
                Mi_SQL.Append("'" + Cls_Sessiones.Empleado_ID + "')");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                //Da de alta los detalles de la póliza
                foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                {
                    Mi_SQL.Length = 0;
                    //consulta el saldo de la cuenta contable
                    Mi_SQL.Append("SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - ");
                    Mi_SQL.Append(" ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'");
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                    Saldo = Comando_SQL.ExecuteScalar();
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                    }

                    Mi_SQL.Length = 0;
                    //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                    Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                    Consecutivo = Comando_SQL.ExecuteScalar();

                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(Consecutivo))
                    {
                        Consecutivo = "1";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                    }
                    Mi_SQL.Length = 0;
                    //Inserta el registro del detalle de la póliza en la base de datos
                    Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Mi_SQL.Append("(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Consecutivo + ")");
                    Mi_SQL.Append(" VALUES('" + No_Poliza + "', '00002', '" + Mes_Anio + "', ");
                    Mi_SQL.Append(Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + ",");
                    Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "',");
                    Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString() + "', ");
                    Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ")");
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos            
                }
                Transaccion_SQL.Commit(); //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
                //Optenemos el total de la reserva con los impuestos que se disminuyeron
                Mi_SQL_2.Append("SELECT " + Ope_Con_Polizas.Campo_Total_Debe);
                Mi_SQL_2.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                Mi_SQL_2.Append(" WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " ='" + No_Poliza + "'");
                Mi_SQL_2.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " ='00002'");
                Mi_SQL_2.Append(" AND " + Ope_Con_Polizas.Campo_Mes_Ano + " ='" + Mes_Anio + "'");
                Total_Presupuesto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL_2.ToString());
                Dt_Partidas = Consultar_detalles_partidas_de_solicitud(null, Datos.P_No_Solicitud_Pago);
                Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza);
                Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(Datos.P_No_Reserva), "DEVENGADO", "EJERCIDO", Dt_Partidas); //Actualiza el impote de la partida presupuestal
                Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "DEVENGADO", "EJERCIDO", Convert.ToDouble(Total_Presupuesto), "", "", "", "");//Agrega el historial del movimiento de la partida presupuestal
                Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Convert.ToString(Datos.P_No_Reserva), "COMPROMETIDO", "DEVENGADO", Dt_Partidas); //Actualiza el impote de la partida presupuestal
                Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva), "COMPROMETIDO", "DEVENGADO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(No_Poliza), "00002", Mes_Anio, "1"); //Agrega el historial del movimiento de la partida presupuestal
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Solicitud_Pago_Sin_Poliza
        /// DESCRIPCION : Modifica los datos de la Solicitud de Pago con lo que fueron 
        ///               introducidos por el usuario
        /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
        ///                       proporcionados por el usuario y van a sustituir a los datos que se
        ///                       encuentran en la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 17-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static String Modificar_Solicitud_Pago_Sin_Poliza(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            //String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos              
            //OracleCommand Comando_SQL = new OracleCommand(); //Sirve para la ejecución de las operaciones a la base de datos
            //OracleConnection Conexion_Base = new OracleConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            //OracleTransaction Transaccion_SQL;                                                  //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            Object Operacion;
            Object Total_Presupuesto;
            String Directorio_Destino;
            DataTable Dt_Detalles_Anteriores = new DataTable();
            Boolean Insertar = true;
            String Respuesta = "SI";
            int Afectacion_Presupuestal = 0;
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_solicitud = new DataTable();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "', ");
                if (!String.IsNullOrEmpty(Datos.P_No_Poliza)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Poliza_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Mes_Ano)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Datos.P_Tipo_Solicitud_Pago_ID + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Jefe_Area + " = '" + Datos.P_Empleado_ID_Jefe_Area + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Contabilidad + " = '" + Datos.P_Empleado_ID_Contabilidad + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Concepto)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Concepto + " = '" + Datos.P_Concepto + "', ");
                if (Datos.P_Monto > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto + " = " + Datos.P_Monto + ", ");
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Jefe)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Jefe + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Jefe) + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Contabi)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Contabi) + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Comentarios_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Jefe_Area + " = '" + Datos.P_Comentarios_Jefe_Area + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Contabilidad + " = '" + Datos.P_Comentarios_Contabilidad + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Comentario_Recepcion)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Recepcion_Doc + " = '" + Datos.P_Comentario_Recepcion + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Usuario_Recibio)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio + " = '" + Datos.P_Usuario_Recibio + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Fecha_recepcion_Documentos)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recepcion_Documentos + " = '" + Datos.P_Fecha_recepcion_Documentos + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Empleado_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + " = '" + Datos.P_Empleado_ID + "', ");
                if (Datos.P_No_Reserva > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = " + Datos.P_No_Reserva + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + " = '" + Datos.P_Nombre_Usuario + "', ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                ////Actualiza el saldo de la reserva
                //Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                //Mi_SQL.Append(" SET " + Ope_Psp_Reservas.Campo_Saldo + " =" + Ope_Psp_Reservas.Campo_Importe_Inicial);
                //Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva_Anterior));
                //Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                //Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos
                //Mi_SQL.Length = 0;
                //Mi_SQL.Length = 0;
                Mi_SQL.Length = 0;
                //Consultamos el tipo de operacion de la solicitud de pago
                Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion);
                Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago+"'");
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Operacion = Cmmd.ExecuteScalar();
                if (Operacion != "ANTICIPO")
                {
                    Mi_SQL.Length = 0;
                    //Actualiza el saldo de la reserva
                    Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                    Mi_SQL.Append(" SET " + Ope_Psp_Reservas.Campo_Saldo + " = " + Ope_Psp_Reservas.Campo_Saldo + " + " + Datos.P_Monto_Anterior);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva_Anterior));
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                
                }
               
                Mi_SQL.Length = 0;
                if (String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area) && String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad))
                {
                    Dt_Partidas = Consultar_detalles_partidas_de_solicitud(Cmmd, Datos.P_No_Solicitud_Pago);
                    foreach (DataRow Renglon in Dt_Partidas.Rows)
                    {
                        Mi_SQL.Length = 0;
                        Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                        Mi_SQL.Append(" SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " + " + Renglon["importe"].ToString() + "");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva_Anterior) + " AND ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = " + Renglon["PARTIDA_ID"].ToString() + " AND ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " = " + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString() + " AND ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " = " + Renglon["PROGRAMA_ID"].ToString());
                        Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    }
                    Mi_SQL.Length = 0;
                    //Actualiza el saldo de la reserva
                    Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                    Mi_SQL.Append(" SET " + Ope_Psp_Reservas.Campo_Saldo + " = " + Ope_Psp_Reservas.Campo_Saldo + " - " + Datos.P_Monto);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva_Anterior));
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    
                }
                if (Datos.P_Modifica_Solicitud == "S")
                {
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Archivo + " FROM " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Dt_Oracle.SelectCommand = Cmmd;
                    Dt_Oracle.Fill(Ds_Oracle);
                    Dt_Detalles_Anteriores = Ds_Oracle.Tables[0];
                    //Comando_SQL.CommandText = Mi_SQL.ToString();
                    //Dt_Detalles_Anteriores = OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    Mi_SQL.Length = 0;
                    //Elimina el registro de los detalles al cual pertenece la solicitud de pago que fue seleccionado por el usuario
                    Mi_SQL.Append("DELETE FROM " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    
                    Directorio_Destino = HttpContext.Current.Server.MapPath("~") + "\\Archivo_Solicitud_Pagos\\" + Datos.P_No_Solicitud_Pago + "\\";
                    foreach (DataRow Renglon in Datos.P_Dt_Detalles_Solicitud.Rows)
                    {
                        Mi_SQL.Length = 0;
                        Mi_SQL.Append("INSERT INTO " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                        Mi_SQL.Append(" (" + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura + ", ");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Factura
                                            + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura
                                            + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID +
                                              ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Documento + ",");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Archivo
                                            + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Ruta
                                            + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID +
                                            ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva
                                            + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID +
                                              ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Nombre_Proveedor_Factura + ",");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_RFC
                                            + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_CURP
                                            + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion
                                            + ", CAP, DIVO, SEFUPU, RAPCE, ICIC, OBS, Est_Y_Proy, Lab_Control_C, Suma, Alcance_Neto, Total_Est, Amortizado,");
                        Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Usuario_Creo + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Creo + ")");
                        Mi_SQL.Append(" VALUES ('" + Datos.P_No_Solicitud_Pago + "', '" + Renglon["No_Documento"].ToString() + "', '" + Renglon["Fecha_Documento"].ToString() + "',");
                        Mi_SQL.Append(" " + Renglon["SubTotal"].ToString().Replace(",", "") + ",'" +
                                            Renglon["Partida_Id"].ToString() +
                                            "','" + Datos.P_Tipo_Documento + "','" +
                                            Renglon["Archivo"].ToString() + "','");
                        if (Renglon["Archivo"].ToString() != null && Renglon["Archivo"].ToString() != "")
                        {
                            Mi_SQL.Append(Directorio_Destino + Renglon["Archivo"].ToString() + "','");
                        }
                        else
                        {
                            Mi_SQL.Append("','");
                        }
                        Mi_SQL.Append(Renglon["Proyecto_Programa_Id"].ToString() + "'," +
                            Renglon["Iva"].ToString() + ", " +
                            "'" + Renglon["Fte_Financiamiento_Id"].ToString() + "','" +
                             Renglon["Nombre_Proveedor_Fact"].ToString() + "','" +
                             Renglon["RFC"].ToString() +
                            "','" + Renglon["CURP"].ToString() + "','" +
                            Renglon["Operacion"].ToString() + "'," +
                              Renglon["Divo"].ToString() + "," +
                              Renglon["Cap"].ToString() + "," +
                              Renglon["Sefupu"].ToString() + "," +
                              Renglon["Rapce"].ToString() + "," +
                              Renglon["Icic"].ToString() + "," +
                              Renglon["Obs"].ToString() + "," +
                              Renglon["EstyProy"].ToString() + "," +
                              Renglon["Lab_Control"].ToString() + "," +
                              Renglon["Suma"].ToString() + "," +
                              Renglon["Alcance_Neto"].ToString() + "," +
                              Renglon["Total"].ToString() + "," +
                              Renglon["Amortizacion"].ToString() + ",'" +
                            Datos.P_Nombre_Usuario + "', GETDATE())");// + Datos.P_No_Factura + "',");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                        if (Renglon["Archivo"].ToString() != null && Renglon["Archivo"].ToString() != "")
                        {
                            foreach (DataRow x in Dt_Detalles_Anteriores.Rows)
                            {
                                if (Renglon["Archivo"].ToString() == x["Archivo"].ToString())
                                {
                                    Insertar = false;
                                }
                            }
                            if (Insertar == true)
                            {
                                System.IO.File.Copy(Renglon["Ruta"].ToString(), Directorio_Destino + Renglon["Archivo"].ToString());
                                System.IO.File.Delete(Renglon["Ruta"].ToString());
                            }
                        }
                    }
                    Directorio_Destino = HttpContext.Current.Server.MapPath("~") + "\\Archivo_Solicitud_Pagos\\Temporal";
                    String[] archivos = System.IO.Directory.GetFiles(Directorio_Destino);
                    int a;
                    for (a = 0; a < archivos.Length; a++)
                    {
                        System.IO.File.Delete(a.ToString());
                    }
                }
                ////Transaccion_SQL.Commit(); //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
                if (String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area) && String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad))
                {
                    Dt_Partidas = Consultar_detalles_partidas_de_solicitud(Cmmd,Datos.P_No_Solicitud_Pago);
                    foreach (DataRow Renglon in Dt_Partidas.Rows)
                    {
                        Mi_SQL.Length = 0;
                        Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                        Mi_SQL.Append(" SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " - " + Renglon["importe"].ToString() + "");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva_Anterior) + " AND ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = " + Renglon["PARTIDA_ID"].ToString() + " AND ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " = " + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString() + " AND ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " = " + Renglon["PROGRAMA_ID"].ToString());
                        Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                        //OracleHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    }
                }
                if (String.IsNullOrEmpty(Datos.P_Modifica_Solicitud))
                {
                    if (Operacion != "ANTICIPO")
                    {
                        Dt_Partidas = Consultar_detalles_partidas_de_solicitud(Cmmd, Datos.P_No_Solicitud_Pago);
                        foreach (DataRow Renglon in Dt_Partidas.Rows)
                        {
                            Mi_SQL.Length = 0;
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                            Mi_SQL.Append(" SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " + " + Renglon["importe"].ToString() + "");
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva_Anterior) + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = " + Renglon["PARTIDA_ID"].ToString() + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " = " + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString() + " AND ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " = " + Renglon["PROGRAMA_ID"].ToString());
                            Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                            Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                            //OracleHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        }
                        Mi_SQL.Length = 0;
                        Mi_SQL.Append("SELECT " + Ope_Con_Polizas.Campo_Total_Debe);
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " ='" + Datos.P_No_Poliza + "'");
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " ='" + Datos.P_Tipo_Poliza_ID + "'");
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Mes_Ano + " ='" + Datos.P_Mes_Ano + "'");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                        Total_Presupuesto = Cmmd.ExecuteScalar();

                        //se registran los momentos contables
                        Afectacion_Presupuestal=Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("COMPROMETIDO", "DEVENGADO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                        if (Afectacion_Presupuestal > 0)
                        {
                            Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Datos.P_No_Reserva_Anterior), "COMPROMETIDO", "DEVENGADO", Convert.ToDouble(Total_Presupuesto), Convert.ToString(Datos.P_No_Poliza), Datos.P_Tipo_Poliza_ID, Datos.P_Mes_Ano, "1", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                            Respuesta = "SI";
                        }
                        else
                        {
                            Respuesta = "NO";
                        }
                    }
                }
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Datos.P_No_Solicitud_Pago + "-"+Respuesta;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }

            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Rechaza_Solicitud_Pago_Sin_Poliza
        /// DESCRIPCION : Modifica los datos de la Solicitud de Pago con lo que fueron 
        ///               introducidos por el usuario
        /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
        ///                       proporcionados por el usuario y van a sustituir a los datos que se
        ///                       encuentran en la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 17-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static String Rechaza_Solicitud_Pago_Sin_Poliza(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            //String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos              
            SqlCommand Comando_SQL = new SqlCommand(); //Sirve para la ejecución de las operaciones a la base de datos
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlTransaction Transaccion_SQL;                                                  //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            DataTable Dt_Detalles_Anteriores = new DataTable();
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_solicitud = new DataTable();
            try
            {
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;

                Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" SET " + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "', ");
                if (!String.IsNullOrEmpty(Datos.P_No_Poliza)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Poliza_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Mes_Ano)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Datos.P_Tipo_Solicitud_Pago_ID + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Jefe_Area + " = '" + Datos.P_Empleado_ID_Jefe_Area + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Empleado_ID_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID_Contabilidad + " = '" + Datos.P_Empleado_ID_Contabilidad + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Concepto)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Concepto + " = '" + Datos.P_Concepto + "', ");
                if (Datos.P_Monto > 0) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto + " = " + Datos.P_Monto + ", ");
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Jefe)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Jefe + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Jefe) + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Contabi)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Autorizo_Rechazo_Contabi) + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Comentarios_Jefe_Area)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Jefe_Area + " = '" + Datos.P_Comentarios_Jefe_Area + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Comentarios_Contabilidad)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Contabilidad + " = '" + Datos.P_Comentarios_Contabilidad + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Comentario_Recepcion)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Comentarios_Recepcion_Doc + " = '" + Datos.P_Comentario_Recepcion + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Usuario_Recibio)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio + " = '" + Datos.P_Usuario_Recibio + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Fecha_recepcion_Documentos)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recepcion_Documentos + " = '" + Datos.P_Fecha_recepcion_Documentos + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Empleado_ID)) Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + " = '" + Datos.P_Empleado_ID + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Usuario_Recibio))
                {
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Pagado + " = '" + Datos.P_Usuario_Recibio + "', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Pagado + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Ejercido + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Contabilidad + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Autoriza_Rechaza_Ejercido + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Ejercido + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Recibio_Contabilidad + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Contabilidad + " = '', ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Autorizo_Ejercido + " = '',");
                }
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Modifico + " = '" + Datos.P_Nombre_Usuario + "', ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                Mi_SQL.Length = 0;
                Transaccion_SQL.Commit(); //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
                return Datos.P_No_Solicitud_Pago;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Eliminar_Solicitud_Pago
        /// DESCRIPCION : Elimina la Solicitud de pago que fue seleccionada por el usuario de la BD
        /// PARAMETROS  : Datos: Obtiene que Cuenta Contable desea eliminar de la BD
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 17-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Eliminar_Solicitud_Pago(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene los datos de la inserción a realizar a la base de datos
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL;                                                  //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        

            try
            {
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;

                //Elimina el registro al cual pertenece la solicitud de pago que fue seleccionado por el usuario
                Mi_SQL.Append("DELETE FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                Mi_SQL.Length = 0;
                //Actualiza el saldo de la reserva
                Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_SQL.Append(" SET " + Ope_Psp_Reservas.Campo_Saldo + " = " + Ope_Psp_Reservas.Campo_Saldo + " + " + Datos.P_Monto);
                Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva));
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos

                Transaccion_SQL.Commit();                    //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        #endregion

        #region(Consultas)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Solicitud_Pago
        /// DESCRIPCION : Consulta todos los datos que se tienen registrados en la base
        ///               de datos ya sea de acuerdo a los parametros proporcionado pos el
        ///               usuario o todos
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 17-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Solicitud_Pago(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + ", ");
                Mi_SQL.Append(Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + "." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + " AS Solicitud, ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario);
                Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + ", " + Cat_Dependencias.Tabla_Cat_Dependencias + ", ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ", " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + ", ");
                Mi_SQL.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos);
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva);
                Mi_SQL.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Recurso + " = 'RAMO 33'");
                Mi_SQL.Append(" AND " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + "." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID);
                Mi_SQL.Append(" = " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID);
                Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID);
                Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_SQL.Append(" = " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_No_Reserva);
                if (!String.IsNullOrEmpty(Datos.P_Nombre_Contrato)) Mi_SQL.Append(" AND " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Nombre_Contrato + " LIKE '%" + Datos.P_Nombre_Contrato.Trim() + "%'");
                if (!String.IsNullOrEmpty(Datos.P_No_Contrato)) Mi_SQL.Append(" AND " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Numero_Contrato + " LIKE '%" + Datos.P_No_Contrato.Trim() + "%'");
                if (!String.IsNullOrEmpty(Datos.P_No_Solicitud_Pago)) Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = '" + Datos.P_No_Solicitud_Pago + "'");
                if (!String.IsNullOrEmpty(Datos.P_Estatus)) Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "'");
                if (!String.IsNullOrEmpty(Datos.P_Dependencia_ID)) Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "'");
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Pago_ID)) Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Datos.P_Tipo_Solicitud_Pago_ID + "'");
                if (Datos.P_No_Reserva > 0) Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = " + Datos.P_No_Reserva);
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicial) && !String.IsNullOrEmpty(Datos.P_Fecha_Final))
                {
                    Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud);
                    Mi_SQL.Append(" BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00'");
                    Mi_SQL.Append(" AND '" + Datos.P_Fecha_Final + " 23:59:59'");
                }
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Reservas
        /// DESCRIPCION : Consulta todas las reservas que tengan un saldo mayor a 0
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 18-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Reservas(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT " + Ope_Psp_Reservas.Campo_No_Reserva + ",");
                Mi_SQL.Append(" (" + Ope_Psp_Reservas.Campo_No_Reserva + "+'-'+" + Ope_Psp_Reservas.Campo_Concepto + ") AS Reserva");
                Mi_SQL.Append(" FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_Saldo + " > 0");
                Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Campo_Estatus + " ='GENERADA'");
                Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Campo_Recurso + " ='RAMO 33'");

                if (!String.IsNullOrEmpty(Datos.P_Dependencia_ID)) Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "'");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_Reserva
        /// DESCRIPCION : Consulta todos los datos de la reserva que fue seleccionada por
        ///               el usuario
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 18-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Datos_Reserva(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + " AS Reservado, ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Proveedor_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Empleado_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Tipo_Solicitud_Pago_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Saldo + ", ");
                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + ", ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Saldo + " As Saldo_Total, ");
                Mi_SQL.Append("(" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                Mi_SQL.Append("+'-'+" + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave);
                Mi_SQL.Append("+'-'+" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                Mi_SQL.Append("+'-'+" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                Mi_SQL.Append("+'-'+" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ") AS Codigo_Programatico, ");
                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS Fuente_Financiamiento, ");
                Mi_SQL.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS Area_Funcional, ");
                Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS Proyectos_Programas, ");
                Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS Dependencia, ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Descripcion + " AS Partida, ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append(" FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ", " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + ", ");
                Mi_SQL.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + ", " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + ", ");
                Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + ", " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "," + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID);
                Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_SQL.Append(" AND " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_SQL.Append(" AND " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                Mi_SQL.Append(" AND " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                Mi_SQL.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                Mi_SQL.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_SQL.Append(" = " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                Mi_SQL.Append(" AND " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + Convert.ToInt32(Datos.P_No_Reserva));
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_Solicitud_Pago
        /// DESCRIPCION : Consulta todos los datos de la solicitud de pago que selecciono
        ///               el usuario
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 19-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Datos_Solicitud_Pago(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            DataTable Dt_Psp = new DataTable();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("SELECT " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Acreedor_ID + " as Pro_Cuenta_Acreedor, ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Contratista_ID + " as Pro_Cuenta_Contratista, ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Anticipo_ID + " as Pro_Cuenta_Anticipo, ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Deudor_ID + " as Pro_Cuenta_Deudor, ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Judicial_ID + " as Pro_Cuenta_Judicial, ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Nomina_ID + " as Pro_Cuenta_Nomina, ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Predial_ID + " as Pro_Cuenta_Predial, ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID + " as Pro_Cuenta_Proveedor, ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + " as Proveedor, ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Cuenta_Contable_ID + " as Empleado_Cuenta, ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + " as Empleado, ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " , ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Poliza + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", ");
                Mi_SQL.Append(" (" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_SQL.Append("+'-'+" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ") AS Reserva, ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Cuenta_Contable_ID + " AS Cuenta_Contable_Reserva, ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", SUM(");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + ") AS Monto_Partida, SUM(");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva + ") AS Monto_IVA, SUM(");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR + ") AS Monto_ISR, SUM(");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula + ") AS Monto_Cedular, SUM(");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".CAP) AS Monto_CAP , SUM(");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".DIVO) AS Monto_DIVO , SUM(");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".SEFUPU) AS Monto_SEFUPU , SUM(");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".ICIC) AS Monto_ICIC , SUM(");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".RAPCE) AS Monto_RAPCE , SUM(");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".Lab_Control_C) AS Monto_Lab_Control_C , SUM(");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".OBS) AS Monto_OBS , SUM(");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".Est_Y_Proy) AS Monto_Est_Y_Proy , SUM(");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".Amortizado) AS Monto_Armonizado,  SUM(");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + ".Suma) AS Monto_Suma, ");
                Mi_SQL.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Contrato_ID + ", ");
                Mi_SQL.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Importe_Total + ", ");
                Mi_SQL.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Anticipo_Monto + ", ");
                Mi_SQL.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Anticipo_Porcentaje + ", ");
                Mi_SQL.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Por_Amortizar + ", ");
                Mi_SQL.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Saldo_Total);
                Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ON  " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + " = ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " ON  " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + " = ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " ON  " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " ON  " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + " = ");
                Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + " ON  " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_No_Reserva + " = ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " ON  " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " = ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " ON  " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + " = ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " AND " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID + " = ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                Mi_SQL.Append(" WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'");
                Mi_SQL.Append(" Group by " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Acreedor_ID + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Contratista_ID + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Deudor_ID + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Anticipo_ID + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Judicial_ID + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Nomina_ID + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Predial_ID + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Cuenta_Contable_ID + ", ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ", ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " , ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Factura + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Monto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Poliza + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ", ");
                Mi_SQL.Append(" (" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_SQL.Append("+'-'+" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + "), ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Cuenta_Contable_ID + ", ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", ");
                Mi_SQL.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Contrato_ID + ", ");
                Mi_SQL.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Importe_Total + ", ");
                Mi_SQL.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Anticipo_Monto + ", ");
                Mi_SQL.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Anticipo_Porcentaje + ", ");
                Mi_SQL.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Amortizado + ", ");
                Mi_SQL.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Por_Amortizar + ", ");
                Mi_SQL.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Saldo_Total);
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                Dt_Psp = Ds_Oracle.Tables[0];
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Dt_Psp;
                //return OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Cuenta_Contable_Proveedor
        /// DESCRIPCION : Consulta si el proveedor proporcionado tiene una cuenta contable
        ///               asiganada
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 23-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Cuenta_Contable_Proveedor(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT " + Cat_Com_Proveedores.Campo_Proveedor_ID + ", " + Cat_Com_Proveedores.Campo_Nombre + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append(" FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores);
                Mi_SQL.Append(" WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "'");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Detalles_Solicitud
        /// DESCRIPCION : Consulta todos los datos de la solicitud de pago que selecciono
        ///               el usuario
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 05-Enero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Detalles_Solicitud(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            DataTable Dt_Psp = new DataTable();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("SELECT detalles.*, partida.Descripcion FROM " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " detalles," + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " partida");
                Mi_SQL.Append(" WHERE detalles." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID + " = partida." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " AND " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'");
                //return OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                Dt_Psp = Ds_Oracle.Tables[0];
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Dt_Psp;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Parametro_ISR
        /// DESCRIPCION : consulta en la tabla de ISR el porcentaje a disminuar  a la factura del personal dependiendo del salario a obtener
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 05-Enero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Parametro_ISR(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT * FROM " + Tab_Nom_ISR.Tabla_Tab_Nom_ISR + " WHERE " + Tab_Nom_ISR.Campo_Limite_Inferior + " <='" + Datos.P_Limite_Inferior + "'");
                Mi_SQL.Append(" ORDER BY " + Tab_Nom_ISR.Campo_Limite_Inferior + " DESC");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Solicitud_Pagos_con_Detalles
        /// DESCRIPCION : Consulta todos los datos de la solicitud de pago que selecciono
        ///               el usuario
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 06-Enero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Solicitud_Pagos_con_Detalles(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", SOLICITUD.");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto + ", SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion + " AS T_OPERACION, DETALLES.");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura + ", DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + " AS IMPORTE , RESERVA.");
                Mi_SQL.Append(Ope_Psp_Reservas.Campo_No_Reserva + " AS RESERVA , PROVEEDOR." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR , PROVEEDOR.");
                Mi_SQL.Append(Cat_Com_Proveedores.Campo_RFC + " AS RFC_PROVEEDOR , PROVEEDOR." + Cat_Com_Proveedores.Campo_Correo_Electronico + " AS CORREO_PROVEEDOR , SOLICITUD.");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + " AS FECHA_SOLICITUD , DEPENDENCIA.");
                Mi_SQL.Append(Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA , RESERVA." + Ope_Psp_Reservas.Campo_Importe_Inicial + ", EMPLEADO.");
                Mi_SQL.Append(Cat_Empleados.Campo_Nombre + " AS NOMBRE_EMPLEADO , EMPLEADO." + Cat_Empleados.Campo_Apellido_Materno + " , EMPLEADO.");
                Mi_SQL.Append(Cat_Empleados.Campo_Apellido_Paterno + ", EMPLEADO." + Cat_Empleados.Campo_RFC + " AS RFC_EMPLEADO, EMPLEADO.");
                Mi_SQL.Append(Cat_Empleados.Campo_Correo_Electronico + " AS CORREO_EMPLEADO , BANCO." + Cat_Nom_Bancos.Campo_Nombre + " AS BANCO_EMPLEADO, BANCO.");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Banco_ID + " AS BANCO_ID_EMPLEADO , BANCO." + Cat_Nom_Bancos.Campo_No_Cuenta + " as NO_CUENTA_BANCO_EMP, ");
                Mi_SQL.Append(" PROVEEDOR." + Cat_Com_Proveedores.Campo_Cuenta + " AS CUENTA , PROVEEDOR." + Cat_Com_Proveedores.Campo_Banco_Proveedor_ID + " AS BANCO_ID , PROVEEDOR.");
                Mi_SQL.Append(Cat_Com_Proveedores.Campo_Clabe + " AS CLABE , PROVEEDOR." + Cat_Com_Proveedores.Campo_Banco_Proveedor + " AS BANCO ,");
                Mi_SQL.Append("( FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                Mi_SQL.Append("+'-'+ AREA." + Cat_SAP_Area_Funcional.Campo_Clave);
                Mi_SQL.Append("+'-'+ PROGRAMA." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                Mi_SQL.Append("+'-'+ DEPENDENCIA." + Cat_Dependencias.Campo_Clave);
                Mi_SQL.Append("+'-'+ PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Clave + ") AS Codigo_Programatico, ");
                Mi_SQL.Append("( FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                Mi_SQL.Append("+'-'+ AREA." + Cat_SAP_Area_Funcional.Campo_Clave);
                Mi_SQL.Append("+'-'+ PROGRAMA." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                Mi_SQL.Append("+'-'+ DEPENDENCIA." + Cat_Dependencias.Campo_Clave + ") AS Codigo_Dependencia ,' ' as CUENTA_A_PAGAR,' ' as CUENTA_DE_PAGO");
                Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SOLICITUD");
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " DETALLES ON  SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + " = DETALLES.");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " RESERVA ON  SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = RESERVA.");
                Mi_SQL.Append(Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " PROGRAMA ON  DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID + " = PROGRAMA.");
                Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIA ON  RESERVA." + Ope_Psp_Reservas.Campo_Dependencia_ID + " = DEPENDENCIA.");
                Mi_SQL.Append(Cat_Dependencias.Campo_Dependencia_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " AREA ON  DEPENDENCIA." + Cat_Dependencias.Campo_Area_Funcional_ID + " = AREA.");
                Mi_SQL.Append(Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FUENTE ON  DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID + " = FUENTE.");
                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDA ON  DETALLES." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID + " = PARTIDA.");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDOR ON  RESERVA." + Ope_Psp_Reservas.Campo_Proveedor_ID + " = PROVEEDOR.");
                Mi_SQL.Append(Cat_Com_Proveedores.Campo_Proveedor_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO ON  RESERVA." + Ope_Psp_Reservas.Campo_Empleado_ID + " = EMPLEADO.");
                Mi_SQL.Append(Cat_Empleados.Campo_Empleado_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO ON  EMPLEADO." + Cat_Empleados.Campo_Banco_ID + " = BANCO.");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Banco_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO1 ON  PROVEEDOR." + Cat_Com_Proveedores.Campo_Banco_ID + " = BANCO1.");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Banco_ID);
                Mi_SQL.Append(" WHERE SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud_Pago + "'");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        #endregion

        #region (Inversiones)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Contratos
        ///DESCRIPCIÓN          : consulta para obtener los datos de los contratos
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 03/Agosto/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Contratos(Cls_Ope_Con_Solicitud_Pago_Ingresos_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Contrato_ID + ", ");
                Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Numero_Contrato + ", ");
                Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Nombre_Contrato + ", ");
                Mi_Sql.Append("'[' + " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Numero_Contrato + " + '] - ' + ");
                Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Nombre_Contrato + " AS CLAVE_NOMBRE, ");
                Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Fecha_Inicio + ", ");
                Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Fecha_Fin + ", ");
                Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Estatus + ", ");
                Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Proveedor_ID + ", ");
                Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Importe_Total + ", ");
                Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Anticipo_Monto + ", ");
                Mi_Sql.Append("ISNULL(" + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Anticipo_Porcentaje + ",0) AS ANTICIPO_PORCENTAJE, ");
                Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Amortizado + ", ");
                Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Por_Amortizar + ", ");
                Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Saldo_Total + ", ");
                Mi_Sql.Append("ISNULL(" + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Avance_Fisico + ",0) AS AVANCE_FISICO, ");
                Mi_Sql.Append("ISNULL(" + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Avance_Financiero + ",0) AS AVANCE_FINANCIERO, ");
                Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Usuario_Creo + ", ");
                Mi_Sql.Append(Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_No_Reserva + ", ");
                Mi_Sql.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre);
                Mi_Sql.Append(" FROM " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos);
                Mi_Sql.Append(" INNER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores);
                Mi_Sql.Append(" ON " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Proveedor_ID);
                Mi_Sql.Append(" = " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID);

                if (!String.IsNullOrEmpty(Negocio.P_Contrato_ID.Trim()))
                {
                    Mi_Sql.Append(" WHERE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Contrato_ID);
                    Mi_Sql.Append(" = '" + Negocio.P_Contrato_ID.Trim() + "'");
                }

                if (!String.IsNullOrEmpty(Negocio.P_Estatus.Trim()))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Estatus);
                        Mi_Sql.Append(" = '" + Negocio.P_Estatus.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Estatus);
                        Mi_Sql.Append(" = '" + Negocio.P_Estatus.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Nombre_Contrato.Trim()))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Nombre_Contrato);
                        Mi_Sql.Append(" = '" + Negocio.P_Nombre_Contrato.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Nombre_Contrato);
                        Mi_Sql.Append(" = '" + Negocio.P_Nombre_Contrato.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_No_Contrato.Trim()))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Numero_Contrato);
                        Mi_Sql.Append(" = '" + Negocio.P_No_Contrato.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Numero_Contrato);
                        Mi_Sql.Append(" = '" + Negocio.P_No_Contrato.Trim() + "'");
                    }
                }

                if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                {
                    Mi_Sql.Append(" AND " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Saldo_Total + " > 0");
                }
                else
                {
                    Mi_Sql.Append(" WHERE " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Saldo_Total + " > 0");
                }

                Mi_Sql.Append(" ORDER BY " + Cat_Con_Contratos.Tabla_Cat_Con_Contratos + "." + Cat_Con_Contratos.Campo_Fecha_Inicio + " DESC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los contratos. Error: [" + Ex.Message + "]");
            }
        }
        #endregion
    }
}
