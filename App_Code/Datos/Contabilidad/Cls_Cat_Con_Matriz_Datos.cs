﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Matriz.Negocio;

namespace JAPAMI.Matriz.Datos
{
    public class Cls_Cat_Con_Matriz_Datos
    {
        #region (Métodos Operación)
        /*
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Alta_Cuenta_Matriz
            /// DESCRIPCION : 1.Consulta el último ID dado de alta para poder ingresar el siguiente
            ///               2. Da de Alta el Nivel en la BD con los datos proporcionados 
            ///                  por elusuario
            /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 09-Junio-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
        public static void Alta_Cuenta_Matriz(Cls_Cat_Con_Matriz_Negocio Datos)
            {
                String Mi_SQL;   //Variable de Consulta para la Alta del Nivel de Poliza
                Object Matriz_ID; //Variable que contendrá el ID de la consulta
                DataTable Dt_Matriz = new DataTable();
                SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
                SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
                SqlTransaction Transaccion_SQL;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
            try
                {
                    Dt_Matriz = Datos.P_Datos_Matriz_Cuenta;
                    foreach (DataRow Renglon in Dt_Matriz.Rows)
                    {
                        //Consulta para la obtención del último consecutivo dado de alta en la tabla de Matriz de cuentas
                        Mi_SQL = "SELECT ISNULL(MAX (" + Cat_Con_Matriz_De_Cuentas.Campo_Matriz_ID + "),'00000') ";
                        Mi_SQL = Mi_SQL + "FROM " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas;                   
                        Comando_SQL.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                        Matriz_ID = Comando_SQL.ExecuteScalar();

                        //Valida si el ID es nulo para asignarle automaticamente el primer registro
                        if (Convert.IsDBNull(Matriz_ID))
                        {
                            Matriz_ID = "00001";
                        }
                        //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                        else
                        {
                            Matriz_ID = String.Format("{0:00000}", Convert.ToInt32(Matriz_ID) + 1);
                        }

                        Mi_SQL = "INSERT INTO " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas  + " (" +
                                 Cat_Con_Matriz_De_Cuentas.Campo_Matriz_ID + ", " + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_ID_Debe  + ", " +
                                 Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_ID_Haber + ", " + Cat_Con_Matriz_De_Cuentas.Campo_Numero_Relaciones  + ", " +
                                 Cat_Con_Matriz_De_Cuentas.Campo_Usuario_Creo + ", " + Cat_Con_Matriz_De_Cuentas.Campo_Fecha_Creo +
                                 ") VALUES(" + "'" + Matriz_ID + "', '" + Renglon["CUENTA_ID_DEBE"].ToString() + "', '" + Datos.P_Cuenta_ID_Haber + "', " +
                                 Datos.P_Numero_Relaciones  + ", '" +
                                Datos.P_Nombre_Usuario + "', GETDATE()" + ")";
                        Comando_SQL.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                        Comando_SQL.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    }
                    Transaccion_SQL.Commit(); //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }
            ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modifica_Cuenta_Matriz
            /// DESCRIPCION : Modifica los datos del Nivel con lo que fueron introducidos 
            ///              por el usuario
            /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
            ///                       proporcionados por el usuario y van a sustituir a los datos que se
            ///                       encuentran en la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 29-diciembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
        public static void Modifica_Cuenta_Matriz(Cls_Cat_Con_Matriz_Negocio Datos)
            {
                String Mi_SQL;   //Variable de Consulta para la Alta del Nivel de Poliza
                Object Matriz_ID; //Variable que contendrá el ID de la consulta
                DataTable Dt_Matriz = new DataTable();
                SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
                SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
                SqlTransaction Transaccion_SQL;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
                try
                {
                    Mi_SQL = "DELETE FROM " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas;
                    Mi_SQL += " WHERE " + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_ID_Haber + " ='" + Datos.P_Cuenta_ID_Haber + "'";
                    Comando_SQL.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Comando_SQL.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    Dt_Matriz = Datos.P_Datos_Matriz_Cuenta;
                    foreach (DataRow Renglon in Dt_Matriz.Rows)
                    {
                        //Consulta para la obtención del último consecutivo dado de alta en la tabla de Matriz de cuentas
                        Mi_SQL = "SELECT ISNULL(MAX (" + Cat_Con_Matriz_De_Cuentas.Campo_Matriz_ID + "),'00000') ";
                        Mi_SQL = Mi_SQL + "FROM " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas;
                        Comando_SQL.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                        Matriz_ID = Comando_SQL.ExecuteScalar();

                        //Valida si el ID es nulo para asignarle automaticamente el primer registro
                        if (Convert.IsDBNull(Matriz_ID))
                        {
                            Matriz_ID = "00001";
                        }
                        //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                        else
                        {
                            Matriz_ID = String.Format("{0:00000}", Convert.ToInt32(Matriz_ID) + 1);
                        }

                        Mi_SQL = "INSERT INTO " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + " (" +
                                 Cat_Con_Matriz_De_Cuentas.Campo_Matriz_ID + ", " + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_ID_Debe + ", " +
                                 Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_ID_Haber + ", " + Cat_Con_Matriz_De_Cuentas.Campo_Numero_Relaciones + ", " +
                                 Cat_Con_Matriz_De_Cuentas.Campo_Usuario_Creo + ", " + Cat_Con_Matriz_De_Cuentas.Campo_Fecha_Creo +
                                 ") VALUES(" + "'" + Matriz_ID + "', '" + Renglon["CUENTA_ID_DEBE"].ToString() + "', '" + Datos.P_Cuenta_ID_Haber + "', " +
                                 Datos.P_Numero_Relaciones + ", '" +
                                Datos.P_Nombre_Usuario + "', GETDATE()" + ")";
                        Comando_SQL.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                        Comando_SQL.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    }
                    Transaccion_SQL.Commit(); //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }

                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
            ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Eliminar_Cuenta
            /// DESCRIPCION : Elimina el Nivel que fue seleccionada por el usuario de la BD
            /// PARAMETROS  : Datos: Obtiene que Nivel desea eliminar de la BD
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 29-diciembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
        public static void Eliminar_Cuenta(Cls_Cat_Con_Matriz_Negocio Datos)
        {
            String Mi_SQL; //Variable de Consulta para la Eliminación del Nivel
         try
            {
            Mi_SQL = "DELETE FROM " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas;
            Mi_SQL += " WHERE " + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_ID_Haber + " ='" + Datos.P_Cuenta_ID_Haber + "'";
           SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
         catch (SqlException Ex)
         {
             throw new Exception("Error: " + Ex.Message);
         }

         catch (DBConcurrencyException Ex)
         {
             throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
         }
         catch (Exception Ex)
         {
             throw new Exception("Error: " + Ex.Message);
         }
         finally
         {
         }
        }*/
        #endregion

        #region (Métodos Consulta)

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Cuentas_Contables
            ///DESCRIPCIÓN          : consulta para obtener los datos de las cuentas contables
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 12/Enero/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Cuentas_Contables(Cls_Cat_Con_Matriz_Negocio Matriz_Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + "  +' - '+ ");
                    Mi_Sql.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " AS CUENTA, ");
                    Mi_Sql.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                    Mi_Sql.Append(" FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Con_Niveles.Tabla_Cat_Con_Niveles);
                    Mi_Sql.Append(" ON " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Nivel_ID);
                    Mi_Sql.Append(" = " + Cat_Con_Niveles.Tabla_Cat_Con_Niveles + "." + Cat_Con_Niveles.Campo_Nivel_ID);
                    Mi_Sql.Append(" WHERE " + Cat_Con_Niveles.Tabla_Cat_Con_Niveles + "." + Cat_Con_Niveles.Campo_Descripcion);
                    Mi_Sql.Append(" = '" + Matriz_Negocio.P_Nivel + "'");
                    Mi_Sql.Append(" ORDER BY " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las cuentas contables. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Matriz
            ///DESCRIPCIÓN          : consulta para obtener los datos de la matriz
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 12/Enero/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Matriz(Cls_Cat_Con_Matriz_Negocio Matriz_Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + "." + Cat_Con_Matriz_De_Cuentas.Campo_Matriz_ID  + ", ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + "." + Cat_Con_Matriz_De_Cuentas.Campo_No_Matriz + ", ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + "." + Cat_Con_Matriz_De_Cuentas.Campo_Nombre + ", ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + "." + Cat_Con_Matriz_De_Cuentas.Campo_Tipo_Gasto + ", ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + "." + Cat_Con_Matriz_De_Cuentas.Campo_Tipo_Matriz + " AS TIPO, ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + "." + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_Abono + " AS CUENTA_ID_ABONO, ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + "." + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_Cargo + " AS CUENTA_ID_CARGO, ");
                    Mi_Sql.Append("(SELECT " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " + ' -  '+ ");
                    Mi_Sql.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion);
                    Mi_Sql.Append(" FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                    Mi_Sql.Append(" WHERE " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + "." + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_Abono);
                    Mi_Sql.Append(" = " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID +") AS CUENTA_ABONO, ");
                    Mi_Sql.Append("(SELECT " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " + ' -  '+ ");
                    Mi_Sql.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion);
                    Mi_Sql.Append(" FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                    Mi_Sql.Append(" WHERE " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + "." + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_Cargo);
                    Mi_Sql.Append(" = " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ") AS CUENTA_CARGO ");
                    Mi_Sql.Append(" FROM " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas);

                    if(!String.IsNullOrEmpty(Matriz_Negocio.P_No_Matriz) && !String.IsNullOrEmpty(Matriz_Negocio.P_Tipo_Matriz))
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + "." + Cat_Con_Matriz_De_Cuentas.Campo_No_Matriz);
                        Mi_Sql.Append(" = '" + Matriz_Negocio.P_No_Matriz +"'");
                        Mi_Sql.Append(" AND " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + "." + Cat_Con_Matriz_De_Cuentas.Campo_Tipo_Matriz);
                        Mi_Sql.Append(" = '" + Matriz_Negocio.P_Tipo_Matriz + "'");
                    }

                    Mi_Sql.Append(" ORDER BY " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + "." + Cat_Con_Matriz_De_Cuentas.Campo_Matriz_ID + " ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de la matriz. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Cuenta_Matriz
            ///DESCRIPCIÓN          : Metodo para dar de alta los datos de la matriz
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 12/Enero/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static void Alta_Cuenta_Matriz(Cls_Cat_Con_Matriz_Negocio Matriz_Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();

                try
                {
                    Mi_Sql.Append("INSERT INTO " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas );
                    Mi_Sql.Append("(" + Cat_Con_Matriz_De_Cuentas.Campo_Matriz_ID + ", ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_No_Matriz + ", ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_Nombre + ", ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_Tipo_Matriz + ", ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_Tipo_Gasto + ", ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_Abono + ", ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_Cargo + ", ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_Usuario_Creo + ", ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_Fecha_Creo + ") ");
                    Mi_Sql.Append(" VALUES('" + Obtener_Consecutivo_ID() + "', ");
                    Mi_Sql.Append("'" + Matriz_Negocio.P_No_Matriz + "', ");
                    Mi_Sql.Append("'" + Matriz_Negocio.P_Nombre.ToUpper() + "', ");
                    Mi_Sql.Append("'" + Matriz_Negocio.P_Tipo_Matriz + "', ");
                    Mi_Sql.Append("'" + Matriz_Negocio.P_Tipo_Gasto + "', ");
                    Mi_Sql.Append("'" + Matriz_Negocio.P_Cuenta_Abono + "', ");
                    Mi_Sql.Append("'" + Matriz_Negocio.P_Cuenta_Cargo + "', ");
                    Mi_Sql.Append("'" + Matriz_Negocio.P_Usuario_Creo + "', GETDATE())");

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar insertar los registros. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Eliminar_Cuenta_Matriz
            ///DESCRIPCIÓN          : Metodo para dar de eliminar los datos de la matriz
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 13/Enero/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static void Eliminar_Cuenta_Matriz(Cls_Cat_Con_Matriz_Negocio Matriz_Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();

                try
                {
                    Mi_Sql.Append("DELETE " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas);
                    Mi_Sql.Append(" WHERE " + Cat_Con_Matriz_De_Cuentas.Campo_Matriz_ID + " = '" + Matriz_Negocio.P_Matriz_ID + "'");

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar eliminar los registros. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Actualizar_Cuenta_Matriz
            ///DESCRIPCIÓN          : Metodo para dar de alta los datos de la matriz
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 13/Enero/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static void Actualizar_Cuenta_Matriz(Cls_Cat_Con_Matriz_Negocio Matriz_Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();

                try
                {
                    Mi_Sql.Append("UPDATE " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas);
                    Mi_Sql.Append(" SET " + Cat_Con_Matriz_De_Cuentas.Campo_No_Matriz + " = '" + Matriz_Negocio.P_No_Matriz + "', ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_Nombre + " = '" + Matriz_Negocio.P_Nombre.ToUpper() + "', ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_Tipo_Matriz + " = '" + Matriz_Negocio.P_Tipo_Matriz + "', ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_Tipo_Gasto + " = '" + Matriz_Negocio.P_Tipo_Gasto + "', ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_Abono + " = '" + Matriz_Negocio.P_Cuenta_Abono + "', ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_Cargo + " = '" + Matriz_Negocio.P_Cuenta_Cargo + "', ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_Usuario_Modifico + " = '" + Matriz_Negocio.P_Usuario_Modifico + "', ");
                    Mi_Sql.Append(Cat_Con_Matriz_De_Cuentas.Campo_Fecha_Modifico + " = GETDATE() ");
                    Mi_Sql.Append(" WHERE " + Cat_Con_Matriz_De_Cuentas.Campo_Matriz_ID + " = '" + Matriz_Negocio.P_Matriz_ID + "'");

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar insertar los registros. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Consecutivo_ID
            ///DESCRIPCIÓN          : Metodo para obtener el consecutivo
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 13/Enero/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static String Obtener_Consecutivo_ID()
            {
                String Consecutivo = "";
                StringBuilder Mi_SQL = new StringBuilder();
                object Consecutivo_Id; //Obtiene el ID con la cual se guardo los datos en la base de datos

                Mi_SQL.Append("SELECT ISNULL(MAX (" + Cat_Con_Matriz_De_Cuentas.Campo_Matriz_ID + "), '00000')");
                Mi_SQL.Append(" FROM " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas);

                Consecutivo_Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (Convert.IsDBNull(Consecutivo_Id))
                {
                    Consecutivo = "00001";
                }
                else
                {
                    Consecutivo = string.Format("{0:00000}", Convert.ToInt32(Consecutivo_Id) + 1);
                }
                return Consecutivo;
            }//fin de consecutivo 


        /*
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Matriz
        /// DESCRIPCION : Consulta las cuentas en la matriz que estan dadas de alta en la BD
        ///               con todos sus datos
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 28-diciembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Matriz(Cls_Cat_Con_Matriz_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta de los Niveles

            try
            {
                Mi_SQL = "SELECT distinct("+ Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_ID_Haber +"),"+Cat_Con_Cuentas_Contables.Campo_Cuenta+","+ Cat_Con_Cuentas_Contables.Campo_Descripcion ;
                Mi_SQL += " FROM " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + ", " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " WHERE ";
                Mi_SQL += Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = " + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_ID_Haber;
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Matriz
        /// DESCRIPCION : Consulta las cuentas en la matriz que estan dadas de alta en la BD
        ///               con todos sus datos
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 28-diciembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Matriz_Detalles(Cls_Cat_Con_Matriz_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta de los Niveles

            try
            {
                Mi_SQL = "SELECT Matriz.*, Cuenta_Haber." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " as Descripcion_Haber, Cuenta_Debe." + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                Mi_SQL += " as Descripcion_Debe,  Cuenta_Debe."+ Cat_Con_Cuentas_Contables.Campo_Cuenta+" as Cuenta_Debe, Cuenta_Haber."+Cat_Con_Cuentas_Contables.Campo_Cuenta+" as Cuenta_haber FROM " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + " Matriz ,";
                Mi_SQL += Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " Cuenta_Haber, " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " Cuenta_Debe WHERE ";
                Mi_SQL += " Cuenta_Haber." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = Matriz." + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_ID_Haber + " AND Matriz." + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_ID_Debe + " = ";
                Mi_SQL += "Cuenta_Debe." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " Order by Matriz." + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_ID_Haber;
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Cuenta
        /// DESCRIPCION : Consulta la Cuenta contables que estan dados de
        ///               alta en la BD 
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 29-diciembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Cuenta(Cls_Cat_Con_Matriz_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta de los Niveles
            String Cuenta_ID;
            DataTable  dt_temporal;
            try
            {
                //Consulta los Niveles que estan dados de alta en la base de datos
                Mi_SQL = "";
                Mi_SQL = " select * from " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables  + " where " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " = '" + Datos.P_Cuenta + "'";
                dt_temporal= SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                if (dt_temporal.Rows.Count>0)
                {
                    Mi_SQL = "";
                    Mi_SQL = " SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ", " + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                    if (Datos.P_Cuenta != null)
                    {
                        Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " = '" + Datos.P_Cuenta + "'";
                    }
                    Cuenta_ID = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).ToString();
                    Mi_SQL = "";
                    Mi_SQL = " SELECT Matriz.*, Cuenta_Haber." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " as Descripcion_Haber, Cuenta_Debe." + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                    Mi_SQL += " as Descripcion_Debe,  Cuenta_Debe." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " as Cuenta_Debe, Cuenta_Haber." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " as Cuenta_haber FROM " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas + " Matriz ,";
                    Mi_SQL += Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " Cuenta_Haber, " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " Cuenta_Debe WHERE ";
                    Mi_SQL += " Cuenta_Haber." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = Matriz." + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_ID_Haber + " AND Matriz." + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_ID_Debe + " = ";
                    Mi_SQL += "Cuenta_Debe." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                    Mi_SQL += " AND Matriz." + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_ID_Haber + "='" + Cuenta_ID + "'";
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                else
                {
                    return null;
                }
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }

       */
        #endregion
    }
}