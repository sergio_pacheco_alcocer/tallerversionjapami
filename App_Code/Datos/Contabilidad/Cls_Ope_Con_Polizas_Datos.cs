﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Sessiones;
using JAPAMI.Cuentas_Contables.Negocio;
using System.Text;

namespace JAPAMI.Polizas.Datos
{
    public class Cls_Ope_Con_Polizas_Datos
    {
        #region (Operaciones [Alta - Actualizar - Eliminar])
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Poliza
        /// DESCRIPCION : 1.Consulta el último ID dado de alta para poder ingresar el siguiente
        ///               2. Da de Alta la Poliza en la BD con los datos proporcionados por el usuario
        /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  :  10-Julio-2011
        /// MODIFICO          : Salvador L. Rea Ayala
        /// FECHA_MODIFICO    : 10/Octubre/2011
        /// CAUSA_MODIFICACION: Se agregaron los nuevos campos a la sentencia de insercion.
        ///*******************************************************************************
        public static string[] Alta_Poliza(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
            Object No_Poliza = null;                //Obtiene el No con la cual se guardo los datos en la base de datos
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL = null;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            string[] Datos_Poliza = new string[4];

            if (Datos.P_Cmmd == null)
            {
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
            }
            else
            {
                Comando_SQL = Datos.P_Cmmd;
            }
            try
            {
                //Consulta para la obtención del último ID dado de alta 
                Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza;
                Mi_SQL += "),'000000000') FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas;
                Mi_SQL += " WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "'";
                Mi_SQL += " AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID+ "'";
                //Mi_SQL += " ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC";

                Comando_SQL.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                No_Poliza = Comando_SQL.ExecuteScalar();

                //Valida si el ID es nulo para asignarle automaticamente el primer registro
                if (Convert.IsDBNull(No_Poliza))
                {
                    Datos.P_No_Poliza = "0000000001";
                }
                //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                else
                {
                    Datos.P_No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                }
                //Consulta para la inserción del Empleado con los datos proporcionados por el usuario
                Mi_SQL = "INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + " (";
                Mi_SQL = Mi_SQL + Ope_Con_Polizas.Campo_No_Poliza + ", ";
                Mi_SQL = Mi_SQL + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", ";
                Mi_SQL = Mi_SQL + Ope_Con_Polizas.Campo_Mes_Ano + ", ";
                Mi_SQL = Mi_SQL + Ope_Con_Polizas.Campo_Fecha_Poliza + ", ";
                Mi_SQL = Mi_SQL + Ope_Con_Polizas.Campo_Concepto + ", ";
                Mi_SQL = Mi_SQL + Ope_Con_Polizas.Campo_Total_Debe + ", ";
                Mi_SQL = Mi_SQL + Ope_Con_Polizas.Campo_Total_Haber + ", ";
                Mi_SQL = Mi_SQL + Ope_Con_Polizas.Campo_No_Partidas + ", ";
                Mi_SQL = Mi_SQL + Cat_Empleados.Campo_Usuario_Creo + ", ";
                Mi_SQL = Mi_SQL + Cat_Empleados.Campo_Fecha_Creo + ", ";
                Mi_SQL = Mi_SQL + Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", ";
                Mi_SQL = Mi_SQL + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + ", " + Ope_Con_Polizas.Campo_Prefijo + ") VALUES (";

                Mi_SQL = Mi_SQL + "'" + Datos.P_No_Poliza + "', ";

                if (Datos.P_Tipo_Poliza_ID != null)
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Tipo_Poliza_ID + "', ";
                }
                else
                {
                    Mi_SQL = Mi_SQL + "NULL, ";
                }
                if (Datos.P_Mes_Ano != null)
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Mes_Ano + "', ";
                }
                else
                {
                    Mi_SQL = Mi_SQL + "NULL, ";
                }
                if (String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Poliza) != "01/01/1900")
                {
                    Mi_SQL = Mi_SQL + "'" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Poliza) + "', ";
                }
                else
                {
                    Mi_SQL = Mi_SQL + "NULL, ";
                }
                if (Datos.P_Concepto != null)
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Concepto + "', ";
                }
                else
                {
                    Mi_SQL = Mi_SQL + "NULL, ";
                }
                if (Datos.P_Total_Debe != 0)
                {
                    Mi_SQL = Mi_SQL + Datos.P_Total_Debe + ", ";
                }
                else
                {
                    Mi_SQL = Mi_SQL + "0, ";
                }
                if (Datos.P_Total_Haber != 0)
                {
                    Mi_SQL = Mi_SQL + Datos.P_Total_Haber + ", ";
                }
                else
                {
                    Mi_SQL = Mi_SQL + "0, ";
                }
                if (Datos.P_No_Partida > 0)
                {
                    Mi_SQL = Mi_SQL + Datos.P_No_Partida + ", ";
                }
                else
                {
                    Mi_SQL = Mi_SQL + "0, ";
                }
                if (Datos.P_Nombre_Usuario != null)
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Nombre_Usuario + "', GETDATE(), ";
                }
                else
                {
                    Mi_SQL = Mi_SQL + "NULL, GETDATE(), ";
                }
                if (Datos.P_Empleado_ID_Creo != null)
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Empleado_ID_Creo + "', ";
                }
                else
                {
                    Mi_SQL = Mi_SQL + "NULL, ";
                }
                if (Datos.P_Empleado_ID_Autorizo != null)
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Empleado_ID_Autorizo + "', ";
                }
                else
                {
                    Mi_SQL = Mi_SQL + "NULL, ";
                }

                //prefijo
                Mi_SQL += "'" + Datos.P_Prefijo + "') ";

                Comando_SQL.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                //Transaccion_SQL.Commit(); //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos

                //Generamos los registros de alta de las percepciones y deducciones que tendrá el empleado.
                Registro_Detalles_Poliza(Datos.P_Dt_Detalles_Polizas, Datos.P_No_Poliza, Datos.P_Tipo_Poliza_ID, Datos.P_Mes_Ano, Datos.P_Fecha_Poliza, Comando_SQL);

                if (Datos.P_Cmmd == null)
                {
                    Transaccion_SQL.Commit();
                }

                Datos_Poliza[0] = Datos.P_No_Poliza;
                Datos_Poliza[1] = Datos.P_Tipo_Poliza_ID;
                Datos_Poliza[2] = Datos.P_Mes_Ano;
                Datos_Poliza[3] = Convert.ToString(Datos.P_Fecha_Poliza);
                return Datos_Poliza;
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Conexion_Base.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Prepoliza
        /// DESCRIPCION : 1.Consulta el último ID dado de alta para poder ingresar el siguiente
        ///               2. Da de Alta la Pre-Poliza en la BD con los datos proporcionados por el usuario
        /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  :  12-SEPTIEMBRE-2012
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        //public static string[] Alta_Prepoliza(Cls_Ope_Con_Polizas_Negocio Datos)
        //{
        //    String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
        //    Object No_PrePoliza = null;                //Obtiene el No con la cual se guardo los datos en la base de datos
        //    SqlConnection Cn = new SqlConnection();
        //    SqlCommand Cmmd = new SqlCommand();
        //    SqlTransaction Trans = null;
        //    string[] Datos_Poliza = new string[4];
        //    if (Datos.P_Cmmd != null)
        //    {
        //        Cmmd = Datos.P_Cmmd;
        //    }
        //    else
        //    {
        //        Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
        //        Cn.Open();
        //        Trans = Cn.BeginTransaction();
        //        Cmmd.Connection = Trans.Connection;
        //        Cmmd.Transaction = Trans;
        //    }
        //    try
        //    {
        //        //Consulta para la obtención del último ID dado de alta 
        //        Mi_SQL = "SELECT NVL(MAX(" + Ope_Con_Prepolizas.Campo_Prepoliza_ID;
        //        Mi_SQL += "),'000000000') FROM " + Ope_Con_Prepolizas.Tabla_Ope_Con_Prepolizas;
        //        Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
        //        No_PrePoliza = Cmmd.ExecuteScalar();

        //        //Valida si el ID es nulo para asignarle automaticamente el primer registro
        //        if (Convert.IsDBNull(No_PrePoliza))
        //        {
        //            Datos.P_Prepoliza_ID = "0000000001";
        //        }
        //        //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
        //        else
        //        {
        //            Datos.P_Prepoliza_ID = String.Format("{0:0000000000}", Convert.ToInt32(No_PrePoliza) + 1);
        //        }
        //        //Consulta para la inserción del Empleado con los datos proporcionados por el usuario
        //        Mi_SQL = "INSERT INTO " + Ope_Con_Prepolizas.Tabla_Ope_Con_Prepolizas + " (";
        //        Mi_SQL = Mi_SQL + Ope_Con_Prepolizas.Campo_Prepoliza_ID + ", ";
        //        Mi_SQL = Mi_SQL + Ope_Con_Prepolizas.Campo_Tipo_Poliza_ID + ", ";
        //        Mi_SQL = Mi_SQL + Ope_Con_Prepolizas.Campo_Concepto + ", ";
        //        Mi_SQL = Mi_SQL + Ope_Con_Prepolizas.Campo_Estatus + ", ";
        //        Mi_SQL = Mi_SQL + Ope_Con_Prepolizas.Campo_Total_Debe + ", ";
        //        Mi_SQL = Mi_SQL + Ope_Con_Prepolizas.Campo_Total_Haber + ", ";
        //        Mi_SQL = Mi_SQL + Ope_Con_Prepolizas.Campo_No_Partidas + ", ";
        //        Mi_SQL = Mi_SQL + Ope_Con_Prepolizas.Campo_Momento_Ingresos + ", ";
        //        Mi_SQL = Mi_SQL + Ope_Con_Prepolizas.Campo_Usuario_Creo + ", ";
        //        Mi_SQL = Mi_SQL + Ope_Con_Prepolizas.Campo_Fecha_Creo + ") VALUES (";
        //        Mi_SQL = Mi_SQL + "'" + Datos.P_Prepoliza_ID + "', ";
        //        if (Datos.P_Tipo_Poliza_ID != null)
        //        {
        //            Mi_SQL = Mi_SQL + "'" + Datos.P_Tipo_Poliza_ID + "', ";
        //        }
        //        else
        //        {
        //            Mi_SQL = Mi_SQL + "'', ";
        //        }
        //        if (Datos.P_Concepto != null)
        //        {
        //            Mi_SQL = Mi_SQL + "'" + Datos.P_Concepto + "', ";
        //        }
        //        else
        //        {
        //            Mi_SQL = Mi_SQL + "NULL, ";
        //        }
        //        if (Datos.P_Estatus != null)
        //        {
        //            Mi_SQL = Mi_SQL + "'" + Datos.P_Estatus + "', ";
        //        }
        //        else
        //        {
        //            Mi_SQL = Mi_SQL + "NULL, ";
        //        }
        //        if (Datos.P_Total_Debe > 0)
        //        {
        //            Mi_SQL = Mi_SQL + Datos.P_Total_Debe + ", ";
        //        }
        //        else
        //        {
        //            Mi_SQL = Mi_SQL + "0, ";
        //        }
        //        if (Datos.P_Total_Haber > 0)
        //        {
        //            Mi_SQL = Mi_SQL + Datos.P_Total_Haber + ", ";
        //        }
        //        else
        //        {
        //            Mi_SQL = Mi_SQL + "0, ";
        //        }
        //        if (Datos.P_No_Partida > 0)
        //        {
        //            Mi_SQL = Mi_SQL + Datos.P_No_Partida + ", ";
        //        }
        //        else
        //        {
        //            Mi_SQL = Mi_SQL + "0, ";
        //        }
        //        if (Datos.P_Momento != null)
        //        {
        //            Mi_SQL = Mi_SQL + "'" + Datos.P_Momento + "', ";
        //        }
        //        else
        //        {
        //            Mi_SQL = Mi_SQL + "NULL, ";
        //        }
        //        if (Datos.P_Nombre_Usuario != null)
        //        {
        //            Mi_SQL = Mi_SQL + "'" + Datos.P_Nombre_Usuario + "', SYSDATE)";
        //        }
        //        else
        //        {
        //            Mi_SQL = Mi_SQL + "NULL,SYSDATE)";
        //        }
        //        Cmmd.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
        //        Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
        //        if (Datos.P_Cmmd == null)
        //        {
        //            Trans.Commit();
        //        }
        //        Registro_Detalles_Prepoliza(Datos.P_Dt_Detalles_Polizas, Datos.P_Prepoliza_ID, Datos.P_Cmmd);
        //        Datos_Poliza[0] = Datos.P_Prepoliza_ID;
        //        return Datos_Poliza;
        //    }
        //    catch (SqlException Ex)
        //    {
        //        if (Datos.P_Cmmd == null)
        //        {
        //            Trans.Rollback();
        //        }
        //        throw new Exception("Error: " + Ex.Message);
        //    }
        //    catch (DBConcurrencyException Ex)
        //    {
        //        if (Datos.P_Cmmd == null)
        //        {
        //            Trans.Rollback();
        //        }
        //        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
        //    }
        //    catch (Exception Ex)
        //    {
        //        if (Datos.P_Cmmd == null)
        //        {
        //            Trans.Rollback();
        //        }
        //        throw new Exception("Error: " + Ex.Message);
        //    }
        //    finally
        //    {
        //        if (Datos.P_Cmmd == null)
        //        {
        //            Cn.Close();
        //        }

        //    }
        //}
        
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Registro_Detalles_Poliza
        /// DESCRIPCION : 
        /// PARAMETROS  : Dt_Datos.- Detalles de la póliza a aplicar 
        ///               No_Poliza.- No de Póliza al que se le aplicaran los detalles
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  :  10-Julio-2011
        /// MODIFICO          : Salvador L. Rea Ayala
        /// FECHA_MODIFICO    : 10/Octubre/2011
        /// CAUSA_MODIFICACION: Se agregaron los nuevos campos a la sentencia de insercion.
        ///*******************************************************************************
        private static void Registro_Detalles_Poliza(DataTable Dt_Datos, String No_Poliza, String Tipo_Poliza_ID, String Mes_Ano, DateTime Fecha_Poliza, SqlCommand Cmmd)
        {
            String Mi_SQL;                                                                      //Obtiene la cadena de inserción hacía la base de datos
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL = null;
            Object Consecutivo = null;                                                          //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
            Object Saldo;
            Object aux; //variable auxiliar para las consultas
            String Area_Funcional_ID = String.Empty; //variable para el area funcional
            String Partida_Especifica_ID = String.Empty; //variable para la partida especifica
            String Cuenta_Contable = String.Empty; //variable para la cuenta contable
            String Proyecto_Programa_ID = String.Empty; //Variable para el proyecto programa
            String Dependencia_ID = String.Empty; //variable para la dependencia
            DataTable Dt_Cuenta = new DataTable();
            Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            if (Cmmd == null)
            {
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
            }
            else
            {
                Comando_SQL = Cmmd;
            }

            try
            {
                foreach (DataRow Renglon in Dt_Datos.Rows)
                {
                    //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000') ";
                    Mi_SQL = Mi_SQL + "FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;

                    Comando_SQL.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Consecutivo = Comando_SQL.ExecuteScalar();

                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(Consecutivo))
                    {
                        Consecutivo = "1";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                    }
                    //////Consulta para obtener el ID del Area funcional
                    ////Mi_SQL = "SELECT " + Cat_Con_Parametros.Campo_Area_Funcional_ID_Poliza + " FROM " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros + " " +
                    ////    "WHERE " + Cat_Con_Parametros.Campo_Parametro_Contabilidad_ID + " = '00001' ";

                    //////Ejecutar consulta
                    ////Comando_SQL.CommandText = Mi_SQL;
                    ////aux = Comando_SQL.ExecuteScalar();

                    //////verificar si la consulta arrojo resultados
                    ////if (aux != null)
                    ////{
                    ////    if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                    ////    {
                    ////        Area_Funcional_ID = "'" + aux.ToString().Trim() + "'";
                    ////    }
                    ////    else
                    ////    {
                    ////        Area_Funcional_ID = "00001";
                    ////    }
                    ////}
                    ////else
                    ////{
                    ////    Area_Funcional_ID = "00001";
                    ////}
                    ////Consulta para tener el numero de la cuenta
                    //Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " " +
                    //    "WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = '" + Renglon["Cuenta_COntable_ID"].ToString().Trim() + "' ";
                    ////Ejecutar consulta
                    //Comando_SQL.CommandText = Mi_SQL;
                    //aux = Comando_SQL.ExecuteScalar();
                    #region "Consulta"
                    //Verificar si la consulta arrojo resultados
                    ////if (aux != null)
                    ////{
                    ////    if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                    ////    {
                    ////        //Colocar la cuenta contable
                    ////        Cuenta_Contable = aux.ToString().Trim();

                    ////        //Consulta para la partida especifica
                    ////        Mi_SQL = "SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " " +
                    ////            "WHERE " + Cat_Sap_Partidas_Especificas.Campo_Clave + " = '" + Cuenta_Contable.Substring(5, 4) + "' ";

                    ////        //Ejecutar consulta
                    ////        Comando_SQL.CommandText = Mi_SQL;
                    ////        aux = Comando_SQL.ExecuteScalar();

                    ////        //Verificar si al consulta arrojo resultados
                    ////        if (aux != null)
                    ////        {
                    ////            if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                    ////            {
                    ////                Partida_Especifica_ID = "'" + aux.ToString().Trim() + "'";
                    ////            }
                    ////            else
                    ////            {
                    ////                Partida_Especifica_ID = "NULL";
                    ////            }
                    ////        }
                    ////        else
                    ////        {
                    ////            Partida_Especifica_ID = "NULL";
                    ////        }

                    ////        //Consulta para el proyecto Programa
                    ////        Mi_SQL = "SELECT " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " " +
                    ////            "WHERE SUBSTRING(" + Cat_Sap_Proyectos_Programas.Campo_Clave + ",3,3) = '" + Cuenta_Contable.Substring(9, 3) + "' ";

                    ////        //ejecutar consulta
                    ////        Comando_SQL.CommandText = Mi_SQL;
                    ////        aux = Comando_SQL.ExecuteScalar();

                    ////        //verificar si la consulta arrojo resultados
                    ////        if (aux != null)
                    ////        {
                    ////            if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                    ////            {
                    ////                Proyecto_Programa_ID = "'" + aux.ToString().Trim() + "'";
                    ////            }
                    ////            else
                    ////            {
                    ////                Proyecto_Programa_ID = "NULL";
                    ////            }
                    ////        }
                    ////        else
                    ////        {
                    ////            Proyecto_Programa_ID = "NULL";
                    ////        }

                    ////        //Consulta para la dependencia
                    ////        Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " " +
                    ////            "WHERE " + Cat_Dependencias.Campo_Clave + " LIKE '%";

                    ////        //Verificar la longitud de la clave
                    ////        if (Convert.ToInt32(Cuenta_Contable.Substring(12, 3)).ToString().Trim().Length == 3)
                    ////        {
                    ////            Mi_SQL += Cuenta_Contable.Substring(12, 3) + "'";
                    ////        }
                    ////        else
                    ////        {
                    ////            Mi_SQL += Cuenta_Contable.Substring(13, 2) + "'";
                    ////        }

                    ////        //ejecutar consulta
                    ////        Comando_SQL.CommandText = Mi_SQL;
                    ////        aux = Comando_SQL.ExecuteScalar();

                    ////        //Verificar si la consulta arrojo resultados
                    ////        if (aux != null)
                    ////        {
                    ////            if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                    ////            {
                    ////                Dependencia_ID = "'" + aux.ToString().Trim() + "'";
                    ////            }
                    ////            else
                    ////            {
                    ////                Dependencia_ID = "NULL";
                    ////            }
                    ////        }
                    ////        else
                    ////        {
                    ////            Dependencia_ID = "NULL";
                    ////        }
                    ////    }
                    ////}
#endregion                    
                    Mi_SQL = "INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + " (" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " +
                        Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", " +
                        Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " , " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", " + Ope_Con_Polizas_Detalles.Campo_Debe + ", " +
                        Ope_Con_Polizas_Detalles.Campo_Haber + ", " + Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Consecutivo + ", "; 
                         if (Dt_Datos.Columns.Contains(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID) && Dt_Datos.Columns.Contains(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID))
                            {
                                Mi_SQL = Mi_SQL + Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID + ", " +
                                    Ope_Con_Polizas_Detalles.Campo_Partida_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Dependencia_ID + ",";
                            }
                         Mi_SQL = Mi_SQL + Ope_Con_Polizas_Detalles.Campo_Fecha + ") " +
                        "VALUES('" + No_Poliza + "', '" + Tipo_Poliza_ID + "', '" + Mes_Ano + "', " +
                        Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + ", " +
                        "'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "', " +
                        "'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString() + "', " +
                        Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", " +
                        Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", " +
                        Convert.ToDouble(0) + ", " + Consecutivo + ",";
                         if (Dt_Datos.Columns.Contains(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID) && Dt_Datos.Columns.Contains(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID))
                         {
                             if (!String.IsNullOrEmpty(Renglon[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID].ToString()))
                             {
                                 Mi_SQL = Mi_SQL + "'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID].ToString() + "',";
                             }
                             else
                             {
                                  Mi_SQL = Mi_SQL + "NULL,";
                             }
                             if (!String.IsNullOrEmpty(Renglon[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID].ToString()))
                             {
                                 Mi_SQL = Mi_SQL + "'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID].ToString() + "',";
                             }
                             else
                             {
                                 Mi_SQL = Mi_SQL + "NULL,";
                             }
                             if (!String.IsNullOrEmpty(Renglon[Ope_Con_Polizas_Detalles.Campo_Partida_ID].ToString()))
                             {
                                 Mi_SQL = Mi_SQL + "'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Partida_ID].ToString() + "',";
                             }
                             else
                             {
                                 Mi_SQL = Mi_SQL + "NULL,";
                             }
                             if (!String.IsNullOrEmpty(Renglon[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID].ToString()))
                             {
                                 Mi_SQL = Mi_SQL + "'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID].ToString() + "',";
                             }
                             else
                             {
                                 Mi_SQL = Mi_SQL + "NULL,";
                             }
                         }
                         Mi_SQL = Mi_SQL + "'" + String.Format("{0:dd/MM/yyyy}", Fecha_Poliza) + "')";

                    Comando_SQL.CommandText = Mi_SQL; //Asigna la inserción para ser ejecutada
                    Comando_SQL.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                }

                Consulta_Saldo_y_Actualiza(Fecha_Poliza, Dt_Datos, Comando_SQL);

                if (Cmmd == null)
                {
                    Transaccion_SQL.Commit(); //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
                }
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                if(Ex.Number == 547)
                    throw new Exception("*Error: " + Ex.Message);
                else
                    throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Cmmd == null)
                {
                    Conexion_Base.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Polizas
        /// DESCRIPCION : Modifica los datos de la Póliza con los datos que fueron proporcionados
        ///               por el usuario
        /// PARAMETROS  : Datos: Contiene los datos que serán modificados en la base de datos
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 11-Julio-2011
        /// MODIFICO          : Salvador L. Rea Ayala
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static void Modificar_Polizas(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Obtiene la cadena de modificación hacía la base de datos
            DataTable Ds_Consulta_Detalles; // se guardan los detalles que se eliminaran para realizar la actualizacion de los saldos
            DataTable Dt_Partidas_Modificadas = new DataTable();
            Object Consecutivo = null;
            String Partida = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Sql = new SqlDataAdapter();
            DataSet Ds_Sql = new DataSet();
            DataTable Dt_Detalle_Solicitud = new DataTable();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL = "UPDATE " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + " SET " +
                         Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "', " +
                         Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "', " +
                    //Ope_Con_Polizas.Campo_Fecha_Poliza + "= TO_DATE('" + String.Format("{0:MM/dd/yy}", Datos.P_Fecha_Poliza) + "','MM/DD/YY'), " +
                         Ope_Con_Polizas.Campo_Concepto + "='" + Datos.P_Concepto + "', " +
                         Ope_Con_Polizas.Campo_Total_Debe + " = " + Datos.P_Total_Debe + ", " +
                         Ope_Con_Polizas.Campo_Total_Haber + " = " + Datos.P_Total_Haber + ", " +
                         Ope_Con_Polizas.Campo_No_Partidas + " = " + Datos.P_No_Partida + ", " +
                         Ope_Con_Polizas.Campo_Usuario_Modifico + " = '" + Datos.P_Nombre_Usuario + "', " +
                         Ope_Con_Polizas.Campo_Fecha_Modifico + " = GETDATE(), " +
                         Ope_Con_Polizas.Campo_Prefijo + " = '" + Datos.P_Prefijo + "' " +
                         "WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "' " + 
                         "AND " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "' " + 
                         "AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "'";
                Cmmd.CommandText = Mi_SQL.ToString();
                Cmmd.ExecuteNonQuery();
                //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                //Consulta los detalles de la póliza que fueron dados de alta
                Mi_SQL = "SELECT * FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                         " WHERE " + Ope_Con_Polizas_Detalles.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "' AND " +
                         Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "' AND " +
                         Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "'";
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Sql.SelectCommand = Cmmd;
                Dt_Sql.Fill(Ds_Sql);
                Ds_Consulta_Detalles = Ds_Sql.Tables[0];
                // Ds_Consulta_Detalles=SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                foreach (DataRow Renglon in Ds_Consulta_Detalles.Rows)
                {
                    // se crea y se agregan columnas al Dt_Temporal
                    DataTable Dt_Temporal = new DataTable();
                    Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_No_Poliza, typeof(System.String));
                    DataRow row;
                    // Se elimina la primera partida del detalle de la poliza
                    Mi_SQL = "DELETE FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                        " WHERE " + Ope_Con_Polizas_Detalles.Campo_No_Poliza + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_No_Poliza].ToString() +
                        "' AND " + Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Mes_Ano].ToString() +
                        "' AND " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID].ToString() +
                        "' AND " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() +
                        "' AND " + Ope_Con_Polizas_Detalles.Campo_Consecutivo + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Consecutivo].ToString() + "'";
                    Cmmd.CommandText = Mi_SQL.ToString();
                    Cmmd.ExecuteNonQuery();
                    // SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    //se agrega una fila al Dt_Temporal
                    row = Dt_Temporal.NewRow();
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
                    row[Ope_Con_Polizas_Detalles.Campo_No_Poliza] = Renglon[Ope_Con_Polizas_Detalles.Campo_No_Poliza].ToString();
                    Dt_Temporal.Rows.Add(row);
                    Dt_Temporal.AcceptChanges();//Actualiza el Datatable
                    Consulta_Saldo_y_Actualiza(Convert.ToDateTime(Renglon[Ope_Con_Polizas_Detalles.Campo_Fecha].ToString()), Dt_Temporal, Cmmd);
                    Dt_Temporal = null;
                }
                Registro_Detalles_Poliza(Datos.P_Dt_Detalles_Polizas, Datos.P_No_Poliza, Datos.P_Tipo_Poliza_ID, Datos.P_Mes_Ano, Datos.P_Fecha_Poliza, Cmmd);
                Dt_Partidas_Modificadas = Datos.P_Dt_Partidas_Modificadas;
                if (Dt_Partidas_Modificadas.Rows.Count > 0)
                {
                    foreach (DataRow Fila in Dt_Partidas_Modificadas.Rows)
                    {
                        //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                        Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Partida_Poliza_Modif.Campo_No_Partida_Modificada + "),'0000000000') ";
                        Mi_SQL = Mi_SQL + "FROM " + Ope_Con_Partida_Poliza_Modif.Tabla_Ope_Con_Partida_Poliza_Modificada;
                        Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                        Consecutivo = Cmmd.ExecuteScalar();
                        //Consecutivo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).ToString();
                        //Valida si el ID es nulo para asignarle automaticamente el primer registro
                        if (Convert.IsDBNull(Consecutivo))
                        {
                            Partida = "0000000001";
                        }
                        //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                        else
                        {
                            Partida = String.Format("{0:0000000000}", Convert.ToInt32(Consecutivo) + 1);
                        }

                        Mi_SQL = "INSERT INTO " + Ope_Con_Partida_Poliza_Modif.Tabla_Ope_Con_Partida_Poliza_Modificada + " (" +
                                 Ope_Con_Partida_Poliza_Modif.Campo_No_Partida_Modificada + ", " + Ope_Con_Partida_Poliza_Modif.Campo_Tipo_Poliza_ID + ", " +
                                 Ope_Con_Partida_Poliza_Modif.Campo_Mes_Anio + ", " + Ope_Con_Partida_Poliza_Modif.Campo_No_Poliza + ", " +
                                 Ope_Con_Partida_Poliza_Modif.Campo_Cuenta_Contable_id + ", " + Ope_Con_Partida_Poliza_Modif.Campo_Concepto + ", " +
                                 Ope_Con_Partida_Poliza_Modif.Campo_Debe + ", " + Ope_Con_Partida_Poliza_Modif.Campo_Haber + ", " +
                                 Ope_Con_Partida_Poliza_Modif.Campo_Fecha_Creo + ", " +
                                 Ope_Con_Partida_Poliza_Modif.Campo_Usuario_Creo +
                                 ") VALUES(" + "'" + Partida + "', '" + Fila["TIPO_POLIZA_ID"].ToString() + "', '" + Fila["MES_ANIO"].ToString() + "', '" +
                                 Fila["NO_POLIZA"].ToString() + "', '" +
                                 Fila["Cuenta_Contable_ID"].ToString() + "', '" +
                                 Fila["Concepto"].ToString() + "', " +
                                 Convert.ToDouble(Fila["DEBE"].ToString()) + ", " +
                                 Convert.ToDouble(Fila["HABER"].ToString()) + ", " + "GETDATE(),'" +
                                 Cls_Sessiones.Nombre_Empleado.ToString() + "')";
                        Cmmd.CommandText = Mi_SQL.ToString();
                        Cmmd.ExecuteNonQuery();
                        //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                }
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Prepoliza
        /// DESCRIPCION : Modifica los datos de la Póliza con los datos que fueron proporcionados
        ///               por el usuario
        /// PARAMETROS  : Datos: Contiene los datos que serán modificados en la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 13-Septiembre-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        ////public static void Modificar_Prepoliza(Cls_Ope_Con_Polizas_Negocio Datos)
        ////{
        ////    String Mi_SQL; //Obtiene la cadena de modificación hacía la base de datos
        ////    DataTable Ds_Consulta_Detalles; // se guardan los detalles que se eliminaran para realizar la actualizacion de los saldos
        ////    SqlConnection Cn = new SqlConnection();
        ////    SqlCommand Cmmd = new SqlCommand();
        ////    SqlTransaction Trans = null;
        ////    if (Datos.P_Cmmd != null)
        ////    {
        ////        Cmmd = Datos.P_Cmmd;
        ////    }
        ////    else
        ////    {
        ////        Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
        ////        Cn.Open();
        ////        Trans = Cn.BeginTransaction();
        ////        Cmmd.Connection = Trans.Connection;
        ////        Cmmd.Transaction = Trans;
        ////    }
        ////    try
        ////    {
        ////        Mi_SQL = "UPDATE " + Ope_Con_Prepolizas.Tabla_Ope_Con_Prepolizas + " SET " +
        ////                 Ope_Con_Prepolizas.Campo_Estatus + " = '" + Datos.P_Estatus + "' WHERE " +
        ////                 Ope_Con_Prepolizas.Campo_Prepoliza_ID + " = '" + Datos.P_Prepoliza_ID + "'";
        ////        Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
        ////        Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
        ////        //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        ////        if (Datos.P_Cmmd == null)
        ////        {
        ////            Trans.Commit();
        ////        }
        ////    }
        ////    catch (SqlException Ex)
        ////    {
        ////        if (Datos.P_Cmmd == null)
        ////        {
        ////            Trans.Rollback();
        ////        }
        ////        if (Trans != null)
        ////        {
        ////            Trans.Rollback();
        ////        }
        ////        throw new Exception("Error: " + Ex.Message);
        ////    }
        ////    catch (DBConcurrencyException Ex)
        ////    {
        ////        if (Trans != null)
        ////        {
        ////            Trans.Rollback();
        ////        }
        ////        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        ////    }
        ////    catch (Exception Ex)
        ////    {
        ////        if (Trans != null)
        ////        {
        ////            Trans.Rollback();
        ////        }
        ////        throw new Exception("Error: " + Ex.Message);
        ////    }
        ////    finally
        ////    {
        ////        if (Datos.P_Cmmd == null)
        ////        {
        ////            Cn.Close();
        ////        }

        ////    }
        ////}
        
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Eliminar_Poliza
        /// DESCRIPCION : Elimina la Póliza y sus detalles que fue seleccionada por el usuario de la BD
        /// PARAMETROS  : Datos: Obtiene que Póliza desea eliminar de la BD
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 11-Julio-2011
        /// MODIFICO          : Salvador L. Rea Ayala
        /// FECHA_MODIFICO    : 10/Octubre/2011
        /// CAUSA_MODIFICACION: Se agregaron los nuevos parametros a la sentencia de eliminacion.
        ///*******************************************************************************
        public static void Eliminar_Poliza(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable de Consulta para la eliminación del Empleado
            DataTable Ds_Consulta_Detalles;

            try
            {
                Mi_SQL = "SELECT * FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                         " WHERE " + Ope_Con_Polizas_Detalles.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "' AND " +
                         Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "' AND " +
                         Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "'";
                Ds_Consulta_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                foreach (DataRow Renglon in Ds_Consulta_Detalles.Rows)
                {
                    // se crea y se agregan columnas al Dt_Temporal
                    DataTable Dt_Temporal = new DataTable();
                    Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_No_Poliza, typeof(System.String));
                    DataRow row;
                    // Se elimina la primera partida del detalle de la poliza
                    Mi_SQL = "DELETE FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                        " WHERE " + Ope_Con_Polizas_Detalles.Campo_No_Poliza + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_No_Poliza].ToString() +
                        "' AND " + Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Mes_Ano].ToString() +
                        "' AND " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID].ToString() +
                        "' AND " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() +
                        "' AND " + Ope_Con_Polizas_Detalles.Campo_Consecutivo + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Consecutivo].ToString() + "'";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    //se agrega una fila al Dt_Temporal
                    row = Dt_Temporal.NewRow();
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
                    row[Ope_Con_Polizas_Detalles.Campo_No_Poliza] = Renglon[Ope_Con_Polizas_Detalles.Campo_No_Poliza].ToString();
                    Dt_Temporal.Rows.Add(row);
                    Dt_Temporal.AcceptChanges();//Actualiza el Datatable
                    Consulta_Saldo_y_Actualiza(Convert.ToDateTime(Renglon[Ope_Con_Polizas_Detalles.Campo_Fecha].ToString()), Dt_Temporal);
                    Dt_Temporal = null;
                }
                //Elimina los datos generales de la póliza
                Mi_SQL = "DELETE FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas;
                Mi_SQL += " WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "'";
                Mi_SQL += " AND " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "'";
                Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "'";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consulta_Prepolizas
        ///DESCRIPCIÓN          : consulta para obtener los datos de los programas que tienen esa fuente de financiamiento seleccionada
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 12/Septiembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        //////public static DataTable Consulta_Prepolizas(Cls_Ope_Con_Polizas_Negocio Datos)
        //////{
        //////    StringBuilder Mi_Sql = new StringBuilder();
        //////    SqlConnection Cn = new SqlConnection();
        //////    SqlCommand Cmmd = new SqlCommand();
        //////    SqlTransaction Trans = null;
        //////    SqlDataAdapter Dt_Sql = new SqlDataAdapter();
        //////    DataSet Ds_Sql = new DataSet();
        //////    if (Datos.P_Cmmd != null)
        //////    {
        //////        Cmmd = Datos.P_Cmmd;
        //////    }
        //////    else
        //////    {
        //////        Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
        //////        Cn.Open();
        //////        Trans = Cn.BeginTransaction();
        //////        Cmmd.Connection = Trans.Connection;
        //////        Cmmd.Transaction = Trans;
        //////    }
        //////    try
        //////    {
        //////        //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
        //////        Mi_Sql.Append("SELECT " + Ope_Con_Prepolizas.Tabla_Ope_Con_Prepolizas + "." + Ope_Con_Prepolizas.Campo_Prepoliza_ID + " || ' - ' || ");
        //////        Mi_Sql.Append(Ope_Con_Prepolizas.Tabla_Ope_Con_Prepolizas + "." + Ope_Con_Prepolizas.Campo_Concepto + " AS CONCEPTO, " + Ope_Con_Prepolizas.Tabla_Ope_Con_Prepolizas + "." + Ope_Con_Prepolizas.Campo_Prepoliza_ID + " FROM  ");
        //////        Mi_Sql.Append(Ope_Con_Prepolizas.Tabla_Ope_Con_Prepolizas);
        //////        if (!String.IsNullOrEmpty(Datos.P_Estatus))
        //////        {
        //////            Mi_Sql.Append(" WHERE  " + Ope_Con_Prepolizas.Tabla_Ope_Con_Prepolizas + "." + Ope_Con_Prepolizas.Campo_Estatus + " = '" + Datos.P_Estatus + "'");
        //////        }
        //////        Mi_Sql.Append(" ORDER BY " + Ope_Con_Prepolizas.Tabla_Ope_Con_Prepolizas + "." + Ope_Con_Prepolizas.Campo_Prepoliza_ID + " ASC");
        //////        Cmmd.CommandText = Mi_Sql.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
        //////        Dt_Sql.SelectCommand = Cmmd;
        //////        Dt_Sql.Fill(Ds_Sql);
        //////        if (Datos.P_Cmmd == null)
        //////        {
        //////            Trans.Commit();
        //////        }
        //////        return Ds_Sql.Tables[0];//SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
        //////    }
        //////    catch (SqlException Ex)
        //////    {
        //////        if (Datos.P_Cmmd == null)
        //////        {
        //////            Trans.Rollback();
        //////        }
        //////        if (Trans != null)
        //////        {
        //////            Trans.Rollback();
        //////        }
        //////        throw new Exception("Error: " + Ex.Message);
        //////    }
        //////    catch (DBConcurrencyException Ex)
        //////    {
        //////        if (Trans != null)
        //////        {
        //////            Trans.Rollback();
        //////        }
        //////        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        //////    }
        //////    catch (Exception Ex)
        //////    {
        //////        if (Trans != null)
        //////        {
        //////            Trans.Rollback();
        //////        }
        //////        throw new Exception("Error: " + Ex.Message);
        //////    }
        //////    finally
        //////    {
        //////        if (Datos.P_Cmmd == null)
        //////        {
        //////            Cn.Close();
        //////        }
        //////    }
        //////}
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consulta_Prepoliza_Datos
        ///DESCRIPCIÓN          : consulta para obtener los datos de los programas que tienen esa fuente de financiamiento seleccionada
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 12/Septiembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        //////public static DataTable Consulta_Prepoliza_Datos(Cls_Ope_Con_Polizas_Negocio Datos)
        //////{
        //////    StringBuilder Mi_Sql = new StringBuilder();
        //////    String Filtro = "NO";
        //////    try
        //////    {
        //////        //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
        //////        Mi_Sql.Append("SELECT  * FROM " + Ope_Con_Prepolizas.Tabla_Ope_Con_Prepolizas);
        //////        if (!String.IsNullOrEmpty(Datos.P_Prepoliza_ID))
        //////        {
        //////            Mi_Sql.Append(" WHERE   " + Ope_Con_Prepolizas.Tabla_Ope_Con_Prepolizas + "." + Ope_Con_Prepolizas.Campo_Prepoliza_ID + " = '" + Datos.P_Prepoliza_ID + "'");
        //////            Filtro = "SI";
        //////        }
        //////        if (!String.IsNullOrEmpty(Datos.P_Estatus))
        //////        {
        //////            if (Filtro == "NO")
        //////            {
        //////                Mi_Sql.Append(" WHERE  " + Ope_Con_Prepolizas.Tabla_Ope_Con_Prepolizas + "." + Ope_Con_Prepolizas.Campo_Estatus + " = '" + Datos.P_Estatus + "'");
        //////            }
        //////            else
        //////            {
        //////                Mi_Sql.Append(" AND  " + Ope_Con_Prepolizas.Tabla_Ope_Con_Prepolizas + "." + Ope_Con_Prepolizas.Campo_Estatus + " = '" + Datos.P_Estatus + "'");
        //////            }

        //////        }
        //////        Mi_Sql.Append(" ORDER BY " + Ope_Con_Prepolizas.Tabla_Ope_Con_Prepolizas + "." + Ope_Con_Prepolizas.Campo_Prepoliza_ID + " ASC");

        //////        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
        //////    }
        //////    catch (Exception Ex)
        //////    {
        //////        throw new Exception("Error al intentar consultar los registros de los programas. Error: [" + Ex.Message + "]");
        //////    }
        //////}
         
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cancelar_Poliza
        /// DESCRIPCION : Cancela la Póliza y sus detalles que fue seleccionada por el usuario de la BD
        /// PARAMETROS  : Datos: Obtiene que Póliza desea cancelar de la BD
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 24/Octubre/2012
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static void Cancelar_Poliza(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_Sql; //Variable de Consulta para la eliminación del Empleado
            DataTable Ds_Consulta_Detalles;
            DataTable Dt_Detalles_Modificados = new DataTable();// se copian los detalles originales pero se les asigan al debe y haberCero para que quede registrada
            Object Consecutivo = null;
            String Partida;//lleva el consecutivo de la tabla de partidas modificadas
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Sql = new SqlDataAdapter();
            DataSet Ds_Sql = new DataSet();
            DataTable Dt_Detalle_Solicitud = new DataTable();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_Sql = "SELECT * FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                         " WHERE " + Ope_Con_Polizas_Detalles.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "' AND " +
                         Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "' AND " +
                         Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "'";
                Cmmd.CommandText = Mi_Sql.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Sql.SelectCommand = Cmmd;
                Dt_Sql.Fill(Ds_Sql);
                Ds_Consulta_Detalles = Ds_Sql.Tables[0];
                // Ds_Consulta_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql).Tables[0];
                if (Dt_Detalles_Modificados.Rows.Count <= 0)
                {
                    Dt_Detalles_Modificados.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.String));
                    Dt_Detalles_Modificados.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Detalles_Modificados.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                    Dt_Detalles_Modificados.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                    Dt_Detalles_Modificados.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                    Dt_Detalles_Modificados.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
                }
                //se agregan los detalles con cargo y abono en cero para volver a realizar el registro
                foreach (DataRow Filas in Ds_Consulta_Detalles.Rows)
                {
                    DataRow row = Dt_Detalles_Modificados.NewRow(); //Crea un nuevo registro a la tabla
                    //Asigna los valores al nuevo registro creado a la tabla
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = Filas[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Filas[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Filas[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString();
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = "";
                    Dt_Detalles_Modificados.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Detalles_Modificados.AcceptChanges();
                }
                foreach (DataRow Renglon in Ds_Consulta_Detalles.Rows)
                {
                    // se crea y se agregan columnas al Dt_Temporal
                    DataTable Dt_Temporal = new DataTable();
                    Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_No_Poliza, typeof(System.String));
                    DataRow row;
                    // Se elimina la primera partida del detalle de la poliza
                    Mi_Sql = "DELETE FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                        " WHERE " + Ope_Con_Polizas_Detalles.Campo_No_Poliza + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_No_Poliza].ToString() +
                        "' AND " + Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Mes_Ano].ToString() +
                        "' AND " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID].ToString() +
                        "' AND " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() +
                        "' AND " + Ope_Con_Polizas_Detalles.Campo_Consecutivo + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Consecutivo].ToString() + "'";
                    Cmmd.CommandText = Mi_Sql.ToString();
                    Cmmd.ExecuteNonQuery();
                    //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                    //se agrega una fila al Dt_Temporal
                    row = Dt_Temporal.NewRow();
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
                    row[Ope_Con_Polizas_Detalles.Campo_No_Poliza] = Renglon[Ope_Con_Polizas_Detalles.Campo_No_Poliza].ToString();
                    Dt_Temporal.Rows.Add(row);
                    Dt_Temporal.AcceptChanges();//Actualiza el Datatable
                    Consulta_Saldo_y_Actualiza(Convert.ToDateTime(Renglon[Ope_Con_Polizas_Detalles.Campo_Fecha].ToString()), Dt_Temporal, Cmmd);
                    Dt_Temporal = null;
                }

                //se actualiza el encabezado de la poliza
                Mi_Sql = "UPDATE " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + " SET " +
                       Ope_Con_Polizas.Campo_Concepto + "='(POLIZA CANCELADA)', " +
                       Ope_Con_Polizas.Campo_Total_Debe + " = " + Datos.P_Total_Debe + ", " +
                       Ope_Con_Polizas.Campo_Total_Haber + " = " + Datos.P_Total_Haber + ", " +
                       Ope_Con_Polizas.Campo_No_Partidas + " = " + Datos.P_No_Partida + ", " +
                       Ope_Con_Polizas.Campo_Usuario_Modifico + " = '" + Sessiones.Cls_Sessiones.Nombre_Empleado.ToString() + "', " +
                       Ope_Con_Polizas.Campo_Fecha_Modifico + " = GETDATE() WHERE " +
                       Ope_Con_Polizas.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "' AND " +
                       Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "' AND " +
                       Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "'";
                Cmmd.CommandText = Mi_Sql.ToString();
                Cmmd.ExecuteNonQuery();
                //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                //se registran los nuevos registro en la poliza de detalles 
                Registro_Detalles_Poliza(Dt_Detalles_Modificados, Datos.P_No_Poliza, Datos.P_Tipo_Poliza_ID, Datos.P_Mes_Ano, Datos.P_Fecha_Poliza, Cmmd);
                if (Ds_Consulta_Detalles.Rows.Count > 0)
                {
                    foreach (DataRow Fila in Ds_Consulta_Detalles.Rows)
                    {
                        //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                        Mi_Sql = "SELECT ISNULL(MAX(" + Ope_Con_Partida_Poliza_Modif.Campo_No_Partida_Modificada + "),'0000000000') ";
                        Mi_Sql = Mi_Sql + "FROM " + Ope_Con_Partida_Poliza_Modif.Tabla_Ope_Con_Partida_Poliza_Modificada;
                        Cmmd.CommandText = Mi_Sql; //Realiza la ejecuón de la obtención del ID del empleado
                        Consecutivo = Cmmd.ExecuteScalar();
                        // Consecutivo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql).ToString();
                        //Valida si el ID es nulo para asignarle automaticamente el primer registro
                        if (Convert.IsDBNull(Consecutivo))
                        {
                            Partida = "0000000001";
                        }
                        //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                        else
                        {
                            Partida = String.Format("{0:0000000000}", Convert.ToInt32(Consecutivo) + 1);
                        }

                        Mi_Sql = "INSERT INTO " + Ope_Con_Partida_Poliza_Modif.Tabla_Ope_Con_Partida_Poliza_Modificada + " (" +
                                 Ope_Con_Partida_Poliza_Modif.Campo_No_Partida_Modificada + ", " + Ope_Con_Partida_Poliza_Modif.Campo_Tipo_Poliza_ID + ", " +
                                 Ope_Con_Partida_Poliza_Modif.Campo_Mes_Anio + ", " + Ope_Con_Partida_Poliza_Modif.Campo_No_Poliza + ", " +
                                 Ope_Con_Partida_Poliza_Modif.Campo_Cuenta_Contable_id + ", " + Ope_Con_Partida_Poliza_Modif.Campo_Concepto + ", " +
                                 Ope_Con_Partida_Poliza_Modif.Campo_Debe + ", " + Ope_Con_Partida_Poliza_Modif.Campo_Haber + ", " +
                                 Ope_Con_Partida_Poliza_Modif.Campo_Fecha_Creo + ", " +
                                 Ope_Con_Partida_Poliza_Modif.Campo_Usuario_Creo +
                                 ") VALUES(" + "'" + Partida + "', '" + Fila["TIPO_POLIZA_ID"].ToString() + "', '" + Fila["MES_ANO"].ToString() + "', '" +
                                 Fila["NO_POLIZA"].ToString() + "', '" +
                                 Fila["Cuenta_Contable_ID"].ToString() + "', '" +
                                 Fila["Concepto"].ToString() + "', " +
                                 Convert.ToDouble(Fila["DEBE"].ToString()) + ", " +
                                 Convert.ToDouble(Fila["HABER"].ToString()) + ", " + "GETDATE(),'" +
                                 Cls_Sessiones.Nombre_Empleado.ToString() + "')";
                        Cmmd.CommandText = Mi_Sql.ToString();
                        Cmmd.ExecuteNonQuery();
                        //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                    }
                }
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Saldo_y_Actualiza
        /// DESCRIPCION : Consulta el ultimo saldo y lo actualiza 
        /// PARAMETROS  : Fecha de tipo DATETIME y DataTable con las partidas de la poliza 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 29/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static void Consulta_Saldo_y_Actualiza(DateTime Fecha_Poliza, DataTable Dt_Datos)
        {
            String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
            String Fecha;
            String Fecha_Saldo;
            String Cuenta_Contable_ID;
            DataTable Saldo;
            Double Saldo2;
            int contador;
            int Bandera;
            Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuenta = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            DataTable Dt_Cuenta_Tipo = new DataTable();
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            if (Conexion_Base.State != ConnectionState.Open)
            {
                Conexion_Base.Open(); //Abre la conexión a la base de datos            
            }
            Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
            Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
            Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
            SqlDataAdapter da = new SqlDataAdapter(Comando_SQL);
            try
            {
                foreach (DataRow Renglon in Dt_Datos.Rows)
                {
                    Cuenta_Contable_ID = Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
                    Rs_Cuenta.P_Cuenta_Contable_ID = Cuenta_Contable_ID;
                    Dt_Cuenta_Tipo = Rs_Cuenta.Consulta_Datos_Cuentas_Contables();
                    Bandera = 0;
                    Saldo2 = 0;
                    contador = 0;
                    //se le resta a la fecha un dia 
                    Fecha = string.Format("{0:dd/MM/yyyy}", Fecha_Poliza.AddDays(-1));
                    //Obtiene el ultimo movimiento de acuerdo a la fecha
                    Mi_SQL = "SELECT MAX(" + Ope_Con_Polizas_Detalles.Campo_Fecha + ")";
                    Mi_SQL += "FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                    Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + "='" + Cuenta_Contable_ID + "' ";
                    Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Fecha + " <= '" + Fecha + "'";
                    Comando_SQL.CommandText = Mi_SQL;
                    Fecha_Saldo = Comando_SQL.ExecuteScalar().ToString();
                    if (Fecha_Saldo != "" && Fecha_Saldo != null)
                    {
                        Fecha_Saldo = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Fecha_Saldo));
                        Mi_SQL = "SELECT " + Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Partida;
                        Mi_SQL += " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + " WHERE " + Ope_Con_Polizas_Detalles.Campo_Fecha + " ='" + Fecha_Saldo + "'";
                        Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + "='" + Cuenta_Contable_ID + "' ORDER BY " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " + Ope_Con_Polizas_Detalles.Campo_Consecutivo + ", ";
                        Mi_SQL += Ope_Con_Polizas_Detalles.Campo_Partida;
                        Comando_SQL.CommandText = Mi_SQL;
                        Saldo = new DataTable();
                        da.Fill(Saldo);
                        if (Saldo.Rows.Count > 0)
                        {
                            Saldo2 = Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Saldo].ToString());
                        }
                    }
                    else
                    {
                        Saldo2 = 0;
                        Bandera = 1;
                        Fecha_Saldo = Fecha;
                    }
                    //Actualiza saldos
                    Mi_SQL = "SELECT * FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                    Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + "='" + Cuenta_Contable_ID + "' ";
                    Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Fecha + " >= '" + Fecha_Saldo + "'";
                    Mi_SQL += " ORDER BY " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " + Ope_Con_Polizas_Detalles.Campo_Consecutivo + ", " + Ope_Con_Polizas_Detalles.Campo_Partida;
                    Comando_SQL.CommandText = Mi_SQL;
                    Saldo = new DataTable();
                    da.Fill(Saldo);                    
                    if (Saldo2 == 0 && Bandera == 1 && Saldo.Rows.Count > 0)
                    {
                        if (Dt_Cuenta_Tipo.Rows[0][Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta].ToString().Trim().ToUpper() == "DEUDOR")
                        {
                            Saldo2 = Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) - Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                        }
                        else
                        {
                            Saldo2 = Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) - Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                        }
                    }
                    if (Saldo.Rows.Count > 0)
                    {
                        while (contador < Saldo.Rows.Count)
                        {
                            if (contador > 0)
                            {
                                if (Dt_Cuenta_Tipo.Rows[0][Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta].ToString().Trim().ToUpper() == "DEUDOR")
                                {
                                    Saldo2 = Saldo2 + Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) - Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                                }
                                else
                                {
                                    Saldo2 = Saldo2 + Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) - Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                                }
                            }
                            Mi_SQL = "UPDATE " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                            Mi_SQL += " SET " + Ope_Con_Polizas_Detalles.Campo_Saldo + " = " + Saldo2;
                            Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_Consecutivo + " = " + Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Consecutivo].ToString();
                            Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Partida + " = " + Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Partida].ToString();
                            Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + "='" + Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'";
                            //Ejecutar consulta
                            Comando_SQL.CommandText = Mi_SQL;
                            Comando_SQL.ExecuteNonQuery();
                            
                            //Incrementar contador
                            contador = contador + 1;
                        }
                    }
                }
                //Ejecutar transaccion
                Transaccion_SQL.Commit();
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            }
        }
        #endregion

        #region (Consulta)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Poliza
        /// DESCRIPCION : Consulta todas las pólizas que coincidan con lo proporcionado
        ///               por el usuario
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 11-Julio-2011
        /// MODIFICO          : Salvador L. Rea Ayala
        /// FECHA_MODIFICO    : 10/Octubre/2011
        /// CAUSA_MODIFICACION: Se agregaron nuevos filtros para la consulta.
        ///*******************************************************************************
        public static DataTable Consulta_Poliza(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta para la póliza
            Int32 No_Poliza;
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL = null;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlDataAdapter Dt_Sql = new SqlDataAdapter();
            DataSet Ds_Sql = new DataSet();
                
            if (Datos.P_Cmmd == null)
            {
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
            }
            else
            {
                Comando_SQL = Datos.P_Cmmd;
            }
            try
            { 
                No_Poliza = Convert.ToInt32(Datos.P_No_Poliza);
                Datos.P_No_Poliza = String.Format("{0:0000000000}", No_Poliza);
                //Consulta todas las Polizas que coincidan con lo proporcionado por el usuario
                Mi_SQL = "SELECT * FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas;
                Mi_SQL += " WHERE " + Ope_Con_Polizas.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "'";
                if (!string.IsNullOrEmpty(Datos.P_Tipo_Poliza_ID))
                {
                    Mi_SQL += " AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID+ "'";
                }
                if (!string.IsNullOrEmpty(Datos.P_Fecha_Inicial))
                {
                    Mi_SQL += " AND " + Ope_Con_Polizas.Campo_Fecha_Poliza + " BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00' AND '" + Datos.P_Fecha_Inicial + " 23:59:59'";
                }
                if(!string.IsNullOrEmpty(Datos.P_Concepto))
                {
                    Mi_SQL += " AND " + Ope_Con_Polizas.Campo_Concepto + " = '" + Datos.P_Concepto + "'";
                }
                if (!string.IsNullOrEmpty(Datos.P_Mes_Ano))
                {
                    Mi_SQL += " AND " + Ope_Con_Polizas.Campo_Mes_Ano+ " = " + Datos.P_Mes_Ano + "";
                }
                Mi_SQL += " ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza;
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Sql.SelectCommand = Comando_SQL;
                Dt_Sql.Fill(Ds_Sql);
                if (Datos.P_Cmmd == null)
                {
                    Transaccion_SQL.Commit();
                }
                //return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Transaccion_SQL.Rollback();
                }
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Conexion_Base.Close();
                }

            }
            return Ds_Sql.Tables[0];
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Poliza_Popup
        /// DESCRIPCION : Consulta todas las pólizas que coincidan con lo proporcionado
        ///               por el usuario
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 22/Septiembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Poliza_Popup(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta para la póliza
            Int32 No_Poliza;
            Boolean Primer_Where = true;
            try
            {
                //Consulta todas las Polizas que coincidan con lo proporcionado por el usuario
                Mi_SQL = "SELECT " + Ope_Con_Polizas.Campo_No_Poliza + ", poliza.";
                Mi_SQL += Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", ";
                Mi_SQL += Cat_Con_Tipo_Polizas.Campo_Descripcion + ",";
                Mi_SQL += Ope_Con_Polizas.Campo_Fecha_Poliza + ", ";
                Mi_SQL += Ope_Con_Polizas.Campo_Mes_Ano + ", ";
                Mi_SQL += Ope_Con_Polizas.Campo_Concepto + " FROM ";
                Mi_SQL += Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas +" tipo, ";
                Mi_SQL += Ope_Con_Polizas.Tabla_Ope_Con_Polizas + " poliza WHERE ";
                Mi_SQL += " tipo." + Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID + " = poliza." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " AND ";

                if (!string.IsNullOrEmpty(Datos.P_Tipo_Poliza_ID))
                {
                    Mi_SQL +=" tipo." +Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = " + Datos.P_Tipo_Poliza_ID;
                    Primer_Where = false;
                }
                if (!string.IsNullOrEmpty(Datos.P_Mes_Ano))
                {
                    if (Primer_Where == true)
                    {
                        Mi_SQL += Ope_Con_Polizas.Campo_Mes_Ano + " = " + Datos.P_Mes_Ano;
                    }
                    else
                        Mi_SQL += " AND " + Ope_Con_Polizas.Campo_Mes_Ano + " = " + Datos.P_Mes_Ano;
                }
                if (!string.IsNullOrEmpty(Datos.P_No_Poliza))
                {
                    No_Poliza = Convert.ToInt32(Datos.P_No_Poliza);
                    Datos.P_No_Poliza = String.Format("{0:0000000000}", No_Poliza);
                    if (Primer_Where == true)
                    {
                        Mi_SQL += Ope_Con_Polizas.Campo_No_Poliza + " = " + Datos.P_No_Poliza;
                    }
                    else
                        Mi_SQL += " AND " + Ope_Con_Polizas.Campo_No_Poliza + " = " + Datos.P_No_Poliza;
                }
                Mi_SQL += " ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Detalles_Poliza
        /// DESCRIPCION : Consulta todos los detalles de la póliza
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 11-Julio-2011
        /// MODIFICO          : Salvador L. Rea Ayala
        /// FECHA_MODIFICO    : 10/Octubre/2011
        /// CAUSA_MODIFICACION: Se agregaron nuevos filtros para la consulta.
        ///*******************************************************************************
        public static DataTable Consulta_Detalles_Poliza(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consult del detalle de la póliza
            Int32 No_Poliza;
            try
            {
                No_Poliza = Convert.ToInt32(Datos.P_No_Poliza);
                Datos.P_No_Poliza = String.Format("{0:0000000000}", No_Poliza);
                //Consulta los detalles de la póliza
                Mi_SQL = "SELECT " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + ".*, " +
                       Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta +
                       " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + ", " +
                       Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables +
                       " WHERE " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID +
                       " AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "'" +
                       " AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "'" +
                       " ORDER BY " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Partida;
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Detalles_Poliza_Cuenta_Contable
        /// DESCRIPCION : Consulta todos los detalles de la póliza de acuerdo a un numero de cuenta
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 26/Octubre/2011
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static DataTable Consulta_Detalles_Poliza_Cuenta_Contable(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consult del detalle de la póliza
            Int32 No_Poliza;
            try
            {
                No_Poliza = Convert.ToInt32(Datos.P_No_Poliza);
                Datos.P_No_Poliza = String.Format("{0:0000000000}", No_Poliza);
                //Consulta los detalles de la póliza
                Mi_SQL = "SELECT " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + ".*" +
                         " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                         " WHERE " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID + "'" +
                         " AND (" + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " >= '" + Datos.P_Mes_Inicio + "' AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " <= '" + Datos.P_Mes_Fin + "')" +
                         " ORDER BY " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Partida;
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Detalles_Poliza_Por_Referencia
        /// DESCRIPCION : Consulta todos los detalles de la póliza de acuerdo a un numero de cuenta
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 26/Octubre/2011
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static DataTable Consulta_Detalles_Poliza_Por_Referencia(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consult del detalle de la póliza
            try
            {
                //Consulta los detalles de la póliza
                Mi_SQL = "SELECT " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + ".*" +
                         " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                         " WHERE " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID + "'" +
                         " AND "+ Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Referencia + " = '" + Datos.P_Referencia + "'" +
                          " AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Fecha +" "+
                " BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00' AND '" + Datos.P_Fecha_Final + " 23:59:59'"+
                         " ORDER BY " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Partida;
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Detalles_Poliza_Seleccionada
        /// DESCRIPCION : Consulta los datos sobre la poliza seleccionada
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 26/Septiembre/2011
        /// MODIFICO          : Salvador L. Rea Ayala
        /// FECHA_MODIFICO    : 10/Octubre/2011
        /// CAUSA_MODIFICACION: Se agregaron nuevos filtros para la consulta.
        ///*******************************************************************************
        public static DataTable Consulta_Detalles_Poliza_Seleccionada(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consult del detalle de la póliza
            Int32 No_Poliza;
            try
            {
                No_Poliza = Convert.ToInt32(Datos.P_No_Poliza);
                Datos.P_No_Poliza = String.Format("{0:0000000000}", No_Poliza);
                //Consulta los detalles de la póliza
                Mi_SQL = "SELECT PARTIDA, " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ", ";
                Mi_SQL += Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " AS CUENTA, ";
                Mi_SQL += Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Concepto + ", ";
                Mi_SQL += Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Debe + ", ";
                Mi_SQL += Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Haber + ", ";
                Mi_SQL += Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Dependencia_ID + ", ";
                Mi_SQL += Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID + ", ";
                Mi_SQL += Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID + ", ";
                Mi_SQL += Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Partida_ID + ", ";
                Mi_SQL += Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " AS Nombre_Cuenta ";

                //Mi_SQL += "((SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + "=" + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Dependencia_ID + ")  +'-'+";
                //Mi_SQL += " (SELECT " + Cat_Com_Proyectos_Programas.Campo_Clave + " FROM " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + " WHERE " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + "." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + "=" + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID + ")  +'-'+";
                //Mi_SQL += " (SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + "=" + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID + ")  +'-'+";
                //Mi_SQL += " (SELECT " + Cat_SAP_Area_Funcional.Campo_Clave + " FROM " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " WHERE " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + "=" + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Area_Funcional_ID + ")  +'-'+";
                //Mi_SQL += " (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "=" + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Partida_ID + ")) AS CODIGO_PROGRAMATICO ";

                Mi_SQL += " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + " LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                Mi_SQL += " ON " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "'";
                Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "'";
                Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Detalles_Empleados
        /// DESCRIPCION : Consulta los datos sobre la poliza seleccionada
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 7/Octubre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Detalles_Empleado_Aprobo(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consult del detalle de la póliza
            try
            {
                Mi_SQL = "SELECT " + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + "+' '+" + Cat_Empleados.Campo_Nombre + " AS EMPLEADO_AUTORIZO, ";
                Mi_SQL += Cat_Empleados.Campo_Empleado_ID + ", " + Cat_Empleados.Campo_No_Empleado;
                Mi_SQL += " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " RIGHT OUTER JOIN " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + " ON ";
                Mi_SQL += Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " = " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo;
                Mi_SQL += " WHERE " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Detalles_Empleado_Creo
        /// DESCRIPCION : Consulta los datos sobre la poliza seleccionada
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 7/Octubre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Detalles_Empleado_Creo(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consult del detalle de la póliza
            try
            {
                Mi_SQL = "SELECT " + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + "+' '+" + Cat_Empleados.Campo_Nombre + " AS EMPLEADO_CREO, ";
                Mi_SQL += Cat_Empleados.Campo_Empleado_ID + ", " + Cat_Empleados.Campo_No_Empleado;
                Mi_SQL += " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " RIGHT OUTER JOIN " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + " ON ";
                Mi_SQL += Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " = " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Empleado_ID_Creo;
                Mi_SQL += " WHERE " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Fecha_Poliza
        /// DESCRIPCION : Consulta la fecha de la poliza a copiar
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 26/Septiembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static Boolean Consulta_Fecha_Poliza(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta la fecha de la póliza
            DataTable Dt_Fecha_Poliza;
            string Fecha_Original;
            try
            {
                //Consulta los detalles de la póliza
                Mi_SQL = "SELECT " + Ope_Con_Polizas_Detalles.Campo_Fecha;
                Mi_SQL += " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_No_Poliza + " = " + Datos.P_No_Poliza;
                Dt_Fecha_Poliza = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                Fecha_Original = Dt_Fecha_Poliza.Rows[0][0].ToString();
                Fecha_Original = String.Format("{0:MM/dd/yy}", Convert.ToDateTime(Fecha_Original));

                if (String.Format("{0:MM/dd/yy}", Convert.ToDateTime(Datos.P_Fecha_Poliza)) == Fecha_Original)
                    return true;
                else
                    return false;

            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Fecha_Poliza_Anual
        /// DESCRIPCION : Este metodo se ocupa para la realizacion del cierre anual
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 26/Septiembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static String Consulta_Fecha_Poliza_Anual(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta la fecha de la póliza
            DataTable Dt_Fecha_Poliza;
            string Fecha_Original= string.Empty;
            try
            {
                //Consulta los detalles de la póliza
                Mi_SQL = "SELECT MAX(" + Ope_Con_Polizas_Detalles.Campo_Fecha;
                Mi_SQL += ") FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_Fecha + " <= CONVERT(DATETIME,'" + Datos.P_Fecha_Inicial;
                Mi_SQL += "') AND " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID+"'";
                Dt_Fecha_Poliza = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                //VERIFICAR SI HAY DATOS
                if (Dt_Fecha_Poliza.Rows.Count > 0)
                {
                    if (Dt_Fecha_Poliza.Rows[0][0]!=DBNull.Value)
                    {
                        Fecha_Original = Dt_Fecha_Poliza.Rows[0][0].ToString();
                        Fecha_Original = String.Format("{0:dd/MM/yy}", Convert.ToDateTime(Fecha_Original));
                    }
                }
                return Fecha_Original;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Saldo_Consecutivo_Anual
        /// DESCRIPCION : Este metodo se ocupa para la realizacion del cierre anual
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 26/Septiembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Saldo_Consecutivo_Anual(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta la fecha de la póliza
            DataTable Dt_Saldo;
            try
            {
                //Consulta los detalles de la póliza
                Mi_SQL = "SELECT " + Ope_Con_Polizas_Detalles.Campo_Saldo+","+Ope_Con_Polizas_Detalles.Campo_Consecutivo;
                Mi_SQL += " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_Fecha + " = '" + Datos.P_Fecha_Inicial;
                Mi_SQL += "' AND " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID;
                Mi_SQL += "' ORDER BY "+Ope_Con_Polizas_Detalles.Campo_Fecha+", "+Ope_Con_Polizas_Detalles.Campo_Consecutivo+" DESC";
                Dt_Saldo = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Saldo;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Detalle_Poliza
        /// DESCRIPCION : Consulta todos los datos de la poliza a generar
        ///               el usuario
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 12-Enero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Detalle_Poliza(Cls_Ope_Con_Polizas_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos             
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                SqlDataAdapter Dt_Sql = new SqlDataAdapter();
                DataSet Ds_Sql = new DataSet();
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }    
            try
                {
                    Mi_SQL.Append("SELECT POLIZA." + Ope_Con_Polizas.Campo_No_Poliza + ", POLIZA.");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Mes_Ano + ", POLIZA." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", POLIZA.");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_No_Partidas + ", POLIZA." + Ope_Con_Polizas.Campo_Concepto + ", POLIZA." + Ope_Con_Polizas.Campo_Fecha_Poliza + ", POLIZA.");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Total_Haber + ", POLIZA." + Ope_Con_Polizas.Campo_Total_Debe + " , DETALLES.");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Partida + ", DETALLES." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", DETALLES.");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Concepto + " AS CONCEPTO_PARTIDA , DETALLES."+Ope_Con_Polizas_Detalles.Campo_Referencia+", DETALLES.");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Debe + ", DETALLES." + Ope_Con_Polizas_Detalles.Campo_Haber + ", CUENTA.");
                    Mi_SQL.Append(Cat_Con_Cuentas_Contables.Campo_Cuenta + ", TIPO." + Cat_Con_Tipo_Polizas.Campo_Descripcion + " AS TIPO_POLIZA ,CUENTA." + Cat_Con_Cuentas_Contables.Campo_Descripcion);
                    Mi_SQL.Append(", POLIZA.PREFIJO ");
                    Mi_SQL.Append(",(select " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " from " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento +
                                    " where " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + "=DETALLES." + Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID + ")");
                    Mi_SQL.Append("  +'-'+ (select " + Cat_SAP_Area_Funcional.Campo_Clave + " from " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional +
                                    " where " + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + "=DETALLES." + Ope_Con_Polizas_Detalles.Campo_Area_Funcional_ID + ")");
                    Mi_SQL.Append("  +'-'+ (select " + Cat_Sap_Proyectos_Programas.Campo_Clave + " from " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas +
                                    " where " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + "=DETALLES." + Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID + ")");
                    Mi_SQL.Append("  +'-'+ (select " + Cat_Dependencias.Campo_Clave + " from " + Cat_Dependencias.Tabla_Cat_Dependencias +
                                    " where " + Cat_Dependencias.Campo_Dependencia_ID + "=DETALLES." + Ope_Con_Polizas_Detalles.Campo_Dependencia_ID + ")");
                    Mi_SQL.Append("  +'-'+ (select " + Cat_Sap_Partidas_Especificas.Campo_Clave + " from " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas +
                                    " where " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "=DETALLES." + Ope_Con_Polizas_Detalles.Campo_Partida_ID + ") as CODIGO_PROGRAMATICO, ' ' AS BENEFICIARIO, ' ' AS CUENTA_A_PAGAR, ' ' AS CUENTA_DE_PAGO");
                    Mi_SQL.Append(", PAGOS." + Ope_Con_Pagos.Campo_Beneficiario_Pago + " AS BENEFICIARIO_PAGO");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + " POLIZA");
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " PAGOS ON PAGOS." + Ope_Con_Pagos.Campo_No_poliza + " =POLIZA." + Ope_Con_Polizas.Campo_No_Poliza + " AND ");
                    Mi_SQL.Append(" PAGOS." + Ope_Con_Pagos.Campo_Tipo_Poliza_ID + " =POLIZA." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " AND PAGOS." + Ope_Con_Pagos.Campo_Mes_Ano + " = POLIZA." + Ope_Con_Polizas.Campo_Mes_Ano); ;
                    Mi_SQL.Append(", " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + " DETALLES, ");
                    Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTA, " + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas + " TIPO ");
                    Mi_SQL.Append(" WHERE POLIZA." + Ope_Con_Polizas.Campo_No_Poliza + "= DETALLES." + Ope_Con_Polizas_Detalles.Campo_No_Poliza + " AND POLIZA.");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Tipo_Poliza_ID + "= DETALLES." + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + " AND POLIZA." + Ope_Con_Polizas.Campo_Mes_Ano + "= DETALLES." + Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " AND poliza.");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Tipo_Poliza_ID + "= TIPO." + Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID + " AND DETALLES." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + "= CUENTA.");
                    Mi_SQL.Append(Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " AND POLIZA." + Ope_Con_Polizas.Campo_No_Poliza + "='" + Datos.P_No_Poliza + "' AND POLIZA.");
                    Mi_SQL.Append(Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " ='" + Datos.P_Tipo_Poliza_ID + "' AND poliza." + Ope_Con_Polizas.Campo_Mes_Ano + "='" + Datos.P_Mes_Ano + "'");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                    Dt_Sql.SelectCommand = Cmmd;
                    Dt_Sql.Fill(Ds_Sql);
                    // return OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Oracle.ToString()).Tables[0];
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    return Ds_Sql.Tables[0];
                     // return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }
                }
            }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Fuente_Financiamiento
        ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 30/Noviembre/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
         public static DataTable Consulta_Fuente_Financiamiento(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + "   +' '+ ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Det_Fte_Concepto.Tabla_Cat_Sap_Det_Fte_Concepto);
                Mi_Sql.Append(" ON " + Cat_Sap_Det_Fte_Concepto.Tabla_Cat_Sap_Det_Fte_Concepto + "." + Cat_Sap_Det_Fte_Concepto.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                Mi_Sql.Append(" ON " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                Mi_Sql.Append(" = " + Cat_Sap_Det_Fte_Concepto.Tabla_Cat_Sap_Det_Fte_Concepto + "." + Cat_Sap_Det_Fte_Concepto.Campo_Concepto_Ing_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                Mi_Sql.Append(" ON " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                Mi_Sql.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Cuenta_Contable_ID);
                Mi_Sql.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Anio);
                Mi_Sql.Append(" = " + String.Format("{0:yyyy}", DateTime.Now) + " AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Estatus +" ='ACTIVO' ");
                if( !String.IsNullOrEmpty(Datos.P_Partida_ID)){
                    Mi_Sql.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Partida_ID + " = '" + Datos.P_Partida_ID + "'");
                }
                Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]");
            }
        }
         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consulta_Fuente_Financiamiento_Egr
         ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento DE EGRESOS
         ///PARAMETROS           : 
         ///CREO                 : SERGIO MANEUL GALLARDO ANDRADE
         ///FECHA_CREO           : 20/ABRIL/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Consulta_Fuente_Financiamiento_Egr(Cls_Ope_Con_Polizas_Negocio Datos)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 //OBTENEMOS LAS FUENTES DEL CATALOGO
                 Mi_Sql.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + "   +' '+ ");
                 Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                 Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                 Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                 Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                 Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                 Mi_Sql.Append(" INNER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                 Mi_Sql.Append(" ON " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Partida_ID);
                 Mi_Sql.Append(" = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                 Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                 Mi_Sql.Append(" = " + String.Format("{0:yyyy}", DateTime.Now));
                 if (!String.IsNullOrEmpty(Datos.P_Partida_ID))
                 {
                     Mi_Sql.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Partida_ID + " = '" + Datos.P_Partida_ID + "'");
                 }
                 Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]");
             }
         }
         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consulta_Dependencia
         ///DESCRIPCIÓN          : consulta para obtener los datos de las dependencias de la fuente de financiamiento seleccionada
         ///PARAMETROS           : 
         ///CREO                 : SERGIO MANEUL GALLARDO ANDRADE
         ///FECHA_CREO           : 20/ABRIL/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Consulta_Dependencia(Cls_Ope_Con_Polizas_Negocio Datos)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                 Mi_Sql.Append("SELECT  DISTINCT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE, ");
                 Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + " AS DEPENDENCIA ");
                 Mi_Sql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_Sql.Append(" INNER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                 Mi_Sql.Append(" ON " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Partida_ID);
                 Mi_Sql.Append(" = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                 Mi_Sql.Append(" = '" +  String.Format("{0:yyyy}", DateTime.Now)+ "'");
                 
                 if (!String.IsNullOrEmpty(Datos.P_Fuente_Financiamiento_ID))
                 {
                     Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                     Mi_Sql.Append(" = '" + Datos.P_Fuente_Financiamiento_ID + "'");
                 }
                 if (!String.IsNullOrEmpty(Datos.P_Partida_ID))
                 {
                     Mi_Sql.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Partida_ID + " = '" + Datos.P_Partida_ID + "'");
                 }
                 if (!String.IsNullOrEmpty(Datos.P_Clave_Dependencia))
                 {
                     Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " = '" + Datos.P_Clave_Dependencia + "'");
                 }
                 Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]");
             }
         }  
        ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consulta_Programas
         ///DESCRIPCIÓN          : consulta para obtener los programas de la dependencia seleccionada
         ///PARAMETROS           : 
         ///CREO                 : SERGIO MANEUL GALLARDO ANDRADE
         ///FECHA_CREO           : 20/ABRIL/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Consulta_Programas(Cls_Ope_Con_Polizas_Negocio Datos)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                 Mi_Sql.Append("SELECT DISTINCT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS CLAVE_NOMBRE, ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " AS PROGRAMA_ID ");
                 if (!String.IsNullOrEmpty(Datos.P_Momento))
                 {
                     Mi_Sql.Append(", "+Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "."+Datos.P_Momento+" AS MOMENTO");
                 }
                 if (!String.IsNullOrEmpty(Datos.P_Momento_Final))
                 {
                     Mi_Sql.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Datos.P_Momento_Final + " AS MOMENTO_FINAL");
                 }
                 Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                 Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                 Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                 Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_Sql.Append(" INNER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                 Mi_Sql.Append(" ON " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Partida_ID);
                 Mi_Sql.Append(" = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                 Mi_Sql.Append(" = '" + String.Format("{0:yyyy}", DateTime.Now) + "'");
                 if (!String.IsNullOrEmpty(Datos.P_Dependencia_ID))
                 {
                     Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                     Mi_Sql.Append(" = '" + Datos.P_Dependencia_ID + "'");
                 }
                 if (!String.IsNullOrEmpty(Datos.P_Fuente_Financiamiento_ID))
                 {
                     Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                     Mi_Sql.Append(" = '" + Datos.P_Fuente_Financiamiento_ID + "'");
                 }
                 if (!String.IsNullOrEmpty(Datos.P_Partida_ID))
                 {
                     Mi_Sql.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Partida_ID + " = '" + Datos.P_Partida_ID + "'");
                 }
                 if (!String.IsNullOrEmpty(Datos.P_Clave_Programa))
                 {
                     Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " = '" + Datos.P_Clave_Programa + "'");
                 }
                 Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]");
             }
         }
         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consulta_Programas_Ing
         ///DESCRIPCIÓN          : consulta para obtener los datos de los programas que tienen esa fuente de financiamiento seleccionada
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 30/Noviembre/2011
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Consulta_Programas_Ing(Cls_Ope_Con_Polizas_Negocio Datos)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                 Mi_Sql.Append("SELECT DISTINCT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + "   +' '+ ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                 //Se utiliza en el combo de programa_poliza para obtener el monto
                 if (!String.IsNullOrEmpty(Datos.P_Programa_ID))
                 {
                     Mi_Sql.Append(", " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Importe);
                 }
                 Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                 Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa);
                 Mi_Sql.Append(" ON " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID);
                 Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                 Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Estatus + " ='ACTIVO' ");
                 if (!String.IsNullOrEmpty(Datos.P_Fuente_Financiamiento_ID))
                 {
                     Mi_Sql.Append(" AND " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID + " = '" + Datos.P_Fuente_Financiamiento_ID + "'");
                 }
                 //Se utiliza en el combo de programa_poliza para obtener el monto
                 if (!String.IsNullOrEmpty(Datos.P_Programa_ID))
                 {
                     Mi_Sql.Append(" AND " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID + " = '" + Datos.P_Programa_ID  + "'");
                 }
                 Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de los programas. Error: [" + Ex.Message + "]");
             }
         }
        
                     ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consulta_Cuentas_Contables_De_Conceptos
         ///DESCRIPCIÓN          : consulta para obtener los datos de los programas que tienen esa fuente de financiamiento seleccionada
         ///PARAMETROS           : 
         ///CREO                 : Sergio Manuel Gallardo Andrade
         ///FECHA_CREO           : 30/Junio/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Consulta_Cuentas_Contables_De_Conceptos(Cls_Ope_Con_Polizas_Negocio Datos)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                 Mi_Sql.Append("SELECT  " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID+", ");
                 Mi_Sql.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ", ");
                 Mi_Sql.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta +", ");
                 Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + ", ");
                 Mi_Sql.Append(Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID+", ");
                 Mi_Sql.Append(Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Importe);
                 Mi_Sql.Append(" FROM " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa);
                 Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                 Mi_Sql.Append(" ON " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID);
                 Mi_Sql.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                 Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                 Mi_Sql.Append(" ON " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Cuenta_Contable_ID);
                 Mi_Sql.Append(" = " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID );
                 Mi_Sql.Append(" WHERE " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Afectable  + " ='SI' ");
                 if (!String.IsNullOrEmpty(Datos.P_Programa_ID))
                 {
                     Mi_Sql.Append(" AND " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID+"='"+Datos.P_Programa_ID+"'");
                 }
                 Mi_Sql.Append(" ORDER BY " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables+"."+Cat_Con_Cuentas_Contables.Campo_Cuenta + " DESC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de los programas. Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         /// NOMBRE DE LA FUNCION: Consulta_Polizas_Tipo
         /// DESCRIPCION : Consultar las polizas de un tipo en particular, asi como de un mes y año particular para el prefijo
         /// PARAMETROS  : Datos: variable de la capa de negocios
         /// CREO        : Noe Mosqueda Valadez
         /// FECHA_CREO  : 10/Abril/2012 13:14
         /// MODIFICO          :
         /// FECHA_MODIFICO    :
         /// CAUSA_MODIFICACION:
         ///*******************************************************************************
         public static DataTable Consulta_Polizas_Tipo(Cls_Ope_Con_Polizas_Negocio Datos)
         {
             //Declaracion de variables
             String Mi_SQL = String.Empty; //variable para las consultas
             DataTable Dt_Resultado = new DataTable(); //tabla para el resultado

             try
             {
                 //Asignar consulta
                 Mi_SQL = "SELECT Ope_Con_Polizas.No_Poliza, Cat_Con_Tipo_Polizas.Descripcion AS Tipo_Poliza, Ope_Con_Polizas.Mes_Ano, " +
                     "SUBSTRING(ISNULL(Ope_Con_Polizas.Prefijo, '0-0'), CHARINDEX('-', ISNULL(Ope_Con_Polizas.Prefijo, '0-0')) + 1, LEN(ISNULL(Ope_Con_Polizas.Prefijo, '0-0'))) AS Consecutivo_Prefijo, " +
                     "Ope_Con_Polizas.Prefijo " +
                     "FROM Ope_Con_Polizas " +
                     "INNER JOIN Cat_Con_Tipo_Polizas ON Ope_Con_Polizas.Tipo_Poliza_ID = Cat_Con_Tipo_Polizas.Tipo_Poliza_ID " +
                     "WHERE Ope_Con_Polizas.Tipo_Poliza_ID = '" + Datos.P_Tipo_Poliza_ID + "' " +
                     "AND Ope_Con_Polizas.Mes_Ano = '" + Datos.P_Mes_Ano + "' " +
                     "ORDER BY ABS(SUBSTRING(ISNULL(Ope_Con_Polizas.Prefijo, '0-0'), CHARINDEX('-', ISNULL(Ope_Con_Polizas.Prefijo, '0-0')) + 1, LEN(ISNULL(Ope_Con_Polizas.Prefijo, '0-0')))) DESC ";

                 //Ejecutar consulta
                 Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                 //Entregar resultado
                 return Dt_Resultado;
             }
             catch (Exception ex)
             {
                 throw new Exception(ex.Message, ex);
             }
         }

        #endregion

        #region (Metodos Clases Externas)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Empleados_Especial
        /// DESCRIPCION : Consulta los empleados que cumplen con los requerimientos.
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 7/Octubre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Empleados_Especial(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL;    //Variable para la consulta para el Empleado
            try
            {
                //Consulta todos los Empleados que coincidan con lo proporcionado por el usuario
                Mi_SQL = "SELECT " + Cat_Empleados.Campo_Empleado_ID + ", " + Cat_Empleados.Campo_No_Empleado + ", ";
                Mi_SQL += Cat_Empleados.Campo_RFC + ", " + Cat_Empleados.Campo_Estatus + ", ";
                Mi_SQL += "" + Cat_Empleados.Campo_Apellido_Paterno;
                Mi_SQL += "+' '+" + Cat_Empleados.Campo_Apellido_Materno;
                Mi_SQL += "+' '+" + Cat_Empleados.Campo_Nombre + " AS EMPLEADO";
                Mi_SQL += " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
                if(!string.IsNullOrEmpty(Datos.P_Nombre))
                {
                    Mi_SQL += " WHERE (" + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno + "+' '+";
                    Mi_SQL += Cat_Empleados.Campo_Apellido_Materno + " LIKE UPPER ('%" + Datos.P_Nombre + "%'))";
                }
                else {
                    Mi_SQL += " Where (" + Cat_Empleados.Campo_No_Empleado + " = '" + String.Format("{0:000000}", Convert.ToInt16(Datos.P_Empleado_ID)) + "')";

                }
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Partida_Especifica
        /// DESCRIPCION : Consulta la partida especifica de acuerdo a la cuenta contable
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 4/Octubre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Partida_Especifica(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta para la póliza            
            try
            {
                Mi_SQL = "SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + ", ";
                Mi_SQL += Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", ";
                Mi_SQL += Cat_Sap_Partidas_Especificas.Campo_Nombre;
                Mi_SQL += " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;

                if (!String.IsNullOrEmpty(Datos.P_Cuenta))
                    Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Cuenta + " = '" + Datos.P_Cuenta + "'";

                if (!String.IsNullOrEmpty(Datos.P_Clave))
                    Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Clave + " = '" + Datos.P_Clave + "'";

                if (!String.IsNullOrEmpty(Datos.P_Partida_ID))
                    Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = '" + Datos.P_Partida_ID + "'";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Area_Funcional_Especial
        ///DESCRIPCIÓN: Consulta las Areas Funcionales asociadas a la Fte de Financiamiento
        ///PARAMETROS:  1.- Cls_Cat_SAP_Area_Funcional_Negocios
        ///CREO: Salvador L. Rea Ayala
        ///FECHA_CREO: 4/Octubre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Area_Funcional_Especial(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            string Mi_SQL;  //Variable que contendra la Query de consutla.

            try
            {
                Mi_SQL = "SELECT " + Cat_SAP_Area_Funcional.Campo_Clave + "  +' - '+ " + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS CLAVE_NOMBRE, ";
                Mi_SQL += Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID;
                Mi_SQL += " FROM " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional;

                if (!String.IsNullOrEmpty(Datos.P_Area_Funcional_ID))
                    Mi_SQL += " WHERE " + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + " = '" + Datos.P_Area_Funcional_ID + "'";

                if (!String.IsNullOrEmpty(Datos.P_Clave))
                    Mi_SQL += " WHERE " + Cat_SAP_Area_Funcional.Campo_Clave + " = '" + Datos.P_Clave + "'";

                Mi_SQL += " ORDER BY " + Cat_SAP_Area_Funcional.Campo_Clave + " ASC";

                //Sentencia que ejecuta el query
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al Alta_Area_Funcional. Error: [" + Ex.Message + "]");
            }
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Consulta_Programas_Especial
        ///DESCRIPCIÓN: Consulta los programas
        ///PARAMETROS: Datos: Variable de negocio que contiene los datos a consultar
        ///CREO: Salvador L. Rea Ayala
        ///FECHA_CREO: 4/Octubre/2011
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************     
        public static DataTable Consulta_Programas_Especial()
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;

            try
            {
                Mi_SQL = "SELECT " + Cat_Com_Proyectos_Programas.Campo_Clave + "  +' - '+ " + Cat_Com_Proyectos_Programas.Campo_Nombre + " AS CLAVE_NOMBRE, ";
                Mi_SQL += Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID;
                Mi_SQL += " FROM " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas;
                Mi_SQL += " ORDER BY " + Cat_Com_Proyectos_Programas.Campo_Clave + " ASC";

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Actualizar_Montos_Presupuesto()
        /// 	DESCRIPCIÓN: Actualiza el monto disponible al igual que el monto comprometido.
        /// 	PARÁMETROS:
        /// 		   Datos: Instancia de la clase de negocio con los datos para actualizar la base de datos
        /// 	CREO: Salvador L. Rea Ayala
        /// 	FECHA_CREO: 19/Octubre/2011
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        public static void Actualizar_Montos_Presupuesto(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Contiene la consulta de modificación hacía la base de datos
            try
            {
                //Da de Alta los datos del Nuevo Parametro con los datos proporcionados por el usuario.
                Mi_SQL = "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                Mi_SQL += " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " = " + Datos.P_Disponible + ", ";
                Mi_SQL += Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " = " + Datos.P_Comprometido;
                Mi_SQL += " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "' ";
                Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID + " = '" + Datos.P_Fuente_Financiamiento_ID + "' ";
                Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID + " = '" + Datos.P_Partida_ID + "' ";
                Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID + " = '" + Datos.P_Programa_ID + "'";

                //Manda Mi_SQL para ser procesada por Sql.
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_Dependencia_Partida_ID
        /// 	DESCRIPCIÓN: Consulta las dependencias ligadas a una partida especifica
        /// 	PARÁMETROS:  Datos: Indica qué registro se desea consultar a la base de datos
        /// 	CREO: Salvador L. Rea Ayala
        /// 	FECHA_CREO: 04-mar-2011
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        public static DataTable Consulta_Dependencia_Partida_ID(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta SQL

            try
            {
                Mi_SQL = "SELECT DISTINCT " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID;
                Mi_SQL += " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                Mi_SQL += " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID + " = '" + Datos.P_Partida_ID + "'";
                Mi_SQL += " ORDER BY " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_GrupoRol
        /// 	DESCRIPCIÓN: Consulta el rol del Empleado que autoriza la poliza si ya esta cerrado el mes
        /// 	PARÁMETROS:  Datos: Indica qué registro se desea consultar a la base de datos
        /// 	CREO:Sergio Manuel Gallardo Andrade 
        /// 	FECHA_CREO:  10/noviembre/2011
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        public static DataTable Consulta_GrupoRol(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta SQL

            try
            {
                Mi_SQL = "SELECT Rol.Grupo_Roles_ID " + Apl_Cat_Roles.Campo_Grupo_Roles_ID ;
                Mi_SQL += " FROM " + Apl_Cat_Roles.Tabla_Apl_Cat_Roles + " Rol, "+ Cat_Empleados.Tabla_Cat_Empleados +" Emp ";
                Mi_SQL += " WHERE Emp." + Cat_Empleados.Campo_Rol_ID + " = Rol." + Apl_Cat_Roles.Campo_Rol_ID  + " AND Emp." + Cat_Empleados.Campo_No_Empleado +"='"+ Datos.P_Empleado_ID +"' ";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_Fte_Area_Funcional_ID
        /// 	DESCRIPCIÓN: Consulta las Areas Funcionales ligadas a la Fuente de Financiamiento
        /// 	PARÁMETROS:  Datos: Indica qué registro se desea consultar a la base de datos
        /// 	CREO: Salvador L. Rea Ayala
        /// 	FECHA_CREO: 6/Octubre/2011
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        public static DataTable Consulta_Fte_Area_Funcional_ID(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta SQL

            try
            {
                Mi_SQL = "SELECT DISTINCT " + Cat_Com_Dep_Presupuesto.Campo_Area_Funcional_ID;
                Mi_SQL += " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                Mi_SQL += " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID + " = '" + Datos.P_Fuente_Financiamiento_ID + "'";
                Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "'";
                Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID + " = '" + Datos.P_Programa_ID + "'";
                Mi_SQL += " ORDER BY " + Cat_Com_Dep_Presupuesto.Campo_Area_Funcional_ID + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_Dependencia_Programa_ID
        /// 	DESCRIPCIÓN: Consulta los Programas ligados a las Dependencias
        /// 	PARÁMETROS:  Datos: Indica qué registro se desea consultar a la base de datos
        /// 	CREO: Salvador L. Rea Ayala
        /// 	FECHA_CREO: 6/Octubre/2011
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        public static DataTable Consulta_Dependencia_Programa_ID(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta SQL

            try
            {
                Mi_SQL = "SELECT DISTINCT " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID;
                Mi_SQL += " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                Mi_SQL += " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "'";
                Mi_SQL += " ORDER BY " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_Programa_Fuente_ID
        /// 	DESCRIPCIÓN: Consulta las Fuentes de Financiamiento ligadas al Programa
        /// 	PARÁMETROS:  Datos: Indica qué registro se desea consultar a la base de datos
        /// 	CREO: Salvador L. Rea Ayala
        /// 	FECHA_CREO: 6/Octubre/2011
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        public static DataTable Consulta_Programa_Fuente_ID(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta SQL

            try
            {
                Mi_SQL = "SELECT DISTINCT " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID;
                Mi_SQL += " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                Mi_SQL += " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID + " = '" + Datos.P_Programa_ID + "'";
                Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "'";
                Mi_SQL += " ORDER BY " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_Empleado_Jefe_Dependencia
        /// 	DESCRIPCIÓN: Consulta si el empleado logeado es Jefe de Dependencia.
        /// 	PARÁMETROS:  Datos: Indica qué registro se desea consultar a la base de datos
        /// 	CREO: Salvador L. Rea Ayala
        /// 	FECHA_CREO: 24/Octubre/2011
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        public static DataTable Consulta_Empleado_Jefe_Dependencia(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta SQL

            try
            {
                Mi_SQL = "SELECT " + Cat_Empleados.Campo_No_Empleado;
                Mi_SQL += " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
                Mi_SQL += " WHERE " + Cat_Empleados.Campo_Rol_ID + " = '00003'";
                Mi_SQL += " AND " + Cat_Empleados.Campo_Empleado_ID + " = '" + Datos.P_Empleado_ID + "'";
                Mi_SQL += " ORDER BY " + Cat_Empleados.Campo_Empleado_ID + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        #endregion

        #region

        /// ********************************************************************************************************************
        /// NOMBRE:         Consulta_Cuenta_Partida_ID
        /// COMENTARIOS:    Consulta la partida id de la cuenta contable que se busca
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     20/Abril/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consulta_Cuenta_Partida_ID(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                Mi_SQL.Append("Select ");
                Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + ".* ");
                Mi_SQL.Append(" from " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                Mi_SQL.Append(" where " + Cat_Con_Cuentas_Contables.Campo_Cuenta + "='" + Datos.P_Clave_Cuenta_Contable + "'");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0]; ;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }

        /// ********************************************************************************************************************
        /// NOMBRE:         Consulta_ID_Carga_Masiva
        /// COMENTARIOS:    Consulta las id del registro que se busca dentro de la carga masiva
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     20/Abril/2012  
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consulta_ID_Fte_Financiamiento(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            StringBuilder Mi_Sql = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                Mi_Sql.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + "   +' '+ ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                Mi_Sql.Append(" ON " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Partida_ID);
                Mi_Sql.Append(" = " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                Mi_Sql.Append(" = " + String.Format("{0:yyyy}", DateTime.Now));
                if (!String.IsNullOrEmpty(Datos.P_Partida_ID))
                {
                    Mi_Sql.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Partida_ID + " = '" + Datos.P_Partida_ID + "'");
                }
                if (!String.IsNullOrEmpty(Datos.P_Clave_Fte_Financiamiento))
                {
                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " = '" + Datos.P_Clave_Fte_Financiamiento + "'");
                }
                Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Consulta_ID_Carga_Masiva
        /// COMENTARIOS:    Consulta las id del registro que se busca dentro de la carga masiva
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     20/Abril/2012  
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consulta_ID_Fte_Financiamiento_Egr(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            StringBuilder Mi_Sql = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                Mi_Sql.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + "   +' '+ ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                Mi_Sql.Append(" ON " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Partida_ID);
                Mi_Sql.Append(" = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                Mi_Sql.Append(" = " + String.Format("{0:yyyy}", DateTime.Now));
                if (!String.IsNullOrEmpty(Datos.P_Partida_ID))
                {
                    Mi_Sql.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Partida_ID + " = '" + Datos.P_Partida_ID + "'");
                }
                if (!String.IsNullOrEmpty(Datos.P_Clave_Fte_Financiamiento))
                {
                    Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " = '" + Datos.P_Clave_Fte_Financiamiento + "'");
                }
                Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }
        #endregion

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Saldo_y_Actualiza
        /// DESCRIPCION : Consulta el ultimo saldo y lo actualiza
        /// PARAMETROS  : Fecha de tipo DATETIME y DataTable con las partidas de la poliza
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 29/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Consulta_Saldo_y_Actualiza(DateTime Fecha_Poliza, DataTable Dt_Datos, SqlCommand P_Cmmd)
        {
            String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
            String Fecha;
            Object Fecha_Saldo;
            Double Diferencia;
            String Cuenta_Contable_ID;
            DataTable Saldo;
            Double Saldo2;
            int contador;
            int Bandera;
            Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuenta = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //variable para la clase de negocio de las cuentas contables
            DataTable Dt_Cuenta_Tipo = new DataTable(); //Tabla para la consulta de las cuentas contables
            DataTable Dt_Psp = new DataTable();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Da_SQL = new SqlDataAdapter();
            DataSet Ds_SQL = new DataSet();
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                foreach (DataRow Renglon in Dt_Datos.Rows)
                {
                    Cuenta_Contable_ID = Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
                    Rs_Cuenta.P_Cuenta_Contable_ID = Cuenta_Contable_ID;
                    Diferencia = 0;
                    Dt_Cuenta_Tipo = Rs_Cuenta.Consulta_Datos_Cuentas_Contables();
                    Bandera = 0;
                    Saldo2 = 0;
                    contador = 0;
                    //se le resta a la fecha un dia
                    Fecha = string.Format("{0:dd/MM/yyyy}", Fecha_Poliza.AddDays(-1));
                    //Obtiene el ultimo movimiento de acuerdo a la fecha
                    Mi_SQL = "SELECT MAX(" + Ope_Con_Polizas_Detalles.Campo_Fecha + ")";
                    Mi_SQL += "FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                    Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + "='" + Cuenta_Contable_ID + "' ";
                    Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Fecha + "<=CONVERT(DATETIME," + "'" + Fecha + "')";
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Fecha_Saldo = Cmmd.ExecuteScalar();
                    //Fecha_Saldo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).ToString();
                    if (!(Fecha_Saldo is Nullable) && !Fecha_Saldo.ToString().Equals(""))
                    {

                        Fecha_Saldo = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Fecha_Saldo));
                        Mi_SQL = "SELECT " + Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Partida;
                        Mi_SQL += " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + " WHERE " + Ope_Con_Polizas_Detalles.Campo_Fecha + "=CONVERT(DATETIME,'" + Fecha_Saldo + "')";
                        Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + "='" + Cuenta_Contable_ID + "' ORDER BY " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " + Ope_Con_Polizas_Detalles.Campo_Consecutivo + ", ";
                        Mi_SQL += Ope_Con_Polizas_Detalles.Campo_Partida;
                        Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                        Da_SQL.SelectCommand = Cmmd;
                        Da_SQL.Fill(Ds_SQL);
                        Saldo = Ds_SQL.Tables[0];
                        //Saldo = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                        if (Saldo.Rows.Count > 0)
                        {
                            Saldo2 = Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Saldo].ToString());
                        }
                    }
                    else
                    {
                        Saldo2 = 0;
                        Bandera = 1;
                        Fecha_Saldo = Fecha;
                    }

                    Ds_SQL = new DataSet();
                    //Actualiza saldos
                    Mi_SQL = "SELECT * FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                    Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + "='" + Cuenta_Contable_ID + "' ";
                    Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Fecha + ">= CONVERT(DATETIME,'" + Fecha_Saldo + "')";
                    Mi_SQL += " ORDER BY " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " + Ope_Con_Polizas_Detalles.Campo_Consecutivo + ", " + Ope_Con_Polizas_Detalles.Campo_Partida;
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    Saldo = Ds_SQL.Tables[0];
                    //Saldo = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                    if (Saldo2 == 0 && Bandera == 1 && Saldo.Rows.Count > 0)
                    {
                        //Veriricar el tipo de cuenta contable
                        if (Dt_Cuenta_Tipo.Rows[0][Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta].ToString().Trim().ToUpper() == "DEUDOR")
                        {
                            Saldo2 = Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) - Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                        }
                        else
                        {
                            Saldo2 = Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) - Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                        }
                    }
                    if (Saldo.Rows.Count > 0)
                    {
                        if (contador > 0)
                        {
                            //Verificar el tipo de cuenta
                            if (Dt_Cuenta_Tipo.Rows[0][Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta].ToString().Trim().ToUpper() == "DEUDOR")
                            {
                                Saldo2 = Saldo2 + Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) - Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                                Diferencia = Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) - Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                            }
                            else
                            {
                                Saldo2 = Saldo2 + Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) - Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                                Diferencia = Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) - Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                            }
                            //}
                        }
                        Mi_SQL = "UPDATE " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                        Mi_SQL += " SET " + Ope_Con_Polizas_Detalles.Campo_Saldo + " = " + Saldo2;
                        Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_Consecutivo + " = " + Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Consecutivo].ToString();
                        Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Partida + " = " + Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Partida].ToString();
                        Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + "='" + Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'";
                        Cmmd.CommandText = Mi_SQL.ToString();
                        Cmmd.ExecuteNonQuery();

                        Mi_SQL = "UPDATE " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                        Mi_SQL += " SET " + Ope_Con_Polizas_Detalles.Campo_Saldo + " = " + Ope_Con_Polizas_Detalles.Campo_Saldo + "+" + Diferencia;
                        Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + "='" + Cuenta_Contable_ID + "' ";
                        Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Fecha + ">= CONVERT(DATETIME,'" + Fecha_Saldo + "')";
                        Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Consecutivo + " NOT IN(" + Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Consecutivo].ToString() + ")";
                        Cmmd.CommandText = Mi_SQL.ToString();
                        Cmmd.ExecuteNonQuery();
                    }
                }
                if (P_Cmmd == null)
                {
                    Trans.Commit();
                }

            }

            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_Solicitud_Pago
        /// 	DESCRIPCIÓN: Consulta la informacion de la solicitud de pago para conocer al proveedor
        /// 	PARÁMETROS:  Datos: Indica qué registro se desea consultar a la base de datos
        /// 	CREO:Sergio Manuel Gallardo Andrade 
        /// 	FECHA_CREO:  10/mayo/2011
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        public static DataTable Consulta_Solicitud_Pago(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta SQL

            try
            {
                Mi_SQL = "SELECT * FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos;
                Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = '" + Datos.P_No_Reserva + "' AND  ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " = '" + Datos.P_No_Poliza + "' AND  ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "' AND  ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "' AND ";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Estatus + " != 'CANCELADO'";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_Pago
        /// 	DESCRIPCIÓN: Consulta la informacion de la solicitud de pago para conocer al proveedor
        /// 	PARÁMETROS:  Datos: Indica qué registro se desea consultar a la base de datos
        /// 	CREO:Sergio Manuel Gallardo Andrade 
        /// 	FECHA_CREO:  10/mayo/2011
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        public static DataTable Consulta_Pago(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta SQL

            try
            {
                Mi_SQL = "SELECT Pago.*,Solicitud." + Ope_Con_Solicitud_Pagos .Campo_No_Solicitud_Pago +" As solicitud  FROM " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " Pago," + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " Solicitud";
                Mi_SQL += " WHERE Pago." + Ope_Con_Pagos.Campo_No_Pago + " = Solicitud." + Ope_Con_Solicitud_Pagos.Campo_No_Pago + " AND  Pago.";
                Mi_SQL += Ope_Con_Pagos.Campo_No_poliza + " = '" + Datos.P_No_Poliza + "' AND  Pago.";
                Mi_SQL += Ope_Con_Pagos.Campo_Mes_Ano + " = '" + Datos.P_Mes_Ano + "' AND  Pago.";
                Mi_SQL += Ope_Con_Pagos.Campo_Tipo_Poliza_ID + " = '" + Datos.P_Tipo_Poliza_ID + "' AND Solicitud.";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " = '" + Datos.P_No_Reserva + "' AND Solicitud.";
                Mi_SQL += Ope_Con_Solicitud_Pagos.Campo_Estatus + " != 'CANCELADO'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_Dependencias_Presupuesto
        /// 	DESCRIPCIÓN: Consulta las dependencias de la tabla del presupuesto aprobado
        /// 	PARÁMETROS:  Datos: Variable de la capa de negocios
        /// 	CREO: Noe Mosqueda Valadez
        /// 	FECHA_CREO:  25/Junio/2013 12:30
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        public static DataTable Consulta_Dependencias_Presupuesto(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            //Declaracion de variables
            string Mi_SQL = string.Empty; //variable para la consulta
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado

            try
            {
                //Asignar la consulta
                Mi_SQL = "SELECT DISTINCT " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", " +
                    "(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' ' + " +
                    Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ") AS UNIDAD_RESPONSABLE, " +
                    Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " FROM " +
                    Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " ON " +
                    Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = " +
                    Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + " " +
                    "WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + " = " +
                    "'" + Datos.P_Cuenta_Contable_ID + "' ORDER BY " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " ";

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_Proyecto_Programa_Presupuesto
        /// 	DESCRIPCIÓN: Consulta los proyectos preogramas de la tabla del presupuesto aprobado
        /// 	PARÁMETROS:  Datos: Variable de la capa de negocios
        /// 	CREO: Noe Mosqueda Valadez
        /// 	FECHA_CREO:  25/Junio/2013 12:30
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        public static DataTable Consulta_Proyecto_Programa_Presupuesto(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            //Declaracion de variables
            string Mi_SQL = string.Empty; //variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //Tabla para el resultado

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT DISTINCT " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", " +
                    "(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + " +
                    Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + ") AS PROYECTO_PROGRAMA, " +
                    Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " FROM " +
                    Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " ON " +
                    Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = " +
                    Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " " +
                    "WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + " = " +
                    "'" + Datos.P_Cuenta_Contable_ID + "' AND " +
                    Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "' " +
                    "ORDER BY " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " ";

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Consulta_Partidas_Presupuesto
        /// 	DESCRIPCIÓN: Consulta las partidas de la tabla del presupuesto aprobado
        /// 	PARÁMETROS:  Datos: Variable de la capa de negocios
        /// 	CREO: Noe Mosqueda Valadez
        /// 	FECHA_CREO:  25/Junio/2013 12:30
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        public static DataTable Consulta_Partidas_Presupuesto(Cls_Ope_Con_Polizas_Negocio Datos)
        {
            //Declaracion de variables
            string Mi_SQL = string.Empty; //Variable para la consulta
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT DISTINCT " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", " +
                    "(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + " +
                    Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ") AS PARTIDA, " +
                    Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " FROM " +
                    Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " ON " +
                    Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = " +
                    Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " " +
                    "WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + " = " +
                    "'" + Datos.P_Cuenta_Contable_ID + "' AND " +
                    Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "' " +
                    "AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = " +
                    "'" + Datos.P_Programa_ID + "' ORDER BY " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " ";

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado 
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }        
    }
}