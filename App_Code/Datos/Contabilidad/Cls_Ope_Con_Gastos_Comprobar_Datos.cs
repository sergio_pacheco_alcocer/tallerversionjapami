﻿using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Gastos_Comprobar.Negocio;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Solicitud_Pagos.Datos;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Sessiones;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Autoriza_Solicitud_Pago_Contabilidad.Negocio;

namespace JAPAMI.Gastos_Comprobar.Datos
{
    public class Cls_Ope_Con_Gastos_Comprobar_Datos
    {
        #region Metodos

        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Estatus
        /// COMENTARIOS:    Consulta el estatus  de los registros en la tabla 
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     20/Enero/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Estatus(Cls_Ope_Con_Gastos_Comprobar_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                Mi_SQL.Append("Select " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "," +Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos+"."+
                                Ope_Con_Solicitud_Pagos.Campo_Estatus + "," + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + ","+Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos+"."+Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID +","+ 
                                Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + "," + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + "," +
                                Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + "," + Ope_Con_Solicitud_Pagos.Campo_Monto + ",");

                Mi_SQL.Append("(Select " + Cat_Empleados.Campo_Apellido_Paterno + "   +' '+ " + Cat_Empleados.Campo_Apellido_Materno +
                                "   +' '+ " + Cat_Empleados.Campo_Nombre + " as Nombre_Completo from " + Cat_Empleados.Tabla_Cat_Empleados +
                                " where " + Cat_Empleados.Campo_Empleado_ID + "=" + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." +
                                Ope_Con_Solicitud_Pagos.Campo_Empleado_ID + ")Nombre,");

                Mi_SQL.Append("(Select " + Cat_Com_Proveedores.Campo_Nombre + " from " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores +
                                " where " + Cat_Com_Proveedores.Campo_Proveedor_ID + "=" + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." +
                                Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + ")Proveedor, "+Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago+"."+Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion +" AS TIPO,"+
                                Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos +"."+Ope_Con_Solicitud_Pagos.Campo_Concepto);

                Mi_SQL.Append(" from " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" left join  " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago +" on "+Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago+"."+Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID+"=");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID);
                Mi_SQL.Append(" Where " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos +"."+Ope_Con_Solicitud_Pagos.Campo_Estatus + "='" + Datos.P_Estatus + "'");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }
        
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Reserva_Patida
        /// COMENTARIOS:    Consulta el estatus  de los registros en la tabla 
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     21/Enero/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Reserva_Patida(Cls_Ope_Con_Gastos_Comprobar_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                Mi_SQL.Append("Select " + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + "+" + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + "+" + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " as Codigo," +
                                               Ope_Psp_Reservas_Detalles.Campo_No_Reserva + "," + Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle +
                                               ",(select "+Cat_Sap_Partidas_Especificas.Campo_Clave + "+" + Cat_Sap_Partidas_Especificas.Campo_Descripcion + " from " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas +
                                               " where " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "=" + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles +
                                               "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ") Nombre_Partida");
                Mi_SQL.Append(" from " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                Mi_SQL.Append(" Where " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + "='" + Datos.P_No_Reserva + "'");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Resesva_Patida
        /// COMENTARIOS:    Consulta el estatus  de los registros en la tabla 
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     21/Enero/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Nombre_Patida(Cls_Ope_Con_Gastos_Comprobar_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                Mi_SQL.Append("Select ("+Cat_Sap_Partidas_Especificas.Campo_Clave+"+"+Cat_Sap_Partidas_Especificas.Campo_Descripcion+") as Nombre," + Cat_Sap_Partidas_Especificas.Campo_Descripcion);
                Mi_SQL.Append(" from " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                Mi_SQL.Append(" where " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "='" + Datos.P_Partida_ID + "'");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Cuenta_Banco
        /// COMENTARIOS:    Consulta el estatus  de los registros en la tabla 
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     23/Enero/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Cuenta_Banco(Cls_Ope_Con_Gastos_Comprobar_Negocio Datos)
        {
            //Declaracion de variables
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Da_SQL = new SqlDataAdapter();
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado de la consulta

            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            try
            {
                Mi_SQL.Append("Select " + Ope_Con_Pagos.Campo_No_Pago + "," + Ope_Con_Pagos.Campo_No_Solicitud_Pago +
                                "," + Ope_Con_Pagos.Campo_No_poliza + "," + Ope_Con_Pagos.Campo_Banco_ID + ",(select " +
                                Cat_Nom_Bancos.Campo_Cuenta_Contable_ID + " from " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " where " +
                                Cat_Nom_Bancos.Campo_Banco_ID + "=" + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." +
                                Ope_Con_Pagos.Campo_Banco_ID +") CUENTA_CONTABLE_BANCO");
                Mi_SQL.Append(" from " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos);
                Mi_SQL.Append(" where " + Ope_Con_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud + "'");

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL.ToString();
                Da_SQL.SelectCommand = Cmmd;
                Da_SQL.Fill(Dt_Resultado);

                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }

        /// ********************************************************************************************************************
        /// NOMBRE:         Cambiar_Datos_Comprobacion_Solicitud_Pago
        /// COMENTARIOS:    cambiara los datos de comproabacion de la solicutud de pago 
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     23/Enero/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static Boolean Cambiar_Datos_Comprobacion_Solicitud_Pago(Cls_Ope_Con_Gastos_Comprobar_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;

            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Da_SQL = new SqlDataAdapter();

            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            try
            {
                Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto_Aprobado + "=" + Datos.P_Monto_Aprobado + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Fecha_Comprobado + "=GETDATE(), ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Usuario_Comprobo + "='" + Datos.P_Usuario_Aprobo + "' ");
                Mi_SQL.Append(" Where " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud + "'"); 
                
                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL.ToString();
                Cmmd.ExecuteNonQuery();

                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                Operacion_Completa = true; 
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
            return Operacion_Completa;
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Alta_Documentos_Comprobados
        /// COMENTARIOS:    cambiara los datos de comproabacion de la solicutud de pago 
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     23/Enero/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static void Alta_Documentos_Comprobados(Cls_Ope_Con_Gastos_Comprobar_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            String Directorio_Destino;

            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Da_SQL = new SqlDataAdapter();

            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            try
            {
                Directorio_Destino = HttpContext.Current.Server.MapPath("~") + "\\Archivo_Solicitud_Pagos\\" + Datos.P_No_Solicitud + "\\";
                if (System.IO.Directory.Exists(Directorio_Destino) == false)
                {
                    System.IO.Directory.CreateDirectory(Directorio_Destino);
                }
                foreach (DataRow Renglon in Datos.P_Dt_Documentos_Comprobados.Rows)
                {
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("INSERT INTO " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                    Mi_SQL.Append(" (" + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura + ", ");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Factura
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID +
                                          ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Documento + ",");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Archivo
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Ruta
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID +
                                        ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_IEPS +
                                          ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID + ",");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Nombre_Proveedor_Factura
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR +
                                        ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Retencion_IVA
                                        + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula +
                                        ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_RFC +
                                          ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_CURP +
                                          ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_ISH + ",");
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_Usuario_Creo + ", " + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Creo + ")");
                    Mi_SQL.Append(" VALUES ('" + Datos.P_No_Solicitud + "', '" + Renglon["No_Documento"].ToString() + "', '" + Renglon["Fecha_Documento"].ToString() + "',");
                    Mi_SQL.Append(" " + Renglon["Monto"].ToString().Replace(",", "") + ",'" +
                                        Renglon["Partida_Id"].ToString() +
                                        "','" + Datos.P_Tipo_Documento + "','" +
                                        Renglon["Archivo"].ToString() + "','");
                    if (Renglon["Archivo"].ToString() != null && Renglon["Archivo"].ToString() != "")
                    {
                        Mi_SQL.Append(Directorio_Destino + Renglon["Archivo"].ToString() + "','");

                    }
                    else
                    {
                        Mi_SQL.Append("','");
                    }
                    Mi_SQL.Append(Renglon["Proyecto_Programa_Id"].ToString() + "'," +
                    Renglon["iva"].ToString() +
                    "," + Renglon["IEPS"].ToString() +
                    ",'" + Renglon["Fte_Financiamiento_Id"].ToString() + "','" +
                    Renglon["Nombre_Proveedor_Fact"].ToString() + "','" +
                    Renglon["Operacion"].ToString() +
                    "'," + Renglon["Retencion_ISR"].ToString() + "," +
                    Renglon["Retencion_IVA"].ToString() +
                    "," + Renglon["Retencion_Celula"].ToString() + ",'" +
                    Renglon["RFC"].ToString() +
                    "','" + Renglon["CURP"].ToString() +
                    "'," + Renglon["ISH"].ToString() + ",'" +
                    Datos.P_Usuario_Creo + "', GETDATE() )");// + Datos.P_No_Factura + "',");
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    if (Renglon["Archivo"].ToString() != null && Renglon["Archivo"].ToString() != "")
                    {
                        System.IO.File.Copy(Renglon["Ruta"].ToString(), Directorio_Destino + Renglon["Archivo"].ToString());
                        System.IO.File.Delete(Renglon["Ruta"].ToString());
                    }
                }
                Directorio_Destino = HttpContext.Current.Server.MapPath("~") + "\\Archivo_Solicitud_Pagos\\Temporal_Comprobacion";
                if (System.IO.Directory.Exists(Directorio_Destino) == true)
                {
                    String[] archivos = System.IO.Directory.GetFiles(Directorio_Destino);
                    int a;
                    for (a = 0; a < archivos.Length; a++)
                    {
                        System.IO.File.Delete(a.ToString());
                    }
                }

                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            }
        /// ********************************************************************************************************************
        /// NOMBRE:         Alta_Comprobacion_Gastos
        /// COMENTARIOS:    cambiara los datos de comproabacion de la solicutud de pago 
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     30/Agosto/2013
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static String Alta_Comprobacion_Gastos(Cls_Ope_Con_Gastos_Comprobar_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Contabilidad_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Contabilidad_Negocio();    //Objeto de acceso a los metodos.
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Solcitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
            Cls_Ope_Con_Gastos_Comprobar_Negocio Rs_Modificar_Comprobacion = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            String Respuesta = "SI";
            int Actualiza_Presupuesto=0;
            int Registra_Movimiento = 0;
            DataTable Dt_Partidas= new DataTable();
            DataTable Dt_solicitud = new DataTable();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Da_SQL = new SqlDataAdapter();

            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            try
            {
                //Creacion De la Poliza de Devengado
                string[] Datos_Poliza=Poliza_Devengado(Datos.P_Dt_Partidas_Poliza, Datos.P_Cmmd, Datos.P_Partidas, Datos.P_Monto_Aprobado);
                foreach (DataRow Fila in Datos.P_Dt_Deudas.Rows)
                {
                    Rs_Solcitud_Pago.P_No_Solicitud_Pago = Fila["No_Solicitud"].ToString();
                    Rs_Solcitud_Pago.P_Cmmd = Cmmd;
                    Dt_solicitud = Rs_Solcitud_Pago.Consulta_Datos_Solicitud_Pago_Gastos();
                    if (Dt_solicitud.Rows.Count > 0)
                    {
                        Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud_Gastos(Fila["No_Solicitud"].ToString(), Cmmd);
                        Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("COMPROMETIDO", "PRE_COMPROMETIDO", Dt_Partidas, Cmmd);
                        if (Actualiza_Presupuesto > 0)
                        {
                            Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal((Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim()), "COMPROMETIDO", "PRE_COMPROMETIDO", Convert.ToDouble(Dt_solicitud.Rows[0]["MONTO"].ToString().Trim()), "", "", "", "", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                            Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DEVENGADO", "COMPROMETIDO", Dt_Partidas, Cmmd);
                            Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal((Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim()), "DEVENGADO", "COMPROMETIDO", Convert.ToDouble(Dt_solicitud.Rows[0]["MONTO"].ToString().Trim()), Datos_Poliza[0].ToString(), Datos_Poliza[1].ToString(), Datos_Poliza[2].ToString(),"2",Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                            Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("EJERCIDO", "DEVENGADO", Dt_Partidas, Cmmd);
                            Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim(), "EJERCIDO", "DEVENGADO", Convert.ToDouble(Dt_solicitud.Rows[0]["MONTO"].ToString().Trim()), "", "", "", "", Cmmd);
                            Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PAGADO", "EJERCIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                            if (Actualiza_Presupuesto > 0)
                            {
                                Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal((Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim()), "PAGADO", "EJERCIDO", Convert.ToDouble(Dt_solicitud.Rows[0]["MONTO"].ToString().Trim()), "", "", "", "", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                            }
                            else
                            {
                                Respuesta = "NO";
                            }
                        }else
                        {
                            Respuesta = "NO";
                        }
                        if (Respuesta == "SI")
                        {
                            //  para el estatus
                            Rs_Solicitud_Pago.P_No_Solicitud_Pago = Fila["No_Solicitud"].ToString();
                            Rs_Solicitud_Pago.P_Estatus = "PAGADO";
                            Rs_Solicitud_Pago.P_Comentario = "";
                            Rs_Solicitud_Pago.P_Empleado_ID_Contabilidad = Cls_Sessiones.Empleado_ID.ToString();
                            Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                            Rs_Solicitud_Pago.P_Cmmd = Cmmd;
                            Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                            //  para los datos de comprobacion de documentos
                            Rs_Modificar_Comprobacion.P_No_Solicitud = Fila["No_Solicitud"].ToString();
                            Rs_Modificar_Comprobacion.P_Monto_Aprobado = Fila["Monto"].ToString();
                            Rs_Modificar_Comprobacion.P_Usuario_Aprobo = Cls_Sessiones.Nombre_Empleado;
                            Rs_Modificar_Comprobacion.P_Cmmd = Cmmd;
                            Rs_Modificar_Comprobacion.Cambiar_Datos_Comprobacion_Solicitud_Pago();
                        }
                    }
                }
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Respuesta;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_No_Deuda
        /// COMENTARIOS:    cambiara los datos de comproabacion de la solicutud de pago 
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Sergio Manuel Gallardo Andrade
        /// FECHA CREÓ:     30/Agosto/2013
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static String Consultar_No_Deuda(Cls_Ope_Con_Gastos_Comprobar_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            String Resultado = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Da_SQL = new SqlDataAdapter();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            try
            {
                Mi_SQL.Append("SELECT  " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores+"."+OPE_CON_DEUDORES.Campo_No_Deuda);
                Mi_SQL.Append(" FROM "+ OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores);
                Mi_SQL.Append(" LEFT JOIN "+Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas +" ON "+ Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas +"."+Ope_Psp_Reservas.Campo_No_Deuda+" = "+OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores +"."+ OPE_CON_DEUDORES.Campo_No_Deuda);
                Mi_SQL.Append(" LEFT JOIN "+ Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos +" ON "+ Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos+"."+Ope_Con_Solicitud_Pagos.Campo_No_Reserva+" = "+Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas +"."+Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_SQL.Append(" Where " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_Solicitud + "'");
                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL.ToString();
                Resultado=Cmmd.ExecuteScalar().ToString();
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Resultado;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        
        /// ********************************************************************************************************************
        /// NOMBRE:         Poliza_Devengado
        /// COMENTARIOS:    Crear la poliza del devengado con el datatable que contiene las partidad
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Sergio Manuel Gallardo Andrade
        /// FECHA CREÓ:     30/Agosto/2013
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static string[] Poliza_Devengado(DataTable Dt_Partidas_Polizas, SqlCommand P_Cmmd, String Partidas, String Monto)//, String Comentario, String Empleado_ID, String Nombre_Empleado
        {
            Cls_Ope_Con_Polizas_Negocio Rs_Polizas = new Cls_Ope_Con_Polizas_Negocio();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            try
            {
                if (P_Cmmd != null)
                {
                    Cmmd = P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                //Alta poliza Devengado
                Rs_Polizas.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
                Rs_Polizas.P_Tipo_Poliza_ID = "00003";
                Rs_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", Convert.ToDateTime(DateTime.Now.ToString()));
                Rs_Polizas.P_Fecha_Poliza = Convert.ToDateTime(DateTime.Now.ToString());
                Rs_Polizas.P_Concepto = "Comprobacion de Gastos";
                Rs_Polizas.P_Total_Debe = Convert.ToDouble(Monto);
                Rs_Polizas.P_Total_Haber = Convert.ToDouble(Monto);
                Rs_Polizas.P_No_Partida = Convert.ToInt32(Partidas);
                Rs_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                Rs_Polizas.P_Dt_Detalles_Polizas = Dt_Partidas_Polizas;
                Rs_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID;
                Rs_Polizas.P_Empleado_ID_Autorizo = Cls_Sessiones.Empleado_ID;
                Rs_Polizas.P_Cmmd = Cmmd;
                string[] Datos_Poliza = Rs_Polizas.Alta_Poliza(); //Da de alta los datos de la Póliza proporcionados por el usuario en la BD
                return Datos_Poliza;
            }
            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        #endregion
    }
}
