﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Con_Parametros_Almacen.Negocio;
using System.Data;
using JAPAMI.Constantes;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;

/// <summary>
/// Summary description for Cls_Cat_Con_Parametros_Almacen_Datos
/// </summary>
/// 
namespace JAPAMI.Con_Parametros_Almacen.Datos
{
    
    public class Cls_Cat_Con_Parametros_Almacen_Datos
    {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Tipos_Polizas
        ///DESCRIPCIÓN: Metodo que consultalos tipos de poliza. 
        ///PARAMETROS: 1.- Cls_Ope_Com_Definir_Cotizadores_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 22/NOV/12
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************

        public static DataTable Consultar_Cuentas_Almacen(Cls_Cat_Con_Parametros_Almacen_Negocio Clase_Negocio)
        {
            String Mi_SQL;
            DataTable Dt_Cuentas_Almacen;
            try
            {
                Mi_SQL = "SELECT *";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Alm_Parametros_Cuentas.Tabla_Cat_Alm_Parametros_Cuentas;

                Dt_Cuentas_Almacen = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Cuentas_Almacen;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Tipos_Polizas
        ///DESCRIPCIÓN: Metodo que consultalos tipos de poliza. 
        ///PARAMETROS: 1.- Cls_Ope_Com_Definir_Cotizadores_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 22/NOV/12
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Tipos_Polizas(Cls_Cat_Con_Parametros_Almacen_Negocio Clase_Negocio)
        {
            String Mi_SQL;
            DataTable Dt_Tipo_Polozas;
            try
            {
            Mi_SQL = "SELECT " + Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID;
            Mi_SQL = Mi_SQL + ", " + Cat_Con_Tipo_Polizas.Campo_Descripcion;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas;

            Dt_Tipo_Polozas = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Tipo_Polozas;
           }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Parametros
        ///DESCRIPCIÓN: Metodo que consultalos los Parametros. 
        ///PARAMETROS: 1.- Cls_Cat_Con_Parametros_Almacen_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 22/NOV/12
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Parametros(Cls_Cat_Con_Parametros_Almacen_Negocio Clase_Negocio)
        {
            String Mi_SQL;
            DataTable Dt_Tipo_Polozas;
            try
            {
                Mi_SQL = "SELECT " + Cat_Con_Parametros.Campo_Tipo_Poliza_Egresos_ID;
                Mi_SQL = Mi_SQL + ", " + Cat_Con_Parametros.Campo_Tipo_Poliza_Diario_ID;
                Mi_SQL = Mi_SQL + ", " + Cat_Con_Parametros.Campo_Cuenta_Almacen_General;
                Mi_SQL = Mi_SQL + ", " + Cat_Con_Parametros.Campo_Cuenta_Almacen_Papeleria;
                Mi_SQL = Mi_SQL + ", " + Cat_Con_Parametros.Campo_Cuenta_IVA_Acreditable;
                 Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros;

                Dt_Tipo_Polozas = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Tipo_Polozas;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Cuentas_Cotables
        ///DESCRIPCIÓN: Metodo que consultalos los Parametros. 
        ///PARAMETROS: 1.- Cls_Ope_Com_Definir_Cotizadores_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 22/NOV/12
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Cuentas_Cotables(Cls_Cat_Con_Parametros_Almacen_Negocio Clase_Negocio)
        {
            String Mi_SQL;
            DataTable Dt_Tipo_Polozas;
            try
            {
                Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Mi_SQL = Mi_SQL + ", " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " + '-' + " + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                Mi_SQL = Mi_SQL + ", " + Cat_Con_Cuentas_Contables.Campo_Cuenta;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Cuenta_Busqueda)) 
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '%" + Clase_Negocio.P_Cuenta_Busqueda + "%'";
                }
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Cuenta_ID))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = '" + Clase_Negocio.P_Cuenta_ID + "'";
                }
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Cuentas_ID))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " in ( " + Clase_Negocio.P_Cuentas_ID + ")";
                }

                Dt_Tipo_Polozas = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Tipo_Polozas;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Tipos_Polizas
        ///DESCRIPCIÓN: Metodo que da de alta la fila de los parametros en caso de no existir ninguno  
        ///PARAMETROS: 1.- Cls_Ope_Com_Definir_Cotizadores_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 22/NOV/12
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Alta_Parametros_Almacen(Cls_Cat_Con_Parametros_Almacen_Negocio Negocios)
        {
            String Mi_SQL;   //Variable de Consulta para la Alta del de una Nueva Mascara
            Object Parametros_ID; //Variable que contendrá el ID de la consulta
            String Parametro_ID = "";
            try
            {
                //Busca el maximo ID de la tabla Parametros.
                Mi_SQL = "SELECT ISNULL(MAX (" + Cat_Con_Parametros.Campo_Parametro_Contabilidad_ID + "),'00000') ";
                Mi_SQL = Mi_SQL + "FROM " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros;
                Parametros_ID = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                if (Convert.IsDBNull(Parametros_ID)) //Si no existen valores en la tabla, asigna el primer valor manualmente.
                {
                   Parametro_ID = "00001";
                }
                else // Si ya existen registros, toma el valor maximo y le suma 1 para el nuevo registro.
                {
                    Parametro_ID = String.Format("{0:00000}", Convert.ToInt32(Parametros_ID) + 1);
                }
                //Da de Alta los datos del Nuevo Parametro con los datos proporcionados por el usuario.
                Mi_SQL = "INSERT INTO " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros + " (";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Parametro_Contabilidad_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Tipo_Poliza_Egresos_ID+ ", ";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Tipo_Poliza_Diario_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Cuenta_Almacen_General + ", ";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Cuenta_Almacen_Papeleria+ ", ";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Cuenta_IVA_Acreditable ;
                Mi_SQL = Mi_SQL + ") VALUES ('" + Parametro_ID + "', '";
                Mi_SQL = Mi_SQL + Negocios.P_Tipo_Poliza_Egresos_ID + "', '";
                Mi_SQL = Mi_SQL + Negocios.P_Tipo_Poliza_Diario_ID + "', '";
                Mi_SQL = Mi_SQL + Negocios.P_Cuenta_Almacen_General + "', '";
                Mi_SQL = Mi_SQL + Negocios.P_Cuenta_Almacen_Papeleria + "', '";
                Mi_SQL = Mi_SQL + Negocios.P_Cuenta_IVA_Acreditable + "')";

                //Manda Mi_SQL para ser procesada.
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Parametros_Almacen
        /// DESCRIPCION : 1. Modifica el unico registro de los parametros
        /// PARAMETROS  : Clase_Negocio: Objeto de la clase de Negocio  que almacena los 
        ///               datos que van a modificarse en la BD.
        /// CREO        : Jennyfer Ivonne Ceja Lemus
        /// FECHA_CREO  : 27/Noviembre/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Modificar_Parametros_Almacen(Cls_Cat_Con_Parametros_Almacen_Negocio Clase_Negocio)
        {
            String Mi_SQL;   //Variable de Consulta para la Alta del de una Nueva Mascara

            try
            {    
                //Consulatamos si existen datos 
                Mi_SQL = "SELECT *";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros;

                DataTable Dt = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                if (Dt.Rows.Count > 0)
                {
                    //Modifica los datos del Nuevo Parametro con los datos proporcionados por el usuario.
                    Mi_SQL = "UPDATE " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros;
                    Mi_SQL += " SET ";
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Tipo_Poliza_Egresos_ID + " = '" + Clase_Negocio.P_Tipo_Poliza_Egresos_ID + "', ";
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Tipo_Poliza_Diario_ID + " = '" + Clase_Negocio.P_Tipo_Poliza_Diario_ID + "' ";
                    Mi_SQL += " WHERE " + Cat_Con_Parametros.Campo_Parametro_Contabilidad_ID + " = '00001'";
                }
                else
                {
                    Mi_SQL = "INSERT INTO " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros;
                    Mi_SQL = Mi_SQL + " (" +  Cat_Con_Parametros.Campo_Parametro_Contabilidad_ID + "," +Cat_Con_Parametros.Campo_Tipo_Poliza_Egresos_ID + "," + Cat_Con_Parametros.Campo_Tipo_Poliza_Diario_ID + ") VALUES(";
                    Mi_SQL = Mi_SQL + "'00001',";
                    Mi_SQL = Mi_SQL + " '" + Clase_Negocio.P_Tipo_Poliza_Egresos_ID + "', ";
                    Mi_SQL = Mi_SQL + " '" + Clase_Negocio.P_Tipo_Poliza_Diario_ID + "') ";
                    
                }
                //Manda Mi_SQL para ser procesada por ORACLE.
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                //Consulatamos si existen datos 
                Mi_SQL = "SELECT *";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros;
                Dt = new DataTable();
                Dt = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                if (Dt.Rows.Count > 0)
                {
                    //Modifica los datos del Nuevo Parametro con los datos proporcionados por el usuario.
                    Mi_SQL = "UPDATE " + Cat_Alm_Parametros_Cuentas.Tabla_Cat_Alm_Parametros_Cuentas;
                    Mi_SQL = Mi_SQL + " SET ";
                    Mi_SQL = Mi_SQL + Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_Papeleria_ID + " = '" + Clase_Negocio.P_Cuenta_Almacen_Papeleria.Trim() + "', ";
                    Mi_SQL = Mi_SQL + Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_General_ID + " = '" + Clase_Negocio.P_Cuenta_Almacen_General.Trim() + "', ";
                    Mi_SQL = Mi_SQL + Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Pendiente + "='" + Clase_Negocio.P_Cuenta_IVA_Pendiente.Trim() + "', ";
                    Mi_SQL = Mi_SQL + Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Acreditable + "='" + Clase_Negocio.P_Cuenta_IVA_Acreditable.Trim() + "' ";
                }
                else
                {
                    Mi_SQL = "INSERT INTO " + Cat_Alm_Parametros_Cuentas.Tabla_Cat_Alm_Parametros_Cuentas;
                    Mi_SQL = Mi_SQL + "(" + Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_General_ID;
                    Mi_SQL = Mi_SQL + "," + Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_Papeleria_ID;
                    Mi_SQL = Mi_SQL + "," + Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Acreditable;
                    Mi_SQL = Mi_SQL + "," + Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Pendiente + ") VALUES(";
                    Mi_SQL = Mi_SQL + "'" + Clase_Negocio.P_Cuenta_Almacen_General.Trim() + "',";
                    Mi_SQL = Mi_SQL + "'" + Clase_Negocio.P_Cuenta_Almacen_Papeleria.Trim() + "',";
                    Mi_SQL = Mi_SQL + "'" + Clase_Negocio.P_Cuenta_IVA_Acreditable.Trim() + "',";
                    Mi_SQL = Mi_SQL + "'" + Clase_Negocio.P_Cuenta_IVA_Pendiente.Trim() + "')";
                }

                //Manda Mi_SQL para ser procesada por ORACLE.
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
    }//Fin del class
}//fin del namespace