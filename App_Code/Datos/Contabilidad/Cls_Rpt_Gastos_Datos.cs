﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Reporte_Gasto.Negocio;
using SharpContent.ApplicationBlocks.Data;

/// <summary>
/// Summary description for Cls_Rpt_Gastos_Datos
/// </summary>
namespace JAPAMI.Reporte_Gasto.Datos
{
    public class Cls_Rpt_Gastos_Datos
    {
        public Cls_Rpt_Gastos_Datos()
        {
        }

        #region (Metodos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Unidades_Responsables
        ///DESCRIPCION:             Consulta de las unidades responsables con filtro de gerencia
        ///PARAMETROS:              Datos: Variable de la capa de negocio
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              23/Abril/2012 18:57
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Unidades_Responsables(Cls_Rpt_Gastos_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado de la consulta

            try
            {
                //Asignar consulta
                //Mi_SQL = "SELECT Dependencia_ID, (Clave + ' ' + Comentarios) AS Unidad_Responsable FROM Cat_Dependencias ";
                Mi_SQL = "SELECT Dependencia_ID, (Clave + ' ' + Nombre) AS Unidad_Responsable FROM Cat_Dependencias ";

                //verificar si hay una gerencia
                if (String.IsNullOrEmpty(Datos.P_Gerencia_ID) == false)
                {
                    Mi_SQL += "WHERE Grupo_Dependencia_ID = '" + Datos.P_Gerencia_ID + "' ";
                }

                Mi_SQL += "ORDER BY Comentarios, Clave";

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Gerencias
        ///DESCRIPCION:             Consultar las gerencias de las Unidades Responsables
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              23/Abril/2012 18:40
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Gerencias()
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado de la consulta

            try
            {
                //Asignar consulta
                //Mi_SQL = "SELECT Grupo_Dependencia_ID, (Clave + ' ' + Comentarios) AS Gerencia FROM Cat_Grupos_Dependencias " +
                //    "ORDER BY Comentarios, Clave ";
                Mi_SQL = "SELECT Grupo_Dependencia_ID, (Clave + ' ' + Nombre) AS Gerencia FROM Cat_Grupos_Dependencias " +
                    "ORDER BY Comentarios, Clave ";

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];


                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Gasto
        ///DESCRIPCION:             Consulta para el gasto de acuerdo a la unidad responsable o la gerencia
        ///PARAMETROS:              Datos: Variable de la capa de negocios
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              24/Abril/2012 18:09
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Gasto(Cls_Rpt_Gastos_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //Variable para la consulta
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado
            Object Aux; //Objeto para las consultas escalares
            int Cont_Elementos = 0; //variable para el contador
            DataTable Dt_Aux = new DataTable(); //Tabla para las consultas auxiliares
            String ID_Egresos = String.Empty; //Variable para el ID de los egresos (tipo de poliza)
            DataTable Dt_Unidades_Responsable = new DataTable(); //Tabla para la consulta de las unidades responsables
            String Gerencia = String.Empty; //variable para la gerencia
            String Unidad_Responsable = String.Empty; //Variable para la unidad responsable
            DataRow Renglon; //renglon para el llenado de al tabla
            int Cont_Aux = 0; //variable auxiliar para el contador
            DataTable Dt_Gerencias = new DataTable(); //tabla para las gerencias
            int Cont_Aux_2 = 0; //variable auxiliar para el contador

            try
            {
                //Consulta para el ID de los egresos de la tabla de los parametros
                Mi_SQL = "SELECT Tipo_Poliza_Egresos_ID FROM Cat_Con_Parametros WHERE Parametro_Contabilidad_ID = '00001' ";

                //Ejecutar consulta
                Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                if (Aux != null)
                {
                    //Asignar el ID de los egresos
                    ID_Egresos = Aux.ToString().Trim();

                    //Construir la estructura de la tabla resultante
                    Dt_Resultado.Columns.Add("Gerencia", typeof(String));
                    Dt_Resultado.Columns.Add("Unidad_Responsable", typeof(String));
                    Dt_Resultado.Columns.Add("Partida", typeof(String));
                    Dt_Resultado.Columns.Add("Total", typeof(Double));

                    //Verificar si hay una dependencia seleccionada
                    if (String.IsNullOrEmpty(Datos.P_Unidad_Responsable_ID) == false)
                    {
                        //Consulta para la gerencia de la unidad responsable
                        //Mi_SQL = "SELECT (Cat_Grupos_Dependencias.Clave + ' ' + Cat_Grupos_Dependencias.Comentarios) AS Gerencia FROM Cat_Grupos_Dependencias " +
                        //    "INNER JOIN Cat_Dependencias ON Cat_Grupos_Dependencias.Grupo_Dependencia_ID = Cat_Dependencias.Grupo_Dependencia_ID " +
                        //    "WHERE Cat_Dependencias.Dependencia_ID = '" + Datos.P_Unidad_Responsable_ID + "' ";
                        Mi_SQL = "SELECT (Cat_Grupos_Dependencias.Clave + ' ' + Cat_Grupos_Dependencias.Nombre) AS Gerencia FROM Cat_Grupos_Dependencias " +
                            "INNER JOIN Cat_Dependencias ON Cat_Grupos_Dependencias.Grupo_Dependencia_ID = Cat_Dependencias.Grupo_Dependencia_ID " +
                            "WHERE Cat_Dependencias.Dependencia_ID = '" + Datos.P_Unidad_Responsable_ID + "' ";

                        //Ejecutar consulta
                        Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                        //Asignar la gerencia
                        if (Aux != null)
                        {
                            Gerencia = Aux.ToString().Trim();
                        }

                        //Consulta para la unidad responsable
                        //Mi_SQL = "SELECT (Clave + ' ' + Comentarios) AS Unidad_Responsable FROM Cat_Dependencias " +
                        //    "WHERE Dependencia_ID = '" + Datos.P_Unidad_Responsable_ID + "' ";
                        Mi_SQL = "SELECT (Clave + ' ' + Nombre) AS Unidad_Responsable FROM Cat_Dependencias " +
                        "WHERE Dependencia_ID = '" + Datos.P_Unidad_Responsable_ID + "' ";

                        //Ejecutar consulta
                        Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                        //Asignar la unidad responsable
                        if (Aux != null)
                        {
                            Unidad_Responsable = Aux.ToString().Trim();
                        }

                        //Consulta para las partidas
                        Mi_SQL = "SELECT (Cat_Sap_Partida_Generica.Clave + ' ' + Cat_Sap_Partida_Generica.Descripcion) AS Partida, " +
                            "SUM(Ope_Con_Polizas_Detalles.Saldo) AS Total " +
                            "FROM Ope_Con_Polizas_Detalles " +
                            "INNER JOIN Cat_Sap_Partidas_Especificas ON Ope_Con_Polizas_Detalles.Partida_ID = Cat_Sap_Partidas_Especificas.Partida_ID " +
                            "INNER JOIN Cat_Sap_Partida_Generica ON Cat_Sap_Partidas_Especificas.Partida_Generica_ID = Cat_Sap_Partida_Generica.Partida_Generica_ID " +
                            "WHERE Ope_Con_Polizas_Detalles.Dependencia_ID = '" + Datos.P_Unidad_Responsable_ID + "' " +
                            "AND Ope_Con_Polizas_Detalles.Tipo_Poliza_ID ='" + ID_Egresos + "' ";

                        //Verificar si hay fechas
                        if (String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Inicio) != "01/01/0001")
                        {
                            Mi_SQL += "AND Ope_Con_Polizas_Detalles.Fecha >= '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Inicio) + " 00:00:00' ";

                            if (String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Fin) != "01/01/0001")
                            {
                                Mi_SQL += "AND Ope_Con_Polizas_Detalles.Fecha <= '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Fin) + " 23:59:59' ";
                            }
                        }

                        Mi_SQL += "GROUP BY Cat_Sap_Partida_Generica.Clave, Cat_Sap_Partida_Generica.Descripcion " +
                            "ORDER BY Cat_Sap_Partida_Generica.Descripcion, Cat_Sap_Partida_Generica.Clave ";

                        //Ejecutar consulta
                        Dt_Aux.Clear();
                        Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                        //Verificar si la consulta arrojo resultados
                        if (Dt_Aux.Rows.Count > 0)
                        {
                            //Ciclo para el barrido de la tabla
                            for (Cont_Elementos = 0; Cont_Elementos < Dt_Aux.Rows.Count; Cont_Elementos++)
                            {
                                //Instanciar renglon de la tabla del resultado
                                Renglon = Dt_Resultado.NewRow();

                                //Llenar e insertar renglon en la tabla
                                Renglon["Gerencia"] = Gerencia;
                                Renglon["Unidad_Responsable"] = Unidad_Responsable;
                                Renglon["Partida"] = Dt_Aux.Rows[Cont_Elementos]["Partida"];
                                Renglon["Total"] = Dt_Aux.Rows[Cont_Elementos]["Total"];
                                Dt_Resultado.Rows.Add(Renglon);
                            }
                        }
                    }
                    else
                    {
                        //Verificar si se tiene el ID de la gerencia
                        if (String.IsNullOrEmpty(Datos.P_Gerencia_ID) == false)
                        {
                            //Consulta para los datos de la gerencia
                            Mi_SQL = "SELECT (Clave + ' ' + Comentarios) AS Gerencia FROM Cat_Grupos_Dependencias " +
                                "WHERE Grupo_Dependencia_ID = '" + Datos.P_Gerencia_ID + "' ";

                            //Ejecutar consulta
                            Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                            //Asignar el valor de la gerencia
                            if (Aux != null)
                            {
                                Gerencia = Aux.ToString().Trim();
                            }

                            //Consulta para las dependencias de la gerencia
                            Mi_SQL = "SELECT Dependencia_ID, (Clave + ' ' + Comentarios) AS Unidad_Responsable FROM Cat_Dependencias " +
                                "WHERE Grupo_Dependencia_ID = '" + Datos.P_Gerencia_ID + "' " +
                                "ORDER BY Comentarios, Clave ";

                            //Ejecutar consulta
                            Dt_Unidades_Responsable = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                            //Verificar si la consulta arrojo resultados
                            if (Dt_Unidades_Responsable.Rows.Count > 0)
                            {
                                //Ciclo para el barrido de la tabla
                                for (Cont_Elementos = 0; Cont_Elementos < Dt_Unidades_Responsable.Rows.Count; Cont_Elementos++)
                                {
                                    //Consulta para los totales por dependencia
                                    Mi_SQL = "SELECT (Cat_Sap_Partida_Generica.Clave + ' ' + Cat_Sap_Partida_Generica.Descripcion) AS Partida, " +
                                        "SUM(Ope_Con_Polizas_Detalles.Saldo) AS Total " +
                                        "FROM Ope_Con_Polizas_Detalles " +
                                        "INNER JOIN Cat_Sap_Partidas_Especificas ON Ope_Con_Polizas_Detalles.Partida_ID = Cat_Sap_Partidas_Especificas.Partida_ID " +
                                        "INNER JOIN Cat_Sap_Partida_Generica ON Cat_Sap_Partidas_Especificas.Partida_Generica_ID = Cat_Sap_Partida_Generica.Partida_Generica_ID " +
                                        "WHERE Ope_Con_Polizas_Detalles.Dependencia_ID = '" + Dt_Unidades_Responsable.Rows[Cont_Elementos][Cat_Dependencias.Campo_Dependencia_ID].ToString().Trim() + "' " +
                                        "AND Ope_Con_Polizas_Detalles.Tipo_Poliza_ID ='" + ID_Egresos + "' ";

                                    //Verificar si hay fechas
                                    if (String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Inicio) != "01/01/0001")
                                    {
                                        Mi_SQL += "AND Ope_Con_Polizas_Detalles.Fecha >= '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Inicio) + " 00:00:00' ";

                                        if (String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Fin) != "01/01/0001")
                                        {
                                            Mi_SQL += "AND Ope_Con_Polizas_Detalles.Fecha <= '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Fin) + " 23:59:59' ";
                                        }
                                    }

                                    Mi_SQL += "GROUP BY Cat_Sap_Partida_Generica.Clave, Cat_Sap_Partida_Generica.Descripcion " +
                                        "ORDER BY Cat_Sap_Partida_Generica.Descripcion, Cat_Sap_Partida_Generica.Clave ";

                                    //ejecutar consulta de los totales
                                    Dt_Aux.Clear();
                                    Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                                    //Verificar si la consulta arrojo resultados
                                    if (Dt_Aux.Rows.Count > 0)
                                    {
                                        //Ciclo para el barrido de la tabla
                                        for (Cont_Aux = 0; Cont_Aux < Dt_Aux.Rows.Count; Cont_Aux++)
                                        {
                                            //Instanciar renglon 
                                            Renglon = Dt_Resultado.NewRow();

                                            //Llenar el renglon y colocarlo en la tabla
                                            Renglon["Gerencia"] = Gerencia;
                                            Renglon["Unidad_Responsable"] = Dt_Unidades_Responsable.Rows[Cont_Elementos]["Unidad_Responsable"];
                                            Renglon["Partida"] = Dt_Aux.Rows[Cont_Aux]["Partida"];
                                            Renglon["Total"] = Dt_Aux.Rows[Cont_Aux]["Total"];
                                            Dt_Resultado.Rows.Add(Renglon);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            //Consulta para las gerencias
                            Mi_SQL = "SELECT Grupo_Dependencia_ID, (Clave + ' ' + Comentarios) AS Gerencia FROM Cat_Grupos_Dependencias " +
                                "ORDER BY Comentarios, Clave ";

                            //Ejecutar consulta
                            Dt_Gerencias = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                            //Verificar si la consulta arrojo resultados
                            if (Dt_Gerencias.Rows.Count > 0)
                            {
                                //Ciclo para el barrido de la tabla
                                for (Cont_Elementos = 0; Cont_Elementos < Dt_Gerencias.Rows.Count; Cont_Elementos++)
                                {
                                    //Consulta para las dependencias de la gerencia
                                    Mi_SQL = "SELECT Dependencia_ID, (Clave + ' ' + Comentarios) AS Unidad_Responsable FROM Cat_Dependencias " +
                                        "WHERE Grupo_Dependencia_ID = '" + Dt_Gerencias.Rows[Cont_Elementos][Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID].ToString().Trim() + "' " +
                                        "ORDER BY Comentarios, Clave ";


                                    //Ejecutar consulta
                                    Dt_Unidades_Responsable.Clear();
                                    Dt_Unidades_Responsable = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                                    //verificar si la consulta arrojo resultados
                                    if (Dt_Unidades_Responsable.Rows.Count > 0)
                                    {
                                        //Ciclo para el barrido de la tabla
                                        for (Cont_Aux = 0; Cont_Aux < Dt_Unidades_Responsable.Rows.Count; Cont_Aux++)
                                        {
                                            //Consulta para los totales por dependencia
                                            Mi_SQL = "SELECT (Cat_Sap_Partida_Generica.Clave + ' ' + Cat_Sap_Partida_Generica.Descripcion) AS Partida, " +
                                                "SUM(Ope_Con_Polizas_Detalles.Saldo) AS Total " +
                                                "FROM Ope_Con_Polizas_Detalles " +
                                                "INNER JOIN Cat_Sap_Partidas_Especificas ON Ope_Con_Polizas_Detalles.Partida_ID = Cat_Sap_Partidas_Especificas.Partida_ID " +
                                                "INNER JOIN Cat_Sap_Partida_Generica ON Cat_Sap_Partidas_Especificas.Partida_Generica_ID = Cat_Sap_Partida_Generica.Partida_Generica_ID " +
                                                "WHERE Ope_Con_Polizas_Detalles.Dependencia_ID = '" + Dt_Unidades_Responsable.Rows[Cont_Aux][Cat_Dependencias.Campo_Dependencia_ID].ToString().Trim() + "' " +
                                                "AND Ope_Con_Polizas_Detalles.Tipo_Poliza_ID ='" + ID_Egresos + "' ";

                                            //Verificar si hay fechas
                                            if (String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Inicio) != "01/01/0001")
                                            {
                                                Mi_SQL += "AND Ope_Con_Polizas_Detalles.Fecha >= '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Inicio) + " 00:00:00' ";

                                                if (String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Fin) != "01/01/0001")
                                                {
                                                    Mi_SQL += "AND Ope_Con_Polizas_Detalles.Fecha <= '" + String.Format("{0:dd/MM/yyyy}", Datos.P_Fecha_Fin) + " 23:59:59' ";
                                                }
                                            }

                                            Mi_SQL += "GROUP BY Cat_Sap_Partida_Generica.Clave, Cat_Sap_Partida_Generica.Descripcion " +
                                                "ORDER BY Cat_Sap_Partida_Generica.Descripcion, Cat_Sap_Partida_Generica.Clave ";

                                            //ejecutar consulta de los totales
                                            Dt_Aux.Clear();
                                            Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                                            //Verificar si la consulta arrojo resultados
                                            if (Dt_Aux.Rows.Count > 0)
                                            {
                                                //Ciclo para el barrido de la tabla
                                                for (Cont_Aux_2 = 0; Cont_Aux_2 < Dt_Aux.Rows.Count; Cont_Aux_2++)
                                                {
                                                    //Instanciar el renglon de la tabla del resultado
                                                    Renglon = Dt_Resultado.NewRow();

                                                    //Llenar el renglon y colocarlo en la tabla
                                                    Renglon["Gerencia"] = Dt_Gerencias.Rows[Cont_Elementos]["Gerencia"];
                                                    Renglon["Unidad_Responsable"] = Dt_Unidades_Responsable.Rows[Cont_Aux]["Unidad_Responsable"];
                                                    Renglon["Partida"] = Dt_Aux.Rows[Cont_Aux_2]["Partida"];
                                                    Renglon["Total"] = Dt_Aux.Rows[Cont_Aux_2]["Total"];
                                                    Dt_Resultado.Rows.Add(Renglon);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
        #endregion
    }
}