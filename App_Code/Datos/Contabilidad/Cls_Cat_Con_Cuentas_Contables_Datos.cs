﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Cuentas_Contables.Negocio;

namespace JAPAMI.Cuentas_Contables.Datos
{
    public class Cls_Cat_Con_Cuentas_Contables_Datos
    {
        #region (Métodos Operación)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Alta_Cuenta_Contable
            /// DESCRIPCION : 1.Consulta el último ID dado de alta para poder ingresar el siguiente
            ///               2. Da de Alta la Cuenta Contable en la BD con los datos proporcionados 
            ///                  por elusuario
            /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 10-Junio-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static void Alta_Cuenta_Contable(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                //Declaracion de variables
                String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
                SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
                SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
                SqlTransaction Transaccion_SQL = null;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
                Object Cuenta_Contable_ID; //Variable que contendrá el ID de la consulta

                try
                {
                    if (Conexion_Base.State != ConnectionState.Open)
                    {
                        Conexion_Base.Open(); //Abre la conexión a la base de datos            
                    }
                    Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                    Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                    Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos

                    //Obtiene el ID del Nivel a dar de alta
                    Mi_SQL = "SELECT ISNULL(MAX (" + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "),'00000') ";
                    Mi_SQL = Mi_SQL + "FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;

                    //Ejecutar consulta
                    Comando_SQL.CommandText = Mi_SQL;
                    Cuenta_Contable_ID = Comando_SQL.ExecuteScalar();

                    if (Convert.IsDBNull(Cuenta_Contable_ID))
                    {
                        Datos.P_Cuenta_Contable_ID = "00001";
                    }
                    else
                    {
                        Datos.P_Cuenta_Contable_ID = String.Format("{0:00000}", Convert.ToInt32(Cuenta_Contable_ID) + 1);
                    }
                    //Da de Alta los datos de la Cuenta Contable con los datos proporcionados por el usuario
                    Mi_SQL = "INSERT INTO " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " (";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ", ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Nivel_ID + ", ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Tipo_Balance_ID + ", ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Tipo_Resultado_ID + ", ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Descripcion + ", ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Cuenta + ", ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Afectable + ", ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Comentarios + ", ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Partida_ID + ", ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Usuario_Creo + ", ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta + ", ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Tipo_Presupuestal + ", ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Fecha_Creo + ", " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID_Padre + ") ";
                    Mi_SQL = Mi_SQL + "VALUES ('" + Datos.P_Cuenta_Contable_ID + "', '";
                    Mi_SQL = Mi_SQL + Datos.P_Nivel_ID + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Balance_ID))
                    {
                        Mi_SQL = Mi_SQL + "'" + Datos.P_Tipo_Balance_ID + "', ";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "NULL, ";
                    }
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Resultado_ID))
                    {
                        Mi_SQL = Mi_SQL + "'" + Datos.P_Tipo_Resultado_ID + "', '";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "NULL, '";
                    }
                    Mi_SQL = Mi_SQL + Datos.P_Descripcion + "', '";
                    Mi_SQL = Mi_SQL + Datos.P_Cuenta + "', '";
                    Mi_SQL = Mi_SQL + Datos.P_Afectable + "', '";
                    Mi_SQL = Mi_SQL + Datos.P_Comentarios + "', ";

                    //Verificar si hay partida
                    if (String.IsNullOrEmpty(Datos.P_Partida_ID) == false)
                    {
                        Mi_SQL = Mi_SQL + "'" + Datos.P_Partida_ID + "', ";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "NULL, ";
                    }

                    Mi_SQL = Mi_SQL + "'" + Datos.P_Nombre_Usuario + "', ";

                    //Verificar si hay tipo cuenta
                    if (String.IsNullOrEmpty(Datos.P_Tipo_Cuenta) == false)
                    {
                        Mi_SQL = Mi_SQL + "'" + Datos.P_Tipo_Cuenta + "', ";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + "NULL, ";
                    }

                    Mi_SQL += "'" + Datos.P_Tipo_Presupuestal + "', ";
                    Mi_SQL += "GETDATE(),'" + Datos.P_Cuenta_Contable_ID_Padre + "')";

                    //Ejecutar consulta
                    Comando_SQL.CommandText = Mi_SQL;
                    Comando_SQL.ExecuteNonQuery();
                    
                    //Ejecutar transaccion
                    Transaccion_SQL.Commit();
                }
                catch (SqlException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    Conexion_Base.Close();
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Modificar_Cuenta_Contable
            /// DESCRIPCION : Modifica los datos de la Cuenta Contable con lo que fueron introducidos 
            ///              por el usuario
            /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
            ///                       proporcionados por el usuario y van a sustituir a los datos que se
            ///                       encuentran en la base de datos
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 10-Junio-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static void Modificar_Cuenta_Contable(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                //Declaracion de variables
                String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
                SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
                SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
                SqlTransaction Transaccion_SQL = null;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        

                try
                {
                    if (Conexion_Base.State != ConnectionState.Open)
                    {
                        Conexion_Base.Open(); //Abre la conexión a la base de datos            
                    }
                    Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                    Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                    Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos

                    //Consulta para la modificación de la Cuenta Contable con los datos proporcionados por el usuario
                    Mi_SQL = "UPDATE " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " SET ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Nivel_ID + " = '" + Datos.P_Nivel_ID + "', ";
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Balance_ID))
                    {
                        Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Tipo_Balance_ID + " = '" + Datos.P_Tipo_Balance_ID + "', ";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Tipo_Balance_ID + " = NULL, ";
                    }
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Resultado_ID))
                    {
                        Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Tipo_Resultado_ID + " = '" + Datos.P_Tipo_Resultado_ID + "', ";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Tipo_Resultado_ID + " = NULL, ";
                    }

                    //Verificar si hay una partida
                    if (String.IsNullOrEmpty(Datos.P_Partida_ID) == false)
                    {
                        Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Partida_ID + "='" + Datos.P_Partida_ID + "',";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Partida_ID + " = NULL,";
                    }
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Descripcion + " = '" + Datos.P_Descripcion + "', ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Cuenta + " = '" + Datos.P_Cuenta + "', ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Afectable + " = '" + Datos.P_Afectable + "', ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Comentarios + " = '" + Datos.P_Comentarios + "', ";
                    //Verificar si hay tipo de cuenta
                    if (String.IsNullOrEmpty(Datos.P_Tipo_Cuenta) == false)
                    {
                        Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta + " = '" + Datos.P_Tipo_Cuenta + "', ";
                    }
                    else
                    {
                        Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta + " = NULL, ";
                    }
                    Mi_SQL += Cat_Con_Cuentas_Contables.Campo_Tipo_Presupuestal + "= '" + Datos.P_Tipo_Presupuestal + "', ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Usuario_Modifico + " = '" + Datos.P_Nombre_Usuario + "', ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Fecha_Modifico + " = GETDATE(), ";
                    Mi_SQL += Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID_Padre + " = '" + Datos.P_Cuenta_Contable_ID_Padre + "' WHERE ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID + "'";
                    
                    //Ejecutar consulta
                    Comando_SQL.CommandText = Mi_SQL;
                    Comando_SQL.ExecuteNonQuery();

                    //Ejecutar transaccion
                    Transaccion_SQL.Commit();
                }
                catch (SqlException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    Conexion_Base.Close();
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Eliminar_Cuenta_Contable
            /// DESCRIPCION : Elimina la Cuenta Contable que fue seleccionada por el usuario de la BD
            /// PARAMETROS  : Datos: Obtiene que Cuenta Contable desea eliminar de la BD
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 10-Junio-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static void Eliminar_Cuenta_Contable(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                String Mi_SQL; //Variable de Consulta para la Eliminación de la Cuenta Contable
                try
                {
                    Mi_SQL = "DELETE FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID + "'";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }

                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
        #endregion

        #region (Métodos Consulta)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Datos_Cuentas_Contables
            /// DESCRIPCION : Consulta las Cuentas Contables que estan dadas de alta en la BD
            ///               con todos sus datos
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 10-Junio-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Datos_Cuentas_Contables(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                String Mi_SQL; //Variable para la consulta de las Cuentas Contables
                Boolean Concatenacion_WHERE = true;

                try
                {
                    Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + ".*, ";
                    Mi_SQL += Cat_Con_Niveles.Tabla_Cat_Con_Niveles + "." + Cat_Con_Niveles.Campo_Descripcion + " AS Nivel, ";
                    Mi_SQL += "Cat_Con_Cuentas_Contables_Padre.Descripcion AS Cuenta_Contable_Padre ";
                    Mi_SQL += " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " LEFT JOIN " + Cat_Con_Niveles.Tabla_Cat_Con_Niveles;
                    Mi_SQL += " ON " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Nivel_ID + " = " + Cat_Con_Niveles.Tabla_Cat_Con_Niveles + "." + Cat_Con_Niveles.Campo_Nivel_ID;
                    Mi_SQL += " LEFT JOIN Cat_Con_Cuentas_Contables Cat_Con_Cuentas_Contables_Padre ON Cat_Con_Cuentas_Contables.Cuenta_Contable_ID_Padre = Cat_Con_Cuentas_Contables_Padre.Cuenta_Contable_ID ";

                    if (!string.IsNullOrEmpty(Datos.P_Cuenta_Contable_ID))
                    {
                        Mi_SQL += " WHERE " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "='" + Datos.P_Cuenta_Contable_ID + "'";
                        Concatenacion_WHERE = false;
                    }

                    if (!string.IsNullOrEmpty(Datos.P_Descripcion))
                    {
                        if (Concatenacion_WHERE == true)
                        {
                            Mi_SQL += " WHERE (UPPER(" + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ") LIKE UPPER('%" + Datos.P_Descripcion + "%')";
                            Mi_SQL += " OR " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '%" + Datos.P_Descripcion + "%')";
                        }
                        else
                        {
                            Mi_SQL += " AND (UPPER(" + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ") LIKE UPPER('%" + Datos.P_Descripcion + "%')";
                            Mi_SQL += " OR " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '%" + Datos.P_Descripcion + "%')";
                        }
                    }
                    Mi_SQL += " ORDER BY " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " asc";
                    /////////////////////////////////////////////////////////
                    //Codigo comentado por Salvador Rea
                    /////////////////////////////////////////////////////////
                    //Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + ".*, ";
                    //Mi_SQL += Cat_Con_Niveles.Tabla_Cat_Con_Niveles + "." + Cat_Con_Niveles.Campo_Descripcion + " AS Nivel";
                    //Mi_SQL += " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + ", " + Cat_Con_Niveles.Tabla_Cat_Con_Niveles;
                    //Mi_SQL += " WHERE " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Nivel_ID + " = " + Cat_Con_Niveles.Tabla_Cat_Con_Niveles + "." + Cat_Con_Niveles.Campo_Nivel_ID;

                    //if (!string.IsNullOrEmpty(Datos.P_Cuenta_Contable_ID))
                    //{
                    //    Mi_SQL += " AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "='" + Datos.P_Cuenta_Contable_ID + "'";
                    //}

                    //if (!string.IsNullOrEmpty(Datos.P_Descripcion))
                    //{
                    //    Mi_SQL += " AND (UPPER(" + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ") LIKE UPPER('%" + Datos.P_Descripcion + "%')";
                    //    Mi_SQL += " OR " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '%" + Datos.P_Descripcion + "%')";
                    //}
                    //Mi_SQL += " ORDER BY " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta;

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
        
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Matriz
            /// DESCRIPCION : Consulta las cuentas en la matriz que estan dadas de alta en la BD
            ///               con todos sus datos
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 28-diciembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Matriz(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                try
                {
                    Mi_SQL.Append("SELECT (" + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_Cargo + "  +'-'+ " + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_Abono + "  +'-'+ " + Cat_Con_Matriz_De_Cuentas.Campo_Matriz_ID + ") as CUENTAS, (");
                    Mi_SQL.Append(Cat_Con_Matriz_De_Cuentas.Campo_No_Matriz + " + '-' +" + Cat_Con_Matriz_De_Cuentas.Campo_Nombre + ") as NOMBRE FROM " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas);
                    Mi_SQL.Append(" WHERE " + Cat_Con_Matriz_De_Cuentas.Campo_Tipo_Matriz + "='" + Datos.P_Tipo_Cuenta + "'");
                    Mi_SQL.Append(" AND (" + Cat_Con_Matriz_De_Cuentas.Campo_Nombre + " Like'%" + Datos.P_Descripcion + "%'");
                    Mi_SQL.Append(" OR " + Cat_Con_Matriz_De_Cuentas.Campo_No_Matriz + " Like'%" + Datos.P_Descripcion + "%')");
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Matriz))
                    {
                        Mi_SQL.Append(" AND " + Cat_Con_Matriz_De_Cuentas.Campo_Tipo_Gasto + " ='" + Datos.P_Tipo_Matriz + "'");
                    }
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Contables_Matriz
            /// DESCRIPCION : Consulta los Tipos de Poliza de las cuentas contables que estan 
            ///              dados de alta en la BD 
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 16-Enero-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Cuentas_Contables_Matriz(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                String Mi_SQL; //Variable para la consulta de las cuentas contables

                try
                {
                    //Consulta las Cuentas Contables que estan dados de alta en la base de datos y cumplen con el requisito de la matriz
                    Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ",(" + Cat_Con_Cuentas_Contables.Campo_Cuenta + "  +'-'+ " + Cat_Con_Cuentas_Contables.Campo_Descripcion + ") as Descripcion ";
                    Mi_SQL = Mi_SQL + ", " + Cat_Con_Cuentas_Contables.Campo_Cuenta;
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                    if (Datos.P_Cuenta != null)
                    {
                        Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta  + " like'" + Datos.P_Cuenta + "%'";
                    }
                    if (Datos.P_Afectable != null)
                    {
                        Mi_SQL = Mi_SQL + " AND " + Cat_Con_Cuentas_Contables.Campo_Afectable + " = '" + Datos.P_Afectable + "'";
                    }
                    Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }

                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Por_Descipcion
            /// DESCRIPCION : Consulta los Tipos de Poliza de las cuentas contables por descripcion que estan 
            ///              dados de alta en la BD 
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Hugo Enrique Ramírez Aguilera
            /// FECHA_CREO  : 08-Febrero-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Por_Descipcion(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                String Mi_SQL; //Variable para la consulta de las cuentas contables

                try
                {
                    Mi_SQL = "SELECT " + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_Abono + ", " + Cat_Con_Matriz_De_Cuentas.Campo_Cuenta_Cargo;
                    Mi_SQL = Mi_SQL + ", " + Cat_Con_Matriz_De_Cuentas.Campo_Matriz_ID;
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Matriz_De_Cuentas.Tabla_Cat_Con_Matriz_Cuentas;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Matriz_De_Cuentas.Campo_Nombre + " ='" + Datos.P_Descripcion + "'";
                    Mi_SQL = Mi_SQL + " AND " + Cat_Con_Matriz_De_Cuentas.Campo_Tipo_Matriz + " ='" + Datos.P_Tipo_Matriz + "'";

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }

                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Contables
            /// DESCRIPCION : Consulta los Tipos de Poliza de las cuentas contables que estan 
            ///              dados de alta en la BD 
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 21-Junio-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Cuentas_Contables(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                String Mi_SQL; //Variable para la consulta de las cuentas contables
                Boolean Where_Utilizado = false; //variable que indica si la clausula where ya ha sido utilizada
                
                try
                {
                    //Consulta las Cuentas Contables que estan dados de alta en la base de datos
                    Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ", " + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                    Mi_SQL = Mi_SQL  +", "+ Cat_Con_Cuentas_Contables.Campo_Cuenta + ", (LTRIM(RTRIM(Cuenta)) + ' ' + Descripcion) AS Cuenta_Descripcion ";
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                    if (Datos.P_Cuenta_Contable_ID != null)
                    {
                        Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID + "'";
                        Where_Utilizado = true;
                    }
                    else
                    {
                        if (Datos.P_Descripcion != null)
                        {
                            //Verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += " AND ";
                            }
                            else
                            {
                                Mi_SQL += " WHERE ";
                                Where_Utilizado = true;
                            }

                            //Resuto de la consulta
                            Mi_SQL += Cat_Con_Cuentas_Contables.Campo_Descripcion + " LIKE '%" + Datos.P_Descripcion + "%'";
                        }
                        if (Datos.P_Afectable != null)
                        {
                            if (Datos.P_Descripcion != null)
                            {
                                //Verificar si la clausula where ya ha sido utilizada
                                if (Where_Utilizado == true)
                                {
                                    Mi_SQL += " AND ";
                                }
                                else
                                {
                                    Mi_SQL += " WHERE ";
                                    Where_Utilizado = true;
                                }

                                //Resuto de la consulta

                                Mi_SQL += Cat_Con_Cuentas_Contables.Campo_Afectable + " = '" + Datos.P_Afectable + "'";
                            }
                            else
                            {
                                //Verificar si la clausula where ya ha sido utilizada
                                if (Where_Utilizado == true)
                                {
                                    Mi_SQL += " AND ";
                                }
                                else
                                {
                                    Mi_SQL += " WHERE ";
                                    Where_Utilizado = true;
                                }

                                //Resuto de la consulta
                                Mi_SQL += Cat_Con_Cuentas_Contables.Campo_Afectable + " = '" + Datos.P_Afectable + "'";
                            }
                        }

                        //Verificar si se tiene un numero de cuenta
                        if (String.IsNullOrEmpty(Datos.P_Cuenta) == false)
                        {
                            //Verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += " AND ";
                            }
                            else
                            {
                                Mi_SQL += " WHERE ";
                                Where_Utilizado = true;
                            }

                            //Resuto de la consulta
                            Mi_SQL += " CUENTA LIKE '" + Datos.P_Cuenta + "%' ";
                        }
                    }

                    Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Con_Cuentas_Contables.Campo_Cuenta;
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }

                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Existencia_Cuenta_Contable
            /// DESCRIPCION : Consulta 
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 23-Junio-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Existencia_Cuenta_Contable(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                String Mi_SQL; //Variable para la consulta de las cuentas contables
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmmd = new SqlCommand();
                SqlTransaction Trans = null;
                SqlDataAdapter Da_SQL = new SqlDataAdapter();
                DataSet Ds_SQL = new DataSet();
                SqlDataAdapter Dt_SQL_Consulta = new SqlDataAdapter();
                DataSet Ds_SQL_Consulta = new DataSet();
                if (Datos.P_Cmmd != null)
                {
                    Cmmd = Datos.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmmd.Connection = Trans.Connection;
                    Cmmd.Transaction = Trans;
                }
                try
                {
                    //Consulta si existe la cuenta contable que se desea dar de alta
                    Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ", " + Cat_Con_Cuentas_Contables.Campo_Cuenta + ", ";
                    Mi_SQL = Mi_SQL + Cat_Con_Cuentas_Contables.Campo_Descripcion + " ";
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                    if (!String.IsNullOrEmpty(Datos.P_Cuenta))
                    {
                        Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " = '" + Datos.P_Cuenta + "'";
                    }
                    if (!String.IsNullOrEmpty(Datos.P_Cuenta_Contable_ID))
                    {
                        if (Mi_SQL.Contains("WHERE"))
                        {
                            Mi_SQL = Mi_SQL + " AND " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID + "'";
                        }
                        else
                        {
                            Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID + "'";
                        }
                    }
                    Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                    Da_SQL.SelectCommand = Cmmd;
                    Da_SQL.Fill(Ds_SQL);
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                    return Ds_SQL.Tables[0];
                    // return OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Trans != null)
                    {
                        Trans.Rollback();
                    }
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                    if (Datos.P_Cmmd == null)
                    {
                        Cn.Close();
                    }
                }
            }

            ///*******************************************************************************************************
            /// 	NOMBRE_FUNCIÓN: Consulta_Nombre_Cuenta_Partidas
            /// 	DESCRIPCIÓN:    Consulta el nombre o el numero de egreso o ingreso.
            /// 	PARÁMETROS:     Datos: Instancia de la clase de negocio que indica el registro a consultar en la base de datos
            /// 	CREO:           Hugo Enrique Ramírez Aguilera
            /// 	FECHA_CREO:     20/Abril/2012
            /// 	MODIFICÓ: 
            /// 	FECHA_MODIFICÓ: 
            /// 	CAUSA_MODIFICACIÓN: 
            ///*******************************************************************************************************
            public static DataTable Consulta_Nombre_Cuenta_Egresos_Ingresos(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                String Mi_SQL = ""; //Variable para la consulta de las Partidas Genericas

                try
                {
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Presupuestal))
                    {
                        if (Datos.P_Tipo_Presupuestal == "EGRESOS")
                        {
                            Mi_SQL = "SELECT " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas + ".* ";
                            Mi_SQL += " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas;

                            //Verificar si hay un tipo de busqueda
                            if (String.IsNullOrEmpty(Datos.P_Tipo_Busqueda) == true)
                            {
                                if (!String.IsNullOrEmpty(Datos.P_Cuenta))
                                    Mi_SQL += " WHERE " + Cat_Com_Partidas.Campo_Cuenta_SAP + " = '" + Datos.P_Cuenta + "'";

                                else if (!String.IsNullOrEmpty(Datos.P_Nombre_Partida_Egresos_Ingresos))
                                    Mi_SQL += " WHERE " + Cat_Com_Partidas.Campo_Nombre + " = '" + Datos.P_Nombre_Partida_Egresos_Ingresos + "'";

                            }
                            else
                            {
                                Mi_SQL += " WHERE " + Cat_Com_Partidas.Campo_Clave + " = '" + Datos.P_Cuenta + "'";
                            }

                            Mi_SQL += " And " + Cat_Com_Partidas.Campo_Estatus + "='ACTIVO'";
                        }

                        else if (Datos.P_Tipo_Presupuestal == "INGRESOS")
                        {
                            Mi_SQL = "SELECT " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + ".* ";
                            Mi_SQL += " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing;

                            //Verificar si hay un tipo de busqueda
                            if (String.IsNullOrEmpty(Datos.P_Tipo_Busqueda) == true)
                            {
                                if (!String.IsNullOrEmpty(Datos.P_Cuenta))
                                    Mi_SQL += " WHERE " + Cat_Psp_Concepto_Ing.Campo_Clave + "= '" + Datos.P_Cuenta + "'";

                                if (!String.IsNullOrEmpty(Datos.P_Nombre_Partida_Egresos_Ingresos))
                                    Mi_SQL += " WHERE " + Cat_Psp_Concepto_Ing.Campo_Descripcion + "= '" + Datos.P_Nombre_Partida_Egresos_Ingresos + "'";
                            }
                            else
                            {
                                Mi_SQL += " WHERE " + Cat_Psp_Concepto_Ing.Campo_Clave + " LIKE '" + Datos.P_Cuenta + "%' ";
                            }

                            Mi_SQL += " And " + Cat_Psp_Concepto_Ing.Campo_Estatus + "='ACTIVO'";
                        }
                    }

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }

                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }

            ///*******************************************************************************************************
            /// 	NOMBRE_FUNCIÓN: Consulta_Nombre_Partidas_Egreso_Ingresos
            /// 	DESCRIPCIÓN:    Consulta el nombre de todas las partidas.
            /// 	PARÁMETROS:     Datos: Instancia de la clase de negocio que indica el registro a consultar en la base de datos
            /// 	CREO:           Hugo Enrique Ramírez Aguilera
            /// 	FECHA_CREO:     20/Abril/2012
            /// 	MODIFICÓ: 
            /// 	FECHA_MODIFICÓ: 
            /// 	CAUSA_MODIFICACIÓN: 
            ///*******************************************************************************************************
            public static DataTable Consulta_Nombre_Partidas_Egreso_Ingresos(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                String Mi_SQL = ""; //Variable para la consulta de las Partidas Genericas

                try
                {
                    if (!String.IsNullOrEmpty(Datos.P_Tipo_Presupuestal))
                    {
                        if (Datos.P_Tipo_Presupuestal == "EGRESOS")
                        {
                            Mi_SQL = "SELECT " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas + ".* ";
                            Mi_SQL += " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas;
                            Mi_SQL += " Where " + Cat_Com_Partidas.Campo_Estatus + "='ACTIVO'";
                        }

                        else if (Datos.P_Tipo_Presupuestal == "INGRESOS")
                        {
                            Mi_SQL = "Select " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + ".* ";
                            Mi_SQL += " from " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing;
                            Mi_SQL += " where " + Cat_Psp_Concepto_Ing.Campo_Estatus + "='ACTIVO'";
                        }
                    }
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }

                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Contables_izquierda
            /// DESCRIPCION : Consulta las cuentas contables por numero de cuenta por la izquierda
            /// PARAMETROS  : Datos: Variable de la capa de negocios
            /// CREO        : Noe Mosqueda Valadez
            /// FECHA_CREO  : 22-Mayo-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Cuentas_Contables_Cuenta_Izquierda(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                String Mi_SQL; //Variable para la consulta de las Cuentas Contables
                DataTable Dt_Resultado = new DataTable(); //Tabla para el resultado de la consulta

                try
                {
                    Mi_SQL = "SELECT Cuenta_Contable_ID, Cuenta, Descripcion, (LTRIM(RTRIM(Cuenta)) + ' ' + Descripcion) AS Cuenta_Descripcion FROM Cat_Con_Cuentas_Contables "
                        + "WHERE Afectable = 'SI' ";

                    //Verificar el tipo de busqueda
                    switch (Datos.P_Tipo_Busqueda)
                    {
                        case "Descripcion":
                            Mi_SQL += "AND Descripcion LIKE '%" + Datos.P_Descripcion + "%' ";
                            break;

                        case "Cuenta":
                            Mi_SQL += "AND Cuenta LIKE '" + Datos.P_Descripcion + "%' ";
                            break;

                        default:
                            break;
                    }

                    Mi_SQL += "ORDER BY Cuenta ASC";
                    //Mi_SQL += "ORDER BY Descripcion ASC";

                    //Ejecutar consulta
                    Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                    //Entregar resultado
                    return Dt_Resultado;
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Contables_Busqueda
            /// DESCRIPCION : Consulta las Cuentas Contables que estan dados de alta en la base de datos
            ///               usando  o no filtros
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 21-Junio-2011
            /// MODIFICO          : Jennyfer Ivonne Ceja Lemus
            /// FECHA_MODIFICO    : 22-Noviembre-2012
            /// CAUSA_MODIFICACION: Adaptacion para que las cuentas sea  de tipo numerico
            ///*******************************************************************************
            public static DataTable Consulta_Cuentas_Contables_Busqueda(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                String Mi_SQL; //Variable para la consulta de las cuentas contables
                Boolean Where_Utilizado = false; //variable que indica si la clausula where ya ha sido utilizada

                try
                {
                    //Consulta las Cuentas Contables que estan dados de alta en la base de datos
                    Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ", " + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                    Mi_SQL = Mi_SQL + ", CAST(" + Cat_Con_Cuentas_Contables.Campo_Cuenta + " AS NUMERIC) AS " + Cat_Con_Cuentas_Contables.Campo_Cuenta + ", (LTRIM(RTRIM(Cuenta)) + ' ' + Descripcion) AS Cuenta_Descripcion ";
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                    if (Datos.P_Cuenta_Contable_ID != null)
                    {
                        Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID + "'";
                        Where_Utilizado = true;
                    }
                    else
                    {
                        if (Datos.P_Descripcion != null)
                        {
                            //Verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += " AND ";
                            }
                            else
                            {
                                Mi_SQL += " WHERE ";
                                Where_Utilizado = true;
                            }

                            //Resuto de la consulta
                            Mi_SQL += Cat_Con_Cuentas_Contables.Campo_Descripcion + " LIKE '%" + Datos.P_Descripcion + "%'";
                        }
                        if (Datos.P_Afectable != null)
                        {
                            if (Datos.P_Descripcion != null)
                            {
                                //Verificar si la clausula where ya ha sido utilizada
                                if (Where_Utilizado == true)
                                {
                                    Mi_SQL += " AND ";
                                }
                                else
                                {
                                    Mi_SQL += " WHERE ";
                                    Where_Utilizado = true;
                                }

                                //Resuto de la consulta

                                Mi_SQL += Cat_Con_Cuentas_Contables.Campo_Afectable + " = '" + Datos.P_Afectable + "'";
                            }
                            else
                            {
                                //Verificar si la clausula where ya ha sido utilizada
                                if (Where_Utilizado == true)
                                {
                                    Mi_SQL += " AND ";
                                }
                                else
                                {
                                    Mi_SQL += " WHERE ";
                                    Where_Utilizado = true;
                                }

                                //Resuto de la consulta
                                Mi_SQL += Cat_Con_Cuentas_Contables.Campo_Afectable + " = '" + Datos.P_Afectable + "'";
                            }
                        }

                        //Verificar si se tiene un numero de cuenta
                        if (String.IsNullOrEmpty(Datos.P_Cuenta) == false)
                        {
                            //Verificar si la clausula where ya ha sido utilizada
                            if (Where_Utilizado == true)
                            {
                                Mi_SQL += " AND ";
                            }
                            else
                            {
                                Mi_SQL += " WHERE ";
                                Where_Utilizado = true;
                            }

                            //Resuto de la consulta
                            Mi_SQL += " CUENTA LIKE '" + Datos.P_Cuenta + "%' ";
                        }
                    }

                    Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }

                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Contables_Concatena
            /// DESCRIPCION : Consulta las cuentas contables
            /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
            /// CREO        : Armando Zavala Moreno
            /// FECHA_CREO  : 28-Agosto-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Cuentas_Contables_Concatena(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                String Mi_SQL; //Variable para la consulta de las cuentas contables

                try
                {
                    //Consulta las Cuentas Contables que estan dados de alta en la base de datos
                    Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ", ('['+" + Cat_Con_Cuentas_Contables.Campo_Cuenta;
                    Mi_SQL += "+'] -- '+" + Cat_Con_Cuentas_Contables.Campo_Descripcion + ")DESCRIPCION";
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                    if (Datos.P_Cuenta_Contable_ID != null)
                    {
                        Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID + "'";
                    }
                    if (Datos.P_Descripcion != null)
                    {
                        Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Descripcion + " LIKE '%" + Datos.P_Descripcion + "%'";
                    }
                    if (Datos.P_Afectable != null)
                    {
                        if (Datos.P_Descripcion != null)
                        {
                            Mi_SQL = Mi_SQL + " AND " + Cat_Con_Cuentas_Contables.Campo_Afectable + " = '" + Datos.P_Afectable + "'";
                        }
                        else
                        {
                            Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Afectable + " = '" + Datos.P_Afectable + "'";
                        }
                    }
                    Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }

                catch (DBConcurrencyException Ex)
                {
                    throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
                finally
                {
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Datos_Parametros
            /// DESCRIPCION : Consulta EL ID DE LA CUENTA PARA LA BUSQUEDA
            /// PARAMETROS  : 
            /// CREO        : Jorge L. Gonzalez Reyes
            /// FECHA_CREO  : 15/Agosto/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Busqueda_Cuenta(Cls_Cat_Con_Cuentas_Contables_Negocio Datos)
            {
                String Mi_SQL; //Variable para almacenar la instruccion de consulta a la BD.
                try
                {
                    Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Afectable + " = 'SI'";
                    if (!String.IsNullOrEmpty(Datos.P_Descripcion))
                    {
                        Mi_SQL = Mi_SQL + " AND " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '%" + Datos.P_Descripcion + "%'";
                        Mi_SQL = Mi_SQL + " OR " + Cat_Con_Cuentas_Contables.Campo_Descripcion + " LIKE '%" + Datos.P_Descripcion + "%'";
                    }
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException Ex)
                {
                    throw new Exception("SQL Error: " + Ex.Message);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error: " + Ex.Message);
                }
            }

        #endregion
    }
}