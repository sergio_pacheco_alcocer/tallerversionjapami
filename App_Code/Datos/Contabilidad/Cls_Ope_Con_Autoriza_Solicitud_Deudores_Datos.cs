﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Sessiones;
using JAPAMI.Autoriza_Solicitud_Deudores.Negocio;
using JAPAMI.Polizas.Datos;

/// <summary>
/// Summary description for Cls_Ope_Con_Autoriza_Solicitud_Pago_Datos
/// </summary>

namespace JAPAMI.Autoriza_Solicitud_Deudores.Datos
{
    public class Cls_Ope_Con_Autoriza_Solicitud_Deudores_Datos
    {
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Solicitudes_SinAutotizar
        /// DESCRIPCION : Consulta las solicitudes de pago que estan pendientes de autorizar o rechazar 
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Agosto/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Solicitudes_SinAutotizar(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            String Bandera = "1";
            try
            {
                Mi_SQL ="SELECT DEUDORES.* , CASE WHEN (DEUDORES." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "= 'EMPLEADO') THEN(";
                Mi_SQL +=" SELECT " + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + " FROM ";
                Mi_SQL +=Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "= DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + ") ELSE ";
                Mi_SQL +=" PROVEEDOR." + Cat_Com_Proveedores.Campo_Compañia + " END AS NOMBRE FROM " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " DEUDORES";
                Mi_SQL +=" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDOR ON DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + "=PROVEEDOR." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                if (!String.IsNullOrEmpty(Datos.P_Dependencia_ID))
                {
                    Bandera = "2";
                    Mi_SQL += " WHERE DEUDORES." + OPE_CON_DEUDORES.Campo_Dependencia_ID + "='" + Datos.P_Dependencia_ID + "'";
                }
                if (!String.IsNullOrEmpty(Datos.P_No_Deuda))
                {
                    if (Bandera == "2")
                    {
                        Mi_SQL += " AND DEUDORES." + OPE_CON_DEUDORES.Campo_No_Deuda + " = '" + Datos.P_No_Deuda + "'";
                    }
                    else
                    {
                        Bandera = "2";
                        Mi_SQL += " WHERE DEUDORES." + OPE_CON_DEUDORES.Campo_No_Deuda + " = '" + Datos.P_No_Deuda + "'";
                    }
                }
                else
                {
                    if (Bandera == "2")
                    {
                        Mi_SQL += " AND DEUDORES." + OPE_CON_DEUDORES.Campo_Estatus +
                            " = 'PENDIENTE'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE DEUDORES." + OPE_CON_DEUDORES.Campo_Estatus +
                            " = 'PENDIENTE'";
                    }
                }
                Mi_SQL += " ORDER BY " + OPE_CON_DEUDORES.Campo_No_Deuda + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cambiar_Estatus_Solicitud
        /// DESCRIPCION : Autoriza o rechaza la solicitud de pago 
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Cambiar_Estatus_Solicitud(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            String Mi_SQL;
            try
            {
                //Modifica el estatus de la solicitud de pago
                Mi_SQL = "UPDATE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores  + " SET ";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Estatus + "='" + Datos.P_Estatus + "',";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Autorizo + "',";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Fecha_Modifico + "= GETDATE(), ";
                if (!String.IsNullOrEmpty(Datos.P_Usuario_Autorizo))
                {
                    Mi_SQL += OPE_CON_DEUDORES.Campo_Usuario_Autorizo_Area + "='" + Datos.P_Usuario_Autorizo + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Comentario))
                {
                    Mi_SQL += OPE_CON_DEUDORES.Campo_Comentario_Area + "='" + Datos.P_Comentario + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Autorizo_Rechazo_Finanzas))
                {
                    Mi_SQL += OPE_CON_DEUDORES.Campo_Fecha_Autoriza_Rechaza_Finanzas + "= GETDATE(),";
                    Mi_SQL += OPE_CON_DEUDORES.Campo_Comentario_Finanzas + "='" + Datos.P_Comentario_Finanza + "' WHERE ";
                }
                else
                {

                    Mi_SQL += OPE_CON_DEUDORES.Campo_Fecha_Autorizo_Rechazo + "= GETDATE() " + " WHERE ";
                }
                Mi_SQL += OPE_CON_DEUDORES.Campo_No_Deuda + " ='" + Datos.P_No_Deuda + "'";
                SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Solicitudes_Autorizadas
        /// DESCRIPCION : Consulta las solicitudes de pago que estan pendientes de autorizar o rechazar 
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Agosto/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Solicitudes_Autorizadas(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            String Bandera = "1";
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                Mi_SQL = "SELECT DEUDORES.* , CASE WHEN (DEUDORES." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "= 'EMPLEADO') THEN(";
                Mi_SQL += " SELECT " + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + " FROM ";
                Mi_SQL += Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "= DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + ") ELSE ";
                Mi_SQL += " PROVEEDOR." + Cat_Com_Proveedores.Campo_Compañia + " END AS NOMBRE FROM " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " DEUDORES";
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDOR ON DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + "=PROVEEDOR." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                if (!String.IsNullOrEmpty(Datos.P_No_Deuda))
                {
                        Bandera = "2";
                        Mi_SQL += " WHERE DEUDORES." + OPE_CON_DEUDORES.Campo_No_Deuda + " = '" + Datos.P_No_Deuda + "'";

                    if(!String.IsNullOrEmpty(Datos.P_Estatus)){
                        Mi_SQL += " AND DEUDORES." + OPE_CON_DEUDORES.Campo_Estatus +" = '" + Datos.P_Estatus + "'";
                    }
                }
                else
                {
                    if (Bandera=="2")
                    {
                        Mi_SQL += " AND DEUDORES." + OPE_CON_DEUDORES.Campo_Estatus +
                            " = 'AUTORIZADO'";
                    }
                    else
                    {
                        Mi_SQL += " WHERE DEUDORES." + OPE_CON_DEUDORES.Campo_Estatus +
                            " = 'AUTORIZADO'";
                    }
                }
                Mi_SQL += " ORDER BY " + OPE_CON_DEUDORES.Campo_No_Deuda + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Solicitudes_PorPagar
        /// DESCRIPCION : Consulta las solicitudes de pago que estan pendientes de autorizar o rechazar 
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Agosto/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Solicitudes_PorPagar(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                Mi_SQL = "SELECT DEUDORES.* , CASE WHEN (DEUDORES." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "= 'EMPLEADO') THEN(";
                Mi_SQL += " SELECT " + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + " FROM ";
                Mi_SQL += Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "= DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + ") ELSE ";
                Mi_SQL += " PROVEEDOR." + Cat_Com_Proveedores.Campo_Compañia + " END AS NOMBRE, BANCO."+Cat_Nom_Bancos.Campo_Nombre +" as BANCO FROM " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " DEUDORES";
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDOR ON DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + "=PROVEEDOR." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO ON DEUDORES." + OPE_CON_DEUDORES.Campo_Cuenta_Banco_Pago + "=BANCO." + Cat_Nom_Bancos.Campo_Banco_ID;
                Mi_SQL += " WHERE DEUDORES." +OPE_CON_DEUDORES.Campo_Transferencia + " IS NULL ";
                if (!String.IsNullOrEmpty(Datos.P_No_Deuda))
                {
                    Mi_SQL += " AND DEUDORES." + OPE_CON_DEUDORES.Campo_No_Deuda + " = '" + Datos.P_No_Deuda + "'";

                    if (!String.IsNullOrEmpty(Datos.P_Estatus))
                    {
                        Mi_SQL += " AND DEUDORES." + OPE_CON_DEUDORES.Campo_Estatus + " = '" + Datos.P_Estatus + "'";
                    }
                }
                else
                {
                        Mi_SQL += " AND DEUDORES." + OPE_CON_DEUDORES.Campo_Estatus +
                            " = 'PORPAGAR'";
                }
                Mi_SQL += " ORDER BY " + OPE_CON_DEUDORES.Campo_No_Deuda + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Solicitudes
        /// DESCRIPCION : Consulta las solicitudes de pago que estan pendientes de autorizar o rechazar 
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Agosto/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Solicitudes(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                Mi_SQL = "SELECT DEUDORES.* , CASE WHEN (DEUDORES." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "= 'EMPLEADO') THEN(";
                Mi_SQL += " SELECT " + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + " FROM ";
                Mi_SQL += Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "= DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + ") ELSE ";
                Mi_SQL += " PROVEEDOR." + Cat_Com_Proveedores.Campo_Compañia + " END AS NOMBRE, BANCO." + Cat_Nom_Bancos.Campo_Nombre + " as BANCO FROM " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " DEUDORES";
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDOR ON DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + "=PROVEEDOR." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO ON DEUDORES." + OPE_CON_DEUDORES.Campo_Cuenta_Banco_Pago + "=BANCO." + Cat_Nom_Bancos.Campo_Banco_ID;
                Mi_SQL += " WHERE DEUDORES." + OPE_CON_DEUDORES.Campo_Transferencia + " IS NULL ";
                if (!String.IsNullOrEmpty(Datos.P_No_Deuda))
                {
                    Mi_SQL += " AND DEUDORES." + OPE_CON_DEUDORES.Campo_No_Deuda + " = '" + Datos.P_No_Deuda + "'";

                    if (!String.IsNullOrEmpty(Datos.P_Estatus))
                    {
                        Mi_SQL += " AND DEUDORES." + OPE_CON_DEUDORES.Campo_Estatus + " = '" + Datos.P_Estatus + "'";
                    }
                }
                else
                {
                    Mi_SQL += " AND DEUDORES." + OPE_CON_DEUDORES.Campo_Estatus +
                        " = '" + Datos.P_Estatus + "'";
                }
                Mi_SQL += " ORDER BY " + OPE_CON_DEUDORES.Campo_No_Deuda + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Autorizacion_de_Solicitud
        /// DESCRIPCION : Autoriza o rechaza la solicitud de pago 
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Autorizacion_de_Solicitud(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            String Mi_SQL;
            try
            {
                //Modifica el estatus de la solicitud de pago
                Mi_SQL = "UPDATE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores  + " SET ";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Estatus + "='" + Datos.P_Estatus + "',";
                if (!String.IsNullOrEmpty(Datos.P_Forma_Pago) && !String.IsNullOrEmpty(Datos.P_Cuenta_Banco_Pago))
                {
                    Mi_SQL += OPE_CON_DEUDORES.Campo_Forma_Pago + "='" + Datos.P_Forma_Pago + "',";
                    Mi_SQL += OPE_CON_DEUDORES.Campo_Cuenta_Banco_Pago + "='" + Datos.P_Cuenta_Banco_Pago + "',";
                }
                Mi_SQL += OPE_CON_DEUDORES.Campo_Usuario_Asigna_Cuenta + "='" + Datos.P_Usuario_Asigno_Banco + "',";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Comentario_Finanzas  + "='" + Datos.P_Comentario_Finanza + "',";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Fecha_Autoriza_Rechaza_Finanzas + "=GETDATE(),";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Asigno_Banco + "',";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Fecha_Modifico + "= GETDATE()  WHERE ";
                Mi_SQL += OPE_CON_DEUDORES.Campo_No_Deuda + " ='" + Datos.P_No_Deuda + "'";
                SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_Solicitud_Pago
        /// DESCRIPCION : Consulta todos los datos de la solicitud de pago que selecciono
        ///               el usuario
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio manuel Gallardo
        /// FECHA_CREO  : 07-Agosto-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Datos_Solicitud_Pago(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
            DataTable Dt_Resultado = new DataTable(); //Tabla para el resultado
            DataRow Renglon; //Renglon para la modificacion de la tabla
            int Cont_Elementos = 0; //variable para el contador
            object aux; //variable auxiliar para las consultas
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Da_SQL = new SqlDataAdapter();

            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            try
            {
                Mi_SQL.Append("SELECT " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_No_Deuda  + ", ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Estatus + ", ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Deudor_ID  + ", ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + ", ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Fecha_Creo + ", ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Tipo_Solicitud_ID + ", ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Usuario_Creo + ", ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Concepto + ", ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Tipo_Movimiento  + ", ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Importe + ", CASE WHEN (" + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "= 'EMPLEADO') THEN(");
                Mi_SQL.Append(" SELECT " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno);
                Mi_SQL.Append(" FROM "+Cat_Empleados.Tabla_Cat_Empleados + " WHERE "+Cat_Empleados.Tabla_Cat_Empleados+"." + Cat_Empleados.Campo_Empleado_ID + "= "+OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores+"." + OPE_CON_DEUDORES.Campo_Deudor_ID + ") ELSE ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores+"." + Cat_Com_Proveedores.Campo_Compañia + " END AS NOMBRE , CASE WHEN (" + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "= 'EMPLEADO') THEN(");
                Mi_SQL.Append(" SELECT " + Cat_Empleados.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append(" FROM " + Cat_Empleados.Tabla_Cat_Empleados + "  WHERE " + Cat_Empleados.Campo_Empleado_ID + "='" + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Deudor_ID + "') ELSE ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores+"." + Cat_Com_Proveedores.Campo_Cuenta_Deudor_ID  + " END AS CUENTA_DEUDOR, CASE WHEN (" + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "= 'EMPLEADO') THEN(");
                Mi_SQL.Append(" SELECT BANCO." + Cat_Nom_Bancos.Campo_Nombre );
                Mi_SQL.Append(" FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO ON EMPLEADO." + Cat_Empleados.Campo_Banco_ID + "=BANCO." + Cat_Nom_Bancos.Campo_Banco_ID + "  WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "= " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Deudor_ID + ") ELSE ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores+"." + Cat_Com_Proveedores.Campo_Banco_Proveedor   + " END AS BANCO_DEUDOR");
                Mi_SQL.Append(" FROM " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores );
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ON " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + "=" + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Deudor_ID);
                if (!String.IsNullOrEmpty(Datos.P_No_Deuda))
                {
                    Mi_SQL.Append(" WHERE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_No_Deuda  + "='" + Datos.P_No_Deuda + "'");
                }
                if (!String.IsNullOrEmpty(Datos.P_Deudor_ID) && !String.IsNullOrEmpty(Datos.P_Tipo_Deudor))
                {
                    Mi_SQL.Append(" WHERE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Deudor_ID + "='" + Datos.P_Deudor_ID + "'");
                    Mi_SQL.Append(" AND " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Tipo_Deudor  + "='" + Datos.P_Tipo_Deudor  + "'");
                }
                
                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL.ToString();
                Da_SQL.SelectCommand = Cmmd;
                Da_SQL.Fill(Dt_Resultado);

                //Verificar si la consulta arrojo resultados
                if (Dt_Resultado.Rows.Count > 0)
                {
                    //Ciclo para el barrido de la tabla
                    for (Cont_Elementos = 0; Cont_Elementos < Dt_Resultado.Rows.Count; Cont_Elementos++)
                    {
                        //limpiar la variable auxiliar
                        aux = null;

                        //Verificar si se encontro una cuenta contable para el deudor
                        if (Dt_Resultado.Rows[Cont_Elementos]["CUENTA_DEUDOR"] == null || Dt_Resultado.Rows[Cont_Elementos]["CUENTA_DEUDOR"] == DBNull.Value)
                        {
                            //Consulta para la cuenta contable 
                            Mi_SQL.Length = 0;
                            Mi_SQL.Append("SELECT " + Cat_Empleados.Campo_Cuenta_Contable_ID + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " ");
                            Mi_SQL.Append("WHERE " + Cat_Empleados.Campo_Empleado_ID + " = '" + Dt_Resultado.Rows[Cont_Elementos][OPE_CON_DEUDORES.Campo_Deudor_ID].ToString().Trim() + "' ");

                            //Ejecutar consulta
                            Cmmd.CommandText = Mi_SQL.ToString();
                            aux = Cmmd.ExecuteScalar();

                            //verificar si es nulo
                            if (aux != null)
                            {
                                if (aux != DBNull.Value)
                                {
                                    //Instanciar el renglon
                                    Renglon = Dt_Resultado.Rows[Cont_Elementos];

                                    //modificar y guardar el renglon
                                    Renglon.BeginEdit();
                                    Renglon["CUENTA_DEUDOR"] = aux.ToString().Trim();
                                    Renglon.EndEdit();
                                    Dt_Resultado.AcceptChanges();
                                }
                            }
                        }
                    }
                }

                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Cheque
        /// DESCRIPCION : Inserta en nuevo Cheque en la BD
        /// PARAMETROS  : Datos: Contiene los datos de los filtros
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 08-Agosto-2012
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static String Alta_Cheque(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            SqlDataAdapter da = new SqlDataAdapter(Cmmd);
            Object aux = new Object(); //Variable auxiliar 
            String Mi_SQL;
            string No_Poliza = string.Empty;                         //Obtiene el No con la cual se guardo los datos en la base de datos
            Int64 Consecutivo = 0;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
            double Saldo = 0;                                    //Obtiene el saldo de la cuenta contable                
            String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            DataTable Dt_Partidas = new DataTable();
            string Mensaje = string.Empty; //variable para el mensaje personalizado de error

            try
            {
                //Consulta para el maximo del numero de pago
                Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Con_Pagos.Campo_No_Pago + "),'00000') ";
                Mi_SQL = Mi_SQL + "FROM " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos;

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                aux = Cmmd.ExecuteScalar();

                //verificar si es nulo
                if (aux != null)
                {
                    if (aux != DBNull.Value)
                    {
                        Datos.P_No_Pago = String.Format("{0:00000}", Convert.ToInt32(aux) + 1);
                    }
                    else
                    {
                        Datos.P_No_Pago = "00001";
                    }
                }
                else
                {
                    Datos.P_No_Pago = "00001";
                }

                //Consulta para los pagos
                Mi_SQL = "INSERT INTO " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "(";
                Mi_SQL += Ope_Con_Pagos.Campo_No_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Banco_ID + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Fecha_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Beneficiario_Pago + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Forma_Pago + ", ";
                if (Datos.P_Forma_Pago == "CHEQUE")
                {
                    Mi_SQL += Ope_Con_Pagos.Campo_No_Cheque + ", ";
                }
                else
                {
                    Mi_SQL += Ope_Con_Pagos.Campo_Referencia_Transferencia_Banca + ", ";
                }
                Mi_SQL += Ope_Con_Pagos.Campo_Estatus + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Comentarios + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Usuario_Creo + ", ";
                Mi_SQL += Ope_Con_Pagos.Campo_Fecha_Creo + ") VALUES('";
                Mi_SQL += Datos.P_No_Pago + "', '";
                Mi_SQL += Datos.P_Cuenta_Banco_Pago + "','";
                Mi_SQL += Datos.P_Fecha_Pago + "','";
                Mi_SQL += Datos.P_Beneficiario_Pago + "', '";
                Mi_SQL += Datos.P_Forma_Pago + "', '";
                if (Datos.P_Forma_Pago == "CHEQUE")
                {
                    Mi_SQL += Datos.P_No_Cheque + "', '";
                }
                else
                {
                    Mi_SQL += Datos.P_Referencia + "', '";
                }
                Mi_SQL += Datos.P_Estatus + "', '";
                Mi_SQL += Datos.P_Comentario + "', '";
                Mi_SQL += Datos.P_Usuario_Autorizo  + "',  GETDATE())";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();

                //Consulta para actualizar la tabla de los deudores
                Mi_SQL = "UPDATE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores  + " SET ";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Estatus + "='PAGADO',";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Autorizo  + "',";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Fecha_Modifico + "= GETDATE() " + " WHERE ";
                Mi_SQL += OPE_CON_DEUDORES.Campo_No_Deuda + " ='" + Datos.P_No_Deuda  + "'";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery(); 

                //Consulta para la obtención del último ID dado de alta 
                Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')" +
                " FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                " WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'" +
                " AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00002'";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                aux = Cmmd.ExecuteScalar();

                //Verificar si no es nulo
                if (aux != null)
                {
                    if (aux != DBNull.Value)
                    {
                        No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(aux) + 1);
                    }
                    else
                    {
                        No_Poliza = "0000000001";
                    }
                }
                else
                {
                    No_Poliza = "0000000001";
                }

                //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                Mi_SQL = "INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                " (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", " +
                Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", " +
                Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", " +
                Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", " +
                Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", " +
                Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + "," + Ope_Con_Polizas.Campo_Referencia + ")" +
                " VALUES ('" + No_Poliza + "', '00002', '" + Mes_Anio + "'," +
                " '" + String.Format("{0:dd/MM/yyyy}", DateTime.Today) + "'," +
                " '" + "CHEQUE " + Datos.P_No_Pago + "', " + Datos.P_Importe + ", " + Datos.P_Importe + "," + Datos.P_No_Partidas + ", " +
                "'" + Datos.P_Usuario_Autorizo  + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', " +
                "'" + Cls_Sessiones.Empleado_ID + "','" + Datos.P_Referencia + "')";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();

                //Da de alta los detalles de la póliza
                foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                {
                    //consulta el saldo de la cuenta contable
                    Mi_SQL = "SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - " +
                    " ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo" +
                    " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                    " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'";

                    //Ejecutar consulta
                    Cmmd.CommandText = Mi_SQL;
                    aux = Cmmd.ExecuteScalar();

                    //verificar si no es nulo
                    if (aux != null)
                    {
                        if (aux != DBNull.Value)
                        {
                            Saldo = Convert.ToDouble(aux);
                        }
                        else
                        {
                            Saldo = 0;
                        }
                    }
                    else
                    {
                        Saldo = 0;
                    }

                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                    {
                        Saldo = Saldo + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                    {
                        Saldo =  Saldo - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                    }

                    //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')" +
                    " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;

                    //Ejecutar consulta
                    Cmmd.CommandText = Mi_SQL;
                    aux = Cmmd.ExecuteScalar();

                    //Verificar si nio es nulo
                    if (aux != null)
                    {
                        if (aux != DBNull.Value)
                        {
                            Consecutivo = Convert.ToInt64(aux) + 1;
                        }
                        else
                        {
                            Consecutivo = 1;
                        }
                    }
                    else
                    {
                        Consecutivo = 1;
                    }
                    
                    //Inserta el registro del detalle de la póliza en la base de datos
                    Mi_SQL = "INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                    "(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Consecutivo + "," + Ope_Con_Polizas_Detalles.Campo_Referencia + ")" +
                    " VALUES('" + No_Poliza + "', '00002', '" + Mes_Anio + "', " +
                    Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + "," +
                    " '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'," +
                    " '" + "CHEQUE " + Datos.P_No_Pago + "', " +
                    Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", " +
                    Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", '" +
                    Renglon[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID].ToString() + "','" + Renglon[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario].ToString() + "', " +
                    Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ",'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Referencia].ToString() + "')";

                    //Ejecutar consulta
                    Cmmd.CommandText = Mi_SQL;
                    Cmmd.ExecuteNonQuery();
                }

                // se inserta el numero de poliza, el tipo de poliza y el mes_anio del pago
                Mi_SQL = "UPDATE " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " SET ";
                Mi_SQL += Ope_Con_Pagos.Campo_Mes_Ano + "='" + Mes_Anio + "',";
                Mi_SQL += Ope_Con_Pagos.Campo_Tipo_Poliza_ID + "='00002',";
                Mi_SQL += Ope_Con_Pagos.Campo_No_poliza + "='" + No_Poliza + "' WHERE ";
                Mi_SQL += Ope_Con_Pagos.Campo_No_Pago + " ='" + Datos.P_No_Pago + "'";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();

                Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza, Cmmd);
                Mi_SQL = "UPDATE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " SET ";
                Mi_SQL += OPE_CON_DEUDORES.Campo_No_Pago + "='" + Datos.P_No_Pago + "',";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Cuenta_Banco_Pago + "='" + Datos.P_Cuenta_Banco_Pago + "'";
                Mi_SQL += " WHERE " + OPE_CON_DEUDORES.Campo_No_Deuda  + "='" + Datos.P_No_Deuda + "'";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();

                //Ejecutar transaccion
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                //Entregar el numero de pago
                return Datos.P_No_Pago + "-" + "SI";
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje); // Se indica el mensaje 
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Vale
        /// DESCRIPCION : Inserta en nuevo Vale en la BD
        /// PARAMETROS  : Datos: Contiene los datos de los filtros
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 08-Agosto-2012
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static String Alta_Vale(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            SqlDataAdapter da = new SqlDataAdapter(Cmmd);
            Object aux = new Object(); //Variable auxiliar 
            String Mi_SQL;
            string No_Poliza = string.Empty;                         //Obtiene el No con la cual se guardo los datos en la base de datos
            Int64 Consecutivo = 0;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
            double Saldo = 0;                                    //Obtiene el saldo de la cuenta contable                
            String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            DataTable Dt_Partidas = new DataTable();
            string Mensaje = string.Empty; //variable para el mensaje personalizado de error

            try
            {
                //Consulta para el maximo del numero de pago
                Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Con_Vales_Deudores.Campo_No_Vale_Deudor + "),'0000000000') ";
                Mi_SQL = Mi_SQL + "FROM " + Ope_Con_Vales_Deudores.Tabla_Ope_Con_Vales_Deudores;

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                aux = Cmmd.ExecuteScalar();

                //verificar si es nulo
                if (aux != null)
                {
                    if (aux != DBNull.Value)
                    {
                        Datos.P_No_Vale_Deudor = String.Format("{0:0000000000}", Convert.ToInt32(aux) + 1);
                    }
                    else
                    {
                        Datos.P_No_Vale_Deudor = "0000000001";
                    }
                }
                else
                {
                    Datos.P_No_Vale_Deudor = "0000000001";
                }
                //Consulta para los pagos
                Mi_SQL = "INSERT INTO " + Ope_Con_Vales_Deudores.Tabla_Ope_Con_Vales_Deudores + "(";
                Mi_SQL += Ope_Con_Vales_Deudores.Campo_No_Vale_Deudor + ", ";
                Mi_SQL += Ope_Con_Vales_Deudores.Campo_No_Deuda+ ", ";
                Mi_SQL += Ope_Con_Vales_Deudores.Campo_Importe + ", ";
                Mi_SQL += Ope_Con_Vales_Deudores.Campo_Estatus + ", ";
                Mi_SQL += Ope_Con_Vales_Deudores.Campo_Usuario_Creo + ", ";
                Mi_SQL += Ope_Con_Vales_Deudores.Campo_Fecha_Creo + ") VALUES('";
                Mi_SQL += Datos.P_No_Vale_Deudor + "', '";
                Mi_SQL += Datos.P_No_Deuda + "',";
                Mi_SQL += Datos.P_Importe + ",'";
                Mi_SQL += Datos.P_Estatus + "', '";
                Mi_SQL += Datos.P_Usuario_Autorizo + "',  GETDATE())";
                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();

                //Consulta para actualizar la tabla de los deudores
                Mi_SQL = "UPDATE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " SET ";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Estatus + "='PAGADO',";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Autorizo + "',";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Fecha_Modifico + "= GETDATE() " + " WHERE ";
                Mi_SQL += OPE_CON_DEUDORES.Campo_No_Deuda + " ='" + Datos.P_No_Deuda + "'";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();

                //Consulta para la obtención del último ID dado de alta 
                Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')" +
                " FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                " WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'" +
                " AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00003'";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                aux = Cmmd.ExecuteScalar();

                //Verificar si no es nulo
                if (aux != null)
                {
                    if (aux != DBNull.Value)
                    {
                        No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(aux) + 1);
                    }
                    else
                    {
                        No_Poliza = "0000000001";
                    }
                }
                else
                {
                    No_Poliza = "0000000001";
                }

                //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                Mi_SQL = "INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                " (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", " +
                Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", " +
                Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", " +
                Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", " +
                Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", " +
                Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + "," + Ope_Con_Polizas.Campo_Referencia + ")" +
                " VALUES ('" + No_Poliza + "', '00003', '" + Mes_Anio + "'," +
                " '" + String.Format("{0:dd/MM/yyyy}", DateTime.Today) + "'," +
                " '" + "Vale de Caja Chica " + Datos.P_No_Pago + "', " + Datos.P_Importe + ", " + Datos.P_Importe + "," + Datos.P_No_Partidas + ", " +
                "'" + Datos.P_Usuario_Autorizo + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', " +
                "'" + Cls_Sessiones.Empleado_ID + "','" + Datos.P_Referencia + "')";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();

                //Da de alta los detalles de la póliza
                foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                {
                    //consulta el saldo de la cuenta contable
                    Mi_SQL = "SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - " +
                    " ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo" +
                    " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                    " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'";

                    //Ejecutar consulta
                    Cmmd.CommandText = Mi_SQL;
                    aux = Cmmd.ExecuteScalar();

                    //verificar si no es nulo
                    if (aux != null)
                    {
                        if (aux != DBNull.Value)
                        {
                            Saldo = Convert.ToDouble(aux);
                        }
                        else
                        {
                            Saldo = 0;
                        }
                    }
                    else
                    {
                        Saldo = 0;
                    }

                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                    {
                        Saldo = Saldo + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                    {
                        Saldo = Saldo - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                    }

                    //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')" +
                    " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;

                    //Ejecutar consulta
                    Cmmd.CommandText = Mi_SQL;
                    aux = Cmmd.ExecuteScalar();

                    //Verificar si nio es nulo
                    if (aux != null)
                    {
                        if (aux != DBNull.Value)
                        {
                            Consecutivo = Convert.ToInt64(aux) + 1;
                        }
                        else
                        {
                            Consecutivo = 1;
                        }
                    }
                    else
                    {
                        Consecutivo = 1;
                    }

                    //Inserta el registro del detalle de la póliza en la base de datos
                    Mi_SQL = "INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                    "(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " +
                    Ope_Con_Polizas_Detalles.Campo_Consecutivo + "," + Ope_Con_Polizas_Detalles.Campo_Referencia + ")" +
                    " VALUES('" + No_Poliza + "', '00003', '" + Mes_Anio + "', " +
                    Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + "," +
                    " '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'," +
                    " '" + "CHEQUE " + Datos.P_No_Pago + "', " +
                    Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", " +
                    Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", '" +
                    Renglon[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID].ToString() + "','" + Renglon[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario].ToString() + "', " +
                    Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ",'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Referencia].ToString() + "')";

                    //Ejecutar consulta
                    Cmmd.CommandText = Mi_SQL;
                    Cmmd.ExecuteNonQuery();
                }

                // se inserta el numero de poliza, el tipo de poliza y el mes_anio del pago
                Mi_SQL = "UPDATE " + Ope_Con_Vales_Deudores.Tabla_Ope_Con_Vales_Deudores + " SET ";
                Mi_SQL += Ope_Con_Vales_Deudores.Campo_Mes_Ano + "='" + Mes_Anio + "',";
                Mi_SQL += Ope_Con_Vales_Deudores.Campo_Tipo_Poliza_ID + "='00003',";
                Mi_SQL += Ope_Con_Vales_Deudores.Campo_No_Poliza + "='" + No_Poliza + "' WHERE ";
                Mi_SQL += Ope_Con_Vales_Deudores.Campo_No_Vale_Deudor + " ='" + Datos.P_No_Vale_Deudor + "'";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();

                Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza, Cmmd);
                ////Mi_SQL = "UPDATE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " SET ";
                ////Mi_SQL += OPE_CON_DEUDORES.Campo_No_Pago + "='" + Datos.P_No_Pago + "',";
                ////Mi_SQL += OPE_CON_DEUDORES.Campo_Cuenta_Banco_Pago + "='" + Datos.P_Cuenta_Banco_Pago + "'";
                ////Mi_SQL += " WHERE " + OPE_CON_DEUDORES.Campo_No_Deuda + "='" + Datos.P_No_Deuda + "'";

                //////Ejecutar consulta
                ////Cmmd.CommandText = Mi_SQL;
                ////Cmmd.ExecuteNonQuery();

                //Ejecutar transaccion
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                //Entregar el numero de pago
                return Datos.P_No_Vale_Deudor + "-" + "SI";
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje); // Se indica el mensaje 
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Comprobacion_Vale
        /// DESCRIPCION : Inserta en nuevo Vale en la BD
        /// PARAMETROS  : Datos: Contiene los datos de los filtros
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 31-Agosto-2013
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static String Comprobacion_Vale(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            SqlDataAdapter da = new SqlDataAdapter(Cmmd);
            Object aux = new Object(); //Variable auxiliar 
            String Mi_SQL;
            string No_Poliza = string.Empty;                         //Obtiene el No con la cual se guardo los datos en la base de datos
            Int64 Consecutivo = 0;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
            double Saldo = 0;                                    //Obtiene el saldo de la cuenta contable                
            String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            DataTable Dt_Partidas = new DataTable();
            string Mensaje = string.Empty; //variable para el mensaje personalizado de error

            try
            {
                foreach (DataRow Row in Datos.P_Dt_Datos_Agrupados.Rows)
                {
                    Mi_SQL = "UPDATE " + Ope_Con_Vales_Deudores.Tabla_Ope_Con_Vales_Deudores + " SET ";
                    Mi_SQL = Mi_SQL + Ope_Con_Vales_Deudores.Campo_Estatus + "='" + Datos.P_Estatus + "',";
                    Mi_SQL = Mi_SQL + Ope_Con_Vales_Deudores.Campo_Fecha_Modifico + "=GETDATE(),";
                    Mi_SQL = Mi_SQL + Ope_Con_Vales_Deudores.Campo_Usuario_Modifico + "='" + Cls_Sessiones.Nombre_Empleado.ToString() + "'";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Con_Vales_Deudores.Campo_No_Deuda + "='" + Row["NO_DEUDA"].ToString()+ "'";
                    Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Cmmd.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                }
                //Ejecutar transaccion
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                //Entregar el numero de pago
                return "SI";
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje); // Se indica el mensaje 
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Solicitudes_Pagadas_o_Porpagar
        /// DESCRIPCION : Consulta las solicitudes de pago que estan Autorizadas o pagadas
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Agosto/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Solicitudes_Pagadas_o_Porpagar(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT DEUDORES.* , CASE WHEN (DEUDORES." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "= 'EMPLEADO') THEN(";
                Mi_SQL += " SELECT " + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + " FROM ";
                Mi_SQL += Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "= DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + ") ELSE ";
                Mi_SQL += " PROVEEDOR." + Cat_Com_Proveedores.Campo_Compañia + " END AS NOMBRE, BANCO." + Cat_Nom_Bancos.Campo_Nombre + " as BANCO FROM " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " DEUDORES";
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDOR ON DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + "=PROVEEDOR." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO ON DEUDORES." + OPE_CON_DEUDORES.Campo_Cuenta_Banco_Pago + "=BANCO." + Cat_Nom_Bancos.Campo_Banco_ID;
                Mi_SQL += " WHERE DEUDORES." + OPE_CON_DEUDORES.Campo_No_Deuda + " IS NOT NULL";
                if (Datos.P_Estatus != "" || Datos.P_Estatus != null)
                {
                    Mi_SQL += " AND (DEUDORES." + OPE_CON_DEUDORES.Campo_Estatus + " ='" + Datos.P_Estatus + "')";
                }
                else
                {
                    Mi_SQL += " AND (DEUDORES." + OPE_CON_DEUDORES.Campo_Estatus + " = 'PORPAGAR')";

                }
                if (!String.IsNullOrEmpty(Datos.P_No_Deuda) && Datos.P_No_Deuda != "0")
                {
                    Mi_SQL += " AND DEUDORES." + OPE_CON_DEUDORES.Campo_No_Deuda +
                    " = '" + Datos.P_No_Deuda + "'";
                    Mi_SQL += " AND DEUDORES." + OPE_CON_DEUDORES.Campo_Fecha_Creo + 
                            " >= '" + Datos.P_Fecha_Inicio + "' AND " +
                    "DEUDORES." + OPE_CON_DEUDORES.Campo_Fecha_Creo + 
                            " <= '" + Datos.P_Fecha_Final + "'";

                }
                if ((String.IsNullOrEmpty(Datos.P_No_Deuda) || Datos.P_No_Deuda == "0") && !String.IsNullOrEmpty(Datos.P_Fecha_Inicio) && !String.IsNullOrEmpty(Datos.P_Fecha_Final))
                {
                    Mi_SQL += " AND DEUDORES." + OPE_CON_DEUDORES.Campo_Fecha_Creo + 
                        " >= '" + Datos.P_Fecha_Inicio + "' AND " +
                "DEUDORES." + OPE_CON_DEUDORES.Campo_Fecha_Creo +
                        " <= '" + Datos.P_Fecha_Final + "'";
                }
                Mi_SQL += " ORDER BY DEUDORES." + OPE_CON_DEUDORES.Campo_No_Deuda  + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Pago
        /// DESCRIPCION : Modifica los datos del  Pago con lo que fueron 
        ///               introducidos por el usuario
        /// PARAMETROS  :  Datos: Datos que son enviados de la capa de Negocios y que fueron 
        ///                       proporcionados por el usuario y van a sustituir a los datos que se
        ///                       encuentran en la base de datos
        /// CREO        : Sergio Manuel Galalrdo
        /// FECHA_CREO  : 17-Noviembre-2011
        /// MODIFICO          :sergio Manuel Gallardo Andrade
        /// FECHA_MODIFICO    :25/nov/2011
        /// CAUSA_MODIFICACION:se adacto el metodo para la cancelacion del pago 
        ///*******************************************************************************
        public static void Modificar_Pago(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            Double Total_Poliza = 0;                         //Obtiene el monto total del debe y haber de la póliza
            String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
            StringBuilder Mi_SQL = new StringBuilder();      //Obtiene los datos de la inserción a realizar a la base de datos
            Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
            Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
            Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
            SqlCommand Comando_SQL = new SqlCommand(); //Sirve para la ejecución de las operaciones a la base de datos
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlTransaction Transaccion_SQL;                                                  //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            try
            {
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;

                Mi_SQL.Append("UPDATE " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos);
                Mi_SQL.Append(" SET " + Ope_Con_Pagos.Campo_Estatus + " = '" + Datos.P_Estatus + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Motivo_Cancelacion)) Mi_SQL.Append(Ope_Con_Pagos.Campo_Motivo_Cancelacion + " = '" + Datos.P_Motivo_Cancelacion + "', ");
                if (!String.IsNullOrEmpty(Datos.P_Comentario)) Mi_SQL.Append(Ope_Con_Pagos.Campo_Comentarios + " = '" + Datos.P_Comentario + "', ");
                Mi_SQL.Append(Ope_Con_Pagos.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Autorizo + "', ");
                Mi_SQL.Append(Ope_Con_Pagos.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + Ope_Con_Pagos.Campo_No_Pago + " = '" + Datos.P_No_Pago + "'");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                Mi_SQL.Length = 0;
                Mi_SQL.Append("UPDATE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores );
                Mi_SQL.Append(" SET " + OPE_CON_DEUDORES.Campo_Estatus + " = 'PORPAGAR', ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_No_Pago + " = '', ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Mes_Ano  + " = '', ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Tipo_Poliza_ID  + " = '', ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_No_Poliza  + " = '', ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Autorizo + "', ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + OPE_CON_DEUDORES.Campo_No_Deuda  + " = '" + Datos.P_No_Deuda + "'");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  


                Mi_SQL.Length = 0;
                //Consulta para la obtención del último ID dado de alta 
                Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')");
                Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'");
                Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00003'");
                Mi_SQL.Append(" ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                No_Poliza = Comando_SQL.ExecuteScalar();
                //Valida si el ID es nulo para asignarle automaticamente el primer registro
                if (Convert.IsDBNull(No_Poliza))
                {
                    No_Poliza = "0000000001";
                }
                //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                else
                {
                    No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                }
                Mi_SQL.Length = 0;
                Total_Poliza += Convert.ToDouble(Datos.P_Importe );
                //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                Mi_SQL.Append(" (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", ");
                Mi_SQL.Append(Cat_Empleados.Campo_Usuario_Creo + ", " + Cat_Empleados.Campo_Fecha_Creo + ", ");
                Mi_SQL.Append(Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + ")");
                Mi_SQL.Append(" VALUES ('" + No_Poliza + "', '00002', '" + Mes_Anio + "',");
                Mi_SQL.Append(" '" + String.Format("{0:dd/MM/yyyy}", DateTime.Today) + "',");
                Mi_SQL.Append(" '" + Datos.P_Comentario + "', " + Total_Poliza + ", " + Total_Poliza + ", 2, ");
                Mi_SQL.Append("'" + Datos.P_Usuario_Autorizo  + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', ");
                Mi_SQL.Append("'" + Cls_Sessiones.Empleado_ID + "')");
                Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos  

                //Da de alta los detalles de la póliza
                foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                {
                    Mi_SQL.Length = 0;
                    //consulta el saldo de la cuenta contable
                    Mi_SQL.Append("SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - ");
                    Mi_SQL.Append(" ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'");
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecución para obtener el Saldo
                    Saldo = Comando_SQL.ExecuteScalar();
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    }
                    if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                    {
                        Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                    }

                    Mi_SQL.Length = 0;
                    //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                    Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')");
                    Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Realiza la ejecución de la obtención del consecutivo
                    Consecutivo = Comando_SQL.ExecuteScalar();

                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(Consecutivo))
                    {
                        Consecutivo = "1";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                    }
                    Mi_SQL.Length = 0;
                    //Inserta el registro del detalle de la póliza en la base de datos
                    Mi_SQL.Append("INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                    Mi_SQL.Append("(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", ");
                    Mi_SQL.Append(Ope_Con_Polizas_Detalles.Campo_Consecutivo + ")");
                    Mi_SQL.Append(" VALUES('" + No_Poliza + "', '00002', '" + Mes_Anio + "', ");
                    Mi_SQL.Append(Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + ",");
                    Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "',");
                    Mi_SQL.Append(" '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Concepto].ToString() + "', ");
                    Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", '");
                    Mi_SQL.Append(Renglon[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID].ToString() + "','");
                    Mi_SQL.Append(Renglon[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario].ToString() + "', ");
                    Mi_SQL.Append(Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ")");
                    Comando_SQL.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                    Comando_SQL.ExecuteNonQuery();               //Ejecuta la inserción en memoria antes de pasarla a la base de datos            
                }
                Transaccion_SQL.Commit(); //Pasa todo el proceso que se encuentra en memoria a la base de datos para ser almacenados los datos
                Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Solicitud_Pago
        /// DESCRIPCION : Consulta los tipos de solicitudes de pago que existen
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Solicitud_Pago(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                //Consulta los movimientos de las cuentas contables.
                Mi_SQL = "SELECT Solicitud.*, Banco." + Cat_Nom_Bancos.Campo_Nombre + " FROM " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " Solicitud ";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN  " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " Banco On Solicitud." + OPE_CON_DEUDORES.Campo_Cuenta_Banco_Pago + " =Banco.";
                Mi_SQL = Mi_SQL + Cat_Nom_Bancos.Campo_Banco_ID + " WHERE " + OPE_CON_DEUDORES.Campo_No_Deuda  + " ='" + Datos.P_No_Deuda  + "'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Datos_Bancarios
        /// COMENTARIOS:    Consulta las solicitudes  con numero de transferencia
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Sergio Manuel Gallardo
        /// FECHA CREÓ:     8/Agosto/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Datos_Bancarios(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                Mi_SQL.Append("Select ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + ".*, ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Nombre + " as Nombre_Banco, ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + " as Nombre_Beneficiario, ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_No_Tarjeta);
                Mi_SQL.Append(" From "+ OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores);

                Mi_SQL.Append(" left outer join ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados);
                Mi_SQL.Append(" ON ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Deudor_ID);
                Mi_SQL.Append("=");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);

                Mi_SQL.Append(" left outer join ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos);
                Mi_SQL.Append(" ON ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Banco_ID);
                Mi_SQL.Append("=");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Banco_ID);

                Mi_SQL.Append(" Where ");

                if (!String.IsNullOrEmpty(Datos.P_No_Deuda))
                {
                    Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_No_Deuda  + "='" + Datos.P_No_Deuda + "' ");
                }

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0]; ;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Datos_Bancarios_Proveedor
        /// COMENTARIOS:    Consulta las solicitudes  con numero de transferencia
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Sergio Manuel Gallardo  
        /// FECHA CREÓ:     18/Agosto/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Datos_Bancarios_Proveedor(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                Mi_SQL.Append("Select ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores  + ".*, ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Banco_Proveedor + " as Nombre_Banco, ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Compañia + " as Nombre_Proveedor, ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Banco_Proveedor_ID + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Clabe);
                Mi_SQL.Append(" From "+ OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores);

                Mi_SQL.Append(" left outer join ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores);
                Mi_SQL.Append(" ON ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Deudor_ID);
                Mi_SQL.Append("=");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID);

                Mi_SQL.Append(" Where ");

                if (!String.IsNullOrEmpty(Datos.P_No_Deuda ))
                {
                    Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_No_Deuda  + "='" + Datos.P_No_Deuda  + "' ");
                }

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0]; ;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_transferencia
        /// DESCRIPCION : Consulta los datos de la solicitud para la transferencia
        /// PARAMETROS  : Datos: Recibe los datos proporcionados por el usuario.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 16/Agosto/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Datos_transferencia(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            String Mi_SQL;  //Almacenara la Query de Consulta.
            try
            {
                Mi_SQL = "SELECT DEUDORES.*" + ", CASE WHEN (DEUDORES." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "='EMPLEADO') THEN ( SELECT " + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno + "+' ' +" + Cat_Empleados.Campo_Apellido_Materno;
                Mi_SQL += " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "= DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + ") ELSE PROVEEDOR." + Cat_Com_Proveedores.Campo_Compañia + " END AS BENEFICIARIO, ";
                Mi_SQL += " CASE WHEN ( DEUDORES." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "='EMPLEADO') THEN (SELECT " + Cat_Empleados.Campo_Correo_Electronico + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "=DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID;
                Mi_SQL += ") ELSE PROVEEDOR." + Cat_Com_Proveedores.Campo_Correo_Electronico + " END AS CORREO, CASE WHEN (DEUDORES." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "='EMPLEADO') THEN( SELECT BANCO." + Cat_Nom_Bancos.Campo_Nombre + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO LEFT OUTER JOIN ";
                Mi_SQL += Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO ON BANCO." + Cat_Nom_Bancos.Campo_Banco_ID + "= EMPLEADO." + Cat_Empleados.Campo_Banco_ID + " WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "= DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + ") ELSE PROVEEDOR." + Cat_Com_Proveedores.Campo_Banco_Proveedor + " END AS BANCO_DEUDOR ";
                Mi_SQL += ", CASE WHEN ( DEUDORES." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + " ='EMPLEADO') THEN(SELECT EMPLEADO." + Cat_Empleados.Campo_No_Cuenta_Bancaria + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "=DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + ") ELSE PROVEEDOR." + Cat_Com_Proveedores.Campo_Cuenta;
                Mi_SQL += " END AS CUENTA_DESTINO, CASE WHEN(DEUDORES." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "='EMPLEADO') THEN( SELECT EMPLEADO." + Cat_Empleados.Campo_No_Tarjeta + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "= DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + ") ELSE PROVEEDOR." + Cat_Com_Proveedores.Campo_Clabe + " END AS CLABE,";
                Mi_SQL += " BANCO." + Cat_Nom_Bancos.Campo_Nombre + " AS BANCO , BANCO." + Cat_Nom_Bancos.Campo_No_Cuenta + " AS CUENTA_ORIGEN, PROVEEDOR."+Cat_Com_Proveedores.Campo_Banco_Proveedor_ID +" AS ID ";
                Mi_SQL += " FROM " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + " DEUDORES";
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDOR ON DEUDORES." + OPE_CON_DEUDORES.Campo_Deudor_ID + " = PROVEEDOR." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL += " LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO ON DEUDORES." + OPE_CON_DEUDORES.Campo_Cuenta_Banco_Pago + " = BANCO." + Cat_Nom_Bancos.Campo_Banco_ID;
                Mi_SQL += " WHERE DEUDORES." + OPE_CON_DEUDORES.Campo_No_Deuda + "='" + Datos.P_No_Deuda + "'";
                Mi_SQL += " AND DEUDORES." + OPE_CON_DEUDORES.Campo_Forma_Pago + "='Transferencia'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION: Alta_Transferencia
        /// DESCRIPCION : Agrega la informacion de la tranferencia a la solicitud de pago 
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 17/agosto/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Alta_Transferencia(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            String Mi_SQL;
            try
            {
                //Da de Alta los datos del Cierre Mensual con los datos proporcionados por el usuario
                Mi_SQL = "UPDATE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores  + " SET ";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Transferencia + "='" + Datos.P_Transferencia + "',";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Cuenta_Transferencia_ID + "='" + Datos.P_Cuenta_Transferencia + "',";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Autorizo  + "',";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Fecha_Layout_Transferencia + "=GETDATE(),";
                Mi_SQL += OPE_CON_DEUDORES.Campo_Fecha_Modifico + "= GETDATE() " + " WHERE ";
                Mi_SQL += OPE_CON_DEUDORES.Campo_No_Deuda  + " ='" + Datos.P_No_Deuda  + "'";
                SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Solicitud_Transferencia
        /// COMENTARIOS:    Consulta las solicitudes de pago con estatus de ejercida y con numero de transferencia
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     18/Abril/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Solicitud_Transferencia(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                Mi_SQL.Append("Select ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores  + ".*, ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Nombre + " as Nombre_Banco, ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_No_Cuenta + " as No_Cuenta, ");
                Mi_SQL.Append("CASE WHEN ("+OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores+"." + OPE_CON_DEUDORES.Campo_Tipo_Deudor + "='EMPLEADO') THEN ( SELECT " + Cat_Empleados.Campo_Nombre + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno + "+' ' +" + Cat_Empleados.Campo_Apellido_Materno);
                Mi_SQL.Append(" FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO WHERE EMPLEADO." + Cat_Empleados.Campo_Empleado_ID + "= "+OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores+"." + OPE_CON_DEUDORES.Campo_Deudor_ID + ") ELSE "+Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores+"." + Cat_Com_Proveedores.Campo_Compañia + " END AS BENEFICIARIO ");
                Mi_SQL.Append(" From ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores );

                Mi_SQL.Append(" left outer join ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos);
                Mi_SQL.Append(" ON ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Cuenta_Transferencia_ID);
                Mi_SQL.Append("=");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Banco_ID);

                Mi_SQL.Append(" left outer join ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores);
                Mi_SQL.Append(" ON ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Deudor_ID);
                Mi_SQL.Append("=");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID);

                Mi_SQL.Append(" Where ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Estatus + "='PORPAGAR' ");
                Mi_SQL.Append(" AND ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Transferencia + " is not null ");

                if (!String.IsNullOrEmpty(Datos.P_No_Deuda))
                {
                    Mi_SQL.Append(" And " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_No_Deuda + "='" + Datos.P_No_Deuda  + "' ");
                }
                if (!String.IsNullOrEmpty(Datos.P_Banco))
                {
                    Mi_SQL.Append(" And " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Nombre + "='" + Datos.P_Banco + "' ");
                }
                Mi_SQL.Append(" Order by ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_No_Deuda);


                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0]; ;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }
        ///****************************************************************************************
        /// NOMBRE DE LA FUNCION:  Consulta_Datos_Deudor
        /// DESCRIPCION :          Consultar los proveedores de acuerdo al ID del proveedor proporcionado por el usuario
        /// PARAMETROS  :          Datos: Variable que contiene los datos para la busqueda
        /// CREO        :          Noe Mosqueda Valadez
        /// FECHA_CREO  :          27/Septiembre/2010 18:59
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///****************************************************************************************/
        public static DataTable Consulta_Datos_Deudor(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            String Mi_SQL; //Variable para las consultas

            try
            {
                //Asignar consulta
                if(Datos.P_Tipo_Deudor=="PROVEEDOR"){
                    Mi_SQL = "SELECT  " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "."+Cat_Com_Proveedores.Campo_Banco_Proveedor +" AS BANCO_DEUDOR ";
                    Mi_SQL = Mi_SQL + "FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ";

                    if (Datos.P_Deudor_ID != null && Datos.P_Deudor_ID != "")     // Si el P_Proveedore_ID no esá vacío, filtrar por ID
                    {
                        Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + " = '" + Datos.P_Deudor_ID + "' ";
                    }
                    Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores +"."+ Cat_Com_Proveedores.Campo_Nombre;
                }else{
                    Mi_SQL = "SELECT  " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Nombre + " AS BANCO_DEUDOR ";
                    Mi_SQL +=  " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " LEFT OUTER JOIN "+ Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos;
                    Mi_SQL += " ON " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Banco_ID + "=" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Banco_ID;
                    if (Datos.P_Deudor_ID != null && Datos.P_Deudor_ID != "")     // Si el P_Proveedore_ID no esá vacío, filtrar por ID
                    {
                        Mi_SQL +=" WHERE " + Cat_Empleados.Campo_Empleado_ID + " = '" + Datos.P_Deudor_ID + "' ";
                    }
                    Mi_SQL += " ORDER BY "+Cat_Empleados.Tabla_Cat_Empleados +"." + Cat_Empleados.Campo_Nombre;
                }
                

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {

            }
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Modificar_Transferencia
        /// COMENTARIOS:    cambiara los datos transferencia, y cuenta_transferencia 
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     18/Sergio Manuel Gallardo/2012  
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static Boolean Modificar_Transferencia(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion;//Variable para la conexión para la base de datos   
            SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;

            Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Comando = new SqlCommand();
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Transaction = Transaccion;
            Comando.Connection = Conexion;
            try
            {
                Mi_SQL.Append("UPDATE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores  + " SET ");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Transferencia + "='',");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Cuenta_Transferencia_ID + "='',");
                Mi_SQL.Append(OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores + "." + OPE_CON_DEUDORES.Campo_Fecha_Layout_Transferencia + "=''");
                Mi_SQL.Append(" Where " + OPE_CON_DEUDORES.Campo_No_Deuda + "='" + Datos.P_No_Deuda + "'");

                //Ejecutar consulta
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Conexion.Close();
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                if (Transaccion != null)
                {
                    Transaccion.Rollback();
                }
                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Conexion.Close();
                Comando = null;
                Conexion = null;
                Transaccion = null;
            }
            return Operacion_Completa;
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Pago
        /// DESCRIPCION : Inserta en nuevo Cheque en la BD
        /// PARAMETROS  : Datos: Contiene los datos de los filtros
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 08/junio/2011
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static String Alta_Pago(Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos)
        {
            try
            {
                String Mi_SQL;
                Object Compromisos_ID; //Variable que contendrá el ID de la consulta
                Object No_Poliza = null;                         //Obtiene el No con la cual se guardo los datos en la base de datos
                Object Consecutivo = null;                       //Obtiene el consecutivo con la cual se guardo los datos en la base de datos
                Object Saldo;                                    //Obtiene el saldo de la cuenta contable                
                String Mes_Anio = String.Format("{0:MMyy}", DateTime.Today); //Obtiene el mes y año que se le asiganara a la póliza
                DataTable Dt_Partidas = new DataTable();
                DataTable Dt_Reserva_Detalle = new DataTable();
                DataTable Dt_Solicitudes = new DataTable();
                DataTable Dt_Reserva = new DataTable();
                DataTable Dt_Saldos_Reserva = new DataTable();
                String Dependencia_ID = "";
                Boolean Agregar;
                Double Monto;
                String Tipo_Reserva = "";
                DataRow Fila;
                Double Saldo_Reserva;
                int Registro_Presupuestal;
                //Busca el maximo ID de la tabla Compromisos
                foreach (DataRow Filas in Datos.P_Dt_Datos_Completos.Rows)
                {
                    //Cuenta = Convert.ToString(Fila["CUENTA_BANCO_PAGO_ID"].ToString()).Trim();
                    if (Dt_Solicitudes.Rows.Count <= 0 && Dt_Solicitudes.Columns.Count <= 0)
                    {
                        Dt_Solicitudes.Columns.Add("No_Solicitud", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Referencia", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Banco", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Deudor", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Cuenta", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Importe", typeof(System.Double));
                        Dt_Solicitudes.Columns.Add("Banco_Deudor", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Cuenta_Contable_ID_Banco", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Cuenta_Contable_Deudor", typeof(System.String));
                        Dt_Solicitudes.Columns.Add("Monto_Transferencia", typeof(System.Double));
                        Dt_Solicitudes.Columns.Add("Tipo_Deudor", typeof(System.String));
                    }
                    DataRow ROW = Dt_Solicitudes.NewRow(); //Crea un nuevo registro a la tabla
                    if (Filas["Deudor"].ToString().Trim() == Datos.P_Dt_Datos_Agrupados.Rows[0]["Deudor"].ToString().Trim() && Filas["Cuenta"].ToString().Trim() == Datos.P_Dt_Datos_Agrupados.Rows[0]["Cuenta"].ToString().Trim() && Filas["Cuenta_Contable_ID_Banco"].ToString().Trim() == Datos.P_Dt_Datos_Agrupados.Rows[0]["Cuenta_Contable_ID_Banco"].ToString().Trim() && Filas["Cuenta_Contable_Deudor"].ToString().Trim() == Datos.P_Dt_Datos_Agrupados.Rows[0]["Cuenta_Contable_Deudor"].ToString().Trim() && Filas["Tipo_Deudor"].ToString().Trim() == Datos.P_Dt_Datos_Agrupados.Rows[0]["Tipo_Deudor"].ToString().Trim())
                    {
                        ROW["No_Solicitud"] = Filas["No_Solicitud"].ToString().Trim();
                        ROW["Referencia"] = Filas["Referencia"].ToString().Trim();
                        ROW["Banco"] = Filas["Banco"].ToString().Trim();
                        ROW["cuenta"] = Filas["cuenta"].ToString().Trim();
                        ROW["Deudor"] = Filas["Deudor"].ToString().Trim();
                        ROW["Importe"] = Filas["Importe"].ToString().Trim();
                        ROW["Banco_Deudor"] = Filas["Banco_Deudor"].ToString().Trim();
                        ROW["Cuenta_Contable_ID_Banco"] = Filas["Cuenta_Contable_ID_Banco"].ToString().Trim();
                        ROW["Cuenta_Contable_Deudor"] = Filas["Cuenta_Contable_Deudor"].ToString().Trim();
                        ROW["Monto_Transferencia"] = Filas["Monto_Transferencia"].ToString().Trim();
                        ROW["Tipo_Deudor"] = Filas["Tipo_Deudor"].ToString().Trim();
                        Dt_Solicitudes.Rows.Add(ROW); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Solicitudes.AcceptChanges();
                    }
                }
                if (Dt_Solicitudes.Rows.Count > 0)
                {
                    Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Con_Pagos.Campo_No_Pago + "),'00000') ";
                    Mi_SQL = Mi_SQL + "FROM " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos;
                    Compromisos_ID = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                    if (Convert.IsDBNull(Compromisos_ID)) //Si no existen valores en la tabla, asigna el primer valor manualmente.
                    {
                        Datos.P_No_Pago = "00001";
                    }
                    else // Si ya existen registros, toma el valor maximo y le suma 1 para el nuevo registro.
                    {
                        Datos.P_No_Pago = String.Format("{0:00000}", Convert.ToInt32(Compromisos_ID) + 1);
                    }

                    Mi_SQL = "INSERT INTO " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "(";
                    Mi_SQL += Ope_Con_Pagos.Campo_No_Pago + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_No_Solicitud_Pago + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Banco_ID + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Fecha_Pago + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Beneficiario_Pago + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Forma_Pago + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Referencia_Transferencia_Banca + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Estatus + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Comentarios + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Usuario_Creo + ", ";
                    Mi_SQL += Ope_Con_Pagos.Campo_Fecha_Creo + ") VALUES('";
                    Mi_SQL += Datos.P_No_Pago + "', ' ', '";
                    Mi_SQL += Datos.P_Dt_Datos_Agrupados.Rows[0]["CUENTA"].ToString().Trim() + "','";
                    Mi_SQL += Datos.P_Fecha_Pago + "','";
                    Mi_SQL += Datos.P_Beneficiario_Pago + "', '";
                    Mi_SQL += Datos.P_Forma_Pago  + "', '";
                    Mi_SQL += Datos.P_Dt_Datos_Agrupados.Rows[0]["REFERENCIA"].ToString().Trim() + "', '";
                    Mi_SQL += Datos.P_Estatus + "', '";
                    Mi_SQL += Datos.P_Comentario + "', '";
                    Mi_SQL += Datos.P_Usuario_Autorizo + "',  GETDATE())";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    Monto = Convert.ToDouble(Datos.P_Dt_Datos_Agrupados.Rows[0]["Importe"].ToString().Trim()) + (Convert.ToDouble(Datos.P_Monto_Iva) + Convert.ToDouble(Datos.P_Monto_Comision));
                    //Consulta para la obtención del último ID dado de alta 
                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas.Campo_No_Poliza + "),'000000000')" +
                    " FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                    " WHERE " + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Mes_Anio + "'" +
                    " AND " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = '00002'" +
                    " ORDER BY " + Ope_Con_Polizas.Campo_No_Poliza + " DESC";
                    No_Poliza = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    //Valida si el ID es nulo para asignarle automaticamente el primer registro
                    if (Convert.IsDBNull(No_Poliza))
                    {
                        No_Poliza = "0000000001";
                    }
                    //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                    else
                    {
                        No_Poliza = String.Format("{0:0000000000}", Convert.ToInt32(No_Poliza) + 1);
                    }
                    Mes_Anio = String.Format("{0:MMyy}", Convert.ToDateTime(Datos.P_Dt_Datos_Agrupados.Rows[0]["Fecha_Poliza"].ToString().Trim()));
                    //Consulta para la inserción de la póliza con los datos proporcionados por el usuario
                    Mi_SQL = "INSERT INTO " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +
                    " (" + Ope_Con_Polizas.Campo_No_Poliza + ", " + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ", " +
                    Ope_Con_Polizas.Campo_Mes_Ano + ", " + Ope_Con_Polizas.Campo_Fecha_Poliza + ", " +
                    Ope_Con_Polizas.Campo_Concepto + ", " + Ope_Con_Polizas.Campo_Total_Debe + ", " +
                    Ope_Con_Polizas.Campo_Total_Haber + ", " + Ope_Con_Polizas.Campo_No_Partidas + ", " +
                    Ope_Con_Polizas.Campo_Usuario_Creo + ", " + Ope_Con_Polizas.Campo_Fecha_Creo + ", " +
                    Ope_Con_Polizas.Campo_Empleado_ID_Creo + ", " + Ope_Con_Polizas.Campo_Empleado_ID_Autorizo + "," + Ope_Con_Polizas.Campo_Referencia + ")" +
                    " VALUES ('" + No_Poliza + "', '00002', '" + Mes_Anio + "'," +
                    " '" + String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Datos.P_Dt_Datos_Agrupados.Rows[0]["Fecha_Poliza"].ToString().Trim())) + "'," +
                    " '" + "TRANSFERENICA " + Datos.P_No_Pago + "', " + Monto + ", " + Monto + "," + Datos.P_No_Partidas + ", " +
                    "'" + Datos.P_Usuario_Autorizo  + "', GETDATE(), '" + Cls_Sessiones.Empleado_ID + "', " +
                    "'" + Cls_Sessiones.Empleado_ID + "','" + Datos.P_Dt_Datos_Agrupados.Rows[0]["REFERENCIA"].ToString().Trim() + "')";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);


                    //Da de alta los detalles de la póliza
                    foreach (DataRow Renglon in Datos.P_Dt_Detalles_Poliza.Rows)
                    {
                        //consulta el saldo de la cuenta contable
                        Mi_SQL = "SELECT (ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "),'0') - " +
                        " NVL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "),'0')) AS Saldo" +
                        " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                        " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + " = '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'";
                        Saldo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) + Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                        }
                        if (Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                        {
                            Saldo = Convert.ToDouble(Saldo) - Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                        }

                        //Consulta para la obtención del último consecutivo dado de alta en la tabla de detalles de poliza
                        Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Con_Polizas_Detalles.Campo_Consecutivo + "),'0000000000')" +
                        " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                        Consecutivo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                        //Valida si el ID es nulo para asignarle automaticamente el primer registro
                        if (Convert.IsDBNull(Consecutivo))
                        {
                            Consecutivo = "1";
                        }
                        //Si no esta vacio el registro entonces al registro que se obtenga se le suma 1 para poder obtener el último registro
                        else
                        {
                            Consecutivo = Convert.ToInt32(Consecutivo) + 1;
                        }

                        //Inserta el registro del detalle de la póliza en la base de datos
                        Mi_SQL = "INSERT INTO " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +
                        "(" + Ope_Con_Polizas_Detalles.Campo_No_Poliza + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + ", " +
                        Ope_Con_Polizas_Detalles.Campo_Mes_Ano + ", " + Ope_Con_Polizas_Detalles.Campo_Partida + ", " +
                        Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Concepto + ", " +
                        Ope_Con_Polizas_Detalles.Campo_Debe + ", " + Ope_Con_Polizas_Detalles.Campo_Haber + ", " +
                        Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID + ", " + Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario + ", " +
                        Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " +
                        Ope_Con_Polizas_Detalles.Campo_Consecutivo + "," + Ope_Con_Polizas_Detalles.Campo_Referencia + ")" +
                        " VALUES('" + No_Poliza + "', '00002', '" + Mes_Anio + "', " +
                        Renglon[Ope_Con_Polizas_Detalles.Campo_Partida].ToString() + "," +
                        " '" + Renglon[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'," +
                        " '" + "TRANSFERENCIA " + Datos.P_No_Pago + "', " +
                        Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + ", " +
                        Convert.ToDouble(Renglon[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + ", '" +
                        Renglon[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID].ToString() + "','" + Renglon[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario].ToString() + "', " +
                        Convert.ToDouble(Saldo) + ", GETDATE(), " + Consecutivo + ",'" + Renglon[Ope_Con_Polizas_Detalles.Campo_Referencia].ToString() + "')";
                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    foreach (DataRow Data in Dt_Solicitudes.Rows)
                    {
                            Mi_SQL = "UPDATE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores  + " SET ";
                            Mi_SQL += OPE_CON_DEUDORES.Campo_Estatus + "='" + Datos.P_Estatus + "',";
                            Mi_SQL += OPE_CON_DEUDORES.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Autorizo + "',";
                            Mi_SQL += OPE_CON_DEUDORES.Campo_Fecha_Modifico + "= GETDATE() " + " WHERE ";
                            Mi_SQL += OPE_CON_DEUDORES.Campo_No_Deuda + " ='" + Data["No_Solicitud"].ToString() + "'";
                            SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                        // se incerta el numero de poliza, el tipo de poliza y el mes_anio del pago
                        Mi_SQL = "UPDATE " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " SET ";
                        Mi_SQL += Ope_Con_Pagos.Campo_Mes_Ano + "='" + Mes_Anio + "',";
                        Mi_SQL += Ope_Con_Pagos.Campo_Tipo_Poliza_ID + "='00002',";
                        Mi_SQL += Ope_Con_Pagos.Campo_No_poliza + "='" + No_Poliza + "' WHERE ";
                        Mi_SQL += Ope_Con_Pagos.Campo_No_Pago + " ='" + Datos.P_No_Pago + "'";
                        SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                        Cls_Ope_Con_Polizas_Datos.Consulta_Saldo_y_Actualiza(DateTime.Today, Datos.P_Dt_Detalles_Poliza);

                        Mi_SQL = "UPDATE " + OPE_CON_DEUDORES.Tabla_Ope_Con_Deudores  + " SET ";
                        Mi_SQL += OPE_CON_DEUDORES.Campo_No_Pago + "='" + Datos.P_No_Pago + "'";
                        Mi_SQL += " WHERE " + OPE_CON_DEUDORES.Campo_No_Deuda + "='" + Data["No_Solicitud"].ToString() + "'";
                        SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                }
                return Datos.P_No_Pago;
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message, ex);
            }
        }
    }
}

