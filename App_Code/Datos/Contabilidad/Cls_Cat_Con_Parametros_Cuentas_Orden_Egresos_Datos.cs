﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
using System.Data;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Parametros_Cuentas_Orden_Egresos.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;

namespace JAPAMI.Parametros_Cuentas_Orden_Egresos.Datos
{
    public class Cls_Cat_Con_Parametros_Cuentas_Orden_Egresos_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Cuentas_Orden
        ///DESCRIPCIÓN          : Obtiene registros de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 1.Parametros. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Cuentas_Orden(Cls_Cat_Con_Parametros_Cuentas_Orden_Egresos_Negocio Parametros)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            DataSet Ds_Datos = new DataSet();
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL.Append("SELECT CUENTAS_EGRESOS.* , ");
                Mi_SQL.Append(" (LTRIM(RTRIM(CUENTAS_CONTABLES." + Cat_Con_Cuentas_Contables.Campo_Cuenta + ")) + ' ' + CUENTAS_CONTABLES." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ") AS Cuenta_Descripcion ");
                Mi_SQL.Append("FROM " + Cat_Con_Parametros_Cuentas_Orden_Egresos.Tabla_Cat_Con_Parametros_Cuentas_Orden_Egresos + " CUENTAS_EGRESOS ");
                Mi_SQL.Append("LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTAS_CONTABLES ");
                Mi_SQL.Append("ON CUENTAS_EGRESOS." + Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Cuenta_Contable_ID + " = CUENTAS_CONTABLES." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                if (!String.IsNullOrEmpty(Parametros.P_ID))
                {
                    Mi_SQL.Append(Entro_Where ? " AND CUENTAS_EGRESOS." : " WHERE CUENTAS_EGRESOS."); Entro_Where = true;
                    Mi_SQL.Append(Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_ID + " = " + Parametros.P_ID);
                }
                if (!String.IsNullOrEmpty(Parametros.P_Momento_Presupuestal))
                {
                    Mi_SQL.Append(Entro_Where ? " AND CUENTAS_EGRESOS." : " WHERE CUENTAS_EGRESOS."); Entro_Where = true;
                    Mi_SQL.Append("UPPER(" + Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Momento_Presupuestal + ") LIKE UPPER('%" + Parametros.P_Momento_Presupuestal + "%')");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Cuenta_Contable_ID))
                {
                    Mi_SQL.Append(Entro_Where ? " AND CUENTAS_EGRESOS." : " WHERE CUENTAS_EGRESOS."); Entro_Where = true;
                    Mi_SQL.Append(Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Cuenta_Contable_ID + " = '" + Parametros.P_Cuenta_Contable_ID + "'");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Descripcion))
                {
                    Mi_SQL.Append(Entro_Where ? " AND " : " WHERE "); Entro_Where = true;
                    Mi_SQL.Append("UPPER(CUENTAS_EGRESOS." + Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Descripcion + ") LIKE UPPER('%" + Parametros.P_Descripcion + "%')");
                }
                if (!String.IsNullOrEmpty(Mi_SQL.ToString()))
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }
                if (Ds_Datos != null && Ds_Datos.Tables.Count > -1)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Requisicion
        ///DESCRIPCIÓN          : Modifica una Requisicion en la base de datos.
        ///PARAMETROS           : Parametros: Contiene el registro que sera modificado.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 12/Enero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Modificar_Cuenta_Orden(Cls_Cat_Con_Parametros_Cuentas_Orden_Egresos_Negocio Parametros)
        {
            StringBuilder Mi_SQL;

            try
            {
                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("UPDATE " + Cat_Con_Parametros_Cuentas_Orden_Egresos.Tabla_Cat_Con_Parametros_Cuentas_Orden_Egresos + " SET ");
                if (!String.IsNullOrEmpty(Parametros.P_Momento_Presupuestal))
                { Mi_SQL.Append(Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Momento_Presupuestal + " = '" + Parametros.P_Momento_Presupuestal + "', "); }
                if (!String.IsNullOrEmpty(Parametros.P_Cuenta_Contable_ID))
                { Mi_SQL.Append(Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Cuenta_Contable_ID+ " = '" + Parametros.P_Cuenta_Contable_ID + "', "); }
                if (!String.IsNullOrEmpty(Parametros.P_Descripcion))
                { Mi_SQL.Append(Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Descripcion + " = '" + Parametros.P_Descripcion + "', "); }
                Mi_SQL.Append(Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "', ");
                Mi_SQL.Append(Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_ID + " = '" + Parametros.P_ID + "'");
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar actualizar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Dividir_Requisicion
        ///DESCRIPCIÓN          : Modifica una Requisicion en la base de datos.
        ///PARAMETROS           : Parametros: Contiene el registro que sera modificado.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 12/Enero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Crear_Cuenta_Orden(Cls_Cat_Con_Parametros_Cuentas_Orden_Egresos_Negocio Parametros)
        {
            StringBuilder Mi_SQL;
            DataTable Dt_Requisicion = new DataTable();

            try
            {
                Int32 Consecutivo = Obtener_Consecutivo(Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_ID, Cat_Con_Parametros_Cuentas_Orden_Egresos.Tabla_Cat_Con_Parametros_Cuentas_Orden_Egresos);
                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("INSERT INTO " + Cat_Con_Parametros_Cuentas_Orden_Egresos.Tabla_Cat_Con_Parametros_Cuentas_Orden_Egresos);
                Mi_SQL.Append(" (" + Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_ID);
                Mi_SQL.Append(", " + Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Momento_Presupuestal);
                Mi_SQL.Append(", " + Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append(", " + Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Descripcion);
                Mi_SQL.Append(", " + Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Fecha_Creo);
                Mi_SQL.Append(", " + Cat_Con_Parametros_Cuentas_Orden_Egresos.Campo_Usuario_Creo);
                Mi_SQL.Append(") VALUES ( " + Consecutivo);
                Mi_SQL.Append(" , '" + Parametros.P_Momento_Presupuestal);
                Mi_SQL.Append("', '" + Parametros.P_Cuenta_Contable_ID);
                Mi_SQL.Append("', '" + Parametros.P_Descripcion);
                Mi_SQL.Append("', GETDATE()");
                Mi_SQL.Append(" , '" + Cls_Sessiones.Nombre_Empleado + "')");
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar actualizar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Consecutivo
        ///DESCRIPCIÓN          : Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS           : 1.-Campo del cual se obtendra el consecutivo
        ///                       2.-Nombre de la tabla
        ///CREO                 : Gustavo Angeles Cruz
        ///FECHA_CREO           : 10/Enero/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'0') FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }
    }
}
