﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Cuentas_Gastos.Negocio;

namespace JAPAMI.Cuentas_Gastos.Datos
{
    public class Cls_Cat_Con_Cuentas_Gastos_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consecutivo_ID
        ///DESCRIPCIÓN          : consulta para obtener el consecutivo de una tabla
        ///PARAMETROS           1 Campo_Id: campo del que se obtendra el consecutivo
        ///                     2 Tabla: tabla del que se obtendra el consecutivo
        ///                     3 Tamaño: longitud del campo 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static String Consecutivo_ID(String Campo_Id, String Tabla, String Tamaño)
        {
            String Consecutivo = "";
            StringBuilder Mi_SQL = new StringBuilder();
            object Id; //Obtiene el ID con la cual se guardo los datos en la base de datos

            if (Tamaño.Equals("5"))
            {
                Mi_SQL.Append("SELECT ISNULL(MAX (" + Campo_Id + "), '00000')");
                Mi_SQL.Append(" FROM " + Tabla);

                Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (Convert.IsDBNull(Id))
                {
                    Consecutivo = "00001";
                }
                else
                {
                    Consecutivo = string.Format("{0:00000}", Convert.ToInt32(Id) + 1);
                }
            }
            else if (Tamaño.Equals("10"))
            {
                Mi_SQL.Append("SELECT ISNULL(MAX (" + Campo_Id + "), '0000000000')");
                Mi_SQL.Append(" FROM " + Tabla);

                Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (Convert.IsDBNull(Id))
                {
                    Consecutivo = "0000000001";
                }
                else
                {
                    Consecutivo = string.Format("{0:0000000000}", Convert.ToInt32(Id) + 1);
                }
            }

            return Consecutivo;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Gasto
        ///DESCRIPCIÓN          : consulta para guardar los datos de las clases
        ///PARAMETROS           1 Negocio: conexion con la capa de negocios
        ///CREO                 : Armando Zavala Morneo
        ///FECHA_CREO           : 09/Agosto/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static Boolean Alta_Gasto(Cls_Cat_Con_Cuentas_Gastos_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
            Boolean Operacion_Completa = false;//Estado de la operacion.
            String Id = Consecutivo_ID(Cat_Con_Cuentas_Gastos.Campo_Id_Gasto, Cat_Con_Cuentas_Gastos.Tabla_Cat_Con_Cuentas_Gastos, "5");

            try
            {
                Mi_SQL.Append("INSERT INTO " + Cat_Con_Cuentas_Gastos.Tabla_Cat_Con_Cuentas_Gastos + "(");
                Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Id_Gasto + ", ");
                //Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Cuenta_Contable_Id + ", ");
                Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Descripcion + ", ");
                Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Estatus + ", ");
                Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Concepto + ", ");
                Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Usuario_Creo + ", ");
                Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Fecha_Creo + ") VALUES( ");
                Mi_SQL.Append("'" + Id + "', ");
                //Mi_SQL.Append("'" + Negocio.P_Cuenta_Contable_Id + "', ");
                Mi_SQL.Append("'" + Negocio.P_Descripcion + "', ");
                Mi_SQL.Append("'" + Negocio.P_Estatus+ "', ");
                Mi_SQL.Append("'" + Negocio.P_Concepto + "', ");
                Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "', ");
                Mi_SQL.Append("GETDATE())");

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                foreach (DataRow Fila in Negocio.P_Dt_Detalles.Rows)
                {
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("INSERT INTO " + Cat_Con_Cuentas_Gastos_Detalles.Tabla_Cat_Con_Cuentas_Gastos_Detalles + "(");
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Id_Gasto + ", ");
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Partida_ID + ", ");
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Fecha_Creo + ") VALUES( ");
                    Mi_SQL.Append("'" + Id + "', ");
                    Mi_SQL.Append("'" + Fila["PARTIDA_ID"].ToString() + "', ");
                    Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "', ");
                    Mi_SQL.Append("GETDATE())");
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al ejecutar el alta de las clases. Error: [" + Ex.Message + "]");
            }
            return Operacion_Completa;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Gastos
        ///DESCRIPCIÓN          : Obtiene datos de los gastos
        ///PARAMETROS           : 
        ///CREO                 : Armando Zavala Moreno
        ///FECHA_CREO           : 09/Agosto/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Gastos(Cls_Cat_Con_Cuentas_Gastos_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
            DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
            DataTable Tabla = new DataTable();
            Boolean Segundo_Filtro = false;
            try
            {
                Mi_SQL.Append("SELECT ");
                Mi_SQL.Append("G.* ");
                Mi_SQL.Append(", (CU." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " +'-'+ CU." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ") AS CUENTA");
                Mi_SQL.Append(", (PE." + Cat_Sap_Partidas_Especificas.Campo_Clave + " +'-'+ PE." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ") AS PARTIDA");
                Mi_SQL.Append(" FROM " + Cat_Con_Cuentas_Gastos.Tabla_Cat_Con_Cuentas_Gastos+" G");
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CU");
                Mi_SQL.Append(" ON G." + Cat_Con_Cuentas_Gastos.Campo_Cuenta_Contable_Id + "= CU." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PE");
                Mi_SQL.Append(" ON G." + Cat_Con_Cuentas_Gastos.Campo_Partida_ID + "= PE." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);

                if (!String.IsNullOrEmpty(Negocio.P_Gasto_Id))
                {
                    if (Segundo_Filtro == true) { Mi_SQL.Append(" AND "); }
                    else { Mi_SQL.Append(" WHERE "); }
                    Mi_SQL.Append("G." + Cat_Con_Cuentas_Gastos.Campo_Id_Gasto + "='" + Negocio.P_Gasto_Id + "'");
                }
                if (!String.IsNullOrEmpty(Negocio.P_Cuenta_Contable_Id))
                {
                    if (Segundo_Filtro == true) { Mi_SQL.Append(" AND "); }
                    else { Mi_SQL.Append(" WHERE "); }
                    Mi_SQL.Append("G." + Cat_Con_Cuentas_Gastos.Campo_Cuenta_Contable_Id + "='" + Negocio.P_Cuenta_Contable_Id + "'");
                }
                if (!String.IsNullOrEmpty(Negocio.P_Estatus))
                {
                    if (Segundo_Filtro == true) { Mi_SQL.Append(" AND "); }
                    else { Mi_SQL.Append(" WHERE "); }
                    Mi_SQL.Append("G." + Cat_Con_Cuentas_Gastos.Campo_Estatus + "='" + Negocio.P_Estatus + "'");
                }
                if (!String.IsNullOrEmpty(Negocio.P_Partida_ID))
                {
                    if (Segundo_Filtro == true) { Mi_SQL.Append(" AND "); }
                    else { Mi_SQL.Append(" WHERE "); }
                    Mi_SQL.Append("G." + Cat_Con_Cuentas_Gastos.Campo_Partida_ID + "='" + Negocio.P_Partida_ID + "'");
                }

                Mi_SQL.Append(" ORDER BY " + Cat_Con_Cuentas_Gastos.Campo_Id_Gasto + " ASC");


                Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds != null)
                {
                    Tabla = Ds.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de los años. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Tabla;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Gastos_Detalle
        ///DESCRIPCIÓN          : Obtiene datos de los gastos
        ///PARAMETROS           : 
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 09/Septiembre/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Gastos_Detalle(Cls_Cat_Con_Cuentas_Gastos_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
            DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
            DataTable Tabla = new DataTable();
            Boolean Segundo_Filtro = false;
            try
            {
                Mi_SQL.Append("SELECT ");
                Mi_SQL.Append("G.*");
                Mi_SQL.Append(",PE." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS CLAVE");
                Mi_SQL.Append(",PE." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA");
                Mi_SQL.Append(" FROM " + Cat_Con_Cuentas_Gastos_Detalles.Tabla_Cat_Con_Cuentas_Gastos_Detalles+" G");
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PE");
                Mi_SQL.Append(" ON G." + Cat_Con_Cuentas_Gastos_Detalles.Campo_Partida_ID + "= PE." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);

                if (!String.IsNullOrEmpty(Negocio.P_Gasto_Id))
                {
                    if (Segundo_Filtro == true) { Mi_SQL.Append(" AND "); }
                    else { Mi_SQL.Append(" WHERE "); }
                    Mi_SQL.Append("G." + Cat_Con_Cuentas_Gastos_Detalles.Campo_Id_Gasto + "='" + Negocio.P_Gasto_Id + "'");
                }
                if (!String.IsNullOrEmpty(Negocio.P_Partida_ID))
                {
                    if (Segundo_Filtro == true) { Mi_SQL.Append(" AND "); }
                    else { Mi_SQL.Append(" WHERE "); }
                    Mi_SQL.Append("G." + Cat_Con_Cuentas_Gastos_Detalles.Campo_Partida_ID + "='" + Negocio.P_Partida_ID + "'");
                }
                Mi_SQL.Append(" ORDER BY " + Cat_Con_Cuentas_Gastos_Detalles.Campo_Id_Gasto + " ASC");
                Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds != null)
                {
                    Tabla = Ds.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de los años. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Tabla;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Elimina_Gasto
        ///DESCRIPCIÓN          : consulta para eliminar los datos de las clases
        ///PARAMETROS           1 Negocio: conexion con la capa de negocios
        ///CREO                 : Armando Zavala Morneo
        ///FECHA_CREO           : 09/Agosto/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static Boolean Elimina_Gasto(Cls_Cat_Con_Cuentas_Gastos_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
            Boolean Operacion_Completa = false;//Estado de la operacion.
            Boolean Segundo_Filtro = false;

            try
            {
                Mi_SQL.Append("DELETE " + Cat_Con_Cuentas_Gastos.Tabla_Cat_Con_Cuentas_Gastos);

                if (!String.IsNullOrEmpty(Negocio.P_Gasto_Id))
                {
                    if (Segundo_Filtro == true) { Mi_SQL.Append(" AND "); }
                    else { Mi_SQL.Append(" WHERE "); }
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Id_Gasto + "='" + Negocio.P_Gasto_Id + "'");
                }
                if (!String.IsNullOrEmpty(Negocio.P_Cuenta_Contable_Id))
                {
                    if (Segundo_Filtro == true) { Mi_SQL.Append(" AND "); }
                    else { Mi_SQL.Append(" WHERE "); }
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Cuenta_Contable_Id + "='" + Negocio.P_Cuenta_Contable_Id + "'");
                }
                if (!String.IsNullOrEmpty(Negocio.P_Estatus))
                {
                    if (Segundo_Filtro == true) { Mi_SQL.Append(" AND "); }
                    else { Mi_SQL.Append(" WHERE "); }
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Estatus + "='" + Negocio.P_Estatus + "'");
                }

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al ejecutar el alta de las clases. Error: [" + Ex.Message + "]");
            }
            return Operacion_Completa;
        }
        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Actualiza_Pae_Det_Etapas
        ///DESCRIPCIÓN: Actualiza en la Base de Datos un Registro de PAE_DET_ETAPAS
        ///PARAMETROS: Pae_Etapas, instancia de la capa de negocios
        ///CREO: Armando Zavala Moreno.
        ///FECHA_CREO: 09/Marzo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        internal static Boolean Actualiza_Gasto(Cls_Cat_Con_Cuentas_Gastos_Negocio Negocio)
        {
            Boolean Operacion_Completa = false;//Estado de la operacion.
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
            try
            {
                Mi_SQL.Append("UPDATE " + Cat_Con_Cuentas_Gastos.Tabla_Cat_Con_Cuentas_Gastos);
                Mi_SQL.Append(" SET ");

                if (!String.IsNullOrEmpty(Negocio.P_Cuenta_Contable_Id))
                {
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Cuenta_Contable_Id + " = '" + Negocio.P_Cuenta_Contable_Id + "', ");
                }
                if (!String.IsNullOrEmpty(Negocio.P_Descripcion))
                {
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Descripcion + " = '" + Negocio.P_Descripcion + "', ");
                }
                if (!String.IsNullOrEmpty(Negocio.P_Estatus))
                {
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Estatus+ " = '" + Negocio.P_Estatus + "', ");
                }
                if (!String.IsNullOrEmpty(Negocio.P_Concepto))
                {
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Concepto+ " = '" + Negocio.P_Concepto + "', ");
                }
                if (!String.IsNullOrEmpty(Negocio.P_Partida_ID))
                {
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Partida_ID + " = '" + Negocio.P_Partida_ID + "', ");
                }
                Mi_SQL.Append( Cat_Con_Cuentas_Gastos.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado.ToUpper() + "', ");
                Mi_SQL.Append( Cat_Con_Cuentas_Gastos.Campo_Fecha_Modifico + " = GETDATE() ");
                Mi_SQL.Append("WHERE " + Cat_Con_Cuentas_Gastos.Campo_Id_Gasto + " = " + Negocio.P_Gasto_Id);
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                //actualizar detalles 
                Mi_SQL.Length = 0;
                Mi_SQL.Append("DELETE " + Cat_Con_Cuentas_Gastos_Detalles.Tabla_Cat_Con_Cuentas_Gastos_Detalles);
                Mi_SQL.Append(" WHERE " + Cat_Con_Cuentas_Gastos_Detalles.Campo_Id_Gasto + "='" + Negocio.P_Gasto_Id + "'");
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                
                foreach (DataRow Fila in Negocio.P_Dt_Detalles.Rows)
                {
                    Mi_SQL.Length = 0;
                    Mi_SQL.Append("INSERT INTO " + Cat_Con_Cuentas_Gastos_Detalles.Tabla_Cat_Con_Cuentas_Gastos_Detalles + "(");
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Id_Gasto + ", ");
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Partida_ID + ", ");
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Cat_Con_Cuentas_Gastos.Campo_Fecha_Creo + ") VALUES( ");
                    Mi_SQL.Append("'" + Negocio.P_Gasto_Id + "', ");
                    Mi_SQL.Append("'" + Fila["PARTIDA_ID"].ToString() + "', ");
                    Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "', ");
                    Mi_SQL.Append("GETDATE())");
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar Actualizar el registro de la Cuenta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Operacion_Completa;
        }
    }
}
