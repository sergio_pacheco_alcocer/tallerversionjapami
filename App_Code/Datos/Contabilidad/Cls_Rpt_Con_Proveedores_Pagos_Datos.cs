﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Reporte_Proveedores_Pagos.Negocio;

namespace JAPAMI.Reporte_Proveedores_Pagos.Datos
{
    public class Cls_Rpt_Con_Proveedores_Pagos_Datos
    {
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Proveedores_Pago
        /// DESCRIPCION: 
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : Armando Zavala Moreno
        /// FECHA_CREO : 19-Abril-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Proveedores_Pago(Cls_Rpt_Con_Proveedores_Pagos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            Boolean Segunda_Consulta = false;
            try
            {
                Mi_SQL.Append("SELECT ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Fecha_Pago + " AS FECHA_DE_PAGO, ");
                Mi_SQL.Append("ISNULL(CUENTA.CUENTA,' ') AS CUENTA_ACREEDOR, ");
                Mi_SQL.Append("ISNULL(CUENTA1.CUENTA,' ') AS CUENTA_DEUDOR, ");
                Mi_SQL.Append("ISNULL(CUENTA2.CUENTA,' ') AS CUENTA_PROVEEDOR, ");
                Mi_SQL.Append("ISNULL(CUENTA3.CUENTA,' ') AS CUENTA_NOMINA, ");
                Mi_SQL.Append("ISNULL(CUENTA4.CUENTA,' ') AS CUENTA_PREDIAL, ");
                Mi_SQL.Append("ISNULL(CUENTA5.CUENTA,' ') AS CUENTA_CONTRATISTA, ");
                Mi_SQL.Append("ISNULL(CUENTA6.CUENTA,' ') AS CUENTA_JUDICIAL, ");

                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Autoriza_Rechaza_Ejercido + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Solicitud_Pago + ", ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Usuario_Creo + ", ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario);

                Mi_SQL.Append(" FROM ");
                Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);
                Mi_SQL.Append(" ON " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Solicitud_Pago + "=" + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles);
                Mi_SQL.Append(" ON " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Solicitud_Pago + "=" + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + "." + Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores);
                Mi_SQL.Append(" ON " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + "=" + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_SQL.Append(" ON " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + "=" + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);

                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTA");
                Mi_SQL.Append(" ON " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Acreedor_ID + "=CUENTA." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTA1");
                Mi_SQL.Append(" ON " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Deudor_ID + "=CUENTA1." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID); 
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTA2");
                Mi_SQL.Append(" ON " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID + "=CUENTA2." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTA3");
                Mi_SQL.Append(" ON " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Nomina_ID + "=CUENTA3." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTA4");
                Mi_SQL.Append(" ON " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Predial_ID + "=CUENTA4." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTA5");
                Mi_SQL.Append(" ON " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Contratista_ID + "=CUENTA5." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTA6");
                Mi_SQL.Append(" ON " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta_Judicial_ID + "=CUENTA6." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);


                if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID))
                {
                    Mi_SQL.Append(" WHERE " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + "=" + Datos.P_Proveedor_ID);
                    Segunda_Consulta = true;
                }
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicial) && !String.IsNullOrEmpty(Datos.P_Fecha_Final))
                {
                    if (Segunda_Consulta)
                    {
                        Mi_SQL.Append(" AND ");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE ");
                    }
                    Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Fecha_Pago + " BETWEEN " + "'" + Datos.P_Fecha_Inicial + "' AND '" + Datos.P_Fecha_Final + "'");
                }
                else
                {
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicial))
                    {
                        if (Segunda_Consulta)
                        {
                            Mi_SQL.Append(" AND ");
                        }
                        else
                        {
                            Mi_SQL.Append(" WHERE ");
                        }
                        Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Fecha_Pago + ">='" + Datos.P_Fecha_Inicial + "'");
                        Segunda_Consulta = true;
                    }

                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Final))
                    {
                        if (Segunda_Consulta)
                        {
                            Mi_SQL.Append(" AND ");
                        }
                        else
                        {
                            Mi_SQL.Append(" WHERE ");
                        }
                        Mi_SQL.Append(Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Fecha_Pago + "<='" + Datos.P_Fecha_Final + "'");
                        Segunda_Consulta = true;
                    }
                }
                
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
    }
}
