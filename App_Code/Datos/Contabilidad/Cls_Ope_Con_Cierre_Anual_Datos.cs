﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Cierre_Anual.Negocio;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Cierre_Mensual.Negocio;

namespace JAPAMI.Cierre_Anual.Datos
{
    public class Cls_Ope_Con_Cierre_Anual_Datos
    {
        #region (Metodos Externos)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_Cuentas_Contables
        /// DESCRIPCION : Consulta las Cuentas Contables que estan dadas de alta en la BD
        ///               con todos sus datos
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 24/Octubre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Datos_Cuentas_Contables(Cls_Ope_Con_Cierre_Anual_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta de las Cuentas Contables

            try
            {
                Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + ".*, ";
                Mi_SQL += Cat_Con_Niveles.Tabla_Cat_Con_Niveles + "." + Cat_Con_Niveles.Campo_Descripcion + " AS Nivel";
                Mi_SQL += " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " LEFT JOIN " + Cat_Con_Niveles.Tabla_Cat_Con_Niveles;
                Mi_SQL += " ON " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Nivel_ID + " = " + Cat_Con_Niveles.Tabla_Cat_Con_Niveles + "." + Cat_Con_Niveles.Campo_Nivel_ID;

                if (Datos.P_Cuenta_Contable_Rango == null)
                    Datos.P_Cuenta_Contable_Rango = false;

                if (Datos.P_Cuenta_Contable_Rango == true)
                {
                    if (Datos.P_Cuenta_Final == null)
                    {
                        Mi_SQL += " WHERE (" + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " > '" + Datos.P_Cuenta_Inicial + "')";
                        if (!String.IsNullOrEmpty(Datos.P_Afectable))
                        {
                            Mi_SQL += " AND "+ Cat_Con_Cuentas_Contables.Campo_Afectable+"='"+Datos.P_Afectable+"'";
                        }
                    }
                    if (!String.IsNullOrEmpty(Datos.P_Cuenta_Final))
                    {
                        Mi_SQL += " WHERE (" + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " >= '" + Datos.P_Cuenta_Inicial + "' AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " <= '" + Datos.P_Cuenta_Final + "')";
                        if (!String.IsNullOrEmpty(Datos.P_Afectable))
                        {
                            Mi_SQL += " AND " + Cat_Con_Cuentas_Contables.Campo_Afectable + "='" + Datos.P_Afectable + "'";
                        }
                    }
                    
                }
                else if (!String.IsNullOrEmpty(Datos.P_Cuenta_Inicial) && Datos.P_Cuenta_Contable_Rango == false)
                {
                    Mi_SQL += " WHERE (" + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " = '" + Datos.P_Cuenta_Inicial + "')";
                    if (!String.IsNullOrEmpty(Datos.P_Afectable))
                    {
                        Mi_SQL += " AND " + Cat_Con_Cuentas_Contables.Campo_Afectable + "='" + Datos.P_Afectable + "'";
                    }
                }

                Mi_SQL += " ORDER BY " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_Cuentas_Contables_Cierre
        /// DESCRIPCION : Consulta las Cuentas Contables que estan dadas de alta en la BD
        ///               con todos sus datos
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 24/Octubre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Datos_Cuentas_Contables_Cierre(Cls_Ope_Con_Cierre_Anual_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta de las Cuentas Contables

            try
            {
                Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta + ",LTRIM(RTRIM(" + Cat_Con_Cuentas_Contables.Campo_Cuenta + "+' - '+SUBSTRING(" + Cat_Con_Cuentas_Contables.Campo_Descripcion + ",0,80))) AS DESCRIPCION ";
                Mi_SQL += " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                if (Datos.P_Cuenta_Contable_Rango == null)
                    Datos.P_Cuenta_Contable_Rango = false;

                if (Datos.P_Cuenta_Contable_Rango == true)
                {
                    if (Datos.P_Cuenta_Final == null)
                    {
                        Mi_SQL += " WHERE (" + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " > '" + Datos.P_Cuenta_Inicial + "')";
                        if (!String.IsNullOrEmpty(Datos.P_Afectable))
                        {
                            Mi_SQL += " AND " + Cat_Con_Cuentas_Contables.Campo_Afectable + "='" + Datos.P_Afectable + "'";
                        }
                        if (!String.IsNullOrEmpty(Datos.P_Nivel_ID))
                        {
                            Mi_SQL += " AND " + Cat_Con_Cuentas_Contables.Campo_Nivel_ID + "='" + Datos.P_Nivel_ID + "'";
                        }
                    }
                    if (!String.IsNullOrEmpty(Datos.P_Cuenta_Final))
                    {
                        Mi_SQL += " WHERE (" + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " >= '" + Datos.P_Cuenta_Inicial + "' AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " <= '" + Datos.P_Cuenta_Final + "')";
                        if (!String.IsNullOrEmpty(Datos.P_Afectable))
                        {
                            Mi_SQL += " AND " + Cat_Con_Cuentas_Contables.Campo_Afectable + "='" + Datos.P_Afectable + "'";
                        }
                        if (!String.IsNullOrEmpty(Datos.P_Nivel_ID))
                        {
                            Mi_SQL += " AND " + Cat_Con_Cuentas_Contables.Campo_Nivel_ID + "='" + Datos.P_Nivel_ID + "'";
                        }
                    }
                }
                else 
                {
                    if (!String.IsNullOrEmpty(Datos.P_Cuenta_Inicial) && Datos.P_Cuenta_Contable_Rango == false)
                    {
                        Mi_SQL += " WHERE (" + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " = '" + Datos.P_Cuenta_Inicial + "')";
                        if (!String.IsNullOrEmpty(Datos.P_Afectable))
                        {
                            Mi_SQL += " AND " + Cat_Con_Cuentas_Contables.Campo_Afectable + "='" + Datos.P_Afectable + "'";
                        }
                        if (!String.IsNullOrEmpty(Datos.P_Nivel_ID))
                        {
                            Mi_SQL += " AND " + Cat_Con_Cuentas_Contables.Campo_Nivel_ID + "='" + Datos.P_Nivel_ID + "'";
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(Datos.P_Afectable))
                        {
                            Mi_SQL += " WHERE " + Cat_Con_Cuentas_Contables.Campo_Afectable + "='" + Datos.P_Afectable + "'";
                        }
                        if (!String.IsNullOrEmpty(Datos.P_Nivel_ID))
                        {
                            Mi_SQL += " WHERE " + Cat_Con_Cuentas_Contables.Campo_Nivel_ID + "='" + Datos.P_Nivel_ID + "'";
                        }
                    }
                }
                Mi_SQL += " ORDER BY " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " ASC";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        #endregion

        #region (Alta - Modificar - Eliminar)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Cierre_Mensual
        /// DESCRIPCION : 1.Consulta el último ID dado de alta en la tabla.
        ///               2. Da de alta el nuevo registro con los datos proporcionados por
        ///                  el usuario.
        /// PARAMETROS  : Datos: Almacena los datos a insertarse en la BD.
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 25/Octubre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Alta_Cierre_Mensual(Cls_Ope_Con_Cierre_Anual_Negocio Datos)
        {
            String Mi_SQL;   //Variable de Consulta para la Alta del de una Nueva Mascara
            Object Parametros_ID; //Variable que contendrá el ID de la consulta

            try
            {
                //Busca el maximo ID de la tabla Parametros.
                Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Con_Cierre_Anual.Campo_No_Cierre_Anual + "),'00000') ";
                Mi_SQL = Mi_SQL + "FROM " + Ope_Con_Cierre_Anual.Tabla_Ope_Con_Cierre_Anual;
                Parametros_ID = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                if (Convert.IsDBNull(Parametros_ID)) //Si no existen valores en la tabla, asigna el primer valor manualmente.
                {
                    Datos.P_No_Cierre_Anual = "00001";
                }
                else // Si ya existen registros, toma el valor maximo y le suma 1 para el nuevo registro.
                {
                    Datos.P_No_Cierre_Anual = String.Format("{0:00000}", Convert.ToInt32(Parametros_ID) + 1);
                }
                //Da de Alta los datos del Nuevo Parametro con los datos proporcionados por el usuario.
                Mi_SQL = "INSERT INTO " + Ope_Con_Cierre_Anual.Tabla_Ope_Con_Cierre_Anual + " (";
                Mi_SQL += Ope_Con_Cierre_Anual.Campo_Anio + ", ";
                Mi_SQL += Ope_Con_Cierre_Anual.Campo_Cuenta_Contable_ID + ", ";
                Mi_SQL += Ope_Con_Cierre_Anual.Campo_Cuenta_Contable_ID_Fin + ", ";
                Mi_SQL += Ope_Con_Cierre_Anual.Campo_Cuenta_Contable_ID_Inicio + ", ";
                Mi_SQL += Ope_Con_Cierre_Anual.Campo_Descripcion + ", ";
                Mi_SQL += Ope_Con_Cierre_Anual.Campo_Diferencia + ", ";
                Mi_SQL += Ope_Con_Cierre_Anual.Campo_Fecha_Creo + ", ";
                Mi_SQL += Ope_Con_Cierre_Anual.Campo_No_Cierre_Anual + ", ";
                Mi_SQL += Ope_Con_Cierre_Anual.Campo_Total_Debe + ", ";
                Mi_SQL += Ope_Con_Cierre_Anual.Campo_Total_Haber + ", ";
                Mi_SQL += Ope_Con_Cierre_Anual.Campo_Usuario_Creo + ") VALUES ('";
                Mi_SQL += Datos.P_Anio + "', '";
                Mi_SQL += Datos.P_Cuenta_Contable_ID + "', '";
                Mi_SQL += Datos.P_Cuenta_Contable_ID_Fin + "', '";
                Mi_SQL += Datos.P_Cuenta_Contable_ID_Inicio + "', '";
                Mi_SQL += Datos.P_Descripcion + "', ";
                Mi_SQL += Datos.P_Diferencia + ", ";
                Mi_SQL += "GETDATE(), '";
                Mi_SQL += Datos.P_No_Cierre_Anual + "', ";
                Mi_SQL += Datos.P_Total_Debe + ", ";
                Mi_SQL += Datos.P_Total_Haber + ", '";
                Mi_SQL += Datos.P_Usuario_Creo + "')";

                //Manda Mi_SQL para ser procesada por ORACLE.
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Obtener_Debe_Haber_Cuentas
        /// DESCRIPCION : Obtiene el total del debe y el haber del rango de cuentas seleccionado
        /// PARAMETROS  : 
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 26/Octubre/2011
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static Decimal Obtener_Debe_Haber_Cuentas(DataRow Registro, String Anio)
        {
            try
            {
                Decimal Saldo = 0;   //Almacena el total del debe y el haber de las cuentas seleccionadas.
                Cls_Ope_Con_Polizas_Negocio Rs_Polizas = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexion con la capa de negocios.
                DataTable Dt_Saldo = null;    //Almacena los datos de la consulta.
                String Fecha = "";
                Rs_Polizas.P_Cuenta_Contable_ID = Registro["CUENTA_CONTABLE_ID"].ToString();
                Rs_Polizas.P_Fecha_Inicial = "31/12/" + Anio;
                Fecha = Rs_Polizas.Consulta_Fecha_Poliza_Anual();
                if (Fecha != "")
                {
                    Rs_Polizas.P_Fecha_Inicial = Fecha;
                    Dt_Saldo = Rs_Polizas.Consulta_Saldo_Consecutivo_Anual();
                    Saldo = Convert.ToDecimal(Dt_Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Saldo].ToString());
                }
                else
                {
                    Saldo = 0;
                }
                return Saldo;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }


        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cierre_Anual
        /// DESCRIPCION : Efectuar la operacion del cierre anual en una transaccion
        /// PARAMETROS  : Datos: Variable de la capa de negocios
        /// CREO        : Noe Mosqueda Valadez
        /// FECHA_CREO  : 04/Mayo/2013 13:00
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static void Cierre_Anual(Cls_Ope_Con_Cierre_Anual_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL = null;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            DataTable Dt_Cuentas = new DataTable(); //Tabla para las cuentas contables
            Decimal Totales = 0;   //Recibe el total del debe y el haber del rango de cuentas seleccionadas.
            Decimal Total_Haber = 0;
            Decimal TOTAL_GENERAL = 0;
            Decimal Total_Debe = 0;
            Decimal Total_Poliza = 0;
            Decimal Total_Cuenta = 0;
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuentas_Contables_Negocio = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //variable para la capa de negocios de las cuentas contables
            DataTable Dt_Cuenta_ID = new DataTable(); //Tabla para los ID de la cuenta contable
            int Partidas = 0; //Contador de las partidas de la poliza
            DataTable Dt_Partidas_Polizas = new DataTable(); //tabla para las partidas de la poliza
            DataRow Rows; //Renglon
            String[] Datos_Poliza; //variable para los datos generados por la poliza
            Cls_Ope_Con_Polizas_Negocio Polizas_Negocio = new Cls_Ope_Con_Polizas_Negocio(); //Variable para la capa de negocios de las polizas
            DataTable Dt_Cuentas_Resultado = new DataTable();
            int Cont_Elementos = 0; //variable para el contador
            object aux; //Objeto auxliar para las consultas
            Cls_Ope_Con_Cierre_Mensual_Negocio Cierre_Mensual_Negocio = new Cls_Ope_Con_Cierre_Mensual_Negocio(); //variable para la capa de negocios
            DataTable Dt_Cuentas_Afectables = new DataTable(); //tabla para las cuentas contables afectables
            DataTable Dt_Cierre_Mensual = new DataTable(); //Tabla para el cierre mensual
            DataTable Dt_Movimientos = new DataTable(); //tabla para los movimientos
            decimal Debe = 0; //variable para el debe
            decimal Haber = 0; //variable para el haber
            DataTable Dt_Cuentas_Cierre_Mensual = new DataTable(); //Tabla para consultar si ya se ha realizado el cierre mensual 13

            try
            {
                #region "PARTIDAS POLIZAS"
                //Ejecutar consulta de las cuentas contables
                Dt_Cuentas = Consulta_Datos_Cuentas_Contables(Datos);

                //columnas del datatable
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add("MOMENTO_FINAL", typeof(System.String));
                Dt_Partidas_Polizas.Columns.Add("Nombre_Cuenta", typeof(System.String));

                //Ciclo para el barrido de las cuentas contables
                foreach (DataRow Registro in Dt_Cuentas.Rows)
                {
                    Totales = Obtener_Debe_Haber_Cuentas(Registro, Datos.P_Anio);
                    if (Totales != 0)
                    {
                        Partidas = Partidas + 1;
                        if (Totales > 0)
                        {
                            TOTAL_GENERAL = TOTAL_GENERAL + Totales;
                            DataRow row = Dt_Partidas_Polizas.NewRow();
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partidas;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
                            row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString();
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CIERRE EJERCICIO " + Datos.P_Anio;
                            if (Registro[Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta].ToString().ToUpper() == "ACREEDOR")
                            {
                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Totales;
                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                                Total_Debe = Total_Debe + Totales;
                                Total_Poliza = Total_Poliza + Totales;
                            }
                            else
                            {
                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Totales;
                                Total_Haber = Total_Haber + Totales;
                            }
                            row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = null;
                            row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = null;
                            row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = null;
                            row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = null;
                            row["MOMENTO_INICIAL"] = "";
                            row["MOMENTO_FINAL"] = "";
                            row["Nombre_Cuenta"] = Registro[Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString();
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                        else
                        {
                            DataRow row = Dt_Partidas_Polizas.NewRow();
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partidas;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
                            row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString();
                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CIERRE EJERCICIO " + Datos.P_Anio;
                            if (Registro[Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta].ToString().ToUpper() == "ACREEDOR")
                            {
                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = (Totales * -1);
                                Total_Haber = Total_Haber + (Totales * -1);
                            }
                            else
                            {
                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = (Totales * -1);
                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                                Total_Debe = Total_Debe + Totales;
                                Total_Poliza = Total_Poliza + (Totales * -1);
                            }
                            row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = null;
                            row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = null;
                            row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = null;
                            row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = null;
                            row["MOMENTO_INICIAL"] = "";
                            row["MOMENTO_FINAL"] = "";
                            row["Nombre_Cuenta"] = Registro[Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString();
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                }
                


                //Consultar el ID de las cuentas contables
                Cuentas_Contables_Negocio.P_Cuenta = Datos.P_Cuenta_Nueva;
                Dt_Cuenta_ID = Cuentas_Contables_Negocio.Consulta_Cuentas_Contables();
                Total_Cuenta = Total_Debe - Total_Haber;
                Partidas++;

                Rows = Dt_Partidas_Polizas.NewRow();
                Rows[Ope_Con_Polizas_Detalles.Campo_Partida] = Partidas;
                Rows[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Cuenta_ID.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString();
                Rows[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Datos.P_Cuenta_Nueva;
                Rows[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CIERRE EJERCICIO " + Datos.P_Anio;
                if (Total_Cuenta < 0)
                {
                    Rows[Ope_Con_Polizas_Detalles.Campo_Debe] = (Total_Cuenta * -1);
                    Rows[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                }
                else
                {
                    Rows[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                    Rows[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Cuenta;
                }
                Rows[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = null;
                Rows[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = null;
                Rows[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = null;
                Rows[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = null;
                Rows["MOMENTO_INICIAL"] = "";
                Rows["MOMENTO_FINAL"] = "";
                Rows["Nombre_Cuenta"] = "";
                Dt_Partidas_Polizas.Rows.Add(Rows); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                #endregion


                //Consultar los ID de las cuentas contables inicial y final
                Dt_Cuentas_Resultado = new DataTable();
                Cuentas_Contables_Negocio.P_Cuenta = Datos.P_Cuenta_Final;
                Dt_Cuentas_Resultado = Cuentas_Contables_Negocio.Consulta_Cuentas_Contables();
                Datos.P_Cuenta_Contable_ID_Fin = Dt_Cuentas_Resultado.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString();
                Dt_Cuentas_Resultado = new DataTable();
                Cuentas_Contables_Negocio.P_Cuenta = Datos.P_Cuenta_Inicial;
                Dt_Cuentas_Resultado = Cuentas_Contables_Negocio.Consulta_Cuentas_Contables();
                Datos.P_Cuenta_Contable_ID_Inicio = Dt_Cuentas_Resultado.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString();






                //Iniciar la transaccion
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos            
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos



                #region "POLIZA CIERRE ANUAL"
                //Dar de alta la poliza del cierre
                Polizas_Negocio.P_Empleado_ID = Datos.P_Empleado_ID;
                Polizas_Negocio = new Cls_Ope_Con_Polizas_Negocio();
                Polizas_Negocio.P_Tipo_Poliza_ID = "00003";
                //Polizas_Negocio.P_Mes_Ano = "12"+Txt_Anio.Text.Substring(2,2);
                Polizas_Negocio.P_Mes_Ano = "13" + Datos.P_Anio.Substring(2, 2);
                Polizas_Negocio.P_Fecha_Poliza = new DateTime(Convert.ToInt32(Datos.P_Anio.Trim()), 12, 31);
                Polizas_Negocio.P_Concepto = "CIERRE EJERCICIO " + Datos.P_Anio;
                Polizas_Negocio.P_Total_Debe = Convert.ToDouble(Total_Poliza);
                Polizas_Negocio.P_Total_Haber = Convert.ToDouble(Total_Poliza);
                Polizas_Negocio.P_No_Partida = Convert.ToInt32(Partidas);
                Polizas_Negocio.P_Nombre_Usuario = Datos.P_Usuario_Creo;
                Polizas_Negocio.P_Dt_Detalles_Polizas = Dt_Partidas_Polizas;
                Polizas_Negocio.P_Empleado_ID_Creo = Datos.P_Empleado_ID;
                Polizas_Negocio.P_Empleado_ID_Autorizo = Datos.P_Empleado_ID;
                Polizas_Negocio.P_Prefijo = "";
                Polizas_Negocio.P_Cmmd = Comando_SQL;
                Datos_Poliza = Polizas_Negocio.Alta_Poliza(); //Da de alta los datos de la Póliza proporcionados por el usuario en la BD
                #endregion


                #region "CIERRE ANUAL"
                //Ciclo para ejecutar el cierre anual
                foreach (DataRow Fila in Dt_Partidas_Polizas.Rows)
                {
                    Datos.P_Cuenta_Contable_ID = Fila[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
                    Datos.P_Diferencia = Math.Abs(Convert.ToDouble(Fila[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) - Convert.ToDouble(Fila[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()));
                    Datos.P_Total_Debe = Convert.ToDouble(Fila[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                    Datos.P_Total_Haber = Convert.ToDouble(Fila[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());

                    //Busca el maximo ID de la tabla Parametros.
                    Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Con_Cierre_Anual.Campo_No_Cierre_Anual + "),'00000') ";
                    Mi_SQL = Mi_SQL + "FROM " + Ope_Con_Cierre_Anual.Tabla_Ope_Con_Cierre_Anual;

                    //Ejecutar consulta
                    Comando_SQL.CommandText = Mi_SQL;
                    aux = Comando_SQL.ExecuteScalar();

                    if (Convert.IsDBNull(aux)) //Si no existen valores en la tabla, asigna el primer valor manualmente.
                    {
                        Datos.P_No_Cierre_Anual = "00001";
                    }
                    else // Si ya existen registros, toma el valor maximo y le suma 1 para el nuevo registro.
                    {
                        Datos.P_No_Cierre_Anual = String.Format("{0:00000}", Convert.ToInt32(aux) + 1);
                    }
                    //Da de Alta los datos del Nuevo Parametro con los datos proporcionados por el usuario.
                    Mi_SQL = "INSERT INTO " + Ope_Con_Cierre_Anual.Tabla_Ope_Con_Cierre_Anual + " (";
                    Mi_SQL += Ope_Con_Cierre_Anual.Campo_Anio + ", ";
                    Mi_SQL += Ope_Con_Cierre_Anual.Campo_Cuenta_Contable_ID + ", ";
                    Mi_SQL += Ope_Con_Cierre_Anual.Campo_Cuenta_Contable_ID_Fin + ", ";
                    Mi_SQL += Ope_Con_Cierre_Anual.Campo_Cuenta_Contable_ID_Inicio + ", ";
                    Mi_SQL += Ope_Con_Cierre_Anual.Campo_Descripcion + ", ";
                    Mi_SQL += Ope_Con_Cierre_Anual.Campo_Diferencia + ", ";
                    Mi_SQL += Ope_Con_Cierre_Anual.Campo_Fecha_Creo + ", ";
                    Mi_SQL += Ope_Con_Cierre_Anual.Campo_No_Cierre_Anual + ", ";
                    Mi_SQL += Ope_Con_Cierre_Anual.Campo_Total_Debe + ", ";
                    Mi_SQL += Ope_Con_Cierre_Anual.Campo_Total_Haber + ", ";
                    Mi_SQL += Ope_Con_Cierre_Anual.Campo_Usuario_Creo + ") VALUES ('";
                    Mi_SQL += Datos.P_Anio + "', '";
                    Mi_SQL += Datos.P_Cuenta_Contable_ID + "', '";
                    Mi_SQL += Datos.P_Cuenta_Contable_ID_Fin + "', '";
                    Mi_SQL += Datos.P_Cuenta_Contable_ID_Inicio + "', '";
                    Mi_SQL += Datos.P_Descripcion + "', ";
                    Mi_SQL += Datos.P_Diferencia + ", ";
                    Mi_SQL += "GETDATE(), '";
                    Mi_SQL += Datos.P_No_Cierre_Anual + "', ";
                    Mi_SQL += Datos.P_Total_Debe + ", ";
                    Mi_SQL += Datos.P_Total_Haber + ", '";
                    Mi_SQL += Datos.P_Usuario_Creo + "')";

                    //Ejecutar consulta
                    Comando_SQL.CommandText = Mi_SQL;
                    Comando_SQL.ExecuteNonQuery();
                }
                #endregion



                #region "CIERRE MENSUAL 13"
                //Se realiza el cierre mensual
                Cierre_Mensual_Negocio.P_Mes_Anio = "13" + Datos.P_Anio.Substring(2, 2);
                Cierre_Mensual_Negocio.P_Cmmd = Comando_SQL;
                Cierre_Mensual_Negocio.P_Anio = Datos.P_Anio;
                Cierre_Mensual_Negocio.Cierre_Mensual_Completo();
                #endregion


                //Ejecutar la transaccion
                Transaccion_SQL.Commit();
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            }
        }
        #endregion
    }
}