﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Constantes;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;


/// <summary>
/// Summary description for Cls_Rpt_Con_Historial_Reservas_Datos
/// </summary>
/// 
namespace JAPAMI.Reporte_Historail_Reservas.Datos
{
    public class Cls_Rpt_Con_Historial_Reservas_Datos
    {
        public Cls_Rpt_Con_Historial_Reservas_Datos()
        {
        }

        #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Consulta_Historial_Reservas
        ///DESCRIPCION:             Consulta historial reservas con diversos filtros
        ///PARAMETROS:              Datos: Variable de la capa de negocios
        ///CREO:                    Luis Daniel Guzmán Malagón
        ///FECHA_CREO:              18/Julio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        public static DataTable Consulta_Historial_Reservas(Cls_Rpt_Con_Historial_Reservas_Negocio negocio) {
            //Variables
            String Mi_SQL = String.Empty;
            DataTable Dt_Resultado = new DataTable();

            try {

                Mi_SQL = "SELECT " + Ope_Psp_Reservas.Campo_No_Reserva + ", ";
                Mi_SQL += Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Estatus + ", ";
                Mi_SQL += Ope_Psp_Reservas.Campo_Concepto + ", ";
                Mi_SQL += Ope_Psp_Reservas.Campo_Fecha + ", ";
                //Mi_SQL += Ope_Psp_Reservas.Campo_Dependencia_ID + ", ";
                Mi_SQL += Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ", ";
                
                //Mi_SQL += Ope_Psp_Reservas.Campo_Proyecto_Programa_ID + ", ";
                Mi_SQL += Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + "." + Cat_Com_Proyectos_Programas.Campo_Nombre + ", ";
                Mi_SQL += Ope_Psp_Reservas.Campo_Partida_ID + ", ";
                Mi_SQL += Ope_Psp_Reservas.Campo_Importe_Inicial + ", ";
                Mi_SQL += Ope_Psp_Reservas.Campo_Saldo + ", ";
                Mi_SQL += Ope_Psp_Reservas.Campo_Fte_Financimiento_ID + ", ";
                Mi_SQL += Ope_Psp_Reservas.Campo_Capitulo_ID + " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                Mi_SQL += " JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias;
                Mi_SQL += " ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Ope_Psp_Reservas.Campo_Dependencia_ID + " like " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL += " JOIN " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas;
                Mi_SQL += " ON " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + "." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + " like " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Proyecto_Programa_ID;


                //filtro de dependencia id
                if(!String.IsNullOrEmpty(negocio.P_Dependencia_Id)){
                    if (Mi_SQL.Contains("WHERE"))
                        Mi_SQL += " OR " + Ope_Psp_Reservas.Campo_Dependencia_ID + " like '" + negocio.P_Dependencia_Id.Trim() + "'";
                    else
                        Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_Dependencia_ID + " like '" + negocio.P_Dependencia_Id.Trim() + "'";
                }

                //filtro de financiamiento  id
                if (!String.IsNullOrEmpty(negocio.P_Financiamiento_Id))
                {
                    if (Mi_SQL.Contains("WHERE"))
                        Mi_SQL += " OR " + Ope_Psp_Reservas.Campo_Fte_Financimiento_ID + " like '" + negocio.P_Financiamiento_Id.Trim() + "'";
                    else
                        Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_Fte_Financimiento_ID + " like '" + negocio.P_Financiamiento_Id.Trim() + "'";
                }

                //filtro de proyectos programas
                if (!String.IsNullOrEmpty(negocio.P_Proyecto_Programas))
                {
                    if (Mi_SQL.Contains("WHERE"))
                        Mi_SQL += " OR " + Ope_Psp_Reservas.Campo_Proyecto_Programa_ID + " like '" + negocio.P_Proyecto_Programas.Trim() + "'";
                    else
                        Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_Proyecto_Programa_ID + " like '" + negocio.P_Proyecto_Programas.Trim() + "'";
                }

                //filtro de partida id
                if (!String.IsNullOrEmpty(negocio.P_Partida))
                {
                    if (Mi_SQL.Contains("WHERE"))
                        Mi_SQL += " OR " + Ope_Psp_Reservas.Campo_Partida_ID + " like '" + negocio.P_Partida.Trim() + "'";
                    else
                        Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_Partida_ID + " like '" + negocio.P_Partida.Trim() + "'";
                }

                //filtro de Capitulo id
                if (!String.IsNullOrEmpty(negocio.P_Capitulo_Id))
                {
                    if (Mi_SQL.Contains("WHERE"))
                        Mi_SQL += " OR " + Ope_Psp_Reservas.Campo_Capitulo_ID + " like '" + negocio.P_Capitulo_Id.Trim() + "'";
                    else
                        Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_Capitulo_ID + " like '" + negocio.P_Capitulo_Id.Trim() + "'";
                }
                
                //filtro de Fecha
                if (!String.IsNullOrEmpty(negocio.P_Fecha))
                {
                    if (Mi_SQL.Contains("WHERE"))
                        Mi_SQL += " OR " + Ope_Psp_Reservas.Campo_Fecha + " like '" + negocio.P_Fecha.Trim() + "'";
                    else
                        Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_Fecha + " like '" + negocio.P_Fecha.Trim() + "'";
                }

                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                return Dt_Resultado;
            
            }catch(Exception Ex){
                throw new Exception(Ex.Message, Ex);
            }

        } 
        #endregion

    }
}