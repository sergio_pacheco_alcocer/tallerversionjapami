﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Parametros_Contabilidad.Negocio;

namespace JAPAMI.Parametros_Contabilidad.Datos
{
    public class Cls_Cat_Con_Parametros_Datos
    {
        #region (Metodos Operacion)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Parametros
        /// DESCRIPCION : 1.Consulta el último ID dado de alta en la tabla.
        ///               2. Da de alta el nuevo registro con los datos proporcionados por
        ///                  el usuario.
        /// PARAMETROS  : Datos: Almacena los datos a insertarse en la BD.
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 15/Septiembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Alta_Parametros(Cls_Cat_Con_Parametros_Negocio Datos)
        {
            String Mi_SQL;   //Variable de Consulta para la Alta del de una Nueva Mascara
            Object Parametros_ID; //Variable que contendrá el ID de la consulta
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;

            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;

            try
            {

                //Busca el maximo ID de la tabla Parametros.
                Mi_SQL = "SELECT ISNULL(MAX (" + Cat_Con_Parametros.Campo_Parametro_Contabilidad_ID + "),'00000') ";
                Mi_SQL = Mi_SQL + "FROM " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros;

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Parametros_ID = Cmmd.ExecuteScalar();

                if (Convert.IsDBNull(Parametros_ID)) //Si no existen valores en la tabla, asigna el primer valor manualmente.
                {
                    Datos.P_Parametro_Contabilidad_ID = "00001";
                }
                else // Si ya existen registros, toma el valor maximo y le suma 1 para el nuevo registro.
                {
                    Datos.P_Parametro_Contabilidad_ID = String.Format("{0:00000}", Convert.ToInt32(Parametros_ID) + 1);
                }
                //Da de Alta los datos del Nuevo Parametro con los datos proporcionados por el usuario.
                Mi_SQL = "INSERT INTO " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros + " (";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Parametro_Contabilidad_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Mes_Contable + ", ";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Mascara_Cuenta_Contable + ", ";
                
                if (string.IsNullOrEmpty(Datos.P_Usuario_Autoriza_Inv) == false)
                {
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv + ", ";
                }

                if (string.IsNullOrEmpty(Datos.P_Usuario_Autoriza_Inv_2) == false)
                {
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv_2 + ", ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Fte_Municipal))
                {
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Fte_Financiamiento_ID_Ingresos + ", ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Fte_Ramo33))
                {
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Fte_Financiamiento_ID_Ramo33 + ", ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Programa_Municipal))
                {
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Programa_ID_Ingresos + ", ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Programa_Ramo33))
                {
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Programa_ID_Ramo33 + ", ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Devengado_Ing))
                {
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Devengado_Ing + ", ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Obras_Proceso))
                {
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Obras_Proceso + ", ";
                }
                if (!String.IsNullOrEmpty(Datos.P_ISR_Arrendamiento))
                {
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_ISR_Arrendamiento + ", ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Cedular_Arrendamiento))
                {
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Cedular_Arrendamiento + ", ";
                } 
                if (!String.IsNullOrEmpty(Datos.P_Honorarios_Arrendamiento))
                {
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Honorarios_Asimilables + ", ";
                }
                if (!String.IsNullOrEmpty(Datos.P_CTA_Baja_Activo))
                {
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_CTA_Baja_Activo_ID + ", ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Poliza_Baja_ID))
                {
                    Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Tipo_Poliza_Baja_ID + ", ";
                }
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Cedular_Arrendamiento_Monto + ", ";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_ISR_Arrendamiento_Monto + ", ";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Honorarios_Asimilables_Monto + ", ";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Usuario_Creo + ", ";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Fecha_Creo + ", ";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_Campo_Escondido_CONAC + ", ";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Campo_CTA_Comision_Bancaria + ") ";
                Mi_SQL = Mi_SQL + "VALUES ('" + Datos.P_Parametro_Contabilidad_ID + "', '";
                Mi_SQL = Mi_SQL + Datos.P_Mes_Contable + "', '";
                Mi_SQL = Mi_SQL + Datos.P_Mascara_Cuenta_Contable + "',";

                if (string.IsNullOrEmpty(Datos.P_Usuario_Autoriza_Inv) == false)
                {
                    Mi_SQL += "'" + Datos.P_Usuario_Autoriza_Inv + "', ";
                }

                if (string.IsNullOrEmpty(Datos.P_Usuario_Autoriza_Inv_2) == false)
                {
                    Mi_SQL += "'" + Datos.P_Usuario_Autoriza_Inv_2 + "', ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Fte_Municipal))
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Fte_Municipal.Trim() + "',";
                }

                if (!String.IsNullOrEmpty(Datos.P_Fte_Ramo33))
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Fte_Ramo33.Trim() + "',";
                }

                if (!String.IsNullOrEmpty(Datos.P_Programa_Municipal))
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Programa_Municipal.Trim() + "',";
                }

                if (!String.IsNullOrEmpty(Datos.P_Programa_Ramo33))
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Programa_Ramo33.Trim() + "',";
                }

                if (!String.IsNullOrEmpty(Datos.P_Devengado_Ing))
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Devengado_Ing.Trim() + "',";
                }

                if (!String.IsNullOrEmpty(Datos.P_Obras_Proceso))
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Obras_Proceso.Trim() + "',";
                }

                if (!String.IsNullOrEmpty(Datos.P_ISR_Arrendamiento))
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_ISR_Arrendamiento.Trim() + "',";
                }

                if (!String.IsNullOrEmpty(Datos.P_Cedular_Arrendamiento))
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Cedular_Arrendamiento.Trim() + "',";
                }

                if (!String.IsNullOrEmpty(Datos.P_Honorarios_Arrendamiento))
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Honorarios_Arrendamiento.Trim() + "',";
                }
                if (!String.IsNullOrEmpty(Datos.P_CTA_Baja_Activo))
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_CTA_Baja_Activo.Trim() + "',";
                }
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Poliza_Baja_ID))
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Tipo_Poliza_Baja_ID.Trim() + "',";
                }
                if (!String.IsNullOrEmpty(Datos.P_Cedular_Arrendamiento_Monto))
                {
                    Mi_SQL = Mi_SQL  + Datos.P_Cedular_Arrendamiento_Monto.Trim() + ",";
                }
                else
                {
                    Mi_SQL = Mi_SQL + "0,";
                }
                if (!String.IsNullOrEmpty(Datos.P_ISR_Arrendamiento_Monto))
                {
                    Mi_SQL = Mi_SQL + Datos.P_ISR_Arrendamiento_Monto.Trim() + ",";
                }
                else
                {
                    Mi_SQL = Mi_SQL + "0,";
                }
                if (!String.IsNullOrEmpty(Datos.P_Honorarios_Arrendamiento_Monto))
                {
                    Mi_SQL = Mi_SQL + Datos.P_Honorarios_Arrendamiento_Monto.Trim() + ",";
                }
                else
                {
                    Mi_SQL = Mi_SQL + "0,";
                }
                Mi_SQL = Mi_SQL + "'" + Datos.P_Usuario_Creo + "', GETDATE(),";
                if (string.IsNullOrEmpty(Datos.P_Campo_Escondido_CONAC) == false)
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Campo_Escondido_CONAC + "', ";
                }
                else
                {
                    Mi_SQL = Mi_SQL + "NULL, ";
                }

                if (string.IsNullOrEmpty(Datos.P_CTA_COMISION_BANCARIA) == false)
                {
                    Mi_SQL += "'" + Datos.P_CTA_COMISION_BANCARIA + "') ";
                }
                else
                {
                    Mi_SQL += "NULL) ";
                }
                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();

                //Ejecutar transaccion
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Parametros
        /// DESCRIPCION : 1.Consulta el último ID dado de alta en la tabla.
        ///               2.Regresa la mascara correspondiente a ese ID.
        /// PARAMETROS  : 
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 19/Septiembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Parametros()
        {
            String Mi_SQL; //Variable para almacenar la instruccion de consulta a la BD.
            try
            {
                Mi_SQL = "SELECT " + Cat_Con_Parametros.Campo_Mascara_Cuenta_Contable;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Parametros.Campo_Parametro_Contabilidad_ID + "=";
                Mi_SQL = Mi_SQL + "(SELECT MAX(" + Cat_Con_Parametros.Campo_Parametro_Contabilidad_ID + ") FROM ";
                Mi_SQL = Mi_SQL + Cat_Con_Parametros.Tabla_Cat_Con_Parametros + ")";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Parametros
        /// DESCRIPCION : 1. Modifica el Unico registro existente
        /// PARAMETROS  : Datos: Almacena los datos que van a modificarse en la BD.
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 21/Septiembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Modificar_Parametros(Cls_Cat_Con_Parametros_Negocio Datos)
        {
            String Mi_SQL;   //Variable de Consulta para la Alta del de una Nueva Mascara
            Object Parametros_ID; //Variable que contendrá el ID de la consulta
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;

            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;

            try
            {
                //Modifica los datos del Nuevo Parametro con los datos proporcionados por el usuario.
                Mi_SQL = "UPDATE " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros;
                Mi_SQL += " SET " + Cat_Con_Parametros.Campo_Mascara_Cuenta_Contable + " = '" + Datos.P_Mascara_Cuenta_Contable + "', ";
                Mi_SQL += Cat_Con_Parametros.Campo_Mes_Contable + " = '" + Datos.P_Mes_Contable + "', ";
                Mi_SQL += Cat_Con_Parametros.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Modifico + "', ";
                Mi_SQL += Cat_Con_Parametros.Campo_Fecha_Modifico + " = GETDATE(), ";
                Mi_SQL += Cat_Con_Parametros.Campo_Campo_Escondido_CONAC + " = '" + Datos.P_Campo_Escondido_CONAC + "' ";
                Mi_SQL += " WHERE " + Cat_Con_Parametros.Campo_Parametro_Contabilidad_ID + " = '00001'";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();

                //Ejecutar transaccion
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Parametros
        /// DESCRIPCION : 1. Modifica el Unico registro existente
        /// PARAMETROS  : Datos: Almacena los datos que van a modificarse en la BD.
        /// CREO        : SERGIO MANUEL GALLARDO
        /// FECHA_CREO  : 24/MARZO/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Modificar_Parametros_Autorizacion(Cls_Cat_Con_Parametros_Negocio Datos)
        {
            String Mi_SQL;   //Variable de Consulta para la Alta del de una Nueva Mascara
            Object Parametros_ID; //Variable que contendrá el ID de la consulta
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;

            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;

            try
            {
                //Modifica los datos del Nuevo Parametro con los datos proporcionados por el usuario.
                Mi_SQL = "UPDATE " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros +" SET " ;
                if (!String.IsNullOrEmpty(Datos.P_Usuario_Autoriza_Inv))
                {
                    Mi_SQL +=  Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv + " = '" + Datos.P_Usuario_Autoriza_Inv + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Usuario_Autoriza_Inv_2))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv_2 + " = '" + Datos.P_Usuario_Autoriza_Inv_2 + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Retencion_IVA))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Retencion_IVA + " = '" + Datos.P_Retencion_IVA + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Cedular))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Cedular  + " = '" + Datos.P_Cedular + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_ISR))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_ISR  + " = '" + Datos.P_ISR + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_IVA))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_IVA + " = '" + Datos.P_IVA + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Iva_Inversiones))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Iva_Inversiones + " = '" + Datos.P_Iva_Inversiones + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_CAP))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CAP + " = '" + Datos.P_CAP + "', ";
                } 
                if (!String.IsNullOrEmpty(Datos.P_ICIC))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_ICIC+ " = '" + Datos.P_ICIC+ "', ";
                } 
                if (!String.IsNullOrEmpty(Datos.P_RAPCE))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_RAPCE + " = '" + Datos.P_RAPCE + "', ";
                } 
                if (!String.IsNullOrEmpty(Datos.P_DIVO))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_DIVO + " = '" + Datos.P_DIVO + "', ";
                } 
                if (!String.IsNullOrEmpty(Datos.P_SEFUPU))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_SEFUPU + " = '" + Datos.P_SEFUPU + "', ";
                } 
                if (!String.IsNullOrEmpty(Datos.P_OBS))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_OBS+ " = '" + Datos.P_OBS+ "', ";
                } 
                if (!String.IsNullOrEmpty(Datos.P_Est_Y_Proy))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Est_Y_Proy + " = '" + Datos.P_Est_Y_Proy+ "', ";
                } 
                if (!String.IsNullOrEmpty(Datos.P_Lab_Control_C))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Lab_Control_C + " = '" + Datos.P_Lab_Control_C + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_CTA_CAP))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_CAP + " = '" + Datos.P_CTA_CAP + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_CTA_ICIC))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_ICIC + " = '" + Datos.P_CTA_ICIC + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_CTA_RAPCE))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_RAPCE + " = '" + Datos.P_CTA_RAPCE + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_CTA_DIVO))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_DIVO + " = '" + Datos.P_CTA_DIVO + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_CTA_SEFUPU))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_SEFUPU + " = '" + Datos.P_CTA_SEFUPU + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_CTA_OBS))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_OBS + " = '" + Datos.P_CTA_OBS + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_CTA_Est_Y_Proy))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_Est_Y_Proy + " = '" + Datos.P_CTA_Est_Y_Proy + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_CTA_Lab_Control_C))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_Lab_Control_C + " = '" + Datos.P_CTA_Lab_Control_C + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_CTA_Retencion_IVA))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_Retencion_IVA + " = '" + Datos.P_CTA_Retencion_IVA + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_CTA_Cedular))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_Cedular + " = '" + Datos.P_CTA_Cedular + "', ";
                }
                if (!String.IsNullOrEmpty(Datos.P_CTA_ISR))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_ISR + " = '" + Datos.P_CTA_ISR + "', ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Fte_Municipal))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Fte_Financiamiento_ID_Ingresos + " = '" + Datos.P_Fte_Municipal.Trim() + "', ";
                }
                else 
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Fte_Financiamiento_ID_Ingresos + " = NULL, ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Fte_Ramo33))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Fte_Financiamiento_ID_Ramo33 + " = '" + Datos.P_Fte_Ramo33.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Fte_Financiamiento_ID_Ramo33 + " = NULL, ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Programa_Municipal))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Programa_ID_Ingresos + " = '" + Datos.P_Programa_Municipal.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Programa_ID_Ingresos + " = NULL, ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Programa_Ramo33))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Programa_ID_Ramo33 + " = '" + Datos.P_Programa_Ramo33.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Programa_ID_Ramo33 + " = NULL, ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Devengado_Ing))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Devengado_Ing + " = '" + Datos.P_Devengado_Ing.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Devengado_Ing + " = NULL, ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Obras_Proceso))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Obras_Proceso + " = '" + Datos.P_Obras_Proceso.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Obras_Proceso + " = NULL, ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Cedular_Arrendamiento))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Cedular_Arrendamiento + " = '" + Datos.P_Cedular_Arrendamiento.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Cedular_Arrendamiento + " = NULL, ";
                }

                if (!String.IsNullOrEmpty(Datos.P_ISR_Arrendamiento))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_ISR_Arrendamiento + " = '" + Datos.P_ISR_Arrendamiento.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_ISR_Arrendamiento + " = NULL, ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Honorarios_Arrendamiento))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Honorarios_Asimilables + " = '" + Datos.P_Honorarios_Arrendamiento.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Honorarios_Asimilables + " = NULL, ";
                }
                if (!String.IsNullOrEmpty(Datos.P_ISR_Arrendamiento_Monto))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_ISR_Arrendamiento_Monto + " = '" + Datos.P_ISR_Arrendamiento_Monto.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_ISR_Arrendamiento_Monto + " = 0, ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Cedular_Arrendamiento_Monto))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Cedular_Arrendamiento_Monto + " = '" + Datos.P_Cedular_Arrendamiento_Monto.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Cedular_Arrendamiento_Monto + " = 0, ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Honorarios_Arrendamiento_Monto))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Honorarios_Asimilables_Monto + " = '" + Datos.P_Honorarios_Arrendamiento_Monto.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Honorarios_Asimilables_Monto + " = 0, ";
                }
                if (!String.IsNullOrEmpty(Datos.P_CTA_Baja_Activo))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_Baja_Activo_ID + " = '" + Datos.P_CTA_Baja_Activo.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_Baja_Activo_ID + " = NULL, ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Poliza_Baja_ID))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Tipo_Poliza_Baja_ID + " = '" + Datos.P_Tipo_Poliza_Baja_ID.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Tipo_Poliza_Baja_ID + " = NULL, ";
                }

                if (string.IsNullOrEmpty(Datos.P_CTA_COMISION_BANCARIA) == false)
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_Comision_Bancaria + " = '" + Datos.P_CTA_COMISION_BANCARIA + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_CTA_Comision_Bancaria + " = NULL, ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Cuenta_Banco_Otros))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Cta_Banco_Otros_Deudor + " = '" + Datos.P_Cuenta_Banco_Otros.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Cta_Banco_Otros_Deudor + " = NULL, ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Cuenta_Por_Pagar_Deudor))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Cta_Por_Pagar_Deudor + " = '" + Datos.P_Cuenta_Por_Pagar_Deudor.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Cta_Por_Pagar_Deudor + " = NULL, ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Caja_Chica))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Tipo_Caja_Chica + " = '" + Datos.P_Tipo_Caja_Chica.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Tipo_Caja_Chica + " = NULL, ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Monto_Caja_Chica))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Monto_Caja_Chica + " = " + Datos.P_Monto_Caja_Chica.Trim() + ", ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Monto_Caja_Chica + " = 0, ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud_Revolvente))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Tipo_Solicitud_Fondo_Revolvente + " = '" + Datos.P_Tipo_Solicitud_Revolvente.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Tipo_Solicitud_Fondo_Revolvente + " = NULL, ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Deudor_Caja_Chica))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Deudor_Caja_Chica + " = '" + Datos.P_Deudor_Caja_Chica.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Deudor_Caja_Chica + " = NULL, ";
                } 
                if (!String.IsNullOrEmpty(Datos.P_Dias_Comprobacion))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Dias_Comprobacion + " = '" + Datos.P_Dias_Comprobacion.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Dias_Comprobacion + " = 0, ";
                } 
                if (!String.IsNullOrEmpty(Datos.P_Usuario_Autoriza_Gastos))
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Usuario_Autoriza_Gastos + " = '" + Datos.P_Usuario_Autoriza_Gastos.Trim() + "', ";
                }
                else
                {
                    Mi_SQL += Cat_Con_Parametros.Campo_Usuario_Autoriza_Gastos + " = NULL, ";
                }
                Mi_SQL += Cat_Con_Parametros.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Modifico + "', ";
                Mi_SQL += Cat_Con_Parametros.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL += " WHERE " + Cat_Con_Parametros.Campo_Parametro_Contabilidad_ID + " = '00001'";

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();

                //Ejecutar transaccion
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_Parametros
        /// DESCRIPCION : Consulta los datos almacenados
        /// PARAMETROS  : 
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 21/Septiembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Datos_Parametros(Cls_Cat_Con_Parametros_Negocio Datos)
        {
            String Mi_SQL; //Variable para almacenar la instruccion de consulta a la BD.
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL = "SELECT PARAMETROS.*,EMPLEADO1." + Cat_Empleados.Campo_No_Empleado + " as usuario1, EMPLEADO2." + Cat_Empleados.Campo_No_Empleado + " as usuario2 FROM " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros +
                    " PARAMETROS LEFT JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO1 ON PARAMETROS." + Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv + " = " +
                    "EMPLEADO1." + Cat_Empleados.Campo_Empleado_ID + " " +
                    "LEFT JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADO2 ON PARAMETROS." + Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv_2 + " = " +
                    "EMPLEADO2." + Cat_Empleados.Campo_Empleado_ID + " ";
                    
                Cmmd.CommandText = Mi_SQL.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle); 
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return Ds_Oracle.Tables[0];
            }
            catch (SqlException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }


        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_Parametros
        /// DESCRIPCION : Consulta los datos almacenados
        /// PARAMETROS  : 
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 21/Septiembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Datos_Parametros_2()
        {
            String Mi_SQL; //Variable para almacenar la instruccion de consulta a la BD.
            
            try
            {
                Mi_SQL = "SELECT PARAMETROS.* FROM " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros;
                Mi_SQL = Mi_SQL + " PARAMETROS ";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Oracle Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Empleados
        /// DESCRIPCION : Consulta los datos almacenados
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 24/Marzo/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Empleados(Cls_Cat_Con_Parametros_Negocio Datos)
        {
            String Mi_SQL; //Variable para almacenar la instruccion de consulta a la BD.

            try
            {
                Mi_SQL = "SELECT * FROM " + Cat_Empleados.Tabla_Cat_Empleados;
                Mi_SQL +=" WHERE "+ Cat_Empleados.Campo_No_Empleado+" = "+ Datos.P_No_Empleado +" AND ";
                Mi_SQL +=  Cat_Empleados.Campo_Estatus + " = 'ACTIVO'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("SQL Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        #endregion

        #region (Datos Ingresos)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Fte_Financiamiento
            ///DESCRIPCIÓN          : consulta para obtener las fuentes de financiamiento
            ///PARAMETROS           1 Negocio: conexion a la capa de negocios
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 03/Diciembre/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consulta_Fte_Financiamiento(Cls_Cat_Con_Parametros_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT UPPER(LTRIM(RTRIM(" + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ")) + ' ' + ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ") AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Estatus + " = 'ACTIVO'");
                    
                    if (!String.IsNullOrEmpty(Negocio.P_Tipo_Fte.Trim()))
                    {
                        if (Negocio.P_Tipo_Fte.Trim().Equals("RAMO33"))
                        {
                            Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI' ");
                        }
                        else 
                        {
                            Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'NO' ");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                    {
                        Mi_Sql.Append(" AND UPPER(TRIM(" + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ") + ' ' + ");
                        Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ")");
                        Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                    }

                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Programas
            ///DESCRIPCIÓN          : consulta para obtener los programas
            ///PARAMETROS           1 Negocio: conexion a la capa de negocios
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 03/Diciembre/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consulta_Programas(Cls_Cat_Con_Parametros_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT UPPER(LTRIM(RTRIM(" + Cat_Sap_Proyectos_Programas.Campo_Clave + ")) + ' ' + ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Campo_Descripcion + ") AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Estatus + " = 'ACTIVO'");

                    if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                    {
                        Mi_Sql.Append(" AND UPPER(LTRIM(RTRIM(" + Cat_Sap_Proyectos_Programas.Campo_Clave + ")) + ' ' + ");
                        Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Campo_Descripcion + ")");
                        Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                    }

                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de lps programas. Error: [" + Ex.Message + "]");
                }
            }
        #endregion
    }
}
