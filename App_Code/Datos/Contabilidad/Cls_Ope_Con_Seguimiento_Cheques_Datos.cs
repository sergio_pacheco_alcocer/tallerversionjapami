﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Ope_Con_Seguimiento_De_Cheques.Negocios;
using JAPAMI.Constantes;

/// <summary>
/// Summary description for Cls_Ope_Con_Seguimiento_Cheques_Datos
/// </summary>
namespace JAPAMI.Ope_Con_Seguimiento_De_Cheques.Datos
{
    public class Cls_Ope_Con_Seguimiento_Cheques_Datos
    {
        public Cls_Ope_Con_Seguimiento_Cheques_Datos()
        {
     
        }
        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Tipos_Beneficiarios
        ///DESCRIPCIÓN: Consulta los tipos de beneficiarios que pueden existir para un cheque
        ///PARAMETROS:  1.- Cls_Cat_Com_Monto_Proceso_Compra_Negocios
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 13/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Tipos_Beneficiarios( )
        {
            String Mi_SQL = string.Empty;
            try {
                Mi_SQL = "SELECT " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + ", ";
                Mi_SQL += "UPPER("+Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + ") AS DESCRIPCION ";
                Mi_SQL += " FROM " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago;
                Mi_SQL += " ORDER BY " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }catch(Exception Ex){
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }
        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Beneficiarios
        ///DESCRIPCIÓN: Consulta a los posibles empleados beneficiarios
        ///PARAMETROS:  1.- Objeto de la clase de negocios para obtener datos
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 14/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Beneficiarios(Cls_Ope_Con_Seguimiento_Cheques_Negocio Clase_Seguimiento_Negocio)
        {
            String Mi_SQL = string.Empty;

            try
            {
                Mi_SQL = "SELECT DISTINCT " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Beneficiario_Pago;
                Mi_SQL += " FROM " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " JOIN " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos;
                Mi_SQL += " ON " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Solicitud_Pago + " = " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago;
                Mi_SQL += " WHERE " + Ope_Con_Pagos.Campo_Beneficiario_Pago + " LIKE UPPER('%" + Clase_Seguimiento_Negocio.P_Beneficiario + "%') ";
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Tipo_Benefciario)) 
                {
                    Mi_SQL += " AND " + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Clase_Seguimiento_Negocio.P_Tipo_Benefciario + "'";
                }
                Mi_SQL += " ORDER BY " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_Beneficiario_Pago;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }
         //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Cheques_Listado
        ///DESCRIPCIÓN: Consulta los cheques registrados
        ///PARAMETROS:  1.- Cls_Cat_Com_Monto_Proceso_Compra_Negocios
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 15/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Cheques_Listado(Cls_Ope_Con_Seguimiento_Cheques_Negocio Clase_Seguimiento_Negocio)
        {
            String Mi_SQL = string.Empty;
            try {
                Mi_SQL = "SELECT " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_No_Cheque;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Beneficiario;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Concepto;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Monto + " AS  Importe ";
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Estatus;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Folio;
                Mi_SQL += " FROM " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + " JOIN " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos;
                Mi_SQL += " ON " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_No_Pago + " = " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Pago;
                Mi_SQL += " JOIN " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " ON ";
                Mi_SQL += Ope_Con_Pagos.Tabla_Ope_Con_Pagos + "." + Ope_Con_Pagos.Campo_No_Solicitud_Pago + " = " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago;
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_No_Cheque)) 
                {
                    Mi_SQL += " WHERE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_No_Cheque + " = " + Clase_Seguimiento_Negocio.P_No_Cheque; 
                }
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Etapa)) 
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Estatus + " = '" + Clase_Seguimiento_Negocio.P_Etapa + "'";
                    }
                    else 
                    {
                        Mi_SQL += " WHERE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Estatus + " = '" + Clase_Seguimiento_Negocio.P_Etapa + "'";
                    }
                }
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Tipo_Benefciario)) 
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Clase_Seguimiento_Negocio.P_Tipo_Benefciario + "'";
                    }
                    else 
                    {
                        Mi_SQL += " WHERE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " = '" + Clase_Seguimiento_Negocio.P_Tipo_Benefciario + "'";
                    }
                }
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Beneficiario)) 
                {
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Beneficiario + " = '" + Clase_Seguimiento_Negocio.P_Beneficiario + "'";
                    }
                    else 
                    {
                        Mi_SQL += " WHERE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Beneficiario + " = '" + Clase_Seguimiento_Negocio.P_Beneficiario + "'";
                    }
                }
                if (!String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Fecha_Emición_Inicio) && !String.IsNullOrEmpty(Clase_Seguimiento_Negocio.P_Fecha_Emición_Fin)) 
                {
                    Clase_Seguimiento_Negocio.P_Fecha_Emición_Inicio = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Clase_Seguimiento_Negocio.P_Fecha_Emición_Inicio));
                    Clase_Seguimiento_Negocio.P_Fecha_Emición_Fin = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Clase_Seguimiento_Negocio.P_Fecha_Emición_Fin));
                    if (Mi_SQL.Contains("WHERE"))
                    {
                        Mi_SQL += " AND ((" + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Fecha + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Emición_Inicio + " 00:00:00' AND ";
                        Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Emición_Fin + " 23:59:00') ";
                        Mi_SQL += " OR (" + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Fecha_Creo + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Emición_Inicio + " 00:00:00' AND ";
                        Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Emición_Fin + " 23:59:00'))";
                    }
                    else 
                    {
                        Mi_SQL += " WHERE ((" + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Fecha + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Emición_Inicio + " 00:00:00' AND ";
                        Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Emición_Fin + " 23:59:00') ";
                        Mi_SQL += " OR (" + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Fecha_Creo + " BETWEEN '" + Clase_Seguimiento_Negocio.P_Fecha_Emición_Inicio + " 00:00:00' AND ";
                        Mi_SQL += " '" + Clase_Seguimiento_Negocio.P_Fecha_Emición_Fin + " 23:59:00'))";
                    }
                }

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }catch(Exception Ex){
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }
        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Detalle_Cheques
        ///DESCRIPCIÓN: Consulta el detalle del cheque selccionado
        ///PARAMETROS:  1.- Cls_Cat_Com_Monto_Proceso_Compra_Negocios
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 17/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Detalle_Cheque(Cls_Ope_Con_Seguimiento_Cheques_Negocio Clase_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                Mi_SQL = "SELECT " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_No_Cheque+ ", ";
                Mi_SQL += Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Estatus;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Beneficiario;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Monto;
                Mi_SQL += ", " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_Folio;
                Mi_SQL += ", " + Ope_Con_Seguimiento_Cheques.Tabla_Ope_Con_Seguimiento_Cheques + "." + Ope_Con_Seguimiento_Cheques.Campo_Estatus_Entrada;
                Mi_SQL += ", " + Ope_Con_Seguimiento_Cheques.Tabla_Ope_Con_Seguimiento_Cheques + "." + Ope_Con_Seguimiento_Cheques.Campo_Fecha_Entrada;
                Mi_SQL += ", " + Ope_Con_Seguimiento_Cheques.Tabla_Ope_Con_Seguimiento_Cheques + "." + Ope_Con_Seguimiento_Cheques.Campo_Estatus_Salida;
                Mi_SQL += ", " + Ope_Con_Seguimiento_Cheques.Tabla_Ope_Con_Seguimiento_Cheques + "." + Ope_Con_Seguimiento_Cheques.Campo_Fecha_Salida;
                Mi_SQL += ", ( SELECT " + Cat_Empleados.Campo_Nombre + " + ' ' +" + Cat_Empleados.Campo_Apellido_Paterno + " + ' '+ " + Cat_Empleados.Campo_Apellido_Materno;
                Mi_SQL += " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
                Mi_SQL += " WHERE " + Cat_Empleados.Campo_Empleado_ID + " = " + Ope_Con_Seguimiento_Cheques.Tabla_Ope_Con_Seguimiento_Cheques + "." + Ope_Con_Seguimiento_Cheques.Campo_Empleado_Recibio_ID;
                Mi_SQL += ") AS EMPLEADO ";
                Mi_SQL += ", " + Ope_Con_Seguimiento_Cheques.Tabla_Ope_Con_Seguimiento_Cheques + "." + Ope_Con_Seguimiento_Cheques.Campo_Comentarios;
                Mi_SQL += " FROM " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + " JOIN " + Ope_Con_Seguimiento_Cheques.Tabla_Ope_Con_Seguimiento_Cheques;
                Mi_SQL += " ON " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_No_Cheque + " = " + Ope_Con_Seguimiento_Cheques.Tabla_Ope_Con_Seguimiento_Cheques + "." + Ope_Con_Seguimiento_Cheques.Campo_No_Cheque;
                Mi_SQL += " WHERE " + Ope_Con_Cheques.Tabla_Ope_Con_Cheques + "." + Ope_Con_Cheques.Campo_No_Cheque + " = " + Clase_Negocio.P_No_Cheque;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los tipos de Beneficiario. Error: [" + Ex.Message + "]");
            }
        }
    }
}
