﻿using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Contabilidad_Transferencia.Negocio;

namespace JAPAMI.Contabilidad_Transferencia.Datos
{
    public class Cls_Ope_Con_Transferencia_Datos
    {
        #region Modificar
        /// ********************************************************************************************************************
        /// NOMBRE:         Modificar_Transferencia
        /// COMENTARIOS:    cambiara los datos transferencia, y cuenta_transferencia 
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     18/Abril/2012  
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static Boolean Modificar_Transferencia(Cls_Ope_Con_Transferencia_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion;//Variable para la conexión para la base de datos   
            SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;

            Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Comando = new SqlCommand();
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Transaction = Transaccion;
            Comando.Connection = Conexion;
            try
            {
                Mi_SQL.Append("UPDATE " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SET ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Transferencia + "='',");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Cuenta_Transferencia_ID + "='',");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Layout_Transferencia + "=''");
                Mi_SQL.Append(" Where " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_solicitud_Pago + "'");

                //Ejecutar consulta
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Conexion.Close();
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                if (Transaccion != null)
                {
                    Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }
                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Conexion.Close();
                Comando = null;
                Conexion = null;
                Transaccion = null;
            }
            return Operacion_Completa;
        }
        #endregion

        #region Consultas
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Solicitud_Transferencia
        /// COMENTARIOS:    Consulta las solicitudes de pago con estatus de ejercida y con numero de transferencia
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     18/Abril/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Solicitud_Transferencia(Cls_Ope_Con_Transferencia_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                Mi_SQL.Append("Select ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + ".*, ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Nombre + " as Nombre_Banco, ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_No_Cuenta + " as No_Cuenta, ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario);

                Mi_SQL.Append(" From ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);

                Mi_SQL.Append(" left outer join ");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos);
                Mi_SQL.Append(" ON ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Cuenta_Transferencia_ID);
                Mi_SQL.Append("=");
                Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Banco_ID);

                Mi_SQL.Append(" left outer join ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_SQL.Append(" ON ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva);
                Mi_SQL.Append("=");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);

                Mi_SQL.Append(" Where ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Estatus + "='EJERCIDO' ");
                Mi_SQL.Append(" AND ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Transferencia + " is not null ");

                if (!String.IsNullOrEmpty(Datos.P_No_solicitud_Pago))
                {
                    Mi_SQL.Append(" And " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_solicitud_Pago + "' ");
                }
                if (!String.IsNullOrEmpty(Datos.P_Banco))
                {
                    Mi_SQL.Append(" And " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Nombre + "='" + Datos.P_Banco + "' ");
                }
                Mi_SQL.Append(" Order by ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago);

                     
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0]; ;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_orden_Transferencia
        /// COMENTARIOS:    Consulta las transferencias realizadas por dia
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Sergio Manuel Gallardo andrade 
        /// FECHA CREÓ:     25/Mayo/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_orden_Transferencia(Cls_Ope_Con_Transferencia_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                Mi_SQL.Append("SELECT DISTINCT "+Ope_Con_Solicitud_Pagos.Campo_Transferencia);
                Mi_SQL.Append(" FROM "+Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos+" WHERE "+ Ope_Con_Solicitud_Pagos.Campo_Estatus+"!='CANCELADO' AND "+Ope_Con_Solicitud_Pagos.Campo_Transferencia+" IS NOT NULL");
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Transferencia))
                {
                    Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Layout_Transferencia);
                    Mi_SQL.Append(" BETWEEN CONVERT(DATETIME,'" + Datos.P_Fecha_Transferencia + "')");
                    Mi_SQL.Append(" AND CONVERT(DATETIME,'" + Datos.P_Fecha_Transferencia + "')");
                }

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0]; ;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Transferencias_Por_Orden
        /// COMENTARIOS:    Consulta las transferencias realizadas por dia
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Sergio Manuel Gallardo andrade 
        /// FECHA CREÓ:     25/Mayo/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Transferencias_Por_Orden(Cls_Ope_Con_Transferencia_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                if (Datos.P_Tipo_Filtro.Equals("1"))
                {
                    Mi_SQL.Append("SELECT DISTINCT BANCO." + Cat_Nom_Bancos.Campo_Nombre + " Banco");
                }
                else {
                    Mi_SQL.Append("SELECT DISTINCT TIPO." +Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion_Finanzas + " DESCRIPCION");
                }
                Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SOLICITUD");
                if (Datos.P_Tipo_Filtro.Equals("1"))
                {
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Cuenta_Transferencia_ID + "= BANCO."+Cat_Nom_Bancos.Campo_Banco_ID );
                }
                else
                {
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + " TIPO ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + "= TIPO." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID);
                }
                
                Mi_SQL.Append(" WHERE SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Estatus + "!='CANCELADO' AND " + Ope_Con_Solicitud_Pagos.Campo_Transferencia + " IS NOT NULL");
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Transferencia))
                {
                    Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Layout_Transferencia);
                    Mi_SQL.Append(" BETWEEN CONVERT(DATETIME,'" + Datos.P_Fecha_Transferencia + "')");
                    Mi_SQL.Append(" AND CONVERT(DATETIME,'" + Datos.P_Fecha_Transferencia + "')");
                }
                if (!String.IsNullOrEmpty(Datos.P_Orden))
                {
                    Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Campo_Transferencia+"='"+Datos.P_Orden+"'");
                }
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0]; ;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Transferencias_Detalles
        /// COMENTARIOS:    Consulta las transferencias realizadas por dia y por banco
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Sergio Manuel Gallardo andrade 
        /// FECHA CREÓ:     25/Mayo/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Transferencias_Detalles(Cls_Ope_Con_Transferencia_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                Mi_SQL.Append("SELECT  PROVEEDOR." + Cat_Com_Proveedores.Campo_Banco_Proveedor + " as BANCO, PROVEEDOR.");
                Mi_SQL.Append(Cat_Com_Proveedores.Campo_Banco_Proveedor_ID + " as ID_BANCO, PROVEEDOR.");
                Mi_SQL.Append(Cat_Com_Proveedores.Campo_Compañia + " as BENEFICIARIO, PROVEEDOR.");
                Mi_SQL.Append(Cat_Com_Proveedores.Campo_Cuenta + " as CUENTA, PROVEEDOR.");
                Mi_SQL.Append(Cat_Com_Proveedores.Campo_Clabe + " as CLABE, SOLICITUD.");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Monto + " as IMPORTE, SOLICITUD.");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Transferencia + " as ORDEN, SOLICITUD.");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_Concepto + " as CONCEPTO, BANCO.");
                Mi_SQL.Append(Cat_Nom_Bancos.Campo_Nombre + " as BANCO_TRANSFERENCIA, TIPO.");
                Mi_SQL.Append(Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion_Finanzas +" as DESCRIPCION, SOLICITUD.");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago);
                Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SOLICITUD");
                Mi_SQL.Append(" LEFT OUTER JOIN "+ Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos +" BANCO ON SOLICITUD."+ Ope_Con_Solicitud_Pagos.Campo_Cuenta_Transferencia_ID+"= BANCO."+Cat_Nom_Bancos.Campo_Banco_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDOR ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + "= PROVEEDOR."+Cat_Com_Proveedores.Campo_Proveedor_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + " TIPO ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + "= TIPO." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID);
                Mi_SQL.Append(" WHERE SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Estatus + "!='CANCELADO' AND " + Ope_Con_Solicitud_Pagos.Campo_Transferencia + " IS NOT NULL");
                if (!String.IsNullOrEmpty(Datos.P_Banco))
                {
                    Mi_SQL.Append(" AND BANCO."+Cat_Nom_Bancos.Campo_Nombre+ "='"+Datos.P_Banco+"'");
                }
                if (!String.IsNullOrEmpty(Datos.P_Tipo_Pago))
                {
                    Mi_SQL.Append(" AND TIPO." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion_Finanzas + "='" + Datos.P_Tipo_Pago + "'");
                }
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Transferencia))
                {
                    Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Layout_Transferencia);
                    Mi_SQL.Append(" BETWEEN CONVERT(DATETIME,'" + Datos.P_Fecha_Transferencia + "')");
                    Mi_SQL.Append(" AND CONVERT(DATETIME,'" + Datos.P_Fecha_Transferencia + "')");
                }
                if (!String.IsNullOrEmpty(Datos.P_Orden))
                {
                    Mi_SQL.Append(" AND " + Ope_Con_Solicitud_Pagos.Campo_Transferencia + "='" + Datos.P_Orden + "'");
                }

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0]; ;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }
        
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Datos_Bancarios_Proveedor
        /// COMENTARIOS:    Consulta las solicitudes  con numero de transferencia
        /// PARÁMETROS:     Datos.- Valor de los campos a consultar 
        /// USUARIO CREÓ:   Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ:     18/Abril/2012 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Datos_Bancarios_Proveedor(Cls_Ope_Con_Transferencia_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            try
            {
                Mi_SQL.Append("Select ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + ".*, ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Banco_Proveedor  + " as Nombre_Banco, ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario +",");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + " as Nombre_Proveedor, ");
                //Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Banco_ID + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Banco_Proveedor_ID + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Clabe + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Tipo_Cuenta + ", ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Cuenta);

                Mi_SQL.Append(" From ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos);

                //Mi_SQL.Append(" left outer join ");
                //Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos);
                //Mi_SQL.Append(" ON ");
                //Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Cuenta_Transferencia_ID);
                //Mi_SQL.Append("=");
                //Mi_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Banco_ID);

                Mi_SQL.Append(" left outer join ");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_SQL.Append(" ON ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva);
                Mi_SQL.Append("=");
                Mi_SQL.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);

                Mi_SQL.Append(" left outer join ");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores);
                Mi_SQL.Append(" ON ");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID);
                Mi_SQL.Append("=");
                Mi_SQL.Append(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID);

                Mi_SQL.Append(" Where ");

                if (!String.IsNullOrEmpty(Datos.P_No_solicitud_Pago))
                {
                    Mi_SQL.Append(Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + "." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "='" + Datos.P_No_solicitud_Pago + "' ");
                }


                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0]; ;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
        }
        #endregion

        
    }
}
