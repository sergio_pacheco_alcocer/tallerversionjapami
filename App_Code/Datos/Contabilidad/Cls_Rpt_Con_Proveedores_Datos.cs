﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Reporte_Proveedores.Negocio;

namespace JAPAMI.Reporte_Proveedores.Datos
{
    public class Cls_Rpt_Con_Proveedores_Datos
    {
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Proveedores_Datos
        /// DESCRIPCION: SE CONSULTAN LAS CUENTAS POR PAGAR DE LOS PROVEEDORES
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : SERGIO MANUEL GALLARDO ANDRADE
        /// FECHA_CREO : 18 DE MAYO 2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Proveedores_Datos(Cls_Rpt_Con_Proveedores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT DETALLE."+Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura+", DETALLE."+ Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Factura+", PROVEEDOR."+Cat_Com_Proveedores.Campo_Proveedor_ID+", ");
                Mi_SQL.Append("DETALLE." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura + ", DETALLE." + Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Creo + ", ");
                Mi_SQL.Append("SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + ", SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Poliza + " AS POLIZA_DEVENGADO, ");
                Mi_SQL.Append("SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Poliza_ID + " AS  TIPO_POLIZA_DEVENGADO, SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Mes_Ano + " AS MES_ANIO_DEVENGADO, ");
                Mi_SQL.Append("SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Concepto + ", SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Reserva + " AS RESERVA, ");
                Mi_SQL.Append("PAGO." + Ope_Con_Pagos.Campo_No_poliza + " AS POLIZA_PAGADO, PAGO." + Ope_Con_Pagos.Campo_Tipo_Poliza_ID + " AS TIPO_POLIZA_PAGADO, ");
                Mi_SQL.Append("PAGO." + Ope_Con_Pagos.Campo_Mes_Ano + " AS MES_ANO_PAGADO, PAGO." + Ope_Con_Pagos.Campo_Fecha_Pago + ", PAGO."+ Ope_Con_Pagos.Campo_Forma_Pago +", ");
                Mi_SQL.Append("PROVEEDOR." + Cat_Com_Proveedores.Campo_Compañia + ", TIPO." + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion);
                Mi_SQL.Append(" FROM "+ Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos +" SOLICITUD ");
                Mi_SQL.Append("LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " DETALLE ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "=DETALLE.");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " PAGO ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Pago + "=PAGO.");
                Mi_SQL.Append(Ope_Con_Pagos.Campo_No_Pago);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDOR ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + "=PROVEEDOR.");
                Mi_SQL.Append(Cat_Com_Proveedores.Campo_Proveedor_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago  + " TIPO ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + "=TIPO.");
                Mi_SQL.Append(Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID +" WHERE SOLICITUD."+Ope_Con_Solicitud_Pagos.Campo_Estatus+" !='CANCELADO' ");
                if (!String.IsNullOrEmpty(Datos.P_Estatus))
                {
                    Mi_SQL.Append(" AND SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Pago + " IS NULL ");
                }
                else
                {
                    Mi_SQL.Append(" AND PAGO." + Ope_Con_Pagos.Campo_Estatus + "!='CANCELADO' ");
                }
                if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID))
                {
                    Mi_SQL.Append(" AND PROVEEDOR." + Cat_Com_Proveedores.Campo_Proveedor_ID + "='" + Datos.P_Proveedor_ID+"' ");
                }
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicial) && !String.IsNullOrEmpty(Datos.P_Fecha_Final))
                {
                    Mi_SQL.Append(" AND CONVERT(DATETIME, " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ")" + " >= '" + Datos.P_Fecha_Inicial + "' AND " + "CONVERT(DATETIME, " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ")" + " <= '" + Datos.P_Fecha_Final + "'");
                }
                Mi_SQL.Append(" ORDER BY PROVEEDOR."+ Cat_Com_Proveedores.Campo_Proveedor_ID);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Proveedores
        /// DESCRIPCION: SE CONSULTAN LAS CUENTAS POR PAGAR DE LOS PROVEEDORES
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : SERGIO MANUEL GALLARDO ANDRADE
        /// FECHA_CREO : 18 DE MAYO 2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Proveedores(Cls_Rpt_Con_Proveedores_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT DISTINCT PROVEEDOR." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", PROVEEDOR."+ Cat_Com_Proveedores.Campo_Nombre+",CUENTA."+Cat_Con_Cuentas_Contables.Campo_Cuenta+", ");
                Mi_SQL.Append("PROVEEDOR." + Cat_Com_Proveedores.Campo_Compañia+", PAGO." + Ope_Con_Pagos.Campo_Fecha_Pago);
                Mi_SQL.Append(" FROM " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SOLICITUD ");
                Mi_SQL.Append("LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos_Detalles.Tabla_Ope_Con_Solicitud_Pagos_Detalles + " DETALLE ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "=DETALLE.");
                Mi_SQL.Append(Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Solicitud_Pago);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " PAGO ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Pago + "=PAGO.");
                Mi_SQL.Append(Ope_Con_Pagos.Campo_No_Pago);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDOR ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Proveedor_ID + "=PROVEEDOR.");
                Mi_SQL.Append(Cat_Com_Proveedores.Campo_Proveedor_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CUENTA ON CUENTA." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + "=PROVEEDOR.");
                Mi_SQL.Append(Cat_Com_Proveedores.Campo_Cuenta_Proveedor_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago + " TIPO ON SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + "=TIPO.");
                Mi_SQL.Append(Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " WHERE SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Estatus + " !='CANCELADO'");
                if (!String.IsNullOrEmpty(Datos.P_Estatus))
                {
                    Mi_SQL.Append(" AND SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_No_Pago + " IS NULL ");
                }
                else
                {
                    Mi_SQL.Append(" AND PAGO." + Ope_Con_Pagos.Campo_Estatus + "!='CANCELADO' "); 
                }
                if (!String.IsNullOrEmpty(Datos.P_Proveedor_ID))
                {
                    Mi_SQL.Append(" AND PROVEEDOR." + Cat_Com_Proveedores.Campo_Proveedor_ID + "='" + Datos.P_Proveedor_ID+"' ");
                }
                if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicial) && !String.IsNullOrEmpty(Datos.P_Fecha_Final))
                {
                    Mi_SQL.Append(" AND CONVERT(DATETIME, " + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ")" + " >= '" + Datos.P_Fecha_Inicial + "' AND " + "CONVERT(DATETIME," + Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud + ")" + " <= '" + Datos.P_Fecha_Final + "'");
                }
                Mi_SQL.Append(" ORDER BY PROVEEDOR." + Cat_Com_Proveedores.Campo_Proveedor_ID);
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
    }
}
