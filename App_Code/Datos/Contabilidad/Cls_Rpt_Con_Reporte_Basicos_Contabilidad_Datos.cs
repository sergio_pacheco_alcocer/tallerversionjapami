﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;

namespace JAPAMI.Reporte_Basicos_Contabilidad.Datos
{
    public class Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos
    {
        public Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Datos()
        {
        }

        #region (Consultas Comunes)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Unidad_Responsable
        /// DESCRIPCION: Consultar las Unidades Responsables
        /// PARAMETROS : Datos: Variable para la capa de negocio
        /// CREO       : Susana Trigueros Armenta
        /// FECHA_CREO : 18/Abril/2012 9:56
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Unidad_Responsable(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            Mi_SQL.Append("SELECT " + Cat_Dependencias.Campo_Dependencia_ID);
            Mi_SQL.Append(", " + Cat_Dependencias.Campo_Clave + " +' '+ " + Cat_Dependencias.Campo_Nombre);
            Mi_SQL.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
            Mi_SQL.Append(" WHERE " + Cat_Dependencias.Campo_Estatus + "='ACTIVO'");
            Mi_SQL.Append(" ORDER BY " + Cat_Dependencias.Campo_Nombre + " ASC ");


            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Contables
        /// DESCRIPCION: Consultar las cuentas contables de acuerdo al nivel y a la dependencia
        /// PARAMETROS : Datos: Variable para la capa de negocio
        /// CREO       : Noe Mosqueda Valadez
        /// FECHA_CREO : 16/Abril/2012 11:41
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Cuentas_Contables(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado de la consulta
            Boolean Where_Utilizado = false; //variable que indica si la clausula where ya ha sido utilizada

            try
            {
                //Consulta las Cuentas Contables que estan dados de alta en la base de datos
                Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ", " + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                Mi_SQL = Mi_SQL + ", " + Cat_Con_Cuentas_Contables.Campo_Cuenta;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " ";

                //Verificar si se tiene que tomar en cuenta la dependencia
                if (String.IsNullOrEmpty(Datos.P_Dependencia_ID) == false)
                {
                    Mi_SQL += "WHERE Dependencia_ID = '" + Datos.P_Dependencia_ID + "' ";
                    Where_Utilizado = true;
                }

                //verificar si se tiene que tomar en cuenta el nivel
                if (String.IsNullOrEmpty(Datos.P_Nivel_ID) == false)
                {
                    //Verificar si ya se ha utilizado la clausula where
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    //resto de la consulta
                    Mi_SQL += Cat_Con_Cuentas_Contables.Campo_Nivel_ID + " = '" + Datos.P_Nivel_ID + "' ";
                }

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Contables_Concatenada
        /// DESCRIPCION: Consultar las cuentas contables de acuerdo al nivel y a la dependencia
        /// PARAMETROS : Datos: Variable para la capa de negocio
        /// CREO       :Sergio Manuel Gallardo  
        /// FECHA_CREO : 16/Abril/2012 11:41
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Cuentas_Contables_Concatenada(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado de la consulta
            Boolean Where_Utilizado = false; //variable que indica si la clausula where ya ha sido utilizada

            try
            {
                //Consulta las Cuentas Contables que estan dados de alta en la base de datos
                Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ", ("+Cat_Con_Cuentas_Contables.Campo_Cuenta+"+'-'+" + Cat_Con_Cuentas_Contables.Campo_Descripcion+") as Descripcion";
                Mi_SQL = Mi_SQL + ", " + Cat_Con_Cuentas_Contables.Campo_Cuenta;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " ";
                
                //Verificar si se tiene que tomar en cuenta la dependencia
                if (String.IsNullOrEmpty(Datos.P_Dependencia_ID) == false)
                {
                    Mi_SQL += "WHERE Dependencia_ID = '" + Datos.P_Dependencia_ID + "' ";
                    Where_Utilizado = true;
                }

                //verificar si se tiene que tomar en cuenta el nivel
                if (String.IsNullOrEmpty(Datos.P_Nivel_ID) == false)
                {
                    //Verificar si ya se ha utilizado la clausula where
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    //resto de la consulta
                    Mi_SQL += Cat_Con_Cuentas_Contables.Campo_Nivel_ID + " = '" + Datos.P_Nivel_ID + "' ";
                }
                Mi_SQL = Mi_SQL + " ORDER BY  " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " ";
                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        #endregion

        #region(Métodos)
            #region (Rpt_Balanza_Mensual)
                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Balanza_Mensual
                /// DESCRIPCION: Consulta los datos del Cierre mensual para poder tener los datos
                ///               de la balanza mensual
                /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
                /// CREO       : Yazmin A Delgado Gómez
                /// FECHA_CREO : 18-Octubre-2011
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataTable Consulta_Balanza_Mensual(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    StringBuilder Mi_SQL= new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
                    try
                    {
                        //Consulta el balance del mes y año que se proporciono por parte del usuario filtrando por rango de cuentas o mostrando todas
                        Mi_SQL.Append("SELECT " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + ", ");
                        Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ", ");
                        Mi_SQL.Append(Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Saldo_Inicial + ", ");
                        Mi_SQL.Append(Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Saldo_Final + ", ");
                        Mi_SQL.Append(Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Total_Debe + ", ");
                        Mi_SQL.Append(Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Total_Haber);
                        Mi_SQL.Append(" FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + ", " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual);
                        Mi_SQL.Append(" WHERE " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                        Mi_SQL.Append(" = " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID);
                        Mi_SQL.Append(" AND " + Ope_Con_Cierre_Mensual.Campo_Mes_Anio + " = '" + Datos.P_Mes_Anio + "'");
                        //Si se esta pidiendo filtrar entre rango de cuentas contables
                        if (!String.IsNullOrEmpty(Datos.P_Cuenta_Inicial) && !String.IsNullOrEmpty(Datos.P_Cuenta_Final))
                        {
                            Mi_SQL.Append(" AND (" + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " >= '" + Datos.P_Cuenta_Inicial + "'");
                            Mi_SQL.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " <= '" + Datos.P_Cuenta_Final + "')");
                        }
                        else
                        {
                            //Si se esta pidiendo consulta cuentas contables mayores o iguales a la indico el usuario
                            if (!String.IsNullOrEmpty(Datos.P_Cuenta_Inicial) && String.IsNullOrEmpty(Datos.P_Cuenta_Final))
                            {
                                Mi_SQL.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " >= '" + Datos.P_Cuenta_Inicial + "'");
                            }
                            else
                            {
                                //Si se esta pidiendo consultar cuentas contables menores o iguales a la que indico el usuario
                                if (String.IsNullOrEmpty(Datos.P_Cuenta_Inicial) && !String.IsNullOrEmpty(Datos.P_Cuenta_Final))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " <= '" + Datos.P_Cuenta_Final + "'");
                                }   
                            }
                        }
                        //Solo mostrara saldos que no esten en ceros
                        if (Datos.P_Montos_Cero == "SI")
                        {
                            Mi_SQL.Append(" AND (" + Ope_Con_Cierre_Mensual.Campo_Saldo_Inicial + " <> 0");
                            Mi_SQL.Append(" OR " + Ope_Con_Cierre_Mensual.Campo_Saldo_Final + " <> 0");
                            Mi_SQL.Append(" OR " + Ope_Con_Cierre_Mensual.Campo_Total_Debe + " <> 0");
                            Mi_SQL.Append(" OR " + Ope_Con_Cierre_Mensual.Campo_Total_Haber + " <> 0)");
                        }
                        Mi_SQL.Append(" ORDER BY " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }
                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Detalles_Poliza_Balanza_Mensual
                /// DESCRIPCION: Consulta el total Haber y Debe de las cuentas contables del Mes
                ///              y Año proporcionado por el usuario
                /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
                /// CREO       : Yazmin A Delgado Gómez
                /// FECHA_CREO : 18-Octubre-2011
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataTable Consulta_Balanza_Mensual_Debe_Haber(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    StringBuilder Mi_SQL= new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
                    try
                    {
                        //Consulta el balance del mes y año que se proporciono por parte del usuario filtrando por rango de cuentas o mostrando todas
                        Mi_SQL.Append("SELECT ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Debe + "), 0) AS Total_Debe, ");
                        Mi_SQL.Append("ISNULL(SUM(" + Ope_Con_Polizas_Detalles.Campo_Haber + "), 0) AS Total_Haber");
                        Mi_SQL.Append(" FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + ", " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Mi_SQL.Append(" WHERE " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas_Detalles.Campo_Mes_Ano + " = '" + Datos.P_Mes_Anio + "'");
                        //Si se esta pidiendo filtrar entre rango de cuentas contables
                        if (!String.IsNullOrEmpty(Datos.P_Cuenta_Inicial) && !String.IsNullOrEmpty(Datos.P_Cuenta_Final))
                        {
                            Mi_SQL.Append(" AND (" + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " >= '" + Datos.P_Cuenta_Inicial + "'");
                            Mi_SQL.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " <= '" + Datos.P_Cuenta_Final + "')");
                        }
                        else
                        {
                            //Si se esta pidiendo consulta cuentas contables mayores o iguales a la indico el usuario
                            if (!String.IsNullOrEmpty(Datos.P_Cuenta_Inicial) && String.IsNullOrEmpty(Datos.P_Cuenta_Final))
                            {
                                Mi_SQL.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " >= '" + Datos.P_Cuenta_Inicial + "'");
                            }
                            else
                            {
                                //Si se esta pidiendo consultar cuentas contables menores o iguales a la que indico el usuario
                                if (String.IsNullOrEmpty(Datos.P_Cuenta_Inicial) && !String.IsNullOrEmpty(Datos.P_Cuenta_Final))
                                {
                                    Mi_SQL.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " <= '" + Datos.P_Cuenta_Final + "'");
                                }
                            }
                        }
                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }
        ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Tipo_Solicitud
                /// DESCRIPCION: Consulta el tipo de solicitud
                /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
                /// CREO       : Hugo Enrique Ramírez Aguilera
                /// FECHA_CREO : 28-Febrero-2012
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataTable Consulta_Tipo_Solicitud(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    StringBuilder Mi_SQL= new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
                    try
                    {
                        //Consulta el balance del mes y año que se proporciono por parte del usuario filtrando por rango de cuentas o mostrando todas
                        Mi_SQL.Append("SELECT * from " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago);
                        Mi_SQL.Append(" where " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + "='" + Datos.P_Tipo_Solicitud + "'");
                        
                        
                        
                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }

                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Nueva_Balanza_Mensual
                /// DESCRIPCION: Consulta para el nuevo formato de la balnza mensual
                /// PARAMETROS : Datos: Variable de la capa de negocios
                /// CREO       : Noe Mosqueda Valadez
                /// FECHA_CREO : 24/Mayo/2012 16:50
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataSet Consulta_Nueva_Balanza_Mensual(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    //Declaracion de variables
                    String Mi_SQL = String.Empty; //variable para las consultas
                    DataSet Ds_Balanza_Mensual = new DataSet(); //Dataset para el resultado
                    DataTable Dt_Cabecera = new DataTable(); //Tabla para los datos de la cabecera
                    DataTable Dt_Detalles = new DataTable(); //Tabla para los detalles de la consulta
                    DataTable Dt_Cuentas_Contables = new DataTable(); //tabla para las cuentas contables
                    int Cont_Elementos = 0; //variable para el contador
                    String Totales_Cuenta = String.Empty; //Variable para los totales
                    String[] Vec_Totales_Cuenta; //vector para los totales de la cuenta
                    DataRow Renglon; //Renglon para el llenado de la tabla

                    try
                    {
                        //Consulta para las cuentas contables
                        Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ", " +
                            Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + ", " +
                            Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ", " +
                            Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Afectable + ", " +
                            Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta + ", " +
                            Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Nivel_ID + ", " +
                            Cat_Con_Niveles.Tabla_Cat_Con_Niveles + "." + Cat_Con_Niveles.Campo_Inicio_Nivel + ", " +
                            Cat_Con_Niveles.Tabla_Cat_Con_Niveles + "." + Cat_Con_Niveles.Campo_Final_Nivel + ", " +
                            "Conteo_Padre = (SELECT COUNT(CAT_CON_CUENTAS_CONTABLES_2." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID_Padre + ") FROM " +
                            Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " CAT_CON_CUENTAS_CONTABLES_2 " +
                            "WHERE CAT_CON_CUENTAS_CONTABLES_2." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID_Padre + " = " +
                            Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ") " +
                            "FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " " +
                            "INNER JOIN " + Cat_Con_Niveles.Tabla_Cat_Con_Niveles +
                            " ON " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Nivel_ID + " = " +
                            Cat_Con_Niveles.Tabla_Cat_Con_Niveles + "." + Cat_Con_Niveles.Campo_Nivel_ID + " " +
                            "ORDER BY " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " ";

                        //Ejecutar consulta
                        Dt_Cuentas_Contables = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                        //Definir la tabla de la cabecera
                        Dt_Cabecera.Columns.Add("ID", typeof(int));
                        Dt_Cabecera.Columns.Add("Fecha", typeof(String));
                        Dt_Cabecera.Columns.Add("Mes", typeof(String));
                        Dt_Cabecera.Columns.Add("Anio", typeof(int));
                        Dt_Cabecera.Columns.Add("Saldo_Inicial", typeof(Decimal));
                        Dt_Cabecera.Columns.Add("Saldo_Final", typeof(Decimal));
                        Dt_Cabecera.Columns.Add("Total_Debe", typeof(Decimal));
                        Dt_Cabecera.Columns.Add("Total_Haber", typeof(Decimal));
                        Dt_Cabecera.TableName = "Cabecera";

                        //Definir la tabla de los detalles
                        Dt_Detalles.Columns.Add("ID", typeof(int));
                        Dt_Detalles.Columns.Add("Cuenta_Padre", typeof(String));
                        Dt_Detalles.Columns.Add("Descripcion_Padre", typeof(String));
                        Dt_Detalles.Columns.Add("Saldo_Inicial_Padre", typeof(Decimal));
                        Dt_Detalles.Columns.Add("Saldo_Final_Padre", typeof(Decimal));
                        Dt_Detalles.Columns.Add("Debe_Padre", typeof(Decimal));
                        Dt_Detalles.Columns.Add("Haber_Padre", typeof(Decimal));
                        Dt_Detalles.Columns.Add("Cuenta", typeof(String));
                        Dt_Detalles.Columns.Add("Descripcion", typeof(String));
                        Dt_Detalles.Columns.Add("Saldo_Inicial", typeof(Decimal));
                        Dt_Detalles.Columns.Add("Saldo_Final", typeof(Decimal));
                        Dt_Detalles.Columns.Add("Debe", typeof(Decimal));
                        Dt_Detalles.Columns.Add("Haber", typeof(Decimal));
                        Dt_Detalles.TableName = "Detalles";

                        //Llenar los datos de la cabecera
                        Renglon = Dt_Cabecera.NewRow();
                        Renglon["ID"] = 1;
                        if (!String.IsNullOrEmpty(Datos.P_Fecha_Final))
                            Renglon["Fecha"] = Datos.P_Fecha_Final;
                        Renglon["Mes"] = Datos.P_Mes;
                        Renglon["Anio"] = Datos.P_Anio;

                        //OBtener los totales del reporte
                        Totales_Cuenta = Consulta_Totales_Cuenta_Padre(String.Empty, 0, Datos.P_Mes_Anio, "Total", String.Empty);

                        //Crear el vector 
                        Vec_Totales_Cuenta = Totales_Cuenta.Split(',');
                        
                        //Verificar si son 4 elementos
                        if (Vec_Totales_Cuenta.Length == 4)
                        {
                            //Verificar si todos los elementos son cero
                            if (Convert.ToDouble(Vec_Totales_Cuenta[0]) != 0 && Convert.ToDouble(Vec_Totales_Cuenta[1]) != 0 && Convert.ToDouble(Vec_Totales_Cuenta[2]) != 0 && Convert.ToDouble(Vec_Totales_Cuenta[3]) != 0)
                            {
                                Renglon["Saldo_Inicial"] = Convert.ToDouble(Vec_Totales_Cuenta[0]);
                                Renglon["Saldo_Final"] = Convert.ToDouble(Vec_Totales_Cuenta[1]);
                                Renglon["Total_Debe"] = Convert.ToDouble(Vec_Totales_Cuenta[2]);
                                Renglon["Total_Haber"] = Convert.ToDouble(Vec_Totales_Cuenta[3]);
                            }
                            else
                            {
                                Renglon["Saldo_Inicial"] = 0;
                                Renglon["Saldo_Final"] = 0;
                                Renglon["Total_Debe"] = 0;
                                Renglon["Total_Haber"] = 0;
                            }
                        }
                        else
                        {
                            Renglon["Saldo_Inicial"] = 0;
                            Renglon["Saldo_Final"] = 0;
                            Renglon["Total_Debe"] = 0;
                            Renglon["Total_Haber"] = 0;
                        }
                        
                        //Colocar renglon en la cabecera
                        Dt_Cabecera.Rows.Add(Renglon);
                        
                        //Ciclo para el barrido de la tabla
                        for (Cont_Elementos = 0; Cont_Elementos < Dt_Cuentas_Contables.Rows.Count; Cont_Elementos++)
                        {
                            //Verificar si la cuenta es padre
                            if (Convert.ToInt32(Dt_Cuentas_Contables.Rows[Cont_Elementos]["Conteo_Padre"]) > 0)
                            {
                                //Obtener los totales
                                Totales_Cuenta = Consulta_Totales_Cuenta_Padre(Dt_Cuentas_Contables.Rows[Cont_Elementos]["Cuenta"].ToString().Trim(), Convert.ToInt32(Dt_Cuentas_Contables.Rows[Cont_Elementos]["Final_Nivel"]), Datos.P_Mes_Anio, "Padre", String.Empty);
                            }
                            else
                            {
                                //Obtener los totales
                                Totales_Cuenta = Consulta_Totales_Cuenta_Padre(String.Empty, 0, Datos.P_Mes_Anio, "Individual", Dt_Cuentas_Contables.Rows[Cont_Elementos]["Cuenta_Contable_ID"].ToString().Trim());
                            }

                            //Crear el vector 
                            Vec_Totales_Cuenta = Totales_Cuenta.Split(',');

                            //Verificar si son 4 elementos
                            if (Vec_Totales_Cuenta.Length == 4)
                            {
                                //Verificar si todos los elementos son ceros
                                //if (Vec_Totales_Cuenta[0] != "0" && Vec_Totales_Cuenta[1] != "0" && Vec_Totales_Cuenta[2] != "0" && Vec_Totales_Cuenta[3] != "0")
                                if (Convert.ToDouble(Vec_Totales_Cuenta[0]) != 0 && Convert.ToDouble(Vec_Totales_Cuenta[1]) != 0)
                                {
                                    //Instanciar renglon de los detalles
                                    Renglon = Dt_Detalles.NewRow();

                                    //Llenar el renglon de los detalles y colocarlo en la tabla
                                    Renglon["ID"] = 1;
                                    Renglon["Cuenta_Padre"] = "";
                                    Renglon["Descripcion_Padre"] = "";
                                    Renglon["Saldo_Inicial_Padre"] = 0;
                                    Renglon["Saldo_Final_Padre"] = 0;
                                    Renglon["Debe_Padre"] = 0;
                                    Renglon["Haber_Padre"] = 0;
                                    Renglon["Cuenta"] = Dt_Cuentas_Contables.Rows[Cont_Elementos]["Cuenta"].ToString().Trim();
                                    Renglon["Descripcion"] = Dt_Cuentas_Contables.Rows[Cont_Elementos]["Descripcion"].ToString().Trim();
                                    Renglon["Saldo_Inicial"] = Convert.ToDouble(Vec_Totales_Cuenta[0]);
                                    Renglon["Saldo_Final"] = Convert.ToDouble(Vec_Totales_Cuenta[1]);
                                    Renglon["Debe"] = Convert.ToDouble(Vec_Totales_Cuenta[2]);
                                    Renglon["Haber"] = Convert.ToDouble(Vec_Totales_Cuenta[3]);
                                    Dt_Detalles.Rows.Add(Renglon);
                                }
                                else
                                {
                                    //Verificar si se tiene que incluir los movimientos cero
                                    if (Datos.P_Montos_Cero == "NO")
                                    {
                                        //Instanciar renglon de los detalles
                                        Renglon = Dt_Detalles.NewRow();

                                        //Llenar el renglon de los detalles y colocarlo en la tabla
                                        Renglon["ID"] = 1;
                                        Renglon["Cuenta_Padre"] = "";
                                        Renglon["Descripcion_Padre"] = "";
                                        Renglon["Saldo_Inicial_Padre"] = 0;
                                        Renglon["Saldo_Final_Padre"] = 0;
                                        Renglon["Debe_Padre"] = 0;
                                        Renglon["Haber_Padre"] = 0;
                                        Renglon["Cuenta"] = Dt_Cuentas_Contables.Rows[Cont_Elementos]["Cuenta"].ToString().Trim();
                                        Renglon["Descripcion"] = Dt_Cuentas_Contables.Rows[Cont_Elementos]["Descripcion"].ToString().Trim();
                                        Renglon["Saldo_Inicial"] = 0;
                                        Renglon["Saldo_Final"] = 0;
                                        Renglon["Debe"] = Convert.ToDouble(Vec_Totales_Cuenta[2]);
                                        Renglon["Haber"] = Convert.ToDouble(Vec_Totales_Cuenta[3]);
                                        Dt_Detalles.Rows.Add(Renglon);
                                    }
                                }
                            }
                        }
                        
                        //Colocar las tablas en el dataset
                        Ds_Balanza_Mensual.Tables.Add(Dt_Cabecera);
                        Ds_Balanza_Mensual.Tables.Add(Dt_Detalles);

                        //Entregar resultado
                        return Ds_Balanza_Mensual;
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }

                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Totales_Cuenta_Padre
                /// DESCRIPCION: Consulta los totales de la cuenta Padre
                /// PARAMETROS : 1. Cuenta: Cadena de texto que contiene el numero de la cuenta
                ///              2. Fin_Nivel: Enterto que contiene el caractere del fin del nivel de la cuenta
                ///              3. Mes_Anio: Cadena de texto que indica el mes y el año de la operacion
                ///              4. Tipo_Consulta: Cadena de texto que indica el tipo de consulta a realizar
                ///              5. Cuenta_Contable_ID: Cadena de texto que contiene el ID de la cuenta contable
                /// CREO       : Noe Mosqueda Valadez
                /// FECHA_CREO : 24/Mayo/2012 14:00
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                private static String Consulta_Totales_Cuenta_Padre(String Cuenta, int Fin_Nivel, String Mes_Anio, String Tipo_Consulta, String Cuenta_Contable_ID)
                {
                    //Declaracion de variables
                    String Mi_SQL = String.Empty; //variable para las consultas
                    DataTable Dt_Consulta = new DataTable(); //tabla para el resultado de la consulta
                    String Resultado = String.Empty; //Variable para el resultado
                    String Cuenta_Filtro = String.Empty; //variable para el filtro de la cuenta

                    try
                    {
                        //Verificar si tipo de consulta
                        switch (Tipo_Consulta)
                        {
                            case "Total":
                                //Asignar consulta
                                Mi_SQL = "SELECT SUM(" + Ope_Con_Cierre_Mensual.Campo_Saldo_Inicial + ") AS Saldo_Inicial, " +
                                    "SUM(" + Ope_Con_Cierre_Mensual.Campo_Saldo_Final + ") AS Saldo_Final, " + 
                                    "SUM(" + Ope_Con_Cierre_Mensual.Campo_Total_Debe + ") AS Total_Debe, " + 
                                    "SUM(" + Ope_Con_Cierre_Mensual.Campo_Total_Haber + ") AS Total_Haber FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + " " +
                                    "WHERE " + Ope_Con_Cierre_Mensual.Campo_Mes_Anio + " = '" + Mes_Anio + "' ";
                                break;

                            case "Padre":
                                //Obtener el filtro den la cuenta
                                Cuenta_Filtro = Cuenta.Substring(0, Fin_Nivel);

                                //Asignar consulta
                                Mi_SQL = "SELECT SUM(" + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Saldo_Inicial + ") AS Saldo_Inicial, " +
                                    "SUM(" + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Saldo_Final + ") AS Saldo_Final, " +
                                    "SUM(" + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Total_Debe + ") AS Total_Debe, " +
                                    "SUM(" + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Total_Haber + ") AS Total_Haber " +
                                    "FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + " " +
                                    "INNER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " ON " +
                                    Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " = " +
                                    Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " " +
                                    "WHERE " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + "." + Ope_Con_Cierre_Mensual.Campo_Mes_Anio + " = '" + Mes_Anio + "' " +
                                    "AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '" + Cuenta_Filtro + "%' ";
                                break;

                            case "Individual":
                                //Asignar consulta
                                Mi_SQL = "SELECT " + Ope_Con_Cierre_Mensual.Campo_Saldo_Inicial + " AS Saldo_Inicial, " + Ope_Con_Cierre_Mensual.Campo_Saldo_Final + " AS Saldo_Final, " +
                                    Ope_Con_Cierre_Mensual.Campo_Total_Debe + " AS Total_Debe, " + Ope_Con_Cierre_Mensual.Campo_Total_Haber + " AS Total_Haber " +
                                    "FROM " + Ope_Con_Cierre_Mensual.Tabla_Ope_Con_Cierre_Mensual + " WHERE " + Ope_Con_Cierre_Mensual.Campo_Mes_Anio + " = '" + Mes_Anio + "' " +
                                    "AND " + Ope_Con_Cierre_Mensual.Campo_Cuenta_Contable_ID + " = '" + Cuenta_Contable_ID + "' ";                                
                                break;

                            default:
                                break;
                        }

                        //Ejecutar consulta
                        Dt_Consulta = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                        //Verificar si la consulta arrojo resultados
                        if (Dt_Consulta.Rows.Count > 0)
                        {
                            //Colocar los valores de los totales
                            if (Dt_Consulta.Rows[0]["Saldo_Inicial"] != DBNull.Value)
                            {
                                Resultado = Dt_Consulta.Rows[0]["Saldo_Inicial"].ToString().Trim() + ",";
                            }
                            else
                            {
                                Resultado = "0,";
                            }

                            if (Dt_Consulta.Rows[0]["Saldo_Final"] != DBNull.Value)
                            {
                                Resultado += Dt_Consulta.Rows[0]["Saldo_Final"].ToString().Trim() + ",";
                            }
                            else
                            {
                                Resultado += "0,";
                            }

                            if (Dt_Consulta.Rows[0]["Total_Debe"] != DBNull.Value)
                            {
                                Resultado += Dt_Consulta.Rows[0]["Total_Debe"].ToString().Trim() + ",";
                            }
                            else
                            {
                                Resultado += "0,";
                            }

                            if (Dt_Consulta.Rows[0]["Total_Haber"] != DBNull.Value)
                            {
                                Resultado += Dt_Consulta.Rows[0]["Total_Haber"].ToString().Trim();
                            }
                            else
                            {
                                Resultado += "0";
                            }
                        }
                        else
                        {
                            Resultado = "0,0,0,0";
                        }

                        //Entregar resultado
                        return Resultado;
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }

            #endregion
            #region (Rpt_Diario_General_CONAC)
                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Diario_General
                /// DESCRIPCION: Consulta todos los movimientos de pólizas realizados durante las
                ///              fechas proporcionadas por el usuario
                /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
                /// CREO       : Yazmin A Delgado Gómez
                /// FECHA_CREO : 19-Octubre-2011
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataTable Consulta_Diario_General(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos

                    try
                    {
                        Mi_SQL.Append("SELECT (" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza + ") AS P_No_Poliza, ");
                        Mi_SQL.Append("(" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ") AS Tipo_Poliza_ID, ");
                        Mi_SQL.Append("(" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Mes_Ano + ") AS Mes_Anio, ");
                        Mi_SQL.Append("(" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza + ") AS Fecha_Poliza, ");
                        Mi_SQL.Append("(" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Concepto + ") AS Concepto, ");
                        Mi_SQL.Append("(" + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas + "." + Cat_Con_Tipo_Polizas.Campo_Abreviatura + ") AS Tipo_Poliza");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + ", " + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID);
                        Mi_SQL.Append(" = " + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas + "." + Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza);
                        Mi_SQL.Append(" BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00'");
                        Mi_SQL.Append(" AND '" + Datos.P_Fecha_Final + " 23:59:59'");
                        Mi_SQL.Append(" ORDER BY " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza);
                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }
                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Diario_General_Detalles
                /// DESCRIPCION: Consulta todos los movimientos de pólizas realizados durante las
                ///              fechas proporcionadas por el usuario con respecto a su cuenta
                /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
                /// CREO       : Yazmin A Delgado Gómez
                /// FECHA_CREO : 19-Octubre-2011
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataTable Consulta_Diario_General_Detalles(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
                    try
                    {
                        Mi_SQL.Append("SELECT (" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza + ") AS P_No_Poliza, ");
                        Mi_SQL.Append("(" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ") AS Tipo_Poliza_ID, ");
                        Mi_SQL.Append("(" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Mes_Ano + ") AS Mes_Anio, ");
                        Mi_SQL.Append("(" + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + ") AS Cuenta, ");
                        Mi_SQL.Append("(" + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Concepto + ") AS Concepto, ");
                        Mi_SQL.Append("(" + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Debe + ") AS Debe, ");
                        Mi_SQL.Append("(" + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Haber + ") AS Haber");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + ", " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "," + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID);
                        Mi_SQL.Append(" = " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_No_Poliza);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Mes_Ano);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Mes_Ano);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza);
                        Mi_SQL.Append(" BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00'");
                        Mi_SQL.Append(" AND '" + Datos.P_Fecha_Final + " 23:59:59'");
                        Mi_SQL.Append(" ORDER BY " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza);
                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }
                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Libro_Diario
                /// DESCRIPCION: Consulta todos los movimientos de pólizas realizados durante las
                ///              fechas proporcionadas por el usuario con respecto a su cuenta
                /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
                /// CREO       : Yazmin A Delgado Gómez
                /// FECHA_CREO : 14-Noviembre-2011
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataTable Consulta_Libro_Diario(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Consulta los datos del detalle 
                    try
                    {
                        Mi_SQL.Append("SELECT " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza + " AS Fecha, ");
                        Mi_SQL.Append(Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza + " AS P_No_Poliza, 'POLIZAS' AS Documento_Fuente, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Partida + " AS No_Asiento, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Concepto + " AS Concepto_Detalle_Poliza, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Debe + " AS Debe, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Haber + " AS Haber, ");
                        Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " AS Cuenta_Contable, ");
                        Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " AS Concepto_Cuenta_Contable, ");
                        Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS Cuenta_Presupuestal, ");
                        Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Descripcion + " AS Concepto_Cuenta_Presupuestal");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
                        Mi_SQL.Append(" ON " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_No_Poliza);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza);
                        Mi_SQL.Append(" BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00'");
                        Mi_SQL.Append(" AND '" + Datos.P_Fecha_Final + " 23:59:59'");
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                        Mi_SQL.Append(" ON " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID);
                        Mi_SQL.Append(" = " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Partida_ID);
                        Mi_SQL.Append(" Where " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza);
                        Mi_SQL.Append(" BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00'");
                        Mi_SQL.Append(" AND '" + Datos.P_Fecha_Final + " 23:59:59'");
                        Mi_SQL.Append(" ORDER BY " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza);
                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }
            #endregion
            #region (Rpt_Libro_Mayor/Rpt_Tipo_Poliza)
                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Libro_Mayor
                /// DESCRIPCION: Consulta todos los movimientos que ha tenido la cuenta que fue
                ///              proporcionada por el usuario desde el primer mes hasta el mes
                ///              que proporciono el usuario
                /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
                /// CREO       : Yazmin A Delgado Gómez
                /// FECHA_CREO : 07-Noviembre-2011
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataTable Consulta_Libro_Mayor(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                    try
                    {
                        Mi_SQL.Append("SELECT " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza + " AS Fecha, ");
                        Mi_SQL.Append(Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza + " AS No_Poliza, ");
                        Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " AS Cuenta_ID, ");
                        Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " AS Cuenta, ");
                        Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " AS Descripcion, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Haber + " AS Haber, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Debe + " AS Debe, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Saldo + " AS Saldo, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Concepto + " AS Concepto, ");
                        Mi_SQL.Append(Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Usuario_Creo + " AS usuario, ");
                        Mi_SQL.Append(Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Mes_Ano  + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas.Tabla_Ope_Con_Polizas  + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID +", (");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Saldo + " + " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Haber+" - ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Debe + ") AS Saldo_Inicial");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + ", " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + ", " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables );
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_No_Poliza);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Mes_Ano);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Mes_Ano);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID);
                        Mi_SQL.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID);
                        if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicial) && !String.IsNullOrEmpty(Datos.P_Fecha_Final))
                        {
                            Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza);
                            Mi_SQL.Append(" BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00'");
                            Mi_SQL.Append(" AND '" + Datos.P_Fecha_Final + " 23:59:59'");
                        }
                        //Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Datos.P_Mes_Anio + "'");
                        if (!String.IsNullOrEmpty(Datos.P_Cuenta_Inicial) && String.IsNullOrEmpty(Datos.P_Cuenta_Final))
                        {
                            Mi_SQL.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " = '" + Datos.P_Cuenta_Inicial + "'");
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(Datos.P_Cuenta_Inicial) && !String.IsNullOrEmpty(Datos.P_Cuenta_Final))
                            {
                                Mi_SQL.Append(" AND (" + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " >= '" + Datos.P_Cuenta_Inicial + "'");
                                Mi_SQL.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables  + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " <= '" + Datos.P_Cuenta_Final + "')");
                            }
                        }
                        Mi_SQL.Append(" ORDER BY " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza + ", ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Consecutivo);
                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }
                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Libro_Mayor
                /// DESCRIPCION: Consulta todos los movimientos que ha tenido la cuenta que fue
                ///              proporcionada por el usuario desde el primer mes hasta el mes
                ///              que proporciono el usuario
                /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
                /// CREO       : Yazmin A Delgado Gómez
                /// FECHA_CREO : 07-Noviembre-2011
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataTable Consulta_Cuentas_Libro_Mayor(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                    try
                    {
                        Mi_SQL.Append("SELECT DISTINCT "+Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " AS Cuenta_ID, ");
                        Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " AS Cuenta, ");
                        Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " AS Descripcion ");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + ", " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + ", " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_No_Poliza);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Mes_Ano);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Mes_Ano);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID);
                        Mi_SQL.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID);
                        if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicial) && !String.IsNullOrEmpty(Datos.P_Fecha_Final))
                        {
                            Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza);
                            Mi_SQL.Append(" BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00'");
                            Mi_SQL.Append(" AND '" + Datos.P_Fecha_Final + " 23:59:59'");
                        }
                        //Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Mes_Ano + " = '" + Datos.P_Mes_Anio + "'");
                        if (!String.IsNullOrEmpty(Datos.P_Cuenta_Inicial) && String.IsNullOrEmpty(Datos.P_Cuenta_Final))
                        {
                            Mi_SQL.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " = '" + Datos.P_Cuenta_Inicial + "'");
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(Datos.P_Cuenta_Inicial) && !String.IsNullOrEmpty(Datos.P_Cuenta_Final))
                            {
                                Mi_SQL.Append(" AND (" + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " >= '" + Datos.P_Cuenta_Inicial + "'");
                                Mi_SQL.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " <= '" + Datos.P_Cuenta_Final + "')");
                            }
                        }
                        if (!String.IsNullOrEmpty(Datos.P_Cuenta))
                        {
                            Mi_SQL.Append(" AND " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta + "'");
                        }
                        Mi_SQL.Append(" ORDER BY " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }
                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Tipo_Poliza
                /// DESCRIPCION: Consulta los datos de las pólizas que fueron generadasde acuerdo
                ///              a los parametros seleccionados por el usuario
                /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
                /// CREO       : Sergio Manuel Gallardo Andrade
                /// FECHA_CREO : 30-Mayo-2012
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataSet Consulta_Tipo_Poliza(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar en la base de datos
                    try
                    {
                        Mi_SQL.Append("SELECT " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza + " AS P_No_Poliza, ");
                        Mi_SQL.Append(Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " AS Tipo_Poliza_ID, ");
                        Mi_SQL.Append(Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Mes_Ano + " AS Mes_Ano, ");
                        Mi_SQL.Append(Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza + " AS Fecha_Poliza, ");
                        Mi_SQL.Append(Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Concepto + " AS Concepto_Poliza, ");
                        Mi_SQL.Append("CAST(" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Total_Debe + " AS NUMERIC(12,2)) AS Debe, ");
                        Mi_SQL.Append("CAST(" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Total_Haber + " AS NUMERIC(12,2)) AS Haber, ");
                        Mi_SQL.Append(Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas + "." + Cat_Con_Tipo_Polizas.Campo_Abreviatura + " AS Tipo_Poliza,");
                        Mi_SQL.Append(Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Usuario_Creo + " AS Usuario,");
                        Mi_SQL.Append(" PAGOS." + Ope_Con_Pagos.Campo_Beneficiario_Pago + " AS BENEFICIARIO");
                        Mi_SQL.Append(" FROM " + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas + ", " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " PAGOS ON PAGOS." + Ope_Con_Pagos.Campo_No_poliza + " =" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza + " AND ");
                        Mi_SQL.Append(" PAGOS." + Ope_Con_Pagos.Campo_Tipo_Poliza_ID + " =" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " AND PAGOS." + Ope_Con_Pagos.Campo_Mes_Ano + " = " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Mes_Ano);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID);
                        Mi_SQL.Append(" = " + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas + "." + Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID);
                        if (!String.IsNullOrEmpty(Datos.P_Tipo_Polizas)) Mi_SQL.Append(Datos.P_Tipo_Polizas);
                        if (!String.IsNullOrEmpty(Datos.P_Poliza_Inicial) && !String.IsNullOrEmpty(Datos.P_Poliza_Final))
                        {
                            Mi_SQL.Append(" AND CAST(" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza + " AS NUMBER) >= " + int.Parse(Datos.P_Poliza_Inicial));
                            Mi_SQL.Append(" AND CAST(" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza + " AS NUMBER) >= " + int.Parse(Datos.P_Poliza_Final));
                        }
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza);
                        Mi_SQL.Append(" BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00'");
                        Mi_SQL.Append(" AND '" + Datos.P_Fecha_Final + " 23:59:59'"); 
                        if (!String.IsNullOrEmpty(Datos.P_Concepto))
                        {
                            Mi_SQL.Append(" AND " +Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Concepto + " LIKE '%"+Datos.P_Concepto+"%'");
                        }
                        DataSet Ds_Polizas = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        return Ds_Polizas;
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }
                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Detalles_Poliza
                /// DESCRIPCION: Consulta los datos de las pólizas que fueron generadasde acuerdo
                ///              a los parametros seleccionados por el usuario
                /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
                /// CREO       : José Antonio López Hernández
                /// FECHA_CREO : 09-Junio-2011
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataTable Consulta_Detalles_Poliza(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar en la base de datos
                    try
                    {
                        Mi_SQL.Append("SELECT " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza + " AS P_No_Poliza, ");
                        Mi_SQL.Append(Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " AS Tipo_Poliza_ID, ");
                        Mi_SQL.Append(Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Mes_Ano + " AS Mes_Ano, ");
                        Mi_SQL.Append(Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza + " AS Fecha_Poliza, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Partida + " AS Partida, ");
                        Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Concepto + " AS Concepto_Partida, ");
                        Mi_SQL.Append("CAST(" + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Debe + " AS NUMERIC(12,2)) AS Debe, ");
                        Mi_SQL.Append("CAST(" + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Haber + " AS NUMERIC(12,2)) AS Haber, ");
                        Mi_SQL.Append("CAST(" + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Saldo + " AS NUMERIC(12,2)) AS Saldo, ");
                        Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " AS Cuenta_Contable, ");
                        Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + " AS Cuenta, ");
                        Mi_SQL.Append(Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas + "." + Cat_Con_Tipo_Polizas.Campo_Abreviatura + " AS Tipo_Poliza,");
                        Mi_SQL.Append(" PAGOS." + Ope_Con_Pagos.Campo_Beneficiario_Pago + " AS BENEFICIARIO_PAGO");
                        Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " PAGOS ON PAGOS." + Ope_Con_Pagos.Campo_No_poliza + " =" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza + " AND ");
                        Mi_SQL.Append(" PAGOS." + Ope_Con_Pagos.Campo_Tipo_Poliza_ID + " =" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " AND PAGOS." + Ope_Con_Pagos.Campo_Mes_Ano + " = " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Mes_Ano);
                        Mi_SQL.Append(", " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + ", ");
                        Mi_SQL.Append(Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas + ", " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                        Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_No_Poliza);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_No_Poliza);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Mes_Ano);
                        Mi_SQL.Append(" = " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Mes_Ano);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID);
                        Mi_SQL.Append(" = " + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas + "." + Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID);
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID);
                        Mi_SQL.Append(" = " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                        if (!String.IsNullOrEmpty(Datos.P_No_Poliza))
                        {
                            Mi_SQL.Append(" AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_No_Poliza + "='" + Datos.P_No_Poliza + "'");
                        }
                        if (!String.IsNullOrEmpty(Datos.P_Mes_Anio))
                        {
                            Mi_SQL.Append(" AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Mes_Ano + "='" + Datos.P_Mes_Anio + "'");
                        }
                        if (!String.IsNullOrEmpty(Datos.P_Tipo_Polizas))
                        {
                            Mi_SQL.Append(" AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID + "='" + Datos.P_Tipo_Polizas + "'");
                        }
                        Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Fecha_Poliza);
                        Mi_SQL.Append(" BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00'");
                        Mi_SQL.Append(" AND '" + Datos.P_Fecha_Final + " 23:59:59'");

                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }
                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Cheques_Por_Fecha
                /// DESCRIPCION: 
                /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
                /// CREO       : Sergio Manuel Gallardo Andrade
                /// FECHA_CREO : 24-Mayo-2012
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataTable Consulta_Cheques_Por_Fecha(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar en la base de datos
                    try
                    {
                        Mi_SQL.Append("SELECT DISTINCT BANCO." + Cat_Nom_Bancos.Campo_Nombre + " FROM " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos  + " PAGO ");
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO ON PAGO." + Ope_Con_Pagos.Campo_Banco_ID);
                        Mi_SQL.Append(" = BANCO. " + Cat_Nom_Bancos.Campo_Banco_ID );
                        Mi_SQL.Append(" WHERE PAGO." + Ope_Con_Pagos.Campo_Estatus  + " != 'CANCELADO' ");
                        Mi_SQL.Append(" AND PAGO." + Ope_Con_Pagos.Campo_Forma_Pago + " = 'CHEQUE' ");
                        if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicial) && !String.IsNullOrEmpty(Datos.P_Fecha_Final))
                        {
                            Mi_SQL.Append(" AND PAGO." + Ope_Con_Pagos.Campo_Fecha_Pago);
                            Mi_SQL.Append(" BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00'");
                            Mi_SQL.Append(" AND '" + Datos.P_Fecha_Final + " 23:59:59'");
                        }
                        if (!String.IsNullOrEmpty(Datos.P_Nombre_Banco))
                        {
                            Mi_SQL.Append(" AND BANCO." + Cat_Nom_Bancos.Campo_Nombre +"='"+Datos.P_Nombre_Banco+"'");
                        }
                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }
                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Cheques_Banco_Fecha
                /// DESCRIPCION: 
                /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
                /// CREO       : Sergio Manuel Gallardo Andrade
                /// FECHA_CREO : 24-Mayo-2012
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataTable Consulta_Cheques_Banco_Fecha(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar en la base de datos
                    try
                    {
                        Mi_SQL.Append("SELECT  PAGO." + Ope_Con_Pagos.Campo_No_Pago + " , PAGO." + Ope_Con_Pagos.Campo_No_poliza + ",PAGO." + Ope_Con_Pagos.Campo_Tipo_Poliza_ID);
                        Mi_SQL.Append(", PAGO." + Ope_Con_Pagos.Campo_Mes_Ano + ", PAGO." + Ope_Con_Pagos.Campo_Fecha_Pago + ", PAGO." + Ope_Con_Pagos.Campo_Beneficiario_Pago);
                        Mi_SQL.Append(", PAGO." + Ope_Con_Pagos.Campo_No_Cheque + ", PAGO." + Ope_Con_Pagos.Campo_Usuario_Creo + ", PAGO." + Ope_Con_Pagos .Campo_No_Solicitud_Pago);
                        Mi_SQL.Append(", BANCO." + Cat_Nom_Bancos.Campo_Nombre + ", SOLICITUD." + Ope_Con_Solicitud_Pagos.Campo_Monto + " FROM " + Ope_Con_Pagos.Tabla_Ope_Con_Pagos + " PAGO ");
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + " BANCO ON PAGO." + Ope_Con_Pagos.Campo_Banco_ID);
                        Mi_SQL.Append(" = BANCO. " + Cat_Nom_Bancos.Campo_Banco_ID);
                        Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SOLICITUD ON PAGO." + Ope_Con_Pagos.Campo_No_Solicitud_Pago);
                        Mi_SQL.Append(" = SOLICITUD. " + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago);
                        Mi_SQL.Append(" WHERE PAGO." + Ope_Con_Pagos.Campo_Estatus + " != 'CANCELADO' ");
                        Mi_SQL.Append(" AND PAGO." + Ope_Con_Pagos.Campo_Forma_Pago+" ='CHEQUE' ");
                        if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicial) && !String.IsNullOrEmpty(Datos.P_Fecha_Final))
                        {
                            Mi_SQL.Append(" AND PAGO." + Ope_Con_Pagos.Campo_Fecha_Pago);
                            Mi_SQL.Append(" BETWEEN '" + Datos.P_Fecha_Inicial + " 00:00:00'");
                            Mi_SQL.Append(" AND '" + Datos.P_Fecha_Final + " 23:59:59'");
                        }
                        if (!String.IsNullOrEmpty(Datos.P_Nombre_Banco))
                        {
                            Mi_SQL.Append(" AND BANCO." + Cat_Nom_Bancos.Campo_Nombre + "='" + Datos.P_Nombre_Banco + "'");
                        }
                        Mi_SQL.Append(" ORDER BY PAGO." + Ope_Con_Pagos.Campo_Fecha_Pago);
                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }
                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Por_Nivel
                /// DESCRIPCION: Consulta las cuentas contables referente a su nivel
                /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
                /// CREO       : Sergio Manuel Gallardo Andrade
                /// FECHA_CREO : 26-mayo-2012
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataTable Consulta_Cuentas_Por_Nivel(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                    try
                    {
                        Mi_SQL.Append("SELECT * FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                        if (!String.IsNullOrEmpty(Datos.P_Nivel_Cuenta))
                        {
                            Mi_SQL.Append(" WHERE " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables  + "." + Cat_Con_Cuentas_Contables.Campo_Nivel_ID+"='"+Datos.P_Nivel_Cuenta+"'");
                            
                        }
                        Mi_SQL.Append(" ORDER BY " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }
                ///*******************************************************************************
                /// NOMBRE DE LA FUNCION: Consultar_Cuentas_Por_Genero
                /// DESCRIPCION: Consulta las cuentas contables referente a su nivel
                /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
                /// CREO       : Sergio Manuel Gallardo Andrade
                /// FECHA_CREO : 26-mayo-2012
                /// MODIFICO          :
                /// FECHA_MODIFICO    :
                /// CAUSA_MODIFICACION:
                ///*******************************************************************************
                public static DataTable Consultar_Cuentas_Por_Genero(Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Datos)
                {
                    StringBuilder Mi_SQL = new StringBuilder(); //Obtiene la consulta a realizar a la base de datos
                    try
                    {
                        Mi_SQL.Append("SELECT " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + ", ");
                        Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Descripcion + ", ");
                        Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Nivel_ID + ", ");
                        Mi_SQL.Append(Cat_Con_Niveles.Tabla_Cat_Con_Niveles + "." + Cat_Con_Niveles.Campo_Descripcion + " AS NIVEL ");
                        Mi_SQL.Append("FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " ");
                        Mi_SQL.Append("INNER JOIN " + Cat_Con_Niveles.Tabla_Cat_Con_Niveles + " ON "); 
                        Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Nivel_ID + " = ");
                        Mi_SQL.Append(Cat_Con_Niveles.Tabla_Cat_Con_Niveles + "." + Cat_Con_Niveles.Campo_Nivel_ID + " ");

                        //Mi_SQL.Append("SELECT "+Cat_Con_Cuentas_Contables.Campo_Cuenta+", "+ Cat_Con_Cuentas_Contables.Campo_Descripcion);
                        //Mi_SQL.Append(", "+ Cat_Con_Cuentas_Contables.Campo_Nivel_ID);
                        //Mi_SQL.Append(" FROM "+ Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);

                        if(!String.IsNullOrEmpty(Datos.P_Cuenta_Inicial) && !String.IsNullOrEmpty(Datos.P_Cuenta_Final))
                        {
                            Mi_SQL.Append(" WHERE " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " >= ");
                            Mi_SQL.Append("'" + Datos.P_Cuenta_Inicial + "' AND "); 
                            Mi_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta + " < '" + Datos.P_Cuenta_Final + "'");
                        }
                        Mi_SQL.Append(" ORDER BY " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                        return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    }
                    catch (SqlException Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }

                    catch (DBConcurrencyException Ex)
                    {
                        throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception("Error: " + Ex.Message);
                    }
                    finally
                    {
                    }
                }
            #endregion
        #endregion
    }
}