﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Generar_Reserva_Nomina.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;
using JAPAMI.Administrar_Requisiciones.Negocios;
using System.Data.SqlClient;
using System.Text;
using JAPAMI.Manejo_Presupuesto.Datos;

namespace JAPAMI.Generar_Reserva_Nomina.Datos
{
    public class Cls_Ope_Con_Reserva_Nomina_Datos
    {
        #region CONSULTAS / FTE FINANCIAMIENTO, PROYECTOS, PARTIDAS, PRESUPUESTOS
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Fuentes_Financiamiento
        ///DESCRIPCIÓN: crea una sentencia sql para Consultar_Fuentes_Financiamiento
        ///PARAMETROS: 1.-Clase de Negocio
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 25/Ene/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        public static DataTable Consultar_Fuentes_Financiamiento(Cls_Ope_Con_Reserva_Nomina_Negocio Reserva_Negocio)
        {
            try
            {
                String Mi_SQL = "";
                Mi_SQL =
                "SELECT FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + "," +
                " FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+" +
                " FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion +
                " FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FUENTE" +
                " JOIN " + Cat_SAP_Det_Fte_Dependencia.Tabla_Cat_SAP_Det_Fte_Financiamiento_Dependencia + " DETALLE" +
                " ON FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = " +
                " DETALLE." + Cat_SAP_Det_Fte_Dependencia.Campo_Fuente_Financiamiento_ID +
                " WHERE DETALLE." + Cat_SAP_Det_Fte_Dependencia.Campo_Dependencia_ID + " = " +
                "'" + Reserva_Negocio.P_Dependencia_ID + "'" +
                " ORDER BY " + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " ASC";
                //" AND FUENTE." + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID + " IN " +
                //"(SELECT " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID + 
                //" FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                //" WHERE " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + " = " +
                //"'" + Reserva_Negocio.P_Dependencia_ID + "'" + ")" +
                
                DataTable Data_Table =
                    SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Data_Table;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Proyectos_Programas
        ///DESCRIPCIÓN: crea una sentencia sql para Consultar_Proyectos_Programas
        ///PARAMETROS: 1.-Clase de Negocio
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 25/Ene/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************  
        public static DataTable Consultar_Proyectos_Programas(Cls_Ope_Con_Reserva_Nomina_Negocio Reserva_Negocio)
        {
            String Mi_SQL = "";
            Mi_SQL =
            "SELECT PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + "," +
            " PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Clave + " +' '+" +
            " PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Nombre + "," +
            " PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Elemento_PEP +
            " FROM " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + " PROGRAMA" +            
            " JOIN " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia + " DETALLE" +
            " ON PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + " = " +
            " DETALLE." + Cat_SAP_Det_Prog_Dependencia.Campo_Proyecto_Programa_ID +
            " WHERE DETALLE." + Cat_SAP_Det_Prog_Dependencia.Campo_Dependencia_ID + " = " +
            "'" + Reserva_Negocio.P_Dependencia_ID + "' ORDER BY " + 
            Cat_Com_Proyectos_Programas.Campo_Descripcion + " ASC";
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Programas_Dependencia
        ///DESCRIPCIÓN: crea una sentencia sql para Consultar_Proyectos_Programas
        ///PARAMETROS: 1.-Clase de Negocio
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 19-julio-2012 
        ///MODIFICO:
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************  
        public static DataTable Consultar_Programas_Dependencia(Cls_Ope_Con_Reserva_Nomina_Negocio Reserva_Negocio)
        {
            String Mi_SQL = "";
            String anio = String.Format("{0:yyyy}", DateTime.Now);
            Mi_SQL = "SELECT PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + ", PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Clave + " +' '+";
            Mi_SQL += " PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Nombre + " AS DESCRIPCION , PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Elemento_PEP;
            Mi_SQL += " FROM " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + " PROGRAMA";
            Mi_SQL += " INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTO  ON PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + "=PROGRAMA.";
            Mi_SQL += Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID;
            Mi_SQL += " INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDA  ON PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "=PRESUPUESTO.";
            Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;
            Mi_SQL += " WHERE PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + "='" + Reserva_Negocio.P_Dependencia_ID + "'";
            Mi_SQL += " AND PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Cuenta_Contable_ID + "='" + Reserva_Negocio.P_Partida_ID+ "'";
            Mi_SQL += " AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + "='" + anio + "'";
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Dependencia_Partida
        ///DESCRIPCIÓN: crea una sentencia sql para Consultar_Proyectos_Programas
        ///PARAMETROS: 1.-Clase de Negocio
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 19-julio-2012 
        ///MODIFICO:
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************  
        public static DataTable Consultar_Dependencia_Partida(Cls_Ope_Con_Reserva_Nomina_Negocio Reserva_Negocio)
        {
            String Mi_SQL = "";
            String anio = String.Format("{0:yyyy}", DateTime.Now);
            Mi_SQL = "SELECT DISTINCT DEPENDENCIA." + Cat_Dependencias.Campo_Dependencia_ID + ", DEPENDENCIA." + Cat_Dependencias.Campo_Nombre+", PARTIDA."+Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
            Mi_SQL += " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIA";
            Mi_SQL += " INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTO  ON PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + "=DEPENDENCIA.";
            Mi_SQL += Cat_Dependencias.Campo_Dependencia_ID;
            Mi_SQL += " INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDA  ON PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "=PRESUPUESTO.";
            Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID;
            Mi_SQL += " WHERE DEPENDENCIA." + Cat_Dependencias.Campo_Estatus + "='ACTIVO'";
            Mi_SQL += " AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + "='" + anio + "'";
            Mi_SQL += " AND PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Cuenta_Contable_ID + "='" + Reserva_Negocio.P_Partida_ID + "' ORDER BY DEPENDENCIA." + Cat_Dependencias.Campo_Nombre;
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Disponible_Partida
        ///DESCRIPCIÓN: crea una sentencia sql para Consultar_Proyectos_Programas
        ///PARAMETROS: 1.-Clase de Negocio
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 19-julio-2012 
        ///MODIFICO:
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************  
        public static DataTable Consultar_Disponible_Partida(Cls_Ope_Con_Reserva_Nomina_Negocio Reserva_Negocio)
        {
            String anio=String.Format("{0:yyyy}",DateTime.Now);
            String Mi_SQL = "";
            Mi_SQL = "SELECT  PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID +" AS FUENTE";
            Mi_SQL += " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTO";
            Mi_SQL += " INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDA  ON PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + "=PARTIDA.";
            Mi_SQL += Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
            Mi_SQL += " WHERE PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + "='" + Reserva_Negocio.P_Proyecto_Programa_ID+ "'";
            Mi_SQL += " AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID  + "='" + Reserva_Negocio.P_Dependencia_ID + "'";
            Mi_SQL += " AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio   + "='"+anio+"'";
            Mi_SQL += " AND PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Cuenta_Contable_ID  + "='" + Reserva_Negocio.P_Partida_ID + "'";
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas_De_Un_Programa
        ///DESCRIPCIÓN          : crea una sentencia sql para Consultar_Partidas_De_Un_Programa
        ///PARAMETROS           : 1.-Clase de Negocio
        ///CREO                 : Gustavo Angeles Cruz
        ///FECHA_CREO           : 25/Ene/2011 
        ///MODIFICO             : Leslie Gonzalez Vazquez
        ///FECHA_MODIFICO       : 20/Enero/2012
        ///CAUSA_MODIFICACIÓN   : Modifique la cosulta para solo obtener las partidas con presupuesto aprobado
        ///*******************************************************************************  
        //CONSULTAR PARTIDAS DE UN PROGRAMA
        public static DataTable Consultar_Partidas_De_Un_Programa(Cls_Ope_Con_Reserva_Nomina_Negocio Reservas_Negocio) 
        {
            StringBuilder Mi_Sql = new StringBuilder();
            Mi_Sql.Append(" SELECT DISTINCT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", (");
            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + "+ ' - ' + ");
            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ") AS NOMBRE, ");
            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible);
            Mi_Sql.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
            Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
            Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
            Mi_Sql.Append(" = '" + Reservas_Negocio.P_Anio_Presupuesto + "'");
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
            Mi_Sql.Append(" = '" + Reservas_Negocio.P_Dependencia_ID + "'");
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
            Mi_Sql.Append(" = '" + Reservas_Negocio.P_Fuente_Financiamiento + "'");
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
            Mi_Sql.Append(" = '" + Reservas_Negocio.P_Proyecto_Programa_ID + "'");
            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + " > 0");
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " > 0");

            if(!String.IsNullOrEmpty(Reservas_Negocio.P_Partida_ID))
            {
                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                Mi_Sql.Append(" = '" + Reservas_Negocio.P_Partida_ID + "'");
            }

            Mi_Sql.Append(" ORDER BY (" +Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + "+ ' - ' + ");
            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre+")");
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            return Data_Table;
        }

        //OBTINE PARTIDAS ESPECIFICAS CON PRESPUESTOS A PARTIR DE LA DEPENDENCIA Y EL PROYECTO
        public static DataTable Consultar_Presupuesto_Partidas(Cls_Ope_Con_Reserva_Nomina_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "";

            Mi_SQL = 
            "SELECT " +
            Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ", " +
                "(SELECT " + Cat_Com_Partidas.Campo_Nombre + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " + 
                Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + 
                Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") NOMBRE, " +

                "(SELECT " + Cat_Com_Partidas.Campo_Clave + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " +
                Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") CLAVE, " +

                "(SELECT " + Cat_Com_Partidas.Campo_Clave + " +' '+" + 
                Cat_Com_Partidas.Campo_Nombre + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " +
                Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") CLAVE_NOMBRE, " +
            Cat_Com_Dep_Presupuesto.Campo_Monto_Presupuestal + ", " +
            Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " MONTO_DISPONIBLE, " +
            Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + ", " +
            Cat_Com_Dep_Presupuesto.Campo_Monto_Ejercido + ", " +
            Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto + ", " +
            Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ", " +
            Cat_Com_Dep_Presupuesto.Campo_Fecha_Creo + 
            //" TO_CHAR(FECHA_CREO ,'DD/MM/YY') FECHA_CREO" + 
            " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
            " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID + 
            " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + 
            " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
            " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
            " IN (" + Requisicion_Negocio.P_Partida_ID + ")" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
            " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +
            " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + " = " +
                "(SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
                " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + 
                " WHERE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                            Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                            " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                            Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
                            " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID + 
                            " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +

                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                            " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +

                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                            " IN (" + Requisicion_Negocio.P_Partida_ID + "))";

            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }
        #endregion                             

        #region CONSULTA RESERVAS
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Reservas
        ///DESCRIPCIÓN: crea una sentencia sql para conultar una Requisa en la base de datos
        ///PARAMETROS: 1.-Clase de Negocio
        ///            2.-Usuario que crea la requisa
        ///CREO: Silvia Morales Portuhondo
        ///FECHA_CREO: Noviembre/2010 
        ///MODIFICO:Gustavo Angeles Cruz
        ///FECHA_MODIFICO: 25/Ene/2011
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        public static DataTable Consultar_Reservas(Cls_Ope_Con_Reserva_Nomina_Negocio Reserva_Negocio)
        {

            String Mi_Sql = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Dt_Oracle = new SqlDataAdapter();
            DataSet Ds_Oracle = new DataSet();
            DataTable Dt_Detalle_Solicitud = new DataTable();
            if (Reserva_Negocio.P_Cmmd != null)
            {
                Cmmd = Reserva_Negocio.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_Sql = "SELECT " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ".*, " +
                "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " +
                Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Dependencia_ID +
                ") NOMBRE_DEPENDENCIA " +
            " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                if (!string.IsNullOrEmpty(Reserva_Negocio.P_Dependencia_ID) && Reserva_Negocio.P_Dependencia_ID != "0")
                {
                    Mi_Sql += " WHERE " + Ope_Psp_Reservas.Campo_Dependencia_ID +
                    " = '" + Reserva_Negocio.P_Dependencia_ID + "'";
                    Mi_Sql += " AND " + Ope_Psp_Reservas.Campo_Fecha + "" +
                            " >= '" + Reserva_Negocio.P_Fecha_Inicial + "' AND " +
                        Ope_Psp_Reservas.Campo_Fecha + 
                            " <= '" + Reserva_Negocio.P_Fecha_Final + "'";
                }
                if (!string.IsNullOrEmpty(Reserva_Negocio.P_No_Reserva))
                {
                    if (string.IsNullOrEmpty(Reserva_Negocio.P_Dependencia_ID))
                    {
                        Mi_Sql +=
                    " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva +
                    " = " + Reserva_Negocio.P_No_Reserva;
                    }
                    else
                    {
                        Mi_Sql +=
                    " AND " + Ope_Psp_Reservas.Campo_No_Reserva +
                    " = " + Reserva_Negocio.P_No_Reserva;
                    }
                }
                Mi_Sql = Mi_Sql + " ORDER BY " + Ope_Psp_Reservas.Campo_No_Reserva + " DESC";
                Cmmd.CommandText = Mi_Sql.ToString(); //Realiza la ejecuón de la obtención del ID del empleado
                Dt_Oracle.SelectCommand = Cmmd;
                Dt_Oracle.Fill(Ds_Oracle);
                DataSet Data_Set = Ds_Oracle;
                if (Reserva_Negocio.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                //DataSet Data_Set = OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0)
                {
                    return (Data_Set.Tables[0]);
                }
                else
                {
                    return null;
                }
            }
            catch (SqlException Ex)
            {
                if (Reserva_Negocio.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Reserva_Negocio.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Reserva
        /// DESCRIPCION : Modifica la reserva seleccionado
        /// PARAMETROS  : Datos: Contiene los datos proporcionados
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 22/Noviembre/2011
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static void Modificar_Reserva(Cls_Ope_Con_Reserva_Nomina_Negocio  Datos)
        {
            try
            {
                String Mi_SQL;  //Almacena la sentencia de modificacion.

                Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ";
                Mi_SQL += Ope_Psp_Reservas.Campo_Estatus + " = '" + Datos.P_Estatus + "', ";
                Mi_SQL += Ope_Psp_Reservas.Campo_Saldo + " = '0', ";
                Mi_SQL += Ope_Psp_Reservas.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Modifico + "',";
                Mi_SQL += Ope_Psp_Reservas.Campo_Fecha_Modifico + " =GETDATE()";
                Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + Datos.P_No_Reserva + "'";

                Datos.P_Cmmd.CommandText = Mi_SQL;
                Datos.P_Cmmd.ExecuteNonQuery();
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message, ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Reservas_Detalladas
        ///DESCRIPCIÓN          : Consulta para obtenes los datos de las reservas y sus detalles
        ///PARAMETROS           : 1.-Clase de Negocio
        ///CREO                 : Leslie Gonzalez Vazquez
        ///FECHA_CREO           : 23/Enero/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************  
        public static DataTable Consultar_Reservas_Detalladas(Cls_Ope_Con_Reserva_Nomina_Negocio Reservas_Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
           
            try
            {
                Mi_Sql.Append(" SELECT " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " AS FUENTE_FINANCIAMIENTO_ID, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " AS PROGRAMA_ID, ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA, ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FUENTE_FINANCIAMIENTO, ");
                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS PROGRAMA, ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Empleado_ID + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Proveedor_ID + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Importe_Inicial + " AS TOTAL, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ", ");
                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " AS IMPORTE, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Saldo + " AS SALDO, ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " AS ANIO, ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Estatus + " AS ESTATUS ");
                Mi_Sql.Append(" FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                Mi_Sql.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID);
                Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID);
                Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID);
                Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                Mi_Sql.Append(" INNER JOIN " + Ope_Nom_Reserva_Nomina.Tabla_Ope_Nom_Reserva_Nomina);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio);
                Mi_Sql.Append(" = " + Ope_Nom_Reserva_Nomina.Tabla_Ope_Nom_Reserva_Nomina + "." + Ope_Nom_Reserva_Nomina.Campo_Anio);

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_No_Reserva))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                        Mi_Sql.Append(" = " + Reservas_Negocio.P_No_Reserva + "");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                        Mi_Sql.Append(" = " + Reservas_Negocio.P_No_Reserva + "");
                    }
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Anio_Presupuesto))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio);
                        Mi_Sql.Append(" = " + Reservas_Negocio.P_Anio_Presupuesto + "");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio);
                        Mi_Sql.Append(" = " + Reservas_Negocio.P_Anio_Presupuesto + "");
                    }
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Fecha_Inicial) && !String.IsNullOrEmpty(Reservas_Negocio.P_Fecha_Final))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Fecha);
                        Mi_Sql.Append(" BETWEEN '" + Reservas_Negocio.P_Fecha_Inicial + " 00:00:00'");
                        Mi_Sql.Append(" AND '" + Reservas_Negocio.P_Fecha_Final + " 23:59:00'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Fecha);
                        Mi_Sql.Append(" BETWEEN '" + Reservas_Negocio.P_Fecha_Inicial + " 00:00:00'");
                        Mi_Sql.Append(" AND '" + Reservas_Negocio.P_Fecha_Final + " 23:59:00'");
                    }
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Dependencia_ID))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Dependencia_ID + ")");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Dependencia_ID + ")");
                    }
                }
                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Grupo_Dependencia))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Grupo_Dependencia + ")");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Grupo_Dependencia + ")");
                    }
                }

                Mi_Sql.Append(" ORDER BY " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + " ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los datos de las reservas. Error[" + Ex.Message + "]");
            }
        }
        #endregion

        #region Consulta de partidas con saldo disponible
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Reservas_Unidad_Responsable
        /// DESCRIPCION: Consulta las partidas que pertenescan a la unidad responsable buscada
        /// PARAMETROS : Datos: Valores que son pasados desde la capa de negocios
        /// CREO       : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO : 28-Febrero-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Reservas_Unidad_Responsable(Cls_Ope_Con_Reserva_Nomina_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable que tendra la consulta a realizar a la base de datos
            try
            {
                Mi_SQL.Append("SELECT  PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");

                Mi_SQL.Append(" (SELECT RTRIM(" + Cat_Dependencias.Campo_Clave + ") + '-' + " + Cat_Dependencias.Campo_Nombre);
                Mi_SQL.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE ");
                Mi_SQL.Append(Cat_Dependencias.Campo_Dependencia_ID + " = PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ") AS CLAVE_NOM_UR,");

                Mi_SQL.Append(" (SELECT RTRIM(" + Cat_Dependencias.Campo_Clave + ") FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_SQL.Append(" WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ") AS CLAVE_UR,");

                Mi_SQL.Append("(SELECT RTRIM(" + Cat_Sap_Partidas_Especificas.Campo_Clave + ") FROM " +
                        Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ")+ '-' +(SELECT "
                        + Cat_Sap_Partidas_Especificas.Campo_Nombre + " FROM " +
                        Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ") Clave_Partida");

                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID);
                Mi_SQL.Append(",(SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " FROM " +
                        Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ")+ '-' +(SELECT "
                        + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " FROM " +
                        Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ") Clave_Programa");
                Mi_SQL.Append(",(SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + "
                        + Cat_Sap_Proyectos_Programas .Campo_Nombre
                        + " FROM " +
                       Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                       "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ")AS Programa");
                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                Mi_SQL.Append(",(SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " FROM " +
                       Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID +
                       "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ") Clave_Financiamiento");

                Mi_SQL.Append(",PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + Datos.P_Filtro_Campo_Mes + " AS DISPONIBLE, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + Datos.P_Filtro_Campo_Mes +
                        " AS DEVENGADO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + Datos.P_Filtro_Campo_Mes + " AS EJERCIDO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + Datos.P_Filtro_Campo_Mes +
                        " AS PAGADO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe + Datos.P_Filtro_Campo_Mes +" AS APROBADO, PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + Datos.P_Filtro_Campo_Mes + " AS COMPROMETIDO, ");

                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ", ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ", ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ", ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ", ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ", ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ", ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ", ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ", ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ", ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ", ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ", ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ", ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " AS IMPORTE, ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " AS PROGRAMA_ID, ");
                Mi_SQL.Append(" PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AS FUENTE_FINANCIAMIENTO_ID ");

                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PRESUPUESTO, ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PARTIDA, ");
                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_SQL.Append(" WHERE PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "=PRESUPUESTO." +
                        Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = " +
                        "PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                Mi_SQL.Append(" AND PARTIDA." + Cat_Sap_Partidas_Especificas.Campo_Tipo_Nomina + " = 'SI' ");

                if (!String.IsNullOrEmpty(Datos.P_Dependencia_ID))
                { 
                     Mi_SQL.Append("AND PRESUPUESTO. " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                     Mi_SQL.Append(" = '" + Datos.P_Dependencia_ID + "'");
                }

                if (!String.IsNullOrEmpty(Datos.P_Proyecto_Programa_ID))
                {
                    Mi_SQL.Append("AND PRESUPUESTO. " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_SQL.Append(" = '" + Datos.P_Proyecto_Programa_ID + "'");
                }

                if (!String.IsNullOrEmpty(Datos.P_Tipo_Psp))
                {
                    Mi_SQL.Append("AND PRESUPUESTO. " + Datos.P_Tipo_Psp.Trim());
                    Mi_SQL.Append(" > 0");
                }

                if (!String.IsNullOrEmpty(Datos.P_Anio_Presupuesto))
                {
                    Mi_SQL.Append(" AND PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Datos.P_Anio_Presupuesto);
                }

                if (!String.IsNullOrEmpty(Datos.P_Comentarios))
                {
                    Mi_SQL.Append(" AND (SELECT RTRIM(" + Cat_Sap_Partidas_Especificas.Campo_Clave + ") + '-' + "
                        + Cat_Sap_Partidas_Especificas.Campo_Nombre + " FROM " +
                        Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID +
                        "=PRESUPUESTO." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ") LIKE '%" + Datos.P_Comentarios.Trim() + "%'");
                }
                Mi_SQL.Append(" ORDER BY Clave_Partida");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }

            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        #endregion

        #region Actualizar Tipo Reserva
        /// ********************************************************************************************************************
        /// NOMBRE: Modificar_Tipo_Reserva
        /// COMENTARIOS: Esta operación actualiza el tipo de reserva
        /// PARÁMETROS: Datos.- Clase de negocios
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  09/Marzo/2012
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static Boolean Modificar_Tipo_Reserva(Cls_Ope_Con_Reserva_Nomina_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;

            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ");
                Mi_SQL.Append(Ope_Psp_Reservas.Campo_Tipo_Reserva + "='" + Datos.P_Tipo_Reserva + "' ");
                if(!String.IsNullOrEmpty(Datos.P_No_Deuda))
                {
                    Mi_SQL.Append(", NO_DEUDA='" + Datos.P_No_Deuda + "'");
                }
                Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + Datos.P_No_Reserva);

                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                //OracleHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
            return Operacion_Completa;
        }

        #endregion

        #region (Reserva Nomina)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Dependencias
            ///DESCRIPCIÓN          : consulta para obtener los datos de las dependencias
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 27/Abril/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Dependencias(Cls_Ope_Con_Reserva_Nomina_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT DISTINCT UPPER(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ") AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + " ");
                    Mi_Sql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_Sql.Append(" = " + DateTime.Now.Year);

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Proyecto_Programa_ID))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Proyecto_Programa_ID.Trim() + "'");
                    }

                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las depencencias. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Anio_Presupuesto
            ///DESCRIPCIÓN          : consulta para obtener los datos del año del presupuesto
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Abril/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Anio_Presupuesto()
            {
                StringBuilder Mi_Sql = new StringBuilder();
                
                try
                {
                    Mi_Sql.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" ORDER BY " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " DESC ");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los años presupuestados. Error: [" + Ex.Message + "]");
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Reserva_Nomina_Anio
            ///DESCRIPCIÓN          : consulta para obtener los datos del año del presupuesto
            ///PARAMETROS           : 
            ///CREO                 : Sergio Manuel Gallardo Andrade
            ///FECHA_CREO           : 29/Abril/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static String Consultar_Reserva_Nomina_Anio(Cls_Ope_Con_Reserva_Nomina_Negocio Datos)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                
                try
                {
                    Mi_Sql.Append("SELECT " + Ope_Nom_Reserva_Nomina.Campo_No_Reserva);
                    Mi_Sql.Append(" FROM " + Ope_Nom_Reserva_Nomina.Tabla_Ope_Nom_Reserva_Nomina);
                    if (!String.IsNullOrEmpty(Datos.P_Anio_Presupuesto))
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Nom_Reserva_Nomina.Campo_Anio+" = "+Datos.P_Anio_Presupuesto);
                    }
                    return SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).ToString();
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los años presupuestados. Error: [" + Ex.Message + "]");
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Reserva_Nomina
            ///DESCRIPCIÓN          : consulta para obtener los datos de la reserva de nomina
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Abril/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Reserva_Nomina()
            {
                StringBuilder Mi_Sql = new StringBuilder();
                
                try
                {
                    Mi_Sql.Append("SELECT " + Ope_Nom_Reserva_Nomina.Tabla_Ope_Nom_Reserva_Nomina + "." + Ope_Nom_Reserva_Nomina.Campo_Anio + ", ");
                    Mi_Sql.Append(Ope_Nom_Reserva_Nomina.Tabla_Ope_Nom_Reserva_Nomina + "." + Ope_Nom_Reserva_Nomina.Campo_No_Reserva + ", ");
                    Mi_Sql.Append(Ope_Nom_Reserva_Nomina.Tabla_Ope_Nom_Reserva_Nomina + "." + Ope_Nom_Reserva_Nomina.Campo_No_Consecutivo + ", ");
                    Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto );
                    Mi_Sql.Append(" FROM " + Ope_Nom_Reserva_Nomina.Tabla_Ope_Nom_Reserva_Nomina);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                    Mi_Sql.Append(" ON " + Ope_Nom_Reserva_Nomina.Tabla_Ope_Nom_Reserva_Nomina + "." + Ope_Nom_Reserva_Nomina.Campo_No_Reserva);
                    Mi_Sql.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                    Mi_Sql.Append(" ORDER BY " + Ope_Nom_Reserva_Nomina.Tabla_Ope_Nom_Reserva_Nomina + "." + Ope_Nom_Reserva_Nomina.Campo_Anio + " DESC ");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los datos de las reservas de nomina. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Afectar_Presupuesto_Nomina
            ///DESCRIPCIÓN          : consulta para hacer los movimientos presupuestales de la reserva de nomina
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Abril/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static int Afectar_Presupuesto_Nomina(Cls_Ope_Con_Reserva_Nomina_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                int No_Registros = 0;
                SqlConnection Conexion = new SqlConnection();
                SqlCommand Comando = new SqlCommand();
                SqlTransaction Transaccion = null;
                Int32 No = 0;

                if (Negocio.P_Cmmd != null)
                {
                    Comando = Negocio.P_Cmmd;
                }
                else
                {
                    Conexion.ConnectionString = Cls_Constantes.Str_Conexion;
                    Conexion.Open();
                    Transaccion = Conexion.BeginTransaction();
                    Comando.Connection = Transaccion.Connection;
                    Comando.Transaction = Transaccion;
                }

                try
                {
                    //OBTENEMOS EL NUMERO DE CONSECUTIVO DE LA RESERVA DE NOMINA
                    Mi_Sql.Append("SELECT ISNULL(MAX(" + Ope_Nom_Reserva_Nomina.Campo_No_Consecutivo +"), 0)");
                    Mi_Sql.Append(" FROM " + Ope_Nom_Reserva_Nomina.Tabla_Ope_Nom_Reserva_Nomina);

                    Comando.CommandText = Mi_Sql.ToString().Trim();
                    No = Convert.ToInt32(Comando.ExecuteScalar());
                    No++;

                    //GUARAMOS LOS DATOS DE LA RESERVA
                    Mi_Sql = new StringBuilder();
                    Mi_Sql.Append("INSERT INTO " + Ope_Nom_Reserva_Nomina.Tabla_Ope_Nom_Reserva_Nomina + "(");
                    Mi_Sql.Append(Ope_Nom_Reserva_Nomina.Campo_No_Consecutivo + ", " );
                    Mi_Sql.Append(Ope_Nom_Reserva_Nomina.Campo_No_Reserva + ", ");
                    Mi_Sql.Append(Ope_Nom_Reserva_Nomina.Campo_Anio + ", ");
                    Mi_Sql.Append(Ope_Nom_Reserva_Nomina.Campo_Usuario_Creo + ", ");
                    Mi_Sql.Append(Ope_Nom_Reserva_Nomina.Campo_Fecha_Creo + ") VALUES( ");
                    Mi_Sql.Append(No.ToString().Trim() + ", ");
                    Mi_Sql.Append(Negocio.P_No_Reserva + ", ");
                    Mi_Sql.Append(Negocio.P_Anio_Presupuesto + ", ");
                    Mi_Sql.Append("'" + Cls_Sessiones.Nombre_Empleado.Trim() + "', ");
                    Mi_Sql.Append(" GETDATE())");

                    Comando.CommandText = Mi_Sql.ToString().Trim();
                    Comando.ExecuteNonQuery();

                    if (Negocio.P_Dt_Detelles != null)
                    {
                        if (Negocio.P_Dt_Detelles.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Negocio.P_Dt_Detelles.Rows)
                            {
                                //ACTUALIZAMOS LOS DATOS DEL PRESUPUESTO
                                Mi_Sql = new StringBuilder();
                                Mi_Sql.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                Mi_Sql.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Enero + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Febrero + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Marzo + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Abril + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Mayo + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Junio + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Julio + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Agosto + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Septiembre + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Octubre + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Noviembre + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Diciembre + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = 0, ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = 0, ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = 0, ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = 0, ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = 0, ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = 0, ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = 0, ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = 0, ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = 0, ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = 0, ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = 0, ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = 0, ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = 0, ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Tipo + " = 'RESERVA_NOMINA' ");
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Negocio.P_Anio_Presupuesto);
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "' ");
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["DEPENDENCIA_ID"].ToString().Trim() + "' ");
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "' ");

                                Comando.CommandText = Mi_Sql.ToString().Trim();
                                No_Registros += Comando.ExecuteNonQuery();
                            }
                        }
                    }

                    if (Negocio.P_Cmmd == null)
                    {
                        Transaccion.Commit();
                    }
                }
                catch (Exception Ex)
                {
                    Transaccion.Rollback();
                    throw new Exception("Error al intentar guardar los datos de las reservas de nomina. Error: [" + Ex.Message + "]");
                }
                finally
                {
                    if (Negocio.P_Cmmd == null)
                    {
                        Conexion.Close();
                        Comando = null;
                        Conexion = null;
                        Transaccion = null;
                    }
                }
                return No_Registros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Programas
            ///DESCRIPCIÓN          : consulta para obtener los programas
            ///PARAMETROS           1 Negocio conexion con la capa de negocio 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 17/Septiembre/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consultar_Programa(Cls_Ope_Con_Reserva_Nomina_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                DataTable Dt_Programas = new DataTable();
                try
                {
                    Mi_Sql.Append("SELECT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ", ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS NOMBRE ");
                    Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Estatus + " = 'ACTIVO'");

                    if (!String.IsNullOrEmpty(Negocio.P_Proyecto_Programa_ID.Trim()))
                    {
                        Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_Sql.Append(" = '" + Negocio.P_Proyecto_Programa_ID + "'");
                    }

                    Mi_Sql.Append(" ORDER BY NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0]; ;
                }
                catch (Exception Ex)
                {
                    Ex.ToString();
                    return null;
                }
            }
        #endregion

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Reservas_Nomina_Detalladas
        ///DESCRIPCIÓN          : Consulta para obtenes los datos de las reservas y sus detalles
        ///PARAMETROS           : 1.-Clase de Negocio
        ///CREO                 : Sergio Manuel Gallardo
        ///FECHA_CREO           : 30/Abril/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************  
        public static DataTable Consultar_Reservas_Nomina_Detalladas(Cls_Ope_Con_Reserva_Nomina_Negocio Reservas_Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            String Mes = String.Format("{0:MM}", DateTime.Now);
            String Mes_Disponible = String.Empty;

            try
            {
                Mes_Disponible = Obtener_Columna(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, Mes);

                Mi_Sql.Append(" SELECT " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " AS FUENTE_FINANCIAMIENTO_ID, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " AS PROGRAMA_ID, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Capitulo_ID + " AS CAPITULO_ID, ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA, ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FUENTE_FINANCIAMIENTO, ");
                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS PROGRAMA, ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Beneficiario + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Empleado_ID + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Proveedor_ID + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Concepto + ", ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Importe_Inicial + " AS TOTAL, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ", ");
                Mi_Sql.Append(" RTRIM(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ") + ' - ' + ");
                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " AS IMPORTE, ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Saldo + " AS SALDO, ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Anio + " AS ANIO, ");
                Mi_Sql.Append(Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Estatus + " AS ESTATUS ");
                Mi_Sql.Append(" FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                Mi_Sql.Append(" = " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID);
                Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID);
                Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID);
                Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                Mi_Sql.Append(" ON " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_No_Reserva))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                        Mi_Sql.Append(" = " + Reservas_Negocio.P_No_Reserva + "");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva);
                        Mi_Sql.Append(" = " + Reservas_Negocio.P_No_Reserva + "");
                    }
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Fecha_Inicial) && !String.IsNullOrEmpty(Reservas_Negocio.P_Fecha_Final))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Fecha);
                        Mi_Sql.Append(" BETWEEN '" + Reservas_Negocio.P_Fecha_Inicial + " 00:00:00'");
                        Mi_Sql.Append(" AND '" + Reservas_Negocio.P_Fecha_Final + " 23:59:00'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_Fecha);
                        Mi_Sql.Append(" BETWEEN '" + Reservas_Negocio.P_Fecha_Inicial + " 00:00:00'");
                        Mi_Sql.Append(" AND '" + Reservas_Negocio.P_Fecha_Final + " 23:59:00'");
                    }
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Dependencia_ID))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Dependencia_ID + ")");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Dependencia_ID + ")");
                    }
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Proyecto_Programa_ID))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID);
                        Mi_Sql.Append(" = '" + Reservas_Negocio.P_Proyecto_Programa_ID + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID);
                        Mi_Sql.Append(" = '" + Reservas_Negocio.P_Proyecto_Programa_ID + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Grupo_Dependencia))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Grupo_Dependencia + ")");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                        Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Grupo_Dependencia + ")");
                    }
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Comentarios))
                {
                    if (Mi_Sql.ToString().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND  RTRIM(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ") + ' - ' + ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " LIKE ");
                        Mi_Sql.Append(" '%" + Reservas_Negocio.P_Comentarios + "%'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE RTRIM(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ") + ' - ' + ");
                        Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " LIKE ");
                        Mi_Sql.Append(" '%" + Reservas_Negocio.P_Comentarios + "%'");
                    }
                }

                Mi_Sql.Append(" UNION ");

                Mi_Sql.Append(" SELECT " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AS FUENTE_FINANCIAMIENTO_ID, ");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " AS PROGRAMA_ID, ");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " AS CAPITULO_ID, ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA, ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FUENTE_FINANCIAMIENTO, ");
                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' - ' + ");
                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS PROGRAMA, ");
                Mi_Sql.Append(" '' AS NO_RESERVA, ");
                Mi_Sql.Append(" '' AS BENEFICIARIO, ");
                Mi_Sql.Append(" '' AS EMPLEADO_ID, ");
                Mi_Sql.Append(" '' AS PROVEEDOR_ID, ");
                Mi_Sql.Append(" '' AS CONCEPTO, ");
                Mi_Sql.Append(" 0 AS TOTAL, ");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                Mi_Sql.Append(" RTRIM(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ") + ' - ' + ");
                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA, ");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Mes_Disponible + " AS IMPORTE, ");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Mes_Disponible + " AS SALDO, ");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " AS ANIO, ");
                Mi_Sql.Append("(SELECT DISTINCT " + Ope_Psp_Reservas.Campo_Estatus + " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas +
                    " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + Reservas_Negocio.P_No_Reserva  + ") AS ESTATUS ");
                Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                Mi_Sql.Append(" = " + String.Format("{0:yyyy}", DateTime.Now));
                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Mes_Disponible + " > 0 ");
                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " = '00001' ");

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Dependencia_ID))
                {
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Dependencia_ID + ")");
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Proyecto_Programa_ID))
                {
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = '" + Reservas_Negocio.P_Proyecto_Programa_ID + "'");
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Grupo_Dependencia))
                {
                    Mi_Sql.Append(" ADN " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                    Mi_Sql.Append(" IN(" + Reservas_Negocio.P_Grupo_Dependencia + ")");
                }

                if (!String.IsNullOrEmpty(Reservas_Negocio.P_Comentarios))
                {
                    Mi_Sql.Append(" AND  RTRIM(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ") + ' - ' + ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " LIKE ");
                    Mi_Sql.Append(" '%" + Reservas_Negocio.P_Comentarios + "%'");
                }

                Mi_Sql.Append(" AND (");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " + '_' + ");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " + '_' + ");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " + '_' + ");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ") ");
                Mi_Sql.Append(" NOT IN (");
                Mi_Sql.Append(" SELECT " + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " + '_' + ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " + '_' + ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + " + '_' + ");
                Mi_Sql.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID );
                Mi_Sql.Append(" FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                Mi_Sql.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva);
                Mi_Sql.Append(" = " + Reservas_Negocio.P_No_Reserva + ")");

                Mi_Sql.Append(" ORDER BY " + Ope_Psp_Reservas.Campo_No_Reserva + " DESC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los datos de las reservas. Error[" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Presupuesto_Egresos
        ///DESCRIPCIÓN          : consulta para actualizar los datos del presupuesto de egresos
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 12/Noviembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static String Obtener_Columna(String Momento_Presupuestal, String Mes)
        {
            String Columna = String.Empty;

            try
            {
                if (Momento_Presupuestal.Trim().Equals(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible.Trim()))
                {
                    #region (Disponible)
                    switch (Mes)
                    {
                        case "01":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero;
                            break;
                        case "02":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero;
                            break;
                        case "03":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo;
                            break;
                        case "04":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril;
                            break;
                        case "05":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo;
                            break;
                        case "06":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio;
                            break;
                        case "07":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio;
                            break;
                        case "08":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto;
                            break;
                        case "09":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre;
                            break;
                        case "10":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre;
                            break;
                        case "11":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre;
                            break;
                        case "12":
                            Columna = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre;
                            break;
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener el nombre de la columna que se afectara. Error[" + ex.Message + "]");
            }
            return Columna;
        }

        /// ********************************************************************************************************************
        /// NOMBRE: Modificar_Saldos_Reserva
        /// COMENTARIOS: Esta operación actualiza el tipo de reserva
        /// PARÁMETROS: Datos.- Clase de negocios
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  09/Marzo/2012
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static void Modificar_Saldos_Reserva(Cls_Ope_Con_Reserva_Nomina_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            Int32 No_Reserva_Detalle;
            SqlDataAdapter Da_Datos = new SqlDataAdapter();
            DataTable Dt_Datos = new DataTable();
            DataSet Ds_Datos = new DataSet();

            if (Datos.P_Cmmd != null)
            {
                Cmmd = Datos.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            try
            {
                Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ");
                if (Datos.P_Tipo_Modificacion == "SUPLEMENTO")
                {
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Importe_Inicial + "=" + Ope_Psp_Reservas.Campo_Importe_Inicial + " + " + Datos.P_Importe + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Saldo + "=" + Ope_Psp_Reservas.Campo_Saldo + " + " + Datos.P_Importe + ", ");
                }
                else
                {
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Importe_Inicial + "=" + Ope_Psp_Reservas.Campo_Importe_Inicial + " - " + Datos.P_Importe + ", ");
                    Mi_SQL.Append(Ope_Psp_Reservas.Campo_Saldo + "=" + Ope_Psp_Reservas.Campo_Saldo + " - " + Datos.P_Importe + ", ");
                }
                Mi_SQL.Append(Ope_Psp_Reservas.Campo_Usuario_Modifico + "='" + Cls_Sessiones.Nombre_Empleado + "', ");
                Mi_SQL.Append(Ope_Psp_Reservas.Campo_Fecha_Modifico + "= GETDATE() ");
                Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + Datos.P_No_Reserva);
                //Ejecutar consulta
                Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  


                if (Datos.P_Tipo_Modificacion == "REDUCCION")
                {
                    foreach (DataRow Partida_Reserva in Datos.P_Dt_Detelles.Rows)
                    {
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " SET ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + "=" + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " - " + Partida_Reserva["IMPORTE"].ToString() + ", ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Saldo + "=" + Ope_Psp_Reservas_Detalles.Campo_Saldo + " - " + Partida_Reserva["IMPORTE"].ToString() + ", ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Usuario_Modifico + "='" + Cls_Sessiones.Nombre_Empleado + "', ");
                        Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fecha_Modifico + "= GETDATE() ");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + "=" + Datos.P_No_Reserva);
                        Mi_SQL.Append(" AND " + Ope_Psp_Reservas_Detalles.Campo_Capitulo_ID + "='" + Partida_Reserva["CAPITULO_ID"].ToString());
                        Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + "='" + Partida_Reserva["DEPENDENCIA_ID"].ToString());
                        Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + "='" + Partida_Reserva["FUENTE_FINANCIAMIENTO_ID"].ToString());
                        Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + "='" + Partida_Reserva["PARTIDA_ID"].ToString());
                        Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + "='" + Partida_Reserva["PROGRAMA_ID"].ToString() + "'");
                        Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                        Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                    }
                }
                else
                {
                    foreach (DataRow Dr in Datos.P_Dt_Detelles.Rows)
                    {
                        Da_Datos = new SqlDataAdapter();
                        Dt_Datos = new DataTable();
                        Ds_Datos = new DataSet();

                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + "=" + Datos.P_No_Reserva);
                        Mi_SQL.Append(" AND " + Ope_Psp_Reservas_Detalles.Campo_Capitulo_ID + "='" + Dr["CAPITULO_ID"].ToString());
                        Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + "='" + Dr["DEPENDENCIA_ID"].ToString());
                        Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + "='" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString());
                        Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + "='" + Dr["PARTIDA_ID"].ToString());
                        Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + "='" + Dr["PROGRAMA_ID"].ToString() + "'");

                        Cmmd.CommandText = Mi_SQL.ToString();
                        Da_Datos = new SqlDataAdapter(Cmmd);

                        if (Da_Datos != null)
                        {
                            Da_Datos.Fill(Ds_Datos);
                            Dt_Datos = Ds_Datos.Tables[0];
                        }

                        //obtenemos si esta partida ya esta en los detalles de la reserva para modificarla
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " SET ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + "=" + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " + " + Dr["IMPORTE"].ToString() + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Saldo + "=" + Ope_Psp_Reservas_Detalles.Campo_Saldo + " + " + Dr["IMPORTE"].ToString() + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Usuario_Modifico + "='" + Cls_Sessiones.Nombre_Empleado + "', ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fecha_Modifico + "= GETDATE() ");
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + "=" + Datos.P_No_Reserva);
                            Mi_SQL.Append(" AND " + Ope_Psp_Reservas_Detalles.Campo_Capitulo_ID + "='" + Dr["CAPITULO_ID"].ToString());
                            Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + "='" + Dr["DEPENDENCIA_ID"].ToString());
                            Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + "='" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString());
                            Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + "='" + Dr["PARTIDA_ID"].ToString());
                            Mi_SQL.Append("' AND " + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + "='" + Dr["PROGRAMA_ID"].ToString() + "'");
                            Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                            Cmmd.ExecuteNonQuery();    //Ejecuta la inserción en memoria antes de pasarla a la base de datos  
                        }
                        else
                        {
                            No_Reserva_Detalle = Cls_Ope_Psp_Manejo_Presupuesto.Obtener_Consecutivo(Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle, Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles, Cmmd);

                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("INSERT INTO " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + "(");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_No_Reserva_Detalle + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_No_Reserva + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Partida_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Saldo + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Usuario_Creo + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Fecha_Creo + ", ");
                            Mi_SQL.Append(Ope_Psp_Reservas_Detalles.Campo_Capitulo_ID + ")");
                            Mi_SQL.Append(" VALUES(" + No_Reserva_Detalle + ", ");
                            Mi_SQL.Append(Datos.P_No_Reserva + ", ");
                            Mi_SQL.Append("'" + Dr["PARTIDA_ID"].ToString() + "', ");
                            Mi_SQL.Append(Dr["IMPORTE"].ToString().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMPORTE"].ToString().Replace(",", "") + ", ");
                            Mi_SQL.Append("'" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString() + "', ");
                            Mi_SQL.Append("'" + Dr["PROGRAMA_ID"].ToString() + "', ");
                            if (!String.IsNullOrEmpty(Dr["DEPENDENCIA_ID"].ToString()))
                            {
                                Mi_SQL.Append("'" + Dr["DEPENDENCIA_ID"].ToString() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(",NULL");
                            }
                            Mi_SQL.Append("'" + Cls_Sessiones.Nombre_Empleado + "', ");
                            Mi_SQL.Append("GETDATE(), ");
                            Mi_SQL.Append("'" + Dr["CAPITULO_ID"].ToString().Trim() + "')");

                            Cmmd.CommandText = Mi_SQL.ToString(); //Asigna la inserción para ser ejecutada
                            Cmmd.ExecuteNonQuery();
                        }
                    }
                }


                if (Datos.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (Datos.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }

        ///*****************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cancelar_Reserva
        ///DESCRIPCIÓN          : consulta para actualizar los datos de la cancelacion de la reserva
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 26/Agosto/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************
        public static Double Cancelar_Reserva_Nomina(String No_Reserva, SqlCommand P_Comando, DataTable Dt_Detalles, String Anio)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion = new SqlConnection();//Variable para la conexión para la base de datos   
            SqlCommand Comando = new SqlCommand();//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Double Importe = 0.00;
            String Mes_Actual = String.Empty;
            StringBuilder Mi_Sql_Complemento = new StringBuilder();
            //StringBuilder Mi_Sql_Sumatoria = new StringBuilder();
            SqlDataAdapter Da_Datos = new SqlDataAdapter();
            DataTable Dt_Datos = new DataTable();
            DataSet Ds_Datos = new DataSet();
            String Mes_Disponible = String.Empty;
            String Mes_Comprometido = String.Empty;

            if (P_Comando != null)
            {
                Comando = P_Comando;
            }
            else
            {
                Conexion.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Conexion.Open();
                Transaccion = Conexion.BeginTransaction();
                Comando.Transaction = Transaccion;
                Comando.Connection = Conexion;
            }

            try
            {
                Mes_Actual = String.Format("{0:MM}", DateTime.Now);

                //obtenemos el importe de la reserva
                Mi_SQL.Append("SELECT ISNULL(" + Ope_Psp_Reservas.Campo_Saldo + ", 0) FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas);
                Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = " + No_Reserva);
                Comando.CommandText = Mi_SQL.ToString();
                Importe = Convert.ToDouble( Comando.ExecuteScalar());

                //obtenemos todos los detalles para afectar el presupuesto
                Mi_SQL = new StringBuilder();
                Mi_SQL.Append(" SELECT RES." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID + " AS FUENTE_FINANCIAMIENTO_ID, ");
                Mi_SQL.Append(" RES." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID + " AS PROGRAMA_ID, ");
                Mi_SQL.Append(" RES." + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID + " AS DEPENDENCIA_ID, ");
                Mi_SQL.Append(" RES." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID + " AS PARTIDA_ID, ");
                Mi_SQL.Append(" RES." + Ope_Psp_Reservas_Detalles.Campo_Saldo + " AS IMPORTE, ");
                Mi_SQL.Append(" PSP." + Ope_Psp_Presupuesto_Aprobado.Campo_Tipo);
                Mi_SQL.Append(" FROM " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " RES");
                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " PSP ");
                Mi_SQL.Append(" ON RES." + Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID);
                Mi_SQL.Append(" = PSP." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                Mi_SQL.Append(" AND RES." + Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID);
                Mi_SQL.Append(" = PSP." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                Mi_SQL.Append(" AND RES." + Ope_Psp_Reservas_Detalles.Campo_Dependencia_ID);
                Mi_SQL.Append(" = PSP." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                Mi_SQL.Append(" AND RES." + Ope_Psp_Reservas_Detalles.Campo_Partida_ID);
                Mi_SQL.Append(" = PSP." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                Mi_SQL.Append(" WHERE RES." + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + No_Reserva.Trim());
                Mi_SQL.Append(" AND RES." + Ope_Psp_Reservas_Detalles.Campo_Saldo + " > 0");
                Mi_SQL.Append(" AND PSP." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio.Trim());
               
                Comando.CommandText = Mi_SQL.ToString();
                Da_Datos = new SqlDataAdapter(Comando);

                if (Da_Datos != null)
                {
                    Da_Datos.Fill(Ds_Datos);
                    Dt_Datos = Ds_Datos.Tables[0];
                }

                //MODIFICAMOS EL SALDO DE LOS DETALLES DE LA RESERVA
                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles);
                Mi_SQL.Append(" SET " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = 0 ");
                Mi_SQL.Append(" WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = " + No_Reserva);
                Comando.CommandText = Mi_SQL.ToString();
                Comando.ExecuteNonQuery();

                #region (Complemento)
                    //obtenemos el cambio de los meses que se afectaran
                    if (Convert.ToInt32(Mes_Actual) <= 1)
                    {
                        if (Convert.ToInt32(Mes_Actual) == 1)
                        {
                            Mes_Disponible = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero;
                            Mes_Comprometido = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Enero;
                        }
                        else 
                        {
                            Mi_Sql_Complemento.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Enero + " = 0.00, ");
                        }
                    }
                    if (Convert.ToInt32(Mes_Actual) <= 2)
                    {
                        if (Convert.ToInt32(Mes_Actual) == 2)
                        {
                            Mes_Disponible = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero;
                            Mes_Comprometido = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Febrero;
                        }
                        else
                        {
                            Mi_Sql_Complemento.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Febrero + " = 0.00, ");
                        }
                    }
                    if (Convert.ToInt32(Mes_Actual) <= 3)
                    {
                        if (Convert.ToInt32(Mes_Actual) == 3)
                        {
                            Mes_Disponible = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo;
                            Mes_Comprometido = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Marzo;
                        }
                        else
                        {
                            Mi_Sql_Complemento.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Marzo + " = 0.00, ");
                        }
                    }
                    if (Convert.ToInt32(Mes_Actual) <= 4)
                    {
                        if (Convert.ToInt32(Mes_Actual) == 4)
                        {
                            Mes_Disponible = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril;
                            Mes_Comprometido = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Abril;
                        }
                        else
                        {
                            Mi_Sql_Complemento.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Abril + " = 0.00, ");
                        }
                    }
                    if (Convert.ToInt32(Mes_Actual) <= 5)
                    {
                        if (Convert.ToInt32(Mes_Actual) == 5)
                        {
                            Mes_Disponible = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo;
                            Mes_Comprometido = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Mayo;
                        }
                        else
                        {
                            Mi_Sql_Complemento.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Mayo + " = 0.00, ");
                        }
                    }
                    if (Convert.ToInt32(Mes_Actual) <= 6)
                    {
                        if (Convert.ToInt32(Mes_Actual) == 6)
                        {
                            Mes_Disponible = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio;
                            Mes_Comprometido = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Junio;
                        }
                        else
                        {
                            Mi_Sql_Complemento.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Junio + " = 0.00, ");
                        }
                    }
                    if (Convert.ToInt32(Mes_Actual) <= 7)
                    {
                        if (Convert.ToInt32(Mes_Actual) == 7)
                        {
                            Mes_Disponible = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio;
                            Mes_Comprometido = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Julio;
                        }
                        else
                        {
                            Mi_Sql_Complemento.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Julio + " = 0.00, ");
                        }
                    }
                    if (Convert.ToInt32(Mes_Actual) <= 8)
                    {
                         if (Convert.ToInt32(Mes_Actual) == 8)
                        {
                            Mes_Disponible = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto;
                            Mes_Comprometido = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Agosto;
                        }
                        else
                        {
                            Mi_Sql_Complemento.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Agosto + " = 0.00, ");
                        }
                    }
                    if (Convert.ToInt32(Mes_Actual) <= 9)
                    {
                        if (Convert.ToInt32(Mes_Actual) == 9)
                        {
                            Mes_Disponible = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre;
                            Mes_Comprometido = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Septiembre;
                        }
                        else
                        {
                            Mi_Sql_Complemento.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Septiembre + " = 0.00, ");
                        }
                    }
                    if (Convert.ToInt32(Mes_Actual) <= 10)
                    {
                        if (Convert.ToInt32(Mes_Actual) == 10)
                        {
                            Mes_Disponible = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre;
                            Mes_Comprometido = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Octubre;
                        }
                        else
                        {
                            Mi_Sql_Complemento.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Octubre + " = 0.00, ");
                        }
                    }
                    if (Convert.ToInt32(Mes_Actual) <= 11)
                    {
                        if (Convert.ToInt32(Mes_Actual) == 11)
                        {
                            Mes_Disponible = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre;
                            Mes_Comprometido = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Noviembre;
                        }
                        else
                        {
                            Mi_Sql_Complemento.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Noviembre + " = 0.00, ");
                        }
                    }
                    if (Convert.ToInt32(Mes_Actual) <= 12)
                    {
                        if (Convert.ToInt32(Mes_Actual) == 12)
                        {
                            Mes_Disponible = Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre;
                            Mes_Comprometido = Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Diciembre;
                        }
                        else
                        {
                            Mi_Sql_Complemento.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Diciembre + " = 0.00 ");
                        }
                    }
                #endregion

                if (Mes_Actual.Trim() != "12")
                {
                    //eliminamos el registro de la reserva de nomina, para poder guardar uno nuevo cuando se genere la reserva de este año
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("DELETE " + Ope_Nom_Reserva_Nomina.Tabla_Ope_Nom_Reserva_Nomina);
                    Mi_SQL.Append(" WHERE " + Ope_Nom_Reserva_Nomina.Campo_Anio + " = " + Anio.Trim());

                    Comando.CommandText = Mi_SQL.ToString();
                    Comando.ExecuteNonQuery();
                }

                foreach (DataRow Dr in Dt_Datos.Rows)
                {
                    //si es de tipo nomina regresamos el saldo de los meses posteriores y si no solo regresamos el saldo de la reserva al mes actual

                    if (Dr[Ope_Psp_Presupuesto_Aprobado.Campo_Tipo].ToString().Trim().Equals("RESERVA_NOMINA"))
                    {
                        //MODIFICAMOS EL PRESUPUESTO
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append(" UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_SQL.Append(" SET ");
                        //modificamos el disponible acumulado
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " + ");
                        Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"]) + ", ");
                        //modificamos el disponible del mes
                        Mi_SQL.Append(Mes_Disponible.Trim() + " = " + Mes_Disponible.Trim() + " + ");
                        Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"]) + ", ");
                        //modificamos el comprometido acumulado
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + " - ");
                        Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"]) + ", ");
                        //modificamos el comprometido del mes
                        Mi_SQL.Append(Mes_Comprometido.Trim() + " = " + Mes_Comprometido + " - ");
                        Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"]) + ", ");
                        //modificamos los comprometidos posteriores
                        Mi_SQL.Append(Mi_Sql_Complemento.ToString().Trim() + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "', ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE()");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["DEPENDENCIA_ID"].ToString().Trim() + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");

                        Comando.CommandText = Mi_SQL.ToString();
                        Comando.ExecuteNonQuery();
                    }
                    else 
                    {
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_SQL.Append(" SET " + Mes_Comprometido.Trim() + " = " + Mes_Comprometido.Trim() + " - ");
                        Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"]) + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + " - ");
                        Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"]) + ", ");
                        Mi_SQL.Append(Mes_Disponible.Trim() + " = " + Mes_Disponible.Trim() + " + ");
                        Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"]) + ", ");
                        Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " + ");
                        Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"]));
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["DEPENDENCIA_ID"].ToString().Trim() + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                        //Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + " < 0");

                        Comando.CommandText = Mi_SQL.ToString();
                        Comando.ExecuteNonQuery();
                    }
                }

            }
            catch (SqlException Ex)
            {
                Transaccion.Rollback();
                Mensaje = "Error:  [" + Ex.Message + "]";
                throw new Exception(Mensaje, Ex);
            }
            catch (DBConcurrencyException Ex)
            {
                Transaccion.Rollback();
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                Transaccion.Rollback();
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Comando == null)
                {
                    Conexion.Close();
                    Comando = null;
                    Conexion = null;
                    Transaccion = null;
                }
            }
            return Importe;
        }
    }
}
