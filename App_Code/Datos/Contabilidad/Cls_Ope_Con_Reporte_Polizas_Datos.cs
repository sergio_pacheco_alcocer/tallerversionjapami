﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Reporte_Polizas.Negocios;
using System.Data;


/// <summary>
/// Summary description for Cls_Ope_Con_Reporte_Polizas_Datos
/// </summary>
/// 
namespace JAPAMI.Reporte_Polizas.Datos
{
    public class Cls_Ope_Con_Reporte_Polizas_Datos
    {


        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Unidad_Responsable
        /// DESCRIPCION: Consultar las Unidades Responsables
        /// PARAMETROS : Datos: Variable para la capa de negocio
        /// CREO       : Susana Trigueros Armenta
        /// FECHA_CREO : 18/Abril/2012 9:56
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Unidad_Responsable(Cls_Ope_Con_Reporte_Polizas_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            Mi_SQL.Append("SELECT " + Cat_Dependencias.Campo_Dependencia_ID);
            Mi_SQL.Append(", " + Cat_Dependencias.Campo_Clave + " +' '+ " + Cat_Dependencias.Campo_Nombre);
            Mi_SQL.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
            Mi_SQL.Append(" WHERE " + Cat_Dependencias.Campo_Estatus + "='ACTIVO'");
            Mi_SQL.Append(" ORDER BY " + Cat_Dependencias.Campo_Nombre + " ASC ");


            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
        }

        public static DataTable Consultar_Polizas(Cls_Ope_Con_Reporte_Polizas_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            Mi_SQL.Append("SELECT " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +"."+ Ope_Con_Polizas.Campo_No_Poliza);
            Mi_SQL.Append(", " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID);
            Mi_SQL.Append(", " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +"."+ Ope_Con_Polizas.Campo_Mes_Ano);
            Mi_SQL.Append(", " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +"."+ Ope_Con_Polizas.Campo_Concepto);
            Mi_SQL.Append(", " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +"."+ Ope_Con_Polizas.Campo_Total_Debe);
            Mi_SQL.Append(", " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +"."+ Ope_Con_Polizas.Campo_Total_Haber);
            Mi_SQL.Append(", (SELECT " + Cat_Con_Tipo_Polizas.Campo_Descripcion + " FROM " + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas);
            Mi_SQL.Append(" WHERE " + Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID + "=" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas); 
            Mi_SQL.Append("." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID + ") AS TIPO_POLIZA" );
            Mi_SQL.Append(" FROM " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas);
            Mi_SQL.Append("," + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
            Mi_SQL.Append(" WHERE " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "."+ Ope_Con_Polizas.Campo_No_Poliza + " = ");
            Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_No_Poliza);
            Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "."+ Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = ");
            Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID);
            Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "."+ Ope_Con_Polizas.Campo_Tipo_Poliza_ID + " = ");
            Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID);
            Mi_SQL.Append(" AND " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "."+ Ope_Con_Polizas.Campo_Mes_Ano + " = ");
            Mi_SQL.Append(Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Mes_Ano);



            if (Datos.P_Concepto != null)
            {
                Mi_SQL.Append(" AND UPPER(" + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +"."+ Ope_Con_Polizas.Campo_Concepto );
                Mi_SQL.Append(") LIKE UPPER('%" + Datos.P_Concepto + ")");
            }

            if (Datos.P_Cuenta_Contable != null)
            {
                Mi_SQL.Append(" AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append("=(SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                Mi_SQL.Append(" FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                Mi_SQL.Append(" WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                Mi_SQL.Append("= '" + Datos.P_Cuenta_Contable + "')");
            }

            if (Datos.P_Unidad_Responsable_ID != null)
            {
                Mi_SQL.Append(" AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." +Ope_Con_Polizas_Detalles.Campo_Dependencia_ID );
                Mi_SQL.Append("='" + Datos.P_Unidad_Responsable_ID.Trim() + "'");

            }

            if (Datos.P_Fecha_Inicial.Trim() != null)
            {
                Mi_SQL.Append(" AND " + Ope_Con_Polizas.Campo_Fecha_Creo + " BETWEEN ");
                Mi_SQL.Append("'" + Datos.P_Fecha_Inicial.Trim() + " 00:00:00' AND '" + Datos.P_Fecha_Fin.Trim() + " 23:59:59'");
            }

            Mi_SQL.Append(" GROUP BY " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +"."+ Ope_Con_Polizas.Campo_No_Poliza);
            Mi_SQL.Append(", " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +"."+ Ope_Con_Polizas.Campo_Mes_Ano);
            Mi_SQL.Append(", " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +"."+ Ope_Con_Polizas.Campo_Concepto);
            Mi_SQL.Append(", " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas +"."+ Ope_Con_Polizas.Campo_Total_Debe);
            Mi_SQL.Append(", " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Total_Haber);
            Mi_SQL.Append(", " + Ope_Con_Polizas.Tabla_Ope_Con_Polizas + "." + Ope_Con_Polizas.Campo_Tipo_Poliza_ID);


            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
        }

        public static DataTable Consultar_Detalle_Polizas(Cls_Ope_Con_Reporte_Polizas_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            Mi_SQL.Append("SELECT " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +"." + Ope_Con_Polizas_Detalles.Campo_Concepto);
            Mi_SQL.Append(", " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_No_Poliza);
            Mi_SQL.Append(", " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +"." + Ope_Con_Polizas_Detalles.Campo_Debe);
            Mi_SQL.Append(", " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles +"." + Ope_Con_Polizas_Detalles.Campo_Haber);
            Mi_SQL.Append(", " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "."  + Ope_Con_Polizas_Detalles.Campo_Partida);
            Mi_SQL.Append(" FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles);
            Mi_SQL.Append(" WHERE " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_No_Poliza);
            Mi_SQL.Append("='" +  Datos.P_No_Poliza.Trim() +"'");
            Mi_SQL.Append(" AND " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + "." + Ope_Con_Polizas_Detalles.Campo_Mes_Ano);
            Mi_SQL.Append("='" + Datos.P_Mes_Ano.Trim() + "'");
            Mi_SQL.Append(" AND " + Ope_Con_Polizas_Detalles.Campo_Tipo_Poliza_ID);
            Mi_SQL.Append("='" + Datos.P_Tipo_Poliza_ID.Trim() + "'" );
            Mi_SQL.Append(" ORDER BY CAST(" + Ope_Con_Polizas_Detalles.Campo_Partida + " AS INT) ");



            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
        }

    }
}