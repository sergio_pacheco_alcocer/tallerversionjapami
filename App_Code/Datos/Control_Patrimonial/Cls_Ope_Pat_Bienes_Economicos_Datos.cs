﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Control_Patrimonial_Bienes_Economicos.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

/// <summary>
/// Summary description for Cls_Ope_Pat_Bienes_Economicos_Datos
/// </summary>

namespace JAPAMI.Control_Patrimonial_Bienes_Economicos.Datos { 

    public class Cls_Ope_Pat_Bienes_Economicos_Datos {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Bien_Economico
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos un nuevo registro 
        ///                       de un Bien que no lleva Inventario.
        ///PARAMETROS           : Parametros.   Contiene los parametros que se van a dar de
        ///                                     Alta en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 03/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Cls_Ope_Pat_Bienes_Economicos_Negocio Alta_Bien_Economico(Cls_Ope_Pat_Bienes_Economicos_Negocio Parametros) {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try {
                String Bien_ID = Obtener_ID_Consecutivo(Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos, Ope_Pat_Bienes_Economicos.Campo_Bien_ID, 10);
                Int32 Bien_Resguardo_ID = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos, Ope_Pat_Bienes_Resguardos.Campo_Bien_Resguardo_ID, 10));
                if (Bien_ID == null || Bien_ID.Trim().Length == 0) {
                    Parametros.P_Bien_ID = 1;
                } else {
                    Parametros.P_Bien_ID = Convert.ToInt32(Bien_ID);
                }
                String Mi_SQL = "INSERT INTO " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos +
                                  " (" + Ope_Pat_Bienes_Economicos.Campo_Bien_ID +
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Producto_ID +
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Nombre +
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Cantidad +
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Gerencia_ID +
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Dependencia_ID +
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Marca_ID +
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Modelo +
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Material_ID +
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Color_ID +
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Costo_Inicial;
                if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Adquisicion).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Fecha_Adquisicion;
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Numero_Serie +
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Estatus +       
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Estado +
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Comentarios +
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Usuario_Creo +
                                  ", " + Ope_Pat_Bienes_Economicos.Campo_Fecha_Creo;
                Mi_SQL = Mi_SQL + ") VALUES ('" + Parametros.P_Bien_ID.ToString() + "'";
                Mi_SQL = Mi_SQL + ",'" + Parametros.P_Producto_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Nombre + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Cantidad + "'";
                Mi_SQL = Mi_SQL + ",'" + Parametros.P_Gerencia_ID + "'";
                Mi_SQL = Mi_SQL + ",'" + Parametros.P_Dependencia_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Marca + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Modelo + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Material + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Color + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Costo_Inicial + "'";
                if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Adquisicion).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))
                    Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Adquisicion) + "'";
                Mi_SQL = Mi_SQL + ",'" + Parametros.P_Numero_Serie + "'";
                Mi_SQL = Mi_SQL + ",'" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estado + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Comentarios + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                Mi_SQL = Mi_SQL + ", GETDATE())"; 
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                if (Parametros.P_Dt_Resguardantes != null)
                {
                    if (Parametros.P_Dt_Resguardantes.Rows.Count > 0)
                    {
                        foreach (DataRow Fila_Actual in Parametros.P_Dt_Resguardantes.Rows)
                        {
                            Mi_SQL = "INSERT INTO " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos +
                                  " (" + Ope_Pat_Bienes_Resguardos.Campo_Bien_Resguardo_ID +
                                  ", " + Ope_Pat_Bienes_Resguardos.Campo_Bien_ID +
                                  ", " + Ope_Pat_Bienes_Resguardos.Campo_Tipo +
                                  ", " + Ope_Pat_Bienes_Resguardos.Campo_Empleado_Resguardo_ID +
                                  ", " + Ope_Pat_Bienes_Resguardos.Campo_Fecha_Inicial +
                                  ", " + Ope_Pat_Bienes_Resguardos.Campo_Comentarios +
                                  ", " + Ope_Pat_Bienes_Resguardos.Campo_Usuario_Creo +
                                  ", " + Ope_Pat_Bienes_Resguardos.Campo_Fecha_Creo +
                                  ", " + Ope_Pat_Bienes_Resguardos.Campo_Estatus +
                                  ", " + Ope_Pat_Bienes_Resguardos.Campo_Empleado_Almacen_ID;
                            Mi_SQL = Mi_SQL + ") VALUES ('" + Bien_Resguardo_ID.ToString() + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_ID + "'";
                            Mi_SQL = Mi_SQL + ", 'RESGUARDO_ECONOMICO'";
                            Mi_SQL = Mi_SQL + ", '" + Fila_Actual["EMPLEADO_ID"].ToString().Trim() + "'";
                            Mi_SQL = Mi_SQL + ", GETDATE()";
                            Mi_SQL = Mi_SQL + ", '" + Fila_Actual["COMENTARIOS"].ToString().Trim() + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                            Mi_SQL = Mi_SQL + ", GETDATE()";
                            Mi_SQL = Mi_SQL + ", 'VIGENTE'";
                            Mi_SQL = Mi_SQL + ", '" + Fila_Actual["EMPLEADO_ALMACEN_ID"].ToString().Trim() + "')";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                        }
                    }
                }

                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
            return Parametros;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modifica_Bien_Economico
        ///DESCRIPCIÓN          : Modifica en la Base de Datos un registro de un Bien que 
        ///                       no lleva Inventario.
        ///PARAMETROS           : Parametros. Contiene los parametros que se van a Modificar
        ///                                   en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 03/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Modifica_Bien_Economico(Cls_Ope_Pat_Bienes_Economicos_Negocio Parametros) {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            String Mi_SQL = "";
            try
            {
                Cls_Ope_Pat_Bienes_Economicos_Negocio Temporal_1 = Consultar_Detalles_Bien_Economico(Parametros);
                Mi_SQL = "UPDATE " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos;
                Mi_SQL = Mi_SQL + " SET " + Ope_Pat_Bienes_Economicos.Campo_Gerencia_ID + " = '" + Parametros.P_Gerencia_ID + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Marca_ID + " = '" + Parametros.P_Marca + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Modelo + " = '" + Parametros.P_Modelo + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Material_ID + " = '" + Parametros.P_Material + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Color_ID + " = '" + Parametros.P_Color + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Cantidad + " = '" + Parametros.P_Cantidad + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Numero_Serie + " = '" + Parametros.P_Numero_Serie + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Costo_Inicial + " = '" + Parametros.P_Costo_Inicial + "'";
                if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Adquisicion).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Fecha_Adquisicion + " = '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Adquisicion) + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Estado + " = '" + Parametros.P_Estado + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Comentarios + " = '" + Parametros.P_Comentarios + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario_Nombre + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Fecha_Modifico + " = GETDATE()";
                if (Parametros.P_Motivo_Baja != null && Parametros.P_Motivo_Baja.Trim().Length > 0) Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Campo_Motivo_Baja + " = '" + Parametros.P_Motivo_Baja + "'";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_Bienes_Economicos.Campo_Bien_ID + " = '" + Parametros.P_Bien_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Actualización de Resguardantes.
                if (Parametros.P_Estatus.Trim().Equals("VIGENTE"))
                {
                    Cls_Ope_Pat_Bienes_Economicos_Negocio Temporal = Obtener_Diferencia_Resguardos(Temporal_1, Parametros);

                    //SE DAN DE BAJA LOS RESGURADANTES ANTERIORES
                    for (Int32 Contador = 0; Contador < Temporal.P_Dt_Resguardantes.Rows.Count; Contador++)
                    {
                        Mi_SQL = "UPDATE " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos;
                        Mi_SQL = Mi_SQL + " SET " + Ope_Pat_Bienes_Resguardos.Campo_Fecha_Final + " = GETDATE()";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Campo_Estatus + " = 'BAJA'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario_Nombre + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Campo_Fecha_Modifico + " = GETDATE()";
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_Bienes_Resguardos.Campo_Bien_Resguardo_ID + " = '" + Temporal.P_Dt_Resguardantes.Rows[Contador][0].ToString() + "'";

                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    Cls_Ope_Pat_Bienes_Economicos_Negocio Temporal_2 = Obtener_Diferencia_Resguardos(Parametros, Temporal_1);

                    //SE DAN DE ALTA LOS NUEVOS RESGUARDANTES
                    if (Temporal_2.P_Dt_Resguardantes != null && Temporal_2.P_Dt_Resguardantes.Rows.Count > 0)
                    {
                        String ID_Consecutivo = "";
                        String Str_Mi_SQL = "SELECT MAX(" + Ope_Pat_Bienes_Resguardos.Campo_Bien_Resguardo_ID + ") FROM " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos;
                        Cmd.CommandText = Str_Mi_SQL;
                        Object Obj_Temp = Cmd.ExecuteScalar();
                        if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                        {
                            ID_Consecutivo = String.Format("{0:0000000000}", (Convert.ToInt32(Obj_Temp) + 1));
                        }
                        for (Int32 Cnt = 0; Cnt < Temporal_2.P_Dt_Resguardantes.Rows.Count; Cnt++)
                        {
                            Mi_SQL = "INSERT INTO " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos;
                            Mi_SQL = Mi_SQL + " (" + Ope_Pat_Bienes_Resguardos.Campo_Bien_Resguardo_ID + ", " + Ope_Pat_Bienes_Resguardos.Campo_Bien_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Campo_Tipo + ", " + Ope_Pat_Bienes_Resguardos.Campo_Empleado_Resguardo_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Campo_Fecha_Inicial + ", " + Ope_Pat_Bienes_Resguardos.Campo_Empleado_Almacen_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Campo_Estatus + ", " + Ope_Pat_Bienes_Resguardos.Campo_Comentarios;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Campo_Usuario_Creo + ", " + Ope_Pat_Bienes_Resguardos.Campo_Fecha_Creo + ")";
                            Mi_SQL = Mi_SQL + " VALUES (" + Convert.ToInt32(ID_Consecutivo) + ",'" + Parametros.P_Bien_ID + "', 'RESGUARDO_ECONOMICO','" + Temporal_2.P_Dt_Resguardantes.Rows[Cnt]["EMPLEADO_ID"].ToString() + "', GETDATE()";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_ID + "', 'VIGENTE', '" + Parametros.P_Dt_Resguardantes.Rows[Cnt]["COMENTARIOS"].ToString() + "'";
                            Mi_SQL = Mi_SQL + ",'" + Parametros.P_Usuario_Nombre + "', GETDATE())";
                            ID_Consecutivo = Convertir_A_Formato_ID(Convert.ToInt32(ID_Consecutivo) + 1, 50);

                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                        }
                    }
                }
                else
                {
                    //SE DAN DE BAJA LOS RESGURADANTES ANTERIORES
                    for (Int32 Contador = 0; Contador < Temporal_1.P_Dt_Resguardantes.Rows.Count; Contador++)
                    {
                        Mi_SQL = "UPDATE " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos;
                        Mi_SQL = Mi_SQL + " SET " + Ope_Pat_Bienes_Resguardos.Campo_Fecha_Final + " = GETDATE()";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Campo_Estatus + " = 'BAJA'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario_Nombre + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Campo_Fecha_Modifico + " = GETDATE()";
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_Bienes_Resguardos.Campo_Bien_Resguardo_ID + " = '" + Temporal_1.P_Dt_Resguardantes.Rows[Contador][0].ToString() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                }

                //SE CARGAN LOS CAMBIOS QUE SE REALIZARÓN EN EL BIEN
                if (Parametros.P_Dt_Cambios != null)
                {
                    if (Parametros.P_Dt_Cambios.Rows.Count > 0)
                    {
                        Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_Historial_Cambios.Tabla_Ope_Pat_Historial_Cambios, Ope_Pat_Historial_Cambios.Campo_No_Registro, 10));
                        Int32 No_Cambio = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_Historial_Cambios.Tabla_Ope_Pat_Historial_Cambios, Ope_Pat_Historial_Cambios.Campo_No_Cambio, 10, Ope_Pat_Historial_Cambios.Campo_Tipo_Bien, "BIEN_ECONOMICO", Ope_Pat_Historial_Cambios.Campo_Bien_ID, Parametros.P_Bien_ID.ToString()));
                        Int32 No_Detalle = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_Hist_Cam_Det.Tabla_Ope_Pat_Hist_Cam_Det, Ope_Pat_Hist_Cam_Det.Campo_No_Detalle, 10));
                        Mi_SQL = "INSERT INTO " + Ope_Pat_Historial_Cambios.Tabla_Ope_Pat_Historial_Cambios +
                                " ( " + Ope_Pat_Historial_Cambios.Campo_No_Registro + ", " + Ope_Pat_Historial_Cambios.Campo_No_Cambio + ", " + Ope_Pat_Historial_Cambios.Campo_Fecha_Hora_Cambio +
                                ", " + Ope_Pat_Historial_Cambios.Campo_Empleado_Cambio_ID + ", " + Ope_Pat_Historial_Cambios.Campo_Bien_ID + ", " + Ope_Pat_Historial_Cambios.Campo_Tipo_Bien + ", " + Ope_Pat_Historial_Cambios.Campo_Usuario_Creo + ", " + Ope_Pat_Historial_Cambios.Campo_Fecha_Creo + ") " +
                                "VALUES ( '" + No_Registro + "','" + No_Cambio + "', GETDATE(), '" + Parametros.P_Usuario_ID.Trim() + "', '" + Parametros.P_Bien_ID.ToString().Trim() + "', 'BIEN_ECONOMICO', '" + Parametros.P_Usuario_Nombre.Trim() + "', GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        for (Int32 Contador = 0; Contador < (Parametros.P_Dt_Cambios.Rows.Count); Contador++)
                        {
                            Mi_SQL = "INSERT INTO " + Ope_Pat_Hist_Cam_Det.Tabla_Ope_Pat_Hist_Cam_Det +
                                     " ( " + Ope_Pat_Hist_Cam_Det.Campo_No_Detalle + ", " + Ope_Pat_Hist_Cam_Det.Campo_No_Registro + ", " + Ope_Pat_Hist_Cam_Det.Campo_Campo +
                                     ", " + Ope_Pat_Hist_Cam_Det.Campo_Valor_Anterior + ", " + Ope_Pat_Hist_Cam_Det.Campo_Valor_Nuevo + ") " +
                                     "VALUES ( '" + No_Detalle + "','" + No_Registro + "', '" + Parametros.P_Dt_Cambios.Rows[Contador]["Campo"].ToString() +
                                     "', '" + Parametros.P_Dt_Cambios.Rows[Contador]["Valor_Anterior"].ToString() + "', '" + Parametros.P_Dt_Cambios.Rows[Contador]["Valor_Nuevo"].ToString() + "')";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            No_Detalle = No_Detalle + 1;
                        }
                    }
                }

                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar Modificar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Bienes_Economicos
        ///DESCRIPCIÓN          : Carga un listado de los bienes 
        ///PARAMETROS           : Parametros. Contiene los parametros que se van a Consultar
        ///                                   en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 03/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Bienes_Economicos(Cls_Ope_Pat_Bienes_Economicos_Negocio Parametros) {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            try {
                if (Parametros.P_No_Empleado_Resguardante != null && Parametros.P_No_Empleado_Resguardante.Trim().Length > 0) {
                    Parametros.P_No_Empleado_Resguardante = Convertir_A_Formato_ID(Convert.ToInt32(Parametros.P_No_Empleado_Resguardante), 6);
                }
                Mi_SQL = "SELECT " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Bien_ID + " AS BIEN_ID";
                Mi_SQL = Mi_SQL + ", 'BIEN_MUEBLE', 'BIEN MUEBLE', 'VEHICULO', 'VEHÍCULO') AS TIPO_PARENT_DECODIFICADO";                
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Nombre + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + "." + Cat_Com_Marcas.Campo_Nombre + " AS MARCA";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Modelo + " AS MODELO";
                Mi_SQL = Mi_SQL + ", " + Cat_Pat_Colores.Tabla_Cat_Pat_Colores + "." + Cat_Pat_Colores.Campo_Descripcion + " AS COLOR";
                Mi_SQL = Mi_SQL + ", " + Cat_Pat_Materiales.Tabla_Cat_Pat_Materiales + "." + Cat_Pat_Materiales.Campo_Descripcion + " AS MATERIAL";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Numero_Serie+ " AS NUMERO_SERIE";
                Mi_SQL = Mi_SQL + ", 'MARCA:  '+ ISNULL(" + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + "." + Cat_Com_Marcas.Campo_Nombre + ", '-')";
                Mi_SQL = Mi_SQL + "  +', MODELO:  '+ ISNULL(" + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Modelo + ", '-')";
                Mi_SQL = Mi_SQL + "  +', COLOR:  '+ ISNULL(" + Cat_Pat_Colores.Tabla_Cat_Pat_Colores + "." + Cat_Pat_Colores.Campo_Descripcion + ", '-')";
                Mi_SQL = Mi_SQL + "  +', MATERIAL:  '+ ISNULL(" + Cat_Pat_Materiales.Tabla_Cat_Pat_Materiales + "." + Cat_Pat_Materiales.Campo_Descripcion + ", '-')";
                Mi_SQL = Mi_SQL + "  +', NO. SERIE:  '+ ISNULL(" + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Numero_Serie + ", 'S/S') AS CARACTERISTICAS";
                Mi_SQL = Mi_SQL + ", DECODE(" + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Estatus + "";
                Mi_SQL = Mi_SQL + ", 'DEFINITIVA', 'BAJA (DEFINITIVA)', 'TEMPORAL', 'BAJA (TEMPORAL)', 'VIGENTE', 'VIGENTE') AS ESTATUS";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas;
                Mi_SQL = Mi_SQL + " ON " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Marca_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + "." + Cat_Com_Marcas.Campo_Marca_ID;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Materiales.Tabla_Cat_Pat_Materiales;
                Mi_SQL = Mi_SQL + " ON " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Material_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Pat_Materiales.Tabla_Cat_Pat_Materiales + "." + Cat_Pat_Materiales.Campo_Material_ID;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Colores.Tabla_Cat_Pat_Colores;
                Mi_SQL = Mi_SQL + " ON " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Color_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Pat_Colores.Tabla_Cat_Pat_Colores + "." + Cat_Pat_Colores.Campo_Color_ID;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Nombre;
                Mi_SQL = Mi_SQL + " LIKE '%" + Parametros.P_Nombre.Trim() + "%'";
                if (Parametros.P_Marca != null && Parametros.P_Marca.Trim().Length > 0) {
                    Mi_SQL = Mi_SQL + " AND " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Marca_ID;
                    Mi_SQL = Mi_SQL + " = '" + Parametros.P_Marca.Trim() + "'";
                }
                if (Parametros.P_Modelo != null && Parametros.P_Modelo.Trim().Length > 0) {
                    Mi_SQL = Mi_SQL + " AND " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Modelo;
                    Mi_SQL = Mi_SQL + " LIKE '%" + Parametros.P_Modelo.Trim() + "%'";
                }
                if (Parametros.P_Material != null && Parametros.P_Material.Trim().Length > 0) {
                    Mi_SQL = Mi_SQL + " AND " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Material_ID;
                    Mi_SQL = Mi_SQL + " = '" + Parametros.P_Material.Trim() + "'";
                }
                if (Parametros.P_Color != null && Parametros.P_Color.Trim().Length > 0) {
                    Mi_SQL = Mi_SQL + " AND " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Color_ID;
                    Mi_SQL = Mi_SQL + " = '" + Parametros.P_Color.Trim() + "'";
                }
                if (Parametros.P_Numero_Serie != null && Parametros.P_Numero_Serie.Trim().Length > 0) {
                    Mi_SQL = Mi_SQL + " AND " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Numero_Serie;
                    Mi_SQL = Mi_SQL + " LIKE '%" + Parametros.P_Numero_Serie.Trim() + "%'";
                }
                if (Parametros.P_Estado != null && Parametros.P_Estado.Trim().Length > 0) {
                    Mi_SQL = Mi_SQL + " AND " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Estado;
                    Mi_SQL = Mi_SQL + " = '" + Parametros.P_Estado.Trim() + "'";
                }
                if (Parametros.P_Estatus != null && Parametros.P_Estatus.Trim().Length > 0) {
                    Mi_SQL = Mi_SQL + " AND " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + "." + Ope_Pat_Bienes_Economicos.Campo_Estatus;
                    Mi_SQL = Mi_SQL + " = '" + Parametros.P_Estatus.Trim() + "'";
                }
                //FALTA LA BUSQUEDA POR LOS DATOS DEL PARENT...
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Datos != null && Ds_Datos.Tables.Count>0) {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            } catch (Exception Ex) {
                throw new Exception("Consultar_Bienes_Sin_Inventario::: [" + Ex.Message + "]");
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_DataTable
        ///DESCRIPCIÓN          : Carga una Tabla con los datos requeridos en los parametros.
        ///PARAMETROS           : Parametros. Contiene los parametros que se van a Consultar
        ///                                   en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 04/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_DataTable(Cls_Ope_Pat_Bienes_Economicos_Negocio Parametros) {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try {
                if (Parametros.P_Tipo_DataTable != null) {
                    if (Parametros.P_Tipo_DataTable.Trim().Equals("MARCAS")) {
                        Mi_SQL = "SELECT " + Cat_Com_Marcas.Campo_Marca_ID + " AS IDENTIFICADOR";
                        Mi_SQL = Mi_SQL + ", " + Cat_Com_Marcas.Campo_Nombre + " AS TEXTO";
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas;
                        if (!String.IsNullOrEmpty(Parametros.P_Identificador_Generico)) {
                            Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Marcas.Campo_Marca_ID + " = '" + Parametros.P_Identificador_Generico + "'";
                        }
                        Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Com_Marcas.Campo_Nombre;
                    } else if (Parametros.P_Tipo_DataTable.Trim().Equals("MATERIALES")) {
                        Mi_SQL = "SELECT " + Cat_Pat_Materiales.Campo_Material_ID + " AS IDENTIFICADOR";
                        Mi_SQL = Mi_SQL + ", " + Cat_Pat_Materiales.Campo_Descripcion + " AS TEXTO";
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Pat_Materiales.Tabla_Cat_Pat_Materiales;
                        if (!String.IsNullOrEmpty(Parametros.P_Identificador_Generico)) {
                            Mi_SQL = Mi_SQL + " WHERE " + Cat_Pat_Materiales.Campo_Material_ID + " = '" + Parametros.P_Identificador_Generico + "'";
                        }
                        Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Pat_Materiales.Campo_Descripcion;
                    } else if (Parametros.P_Tipo_DataTable.Trim().Equals("COLORES")) {
                        Mi_SQL = "SELECT " + Cat_Pat_Colores.Campo_Color_ID + " AS IDENTIFICADOR";
                        Mi_SQL = Mi_SQL + ", " + Cat_Pat_Colores.Campo_Descripcion + " AS TEXTO";
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Pat_Colores.Tabla_Cat_Pat_Colores;
                        if (!String.IsNullOrEmpty(Parametros.P_Identificador_Generico)) {
                            Mi_SQL = Mi_SQL + " WHERE " + Cat_Pat_Colores.Campo_Color_ID + " = '" + Parametros.P_Identificador_Generico + "'";
                        }
                        Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Pat_Colores.Campo_Descripcion;
                    } else if (Parametros.P_Tipo_DataTable.Trim().Equals("DEPENDENCIAS_COMBOS")) {
                        Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + " AS IDENTIFICADOR";
                        Mi_SQL = Mi_SQL + ", " + Cat_Dependencias.Campo_Clave + "  +' - '+  " + Cat_Dependencias.Campo_Nombre + " AS TEXTO";
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
                        if (!String.IsNullOrEmpty(Parametros.P_Identificador_Generico)) {
                            Mi_SQL = Mi_SQL + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Parametros.P_Identificador_Generico + "'";
                        }
                        Mi_SQL = Mi_SQL + " ORDER BY TEXTO";
                    } else if (Parametros.P_Tipo_DataTable.Trim().Equals("TIPOS_VEHICULO")) {
                        Mi_SQL = "SELECT " + Cat_Pat_Tipos_Vehiculo.Campo_Tipo_Vehiculo_ID + " AS IDENTIFICADOR";
                        Mi_SQL = Mi_SQL + ", " + Cat_Pat_Tipos_Vehiculo.Campo_Descripcion + " AS TEXTO";
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Pat_Tipos_Vehiculo.Tabla_Cat_Pat_Tipos_Vehiculo;
                        if (!String.IsNullOrEmpty(Parametros.P_Identificador_Generico)) {
                            Mi_SQL = Mi_SQL + " WHERE " + Cat_Pat_Tipos_Vehiculo.Campo_Tipo_Vehiculo_ID + " = '" + Parametros.P_Identificador_Generico + "'";
                        }
                        Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Pat_Tipos_Vehiculo.Campo_Descripcion;
                    } else if (Parametros.P_Tipo_DataTable.Trim().Equals("EMPLEADOS")) {
                        Mi_SQL = "SELECT " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " AS EMPLEADO_ID";
                        Mi_SQL += ", " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_No_Empleado + " AS NO_EMPLEDO";
                        Mi_SQL += ", " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + " AS APELLIDO_PATERNO";
                        Mi_SQL += ", " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + " AS APELLIDO_MATERNO";
                        Mi_SQL += ", " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + " AS NOMBRE";
                        Mi_SQL += ", " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + "";
                        Mi_SQL += " +' '+ " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + "";
                        Mi_SQL += " +' '+ " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + " AS NOMBRE_COMPLETO";
                        Mi_SQL += ", " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_No_Empleado + "";
                        Mi_SQL += "  +' - '+ " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + "";
                        Mi_SQL += " +' '+ " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + "";
                        Mi_SQL += " +' '+ " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + " AS EMPLEADO";
                        Mi_SQL += " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
                        if (Parametros.P_Dependencia_ID != null && Parametros.P_Dependencia_ID.Trim().Length > 0) {
                            if (Entro_Where) {
                                Mi_SQL += " AND ";
                            } else {
                                Mi_SQL += " WHERE ";
                            }
                            Mi_SQL += Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Dependencia_ID + " ='" + Parametros.P_Dependencia_ID + "'";
                        }
                        Mi_SQL += " ORDER BY NOMBRE_COMPLETO";
                    }
                    else if (Parametros.P_Tipo_DataTable.Equals("GERENCIAS"))
                    {
                        Mi_SQL = "SELECT " + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + " AS GERENCIA_ID, " + Cat_Grupos_Dependencias.Campo_Clave + " +' - '+ " + Cat_Grupos_Dependencias.Campo_Nombre + " AS NOMBRE";
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + " ORDER BY " + Cat_Grupos_Dependencias.Campo_Nombre;
                    }
                    else if (Parametros.P_Tipo_DataTable.Equals("DEPENDENCIAS"))
                    {
                        Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + " AS DEPENDENCIA_ID, " + Cat_Dependencias.Campo_Clave + " +'-'+ " + Cat_Dependencias.Campo_Nombre + " AS NOMBRE, " + Cat_Dependencias.Campo_Grupo_Dependencia_ID + " AS GERENCIA_ID";
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
                        if (!String.IsNullOrEmpty(Parametros.P_Gerencia_ID))
                            Mi_SQL = Mi_SQL + " WHERE " + Cat_Dependencias.Campo_Grupo_Dependencia_ID + " = '" + Parametros.P_Gerencia_ID + "'";

                        Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Dependencias.Campo_Nombre;
                    }
                    else if (Parametros.P_Tipo_DataTable.Equals("BIENES_ECONOMICOS"))
                    {
                        Mi_SQL = "SELECT CONVERT(INT, BE." + Ope_Pat_Bienes_Economicos.Campo_Bien_ID + ") AS CLAVE";
                        Mi_SQL += ", BE." + Ope_Pat_Bienes_Economicos.Campo_Nombre + " AS NOMBRE_PRODUCTO";
                        Mi_SQL += ", MARCAS." + Cat_Com_Marcas.Campo_Nombre + " AS MARCA";
                        Mi_SQL += ", BE." + Ope_Pat_Bienes_Economicos.Campo_Modelo + " AS MODELO";
                        Mi_SQL += ", MATERIALES." + Cat_Pat_Materiales.Campo_Descripcion + " AS MATERIAL";
                        Mi_SQL += ", BE." + Ope_Pat_Bienes_Economicos.Campo_Cantidad + " AS CANTIDAD";
                        Mi_SQL += ", COLORES." + Cat_Pat_Colores.Campo_Descripcion + " AS COLOR";
                        Mi_SQL += ", BE." + Ope_Pat_Bienes_Economicos.Campo_Numero_Serie + " AS NUMERO_SERIE";
                        Mi_SQL += ", ESTATUS = CASE BE." + Ope_Pat_Bienes_Economicos.Campo_Estatus + "  WHEN 'DEFINITIVA' THEN 'BAJA (DEFINITIVA)' WHEN 'TEMPORAL' THEN 'BAJA (TEMPORAL)' WHEN 'VIGENTE' THEN 'VIGENTE' END ";
                        Mi_SQL += " FROM " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + " BE";
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Pat_Colores.Tabla_Cat_Pat_Colores + " COLORES ON BE." + Ope_Pat_Bienes_Economicos.Campo_Color_ID + " = COLORES." + Cat_Pat_Colores.Campo_Color_ID;
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " MARCAS ON BE." + Ope_Pat_Bienes_Economicos.Campo_Marca_ID + " = MARCAS." + Cat_Com_Marcas.Campo_Marca_ID;
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Pat_Materiales.Tabla_Cat_Pat_Materiales + " MATERIALES ON BE." + Ope_Pat_Bienes_Economicos.Campo_Material_ID + " = MATERIALES." + Cat_Pat_Materiales.Campo_Material_ID;
                        if (Parametros.P_Bien_ID > 0) {
                            if (Entro_Where) Mi_SQL += " AND "; else { Mi_SQL += " WHERE "; Entro_Where = true; }
                            Mi_SQL += " BE." + Ope_Pat_Bienes_Economicos.Campo_Bien_ID + " = '" + Parametros.P_Bien_ID.ToString().Trim() + "'";
                        }
                        if (!String.IsNullOrEmpty(Parametros.P_Nombre)) {
                            if (Entro_Where) Mi_SQL += " AND "; else { Mi_SQL += " WHERE "; Entro_Where = true; }
                            Mi_SQL += " BE." + Ope_Pat_Bienes_Economicos.Campo_Nombre + " LIKE '%" + Parametros.P_Nombre.Trim() + "%'";
                        }
                        if (!String.IsNullOrEmpty(Parametros.P_Modelo))
                        {
                            if (Entro_Where) Mi_SQL += " AND "; else { Mi_SQL += " WHERE "; Entro_Where = true; }
                            Mi_SQL += " BE." + Ope_Pat_Bienes_Economicos.Campo_Modelo + " LIKE '%" + Parametros.P_Nombre.Trim() + "%'";
                        }
                        if (!String.IsNullOrEmpty(Parametros.P_Marca))
                        {
                            if (Entro_Where) Mi_SQL += " AND "; else { Mi_SQL += " WHERE "; Entro_Where = true; }
                            Mi_SQL += " BE." + Ope_Pat_Bienes_Economicos.Campo_Marca_ID + " IN ('" + Parametros.P_Marca.Trim() + "')";
                        }
                        if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                        {
                            if (Entro_Where) Mi_SQL += " AND "; else { Mi_SQL += " WHERE "; Entro_Where = true; }
                            Mi_SQL += " BE." + Ope_Pat_Bienes_Economicos.Campo_Estatus + " IN ('" + Parametros.P_Estatus.Trim() + "')";
                        }
                        if (!String.IsNullOrEmpty(Parametros.P_Numero_Serie))
                        {
                            if (Entro_Where) Mi_SQL += " AND "; else { Mi_SQL += " WHERE "; Entro_Where = true; }
                            Mi_SQL += " BE." + Ope_Pat_Bienes_Economicos.Campo_Numero_Serie + " LIKE '%" + Parametros.P_Numero_Serie.Trim() + "%'";
                        }
                        if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                        {
                            if (Entro_Where) Mi_SQL += " AND "; else { Mi_SQL += " WHERE "; Entro_Where = true; }
                            Mi_SQL += " BE." + Ope_Pat_Bienes_Economicos.Campo_Dependencia_ID + " IN ('" + Parametros.P_Dependencia_ID.Trim() + "')";
                        }
                        if (!String.IsNullOrEmpty(Parametros.P_Resguardante_ID))
                        {
                            if (Entro_Where) Mi_SQL += " AND "; else { Mi_SQL += " WHERE "; Entro_Where = true; }
                            Mi_SQL += " BE." + Ope_Pat_Bienes_Economicos.Campo_Bien_ID + " IN (";
                            Mi_SQL += " SELECT BR." + Ope_Pat_Bienes_Resguardos.Campo_Bien_ID + " FROM " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + " BR";
                            Mi_SQL += " WHERE BR." + Ope_Pat_Bienes_Resguardos.Campo_Tipo + " = 'RESGUARDO_ECONOMICO'";
                            Mi_SQL += " AND BR." + Ope_Pat_Bienes_Resguardos.Campo_Estatus + " = 'VIGENTE'";
                            Mi_SQL += " AND BR." + Ope_Pat_Bienes_Resguardos.Campo_Empleado_Resguardo_ID + " IN ('" + Parametros.P_Resguardante_ID + "')";
                            Mi_SQL += ")";
                        }
                        if (!String.IsNullOrEmpty(Parametros.P_No_Empleado_Resguardante))
                        {
                            if (Entro_Where) Mi_SQL += " AND "; else { Mi_SQL += " WHERE "; Entro_Where = true; }
                            Mi_SQL += " BE." + Ope_Pat_Bienes_Economicos.Campo_Bien_ID + " IN (";
                            Mi_SQL += " SELECT BR." + Ope_Pat_Bienes_Resguardos.Campo_Bien_ID + " FROM " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + " BR";
                            Mi_SQL += " WHERE BR." + Ope_Pat_Bienes_Resguardos.Campo_Tipo + " = 'RESGUARDO_ECONOMICO'";
                            Mi_SQL += " AND BR." + Ope_Pat_Bienes_Resguardos.Campo_Estatus + " = 'VIGENTE'";
                            Mi_SQL += " AND BR." + Ope_Pat_Bienes_Resguardos.Campo_Empleado_Resguardo_ID + " IN (";
                            Mi_SQL += " SELECT EMP." + Cat_Empleados.Campo_Empleado_ID + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMP";
                            Mi_SQL += " WHERE EMP." + Cat_Empleados.Campo_No_Empleado + " IN ('" + Parametros.P_No_Empleado_Resguardante + "')";
                            Mi_SQL += ")";
                            Mi_SQL += ")";
                        }
                        Mi_SQL += " ORDER BY CLAVE";
                    }
                }
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) { 
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Ds_Datos != null && Ds_Datos.Tables.Count>0) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
            } catch (Exception Ex) {
                throw new Exception("Consultar_Bienes_Sin_Inventario::: [" + Ex.Message + "]");
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Empleados_Resguardos
        ///DESCRIPCIÓN          : Obtiene empleados de la Base de Datos y los regresa en un 
        ///                       DataTable de acuerdo a los filtros pasados.
        ///PARAMETROS           : Parametros.  Contiene los parametros que se van a utilizar para
        ///                                         hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 24/Octubre/2011 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static DataTable Consultar_Empleados_Resguardos(Cls_Ope_Pat_Bienes_Economicos_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            try
            {
                Mi_SQL = "SELECT " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " AS EMPLEADO_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_No_Empleado + " AS NO_EMPLEADO";
                Mi_SQL = Mi_SQL + ", REPLACE(LTRIM(RTRIM(ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + ", '')";
                Mi_SQL = Mi_SQL + " +' '+ ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + ", '')";
                Mi_SQL = Mi_SQL + " +' '+ ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ", ''))) , '  ', ' ') AS NOMBRE";
                Mi_SQL = Mi_SQL + ", (" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + "  +' - '+ " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ") AS DEPENDENCIA";
                Mi_SQL = Mi_SQL + ", " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias;
                Mi_SQL = Mi_SQL + " ON " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                Mi_SQL = Mi_SQL + " AND " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Tipo_Empleado + " = 'EMPLEADO'";
                if (Parametros.P_Dependencia_ID != null && Parametros.P_Dependencia_ID.Trim().Length > 0)
                {
                    Mi_SQL = Mi_SQL + " AND " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID.Trim() + "'";
                }
                if (Parametros.P_RFC_Resguardante != null && Parametros.P_RFC_Resguardante.Trim().Length > 0)
                {
                    Mi_SQL = Mi_SQL + " AND " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_RFC + " = '" + Parametros.P_RFC_Resguardante.Trim() + "'";
                }
                if (Parametros.P_No_Empleado_Resguardante != null && Parametros.P_No_Empleado_Resguardante.Trim().Length > 0)
                {
                    Mi_SQL = Mi_SQL + " AND " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_No_Empleado + " = '" + Convertir_A_Formato_ID(Convert.ToInt32(Parametros.P_No_Empleado_Resguardante.Trim()), 6) + "'";
                }
                if (Parametros.P_Nombre != null && Parametros.P_Nombre.Trim().Length > 0)
                {
                    Mi_SQL = Mi_SQL + " AND (REPLACE(LTRIM(RTRIM(ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + ", '')";
                    Mi_SQL = Mi_SQL + " +' '+ ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + ", '')";
                    Mi_SQL = Mi_SQL + " +' '+ ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ", ''))) , '  ', ' ') LIKE '%" + Parametros.P_Nombre.Trim() + "%'";
                    Mi_SQL = Mi_SQL + " OR REPLACE(LTRIM(RTRIM(ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ", '')";
                    Mi_SQL = Mi_SQL + " +' '+ ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + ", '')";
                    Mi_SQL = Mi_SQL + " +' '+ ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + ", ''))) , '  ', ' ') LIKE '%" + Parametros.P_Nombre.Trim() + "%')";
                }
                Mi_SQL = Mi_SQL + " ORDER BY NOMBRE";
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Bien_Economico
        ///DESCRIPCIÓN          : Carga un los Detalles de un Bien 
        ///PARAMETROS           : Parametros. Contiene los parametros que se van a Consultar
        ///                                   en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 03/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Cls_Ope_Pat_Bienes_Economicos_Negocio Consultar_Detalles_Bien_Economico(Cls_Ope_Pat_Bienes_Economicos_Negocio Parametros) {
            String Mi_SQL = "SELECT * FROM " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos +
                            " WHERE " + Ope_Pat_Bienes_Economicos.Campo_Bien_ID + " = '" + Parametros.P_Bien_ID + "'"; 
            Cls_Ope_Pat_Bienes_Economicos_Negocio Bien = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
            SqlDataReader Data_Reader;
            DataSet Ds_Bien = null;
            try {
                Data_Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                while (Data_Reader.Read()) {
                    Bien.P_Bien_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Bien_ID].ToString())) ? Convert.ToInt32(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Bien_ID]) : 0;
                    Bien.P_Gerencia_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Gerencia_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Gerencia_ID].ToString() : "";
                    Bien.P_Dependencia_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Dependencia_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Dependencia_ID].ToString() : "";
                    Bien.P_Producto_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Producto_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Producto_ID].ToString() : "";
                    Bien.P_Nombre = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Nombre].ToString())) ? Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Nombre].ToString() : "";
                    Bien.P_Marca = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Marca_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Marca_ID].ToString() : "";
                    Bien.P_Modelo = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Modelo].ToString())) ? Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Modelo].ToString() : "";
                    Bien.P_Costo_Inicial = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Costo_Inicial].ToString())) ? Convert.ToDouble(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Costo_Inicial]) : 0.0;
                    Bien.P_Material = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Material_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Material_ID].ToString() : "";
                    Bien.P_Color = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Color_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Color_ID].ToString() : "";
                    Bien.P_Estado = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Estado].ToString())) ? Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Estado].ToString() : "";
                    Bien.P_Estatus = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Estatus].ToString())) ? Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Estatus].ToString() : "";
                    Bien.P_Comentarios = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Comentarios].ToString())) ? Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Comentarios].ToString() : "";
                    Bien.P_Motivo_Baja = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Motivo_Baja].ToString())) ? Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Motivo_Baja].ToString() : "";
                    Bien.P_Fecha_Adquisicion = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Fecha_Adquisicion].ToString())) ? Convert.ToDateTime(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Fecha_Adquisicion]) : new DateTime();
                    Bien.P_Numero_Serie = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Numero_Serie].ToString())) ? Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Numero_Serie].ToString() : "";
                    Bien.P_Cantidad = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Cantidad].ToString())) ? Convert.ToInt32(Data_Reader[Ope_Pat_Bienes_Economicos.Campo_Cantidad]) : 0;
                }
                Data_Reader.Close();
                if (Bien.P_Bien_ID > (-1)) {
                    Mi_SQL = "SELECT (" + Ope_Pat_Bienes_Economicos.Campo_Usuario_Creo + " + ' [ ' + REPLACE(CONVERT(VARCHAR(11), " + Ope_Pat_Bienes_Economicos.Campo_Fecha_Creo + ", 106), ' ' ,'/') + ']') AS CREO";
                    Mi_SQL += ", (" + Ope_Pat_Bienes_Economicos.Campo_Usuario_Modifico + " + ' [ ' + REPLACE(CONVERT(VARCHAR(11), " + Ope_Pat_Bienes_Economicos.Campo_Fecha_Modifico + ", 106), ' ' ,'/') + ']') AS MODIFICO";
                    Mi_SQL += " FROM " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos;
                    Mi_SQL += " WHERE " + Ope_Pat_Bienes_Economicos.Campo_Bien_ID + " = '" + Bien.P_Bien_ID.ToString().Trim() + "'";
                    Data_Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    while (Data_Reader.Read()) {
                         Bien.P_Dato_Creo = (!String.IsNullOrEmpty(Data_Reader["CREO"].ToString())) ? Data_Reader["CREO"].ToString() : "";
                         Bien.P_Dato_Modifico = (!String.IsNullOrEmpty(Data_Reader["MODIFICO"].ToString())) ? Data_Reader["MODIFICO"].ToString() : "";
                     }
                     Data_Reader.Close();
                } 
                if (Bien.P_Bien_ID  > 0)
                {
                    Mi_SQL = "SELECT " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Bien_Resguardo_ID + " AS BIEN_RESGUARDO_ID";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Empleado_Resguardo_ID + " AS EMPLEADO_ID";
                    Mi_SQL = Mi_SQL + ", ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + ", '')";
                    Mi_SQL = Mi_SQL + " +' '+ ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + ", '')";
                    Mi_SQL = Mi_SQL + " +' '+ ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ", '') AS NOMBRE_EMPLEADO";
                    Mi_SQL = Mi_SQL + ", " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_No_Empleado + " AS NO_EMPLEADO";
                    Mi_SQL = Mi_SQL + ", " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Dependencia_ID + " AS DEPENDENCIA_ID";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Empleado_Almacen_ID + " AS EMPLEADO_ALMACEN_ID";
                    Mi_SQL = Mi_SQL + "," + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Comentarios + " AS COMENTARIOS";
                    Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Dependencias.Campo_Grupo_Dependencia_ID + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Dependencia_ID + ") AS GERENCIA_ID";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + ", " + Cat_Empleados.Tabla_Cat_Empleados;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Empleado_Resguardo_ID;
                    Mi_SQL = Mi_SQL + " = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID;
                    Mi_SQL = Mi_SQL + " AND " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Estatus;
                    Mi_SQL = Mi_SQL + " = 'VIGENTE'";
                    Mi_SQL = Mi_SQL + " AND " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Tipo;
                    Mi_SQL = Mi_SQL + " = 'RESGUARDO_ECONOMICO'";
                    Mi_SQL = Mi_SQL + " AND " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Bien_ID;
                    Mi_SQL = Mi_SQL + " = " + Bien.P_Bien_ID + "";
                    Ds_Bien = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                }
                if (Ds_Bien == null)
                {
                    Bien.P_Dt_Resguardantes = new DataTable();
                }
                else
                {
                    Bien.P_Dt_Resguardantes = Ds_Bien.Tables[0];
                }
                //Obtener el Historial de Resguardantes.
                Ds_Bien = null;
                if (Bien.P_Bien_ID > 0)
                {
                    Mi_SQL = "SELECT " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Bien_Resguardo_ID + " AS BIEN_RESGUARDO_ID";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Empleado_Resguardo_ID + " AS EMPLEADO_ID";
                    Mi_SQL = Mi_SQL + ", ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + ", '')";
                    Mi_SQL = Mi_SQL + " +' '+ ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + ", '')";
                    Mi_SQL = Mi_SQL + " +' '+ ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ", '') AS NOMBRE_EMPLEADO";
                    Mi_SQL = Mi_SQL + ", " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_No_Empleado + " AS NO_EMPLEADO";
                    Mi_SQL = Mi_SQL + ", " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Dependencia_ID + " AS DEPENDENCIA_ID";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Empleado_Almacen_ID + " AS EMPLEADO_ALMACEN_ID";
                    Mi_SQL = Mi_SQL + "," + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Comentarios + " AS COMENTARIOS";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Fecha_Inicial + " AS FECHA_INICIAL";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Fecha_Final + " AS FECHA_FINAL";
                    Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Dependencias.Campo_Grupo_Dependencia_ID + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Dependencia_ID + ") AS GERENCIA_ID";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + ", " + Cat_Empleados.Tabla_Cat_Empleados;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Empleado_Resguardo_ID;
                    Mi_SQL = Mi_SQL + " = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID;
                    Mi_SQL = Mi_SQL + " AND " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Estatus;
                    Mi_SQL = Mi_SQL + " = 'BAJA'";
                    Mi_SQL = Mi_SQL + " AND " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Tipo;
                    Mi_SQL = Mi_SQL + " = 'RESGUARDO_ECONOMICO'";
                    Mi_SQL = Mi_SQL + " AND " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + "." + Ope_Pat_Bienes_Resguardos.Campo_Bien_ID;
                    Mi_SQL = Mi_SQL + " = " + Bien.P_Bien_ID + "";
                    Ds_Bien = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                }
                if (Ds_Bien == null)
                {
                    Bien.P_Dt_Historial_Resguardos = new DataTable();
                }
                else
                {
                    Bien.P_Dt_Historial_Resguardos = Ds_Bien.Tables[0];
                }
            } catch (Exception Ex) {
                String Mensaje = "Error al intentar consultar los datos de la Parte. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Bien;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARÁMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID, String Campo_Condicion, String Valor_Condicion, String Campo_Condicion_2, String Valor_Condicion_2)
        {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try
            {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla + " WHERE " + Campo_Condicion + " = '" + Valor_Condicion + "'";
                if (!String.IsNullOrEmpty(Campo_Condicion_2) && !String.IsNullOrEmpty(Valor_Condicion_2))
                {
                    Mi_SQL += " AND " + Campo_Condicion_2 + " = '" + Valor_Condicion_2 + "'";
                }
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            }
            catch (SqlException Ex)
            {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID) {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals("")) {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            } catch (SqlException Ex) {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
        ///PARAMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID) {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (Int32 Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++) {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Diferencia_Resguardos
        ///DESCRIPCIÓN: Saca la diferencia de unos resguardantes a otros.
        ///PARAMETROS:     
        ///             1. Actuales.        Bien Mueble como esta actualmente en la Base de Datos.
        ///             2. Actualizados.    Bien Mueble como quiere que quede al Actualizarlo.
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 02/Diciembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private static Cls_Ope_Pat_Bienes_Economicos_Negocio Obtener_Diferencia_Resguardos(Cls_Ope_Pat_Bienes_Economicos_Negocio Comparar, Cls_Ope_Pat_Bienes_Economicos_Negocio Base_Comparacion) {
            Cls_Ope_Pat_Bienes_Economicos_Negocio Resguardos = new Cls_Ope_Pat_Bienes_Economicos_Negocio();

            //SE OBTIENEN LOS NUEVOS RESGUARDANTES QUE SE DIERON DE ALTA PARA UN BIEN MUEBLE
            DataTable Dt_Tabla = new DataTable();
            Dt_Tabla.Columns.Add("BIEN_RESGUARDO_ID", Type.GetType("System.Int32"));
            Dt_Tabla.Columns.Add("EMPLEADO_ID", Type.GetType("System.String"));
            Dt_Tabla.Columns.Add("NOMBRE_EMPLEADO", Type.GetType("System.String"));
            Dt_Tabla.Columns.Add("COMENTARIOS", Type.GetType("System.String"));

            if (Comparar.P_Dt_Resguardantes != null) {
                for (int Contador_1 = 0; Contador_1 < Comparar.P_Dt_Resguardantes.Rows.Count; Contador_1++) {
                    Boolean Eliminar = true;
                    if (Base_Comparacion.P_Dt_Resguardantes != null) { 
                        for (int Contador_2 = 0; Contador_2 < Base_Comparacion.P_Dt_Resguardantes.Rows.Count; Contador_2++) {
                            if (Comparar.P_Dt_Resguardantes.Rows[Contador_1]["EMPLEADO_ID"].ToString().Equals(Base_Comparacion.P_Dt_Resguardantes.Rows[Contador_2]["EMPLEADO_ID"].ToString()))
                            {
                                Eliminar = false;
                                break;
                            }
                        }
                    }
                    if (Eliminar) {
                        DataRow Fila = Dt_Tabla.NewRow();
                        Fila["BIEN_RESGUARDO_ID"] = Convert.ToInt32(Comparar.P_Dt_Resguardantes.Rows[Contador_1]["BIEN_RESGUARDO_ID"]);
                        Fila["EMPLEADO_ID"] = Comparar.P_Dt_Resguardantes.Rows[Contador_1]["EMPLEADO_ID"].ToString();
                        Fila["NOMBRE_EMPLEADO"] = Comparar.P_Dt_Resguardantes.Rows[Contador_1]["NOMBRE_EMPLEADO"].ToString();
                        Fila["COMENTARIOS"] = Comparar.P_Dt_Resguardantes.Rows[Contador_1]["COMENTARIOS"].ToString();
                        Dt_Tabla.Rows.Add(Fila);
                    }
                }
            }

            Resguardos.P_Dt_Resguardantes = Dt_Tabla;
            return Resguardos;
        }

    }

}
