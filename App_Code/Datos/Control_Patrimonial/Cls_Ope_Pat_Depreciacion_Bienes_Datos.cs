﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Control_Patrimonial_Operacion_Depreciacion_Bienes.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Polizas.Negocios;

/// <summary>
/// Summary description for Cls_Ope_Pat_Depreciacion_Bienes_Datos
/// </summary>
/// 
namespace JAPAMI.Control_Patrimonial_Operacion_Depreciacion_Bienes.Datos {
    public class Cls_Ope_Pat_Depreciacion_Bienes_Datos {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Bienes_Muebles
        ///DESCRIPCIÓN: Consulta los Bienes Muebles para Depreciar
        ///PROPIEDADES:     
        ///CREO:                 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: Noviembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Bienes_Muebles(Cls_Ope_Pat_Depreciacion_Bienes_Negocio Parametros) {
            String Mi_SQL = null;
            DataSet Ds_Bienes_Muebles = null;
            DataTable Dt_Bienes_Muebles = new DataTable();
            Boolean Entro_Where = false;
            try {
                Mi_SQL = "SELECT " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + " AS CLAVE_BIEN";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_No_Inventario_Anterior + " AS INVENTARIO_ANTERIOR";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " AS INVENTARIO_SIAS";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Costo_Actual + " AS VALOR_ANTERIOR";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Costo_Inicial + " AS VALOR_INCIAL";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Costo_Actual + " AS VALOR_ACTUAL";
                Mi_SQL = Mi_SQL + ", 0.0 AS VALOR_DEPRECIADO"; 
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Fecha_Adquisicion + " AS FECHA_ADQUISICION";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Clase_Activo_ID + " AS CLASE_ACTIVO_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo + "." + Cat_Pat_Clases_Activo.Campo_Anios_Vida_Util + " AS ANIOS_VIDA_UTIL";
                Mi_SQL = Mi_SQL + ", " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo + "." + Cat_Pat_Clases_Activo.Campo_Porcentaje_Depreciacion + " AS PORCENTAJE_DEPRECIACION";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Cuenta_Gasto_ID + " AS CUENTA_BIEN_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo + "." + Cat_Pat_Clases_Activo.Campo_Cuenta_Depreciacion_ID + " AS CUENTA_CLASE_ID";
                Mi_SQL = Mi_SQL + ", 'BIEN_MUEBLE' AS TIPO_BIEN";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo;
                Mi_SQL = Mi_SQL + " ON " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Clase_Activo_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo + "." + Cat_Pat_Clases_Activo.Campo_Clase_Activo_ID;
                if (Parametros.P_Tipo_Activo_ID != null && Parametros.P_Tipo_Activo_ID.Trim().Length > 0) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Clasificacion_ID + " = '" + Parametros.P_Tipo_Activo_ID + "'";
                } else {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Clasificacion_ID + " IS NOT NULL";
                }
                if (Parametros.P_Clase_Activo_ID != null && Parametros.P_Clase_Activo_ID.Trim().Length > 0) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Clase_Activo_ID + " = '" + Parametros.P_Clase_Activo_ID + "'";
                }
                if (Parametros.P_Estatus != null && Parametros.P_Estatus.Trim().Length > 0) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                }
                if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Limite).Equals(String.Format("{0:ddMMyyyy}", new DateTime())))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "(" + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Fecha_Ultima_Depreciacion + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Limite) + "'";
                    Mi_SQL = Mi_SQL + " OR " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Fecha_Ultima_Depreciacion + " IS NULL)";
                }
                if (Parametros.P_Valor_Minimo > (-1)) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Costo_Actual + " > " + Parametros.P_Valor_Minimo + "";
                }
                Ds_Bienes_Muebles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Bienes_Muebles != null) {
                    Dt_Bienes_Muebles = Ds_Bienes_Muebles.Tables[0];
                }
            } catch (Exception Ex) {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Bienes_Muebles;            
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Vehiculos
        ///DESCRIPCIÓN: Consulta los Vehiculos para Depreciar
        ///PROPIEDADES:     
        ///CREO:                 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: Noviembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Vehiculos(Cls_Ope_Pat_Depreciacion_Bienes_Negocio Parametros) {
            String Mi_SQL = null;
            DataSet Ds_Vehiculos = null;
            DataTable Dt_Vehiculos = new DataTable();
            Boolean Entro_Where = false;
            try {
                Mi_SQL = "SELECT " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + " AS CLAVE_BIEN";
                Mi_SQL = Mi_SQL + ", LTRIM(RTRIM(STR(" + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + "))) AS INVENTARIO_ANTERIOR";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS INVENTARIO_SIAS";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Costo_Actual + " AS VALOR_ANTERIOR";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Costo_Inicial + " AS VALOR_INCIAL";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Costo_Actual + " AS VALOR_ACTUAL";
                Mi_SQL = Mi_SQL + ", 0.0 AS VALOR_DEPRECIADO"; 
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Fecha_Adquisicion + " AS FECHA_ADQUISICION";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Clase_Activo_ID + " AS CLASE_ACTIVO_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo + "." + Cat_Pat_Clases_Activo.Campo_Anios_Vida_Util + " AS ANIOS_VIDA_UTIL";
                Mi_SQL = Mi_SQL + ", " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo + "." + Cat_Pat_Clases_Activo.Campo_Porcentaje_Depreciacion + " AS PORCENTAJE_DEPRECIACION";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Cuenta_Gasto_ID + " AS CUENTA_BIEN_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo + "." + Cat_Pat_Clases_Activo.Campo_Cuenta_Depreciacion_ID + " AS CUENTA_CLASE_ID";
                Mi_SQL = Mi_SQL + ", 'VEHICULO' AS TIPO_BIEN";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo;
                Mi_SQL = Mi_SQL + " ON " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Clase_Activo_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo + "." + Cat_Pat_Clases_Activo.Campo_Clase_Activo_ID;
                if (Parametros.P_Tipo_Activo_ID != null && Parametros.P_Tipo_Activo_ID.Trim().Length > 0) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Clasificacion_ID + " = '" + Parametros.P_Tipo_Activo_ID + "'";
                } else {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Clasificacion_ID + " IS NOT NULL";
                }
                if (Parametros.P_Clase_Activo_ID != null && Parametros.P_Clase_Activo_ID.Trim().Length > 0) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Clase_Activo_ID + " = '" + Parametros.P_Clase_Activo_ID + "'";
                }
                if (Parametros.P_Estatus != null && Parametros.P_Estatus.Trim().Length > 0) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                }
                if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Limite).Equals(String.Format("{0:ddMMyyyy}", new DateTime())))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "(" + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Fecha_Ultima_Depreciacion + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Limite) + "'";
                    Mi_SQL = Mi_SQL + " OR " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Fecha_Ultima_Depreciacion + " IS NULL)";
                }
                if (Parametros.P_Valor_Minimo > (-1))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Costo_Actual + " > " + Parametros.P_Valor_Minimo + "";
                }
                Ds_Vehiculos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Vehiculos != null)
                {
                    Dt_Vehiculos = Ds_Vehiculos.Tables[0];
                }
            } catch (Exception Ex) {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Vehiculos;            
        }
       
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Actualizar_Bienes_Depreciados
        ///DESCRIPCIÓN: Actualiza los Bienes con su valor ya depreciado.
        ///PROPIEDADES:     
        ///CREO:                 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: Noviembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static String[] Actualizar_Bienes_Depreciados(Cls_Ope_Pat_Depreciacion_Bienes_Negocio Parametros)
        {
            Cls_Ope_Con_Polizas_Negocio Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión hacia la capa de negocios para envio de los datos a dar de alta
            String Mensaje = "";
            String[] Datos_Poliza = null;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Boolean Ejecutar = false;
            try {
                String Tipo_Poliza_ID = Consultar_Datos_Poliza("TIPO_POLIZA");
                DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
                Int32 No_Depreciacion = Convert.ToInt32(Obtener_ID_Consecutivo(ref Cmd, Ope_Pat_Depreciacion.Tabla_Ope_Pat_Depreciacion, Ope_Pat_Depreciacion.Campo_No_Depreciacion, 5));
                String Mi_SQL = "";
                // Se crea el registro de la depreciación para el historial
                Mi_SQL = "INSERT INTO " + Ope_Pat_Depreciacion.Tabla_Ope_Pat_Depreciacion + " ( ";
                Mi_SQL += Ope_Pat_Depreciacion.Campo_No_Depreciacion + "," + Ope_Pat_Depreciacion.Campo_Tipo_Bien + ", ";
                Mi_SQL += Ope_Pat_Depreciacion.Campo_Tipo_Activo_ID + "," + Ope_Pat_Depreciacion.Campo_Clase_Activo_ID + ", ";
                Mi_SQL += Ope_Pat_Depreciacion.Campo_Usuario_Deprecio_ID + "," + Ope_Pat_Depreciacion.Campo_Usuario_Deprecio_Nombre + ", ";
                Mi_SQL += Ope_Pat_Depreciacion.Campo_Fecha_Depreciacion + ") VALUES(";
                Mi_SQL += No_Depreciacion + ", '" + Parametros.P_Tipo_Bien + "', ";
                if (!String.IsNullOrEmpty(Parametros.P_Tipo_Activo_ID)) {
                    Mi_SQL += "'" + Parametros.P_Tipo_Activo_ID + "', ";
                } else {
                    Mi_SQL += "NULL , ";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Clase_Activo_ID)) {
                    Mi_SQL += "'" + Parametros.P_Clase_Activo_ID + "', ";
                } else {
                    Mi_SQL += "NULL , ";
                }
                Mi_SQL += "'" + Parametros.P_Usuario_ID.Trim() + "', '" + Parametros.P_Usuario_ID.Trim() + "', GETDATE())";
                if (!String.IsNullOrEmpty(Mi_SQL)) {
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                //Se crean las columnas del datatable que contendran los detalles de la poliza
                if (Dt_Partidas_Polizas.Rows.Count == 0)
                {
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                    Dt_Partidas_Polizas.Columns.Add("BENEFICIARIO_ID", typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Area_Funcional_ID, typeof(System.String));
                }

                foreach (DataRow Fila in Parametros.P_Dt_Bienes_Depreciar.Rows) {
                    Mi_SQL = "";
                    if (Fila["TIPO_BIEN"].ToString().Trim().Equals("BIEN_MUEBLE")) {
                        Mi_SQL = "UPDATE " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " SET ";
                        Mi_SQL += Ope_Pat_Bienes_Muebles.Campo_Costo_Actual + " = '" + Convert.ToDouble(Fila["VALOR_ACTUAL"]) + "'";
                        Mi_SQL += ", " + Ope_Pat_Bienes_Muebles.Campo_Fecha_Ultima_Depreciacion + " = GETDATE()";
                        Mi_SQL += " WHERE " + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + " = '" + Fila["CLAVE_BIEN"].ToString().Trim() + "'";
                    } else if (Fila["TIPO_BIEN"].ToString().Trim().Equals("VEHICULO")) {
                        Mi_SQL = "UPDATE " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " SET ";
                        Mi_SQL += Ope_Pat_Vehiculos.Campo_Costo_Actual + " = '" + Convert.ToDouble(Fila["VALOR_ACTUAL"]) + "'";
                        Mi_SQL += ", " + Ope_Pat_Vehiculos.Campo_Fecha_Ultima_Depreciacion + " = GETDATE()";
                        Mi_SQL += " WHERE " + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + " = '" + Fila["CLAVE_BIEN"].ToString().Trim() + "'";
                    } else if (Fila["TIPO_BIEN"].ToString().Trim().Equals("BIEN_INMUEBLE")) {
                        Mi_SQL = "UPDATE " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + " SET ";
                        Mi_SQL += Ope_Pat_Bienes_Inmuebles.Campo_Valor_Actual + " = '" + Convert.ToDouble(Fila["VALOR_ACTUAL"]) + "'";
                        Mi_SQL += ", " + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Ultima_Depreciacion + " = GETDATE()";
                        Mi_SQL += " WHERE " + Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID + " = '" + Fila["CLAVE_BIEN"].ToString().Trim() + "'";
                    } 
                    if (!String.IsNullOrEmpty(Mi_SQL)) {
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        Ejecutar = true;
                    }
                    Int32 No_Movimiento = Convert.ToInt32(Obtener_ID_Consecutivo(ref Cmd, Ope_Pat_Depreciacion_Det.Tabla_Ope_Pat_Depreciacion_Det, Ope_Pat_Depreciacion_Det.Campo_No_Movimiento, 1));
                    Mi_SQL = "";

                    // insertan los detalles de la depreciacion
                    Mi_SQL = "INSERT INTO " + Ope_Pat_Depreciacion_Det.Tabla_Ope_Pat_Depreciacion_Det + " ( ";
                    Mi_SQL += Ope_Pat_Depreciacion_Det.Campo_No_Movimiento + "," + Ope_Pat_Depreciacion_Det.Campo_No_Depreciacion + ", ";
                    Mi_SQL += Ope_Pat_Depreciacion_Det.Campo_Tipo_Bien + "," + Ope_Pat_Depreciacion_Det.Campo_Bien_ID + ", ";
                    Mi_SQL += Ope_Pat_Depreciacion_Det.Campo_Valor_Anterior + "," + Ope_Pat_Depreciacion_Det.Campo_Valor_Actualizado;
                    Mi_SQL += "," + Ope_Pat_Depreciacion_Det.Campo_Valor_Inicial + "," + Ope_Pat_Depreciacion_Det.Campo_Valor_Depreciado; 
                    Mi_SQL += "," + Ope_Pat_Depreciacion_Det.Campo_Cuenta_Bien_ID + "," + Ope_Pat_Depreciacion_Det.Campo_Cuenta_Clase_ID + ") VALUES(";
                    Mi_SQL += No_Movimiento + ", " + No_Depreciacion + ", '";
                    Mi_SQL += Fila["TIPO_BIEN"].ToString() + "', '" + Fila["CLAVE_BIEN"].ToString();
                    Mi_SQL += "', '" + Fila["VALOR_ANTERIOR"].ToString() + "', '" + Fila["VALOR_ACTUAL"].ToString();
                    Mi_SQL += "', '" + Fila["VALOR_INCIAL"].ToString() + "', '" + Fila["VALOR_DEPRECIADO"].ToString();
                    Mi_SQL += "', '" + Fila["CUENTA_BIEN_ID"].ToString() + "', '" + Fila["CUENTA_CLASE_ID"].ToString() + "') ";
                    if (!String.IsNullOrEmpty(Mi_SQL))
                    {
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                }

                ///CARGAR DETALLES POLIZA DEPRECIACION
                DataRow[] Detalles_Poliza_Datos = Parametros.P_Dt_Bienes_Depreciar.Select("CUENTA_BIEN_ID IS NOT NULL AND CUENTA_CLASE_ID IS NOT NULL");
                if (Detalles_Poliza_Datos.Length > 0) {
                    Int32 Contador = 1;
                    foreach (DataRow Fila_Actual in Detalles_Poliza_Datos)
                    {
                        DataRow Fila_Nueva = Dt_Partidas_Polizas.NewRow();
                        Fila_Nueva[Ope_Con_Polizas_Detalles.Campo_Partida] = Contador;
                        Fila_Nueva[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Fila_Actual["CUENTA_BIEN_ID"].ToString().Trim();
                        Fila_Nueva[Ope_Con_Polizas_Detalles.Campo_Concepto] = "Depreciación a " + Fila_Actual["TIPO_BIEN"].ToString().Replace("_", " ") + " con No. Inventario " + Fila_Actual["INVENTARIO_SIAS"].ToString();
                        Fila_Nueva[Ope_Con_Polizas_Detalles.Campo_Haber] = 0.0;
                        Fila_Nueva[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Fila_Actual["VALOR_DEPRECIADO"].ToString());
                        Dt_Partidas_Polizas.Rows.Add(Fila_Nueva);
                        Contador++;
                        DataRow Fila_Nueva_ = Dt_Partidas_Polizas.NewRow();
                        Fila_Nueva_[Ope_Con_Polizas_Detalles.Campo_Partida] = Contador;
                        Fila_Nueva_[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Fila_Actual["CUENTA_CLASE_ID"].ToString().Trim();
                        Fila_Nueva_[Ope_Con_Polizas_Detalles.Campo_Concepto] = "Depreciación a " + Fila_Actual["TIPO_BIEN"].ToString().Replace("_", " ") + " con No. Inventario " + Fila_Actual["INVENTARIO_SIAS"].ToString();
                        Fila_Nueva_[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Fila_Actual["VALOR_DEPRECIADO"].ToString());
                        Fila_Nueva_[Ope_Con_Polizas_Detalles.Campo_Debe] = 0.0;
                        Dt_Partidas_Polizas.Rows.Add(Fila_Nueva_);
                        Contador++;
                    }

                    ///GENERACIÓN DE POLIZA PARA DEPRECIACIÓN
                    Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = Tipo_Poliza_ID;
                    Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now);
                    Rs_Alta_Ope_Con_Polizas.P_Fecha_Poliza = DateTime.Now;
                    Rs_Alta_Ope_Con_Polizas.P_Concepto = "DEPRECIACIÓN No. " + No_Depreciacion + " APLICADA A LOS BIENES ";
                    Rs_Alta_Ope_Con_Polizas.P_Concepto += String.Format("{0:MMMMMMMMMMMMMMM 'de' yyyy}", DateTime.Now).ToUpper();
                    Rs_Alta_Ope_Con_Polizas.P_Total_Debe = Convert.ToDouble(Parametros.P_Dt_Bienes_Depreciar.Compute("Sum(VALOR_DEPRECIADO)", "VALOR_DEPRECIADO <> 0 AND CUENTA_BIEN_ID IS NOT NULL AND CUENTA_CLASE_ID IS NOT NULL")); //monto total con iva de productos
                    Rs_Alta_Ope_Con_Polizas.P_Total_Haber = Convert.ToDouble(Parametros.P_Dt_Bienes_Depreciar.Compute("Sum(VALOR_DEPRECIADO)", "VALOR_DEPRECIADO <> 0 AND CUENTA_BIEN_ID IS NOT NULL AND CUENTA_CLASE_ID IS NOT NULL")); //monto total con iva de productos
                    Rs_Alta_Ope_Con_Polizas.P_No_Partida = Dt_Partidas_Polizas.Rows.Count;
                    Rs_Alta_Ope_Con_Polizas.P_Nombre_Usuario = Parametros.P_Nombre_Usuario;
                    Rs_Alta_Ope_Con_Polizas.P_Dt_Detalles_Polizas = Dt_Partidas_Polizas;
                    Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Creo = Parametros.P_Usuario_ID;
                    Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Autorizo = Parametros.P_Usuario_ID;
                    Rs_Alta_Ope_Con_Polizas.P_Prefijo = "";
                    Rs_Alta_Ope_Con_Polizas.P_Cmmd = Cmd;
                    Datos_Poliza = Rs_Alta_Ope_Con_Polizas.Alta_Poliza();
                }                
                if (Ejecutar) Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
            return Datos_Poliza;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Inventario
        ///DESCRIPCIÓN: Consulta el numero de inventario 
        ///PROPIEDADES:     
        ///CREO:                 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: Noviembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private static String Consultar_Inventario(String CLAVE_PRODUCTO, String TIPO_BIEN, SqlCommand P_Cmmd)
        {
            String Mi_SQL = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            Object NO_INVENTARIO = null;
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {
                if (TIPO_BIEN == "BIEN_MUEBLE")
                {
                    Mi_SQL = "SELECT  " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario;
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + "." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + " = '" + CLAVE_PRODUCTO + "'";
                }
                else
                {
                    if (TIPO_BIEN == "VEHICULO")
                    {
                        Mi_SQL = "SELECT  " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Numero_Inventario;
                        Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos;
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + "." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + " = '" + CLAVE_PRODUCTO + "'";
                    }
                }
                Cmmd.CommandText = Mi_SQL; //Realiza la ejecuón de la obtención del ID del empleado
                NO_INVENTARIO = Cmmd.ExecuteScalar();
                if (P_Cmmd == null)
                {
                    Trans.Commit();
                }
                return NO_INVENTARIO.ToString();
            }
            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARÁMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(ref SqlCommand Cmmd, String Tabla, String Campo, Int32 Longitud_ID)
        {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try
            {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Cmmd.CommandText = Mi_SQL;
                Object Obj_Temp = Cmmd.ExecuteScalar();
                //Object Obj_Temp = OracleHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            }
            catch (SqlException Ex)
            {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
        ///PARÁMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
        {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
            {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Datos_Poliza
        ///DESCRIPCIÓN: Consultar_Datos_Poliza
        ///PARÁMETROS: 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Consultar_Datos_Poliza(String Dato) {
            String Dato_Requerido = String.Empty;
            try {
                DataSet Ds_Datos = null;
                String Mi_SQL = String.Empty;
                if (Dato.Trim().Equals("TIPO_POLIZA")) Mi_SQL = "SELECT TIPO_POLIZA_BAJA_ID FROM CAT_CON_PARAMETROS";
                if (!String.IsNullOrEmpty(Mi_SQL))
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Datos != null) if (Ds_Datos.Tables.Count > 0) if (Ds_Datos.Tables[0].Rows.Count > 0) Dato_Requerido = Ds_Datos.Tables[0].Rows[0]["TIPO_POLIZA_BAJA_ID"].ToString().Trim();
            }catch (Exception Ex) {
                throw new Exception(Ex.Message);
            }
            return Dato_Requerido;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Bienes_Inmuebles
        ///DESCRIPCIÓN: Consulta los Bienes Inmuebles para Depreciar
        ///PROPIEDADES:     
        ///CREO:                 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: Noviembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Bienes_Inmuebles(Cls_Ope_Pat_Depreciacion_Bienes_Negocio Parametros) {
            String Mi_SQL = null;
            DataSet Ds_Bienes_Inmuebles = null;
            DataTable Dt_Bienes_Inmuebles = new DataTable();
            Boolean Entro_Where = false;
            try {
                Mi_SQL = "SELECT " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID + " AS CLAVE_BIEN";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID + " AS INVENTARIO_ANTERIOR";
                Mi_SQL = Mi_SQL + ", CONVERT(DECIMAL, " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID + ") AS INVENTARIO_SIAS";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Valor_Actual + " AS VALOR_ANTERIOR";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Valor_Fiscal + " AS VALOR_INCIAL";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Valor_Actual + " AS VALOR_ACTUAL";
                Mi_SQL = Mi_SQL + ", 0.0 AS VALOR_DEPRECIADO";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Alta_Cta_Pub + " AS FECHA_ADQUISICION";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Clase_Activo_ID + " AS CLASE_ACTIVO_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo + "." + Cat_Pat_Clases_Activo.Campo_Anios_Vida_Util + " AS ANIOS_VIDA_UTIL";
                Mi_SQL = Mi_SQL + ", " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo + "." + Cat_Pat_Clases_Activo.Campo_Porcentaje_Depreciacion + " AS PORCENTAJE_DEPRECIACION";
                Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Gasto_ID + " AS CUENTA_BIEN_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo + "." + Cat_Pat_Clases_Activo.Campo_Cuenta_Depreciacion_ID + " AS CUENTA_CLASE_ID";
                Mi_SQL = Mi_SQL + ", 'BIEN_INMUEBLE' AS TIPO_BIEN";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo;
                Mi_SQL = Mi_SQL + " ON " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Clase_Activo_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo + "." + Cat_Pat_Clases_Activo.Campo_Clase_Activo_ID;
                if (Parametros.P_Clase_Activo_ID != null && Parametros.P_Clase_Activo_ID.Trim().Length > 0) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Clase_Activo_ID + " = '" + Parametros.P_Clase_Activo_ID + "'";
                }
                if (Parametros.P_Estatus != null && Parametros.P_Estatus.Trim().Length > 0) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Estado + " = '" + Parametros.P_Estatus + "'";
                }
                if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Limite).Equals(String.Format("{0:ddMMyyyy}", new DateTime())))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "(" + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Ultima_Depreciacion + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Limite) + "'";
                    Mi_SQL = Mi_SQL + " OR " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Ultima_Depreciacion + " IS NULL)";
                }
                if (Parametros.P_Valor_Minimo > (-1)) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + "." + Ope_Pat_Bienes_Inmuebles.Campo_Valor_Fiscal + " > " + Parametros.P_Valor_Minimo + "";
                }
                Ds_Bienes_Inmuebles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Bienes_Inmuebles != null) {
                    Dt_Bienes_Inmuebles = Ds_Bienes_Inmuebles.Tables[0];
                }
            } catch (Exception Ex) {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Bienes_Inmuebles;            
        }

	}
}