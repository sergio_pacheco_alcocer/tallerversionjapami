﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Control_Patrimonial_Operacion_Bienes_Inmuebles.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using JAPAMI.Polizas.Negocios;

/// <summary>
/// Summary description for Cls_Ope_Pat_Bienes_Inmuebles_Datos
/// </summary>

namespace JAPAMI.Control_Patrimonial_Operacion_Bienes_Inmuebles.Datos { 
    
    public class Cls_Ope_Pat_Bienes_Inmuebles_Datos {

        #region "Metodos"

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Bien_Inmueble
            ///DESCRIPCIÓN          : Da de alta en la Base de Datos un nuevo registro.
            ///PARAMETROS           : Parametros.   Contiene los parametros que se van a dar de
            ///                                     Alta en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 28/Febrero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Cls_Ope_Pat_Bienes_Inmuebles_Negocio Alta_Bien_Inmueble(Cls_Ope_Pat_Bienes_Inmuebles_Negocio Parametros) {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                String Mi_SQL = "";
                try {

                    Parametros.P_Bien_Inmueble_ID = Obtener_ID_Consecutivo(Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles, Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID, 10);
                    Mi_SQL = "INSERT INTO " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles;
                    Mi_SQL = Mi_SQL + " (" + Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Clave;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Nombre;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Calle;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Colonia;
                    if(!String.IsNullOrEmpty(Parametros.P_Uso_ID))
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Uso_ID;
                    if (Parametros.P_Superficie > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Superficie;
                    }
                    if (Parametros.P_Construccion_Construida > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Construccion;
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Manzana;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Predial;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Lote;
                    if (Parametros.P_Ocupacion > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Porcentaje_Ocupacion;
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Efectos_Fiscales;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Sector_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Distrito_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Clasificacion_Zona_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Tipo_Predio_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Vias_Acceso;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Estado;
                    if (Parametros.P_Densidad_Construccion > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Densidad_Construccion;
                    }
                    if (Parametros.P_Valor_Comercial > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Valor_Comercial;
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Numero_Exterior;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Numero_Interior;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Destino_ID;
                    if (!String.IsNullOrEmpty(Parametros.P_Origen_ID))
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Origen_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Estatus;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Area_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Registro;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Hoja;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Tomo;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Numero_Acta;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Cartilla_Parcelaria;
                    if (Parametros.P_Superficie_Contable > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Superficie_Contable;
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Unidad_Superficie;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Tipo_Bien;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Registro_Propiedad;
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Alta_Cuenta_Publica).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Alta_Cta_Pub;
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Clase_Activo_ID;
                    if (!String.IsNullOrEmpty(Parametros.P_Cuenta_Contable_ID))
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Contable_ID;
                    if (!String.IsNullOrEmpty(Parametros.P_Cuenta_Gasto_ID))
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Gasto_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Nombre_Perito;
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Avaluo).Equals(String.Format("{0:ddMMyyyy}", new DateTime())))
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Avaluo;
                    if (Parametros.P_Valor_Fiscal > (-1))
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Valor_Fiscal;
                    if (Parametros.P_Valor_Actual > (-1))
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Valor_Actual;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Clase_Activo_Terreno_ID;
                    if (!String.IsNullOrEmpty(Parametros.P_Cuenta_Contable_Terreno_ID))
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Contable_Terreno;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Nombre_Perito_Terreno;
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Avaluo_Terreno).Equals(String.Format("{0:ddMMyyyy}", new DateTime())))
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Avaluo_Terreno;
                    if (Parametros.P_Valor_Fiscal_Terreno > (-1))
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Valor_Fiscal_Terreno;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Usuario_Creo;
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Creo;
                    Mi_SQL = Mi_SQL + ") VALUES ('" + Parametros.P_Bien_Inmueble_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Clave + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Nombre_Comun + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Calle + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Colonia + "'";
                    if (!String.IsNullOrEmpty(Parametros.P_Uso_ID))
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Uso_ID + "'";
                    if (Parametros.P_Superficie > (-1)) {
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Superficie + "'";
                    }
                    if (Parametros.P_Construccion_Construida > (-1)) {
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Construccion_Construida + "'";
                    }
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Manzana + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Cuenta_Predial_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Lote + "'";
                    if (Parametros.P_Ocupacion > (-1)) {
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Ocupacion + "'";
                    }
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Efectos_Fiscales + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Sector_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Distrito_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Clasificacion_Zona_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo_Predio_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Vias_Acceso + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estado + "'";
                    if (Parametros.P_Densidad_Construccion > (-1)) {
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Densidad_Construccion + "'";
                    }
                    if (Parametros.P_Valor_Comercial > (-1)) {
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Valor_Comercial + "'";
                    }
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Exterior + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Interior+ "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Destino_ID + "'";
                    if (!String.IsNullOrEmpty(Parametros.P_Origen_ID))
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Origen_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Area_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Registro) + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Hoja + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tomo + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Numero_Acta + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Cartilla_Parcelaria + "'";
                    if (Parametros.P_Superficie_Contable > (-1)) {
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Superficie_Contable + "'";
                    }
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Unidad_Superficie + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo_Bien + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Registro_Propiedad + "'";
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Alta_Cuenta_Publica).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) {
                        Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Alta_Cuenta_Publica) + "'";
                    }
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Clase_Activo_ID + "'";
                    if (!String.IsNullOrEmpty(Parametros.P_Cuenta_Contable_ID))
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Cuenta_Contable_ID + "'";
                    if (!String.IsNullOrEmpty(Parametros.P_Cuenta_Gasto_ID))
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Cuenta_Gasto_ID+ "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Nombre_Perito + "'";
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Avaluo).Equals(String.Format("{0:ddMMyyyy}", new DateTime())))
                        Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Avaluo) + "'";
                    if (Parametros.P_Valor_Fiscal > (-1))
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Valor_Fiscal + "'";
                    if (Parametros.P_Valor_Actual > (-1))
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Valor_Actual + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Clase_Activo_Terreno_ID + "'";
                    if (!String.IsNullOrEmpty(Parametros.P_Cuenta_Contable_Terreno_ID))
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Cuenta_Contable_Terreno_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Nombre_Perito_Terreno + "'";
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Avaluo_Terreno).Equals(String.Format("{0:ddMMyyyy}", new DateTime())))
                        Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Avaluo_Terreno) + "'";
                    if (Parametros.P_Valor_Fiscal_Terreno > (-1))
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Valor_Fiscal_Terreno + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                    Mi_SQL = Mi_SQL + ", GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    if (!String.IsNullOrEmpty(Parametros.P_Observaciones)) {
                        Int32 No_Observacion = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_B_Inm_Observaciones.Tabla_Ope_Pat_B_Inm_Observaciones, Ope_Pat_B_Inm_Observaciones.Campo_No_Observacion, 20));
                        Mi_SQL = "INSERT INTO " + Ope_Pat_B_Inm_Observaciones.Tabla_Ope_Pat_B_Inm_Observaciones;
                        Mi_SQL = Mi_SQL + " (" + Ope_Pat_B_Inm_Observaciones.Campo_No_Observacion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Bien_Inmueble_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Fecha_Observacion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Usuario_Observacion_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Observacion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Fecha_Creo;
                        Mi_SQL = Mi_SQL + ") VALUES ('" + No_Observacion + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE()";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_ID + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Observaciones + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    if (Parametros.P_Dt_Medidas_Colindancias != null && Parametros.P_Dt_Medidas_Colindancias.Rows.Count > 0) {
                        Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas, Ope_Pat_B_Inm_Medidas.Campo_No_Registro, 20));
                        foreach (DataRow Fila in Parametros.P_Dt_Medidas_Colindancias.Rows) {
                            Mi_SQL = "INSERT INTO " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas;
                            Mi_SQL = Mi_SQL + " (" + Ope_Pat_B_Inm_Medidas.Campo_No_Registro;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Campo_Bien_Inmueble_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Campo_Orientacion_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Campo_Medida;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Campo_Colindancia;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Campo_Usuario_Creo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Campo_Fecha_Creo;
                            Mi_SQL = Mi_SQL + ") VALUES ('" + No_Registro + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'";
                            Mi_SQL = Mi_SQL + ", '" + Fila["ORIENTACION_ID"].ToString() + "'";
                            Mi_SQL = Mi_SQL + ", '" + Fila["MEDIDA"].ToString() + "'";
                            Mi_SQL = Mi_SQL + ", '" + Fila["COLINDANCIA"].ToString() + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                            Mi_SQL = Mi_SQL + ", GETDATE())";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            No_Registro = No_Registro + 1;
                        }
                    }

                    if (Parametros.P_No_Escritura != null && Parametros.P_No_Escritura.Trim().Length > 0) {
                        Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico, Ope_Pat_B_Inm_Juridico.Campo_No_Registro, 20));
                        Mi_SQL = "INSERT INTO " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico;
                        Mi_SQL = Mi_SQL + " (" + Ope_Pat_B_Inm_Juridico.Campo_No_Registro;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Bien_Inmueble_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Escritura;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Fecha_Escritura;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_No_Notario;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Nombre_Notario;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Constancia_Registral;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Folio_Real;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Libertad_Gravament;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Antecedente_Registral;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Proveedor;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Observaciones;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_No_Contrato;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_No_Consecion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Movimiento;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Fecha_Creo;
                        Mi_SQL = Mi_SQL + ") VALUES ('" + No_Registro + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'"; 
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Escritura + "'";
                        Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Escritura) + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Notario + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Notario_Nombre + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Constancia_Registral + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Folio_Real + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Libre_Gravament + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Antecedente + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Proveedor + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Observaciones + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Contrato_Juridico + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Consecion + "'";
                        Mi_SQL = Mi_SQL + ", 'ALTA'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    if (Parametros.P_Archivo != null && Parametros.P_Archivo.Trim().Length > 0) {
                        Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_B_Inm_Archivos.Tabla_Ope_Pat_B_Inm_Archivos, Ope_Pat_B_Inm_Archivos.Campo_No_Registro, 20));
                        Mi_SQL = "INSERT INTO " + Ope_Pat_B_Inm_Archivos.Tabla_Ope_Pat_B_Inm_Archivos;
                        Mi_SQL = Mi_SQL + " (" + Ope_Pat_B_Inm_Archivos.Campo_No_Registro;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Bien_Inmueble_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Tipo_Archivo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Descripcion_Archivo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Ruta_Archivo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Usuario_Cargo_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Estatus;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Fecha_Cargo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Fecha_Creo;
                        Mi_SQL = Mi_SQL + ") VALUES ('" + No_Registro + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'"; 
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo_Anexo + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Descripcion_Anexo + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Archivo + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_ID + "'";
                        Mi_SQL = Mi_SQL + ", 'ACTIVO'";
                        Mi_SQL = Mi_SQL + ", GETDATE()";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    if (!String.IsNullOrEmpty(Parametros.P_Expropiacion)) {
                        Int32 No_Expropiacion = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_B_Inm_Expropiaciones.Tabla_Ope_Pat_B_Inm_Expropiaciones, Ope_Pat_B_Inm_Expropiaciones.Campo_No_Expropiacion, 20));
                        Mi_SQL = "INSERT INTO " + Ope_Pat_B_Inm_Expropiaciones.Tabla_Ope_Pat_B_Inm_Expropiaciones;
                        Mi_SQL = Mi_SQL + " (" + Ope_Pat_B_Inm_Expropiaciones.Campo_No_Expropiacion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Bien_Inmueble_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Fecha_Expropiacion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Usuario_Expropiacion_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Descripcion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Fecha_Creo;
                        Mi_SQL = Mi_SQL + ") VALUES ('" + No_Expropiacion + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE()";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_ID + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Expropiacion + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    if (Parametros.P_Agregar_Afectacion) {
                        Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_B_Inm_Afectaciones.Tabla_Ope_Pat_B_Inm_Afectaciones, Ope_Pat_B_Inm_Afectaciones.Campo_No_Registro, 20));
                        Mi_SQL = "INSERT INTO " + Ope_Pat_B_Inm_Afectaciones.Tabla_Ope_Pat_B_Inm_Afectaciones;
                        Mi_SQL = Mi_SQL + " (" + Ope_Pat_B_Inm_Afectaciones.Campo_No_Registro;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Bien_Inmueble_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Fecha_Afectacion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Fecha_Registro;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Usuario_Registro_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Propietario;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Session_Ayuntamiento;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Tramo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_No_Contrato;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Fecha_Creo;
                        Mi_SQL = Mi_SQL + ") VALUES ('" + No_Registro + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'";
                        Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Afectacion) + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE()";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_ID + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Nuevo_Propietario + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Session_Ayuntamiento + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tramo + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Contrato + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    if (Parametros.Obtener_Dt_Caracteristicas() != null)
                    {
                        if (Parametros.Obtener_Dt_Caracteristicas().Rows.Count > 0)
                        {
                            Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_Bienes_Inmuebles_Caracteristicas.Tabla_Ope_Pat_Bienes_Inmuebles_Caracteristicas, Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_No_Registro, 15));
                            DataTable Dt_Caracteristicas = Parametros.Obtener_Dt_Caracteristicas();
                            Mi_SQL = "INSERT INTO " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Tabla_Ope_Pat_Bienes_Inmuebles_Caracteristicas;
                            Mi_SQL = Mi_SQL + " (" + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_No_Registro;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Bien_Inmueble_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Estado_Actual;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Uso_Inicial;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Usuarios_Beneficiados;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Anios_Sin_Operar;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Region;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Acuifero;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Cuenca;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Latitud;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Longitud;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Vol_Anual_Consumo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Vol_Anual_Extraccion;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Vol_Anual_Descarga;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Diametro_Columna_Succionadora;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Diametro_Descarga;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Tipo_Bomba;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Accionada_Por_Motor;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Medidor;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Profundidad;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Diametro_Perforacion;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Diametro_Ademe;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Requerido;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Maximo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Usuario_Creo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Fecha_Creo;
                            Mi_SQL = Mi_SQL + ") VALUES ('" + No_Registro + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'";
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Estado_Actual"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Estado_Actual"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Uso_Inicial"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Uso_Inicial"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Usuarios_Beneficiados"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Usuarios_Beneficiados"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Anios_Sin_Operar"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Anios_Sin_Operar"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Region"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Region"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Acuifero"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Acuifero"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Cuenca"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Cuenca"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Latitud"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Latitud"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Longitud"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Longitud"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Vol_Anual_Consumo"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Vol_Anual_Consumo"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Vol_Anual_Extraccion"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Vol_Anual_Extraccion"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Vol_Anual_Descarga"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Vol_Anual_Descarga"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Diametro_Columna_Succionadora"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Diametro_Columna_Succionadora"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Diametro_Descarga"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Diametro_Descarga"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Tipo_Bomba"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Tipo_Bomba"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Accionada_Por_Motor"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Accionada_Por_Motor"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Medidor"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Medidor"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Profundidad"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Profundidad"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Diametro_Perforacion"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Diametro_Perforacion"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Diametro_Ademe"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Diametro_Ademe"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Requerido"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Requerido"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Maximo"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Maximo"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                            Mi_SQL = Mi_SQL + ", GETDATE())";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                        }
                    }

                    Trans.Commit();
                } catch (SqlException Ex) {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152) {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 2627) {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    } else if (Ex.Number == 547) {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 515) {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }
                return Parametros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modifica_Bien_Inmueble
            ///DESCRIPCIÓN          : Actualiza el registro.
            ///PARAMETROS           : Parametros.   Contiene los parametros que se van a dar de
            ///                                     Alta en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 29/Febrero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Cls_Ope_Pat_Bienes_Inmuebles_Negocio Modifica_Bien_Inmueble(Cls_Ope_Pat_Bienes_Inmuebles_Negocio Parametros) {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                String Mi_SQL = "";
                try {
                    String Tipo_Poliza_ID = Consultar_Datos_Poliza("TIPO_POLIZA", "");
                    String Cuenta_Depreciacion = Consultar_Datos_Poliza("CUENTA_DEPRECIACION", Parametros.P_Clase_Activo_ID);
                    String Cuenta_Otros_Gastos = Consultar_Datos_Poliza("CUENTA_OTROS_GASTOS", "");
                    Mi_SQL = "UPDATE " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Pat_Bienes_Inmuebles.Campo_Calle + " = '" + Parametros.P_Calle + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Clave + " = '" + Parametros.P_Clave + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Nombre + " = '" + Parametros.P_Nombre_Comun + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Colonia + " = '" + Parametros.P_Colonia + "'";
                    if(!String.IsNullOrEmpty(Parametros.P_Uso_ID))
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Uso_ID + " = '" + Parametros.P_Uso_ID + "'";
                    if (Parametros.P_Superficie > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Superficie + " = '" + Parametros.P_Superficie + "'";
                    } else {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Superficie + " = NULL";
                    }
                    if (Parametros.P_Construccion_Construida > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Construccion + " = '" + Parametros.P_Construccion_Construida + "'";
                    } else {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Construccion + " = NULL";
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Manzana + " = '" + Parametros.P_Manzana + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Predial + " = '" + Parametros.P_Cuenta_Predial_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Lote + " = '" + Parametros.P_Lote + "'";
                    if (Parametros.P_Ocupacion > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Porcentaje_Ocupacion + " = '" + Parametros.P_Ocupacion + "'";
                    } else {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Porcentaje_Ocupacion + " = NULL";
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Efectos_Fiscales + " = '" + Parametros.P_Efectos_Fiscales + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Sector_ID + " = '" + Parametros.P_Sector_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Distrito_ID + " = '" + Parametros.P_Distrito_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Clasificacion_Zona_ID + " = '" + Parametros.P_Clasificacion_Zona_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Tipo_Predio_ID + " = '" + Parametros.P_Tipo_Predio_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Vias_Acceso + " = '" + Parametros.P_Vias_Acceso + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Estado + " = '" + Parametros.P_Estado + "'";
                    if (Parametros.P_Densidad_Construccion > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Densidad_Construccion + " = '" + Parametros.P_Densidad_Construccion + "'";
                    } else {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Densidad_Construccion + " = NULL";
                    }
                    if (Parametros.P_Valor_Comercial > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Valor_Comercial + " = '" + Parametros.P_Valor_Comercial + "'";
                    } else {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Valor_Comercial + " = NULL";
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Numero_Exterior + " = '" + Parametros.P_No_Exterior + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Numero_Interior + " = '" + Parametros.P_No_Interior + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Destino_ID + " = '" + Parametros.P_Destino_ID + "'";
                    if (!String.IsNullOrEmpty(Parametros.P_Origen_ID))
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Origen_ID + " = '" + Parametros.P_Origen_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Area_ID + " = '" + Parametros.P_Area_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Registro + " = '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Registro) + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Tomo + " = '" + Parametros.P_Tomo + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Hoja + " = '" + Parametros.P_Hoja + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Numero_Acta + " = '" + Parametros.P_Numero_Acta + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Cartilla_Parcelaria + " = '" + Parametros.P_Cartilla_Parcelaria + "'";
                    if (Parametros.P_Superficie_Contable > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Superficie_Contable + " = '" + Parametros.P_Superficie_Contable + "'";
                    } else {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Superficie_Contable + " = NULL";
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Unidad_Superficie + " = '" + Parametros.P_Unidad_Superficie + "'";
                    if (Parametros.P_Estado.Trim().Equals("BAJA")) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Baja + " = '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Baja) + "'";
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Tipo_Bien + " = '" + Parametros.P_Tipo_Bien + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Registro_Propiedad + " = '" + Parametros.P_Registro_Propiedad + "'";
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Alta_Cuenta_Publica).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Alta_Cta_Pub + " = '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Alta_Cuenta_Publica) + "'";
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Clase_Activo_ID + " = '" + Parametros.P_Clase_Activo_ID + "'";
                    if (!String.IsNullOrEmpty(Parametros.P_Cuenta_Contable_ID)) Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Contable_ID + " = '" + Parametros.P_Cuenta_Contable_ID + "'";
                    else Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Contable_ID + " = NULL";
                    if (!String.IsNullOrEmpty(Parametros.P_Cuenta_Gasto_ID)) Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Gasto_ID + " = '" + Parametros.P_Cuenta_Gasto_ID + "'";
                    else Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Gasto_ID + " = NULL";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Nombre_Perito + " = '" + Parametros.P_Nombre_Perito.Trim() + "'";
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Avaluo).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Avaluo + " = '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Avaluo) + "'";
                    if (Parametros.P_Valor_Fiscal > (-1)) Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Valor_Fiscal + " = '" + Parametros.P_Valor_Fiscal + "'";
                    else Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Valor_Fiscal + " = NULL";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Clase_Activo_Terreno_ID + " = '" + Parametros.P_Clase_Activo_Terreno_ID + "'";
                    if (!String.IsNullOrEmpty(Parametros.P_Cuenta_Contable_Terreno_ID)) Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Contable_Terreno + " = '" + Parametros.P_Cuenta_Contable_Terreno_ID + "'";
                    else Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Contable_Terreno + " = NULL";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Nombre_Perito_Terreno + " = '" + Parametros.P_Nombre_Perito_Terreno.Trim() + "'";
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Avaluo_Terreno).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Avaluo_Terreno + " = '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Avaluo_Terreno) + "'";
                    if (Parametros.P_Valor_Fiscal_Terreno > (-1)) Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Valor_Fiscal_Terreno + " = '" + Parametros.P_Valor_Fiscal_Terreno + "'";
                    else Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Valor_Fiscal_Terreno + " = NULL";
 
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Usuario_Modifico + " =  '" + Parametros.P_Usuario_Nombre + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID + " = '" + Parametros.P_Bien_Inmueble_ID + "'";

                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    if (!String.IsNullOrEmpty(Parametros.P_Observaciones)) {
                        String ID_Consecutivo = "";
                        String Str_Mi_SQL = "SELECT MAX(" + Ope_Pat_B_Inm_Observaciones.Campo_No_Observacion + ") FROM " + Ope_Pat_B_Inm_Observaciones.Tabla_Ope_Pat_B_Inm_Observaciones;
                        Cmd.CommandText = Str_Mi_SQL;
                        Object Obj_Temp = Cmd.ExecuteScalar();
                        if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                        {
                            ID_Consecutivo = String.Format("{0:0000000000}", (Convert.ToInt32(Obj_Temp) + 1));
                        }
                        else
                        {
                            ID_Consecutivo = String.Format("{0:0000000000}", 1);
                        }
                        Int32 No_Observacion = Convert.ToInt32(ID_Consecutivo);

                        Mi_SQL = "INSERT INTO " + Ope_Pat_B_Inm_Observaciones.Tabla_Ope_Pat_B_Inm_Observaciones;
                        Mi_SQL = Mi_SQL + " (" + Ope_Pat_B_Inm_Observaciones.Campo_No_Observacion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Bien_Inmueble_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Fecha_Observacion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Usuario_Observacion_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Observacion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Fecha_Creo;
                        Mi_SQL = Mi_SQL + ") VALUES ('" + No_Observacion + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE()";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_ID + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Observaciones + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    Mi_SQL = "DELETE FROM " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas
                           + " WHERE " + Ope_Pat_B_Inm_Medidas.Campo_Bien_Inmueble_ID + " = '" + Parametros.P_Bien_Inmueble_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    if (Parametros.P_Dt_Medidas_Colindancias != null && Parametros.P_Dt_Medidas_Colindancias.Rows.Count > 0) {
                        String ID_Consecutivo = "";
                        String Str_Mi_SQL = "SELECT MAX(" + Ope_Pat_B_Inm_Medidas.Campo_No_Registro + ") FROM " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas;
                        Cmd.CommandText = Str_Mi_SQL;
                        Object Obj_Temp = Cmd.ExecuteScalar();
                        if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                        {
                            ID_Consecutivo = String.Format("{0:0000000000}", (Convert.ToInt32(Obj_Temp) + 1));
                        }
                        else
                        {
                            ID_Consecutivo = String.Format("{0:0000000000}", 1);
                        }
                        //Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas, Ope_Pat_B_Inm_Medidas.Campo_No_Registro, 20));
                        Int32 No_Registro = Convert.ToInt32(ID_Consecutivo);

                        foreach (DataRow Fila in Parametros.P_Dt_Medidas_Colindancias.Rows) {
                            Mi_SQL = "INSERT INTO " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas;
                            Mi_SQL = Mi_SQL + " (" + Ope_Pat_B_Inm_Medidas.Campo_No_Registro;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Campo_Bien_Inmueble_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Campo_Orientacion_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Campo_Medida;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Campo_Colindancia;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Campo_Usuario_Creo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Campo_Fecha_Creo;
                            Mi_SQL = Mi_SQL + ") VALUES ('" + No_Registro + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'";
                            Mi_SQL = Mi_SQL + ", '" + Fila["ORIENTACION_ID"].ToString() + "'";
                            Mi_SQL = Mi_SQL + ", '" + Fila["MEDIDA"].ToString() + "'";
                            Mi_SQL = Mi_SQL + ", '" + Fila["COLINDANCIA"].ToString() + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                            Mi_SQL = Mi_SQL + ", GETDATE())";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            No_Registro = No_Registro + 1;
                        }
                    }

                    String ID_Consecutivo2 = "";
                    String Str_Mi_SQL2 = "SELECT MAX(" + Ope_Pat_B_Inm_Juridico.Campo_No_Registro + ") FROM " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico;
                    Cmd.CommandText = Str_Mi_SQL2;
                    Object Obj_Temp2 = Cmd.ExecuteScalar();
                    if (!(Obj_Temp2 is Nullable) && !Obj_Temp2.ToString().Equals(""))
                    {
                        ID_Consecutivo2 = String.Format("{0:0000000000}", (Convert.ToInt32(Obj_Temp2) + 1));
                    } else {
                        ID_Consecutivo2 = String.Format("{0:0000000000}", 1);
                    }
                    //Int32 No_Registro_Tabla_Juridico = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico, Ope_Pat_B_Inm_Juridico.Campo_No_Registro, 20));
                    Int32 No_Registro_Tabla_Juridico = Convert.ToInt32(ID_Consecutivo2);

                    if (Parametros.P_No_Escritura != null && Parametros.P_No_Escritura.Trim().Length > 0) {
                        if (Parametros.P_No_Registro_Alta_Juridico.Trim().Length == 0) {
                            Mi_SQL = "INSERT INTO " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico;
                            Mi_SQL = Mi_SQL + " (" + Ope_Pat_B_Inm_Juridico.Campo_No_Registro;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Bien_Inmueble_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Escritura;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Fecha_Escritura;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_No_Notario;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Nombre_Notario;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Constancia_Registral;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Folio_Real;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Libertad_Gravament;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Antecedente_Registral;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Proveedor;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Observaciones;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_No_Contrato;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_No_Consecion;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Movimiento;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Usuario_Creo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Fecha_Creo;
                            Mi_SQL = Mi_SQL + ") VALUES ('" + No_Registro_Tabla_Juridico + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Escritura + "'";
                            Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Escritura) + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Notario + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Notario_Nombre + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Constancia_Registral + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Folio_Real + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Libre_Gravament + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Antecedente + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Proveedor + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Observaciones + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Contrato_Juridico + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Consecion + "'";
                            Mi_SQL = Mi_SQL + ", 'ALTA'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                            Mi_SQL = Mi_SQL + ", GETDATE())";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            No_Registro_Tabla_Juridico = No_Registro_Tabla_Juridico + 1;
                        } else {
                            Mi_SQL = "UPDATE " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico;
                            Mi_SQL = Mi_SQL + " SET " + Ope_Pat_B_Inm_Juridico.Campo_Escritura + " = '" + Parametros.P_No_Escritura + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Fecha_Escritura + " = '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Escritura) + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_No_Notario + " = '" + Parametros.P_No_Notario + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Nombre_Notario + " = '" + Parametros.P_Notario_Nombre + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Constancia_Registral + " = '" + Parametros.P_Constancia_Registral + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Folio_Real + " = '" + Parametros.P_Folio_Real + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Libertad_Gravament + " = '" + Parametros.P_Libre_Gravament + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Antecedente_Registral + " = '" + Parametros.P_Antecedente + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Proveedor + " = '" + Parametros.P_Proveedor + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_No_Contrato + " = '" + Parametros.P_No_Contrato_Juridico + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_No_Consecion + " = '" + Parametros.P_No_Consecion + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario_Nombre + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Fecha_Modifico + " = GETDATE()";
                            Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_B_Inm_Juridico.Campo_No_Registro + "= '" + Parametros.P_No_Registro_Alta_Juridico + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                        }
                    }

                    if (Parametros.P_Estado.Trim().Equals("BAJA")) {
                        if (Parametros.P_No_Registro_Baja_Juridico.Trim().Length == 0) {
                            Mi_SQL = "INSERT INTO " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico;
                            Mi_SQL = Mi_SQL + " (" + Ope_Pat_B_Inm_Juridico.Campo_No_Registro;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Bien_Inmueble_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Escritura;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Fecha_Escritura;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_No_Notario;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Nombre_Notario;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Constancia_Registral;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Folio_Real;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Nuevo_Propietario;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_No_Contrato;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Movimiento;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Usuario_Creo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Fecha_Creo;
                            Mi_SQL = Mi_SQL + ") VALUES ('" + No_Registro_Tabla_Juridico + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Escritura_Baja + "'";
                            Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Escritura_Baja) + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Notario_Baja + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Notario_Nombre_Baja + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Constancia_Registral_Baja + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Folio_Real_Baja + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Nuevo_Propietario_Juridico + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Contrato_Baja + "'";
                            Mi_SQL = Mi_SQL + ", 'BAJA'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                            Mi_SQL = Mi_SQL + ", GETDATE())";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            No_Registro_Tabla_Juridico = No_Registro_Tabla_Juridico + 1;
                        } else {
                            Mi_SQL = "UPDATE " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico;
                            Mi_SQL = Mi_SQL + " SET " + Ope_Pat_B_Inm_Juridico.Campo_Escritura + " = '" + Parametros.P_No_Escritura_Baja + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Fecha_Escritura + " = '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Escritura_Baja) + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_No_Notario + " = '" + Parametros.P_No_Notario_Baja + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Nombre_Notario + " = '" + Parametros.P_Notario_Nombre_Baja + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Constancia_Registral + " = '" + Parametros.P_Constancia_Registral_Baja + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Folio_Real + " = '" + Parametros.P_Folio_Real_Baja + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Nuevo_Propietario + " = '" + Parametros.P_Nuevo_Propietario_Juridico + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_No_Contrato + " = '" + Parametros.P_No_Contrato_Baja + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario_Nombre + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Campo_Fecha_Modifico + " = GETDATE()";
                            Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_B_Inm_Juridico.Campo_No_Registro + "= '" + Parametros.P_No_Registro_Baja_Juridico + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                        }
                    }

                    if (Parametros.P_Archivo != null && Parametros.P_Archivo.Trim().Length > 0) {
                        String ID_Consecutivo = "";
                        String Str_Mi_SQL = "SELECT MAX(" + Ope_Pat_B_Inm_Archivos.Campo_No_Registro + ") FROM " + Ope_Pat_B_Inm_Archivos.Tabla_Ope_Pat_B_Inm_Archivos;
                        Cmd.CommandText = Str_Mi_SQL;
                        Object Obj_Temp = Cmd.ExecuteScalar();
                        if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                        {
                            ID_Consecutivo = String.Format("{0:0000000000}", (Convert.ToInt32(Obj_Temp) + 1));
                        }
                        else
                        {
                            ID_Consecutivo = String.Format("{0:0000000000}", 1);
                        }
                        //Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_B_Inm_Archivos.Tabla_Ope_Pat_B_Inm_Archivos, Ope_Pat_B_Inm_Archivos.Campo_No_Registro, 20));
                        Int32 No_Registro = Convert.ToInt32(ID_Consecutivo);

                        Mi_SQL = "INSERT INTO " + Ope_Pat_B_Inm_Archivos.Tabla_Ope_Pat_B_Inm_Archivos;
                        Mi_SQL = Mi_SQL + " (" + Ope_Pat_B_Inm_Archivos.Campo_No_Registro;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Bien_Inmueble_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Tipo_Archivo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Descripcion_Archivo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Ruta_Archivo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Usuario_Cargo_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Estatus;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Fecha_Cargo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Fecha_Creo;
                        Mi_SQL = Mi_SQL + ") VALUES ('" + No_Registro + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'"; 
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo_Anexo + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Descripcion_Anexo + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Archivo + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_ID + "'";
                        Mi_SQL = Mi_SQL + ", 'ACTIVO'";
                        Mi_SQL = Mi_SQL + ", GETDATE()";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    if (Parametros.P_Dt_Anexos_Bajas != null && Parametros.P_Dt_Anexos_Bajas.Rows.Count > 0) {
                        foreach (DataRow Fila in Parametros.P_Dt_Anexos_Bajas.Rows) {
                            Mi_SQL = "UPDATE " + Ope_Pat_B_Inm_Archivos.Tabla_Ope_Pat_B_Inm_Archivos;
                            Mi_SQL = Mi_SQL + " SET " + Ope_Pat_B_Inm_Archivos.Campo_Estatus + " = 'INACTIVO'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario_Nombre + "'";
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Fecha_Modifico + " = GETDATE()";
                            Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_B_Inm_Archivos.Campo_No_Registro + " = '" + Fila["No_Registro"].ToString().Trim() + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                        }
                    }

                    if (!String.IsNullOrEmpty(Parametros.P_Expropiacion)) {
                        String ID_Consecutivo = "";
                        String Str_Mi_SQL = "SELECT MAX(" + Ope_Pat_B_Inm_Expropiaciones.Campo_No_Expropiacion + ") FROM " + Ope_Pat_B_Inm_Expropiaciones.Tabla_Ope_Pat_B_Inm_Expropiaciones;
                        Cmd.CommandText = Str_Mi_SQL;
                        Object Obj_Temp = Cmd.ExecuteScalar();
                        if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                        {
                            ID_Consecutivo = String.Format("{0:0000000000}", (Convert.ToInt32(Obj_Temp) + 1));
                        }
                        else
                        {
                            ID_Consecutivo = String.Format("{0:0000000000}", 1);
                        }
                        //Int32 No_Expropiacion = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_B_Inm_Expropiaciones.Tabla_Ope_Pat_B_Inm_Expropiaciones, Ope_Pat_B_Inm_Expropiaciones.Campo_No_Expropiacion, 20));
                        Int32 No_Expropiacion = Convert.ToInt32(ID_Consecutivo);

                        Mi_SQL = "INSERT INTO " + Ope_Pat_B_Inm_Expropiaciones.Tabla_Ope_Pat_B_Inm_Expropiaciones;
                        Mi_SQL = Mi_SQL + " (" + Ope_Pat_B_Inm_Expropiaciones.Campo_No_Expropiacion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Bien_Inmueble_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Fecha_Expropiacion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Usuario_Expropiacion_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Descripcion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Fecha_Creo;
                        Mi_SQL = Mi_SQL + ") VALUES ('" + No_Expropiacion + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE()";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_ID + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Expropiacion + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    if (Parametros.P_Agregar_Afectacion) {
                        String ID_Consecutivo = "";
                        String Str_Mi_SQL = "SELECT MAX(" + Ope_Pat_B_Inm_Afectaciones.Campo_No_Registro + ") FROM " + Ope_Pat_B_Inm_Afectaciones.Tabla_Ope_Pat_B_Inm_Afectaciones;
                        Cmd.CommandText = Str_Mi_SQL;
                        Object Obj_Temp = Cmd.ExecuteScalar();
                        if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                        {
                            ID_Consecutivo = String.Format("{0:0000000000}", (Convert.ToInt32(Obj_Temp) + 1));
                        }
                        else
                        {
                            ID_Consecutivo = String.Format("{0:0000000000}", 1);
                        }
                        //Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_B_Inm_Afectaciones.Tabla_Ope_Pat_B_Inm_Afectaciones, Ope_Pat_B_Inm_Afectaciones.Campo_No_Registro, 20));
                        Int32 No_Registro = Convert.ToInt32(ID_Consecutivo);

                        Mi_SQL = "INSERT INTO " + Ope_Pat_B_Inm_Afectaciones.Tabla_Ope_Pat_B_Inm_Afectaciones;
                        Mi_SQL = Mi_SQL + " (" + Ope_Pat_B_Inm_Afectaciones.Campo_No_Registro;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Bien_Inmueble_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Fecha_Afectacion;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Fecha_Registro;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Usuario_Registro_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Propietario;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Session_Ayuntamiento;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Tramo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_No_Contrato;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Fecha_Creo;
                        Mi_SQL = Mi_SQL + ") VALUES ('" + No_Registro + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'";
                        Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Afectacion) + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE()";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_ID + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Nuevo_Propietario + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Session_Ayuntamiento + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tramo + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Contrato + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }


                    if (Parametros.Obtener_Dt_Caracteristicas() != null)
                    {
                        if (Parametros.Obtener_Dt_Caracteristicas().Rows.Count > 0)
                        {
                            Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Pat_Bienes_Inmuebles_Caracteristicas.Tabla_Ope_Pat_Bienes_Inmuebles_Caracteristicas, Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_No_Registro, 15));
                            Mi_SQL = "DELETE FROM " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Tabla_Ope_Pat_Bienes_Inmuebles_Caracteristicas
                                    + " WHERE " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Bien_Inmueble_ID + " = '" + Parametros.P_Bien_Inmueble_ID + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            DataTable Dt_Caracteristicas = Parametros.Obtener_Dt_Caracteristicas();
                            Mi_SQL = "INSERT INTO " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Tabla_Ope_Pat_Bienes_Inmuebles_Caracteristicas;
                            Mi_SQL = Mi_SQL + " (" + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_No_Registro;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Bien_Inmueble_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Estado_Actual;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Uso_Inicial;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Usuarios_Beneficiados;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Anios_Sin_Operar;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Region;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Acuifero;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Cuenca;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Latitud;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Longitud;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Vol_Anual_Consumo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Vol_Anual_Extraccion;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Vol_Anual_Descarga;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Diametro_Columna_Succionadora;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Diametro_Descarga;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Tipo_Bomba;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Accionada_Por_Motor;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Medidor;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Profundidad;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Diametro_Perforacion;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Diametro_Ademe;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Requerido;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Maximo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Usuario_Creo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Fecha_Creo;
                            Mi_SQL = Mi_SQL + ") VALUES ('" + No_Registro + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_Inmueble_ID + "'";
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Estado_Actual"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Estado_Actual"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Uso_Inicial"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Uso_Inicial"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Usuarios_Beneficiados"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Usuarios_Beneficiados"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Anios_Sin_Operar"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Anios_Sin_Operar"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Region"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Region"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Acuifero"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Acuifero"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Cuenca"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Cuenca"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Latitud"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Latitud"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Longitud"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Longitud"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Vol_Anual_Consumo"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Vol_Anual_Consumo"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Vol_Anual_Extraccion"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Vol_Anual_Extraccion"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Vol_Anual_Descarga"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Vol_Anual_Descarga"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Diametro_Columna_Succionadora"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Diametro_Columna_Succionadora"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Diametro_Descarga"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Diametro_Descarga"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Tipo_Bomba"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Tipo_Bomba"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Accionada_Por_Motor"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Accionada_Por_Motor"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Medidor"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Medidor"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Profundidad"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Profundidad"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Diametro_Perforacion"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Diametro_Perforacion"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Diametro_Ademe"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Diametro_Ademe"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Requerido"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Requerido"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", " + ((Dt_Caracteristicas.Rows[0]["Maximo"].ToString().Trim().Length > 0) ? ("'" + Dt_Caracteristicas.Rows[0]["Maximo"].ToString().Trim() + "'") : "NULL");
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario_Nombre + "'";
                            Mi_SQL = Mi_SQL + ", GETDATE())";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                        }
                    }

                    ///GENERACIÓN DE POLIZA DE BAJA
                    if (Parametros.P_Estado.Equals("BAJA"))
                    {
                        if (!String.IsNullOrEmpty(Tipo_Poliza_ID) && !String.IsNullOrEmpty(Cuenta_Depreciacion) && !String.IsNullOrEmpty(Parametros.P_Cuenta_Contable_ID) && !String.IsNullOrEmpty(Cuenta_Otros_Gastos))
                        {
                            Double Costo_Depreciado_Al_Momento = Parametros.P_Valor_Fiscal - Parametros.P_Valor_Actual;
                            DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
                            //Se crean las columnas del datatable que contendran los detalles de la poliza
                            if (Dt_Partidas_Polizas.Rows.Count == 0)
                            {
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                                Dt_Partidas_Polizas.Columns.Add("BENEFICIARIO_ID", typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));
                                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
                            }
                            if (Costo_Depreciado_Al_Momento > 0)
                            {
                                //FILA DE POLIZA -> OTROS GASTOS
                                DataRow Fila_Otros_Gastos = Dt_Partidas_Polizas.NewRow();
                                Fila_Otros_Gastos[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                                Fila_Otros_Gastos[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Otros_Gastos;
                                Fila_Otros_Gastos[Ope_Con_Polizas_Detalles.Campo_Concepto] = "OTROS GASTOS";
                                Fila_Otros_Gastos[Ope_Con_Polizas_Detalles.Campo_Debe] = Parametros.P_Valor_Actual;
                                Fila_Otros_Gastos[Ope_Con_Polizas_Detalles.Campo_Haber] = 0.0;
                                Dt_Partidas_Polizas.Rows.Add(Fila_Otros_Gastos);
                                //FILA DE POLIZA -> DEPRECIACIÓN
                                DataRow Fila_Depreciacion = Dt_Partidas_Polizas.NewRow();
                                Fila_Depreciacion[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                                Fila_Depreciacion[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Depreciacion;
                                Fila_Depreciacion[Ope_Con_Polizas_Detalles.Campo_Concepto] = "DEPRECIACIÓN ACUMULADA";
                                Fila_Depreciacion[Ope_Con_Polizas_Detalles.Campo_Debe] = Costo_Depreciado_Al_Momento;
                                Fila_Depreciacion[Ope_Con_Polizas_Detalles.Campo_Haber] = 0.0;
                                Dt_Partidas_Polizas.Rows.Add(Fila_Depreciacion);
                                //FILA DE POLIZA -> CONVEPTO DE BIEN MUEBLE
                                DataRow Fila_Activo = Dt_Partidas_Polizas.NewRow();
                                Fila_Activo[Ope_Con_Polizas_Detalles.Campo_Partida] = 3;
                                Fila_Activo[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Parametros.P_Cuenta_Contable_ID;
                                Fila_Activo[Ope_Con_Polizas_Detalles.Campo_Concepto] = "INMUEBLE";
                                Fila_Activo[Ope_Con_Polizas_Detalles.Campo_Debe] = 0.0;
                                Fila_Activo[Ope_Con_Polizas_Detalles.Campo_Haber] = Parametros.P_Valor_Fiscal;
                                Dt_Partidas_Polizas.Rows.Add(Fila_Activo);
                            }
                            else
                            {
                                //FILA DE POLIZA -> OTROS GASTOS
                                DataRow Fila_Otros_Gastos = Dt_Partidas_Polizas.NewRow();
                                Fila_Otros_Gastos[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
                                Fila_Otros_Gastos[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Otros_Gastos;
                                Fila_Otros_Gastos[Ope_Con_Polizas_Detalles.Campo_Concepto] = "OTROS GASTOS";
                                Fila_Otros_Gastos[Ope_Con_Polizas_Detalles.Campo_Debe] = Parametros.P_Valor_Actual;
                                Fila_Otros_Gastos[Ope_Con_Polizas_Detalles.Campo_Haber] = 0.0;
                                Dt_Partidas_Polizas.Rows.Add(Fila_Otros_Gastos);
                                //FILA DE POLIZA -> CONVEPTO DE BIEN MUEBLE
                                DataRow Fila_Activo = Dt_Partidas_Polizas.NewRow();
                                Fila_Activo[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                                Fila_Activo[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Parametros.P_Cuenta_Contable_ID;
                                Fila_Activo[Ope_Con_Polizas_Detalles.Campo_Concepto] = "INMUEBLE";
                                Fila_Activo[Ope_Con_Polizas_Detalles.Campo_Debe] = 0.0;
                                Fila_Activo[Ope_Con_Polizas_Detalles.Campo_Haber] = Parametros.P_Valor_Fiscal;
                                Dt_Partidas_Polizas.Rows.Add(Fila_Activo);
                            }
                            //GENERACIÓN DE POLIZA PARA DEPRECIACIÓN
                            Cls_Ope_Con_Polizas_Negocio Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexión hacia la capa de negocios para envio de los datos a dar de alta
                            Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = Tipo_Poliza_ID;
                            Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now);
                            Rs_Alta_Ope_Con_Polizas.P_Fecha_Poliza = DateTime.Now;
                            Rs_Alta_Ope_Con_Polizas.P_Concepto = "BAJA DE ACTIVO [BIEN INMUEBLE] -> No. Inventario: " + Convert.ToInt64(Parametros.P_Bien_Inmueble_ID) + ", Producto:" + Parametros.P_Nombre_Comun;
                            Rs_Alta_Ope_Con_Polizas.P_Total_Debe = Parametros.P_Valor_Fiscal;
                            Rs_Alta_Ope_Con_Polizas.P_Total_Haber = Parametros.P_Valor_Fiscal;
                            Rs_Alta_Ope_Con_Polizas.P_No_Partida = Dt_Partidas_Polizas.Rows.Count;
                            Rs_Alta_Ope_Con_Polizas.P_Nombre_Usuario = Parametros.P_Usuario_Nombre;
                            Rs_Alta_Ope_Con_Polizas.P_Dt_Detalles_Polizas = Dt_Partidas_Polizas;
                            Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Creo = Parametros.P_Usuario_ID;
                            Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Autorizo = Parametros.P_Usuario_ID;
                            Rs_Alta_Ope_Con_Polizas.P_Prefijo = "";
                            Rs_Alta_Ope_Con_Polizas.P_Cmmd = Cmd;
                            String[] Datos_PolizaDatos_Poliza = Rs_Alta_Ope_Con_Polizas.Alta_Poliza();
                        }
                        else
                        {
                            throw new Exception("No se puede dar da Baja el Activo debido a que le faltan Datos para hacer la Póliza de Baja [Datos Requeridos: Tipo de Poliza de Baja, Cuenta para la Depreciación, Cuenta Contable de Activo y Cuenta Contable de Gasto]. ");
                        }
                    }

                    Trans.Commit();
                } catch (SqlException Ex) {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152) {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 2627) {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    } else if (Ex.Number == 547) {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 515) {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }
                return Parametros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Bienes_Inmuebles
            ///DESCRIPCIÓN          : Carga un listado de los bienes.
            ///PARAMETROS           : Parametros. Contiene los parametros que se van a Consultar
            ///                                   en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 28/Febrero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consultar_Bienes_Inmuebles(Cls_Ope_Pat_Bienes_Inmuebles_Negocio Parametros) {
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try {
                    Mi_SQL = "SELECT BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID + " AS BIEN_INMUEBLE_ID";
                    Mi_SQL = Mi_SQL + ", BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Clave + " AS CLAVE";
                    Mi_SQL = Mi_SQL + ", BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Nombre + " AS NOMBRE_COMUN";
                    Mi_SQL = Mi_SQL + ", BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Calle + " AS CALLE";
                    Mi_SQL = Mi_SQL + ", BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Calle + " AS VIALIDAD_CALLE";
                    Mi_SQL = Mi_SQL + ", BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Numero_Exterior + " AS NUMERO_EXTERIOR";
                    Mi_SQL = Mi_SQL + ", BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Numero_Interior + " AS NUMERO_INTERIOR";
                    Mi_SQL = Mi_SQL + ", BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Colonia + " AS COLONIA";
                    Mi_SQL = Mi_SQL + ", BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Predial + " AS CUENTA_PREDIAL";
                    Mi_SQL = Mi_SQL + ", BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Manzana + " AS MANZANA";
                    Mi_SQL = Mi_SQL + ", BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Lote + " AS LOTE";
                    Mi_SQL = Mi_SQL + ", USOS_INMUEBLES." + Cat_Pat_Usos_Inmuebles.Campo_Descripcion + " AS USO_INMUEBLE";
                    Mi_SQL = Mi_SQL + ", ISNULL((SELECT T_JURIDICO." + Ope_Pat_B_Inm_Juridico.Campo_Escritura + " FROM " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + " T_JURIDICO WHERE T_JURIDICO." + Ope_Pat_B_Inm_Juridico.Campo_Movimiento + " = 'ALTA' AND T_JURIDICO." + Ope_Pat_B_Inm_Juridico.Campo_Bien_Inmueble_ID + " = BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID + " ), '') AS NO_ESCRITURA";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + " BIENES_INMUEBLES";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Usos_Inmuebles.Tabla_Cat_Pat_Usos_Inmuebles + " USOS_INMUEBLES";
                    Mi_SQL = Mi_SQL + " ON BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Uso_ID + " = USOS_INMUEBLES." + Cat_Pat_Usos_Inmuebles.Campo_Uso_ID;
                    if (Parametros.P_No_Escritura != null && Parametros.P_No_Escritura.Trim().Length > 0) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID + " IN (SELECT T_JURIDICO." + Ope_Pat_B_Inm_Juridico.Campo_Bien_Inmueble_ID + " FROM " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + " T_JURIDICO WHERE T_JURIDICO." + Ope_Pat_B_Inm_Juridico.Campo_Movimiento + " = 'ALTA' AND T_JURIDICO." + Ope_Pat_B_Inm_Juridico.Campo_Escritura + " LIKE '%" + Parametros.P_No_Escritura + "%')";
                    }
                    if (Parametros.P_Calle != null && Parametros.P_Calle.Trim().Length > 0) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Calle + " LIKE '%" + Parametros.P_Calle + "%'";
                    }
                    if (Parametros.P_Colonia != null && Parametros.P_Colonia.Trim().Length > 0) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Colonia + " LIKE '%" + Parametros.P_Colonia + "%'";
                    }
                    if (Parametros.P_Uso_ID != null && Parametros.P_Uso_ID.Trim().Length > 0) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Uso_ID + " = '" + Parametros.P_Uso_ID + "'";
                    }
                    if (Parametros.P_Distrito_ID != null && Parametros.P_Distrito_ID.Trim().Length > 0)
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Distrito_ID + " = '" + Parametros.P_Distrito_ID + "'";
                    }
                    if (Parametros.P_Destino_ID != null && Parametros.P_Destino_ID.Trim().Length > 0) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Destino_ID + " = '" + Parametros.P_Destino_ID + "'";
                    }
                    if (Parametros.P_Bien_Inmueble_ID != null && Parametros.P_Bien_Inmueble_ID.Trim().Length > 0) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID + " IN ('" + Parametros.P_Bien_Inmueble_ID + "')";
                    }
                    if (Parametros.P_Construccion_Desde > (-1)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Superficie + " >= '" + Parametros.P_Construccion_Desde.ToString() + "'";
                    }
                    if (Parametros.P_Construccion_Hasta > (-1)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " BIENES_INMUEBLES." + Ope_Pat_Bienes_Inmuebles.Campo_Superficie + " <= '" + (Parametros.P_Construccion_Hasta + 0.01).ToString() + "'";
                    }
                    Mi_SQL = Mi_SQL + " ORDER BY NOMBRE_COMUN";
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Ds_Datos != null && Ds_Datos.Tables.Count>0) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    throw new Exception("Consultar_::: [" + Ex.Message + "]");
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Bien_Inmueble
            ///DESCRIPCIÓN          : Carga un los Detalles de un Bien.
            ///PARAMETROS           : Parametros. Contiene los parametros que se van a Consultar
            ///                                   en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 28/Febrero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Cls_Ope_Pat_Bienes_Inmuebles_Negocio Consultar_Detalles_Bien_Inmueble(Cls_Ope_Pat_Bienes_Inmuebles_Negocio Parametros) {
                String Mi_SQL = ""; 
                Cls_Ope_Pat_Bienes_Inmuebles_Negocio Bien = new Cls_Ope_Pat_Bienes_Inmuebles_Negocio();
                SqlDataReader Data_Reader;
                try {
                    Mi_SQL = "SELECT * FROM " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + " WHERE " + Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID + " = '" + Parametros.P_Bien_Inmueble_ID.Trim() + "'";
                    Data_Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    while (Data_Reader.Read()) {
                        Bien.P_Bien_Inmueble_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID].ToString() : null;
                        Bien.P_Clave = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Clave].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Clave].ToString() : null;
                        Bien.P_Nombre_Comun = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Nombre].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Nombre].ToString() : null;
                        Bien.P_Calle = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Calle].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Calle].ToString() : null;
                        Bien.P_Colonia = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Colonia].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Colonia].ToString() : null;
                        Bien.P_Uso_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Uso_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Uso_ID].ToString() : null;
                        Bien.P_Superficie = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Superficie].ToString())) ? Convert.ToDouble(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Superficie]) : (-1);
                        Bien.P_Construccion_Construida = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Construccion].ToString())) ? Convert.ToDouble(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Construccion]) : (-1);
                        Bien.P_Manzana = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Manzana].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Manzana].ToString() : null;
                        Bien.P_Cuenta_Predial_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Predial].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Predial].ToString() : null;
                        Bien.P_Lote = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Lote].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Lote].ToString() : null;
                        Bien.P_Ocupacion = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Porcentaje_Ocupacion].ToString())) ? Convert.ToDouble(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Porcentaje_Ocupacion]) : (-1);
                        Bien.P_Efectos_Fiscales = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Efectos_Fiscales].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Efectos_Fiscales].ToString() : null;
                        Bien.P_Sector_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Sector_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Sector_ID].ToString() : null;
                        Bien.P_Clasificacion_Zona_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Clasificacion_Zona_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Clasificacion_Zona_ID].ToString() : null;
                        Bien.P_Tipo_Predio_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Tipo_Predio_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Tipo_Predio_ID].ToString() : null;
                        Bien.P_Vias_Acceso = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Vias_Acceso].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Vias_Acceso].ToString() : null;
                        Bien.P_Estado = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Estado].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Estado].ToString() : null;
                        Bien.P_Densidad_Construccion = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Densidad_Construccion].ToString())) ? Convert.ToDouble(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Densidad_Construccion]) : (-1);
                        Bien.P_Valor_Comercial = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Valor_Comercial].ToString())) ? Convert.ToDouble(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Valor_Comercial]) : (-1);
                        Bien.P_No_Exterior = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Numero_Exterior].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Numero_Exterior].ToString() : null;
                        Bien.P_No_Interior = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Numero_Interior].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Numero_Interior].ToString() : null;
                        Bien.P_Destino_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Destino_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Destino_ID].ToString() : null;
                        Bien.P_Origen_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Origen_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Origen_ID].ToString() : null;
                        Bien.P_Estatus = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Estatus].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Estatus].ToString() : null;
                        Bien.P_Fecha_Registro = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Registro].ToString())) ? Convert.ToDateTime(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Registro]) : new DateTime();
                        Bien.P_Area_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Area_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Area_ID].ToString() : null;
                        Bien.P_Hoja = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Hoja].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Hoja].ToString() : null;
                        Bien.P_Tomo = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Tomo].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Tomo].ToString() : null;
                        Bien.P_Distrito_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Distrito_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Distrito_ID].ToString() : null;
                        Bien.P_Numero_Acta = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Numero_Acta].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Numero_Acta].ToString() : null;
                        Bien.P_Cartilla_Parcelaria = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Cartilla_Parcelaria].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Cartilla_Parcelaria].ToString() : null;
                        Bien.P_Superficie_Contable = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Superficie_Contable].ToString())) ? Convert.ToDouble(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Superficie_Contable]) : -1;
                        Bien.P_Unidad_Superficie = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Unidad_Superficie].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Unidad_Superficie].ToString() : null;
                        Bien.P_Tipo_Bien = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Tipo_Bien].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Tipo_Bien].ToString() : null;
                        Bien.P_Registro_Propiedad = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Registro_Propiedad].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Registro_Propiedad].ToString() : null;                        
                        Bien.P_Fecha_Baja = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Baja].ToString())) ? Convert.ToDateTime(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Baja]) : new DateTime();
                        Bien.P_Fecha_Alta_Cuenta_Publica = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Alta_Cta_Pub].ToString())) ? Convert.ToDateTime(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Alta_Cta_Pub]) : new DateTime();
                        Bien.P_Clase_Activo_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Clase_Activo_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Clase_Activo_ID].ToString() : null;
                        Bien.P_Cuenta_Contable_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Contable_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Contable_ID].ToString() : null;
                        Bien.P_Nombre_Perito = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Nombre_Perito].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Nombre_Perito].ToString() : null;
                        Bien.P_Fecha_Avaluo = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Avaluo].ToString())) ? Convert.ToDateTime(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Avaluo]) : new DateTime();
                        Bien.P_Valor_Fiscal = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Valor_Fiscal].ToString())) ? Convert.ToDouble(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Valor_Fiscal]) : (-1);
                        Bien.P_Clase_Activo_Terreno_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Clase_Activo_Terreno_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Clase_Activo_Terreno_ID].ToString() : null;
                        Bien.P_Cuenta_Contable_Terreno_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Contable_Terreno].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Contable_Terreno].ToString() : null;
                        Bien.P_Nombre_Perito_Terreno = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Nombre_Perito_Terreno].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Nombre_Perito_Terreno].ToString() : null;
                        Bien.P_Fecha_Avaluo_Terreno = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Avaluo_Terreno].ToString())) ? Convert.ToDateTime(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Fecha_Avaluo_Terreno]) : new DateTime();
                        Bien.P_Valor_Fiscal_Terreno = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Valor_Fiscal_Terreno].ToString())) ? Convert.ToDouble(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Valor_Fiscal_Terreno]) : (-1);
                        Bien.P_Valor_Actual = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Valor_Actual].ToString())) ? Convert.ToDouble(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Valor_Actual]) : (-1);
                        Bien.P_Cuenta_Gasto_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Gasto_ID].ToString())) ? Data_Reader[Ope_Pat_Bienes_Inmuebles.Campo_Cuenta_Gasto_ID].ToString() : null;
                    }
                    Data_Reader.Close();
                    if (!String.IsNullOrEmpty(Bien.P_Bien_Inmueble_ID)) {
                        Mi_SQL = "SELECT " + Ope_Pat_B_Inm_Observaciones.Campo_No_Observacion + " AS NO_OBSERVACION";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Fecha_Observacion + " AS FECHA_OBSERVACION";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Usuario_Observacion_ID + " AS USUARIO_OBSERVACION_ID";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Observacion + " AS OBSERVACION";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Usuario_Creo + " AS USUARIO_CREO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Fecha_Creo + " AS FECHA_CREO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Usuario_Modifico + " AS USUARIO_MODIFICO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Observaciones.Campo_Fecha_Modifico + " AS FECHA_MODIFICO";
                        Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_B_Inm_Observaciones.Tabla_Ope_Pat_B_Inm_Observaciones;
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_B_Inm_Observaciones.Campo_Bien_Inmueble_ID + " = '" + Bien.P_Bien_Inmueble_ID + "'";
                        DataSet Ds_Tmp = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Tmp != null && Ds_Tmp.Tables.Count > 0) {
                            Bien.P_Dt_Observaciones = Ds_Tmp.Tables[0];
                        }
                    }

                    if (!String.IsNullOrEmpty(Bien.P_Bien_Inmueble_ID)) {
                        Mi_SQL = "SELECT " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas + "." + Ope_Pat_B_Inm_Medidas.Campo_No_Registro + " AS NO_REGISTRO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas + "." + Ope_Pat_B_Inm_Medidas.Campo_Orientacion_ID + " AS ORIENTACION_ID";
                        Mi_SQL = Mi_SQL + ", " + Cat_Pat_Orientaciones_Inm.Tabla_Cat_Pat_Orientaciones_Inm + "." + Cat_Pat_Orientaciones_Inm.Campo_Descripcion + " AS ORIENTACION";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas + "." + Ope_Pat_B_Inm_Medidas.Campo_Medida + " AS MEDIDA";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas + "." + Ope_Pat_B_Inm_Medidas.Campo_Colindancia + " AS COLINDANCIA";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas + "." + Ope_Pat_B_Inm_Medidas.Campo_Usuario_Creo + " AS USUARIO_CREO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas + "." + Ope_Pat_B_Inm_Medidas.Campo_Fecha_Creo + " AS FECHA_CREO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas + "." + Ope_Pat_B_Inm_Medidas.Campo_Usuario_Modifico + " AS USUARIO_MODIFICO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas + "." + Ope_Pat_B_Inm_Medidas.Campo_Fecha_Modifico + " AS FECHA_MODIFICO";
                        Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas;
                        Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Orientaciones_Inm.Tabla_Cat_Pat_Orientaciones_Inm;
                        Mi_SQL = Mi_SQL + " ON " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas + "." + Ope_Pat_B_Inm_Medidas.Campo_Orientacion_ID;
                        Mi_SQL = Mi_SQL + " = " + Cat_Pat_Orientaciones_Inm.Tabla_Cat_Pat_Orientaciones_Inm + "." + Cat_Pat_Orientaciones_Inm.Campo_Orientacion_ID;
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_B_Inm_Medidas.Tabla_Ope_Pat_B_Inm_Medidas + "." + Ope_Pat_B_Inm_Medidas.Campo_Bien_Inmueble_ID + " = '" + Bien.P_Bien_Inmueble_ID + "'";
                        DataSet Ds_Tmp = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Tmp != null && Ds_Tmp.Tables.Count > 0) {
                            Bien.P_Dt_Medidas_Colindancias = Ds_Tmp.Tables[0];
                        }
                    }

                    if (!String.IsNullOrEmpty(Bien.P_Bien_Inmueble_ID)) {
                        Mi_SQL = "SELECT " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_No_Registro + " AS NO_REGISTRO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Escritura + " AS ESCRITURA";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Fecha_Escritura + " AS FECHA_ESCRITURA";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_No_Notario + " AS NO_NOTARIO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Nombre_Notario + " AS NOMBRE_COMPLETO_NOTARIO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Constancia_Registral + " AS CONSTANCIA_REGISTRAL";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Folio_Real + " AS FOLIO_REAL";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Libertad_Gravament + " AS LIBERTAD_GRAVAMEN";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Antecedente_Registral + " AS ANTECEDENTE_REGISTRAL";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Proveedor + " AS PROVEEDOR";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Observaciones + " AS OBSERVACIONES";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_No_Contrato + " AS NO_CONTRATO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Usuario_Creo + " AS USUARIO_CREO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Fecha_Creo + " AS FECHA_CREO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Usuario_Modifico + " AS USUARIO_MODIFICO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Fecha_Modifico + " AS FECHA_MODIFICO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Movimiento + " AS MOVIMIENTO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_Nuevo_Propietario + " AS NUEVO_PROPIETARIO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." + Ope_Pat_B_Inm_Juridico.Campo_No_Consecion + " AS NO_CONSECION";
                        Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico;
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_B_Inm_Juridico.Tabla_Ope_Pat_B_Inm_Juridico + "." +  Ope_Pat_B_Inm_Juridico.Campo_Bien_Inmueble_ID + " = '" + Bien.P_Bien_Inmueble_ID + "'";
                        DataSet Ds_Tmp = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Tmp != null && Ds_Tmp.Tables.Count > 0) {
                            Bien.P_Dt_Historico_Juridico = Ds_Tmp.Tables[0];
                        }
                    }

                    if (!String.IsNullOrEmpty(Bien.P_Bien_Inmueble_ID)) {
                        Mi_SQL = "SELECT " + Ope_Pat_B_Inm_Archivos.Campo_No_Registro + " AS NO_REGISTRO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Tipo_Archivo + " AS TIPO_ARCHIVO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Descripcion_Archivo + " AS DESCRIPCION_ARCHIVO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Ruta_Archivo + " AS RUTA_ARCHIVO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Usuario_Creo + " AS USUARIO_CREO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Archivos.Campo_Fecha_Cargo + " AS FECHA_CARGO";
                        Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_B_Inm_Archivos.Tabla_Ope_Pat_B_Inm_Archivos;
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_B_Inm_Archivos.Campo_Bien_Inmueble_ID + " = '" + Bien.P_Bien_Inmueble_ID + "'";
                        Mi_SQL = Mi_SQL + " AND " + Ope_Pat_B_Inm_Archivos.Campo_Estatus + " = 'ACTIVO'";
                        DataSet Ds_Tmp = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Tmp != null && Ds_Tmp.Tables.Count > 0) {
                            Bien.P_Dt_Anexos = Ds_Tmp.Tables[0];
                        }
                    }
                    if (!String.IsNullOrEmpty(Bien.P_Bien_Inmueble_ID)) {
                        Mi_SQL = "SELECT " + Ope_Pat_B_Inm_Expropiaciones.Campo_No_Expropiacion + " AS NO_EXPROPIACION";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Fecha_Expropiacion + " AS FECHA_EXPROPIACION";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Usuario_Expropiacion_ID + " AS USUARIO_EXPROPIACION_ID";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Descripcion + " AS DESCRIPCION";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Usuario_Creo + " AS USUARIO_CREO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Fecha_Creo + " AS FECHA_CREO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Usuario_Modifico + " AS USUARIO_MODIFICO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Expropiaciones.Campo_Fecha_Modifico + " AS FECHA_MODIFICO";
                        Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_B_Inm_Expropiaciones.Tabla_Ope_Pat_B_Inm_Expropiaciones;
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_B_Inm_Expropiaciones.Campo_Bien_Inmueble_ID + " = '" + Bien.P_Bien_Inmueble_ID + "'";
                        DataSet Ds_Tmp = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Tmp != null && Ds_Tmp.Tables.Count > 0) {
                            Bien.P_Dt_Expropiaciones = Ds_Tmp.Tables[0];
                        }
                    }
                    if (!String.IsNullOrEmpty(Bien.P_Bien_Inmueble_ID)) {
                        Mi_SQL = "SELECT " + Ope_Pat_B_Inm_Afectaciones.Campo_No_Registro + " AS NO_REGISTRO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Fecha_Afectacion + " AS FECHA_AFECTACION";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Fecha_Registro + " AS FECHA_REGISTRO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Usuario_Registro_ID + " AS USUARIO_REGISTRO_ID";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Propietario + " AS PROPIETARIO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Session_Ayuntamiento + " AS SESSION_AYUNTAMIENTO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Tramo + " AS TRAMO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_No_Contrato + " AS NO_CONTRATO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Usuario_Creo + " AS USUARIO_CREO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Fecha_Creo + " AS FECHA_CREO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Usuario_Modifico + " AS USUARIO_MODIFICO";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_B_Inm_Afectaciones.Campo_Fecha_Modifico + " AS FECHA_MODIFICO";
                        Mi_SQL = Mi_SQL + ", 'GUARDADO' AS ESTATUS";
                        Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_B_Inm_Afectaciones.Tabla_Ope_Pat_B_Inm_Afectaciones;
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Pat_B_Inm_Afectaciones.Campo_Bien_Inmueble_ID + " = '" + Bien.P_Bien_Inmueble_ID + "'";
                        DataSet Ds_Tmp = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Tmp != null && Ds_Tmp.Tables.Count > 0) {
                            Bien.P_Dt_Afectaciones = Ds_Tmp.Tables[0];
                        }
                    }
                    if (!String.IsNullOrEmpty(Bien.P_Bien_Inmueble_ID))
                    {
                        Mi_SQL = "SELECT B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_No_Registro + " AS NO_REGISTRO";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Bien_Inmueble_ID + " AS No_Inventario";
                        Mi_SQL = Mi_SQL + ", B_PRINCIPAL." + Ope_Pat_Bienes_Inmuebles.Campo_Clave + " AS Clave_Bien";
                        Mi_SQL = Mi_SQL + ", B_PRINCIPAL." + Ope_Pat_Bienes_Inmuebles.Campo_Nombre + " AS Nombre_Comun";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Estado_Actual + " AS Estado_Actual";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Uso_Inicial + " AS Uso_Inicial";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Usuarios_Beneficiados + " AS Usuarios_Beneficiados";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Anios_Sin_Operar + " AS Anios_Sin_Operar";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Region + " AS Region";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Acuifero + " AS Acuifero";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Cuenca + " AS Cuenca";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Latitud + " AS Latitud";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Longitud + " AS Longitud";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Vol_Anual_Consumo + " AS Vol_Anual_Consumo";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Vol_Anual_Extraccion + " AS Vol_Anual_Extraccion";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Vol_Anual_Descarga + " AS Vol_Anual_Descarga";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Diametro_Columna_Succionadora + " AS Diametro_Columna_Succionadora";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Diametro_Descarga + " AS Diametro_Descarga";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Tipo_Bomba + " AS Tipo_Bomba";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Accionada_Por_Motor + " AS Accionada_Por_Motor";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Medidor + " AS Medidor";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Profundidad + " AS Profundidad";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Diametro_Perforacion + " AS Diametro_Perforacion";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Diametro_Ademe + " AS Diametro_Ademe";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Requerido + " AS Requerido";
                        Mi_SQL = Mi_SQL + ", B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Maximo + " AS Maximo";
                        Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Tabla_Ope_Pat_Bienes_Inmuebles_Caracteristicas + " B_CARACTERISTICAS";
                        Mi_SQL = Mi_SQL + ", " + Ope_Pat_Bienes_Inmuebles.Tabla_Ope_Pat_Bienes_Inmuebles + " B_PRINCIPAL";
                        Mi_SQL = Mi_SQL + " WHERE B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Bien_Inmueble_ID + " = B_PRINCIPAL." + Ope_Pat_Bienes_Inmuebles.Campo_Bien_Inmueble_ID;
                        Mi_SQL = Mi_SQL + " AND B_CARACTERISTICAS." + Ope_Pat_Bienes_Inmuebles_Caracteristicas.Campo_Bien_Inmueble_ID + " = '" + Bien.P_Bien_Inmueble_ID + "'";
                        DataSet Ds_Tmp = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Tmp != null && Ds_Tmp.Tables.Count > 0)
                        {
                            Bien.P_Dt_Caracteristicas = Ds_Tmp.Tables[0];
                        }
                    }

                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los datos. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Bien;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
            ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
            ///PARAMETROS:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Marzo/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID) {
                String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
                try {
                    String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                    Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals("")) {
                        Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                    }
                } catch (SqlException Ex) {
                    new Exception(Ex.Message);
                }
                return Id;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
            ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
            ///PARAMETROS:     
            ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
            ///             2. Longitud_ID. Longitud que tendra el ID. 
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Marzo/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID) {
                String Retornar = "";
                String Dato = "" + Dato_ID;
                for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++) {
                    Retornar = Retornar + "0";
                }
                Retornar = Retornar + Dato;
                return Retornar;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_Complementos
            ///DESCRIPCIÓN: Se obtienen los complementos.
            ///PARAMETROS:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 29/Febrero/2012 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            public static Cls_Ope_Pat_Bienes_Inmuebles_Negocio Obtener_Complementos(Cls_Ope_Pat_Bienes_Inmuebles_Negocio Parametros) {
                try{
                    Boolean Entro_Where = false;
                    if (Parametros.P_Tipo_Complento.Trim().Equals("COLONIA_DE_CALLE")) {
                        String Mi_SQL = "SELECT CALLES." + Cat_Cor_Calles.Campo_Nombre + " AS CALLE_NOMBRE"
                                      + ", COLONIAS." + Cat_Cor_Colonias.Campo_Colonia_ID + " AS COLONIA_ID"
                                      + ", COLONIAS." + Cat_Cor_Colonias.Campo_Nombre + " AS COLONIA_NOMBRE"
                                      + " FROM " + Cat_Cor_Calles_Colonias.Tabla_Cat_Cor_Calles_Colonias + " CALLE_COLONIA"
                                      + " LEFT OUTER JOIN " + Cat_Cor_Calles.Tabla_Cat_Cor_Calles + " CALLES"
                                      + " ON CALLES." + Cat_Cor_Calles.Campo_Calle_ID + " = CALLE_COLONIA." + Cat_Cor_Calles_Colonias.Campo_Calle_ID
                                      + " LEFT OUTER JOIN " + Cat_Cor_Colonias.Tabla_Cat_Cor_Colonias + " COLONIAS" 
                                      + " ON CALLE_COLONIA." + Cat_Cor_Calles_Colonias.Campo_Colonia_ID + " = COLONIAS." + Cat_Cor_Colonias.Campo_Colonia_ID
                                      + " WHERE CALLES." + Cat_Cor_Calles.Campo_Calle_ID + " = '" + Parametros.P_Calle + "'";
                        SqlDataReader Lector = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        while (Lector.Read()) {
                            Parametros.P_Colonia = Lector["COLONIA_ID"].ToString();
                            Parametros.P_Tipo_Complento = Lector["COLONIA_NOMBRE"].ToString();
                            Parametros.P_Calle = Lector["CALLE_NOMBRE"].ToString();
                        }
                        Lector.Close();
                    } else if (Parametros.P_Tipo_Complento.Trim().Equals("NOTARIOS")) {
                        String Mi_SQL = "SELECT " + Cat_Pre_Notarios.Campo_Notario_ID + " AS NOTARIO_ID";
                        Mi_SQL = Mi_SQL + ", " + Cat_Pre_Notarios.Campo_RFC + " AS RFC";
                        Mi_SQL = Mi_SQL + ", " + Cat_Pre_Notarios.Campo_Numero_Notaria + " AS NO_NOTARIA";
                        Mi_SQL = Mi_SQL + ", (" + Cat_Pre_Notarios.Campo_Apellido_Paterno + "  +' '+" + Cat_Pre_Notarios.Campo_Apellido_Materno + "  +' '+" + Cat_Pre_Notarios.Campo_Nombre + ") AS NOMBRE_COMPLETO";
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Pre_Notarios.Tabla_Cat_Pre_Notarios;
                        if (Parametros.P_No_Notario != null && Parametros.P_No_Notario.Trim().Length > 0) {
                            if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where=true; }
                            Mi_SQL = Mi_SQL + Cat_Pre_Notarios.Campo_Notario_ID + " = '" + Parametros.P_No_Notario + "'";
                        }
                        if (Parametros.P_Notario_Nombre != null && Parametros.P_Notario_Nombre.Trim().Length > 0) {
                            if (Entro_Where) { Mi_SQL = Mi_SQL + " AND ("; } else { Mi_SQL = Mi_SQL + " WHERE ("; Entro_Where=true; }
                            Mi_SQL = Mi_SQL + " (" + Cat_Pre_Notarios.Campo_Apellido_Paterno + "  +' '+" + Cat_Pre_Notarios.Campo_Apellido_Materno + "  +' '+" + Cat_Pre_Notarios.Campo_Nombre + ") LIKE '%" + Parametros.P_Notario_Nombre + "%'";
                            Mi_SQL = Mi_SQL + " OR (" + Cat_Pre_Notarios.Campo_Nombre + "  +' '+" + Cat_Pre_Notarios.Campo_Apellido_Paterno + "  +' '+" + Cat_Pre_Notarios.Campo_Apellido_Materno + ") LIKE '%" + Parametros.P_Notario_Nombre + "%'";
                            Mi_SQL = Mi_SQL + " OR (" + Cat_Pre_Notarios.Campo_Numero_Notaria + ") = '" + Parametros.P_Notario_Nombre + "'";
                            Mi_SQL = Mi_SQL + ")";
                        }
                        Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Pre_Notarios.Campo_Apellido_Paterno + ", " + Cat_Pre_Notarios.Campo_Apellido_Materno + ", " + Cat_Pre_Notarios.Campo_Nombre;
                        DataSet Ds_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Temporal != null && Ds_Temporal.Tables.Count > 0) {
                            Parametros.P_Dt_Historico_Juridico = Ds_Temporal.Tables[0];
                        }
                    }
                } catch (SqlException Ex) {
                    throw new Exception(Ex.Message);
                }
                return Parametros;  
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Datos_Poliza
            ///DESCRIPCIÓN: Consultar_Datos_Poliza
            ///PARÁMETROS: 
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Marzo/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            private static String Consultar_Datos_Poliza(String Dato, String Filtro)
            {
                String Dato_Requerido = String.Empty;
                try
                {
                    DataSet Ds_Datos = null;
                    String Mi_SQL = String.Empty;
                    if (Dato.Trim().Equals("TIPO_POLIZA")) Mi_SQL = "SELECT " + Cat_Con_Parametros.Campo_Tipo_Poliza_Baja_ID + " FROM " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros;
                    else if (Dato.Trim().Equals("CUENTA_DEPRECIACION")) Mi_SQL = "SELECT " + Cat_Pat_Clases_Activo.Campo_Cuenta_Depreciacion_ID + " FROM " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo + " WHERE " + Cat_Pat_Clases_Activo.Campo_Clase_Activo_ID + " IN ('" + Filtro.Trim() + "')";
                    else if (Dato.Trim().Equals("CUENTA_OTROS_GASTOS")) Mi_SQL = "SELECT " + Cat_Con_Parametros.Campo_CTA_Baja_Activo_ID + " FROM " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros;
                    if (!String.IsNullOrEmpty(Mi_SQL))
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Ds_Datos != null)
                        if (Ds_Datos.Tables.Count > 0)
                            if (Ds_Datos.Tables[0].Rows.Count > 0)
                            {
                                if (Dato.Trim().Equals("TIPO_POLIZA"))
                                    Dato_Requerido = Ds_Datos.Tables[0].Rows[0][Cat_Con_Parametros.Campo_Tipo_Poliza_Baja_ID].ToString().Trim();
                                else if (Dato.Trim().Equals("CUENTA_DEPRECIACION"))
                                    Dato_Requerido = Ds_Datos.Tables[0].Rows[0][Cat_Pat_Clases_Activo.Campo_Cuenta_Depreciacion_ID].ToString().Trim();
                                else if (Dato.Trim().Equals("CUENTA_OTROS_GASTOS"))
                                    Dato_Requerido = Ds_Datos.Tables[0].Rows[0][Cat_Con_Parametros.Campo_CTA_Baja_Activo_ID].ToString().Trim();
                            }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
                return Dato_Requerido;
            }

        #endregion

    }

}

