﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Control_Patrimonial_Catalogo_Areas_Donacion.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

namespace JAPAMI.Control_Patrimonial_Parametros.Autocompletados.Datos
{
    public class Cls_Ope_Pat_Autocompletados_Datos
    {
        public Cls_Ope_Pat_Autocompletados_Datos(){}
        public static DataTable Consultar_Cuentas_Contables(String Filtro_Nombre_Clave, String Filtro_Identificador) {
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            try {
                String Mi_SQL = "SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " as cuenta_contable_id, " + Cat_Con_Cuentas_Contables.Campo_Descripcion;
                Mi_SQL = Mi_SQL + " as descripcion, " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " as cuenta_contable, (LTRIM(RTRIM(" + Cat_Con_Cuentas_Contables.Campo_Cuenta + ")) + ' ' + " + Cat_Con_Cuentas_Contables.Campo_Descripcion + ") AS cuenta_descripcion ";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables;
                if (!String.IsNullOrEmpty(Filtro_Nombre_Clave)) {
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '%" + Filtro_Nombre_Clave.Trim() + "%'";
                    Mi_SQL = Mi_SQL + " OR " + Cat_Con_Cuentas_Contables.Campo_Descripcion + " LIKE '%" + Filtro_Nombre_Clave.Trim() + "%'";
                    Mi_SQL = Mi_SQL + " OR (LTRIM(RTRIM(" + Cat_Con_Cuentas_Contables.Campo_Cuenta + ")) + ' ' + " + Cat_Con_Cuentas_Contables.Campo_Descripcion + ") LIKE '%" + Filtro_Nombre_Clave.Trim() + "%'";
                }
                if (!String.IsNullOrEmpty(Filtro_Identificador))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = '" + Filtro_Identificador.Trim() + "'";
                }
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Datos != null) {
                    if (Ds_Datos.Tables.Count > 0) { Dt_Datos = Ds_Datos.Tables[0]; }
                }
            }catch (Exception Ex) {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }
        public static DataTable Consultar_Productos(String Filtro_Nombre_Clave, String Filtro_Identificador, String Filtro_Tipo_Articulo)
        {
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                String Mi_SQL = "SELECT " + Cat_Com_Productos.Campo_Producto_ID + " as producto_id, " + Cat_Com_Productos.Campo_Nombre + " as nombre";
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Productos.Campo_Clave + " + ' ' + " + Cat_Com_Productos.Campo_Nombre + " as clave_nombre";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos;
                if (!String.IsNullOrEmpty(Filtro_Nombre_Clave))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " (" + Cat_Com_Productos.Campo_Nombre + " LIKE '%" + Filtro_Nombre_Clave.Trim() + "%'";
                    Mi_SQL = Mi_SQL + " OR " + Cat_Com_Productos.Campo_Clave + " LIKE '%" + Filtro_Nombre_Clave.Trim() + "%'";
                    Mi_SQL = Mi_SQL + " OR (LTRIM(RTRIM(" + Cat_Com_Productos.Campo_Clave + ")) + ' ' + " + Cat_Com_Productos.Campo_Nombre + ") LIKE '%" + Filtro_Nombre_Clave.Trim() + "%')";
                }
                if (!String.IsNullOrEmpty(Filtro_Identificador))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " (" + Cat_Com_Productos.Campo_Producto_ID + " = '" + Filtro_Identificador.Trim() + "')";
                }
                if (!String.IsNullOrEmpty(Filtro_Identificador))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " (" + Cat_Com_Productos.Campo_Producto_ID + " = '" + Filtro_Identificador.Trim() + "')";
                }
                if (!String.IsNullOrEmpty(Filtro_Tipo_Articulo))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " (" + Cat_Com_Productos.Campo_Tipo + " IN ('" + Filtro_Tipo_Articulo.Trim() + "'))";
                }
                Mi_SQL = Mi_SQL + " ORDER BY clave_nombre";
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Datos != null)
                {
                    if (Ds_Datos.Tables.Count > 0) { Dt_Datos = Ds_Datos.Tables[0]; }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return Dt_Datos;
        }
    }
}
