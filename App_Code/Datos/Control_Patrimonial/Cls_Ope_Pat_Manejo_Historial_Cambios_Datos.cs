﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Control_Patrimonial.Manejo_Historial_Cambios.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Ope_Pat_Manejo_Historial_Cambios_Datos
/// </summary>
/// 

namespace JAPAMI.Control_Patrimonial.Manejo_Historial_Cambios.Datos {
    public class Cls_Ope_Pat_Manejo_Historial_Cambios_Datos {

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_DataTable
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 
            ///                         1.  Parametros.  Contiene los parametros que se van a utilizar para
            ///                                             hacer la consulta de la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 23/Enero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
        public static DataTable Consultar_Cambios_Bien(Cls_Ope_Pat_Manejo_Historial_Cambios_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Registros = null;
            DataTable Dt_Registros = new DataTable();
            try
            {
                if (!String.IsNullOrEmpty(Parametros.P_Tipo_Bien) && !String.IsNullOrEmpty(Parametros.P_Tipo_Bien))
                {
                    Mi_SQL = "SELECT HISTORICO." + Ope_Pat_Historial_Cambios.Campo_No_Registro + " AS NUMERO_REGISTRO";
                    Mi_SQL += ", HISTORICO." + Ope_Pat_Historial_Cambios.Campo_No_Cambio + " AS NUMERO_CAMBIO";
                    Mi_SQL += ", HISTORICO." + Ope_Pat_Historial_Cambios.Campo_Fecha_Hora_Cambio + " AS FECHA_CAMBIO";
                    Mi_SQL += ", (EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + " + ' - ' + EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + " + ' ' + EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + " + ' ' + EMPLEADOS." + Cat_Empleados.Campo_Nombre + ") AS EMPLEADO_CAMBIO";
                    if (Parametros.P_Tipo_Bien.Equals("VEHICULO"))
                    {
                        Mi_SQL += ", BIENES_PRINCIPAL." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS NUMERO_INVENTARIO";
                        Mi_SQL = Mi_SQL + " , ( ISNULL(MARCAS." + Cat_Com_Marcas.Campo_Nombre + ", 'INDISTINTA')";
                        Mi_SQL = Mi_SQL + "  +', '+ ISNULL(BIENES_PRINCIPAL." + Ope_Pat_Vehiculos.Campo_Modelo + ", '-')";
                        Mi_SQL = Mi_SQL + "  +', '+ ISNULL(COLORES." + Cat_Pat_Colores.Campo_Descripcion + ", '-')";
                        Mi_SQL = Mi_SQL + "  +', '+ ISNULL(BIENES_PRINCIPAL." + Ope_Pat_Vehiculos.Campo_Serie_Carroceria + ", 'S/S')";
                        Mi_SQL = Mi_SQL + "  +', '+ ISNULL(TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Descripcion + ", '-')) AS DESCRIPCION";
                    }
                    else if (Parametros.P_Tipo_Bien.Equals("BIEN_ECONOMICO"))
                    {
                        Mi_SQL += ", BIENES_PRINCIPAL." + Ope_Pat_Bienes_Economicos.Campo_Bien_ID + " AS NUMERO_INVENTARIO";
                        Mi_SQL = Mi_SQL + " , ( ISNULL(MARCAS." + Cat_Com_Marcas.Campo_Nombre + ", 'INDISTINTA')";
                        Mi_SQL = Mi_SQL + "  +', '+ ISNULL(BIENES_PRINCIPAL." + Ope_Pat_Bienes_Economicos.Campo_Modelo + ", '-')";
                        Mi_SQL = Mi_SQL + "  +', '+ ISNULL(COLORES." + Cat_Pat_Colores.Campo_Descripcion + ", '-')";
                        Mi_SQL = Mi_SQL + "  +', '+ ISNULL(BIENES_PRINCIPAL." + Ope_Pat_Bienes_Economicos.Campo_Numero_Serie + ", 'S/S')";
                        Mi_SQL = Mi_SQL + "  +', '+ ISNULL(MATERIALES." + Cat_Pat_Materiales.Campo_Descripcion + ", '-')) AS DESCRIPCION";
                    }
                    else
                    {
                        Mi_SQL += ", BIENES_PRINCIPAL." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " AS NUMERO_INVENTARIO";
                        Mi_SQL = Mi_SQL + " , ( (ISNULL(MARCAS." + Cat_Com_Marcas.Campo_Nombre + ", 'INDISTINTA'))";
                        Mi_SQL = Mi_SQL + "  +', '+ (ISNULL(BIENES_PRINCIPAL." + Ope_Pat_Bienes_Muebles.Campo_Modelo + ", '-'))";
                        Mi_SQL = Mi_SQL + "  +', '+ (ISNULL(COLORES." + Cat_Pat_Colores.Campo_Descripcion + ", '-'))";
                        Mi_SQL = Mi_SQL + "  +', '+ (ISNULL(BIENES_PRINCIPAL." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie + ", 'S/S'))";
                        Mi_SQL = Mi_SQL + "  +', '+ (ISNULL(MATERIALES." + Cat_Pat_Materiales.Campo_Descripcion + ", '-'))) AS DESCRIPCION";
                    }
                    Mi_SQL += ", (DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + " + ' - ' + DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + ") AS DEPENDENCIA";
                    Mi_SQL += ", (GRUPOS_DEPENDENCIAS." + Cat_Grupos_Dependencias.Campo_Nombre + ") AS GERENCIA";
                    Mi_SQL += ", HIS_DET." + Ope_Pat_Hist_Cam_Det.Campo_Campo + " AS CAMPO";
                    Mi_SQL += ", HIS_DET." + Ope_Pat_Hist_Cam_Det.Campo_Valor_Anterior + " AS VALOR_ANTERIOR";
                    Mi_SQL += ", HIS_DET." + Ope_Pat_Hist_Cam_Det.Campo_Valor_Nuevo + " AS VALOR_NUEVO";
                    Mi_SQL += " FROM " + Ope_Pat_Historial_Cambios.Tabla_Ope_Pat_Historial_Cambios + " HISTORICO";
                    Mi_SQL += " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS ON HISTORICO." + Ope_Pat_Historial_Cambios.Campo_Empleado_Cambio_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + "";
                    if (Parametros.P_Tipo_Bien.Equals("VEHICULO"))
                    {
                        Mi_SQL += " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " BIENES_PRINCIPAL ON HISTORICO." + Ope_Pat_Historial_Cambios.Campo_Bien_ID + " = BIENES_PRINCIPAL." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON BIENES_PRINCIPAL." + Ope_Pat_Vehiculos.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + " GRUPOS_DEPENDENCIAS ON BIENES_PRINCIPAL." + Ope_Pat_Vehiculos.Campo_Gerencia_ID + " = GRUPOS_DEPENDENCIAS." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + "";
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " MARCAS ON BIENES_PRINCIPAL." + Ope_Pat_Vehiculos.Campo_Marca_ID + " = MARCAS." + Cat_Com_Marcas.Campo_Marca_ID;
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Pat_Colores.Tabla_Cat_Pat_Colores + " COLORES ON BIENES_PRINCIPAL." + Ope_Pat_Vehiculos.Campo_Color_ID + " = COLORES." + Cat_Pat_Colores.Campo_Color_ID;
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Pat_Tipos_Vehiculo.Tabla_Cat_Pat_Tipos_Vehiculo + " TIPOS_VEHICULO ON BIENES_PRINCIPAL." + Ope_Pat_Vehiculos.Campo_Tipo_Vehiculo_ID + " = TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Tipo_Vehiculo_ID;
                    }
                    else if (Parametros.P_Tipo_Bien.Equals("BIEN_ECONOMICO"))
                    {
                        Mi_SQL += " LEFT OUTER JOIN " + Ope_Pat_Bienes_Economicos.Tabla_Ope_Pat_Bienes_Economicos + " BIENES_PRINCIPAL ON HISTORICO." + Ope_Pat_Historial_Cambios.Campo_Bien_ID + " = BIENES_PRINCIPAL." + Ope_Pat_Bienes_Economicos.Campo_Bien_ID + "";
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON BIENES_PRINCIPAL." + Ope_Pat_Bienes_Economicos.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + " GRUPOS_DEPENDENCIAS ON BIENES_PRINCIPAL." + Ope_Pat_Bienes_Economicos.Campo_Gerencia_ID + " = GRUPOS_DEPENDENCIAS." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + "";
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " MARCAS ON BIENES_PRINCIPAL." + Ope_Pat_Bienes_Economicos.Campo_Marca_ID + " = MARCAS." + Cat_Com_Marcas.Campo_Marca_ID;
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Pat_Colores.Tabla_Cat_Pat_Colores + " COLORES ON BIENES_PRINCIPAL." + Ope_Pat_Bienes_Economicos.Campo_Color_ID + " = COLORES." + Cat_Pat_Colores.Campo_Color_ID;
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Pat_Materiales.Tabla_Cat_Pat_Materiales + " MATERIALES ON BIENES_PRINCIPAL." + Ope_Pat_Bienes_Economicos.Campo_Material_ID + " = MATERIALES." + Cat_Pat_Materiales.Campo_Material_ID;
                    }
                    else
                    {
                        Mi_SQL += " LEFT OUTER JOIN " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " BIENES_PRINCIPAL ON HISTORICO." + Ope_Pat_Historial_Cambios.Campo_Bien_ID + " = BIENES_PRINCIPAL." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + "";
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON BIENES_PRINCIPAL." + Ope_Pat_Bienes_Muebles.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + " GRUPOS_DEPENDENCIAS ON BIENES_PRINCIPAL." + Ope_Pat_Bienes_Muebles.Campo_Gerencia_ID + " = GRUPOS_DEPENDENCIAS." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + "";
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " MARCAS ON BIENES_PRINCIPAL." + Ope_Pat_Bienes_Muebles.Campo_Marca_ID + " = MARCAS." + Cat_Com_Marcas.Campo_Marca_ID;
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Pat_Colores.Tabla_Cat_Pat_Colores + " COLORES ON BIENES_PRINCIPAL." + Ope_Pat_Bienes_Muebles.Campo_Color_ID + " = COLORES." + Cat_Pat_Colores.Campo_Color_ID;
                        Mi_SQL += " LEFT OUTER JOIN " + Cat_Pat_Materiales.Tabla_Cat_Pat_Materiales + " MATERIALES ON BIENES_PRINCIPAL." + Ope_Pat_Bienes_Muebles.Campo_Material_ID + " = MATERIALES." + Cat_Pat_Materiales.Campo_Material_ID;
                    }
                    Mi_SQL += " LEFT OUTER JOIN " + Ope_Pat_Hist_Cam_Det.Tabla_Ope_Pat_Hist_Cam_Det + " HIS_DET ON HISTORICO." + Ope_Pat_Historial_Cambios.Campo_No_Registro + " = HIS_DET." + Ope_Pat_Hist_Cam_Det.Campo_No_Registro + "";
                    Mi_SQL += " WHERE HISTORICO." + Ope_Pat_Historial_Cambios.Campo_Tipo_Bien + " = '" + Parametros.P_Tipo_Bien + "'";
                    Mi_SQL += " AND HISTORICO." + Ope_Pat_Historial_Cambios.Campo_Bien_ID + " = '" + Parametros.P_Bien_ID + "'";
                }
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Registros = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Registros != null)
                {
                    Dt_Registros = Ds_Registros.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Registros;
        }
    }
}