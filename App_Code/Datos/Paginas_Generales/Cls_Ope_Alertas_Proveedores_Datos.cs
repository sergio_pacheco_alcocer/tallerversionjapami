﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Alertas_Proveedores.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;


/// <summary>
/// Summary description for Cls_Ope_Alertas_Proveedores_Datos
/// </summary>
/// 
namespace JAPAMI.Alertas_Proveedores.Datos
{
    public class Cls_Ope_Alertas_Proveedores_Datos
    {

        public static DataTable Consultar_Ordenes_Asignadas(Cls_Ope_Alertas_Proveedores_Negocio obj_negocio)
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT " + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra;
            Mi_SQL = Mi_SQL + " " + Ope_Com_Ordenes_Compra.Campo_Lista_Requisiciones;
            Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Ordenes_Compra.Campo_Estatus;
            Mi_SQL = Mi_SQL + " IN ('AUTORIZADA_GERENCIA')";
            Mi_SQL = Mi_SQL + " AND " + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID;
            Mi_SQL = Mi_SQL + "='" + Cls_Sessiones.Datos_Proveedor.Rows[0]["PRODUCTO_ID"].ToString().Trim() + "'";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }
    }
}