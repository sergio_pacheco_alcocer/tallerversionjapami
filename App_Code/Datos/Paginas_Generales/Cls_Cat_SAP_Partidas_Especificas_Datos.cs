﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.SAP_Partidas_Especificas.Negocio;

namespace JAPAMI.SAP_Partidas_Especificas.Datos
{
    public class Cls_Cat_SAP_Partidas_Especificas_Datos
    {
        #region (Metodos Consulta)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Partida_Especifica
        /// DESCRIPCION : Consulta la partida especifica de acuerdo a la cuenta contable
        /// PARAMETROS  : Datos: Indica que registro se desea consultar a la base de datos
        /// CREO        : Salvador L. Rea Ayala
        /// FECHA_CREO  : 4/Octubre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Partida_Especifica(Cls_Cat_SAP_Partidas_Especificas_Negocio Datos)
        {
            String Mi_SQL; //Variable para la consulta para la póliza            
            try
            {
                Mi_SQL = "SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + ", ";
                Mi_SQL += Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", ";
                Mi_SQL += Cat_Sap_Partidas_Especificas.Campo_Nombre;
                Mi_SQL += " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;

                if (!String.IsNullOrEmpty(Datos.P_Cuenta))
                    Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Cuenta + " = '" + Datos.P_Cuenta + "'";

                if (!String.IsNullOrEmpty(Datos.P_Clave))
                    Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Clave + " = '" + Datos.P_Clave + "'";

                if (!String.IsNullOrEmpty(Datos.P_Partida_ID))
                    Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = '" + Datos.P_Partida_ID + "'";
                
                if (!String.IsNullOrEmpty(Datos.P_Partida_Generica_ID))
                    Mi_SQL += " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Datos.P_Partida_Generica_ID + "'";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Partida_Especifica
        /// DESCRIPCION : metodo consulta para llenar combo en SubNivel_Presupuestos
        /// PARAMETROS  : 
        /// CREO        : Luis Daniel Guzmán malagón
        /// FECHA_CREO  : 13/Julio/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable consulta_Nombre_Partida()
        {
            String My_Query = "";

            My_Query += "Select " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", ";
            My_Query += Cat_Sap_Partidas_Especificas.Campo_Nombre + ", ";
            My_Query += Cat_Sap_Partidas_Especificas.Campo_Clave + ", ";
            My_Query += Cat_Sap_Partidas_Especificas.Campo_Descripcion;
            My_Query += " From " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            My_Query += " Where " + Cat_Sap_Partidas_Especificas.Campo_Estatus + " like 'ACTIVO'";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, My_Query).Tables[0];

        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Partida_Especifica_by_ID
        /// DESCRIPCION : metodo consulta para llenar combo en SubNivel_Presupuestos
        /// PARAMETROS  : 
        /// CREO        : Luis Daniel Guzmán malagón
        /// FECHA_CREO  : 14/Julio/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable consulta_Nombre_Partida_By_ID(String Id)
        {
            String My_Query = "";

            My_Query += "Select " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", ";
            My_Query += Cat_Sap_Partidas_Especificas.Campo_Nombre + ", ";
            My_Query += Cat_Sap_Partidas_Especificas.Campo_Clave + ", ";
            My_Query += Cat_Sap_Partidas_Especificas.Campo_Descripcion;
            My_Query += " From " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            My_Query += " Where " + Cat_Sap_Partidas_Especificas.Campo_Estatus + " like 'ACTIVO'";
            My_Query += " and " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " like " + "'" + Id + "'";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, My_Query).Tables[0];

        }
        #endregion
    }
}