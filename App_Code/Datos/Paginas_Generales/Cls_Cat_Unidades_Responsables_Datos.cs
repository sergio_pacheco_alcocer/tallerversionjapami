﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Unidades_Responsables.Negocio;
using System.Text;

namespace JAPAMI.Catalogo_Unidades_Responsables.Datos
{
    public class Cls_Cat_Unidades_Responsables_Datos
    {
        public Cls_Cat_Unidades_Responsables_Datos()
        {
        }

        #region (Metodos)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:   Alta_Unidad_Responsable
        /// DESCRIPCION :           Dar de alta una unidad responsable
        /// PARAMETROS  :           Datos: Variable para la capa de negocios
        /// CREO        :           Noe Mosqueda Valadez
        /// FECHA_CREO  :           18/Junio/2012 14:00
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static void Alta_Unidad_Responsable(Cls_Cat_Unidades_Responsables_Negocio Datos)
        {
            String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            Object aux; //variable auxiliar para las consultas

            if (Conexion_Base.State != ConnectionState.Open)
            {
                Conexion_Base.Open(); //Abre la conexión a la base de datos            
            }
            Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
            Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
            Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos

            try
            {
                //Consulta para el ID de la unidad responsable
                Mi_SQL = "SELECT MAX(Dependencia_ID) FROM Cat_Dependencias";

                //Ejecutar consulta
                Comando_SQL.CommandText = Mi_SQL;
                aux = Comando_SQL.ExecuteScalar();

                //verificar si no es nulo
                if (aux == null || aux == DBNull.Value)
                {
                    Datos.P_Unidad_Responsable_ID = "00001";
                }
                else
                {
                    Datos.P_Unidad_Responsable_ID = String.Format("{0:00000}", Convert.ToInt32(aux) + 1);
                }

                //Asignar consulta
                Mi_SQL = "INSERT INTO CAT_DEPENDENCIAS (DEPENDENCIA_ID, NOMBRE, ESTATUS, COMENTARIOS, USUARIO_CREO, FECHA_CREO, CLAVE, GRUPO_DEPENDENCIA_ID,CLAVE_CONTABILIDAD, "
                    + Cat_Dependencias.Campo_Area_Funcional_ID + ") " +
                    "VALUES('" + Datos.P_Unidad_Responsable_ID + "','" + Datos.P_Nombre + "','" + Datos.P_Estatus + "','" + Datos.P_Comentarios + "','" + Datos.P_Usuario + "'," +
                    "GETDATE(),'" + Datos.P_Clave + "'"; 

                //Verificar si hay grupo dependencia
                if (String.IsNullOrEmpty(Datos.P_Grupo_Dependencia_ID) == true)
                {
                    Mi_SQL += ",NULL)";
                }
                else
                {
                    Mi_SQL += ",'" + Datos.P_Grupo_Dependencia_ID + "'";
                }

                if (String.IsNullOrEmpty(Datos.P_Clave_Contabilidad) == true)
                {
                    Mi_SQL += ",NULL)";
                }
                else
                {
                    Mi_SQL += ",'" + Datos.P_Clave_Contabilidad + "'";
                }

                if (!String.IsNullOrEmpty(Datos.P_Area_Funcional_ID))
                {
                    Mi_SQL += ", '" + Datos.P_Area_Funcional_ID.Trim() + "'";
                }
                else 
                {
                    Mi_SQL += ", NULL";
                }

                Mi_SQL = Mi_SQL + ")";

                //Ejecutar consulta
                Comando_SQL.CommandText = Mi_SQL;
                Comando_SQL.ExecuteNonQuery();

                //Ejecutar la transaccion
                Transaccion_SQL.Commit();
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:   Baja_Unidad_Responsable
        /// DESCRIPCION :           Dar de baja una unidad responsable
        /// PARAMETROS  :           Datos: Variable para la capa de negocios
        /// CREO        :           Noe Mosqueda Valadez
        /// FECHA_CREO  :           18/Junio/2012 14:00
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static void Baja_Unidad_Responsable(Cls_Cat_Unidades_Responsables_Negocio Datos)
        {
            String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            Object aux; //variable auxiliar para las consultas

            if (Conexion_Base.State != ConnectionState.Open)
            {
                Conexion_Base.Open(); //Abre la conexión a la base de datos            
            }
            Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
            Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
            Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos

            try
            {
                //Asignr consulta
                Mi_SQL = "UPDATE Cat_Dependencias SET Estatus = 'INACTIVO' WHERE Dependencia_ID = '" + Datos.P_Unidad_Responsable_ID + "' ";

                //Ejecutar consulta
                Comando_SQL.CommandText = Mi_SQL;
                Comando_SQL.ExecuteNonQuery();

                //Ejecutar transaccion
                Transaccion_SQL.Commit();
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:   Cambio_Unidad_Responsable
        /// DESCRIPCION :           Modificar una unidad responsable
        /// PARAMETROS  :           Datos: Variable para la capa de negocios
        /// CREO        :           Noe Mosqueda Valadez
        /// FECHA_CREO  :           18/Junio/2012 19:00
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static void Cambio_Unidad_Responsable(Cls_Cat_Unidades_Responsables_Negocio Datos)
        {
            String Mi_SQL;                          //Obtiene la cadena de inserción hacía la base de datos
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL;    //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            Object aux; //variable auxiliar para las consultas

            if (Conexion_Base.State != ConnectionState.Open)
            {
                Conexion_Base.Open(); //Abre la conexión a la base de datos            
            }
            Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
            Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
            Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos

            try
            {
                //Asignar consulta
                Mi_SQL = "UPDATE CAT_DEPENDENCIAS SET NOMBRE = '" + Datos.P_Nombre + "', ESTATUS = '" + Datos.P_Estatus + "', COMENTARIOS = '" + Datos.P_Comentarios + "', " +
                    "CLAVE = '" + Datos.P_Clave + "', ";

                //Verificar si hay un grupo dependencia
                if (String.IsNullOrEmpty(Datos.P_Grupo_Dependencia_ID) == true)
                {
                    Mi_SQL += "GRUPO_DEPENDENCIA_ID = NULL,";
                }
                else
                {
                    Mi_SQL += "GRUPO_DEPENDENCIA_ID = '" + Datos.P_Grupo_Dependencia_ID + "', ";
                }
                //Verificar si hay un grupo dependencia
                if (String.IsNullOrEmpty(Datos.P_Grupo_Dependencia_ID) == true)
                {
                    Mi_SQL += "CLAVE_CONTABILIDAD = NULL,";
                }
                else
                {
                    Mi_SQL += "CLAVE_CONTABILIDAD = '" + Datos.P_Clave_Contabilidad + "', ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Area_Funcional_ID))
                {
                    Mi_SQL += Cat_Dependencias.Campo_Area_Funcional_ID + " = '" + Datos.P_Area_Funcional_ID + "', ";
                }
                else 
                {
                    Mi_SQL += Cat_Dependencias.Campo_Area_Funcional_ID + " = NULL, ";
                }

                Mi_SQL += "USUARIO_MODIFICO = '" + Datos.P_Usuario + "', FECHA_MODIFICO = GETDATE() " +
                    "WHERE Dependencia_ID = '" + Datos.P_Unidad_Responsable_ID + "' ";

                //Ejecutar consulta
                Comando_SQL.CommandText = Mi_SQL;
                Comando_SQL.ExecuteNonQuery();

                //Ejecutar la transaccion
                Transaccion_SQL.Commit();
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:   Valida_Unidad_Responsable
        /// DESCRIPCION :           Validar si una unidad responsable ya existe
        /// PARAMETROS  :           Datos: Variable para la capa de negocios
        /// CREO        :           Noe Mosqueda Valadez
        /// FECHA_CREO  :           18/Junio/2012 19:00
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static String Valida_Unidad_Responsable(Cls_Cat_Unidades_Responsables_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //Variable para la consulta
            String Resultado = String.Empty; //variable para el resultado
            DataTable Dt_Consulta = new DataTable(); //tabla para la consulta
            
            try
            {
                //Asiganr consulta
                Mi_SQL = "SELECT ('Clave: ' + Clave + ' Nombre: ' + Nombre) AS Unidad_Responsable FROM Cat_Dependencias " +
                    "WHERE (Nombre COLLATE SQL_Latin1_General_CP850_CI_AI = '" + Datos.P_Nombre + "' " +
                    "OR Clave COLLATE SQL_Latin1_General_CP850_CI_AI = '" + Datos.P_Clave + "') ";
                
                //verificar si existe un ID
                if (String.IsNullOrEmpty(Datos.P_Unidad_Responsable_ID) == false)
                {
                    Mi_SQL += "AND Dependencia_ID <> '" + Datos.P_Unidad_Responsable_ID + "' ";
                }

                //Ejecutar consulta
                Dt_Consulta = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                
                //Verifricar si la consulta arrojo resultados
                if (Dt_Consulta.Rows.Count > 0)
                {
                    //COlocar el primer resultado
                    Resultado = "Ya existe un dato con los datos proporcionados<br />" + Dt_Consulta.Rows[0][0].ToString().Trim();
                }

                //Entregar resultado
                return Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:   Consulta_Unidades_Responsables
        /// DESCRIPCION :           Consultar las unidades responsables
        /// PARAMETROS  :           Datos: Variable para la capa de negocios
        /// CREO        :           Noe Mosqueda Valadez
        /// FECHA_CREO  :           18/Junio/2012 19:00
        /// MODIFICO          : 
        /// FECHA_MODIFICO    : 
        /// CAUSA_MODIFICACION: 
        ///*******************************************************************************
        public static DataTable Consulta_Unidades_Responsables(Cls_Cat_Unidades_Responsables_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //tabla para el resultado

            try
            {
                //Verificar el tipo de la consulta
                if (Datos.P_Tipo_Consulta == "Detalles")
                {
                    Mi_SQL = "SELECT Dependencia_ID, Nombre, Estatus, Clave, Grupo_Dependencia_ID, Comentarios,CLAVE_CONTABILIDAD " +
                         ", " + Cat_Dependencias.Campo_Area_Funcional_ID  + " FROM Cat_Dependencias " +
                        " WHERE Dependencia_ID = '" + Datos.P_Unidad_Responsable_ID + "' ";
                }
                else
                {
                    Mi_SQL = "SELECT Cat_Dependencias.Dependencia_ID, Cat_Dependencias.Clave, Cat_Dependencias.Nombre, Cat_Dependencias.Estatus, " +
                        "(Cat_Grupos_Dependencias.Clave + ' ' + Cat_Grupos_Dependencias.Nombre) AS Grupo_Dependencia " +
                        ", " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID +
                        " FROM Cat_Dependencias " +
                        "LEFT JOIN Cat_Grupos_Dependencias ON Cat_Dependencias.Grupo_Dependencia_ID = Cat_Grupos_Dependencias.Grupo_Dependencia_ID " +
                        "WHERE Cat_Dependencias.Clave LIKE '%" + Datos.P_Busqueda + "%' OR Cat_Dependencias.Nombre LIKE '%" + Datos.P_Busqueda + "%' " +
                        "ORDER BY Cat_Dependencias.Nombre ";
                }
                
                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado 
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION : Consultar_Area_Funcional
        /// DESCRIPCION          : Metodo para obtener las areas funcionales del catalogo
        /// PARAMETROS           :           
        /// CREO                 : Leslie González Vázquez
        /// FECHA_CREO           : 05/Agosto/2013
        ///*******************************************************************************
        internal static DataTable Obtener_Area_Funcional() 
        {
            DataSet Ds_Datos_Consulta = new DataSet();
            DataTable Dt_Datos_Consulta = new DataTable();
            StringBuilder Mi_Sql = new StringBuilder();

            try
            {
                Mi_Sql.Append("SELECT " + Cat_SAP_Area_Funcional.Campo_Clave + " + ' ' + ");
                Mi_Sql.Append(Cat_SAP_Area_Funcional.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                Mi_Sql.Append(Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID );
                Mi_Sql.Append(" FROM " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                Mi_Sql.Append(" WHERE " + Cat_SAP_Area_Funcional.Campo_Estatus + " = 'ACTIVO'");

                Ds_Datos_Consulta = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());

                if (Ds_Datos_Consulta != null)
                {
                    Dt_Datos_Consulta = Ds_Datos_Consulta.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al obtener los datos de las Areas Funcionales. Error[" + Ex.Message + "]");
            }
            
            return Dt_Datos_Consulta;
        }
        #endregion
    }
}