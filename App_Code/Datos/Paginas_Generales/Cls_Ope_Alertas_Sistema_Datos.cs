﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;

/// <summary>
/// Summary description for Cls_Ope_Alertas_Sistema_Datos
/// </summary>

namespace JAPAMI.Alertas.Datos
{
    public class Cls_Ope_Alertas_Sistema_Datos
    {

        public static DataTable Consultar_Requisiciones_Entregadas()
        {
            String Mi_SQL = "SELECT 'ALMACEN' AS Modulo, 'Ya esta en Almacen el producto de la ' +  " + Ope_Com_Requisiciones.Campo_Folio + " as Alerta";
            Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Requisiciones.Campo_Estatus;
            Mi_SQL = Mi_SQL + " IN  ('SURTIDA','SURTIDA_PARCIAL')";
            Mi_SQL = Mi_SQL + " AND (" + Ope_Com_Requisiciones.Campo_Empleado_Generacion_ID + "='" + Cls_Sessiones.Empleado_ID.Trim() + "'";
            Mi_SQL = Mi_SQL + " OR " + Ope_Com_Requisiciones.Campo_Empleado_Construccion_ID + "='" + Cls_Sessiones.Empleado_ID.Trim() + "')";
                
            
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

        }
    }
}