﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Modulos.Negocio;
using System.Text;
using SharpContent.ApplicationBlocks.Data;

/// <summary>
/// Summary description for Cls_Apl_Cat_Modulos_SIAG_Datos
/// </summary>
/// 
namespace JAPAMI.Modulos.Datos
{
    public class Cls_Apl_Cat_Modulos_SIAG_Datos
    {
        public Cls_Apl_Cat_Modulos_SIAG_Datos()
        {
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Alta_Modulo
        /// DESCRIPCION : 1.Consulta el último ID dado de alta para poder ingresar el siguiente
        ///               2. Da de Alta el Modulo en la BD con los datos proporcionados por el usuario
        /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
        /// CREO        : Fernando Gonzalez B.
        /// FECHA_CREO  : 04-Mayo-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Alta_Modulo(Cls_Apl_Cat_Modulos_SIAG_Negocios Datos)
        {
            String Mi_SQL;  //Obtiene la cadena de inserción hacía la base de datos
            SqlCommand Comando_SQL = new SqlCommand();  //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL;     //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion);   //Variable para la conexión para la base de datos        
            Object Modulo_id; //Contiene el Id del nuevo modulo

            if (Conexion_Base.State != ConnectionState.Open)
            {
                Conexion_Base.Open();          
            }
            Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);
            Comando_SQL.Connection = Conexion_Base; 
            Comando_SQL.Transaction = Transaccion_SQL;

            try
            {
                //Consulta el ultimo ID dado de Alta en la tabla De Modulos
                Mi_SQL = "SELECT ISNULL(MAX(" + Apl_Cat_Modulos_Siag.Campo_Modulo_ID + "), '00000') FROM " +
                    Apl_Cat_Modulos_Siag.Tabla_Apl_Cat_Modulos_Siag;

                //Ejecutar consulta
                Comando_SQL.CommandText = Mi_SQL;
                Modulo_id = Comando_SQL.ExecuteScalar();

                //Verificar si no es nulo
                if (!(Modulo_id is Nullable))
                {
                    Datos.P_Modulo_ID = String.Format("{0:00000}", (Convert.ToInt32(Modulo_id) + 1));
                }
                else
                {
                    Datos.P_Modulo_ID = "00001";
                }

                //Inserta en la tabal de Modulos con los datos proporcionados por el usuario
                Mi_SQL = "INSERT INTO " + Apl_Cat_Modulos_Siag.Tabla_Apl_Cat_Modulos_Siag + " (";
                Mi_SQL = Mi_SQL + Apl_Cat_Modulos_Siag.Campo_Modulo_ID + ", ";
                Mi_SQL = Mi_SQL + Apl_Cat_Modulos_Siag.Campo_Nombre + ", ";
                Mi_SQL = Mi_SQL + Apl_Cat_Modulos_Siag.Campo_Usuario_Creo + ", ";
                Mi_SQL = Mi_SQL + Apl_Cat_Modulos_Siag.Campo_Fecha_Creo + ") VALUES ('";
                Mi_SQL = Mi_SQL + Datos.P_Modulo_ID + "','" + Datos.P_Nombre+ "','";
                Mi_SQL = Mi_SQL + Datos.P_Nombre_Usuario + "', CONVERT(VARCHAR(10), GETDATE(), 103))";

                Comando_SQL.CommandText = Mi_SQL;
                Comando_SQL.ExecuteNonQuery();   

                Transaccion_SQL.Commit(); 
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null) Transaccion_SQL.Rollback();
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Modificar_Modulo
        /// DESCRIPCION : 1.Actualiza los datos del Modulo con los datos proporcionados por el usuario
        /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
        /// CREO        : Fernando Gonzalez B.
        /// FECHA_CREO  : 04-Mayo-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Modificar_Modulo(Cls_Apl_Cat_Modulos_SIAG_Negocios Datos)
        {
            String Mi_SQL;  //Obtiene la cadena de inserción hacía la base de datos
            SqlCommand Comando_SQL = new SqlCommand();  //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL;  //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        

            if (Conexion_Base.State != ConnectionState.Open)
            {
                Conexion_Base.Open();           
            }
            Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted); 
            Comando_SQL.Connection = Conexion_Base;                                       
            Comando_SQL.Transaction = Transaccion_SQL;

            try
            {

                //Actualiza los datos de Modulo con los datos proporcionados por el usuario
                Mi_SQL = "UPDATE " + Apl_Cat_Modulos_Siag.Tabla_Apl_Cat_Modulos_Siag + " SET ";
                Mi_SQL = Mi_SQL + Apl_Cat_Modulos_Siag.Campo_Modulo_ID + "='" + Datos.P_Modulo_ID + "',";
                Mi_SQL = Mi_SQL + Apl_Cat_Modulos_Siag.Campo_Nombre + "='" + Datos.P_Nombre + "',";
                Mi_SQL = Mi_SQL + Apl_Cat_Modulos_Siag.Campo_Usuario_Modifico + "='" + Datos.P_Nombre_Usuario + "',";
                Mi_SQL = Mi_SQL + Apl_Cat_Modulos_Siag.Campo_Fecha_Modifico + "= CONVERT(VARCHAR(10), GETDATE(), 103) WHERE ";
                Mi_SQL = Mi_SQL + Apl_Cat_Modulos_Siag.Campo_Modulo_ID + "='" + Datos.P_Modulo_ID + "'";

                Comando_SQL.CommandText = Mi_SQL; 
                Comando_SQL.ExecuteNonQuery(); 

                Transaccion_SQL.Commit();
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null) Transaccion_SQL.Rollback();
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Eliminar_Modulo
        /// DESCRIPCION : 1.Elimina el modulo seleccionado por el usuario
        /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
        /// CREO        : Fernando Gonzalez Bautista
        /// FECHA_CREO  : 04-Mayo-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static void Eliminar_Modulo(Cls_Apl_Cat_Modulos_SIAG_Negocios Datos)
        {
            String Mi_SQL;  //Obtiene la cadena de inserción hacía la base de datos
            SqlCommand Comando_SQL = new SqlCommand();  //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL;  //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        

            if (Conexion_Base.State != ConnectionState.Open)
            {
                Conexion_Base.Open();             
            }
            Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted); 
            Comando_SQL.Connection = Conexion_Base;
            Comando_SQL.Transaction = Transaccion_SQL;

            try
            {

                //Elimina el registro del Modulo seleccionado por el usuario
                Mi_SQL = "DELETE FROM " + Apl_Cat_Modulos_Siag.Tabla_Apl_Cat_Modulos_Siag + " WHERE ";
                Mi_SQL = Mi_SQL + Apl_Cat_Modulos_Siag.Campo_Modulo_ID + "='" + Datos.P_Modulo_ID + "'";

                Comando_SQL.CommandText = Mi_SQL; 
                Comando_SQL.ExecuteNonQuery(); 

                Transaccion_SQL.Commit();
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null) Transaccion_SQL.Rollback();
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Modulo
        /// DESCRIPCION : 1.Consulta los reistros de los modulos en la tabla de Cat_Modulos
        /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
        /// CREO        : Fernando Gonzalez B
        /// FECHA_CREO  : 04-Mayo-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Modulo(Cls_Apl_Cat_Modulos_SIAG_Negocios Datos)
        {
            String Mi_SQL;  //Obtiene la cadena de inserción hacía la base de datos
            SqlCommand Comando_SQL = new SqlCommand(); //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL; //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            DataTable Dt_Consulta_Modulos;
            if (Conexion_Base.State != ConnectionState.Open)
            {
                Conexion_Base.Open();       
            }
            Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted); 
            Comando_SQL.Connection = Conexion_Base; 
            Comando_SQL.Transaction = Transaccion_SQL;

            try
            {

                //Consulta todos los registros de la tabla Cat_Modulos
                Mi_SQL = "SELECT * FROM " + Apl_Cat_Modulos_Siag.Tabla_Apl_Cat_Modulos_Siag;
                //if (Datos.P_Nombre != "")
                if (Datos.P_Modulo_ID != null)
                { Mi_SQL = Mi_SQL + " WHERE " + Apl_Cat_Modulos_Siag.Campo_Modulo_ID+ "='" + Datos.P_Modulo_ID + "'"; }

                Dt_Consulta_Modulos=SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                Transaccion_SQL.Commit();
                return Dt_Consulta_Modulos;
                
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null) Transaccion_SQL.Rollback();
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Modulo_By_Nombre
        /// DESCRIPCION : 1.Consulta los reistros de los modulos en la tabla de Cat_Modulos
        ///                 segun el nombre porporcionado por el usuario
        /// PARAMETROS  : Datos: Contiene los datos que serán insertados en la base de datos
        /// CREO        : Fernando Gonzalez B
        /// FECHA_CREO  : 07-Mayo-2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Modulo_By_Nombre(Cls_Apl_Cat_Modulos_SIAG_Negocios Datos)
        {
            String Mi_SQL;  //Obtiene la cadena de inserción hacía la base de datos
            SqlCommand Comando_SQL = new SqlCommand(); //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL; //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            DataTable Dt_Consulta_Modulos;
            if (Conexion_Base.State != ConnectionState.Open)
            {
                Conexion_Base.Open();
            }
            Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);
            Comando_SQL.Connection = Conexion_Base;
            Comando_SQL.Transaction = Transaccion_SQL;

            try
            {

                //Consulta todos los registros de la tabla Cat_Modulos
                Mi_SQL = "SELECT * FROM " + Apl_Cat_Modulos_Siag.Tabla_Apl_Cat_Modulos_Siag;
                //if (Datos.P_Nombre != "")
                if (Datos.P_Nombre != null)
                { Mi_SQL = Mi_SQL + " WHERE " + Apl_Cat_Modulos_Siag.Campo_Nombre + " like '%" + Datos.P_Nombre + "%'"; }

                Dt_Consulta_Modulos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                Transaccion_SQL.Commit();
                return Dt_Consulta_Modulos;

            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null) Transaccion_SQL.Rollback();
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            }
        }
    }
}