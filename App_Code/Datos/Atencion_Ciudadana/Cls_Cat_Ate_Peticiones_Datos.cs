using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Registro_Peticion.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Seguimiento_Peticiones.Negocios;
namespace JAPAMI.Registro_Peticion.Datos
{
    /****************************************************************************************
         NOMBRE DE LA CLASE: Cls_Cat_Ate_Peticiones_Datos
         DESCRIPCION : Clase que contiene los metodos para las operaciones con la tabla Ope_Ate_Peticiones     
         CREO        : Toledo Rodriguez Jesus S.
         FECHA_CREO  : 24-Agosto-2010
         MODIFICO          :
         FECHA_MODIFICO    :
         CAUSA_MODIFICACION:
        ****************************************************************************************/

    public class Cls_Cat_Ate_Peticiones_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA METODO: Alberto Pantoja Hernandez 
        ///        DESCRIPCIÓN: Alta de registro de Peticion
        ///         PARAMETROS: 1.-Datos, datos de 
        ///                     2.-
        ///               CREO: Alberto Pantoja Hernández
        ///         FECHA_CREO: 25/8/2010
        ///           MODIFICO:
        ///     FECHA_MODIFICO:
        /// CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Alta_Peticion(Cls_Cat_Ate_Peticiones_Negocio Datos)
        {
            //Declaraion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL;
            Object ID_Consecutivo;
            //Inicializacion de variables
            Mi_SQL = String.Empty;
            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;
                Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Ate_Peticiones.Campo_Peticion_ID + "),'0000000000') " +
                         " FROM " + Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones;

                ID_Consecutivo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Convert.IsDBNull(ID_Consecutivo))
                {
                    Datos.P_Peticion_ID = "0000000001";
                    Datos.P_Folio = "PT-0000000001";
                }
                else
                {
                    Datos.P_Peticion_ID = string.Format("{0:0000000000}", Convert.ToInt32(ID_Consecutivo) + 1);
                    Datos.P_Folio = "PT-" + Datos.P_Peticion_ID;
                }
                String Mi_SQL_Aux = "(SELECT " +
                    Cat_Ate_Asuntos.Campo_AreaID + " FROM " +
                    Cat_Ate_Asuntos.Tabla_Cat_Ate_Asuntos + " WHERE " +
                    Cat_Ate_Asuntos.Campo_AsuntoID + " = " +
                    Datos.P_Asunto_ID + ")";
                Datos.P_Area_ID = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL_Aux).Tables[0].Rows[0].ItemArray[0].ToString();
                Mi_SQL = "INSERT INTO " + Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + "(" +
                         Ope_Ate_Peticiones.Campo_Peticion_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Folio + ", " +
                         Ope_Ate_Peticiones.Campo_Origen_De_Registro + ", " +
                         Ope_Ate_Peticiones.Campo_Fecha_Solucion_Probable + ", " +
                         Ope_Ate_Peticiones.Campo_Nombre_Solicitante + ", " +
                         Ope_Ate_Peticiones.Campo_Apellido_Paterno + ", " +
                         Ope_Ate_Peticiones.Campo_Apellido_Materno + ", " +
                         Ope_Ate_Peticiones.Campo_Edad + ", " +
                         Ope_Ate_Peticiones.Campo_Sexo + ", " +

                         Ope_Ate_Peticiones.Campo_Calle_No + ", " +
                         Ope_Ate_Peticiones.Campo_Colonia_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Localidad_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Codigo_Postal + ", " +
                         Ope_Ate_Peticiones.Campo_Telefono + ", " +
                         Ope_Ate_Peticiones.Campo_Email + ", " +
                         Ope_Ate_Peticiones.Campo_Descripcion_Peticion + ", " +
                         Ope_Ate_Peticiones.Campo_Fecha_Peticion + ", " +
                         Ope_Ate_Peticiones.Campo_Dependencia_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Asunto_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Area_ID + ", " +

                         Ope_Ate_Peticiones.Campo_Nivel_Importancia + ", " +
                         Ope_Ate_Peticiones.Campo_Genera_Noticia + ", " +
                         Ope_Ate_Peticiones.Campo_Estatus + ", " +
                         Ope_Ate_Peticiones.Campo_Asignado + ", " +
                         Ope_Ate_Peticiones.Campo_Usuario_Creo + ", " +
                         Ope_Ate_Peticiones.Campo_Fecha_Creo + ")" +

                        " VALUES ('" +
                        Datos.P_Peticion_ID + "', '" +
                        Datos.P_Folio + "', '" +
                        Datos.P_Origen + "', '" +
                        Datos.P_Fecha_Solucion_Probable + "', '" +
                        Datos.P_Nombre + "', '" +
                        Datos.P_Apellido_Paterno + "', '" +
                        Datos.P_Apellido_Materno + "', " +
                        Datos.P_Edad + ", '" +
                        Datos.P_Sexo + "', '" +
                        Datos.P_Calle_No + "', '" +
                        Datos.P_Colonia_ID + "', '" +
                        Datos.P_Localidad + "', '" +
                        Datos.P_Codigo_Postal + "', '" +
                        Datos.P_Telefono + "', '" +
                        Datos.P_Email + "', '" +
                        Datos.P_Peticion + "', " +
                        "GETDATE(), '" +
                        Datos.P_Dependencia_ID + "', '" +
                        Datos.P_Asunto_ID + "', '" +
                    //Mi_SQL_Aux + ", '" +
                        Datos.P_Area_ID + "', '" +
                        Datos.P_Nivel_Importancia + "', '" +
                        Datos.P_Genera_Noticia + "', '" +
                        Datos.P_Estatus + "', '" +
                        Datos.P_Asignado + "', '" +
                        Datos.P_Usuario_Creo + "'," +
                        "CONVERT(VARCHAR(10), GETDATE(), 103))";
                //                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                String Mensaje = "Error: ";                
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "No existe un registro relacionado con esta operacion";
                        break;
                    case "923":
                        Mensaje = "Consulta SQL";
                        break;
                    case "12170":
                        Mensaje = "Conexion con el Servidor";
                        break;
                    default:
                        Mensaje = Ex.Message;
                        break;
                }
                throw new Exception(Mensaje+ "["+Ex.ToString()+"]");
            }
            finally
            {
                Cls_Ope_Ate_Seguimiento_Peticiones_Negocio Obj_Seg_Negocio;
                Obj_Seg_Negocio = new Cls_Ope_Ate_Seguimiento_Peticiones_Negocio();
                Obj_Seg_Negocio.P_Peticion_ID = Datos.P_Peticion_ID;
                Obj_Seg_Negocio.P_Asunto_ID = Datos.P_Asunto_ID;
                Obj_Seg_Negocio.P_Area_ID = Datos.P_Area_ID;
                Obj_Seg_Negocio.P_Dependencia_ID = Datos.P_Dependencia_ID;
                Obj_Seg_Negocio.P_Observaciones = "Inicial";
                Obj_Seg_Negocio.Alta_Seguimiento();
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;

            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA METODO: Modificar_Peticion
        ///        DESCRIPCIÓN: Modifica la peticion
        ///         PARAMETROS: 1.-
        ///                     2.-
        ///               CREO: Alberto Pantoja Hernández
        ///         FECHA_CREO: 1/9/2010
        ///           MODIFICO:
        ///     FECHA_MODIFICO:
        /// CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Modificar_Peticion(Cls_Cat_Ate_Peticiones_Negocio Datos)
        {
            //Declaraion de variables
            String Mi_SQL;

            //Inicializacion de variables
            Mi_SQL = String.Empty;

            try
            {
                //Se forma la cadena de la consulta.
                Mi_SQL = "UPDATE " +
                         Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + " SET " +
                    //Ope_Ate_Peticiones.Campo_Fecha_Solucion_Probable + " = '" + Datos.P_Fecha_Solucion_Probable + "', " +
                         Ope_Ate_Peticiones.Campo_Nombre_Solicitante + " = '" + Datos.P_Nombre + "', " +
                         Ope_Ate_Peticiones.Campo_Apellido_Paterno + " = '" + Datos.P_Apellido_Paterno + "', " +
                         Ope_Ate_Peticiones.Campo_Apellido_Materno + " = '" + Datos.P_Apellido_Materno + "', " +
                         Ope_Ate_Peticiones.Campo_Edad + " = " + Datos.P_Edad + ", " +
                         Ope_Ate_Peticiones.Campo_Sexo + " = '" + Datos.P_Sexo + "', " +

                         Ope_Ate_Peticiones.Campo_Calle_No + " = '" + Datos.P_Calle_No + "', " +
                         Ope_Ate_Peticiones.Campo_Colonia_ID + " = '" + Datos.P_Colonia_ID + "', " +
                         Ope_Ate_Peticiones.Campo_Localidad_ID + " = '" + Datos.P_Localidad + "', " +
                         Ope_Ate_Peticiones.Campo_Codigo_Postal + " = '" + Datos.P_Codigo_Postal + "', " +
                         Ope_Ate_Peticiones.Campo_Telefono + " = '" + Datos.P_Telefono + "', " +
                         Ope_Ate_Peticiones.Campo_Email + " = '" + Datos.P_Email + "', " +
                         Ope_Ate_Peticiones.Campo_Descripcion_Peticion + " = '" + Datos.P_Peticion + "', " +
                         Ope_Ate_Peticiones.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "', " +
                         Ope_Ate_Peticiones.Campo_Asunto_ID + " = '" + Datos.P_Asunto_ID + "', " +
                         Ope_Ate_Peticiones.Campo_Area_ID + " = '" + Datos.P_Area_ID + "', " +

                         Ope_Ate_Peticiones.Campo_Nivel_Importancia + " = '" + Datos.P_Nivel_Importancia + "', " +
                         Ope_Ate_Peticiones.Campo_Genera_Noticia + " = '" + Datos.P_Genera_Noticia + "', " +
                         Ope_Ate_Peticiones.Campo_Asignado + " = '" + Datos.P_Asignado + "', " +
                         Ope_Ate_Peticiones.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Modifico + "', " +
                         Ope_Ate_Peticiones.Campo_Fecha_Modifico + " = CONVERT(VARCHAR(10), GETDATE(), 103) " +
                         " WHERE " + Ope_Ate_Peticiones.Campo_Folio + " = '" + Datos.P_Folio + "'";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            }
            catch (SqlException Ex)
            {
                //Se atrapa la excepcion
                throw new Exception( Ex.ToString() );
                //switch (Ex.Number.ToString())
                //{
                //    case "2291":
                //        throw new Exception("Error: No existe un registro relacionado con esta operacion");
                //        break;
                //    case "923":
                //        throw new Exception("Error: Consulta SQL");
                //        break;
                //    case "12170":
                //        throw new Exception("Error: Conexion con el Servidor");
                //        break;
                //    default:
                //        throw new Exception("Error: " + Ex.Message);
                //        break;
                //}
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA METODO: Alberto Pantoja Hernandez 
        ///        DESCRIPCIÓN: Alta de registro de Peticion
        ///         PARAMETROS: 1.-Datos, datos de 
        ///                     2.-
        ///               CREO: Alberto Pantoja Hernández
        ///         FECHA_CREO: 25/8/2010
        ///           MODIFICO: Jesus Toledo
        ///     FECHA_MODIFICO: 19/10/10
        /// CAUSA_MODIFICACIÓN: Guardar peticon sin datos de dependencia, asumto y area
        ///*******************************************************************************
        public static void Alta_Peticion_Ciudadano(Cls_Cat_Ate_Peticiones_Negocio Datos)
        {
            //Declaraion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL;
            Object ID_Consecutivo;
            //Inicializacion de variables
            Mi_SQL = String.Empty;
            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;
                Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Ate_Peticiones.Campo_Peticion_ID + "),'0000000000') " +
                         " FROM " + Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones;

                ID_Consecutivo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Convert.IsDBNull(ID_Consecutivo))
                {
                    Datos.P_Peticion_ID = "0000000001";
                    Datos.P_Folio = "PT-0000000001";
                }
                else
                {
                    Datos.P_Peticion_ID = string.Format("{0:0000000000}", Convert.ToInt32(ID_Consecutivo) + 1);
                    Datos.P_Folio = "PT-" + Datos.P_Peticion_ID;
                }

                Mi_SQL = "INSERT INTO " + Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + "(" +
                         Ope_Ate_Peticiones.Campo_Peticion_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Folio + ", " +
                         Ope_Ate_Peticiones.Campo_Origen_De_Registro + ", " +
                         Ope_Ate_Peticiones.Campo_Fecha_Solucion_Probable + ", " +
                         Ope_Ate_Peticiones.Campo_Nombre_Solicitante + ", " +
                         Ope_Ate_Peticiones.Campo_Apellido_Paterno + ", " +
                         Ope_Ate_Peticiones.Campo_Apellido_Materno + ", " +
                         Ope_Ate_Peticiones.Campo_Edad + ", " +
                         Ope_Ate_Peticiones.Campo_Sexo + ", " +

                         Ope_Ate_Peticiones.Campo_Calle_No + ", " +
                         Ope_Ate_Peticiones.Campo_Colonia_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Localidad_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Codigo_Postal + ", " +
                         Ope_Ate_Peticiones.Campo_Telefono + ", " +
                         Ope_Ate_Peticiones.Campo_Email + ", " +
                         Ope_Ate_Peticiones.Campo_Descripcion_Peticion + ", " +
                         Ope_Ate_Peticiones.Campo_Fecha_Peticion + ", " +
                         Ope_Ate_Peticiones.Campo_Dependencia_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Asunto_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Area_ID + ", " +

                         Ope_Ate_Peticiones.Campo_Nivel_Importancia + ", " +
                         Ope_Ate_Peticiones.Campo_Genera_Noticia + ", " +
                         Ope_Ate_Peticiones.Campo_Estatus + ", " +
                         Ope_Ate_Peticiones.Campo_Asignado + ", " +
                         Ope_Ate_Peticiones.Campo_Usuario_Creo + ", " +
                         Ope_Ate_Peticiones.Campo_Fecha_Creo + ")" +

                        " VALUES ('" +
                        Datos.P_Peticion_ID + "', '" +
                        Datos.P_Folio + "', '" +
                        Datos.P_Origen + "', CONVERT(VARCHAR(10), GETDATE(), 103),'" +
                        Datos.P_Nombre + "', '" +
                        Datos.P_Apellido_Paterno + "', '" +
                        Datos.P_Apellido_Materno + "', " +
                        Datos.P_Edad + ", '" +
                        Datos.P_Sexo + "', '" +
                        Datos.P_Calle_No + "', '" +
                        Datos.P_Colonia_ID + "', '" +
                        Datos.P_Localidad + "', '" +
                        Datos.P_Codigo_Postal + "', '" +
                        Datos.P_Telefono + "', '" +
                        Datos.P_Email + "', '" +
                        Datos.P_Peticion + "', " +
                        "GETDATE(), '" +
                        Datos.P_Dependencia_ID + "', '" +
                        Datos.P_Asunto_ID + "', '" +
                    //Mi_SQL_Aux + ", '" +
                        Datos.P_Area_ID + "', '" +
                        Datos.P_Nivel_Importancia + "', '" +
                        Datos.P_Genera_Noticia + "', '" +
                        Datos.P_Estatus + "', '" +
                        Datos.P_Asignado + "', '" +
                        Datos.P_Usuario_Creo + "'," +
                        "CONVERT(VARCHAR(10), GETDATE(), 103))";
                //                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                throw new Exception( Ex.ToString() );
                //switch (Ex.Number.ToString())
                //{
                //    case "2291":
                //        throw new Exception("Error: No existe un registro relacionado con esta operacion");
                //        break;
                //    case "923":
                //        throw new Exception("Error: Consulta SQL");
                //        break;
                //    case "12170":
                //        throw new Exception("Error: Conexion con el Servidor");
                //        break;
                //    default:
                //        throw new Exception("Error: " + Ex.Message);
                //        break;
                //}
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA METODO: Modificar_Peticion_Reasignacion
        ///        DESCRIPCIÓN: Modifica la peticion
        ///         PARAMETROS: 1.-
        ///                     2.-
        ///               CREO: Jesus Toledo
        ///         FECHA_CREO: 1/9/2010
        ///           MODIFICO:
        ///     FECHA_MODIFICO:
        /// CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Modificar_Peticion_Reasignacion(Cls_Cat_Ate_Peticiones_Negocio Datos)
        {
            //Declaraion de variables
            //SqlTransaction Obj_Transaccion = null;
            //SqlConnection Obj_Conexion;
            //SqlCommand Obj_Comando;
            String Mi_SQL;

            //Inicializacion de variables
            Mi_SQL = String.Empty;

            try
            {

                //Se forma la cadena de la consulta.
                Mi_SQL = "UPDATE " +
                         Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + " SET " +
                         Ope_Ate_Peticiones.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "', " +
                         Ope_Ate_Peticiones.Campo_Asunto_ID + " = '" + Datos.P_Asunto_ID + "', " +
                         Ope_Ate_Peticiones.Campo_Area_ID + " = '" + Datos.P_Area_ID + "', " +
                         Ope_Ate_Peticiones.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Modifico + "', " +
                         Ope_Ate_Peticiones.Campo_Fecha_Modifico + " = CONVERT(VARCHAR(10), GETDATE(), 103) " +
                         " WHERE " + Ope_Ate_Peticiones.Campo_Folio + " = '" + Datos.P_Folio + "'";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            }
            catch (SqlException Ex)
            {
                //Se atrapa la excepcion
                //String Mensaje = "";
                //switch (Ex.Number.ToString())
                //{
                //    case "2291":
                //        Mensaje = "Error: No existe un registro relacionado con esta operacion";
                //        break;
                //    case "923":
                //        throw new Exception("Error: Consulta SQL");
                //        break;
                //    case "12170":
                //        throw new Exception("Error: Conexion con el Servidor");
                //        break;
                //    default:
                //        throw new Exception("Error: " + Ex.Message);
                //        break;
                //}
                throw new Exception( Ex.ToString());
            }
            finally
            {
                Cls_Ope_Ate_Seguimiento_Peticiones_Negocio Obj_Seg_Negocio;
                Obj_Seg_Negocio = new Cls_Ope_Ate_Seguimiento_Peticiones_Negocio();
                Obj_Seg_Negocio.P_Peticion_ID = Datos.P_Peticion_ID;
                Obj_Seg_Negocio.P_Asunto_ID = Datos.P_Asunto_ID;
                Obj_Seg_Negocio.P_Area_ID = Datos.P_Area_ID;
                Obj_Seg_Negocio.P_Dependencia_ID = Datos.P_Dependencia_ID;
                Obj_Seg_Negocio.P_Observaciones = Datos.P_Descripcion_Cambio;
                Obj_Seg_Negocio.Alta_Seguimiento();
                //Obj_Comando = null;
                //Obj_Conexion = null;
                //Obj_Transaccion = null;
            }


        }

        ///*******************************************************************************
        ///NOMBRE DE LA METODO: Modificar_Peticion_Solucion
        ///        DESCRIPCIÓN: Modifica la peticion
        ///         PARAMETROS: 1.-
        ///                     2.-
        ///               CREO: Jesus Toledo
        ///         FECHA_CREO: 1/9/2010
        ///           MODIFICO:
        ///     FECHA_MODIFICO:
        /// CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Modificar_Peticion_Solucion(Cls_Cat_Ate_Peticiones_Negocio Datos)
        {
            //Declaraion de variables
            String Mi_SQL;

            //Inicializacion de variables
            Mi_SQL = String.Empty;

            try
            {

                //Se forma la cadena de la consulta.
                Mi_SQL = "UPDATE " +
                         Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + " SET " +
                         Ope_Ate_Peticiones.Campo_Descripcion_Solucion + " = '" + Datos.P_Descripcion_Solucion + "', " +
                         Ope_Ate_Peticiones.Campo_Estatus + " = '" + Datos.P_Estatus + "', ";
                if (Datos.P_Fecha_Solucion_Real != null)
                {
                    Mi_SQL += Ope_Ate_Peticiones.Campo_Fecha_Solucion_Real + " = GETDATE(), ";
                }
                else
                {
                    Mi_SQL += Ope_Ate_Peticiones.Campo_Fecha_Solucion_Probable + " = '" + Datos.P_Fecha_Solucion_Probable + "', ";
                    Mi_SQL += Ope_Ate_Peticiones.Campo_Fecha_Solucion_Real + " = NULL" + ", ";

                }

                Mi_SQL += Ope_Ate_Peticiones.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Modifico + "', " +
                Ope_Ate_Peticiones.Campo_Fecha_Modifico + " = CONVERT(VARCHAR(10), GETDATE(), 103) " +
                   " WHERE " + Ope_Ate_Peticiones.Campo_Folio + " = '" + Datos.P_Folio + "'";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            }
            catch (SqlException Ex)
            {
                //Se atrapa la excepcion
                throw new Exception(Ex.ToString());
                //switch (Ex.Number.ToString())
                //{
                //    case "2291":
                //        throw new Exception("Error: No existe un registro relacionado con esta operacion");
                //        break;
                //    case "923":
                //        throw new Exception("Error: Consulta SQL");
                //        break;
                //    case "12170":
                //        throw new Exception("Error: Conexion con el Servidor");
                //        break;
                //    default:
                //        throw new Exception("Error: " + Ex.Message);
                //        break;
                //}
            }
        }

        //    /****************************************************************************************
        //    NOMBRE DE LA FUNCION: Consultar_Peticion
        //    DESCRIPCION : consulta la peticion genarada por el ciudadano                  
        //    PARAMETROS  : Datos: Datos que son enviados de la capa de Negocios para consultar la peticion
        //    CREO        : Toledo Rodriguez Jesus S.
        //    FECHA_CREO  : 25-Agosto-2010
        //    MODIFICO          :
        //    FECHA_MODIFICO    :
        //    CAUSA_MODIFICACION:
        //   ****************************************************************************************/
        public static DataTable Consulta_Peticion(Cls_Cat_Ate_Peticiones_Negocio Datos)
        {
            DataTable Dt_Temporal = null;
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT " +
                         Ope_Ate_Peticiones.Campo_Folio + ", " +
                         Ope_Ate_Peticiones.Campo_Origen_De_Registro + ", " +
                         Ope_Ate_Peticiones.Campo_Fecha_Solucion_Probable + ", " +
                         Ope_Ate_Peticiones.Campo_Nombre_Solicitante + ", " +
                         Ope_Ate_Peticiones.Campo_Apellido_Paterno + ", " +
                         Ope_Ate_Peticiones.Campo_Apellido_Materno + ", " +
                         Ope_Ate_Peticiones.Campo_Edad + ", " +
                         Ope_Ate_Peticiones.Campo_Sexo + ", " +

                         Ope_Ate_Peticiones.Campo_Localidad_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Colonia_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Calle_No + ", " +
                         Ope_Ate_Peticiones.Campo_Codigo_Postal + ", " +
                         Ope_Ate_Peticiones.Campo_Telefono + ", " +
                         Ope_Ate_Peticiones.Campo_Email + ", " +
                         Ope_Ate_Peticiones.Campo_Descripcion_Peticion + ", " +
                         Ope_Ate_Peticiones.Campo_Dependencia_ID + ", " +

                         Ope_Ate_Peticiones.Campo_Asunto_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Nivel_Importancia + ", " +
                         Ope_Ate_Peticiones.Campo_Genera_Noticia +

                    " FROM " + Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones +
                    " WHERE " + Ope_Ate_Peticiones.Campo_Folio + " = '" +
                    Datos.P_Folio + "'";
                Dt_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Temporal;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }

        }

        ///    /****************************************************************************************
        ///    NOMBRE DE LA FUNCION: Consultar_Peticion
        ///    DESCRIPCION : consulta la peticion genarada por el ciudadano                  
        ///    PARAMETROS  : Datos: Datos que son enviados de la capa de Negocios para consultar la peticion
        ///    CREO        : Toledo Rodriguez Jesus S.
        ///    FECHA_CREO  : 25-Agosto-2010
        ///    MODIFICO          :
        ///    FECHA_MODIFICO    :
        ///    CAUSA_MODIFICACION:
        ///   ****************************************************************************************/
        public static DataTable Consulta_Peticion_Respuesta(Cls_Cat_Ate_Peticiones_Negocio Datos)
        {
            DataTable Dt_Temporal = null;
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT " +
                         Ope_Ate_Peticiones.Campo_Folio + ", " +

                         Ope_Ate_Peticiones.Campo_Fecha_Solucion_Probable + ", " +
                         Ope_Ate_Peticiones.Campo_Nombre_Solicitante + ", " +
                         Ope_Ate_Peticiones.Campo_Apellido_Paterno + ", " +
                         Ope_Ate_Peticiones.Campo_Apellido_Materno + ", " +

                         Ope_Ate_Peticiones.Campo_Email + ", " +
                         Ope_Ate_Peticiones.Campo_Descripcion_Peticion + ", " +
                         Ope_Ate_Peticiones.Campo_Descripcion_Solucion + ", " +
                         Ope_Ate_Peticiones.Campo_Dependencia_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Area_ID + ", " +

                         Ope_Ate_Peticiones.Campo_Asunto_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Descripcion_Solucion + ", " +
                         Ope_Ate_Peticiones.Campo_Fecha_Peticion + ", " +
                         Ope_Ate_Peticiones.Campo_Peticion_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Estatus +

                    " FROM " + Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones +
                    " WHERE " + Ope_Ate_Peticiones.Campo_Folio + " = '" +
                    Datos.P_Folio + "'";
                Dt_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Temporal;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA METODO: Modificar_Peticion
        ///        DESCRIPCIÓN: Modifica la peticion
        ///         PARAMETROS: 1.-
        ///                     2.-
        ///               CREO: Alberto Pantoja Hernández
        ///         FECHA_CREO: 1/9/2010
        ///           MODIFICO:
        ///     FECHA_MODIFICO:
        /// CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Alta_Seguimiento_Peticion(Cls_Cat_Ate_Peticiones_Negocio Datos)
        {
            //Declaraion de variables
            String Mi_SQL;

            //Inicializacion de variables
            Mi_SQL = String.Empty;

            try
            {

                //Se forma la cadena de la consulta.
                Mi_SQL = "UPDATE " +
                         Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + " SET " +

                         Ope_Ate_Peticiones.Campo_Nombre_Solicitante + " = '" + Datos.P_Nombre + "', " +
                         Ope_Ate_Peticiones.Campo_Apellido_Paterno + " = '" + Datos.P_Apellido_Paterno + "', " +
                         Ope_Ate_Peticiones.Campo_Apellido_Materno + " = '" + Datos.P_Apellido_Materno + "', " +
                         Ope_Ate_Peticiones.Campo_Edad + " = " + Datos.P_Edad + ", " +
                         Ope_Ate_Peticiones.Campo_Sexo + " = '" + Datos.P_Sexo + "', " +

                         Ope_Ate_Peticiones.Campo_Calle_No + " = '" + Datos.P_Calle_No + "', " +
                         Ope_Ate_Peticiones.Campo_Colonia_ID + " = '" + Datos.P_Colonia_ID + "', " +
                         Ope_Ate_Peticiones.Campo_Localidad_ID + " = '" + Datos.P_Localidad + "', " +
                         Ope_Ate_Peticiones.Campo_Codigo_Postal + " = '" + Datos.P_Codigo_Postal + "', " +
                         Ope_Ate_Peticiones.Campo_Telefono + " = '" + Datos.P_Telefono + "', " +
                         Ope_Ate_Peticiones.Campo_Email + " = '" + Datos.P_Email + "', " +
                         Ope_Ate_Peticiones.Campo_Descripcion_Peticion + " = '" + Datos.P_Peticion + "', " +
                         Ope_Ate_Peticiones.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "', " +
                         Ope_Ate_Peticiones.Campo_Asunto_ID + " = '" + Datos.P_Asunto_ID + "', " +
                         Ope_Ate_Peticiones.Campo_Area_ID + " = '" + Datos.P_Area_ID + "', " +

                         Ope_Ate_Peticiones.Campo_Nivel_Importancia + " = '" + Datos.P_Nivel_Importancia + "', " +
                         Ope_Ate_Peticiones.Campo_Genera_Noticia + " = '" + Datos.P_Genera_Noticia + "', " +
                         Ope_Ate_Peticiones.Campo_Asignado + " = '" + Datos.P_Asignado + "', " +
                         Ope_Ate_Peticiones.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Modifico + "', " +
                         Ope_Ate_Peticiones.Campo_Fecha_Modifico + " = CONVERT(VARCHAR(10), GETDATE(), 103) " +
                         " WHERE " + Ope_Ate_Peticiones.Campo_Folio + " = '" + Datos.P_Folio + "'";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            }
            catch (SqlException Ex)
            {
                //Se atrapa la excepcion
                //switch (Ex.Number.ToString())
                //{
                //    case "2291":
                //        throw new Exception("Error: No existe un registro relacionado con esta operacion");
                //        break;
                //    case "923":
                //        throw new Exception("Error: Consulta SQL");
                //        break;
                //    case "12170":
                //        throw new Exception("Error: Conexion con el Servidor");
                //        break;
                //    default:
                //        throw new Exception("Error: " + Ex.Message);
                //        break;
                //}
                throw new Exception(Ex.ToString());
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA METODO: Modificar_Peticion
        ///        DESCRIPCIÓN: Modifica la peticion
        ///         PARAMETROS: 1.-
        ///                     2.-
        ///               CREO: 
        ///         FECHA_CREO: 
        ///           MODIFICO:
        ///     FECHA_MODIFICO:
        /// CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        ///

        public static DataTable Consulta_Parametros()
        {
            DataTable Dt_Temporal = null;
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT " +
                         Apl_Parametros.Campo_Servidor_Correo + ", " +
                         Apl_Parametros.Campo_Correo_Saliente + ", " +
                         Apl_Parametros.Campo_Password_Correo +

                    " FROM " + Apl_Parametros.Tabla_Apl_Parametros;

                Dt_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Temporal;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }

        }

        /****************************************************************************************
                    NOMBRE DE LA FUNCION: Correo_Jefe
                    DESCRIPCION : Consulta el correo electronico de un jefe de dependencia y un jefe de area                         
                    PARAMETROS  : P_Dependencia_ID, P_Area_ID: Indica el id de la dependencia o area
                    CREO        : Jesus Toledo Rdz
                    FECHA_CREO  : 22 sep 2010
                    MODIFICO          :
                    FECHA_MODIFICO    :
                    CAUSA_MODIFICACION: 
        ****************************************************************************************/
        public static string Correo_Jefe(string P_Dependencia_ID)
        {
            String Mi_SQL;
            try
            {
                Mi_SQL = " SELECT " + Constantes.Cat_Empleados.Tabla_Cat_Empleados + "." + Constantes.Cat_Empleados.Campo_Correo_Electronico;
                Mi_SQL += ", ";
                Mi_SQL += Constantes.Cat_Empleados.Tabla_Cat_Empleados + "." + Constantes.Cat_Empleados.Campo_Dependencia_ID;

                Mi_SQL += " FROM " + Constantes.Cat_Empleados.Tabla_Cat_Empleados;

                Mi_SQL += " WHERE " + Constantes.Cat_Empleados.Tabla_Cat_Empleados + "." + Constantes.Cat_Empleados.Campo_Dependencia_ID + " = '" + P_Dependencia_ID;
                Mi_SQL += "' AND " + Constantes.Cat_Empleados.Tabla_Cat_Empleados + "." + Constantes.Cat_Empleados.Campo_Rol_ID + " IN(SELECT " + Constantes.Apl_Cat_Roles.Tabla_Apl_Cat_Roles + ".";
                Mi_SQL += Constantes.Apl_Cat_Roles.Campo_Rol_ID;
                Mi_SQL += " FROM ";
                Mi_SQL += Constantes.Apl_Cat_Roles.Tabla_Apl_Cat_Roles;
                Mi_SQL += " WHERE " + Constantes.Apl_Cat_Roles.Tabla_Apl_Cat_Roles + "." + Constantes.Apl_Cat_Roles.Campo_Nombre + " LIKE '%Jefe de Dependencia%' OR ";
                Mi_SQL += Constantes.Apl_Cat_Roles.Tabla_Apl_Cat_Roles + "." + Constantes.Apl_Cat_Roles.Campo_Nombre + " LIKE '%jefe de dependencia%' OR ";
                Mi_SQL += Constantes.Apl_Cat_Roles.Tabla_Apl_Cat_Roles + "." + Constantes.Apl_Cat_Roles.Campo_Nombre + " LIKE '%JEFE DE DEPENDENCIA%')";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0][0].ToString();
            }
            catch (SqlException Ex)
            {
                throw new Exception(Ex.ToString());
                //switch (Ex.Number.ToString())
                //{
                //    case "923":
                //        throw new Exception("Error: Consulta SQL");
                //        break;
                //    case "12170":
                //        throw new Exception("Error: Conexion con el Servidor");
                //        break;
                //    default:
                //        throw new Exception("Error: " + Ex.Message);
                //        break;
                //}
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        /****************************************************************************************
        NOMBRE DE LA FUNCION: Correo_Jefe
        DESCRIPCION : consulta el correo del jefe de dependencia               
        PARAMETROS  : P_Dependencia_ID: id de la dependencia a la que pertenece
                      P_Area_ID: id del area a la que pertenece
        CREO        : Toledo Rodriguez Jesus S.
        FECHA_CREO  : 25-Agosto-2010
        MODIFICO          :
        FECHA_MODIFICO    :
        CAUSA_MODIFICACION:
       ****************************************************************************************/
        public static string Correo_Jefe(string P_Dependencia_ID, string P_Area_ID)
        {
            String Mi_SQL;
            try
            {
                Mi_SQL = " SELECT " + Constantes.Cat_Empleados.Tabla_Cat_Empleados + "." + Constantes.Cat_Empleados.Campo_Correo_Electronico;
                Mi_SQL += ", ";
                Mi_SQL += Constantes.Cat_Empleados.Tabla_Cat_Empleados + "." + Constantes.Cat_Empleados.Campo_Dependencia_ID;
                Mi_SQL += ", ";
                Mi_SQL += Constantes.Cat_Empleados.Tabla_Cat_Empleados + "." + Constantes.Cat_Empleados.Campo_Area_ID;

                Mi_SQL += " FROM " + Constantes.Cat_Empleados.Tabla_Cat_Empleados;

                Mi_SQL += " WHERE " + Constantes.Cat_Empleados.Tabla_Cat_Empleados + "." + Constantes.Cat_Empleados.Campo_Dependencia_ID + " = '" + P_Dependencia_ID;
                Mi_SQL += "' AND " + Constantes.Cat_Empleados.Tabla_Cat_Empleados + "." + Constantes.Cat_Empleados.Campo_Area_ID + " = '" + P_Area_ID;
                Mi_SQL += "' AND " + Constantes.Cat_Empleados.Tabla_Cat_Empleados + "." + Constantes.Cat_Empleados.Campo_Rol_ID + " IN(SELECT " + Constantes.Apl_Cat_Roles.Tabla_Apl_Cat_Roles + ".";
                Mi_SQL += Constantes.Apl_Cat_Roles.Campo_Rol_ID;
                Mi_SQL += " FROM ";
                Mi_SQL += Constantes.Apl_Cat_Roles.Tabla_Apl_Cat_Roles;
                Mi_SQL += " WHERE " + Constantes.Apl_Cat_Roles.Tabla_Apl_Cat_Roles + "." + Constantes.Apl_Cat_Roles.Campo_Nombre + " LIKE '%Responsable de Area%' OR ";
                Mi_SQL += Constantes.Apl_Cat_Roles.Tabla_Apl_Cat_Roles + "." + Constantes.Apl_Cat_Roles.Campo_Nombre + " LIKE '%responsable de area%' OR ";
                Mi_SQL += Constantes.Apl_Cat_Roles.Tabla_Apl_Cat_Roles + "." + Constantes.Apl_Cat_Roles.Campo_Nombre + " LIKE '%RESPONSABLE DE AREA%')";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0][0].ToString();
            }
            catch (SqlException Ex)
            {
                throw new Exception(Ex.ToString());
                //switch (Ex.Number.ToString())
                //{
                //    case "923":
                //        throw new Exception("Error: Consulta SQL");
                //        break;
                //    case "12170":
                //        throw new Exception("Error: Conexion con el Servidor");
                //        break;
                //    default:
                //        throw new Exception("Error: " + Ex.Message);
                //        break;
                //}
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        /****************************************************************************************
            NOMBRE DE LA FUNCION: Consultar_Grupo_Rol
            DESCRIPCION : Cosnulta el ID del grupo de roles al que pertenece el rol del usuario
            PARAMETROS  : P_Dependencia_ID, P_Area_ID: Indica el id de la dependencia o area
            CREO        : Jesus Toledo Rdz
            FECHA_CREO  : 22 sep 2010
            MODIFICO          :
            FECHA_MODIFICO    :
            CAUSA_MODIFICACION: 
        ****************************************************************************************/
        public static String Consultar_Grupo_Rol(String Datos)
        {
            String Mi_SQL;
            try
            {
                Mi_SQL = " SELECT " + Constantes.Apl_Cat_Roles.Campo_Grupo_Roles_ID +
                    " FROM " + Constantes.Apl_Cat_Roles.Tabla_Apl_Cat_Roles +
                    " WHERE " + Constantes.Apl_Cat_Roles.Campo_Rol_ID + " = " + Datos;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0][0].ToString();
            }
            catch (SqlException Ex)
            {
                throw new Exception(Ex.ToString());
                //switch (Ex.Number.ToString())
                //{
                //    case "923":
                //        throw new Exception("Error: Consulta SQL");
                //        break;
                //    case "12170":
                //        throw new Exception("Error: Conexion con el Servidor");
                //        break;
                //    default:
                //        throw new Exception("Error: " + Ex.Message);
                //        break;
                //}
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }
        }
        /****************************************************************************************
        NOMBRE DE LA FUNCION: Consultar_Peticion_Bandeja
        DESCRIPCION : consulta la peticion genarada por el ciudadano                  
        PARAMETROS  : Datos: Datos que son enviados de la capa de Negocios para consultar la peticion
        CREO        : Toledo Rodriguez Jesus S.
        FECHA_CREO  : 25-Agosto-2010
        MODIFICO          :
        FECHA_MODIFICO    :
        CAUSA_MODIFICACION:
       ****************************************************************************************/
        public static DataTable Consulta_Peticion_Bandeja(Cls_Cat_Ate_Peticiones_Negocio Datos)
        {
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT ";
                Mi_SQL += Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".*, ";

                Mi_SQL += Constantes.Cat_Ate_Asuntos.Tabla_Cat_Ate_Asuntos + "." + Constantes.Cat_Ate_Asuntos.Campo_Nombre;
                Mi_SQL += " ASUNTO ";
                Mi_SQL += ", " + Constantes.Cat_Dependencias.Tabla_Cat_Dependencias + "." + Constantes.Cat_Dependencias.Campo_Nombre;
                Mi_SQL += " DEPENDENCIA ";
                Mi_SQL += " FROM " + Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones;
                Mi_SQL += " , ";
                Mi_SQL += Constantes.Cat_Ate_Asuntos.Tabla_Cat_Ate_Asuntos;
                Mi_SQL += " , ";
                Mi_SQL += Constantes.Cat_Dependencias.Tabla_Cat_Dependencias;
                Mi_SQL += " WHERE ";
                Mi_SQL += Constantes.Cat_Ate_Asuntos.Tabla_Cat_Ate_Asuntos + ".";
                Mi_SQL += Constantes.Cat_Ate_Asuntos.Campo_AsuntoID;
                Mi_SQL += " = " + Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".";
                Mi_SQL += Constantes.Ope_Ate_Peticiones.Campo_Asunto_ID;
                Mi_SQL += " AND " + Constantes.Cat_Dependencias.Tabla_Cat_Dependencias + ".";
                Mi_SQL += Constantes.Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL += " = " + Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".";
                Mi_SQL += Constantes.Ope_Ate_Peticiones.Campo_Dependencia_ID;

                if (Datos.P_Folio != null)
                {
                    Mi_SQL += " AND " + Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".";
                    Mi_SQL += Constantes.Ope_Ate_Peticiones.Campo_Folio + " = '" + String.Format("{0:00000}", Datos.P_Folio) + "'";
                }
                if (Datos.P_Dependencia_ID != null)
                {
                    Mi_SQL += " AND " + Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".";
                    Mi_SQL += Constantes.Ope_Ate_Peticiones.Campo_Dependencia_ID + " = '" + String.Format("{0:00000}", Datos.P_Dependencia_ID) + "'";
                    if (Datos.P_Area_ID != null)
                    {
                        Mi_SQL += " AND " + Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".";
                        Mi_SQL += Constantes.Ope_Ate_Peticiones.Campo_Area_ID + " = '" + String.Format("{0:00000}", Datos.P_Area_ID) + "'";
                    }
                }
                if (Datos.P_Estatus != null)
                {
                    if (Datos.P_Estatus == "Pendiente y Proceso")
                    {
                        Mi_SQL += " AND " + Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".";
                        Mi_SQL += Constantes.Ope_Ate_Peticiones.Campo_Estatus + " != 'POSITIVO'";

                        Mi_SQL += " AND " + Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".";
                        Mi_SQL += Constantes.Ope_Ate_Peticiones.Campo_Estatus + " != 'NEGATIVO'";
                    }
                    else
                    {
                        Mi_SQL += " AND " + Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".";
                        Mi_SQL += Constantes.Ope_Ate_Peticiones.Campo_Estatus + " = '" + Datos.P_Estatus + "'";
                    }

                }
                if (Datos.P_Fecha_Inicio != null && Datos.P_Fecha_Final != null)
                {
                    Mi_SQL += " AND CONVERT(DATETIME,CAST(" + Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".";
                    Mi_SQL += Constantes.Ope_Ate_Peticiones.Campo_Fecha_Peticion + " AS VARCHAR(20))) BETWEEN '" + String.Format("{0:dd/MMM/yyyy}", Datos.P_Fecha_Inicio) + "' AND '" + String.Format("{0:dd/MMM/yyyy}", Datos.P_Fecha_Final) + "'";
                }

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception(Ex.ToString());
                //switch (Ex.Number.ToString())
                //{
                //    case "923":
                //        throw new Exception("Error: Consulta SQL");
                //        break;
                //    case "12170":
                //        throw new Exception("Error: Conexion con el Servidor");
                //        break;
                //    default:
                //        throw new Exception("Error: " + Ex.Message);
                //        break;
                //}
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }

        }
        /****************************************************************************************
        NOMBRE DE LA FUNCION: Consultar_Peticion_Bandeja_No_Asignados
        DESCRIPCION : consulta la peticion genarada por el ciudadano                  
        PARAMETROS  : Datos: Datos que son enviados de la capa de Negocios para consultar la peticion
        CREO        : Toledo Rodriguez Jesus S.
        FECHA_CREO  : 25-Agosto-2010
        MODIFICO          :
        FECHA_MODIFICO    :
        CAUSA_MODIFICACION:
       ****************************************************************************************/
        public static DataTable Consultar_Peticion_Bandeja_No_Asignados(Cls_Cat_Ate_Peticiones_Negocio Datos)
        {
            String Mi_SQL;
            try
            {
                //SELECT OPE_ATE_PETICIONES.*, 'SIN ASIGNAR' AS ASUNTO FROM OPE_ATE_PETICIONES WHERE OPE_ATE_PETICIONES.AREA_ID IS NULL AND OPE_ATE_PETICIONES.DEPENDENCIA_ID IS NULL AND OPE_ATE_PETICIONES.ASUNTO_ID IS NULL
                Mi_SQL = "SELECT ";
                Mi_SQL += Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".*, ";
                Mi_SQL += "'SIN ASIGNAR'AS ASUNTO ";
                Mi_SQL += " FROM " + Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones;
                Mi_SQL += " WHERE ";

                Mi_SQL += Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".";
                Mi_SQL += Constantes.Ope_Ate_Peticiones.Campo_Dependencia_ID + " IS NULL AND ";

                Mi_SQL += Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".";
                Mi_SQL += Constantes.Ope_Ate_Peticiones.Campo_Area_ID + " IS NULL AND ";

                Mi_SQL += Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".";
                Mi_SQL += Constantes.Ope_Ate_Peticiones.Campo_Asunto_ID + " IS NULL AND ";

                Mi_SQL += Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".";
                Mi_SQL += Constantes.Ope_Ate_Peticiones.Campo_Estatus + " <> 'POSITIVO' AND ";

                Mi_SQL += Constantes.Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ".";
                Mi_SQL += Constantes.Ope_Ate_Peticiones.Campo_Estatus + " <> 'NEGATIVO'";


                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException Ex)
            {
                throw new Exception(Ex.ToString());
                //switch (Ex.Number.ToString())
                //{
                //    case "923":
                //        throw new Exception("Error: Consulta SQL");
                //        break;
                //    case "12170":
                //        throw new Exception("Error: Conexion con el Servidor");
                //        break;
                //    default:
                //        throw new Exception("Error: " + Ex.Message);
                //        break;
                //}
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
            }

        }
        /****************************************************************************************
        NOMBRE DE LA FUNCION: Consultar_Dependencia_ID
        DESCRIPCION : consulta el ID de la dependencia a la que pertenece el empleado             
        PARAMETROS  : Usu_ID: ID del usuario
        CREO        : Toledo Rodriguez Jesus S.
        FECHA_CREO  : 25-Agosto-2010
        MODIFICO          :
        FECHA_MODIFICO    :
        CAUSA_MODIFICACION:
       ****************************************************************************************/
        public static String Consultar_Dependencia_ID(String Usu_ID)
        {
            String Mi_SQL;
            try
            {
                Mi_SQL = " SELECT " + Constantes.Cat_Empleados.Campo_Dependencia_ID;
                Mi_SQL += " FROM " + Constantes.Cat_Empleados.Tabla_Cat_Empleados;
                Mi_SQL += " WHERE " + Constantes.Cat_Empleados.Campo_Empleado_ID + " = " + String.Format("{0:00000}", Usu_ID);

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0][0].ToString();

            }
            catch (SqlException Ex)
            {
                throw new Exception(Ex.ToString());
                //switch (Ex.Number.ToString())
                //{
                //    case "923":
                //        throw new Exception("Error: Consulta SQL");
                //        break;
                //    case "12170":
                //        throw new Exception("Error: Conexion con el Servidor");
                //        break;
                //    default:
                //        throw new Exception("Error: " + Ex.Message);
                //        break;
                //}
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: " + Ex.Message);
                //Ex = null;
            }
            finally
            {

            }

        }

        /****************************************************************************************
                NOMBRE DE LA FUNCION: Consultar_Area_ID
                DESCRIPCION : consulta el Id del area a la que pertenece el empleado
                PARAMETROS  : Usu_ID: Id del usuario
                CREO        : Toledo Rodriguez Jesus S.
                FECHA_CREO  : 25-Agosto-2010
                MODIFICO          :
                FECHA_MODIFICO    :
                CAUSA_MODIFICACION:
               ****************************************************************************************/
        public static String Consultar_Area_ID(String Usu_ID)
        {
            String Mi_SQL;
            try
            {
                Mi_SQL = " SELECT " + Constantes.Cat_Empleados.Campo_Area_ID;
                Mi_SQL += " FROM " + Constantes.Cat_Empleados.Tabla_Cat_Empleados;
                Mi_SQL += " WHERE " + Constantes.Cat_Empleados.Campo_Empleado_ID + " = " + Usu_ID;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0][0].ToString();
            }
            catch (SqlException Ex)
            {
                throw new Exception(Ex.ToString());
                //switch (Ex.Number.ToString())
                //{
                //    case "923":
                //        throw new Exception("Error: Consulta SQL");
                //        break;
                //    case "12170":
                //        throw new Exception("Error: Conexion con el Servidor");
                //        break;
                //    default:
                //        throw new Exception("Error: " + Ex.Message);
                //        break;
                //}
            }
            catch (DBConcurrencyException Ex)
            {
                throw new Exception("Error: " + Ex.Message);
            }
            catch (Exception Ex)
            {
                String Mensaje = Ex.ToString();
                return "0";
            }
            finally
            {
            }
        }
        /****************************************************************************************
        NOMBRE DE LA FUNCION: Consulta_Peticion_Correo_Solucion
        DESCRIPCION : consulta la peticion genarada por el ciudadano  para ser mostrada en el formulario de solucion                
        PARAMETROS  : Datos: Datos que son enviados de la capa de Negocios para consultar la peticion
        CREO        : Toledo Rodriguez Jesus S.
        FECHA_CREO  : 25-Agosto-2010
        MODIFICO          :
        FECHA_MODIFICO    :
        CAUSA_MODIFICACION:
       ****************************************************************************************/
        internal static DataTable Consulta_Peticion_Correo_Solucion(Cls_Cat_Ate_Peticiones_Negocio Datos)
        {
            DataTable Dt_Temporal = null;
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT " +
                         Ope_Ate_Peticiones.Campo_Folio + ", " +

                         Ope_Ate_Peticiones.Campo_Fecha_Solucion_Probable + ", " +
                         Ope_Ate_Peticiones.Campo_Nombre_Solicitante + ", " +
                         Ope_Ate_Peticiones.Campo_Apellido_Paterno + ", " +
                         Ope_Ate_Peticiones.Campo_Apellido_Materno + ", " +

                         Ope_Ate_Peticiones.Campo_Email + ", " +
                         Ope_Ate_Peticiones.Campo_Descripcion_Peticion + " AS PETICION, " +
                         Ope_Ate_Peticiones.Campo_Descripcion_Solucion + ", " +
                         Ope_Ate_Peticiones.Campo_Dependencia_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Area_ID + ", " +

                         Ope_Ate_Peticiones.Campo_Asunto_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Descripcion_Solucion + ", CAST(" +
                         Ope_Ate_Peticiones.Campo_Fecha_Peticion + " AS VARCHAR(20)) AS FECHA_PETICION, " +
                         Ope_Ate_Peticiones.Campo_Peticion_ID + ", " +
                         Ope_Ate_Peticiones.Campo_Usuario_Modifico + ", " +
                         Ope_Ate_Peticiones.Campo_Calle_No + ", " +
                         Ope_Ate_Peticiones.Campo_Estatus +

                    " FROM " + Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones +
                    " WHERE " + Ope_Ate_Peticiones.Campo_Folio + " = '" +
                    Datos.P_Folio + "'";
                Dt_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Temporal;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }
        /****************************************************************************************
        NOMBRE DE LA FUNCION: Consulta_Peticion_Seguimiento
        DESCRIPCION : consulta el seguimiento de la peticion genarada por el ciudadano
        PARAMETROS  : Datos: Datos que son enviados de la capa de Negocios para consultar la peticion
        CREO        : Toledo Rodriguez Jesus S.
        FECHA_CREO  : 25-Agosto-2010
        MODIFICO          :
        FECHA_MODIFICO    :
        CAUSA_MODIFICACION:
       ****************************************************************************************/
        internal static DataTable Consulta_Peticion_Seguimiento(Cls_Cat_Ate_Peticiones_Negocio Datos)
        {
            DataTable Dt_Temporal = null;
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT " +
                         Ope_Ate_Seguimiento_Peticiones.Tabla_Ope_Ate_Seguimiento_Peticiones + "." +
                         Ope_Ate_Seguimiento_Peticiones.Campo_Seguimiento_ID + ", " +
                         Ope_Ate_Seguimiento_Peticiones.Tabla_Ope_Ate_Seguimiento_Peticiones + "." +
                         Ope_Ate_Seguimiento_Peticiones.Campo_Peticion_ID + ", " +
                         Ope_Ate_Seguimiento_Peticiones.Tabla_Ope_Ate_Seguimiento_Peticiones + "." +
                         Ope_Ate_Seguimiento_Peticiones.Campo_Dependencia_ID + ", " +
                         Ope_Ate_Seguimiento_Peticiones.Tabla_Ope_Ate_Seguimiento_Peticiones + "." +
                         Ope_Ate_Seguimiento_Peticiones.Campo_Area_ID + ", " +
                         Ope_Ate_Seguimiento_Peticiones.Tabla_Ope_Ate_Seguimiento_Peticiones + "." +
                         Ope_Ate_Seguimiento_Peticiones.Campo_Observaciones + ", " +
                         Ope_Ate_Seguimiento_Peticiones.Tabla_Ope_Ate_Seguimiento_Peticiones + "." +
                         Ope_Ate_Seguimiento_Peticiones.Campo_Fecha_Asignacion + ", " +

                         Cat_Dependencias.Tabla_Cat_Dependencias + "." +
                         Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA , " +

                         Cat_Areas.Tabla_Cat_Areas + "." +
                         Cat_Areas.Campo_Nombre + " AS AREA, " +

                         Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + "." +
                         Ope_Ate_Peticiones.Campo_Peticion_ID + ", " +
                         Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + "." +
                         Ope_Ate_Peticiones.Campo_Folio +

                    " FROM " + Ope_Ate_Seguimiento_Peticiones.Tabla_Ope_Ate_Seguimiento_Peticiones + ", " +
                    Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + ", " +
                    Cat_Dependencias.Tabla_Cat_Dependencias + ", " +
                    Cat_Areas.Tabla_Cat_Areas +

                    " WHERE " +
                    Cat_Dependencias.Tabla_Cat_Dependencias + "." +
                    Cat_Dependencias.Campo_Dependencia_ID + " = " +
                    Ope_Ate_Seguimiento_Peticiones.Tabla_Ope_Ate_Seguimiento_Peticiones + "." +
                    Ope_Ate_Seguimiento_Peticiones.Campo_Dependencia_ID + " AND " +

                    Cat_Areas.Tabla_Cat_Areas + "." +
                    Cat_Areas.Campo_Area_ID + " = " +
                    Ope_Ate_Seguimiento_Peticiones.Tabla_Ope_Ate_Seguimiento_Peticiones + "." +
                    Ope_Ate_Seguimiento_Peticiones.Campo_Area_ID + " AND " +

                    Ope_Ate_Seguimiento_Peticiones.Tabla_Ope_Ate_Seguimiento_Peticiones + "." +
                    Ope_Ate_Seguimiento_Peticiones.Campo_Peticion_ID + " = " +
                    Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + "." +
                    Ope_Ate_Peticiones.Campo_Peticion_ID + " AND " +

                    Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones + "." +
                    Ope_Ate_Peticiones.Campo_Folio + " = '" +
                    Datos.P_Folio + "' ORDER BY " +
                    Ope_Ate_Seguimiento_Peticiones.Tabla_Ope_Ate_Seguimiento_Peticiones + "." +
                    Ope_Ate_Seguimiento_Peticiones.Campo_Seguimiento_ID + " DESC ";

                Dt_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Temporal;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }
        /****************************************************************************************
        NOMBRE DE LA FUNCION: Consulta_Folio
        DESCRIPCION : consulta el folio consecutivo para mostrarselo al ciudadano cuando realize peticion
        PARAMETROS  : 
        CREO        : Toledo Rodriguez Jesus S.
        FECHA_CREO  : 25-Agosto-2010
        MODIFICO          :
        FECHA_MODIFICO    :
        CAUSA_MODIFICACION:
        ****************************************************************************************/
        internal static string Consulta_Folio()
        {
            //Declaraion de variables            
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL;
            Object ID_Consecutivo;
            //Inicializacion de variables
            Mi_SQL = String.Empty;
            String Folio;
            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Comando.Connection = Obj_Conexion;
                Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Ate_Peticiones.Campo_Peticion_ID + "),'0000000000') " +
                         " FROM " + Ope_Ate_Peticiones.Tabla_Ope_Ate_Peticiones;

                ID_Consecutivo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Convert.IsDBNull(ID_Consecutivo))
                {
                    Folio = "PT-0000000001";
                }
                else
                {
                    String ID = string.Format("{0:0000000000}", Convert.ToInt32(ID_Consecutivo) );
                    Folio = "PT-" + ID;
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            return Folio;
        }
    }
}