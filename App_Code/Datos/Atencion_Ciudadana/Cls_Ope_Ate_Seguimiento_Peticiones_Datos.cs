﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Seguimiento_Peticiones.Negocios;
using SharpContent.ApplicationBlocks.Data;
namespace JAPAMI.Seguimiento_Peticiones.Datos
{
    public class Cls_Ope_Ate_Seguimiento_Peticiones_Datos
    {
        #region Metodos
        public static int Alta_Seguimiento(Cls_Ope_Ate_Seguimiento_Peticiones_Negocio Datos) {
            int Registros_Afectados = 0;            
            String Mi_SQL = "";
            Object Obj;           
            try
            {
                Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Ate_Seguimiento_Peticiones.Campo_Seguimiento_ID + "),'00000') " +
                         "FROM " + Ope_Ate_Seguimiento_Peticiones.Tabla_Ope_Ate_Seguimiento_Peticiones;
                Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Convert.IsDBNull(Obj))
                {
                    Datos.P_Seguimiento_ID = "0000000001";
                }
                else
                {
                    Datos.P_Seguimiento_ID = string.Format("{0:0000000000}", Convert.ToInt32(Obj) + 1);
                }
                Mi_SQL = "INSERT INTO " +
                    Ope_Ate_Seguimiento_Peticiones.Tabla_Ope_Ate_Seguimiento_Peticiones + " (" +
                    Ope_Ate_Seguimiento_Peticiones.Campo_Seguimiento_ID + ", " +
                    Ope_Ate_Seguimiento_Peticiones.Campo_Peticion_ID + ", " +
                    Ope_Ate_Seguimiento_Peticiones.Campo_Asunto_ID + ", " +
                    Ope_Ate_Seguimiento_Peticiones.Campo_Area_ID + ", " +
                    Ope_Ate_Seguimiento_Peticiones.Campo_Dependencia_ID + ", " +
                    Ope_Ate_Seguimiento_Peticiones.Campo_Observaciones + ") VALUES ('" +
                    Datos.P_Seguimiento_ID + "', '" +
                    Datos.P_Peticion_ID + "', '" +
                    Datos.P_Asunto_ID + "', '" +
                    Datos.P_Area_ID + "', '" +
                    Datos.P_Dependencia_ID + "', '" +
                    Datos.P_Observaciones + "')";
                Registros_Afectados = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                return Registros_Afectados;
            }catch(Exception Ex){
                throw new Exception(Ex.Message);
            }
        }
        public static DataSet Consultar_Seguimiento(Cls_Ope_Ate_Seguimiento_Peticiones_Negocio Datos)
        {
            try
            {
                DataSet Registros = null;
                String Mi_SQL = "SELECT " +
                Ope_Ate_Seguimiento_Peticiones.Campo_Seguimiento_ID + ", " +
                Ope_Ate_Seguimiento_Peticiones.Campo_Peticion_ID + ", " +
                Ope_Ate_Seguimiento_Peticiones.Campo_Asunto_ID + ", " +
                Ope_Ate_Seguimiento_Peticiones.Campo_Area_ID + ", " +
                Ope_Ate_Seguimiento_Peticiones.Campo_Dependencia_ID + ", " +
                Ope_Ate_Seguimiento_Peticiones.Campo_Observaciones + ", " +
                Ope_Ate_Seguimiento_Peticiones.Campo_Fecha_Asignacion +
                " FROM " +
                Ope_Ate_Seguimiento_Peticiones.Tabla_Ope_Ate_Seguimiento_Peticiones;
                if (Datos.P_Seguimiento_ID != null)
                    Mi_SQL += " WHERE " + Ope_Ate_Seguimiento_Peticiones.Campo_Seguimiento_ID +
                    " ='" + Datos.P_Seguimiento_ID + "'";
                else
                    if (Datos.P_Peticion_ID != null)
                        Mi_SQL += " WHERE " + Ope_Ate_Seguimiento_Peticiones.Campo_Peticion_ID +
                        " ='" + Datos.P_Peticion_ID + "'";
                Registros = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                return Registros;
            }catch(Exception Ex){
                throw new Exception(Ex.Message);
            }
        }
        #endregion

    }
}
