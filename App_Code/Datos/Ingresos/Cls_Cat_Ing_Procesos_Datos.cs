﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Cls_Cat_Ing_Procesos.Negocio;

namespace JAPAMI.Cls_Cat_Ing_Procesos.Datos
{

    public class Cls_Cat_Ing_Procesos_Datos
    {

        #region Altas

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Proceso
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos los datos del Tipo de Pago
        ///PARAMETROS           : Proceso, instancia de Cls_Cat_Ing_Procesos_Negocio
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 30/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static Boolean Alta_Proceso(Cls_Cat_Ing_Procesos_Negocio Proceso)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            Boolean Estatus_Alta = false;

            if (Proceso.P_Cmmd != null)
            {
                Cmmd = Proceso.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            try
            {
                Proceso.P_Proceso_ID = Obtener_ID_Consecutivo(ref Cmmd, Cat_Ing_Procesos.Campo_Proceso_ID, 10);
                String Mi_SQL = "INSERT INTO " + Cat_Ing_Procesos.Tabla_Cat_Ing_Procesos;
                Mi_SQL += " (" + Cat_Ing_Procesos.Campo_Proceso_ID;
                Mi_SQL += ", " + Cat_Ing_Procesos.Campo_Descripcion;
                Mi_SQL += ", " + Cat_Ing_Procesos.Campo_Estatus;
                Mi_SQL += ", " + Cat_Ing_Procesos.Campo_Usuario_Creo;
                Mi_SQL += ", " + Cat_Ing_Procesos.Campo_Fecha_Creo + ")";
                Mi_SQL += " VALUES ('" + Proceso.P_Proceso_ID + "'";
                Mi_SQL += ", '" + Proceso.P_Descripcion + "'";
                Mi_SQL += ", '" + Proceso.P_Estatus + "'";
                Mi_SQL += ", '" + Proceso.P_Usuario + "'";
                Mi_SQL += ", GETDATE()";
                Mi_SQL += ")";
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();

                if (Proceso.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                Estatus_Alta = true;
            }
            catch (SqlException Ex)
            {
                if (Proceso.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta una P_Clave de Ingreso. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Proceso.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }

            return Estatus_Alta;
        }
        #endregion

        #region Modificaciones
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Proceso
        ///DESCRIPCIÓN          : Modifica en la Base de Datos los datos del Tipo de Pago
        ///PARAMETROS           : Proceso, instancia de Cls_Cat_Ing_Procesos_Negocio
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 30/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static Boolean Modificar_Proceso(Cls_Cat_Ing_Procesos_Negocio Proceso)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            Boolean Estatus_Modificar = false;

            if (Proceso.P_Cmmd != null)
            {
                Cmmd = Proceso.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            try
            {
                String Mi_SQL = "UPDATE " + Cat_Ing_Procesos.Tabla_Cat_Ing_Procesos;
                Mi_SQL += " SET ";
                Mi_SQL += Cat_Ing_Procesos.Campo_Descripcion + " = '" + Proceso.P_Descripcion + "'";
                Mi_SQL += ", " + Cat_Ing_Procesos.Campo_Estatus + " = '" + Proceso.P_Estatus + "'";
                Mi_SQL += ", " + Cat_Ing_Procesos.Campo_Usuario_Modifico + " = '" + Proceso.P_Usuario + "'";
                Mi_SQL += ", " + Cat_Ing_Procesos.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL += " WHERE " + Cat_Ing_Procesos.Campo_Proceso_ID + " = '" + Proceso.P_Proceso_ID + "'";
                Cmmd.CommandText = Mi_SQL;
                Cmmd.ExecuteNonQuery();

                if (Proceso.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                Estatus_Modificar = true;
            }
            catch (SqlException Ex)
            {
                if (Proceso.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar modificar un Registro de Colonias. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Proceso.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }

            return Estatus_Modificar;
        }
        #endregion

        #region Eliminaciones

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar_Proceso
        ///DESCRIPCIÓN          : Elimina de la Base de Datos los registros de Tipo de Pago
        ///PARAMETROS           : Proceso, instancia de Cls_Cat_Ing_Procesos_Negocio
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 30/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static Boolean Eliminar_Proceso(Cls_Cat_Ing_Procesos_Negocio Proceso)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            Boolean Estatus_Eliminar = false;

            if (Proceso.P_Cmmd != null)
            {
                Cmmd = Proceso.P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            try
            {
                String Mi_SQL = "";
                try
                {
                    Mi_SQL = "DELETE FROM " + Cat_Ing_Procesos.Tabla_Cat_Ing_Procesos;
                    Mi_SQL += " WHERE " + Cat_Ing_Procesos.Campo_Proceso_ID + " = '" + Proceso.P_Proceso_ID + "'";
                    Cmmd.CommandText = Mi_SQL;
                    Cmmd.ExecuteNonQuery();
                }
                catch (SqlException Ex)
                {
                    if (Ex.Number == 547)
                    {
                        Mi_SQL = "UPDATE " + Cat_Ing_Procesos.Tabla_Cat_Ing_Procesos;
                        Mi_SQL += " SET " + Cat_Ing_Procesos.Campo_Estatus + " = 'CANCELADO'";
                        Mi_SQL += " WHERE " + Cat_Ing_Procesos.Campo_Proceso_ID + " = '" + Proceso.P_Proceso_ID + "'";
                        Cmmd.CommandText = Mi_SQL;
                        Cmmd.ExecuteNonQuery();
                    }
                    else
                    {
                        throw new Exception();
                    }
                }

                if (Proceso.P_Cmmd == null)
                {
                    Trans.Commit();
                }
                Estatus_Eliminar = true;
            }
            catch (SqlException Ex)
            {
                if (Ex.Number == 547)
                {
                    Mensaje = "No se puede eliminar el registro, ya que está relacionado con datos. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar eliminar el registro de Tipos de Pagos. Error: [" + Ex.Message + "]";
                }
                throw new Exception(Mensaje);
            }
            catch (Exception Ex)
            {
                Mensaje = "Error al intentar eliminar el registro de Tipos de Pagos. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Proceso.P_Cmmd == null)
                {
                    Cn.Close();
                }
            }

            return Estatus_Eliminar;
        }
        #endregion

        #region Consultas

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Procesos
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos los datos del Tipo de Pago
        ///PARAMETROS           : Proceso, instancia de Cls_Cat_Ing_Procesos_Negocio
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 30/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Procesos(Cls_Cat_Ing_Procesos_Negocio Proceso)
        {
            DataTable Dt_Procesos = new DataTable();
            String Mi_SQL;
            String Mi_SQL_Campos_Foraneos = "";
            try
            {
                if (Proceso.P_Campos_Dinamicos != null && Proceso.P_Campos_Dinamicos != "")
                {
                    Mi_SQL = "SELECT " + Mi_SQL_Campos_Foraneos + Proceso.P_Campos_Dinamicos;
                }
                else
                {
                    Mi_SQL = "SELECT ";
                    Mi_SQL += Cat_Ing_Procesos.Tabla_Cat_Ing_Procesos + "." + Cat_Ing_Procesos.Campo_Proceso_ID + ", ";
                    Mi_SQL += Cat_Ing_Procesos.Tabla_Cat_Ing_Procesos + "." + Cat_Ing_Procesos.Campo_Descripcion + ", ";
                    Mi_SQL += Cat_Ing_Procesos.Tabla_Cat_Ing_Procesos + "." + Cat_Ing_Procesos.Campo_Estatus + ", ";
                    if (Mi_SQL.EndsWith(", "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 2);
                    }
                }
                Mi_SQL += " FROM " + Cat_Ing_Procesos.Tabla_Cat_Ing_Procesos;
                if (Proceso.P_Join != null && Proceso.P_Join != "")
                {
                    Mi_SQL += " " + Proceso.P_Join;
                }
                if (Proceso.P_Filtros_Dinamicos != null && Proceso.P_Filtros_Dinamicos != "")
                {
                    Mi_SQL += " WHERE " + Proceso.P_Filtros_Dinamicos;
                }
                else
                {
                    Mi_SQL += " WHERE ";
                    if (Proceso.P_Proceso_ID != null && Proceso.P_Proceso_ID != "")
                    {
                        Mi_SQL += Cat_Ing_Procesos.Tabla_Cat_Ing_Procesos + "." + Cat_Ing_Procesos.Campo_Proceso_ID + " = '" + Proceso.P_Proceso_ID + "' AND ";
                    }
                    if (Proceso.P_Estatus != null && Proceso.P_Estatus != "")
                    {
                        Mi_SQL += Cat_Ing_Procesos.Tabla_Cat_Ing_Procesos + "." + Cat_Ing_Procesos.Campo_Estatus + " = '" + Proceso.P_Estatus + "' AND ";
                    }
                    if (Proceso.P_Descripcion != null && Proceso.P_Descripcion != "")
                    {
                        Mi_SQL += Cat_Ing_Procesos.Tabla_Cat_Ing_Procesos + "." + Cat_Ing_Procesos.Campo_Descripcion + " = '" + Proceso.P_Descripcion + "' AND ";
                    }
                    if (Mi_SQL.EndsWith(" AND "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 5);
                    }
                    if (Mi_SQL.EndsWith(" WHERE "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 7);
                    }
                }
                if (Proceso.P_Agrupar_Dinamico != null && Proceso.P_Agrupar_Dinamico != "")
                {
                    Mi_SQL += " GROUP BY " + Proceso.P_Agrupar_Dinamico;
                }
                if (Proceso.P_Ordenar_Dinamico != null && Proceso.P_Ordenar_Dinamico != "")
                {
                    Mi_SQL += " ORDER BY " + Proceso.P_Ordenar_Dinamico;
                }
                DataSet dataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataSet != null)
                {
                    Dt_Procesos = dataSet.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de la Cuentas. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Procesos;
        }

        #endregion

        #region Consulta de ID Consecutivo

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_ID_Consecutivo
        ///DESCRIPCIÓN          : Método para generar un nuevo Id del Catálogo
        ///PARAMETROS           : Cmmd, Referencia del Comando de la transacción avierta previamente por el método que lo crea.
        ///                       Campos, Columnas de la tabla en la base de datos a ser conusultadas
        ///                       Longitud_ID, tamaño final con formato del Nuevo_Valor_ID a generar
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 30/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private static String Obtener_ID_Consecutivo(ref SqlCommand Cmmd, String Campos, Int32 Longitud_ID)
        {
            String Nuevo_Valor_ID = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try
            {
                String Mi_SQL = "SELECT MAX(" + Campos + ") FROM " + Cat_Ing_Procesos.Tabla_Cat_Ing_Procesos;
                Cmmd.CommandText = Mi_SQL;
                Object Obj_Temp = Cmmd.ExecuteScalar();
                if (Obj_Temp != null)
                {
                    if (Obj_Temp.ToString() != "")
                    {
                        Nuevo_Valor_ID = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp.ToString()) + 1), Longitud_ID);
                    }
                }
            }
            catch (SqlException Ex)
            {
                new Exception(Ex.Message);
            }
            return Nuevo_Valor_ID;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Convertir_A_Formato_ID
        ///DESCRIPCIÓN          : Método para dar el formato y tamaño a la cadena del nuevo Id del Catálogo
        ///PARAMETROS           : Dato_ID, valor que va a ser formateado
        ///                       Longitud_ID, valore que se usará para dar la longitud final del nuevo Id del Catálogo
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 30/Junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
        {
            String Cadena_Formateada = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
            {
                Cadena_Formateada = Cadena_Formateada + "0";
            }
            Cadena_Formateada = Cadena_Formateada + Dato;
            return Cadena_Formateada;
        }

        #endregion

    }
}