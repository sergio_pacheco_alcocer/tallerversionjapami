﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Almacen.Contrarecibos_Caja.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Alm_Contrarecibos_Datos
/// </summary>
namespace JAPAMI.Almacen.Contrarecibos_Caja.Datos
{
    public class Cls_Ope_Alm_Contrarecibos_Caja_Datos
    {
        public Cls_Ope_Alm_Contrarecibos_Caja_Datos()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region (Metodos)
        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Alta_Contrarecibo
        /// DESCRIPCION:            Guardar el contrarecibo con los datos de las facturas o modificaciones de las mismas
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            28/Enero/2013 11:12 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static Int64 Alta_Contrarecibo(Cls_Ope_Alm_Contrarecibos_Caja_Negocio Datos)
        {
            //Declaracion de variables
            String Mensaje = "";
            String Mi_SQL = String.Empty;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            Object aux; //Variable auxiliar 
            Int64 No_Contra_Recibo = 0; //variable para el numero de contrarecibo
            int Cont_Elementos = 0; //variable para el contador
            Int64 Factura_ID = 0; //variable para el ID de la factura
            String No_Solicitud_Pago = String.Empty; //variable para la solicitud de pago
            Int64 No_Reserva = 0; //variable para consultar el numero de la reserva de la orden de compra
            
            try
            {   
                //Consulta para el numero de contrarecibo
                Mi_SQL = "SELECT MAX(NO_CONTRA_RECIBO) FROM OPE_ALM_CONTRARECIBOS ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                aux = Cmd.ExecuteScalar();

                //Verificar si no es nulo
                if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                {
                    No_Contra_Recibo = Convert.ToInt64(aux) + 1;
                }
                else
                {
                    No_Contra_Recibo = 1;
                }

                //Asignar consulta para el contrarecibo
                Mi_SQL = "INSERT INTO OPE_ALM_CONTRARECIBOS (NO_CONTRA_RECIBO, EMPLEADO_RECIBIO_ID, PROVEEDOR_ID, FECHA_RECEPCION, FECHA_PAGO, IMPORTE_TOTAL, " +
                        "USUARIO_CREO, FECHA_CREO, ESTATUS) VALUES(" + No_Contra_Recibo.ToString().Trim() + ",'" + Datos.P_Usuario_ID + "'," +
                        "'" + Datos.P_Proveedor_ID + "','" + Datos.P_Fecha_Recepcion + "'," +
                        "'" + Datos.P_Fecha_Pago + "'," + Datos.P_Importe_Total.ToString().Trim() + "," + 
                        "'" + Datos.P_Usuario + "',GETDATE(), 'GENERADO')";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Consulta para el numero de solicitud de pago
                Mi_SQL = "SELECT MAX(NO_SOLICITUD_PAGO) FROM OPE_CON_SOLICITUD_PAGOS ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                aux = Cmd.ExecuteScalar();

                //verificar si es nulo
                if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                {
                    //Verificar si no es -1
                    if (Convert.ToInt32(aux) > -1)
                    {
                        No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(aux) + 1);
                    }
                    else
                    {
                        No_Solicitud_Pago = "0000000001";
                    }
                }
                else
                {
                    No_Solicitud_Pago = "0000000001";
                }

                //Consulta para la reserva de la orden de compra
                Mi_SQL = "SELECT NO_RESERVA FROM OPE_COM_ORDENES_COMPRA WHERE NO_ORDEN_COMPRA = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                aux = Cmd.ExecuteScalar();

                //Colocar el numero de la reserva
                No_Reserva = Convert.ToInt64(aux);

                //Asignar consulta para la solicitud de pago
                Mi_SQL = "INSERT INTO OPE_CON_SOLICITUD_PAGOS (NO_SOLICITUD_PAGO, TIPO_SOLICITUD_PAGO_ID, CONCEPTO, FECHA_SOLICITUD, MONTO, ESTATUS, USUARIO_CREO, " + 
                    "FECHA_CREO, NO_RESERVA) " +
                    "VALUES('" + No_Solicitud_Pago + "','00004','CONTRARECIBO NO " + No_Contra_Recibo.ToString().Trim() + "',GETDATE()," + 
                    Datos.P_Importe_Total.ToString().Trim() + ",'PENDIENTE','" + Datos.P_Usuario + "',GETDATE(), " + No_Reserva.ToString().Trim() + ")";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Ciclo para el barrido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Datos.P_Dt_Facturas.Rows.Count; Cont_Elementos++)
                {
                    //Limpiar la variable del SQL
                    Mi_SQL = "";

                    //Verificar el tipo (alta, baja, cambio)
                    switch (Datos.P_Dt_Facturas.Rows[Cont_Elementos]["Estatus"].ToString().Trim())
                    {
                        case "ALTA": //Consulta para darlo de alta
                            //COnuslta para un nuevo ID de la factura
                            Mi_SQL = "SELECT MAX(FACTURA_ID) FROM OPE_ALM_REGISTRO_FACTURAS";

                            //Ejecutar consulta
                            Cmd.CommandText = Mi_SQL;
                            aux = Cmd.ExecuteScalar();

                            //Verificar si no es nulo
                            if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                            {
                                Factura_ID = Convert.ToInt64(aux) + 1;
                            }
                            else
                            {
                                Factura_ID = 1;
                            }

                            Mi_SQL = "INSERT INTO OPE_ALM_REGISTRO_FACTURAS (FACTURA_ID, NO_FACTURA_PROVEEDOR, NO_CONTRA_RECIBO, IMPORTE_FACTURA, FECHA_FACTURA, " +
                                "USUARIO_CREO, FECHA_CREO) VALUES(" + Factura_ID.ToString().Trim() + "," +
                                "'" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "'," + No_Contra_Recibo.ToString().Trim() + "," +
                                Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IMPORTE_FACTURA"].ToString().Trim() + "," +
                                "'" + String.Format("{0:yyyyMMdd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "'," +
                                "'" + Datos.P_Usuario + "',GETDATE())";
                            break;

                        case "BAJA":
                            Mi_SQL = "DELETE FROM OPE_ALM_REGISTRO_FACTURAS WHERE FACTURA_ID = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                            break;

                        case "CAMBIO":
                            Mi_SQL = "UPDATE OPE_ALM_REGISTRO_FACTURAS " +
                                "SET NO_FACTURA_PROVEEDOR = '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "', " + 
                                "NO_CONTRA_RECIBO = " + No_Contra_Recibo.ToString().Trim() + ", " + 
                                "IMPORTE_FACTURA = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IMPORTE_FACTURA"].ToString().Trim() + ", " + 
                                "FECHA_FACTURA = '" + String.Format("{0:yyyyMMdd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "' " + 
                                "WHERE FACTURA_ID = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                            break;

                        default:
                            Mi_SQL = "UPDATE OPE_ALM_REGISTRO_FACTURAS " +
                                "SET NO_CONTRA_RECIBO = " + No_Contra_Recibo.ToString().Trim() + " " +
                                "WHERE FACTURA_ID = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                            break;
                    }

                    //Verificar si la cadena SQL no es nula
                    if (String.IsNullOrEmpty(Mi_SQL) == false)
                    {
                        //Ejecutar consulta
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    //Agregar los detalles de la solicitud de pago
                    Mi_SQL = "INSERT INTO OPE_CON_SOLICITUD_PAGOS_DETALL (NO_SOLICITUD_PAGO, NO_FACTURA, FECHA_FACTURA, MONTO_FACTURA, USUARIO_CREO, FECHA_CREO, " +
                        "NOMBRE_PROVEEDOR_FACTURA) VALUES('" + No_Solicitud_Pago + "'," +
                        "'" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "'," +
                        "'" + String.Format("{0:yyyyMMdd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "'," +
                        Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IMPORTE"].ToString().Trim() + ",'" + Datos.P_Usuario + "',GETDATE()," + 
                        "'" + Datos.P_Proveedor + "')";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                //Ejecutar transaccion
                Trans.Commit();

                //Entregar el numero de contrarecibo
                return No_Contra_Recibo;
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje); // Se indica el mensaje 
            }
            finally
            {
                Cn.Close();
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Modificar_Contrarecibo
        /// DESCRIPCION:            Modificar un contrarecibo existente, asi como las facturas quer incluye
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            28/Enero/2013 11:39 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static void Modificar_Contrarecibo(Cls_Ope_Alm_Contrarecibos_Caja_Negocio Datos)
        {
            //Declaracion de variables
            String Mensaje = "";
            String Mi_SQL = String.Empty;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            Object aux; //Variable auxiliar 
            int Cont_Elementos = 0; //variable para el contador
            Int64 Factura_ID = 0; //variable para el ID de la factura

            try
            {
                //Asignar consulta
                Mi_SQL = "UPDATE OPE_ALM_CONTRARECIBOS SET FECHA_PAGO = '" + String.Format("{0:yyyy/MM/dd}", Datos.P_Fecha_Pago) + "', " +
                    "IMPORTE_TOTAL = " + Datos.P_Importe_Total.ToString().Trim() + ", USUARIO_MODIFICO = '" + Datos.P_Usuario + "', FECHA_MODIFICO = GETDATE() " +
                    "WHERE NO_CONTRA_RECIBO = " + Datos.P_No_Contra_Recibo.ToString().Trim();

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Ciclo para el barrido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Datos.P_Dt_Facturas.Rows.Count; Cont_Elementos++)
                {
                    //Verificar el tipo (alta, baja, cambio)
                    switch (Datos.P_Dt_Facturas.Rows[Cont_Elementos]["ESTATUS"].ToString().Trim())
                    {
                        case "ALTA": //Consulta para darlo de alta
                            //COnuslta para un nuevo ID de la factura
                            Mi_SQL = "SELECT MAX(FACTURA_ID) FROM OPE_ALM_REGISTRO_FACTURAS";

                            //Ejecutar consulta
                            Cmd.CommandText = Mi_SQL;
                            aux = Cmd.ExecuteScalar();

                            //Verificar si no es nulo
                            if (String.IsNullOrEmpty(aux.ToString().Trim()) == false)
                            {
                                Factura_ID = Convert.ToInt64(aux) + 1;
                            }
                            else
                            {
                                Factura_ID = 1;
                            }

                            Mi_SQL = "INSERT INTO OPE_ALM_REGISTRO_FACTURAS (FACTURA_ID, NO_FACTURA_PROVEEDOR, NO_CONTRA_RECIBO, IMPORTE_FACTURA, FECHA_FACTURA, " +
                                "USUARIO_CREO, FECHA_CREO) VALUES(" + Factura_ID.ToString().Trim() + "," +
                                "'" + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "'," + Datos.P_No_Contra_Recibo.ToString().Trim() + "," +
                                Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IMPORTE_FACTURA"].ToString().Trim() + "," +
                                "'" + String.Format("{0:yyyy/MM/dd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "'," +
                                "'" + Datos.P_Usuario + "',GETDATE())";
                            break;

                        case "BAJA":
                            Mi_SQL = "DELETE FROM OPE_ALM_REGISTRO_FACTURAS WHERE FACTURA_ID = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                            break;

                        case "CAMBIO":
                            Mi_SQL = "UPDATE OPE_ALM_REGISTRO_FACTURAS " +
                                "SET NO_FACTURA_PROVEEDOR = '" + Datos.P_Dt_Facturas.Rows[Cont_Elementos][" NO_FACTURA_PROVEEDOR"].ToString().Trim() + "', " +
                                "NO_CONTRA_RECIBO = " + Datos.P_No_Contra_Recibo.ToString().Trim() + ", " +
                                "IMPORTE_FACTURA = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["IMPORTE_FACTURA"].ToString().Trim() + ", " +
                                "FECHA_FACTURA = '" + String.Format("{0:yyyy/MM/dd}", Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FECHA_FACTURA"]) + "' " +
                                "WHERE FACTURA_ID = " + Datos.P_Dt_Facturas.Rows[Cont_Elementos]["FACTURA_ID"].ToString().Trim();
                            break;

                        default:
                            break;
                    }

                    //Verificar si la cadena SQL no es nula
                    if (String.IsNullOrEmpty(Mi_SQL) == false)
                    {
                        //Ejecutar consulta
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                }
             
                //Ejecutar transaccion 
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje); // Se indica el mensaje 
            }
            finally
            {
                Cn.Close();
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Contrarecibos
        /// DESCRIPCION:            Consultar los contrarecibos
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            28/Enero/2013 18:59 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Contrarecibos(Cls_Ope_Alm_Contrarecibos_Caja_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //Tabla para el resultado

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT OPE_COM_ORDENES_COMPRA.NO_ORDEN_COMPRA, OPE_COM_ORDENES_COMPRA.FOLIO AS OC, OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO, " +
                    "OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION, OPE_ALM_CONTRARECIBOS.FECHA_PAGO, CAT_COM_PROVEEDORES.NOMBRE AS PROVEEDOR, OPE_ALM_CONTRARECIBOS.IMPORTE_TOTAL " +
                    "FROM OPE_COM_ORDENES_COMPRA " +
                    "INNER JOIN CAT_COM_PROVEEDORES ON OPE_COM_ORDENES_COMPRA.PROVEEDOR_ID = CAT_COM_PROVEEDORES.PROVEEDOR_ID " +
                    "LEFT JOIN OPE_ALM_CONTRARECIBOS ON OPE_COM_ORDENES_COMPRA.NO_CONTRA_RECIBO = OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO " +
                    "WHERE (OPE_COM_ORDENES_COMPRA.ESTATUS = 'SURTIDA' OR OPE_COM_ORDENES_COMPRA.ESTATUS = 'SURTIDA_FACTURA') ";

                //Verificar si se tiene el numero de contrarecibo
                if (Datos.P_No_Contra_Recibo > 0 || Datos.P_No_Orden_Compra > 0)
                {
                    //Verificar si fue busqueda de contrarecibo u orden compra
                    if (Datos.P_No_Contra_Recibo > 0)
                    {
                        Mi_SQL += "AND OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                    }
                    else
                    {
                        Mi_SQL += "AND OPE_COM_ORDENES_COMPRA.NO_ORDEN_COMPRA = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";
                    }
                }
                else
                {
                    //Verificar los filtros
                    //Proveedor
                    if (String.IsNullOrEmpty(Datos.P_Proveedor_ID) == false)
                    {
                        Mi_SQL += "AND OPE_COM_ORDENES_COMPRA.PROVEEDOR_ID = '" + Datos.P_Proveedor_ID + "' ";
                    }

                    //Estatus
                    if (String.IsNullOrEmpty(Datos.P_Estatus) == false)
                    {
                        //Resto de la consulta
                        Mi_SQL += "AND OPE_ALM_CONTRARECIBOS.ESTATUS = '" + Datos.P_Estatus + "' ";
                    }
                    
                    //Rango de fechas
                    if (Datos.P_Fecha_Inicio != "01/01/1900")
                    {
                        //Fecha final
                        if (Datos.P_Fecha_Fin != "01/01/1900")
                        {
                            Mi_SQL += "AND OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION >= '" + String.Format("{0:yyyy/MM/dd}", Datos.P_Fecha_Inicio) + " 00:00:00' " +
                                "AND OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION <= '" + String.Format("{0:yyyy/MM/dd}", Datos.P_Fecha_Fin) + " 23:59:59' ";
                        }
                        else
                        {
                            Mi_SQL += "OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION >= '" + String.Format("{0:yyyy/MM/dd}", Datos.P_Fecha_Inicio) + " 00:00:00' " +
                                "AND OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION <= '" + String.Format("{0:yyyy/MM/dd}", DateTime.Now) + " 23:59:59' ";
                        }
                    }

                    //Orden de la consulta
                    Mi_SQL += "ORDER BY OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO, OPE_COM_ORDENES_COMPRA.NO_ORDEN_COMPRA ";
                }

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Proveedores
        /// DESCRIPCION:            Consultar los proveedores
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            30/Enero/2013 11:50 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Proveedores(Cls_Ope_Alm_Contrarecibos_Caja_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DataTable Dt_Proveedores = new DataTable(); //tabla para el resultado de la consulta

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT PROVEEDOR_ID, COMPANIA AS PROVEEDOR FROM CAT_COM_PROVEEDORES " +
                    "WHERE NOMBRE COLLATE SQL_Latin1_General_CP850_CI_AI LIKE '%" + Datos.P_Busqueda + "%' " +
                    "OR COMPANIA COLLATE SQL_Latin1_General_CP850_CI_AI LIKE '%" + Datos.P_Busqueda + "%' " +
                    "ORDER BY COMPANIA ";

                //Ejecutar consulta
                Dt_Proveedores = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Proveedores;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Datos_Contrarecibo
        /// DESCRIPCION:            Consultar los datos de los contrarecibos
        /// PARAMETROS :            Datos: Variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            30/Enero/2013 14:00 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataSet Consulta_Datos_Contrarecibo(Cls_Ope_Alm_Contrarecibos_Caja_Negocio Datos)
        {
            //Declaracion de variables
            DataTable Dt_Cabecera = new DataTable(); //tabla para los datos de la cabecera
            DataTable Dt_Detalles = new DataTable(); //Tabla para los detalles
            DataTable Dt_Aux = new DataTable(); //tabla auxiliar para las consultas
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataSet Ds_Resultado = new DataSet(); //dataset para el resultado
            
            try
            {
                //Consulta para los datos de la cabecera
                Mi_SQL = "SELECT OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO, OPE_ALM_CONTRARECIBOS.ESTATUS, OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION, " +
                    "OPE_ALM_CONTRARECIBOS.FECHA_PAGO, CAT_COM_PROVEEDORES.COMPANIA AS PROVEEDOR, OPE_COM_ORDENES_COMPRA.PROVEEDOR_ID, " +
                    "OPE_COM_ORDENES_COMPRA.NO_ORDEN_COMPRA FROM OPE_COM_ORDENES_COMPRA " +
                    "LEFT JOIN OPE_ALM_CONTRARECIBOS ON OPE_COM_ORDENES_COMPRA.NO_CONTRA_RECIBO = OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO " +
                    "INNER JOIN CAT_COM_PROVEEDORES ON OPE_COM_ORDENES_COMPRA.PROVEEDOR_ID = CAT_COM_PROVEEDORES.PROVEEDOR_ID ";

                //Verificar si hay un contrarecibo
                if (Datos.P_No_Contra_Recibo > 0)
                {
                    Mi_SQL += "WHERE OPE_COM_ORDENES_COMPRA.NO_CONTRA_RECIBO = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                }

                //Verificar si hay una orden de compra
                if (Datos.P_No_Orden_Compra > 0)
                {
                    Mi_SQL += "WHERE OPE_COM_ORDENES_COMPRA.NO_ORDEN_COMPRA = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";
                }

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar los datos a la tabla de la cabecera
                Dt_Cabecera = Dt_Aux.Copy();

                //Consulta para los detalles del contrarecibo
                Mi_SQL = "SELECT OPE_ALM_REGISTRO_FACTURAS.FACTURA_ID, OPE_ALM_REGISTRO_FACTURAS.NO_FACTURA_PROVEEDOR, OPE_ALM_REGISTRO_FACTURAS.IMPORTE_FACTURA AS IMPORTE, " +
                    "ISNULL(OPE_ALM_FACTURAS_ELECTRONICAS.RUTA, '') AS ARCHIVO_XML, ESTATUS = 'ORIGINAL', OPE_ALM_REGISTRO_FACTURAS.FECHA_FACTURA, " +
                    "ISNULL(OPE_ALM_FACTURAS_ELECTRONICAS.DESCRIPCION, '') AS DESCRIPCION FROM OPE_ALM_REGISTRO_FACTURAS " +
                    "LEFT JOIN OPE_ALM_FACTURAS_ELECTRONICAS ON OPE_ALM_REGISTRO_FACTURAS.FACTURA_ID = OPE_ALM_FACTURAS_ELECTRONICAS.FACTURA_ID ";

                //Verificar si hay un contrarecibo
                if (Datos.P_No_Contra_Recibo > 0)
                {
                    Mi_SQL += "WHERE OPE_ALM_REGISTRO_FACTURAS.NO_CONTRA_RECIBO = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                }

                //Verificar si hay una orden de compra
                if (Datos.P_No_Orden_Compra > 0)
                {
                    Mi_SQL += "WHERE OPE_ALM_REGISTRO_FACTURAS.NO_ORDEN_COMPRA = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";
                }                   

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar los datos a la tabla de los detalles
                Dt_Detalles = Dt_Aux.Copy();

                //Colocat tablas en el dataset:
                Dt_Cabecera.TableName = "Cabecera";
                Dt_Detalles.TableName = "Detalles";
                Ds_Resultado.Tables.Add(Dt_Cabecera);
                Ds_Resultado.Tables.Add(Dt_Detalles);

                //Entregar resultado
                return Ds_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Proximo_No_Contrarecibo
        /// DESCRIPCION:            Consultar el proximo numero de contrarecibo
        /// PARAMETROS :            
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            31/Enero/2013 19:26 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static Int64 Proximo_No_Contrarecibo()
        {
            //Declaracion de variables
            Int64 No_Contrarecibo = 0; //Variable para el numero de contrarecibo
            String Mi_SQL = String.Empty; //variable para las consultas
            Object Aux; //Variable auxiliar para las consultas

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT MAX(NO_CONTRA_RECIBO) FROM OPE_ALM_CONTRARECIBOS";

                //Ejecutar consulta
                Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                //Verificatr si no es nulo
                if (String.IsNullOrEmpty(Aux.ToString().Trim()) == false)
                {
                    No_Contrarecibo = Convert.ToInt64(Aux) + 1;
                }
                else
                {
                    No_Contrarecibo = 1;
                }

                //Entregar resultado
                return No_Contrarecibo;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Reporte_Contrarecibo
        /// DESCRIPCION:            Consulta para el llenado del reporte del contrarecibo
        /// PARAMETROS :            Datos: variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            05/Febrero/2013 12:00 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataSet Consulta_Reporte_Contrarecibo(Cls_Ope_Alm_Contrarecibos_Caja_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataSet Ds_Resultado = new DataSet(); //Dataset para el resultado
            DataTable Dt_Aux = new DataTable(); //tabla auxiliar para las consultas
            DataTable Dt_Cabecera = new DataTable(); //tabla para la cabecera del reporte
            DataTable Dt_Detalles = new DataTable(); //tabla para los detalles

            try
            {
                //Consulta para la cabecera
                Mi_SQL = "SELECT OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO, OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION, OPE_ALM_CONTRARECIBOS.FECHA_PAGO, " +
                    "CAT_COM_PROVEEDORES.COMPANIA AS PROVEEDOR, " +
                    "(Cat_Empleados.APELLIDO_PATERNO + ' ' + ISNULL(Cat_Empleados.APELLIDO_MATERNO, '') + ' ' + Cat_Empleados.NOMBRE) AS RECIBIO, " +
                    "OPE_ALM_CONTRARECIBOS.IMPORTE_TOTAL FROM OPE_ALM_CONTRARECIBOS " +
                    "INNER JOIN CAT_COM_PROVEEDORES ON OPE_ALM_CONTRARECIBOS.PROVEEDOR_ID = CAT_COM_PROVEEDORES.PROVEEDOR_ID " +
                    "INNER JOIN Cat_Empleados ON OPE_ALM_CONTRARECIBOS.EMPLEADO_RECIBIO_ID = Cat_Empleados.EMPLEADO_ID " +
                    "WHERE OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";

                //Ejecutar consulta
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar la tabla con estructrua y datos
                Dt_Cabecera = Dt_Aux.Copy();

                //Consulta para los detalles
                Mi_SQL = "SELECT NO_CONTRA_RECIBO, NO_FACTURA_PROVEEDOR, FECHA_FACTURA, IMPORTE_FACTURA FROM OPE_ALM_REGISTRO_FACTURAS " +
                    "WHERE NO_CONTRA_RECIBO = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Copiar la tabla con estructura y datos
                Dt_Detalles = Dt_Aux.Copy();

                //Agregar las tablas al dataset
                Dt_Cabecera.TableName = "Cabecera";
                Ds_Resultado.Tables.Add(Dt_Cabecera);
                Dt_Detalles.TableName = "Detalles";
                Ds_Resultado.Tables.Add(Dt_Detalles);

                //Entregar resultado
                return Ds_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        public static void Cancelar_Contrarecibo(Cls_Ope_Alm_Contrarecibos_Caja_Negocio Datos)
        {
            //Declaracion de variables
            String Mensaje = "";
            String Mi_SQL = String.Empty;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                //Asignar consulta para la cancelación del contrarecibo
                Mi_SQL = "UPDATE OPE_ALM_CONTRARECIBOS SET ESTATUS = 'CANCELADO', FECHA_CANCELACION = '" + Datos.P_Fecha_Cancelacion + "', " +
                    "MOTIVO_CANCELACION = '" + Datos.P_Motivo_Cancelacion + "' USUARIO_MODIFICO = '" + Datos.P_Usuario + "', FECHA_MODIFICO = GETDATE() " +
                    "WHERE NO_CONTRA_RECIBO = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Consulta para la cancelacion de la solicitud de pago

                //Ejecutar transaccion
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje); // Se indica el mensaje 
            }
            finally
            {
                Cn.Close();
            }
        }
        
        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Solo_Contrarecibos
        /// DESCRIPCION:            Consultar las ordenes de compra que ya tienen un contrarecibo asignado
        /// PARAMETROS :            Datos: variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            16/Febrero/2013 12:00 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Solo_Contrarecibos(Cls_Ope_Alm_Contrarecibos_Caja_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DataTable Dt_Resultado = new DataTable(); //Tabla para el resultado
            Boolean Where_Utilizado = false; //variable que indica si la clausula where ya ha sido utilizada

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT OPE_COM_ORDENES_COMPRA.NO_ORDEN_COMPRA, OPE_COM_ORDENES_COMPRA.FOLIO AS OC, OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO, " +
                    "OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION, OPE_ALM_CONTRARECIBOS.FECHA_PAGO, CAT_COM_PROVEEDORES.NOMBRE AS PROVEEDOR, OPE_ALM_CONTRARECIBOS.IMPORTE_TOTAL, " +
                    "OPE_ALM_CONTRARECIBOS.ESTATUS FROM OPE_COM_ORDENES_COMPRA " +
                    "INNER JOIN CAT_COM_PROVEEDORES ON OPE_COM_ORDENES_COMPRA.PROVEEDOR_ID = CAT_COM_PROVEEDORES.PROVEEDOR_ID " +
                    "INNER JOIN OPE_ALM_CONTRARECIBOS ON OPE_COM_ORDENES_COMPRA.NO_CONTRA_RECIBO = OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO ";

                //Verificar si se tiene el numero de contrarecibo
                if (Datos.P_No_Contra_Recibo > 0 || Datos.P_No_Orden_Compra > 0)
                {
                    //Verificar si hay contrarecibo
                    if (Datos.P_No_Contra_Recibo > 0)
                    {
                        Mi_SQL += "WHERE OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO = " + Datos.P_No_Contra_Recibo.ToString().Trim() + " ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE OPE_ALM_CONTRARECIBOS.NO_ORDEN_COMPRA = " + Datos.P_No_Orden_Compra.ToString().Trim() + " ";
                    }
                }
                else
                {
                    //Verificar los filtros
                    //Proveedor
                    if (String.IsNullOrEmpty(Datos.P_Proveedor_ID) == false)
                    {
                        Where_Utilizado = true;

                        Mi_SQL += "WHERE OPE_ALM_CONTRARECIBOS.PROVEEDOR_ID = '" + Datos.P_Proveedor_ID + "' ";
                    }

                    //Estatus
                    if (String.IsNullOrEmpty(Datos.P_Estatus) == false)
                    {
                        //Verificar si la clausula where ya ha sido utilizado
                        if (Where_Utilizado == true)
                        {
                            Mi_SQL += "AND ";
                        }
                        else
                        {
                            Mi_SQL += "WHERE ";
                        }

                        //Resto de la consulta
                        Mi_SQL += "OPE_ALM_CONTRARECIBOS.ESTATUS = '" + Datos.P_Estatus + "' ";
                    }

                    //Rango de fechas
                    if (Datos.P_Fecha_Inicio != "01/01/1900")
                    {
                        //Verificar si la clausula where ya ha sido utilizado
                        if (Where_Utilizado == true)
                        {
                            Mi_SQL += "AND ";
                        }
                        else
                        {
                            Mi_SQL += "WHERE ";
                        }

                        //Fecha final
                        if (Datos.P_Fecha_Fin != "01/01/1900")
                        {
                            Mi_SQL += "OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION >= '" + String.Format("{0:yyyy/MM/dd}", Datos.P_Fecha_Inicio) + " 00:00:00' " +
                                "AND OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION <= '" + String.Format("{0:yyyy/MM/dd}", Datos.P_Fecha_Fin) + " 23:59:59' ";
                        }
                        else
                        {
                            Mi_SQL += "OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION >= '" + String.Format("{0:yyyy/MM/dd}", Datos.P_Fecha_Inicio) + " 00:00:00' " +
                                "AND OPE_ALM_CONTRARECIBOS.FECHA_RECEPCION <= '" + String.Format("{0:yyyy/MM/dd}", DateTime.Now) + " 23:59:59' ";
                        }
                    }

                    //Orden de la consulta
                    Mi_SQL += "ORDER BY OPE_ALM_CONTRARECIBOS.NO_CONTRA_RECIBO ";
                }

                //Ejecutar consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///******************************************************************************* 
        /// NOMBRE DE LA CLASE:     Consulta_Fecha_Pago
        /// DESCRIPCION:            Consultar la fecha de pago para el contrarecibo
        /// PARAMETROS :            Datos: variable de la capa de negocios
        /// CREO       :            Noe Mosqueda Valadez  
        /// FECHA_CREO :            01/Marzo/2013 14:35 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DateTime Consulta_Fecha_Pago(Cls_Ope_Alm_Contrarecibos_Caja_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable para las consultas
            DateTime Resultado = DateTime.Today; //Variable para el resultado
            Object aux; //Variable auxiliar para las consultas

            try
            {
                //Consulta para la proxima fecha de pago
                Mi_SQL = "SELECT FECHA_PAGO FROM OPE_ALM_FECHAS_PAGOS " + 
                    "WHERE ANIO_NO_FECHA_PAGO = " + Datos.P_Fecha_Recepcion_dt.Year.ToString().Trim() + " " +
                    "AND MONTH(FECHA_LIMITE_RECEPCION) = " + Datos.P_Fecha_Recepcion_dt.Month + " " +
                    "AND DAY(FECHA_LIMITE_RECEPCION) <= " + Datos.P_Fecha_Recepcion_dt.Day + " ";

                //Ejecutar consulta
                aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                //Verificar si no es nulo
                if (aux != null)
                {
                    //Convertir la fecha
                    Resultado = Convert.ToDateTime(aux);
                }

                //Entregar el resultado
                return Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
        #endregion
    }
}