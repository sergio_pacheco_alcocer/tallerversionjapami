﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Autorizar_Ajuste_Stock.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;

/// <summary>
/// Summary description for Cls_Ope_Tal_Autorizar_Ajuste_Inventario_Datos
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Autorizar_Ajuste_Stock.Datos {
    public class Cls_Ope_Tal_Autorizar_Ajuste_Inventario_Datos {

        #region METODOS

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Ajustes_Inventario
            ///DESCRIPCIÓN:  Consulta los Ajustes de Inventario
            ///PARAMETROS: Clase_Negocio. Datos para la Operacion
            ///CREO: 
            ///FECHA_CREO: 2011 
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16/Julio/2012
            ///CAUSA_MODIFICACIÓN:Adaptacion a Taller Municipal
            ///*******************************************************************************
            public static DataTable Consultar_Ajustes_Inventario(Cls_Ope_Tal_Autorizar_Ajuste_Inventario_Negocio Clase_Negocio)
            {
                String Mi_SQL = "";

                Mi_SQL = "SELECT AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_No_Ajuste;
                Mi_SQL = Mi_SQL + ", convert(varchar,AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Hora + ",103) AS " + Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Hora;
                Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Empleados.Campo_Apellido_Paterno;
                Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Apellido_Materno;
                Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Nombre;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Empleados.Campo_Empleado_ID;
                Mi_SQL = Mi_SQL + " = AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_Empleado_Elaboro_ID + ") AS EMPLEADO_ELABORO";
                Mi_SQL = Mi_SQL + ", AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_Estatus;
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Ajustes_Inv_Stock.Tabla_Ope_Tal_Ajustes_Inv_Stock + " AJUSTE";
                Mi_SQL = Mi_SQL + " WHERE AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_Estatus + " IN('GENERADO','AUTORIZADO','CANCELADO')";

                Mi_SQL = Mi_SQL + " ORDER BY AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_No_Ajuste + " DESC";

                if (Clase_Negocio.P_No_Ajuste != null)
                {
                    Mi_SQL = "SELECT AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_No_Ajuste;
                    Mi_SQL = Mi_SQL + ", convert(varchar, AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Hora + ",103) AS " + Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Hora;
                    Mi_SQL = Mi_SQL + ", AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_Motivo_Ajuste_Coor;
                    Mi_SQL = Mi_SQL + ", AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_Motivo_Ajuste_Dir;
                    Mi_SQL = Mi_SQL + ", convert(varchar,AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Elaboro + ",103) AS " + Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Elaboro;
                    Mi_SQL = Mi_SQL + ", convert(varchar,AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Autorizo + ",103) AS " + Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Autorizo;
                    Mi_SQL = Mi_SQL + ", convert(varchar,AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Rechazo + ",103) AS " + Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Rechazo;
                    Mi_SQL = Mi_SQL + ", AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_Estatus;
                    Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Empleados.Campo_Apellido_Paterno;
                    Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Apellido_Materno;
                    Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Nombre;
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Empleados.Campo_Empleado_ID;
                    Mi_SQL = Mi_SQL + " = AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_Empleado_Elaboro_ID + ") AS " + Ope_Tal_Ajustes_Inv_Stock.Campo_Empleado_Elaboro_ID;
                    Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Empleados.Campo_Apellido_Paterno;
                    Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Apellido_Materno;
                    Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Nombre;
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Empleados.Campo_Empleado_ID;
                    Mi_SQL = Mi_SQL + " = AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_Empleado_Autorizo_ID + ") AS " + Ope_Tal_Ajustes_Inv_Stock.Campo_Empleado_Autorizo_ID;
                    Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Empleados.Campo_Apellido_Paterno;
                    Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Apellido_Materno;
                    Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Nombre;
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Empleados.Campo_Empleado_ID;
                    Mi_SQL = Mi_SQL + " = AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_Empleado_Rechazo_ID + ") AS " + Ope_Tal_Ajustes_Inv_Stock.Campo_Empleado_Rechazo_ID;
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Ajustes_Inv_Stock.Tabla_Ope_Tal_Ajustes_Inv_Stock + " AJUSTE";
                    Mi_SQL = Mi_SQL + " WHERE AJUSTE." + Ope_Tal_Ajustes_Inv_Stock.Campo_No_Ajuste + " = '" + Clase_Negocio.P_No_Ajuste.Trim() + "'";
                }

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Detalle_Ajustes
            ///DESCRIPCIÓN:  Consulta los Detalles de un Ajuste de Inventario
            ///PARAMETROS: Clase_Negocio. Datos para la Operacion
            ///CREO: 
            ///FECHA_CREO: 2011 
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16/Julio/2012
            ///CAUSA_MODIFICACIÓN:Adaptacion a Taller Municipal
            ///*******************************************************************************
            public static DataTable Consultar_Detalle_Ajustes(Cls_Ope_Tal_Autorizar_Ajuste_Inventario_Negocio Clase_Negocio)
            {
                String Mi_SQL = "";
                Mi_SQL = "SELECT DET." + Ope_Tal_Ajustes_Detalles.Campo_Refaccion_ID;
                Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Tal_Refacciones.Campo_Clave;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID;
                Mi_SQL = Mi_SQL + " = DET." + Ope_Tal_Ajustes_Detalles.Campo_Refaccion_ID;
                Mi_SQL = Mi_SQL + ") AS CLAVE";
                Mi_SQL = Mi_SQL + ", DET." + Ope_Tal_Ajustes_Detalles.Campo_Nombre_Descipcion;
                Mi_SQL = Mi_SQL + ", DET." + Ope_Tal_Ajustes_Detalles.Campo_Existencia_Sistema;
                Mi_SQL = Mi_SQL + ", DET." + Ope_Tal_Ajustes_Detalles.Campo_Conteo_Fisico;
                Mi_SQL = Mi_SQL + ", DET." + Ope_Tal_Ajustes_Detalles.Campo_Diferencia;
                Mi_SQL = Mi_SQL + ", DET." + Ope_Tal_Ajustes_Detalles.Campo_Tipo_Movimiento;
                Mi_SQL = Mi_SQL + ", DET." + Ope_Tal_Ajustes_Detalles.Campo_Importe_Diferencia;
                Mi_SQL = Mi_SQL + ", DET." + Ope_Tal_Ajustes_Detalles.Campo_Precio_Promedio;
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Ajustes_Detalles.Tabla_Ope_Tal_Ajustes_Almacen + " DET";
                Mi_SQL = Mi_SQL + " WHERE DET." + Ope_Tal_Ajustes_Detalles.Campo_No_Ajuste;
                Mi_SQL = Mi_SQL + "='" + Clase_Negocio.P_No_Ajuste + "'";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Modificar_Ajuste_Inventario
            ///DESCRIPCIÓN:  Autoriza o Rechaza el Ajuste de Inventario
            ///PARAMETROS: Clase_Negocio. Datos para la Operacion
            ///CREO: 
            ///FECHA_CREO: 2011 
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16/Julio/2012
            ///CAUSA_MODIFICACIÓN:Adaptacion a Taller Municipal
            ///*******************************************************************************
            public static Boolean Modificar_Ajuste_Inventario(Cls_Ope_Tal_Autorizar_Ajuste_Inventario_Negocio Clase_Negocio) {
                String Mi_SQL = "";
                bool Operacion_Realizada = false;
                try {
                    Mi_SQL = "UPDATE " + Ope_Tal_Ajustes_Inv_Stock.Tabla_Ope_Tal_Ajustes_Inv_Stock;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Ajustes_Inv_Stock.Campo_Motivo_Ajuste_Dir + " = '" + Clase_Negocio.P_Motivo_Ajuste_Dir + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ajustes_Inv_Stock.Campo_Estatus + " = '" + Clase_Negocio.P_Estatus.Trim() + "'";
                    if (Clase_Negocio.P_Empleado_Autorizo_ID != null) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ajustes_Inv_Stock.Campo_Empleado_Autorizo_ID + " = '" + Clase_Negocio.P_Empleado_Autorizo_ID.Trim() + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Autorizo + " = GETDATE()";

                    }
                    if (Clase_Negocio.P_Empleado_Rechazo_ID != null) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ajustes_Inv_Stock.Campo_Empleado_Rechazo_ID + " = '" + Clase_Negocio.P_Empleado_Rechazo_ID.Trim() + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Rechazo + " = GETDATE()";
                    }
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Ajustes_Inv_Stock.Campo_No_Ajuste + " = '" + Clase_Negocio.P_No_Ajuste.Trim() + "'";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    Operacion_Realizada = true;
                } catch {
                    Operacion_Realizada = false;
                }

                return Operacion_Realizada;
            }            

        #endregion

    }
}