﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Ajustar_Stock.Negocio;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;

/// <summary>
/// Summary description for Cls_Ope_Tal_Ajustar_Stock_Datos
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Ajustar_Stock.Datos {
    public class Cls_Ope_Tal_Ajustar_Stock_Datos {

        #region MÉTODOS

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Productos
            ///DESCRIPCIÓN: Consulta los Productos
            ///PARAMETROS: 1.-Negocio. Parametros para ejecutar la Operacion
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptacion a Modulo de Taller Municipal
            ///*******************************************************************************
            public static DataTable Consultar_Productos(Cls_Ope_Tal_Ajustar_Stock_Negocio Negocio) {
                String Mi_SQL = " SELECT * FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
                Mi_SQL += " WHERE UPPER(" + Cat_Tal_Refacciones.Campo_Nombre + ") LIKE UPPER('%" + Negocio.P_Producto + "%')";
                Mi_SQL += " AND " + Cat_Tal_Refacciones.Campo_Tipo + " ='STOCK'";
                if (!String.IsNullOrEmpty(Negocio.P_Clave)) {
                    Mi_SQL += " AND " + Cat_Tal_Refacciones.Campo_Clave + " = '" + Negocio.P_Clave + "'";
                }
                DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                DataTable Dt_Table = null;
                if (_DataSet != null && _DataSet.Tables.Count > 0 && _DataSet.Tables[0].Rows.Count > 0) {
                    Dt_Table = _DataSet.Tables[0];
                }
                return Dt_Table;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Actualizar_Productos
            ///DESCRIPCIÓN: Actualiza los Productos los Productos
            ///PARAMETROS: 1.-Negocio. Parametros para ejecutar la Operacion
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptacion a Modulo de Taller Municipal
            ///*******************************************************************************
            public static Int32 Actualizar_Productos(Cls_Ope_Tal_Ajustar_Stock_Negocio Negocio) {
                try {
                    String Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " SET " + Cat_Tal_Refacciones.Campo_Existencia + " = " + Negocio.P_Existencia + "," +
                                    Cat_Tal_Refacciones.Campo_Disponible + " = " + Negocio.P_Disponible + " WHERE " + Cat_Tal_Refacciones.Campo_Clave + " = '" + Negocio.P_Clave + "'";
                    Int32 Rows = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    return Rows;
                } catch (Exception Ex) {
                    Ex.ToString();
                    return 0;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Ajustes_Inventario
            ///DESCRIPCIÓN: Consulta los Ajustes de Inventario
            ///PARAMETROS: 1.-Negocio. Parametros para ejecutar la Operacion
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptacion a Modulo de Taller Municipal
            ///*******************************************************************************
            public static DataTable Consultar_Ajustes_Inventario(Cls_Ope_Tal_Ajustar_Stock_Negocio Negocio) {
                DataTable Dt_Datos = new DataTable();
                DataSet Ds_Datos = null;
                try {
                    //String Mi_SQL = "SELECT * FROM " + Ope_Tal_Ajustes_Inv_Stock.Tabla_Ope_Tal_Ajustes_Inv_Stock +
                    //               " WHERE " + " CONVERT(DATETIME,(CONVERT(Varchar,(" + Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Creo + "))))" + " >= '" + Negocio.P_Fecha_Inicial + "' AND " +
                    //               "CONVERT(DATETIME,(CONVERT(Varchar,(" + Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Creo + "))))" + " <= '" + Negocio.P_Fecha_Final + "'";

                    String Mi_SQL;

                    Mi_SQL = "SELECT * FROM OPE_TAL_AJUSTES_INV_STOCK  " +
                             "WHERE  FECHA_CREO >= '" + Negocio.P_Fecha_Inicial + "' AND FECHA_CREO <= '" + Negocio.P_Fecha_Final + "'";
                    if (!String.IsNullOrEmpty(Negocio.P_No_Ajuste)) {
                        Mi_SQL = "SELECT * FROM " + Ope_Tal_Ajustes_Inv_Stock.Tabla_Ope_Tal_Ajustes_Inv_Stock + " WHERE " + Ope_Tal_Ajustes_Inv_Stock.Campo_No_Ajuste + " = " + Negocio.P_No_Ajuste;
                    }
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Ds_Datos != null) {
                        if (Ds_Datos.Tables.Count > 0) {
                            Dt_Datos = Ds_Datos.Tables[0];
                        }
                    }
                } catch (Exception Ex) {
                    throw new Exception(Ex.Message);
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Guardar_Ajustes_Inventario
            ///DESCRIPCIÓN: Guarda los Ajustes de Inventario
            ///PARAMETROS: 1.-Negocio. Parametros para ejecutar la Operacion
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptacion a Modulo de Taller Municipal
            ///*******************************************************************************
            public static Int32 Guardar_Ajustes_Inventario(Cls_Ope_Tal_Ajustar_Stock_Negocio Negocio) {
                Int32 Registros_Afectados = 0;
                Int32 No_Ajuste = 0;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try
                {
                    No_Ajuste = Obtener_Consecutivo(Ope_Tal_Ajustes_Inv_Stock.Campo_No_Ajuste, Ope_Tal_Ajustes_Inv_Stock.Tabla_Ope_Tal_Ajustes_Inv_Stock);
                    String Mi_SQL = "INSERT INTO " + Ope_Tal_Ajustes_Inv_Stock.Tabla_Ope_Tal_Ajustes_Inv_Stock +
                    "(" + Ope_Tal_Ajustes_Inv_Stock.Campo_No_Ajuste + "," +
                    Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Hora + "," +
                    Ope_Tal_Ajustes_Inv_Stock.Campo_Motivo_Ajuste_Coor + "," +
                    Ope_Tal_Ajustes_Inv_Stock.Campo_Empleado_Elaboro_ID + "," +
                    Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Elaboro + "," +
                    Ope_Tal_Ajustes_Inv_Stock.Campo_Estatus + "," +
                    Ope_Tal_Ajustes_Inv_Stock.Campo_Usuario_Creo + "," +
                    Ope_Tal_Ajustes_Inv_Stock.Campo_Fecha_Creo + ") VALUES (" +
                    No_Ajuste + ",GETDATE(),'" + Negocio.P_Motivo_Ajuste_Coordinador + "','" +
                    Cls_Sessiones.Empleado_ID + "',GETDATE(),'" + Negocio.P_Estatus + "','" +
                    Cls_Sessiones.Nombre_Empleado + "',GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    foreach (DataRow Producto in Negocio.P_Dt_Productos_Ajustados.Rows) {
                        Mi_SQL = "INSERT INTO " + Ope_Tal_Ajustes_Detalles.Tabla_Ope_Tal_Ajustes_Almacen +
                        "(" + Ope_Tal_Ajustes_Detalles.Campo_No_Ajuste + "," +
                        Ope_Tal_Ajustes_Detalles.Campo_Refaccion_ID + "," +
                        Ope_Tal_Ajustes_Detalles.Campo_Existencia_Sistema + "," +
                        Ope_Tal_Ajustes_Detalles.Campo_Conteo_Fisico + "," +
                        Ope_Tal_Ajustes_Detalles.Campo_Diferencia + "," +
                        Ope_Tal_Ajustes_Detalles.Campo_Tipo_Movimiento + "," +
                        Ope_Tal_Ajustes_Detalles.Campo_Importe_Diferencia + "," +
                        Ope_Tal_Ajustes_Detalles.Campo_Precio_Promedio + "," +
                        Ope_Tal_Ajustes_Detalles.Campo_Nombre_Descipcion + "," +
                        Ope_Tal_Ajustes_Detalles.Campo_Usuario_Creo + "," +
                        Ope_Tal_Ajustes_Detalles.Campo_Fecha_Creo + ") VALUES (" +
                        No_Ajuste + "," +
                        "'" + Producto[Ope_Tal_Ajustes_Detalles.Campo_Refaccion_ID].ToString() + "'," +
                        "" + Producto[Ope_Tal_Ajustes_Detalles.Campo_Existencia_Sistema].ToString() + "," +
                        "" + Producto[Ope_Tal_Ajustes_Detalles.Campo_Conteo_Fisico].ToString() + "," +
                        "" + Producto[Ope_Tal_Ajustes_Detalles.Campo_Diferencia].ToString() + "," +
                        "'" + Producto[Ope_Tal_Ajustes_Detalles.Campo_Tipo_Movimiento].ToString() + "'," +
                        "" + Producto[Ope_Tal_Ajustes_Detalles.Campo_Importe_Diferencia].ToString() + "," +
                        "" + Producto[Ope_Tal_Ajustes_Detalles.Campo_Precio_Promedio].ToString() + "," +
                        "'" + Producto[Ope_Tal_Ajustes_Detalles.Campo_Nombre_Descipcion].ToString() + "'," +
                        "'" + Cls_Sessiones.Nombre_Empleado + "',GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                    Trans.Commit();
                    Registros_Afectados = 1;
                } catch (Exception Ex) {
                    Trans.Rollback();
                    No_Ajuste = 0;
                    throw new Exception(Ex.ToString());
                }
                finally {
                    Cn.Close();
                }
                return No_Ajuste;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Aplicar_Ajuste_Inventario
            ///DESCRIPCIÓN: Aplica un Ajuste Autorizado
            ///PARAMETROS: 1.-Negocio. Parametros para ejecutar la Operacion
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptacion a Modulo de Taller Municipal
            ///*******************************************************************************
            public static Int32 Aplicar_Ajuste_Inventario(Cls_Ope_Tal_Ajustar_Stock_Negocio Negocio) {
                Int32 Registros_Afectados = 0;
                Int32 No_Ajuste = 0;
                Int32 Existencia = 0;
                Int32 Disponible = 0;
                Int32 Comprometido = 0;
                Int32 Diferencia = 0;
                DataTable Dt_Existencia = null;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try {
                    String Mi_SQL = "SELECT * FROM " + Ope_Tal_Ajustes_Detalles.Tabla_Ope_Tal_Ajustes_Almacen + " WHERE " + Ope_Tal_Ajustes_Detalles.Campo_No_Ajuste + " = " + Negocio.P_No_Ajuste;
                    DataTable Dt_Productos_De_Ajuste = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                    foreach (DataRow Producto in Dt_Productos_De_Ajuste.Rows) {
                        Mi_SQL = "SELECT " + Cat_Tal_Refacciones.Campo_Existencia + "," +
                        Cat_Tal_Refacciones.Campo_Comprometido + "," + Cat_Tal_Refacciones.Campo_Disponible +
                        " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                        " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " ='" + Producto[Cat_Tal_Refacciones.Campo_Refaccion_ID].ToString().Trim() + "'";
                        Dt_Existencia = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                        Existencia = int.Parse(Dt_Existencia.Rows[0][Cat_Tal_Refacciones.Campo_Existencia].ToString().Trim());
                        Disponible = int.Parse(Dt_Existencia.Rows[0][Cat_Tal_Refacciones.Campo_Disponible].ToString().Trim());
                        Comprometido = int.Parse(Dt_Existencia.Rows[0][Cat_Tal_Refacciones.Campo_Comprometido].ToString().Trim());
                        Diferencia = int.Parse(Producto["DIFERENCIA"].ToString().Trim());

                        //hacer update
                        Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " SET ";
                        if (Producto["TIPO_MOVIMIENTO"].ToString().Trim() == "ENTRADA") {
                            Mi_SQL += Cat_Tal_Refacciones.Campo_Existencia + " = " + Cat_Tal_Refacciones.Campo_Existencia + " + " + Diferencia + ",";
                            Mi_SQL += Cat_Tal_Refacciones.Campo_Disponible + " = " + Cat_Tal_Refacciones.Campo_Disponible + " + " + Diferencia;
                        }
                        else if (Producto["TIPO_MOVIMIENTO"].ToString().Trim() == "SALIDA") {
                            Mi_SQL += Cat_Tal_Refacciones.Campo_Existencia + " = " + Cat_Tal_Refacciones.Campo_Existencia + " - " + Diferencia + ",";
                            Mi_SQL += Cat_Tal_Refacciones.Campo_Comprometido + " = " + Cat_Tal_Refacciones.Campo_Comprometido + " - " + Diferencia;
                        }

                        Mi_SQL += " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '";
                        Mi_SQL += Producto[Cat_Tal_Refacciones.Campo_Refaccion_ID].ToString().Trim() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                    Trans.Commit();
                    Registros_Afectados = 1;
                } catch (Exception Ex) {
                    Trans.Rollback();
                    Ex.ToString();
                    Registros_Afectados = 0;
                    throw new Exception(Ex.ToString());
                } finally {
                    Cn.Close();
                }
                return Registros_Afectados;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Productos_De_Ajuste
            ///DESCRIPCIÓN:Consulta los Productos de Un Ajuste
            ///PARAMETROS: 1.-Negocio. Parametros para ejecutar la Operacion
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptacion a Modulo de Taller Municipal
            ///*******************************************************************************
            public static DataTable Consultar_Productos_De_Ajuste(Cls_Ope_Tal_Ajustar_Stock_Negocio Negocio) {
                DataTable Dt_Tabla = new DataTable();
                DataSet Ds_Tabla = null;
                try {
                    String Mi_SQL = "SELECT * FROM " + Ope_Tal_Ajustes_Detalles.Tabla_Ope_Tal_Ajustes_Almacen +
                    " WHERE " + Ope_Tal_Ajustes_Detalles.Campo_No_Ajuste + " = " + Negocio.P_No_Ajuste;
                    
                    Ds_Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Ds_Tabla != null) {
                        if (Ds_Tabla.Tables.Count > 0) {
                            Dt_Tabla = Ds_Tabla.Tables[0];
                        }
                    }
                }
                catch (Exception Ex) {
                    throw new Exception(Ex.ToString());
                }
                return Dt_Tabla;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
            ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
            ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
            ///            2.-Nombre de la tabla
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 10/Enero/2011
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            public static Int32 Obtener_Consecutivo(String Campo_ID, String Tabla)
            {
                Int32 Consecutivo = 0;
                String Mi_Sql;
                Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
                Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),0) FROM " + Tabla;
                Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                Consecutivo = (Convert.ToInt32(Obj) + 1);
                return Consecutivo;
            }

        #endregion

	}
}