﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Catalogo_Tarjetas_Gasolina.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using System.Text;

/// <summary>
/// Summary description for Cls_Cat_Tal_Tarjetas_Gasolina_Datos
/// </summary>
namespace JAPAMI.Taller_Mecanico.Catalogo_Tarjetas_Gasolina.Datos
{
    public class Cls_Cat_Tal_Tarjetas_Gasolina_Datos
    {
        #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Tarjetas_Gasolina
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 13/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Tarjetas_Gasolina(Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Parametros.P_Tarjeta_ID = Obtener_ID_Consecutivo(Cat_Tal_Tarjetas_Gasolina.Tabla_Cat_Tal_Tarjetas_Gasolina, Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID, 10);
            try
            {
                String Mi_SQL = "INSERT INTO " + Cat_Tal_Tarjetas_Gasolina.Tabla_Cat_Tal_Tarjetas_Gasolina;
                Mi_SQL = Mi_SQL + " (" + Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Numero_Tarjeta;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Emp_Asig_Tarjeta_ID;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Vehiculo_ID;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Bien_ID;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Inicial;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Actual;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Comentarios;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Usuario_Creo;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Fecha_Creo;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Estatus;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Programa_ID;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Partida_ID;
                Mi_SQL = Mi_SQL + ") VALUES ('" + Parametros.P_Tarjeta_ID;
                Mi_SQL = Mi_SQL + "', '" + Parametros.P_Numero_Tarjeta;
                Mi_SQL = Mi_SQL + "', NULL";
                if (Parametros.P_Tipo_Bien == "VEHICULO")
                {
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Vehiculo_ID;
                    Mi_SQL = Mi_SQL + "', NULL";
                }
                else if (Parametros.P_Tipo_Bien == "BIEN_MUEBLE")
                {
                    Mi_SQL = Mi_SQL + ", NULL";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Vehiculo_ID + "'";
                }
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Saldo_Inicial;
                Mi_SQL = Mi_SQL + "', '" + Parametros.P_Saldo_Inicial;// Al crear la tarjeta el saldo actual es el inicial
                Mi_SQL = Mi_SQL + "', '" + Parametros.P_Comentarios;
                Mi_SQL = Mi_SQL + "', '" + Parametros.P_Usuario_Creo;
                Mi_SQL = Mi_SQL + "', GETDATE()";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus;
                Mi_SQL = Mi_SQL + "', '" + Parametros.P_Programa_Proyecto_ID;
                Mi_SQL = Mi_SQL + "', '" + Parametros.P_Partida_ID + "' )";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
                Actualizar_Saldos_Movimientos(Parametros);
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al Number de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Tarjetas_Gasolina
        ///DESCRIPCIÓN          : Modifica en la Base de Datos el registro indicado
        ///PARAMETROS           : 
        ///                     1.Parametros. Contiene los parametros que se van hacer la
        ///                       Modificación en la Base de Datos.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 13/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Modificar_Tarjetas_Gasolina(Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                String Mi_SQL = "UPDATE " + Cat_Tal_Tarjetas_Gasolina.Tabla_Cat_Tal_Tarjetas_Gasolina;
                Mi_SQL += " SET " + Cat_Tal_Tarjetas_Gasolina.Campo_Numero_Tarjeta + " = " + Parametros.P_Numero_Tarjeta;
                if (Parametros.P_Tipo_Bien == "VEHICULO")
                {
                    Mi_SQL += ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Vehiculo_ID + " = '" + Parametros.P_Vehiculo_ID;
                    Mi_SQL += ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Bien_ID + " = NULL";
                }
                else if (Parametros.P_Tipo_Bien == "BIEN_MUEBLE")
                {
                    Mi_SQL += ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Vehiculo_ID + " = NULL";
                    Mi_SQL += ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Bien_ID + " = '" + Parametros.P_Vehiculo_ID + "'";
                }
                Mi_SQL += ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Inicial + " = " + Parametros.P_Saldo_Inicial;
                Mi_SQL += ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Comentarios + " = '" + Parametros.P_Comentarios + "'";
                Mi_SQL += ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario_Modifico + "'";
                Mi_SQL += ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL += ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                Mi_SQL += ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Partida_ID + " = '" + Parametros.P_Partida_ID + "'";
                Mi_SQL += ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Programa_ID + " = '" + Parametros.P_Programa_Proyecto_ID + "'";
                Mi_SQL += " WHERE " + Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID + " = '" + Parametros.P_Tarjeta_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
                Actualizar_Saldos_Movimientos(Parametros);
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al Number de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar Modificar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Tarjetas_Gasolina
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 13/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Tarjetas_Gasolina(Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID + " AS TARJETA_ID";
                Mi_SQL = Mi_SQL + ", convert(char,TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Numero_Tarjeta + ") AS Number_TARJETA";
                Mi_SQL = Mi_SQL + ", convert(char,TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Numero_Tarjeta + ") AS NUMERO_TARJETA";
                Mi_SQL = Mi_SQL + ", TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Emp_Asig_Tarjeta_ID + " AS EMP_ASIG_TARJETA_ID";
                Mi_SQL = Mi_SQL + ", TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Vehiculo_ID + " AS VEHICULO_ID";
                Mi_SQL = Mi_SQL + ", TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Inicial + " AS SALDO_INICIAL";
                Mi_SQL = Mi_SQL + ", TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Cargado + " AS SALDO_CARGADO";
                Mi_SQL = Mi_SQL + ", TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Consumido + " AS SALDO_CONSUMIDO";
                Mi_SQL = Mi_SQL + ", TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Actual + " AS SALDO_ACTUAL";
                Mi_SQL = Mi_SQL + ", TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Comentarios + " AS COMENTARIOS";
                Mi_SQL = Mi_SQL + ", TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + ", (EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + " +' - '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno;
                Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Nombre + ") AS NOMBRE_EMPLEADO";
                Mi_SQL = Mi_SQL + ", ISNULL(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + ", BIEN." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + ") AS NO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " AS NO_ECONOMICO";
                Mi_SQL = Mi_SQL + ", DEP." + Cat_Dependencias.Campo_Clave + " +' - '+ DEP." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Tarjetas_Gasolina.Tabla_Cat_Tal_Tarjetas_Gasolina + " TARJETAS_GASOLINA";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS";
                Mi_SQL = Mi_SQL + " ON TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Emp_Asig_Tarjeta_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS";
                Mi_SQL = Mi_SQL + " ON TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Vehiculo_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " BIEN";
                Mi_SQL = Mi_SQL + " ON TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Bien_ID + " = BIEN." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP ";
                Mi_SQL = Mi_SQL + " ON BIEN." + Ope_Pat_Bienes_Muebles.Campo_Dependencia_ID + " = DEP." + Cat_Dependencias.Campo_Dependencia_ID + " ";
                Mi_SQL = Mi_SQL + " OR VEHICULOS." + Ope_Pat_Vehiculos.Campo_Dependencia_ID + " = DEP." + Cat_Dependencias.Campo_Dependencia_ID + "";
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Numero_Empleado))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + " = '" + Parametros.P_Numero_Empleado + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Nombre_Empleado))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " (TRIM(EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Nombre + ") LIKE '%" + Parametros.P_Nombre_Empleado.Trim().ToUpper() + "%'";
                    Mi_SQL = Mi_SQL + " OR TRIM(EMPLEADOS." + Cat_Empleados.Campo_Nombre + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + ") LIKE '%" + Parametros.P_Nombre_Empleado.Trim().ToUpper() + "%')";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Rfc))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " EMPLEADOS." + Cat_Empleados.Campo_RFC + " = '" + Parametros.P_Rfc + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Unidad_Responsble))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " (VEHICULOS." + Ope_Pat_Vehiculos.Campo_Dependencia_ID + " = '" + Parametros.P_Unidad_Responsble + "' OR BIEN." + Ope_Pat_Bienes_Muebles.Campo_Dependencia_ID + " = '" + Parametros.P_Unidad_Responsble + "')";

                }
                if (!String.IsNullOrEmpty(Parametros.P_Numero_Inventario))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " (VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " = '" + Parametros.P_Numero_Inventario + "' OR BIEN." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " = '" + Parametros.P_Numero_Inventario + "')";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Numero_Economico))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " = '" + Parametros.P_Numero_Economico + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Numero_Tarjeta))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " TARJETAS_GASOLINA." + Cat_Tal_Tarjetas_Gasolina.Campo_Numero_Tarjeta + " = '" + Parametros.P_Numero_Tarjeta + "'";
                }
                Mi_SQL = Mi_SQL + " ORDER BY TARJETA_ID ASC";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Tarjetas_Gasolina
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 13/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Consultar_Detalles_Tarjetas_Gasolina(Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Parametros)
        {

            String Mi_SQL = null;
            Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Obj_Cargado = new Cls_Cat_Tal_Tarjetas_Gasolina_Negocio();
            try
            {
                Mi_SQL = "SELECT * FROM " + Cat_Tal_Tarjetas_Gasolina.Tabla_Cat_Tal_Tarjetas_Gasolina + " WHERE ";
                if (!String.IsNullOrEmpty(Parametros.P_Tarjeta_ID))
                    Mi_SQL = Mi_SQL + Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID + " = '" + Parametros.P_Tarjeta_ID + "'";
                else if (!String.IsNullOrEmpty(Parametros.P_Numero_Tarjeta))
                    Mi_SQL = Mi_SQL + Cat_Tal_Tarjetas_Gasolina.Campo_Numero_Tarjeta + " = " + Parametros.P_Numero_Tarjeta + "";
                SqlDataReader Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                while (Reader.Read())
                {
                    Obj_Cargado.P_Tarjeta_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID].ToString())) ? Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID].ToString() : "";
                    Obj_Cargado.P_Numero_Tarjeta = (!String.IsNullOrEmpty(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Numero_Tarjeta].ToString())) ? Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Numero_Tarjeta].ToString() : "";
                    Obj_Cargado.P_Emp_Asig_Tarjeta_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Emp_Asig_Tarjeta_ID].ToString())) ? Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Emp_Asig_Tarjeta_ID].ToString() : "";
                    Obj_Cargado.P_Vehiculo_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Vehiculo_ID].ToString())) ? Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Vehiculo_ID].ToString() : "";
                    Obj_Cargado.P_Bien_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Bien_ID].ToString())) ? Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Bien_ID].ToString() : "";
                    Obj_Cargado.P_Saldo_Inicial = (!String.IsNullOrEmpty(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Inicial].ToString())) ? Convert.ToDouble(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Inicial]) : (-1.0);
                    Obj_Cargado.P_Saldo_Cargado = (!String.IsNullOrEmpty(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Cargado].ToString())) ? Convert.ToDouble(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Cargado]) : (-1.0);
                    Obj_Cargado.P_Saldo_Consumido = (!String.IsNullOrEmpty(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Consumido].ToString())) ? Convert.ToDouble(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Consumido]) : (-1.0);
                    Obj_Cargado.P_Saldo_Actual = (!String.IsNullOrEmpty(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Actual].ToString())) ? Convert.ToDouble(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Actual]) : (-1.0);
                    Obj_Cargado.P_Comentarios = (!String.IsNullOrEmpty(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Comentarios].ToString())) ? Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Comentarios].ToString() : "";
                    Obj_Cargado.P_Estatus = (!String.IsNullOrEmpty(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Estatus].ToString())) ? Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Estatus].ToString() : "";
                    Obj_Cargado.P_Partida_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Partida_ID].ToString())) ? Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Partida_ID].ToString() : "";
                    Obj_Cargado.P_Programa_Proyecto_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Programa_ID].ToString())) ? Reader[Cat_Tal_Tarjetas_Gasolina.Campo_Programa_ID].ToString() : "";
                }
                Reader.Close();

                Mi_SQL = "SELECT * FROM " + Ope_Tal_Mov_Tarj_Gas.Tabla_Ope_Tal_Mov_Tarj_Gas + " WHERE " + Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID + " = '" + Parametros.P_Tarjeta_ID + "'";

                DataSet Ds_Historial = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Historial != null && Ds_Historial.Tables.Count > 0)
                {
                    Obj_Cargado.P_Dt_Historial = Ds_Historial.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Obj_Cargado;

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Movimientos_Detalles
        ///DESCRIPCIÓN          : Consulta los movimientos con sus detalles de vehiculo y fechas
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta en la Base de Datos.
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 06/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Movimientos_Detalles(Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Parametros)
        {
            Object Aux;//Variable auxiliar para las consultas
            String Mi_SQL = "";//Variable Para formar las sentencias SQL
            String Mensaje = String.Empty;//Variable que regresa el mensaje de error                
            SqlConnection Sql_Conexion = new SqlConnection();//Conexion a SQL
            SqlCommand Cmd = new SqlCommand();//Comando para ejecutar la sentencia SQL
            SqlTransaction Trans = null;//Variable para crear una transaccion para los comandos
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();//Variable para manejar DataTables
            DataTable Dt_Resultado = new DataTable();//Tabla donde se guardará el resultado de la consulta
            try
            {
                //Se crea la conección
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Sql_Conexion.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Sql_Conexion.Open();
                    Trans = Sql_Conexion.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                //Formar Query
                Mi_SQL = "SELECT TARJETAS." + Cat_Tal_Tarjetas_Gasolina.Campo_Numero_Tarjeta + " AS NUMERO_TARJETA";
                Mi_SQL = Mi_SQL + ", TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Descripcion;
                Mi_SQL = Mi_SQL + " +' Marca:  '+ MARCAS." + Cat_Com_Marcas.Campo_Nombre;
                Mi_SQL = Mi_SQL + " +' Modelo: '+ VEHICULOS." + Ope_Pat_Vehiculos.Campo_Modelo + " AS VEHICULO_DESCRIPCION";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " AS ECONOMICO";
                Mi_SQL = Mi_SQL + ", MOVIMIENTOS." + Ope_Tal_Mov_Tarj_Gas.Campo_Fecha_Movimiento + " AS FECHA_GENERACION";
                Mi_SQL = Mi_SQL + ", MOVIMIENTOS." + Ope_Tal_Mov_Tarj_Gas.Campo_Saldo_Movimiento + " AS MONTO";
                Mi_SQL = Mi_SQL + ", MOVIMIENTOS." + Ope_Tal_Mov_Tarj_Gas.Campo_No_Reserva + " AS RESERVA";
                Mi_SQL = Mi_SQL + ", MOVIMIENTOS." + Ope_Tal_Mov_Tarj_Gas.Campo_Numero_Registro + " AS NUMERO_MOVIMIENTO";
                Mi_SQL = Mi_SQL + ", TARJETAS." + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Cargado + " AS SALDO";
                Mi_SQL = Mi_SQL + ", DEPENDENCIA." + Cat_Dependencias.Campo_Nombre + " AS NOMBRE_DEPENDENCIA";
                Mi_SQL = Mi_SQL + ", DEPENDENCIA." + Cat_Dependencias.Campo_Dependencia_ID + " AS DEPENDENCIA_ID";
                Mi_SQL += " FROM " + Cat_Tal_Tarjetas_Gasolina.Tabla_Cat_Tal_Tarjetas_Gasolina + " TARJETAS ";
                Mi_SQL += " INNER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS ON TARJETAS." + Cat_Tal_Tarjetas_Gasolina.Campo_Vehiculo_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID;
                Mi_SQL += " INNER JOIN " + Ope_Tal_Mov_Tarj_Gas.Tabla_Ope_Tal_Mov_Tarj_Gas + " MOVIMIENTOS ON TARJETAS." + Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID + " = MOVIMIENTOS." + Ope_Tal_Mov_Tarj_Gas.Campo_Tarjeta_ID;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " MARCAS";
                Mi_SQL = Mi_SQL + " ON MARCAS." + Cat_Com_Marcas.Campo_Marca_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Marca_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Tipos_Vehiculo.Tabla_Cat_Pat_Tipos_Vehiculo + " TIPOS_VEHICULO";
                Mi_SQL = Mi_SQL + " ON TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Tipo_Vehiculo_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Tipo_Vehiculo_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIA";
                Mi_SQL = Mi_SQL + " ON VEHICULOS." + Ope_Pat_Vehiculos.Campo_Dependencia_ID + " = DEPENDENCIA." + Cat_Dependencias.Campo_Dependencia_ID + "";
                Mi_SQL = Mi_SQL + " WHERE MOVIMIENTOS." + Ope_Tal_Mov_Tarj_Gas.Campo_Fecha_Movimiento + " between '" + Parametros.P_Fecha_Inicial + " 00:00:00' AND '" + Parametros.P_Fecha_Final + " 23:59:00'";
                Mi_SQL = Mi_SQL + " AND MOVIMIENTOS." + Ope_Tal_Mov_Tarj_Gas.Campo_Estatus + " is null ";
                if (!string.IsNullOrEmpty(Parametros.P_Dependencia_ID) && Parametros.P_Dependencia_ID != "0")
                {
                    Mi_SQL = Mi_SQL + " AND DEPENDENCIA." + Ope_Pat_Vehiculos.Campo_Dependencia_ID + " IN (" +
                         "'" + Parametros.P_Dependencia_ID + "')";
                }
                if (!string.IsNullOrEmpty(Parametros.P_Numero_Tarjeta))
                {
                    Mi_SQL +=
                    " AND TARJETAS." + Cat_Tal_Tarjetas_Gasolina.Campo_Numero_Tarjeta +
                    " = " + Parametros.P_Numero_Tarjeta;
                }
                if (!string.IsNullOrEmpty(Parametros.P_Numero_Economico))
                {
                    Mi_SQL +=
                    " AND VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico +
                    " LIKE '%" + Parametros.P_Numero_Economico + "%'";
                }
                Mi_SQL += " UNION ";

                //Formar Query
                Mi_SQL += "SELECT TARJETAS." + Cat_Tal_Tarjetas_Gasolina.Campo_Numero_Tarjeta + " AS NUMERO_TARJETA";
                Mi_SQL = Mi_SQL + ", 'Marca:  '+ MARCAS." + Cat_Com_Marcas.Campo_Nombre;
                Mi_SQL = Mi_SQL + " +' Modelo: '+ BM." + Ope_Pat_Bienes_Muebles.Campo_Modelo + " AS VEHICULO_DESCRIPCION";
                Mi_SQL = Mi_SQL + ", BM." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie + " AS ECONOMICO";
                Mi_SQL = Mi_SQL + ", MOVIMIENTOS." + Ope_Tal_Mov_Tarj_Gas.Campo_Fecha_Movimiento + " AS FECHA_GENERACION";
                Mi_SQL = Mi_SQL + ", MOVIMIENTOS." + Ope_Tal_Mov_Tarj_Gas.Campo_Saldo_Movimiento + " AS MONTO";
                Mi_SQL = Mi_SQL + ", MOVIMIENTOS." + Ope_Tal_Mov_Tarj_Gas.Campo_No_Reserva + " AS RESERVA";
                Mi_SQL = Mi_SQL + ", MOVIMIENTOS." + Ope_Tal_Mov_Tarj_Gas.Campo_Numero_Registro + " AS NUMERO_MOVIMIENTO";
                Mi_SQL = Mi_SQL + ", TARJETAS." + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Cargado + " AS SALDO";
                Mi_SQL = Mi_SQL + ", DEPENDENCIA." + Cat_Dependencias.Campo_Nombre + " AS NOMBRE_DEPENDENCIA";
                Mi_SQL = Mi_SQL + ", DEPENDENCIA." + Cat_Dependencias.Campo_Dependencia_ID + " AS DEPENDENCIA_ID";
                Mi_SQL += " FROM " + Cat_Tal_Tarjetas_Gasolina.Tabla_Cat_Tal_Tarjetas_Gasolina + " TARJETAS ";
                Mi_SQL += " INNER JOIN " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " BM ON TARJETAS." + Cat_Tal_Tarjetas_Gasolina.Campo_Bien_ID + " = BM." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID;
                Mi_SQL += " INNER JOIN " + Ope_Tal_Mov_Tarj_Gas.Tabla_Ope_Tal_Mov_Tarj_Gas + " MOVIMIENTOS ON TARJETAS." + Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID + " = MOVIMIENTOS." + Ope_Tal_Mov_Tarj_Gas.Campo_Tarjeta_ID;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " MARCAS";
                Mi_SQL = Mi_SQL + " ON MARCAS." + Cat_Com_Marcas.Campo_Marca_ID + " = BM." + Ope_Pat_Bienes_Muebles.Campo_Marca_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIA";
                Mi_SQL = Mi_SQL + " ON BM." + Ope_Pat_Bienes_Muebles.Campo_Dependencia_ID + " = DEPENDENCIA." + Cat_Dependencias.Campo_Dependencia_ID + "";
                Mi_SQL = Mi_SQL + " WHERE MOVIMIENTOS." + Ope_Tal_Mov_Tarj_Gas.Campo_Fecha_Movimiento + " between '" + Parametros.P_Fecha_Inicial + " 00:00:00' AND '" + Parametros.P_Fecha_Final + " 23:59:00'";
                Mi_SQL = Mi_SQL + " AND MOVIMIENTOS." + Ope_Tal_Mov_Tarj_Gas.Campo_Estatus + " is null ";
                if (!string.IsNullOrEmpty(Parametros.P_Dependencia_ID) && Parametros.P_Dependencia_ID != "0")
                {
                    Mi_SQL = Mi_SQL + " AND DEPENDENCIA." + Ope_Pat_Bienes_Muebles.Campo_Dependencia_ID + " IN (" +
                         "'" + Parametros.P_Dependencia_ID + "')";
                }
                if (!string.IsNullOrEmpty(Parametros.P_Numero_Tarjeta))
                {
                    Mi_SQL +=
                    " AND TARJETAS." + Cat_Tal_Tarjetas_Gasolina.Campo_Numero_Tarjeta +
                    " = " + Parametros.P_Numero_Tarjeta;
                }
                if (!string.IsNullOrEmpty(Parametros.P_Numero_Economico))
                {
                    Mi_SQL +=
                    " AND BM." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie +
                    " LIKE '%" + Parametros.P_Numero_Economico + "%'";
                }
                ////Ejecutar Consulta
                Cmd.CommandText = Mi_SQL;
                Obj_Adaptador.SelectCommand = Cmd;
                Obj_Adaptador.Fill(Dt_Resultado);
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Sql_Conexion.Close();
                    Sql_Conexion = null;
                    Cmd = null;
                    Trans = null;
                }
            }
            return Dt_Resultado;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Movimientos_Detalles
        ///DESCRIPCIÓN          : Consulta los movimientos con sus detalles de vehiculo y fechas
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta en la Base de Datos.
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 06/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Facturas_Movimientos_Tarjetas(Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Parametros)
        {
            Object Aux;//Variable auxiliar para las consultas
            String Mi_SQL = "";//Variable Para formar las sentencias SQL
            String Mensaje = String.Empty;//Variable que regresa el mensaje de error                
            SqlConnection Sql_Conexion = new SqlConnection();//Conexion a SQL
            SqlCommand Cmd = new SqlCommand();//Comando para ejecutar la sentencia SQL
            SqlTransaction Trans = null;//Variable para crear una transaccion para los comandos
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();//Variable para manejar DataTables
            DataTable Dt_Resultado = new DataTable();//Tabla donde se guardará el resultado de la consulta
            String Proveedor_Id = "";
            Cls_Tal_Parametros_Negocio Negocio_Parametros = new Cls_Tal_Parametros_Negocio();
            try
            {
                //Se crea la conección
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Sql_Conexion.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Sql_Conexion.Open();
                    Trans = Sql_Conexion.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                Negocio_Parametros.P_Cmmd = Cmd;
                Negocio_Parametros = Negocio_Parametros.Consulta_Parametros();
                Proveedor_Id = Negocio_Parametros.P_Proveedor_Gasolina_Id;
                //Formar Query
                if (Parametros.P_obj_Bean_Facturas != null)
                {
                    for (int Cont_Facturas = 0; Cont_Facturas < Parametros.P_obj_Bean_Facturas.Count; Cont_Facturas++)
                    {
                        Mi_SQL = "INSERT INTO " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio + "(";
                        Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + "," + Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio;
                        Mi_SQL = Mi_SQL + "," + Ope_Tal_Facturas_Servicio.Campo_Factura + "," + Ope_Tal_Facturas_Servicio.Campo_Monto_Factura;
                        Mi_SQL = Mi_SQL + "," + Ope_Tal_Facturas_Servicio.Campo_Iva_Factura + "," + Ope_Tal_Facturas_Servicio.Campo_Total_Factura;
                        Mi_SQL = Mi_SQL + "," + Ope_Tal_Facturas_Servicio.Campo_Proveedor_Id + "," + Ope_Tal_Facturas_Servicio.Campo_Folio_Solicitud;
                        Mi_SQL = Mi_SQL + "," + Ope_Tal_Facturas_Servicio.Campo_Fecha_Factura + "," + Ope_Tal_Facturas_Servicio.Campo_Fecha_Creo;
                        Mi_SQL = Mi_SQL + "," + Ope_Tal_Facturas_Servicio.Campo_Usuario_Creo + ") VALUES(";
                        Mi_SQL = Mi_SQL + "(select MAX(no_factura_Servicio)+1 from OPE_TAL_FACTURAS_SERVICIO),'TARJETAS_GASOLINA'";
                        Mi_SQL = Mi_SQL + ",'" + Parametros.P_obj_Bean_Facturas[Cont_Facturas].folio_factura;
                        Mi_SQL = Mi_SQL + "'," + Parametros.P_obj_Bean_Facturas[Cont_Facturas].monto_factura.Replace(",", "");
                        Mi_SQL = Mi_SQL + "," + Parametros.P_obj_Bean_Facturas[Cont_Facturas].iva_factura.Replace(",", "");
                        Mi_SQL = Mi_SQL + "," + Parametros.P_obj_Bean_Facturas[Cont_Facturas].total_factura.Replace(",", "");
                        Mi_SQL = Mi_SQL + ",'" + Proveedor_Id;
                        Mi_SQL = Mi_SQL + "','" + Parametros.P_Movimientos;
                        Mi_SQL = Mi_SQL + "','" + Parametros.P_obj_Bean_Facturas[Cont_Facturas].fecha_factura + "',GETDATE(),'" + Parametros.P_Usuario_Creo + "')";
                        ////Ejecutar Consulta
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                    Mi_SQL = "UPDATE " + Ope_Tal_Mov_Tarj_Gas.Tabla_Ope_Tal_Mov_Tarj_Gas + " SET " + Ope_Tal_Mov_Tarj_Gas.Campo_Estatus + " = 'FACTURADO'";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Mov_Tarj_Gas.Campo_Numero_Registro + " IN('" + Parametros.P_Movimientos.Replace("-", "','") + "')";
                    ////Ejecutar Consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                if (Parametros.P_Cmmd == null)
                    Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Sql_Conexion.Close();
                    Sql_Conexion = null;
                    Cmd = null;
                    Trans = null;
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar_Tarjetas_Gasolina
        ///DESCRIPCIÓN          : Elimina un Registro de la Base de Datos
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la eliminacion de la Base de Datos.
        ///CREO                 : Salavdor Vázquez Camacho
        ///FECHA_CREO           : 13/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Eliminar_Tarjetas_Gasolina(Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                String Mi_SQL = "DELETE FROM " + Cat_Tal_Tarjetas_Gasolina.Tabla_Cat_Tal_Tarjetas_Gasolina;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID + " = '" + Parametros.P_Tarjeta_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                if (Ex.Number == 547)
                {
                    Mensaje = "No se puede eliminar el registro, ya que está relacionado con datos. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar eliminar. Error: [" + Ex.Message + "]";
                }
                throw new Exception(Mensaje);
            }
            catch (Exception Ex)
            {
                Mensaje = "Error al intentar eliminar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID)
        {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try
            {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            }
            catch (SqlException Ex)
            {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un Number entero a Formato de ID.
        ///PARAMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
        {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
            {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Agregar_Movimiento_Tarjete_Gasolina
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos un nuevo movimiento
        ///PARAMETROS           : 1. Parametros. Contiene los parametros que se van a dar de 
        ///                       Alta en la Base de Datos.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 18/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Agregar_Movimiento_Tarjete_Gasolina(Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Int32 Mi_Reserva = -1;
            SqlDataAdapter Adaptador = new SqlDataAdapter();
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            DataTable Dt_Parametros = new DataTable();
            Parametros.P_Numero_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Mov_Tarj_Gas.Tabla_Ope_Tal_Mov_Tarj_Gas, Ope_Tal_Mov_Tarj_Gas.Campo_Numero_Registro, 10)).ToString();
            String Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Tal_Mov_Tarj_Gas.Campo_Numero_Movimiento + "),0) FROM " + Ope_Tal_Mov_Tarj_Gas.Tabla_Ope_Tal_Mov_Tarj_Gas + " WHERE " + Ope_Tal_Mov_Tarj_Gas.Campo_Tarjeta_ID + "= '" + Parametros.P_Tarjeta_ID + "'";
            Cmd.CommandText = Mi_SQL;
            Object Aux = Cmd.ExecuteScalar();
            if (Aux != DBNull.Value)
            {
                Parametros.P_Numero_Movimiento = (Convert.ToInt32(Aux) + 1).ToString();
            }
            else
            {
                Parametros.P_Numero_Movimiento = "1";
            }
            try
            {
                ////Consultamos la tabla de parametros para obtener la partida especifica
                //Mi_SQL = "SELECT " + Cat_Tal_Parametros.Campo_Fuente_Financiamiento;
                //Mi_SQL += ", " + Cat_Tal_Parametros.Campo_Partida_ID;
                //Mi_SQL += ", " + Cat_Tal_Parametros.Campo_Dependencia_ID;
                //Mi_SQL += ", " + Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID;
                //Mi_SQL += ", " + Cat_Tal_Parametros.Campo_Programa_ID;
                //Mi_SQL += " FROM " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros;

                ////Sentencia que ejecuta el query
                //Cmd.CommandText = Mi_SQL;
                //Adaptador.SelectCommand = Cmd;
                //Adaptador.Fill(Dt_Parametros);

                //String Partida_ID = "0";
                //String Proyecto_ID = "0";
                //String FF = "0";
                //String Dependencia_ID = "0";
                //String Capitulo_ID = "0";
                //String Tipo_Solicitud_Pago_ID = "0";
                //String Recurso = "MUNICIPAL";
                //if (Dt_Parametros.Rows.Count > 0)
                //{
                //    FF = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Fuente_Financiamiento].ToString();
                //    Tipo_Solicitud_Pago_ID = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID].ToString();
                //    Proyecto_ID = Parametros.P_Programa_Proyecto_ID;
                //}
                //Partida_ID = Parametros.P_Partida_ID;
                //Dependencia_ID = Parametros.P_Dependencia_ID;
                //////Se consulta el CAPITULO_ID
                //Mi_SQL = "SELECT " + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado;
                //Mi_SQL += " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dependencia_ID + "'";
                //Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Proyecto_ID + "'";
                //Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + FF + "'";
                //Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida_ID + "'";
                //Cmd.CommandText = Mi_SQL;
                //Aux = Cmd.ExecuteScalar();
                //if (Aux != null)
                //    Capitulo_ID = Aux.ToString();
                //DataTable Dt_Partidas_Reserva = new DataTable();
                //Dt_Partidas_Reserva = new DataTable("Dt_Partidas_Reserva");
                //Dt_Partidas_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                //Dt_Partidas_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                //Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                //Dt_Partidas_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                //Dt_Partidas_Reserva.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                //Dt_Partidas_Reserva.Columns.Add("FECHA_CREO", System.Type.GetType("System.DateTime"));
                //Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                //Dt_Partidas_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                //DataRow Dr_Partida = Dt_Partidas_Reserva.NewRow();
                //Dr_Partida["FUENTE_FINANCIAMIENTO_ID"] = FF;
                //Dr_Partida["PROGRAMA_ID"] = Parametros.P_Programa_Proyecto_ID;
                //Dr_Partida["PARTIDA_ID"] = Partida_ID;
                //Dr_Partida["CAPITULO_ID"] = Capitulo_ID;
                //Dr_Partida["FECHA_CREO"] = DateTime.Today;
                //Dr_Partida["DEPENDENCIA_ID"] = Dependencia_ID;
                //Dr_Partida["IMPORTE"] = Convert.ToDouble(Parametros.P_Saldo_Movimiento);
                //Dr_Partida["ANIO"] = DateTime.Now.Year.ToString();
                //Dt_Partidas_Reserva.Rows.Add(Dr_Partida);
                //if (!Parametros.P_Ignorar_Reserva)
                //{
                //    //Crear Reserva
                //    Mi_Reserva = Cls_Ope_Psp_Manejo_Presupuesto.Crear_Reserva(
                //        Dependencia_ID,
                //        "GENERADA",
                //        "Beneficiario", //Beneficiario
                //        FF,
                //        Proyecto_ID,
                //        "CARGA DE GASOLINA A LA TARJETA CON NO. " + Convert.ToInt64(Parametros.P_Numero_Tarjeta).ToString().Trim(),//Concepto
                //        DateTime.Now.Year.ToString(),
                //        Convert.ToDouble(Parametros.P_Saldo_Movimiento),
                //        "", //proveedor_id
                //        "", //id del beneficiario que puede ser proveedor o empleado
                //        Tipo_Solicitud_Pago_ID,
                //        Dt_Partidas_Reserva,
                //        Recurso, Cmd);

                //    Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ";
                //    Mi_SQL += Ope_Psp_Reservas.Campo_Tipo_Reserva + "='UNICA' ";
                //    Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + Mi_Reserva;
                //    Cmd.CommandText = Mi_SQL;
                //    Cmd.ExecuteNonQuery();

                //    int Registro_Afectado;
                //    int Registro_Movimiento;
                //    Registro_Afectado = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PRE_COMPROMETIDO", "DISPONIBLE", Dt_Partidas_Reserva, ref Cmd);
                //    Registro_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Mi_Reserva), "PRE_COMPROMETIDO", "DISPONIBLE", Convert.ToDouble(Parametros.P_Saldo_Movimiento), "", "", "", "", ref Cmd);
                //}
                //Insertar el movimiento de la tarjeta
                Mi_SQL = "INSERT INTO " + Ope_Tal_Mov_Tarj_Gas.Tabla_Ope_Tal_Mov_Tarj_Gas;
                Mi_SQL += " (" + Ope_Tal_Mov_Tarj_Gas.Campo_Numero_Registro;
                Mi_SQL += ", " + Ope_Tal_Mov_Tarj_Gas.Campo_Numero_Movimiento;
                Mi_SQL += ", " + Ope_Tal_Mov_Tarj_Gas.Campo_Fecha_Movimiento;
                Mi_SQL += ", " + Ope_Tal_Mov_Tarj_Gas.Campo_Tipo_Movimiento;
                Mi_SQL += ", " + Ope_Tal_Mov_Tarj_Gas.Campo_Empleado_Realizo_ID;
                Mi_SQL += ", " + Ope_Tal_Mov_Tarj_Gas.Campo_Comentarios;
                Mi_SQL += ", " + Ope_Tal_Mov_Tarj_Gas.Campo_Saldo_Movimiento;
                Mi_SQL += ", " + Ope_Tal_Mov_Tarj_Gas.Campo_Tarjeta_ID;
                Mi_SQL += ", " + Ope_Tal_Mov_Tarj_Gas.Campo_No_Reserva;
                //Mi_SQL += ", " + Ope_Tal_Mov_Tarj_Gas.Campo_Estatus;
                Mi_SQL += ", " + Ope_Tal_Mov_Tarj_Gas.Campo_Usuario_Creo;
                Mi_SQL += ", " + Ope_Tal_Mov_Tarj_Gas.Campo_Fecha_Creo;
                Mi_SQL += ") VALUES (" + Parametros.P_Numero_Registro;
                Mi_SQL += ", " + Parametros.P_Numero_Movimiento;
                if (Parametros.P_Fecha_Movimiento == new DateTime())
                    Mi_SQL += ", GETDATE()";
                else
                    Mi_SQL += ",'" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Movimiento) + "'";//Mi_SQL = Mi_SQL + String.Format("{0:dd/MM/yyyy}", Clase_Negocio.P_Vigencia_Propuesta_) + "'";
                Mi_SQL += ", '" + Parametros.P_Tipo_Movimiento;
                Mi_SQL += "', '" + Parametros.P_Empleado_Realizo_ID;
                Mi_SQL += "', '" + Parametros.P_Comentarios;
                Mi_SQL += "', '" + Parametros.P_Saldo_Movimiento;
                Mi_SQL += "', '" + Parametros.P_Tarjeta_ID;
                Mi_SQL += "', " + Mi_Reserva;
                //Mi_SQL += " , 'APLICADO";
                Mi_SQL += ", '" + Parametros.P_Usuario_Creo;
                Mi_SQL += "', GETDATE() )";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
                if (!Parametros.P_Ignorar_Reserva)
                    Actualizar_Saldos_Movimientos(Parametros);
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al Number de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Actualizar_Saldos_Movimientos
        ///DESCRIPCIÓN          : Actualiza los Saldos Deacuerdo a los Movimientos
        ///                       Alta en la Base de Datos.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 18/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Actualizar_Saldos_Movimientos(Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Actualiza los Saldos
                String Mi_SQL = "UPDATE " + Cat_Tal_Tarjetas_Gasolina.Tabla_Cat_Tal_Tarjetas_Gasolina;
                Mi_SQL = Mi_SQL + " SET " + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Cargado + " = (SELECT ISNULL(SUM(" + Ope_Tal_Mov_Tarj_Gas.Campo_Saldo_Movimiento + "), 0) FROM " + Ope_Tal_Mov_Tarj_Gas.Tabla_Ope_Tal_Mov_Tarj_Gas + " WHERE " + Ope_Tal_Mov_Tarj_Gas.Tabla_Ope_Tal_Mov_Tarj_Gas + "." + Ope_Tal_Mov_Tarj_Gas.Campo_Tarjeta_ID + " = " + Cat_Tal_Tarjetas_Gasolina.Tabla_Cat_Tal_Tarjetas_Gasolina + "." + Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID + " AND " + Ope_Tal_Mov_Tarj_Gas.Campo_Tipo_Movimiento + " = 'CARGAR')";
                Mi_SQL = Mi_SQL + " , " + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Consumido + " = (SELECT ISNULL(SUM(" + Ope_Tal_Mov_Tarj_Gas.Campo_Saldo_Movimiento + "), 0) FROM " + Ope_Tal_Mov_Tarj_Gas.Tabla_Ope_Tal_Mov_Tarj_Gas + " WHERE " + Ope_Tal_Mov_Tarj_Gas.Tabla_Ope_Tal_Mov_Tarj_Gas + "." + Ope_Tal_Mov_Tarj_Gas.Campo_Tarjeta_ID + " = " + Cat_Tal_Tarjetas_Gasolina.Tabla_Cat_Tal_Tarjetas_Gasolina + "." + Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID + " AND " + Ope_Tal_Mov_Tarj_Gas.Campo_Estatus + " = 'CANCELADO')";
                if (!String.IsNullOrEmpty(Parametros.P_Tarjeta_ID))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID + " = '" + Parametros.P_Tarjeta_ID + "'";
                }
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Mi_SQL = "UPDATE " + Cat_Tal_Tarjetas_Gasolina.Tabla_Cat_Tal_Tarjetas_Gasolina;
                //Mi_SQL = Mi_SQL + " SET " + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Actual + " = (" + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Inicial + " + " + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Cargado + ") - " + Cat_Tal_Tarjetas_Gasolina.Campo_Saldo_Consumido + "";
                //if (!String.IsNullOrEmpty(Parametros.P_Tarjeta_ID))
                //{
                //    Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Tarjetas_Gasolina.Campo_Tarjeta_ID + " = '" + Parametros.P_Tarjeta_ID + "'";
                //}
                //Cmd.CommandText = Mi_SQL;
                //Cmd.ExecuteNonQuery();

                if (!String.IsNullOrEmpty(Parametros.P_Tarjeta_ID)) {
                    double saldoCargado =0;
                    Mi_SQL = "SELECT isnull(SUM(SALDO_MOVIMIENTO),0) FROM OPE_TAL_MOV_TARJ_GAS WHERE TARJETA_ID='" + Parametros.P_Tarjeta_ID + "' AND TIPO_MOVIMIENTO='CARGAR'";
                    Cmd.CommandText = Mi_SQL;
                    saldoCargado = Convert.ToDouble( Cmd.ExecuteScalar());


                    double saldoConsumido = 0;
                    Mi_SQL = "SELECT isnull(SUM(SALDO_MOVIMIENTO),0) FROM OPE_TAL_MOV_TARJ_GAS WHERE TARJETA_ID='" + Parametros.P_Tarjeta_ID + "' AND ESTATUS='CANCELADO'";
                    Cmd.CommandText = Mi_SQL;
                    saldoConsumido = Convert.ToDouble(Cmd.ExecuteScalar());

                    double saldoActual = 0;

                    saldoActual = saldoCargado - saldoConsumido;

                    Mi_SQL = "UPDATE Cat_Tal_Tarjetas_Gasolina SET SALDO_CARGADO=" + saldoCargado + ", SALDO_CONSUMIDO= "+  saldoConsumido +", SALDO_ACTUAL = "+ saldoActual +" WHERE TARJETA_ID='" + Parametros.P_Tarjeta_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }


                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al Number de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cancelar_Reserva
        ///DESCRIPCIÓN          : Cancela la reserva y regresa el presupuesto
        ///PARAMETROS           : 1. Parametros. Contiene los parametros que se van a 
        ///                       cancelar en la Base de Datos.
        ///CREO                 : Salvador Vázquez Camacho
        ///FECHA_CREO           : 18/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Cancelar_Reserva(Cls_Cat_Tal_Tarjetas_Gasolina_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Int32 Mi_Reserva = -1;
            String Mi_SQL = "";
            String Proyecto_ID = "0";
            String FF = "0";
            String Tipo_Solicitud_Pago_ID = "0";
            Double Costo = -1.0;

            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                Mi_SQL = " SELECT * FROM " + Ope_Tal_Mov_Tarj_Gas.Tabla_Ope_Tal_Mov_Tarj_Gas;
                Mi_SQL += " WHERE " + Ope_Tal_Mov_Tarj_Gas.Campo_Numero_Registro + " = '" + Parametros.P_Numero_Registro + "'";
                Cmd.CommandText = Mi_SQL;
                DataTable Dt_Movimiento = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                if (Dt_Movimiento.Rows.Count > 0)
                {
                    Mi_Reserva = Convert.ToInt16(Dt_Movimiento.Rows[0]["NO_RESERVA"].ToString().Trim());
                    Costo = Convert.ToDouble(Dt_Movimiento.Rows[0]["SALDO_MOVIMIENTO"].ToString().Trim());
                }

                //Consultamos la tabla de parametros para obtener la partida especifica
                Mi_SQL = "SELECT " + Cat_Tal_Parametros.Campo_Fuente_Financiamiento;
                Mi_SQL += ", " + Cat_Tal_Parametros.Campo_Partida_ID;
                Mi_SQL += ", " + Cat_Tal_Parametros.Campo_Dependencia_ID;
                Mi_SQL += ", " + Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID;
                Mi_SQL += ", " + Cat_Tal_Parametros.Campo_Programa_ID;
                Mi_SQL += " FROM " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros;
                DataTable Dt_Parametros = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                if (Dt_Parametros.Rows.Count > 0)
                {
                    FF = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Fuente_Financiamiento].ToString();
                    Tipo_Solicitud_Pago_ID = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID].ToString();
                    Proyecto_ID = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Programa_ID].ToString();
                }

                DataTable Dt_Partidas_Reserva = new DataTable();
                Dt_Partidas_Reserva = new DataTable("Dt_Partidas_Reserva");
                Dt_Partidas_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("FECHA_CREO", System.Type.GetType("System.DateTime"));
                Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                DataRow Dr_Partida = Dt_Partidas_Reserva.NewRow();
                Dr_Partida["FUENTE_FINANCIAMIENTO_ID"] = FF;
                Dr_Partida["PROGRAMA_ID"] = Proyecto_ID;
                Dr_Partida["PARTIDA_ID"] = Parametros.P_Partida_ID;
                Dr_Partida["FECHA_CREO"] = DateTime.Today;
                Dr_Partida["DEPENDENCIA_ID"] = Parametros.P_Dependencia_ID;
                Dr_Partida["IMPORTE"] = Costo;
                Dr_Partida["ANIO"] = DateTime.Now.Year.ToString();
                Dt_Partidas_Reserva.Rows.Add(Dr_Partida);

                //Verificar si la consulta arrojo resultado
                if (   Costo > -1)
                {
                    //int Registro_Afectado;
                    //int Registro_Movimiento;
                    //Se cancela Reserva
                    //Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ";
                    //Mi_SQL += Ope_Psp_Reservas.Campo_Estatus + "='CANCELADA' ";
                    //Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + Mi_Reserva;
                    //Cmd.CommandText = Mi_SQL;
                    //Cmd.ExecuteNonQuery();
                    //Se borra la reserva de los movimientos
                    Mi_SQL = "UPDATE " + Ope_Tal_Mov_Tarj_Gas.Tabla_Ope_Tal_Mov_Tarj_Gas;
                    Mi_SQL += " SET " + Ope_Tal_Mov_Tarj_Gas.Campo_Estatus + " = 'CANCELADO'";
                    Mi_SQL += ", " + Ope_Tal_Mov_Tarj_Gas.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario_Modifico + "'";
                    Mi_SQL += ", " + Ope_Tal_Mov_Tarj_Gas.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL += " WHERE " + Ope_Tal_Mov_Tarj_Gas.Campo_Numero_Registro + " = '" + Parametros.P_Numero_Registro + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Se Libera el Presupuesto PRE-comprometido
                    //Registro_Afectado = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DISPONIBLE", "PRE_COMPROMETIDO", Dt_Partidas_Reserva, Cmd);
                    //Registro_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Mi_Reserva), "DISPONIBLE", "PRE_COMPROMETIDO", Costo, "", "", "", "", Cmd);
                }
                Trans.Commit();
                Actualizar_Saldos_Movimientos(Parametros);
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al Number de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        #endregion
    }
}
