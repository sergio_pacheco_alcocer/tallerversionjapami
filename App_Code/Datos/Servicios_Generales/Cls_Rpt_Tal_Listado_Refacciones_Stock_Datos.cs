﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Reporte_Listado_Refacciones_Stock.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Rpt_Tal_Listado_Refacciones_Stock_Datos
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Reporte_Listado_Refacciones_Stock.Datos { 
    public class Cls_Rpt_Tal_Listado_Refacciones_Stock_Datos {
        
        #region METODOS

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Listado_Refacciones_Stock      
            ///DESCRIPCIÓN          : Consulta el Listado de Stock de las Refacciones
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a dar de
            ///                         Filtros para la consulta.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 17/Julio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consultar_Listado_Refacciones_Stock(Cls_Rpt_Tal_Listado_Refacciones_Stock_Negocio Parametros) {
                DataTable Dt_Datos = new DataTable();
                DataSet Ds_Datos = null;
                String Mi_SQL = "";
                Boolean Entro_Where = false;
                try {
                    Mi_SQL = "SELECT REFACCIONES." + Cat_Tal_Refacciones.Campo_Refaccion_ID + " AS REFACCION_ID";
                    Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Clave + " AS CLAVE_REFACCION";
                    Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Nombre + " AS NOMBRE_REFACCION";
                    Mi_SQL = Mi_SQL + ", (REFACCIONES." + Cat_Tal_Refacciones.Campo_Clave + " +' - '+ REFACCIONES." + Cat_Tal_Refacciones.Campo_Nombre + ") AS CLAVE_NOMBRE_REFACCION";
                    Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Existencia + " AS EXISTENCIA";
                    Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Comprometido + " AS COMPROMETIDO";
                    Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Disponible + "  AS DISPONIBLE";
                    Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Maximo + " AS MAXIMO ";
                    Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Minimo + " AS MINIMO ";
                    Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Reorden + " AS REORDEN";
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " REFACCIONES";
                    if (!String.IsNullOrEmpty(Parametros.P_Tipo_Refaccion_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " REFACCIONES." + Cat_Tal_Refacciones.Campo_Tipo_Refaccion_ID + " IN ('" + Parametros.P_Tipo_Refaccion_ID + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Ubicacion)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " REFACCIONES." + Cat_Tal_Refacciones.Campo_Ubicacion + " LIKE '%" + Parametros.P_Ubicacion + "%'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Capitulo_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " REFACCIONES." + Cat_Tal_Refacciones.Campo_Partida_ID + " IN (";
                        Mi_SQL = Mi_SQL + " SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " IN (";
                        Mi_SQL = Mi_SQL + " SELECT " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " IN (";
                        Mi_SQL = Mi_SQL + " SELECT " + Cat_Sap_Concepto.Campo_Concepto_ID + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Capitulo_ID + " IN ('" + Parametros.P_Capitulo_ID + "')";
                        Mi_SQL = Mi_SQL + ")";
                        Mi_SQL = Mi_SQL + ")";
                        Mi_SQL = Mi_SQL + ")";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Concepto_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " REFACCIONES." + Cat_Tal_Refacciones.Campo_Partida_ID + " IN (";
                        Mi_SQL = Mi_SQL + " SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " IN (";
                        Mi_SQL = Mi_SQL + " SELECT " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " IN ('" + Parametros.P_Concepto_ID + "')";
                        Mi_SQL = Mi_SQL + ")";
                        Mi_SQL = Mi_SQL + ")";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Partida_Generica_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " REFACCIONES." + Cat_Tal_Refacciones.Campo_Partida_ID + " IN (";
                        Mi_SQL = Mi_SQL + " SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " IN ('" + Parametros.P_Partida_Generica_ID + "')";
                        Mi_SQL = Mi_SQL + ")";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Partida_Especifica_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " REFACCIONES." + Cat_Tal_Refacciones.Campo_Partida_ID + " IN ('" + Parametros.P_Partida_Especifica_ID + "')";
                    }
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Ds_Datos != null) {
                        if (Ds_Datos.Tables.Count > 0) {
                            Dt_Datos = Ds_Datos.Tables[0];
                        }
                    }
                } catch (Exception Ex) {
                    throw new Exception(Ex.Message);
                }
                return Dt_Datos;
            }

        #endregion

    }
}