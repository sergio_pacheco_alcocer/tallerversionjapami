﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Listado_Almacen.Negocio;
























using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;

/// <summary>
/// Summary description for Ope_Tal_Listado_Datos
/// </summary>

namespace JAPAMI.Taller_Mecanico.Listado_Almacen.Datos
{
    public class Cls_Ope_Tal_Listado_Datos
    {

        #region Variables

        #endregion
        public Cls_Ope_Tal_Listado_Datos()
        {
            
        }

        #region Metodos

        #region Metodos Proyectos_Partidas
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Proyectos
        ///DESCRIPCIÓN: Metodo que consulta la tabla de Proyectos_Programas
        ///PARAMETROS: 1.- Cls_Ope_Tal_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 22/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************

        public DataTable Consulta_Proyectos(Cls_Ope_Tal_Listado_Negocio Datos_Listado)
        {
            String Mi_SQL = "SELECT DET." + Cat_Sap_Det_Prog_Dependencias.Campo_Proyecto_Programa_ID +
                            ", (SELECT " + Cat_Com_Proyectos_Programas.Campo_Nombre + " FROM " +
                            Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + " WHERE " +
                            Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID +
                            "=DET." + Cat_Sap_Det_Prog_Dependencias.Campo_Proyecto_Programa_ID + ")" +
                            " FROM " + Cat_Sap_Det_Prog_Dependencias.Tabla_Cat_Sap_Det_Prog_Dependencias + " DET " +
                            " WHERE DET." + Cat_Sap_Det_Prog_Dependencias.Campo_Dependencia_ID +
                            "='" + Cls_Sessiones.Dependencia_ID_Empleado + "'" +
                            " ORDER BY (SELECT " + Cat_Com_Proyectos_Programas.Campo_Nombre + " FROM " +
                            Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + " WHERE " +
                            Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID +
                            "=DET." + Cat_Sap_Det_Prog_Dependencias.Campo_Proyecto_Programa_ID + ")";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }
      
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Partidas
        ///DESCRIPCIÓN: Metodo que consulta la tabla de Partidas
        ///PARAMETROS: 1.- Cls_Ope_Tal_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 22/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Consulta_Partidas(Cls_Ope_Tal_Listado_Negocio Datos_Listado)
        {
            String Mi_SQL = "SELECT PRO." + Cat_Tal_Refacciones.Campo_Partida_ID +
                            ", (SELECT " + Cat_Com_Partidas.Campo_Clave + "+' '+"+ Cat_Com_Partidas.Campo_Nombre +
                            " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                            " WHERE " + Cat_Com_Partidas.Campo_Partida_ID +
                            " = PRO." + Cat_Tal_Refacciones.Campo_Partida_ID + ") AS NOMBRE" +
                            " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRO" +
                            " WHERE PRO." + Cat_Tal_Refacciones.Campo_Estatus + "='VIGENTE'" +
                            " GROUP BY (PRO. " + Cat_Tal_Refacciones.Campo_Partida_ID + ")" +
                            " ORDER BY (SELECT " + Cat_Com_Partidas.Campo_Clave + "+' '+"+ Cat_Com_Partidas.Campo_Nombre +
                            " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                            " WHERE " + Cat_Com_Partidas.Campo_Partida_ID +
                            " = PRO." + Cat_Tal_Refacciones.Campo_Partida_ID + ")";                            
            
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Presupuesto_Partidas
        ///DESCRIPCIÓN: Metodo que consulta la tabla de Presupuestos partidas
        ///PARAMETROS: 1.- Cls_Ope_Tal_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 22/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Consultar_Presupuesto_Partidas(Cls_Ope_Tal_Listado_Negocio Datos_Listado)
        {
            //String Mi_SQL = "SELECT " + Cat_Com_Parametros.Campo_Partida_Esp_Almacen_Global +
            //        ", " + Cat_Com_Parametros.Campo_Programa_Almacen +
            //        " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros;
            //DataTable Dt_Almacen = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            String Mi_SQL = "SELECT " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                            ", " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                            ", " + Cat_Com_Dep_Presupuesto.Campo_Monto_Presupuestal +
                            ", " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible +
                            ", " + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido +
                            ", " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio +
                            " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                            " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID + "= (SELECT " + Cat_Com_Parametros.Campo_Partida_Esp_Almacen_Global +
                            " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + ")" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID + "=(SELECT " + Cat_Com_Parametros.Campo_Programa_Almacen +
                            " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + ")" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + "=(SELECT " + Cat_Com_Parametros.Campo_Dependencia_ID_Almacen +
                            " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + ")" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto + "= TO_CHAR(GETDATE(),'YYYY')" +
                            " ORDER BY " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + " DESC";
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Giro
        ///DESCRIPCIÓN: Metodo que consulta la tabla de Giros
        ///PARAMETROS: 1.- Cls_Ope_Tal_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 22/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Consulta_Giro()
        {
            String Mi_SQL = "SELECT " + Cat_Com_Giros.Campo_Giro_ID +
                            ", " + Cat_Com_Giros.Campo_Nombre +
                            " FROM " + Cat_Com_Giros.Tabla_Cat_Com_Giros +
                            " ORDER BY " + Cat_Com_Giros.Campo_Nombre;
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        #endregion fin Metodos Proyectos Partidas

        #region Manejo de las tablas Listado

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Listado
        ///DESCRIPCIÓN: Metodo que consulta la tabla de listado
        ///PARAMETROS: 1.- Cls_Ope_Tal_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 22/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataSet Consulta_Listado(Cls_Ope_Tal_Listado_Negocio Datos_Listado)
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT LISTADO." + Ope_Tal_Listado.Campo_Folio +
                     ",  REPLACE(CONVERT(VARCHAR(11),LISTADO." + Ope_Tal_Listado.Campo_Fecha_Creo + ",106),' ','/') AS FECHA_CREO" +
                     ", LISTADO." + Ope_Tal_Listado.Campo_Tipo +
                     ", LISTADO." + Ope_Tal_Listado.Campo_Estatus +
                     ", LISTADO." + Ope_Tal_Listado.Campo_Total +
                     ", LISTADO." + Ope_Tal_Listado.Campo_Listado_ID +
                     ", (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + "+' '+" +
                     Cat_Sap_Partidas_Especificas.Campo_Nombre + " FROM " +
                     Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " +
                     Cat_Sap_Partidas_Especificas.Campo_Partida_ID + "=LISTADO." + Ope_Tal_Listado.Campo_No_Partida_ID + ") AS PARTIDA" +
                     " FROM " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado + " LISTADO";
                     

            if (Datos_Listado.P_Estatus_Busqueda != null)
            {

                Mi_SQL = Mi_SQL + " WHERE LISTADO." + Ope_Tal_Listado.Campo_Estatus + "=" + "'" + Datos_Listado.P_Estatus_Busqueda + "'";
            }
            else
            {
                Mi_SQL = Mi_SQL + " WHERE LISTADO." + Ope_Tal_Listado.Campo_Estatus + " IN(" +
                     "'EN CONSTRUCCION')";
            }

            if (Datos_Listado.P_Fecha_Inicial != null)
            {
                Mi_SQL = Mi_SQL + " AND LISTADO." + Ope_Tal_Listado.Campo_Fecha_Creo + " BETWEEN '" + Datos_Listado.P_Fecha_Inicial + "'" +
                    " AND '" + Datos_Listado.P_Fecha_Final + "'";
            }            
            if (Datos_Listado.P_Folio_Busqueda != null)
            {
                Mi_SQL = Mi_SQL + " AND UPPER(LISTADO." + Ope_Tal_Listado.Campo_Folio +
                    ") LIKE UPPER('%" + Datos_Listado.P_Folio_Busqueda + "%')";
            }

            if (Datos_Listado.P_Folio != null)
            {

                Mi_SQL = "SELECT " + Ope_Tal_Listado.Campo_Folio +
                         ",REPLACE(CONVERT(VARCHAR(11)," + Ope_Tal_Listado.Campo_Fecha_Creo + ",106),' ','/') AS FECHA_CREO" +
                         ", " + Ope_Tal_Listado.Campo_Estatus +
                         ", " + Ope_Tal_Listado.Campo_Tipo +
                         ", " + Ope_Tal_Listado.Campo_Total +
                         ", " + Ope_Tal_Listado.Campo_No_Partida_ID +
                         ", " + Ope_Tal_Listado.Campo_Listado_ID +
                         " FROM " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
                         " WHERE " + Ope_Tal_Listado.Campo_Folio + "='" + Datos_Listado.P_Folio + "'";

            }

            DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            return Data_Set;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Listado
        ///DESCRIPCIÓN: Metodo que da de alta un listado 
        ///PARAMETROS: 1.- Cls_Ope_Tal_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 22/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public String Alta_Listado(Cls_Ope_Tal_Listado_Negocio Datos_Listado) { 
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {
                String Mi_SQL = "SELECT " + Cat_Tal_Parametros.Campo_Partida_ID + ", " + Cat_Tal_Parametros.Campo_Programa_ID + " FROM " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros;
                DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                Datos_Listado.P_Partida_ID = Data_Table.Rows[0][Cat_Tal_Parametros.Campo_Partida_ID].ToString();
                Datos_Listado.P_Proyecto_ID = Data_Table.Rows[0][Cat_Tal_Parametros.Campo_Programa_ID].ToString();
                Datos_Listado.P_Listado_ID = Consecutivo(Ope_Tal_Listado.Campo_Listado_ID, Ope_Tal_Listado.Tabla_Ope_Tal_Listado);
                Datos_Listado.P_Folio = "LA-" + int.Parse(Datos_Listado.P_Listado_ID).ToString();

                Mi_SQL = " INSERT INTO " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
                                " (" + Ope_Tal_Listado.Campo_Listado_ID +
                                ", " + Ope_Tal_Listado.Campo_No_Partida_ID +
                                ", " + Ope_Tal_Listado.Campo_No_Proyecto_ID +
                                ", " + Ope_Tal_Listado.Campo_Folio +
                                ", " + Ope_Tal_Listado.Campo_Fecha_Creo +
                                ", " + Ope_Tal_Listado.Campo_Estatus;
                if (Datos_Listado.P_Estatus == "EN CONSTRUCCION") { Mi_SQL += ", " + Ope_Tal_Listado.Campo_Empleado_Construccion_ID + ", " + Ope_Tal_Listado.Campo_Fecha_Construccion; }
                if (Datos_Listado.P_Estatus == "GENERADA") { Mi_SQL += ", " + Ope_Tal_Listado.Campo_Empleado_Generacion_ID + ", " + Ope_Tal_Listado.Campo_Fecha_Generacion; }
                if (Datos_Listado.P_Estatus == "CANCELADA") { Mi_SQL += ", " + Ope_Tal_Listado.Campo_Empleado_Cancelacion_ID + "', " + Ope_Tal_Listado.Campo_Fecha_Cancelacion; }

                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Listado.Campo_Tipo +
                                ", " + Ope_Tal_Listado.Campo_Comentarios +
                                ", " + Ope_Tal_Listado.Campo_Total + ") " +
                                " VALUES ('" + Datos_Listado.P_Listado_ID +
                                "', '" + Datos_Listado.P_Partida_ID +
                                "', '" + Datos_Listado.P_Proyecto_ID +
                                "', '" + Datos_Listado.P_Folio +    
                                "', GETDATE()" +
                                ", '" + Datos_Listado.P_Estatus +
                                "', '" + Datos_Listado.P_Usuario_ID +
                                "', GETDATE()" +
                                ", '" + Datos_Listado.P_Tipo +
                                "', '" + Datos_Listado.P_Comentarios +
                                "', '" + Datos_Listado.P_Total + "')";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                if (Datos_Listado.P_Productos_Seleccionados.Rows.Count != 0) {
                    for (int i = 0; i < Datos_Listado.P_Productos_Seleccionados.Rows.Count; i++) {
                        Mi_SQL = " INSERT INTO " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle +
                                 " (" + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID +
                                 ", " + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID +
                                 ", " + Ope_Tal_Listado_Detalle.Campo_Cantidad +
                                 ", " + Ope_Tal_Listado_Detalle.Campo_Costo_Compra +
                                 ", " + Ope_Tal_Listado_Detalle.Campo_Importe +
                                 ", " + Ope_Tal_Listado_Detalle.Campo_Monto_IVA +
                                 ", " + Ope_Tal_Listado_Detalle.Campo_Monto_IEPS +
                                 ", " + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IVA +
                                 ", " + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IEPS +
                                 ", " + Ope_Tal_Listado_Detalle.Campo_Usuario_Creo +
                                 ", " + Ope_Tal_Listado_Detalle.Campo_Fecha_Creo +
                                 ")" +
                                 " VALUES ('" + Datos_Listado.P_Listado_ID +
                                 "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["REFACCION_ID"] +
                                 "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["CANTIDAD"] +
                                 "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["PRECIO_UNITARIO"] +
                                 "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["IMPORTE"] +
                                 "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["MONTO_IVA"] +
                                 "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["MONTO_IEPS"] +
                                 "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["PORCENTAJE_IVA"] +
                                 "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["PORCENTAJE_IEPS"] +
                                 "', '" + Cls_Sessiones.Nombre_Empleado +
                                 "', GETDATE()) ";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }//fin del for
                }   
                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
            return "Se dio de alta el listado " + Datos_Listado.P_Folio;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Listado
        ///DESCRIPCIÓN: Metodo que verifica el consecutivo en la tabla y ayuda a generar el nuevo Id. 
        ///PARAMETROS: 1.- Cls_Ope_Tal_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 24/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        //public String Modificar_Listado(Cls_Ope_Tal_Listado_Negocio Datos_Listado)
        //{
        //    String Mi_SQL = "";
        //    String Mensaje="";
        //    //PASO 1
        //    //Obtenemos los datos anteriores del listado para en caso de ser necesario restar el presupuesto y sumar el nuevo
        //    Mi_SQL = "SELECT " + Ope_Tal_Listado.Campo_Listado_ID +
        //             ", " + Ope_Tal_Listado.Campo_Folio +
        //             ", " + Ope_Tal_Listado.Campo_No_Proyecto_ID + 
        //             ", " + Ope_Tal_Listado.Campo_No_Partida_ID +
        //             ", " + Ope_Tal_Listado.Campo_Tipo +
        //             ", " + Ope_Tal_Listado.Campo_Estatus +
        //             ", " + Ope_Tal_Listado.Campo_Total +
        //             " FROM " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
        //             " WHERE " + Ope_Tal_Listado.Campo_Folio + " ='" + Datos_Listado.P_Folio + "'";

        //    DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        //    //Guardamos los valores de total para restarcelo al presupuesto 
        //   // double Presupuesto_Anterior = double.Parse(Data_Table.Rows[0][Ope_Tal_Listado.Campo_Total].ToString());
        //    //String Estatus_Anterior = Data_Table.Rows[0][Ope_Tal_Listado.Campo_Estatus].ToString();
        //    //ASIGNAMOS EL valor de Listado_Id para tenerlo siempre 
        //    Datos_Listado.P_Listado_ID = Data_Table.Rows[0][Ope_Tal_Listado.Campo_Listado_ID].ToString();
        //    //los unicos datos que se pueden modificar en un listado son el estatus, el tipo, el total, el comentario
        //    Mi_SQL = "UPDATE " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
        //                    " SET " + Ope_Tal_Listado.Campo_Estatus +
        //                    " = '" + Datos_Listado.P_Estatus +
        //                    "', " + Ope_Tal_Listado.Campo_Tipo +
        //                    " = '" + Datos_Listado.P_Tipo +
        //                    "', " + Ope_Tal_Listado.Campo_Total +
        //                    " = '" + Datos_Listado.P_Total + "'";
        //    //Dependiendo del estatus se actualiza el id del empleado que cambio el estatus y la fecha
        //    if (Datos_Listado.P_Estatus == "EN CONSTRUCCION")
        //    {
        //        Mi_SQL += ", " + Ope_Tal_Listado.Campo_Empleado_Construccion_ID +
        //                    " = '" + Datos_Listado.P_Usuario_ID + "'" +
        //                    ", " + Ope_Tal_Listado.Campo_Fecha_Construccion +
        //                    " = GETDATE()";
        //    }
        //    if (Datos_Listado.P_Estatus == "GENERADA")
        //    {
        //        Mi_SQL += ", " + Ope_Tal_Listado.Campo_Empleado_Generacion_ID +
        //                    " = '" + Datos_Listado.P_Usuario_ID + "'" +
        //                    ", " + Ope_Tal_Listado.Campo_Fecha_Generacion +
        //                    " = GETDATE()";
        //        //Modificamos el presupuesto asignado para este listado
        //        //Modificar_Presupuestos(Datos_Listado);
        //    }
        //    if (Datos_Listado.P_Estatus == "CANCELADA")
        //    {
        //        Mi_SQL += ", " + Ope_Tal_Listado.Campo_Empleado_Cancelacion_ID +
        //                    " = '" + Datos_Listado.P_Usuario_ID + "'" +
        //                    ", " + Ope_Tal_Listado.Campo_Fecha_Cancelacion +
        //                    " = GETDATE()";
        //    }
        //    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Listado.Campo_Listado_ID +
        //                    " = '" + Datos_Listado.P_Listado_ID + "'";
        //    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        //    //###################################
        //    if (Datos_Listado.P_Estatus != "CANCELADA")
        //    {
        //    //PASO 2
        //    //Consultamos los productos anteriores para ver si a se modificaron los productos del Listado 
        //    Mi_SQL = "SELECT * FROM " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle +
        //             " WHERE " + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID +
        //             "='" + Datos_Listado.P_Listado_ID + "'";
        //    DataTable Productos_Anteriores = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            
        //        //Recorremos el listado de los productos anteriores y lo comparamos con el nuevo si no lo encuentra lo elimina
        //        bool existe_producto = false;
        //        for (int i = 0; i < Productos_Anteriores.Rows.Count; i++)
        //        {

        //            for (int j = 0; j < Datos_Listado.P_Productos_Seleccionados.Rows.Count; j++)
        //            {
        //                if ((Productos_Anteriores.Rows[i][Ope_Tal_Listado_Detalle.Campo_No_Producto_ID].ToString()) == (Datos_Listado.P_Productos_Seleccionados.Rows[j][0].ToString()))
        //                {
        //                    existe_producto = true;
        //                    Mi_SQL = "UPDATE " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle +
        //                         " SET " + Ope_Tal_Listado_Detalle.Campo_Cantidad +
        //                         " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j]["CANTIDAD"] + "'" +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Costo_Compra +
        //                         " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j]["PRECIO_UNITARIO"] + "'" +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Importe +
        //                         " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j]["IMPORTE"] + "'" +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Monto_IVA +
        //                         " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j]["MONTO_IVA"] + "'" +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Monto_IEPS +
        //                         " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j]["MONTO_IEPS"] + "'" +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IVA +
        //                         " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j]["PORCENTAJE_IVA"] + "'" +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IEPS +
        //                         " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j]["PORCENTAJE_IEPS"] + "'" +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Usuario_Modifico +
        //                         " ='" + Datos_Listado.P_Usuario + "'" +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Fecha_Modifico +
        //                         "= GETDATE()" +
        //                         " WHERE " + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID +
        //                         " ='" + Datos_Listado.P_Listado_ID + "'" +
        //                         " AND " + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID +
        //                         " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j][0] + "'";
        //                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        //                    break;
        //                }
        //                else
        //                    existe_producto = false;
        //            }//fin for j

        //            //en caso de no existir el producto se elimina de la base de datos
        //            if (existe_producto == false)
        //            {
        //                Mi_SQL = "DELETE FROM " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle +
        //                         " WHERE " + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID +
        //                         "='" + Productos_Anteriores.Rows[i][Ope_Tal_Listado_Detalle.Campo_No_Producto_ID].ToString() + "'" +
        //                         " AND " + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID +
        //                         "='" + Datos_Listado.P_Listado_ID + "'";
        //                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        //            }//Fin del if

        //        }//fin for i
        //        //AGREGAMOS LOS NUEVOS PRODUCTOS 
        //        //ahora recorremos los dos data table pero a la inversa en busca de nuevos productos agregados 
        //        existe_producto = true;
        //        for (int i = 0; i < Datos_Listado.P_Productos_Seleccionados.Rows.Count; i++)
        //        {
        //            for (int j = 0; j < Productos_Anteriores.Rows.Count; j++)
        //            {
        //                if ((Productos_Anteriores.Rows[j][Ope_Tal_Listado_Detalle.Campo_No_Producto_ID].ToString()) == (Datos_Listado.P_Productos_Seleccionados.Rows[i][0].ToString()))
        //                {
        //                    existe_producto = true;
        //                    break;
        //                }
        //                else
        //                    existe_producto = false;
        //            }//fin for j 
        //            //Insertamos el nuevo producto agregado
        //            if (existe_producto == false)
        //            {
        //                Mi_SQL = " INSERT INTO " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle +
        //                         " (" + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Cantidad +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Costo_Compra +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Importe +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Monto_IVA +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Monto_IEPS +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IVA +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IEPS +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Usuario_Creo +
        //                         ", " + Ope_Tal_Listado_Detalle.Campo_Fecha_Creo +

        //                         ")" +
        //                         " VALUES ('" + Datos_Listado.P_Listado_ID +
        //                         "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i][0] +
        //                         "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["CANTIDAD"] +
        //                         "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["PRECIO_UNITARIO"] +
        //                         "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["IMPORTE"] +
        //                         "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["MONTO_IVA"] +
        //                         "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["MONTO_IEPS"] +
        //                         "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["PORCENTAJE_IVA"] +
        //                         "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["PORCENTAJE_IEPS"] +
        //                         "', '" + Datos_Listado.P_Usuario +
        //                         "', GETDATE()) ";
        //                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        //            }
        //        }//fin for i


        //        //AHORA MODIFICAMOS EL PRESUPUESTO RESTANDO EL ANTERIOR DEL COMPROMETIDO Y ASIGNANDOLE EL NUEVO
        //        //Modificar_Presupuestos(Datos_Listado);
        //        //Y al final se modifica el nuevo presupuesto
        //        Mi_SQL = "UPDATE " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
        //                " SET " + Ope_Tal_Listado.Campo_Total +
        //                " = '" + Datos_Listado.P_Total + "'" +
        //                " WHERE " + Ope_Tal_Listado.Campo_Listado_ID +
        //                " = '" + Datos_Listado.P_Listado_ID + "'";
        //        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        //        Mensaje = "Se modifico satisfactoriamente el Listado de Almacen " + Datos_Listado.P_Folio;
        //    }
        //    else
        //    {
        //        //Cancelamos el presupuesto asignado 
        //        //Liberar_Presupuesto_Cancelada(Datos_Listado);
        //        Mensaje = "Se cancelo satisfactoriamente el listado";
        //    }

        //    return Mensaje;


        //}//Fin de Modificar_Listado

        public String Modificar_Listado(Cls_Ope_Tal_Listado_Negocio Datos_Listado) { 
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {
                String Mi_SQL = "";
                //PASO 1
                //Obtenemos los datos anteriores del listado para en caso de ser necesario restar el presupuesto y sumar el nuevo
                Mi_SQL = "SELECT " + Ope_Tal_Listado.Campo_Listado_ID +
                         ", " + Ope_Tal_Listado.Campo_Folio +
                         ", " + Ope_Tal_Listado.Campo_No_Proyecto_ID +
                         ", " + Ope_Tal_Listado.Campo_No_Partida_ID +
                         ", " + Ope_Tal_Listado.Campo_Tipo +
                         ", " + Ope_Tal_Listado.Campo_Estatus +
                         ", " + Ope_Tal_Listado.Campo_Total +
                         " FROM " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
                         " WHERE " + Ope_Tal_Listado.Campo_Folio + " ='" + Datos_Listado.P_Folio + "'";

                DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                Datos_Listado.P_Listado_ID = Data_Table.Rows[0][Ope_Tal_Listado.Campo_Listado_ID].ToString();

                Mi_SQL = "UPDATE " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
                                " SET " + Ope_Tal_Listado.Campo_Estatus +
                                " = '" + Datos_Listado.P_Estatus +
                                "', " + Ope_Tal_Listado.Campo_Tipo +
                                " = '" + Datos_Listado.P_Tipo +
                                "', " + Ope_Tal_Listado.Campo_Total +
                                " = '" + Datos_Listado.P_Total + "'";
                if (Datos_Listado.P_Estatus == "EN CONSTRUCCION") {
                    Mi_SQL += ", " + Ope_Tal_Listado.Campo_Empleado_Construccion_ID + " = '" + Datos_Listado.P_Usuario_ID + "'" +
                                ", " + Ope_Tal_Listado.Campo_Fecha_Construccion + " = GETDATE()";
                }
                if (Datos_Listado.P_Estatus == "GENERADA") {
                    Mi_SQL += ", " + Ope_Tal_Listado.Campo_Empleado_Generacion_ID + " = '" + Datos_Listado.P_Usuario_ID + "'" +
                                ", " + Ope_Tal_Listado.Campo_Fecha_Generacion + " = GETDATE()";
                    //Modificamos el presupuesto asignado para este listado
                    //Modificar_Presupuestos(Datos_Listado);
                }
                if (Datos_Listado.P_Estatus == "CANCELADA") {
                    Mi_SQL += ", " + Ope_Tal_Listado.Campo_Empleado_Cancelacion_ID + " = '" + Datos_Listado.P_Usuario_ID + "'" +
                                ", " + Ope_Tal_Listado.Campo_Fecha_Cancelacion + " = GETDATE()";
                }
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Listado.Campo_Listado_ID + " = '" + Datos_Listado.P_Listado_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                if (Datos_Listado.P_Estatus != "CANCELADA") {

                    Mi_SQL = "SELECT * FROM " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle + " WHERE " + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID + " = '" + Datos_Listado.P_Listado_ID + "'";
                    DataTable Productos_Anteriores = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                    bool existe_producto = false;
                    for (int i = 0; i < Productos_Anteriores.Rows.Count; i++) {
                        for (int j = 0; j < Datos_Listado.P_Productos_Seleccionados.Rows.Count; j++) {
                            if ((Productos_Anteriores.Rows[i][Ope_Tal_Listado_Detalle.Campo_No_Producto_ID].ToString()) == (Datos_Listado.P_Productos_Seleccionados.Rows[j][0].ToString())) {
                                existe_producto = true;
                                Mi_SQL = "UPDATE " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle +
                                     " SET " + Ope_Tal_Listado_Detalle.Campo_Cantidad +
                                     " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j]["CANTIDAD"] + "'" +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Costo_Compra +
                                     " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j]["PRECIO_UNITARIO"] + "'" +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Importe +
                                     " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j]["IMPORTE"] + "'" +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Monto_IVA +
                                     " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j]["MONTO_IVA"] + "'" +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Monto_IEPS +
                                     " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j]["MONTO_IEPS"] + "'" +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IVA +
                                     " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j]["PORCENTAJE_IVA"] + "'" +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IEPS +
                                     " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j]["PORCENTAJE_IEPS"] + "'" +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Usuario_Modifico +
                                     " ='" + Datos_Listado.P_Usuario + "'" +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Fecha_Modifico +
                                     "= GETDATE()" +
                                     " WHERE " + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID +
                                     " ='" + Datos_Listado.P_Listado_ID + "'" +
                                     " AND " + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID +
                                     " ='" + Datos_Listado.P_Productos_Seleccionados.Rows[j][0] + "'";
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                break;
                            }
                            else
                                existe_producto = false;
                        }
                        if (existe_producto == false) {
                            Mi_SQL = "DELETE FROM " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle +
                                     " WHERE " + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID +
                                     "='" + Productos_Anteriores.Rows[i][Ope_Tal_Listado_Detalle.Campo_No_Producto_ID].ToString() + "'" +
                                     " AND " + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID +
                                     "='" + Datos_Listado.P_Listado_ID + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                        } 

                    } 
                    existe_producto = true;
                    for (int i = 0; i < Datos_Listado.P_Productos_Seleccionados.Rows.Count; i++) {
                        for (int j = 0; j < Productos_Anteriores.Rows.Count; j++) {
                            if ((Productos_Anteriores.Rows[j][Ope_Tal_Listado_Detalle.Campo_No_Producto_ID].ToString()) == (Datos_Listado.P_Productos_Seleccionados.Rows[i][0].ToString())) {
                                existe_producto = true;
                                break;
                            }
                            else
                                existe_producto = false;
                        }  
                        if (existe_producto == false) {
                            Mi_SQL = " INSERT INTO " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle +
                                     " (" + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Cantidad +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Costo_Compra +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Importe +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Monto_IVA +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Monto_IEPS +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IVA +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IEPS +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Usuario_Creo +
                                     ", " + Ope_Tal_Listado_Detalle.Campo_Fecha_Creo +

                                     ")" +
                                     " VALUES ('" + Datos_Listado.P_Listado_ID +
                                     "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i][0] +
                                     "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["CANTIDAD"] +
                                     "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["PRECIO_UNITARIO"] +
                                     "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["IMPORTE"] +
                                     "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["MONTO_IVA"] +
                                     "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["MONTO_IEPS"] +
                                     "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["PORCENTAJE_IVA"] +
                                     "', '" + Datos_Listado.P_Productos_Seleccionados.Rows[i]["PORCENTAJE_IEPS"] +
                                     "', '" + Datos_Listado.P_Usuario +
                                     "', GETDATE()) ";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                        }
                    } 

                    Mi_SQL = "UPDATE " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
                            " SET " + Ope_Tal_Listado.Campo_Total + " = '" + Datos_Listado.P_Total + "'" +
                            " WHERE " + Ope_Tal_Listado.Campo_Listado_ID + " = '" + Datos_Listado.P_Listado_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Mensaje = "Se MODIFICO satisfactoriamente el Listado de Almacen " + Datos_Listado.P_Folio;
                } else {
                    Mensaje = "Se CANCELO satisfactoriamente el listado" + Datos_Listado.P_Folio;
                }
                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
            return Mensaje;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Liberar_Presupuesto_Cancelada
        ///DESCRIPCIÓN: Metodo que consulta los productos que se encunentran en reorden 
        ///PARAMETROS: 1.- Cls_Ope_Tal_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 24/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public void Liberar_Presupuesto_Cancelada(Cls_Ope_Tal_Listado_Negocio Datos_Listado)
        {
            //Obtenemos los datos anteriores del listado para en caso de ser necesario restar el presupuesto y sumar el nuevo
            String Mi_SQL = "SELECT " + Ope_Tal_Listado.Campo_Listado_ID +
                     ", " + Ope_Tal_Listado.Campo_Folio +
                     ", " + Ope_Tal_Listado.Campo_No_Proyecto_ID +
                     ", " + Ope_Tal_Listado.Campo_No_Partida_ID +
                     ", " + Ope_Tal_Listado.Campo_Tipo +
                     ", " + Ope_Tal_Listado.Campo_Estatus +
                     ", " + Ope_Tal_Listado.Campo_Total +
                     " FROM " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
                     " WHERE " + Ope_Tal_Listado.Campo_Listado_ID + " ='" + Datos_Listado.P_Listado_ID + "'";

            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            //Como ya se obtuvieron los detalles d la licitacion antes de ser modificada
            //Usamos los datos del monto y la partida para liberar presupuestos correspondientes
            double Monto = double.Parse(Data_Table.Rows[0][Ope_Tal_Listado.Campo_Total].ToString());
            String Partida = Data_Table.Rows[0][Ope_Tal_Listado.Campo_No_Partida_ID].ToString();
            String Programa = Data_Table.Rows[0][Ope_Tal_Listado.Campo_No_Proyecto_ID].ToString();

          
            //liberamos el presupuesto que era para este listado
            //se ACTUALIZAN LOS PRESUPUESTOS DE LA DEPENDENCIA, CORRESPONDIENTE AL PROYECTO Y PARTIDA

            Mi_SQL = "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible +
                " =" + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " + " + Monto +
                "," + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido +
                "=" + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " - " + Monto +
                " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                "='" + Partida + "'" +
                " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                "='" + Programa + "'" +
                " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio +
                " = (SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
                " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                "='" + Partida + "'" +
                " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                "='" + Programa + "'" +
                " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                "= YEAR(GETDATE()))" +
                " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                "= YEAR(GETDATE())";
            //Sentencia que ejecuta el query
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            //ACTUALIZAMOS LOS PRESUPUESTOS DE LA PARTIDA
            //Mi_SQL = "UPDATE " + Ope_Com_Pres_Partida.Tabla_Ope_Com_Pres_Partida +
            //    " SET " + Ope_Com_Pres_Partida.Campo_Monto_Disponible +
            //    " =" + Ope_Com_Pres_Partida.Campo_Monto_Disponible + " + " + Monto +
            //    "," + Ope_Com_Pres_Partida.Campo_Monto_Comprometido +
            //    "=" + Ope_Com_Pres_Partida.Campo_Monto_Comprometido + " - " + Monto +
            //    " WHERE " + Ope_Com_Pres_Partida.Campo_Partida_ID +
            //    "='" + Partida + "'" +
            //    " AND " + Ope_Com_Pres_Partida.Campo_Anio_Presupuesto +
            //    "= TO_CHAR(GETDATE(),'YYYY')";
            //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            ////ACUTUALIZAMOS LOS PRESUPUESTOS DEL PROYECTO
            //Mi_SQL = "UPDATE " + Ope_Com_Pres_Prog_Proy.Tabla_Ope_Com_Pres_Prog_Proy +
            //    " SET " + Ope_Com_Pres_Prog_Proy.Campo_Monto_Disponible +
            //    " =" + Ope_Com_Pres_Prog_Proy.Campo_Monto_Disponible + " + " + Monto +
            //    "," + Ope_Com_Pres_Prog_Proy.Campo_Monto_Comprometido +
            //    "=" + Ope_Com_Pres_Prog_Proy.Campo_Monto_Comprometido + " - " + Monto +
            //    " WHERE " + Ope_Com_Pres_Prog_Proy.Campo_Pres_Prog_Proy_ID +
            //    "='" + Programa + "'" +
            //    " AND " + Ope_Com_Pres_Prog_Proy.Campo_Anio_Presupuesto +
            //    "= TO_CHAR(GETDATE(),'YYYY')";
            //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);


        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Presupuestos
        ///DESCRIPCIÓN: Metodo que modifica los presupuestos en caso de tener monto disponible 
        ///PARAMETROS: 1.- Cls_Ope_Tal_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 08/Febrero/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public bool Modificar_Presupuestos(Cls_Ope_Tal_Listado_Negocio Datos_Listado)
        {
            DataTable Data_Table = Consultar_Presupuesto_Partidas(Datos_Listado);
            String No_Asignacion_Anio = Data_Table.Rows[0][Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio].ToString();
            double Monto_Disponible = double.Parse(Data_Table.Rows[0][Ope_Com_Pres_Partida.Campo_Monto_Disponible].ToString());
            double Monto_Total = double.Parse(Datos_Listado.P_Total);
            String Mi_SQL = "";
            bool Existe_Presupuesto = false;

            if (Monto_Disponible >= Monto_Total)
            {
            
                //se ACTUALIZAN LOS PRESUPUESTOS DE LA DEPENDENCIA, CORRESPONDIENTE AL PROYECTO Y PARTIDA

                Mi_SQL = "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                    " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible +
                    " =" + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " - " + Monto_Total +
                    "," + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido +
                    "=" + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " + " + Monto_Total +
                    " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID + "= (SELECT " + Cat_Com_Parametros.Campo_Partida_Esp_Almacen_Global +
                            " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + ")" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID + "=(SELECT " + Cat_Com_Parametros.Campo_Programa_Almacen +
                            " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + ")" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + "=(SELECT " + Cat_Com_Parametros.Campo_Dependencia_ID_Almacen +
                            " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + ")" +
                    " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio +
                    " = '" + No_Asignacion_Anio + "'" +
                    " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                    "= TO_CHAR(GETDATE(),'YYYY')";
                //Sentencia que ejecuta el query
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                ////ACTUALIZAMOS LOS PRESUPUESTOS DE LA PARTIDA
                //Mi_SQL = "UPDATE " + Ope_Com_Pres_Partida.Tabla_Ope_Com_Pres_Partida +
                //    " SET " + Ope_Com_Pres_Partida.Campo_Monto_Disponible +
                //    " =" + Ope_Com_Pres_Partida.Campo_Monto_Disponible + " - " + Monto_Total +
                //    "," + Ope_Com_Pres_Partida.Campo_Monto_Comprometido +
                //    "=" + Ope_Com_Pres_Partida.Campo_Monto_Comprometido + " + " + Monto_Total +
                //    " WHERE " + Ope_Com_Pres_Partida.Campo_Partida_ID +
                //    "='" + Datos_Listado.P_Partida_ID + "'" +
                //    " AND " + Ope_Com_Pres_Partida.Campo_Anio_Presupuesto +
                //    "= TO_CHAR(GETDATE(),'YYYY')";
                //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                ////ACUTUALIZAMOS LOS PRESUPUESTOS DEL PROYECTO
                //Mi_SQL = "UPDATE " + Ope_Com_Pres_Prog_Proy.Tabla_Ope_Com_Pres_Prog_Proy +
                //    " SET " + Ope_Com_Pres_Prog_Proy.Campo_Monto_Disponible +
                //    " =" + Ope_Com_Pres_Prog_Proy.Campo_Monto_Disponible + " + " + Monto_Total +
                //    "," + Ope_Com_Pres_Prog_Proy.Campo_Monto_Comprometido +
                //    "=" + Ope_Com_Pres_Prog_Proy.Campo_Monto_Comprometido + " - " + Monto_Total +
                //    " WHERE " + Ope_Com_Pres_Prog_Proy.Campo_Pres_Prog_Proy_ID +
                //    "='" + Datos_Listado.P_Proyecto_ID + "'" +
                //    " AND " + Ope_Com_Pres_Prog_Proy.Campo_Anio_Presupuesto +
                //    "= TO_CHAR(GETDATE(),'YYYY')";
                //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                Existe_Presupuesto = true;
            }
            else
            {
                Existe_Presupuesto = false;

            }
            return Existe_Presupuesto;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Afectar_Presupuesto
        ///DESCRIPCIÓN: Metodo que afecta el presupuesto ya ocupado.  
        ///PARAMETROS: 1.- Cls_Ope_Tal_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 24/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************

        public bool Afectar_Presupuesto(Cls_Ope_Tal_Listado_Negocio Datos_Listado)
        {
            //variable que indica si afecta o no el presupuesto
            bool Afecta_Presupuesto = false;
            bool Suma_Diferencia = false;
            //consultamos el total anterior al cual ya se le asigno presupuesto
            String Mi_SQL = "SELECT " + Ope_Tal_Listado.Campo_Total +
                " FROM " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado + 
                " WHERE " + Ope_Tal_Listado.Campo_Listado_ID + 
                "='" +Datos_Listado.P_Listado_ID + "'";
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            double Total_Ultimo_Presupuestado = double.Parse(Data_Table.Rows[0][Ope_Tal_Listado.Campo_Total].ToString());
            
            //PASO 1
            //Verificamos si existe alguna diferencia de los totales el anterior ya presupuestado y el nuevo 
            // PASO 1 VERIFICAMOS CUAL DE LOS 2 MONTOS ES MAYOR SI EL COTIZADO O  EL ANTERIOR

            //Variable del Monto nuevo a cotizar
            double Total_Nuevo_A_Presupuestar = double.Parse(Datos_Listado.P_Total);
            //VAriable del Diferencia de los montos el cual se cotizara o no 
            double Diferencia = 0;
            //Variable que indica si existe una diferencia ositiva a la cual se tiene que solicitar mas 
            if (Total_Nuevo_A_Presupuestar > Total_Ultimo_Presupuestado)
            {
                //Obtenemos la resta
                Diferencia = Total_Nuevo_A_Presupuestar - Total_Ultimo_Presupuestado;
                Suma_Diferencia = true;
            }
            if (Total_Nuevo_A_Presupuestar < Total_Ultimo_Presupuestado)
            {
                //obtener resta
                Diferencia = Total_Ultimo_Presupuestado - Total_Nuevo_A_Presupuestar;
                Suma_Diferencia = false;
            }

            //PASO 2 AFECTAMOS PRESUPUESTO
            //Consultamos el presupuesto que aun se tiene
            DataTable Data_Partida = Consultar_Presupuesto_Partidas(Datos_Listado);
            double Presupuesto_Disponible = double.Parse(Data_Partida.Rows[0][Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible].ToString());
            String No_Asignacion_Anio = Data_Partida.Rows[0][Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio].ToString();
            //MODIFICAMOS LOS PRESUPUESTOS DE ACUERDO AL CASO EN EL QUE ENTRE EL MONTO RESTANTE DE LO COTIZADO 
            //Es true cuando necesitamos pedir mas presupuesto
            //Es false si sobra dinero, osea que se necesita liberar presupuesto ps este presupuesto sobro
            if (Suma_Diferencia == true)
            {
                if (Diferencia < Presupuesto_Disponible)
                {
                    //se ACTUALIZAN LOS PRESUPUESTOS DE LA DEPENDENCIA, CORRESPONDIENTE AL PROYECTO Y PARTIDA

                    Mi_SQL = "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                        " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible +
                        " =" + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " - " + Diferencia.ToString() +
                        "," + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido +
                        "=" + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " + " + Diferencia.ToString() +
                        " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID + "= (SELECT " + Cat_Com_Parametros.Campo_Partida_Esp_Almacen_Global +
                            " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + ")" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID + "=(SELECT " + Cat_Com_Parametros.Campo_Programa_Almacen +
                            " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + ")" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + "=(SELECT " + Cat_Com_Parametros.Campo_Dependencia_ID_Almacen +
                            " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + ")" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto + "= TO_CHAR(GETDATE(),'YYYY')" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio +
                            "= " + No_Asignacion_Anio + "'" +
                            
                    //Sentencia que ejecuta el query
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    Afecta_Presupuesto = true;    
                 

                }
                else
                {
                    //Si no existe presupuesto modificamos Mandamos hacemos negativa la variable de afectar_Presupuesto, para porsteriormente realizar validaciones 
                    Afecta_Presupuesto = false;                  
                }

            }//fin if SumaDiferencia
            else
            {
                //Modificamos el presupuesto, ya que se resta el monto que sobro pues el valor Total nuevo de la licitacion es menor k el anterior
                //se ACTUALIZAN LOS PRESUPUESTOS DE LA DEPENDENCIA, CORRESPONDIENTE AL PROYECTO Y PARTIDA DE ALMACEN

                Mi_SQL = "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                    " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible +
                    " =" + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " + " + Diferencia.ToString() +
                    "," + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido +
                    "=" + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " - " + Diferencia.ToString() +
                    " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID + "= (SELECT " + Cat_Com_Parametros.Campo_Partida_Esp_Almacen_Global +
                            " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + ")" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID + "=(SELECT " + Cat_Com_Parametros.Campo_Programa_Almacen +
                            " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + ")" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + "=(SELECT " + Cat_Com_Parametros.Campo_Dependencia_ID_Almacen +
                            " FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros + ")" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto + "= TO_CHAR(GETDATE(),'YYYY')" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + "='" +No_Asignacion_Anio + "'";
                //Sentencia que ejecuta el query
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                Afecta_Presupuesto = true;
               

            }


            //En caso de ser falso la variable de Afectar_Presupuesto es por k no existe presupuesto 


            return Afecta_Presupuesto;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Productos_Reorden
        ///DESCRIPCIÓN: Metodo que consulta los productos que se encunentran en reorden 
        ///PARAMETROS: 1.- Cls_Ope_Tal_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 24/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************

        public DataTable Consultar_Productos_Reorden(Cls_Ope_Tal_Listado_Negocio Datos_Listado)
        {
            String Mi_SQL = "";

            if (Datos_Listado.P_Tipo == "AUTOMATICO")
            {
                Mi_SQL = "SELECT PRODUCTOS." + Cat_Tal_Refacciones.Campo_Refaccion_ID +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Clave +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Nombre + " AS PRODUCTO_NOMBRE" +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Descripcion +
                         ", (SELECT " + Cat_Com_Unidades.Campo_Nombre + 
                         " FROM " +Cat_Com_Unidades.Tabla_Cat_Com_Unidades +
                         " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID + 
                         "= PRODUCTOS." + Cat_Tal_Refacciones.Campo_Unidad_ID + ") AS UNIDAD"+
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Existencia +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Reorden +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Maximo + "-" +
                         " PRODUCTOS." + Cat_Tal_Refacciones.Campo_Existencia + " AS CANTIDAD" +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Costo_Unitario + " AS PRECIO_UNITARIO" +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Impuesto_ID +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Impuesto_2_ID +
                         ",(SELECT " + Cat_Com_Impuestos.Campo_Nombre +
                         " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                         " WHERE PRODUCTOS." + Cat_Tal_Refacciones.Campo_Impuesto_ID + "=" +
                         Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS TIPO_IMPUESTO_1 " +
                         ",(SELECT " + Cat_Com_Impuestos.Campo_Nombre +
                         " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                         " WHERE PRODUCTOS." + Cat_Tal_Refacciones.Campo_Impuesto_2_ID + "=" +
                         Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS TIPO_IMPUESTO_2 " +
                         ",(SELECT " + Cat_Com_Impuestos.Campo_Porcentaje_Impuesto +
                         " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                         " WHERE PRODUCTOS." + Cat_Tal_Refacciones.Campo_Impuesto_ID + "=" +
                         Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS IMPUESTO_PORCENTAJE_1 " +
                         ",(SELECT " + Cat_Com_Impuestos.Campo_Porcentaje_Impuesto +
                         " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                         " WHERE PRODUCTOS." + Cat_Tal_Refacciones.Campo_Impuesto_2_ID + "=" +
                         Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS IMPUESTO_PORCENTAJE_2 " +
                         " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTOS" +
                         " JOIN " + Cat_Com_Impuestos.Tabla_Cat_Impuestos + " IMPUESTOS" +
                         " ON IMPUESTOS." + Cat_Com_Impuestos.Campo_Impuesto_ID + "= PRODUCTOS." + Cat_Tal_Refacciones.Campo_Impuesto_ID +
                         " WHERE PRODUCTOS." + Cat_Tal_Refacciones.Campo_Tipo + " = 'STOCK'" +
                         " AND PRODUCTOS." + Cat_Tal_Refacciones.Campo_Partida_ID + "='" + Datos_Listado.P_Partida_ID + "'" +
                         " AND ((PRODUCTOS." + Cat_Tal_Refacciones.Campo_Existencia + "<=" +
                         " PRODUCTOS." + Cat_Tal_Refacciones.Campo_Reorden +
                         ") OR (PRODUCTOS." + Cat_Tal_Refacciones.Campo_Existencia + "<=" +
                         " PRODUCTOS." + Cat_Tal_Refacciones.Campo_Minimo +
                         ")) AND PRODUCTOS." + Cat_Tal_Refacciones.Campo_Estatus + "='VIGENTE'" + 
                         " AND PRODUCTOS." + Cat_Tal_Refacciones.Campo_Maximo + "> 0" +
                         " AND PRODUCTOS." +Cat_Tal_Refacciones.Campo_Maximo + " - " +
                         " PRODUCTOS." + Cat_Tal_Refacciones.Campo_Disponible + " > 0"+
                         " ORDER BY PRODUCTOS." + Cat_Tal_Refacciones.Campo_Nombre;

            }

            if (Datos_Listado.P_Producto_ID != null)
            {

                Mi_SQL = "SELECT PRODUCTOS." + Cat_Tal_Refacciones.Campo_Refaccion_ID + 
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Clave +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Nombre + " AS PRODUCTO_NOMBRE" +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Descripcion +
                         ", (SELECT " + Cat_Com_Unidades.Campo_Nombre +
                         " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades +
                         " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID +
                         "=PRODUCTOS." + Cat_Tal_Refacciones.Campo_Unidad_ID + ") AS UNIDAD" +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Existencia +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Reorden +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Maximo + "-" +
                         " PRODUCTOS." + Cat_Tal_Refacciones.Campo_Disponible + " AS CANTIDAD" +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Costo_Unitario + " AS PRECIO_UNITARIO" +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Impuesto_ID +
                         ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Impuesto_2_ID +
                         ",(SELECT " + Cat_Com_Impuestos.Campo_Nombre +
                         " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                         " WHERE PRODUCTOS." + Cat_Tal_Refacciones.Campo_Impuesto_ID + "=" +
                         Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS TIPO_IMPUESTO_1 " +
                         ",(SELECT " + Cat_Com_Impuestos.Campo_Nombre +
                         " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                         " WHERE PRODUCTOS." + Cat_Tal_Refacciones.Campo_Impuesto_2_ID + "=" +
                         Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS TIPO_IMPUESTO_2 " +
                         ",(SELECT " + Cat_Com_Impuestos.Campo_Porcentaje_Impuesto +
                         " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                         " WHERE PRODUCTOS." + Cat_Tal_Refacciones.Campo_Impuesto_ID + "=" +
                         Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS IMPUESTO_PORCENTAJE_1 " +
                         ",(SELECT " + Cat_Com_Impuestos.Campo_Porcentaje_Impuesto +
                         " FROM " + Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                         " WHERE PRODUCTOS." + Cat_Tal_Refacciones.Campo_Impuesto_2_ID + "=" +
                         Cat_Com_Impuestos.Campo_Impuesto_ID + ") AS IMPUESTO_PORCENTAJE_2 " +
                         " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTOS" +
                         " JOIN " + Cat_Com_Impuestos.Tabla_Cat_Impuestos + " IMPUESTOS" +
                         " ON IMPUESTOS." + Cat_Com_Impuestos.Campo_Impuesto_ID + "= PRODUCTOS." + Cat_Tal_Refacciones.Campo_Impuesto_ID +
                         " WHERE PRODUCTOS." + Cat_Tal_Refacciones.Campo_Refaccion_ID + "='" +
                         Datos_Listado.P_Producto_ID + "'";

            }


            if (Datos_Listado.P_Folio != null)
            {
                Mi_SQL = "SELECT LISTADO." + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID + " AS REFACCION_ID" +
                         ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Clave +
                         ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Nombre + " AS PRODUCTO_NOMBRE" +
                         ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Descripcion +
                         ", (SELECT " + Cat_Com_Unidades.Campo_Nombre +
                         " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades +
                         " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID +
                         "=PRODUCTO." + Cat_Tal_Refacciones.Campo_Unidad_ID + ") AS UNIDAD" +
                         ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Existencia +
                         ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Reorden +
                         ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Cantidad +
                         ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Costo_Compra + " AS PRECIO_UNITARIO" +
                         ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Importe +
                         ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Monto_IVA +
                         ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Monto_IEPS +
                         ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IVA +
                         ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IEPS +
                         " FROM " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle + " LISTADO" +
                         " JOIN " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTO" +
                         " ON PRODUCTO." + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = LISTADO." + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID +
                         " WHERE LISTADO." + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID + " = '" + Datos_Listado.P_Listado_ID + "'" +
                         " AND PRODUCTO." + Cat_Tal_Refacciones.Campo_Estatus + "='VIGENTE'" +
                         " ORDER BY PRODUCTO." + Cat_Tal_Refacciones.Campo_Nombre;
            }
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }
      
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Modelos
        ///DESCRIPCIÓN: Metodo que consulta de los modelos en existencia de los productos
        ///PARAMETROS: 1.- Cls_Ope_Tal_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 02/Marzo/11
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Consultar_Modelos(Cls_Ope_Tal_Listado_Negocio Datos_Listado)
        {
            String Mi_SQL = "SELECT " + Cat_Com_Modelos.Campo_Modelo_ID +
                            ", " + Cat_Com_Modelos.Campo_Nombre +
                            " FROM " + Cat_Com_Modelos.Tabla_Cat_Com_Modelos +
                            " ORDER BY " + Cat_Com_Subfamilias.Campo_Nombre;
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Productos
        ///DESCRIPCIÓN: Metodo que consulta de los modelos en existencia de los productos
        ///PARAMETROS: 1.- Cls_Ope_Tal_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 02/Marzo/11
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public DataTable Consultar_Productos(Cls_Ope_Tal_Listado_Negocio Datos_Listado)
        {
            String Mi_SQL = "";

            Mi_SQL = "SELECT PRODUCTO." + Cat_Tal_Refacciones.Campo_Clave + ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Refaccion_ID +
                 " AS PRODUCTO_SERVICIO_ID, PRODUCTO." + Cat_Tal_Refacciones.Campo_Nombre +
                 " AS PRODUCTO_SERVICIO, PRODUCTO." + Cat_Tal_Refacciones.Campo_Descripcion +
                 " AS DESCRIPCION, (SELECT " + Cat_Com_Unidades.Campo_Nombre +
                 " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades +
                 " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID +
                 "=PRODUCTO." + Cat_Tal_Refacciones.Campo_Unidad_ID + ") AS UNIDAD" + 
                 ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Costo_Unitario +
                 " AS PRECIO_UNITARIO, PRODUCTO." + Cat_Tal_Refacciones.Campo_Existencia + " FROM " +
                 Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTO " +
                 " WHERE PRODUCTO." + Cat_Tal_Refacciones.Campo_Tipo + " = 'STOCK'" +
                 " AND PRODUCTO." + Cat_Tal_Refacciones.Campo_Estatus + "='VIGENTE'";
                 
                  
            if (Datos_Listado.P_Partida_ID != null)
            {
                Mi_SQL = Mi_SQL + " AND PRODUCTO." + Cat_Tal_Refacciones.Campo_Partida_ID +
                    " = '" + Datos_Listado.P_Partida_ID + "'";
            }
            if (Datos_Listado.P_Nombre_Producto != null)
            {
                Mi_SQL = Mi_SQL + " AND UPPER(PRODUCTO." + Cat_Tal_Refacciones.Campo_Nombre +
                    ") LIKE UPPER('%" +Datos_Listado.P_Nombre_Producto +"%')";
            }
            Mi_SQL = Mi_SQL + " ORDER BY PRODUCTO." + Cat_Tal_Refacciones.Campo_Nombre;


            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consecutivo
        ///DESCRIPCIÓN: Metodo que verfifica el consecutivo en la tabla y ayuda a generar el nuevo Id. 
        ///PARAMETROS: 
        ///CREO:
        ///FECHA_CREO: 24/Agosto/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Consecutivo(String Campo_ID, String Tabla)
        {
            String Consecutivo = "";
            String Mi_SQL;         //Obtiene la cadena de inserción hacía la base de datos
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos

            Mi_SQL = "SELECT ISNULL(MAX (" + Campo_ID + "),'0000000000') ";
            Mi_SQL = Mi_SQL + "FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            if (Convert.IsDBNull(Obj))
            {
                Consecutivo = "0000000001";
            }
            else
            {
                Consecutivo = string.Format("{0:0000000000}", Convert.ToInt32(Obj) + 1);
            }
            return Consecutivo;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Observaciones
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para dar de alta observaciones
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 10/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public void Alta_Observaciones_Listado(Cls_Ope_Tal_Listado_Negocio Listado_Negocio)
        {
            String Mi_SQL = "INSERT INTO " + Ope_Tal_Obs_Listado.Tabla_Ope_Tal_Obs_Listados +
                            " (" + Ope_Tal_Obs_Listado.Campo_Obs_listado_ID +
                            ", " + Ope_Tal_Obs_Listado.Campo_No_Listado_ID +
                            ", " + Ope_Tal_Obs_Listado.Campo_Comentario +
                            ", " + Ope_Tal_Obs_Listado.Campo_Estatus +
                            ", " + Ope_Tal_Obs_Listado.Campo_Usuario_Creo +
                            ", " + Ope_Tal_Obs_Listado.Campo_Fecha_Creo +
                            ") VALUES ('" + Obtener_Consecutivo(Ope_Tal_Obs_Listado.Campo_Obs_listado_ID, Ope_Tal_Obs_Listado.Tabla_Ope_Tal_Obs_Listados).ToString() + "','" +
                                Listado_Negocio.P_Listado_ID + "','" +
                                Listado_Negocio.P_Comentarios + "','" +
                                Listado_Negocio.P_Estatus + "','" +
                                Listado_Negocio.P_Usuario + "',GETDATE())";
            //Sentencia que ejecuta el query
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

        }//fin de Alta_Observacion

        public DataTable Consultar_Observaciones_Listado(Cls_Ope_Tal_Listado_Negocio Listado_Negocio)
        {
            String Mi_SQL = "SELECT " + Ope_Tal_Obs_Listado.Campo_Comentario +
                            ", " + Ope_Tal_Obs_Listado.Campo_Estatus +
                            ",  REPLACE(CONVERT(VARCHAR(11)," + Ope_Tal_Obs_Listado.Campo_Fecha_Creo + ",106),' ','/') AS FECHA_CREO" +
                            ", " + Ope_Tal_Obs_Listado.Campo_Usuario_Creo +
                            " FROM " + Ope_Tal_Obs_Listado.Tabla_Ope_Tal_Obs_Listados +
                            " WHERE " + Ope_Tal_Obs_Listado.Campo_No_Listado_ID +
                            " = '" + Listado_Negocio.P_Listado_ID + "'";

            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }

        #endregion

        #endregion Fin_Metodos

    }//fin del class
}//fin del namespace