﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Unidades_Responsables_Rol.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Ope_Tal_Unidades_Responsables_Rol_Datos
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Operacion_Unidades_Responsables_Rol.Datos {
    public class Cls_Ope_Tal_Unidades_Responsables_Rol_Datos{
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Actualizar_Listado_UR
        ///DESCRIPCIÓN          : Modifica en la Base de Datos el registro indicado
        ///PARAMETROS           : 
        ///                     1.Parametros. Contiene los parametros que se van hacer la
        ///                       Modificación en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 10/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Actualizar_Listado_UR(Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio Parametros) {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {
                //Eliminamos las UR Anteriores
                String Mi_SQL = "DELETE FROM " + Ope_Tal_Accesos_Dependencias.Tabla_Ope_Tal_Accesos_Dependencias + " WHERE " + Ope_Tal_Accesos_Dependencias.Campo_No_Empleado + " = '" +Parametros.P_No_Empleado.Trim() + "'";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Eliminamos las UR Anteriores
                Mi_SQL = "SELECT ISNULL(MAX("+ Ope_Tal_Accesos_Dependencias.Campo_No_Registro +"),1) FROM " + Ope_Tal_Accesos_Dependencias.Tabla_Ope_Tal_Accesos_Dependencias;

                Cmd.CommandText = Mi_SQL;
                Int64 No_Registro = Convert.ToInt64(Cmd.ExecuteScalar())+1;

                
                foreach (DataRow Fila_Actual in Parametros.P_Dt_Unidades_Responsables.Rows) {
                    Mi_SQL = "INSERT INTO " + Ope_Tal_Accesos_Dependencias.Tabla_Ope_Tal_Accesos_Dependencias;
                    Mi_SQL = Mi_SQL + " (" + Ope_Tal_Accesos_Dependencias.Campo_No_Registro;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Accesos_Dependencias.Campo_No_Empleado;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Accesos_Dependencias.Campo_Dependencia_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Accesos_Dependencias.Campo_Usuario_Creo;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Accesos_Dependencias.Campo_Fecha_Creo;
                    Mi_SQL = Mi_SQL + " ) VALUES ('" + No_Registro + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Empleado.Trim() + "'";
                    Mi_SQL = Mi_SQL + ", '" + Fila_Actual["DEPENDENCIA_ID"].ToString().Trim() +  "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario +  "'";
                    Mi_SQL = Mi_SQL + ", GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    No_Registro = No_Registro + 1;
                }

                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar Modificar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Listado_UR
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 30/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Listado_UR(Cls_Ope_Tal_Unidades_Responsables_Rol_Negocio Parametros)  {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try {
                Mi_SQL = "SELECT ACCESOS." + Ope_Tal_Accesos_Dependencias.Campo_Dependencia_ID + " AS DEPENDENCIA_ID";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + " AS CLAVE";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Comentarios + " AS COMENTARIOS";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Accesos_Dependencias.Tabla_Ope_Tal_Accesos_Dependencias + " ACCESOS";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON ACCESOS." + Ope_Tal_Accesos_Dependencias.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                if (Parametros.P_Estatus != null && Parametros.P_Estatus.Trim().Length > 0) {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " DEPENDENCIAS." + Cat_Dependencias.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                if (Parametros.P_No_Empleado != null && Parametros.P_No_Empleado.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " ACCESOS." + Ope_Tal_Accesos_Dependencias.Campo_No_Empleado + " IN ('" + Parametros.P_No_Empleado.Trim() + "')";
                }
                Mi_SQL = Mi_SQL + " ORDER BY CLAVE_NOMBRE";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null) {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            } catch (Exception Ex) {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Listado_UR
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 30/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Consultar_Accesos_UR(string No_Empleado)
        {
            String Mi_SQL = null;
            String Resultado_Acceso = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Resultado_Acceso = Sessiones.Cls_Sessiones.Dependencia_ID_Empleado;
                Mi_SQL = "SELECT ACCESOS." + Ope_Tal_Accesos_Dependencias.Campo_Dependencia_ID + " AS DEPENDENCIA_ID";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + " AS CLAVE";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Comentarios + " AS COMENTARIOS";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Accesos_Dependencias.Tabla_Ope_Tal_Accesos_Dependencias + " ACCESOS";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON ACCESOS." + Ope_Tal_Accesos_Dependencias.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";

                if (No_Empleado != null && No_Empleado.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " ACCESOS." + Ope_Tal_Accesos_Dependencias.Campo_No_Empleado + " IN ('" + No_Empleado.Trim() + "')";
                }
                Mi_SQL = Mi_SQL + " ORDER BY CLAVE_NOMBRE";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
                if (Dt_Datos.Rows.Count == 1) 
                {
                    Resultado_Acceso = Dt_Datos.Rows[0]["DEPENDENCIA_ID"].ToString();
                }
                else
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        Resultado_Acceso = "";
                        foreach (DataRow Dr_Contador_Renglones in Dt_Datos.Rows)
                        {
                            Resultado_Acceso += Dr_Contador_Renglones["DEPENDENCIA_ID"].ToString() + "','";
                        }
                        Resultado_Acceso = Resultado_Acceso.Substring(0, Resultado_Acceso.Length - 3);
                    }
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Resultado_Acceso;
        }

	}
}
