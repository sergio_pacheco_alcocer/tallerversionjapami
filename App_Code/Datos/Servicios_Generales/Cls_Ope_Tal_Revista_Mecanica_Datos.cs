﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Revista_Mecanica.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Datos;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Datos;
using JAPAMI.Taller_Mecanico.Operacion_Calendarizacion_Revista_Mecanica.Negocio;
using JAPAMI.Constantes;

/// <summary>
/// Summary description for Cls_Ope_Tal_Revista_Mecanica_Datos
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Operacion_Revista_Mecanica.Datos
{
    public class Cls_Ope_Tal_Revista_Mecanica_Datos
    {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Revista_Mecanica      
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 22/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Revista_Mecanica(Cls_Ope_Tal_Revista_Mecanica_Negocio Parametros) {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Parametros.P_No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Rev_Mecanica.Tabla_Ope_Tal_Rev_Mecanica, Ope_Tal_Rev_Mecanica.Campo_No_Registro, 20));
            try
            {
                String Mi_SQL = "INSERT INTO " + Ope_Tal_Rev_Mecanica.Tabla_Ope_Tal_Rev_Mecanica;
                Mi_SQL = Mi_SQL + " (" + Ope_Tal_Rev_Mecanica.Campo_No_Registro + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_No_Entrada + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Mecanico_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Estatus + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Fecha_Revision_Inicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Vehiculo_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Usuario_Creo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Fecha_Creo + ")";
                Mi_SQL = Mi_SQL + " VALUES (" + Parametros.P_No_Registro + "";
                Mi_SQL = Mi_SQL + ", " + Parametros.P_No_Entrada + "";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Mecanico_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Solicitud + "'";
                Mi_SQL = Mi_SQL + ", GETDATE()";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Vehiculo_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE())";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Revista_Mecanica      
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///                     2.  P_Cmd.      Comando en caso de ser remota la ejecucion final.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 22/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Revista_Mecanica(Cls_Ope_Tal_Revista_Mecanica_Negocio Parametros, ref SqlCommand P_Cmd)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Parametros.P_No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Rev_Mecanica.Tabla_Ope_Tal_Rev_Mecanica, Ope_Tal_Rev_Mecanica.Campo_No_Registro, 20));
            try
            {
                String Mi_SQL = "INSERT INTO " + Ope_Tal_Rev_Mecanica.Tabla_Ope_Tal_Rev_Mecanica;
                Mi_SQL = Mi_SQL + " (" + Ope_Tal_Rev_Mecanica.Campo_No_Registro + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_No_Entrada + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Mecanico_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Estatus + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Fecha_Revision_Inicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Vehiculo_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Usuario_Creo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Fecha_Creo + ")";
                Mi_SQL = Mi_SQL + " VALUES (" + Parametros.P_No_Registro + "";
                Mi_SQL = Mi_SQL + ", " + Parametros.P_No_Entrada + "";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Mecanico_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Solicitud + "'";
                Mi_SQL = Mi_SQL + ", GETDATE()";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Vehiculo_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE())";
                P_Cmd.CommandText = Mi_SQL;
                P_Cmd.ExecuteNonQuery();

            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modifica_Revista_Mecanica      
        ///DESCRIPCIÓN          : Modifica los Datos Iniciales del Registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 22/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Modifica_Revista_Mecanica(Cls_Ope_Tal_Revista_Mecanica_Negocio Parametros) {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                String Mi_SQL = "UPDATE " + Ope_Tal_Rev_Mecanica.Tabla_Ope_Tal_Rev_Mecanica;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Rev_Mecanica.Campo_Mecanico_ID + " = '" + Parametros.P_Mecanico_ID + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Vehiculo_ID + " = '" + Parametros.P_Vehiculo_ID + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Fecha_Revision_Inicio + " = GETDATE()";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Rev_Mecanica.Campo_No_Registro + " = '" + Parametros.P_No_Registro + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Cls_Ope_Tal_Entradas_Vehiculos_Negocio Ent_Negocio = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
                Ent_Negocio.P_No_Entrada = Parametros.P_No_Entrada;
                Ent_Negocio.P_Estatus = "PROCESO";
                Ent_Negocio.P_Usuario = Parametros.P_Usuario;
                Cls_Ope_Tal_Entradas_Vehiculos_Datos.Actualizacion_Estatus_Entrada(Ent_Negocio, ref Cmd);

                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Asignar_Diagnostico_Revista_Mecanica      
        ///DESCRIPCIÓN          : Asigna Diagnostico a la Revista
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 22/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Asignar_Diagnostico_Revista_Mecanica(Cls_Ope_Tal_Revista_Mecanica_Negocio Parametros) {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            try
            {
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                String Mi_SQL = "UPDATE " + Ope_Tal_Rev_Mecanica.Tabla_Ope_Tal_Rev_Mecanica;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Rev_Mecanica.Campo_Diagnostico + " = '" + Parametros.P_Diagnostico + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Fecha_Revision_Fin + " = GETDATE()";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mecanica.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Rev_Mecanica.Campo_No_Registro + " = '" + Parametros.P_No_Registro + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Int32 No_Detalle = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Rev_Mec_Det.Tabla_Ope_Tal_Rev_Mec_Det, Ope_Tal_Rev_Mec_Det.Campo_No_Detalle, 20));
                if (Parametros.P_Dt_Detalles_Revista != null) {
                    foreach (DataRow Fila in Parametros.P_Dt_Detalles_Revista.Rows) {
                        Mi_SQL = "INSERT INTO " + Ope_Tal_Rev_Mec_Det.Tabla_Ope_Tal_Rev_Mec_Det;
                        Mi_SQL = Mi_SQL + " (" + Ope_Tal_Rev_Mec_Det.Campo_No_Detalle + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mec_Det.Campo_No_Registro + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mec_Det.Campo_Parte_ID + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mec_Det.Campo_SubParte_ID + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mec_Det.Campo_Valor + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mec_Det.Campo_Observaciones + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mec_Det.Campo_Usuario_Creo + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Rev_Mec_Det.Campo_Fecha_Creo + ")";
                        Mi_SQL = Mi_SQL + " VALUES (" + No_Detalle + "";
                        Mi_SQL = Mi_SQL + ", " + Parametros.P_No_Registro + "";
                        Mi_SQL = Mi_SQL + ", '" + Fila["PARTE_ID"].ToString().Trim() +"'";
                        Mi_SQL = Mi_SQL + ", '" + Fila["SUBPARTE_ID"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila["VALOR"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila["OBSERVACIONES"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        No_Detalle = No_Detalle + 1;
                    }
                }

                Cls_Ope_Tal_Entradas_Vehiculos_Negocio Ent_Negocio = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
                Ent_Negocio.P_No_Entrada = Parametros.P_No_Entrada;
                Ent_Negocio.P_Estatus = "CERRADA";
                Ent_Negocio.P_Usuario = Parametros.P_Usuario;
                Cls_Ope_Tal_Entradas_Vehiculos_Datos.Actualizacion_Estatus_Entrada(Ent_Negocio, ref Cmd);

                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_No_Solicitud = Parametros.P_No_Solicitud;
                Solicitud.P_Estatus = "TERMINADO";
                Solicitud.P_Empleado_Solicito_ID = Parametros.P_Usuario_ID;
                Solicitud.P_Usuario = Parametros.P_Usuario;
                Cls_Ope_Tal_Solicitud_Servicio_Datos.Alta_Registro_Seguimiento_Solicitud(Solicitud, ref Cmd);

                Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio Cal_Negocio = new Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio();
                Cal_Negocio.P_Vehiculo_ID = Parametros.P_Vehiculo_ID;
                Cal_Negocio.P_Fecha_Ultima_Revision = DateTime.Today;
                Cal_Negocio.P_Cmmd = Cmd;
                Cal_Negocio.Sincronizacion_Revista_Mecanica_Periodo();

                if (Parametros.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                } 
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Revista_Mecanica
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un Objeto.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 22/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Cls_Ope_Tal_Revista_Mecanica_Negocio Consultar_Detalles_Revista_Mecanica(Cls_Ope_Tal_Revista_Mecanica_Negocio Parametros) {
            String Mi_SQL = null;
            Cls_Ope_Tal_Revista_Mecanica_Negocio Obj_Cargado = new Cls_Ope_Tal_Revista_Mecanica_Negocio();
            try {
                Mi_SQL = "SELECT * FROM " + Ope_Tal_Rev_Mecanica.Tabla_Ope_Tal_Rev_Mecanica;
                if (Parametros.P_No_Entrada > (-1)) {
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Rev_Mecanica.Campo_No_Entrada + " = '" + Parametros.P_No_Entrada + "'";
                } else if (Parametros.P_No_Registro > (-1)) {
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Rev_Mecanica.Campo_No_Registro + " = '" + Parametros.P_No_Registro + "'";
                } else if (Parametros.P_No_Solicitud > (-1)) {
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Rev_Mecanica.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                }
                SqlDataReader Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                while (Reader.Read()) {
                    Obj_Cargado.P_No_Registro = (!String.IsNullOrEmpty(Reader[Ope_Tal_Rev_Mecanica.Campo_No_Registro].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Rev_Mecanica.Campo_No_Registro]) : -1;
                    Obj_Cargado.P_No_Entrada = (!String.IsNullOrEmpty(Reader[Ope_Tal_Rev_Mecanica.Campo_No_Entrada].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Rev_Mecanica.Campo_No_Entrada]) : -1;
                    Obj_Cargado.P_Mecanico_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Rev_Mecanica.Campo_Mecanico_ID].ToString())) ? Reader[Ope_Tal_Rev_Mecanica.Campo_Mecanico_ID].ToString() : "";
                    Obj_Cargado.P_Estatus = (!String.IsNullOrEmpty(Reader[Ope_Tal_Rev_Mecanica.Campo_Estatus].ToString())) ? Reader[Ope_Tal_Rev_Mecanica.Campo_Estatus].ToString() : "";
                    Obj_Cargado.P_Fecha_Inicio_Revision = (!String.IsNullOrEmpty(Reader[Ope_Tal_Rev_Mecanica.Campo_Fecha_Revision_Inicio].ToString())) ? Convert.ToDateTime(Reader[Ope_Tal_Rev_Mecanica.Campo_Fecha_Revision_Inicio]) : new DateTime();
                    Obj_Cargado.P_Fecha_Fin_Revision = (!String.IsNullOrEmpty(Reader[Ope_Tal_Rev_Mecanica.Campo_Fecha_Revision_Fin].ToString())) ? Convert.ToDateTime(Reader[Ope_Tal_Rev_Mecanica.Campo_Fecha_Revision_Fin]) : new DateTime();
                    Obj_Cargado.P_Diagnostico = (!String.IsNullOrEmpty(Reader[Ope_Tal_Rev_Mecanica.Campo_Diagnostico].ToString())) ? Reader[Ope_Tal_Rev_Mecanica.Campo_Diagnostico].ToString() : "";
                    Obj_Cargado.P_Comentarios = (!String.IsNullOrEmpty(Reader[Ope_Tal_Rev_Mecanica.Campo_Comentarios].ToString())) ? Reader[Ope_Tal_Rev_Mecanica.Campo_Comentarios].ToString() : "";
                    Obj_Cargado.P_Vehiculo_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Rev_Mecanica.Campo_Vehiculo_ID].ToString())) ? Reader[Ope_Tal_Rev_Mecanica.Campo_Vehiculo_ID].ToString() : "";
                }
                Reader.Close();
                if (Obj_Cargado.P_No_Registro > (-1)) {
                    Mi_SQL = "SELECT DETALLES." + Ope_Tal_Rev_Mec_Det.Campo_No_Detalle + " AS NO_DETALLE";
                    Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Rev_Mec_Det.Campo_No_Registro + " AS NO_REGISTRO";
                    Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Rev_Mec_Det.Campo_Parte_ID + " AS PARTE_ID";
                    Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Tal_Detalles_Rev_Mec.Campo_Nombre + " FROM " + Cat_Tal_Detalles_Rev_Mec.Tabla_Cat_Tal_Detalles_Rev_Mec + " WHERE " + Cat_Tal_Detalles_Rev_Mec.Campo_Detalle_ID + " = DETALLES." + Ope_Tal_Rev_Mec_Det.Campo_Parte_ID + ") AS PARTE_NOMBRE";
                    Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Rev_Mec_Det.Campo_SubParte_ID + " AS SUBPARTE_ID";
                    Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Tal_Detalles_Rev_Mec.Campo_Nombre + " FROM " + Cat_Tal_Detalles_Rev_Mec.Tabla_Cat_Tal_Detalles_Rev_Mec + " WHERE " + Cat_Tal_Detalles_Rev_Mec.Campo_Detalle_ID + " = DETALLES." + Ope_Tal_Rev_Mec_Det.Campo_SubParte_ID + ") AS SUBPARTE_NOMBRE";
                    Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Rev_Mec_Det.Campo_Valor + " AS VALOR";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Rev_Mec_Det.Tabla_Ope_Tal_Rev_Mec_Det + " DETALLES";
                    Mi_SQL = Mi_SQL + " WHERE DETALLES." + Ope_Tal_Rev_Mec_Det.Campo_No_Registro + " = '" + Obj_Cargado.P_No_Registro + "'";
                    DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Ds_Datos != null && Ds_Datos.Tables.Count > 0) {
                        Obj_Cargado.P_Dt_Detalles_Revista = Ds_Datos.Tables[0];
                    }
                }
            } catch (Exception Ex) {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Obj_Cargado;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Revistas_Mecanicas
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 22/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Revistas_Mecanicas(Cls_Ope_Tal_Revista_Mecanica_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT REVISTA." + Ope_Tal_Rev_Mecanica.Campo_No_Registro + " AS NO_REGISTRO";
                Mi_SQL = Mi_SQL + ", REVISTA." + Ope_Tal_Rev_Mecanica.Campo_No_Entrada + " AS NO_ENTRADA";
                Mi_SQL = Mi_SQL + ", REVISTA." + Ope_Tal_Rev_Mecanica.Campo_No_Solicitud + " AS NO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " FOLIO";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " AS FECHA_RECEPCION";
                Mi_SQL = Mi_SQL + ", '' AS REPARACION";
                Mi_SQL = Mi_SQL + ", '' AS COSTO";
                Mi_SQL = Mi_SQL + ", 'REVISTA MECANICA' AS TIPO_SERVICIO";
                Mi_SQL = Mi_SQL + ", (DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + ") AS DEPENDENCIA";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                Mi_SQL = Mi_SQL + ",  CASE REVISTA." + Ope_Tal_Serv_Preventivos.Campo_Estatus + " when 'PENDIENTE'Then 'POR ASIGNAR' when 'PROCESO'then 'ASIGNADO' end AS ESTATUS ";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Rev_Mecanica.Tabla_Ope_Tal_Rev_Mecanica + " REVISTA";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES ON REVISTA." + Ope_Tal_Rev_Mecanica.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS ON REVISTA." + Ope_Tal_Rev_Mecanica.Campo_Vehiculo_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                if (!String.IsNullOrEmpty(Parametros.P_Estatus)) {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "REVISTA." + Ope_Tal_Rev_Mecanica.Campo_Estatus + " IN ('" + Parametros.P_Estatus.Trim() + "')";
                }
                if (Parametros.P_No_Registro > 0)
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "REVISTA." + Ope_Tal_Rev_Mecanica.Campo_No_Registro + " = " + Parametros.P_No_Registro + " ";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Estatus_Solicitud))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " IN ('" + Parametros.P_Estatus_Solicitud.Trim() + "')";
                }
                Mi_SQL = Mi_SQL + "ORDER BY NO_REGISTRO";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID)
        {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try
            {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            }
            catch (SqlException Ex)
            {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
        ///PARAMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
        {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
            {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }

    }
}
