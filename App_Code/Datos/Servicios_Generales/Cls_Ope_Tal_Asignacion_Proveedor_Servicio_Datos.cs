﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Operacion_Asignacion_Proveedor.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;

namespace JAPAMI.Taller_Mecanico.Operacion_Asignacion_Proveedor.Datos
{
    public class Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Datos
    {
        #region [Metodos]
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Proveedor_Servicio     
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Proveedor_Servicio(Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            String Mi_SQL = "";
            Object Aux; //Variable auxiliar para las consultas
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            try
            {
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                //Formar Sentencia para obtener el consecutivo
                Mi_SQL = "";
                Mi_SQL = "SELECT ISNULL(MAX(";
                Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + "),0)";
                Mi_SQL = Mi_SQL + " FROM ";
                Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;

                //Ejecutar consulta del consecutivo
                Cmd.CommandText = Mi_SQL;
                Aux = Cmd.ExecuteScalar();
                //Aux = OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0][0];

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                {
                    Parametros.P_No_Asignacion_Proveedor = Convert.ToInt32(Aux) + 1;
                }
                else
                    Parametros.P_No_Asignacion_Proveedor = 1;
                Mi_SQL = "UPDATE " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " = 'RECHAZADO'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Usuario_Creo + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Fecha_Creo + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = '" + (Parametros.P_Tipo.Trim().Contains("PREV") ? "SERVICIO_PREVENTIVO" : "SERVICIO_CORRECTIVO") + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                //Insertar el nuevo proveedor
                Mi_SQL = "INSERT INTO " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;
                Mi_SQL = Mi_SQL + " (" + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Observaciones + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Usuario_Creo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Fecha_Creo + ")";
                Mi_SQL = Mi_SQL + " VALUES (" + Parametros.P_No_Asignacion_Proveedor + "";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Proveedor_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Servicio + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Observaciones + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE())";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Actualiza estatus de Servicio
                //Si es preventivo
                if (Parametros.P_Tipo.Contains("PREV"))
                {
                    Mi_SQL = "UPDATE " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = '" + Parametros.P_Estatus_Servicio + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                //Si es correctivo
                else if (Parametros.P_Tipo.Contains("CORR"))
                {
                    Mi_SQL = "UPDATE " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Correctivos.Campo_Estatus + " = '" + Parametros.P_Estatus_Servicio + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                if (Parametros.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }                
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consulta_Proveedor_Servicio     
        ///DESCRIPCIÓN          : Consultar un registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consulta_Proveedor_Servicio(Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT ";
                if (!String.IsNullOrEmpty(Parametros.P_Campos_Dinamicos))
                {
                    Mi_SQL = Mi_SQL + Parametros.P_Campos_Dinamicos;
                }
                else
                {
                    Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " .* ";
                }
                Mi_SQL = Mi_SQL + "," + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Compañia + " AS NOMBRE_PROVEEDOR ";
                if (!String.IsNullOrEmpty(Parametros.P_Campos_Dinamicos_Proveedor))
                {
                    Mi_SQL = Mi_SQL + Parametros.P_Campos_Dinamicos_Proveedor;
                }
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ON " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + " ";
                Mi_SQL = Mi_SQL + " = " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + "." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID;
                if (!String.IsNullOrEmpty(Parametros.P_Filtros_Dinamicos))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Parametros.P_Filtros_Dinamicos;
                }
                else
                {
                    if (Parametros.P_No_Servicio > 0 && !string.IsNullOrEmpty(Parametros.P_Tipo))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + "." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = " + Parametros.P_No_Servicio + " ";
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + "." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = '" + (Parametros.P_Tipo.Trim().Contains("PREV") ? "SERVICIO_PREVENTIVO" : "SERVICIO_CORRECTIVO") + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + "." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN ('" + Parametros.P_Estatus.Trim() + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Proveedor_ID))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + "." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + " IN ('" + Parametros.P_Proveedor_ID.Trim() + "')";
                    }

                    Mi_SQL = Mi_SQL + " ORDER BY 1 DESC";
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                    {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        #endregion        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Proveedor_Servicio     
        ///DESCRIPCIÓN          : Mofificar un registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Modificar_Proveedor_Servicio(Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Parametros)
        {
            String Mi_SQL = "";
            String Mensaje = "";
            SqlTransaction Trans = null;
            SqlCommand Cmd = new SqlCommand();
            SqlConnection Cn = new SqlConnection();
            DataTable Dt_Parametros = new DataTable();
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            try
            {
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }    
                //Actualiza los datos de la tabla Asignacion de Proveedor donde estan los proveedores
                //para este servicio Externo con su estatus y su costo
                Mi_SQL = "UPDATE " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " = '"+Parametros.P_Estatus+"'";
                if (!String.IsNullOrEmpty(Parametros.P_Diagnostico.Trim()))
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Diagnostico + " = '" + Parametros.P_Diagnostico.ToUpper() + "'";
                if (!String.IsNullOrEmpty(Parametros.P_Observaciones.Trim()))
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Observaciones + " = '" + Parametros.P_Observaciones.ToUpper() + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Costo + " = " + Parametros.P_Costo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + " = '" + Parametros.P_Proveedor_ID.Trim() + "'";
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = '" + (Parametros.P_Tipo.Trim().Contains("PREV") ? "SERVICIO_PREVENTIVO" : "SERVICIO_CORRECTIVO") + "'";
                //Se ejecuta la operacion
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();                
                //Actualiza estatus de Servicio Preventivo o correctivo para avanzarlo en la etapa en que esta
                //Si es preventivo
                if (Parametros.P_Tipo.Contains("PREV"))
                {
                    Mi_SQL = "UPDATE " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = '" + Parametros.P_Estatus_Servicio + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                //Si es correctivo
                else if (Parametros.P_Tipo.Contains("CORR"))
                {
                    Mi_SQL = "UPDATE " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Correctivos.Campo_Estatus + " = '" + Parametros.P_Estatus_Servicio + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                //SOLO APLICA PARA LAS VERIFICACIONES YA QUE NO ENTRAN EN EL TALLER
                if (Parametros.P_Estatus_Servicio.Trim().Equals("REPARADO")) {
                    Mi_SQL = "UPDATE " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " = 'ENTREGADO'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                //Transaccion
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Commit();
                } 
            }
            catch (SqlException Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }  
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Reserva_Servicios     
        ///DESCRIPCIÓN          : Para crear una reserva
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static int Crear_Reserva_Servicio(Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            String Mi_SQL = "";
            String Partida_ID = "0";
            String Proyecto_ID = "0";
            String FF = "0";
            String Dependencia_ID = "0";
            String Tipo_Solicitud_Pago_ID = "0";
            String Capitulo_ID = "0";
            Object Aux = null;
            DataTable Dt_Parametros = new DataTable();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            Cls_Tal_Parametros_Negocio Negocio_Parametros = new Cls_Tal_Parametros_Negocio();
            Cls_Ope_Tal_Consultas_Generales_Negocio Cosnultas_Generales_Ng = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Int32 Mi_Reserva = -1;
            try
            {
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                //Limpiamos la Variable Mi_SQL
                Mi_SQL = "";
                //Consultamos Valores del Catalogo de Parametros
                Negocio_Parametros.P_Cmmd = Cmd;
                Negocio_Parametros = Negocio_Parametros.Consulta_Parametros_Presupuesto(Parametros.P_No_Economico, Parametros.P_Tipo_Bien);                
                //Se asignan a las Variables para los datos de la partida
                Partida_ID = Negocio_Parametros.P_Partida_ID;
                FF = Negocio_Parametros.P_Fuente_Financiamiento_ID;
                Capitulo_ID = Negocio_Parametros.P_Capitulo_ID;
                //Se obtiene La dependencia del objeto de Negocio y no de los parametros
                Dependencia_ID = Parametros.P_Dependencia_ID.ToString();
                Tipo_Solicitud_Pago_ID = Negocio_Parametros.P_Tipo_Solicitud_Pago_ID;
                //Se consulta Programa de la solicitud
                Cosnultas_Generales_Ng.P_Cmmd = Cmd;
                Cosnultas_Generales_Ng.P_No_Solicitud = Parametros.P_No_Solicitud;
                Proyecto_ID = Cosnultas_Generales_Ng.Consultar_Proyecto_Programa_Solicitud();
                ////Se consulta el CAPITULO_ID
                //Mi_SQL = "SELECT " + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado;
                //Mi_SQL += " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dependencia_ID + "'";
                //Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Proyecto_ID + "'";
                //Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + FF + "'";
                //Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida_ID + "'";
                //Cmd.CommandText = Mi_SQL;
                //Aux = Cmd.ExecuteScalar();
                //if (Aux != null)
                //    Capitulo_ID = Aux.ToString();
                //Se crea la Tabla pra la Partida
                DataTable Dt_Partidas_Reserva = new DataTable();
                Dt_Partidas_Reserva = new DataTable("Dt_Partidas_Reserva");
                Dt_Partidas_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("FECHA_CREO", System.Type.GetType("System.DateTime"));
                Dt_Partidas_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                DataRow Dr_Partida = Dt_Partidas_Reserva.NewRow();
                Dr_Partida["FUENTE_FINANCIAMIENTO_ID"] = FF;
                Dr_Partida["PROGRAMA_ID"] = Proyecto_ID;
                Dr_Partida["PARTIDA_ID"] = Partida_ID;
                Dr_Partida["FECHA_CREO"] = DateTime.Today;
                Dr_Partida["DEPENDENCIA_ID"] = Dependencia_ID;
                Dr_Partida["CAPITULO_ID"] = Capitulo_ID;
                Dr_Partida["IMPORTE"] = Convert.ToDouble(Parametros.P_Costo);
                Dr_Partida["ANIO"] = DateTime.Now.Year.ToString();
                Dt_Partidas_Reserva.Rows.Add(Dr_Partida);
                //especifica si es RAMO 33 o NORMAL
                String Recurso = "RECURSO ASIGNADO";

                //Crear Reserva
                Mi_Reserva = Cls_Ope_Psp_Manejo_Presupuesto.Crear_Reserva(
                    Dependencia_ID,
                    "GENERADA",
                    "P-"+Parametros.P_Nombre_Proveedor, //Beneficiario
                    FF,
                    Proyecto_ID,
                    "REPARACION DE SERVICIO A VEHICULO CON NO INV. " + Parametros.P_No_Inventario,
                    DateTime.Now.Year.ToString(),
                    Convert.ToDouble(Parametros.P_Costo),
                    Parametros.P_Proveedor_ID, //proveedor_id
                    "", //id del beneficiario que puede ser proveedor o empleado
                    Tipo_Solicitud_Pago_ID,
                    Dt_Partidas_Reserva,
                    Recurso,Cmd);
                Mi_SQL = "UPDATE " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Solicitudes_Serv.Campo_No_Reserva + " = '" + Mi_Reserva + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ";
                Mi_SQL += Ope_Psp_Reservas.Campo_Tipo_Reserva + "='UNICA' ";
                Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + Mi_Reserva;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                int Registro_Afectado;
                int Registro_Movimiento;
                Registro_Afectado = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Parametros.P_Cargo, Parametros.P_Abono, Dt_Partidas_Reserva, Cmd);
                Registro_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Mi_Reserva), Parametros.P_Cargo, Parametros.P_Abono, Convert.ToDouble(Parametros.P_Costo), "", "", "", "", Cmd);

                if (Parametros.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
            return Mi_Reserva;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Reserva_Servicio     
        ///DESCRIPCIÓN          : Para crear una reserva
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Modificar_Reserva_Servicio(Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            String Mi_SQL = "";
            String Partida_ID = "0";
            String Proyecto_ID = "0";
            String FF = "0";
            String Dependencia_ID = "0";
            String Tipo_Solicitud_Pago_ID = "0";
            DataTable Dt_Parametros = new DataTable();
            Object Aux = null; //Variable auxiliar para las consultas
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            Cls_Ope_Tal_Consultas_Generales_Negocio Cosnultas_Generales_Ng = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Cls_Tal_Parametros_Negocio Negocio_Parametros = new Cls_Tal_Parametros_Negocio();
            Int32 Mi_Reserva = -1;
            String Capitulo_ID = "0";

            try
            {
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                Mi_SQL = " SELECT " + Ope_Tal_Solicitudes_Serv.Campo_No_Reserva + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                Cmd.CommandText = Mi_SQL;
                Aux = Cmd.ExecuteScalar();

                //Verificar si la consulta arrojo resultado
                if (!(Aux is Nullable) && !Aux.ToString().Equals("") && Aux != null && Aux != DBNull.Value)
                {
                    Mi_Reserva = Convert.ToInt32(Aux.ToString());
                    //Limpiamos la Variable Mi_SQL
                    Mi_SQL = "";
                    Mi_SQL = " SELECT " + Ope_Psp_Reservas.Campo_Importe_Inicial + " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + Mi_Reserva + "'";
                    Cmd.CommandText = Mi_SQL;
                    Aux = Cmd.ExecuteScalar();
                    if (!(Aux is Nullable) && !Aux.ToString().Equals("") && Aux != null && Aux != DBNull.Value)
                    {
                        Parametros.P_Costo = Convert.ToDouble(Aux);
                    }
                    Negocio_Parametros.P_Cmmd = Cmd;
                    Negocio_Parametros = Negocio_Parametros.Consulta_Parametros_Presupuesto(Parametros.P_No_Economico, Parametros.P_Tipo_Bien);
                    Partida_ID = Negocio_Parametros.P_Partida_ID;
                    FF = Negocio_Parametros.P_Fuente_Financiamiento_ID;
                    Capitulo_ID = Negocio_Parametros.P_Capitulo_ID;
                    ////Limpiamos la Variable Mi_SQL
                    //Mi_SQL = "";
                    ////Consultamos la tabla de parametros para obtener la partida especifica
                    //Mi_SQL = "SELECT " + Cat_Tal_Parametros.Campo_Fuente_Financiamiento;
                    //Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Partida_ID;
                    //Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Dependencia_ID;
                    //Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID;
                    //Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Programa_ID;
                    //Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros;
                    ////Ejecutar Consulta
                    //Cmd.CommandText = Mi_SQL;
                    //Obj_Adaptador.SelectCommand = Cmd;
                    //Obj_Adaptador.Fill(Dt_Parametros);
                    //Dt_Parametros = OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                    
                    //if (Dt_Parametros.Rows.Count > 0)
                    //{
                    //    Partida_ID = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Partida_ID].ToString();
                    //    FF = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Fuente_Financiamiento].ToString();
                    //    Tipo_Solicitud_Pago_ID = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID].ToString();
                    //}
                    //Se consulta Programa de la solicitud
                    Cosnultas_Generales_Ng.P_Cmmd = Cmd;
                    Cosnultas_Generales_Ng.P_No_Solicitud = Parametros.P_No_Solicitud;
                    Proyecto_ID = Cosnultas_Generales_Ng.Consultar_Proyecto_Programa_Solicitud();
                    //Utilizamos la dependencia de la unidad en lugar de la de los parametros
                    Dependencia_ID = Parametros.P_Dependencia_ID;
                    ////Se consulta el CAPITULO_ID
                    //Mi_SQL = "SELECT " + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado;
                    //Mi_SQL += " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dependencia_ID + "'";
                    //Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Proyecto_ID + "'";
                    //Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + FF + "'";
                    //Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida_ID + "'";
                    //Cmd.CommandText = Mi_SQL;
                    //Aux = Cmd.ExecuteScalar();
                    //if (Aux != null)
                    //    Capitulo_ID = Aux.ToString();
                   
                    DataTable Dt_Partidas_Reserva = new DataTable();
                    Dt_Partidas_Reserva = new DataTable("Dt_Partidas_Reserva");
                    Dt_Partidas_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("FECHA_CREO", System.Type.GetType("System.DateTime"));
                    Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    DataRow Dr_Partida = Dt_Partidas_Reserva.NewRow();
                    Dr_Partida["FUENTE_FINANCIAMIENTO_ID"] = FF;
                    Dr_Partida["PROGRAMA_ID"] = Proyecto_ID;
                    Dr_Partida["PARTIDA_ID"] = Partida_ID;
                    Dr_Partida["ANIO"] = DateTime.Today.Year.ToString();
                    Dr_Partida["FECHA_CREO"] = DateTime.Today;
                    Dr_Partida["DEPENDENCIA_ID"] = Dependencia_ID;
                    Dr_Partida["IMPORTE"] = Convert.ToDouble(Parametros.P_Costo);
                    Dr_Partida["CAPITULO_ID"] = Capitulo_ID;
                    Dt_Partidas_Reserva.Rows.Add(Dr_Partida);
                    //especifica si es RAMO 33 o NORMAL
                    String Recurso = "RECURSO ASIGNADO";

                    int Registro_Afectado;
                    int Registro_Movimiento;
                    //Movimiento Presupuestal
                    Registro_Afectado = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Parametros.P_Cargo, Parametros.P_Abono, Dt_Partidas_Reserva, Cmd);
                    Registro_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Mi_Reserva), Parametros.P_Cargo, Parametros.P_Abono, Convert.ToDouble(Parametros.P_Costo), "", "", "", "", Cmd);
                    if (Registro_Afectado == 0 || Registro_Movimiento == 0)
                        throw new Exception("Error al Liberar el Presupuesto. Pongase en Contacto con el Administrador del Sistema");
                    if (Parametros.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                }
            }
            catch (SqlException Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Extender_Reserva_Servicio     
        ///DESCRIPCIÓN          : Para extender una reserva
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 17/Enero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Extender_Reserva_Servicio(Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            String Mi_SQL = "";
            DataTable Dt_Parametros = new DataTable();
            Object Aux = null; //Variable auxiliar para las consultas
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            Int32 Mi_Reserva = -1;            
            try
            {
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                Mi_SQL = " SELECT " + Ope_Tal_Solicitudes_Serv.Campo_No_Reserva + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                Cmd.CommandText = Mi_SQL;
                Aux = Cmd.ExecuteScalar();

                //Verificar si la consulta arrojo resultado
                if (!(Aux is Nullable) && !Aux.ToString().Equals("") && Aux != null && Aux != DBNull.Value)
                {
                    Mi_Reserva = Convert.ToInt32(Aux.ToString());
                    int Registro_Afectado;
                    int Registro_Movimiento;
                    //Se Modifica la Reserva
                    Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ";
                    Mi_SQL += Ope_Psp_Reservas.Campo_Importe_Inicial + " = " + Parametros.P_Costo;
                    Mi_SQL += ", " + Ope_Psp_Reservas.Campo_Saldo + " = " + Parametros.P_Costo;
                    Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + Mi_Reserva;
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Mi_SQL = "UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles + " SET ";
                    Mi_SQL += Ope_Psp_Reservas.Campo_Importe_Inicial + " = " + Parametros.P_Costo;
                    Mi_SQL += ", " + Ope_Psp_Reservas.Campo_Saldo + " = " + Parametros.P_Costo;
                    Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + Mi_Reserva;
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Movimiento Presupuestal
                    Registro_Afectado = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Parametros.P_Cargo, Parametros.P_Abono, Parametros.P_Dt_Manejo_Psp, Cmd);
                    Registro_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Mi_Reserva), Parametros.P_Cargo, Parametros.P_Abono, Convert.ToDouble(Parametros.P_Costo_Extra), "", "", "", "", Cmd);
                    if (Registro_Afectado == 0 || Registro_Movimiento == 0)
                        throw new Exception("Error al Liberar el Presupuesto. Pongase en Contacto con el Administrador del Sistema");
                    if (Parametros.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                }
            }
            catch (SqlException Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cancelar_Reserva_Servicio     
        ///DESCRIPCIÓN          : Para Cancelar una reserva al cancelar 
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Cancelar_Reserva_Servicio(Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            String Mi_SQL = "";
            String Partida_ID = "0";
            String Proyecto_ID = "0";
            String FF = "0";
            String Capitulo_ID = "0";
            String Dependencia_ID = "0";
            String Tipo_Solicitud_Pago_ID = "0";
            DataTable Dt_Parametros = new DataTable();
            Object Aux = null; //Variable auxiliar para las consultas
            DataTable Dt_Aux = null; //Tabla auxiliar para las consultas
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            Cls_Ope_Tal_Consultas_Generales_Negocio Cosnultas_Generales_Ng = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            Cls_Tal_Parametros_Negocio Negocio_Parametros = new Cls_Tal_Parametros_Negocio();
            Int32 Mi_Reserva = -1;
            
            try
            {
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                Mi_SQL = " SELECT " + Ope_Tal_Solicitudes_Serv.Campo_No_Reserva + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                Cmd.CommandText = Mi_SQL;
                Aux = Cmd.ExecuteScalar();

                //Verificar si la consulta arrojo resultado
                if (!(Aux is Nullable) && !Aux.ToString().Equals("") && Aux != null && Aux != DBNull.Value)
                {
                    Mi_Reserva = Convert.ToInt32(Aux.ToString());
                    //Limpiamos la Variable Mi_SQL
                    Mi_SQL = "";
                    Mi_SQL = " SELECT " + Ope_Psp_Reservas.Campo_Importe_Inicial + " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + Mi_Reserva + "'";
                    Cmd.CommandText = Mi_SQL;
                    Aux = Cmd.ExecuteScalar();
                    if (!(Aux is Nullable) && !Aux.ToString().Equals("") && Aux != null && Aux != DBNull.Value)
                    {
                        Parametros.P_Costo = Convert.ToDouble(Aux);
                    }
                    Negocio_Parametros.P_Cmmd = Cmd;
                    Negocio_Parametros.P_Numero_Economico = Parametros.P_No_Inventario;
                    Negocio_Parametros = Negocio_Parametros.Consulta_Parametros_Presupuesto(Parametros.P_No_Economico, Parametros.P_Tipo_Bien);
                    Partida_ID = Negocio_Parametros.P_Partida_ID;
                    FF = Negocio_Parametros.P_Fuente_Financiamiento_ID;
                    Capitulo_ID = Negocio_Parametros.P_Capitulo_ID;
                    //Limpiamos la Variable Mi_SQL
                    //Mi_SQL = "";
                    ////Consultamos la tabla de parametros para obtener la partida especifica
                    //Mi_SQL = "SELECT " + Cat_Tal_Parametros.Campo_Fuente_Financiamiento;
                    //Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Partida_ID;
                    //Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Dependencia_ID;
                    //Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID;
                    //Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Programa_ID;
                    //Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros;
                    ////Ejecutar Consulta
                    //Cmd.CommandText = Mi_SQL;
                    //Obj_Adaptador.SelectCommand = Cmd;
                    //Obj_Adaptador.Fill(Dt_Parametros);                
                    ////Dt_Parametros = OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                    //String Partida_ID = "0";//Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Partida_ID].ToString().Trim();
                    //String Proyecto_ID = "0";//Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID].ToString().Trim();
                    //String FF = "0";//Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                    //String Dependencia_ID = "0";
                    //String Tipo_Solicitud_Pago_ID = "0";
                    //if (Dt_Parametros.Rows.Count > 0)
                    //{
                    //    Partida_ID = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Partida_ID].ToString();
                    //    FF = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Fuente_Financiamiento].ToString();
                    //    Tipo_Solicitud_Pago_ID = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID].ToString();
                    //}
                    //Se consulta Programa de la solicitud
                    Cosnultas_Generales_Ng.P_Cmmd = Cmd;
                    Cosnultas_Generales_Ng.P_No_Solicitud = Parametros.P_No_Solicitud;
                    Proyecto_ID = Cosnultas_Generales_Ng.Consultar_Proyecto_Programa_Solicitud();
                    //Se ultiliza la Dependencia de la Unidad
                    Dependencia_ID = Parametros.P_Dependencia_ID;
                    DataTable Dt_Partidas_Reserva = new DataTable();
                    Dt_Partidas_Reserva = new DataTable("Dt_Partidas_Reserva");
                    Dt_Partidas_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("FECHA_CREO", System.Type.GetType("System.DateTime"));
                    Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                    DataRow Dr_Partida = Dt_Partidas_Reserva.NewRow();
                    Dr_Partida["FUENTE_FINANCIAMIENTO_ID"] = FF;
                    Dr_Partida["PROGRAMA_ID"] = Proyecto_ID;
                    Dr_Partida["PARTIDA_ID"] = Partida_ID;
                    Dr_Partida["FECHA_CREO"] = DateTime.Today;
                    Dr_Partida["CAPITULO_ID"] = Capitulo_ID;
                    Dr_Partida["DEPENDENCIA_ID"] = Dependencia_ID;
                    Dr_Partida["ANIO"] = DateTime.Today.Year.ToString();
                    Dr_Partida["IMPORTE"] = Convert.ToDouble(Parametros.P_Costo);
                    Dt_Partidas_Reserva.Rows.Add(Dr_Partida);
                    //especifica si es RAMO 33 o NORMAL
                    String Recurso = "RECURSO ASIGNADO";
                       
                        int Registro_Afectado;
                        int Registro_Movimiento;
                        //Se cancela Reserva
                        Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET ";
                        Mi_SQL += Ope_Psp_Reservas.Campo_Estatus + "='CANCELADA' ";
                        Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + "=" + Mi_Reserva;
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        //Se borra la reserva de la solicitud
                        Mi_SQL = "UPDATE " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                        Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Solicitudes_Serv.Campo_No_Reserva + " = null";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Modifico + " = GETDATE()";
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        //Se Libera el Presupuesto PRE-comprometido
                        Registro_Afectado = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Parametros.P_Cargo, Parametros.P_Abono, Dt_Partidas_Reserva, Cmd);
                        Registro_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Mi_Reserva), Parametros.P_Cargo, Parametros.P_Abono, Convert.ToDouble(Parametros.P_Costo), "", "", "", "", Cmd);
                        if (Registro_Afectado == 0 || Registro_Movimiento == 0)
                            throw new Exception("Error al Liberar el Presupuesto. Pongase en Contacto con el Administrador del Sistema");
                    }
            }
            catch (SqlException Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Asignaciones_Servicios
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 31/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Asignaciones_Servicios(Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Parametros)  {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            try {
                Mi_SQL = "SELECT ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " AS NO_ASIGNACION_PROV_SERV";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " AS NO_SERVICIO";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " AS FOLIO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " AS FECHA_RECEPCION_REAL";
                Mi_SQL = Mi_SQL + ", 'SERVICIO CORRECTIVO' AS TIPO_SERVICIO";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " AS NO_ECONOMICO";
                Mi_SQL = Mi_SQL + ", ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " ASIGNACION";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Prov_Ent_Sal.Tabla_Ope_Tal_Prov_Ent_Sal + " ENT_SAL ON ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " = ENT_SAL." + Ope_Tal_Prov_Ent_Sal.Campo_No_Asignacion_Prov_Serv + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos + " SERVICIOS ON ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES ON SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                Mi_SQL = Mi_SQL + " WHERE SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Estatus + " = 'ENTRADA_PROVEEDOR' AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = 'SERVICIO_CORRECTIVO'";
                Mi_SQL = Mi_SQL + " AND ENT_SAL." + Ope_Tal_Prov_Ent_Sal.Campo_Estatus + " = 'ABIERTA'";
                Mi_SQL = Mi_SQL + " AND SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Tipo_Bien + " = 'VEHICULO'";
                if (!String.IsNullOrEmpty(Parametros.P_Estatus)) {
                    Mi_SQL = Mi_SQL + " AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                
                Mi_SQL = Mi_SQL + " UNION ";

                Mi_SQL = Mi_SQL + " SELECT ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " AS NO_ASIGNACION_PROV_SERV";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " AS NO_SERVICIO";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " AS FOLIO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " AS FECHA_RECEPCION_REAL";
                Mi_SQL = Mi_SQL + ", 'SERVICIO CORRECTIVO' AS TIPO_SERVICIO";
                Mi_SQL = Mi_SQL + ", MUEBLE." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", CONVERT(CHAR,MUEBLE." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + ") AS NO_ECONOMICO";
                Mi_SQL = Mi_SQL + ", ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " ASIGNACION";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Prov_Ent_Sal.Tabla_Ope_Tal_Prov_Ent_Sal + " ENT_SAL ON ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " = ENT_SAL." + Ope_Tal_Prov_Ent_Sal.Campo_No_Asignacion_Prov_Serv + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos + " SERVICIOS ON ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES ON SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " MUEBLE ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = MUEBLE." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + "";
                Mi_SQL = Mi_SQL + " WHERE SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Estatus + " = 'ENTRADA_PROVEEDOR' AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = 'SERVICIO_CORRECTIVO'";
                Mi_SQL = Mi_SQL + " AND ENT_SAL." + Ope_Tal_Prov_Ent_Sal.Campo_Estatus + " = 'ABIERTA'";
                Mi_SQL = Mi_SQL + " AND SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Tipo_Bien + " = 'BIEN_MUEBLE'";
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    Mi_SQL = Mi_SQL + " AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                //Union con servicios Preventivos
                Mi_SQL = Mi_SQL + " UNION ";

                Mi_SQL = Mi_SQL + "SELECT ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " AS NO_ASIGNACION_PROV_SERV";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " AS NO_SERVICIO";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " AS FOLIO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " AS FECHA_RECEPCION_REAL";
                Mi_SQL = Mi_SQL + ", 'SERVICIO CORRECTIVO' AS TIPO_SERVICIO";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " AS NO_ECONOMICO";
                Mi_SQL = Mi_SQL + ", ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " ASIGNACION";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Prov_Ent_Sal.Tabla_Ope_Tal_Prov_Ent_Sal + " ENT_SAL ON ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " = ENT_SAL." + Ope_Tal_Prov_Ent_Sal.Campo_No_Asignacion_Prov_Serv + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos + " SERVICIOS ON ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES ON SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                Mi_SQL = Mi_SQL + " WHERE SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = 'ENTRADA_PROVEEDOR' AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = 'SERVICIO_PREVENTIVO'";
                Mi_SQL = Mi_SQL + " AND ENT_SAL." + Ope_Tal_Prov_Ent_Sal.Campo_Estatus + " = 'ABIERTA'";
                Mi_SQL = Mi_SQL + " AND SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Tipo_Bien + " = 'VEHICULO'";
                if (!String.IsNullOrEmpty(Parametros.P_Estatus)) {
                    Mi_SQL = Mi_SQL + " AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                Mi_SQL = Mi_SQL + " UNION ";
                Mi_SQL = Mi_SQL + "SELECT ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " AS NO_ASIGNACION_PROV_SERV";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " AS NO_SERVICIO";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " AS FOLIO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " AS FECHA_RECEPCION_REAL";
                Mi_SQL = Mi_SQL + ", 'SERVICIO CORRECTIVO' AS TIPO_SERVICIO";
                Mi_SQL = Mi_SQL + ", MUEBLE." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", CONVERT(CHAR,MUEBLE." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + ") AS NO_ECONOMICO";
                Mi_SQL = Mi_SQL + ", ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " ASIGNACION";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Prov_Ent_Sal.Tabla_Ope_Tal_Prov_Ent_Sal + " ENT_SAL ON ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " = ENT_SAL." + Ope_Tal_Prov_Ent_Sal.Campo_No_Asignacion_Prov_Serv + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos + " SERVICIOS ON ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES ON SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " MUEBLE ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = MUEBLE." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + "";
                Mi_SQL = Mi_SQL + " WHERE SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = 'ENTRADA_PROVEEDOR' AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = 'SERVICIO_PREVENTIVO'";
                Mi_SQL = Mi_SQL + " AND ENT_SAL." + Ope_Tal_Prov_Ent_Sal.Campo_Estatus + " = 'ABIERTA'";
                Mi_SQL = Mi_SQL + " AND SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Tipo_Bien + " = 'BIEN_MUEBLE'";
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    Mi_SQL = Mi_SQL + " AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                Mi_SQL = Mi_SQL + " ORDER BY NO_ASIGNACION_PROV_SERV";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null) {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            } catch (Exception Ex) {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Asignacion_Servicio
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un Objeto.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 1/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Consultar_Detalles_Asignacion_Servicio(Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Parametros)  {
            String Mi_SQL = null;
            Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Obj_Cargado = new Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio();
            try {
                Mi_SQL = "SELECT * FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " WHERE " + Ope_Tal_Prov_Ent_Sal.Campo_No_Asignacion_Prov_Serv + " = '" + Parametros.P_No_Asignacion_Proveedor + "'";
               
                SqlDataReader Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                while (Reader.Read()) {
                    Obj_Cargado.P_No_Servicio = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio]) : (-1);
                    Obj_Cargado.P_No_Asignacion_Proveedor = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion]) : (-1);
                    Obj_Cargado.P_Proveedor_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID].ToString())) ? Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID].ToString() : "";
                    Obj_Cargado.P_Observaciones = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Observaciones].ToString())) ? Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Observaciones].ToString() : "";
                    Obj_Cargado.P_Estatus = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus].ToString())) ? Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus].ToString() : "";
                    Obj_Cargado.P_Tipo = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio].ToString())) ? Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio].ToString() : "";
                    Obj_Cargado.P_Diagnostico = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Diagnostico].ToString())) ? Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Diagnostico].ToString() : "";
                    Obj_Cargado.P_Costo = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Costo].ToString())) ? Convert.ToDouble(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Costo]) : (-1.0);
                    }
                Reader.Close();
            } catch (Exception Ex) {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Obj_Cargado;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Seguimiento_Proveedor     
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 31/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Seguimiento_Proveedor(Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            String Mi_SQL = "";
            Object Aux; //Variable auxiliar para las consultas
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Formar Sentencia para obtener el consecutivo
                Mi_SQL = "";
                Mi_SQL = "SELECT ISNULL(MAX(";
                Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Proveedor_Servicio.Campo_No_Seguimiento + "),0)";
                Mi_SQL = Mi_SQL + " FROM ";
                Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Proveedor_Servicio.Tabla_Ope_Tal_Seguimiento_Proveedor;

                //Ejecutar consulta del consecutivo
                Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0][0];

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                {
                    Parametros.P_No_Seguimiento_Proveedor = Convert.ToInt32(Aux) + 1;
                }
                else
                    Parametros.P_No_Seguimiento_Proveedor = 1;
               
                //Insertar el nuevo registro
                Mi_SQL = "INSERT INTO " + Ope_Tal_Seguimiento_Proveedor_Servicio.Tabla_Ope_Tal_Seguimiento_Proveedor;
                Mi_SQL = Mi_SQL + " (" + Ope_Tal_Seguimiento_Proveedor_Servicio.Campo_No_Seguimiento + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seguimiento_Proveedor_Servicio.Campo_No_Asignacion + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seguimiento_Proveedor_Servicio.Campo_Observaciones + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seguimiento_Proveedor_Servicio.Campo_Tipo_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seguimiento_Proveedor_Servicio.Campo_No_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seguimiento_Proveedor_Servicio.Campo_Usuario_Creo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seguimiento_Proveedor_Servicio.Campo_Fecha_Creo + "";
                Mi_SQL = Mi_SQL + ") VALUES (" + Parametros.P_No_Seguimiento_Proveedor + "";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Asignacion_Proveedor + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Observaciones + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Servicio + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE())";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consulta_Seguimiento_Proveedor     
        ///DESCRIPCIÓN          : Consultar un registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 31/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consulta_Seguimiento_Proveedor(Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT ";
                if (!String.IsNullOrEmpty(Parametros.P_Campos_Dinamicos))
                {
                    Mi_SQL = Mi_SQL + Parametros.P_Campos_Dinamicos;
                }
                else
                {
                    Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Proveedor_Servicio.Tabla_Ope_Tal_Seguimiento_Proveedor + " .* ";
                }
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Seguimiento_Proveedor_Servicio.Tabla_Ope_Tal_Seguimiento_Proveedor;
                if (!String.IsNullOrEmpty(Parametros.P_Filtros_Dinamicos))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Parametros.P_Filtros_Dinamicos;
                }
                else
                {
                    if (Parametros.P_No_Asignacion_Proveedor > 0)
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Proveedor_Servicio.Campo_No_Asignacion + " = " + Parametros.P_No_Asignacion_Proveedor + " ";
                    }
                    
                    Mi_SQL = Mi_SQL + " ORDER BY 1 DESC";
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                    {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consulta_Seguimiento_Proveedor     
        ///DESCRIPCIÓN          : Consultar un registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 31/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consulta_Seguimiento_Mecanico(Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT ";
                if (!String.IsNullOrEmpty(Parametros.P_Campos_Dinamicos))
                {
                    Mi_SQL = Mi_SQL + Parametros.P_Campos_Dinamicos;
                }
                else
                {
                    Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Mecanico.Tabla_Ope_Tal_Seguimiento_Mecanico + " .* ";
                }
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Seguimiento_Mecanico.Tabla_Ope_Tal_Seguimiento_Mecanico;
                if (!String.IsNullOrEmpty(Parametros.P_Filtros_Dinamicos))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Parametros.P_Filtros_Dinamicos;
                }
                else
                {
                    if (Parametros.P_No_Servicio > 0)
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Mecanico.Campo_No_Servicio + " = " + Parametros.P_No_Servicio  + " ";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Tipo))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Mecanico.Campo_Tipo + " = '" + Parametros.P_Tipo + "' ";
                    }
                    Mi_SQL = Mi_SQL + " ORDER BY 1 DESC";
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                    {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultamos_Presupuesto_Existente     
        ///DESCRIPCIÓN          : Consultar Presupuesto
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 31/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static bool Consultamos_Presupuesto_Existente(Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Parametros)
        {
            bool Existe_Presupuesto = false;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            String Mi_SQL = "";
            DataTable Dt_Requisicion = new DataTable();
            DataTable Dt_Parametros = new DataTable();
            String Partida_ID = "0";
             String Programa_ID = "0";
            String FF = "0";
             String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            Cls_Ope_Tal_Consultas_Generales_Negocio Cosnultas_Generales_Ng = new Cls_Ope_Tal_Consultas_Generales_Negocio();
            try
            {
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
            //Primero Revisamos de que tipo viene el servicio PREVENTIVO/CORRECTIVO
            if (Parametros.P_Tipo.Trim().Contains("PREV"))
            {
                Mi_SQL = "SELECT ";
                Mi_SQL = Mi_SQL + " AP." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Costo;
                Mi_SQL = Mi_SQL + ", SR." + Ope_Tal_Serv_Preventivos.Campo_Reparacion;
                Mi_SQL = Mi_SQL + ", SL." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " AP ";
                Mi_SQL = Mi_SQL + " JOIN " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos + " SR ";
                Mi_SQL = Mi_SQL + " ON " + " SR." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = " + " AP." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio;
                Mi_SQL = Mi_SQL + " JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SL ";
                Mi_SQL = Mi_SQL + " ON " + " SL." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = " + " SR." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud;

                if (Parametros.P_No_Asignacion_Proveedor > 0)
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "AP." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " = " + Parametros.P_No_Asignacion_Proveedor + "";
                }

                Mi_SQL = Mi_SQL + " ORDER BY 1 DESC";

                //Ejecutamos la Sentencia SQL y llenamos el DataTable
                    Cmd.CommandText = Mi_SQL;
                    Obj_Adaptador.SelectCommand = Cmd;
                    Obj_Adaptador.Fill(Dt_Requisicion);
            }
            if (Parametros.P_Tipo.Trim().Contains("CORR"))
            {
                Mi_SQL = "SELECT ";
                Mi_SQL = Mi_SQL + " AP." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Costo;
                Mi_SQL = Mi_SQL + ", SR." + Ope_Tal_Serv_Correctivos.Campo_Reparacion;
                Mi_SQL = Mi_SQL + ", SL." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " AP ";
                Mi_SQL = Mi_SQL + " JOIN " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos + " SR ";
                Mi_SQL = Mi_SQL + " ON " + " SR." + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " = " + " AP." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio;
                Mi_SQL = Mi_SQL + " JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SL ";
                Mi_SQL = Mi_SQL + " ON " + " SL." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = " + " SR." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud;

                if (Parametros.P_No_Asignacion_Proveedor > 0)
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "AP." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " = " + Parametros.P_No_Asignacion_Proveedor + "";
                }

                //if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                //{
                //    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                //    Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + "." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN ('" + Parametros.P_Estatus.Trim() + "')";
                //}
                if (!String.IsNullOrEmpty(Parametros.P_Proveedor_ID))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "AP." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + " IN ('" + Parametros.P_Proveedor_ID.Trim() + "')";
                }

                Mi_SQL = Mi_SQL + " ORDER BY 1 DESC";

                //Ejecutamos la Sentencia SQL y llenamos el DataTable
                    Cmd.CommandText = Mi_SQL;
                    Obj_Adaptador.SelectCommand = Cmd;
                    Obj_Adaptador.Fill(Dt_Requisicion);
            }

            //Guardamos en la variable Monto_Restante la diferencia de Total_Cotizado y P_Monto_Total inicial del producto
            //Primero checamos si se va a restar o a aumentar el presupuesto
            double Disponible = 0;
            double Monto_Cotizado = double.Parse(Dt_Requisicion.Rows[0][Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Costo].ToString().Trim());
            String Dependencia_ID = Parametros.P_Dependencia_ID;// Dt_Requisicion.Rows[0][Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID].ToString().Trim();
            Cls_Tal_Parametros_Negocio Parametros_Negocio = new Cls_Tal_Parametros_Negocio();
            Parametros_Negocio.P_Cmmd = Cmd;
            Parametros_Negocio = Parametros_Negocio.Consulta_Parametros_Presupuesto(Parametros.P_No_Economico.Trim(), Parametros.P_Tipo_Bien);
            Partida_ID = Parametros_Negocio.P_Partida_ID;
            FF = Parametros_Negocio.P_Fuente_Financiamiento_ID;
            //Sentencia que ejecuta el query
                //Ejecutamos la Sentencia SQL y llenamos el DataTable
                //Cmd.CommandText = Mi_SQL;
                //Obj_Adaptador.SelectCommand = Cmd;
                //Obj_Adaptador.Fill(Dt_Parametros);

            //if (Dt_Parametros.Rows.Count > 0)
            //{
                //Partida_ID = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Partida_ID].ToString();
                //FF = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Fuente_Financiamiento].ToString();
            //}
            //Se consulta Programa de la solicitud
            Cosnultas_Generales_Ng.P_Cmmd = Cmd;
            Cosnultas_Generales_Ng.P_No_Solicitud = Parametros.P_No_Solicitud;
            Programa_ID = Cosnultas_Generales_Ng.Consultar_Proyecto_Programa_Solicitud();
            //Se toma Dependencia de Servicio
            Dependencia_ID = Parametros.P_Dependencia_ID;
            Disponible = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Disponible_Partida(FF, Programa_ID, Dependencia_ID, Partida_ID, DateTime.Today.Year.ToString(), Ope_Psp_Presupuesto_Aprobado.Campo_Disponible.Trim());
            if (Disponible >= Monto_Cotizado)
                Existe_Presupuesto = true;
            else
                Existe_Presupuesto = false;
            }
            catch (SqlException Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
            return Existe_Presupuesto;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Seguimiento_Mecanico     
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 31/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Seguimiento_Mecanico(Cls_Ope_Tal_Asignacion_Proveedor_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            String Mi_SQL = "";
            Object Aux; //Variable auxiliar para las consultas
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Formar Sentencia para obtener el consecutivo
                Mi_SQL = "";
                Mi_SQL = "SELECT ISNULL(MAX(";
                Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Mecanico.Campo_No_Seguimiento + "),0)";
                Mi_SQL = Mi_SQL + " FROM ";
                Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Mecanico.Tabla_Ope_Tal_Seguimiento_Mecanico;

                //Ejecutar consulta del consecutivo
                Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0][0];

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                {
                    Parametros.P_No_Seguimiento_Proveedor = Convert.ToInt32(Aux) + 1;
                }
                else
                    Parametros.P_No_Seguimiento_Proveedor = 1;

                //Insertar el nuevo registro
                Mi_SQL = "INSERT INTO " + Ope_Tal_Seguimiento_Mecanico.Tabla_Ope_Tal_Seguimiento_Mecanico;
                Mi_SQL = Mi_SQL + " (" + Ope_Tal_Seguimiento_Mecanico.Campo_No_Seguimiento + "";
                Mi_SQL = Mi_SQL + "," + Ope_Tal_Seguimiento_Mecanico.Campo_No_Servicio + "";
                Mi_SQL = Mi_SQL + "," + Ope_Tal_Seguimiento_Mecanico.Campo_Tipo + "";
                Mi_SQL = Mi_SQL + "," + Ope_Tal_Seguimiento_Mecanico.Campo_Mecanico_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seguimiento_Mecanico.Campo_Observaciones + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seguimiento_Mecanico.Campo_Usuario_Creo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seguimiento_Mecanico.Campo_Fecha_Creo + "";
                Mi_SQL = Mi_SQL + ") VALUES (" + Parametros.P_No_Seguimiento_Proveedor + "";
                Mi_SQL = Mi_SQL + ", " + Parametros.P_No_Servicio + "";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Mecanico_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Observaciones + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE())";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }
    }
}