﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Distribuir_RQ_Proveedores.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Sessiones;
/// <summary>
/// Summary description for Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Datos
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Operacion_Distribuir_RQ_Proveedores.Datos {
    public class Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Datos
    {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Productos_Servicios
        ///DESCRIPCIÓN: Metodo que Consulta los detalles de la Requisicion seleccionada, ya sea Producto o servicio.
        ///PARAMETROS: 1.- Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 26/Junio/2012
        ///CAUSA_MODIFICACIÓN: Adecuar Proceso a Taller Mecanico Municipal
        ///*******************************************************************************
        public static DataTable Consultar_Productos_Servicios(Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocio)
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT PRO." + Cat_Tal_Refacciones.Campo_Clave;
            Mi_SQL = Mi_SQL + ", PRO." + Cat_Tal_Refacciones.Campo_Nombre;
            Mi_SQL = Mi_SQL + ", PRO." + Cat_Tal_Refacciones.Campo_Descripcion;
            Mi_SQL = Mi_SQL + ", PRO." + Cat_Tal_Refacciones.Campo_Costo_Unitario;
            Mi_SQL = Mi_SQL + ", REQ_PRO." + Ope_Tal_Req_Refaccion.Campo_Cantidad;
            Mi_SQL = Mi_SQL + ", REQ_PRO." + Ope_Tal_Req_Refaccion.Campo_Monto_Total;
            Mi_SQL = Mi_SQL + ", REQ_PRO." + Ope_Tal_Req_Refaccion.Campo_Importe;
            Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Com_Unidades.Campo_Nombre;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID;
            Mi_SQL = Mi_SQL + "= PRO." + Cat_Tal_Refacciones.Campo_Unidad_ID + ") AS UNIDAD";
            Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQ";
            Mi_SQL = Mi_SQL + " INNER JOIN " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " REQ_PRO ";
            Mi_SQL = Mi_SQL + " ON REQ." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "=";
            Mi_SQL = Mi_SQL + " REQ_PRO." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID;
            Mi_SQL = Mi_SQL + " INNER JOIN " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRO";
            Mi_SQL = Mi_SQL + " ON PRO." + Cat_Tal_Refacciones.Campo_Refaccion_ID + "=";
            Mi_SQL = Mi_SQL + " REQ_PRO." + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID;
            Mi_SQL = Mi_SQL + " WHERE REQ." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "='" + Clase_Negocio.P_No_Requisicion.Trim() + "'";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Requisiciones
        ///DESCRIPCIÓN: Metodo que consulta las requisiciones listas para ser distribuidas a los cotizadores
        ///PARAMETROS: 1.- Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 26/Junio/2012
        ///CAUSA_MODIFICACIÓN: Adecuar Proceso a Taller Mecanico Municipal
        ///*******************************************************************************
        public static DataTable Consultar_Requisiciones(Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocio)
        {
            String Mi_SQL = "SELECT REQ." + Ope_Tal_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL = Mi_SQL + ", REQ." + Ope_Tal_Requisiciones.Campo_Folio;
            Mi_SQL = Mi_SQL + ", DEP." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
            Mi_SQL = Mi_SQL + ", REQ." + Ope_Tal_Requisiciones.Campo_Total;
            Mi_SQL = Mi_SQL + ", REQ." + Ope_Tal_Requisiciones.Campo_Tipo_Articulo;
            Mi_SQL = Mi_SQL + ", REQ." + Ope_Tal_Requisiciones.Campo_Estatus;
            Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Sap_Concepto.Campo_Clave + " +' '+ " + Cat_Sap_Concepto.Campo_Descripcion + " FROM ";
            Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
            Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
            Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
            Mi_SQL = Mi_SQL + "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
            Mi_SQL = Mi_SQL + "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = REQ." + Ope_Tal_Requisiciones.Campo_Partida_ID + "))) AS CONCEPTO";
            Mi_SQL = Mi_SQL + ", REQ." + Ope_Tal_Requisiciones.Campo_Alerta;
            Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQ";
            Mi_SQL = Mi_SQL + " JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP";
            Mi_SQL = Mi_SQL + " ON DEP." + Cat_Dependencias.Campo_Dependencia_ID + "=";
            Mi_SQL = Mi_SQL + " REQ." + Ope_Tal_Requisiciones.Campo_Dependencia_ID;
            Mi_SQL = Mi_SQL + " WHERE REQ." + Ope_Tal_Requisiciones.Campo_Tipo + "='TRANSITORIA'";
            //Mi_SQL = Mi_SQL + " AND REQ." + Ope_Tal_Requisiciones.Campo_Cotizador_ID + "='" + Cls_Sessiones.Empleado_ID +"'";
            if (Clase_Negocio.P_Cotizador_ID != null && Clase_Negocio.P_Cotizador_ID != "0")
            {
                Mi_SQL = Mi_SQL + " AND REQ." + Ope_Tal_Requisiciones.Campo_Cotizador_ID + "='" + Clase_Negocio.P_Cotizador_ID + "'";
            }
            Mi_SQL = Mi_SQL + " AND REQ." + Ope_Tal_Requisiciones.Campo_Estatus + " IN ('FILTRADA','COTIZADA-RECHAZADA')";
            Mi_SQL = Mi_SQL + " ORDER BY REQ." + Ope_Tal_Requisiciones.Campo_Requisicion_ID;


            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Detalle_Requisicion
        ///DESCRIPCIÓN: Metodo que consulta los detalles de la requisicion seleccionada en el Grid_Requisiciones
        ///PARAMETROS: 1.- Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 26/Junio/2012
        ///CAUSA_MODIFICACIÓN: Adecuar Proceso a Taller Mecanico Municipal
        ///*******************************************************************************
        public static DataTable Consultar_Detalle_Requisicion(Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocio)
        {
            String Mi_SQL = "SELECT ";
            Mi_SQL = Mi_SQL + " DEPENDENCIA." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
            Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Sap_Concepto.Campo_Clave + " +' '+ " + Cat_Sap_Concepto.Campo_Descripcion + " FROM ";
            Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
            Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
            Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
            Mi_SQL = Mi_SQL + "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
            Mi_SQL = Mi_SQL + "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = REQUISICION." + Ope_Tal_Requisiciones.Campo_Partida_ID + "))) AS CONCEPTO";
            Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Sap_Concepto.Campo_Concepto_ID + " FROM ";
            Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
            Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
            Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
            Mi_SQL = Mi_SQL + "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
            Mi_SQL = Mi_SQL + "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = REQUISICION." + Ope_Tal_Requisiciones.Campo_Partida_ID + "))) AS CONCEPTO_ID";
            Mi_SQL = Mi_SQL + ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Tipo;
            Mi_SQL = Mi_SQL + ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Folio;
            Mi_SQL = Mi_SQL + ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Fecha_Autorizacion + " AS FECHA_GENERACION";
            Mi_SQL = Mi_SQL + ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Estatus;
            Mi_SQL = Mi_SQL + ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Subtotal;
            Mi_SQL = Mi_SQL + ", REQUISICION." + Ope_Tal_Requisiciones.Campo_IEPS;
            Mi_SQL = Mi_SQL + ", REQUISICION." + Ope_Tal_Requisiciones.Campo_IVA;
            Mi_SQL = Mi_SQL + ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Total;
            Mi_SQL = Mi_SQL + ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL = Mi_SQL + ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Justificacion_Compra;
            Mi_SQL = Mi_SQL + ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Especificacion_Prod_Serv;
            Mi_SQL = Mi_SQL + ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Verificaion_Entrega;
            Mi_SQL = Mi_SQL + ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Tipo_Articulo;
            Mi_SQL = Mi_SQL + ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Codigo_Programatico;
            Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQUISICION ";
            Mi_SQL = Mi_SQL + " JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIA ";
            Mi_SQL = Mi_SQL + " ON REQUISICION." + Ope_Tal_Requisiciones.Campo_Dependencia_ID + "= DEPENDENCIA.";
            Mi_SQL = Mi_SQL + Cat_Dependencias.Campo_Dependencia_ID;
            Mi_SQL = Mi_SQL + " WHERE REQUISICION." + Ope_Tal_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL = Mi_SQL + "='" + Clase_Negocio.P_No_Requisicion.Trim() + "'";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Proveedores_Asignados
        ///DESCRIPCIÓN: Metodo que Consulta los proveedores asignados de la Requisicion seleccionada.
        ///PARAMETROS: 1.- Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 26/Junio/2012
        ///CAUSA_MODIFICACIÓN: Adecuar Proceso a Taller Mecanico Municipal
        ///*******************************************************************************
        public static DataTable Consultar_Proveedores_Asignados(Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocio)
        {
            String Mi_SQL = "";

            if (Clase_Negocio.P_Proveedor_ID != null)
            {
                Mi_SQL = "SELECT " + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Nombre;
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Compañia;
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Telefono_1 + " +' , '+";
                Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Telefono_2 + " AS TELEFONOS";
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Correo_Electronico;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID + "='" + Clase_Negocio.P_Proveedor_ID.Trim() + "'";
            }
            else
            {
                Mi_SQL = "SELECT " + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Nombre;
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Compañia;
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Telefono_1 + " +' , '+";
                Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Campo_Telefono_2 + " AS TELEFONOS";
                Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Correo_Electronico;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL = Mi_SQL + " IN (SELECT " + Ope_Tal_Propuesta_Cotizacion.Campo_Proveedor_ID;
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Propuesta_Cotizacion.Tabla_Ope_Tal_Propuesta_Cotizacion;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Propuesta_Cotizacion.Campo_No_Requisicion;
                Mi_SQL = Mi_SQL + " ='" + Clase_Negocio.P_No_Requisicion.Trim();
                Mi_SQL = Mi_SQL + "' GROUP BY " + Ope_Tal_Propuesta_Cotizacion.Campo_Proveedor_ID + ")";
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Com_Proveedores.Campo_Nombre;
            }

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Proveedores
        ///DESCRIPCIÓN: Metodo que Consulta los proveedores
        ///PARAMETROS: 1.- Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 26/Junio/2012
        ///CAUSA_MODIFICACIÓN: Adecuar Proceso a Taller Mecanico Municipal
        ///*******************************************************************************
        public static DataTable Consultar_Proveedores(Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocio)
        {
            String Mi_SQL = "SELECT " + Cat_Com_Proveedores.Campo_Proveedor_ID;
            Mi_SQL = Mi_SQL + ", ISNULL(" + Cat_Com_Proveedores.Campo_Nombre + "," + Cat_Com_Proveedores.Campo_Compañia + ") +' '+ STR(CONVERT(INT," + Cat_Com_Proveedores.Campo_Proveedor_ID + "))";
            Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Proveedores.Campo_Estatus + "='ACTIVO'";
                                    
            //Mi_SQL = Mi_SQL + " AND " + Cat_Com_Proveedores.Campo_Tipo;
            //Mi_SQL = Mi_SQL + "='TALLER'";
            Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Com_Proveedores.Campo_Nombre + " ASC";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Eliminar_Proveedores
        ///DESCRIPCIÓN: Elimina los proveedores de una RQ.
        ///PARAMETROS: 1.- Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 26/Junio/2012
        ///CAUSA_MODIFICACIÓN: Adecuar Proceso a Taller Mecanico Municipal
        ///*******************************************************************************
        public static void Eliminar_Proveedores(Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocio)
        {
            String Mi_SQL = "";

            Mi_SQL = "DELETE " + Ope_Tal_Propuesta_Cotizacion.Tabla_Ope_Tal_Propuesta_Cotizacion;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Propuesta_Cotizacion.Campo_No_Requisicion;
            Mi_SQL = Mi_SQL + " ='" + Clase_Negocio.P_No_Requisicion.Trim() + "'";
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Alta_Proveedores_Asignados
        ///DESCRIPCIÓN: Guarda los proveedores asignados a una RQ.
        ///PARAMETROS: 1.- Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 26/Junio/2012
        ///CAUSA_MODIFICACIÓN: Adecuar Proceso a Taller Mecanico Municipal
        ///*******************************************************************************
        public static bool Alta_Proveedores_Asignados(Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocio)
        {
            String Mi_SQL = "";
            bool Operacion_Realizada = false;
            //Consultamos los Productos de esta Requisicion pára darlos de alta en la Propuesta de Cotizacion
            Mi_SQL = "SELECT * FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " WHERE ";
            Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + "='" + Clase_Negocio.P_No_Requisicion + "'";
            DataTable Dt_Productos_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            try
            {

                if (Clase_Negocio.P_Dt_Proveedores != null && Dt_Productos_Requisicion.Rows.Count != 0)
                    for (int i = 0; i < Clase_Negocio.P_Dt_Proveedores.Rows.Count; i++)//For k buscara a los proveedores seleccionados 
                    {
                        for (int y = 0; y < Dt_Productos_Requisicion.Rows.Count; y++)
                        {
                            Mi_SQL = "INSERT INTO " + Ope_Tal_Propuesta_Cotizacion.Tabla_Ope_Tal_Propuesta_Cotizacion;
                            Mi_SQL = Mi_SQL + "(" + Ope_Tal_Propuesta_Cotizacion.Campo_No_Propuesta_Cotizacion;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Ope_Tal_Req_Refaccion_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_No_Requisicion;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Cantidad;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Refaccion_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Tipo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Nombre_Producto_Servicion;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Proveedor_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Nombre_Cotizador;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Fecha_Creo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Usuario_Creo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_IVA_Cotizado;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_IEPS_Cotizado;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Precio_U_Con_Imp_Cotizado;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Precio_U_Sin_Imp_Cotizado;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Subtota_Cotizado;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Total_Cotizado;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Estatus;
                            Mi_SQL = Mi_SQL + ") VALUES(" + Obtener_Consecutivo(Ope_Tal_Propuesta_Cotizacion.Campo_No_Propuesta_Cotizacion, Ope_Tal_Propuesta_Cotizacion.Tabla_Ope_Tal_Propuesta_Cotizacion);
                            Mi_SQL = Mi_SQL + ",'" + Dt_Productos_Requisicion.Rows[y][Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID].ToString().Trim();
                            Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_No_Requisicion;
                            Mi_SQL = Mi_SQL + "','" + Dt_Productos_Requisicion.Rows[y][Ope_Tal_Req_Refaccion.Campo_Cantidad].ToString().Trim();
                            Mi_SQL = Mi_SQL + "','" + Dt_Productos_Requisicion.Rows[y][Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID].ToString().Trim();
                            Mi_SQL = Mi_SQL + "','" + Dt_Productos_Requisicion.Rows[y][Ope_Tal_Req_Refaccion.Campo_Tipo].ToString().Trim();
                            Mi_SQL = Mi_SQL + "','" + Dt_Productos_Requisicion.Rows[y][Ope_Tal_Req_Refaccion.Campo_Nombre_Producto_Servicio].ToString().Trim();
                            Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Proveedores.Rows[i]["Proveedor_ID"];
                            Mi_SQL = Mi_SQL + "','" + Cls_Sessiones.Nombre_Empleado;
                            Mi_SQL = Mi_SQL + "',GETDATE(),'" + Cls_Sessiones.Nombre_Empleado + "',0,0,0,0,0,0,'EN CONSTRUCCION')";

                            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        }//fin del for y
                    }//fin del for i
                //Actualizamos la requisicion 

                Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Requisiciones.Campo_Estatus + "='PROVEEDOR'";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "='" + Clase_Negocio.P_No_Requisicion + "'";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                //REalizamos el insert del historial
                Cls_Util.Registrar_Historial("PROVEEDOR", Clase_Negocio.P_No_Requisicion);

                Operacion_Realizada = true;
            }//Fin del try 
            catch
            {
                Operacion_Realizada = false;
            }
            return Operacion_Realizada;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 26/Junio/2012
        ///CAUSA_MODIFICACIÓN: Adecuar Proceso a Taller Mecanico Municipal
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA METODO: Consulta_Parametros
        ///        DESCRIPCIÓN: Consulta los Parametros
        ///         PARAMETROS: 1.-
        ///                     2.-
        ///               CREO: 
        ///         FECHA_CREO: 
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 26/Junio/2012
        ///CAUSA_MODIFICACIÓN: Adecuar Proceso a Taller Mecanico Municipal
        ///*******************************************************************************
        public static DataTable Consulta_Parametros()
        {
            DataTable Dt_Temporal = null;
            String Mi_SQL;
            try
            {
                Mi_SQL = "SELECT " +
                         Apl_Parametros.Campo_Servidor_Correo + ", " +
                         Apl_Parametros.Campo_Correo_Saliente + ", " +
                         Apl_Parametros.Campo_Password_Correo +

                    " FROM " + Apl_Parametros.Tabla_Apl_Parametros;

                Dt_Temporal = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Temporal;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }


        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Email_Proveedores
        ///DESCRIPCIÓN: Consulta el Email de los proveedores
        ///PARAMETROS:
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 26 Mayo 2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Email_Proveedores(Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocio)
        {
            String Mi_SQL = "SELECT " + Cat_Com_Proveedores.Campo_Correo_Electronico;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID;
            Mi_SQL = Mi_SQL + " IN (SELECT " + Ope_Com_Propuesta_Cotizacion.Campo_Proveedor_ID;
            Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Propuesta_Cotizacion.Tabla_Ope_Com_Propuesta_Cotizacion;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Propuesta_Cotizacion.Campo_No_Requisicion;
            Mi_SQL = Mi_SQL + " ='" + Clase_Negocio.P_No_Requisicion + "')";

            DataTable Dt_Proveedores = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Proveedores;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Datos_Cotizador
        ///DESCRIPCIÓN: Consulta los datos del cotizador
        ///PARAMETROS:  
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 26 Mayo 2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Datos_Cotizador(Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocio)
        {
            String Mi_SQL = "SELECT " + Cat_Tal_Cotizadores.Campo_Correo;
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Cotizadores.Campo_Password_Correo;
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Cotizadores.Campo_IP_Correo_Saliente;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Cotizadores.Tabla_Cat_Tal_Cotizadores;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Cotizadores.Campo_Empleado_ID;
            Mi_SQL = Mi_SQL + "='" + Cls_Sessiones.Empleado_ID + "'";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Comentarios
        ///DESCRIPCIÓN: Consulta los comentarios
        ///PARAMETROS:  
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 26 Mayo 2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Comentarios(Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Negocio)
        {
            String Mi_SQL = "SELECT " + Ope_Tal_Req_Observaciones.Campo_Estatus;
            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Observaciones.Campo_Comentario;
            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Observaciones.Campo_Usuario_Creo;
            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Observaciones.Campo_Fecha_Creo + "  AS " + Ope_Tal_Req_Observaciones.Campo_Fecha_Creo;
            Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Req_Observaciones.Tabla_Ope_Tal_Req_Observaciones;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Req_Observaciones.Campo_Requisicion_ID;
            Mi_SQL = Mi_SQL + "='" + Clase_Negocio.P_No_Requisicion.Trim() + "'";
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Req_Origen
        ///DESCRIPCIÓN: Obtiene la RQ de origen.
        ///PARAMETROS:  
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 26 Mayo 2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Req_Origen(Cls_Ope_Tal_Distribuir_Requisiciones_Prov_Negocio Clase_Datos)
        {
            String Mi_SQL = "SELECT  REQ_ORIGEN_ID ";
            Mi_SQL = Mi_SQL + " FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Com_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL = Mi_SQL + "=" + Clase_Datos.P_No_Requisicion;
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }        
    }
}
