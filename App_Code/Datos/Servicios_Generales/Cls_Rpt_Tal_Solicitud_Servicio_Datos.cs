﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Reporte_Solicitud_Servicio.Negocio;

/// <summary>
/// Summary description for Cls_Rpt_Tal_Solicitud_Servicio_Datos
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Reporte_Solicitud_Servicio.Datos {
    public class Cls_Rpt_Tal_Solicitud_Servicio_Datos {
        #region Metodos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Datos_Solicitud      
            ///DESCRIPCIÓN          : Carga los Datos Generales de la Solicitud
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a
            ///                         Consultar en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 07/Julio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consulta_Datos_Solicitud(Cls_Rpt_Tal_Solicitud_Servicio_Negocio Parametros) { 
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                SqlTransaction Trans = null;
                SqlCommand Cmd = new SqlCommand();
                SqlConnection Cn = new SqlConnection();
                SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
                Boolean Entro_Where = false;

                try {
                    
                        Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                        Cn.Open();
                        Trans = Cn.BeginTransaction();
                        Cmd.Connection = Trans.Connection;
                        Cmd.Transaction = Trans;
                    
                    Mi_SQL = "SELECT SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", ISNULL(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + ", 'NO AUTORIZADA') AS FOLIO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Prog + " AS FECHA_RECEPCION_PROGRAMADA";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Prog + " AS FECHA_RECEPCION_REAL";
                    Mi_SQL = Mi_SQL + ", CASE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                    Mi_SQL = Mi_SQL + " WHEN 'VERIFICACION' THEN 'VERIFICACION' WHEN 'SERVICIO_CORRECTIVO' THEN 'SERVICIO CORRECTIVO' WHEN 'SERVICIO_PREVENTIVO' THEN 'SERVICIO PREVENTIVO' WHEN 'REVISTA_MECANICA' THEN 'REVISTA MECANICA' END AS TIPO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                    Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", STR(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + ") AS NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " AS NO_ECONOMICO";
                    Mi_SQL = Mi_SQL + ", (TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Descripcion + " +' '+ MARCAS." + Cat_Com_Marcas.Campo_Nombre + " +' '+ VEHICULOS." + Ope_Pat_Vehiculos.Campo_Modelo + ") AS VEHICULO";
                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Placas + " AS PLACAS";
                    Mi_SQL = Mi_SQL + ", STR(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Anio_Fabricacion + ") AS ANIO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Kilometraje + " AS KILOMETRAJE_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL = Mi_SQL + ", 'CODIGO_PROGRAMATICO'" + " AS CODIGO_PROGRAMATICO";
                    Mi_SQL = Mi_SQL + ", '0.0'" + " AS COSTO_TOTAL";
                    //Consulta interna para obtener nombre de empleado que entrego la unidad
                    Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + "+' '+" + Cat_Empleados.Campo_Nombre;
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " WHERE " + Cat_Empleados.Campo_Empleado_ID + " IN (";
                    Mi_SQL = Mi_SQL + " SELECT " + Ope_Tal_Entradas_Vehiculos.Campo_Empleado_Entrego_ID + " FROM " + Ope_Tal_Entradas_Vehiculos.Tabla_Ope_Tal_Entradas_Vehiculos;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Entradas_Vehiculos.Campo_No_Solicitud + " = " + Parametros.P_No_Solicitud + ")) EMPLEADO_ENTREGA";
                    //Consulta interna para obtener el seguimiento de terminacion del servicio
                    Mi_SQL = Mi_SQL + ",'' SERVICIO_REALIZADO";
                    //Consulta interna para obtener nombre de empleado que recibe la unidad
                    Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + "+' '+" + Cat_Empleados.Campo_Nombre;
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " WHERE " + Cat_Empleados.Campo_Empleado_ID + " IN (";
                    Mi_SQL = Mi_SQL + " SELECT " + Ope_Tal_Salidas_Vehiculos.Campo_Empleado_Recibio_ID + " FROM " + Ope_Tal_Salidas_Vehiculos.Tabla_Ope_Tal_Salidas_Vehiculos;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Salidas_Vehiculos.Campo_No_Solicitud + " = " + Parametros.P_No_Solicitud + ")) EMPLEADO_RECIBE";

                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + " INNER JOIN " + Cat_Pat_Tipos_Vehiculo.Tabla_Cat_Pat_Tipos_Vehiculo + " TIPOS_VEHICULO ON VEHICULOS." + Ope_Pat_Vehiculos.Campo_Tipo_Vehiculo_ID + " = TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Tipo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " MARCAS ON VEHICULOS." + Ope_Pat_Vehiculos.Campo_Marca_ID + " = MARCAS." + Cat_Com_Marcas.Campo_Marca_ID + "";
                    
                    if (Parametros.P_No_Solicitud > (-1))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " IN ('" + Parametros.P_No_Solicitud + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Tipo_Bien))
                    {
                        if (Parametros.P_Tipo_Bien.Contains("MUEBLE"))
                        {
                            Entro_Where = false;
                            //Union con Tabla de Bienes Muebles
                            Mi_SQL = " SELECT SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                            Mi_SQL = Mi_SQL + ", ISNULL(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + ", 'NO AUTORIZADA') AS FOLIO_SOLICITUD";
                            Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                            Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Prog + " AS FECHA_RECEPCION_PROGRAMADA";
                            Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Prog + " AS FECHA_RECEPCION_REAL";
                            Mi_SQL = Mi_SQL + ", CASE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                            Mi_SQL = Mi_SQL + " WHEN 'VERIFICACION' THEN 'VERIFICACION' WHEN 'SERVICIO_CORRECTIVO' THEN 'SERVICIO CORRECTIVO' WHEN 'SERVICIO_PREVENTIVO' THEN 'SERVICIO PREVENTIVO' WHEN 'REVISTA_MECANICA' THEN 'REVISTA MECANICA' END AS TIPO_SOLICITUD";
                            Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                            Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                            Mi_SQL = Mi_SQL + ", STR(MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + ") AS NO_INVENTARIO";
                            Mi_SQL = Mi_SQL + ", STR(MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + ") AS NO_ECONOMICO";
                            Mi_SQL = Mi_SQL + ", (MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Nombre + " +' '+ MARCAS." + Cat_Com_Marcas.Campo_Nombre + " +' '+ MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Modelo + ") AS VEHICULO";
                            Mi_SQL = Mi_SQL + ", MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie + " AS PLACAS";
                            Mi_SQL = Mi_SQL + ", '0' AS ANIO";
                            Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Kilometraje + " AS KILOMETRAJE_SOLICITUD";
                            Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                            Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " AS ESTATUS";
                            Mi_SQL = Mi_SQL + ", 'CODIGO_PROGRAMATICO'" + " AS CODIGO_PROGRAMATICO";
                            Mi_SQL = Mi_SQL + ", '0.0'" + " AS COSTO_TOTAL";
                            //Consulta interna para obtener nombre de empleado que entrego la unidad
                            Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + "+' '+" + Cat_Empleados.Campo_Nombre;
                            Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " WHERE " + Cat_Empleados.Campo_Empleado_ID + " IN (";
                            Mi_SQL = Mi_SQL + " SELECT " + Ope_Tal_Entradas_Vehiculos.Campo_Empleado_Entrego_ID + " FROM " + Ope_Tal_Entradas_Vehiculos.Tabla_Ope_Tal_Entradas_Vehiculos;
                            Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Entradas_Vehiculos.Campo_No_Solicitud + " = " + Parametros.P_No_Solicitud + ")) EMPLEADO_ENTREGA";
                            //Consulta interna para obtener el seguimiento de terminacion del servicio
                            Mi_SQL = Mi_SQL + ",'' SERVICIO_REALIZADO";
                            //Consulta interna para obtener nombre de empleado que recibe la unidad
                            Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Empleados.Campo_Apellido_Paterno + "+' '+" + Cat_Empleados.Campo_Apellido_Materno + "+' '+" + Cat_Empleados.Campo_Nombre;
                            Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " WHERE " + Cat_Empleados.Campo_Empleado_ID + " IN (";
                            Mi_SQL = Mi_SQL + " SELECT " + Ope_Tal_Salidas_Vehiculos.Campo_Empleado_Recibio_ID + " FROM " + Ope_Tal_Salidas_Vehiculos.Tabla_Ope_Tal_Salidas_Vehiculos;
                            Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Salidas_Vehiculos.Campo_No_Solicitud + " = " + Parametros.P_No_Solicitud + ")) EMPLEADO_RECIBE";

                            Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES";
                            Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                            Mi_SQL = Mi_SQL + " INNER JOIN " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " MUEBLES ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + "";
                            Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " MARCAS ON MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Marca_ID + " = MARCAS." + Cat_Com_Marcas.Campo_Marca_ID + "";
                            if (Parametros.P_No_Solicitud > (-1))
                            {
                                if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                                Mi_SQL = Mi_SQL + "SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " IN ('" + Parametros.P_No_Solicitud + "')";
                            }
                        }
                    }
                    //Ejecutar Consulta
                    Cmd.CommandText = Mi_SQL;
                    Obj_Adaptador.SelectCommand = Cmd;
                    Obj_Adaptador.Fill(Dt_Datos);

                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }
        
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Datos_Servicio_Correctivo      
            ///DESCRIPCIÓN          : Carga los Datos Generales del Servicio Correctivo
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a
            ///                         Consultar en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 09/Julio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consulta_Datos_Servicio_Correctivo(Cls_Rpt_Tal_Solicitud_Servicio_Negocio Parametros) { 
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try {
                    Mi_SQL = "SELECT SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " AS NO_SERVICIO ";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Reparacion + " AS REPARACION";
                    Mi_SQL = Mi_SQL + ", ( '[ '+ EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + "  +']  '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Nombre + " ) AS MECANICO";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Diagnostico + " AS DIAGNOSTICO_MECANICO";
                    Mi_SQL = Mi_SQL + ", ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " AS ESTATUS_PROVEEDOR";
                    Mi_SQL = Mi_SQL + ", ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Diagnostico + " AS DIAGNOSTICO_PROVEEDOR";
                    Mi_SQL = Mi_SQL + ", ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " AS TIPO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", '[ '+ PROVEEDORES." + Cat_Com_Proveedores.Campo_RFC + "  +']  '+ PROVEEDORES." + Cat_Com_Proveedores.Campo_Compañia + " AS PROVEEDOR";

                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos + " SERVICIOS";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " MECANICOS ON SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Mecanico_ID + " = MECANICOS." + Cat_Tal_Mecanicos.Campo_Mecanico_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS ON MECANICOS." + Cat_Tal_Mecanicos.Campo_Empleado_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " ASIG_PROV ON SERVICIOS.NO_SERVICIO = ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " AND ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = 'SERVICIO_CORRECTIVO' AND ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN ('ACEPTADO','REPARADO', 'POR VALIDAR')";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDORES ON ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + " = PROVEEDORES." + Cat_Com_Proveedores.Campo_Proveedor_ID + "";
                    
                    if (Parametros.P_No_Solicitud > (-1)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Solicitud + " IN ('" + Parametros.P_No_Solicitud + "')";
                    }
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }



            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Ultimo_Seg_Proveedor      
            ///DESCRIPCIÓN          : Carga los Datos Generales del Servicio Correctivo
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a
            ///                         Consultar en la Base de Datos.
            ///CREO                 : Jesus Toledo
            ///FECHA_CREO           : 02/Mayo/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consulta_Ultimo_Seg_Proveedor(Cls_Rpt_Tal_Solicitud_Servicio_Negocio Parametros)
            {
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                try
                {
                    Mi_SQL = Mi_SQL + "SELECT ISNULL(SERVICIOS." + Ope_Tal_Seguimiento_Proveedor_Servicio.Campo_Observaciones + ",'') AS SERVICIO_REALIZADO";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Seguimiento_Proveedor_Servicio.Tabla_Ope_Tal_Seguimiento_Proveedor + " SERVICIOS";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Seguimiento_Proveedor_Servicio.Campo_No_Seguimiento + " IN(SELECT MAX(";
                    Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Proveedor_Servicio.Campo_No_Seguimiento + ") FROM ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Proveedor_Servicio.Tabla_Ope_Tal_Seguimiento_Proveedor + " WHERE ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Proveedor_Servicio.Campo_No_Servicio + " = " + Parametros.P_No_Servicio;
                    Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Seguimiento_Proveedor_Servicio.Campo_Tipo_Servicio + " = '" + Parametros.P_Tipo_Servicio + "')";
                    
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                    {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Ultimo_Seg_Mecanico      
            ///DESCRIPCIÓN          : Carga los Datos Generales del Servicio Correctivo
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a
            ///                         Consultar en la Base de Datos.
            ///CREO                 : Jesus Toledo
            ///FECHA_CREO           : 02/Mayo/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consulta_Ultimo_Seg_Mecanico(Cls_Rpt_Tal_Solicitud_Servicio_Negocio Parametros)
            {
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try
                {
                    Mi_SQL = Mi_SQL + "SELECT ISNULL(SERVICIOS." + Ope_Tal_Seguimiento_Mecanico.Campo_Observaciones + ",'') AS SERVICIO_REALIZADO";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Seguimiento_Mecanico.Tabla_Ope_Tal_Seguimiento_Mecanico + " SERVICIOS";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Seguimiento_Mecanico.Campo_No_Seguimiento + " IN(SELECT MAX(";
                    Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Mecanico.Campo_No_Seguimiento + ") FROM ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Mecanico.Tabla_Ope_Tal_Seguimiento_Mecanico + " WHERE ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Seguimiento_Mecanico.Campo_No_Servicio + " = " + Parametros.P_No_Servicio;
                    Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Seguimiento_Mecanico.Campo_Tipo + " = '" + Parametros.P_Tipo_Servicio + "')";

                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                    {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Datos_Servicio_Preventivo      
            ///DESCRIPCIÓN          : Carga los Datos Generales del Servicio Preventivo
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a
            ///                         Consultar en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 09/Julio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consulta_Datos_Servicio_Preventivo(Cls_Rpt_Tal_Solicitud_Servicio_Negocio Parametros) { 
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try {
                    Mi_SQL = "SELECT SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " AS NO_SERVICIO ";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Reparacion + " AS REPARACION";
                    Mi_SQL = Mi_SQL + ", ( '[ '+ EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + "  +']  '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Nombre + " ) AS MECANICO";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Diagnostico + " AS DIAGNOSTICO_MECANICO";
                    Mi_SQL = Mi_SQL + ", ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " AS ESTATUS_PROVEEDOR";
                    Mi_SQL = Mi_SQL + ", ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Diagnostico + " AS DIAGNOSTICO_PROVEEDOR";
                    Mi_SQL = Mi_SQL + ", ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " AS TIPO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Reparacion + " AS REPARACION";
                    Mi_SQL = Mi_SQL + ", '['+ PROVEEDORES." + Cat_Com_Proveedores.Campo_RFC + " +'] '+ PROVEEDORES." + Cat_Com_Proveedores.Campo_Compañia + " AS PROVEEDOR";

                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos + " SERVICIOS";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " MECANICOS ON SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Mecanico_ID + " = MECANICOS." + Cat_Tal_Mecanicos.Campo_Mecanico_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS ON MECANICOS." + Cat_Tal_Mecanicos.Campo_Empleado_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " ASIG_PROV ON SERVICIOS.NO_SERVICIO = ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " AND ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = 'SERVICIO_PREVENTIVO' AND ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN ('ACEPTADO','REPARADO', 'POR VALIDAR')";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDORES ON ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + " = PROVEEDORES." + Cat_Com_Proveedores.Campo_Proveedor_ID + "";
                    
                    if (Parametros.P_No_Solicitud > (-1)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Solicitud + " IN ('" + Parametros.P_No_Solicitud + "')";
                    }
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }
        
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Datos_Revista_Mecanica      
            ///DESCRIPCIÓN          : Carga los Datos Generales de la Revista Mecanica.
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a
            ///                         Consultar en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 09/Julio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consulta_Datos_Revista_Mecanica(Cls_Rpt_Tal_Solicitud_Servicio_Negocio Parametros) { 
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try {
                    Mi_SQL = "SELECT REV_MECANICA." + Ope_Tal_Rev_Mecanica.Campo_No_Registro + " AS NO_REGISTRO ";
                    Mi_SQL = Mi_SQL + ", REV_MECANICA." + Ope_Tal_Rev_Mecanica.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", ( '[ '+ EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + "  +']  '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Nombre + " ) AS MECANICO";
                    Mi_SQL = Mi_SQL + ", REV_MECANICA." + Ope_Tal_Rev_Mecanica.Campo_Diagnostico + " AS DIAGNOSTICO_MECANICO";

                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Rev_Mecanica.Tabla_Ope_Tal_Rev_Mecanica + " REV_MECANICA";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " MECANICOS ON REV_MECANICA." + Ope_Tal_Rev_Mecanica.Campo_Mecanico_ID + " = MECANICOS." + Cat_Tal_Mecanicos.Campo_Mecanico_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS ON MECANICOS." + Cat_Tal_Mecanicos.Campo_Empleado_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + "";
                    
                    if (Parametros.P_No_Solicitud > (-1)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "REV_MECANICA." + Ope_Tal_Rev_Mecanica.Campo_No_Solicitud + " IN ('" + Parametros.P_No_Solicitud + "')";
                    }
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Costo_Total_Servicio      
            ///DESCRIPCIÓN          : Carga el costo total del servicio interno
            ///PARAMETROS           : 
            ///                     1.  No_Orden_Compra. Contiene el numero de la orden de 
            ///                     compra de las refacciones utilizadas en el servicio
            ///CREO                 : Jesus Toledo Rodriguez
            ///FECHA_CREO           : 09/Julio/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Double Consulta_Costo_Total_Servicio(String P_No_Solicitud)
            {
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                Object Aux = null;
                Double Resultado = 0;
                try
                {
                    Mi_SQL = "SELECT ISNULL(SUM(f." + Ope_Tal_Registro_Facturas.Campo_Total_Factura + "),0.0) COSTO_SERVICIO ";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas + " f ";
                    Mi_SQL = Mi_SQL + " inner join " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " o on f." + Ope_Tal_Registro_Facturas.Campo_No_Orden_Compra + " = o." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra;
                    Mi_SQL = Mi_SQL + " inner join " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " r on o." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = r." + Ope_Tal_Requisiciones.Campo_No_Orden_Compra;

                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "r." + Ope_Tal_Requisiciones.Campo_No_Solicitud + " IN ('" + P_No_Solicitud + "')";
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                    {
                        Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Aux != null)
                    {
                        Resultado = Double.Parse(Aux.ToString());
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Resultado;
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Costo_Total_Servicio      
            ///DESCRIPCIÓN          : Carga el costo total del servicio interno
            ///PARAMETROS           : 
            ///                     1.  No_Orden_Compra. Contiene el numero de la orden de 
            ///                     compra de las refacciones utilizadas en el servicio
            ///CREO                 : Jesus Toledo Rodriguez
            ///FECHA_CREO           : 09/Julio/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Double Consulta_Costo_Total_Servicio(String No_Servicio, String Tipo_Servicio)
            {
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                Object Aux = null;
                Double Resultado = 0;
                try
                {
                    Mi_SQL = "SELECT ISNULL(SUM(f." + Ope_Tal_Facturas_Servicio.Campo_Total_Factura + "),0.0) COSTO_SERVICIO ";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio + " f ";

                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "f." + Ope_Tal_Facturas_Servicio.Campo_No_Servicio + " IN ('" + No_Servicio + "')";
                    if (!String.IsNullOrEmpty(Tipo_Servicio))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "f." + Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio + " IN ('" + Tipo_Servicio + "')";
                    }
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                    {
                        Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Aux != null)
                    {
                        Resultado = Double.Parse(Aux.ToString());
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Resultado;
            }
        #endregion	    
	}
}
