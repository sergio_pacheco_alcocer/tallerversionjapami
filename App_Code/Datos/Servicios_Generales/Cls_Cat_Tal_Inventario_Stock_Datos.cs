﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Catalogo_Inventario_Stock.Negocio;

/// <summary>
/// Summary description for Cls_Cat_Tal_Inventario_Stock_Datos
/// </summary>
namespace JAPAMI.Taller_Mecanico.Catalogo_Inventario_Stock.Datos
{
    public class Cls_Cat_Tal_Inventario_Stock_Datos
    {
        #region Metodos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Salidas_Express
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 1.- Parametros. Contiene los parametros que se van a utilizar para
            ///                           hacer la consulta de la Base de Datos.
            ///CREO                 : Salvador Vázquez Camacho
            ///FECHA_CREO           : 29/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consultar_Salidas_Express(Cls_Cat_Tal_Inventario_Stock_Negocio Parametros)
            {
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try
                {
                    Mi_SQL = "SELECT SALIDAS_EXPRESS." + Ope_Tal_Man_Val_Ref_Det.Campo_No_Detalle + " AS NO_DETALLE";
                    Mi_SQL = Mi_SQL + ", SALIDAS_EXPRESS." + Ope_Tal_Man_Val_Ref_Det.Campo_No_Registro + " AS NO_REGISTRO";
                    Mi_SQL = Mi_SQL + ", SALIDAS_EXPRESS." + Ope_Tal_Man_Val_Ref_Det.Campo_Refaccion_ID + " AS REFACCION_ID";
                    Mi_SQL = Mi_SQL + ", SALIDAS_EXPRESS." + Ope_Tal_Man_Val_Ref_Det.Campo_Cantidad + " AS CANTIDAD";
                    Mi_SQL = Mi_SQL + ", SALIDAS_EXPRESS." + Ope_Tal_Man_Val_Ref_Det.Campo_Costo_Unitario + " AS COSTO_UNITARIO";
                    Mi_SQL = Mi_SQL + ", SALIDAS_EXPRESS." + Ope_Tal_Man_Val_Ref_Det.Campo_Costo_Total + " AS COSTO_TOTAL";
                    Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Clave;
                    Mi_SQL = Mi_SQL + " +' - '+ REFACCIONES." + Cat_Tal_Refacciones.Campo_Nombre + " AS REFACCIONES_CLAVE_NOMBRE";
                    Mi_SQL = Mi_SQL + ", ISNULL(SALIDAS_EXPRESS." + Ope_Tal_Man_Val_Ref_Det.Campo_Fecha_Modifico + ", SALIDAS_EXPRESS." + Ope_Tal_Man_Val_Ref_Det.Campo_Fecha_Creo + ") AS FECHA";
                    Mi_SQL = Mi_SQL + ", ISNULL(SALIDAS_EXPRESS." + Ope_Tal_Man_Val_Ref_Det.Campo_Usuario_Modifico + ",SALIDAS_EXPRESS." + Ope_Tal_Man_Val_Ref_Det.Campo_Usuario_Creo + ")  AS REALIZO_MOVIMIENTO";
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " REFACCIONES";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Man_Val_Ref_Det.Tabla_Ope_Tal_Man_Val_Ref_Det + " SALIDAS_EXPRESS";
                    Mi_SQL = Mi_SQL + " ON SALIDAS_EXPRESS." + Ope_Tal_Man_Val_Ref_Det.Campo_Refaccion_ID + " = REFACCIONES." + Cat_Tal_Refacciones.Campo_Refaccion_ID;

                    if (!String.IsNullOrEmpty(Parametros.P_Refaccion_ID))
                    {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "REFACCIONES." + Cat_Tal_Refacciones.Campo_Clave + " = '" + Parametros.P_Refaccion_ID + "'";
                    }
                    Mi_SQL = Mi_SQL + " ORDER BY CLAVE DESC";
                    
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                    {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Salidas
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 1.- Parametros. Contiene los parametros que se van a utilizar para
            ///                           hacer la consulta de la Base de Datos.
            ///CREO                 : Salvador Vázquez Camacho
            ///FECHA_CREO           : 29/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consultar_Salidas_Almacen(Cls_Cat_Tal_Inventario_Stock_Negocio Parametros)
            {
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try
                {
                    Mi_SQL = "SELECT SALIDAS_ALMACEN_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_No_Salida + " AS NO_SALIDA";
                    Mi_SQL = Mi_SQL + ", SALIDAS_ALMACEN_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Refaccion_ID + " AS REFACCION_ID";
                    Mi_SQL = Mi_SQL + ", SALIDAS_ALMACEN_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Cantidad + " AS CANTIDAD";
                    Mi_SQL = Mi_SQL + ", SALIDAS_ALMACEN_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Costo + " AS COSTO";
                    Mi_SQL = Mi_SQL + ", SALIDAS_ALMACEN_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Costo_Promedio + " AS COSTO_PROMEDIO";
                    Mi_SQL = Mi_SQL + ", SALIDAS_ALMACEN_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Importe + " AS IMPORTE";
                    Mi_SQL = Mi_SQL + ", SALIDAS_ALMACEN_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Subtotal + " AS SUBTOTAL";
                    Mi_SQL = Mi_SQL + ", SALIDAS_ALMACEN_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_IVA + " AS IVA";
                    Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Clave;
                    Mi_SQL = Mi_SQL + " +' - '+ REFACCIONES." + Cat_Tal_Refacciones.Campo_Nombre + " AS REFACCIONES_CLAVE_NOMBRE";
                    Mi_SQL = Mi_SQL + ", ISNULL(SALIDAS_ALMACEN." + Tal_Alm_Salidas.Campo_Fecha_Modifico + ", SALIDAS_ALMACEN." + Tal_Alm_Salidas.Campo_Fecha_Creo + ") AS FECHA";
                    Mi_SQL = Mi_SQL + ", ISNULL(SALIDAS_ALMACEN." + Tal_Alm_Salidas.Campo_Usuario_Modifico + ", SALIDAS_ALMACEN." + Tal_Alm_Salidas.Campo_Usuario_Creo + ") AS REALIZO_MOVIMIENTO";
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " REFACCIONES";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Tal_Alm_Salidas_Detalles.Tabla_Tal_Alm_Salidas_Detalles + " SALIDAS_ALMACEN_DETALLES";
                    Mi_SQL = Mi_SQL + " ON SALIDAS_ALMACEN_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Refaccion_ID + " = REFACCIONES." + Cat_Tal_Refacciones.Campo_Refaccion_ID;
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + " SALIDAS_ALMACEN";
                    Mi_SQL = Mi_SQL + " ON SALIDAS_ALMACEN_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_No_Salida + " = SALIDAS_ALMACEN." + Tal_Alm_Salidas.Campo_No_Salida;

                    if (!String.IsNullOrEmpty(Parametros.P_Refaccion_ID))
                    {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "REFACCIONES." + Cat_Tal_Refacciones.Campo_Clave + " = '" + Parametros.P_Refaccion_ID + "'";
                    }
                    Mi_SQL = Mi_SQL + " ORDER BY CLAVE DESC";

                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                    {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }
            
        #endregion
    }
}