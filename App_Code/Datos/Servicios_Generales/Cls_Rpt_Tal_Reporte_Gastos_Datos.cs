﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Reporte_Gastos.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

namespace JAPAMI.Taller_Mecanico.Reporte_Gastos.Datos {
    public class Cls_Rpt_Tal_Reporte_Gastos_Datos {
        #region Metodos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Servicios_Preventivos      
            ///DESCRIPCIÓN          : Saca un listado de las Solicitudes Cargadas con 
            ///                       Servicio Preventivo
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a
            ///                         Consultar en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 24/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
        public static DataTable Consulta_Servicios_Preventivos(Cls_Rpt_Tal_Reporte_Gastos_Negocio Parametros)
        { 
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try
                {
                    Mi_SQL = "SELECT TO_CHAR(ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_No_Entrada + ") AS NO_ENTRADA";
                    Mi_SQL = Mi_SQL + ", ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Entrada + " AS FECHA_RECEPCION";
                    Mi_SQL = Mi_SQL + ", ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Kilometraje + " AS KILOMETRAJE";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", ISNULL(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + ", 'NO AUTORIZADA') AS FOLIO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", DECODE(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                    Mi_SQL = Mi_SQL + ", 'SERVICIO_CORRECTIVO','SERVICIO CORRECTIVO','SERVICIO_PREVENTIVO','SERVICIO PREVENTIVO','REVISTA_MECANICA','REVISTA MECANICA') AS TIPO_SOLICIUD";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                    Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", TO_CHAR(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + ") AS NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", TO_CHAR(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + ") AS NO_ECONOMICO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Diagnostico + " AS DIAGNOSTICO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL = Mi_SQL + ", TRIM(EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + "";
                    Mi_SQL = Mi_SQL + " +' - '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Nombre + ") AS MECANICO";

                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Entradas_Vehiculos.Tabla_Ope_Tal_Entradas_Vehiculos + " ENTRADAS";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES";
                    Mi_SQL = Mi_SQL + " ON ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos + " SERVICIOS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " MECANICOS";
                    Mi_SQL = Mi_SQL + " ON SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Mecanico_ID + " = MECANICOS." + Cat_Tal_Mecanicos.Campo_Mecanico_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS";
                    Mi_SQL = Mi_SQL + " ON MECANICOS." + Cat_Tal_Mecanicos.Campo_Empleado_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + "";
                    Mi_SQL = Mi_SQL + " WHERE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " = 'SERVICIO_PREVENTIVO'"; Entro_Where = true;
                    if (!String.IsNullOrEmpty(Parametros.P_Vehiculo_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " IN ('" + Parametros.P_Vehiculo_ID + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Tipo_Reparacion))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Reparacion + " = '" + Parametros.P_Tipo_Reparacion + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Ini).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Entrada + " >= '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Recep_Ini) + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Fin).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Entrada + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Recep_Fin.AddDays(1)) + "'";
                    }
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Servicios_Correctivos      
            ///DESCRIPCIÓN          : Saca un listado de las Solicitudes Cargadas con 
            ///                       Servicio Correctivo
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a
            ///                         Consultar en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 24/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
        public static DataTable Consulta_Servicios_Correctivos(Cls_Rpt_Tal_Reporte_Gastos_Negocio Parametros)
        { 
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try {
                    Mi_SQL = "SELECT TO_CHAR(ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_No_Entrada + ") AS NO_ENTRADA";
                    Mi_SQL = Mi_SQL + ", ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Entrada + " AS FECHA_RECEPCION";
                    Mi_SQL = Mi_SQL + ", ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Kilometraje + " AS KILOMETRAJE";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", ISNULL(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + ", 'NO AUTORIZADA') AS FOLIO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", DECODE(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                    Mi_SQL = Mi_SQL + ", 'SERVICIO_CORRECTIVO','SERVICIO CORRECTIVO','SERVICIO_PREVENTIVO','SERVICIO PREVENTIVO','REVISTA_MECANICA','REVISTA MECANICA') AS TIPO_SOLICIUD";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                    Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", TO_CHAR(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + ") AS NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", TO_CHAR(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + ") AS NO_ECONOMICO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Diagnostico + " AS DIAGNOSTICO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL = Mi_SQL + ", TRIM(EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + "";
                    Mi_SQL = Mi_SQL + " +' - '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Nombre + ") AS MECANICO";

                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Entradas_Vehiculos.Tabla_Ope_Tal_Entradas_Vehiculos + " ENTRADAS";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES";
                    Mi_SQL = Mi_SQL + " ON ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos + " SERVICIOS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " MECANICOS";
                    Mi_SQL = Mi_SQL + " ON SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Mecanico_ID + " = MECANICOS." + Cat_Tal_Mecanicos.Campo_Mecanico_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS";
                    Mi_SQL = Mi_SQL + " ON MECANICOS." + Cat_Tal_Mecanicos.Campo_Empleado_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + "";
                    Mi_SQL = Mi_SQL + " WHERE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " = 'SERVICIO_CORRECTIVO'"; Entro_Where = true;
                    if (!String.IsNullOrEmpty(Parametros.P_Vehiculo_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " IN ('" + Parametros.P_Vehiculo_ID + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Tipo_Reparacion))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Reparacion + " = '" + Parametros.P_Tipo_Reparacion + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Ini).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Entrada + " >= '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Recep_Ini) + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Fin).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Entrada + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Recep_Fin.AddDays(1)) + "'";
                    }
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Servicios_Revista_Mecanica      
            ///DESCRIPCIÓN          : Saca un listado de las Solicitudes Cargadas con 
            ///                       Servicio de Revista Mecanica
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a
            ///                         Consultar en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 24/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
        public static DataTable Consulta_Servicios_Revista_Mecanica(Cls_Rpt_Tal_Reporte_Gastos_Negocio Parametros)
        { 
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try
                {
                    Mi_SQL = "SELECT TO_CHAR(ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_No_Entrada + ") AS NO_ENTRADA";
                    Mi_SQL = Mi_SQL + ", ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Entrada + " AS FECHA_RECEPCION";
                    Mi_SQL = Mi_SQL + ", ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Kilometraje + " AS KILOMETRAJE";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", ISNULL(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + ", 'NO AUTORIZADA') AS FOLIO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", DECODE(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                    Mi_SQL = Mi_SQL + ", 'SERVICIO_CORRECTIVO','SERVICIO CORRECTIVO','SERVICIO_PREVENTIVO','SERVICIO PREVENTIVO','REVISTA_MECANICA','REVISTA MECANICA') AS TIPO_SOLICIUD";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                    Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", TO_CHAR(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + ") AS NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", TO_CHAR(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + ") AS NO_ECONOMICO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Rev_Mecanica.Campo_Diagnostico + " AS DIAGNOSTICO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL = Mi_SQL + ", TRIM(EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + "";
                    Mi_SQL = Mi_SQL + " +' - '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Nombre + ") AS MECANICO";

                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Entradas_Vehiculos.Tabla_Ope_Tal_Entradas_Vehiculos + " ENTRADAS";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES";
                    Mi_SQL = Mi_SQL + " ON ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Rev_Mecanica.Tabla_Ope_Tal_Rev_Mecanica + " SERVICIOS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = SERVICIOS." + Ope_Tal_Rev_Mecanica.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " MECANICOS";
                    Mi_SQL = Mi_SQL + " ON SERVICIOS." + Ope_Tal_Rev_Mecanica.Campo_Mecanico_ID + " = MECANICOS." + Cat_Tal_Mecanicos.Campo_Mecanico_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS";
                    Mi_SQL = Mi_SQL + " ON MECANICOS." + Cat_Tal_Mecanicos.Campo_Empleado_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + "";
                    Mi_SQL = Mi_SQL + " WHERE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " = 'REVISTA_MECANICA'"; Entro_Where = true;
                    if (!String.IsNullOrEmpty(Parametros.P_Vehiculo_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " IN ('" + Parametros.P_Vehiculo_ID + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Ini).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Entrada + " >= '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Recep_Ini) + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Fin).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Entrada + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Recep_Fin.AddDays(1)) + "'";
                    }
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consulta_Gastos
        ///DESCRIPCIÓN          : Saca un listado de las Solicitudes Cargadas con 
        ///                       Servicio Preventivo
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a
        ///                         Consultar en la Base de Datos.
        ///CREO                 : Jesus Toledo
        ///FECHA_CREO           : 24/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consulta_Gastos(Cls_Rpt_Tal_Reporte_Gastos_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                
                Mi_SQL = " ( SELECT VISTA_SRV.";
                Mi_SQL = Mi_SQL + Vw_Tal_Servicios.Campo_Vehiculo_Descripcion + " AS VEHICULO_DESCRIPCION";
                Mi_SQL = Mi_SQL + ", VISTA_SRV." + Vw_Tal_Servicios.Campo_Numero_Economico + " AS NUMERO_ECONOMICO";
                Mi_SQL = Mi_SQL + ", VISTA_SRV." + Vw_Tal_Servicios.Campo_Fecha_Creo + " AS FECHA";
                if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Ini).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                    Mi_SQL = Mi_SQL + ",'" + Parametros.P_F_Recep_Ini.ToLongDateString() + "' AS FECHA_INICIAL";
                else
                    Mi_SQL = Mi_SQL + ",'" + DateTime.Today.ToLongDateString() + "' AS FECHA_INICIAL";
                if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Fin).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                    Mi_SQL = Mi_SQL + ",'" + Parametros.P_F_Recep_Fin.ToLongDateString() + "' AS FECHA_FINAL";
                else
                    Mi_SQL = Mi_SQL + ",'" + DateTime.Today.ToLongDateString() + "' AS FECHA_FINAL";
                Mi_SQL = Mi_SQL + ", VISTA_SRV." + Vw_Tal_Servicios.Campo_Tipo_Servicio + " AS TIPO_SERVICIO";
                Mi_SQL = Mi_SQL + ", ISNULL(VISTA_SRV." + Vw_Tal_Servicios.Campo_Tipo_Trabajo + ",'OTROS') AS TIPO_TRABAJO";
                Mi_SQL = Mi_SQL + ", VISTA_SRV." + Vw_Tal_Servicios.Campo_Reparacion + " AS TIPO_REPARACION";
                Mi_SQL = Mi_SQL + ", VISTA_SRV." + Vw_Tal_Servicios.Campo_Nombre_Dependencia + " AS NOMBRE_DEPENDENCIA";
                Mi_SQL = Mi_SQL + ", FS." + Ope_Tal_Facturas_Servicio.Campo_Total_Factura + " AS GASTO";
                Mi_SQL = Mi_SQL + ", FS." + Ope_Tal_Facturas_Servicio.Campo_Factura + " AS FOLIO_FACTURA";

                //Filtros
                Mi_SQL = Mi_SQL + ",'" + Parametros.P_Tipo_Servicio + "' AS TIPO_SERVICIO_FILTRO";
                Mi_SQL = Mi_SQL + ",'" + Parametros.P_Tipo_Reparacion + "' AS TIPO_REPARACION_FILTRO";
                Mi_SQL = Mi_SQL + ",'" + Parametros.P_Tipo_Trabajo + "' AS TIPO_TRABAJO_FILTRO";
                if (!String.IsNullOrEmpty(Parametros.P_Filtro))
                    Mi_SQL = Mi_SQL + ",'" + Parametros.P_Filtro + "' AS FILTRO";
                else
                    Mi_SQL = Mi_SQL + ",'GASTO TOTAL DEL TALLER' AS FILTRO";

                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio + " FS";
                Mi_SQL = Mi_SQL + " LEFT JOIN " + Vw_Tal_Servicios.Vista_Vw_Tal_Servicios + " VISTA_SRV ";
                Mi_SQL = Mi_SQL + " ON (VISTA_SRV." + Vw_Tal_Servicios.Campo_No_servicio + " = FS." + Ope_Tal_Facturas_Servicio.Campo_No_Servicio;
                Mi_SQL = Mi_SQL + " AND VISTA_SRV." + Vw_Tal_Servicios.Campo_Tipo_Servicio + " = FS." + Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio + ")";
                //Filtros para consulta 1
                if (Parametros.P_Numero_Inventario > 0 )
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VISTA_SRV." + Vw_Tal_Servicios.Campo_Numero_Inventario + " = " + Parametros.P_Numero_Inventario;
                }
                if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VISTA_SRV." + Vw_Tal_Servicios.Campo_Dependencia_Id + " = '" + Parametros.P_Dependencia_ID + "'";
                }
                if (Parametros.P_Tipo_Reparacion != "TODOS")
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VISTA_SRV." + Vw_Tal_Servicios.Campo_Reparacion + " = '" + Parametros.P_Tipo_Reparacion + "'";
                }
                if (Parametros.P_Tipo_Servicio != "TODOS")
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VISTA_SRV." + Vw_Tal_Servicios.Campo_Tipo_Servicio + " = '" + Parametros.P_Tipo_Servicio + "'";
                }
                if (Parametros.P_Tipo_Trabajo != "TODOS")
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VISTA_SRV." + Vw_Tal_Servicios.Campo_Tipo_Trabajo + " = '" + Parametros.P_Tipo_Trabajo + "'";
                }
                if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Ini).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VISTA_SRV." + Vw_Tal_Servicios.Campo_Fecha_Creo + " >= CONVERT(DATETIME,'" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Recep_Ini) + "')";
                }
                if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Fin).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VISTA_SRV." + Vw_Tal_Servicios.Campo_Fecha_Creo + " < CONVERT(DATETIME,'" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Recep_Fin.AddDays(1)) + "')";
                }
                Mi_SQL = Mi_SQL + " )";
                //Union con la tabla de Registro de Facturas
                
                Mi_SQL = Mi_SQL + " UNION ";
                Entro_Where = false;
                Mi_SQL = Mi_SQL + " ( SELECT VISTA_SRV.";
                Mi_SQL = Mi_SQL + Vw_Tal_Servicios.Campo_Vehiculo_Descripcion + " AS VEHICULO_DESCRIPCION";
                Mi_SQL = Mi_SQL + ", VISTA_SRV." + Vw_Tal_Servicios.Campo_Numero_Economico + " AS NUMERO_ECONOMICO";
                Mi_SQL = Mi_SQL + ", VISTA_SRV." + Vw_Tal_Servicios.Campo_Fecha_Creo + " AS FECHA";
                if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Ini).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                    Mi_SQL = Mi_SQL + ",'" + Parametros.P_F_Recep_Ini.ToLongDateString() + "' AS FECHA_INICIAL";
                else
                    Mi_SQL = Mi_SQL + ",'" + DateTime.Today.ToLongDateString() + "' AS FECHA_INICIAL";
                if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Fin).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                    Mi_SQL = Mi_SQL + ",'" + Parametros.P_F_Recep_Fin.ToLongDateString() + "' AS FECHA_FINAL";
                else
                    Mi_SQL = Mi_SQL + ",'" + DateTime.Today.ToLongDateString() + "' AS FECHA_FINAL";
                Mi_SQL = Mi_SQL + ", VISTA_SRV." + Vw_Tal_Servicios.Campo_Tipo_Servicio + " AS TIPO_SERVICIO";
                Mi_SQL = Mi_SQL + ", ISNULL(VISTA_SRV." + Vw_Tal_Servicios.Campo_Tipo_Trabajo + ",'OTROS') AS TIPO_TRABAJO";
                Mi_SQL = Mi_SQL + ", VISTA_SRV." + Vw_Tal_Servicios.Campo_Reparacion + " AS TIPO_REPARACION";
                Mi_SQL = Mi_SQL + ", VISTA_SRV." + Vw_Tal_Servicios.Campo_Nombre_Dependencia + " AS NOMBRE_DEPENDENCIA";
                Mi_SQL = Mi_SQL + ", RG." + Ope_Tal_Registro_Facturas.Campo_Total_Factura + " AS GASTO";
                Mi_SQL = Mi_SQL + ", RG." + Ope_Tal_Registro_Facturas.Campo_Factura_Proveedor + " AS FOLIO_FACTURA";
                //Filtros
                Mi_SQL = Mi_SQL + ",'" + Parametros.P_Tipo_Servicio + "' AS TIPO_SERVICIO_FILTRO";
                Mi_SQL = Mi_SQL + ",'" + Parametros.P_Tipo_Reparacion + "' AS TIPO_REPARACION_FILTRO";
                Mi_SQL = Mi_SQL + ",'" + Parametros.P_Tipo_Trabajo + "' AS TIPO_TRABAJO_FILTRO";
                if (!String.IsNullOrEmpty(Parametros.P_Filtro))
                    Mi_SQL = Mi_SQL + ",'" + Parametros.P_Filtro + "' AS FILTRO";
                else
                    Mi_SQL = Mi_SQL + ",'GASTO TOTAL DEL TALLER' AS FILTRO";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas + " RG";
                Mi_SQL = Mi_SQL + " LEFT JOIN " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " RQ ON RQ." + Ope_Tal_Requisiciones.Campo_No_Orden_Compra;
                Mi_SQL = Mi_SQL + " = RG." + Ope_Tal_Registro_Facturas.Campo_No_Orden_Compra;
                Mi_SQL = Mi_SQL + " LEFT JOIN " + Vw_Tal_Servicios.Vista_Vw_Tal_Servicios + " VISTA_SRV ON VISTA_SRV." + Vw_Tal_Servicios.Campo_No_Solicitud;
                Mi_SQL = Mi_SQL + " = RQ." + Ope_Tal_Requisiciones.Campo_No_Solicitud;
                
                //Filtros para consulta 2    
                if (Parametros.P_Numero_Inventario > 0)
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VISTA_SRV." + Vw_Tal_Servicios.Campo_Numero_Inventario + " = " + Parametros.P_Numero_Inventario;
                }
                if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VISTA_SRV." + Vw_Tal_Servicios.Campo_Dependencia_Id + " = '" + Parametros.P_Dependencia_ID + "'";
                }
                if (Parametros.P_Tipo_Reparacion != "TODOS")
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VISTA_SRV." + Vw_Tal_Servicios.Campo_Reparacion + " = '" + Parametros.P_Tipo_Reparacion + "'";
                }
                if (Parametros.P_Tipo_Servicio != "TODOS")
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VISTA_SRV." + Vw_Tal_Servicios.Campo_Tipo_Servicio + " = '" + Parametros.P_Tipo_Servicio + "'";
                }
                if (Parametros.P_Tipo_Trabajo != "TODOS")
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VISTA_SRV." + Vw_Tal_Servicios.Campo_Tipo_Trabajo + " = '" + Parametros.P_Tipo_Trabajo + "'";
                }
                if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Ini).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VISTA_SRV." + Vw_Tal_Servicios.Campo_Fecha_Creo + " >= CONVERT(DATETIME,'" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Recep_Ini) + "')";
                }
                if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Fin).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VISTA_SRV." + Vw_Tal_Servicios.Campo_Fecha_Creo + " < CONVERT(DATETIME,'" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Recep_Fin.AddDays(1)) + "')";
                }
                Mi_SQL = Mi_SQL + " )";                                            
                
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consulta_Reporte_Refacciones
        ///DESCRIPCIÓN          : Saca un listado de las Refacciones Cargadas con 
        ///                       Servicio Interno
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a
        ///                         Consultar en la Base de Datos.
        ///CREO                 : Jesus Toledo
        ///FECHA_CREO           : 24/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consulta_Reporte_Refacciones(Cls_Rpt_Tal_Reporte_Gastos_Negocio Parametros)
        {
             String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                
                Mi_SQL = "SELECT LISTADO." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " NUMERO_REQUISICION ";
                Mi_SQL = Mi_SQL + ", LISTADO." + Ope_Tal_Req_Refaccion.Campo_Cantidad + " CANTIDAD ";
                Mi_SQL = Mi_SQL + ", REFACCION." + Cat_Tal_Refacciones.Campo_Nombre + " NOMBRE_REFACCION ";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Vw_Tal_Servicios.Campo_Vehiculo_Descripcion + " VEHICULO ";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Vw_Tal_Servicios.Campo_Mecanico + " NOMBRE_MECANICO ";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Vw_Tal_Servicios.Campo_Nombre_Dependencia + " NOMBRE_DEPENDENCIA ";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Vw_Tal_Servicios.Campo_Numero_Economico + " NO_ECONOMICO ";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Vw_Tal_Servicios.Campo_Tipo_Trabajo + " TIPO_TRABAJO ";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Vw_Tal_Servicios.Campo_Fecha_Creo + " FECHA_SERVICIO ";
                if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Ini).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                    Mi_SQL = Mi_SQL + ",'" + Parametros.P_F_Recep_Ini.ToLongDateString() + "' AS FECHA_INICIAL";
                else
                    Mi_SQL = Mi_SQL + ",'" + DateTime.Today.ToLongDateString() + "' AS FECHA_INICIAL";
                if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Fin).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                    Mi_SQL = Mi_SQL + ",'" + Parametros.P_F_Recep_Fin.ToLongDateString() + "' AS FECHA_FINAL";
                else
                    Mi_SQL = Mi_SQL + ",'" + DateTime.Today.ToLongDateString() + "' AS FECHA_FINAL";
                if (!String.IsNullOrEmpty(Parametros.P_Filtro))
                    Mi_SQL = Mi_SQL + ",'" + Parametros.P_Filtro + "' AS FILTRO";
                else
                    Mi_SQL = Mi_SQL + ",'REFACCIONES TOTALES DEL TALLER' AS FILTRO";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Vw_Tal_Servicios.Campo_Diagnostico + " DIAGNOSTICO ";
                Mi_SQL = Mi_SQL + ", cast(SERVICIOS." + Vw_Tal_Servicios.Campo_Folio + " as int) FOLIO_SOLICITUD ";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Vw_Tal_Servicios.Campo_Tipo_Servicio + " TIPO_SERVICIO ";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Vw_Tal_Servicios.Campo_Reparacion + " TIPO_REPARACION ";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " LISTADO ";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQ ON REQ." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = LISTADO." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " REFACCION ON REFACCION." + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = LISTADO." + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Vw_Tal_Servicios.Vista_Vw_Tal_Servicios + " SERVICIOS ON SERVICIOS." + Vw_Tal_Servicios.Campo_No_Solicitud + " = REQ." + Ope_Tal_Requisiciones.Campo_No_Solicitud; 
 
                //FILTROS DEL REPORTE                
                if (Parametros.P_Numero_Inventario > 0)
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " SERVICIOS." + Vw_Tal_Servicios.Campo_Numero_Inventario + " = " + Parametros.P_Numero_Inventario;
                }
                if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " SERVICIOS." + Vw_Tal_Servicios.Campo_Dependencia_Id + " = '" + Parametros.P_Dependencia_ID + "'";
                }
                if (Parametros.P_Tipo_Reparacion != "TODOS")
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " SERVICIOS." + Vw_Tal_Servicios.Campo_Reparacion + " = '" + Parametros.P_Tipo_Reparacion + "'";
                }
                if (Parametros.P_Tipo_Servicio != "TODOS")
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " SERVICIOS." + Vw_Tal_Servicios.Campo_Tipo_Servicio + " = '" + Parametros.P_Tipo_Servicio + "'";
                }
                if (Parametros.P_Tipo_Trabajo != "TODOS")
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " SERVICIOS." + Vw_Tal_Servicios.Campo_Tipo_Trabajo + " = '" + Parametros.P_Tipo_Trabajo + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Nombre_Mecanico))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " SERVICIOS." + Vw_Tal_Servicios.Campo_Mecanico + " = '" + Parametros.P_Nombre_Mecanico + "'";
                }
                if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Ini).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " SERVICIOS." + Vw_Tal_Servicios.Campo_Fecha_Creo + " >= CONVERT(DATETIME,'" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Recep_Ini) + "')";
                }
                if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Recep_Fin).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " SERVICIOS." + Vw_Tal_Servicios.Campo_Fecha_Creo + " < CONVERT(DATETIME,'" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Recep_Fin.AddDays(1)) + "')";
                }

                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }
        #endregion
	}
}