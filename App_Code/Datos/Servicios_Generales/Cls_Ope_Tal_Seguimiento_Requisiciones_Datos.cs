﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Seguimiento_Requisiciones_Taller.Negocio;
using System.Text;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
/// <summary>
/// Summary description for Cls_Ope_Tal_Seguimiento_Requisiciones_Datos
/// </summary>
namespace JAPAMI.Seguimiento_Requisiciones_Taller.Datos
{
    public class Cls_Ope_Tal_Seguimiento_Requisiciones_Datos
    {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Listado_Requisiciones
        ///DESCRIPCIÓN: Hace la Consulta General sobre las Requisiciones
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 04/Septiembre/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Listado_Requisiciones(Cls_Ope_Tal_Seguimiento_Requisiciones_Negocio Parametros) {
            DataTable Dt_Datos = new DataTable();
            DataSet Ds_Datos = null;
            Boolean Entro_Where = false;
            try
            {
                StringBuilder Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " AS NO_REQUISICION");
                Mi_SQL.Append(", RTRIM(LTRIM(REQ." + Ope_Com_Requisiciones.Campo_Folio + ")) AS FOLIO_RQ");
                Mi_SQL.Append(", RTRIM(LTRIM(OC." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + ")) AS NO_ORDEN_COMPRA");
                Mi_SQL.Append(", RTRIM(LTRIM(OC." + Ope_Com_Ordenes_Compra.Campo_Folio + ")) AS FOLIO_OC");
                Mi_SQL.Append(", REQ." + Ope_Com_Requisiciones.Campo_Fecha_Generacion + " AS FECHA");
                Mi_SQL.Append(", RTRIM(LTRIM(DEP." + Cat_Dependencias.Campo_Nombre + ")) AS DEPENDENCIA");
                Mi_SQL.Append(", RTRIM(LTRIM(REQ." + Ope_Com_Requisiciones.Campo_Estatus + ")) AS ESTATUS_RQ");
                Mi_SQL.Append(", REQ." + Ope_Com_Requisiciones.Campo_Total + " AS TOTAL");
                Mi_SQL.Append(", REQ." + Ope_Com_Requisiciones.Campo_Total_Cotizado + " AS TOTAL_COTIZADO");
                Mi_SQL.Append(", RTRIM(LTRIM(PRO." + Cat_Com_Proveedores.Campo_Compañia + "))  +' / '+ RTRIM(LTRIM(PRO." + Cat_Com_Proveedores.Campo_Nombre + ")) AS NOMBRE_PROVEEDOR");
                Mi_SQL.Append(" FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQ");
                Mi_SQL.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEP ON DEP." + Cat_Dependencias.Campo_Dependencia_ID + " = REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " OC ON OC." + Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra + " = REQ." + Ope_Com_Requisiciones.Campo_No_Orden_Compra);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PRO ON OC." + Ope_Com_Ordenes_Compra.Campo_Proveedor_ID + " = PRO." + Cat_Com_Proveedores.Campo_Proveedor_ID);
                if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID)) {
                    if (Entro_Where) { Mi_SQL.Append(" AND "); } else { Mi_SQL.Append(" WHERE "); Entro_Where = true; }
                    Mi_SQL.Append("REQ." + Ope_Com_Requisiciones.Campo_Dependencia_ID + " IN ('" + Parametros.P_Dependencia_ID + "')");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Estatus)) {
                    if (Entro_Where) { Mi_SQL.Append(" AND "); } else { Mi_SQL.Append(" WHERE "); Entro_Where = true; }
                    Mi_SQL.Append("REQ." + Ope_Com_Requisiciones.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')");
                }
                if (!String.IsNullOrEmpty(Parametros.P_Requisicion_ID)) {
                    if (Entro_Where) { Mi_SQL.Append(" AND "); } else { Mi_SQL.Append(" WHERE "); Entro_Where = true; }
                    Mi_SQL.Append("REQ." + Ope_Com_Requisiciones.Campo_Requisicion_ID + " IN ('" + Parametros.P_Requisicion_ID + "')");
                }
                if (!String.Format("{0:ddMMyyyy}", new DateTime()).Equals(String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Inicio)))
                {
                    if (Entro_Where) { Mi_SQL.Append(" AND "); } else { Mi_SQL.Append(" WHERE "); Entro_Where = true; }
                    Mi_SQL.Append("REQ." + Ope_Com_Requisiciones.Campo_Fecha_Generacion + " >= '" + String.Format("{0:dd/MM/yyyy}",Parametros.P_Fecha_Inicio) + "'");
                }
                if (!String.Format("{0:ddMMyyyy}", new DateTime()).Equals(String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Fin)))
                {
                    if (Entro_Where) { Mi_SQL.Append(" AND "); } else { Mi_SQL.Append(" WHERE "); Entro_Where = true; }
                    Mi_SQL.Append("REQ." + Ope_Com_Requisiciones.Campo_Fecha_Generacion + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Fin.AddDays(1)) + "'");
                }
                Mi_SQL.Append(" ORDER BY NO_REQUISICION ASC");
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds_Datos != null) if (Ds_Datos.Tables.Count > 0) Dt_Datos = Ds_Datos.Tables[0];
            }
            catch (Exception Ex) 
            {
                throw new Exception("Consultar_Listado_Requisiciones. " + Ex.Message);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Historial_Requisiciones
        ///DESCRIPCIÓN: Hace la Consulta del Historial sobre las Requisiciones
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 04/Septiembre/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Historial_Requisiciones(Cls_Ope_Tal_Seguimiento_Requisiciones_Negocio Parametros) {
            DataTable Dt_Datos = new DataTable();
            DataSet Ds_Datos = null;
            Boolean Entro_Where = false;
            try
            {
                StringBuilder Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT HIST." + Ope_Tal_Req_Historial.Campo_Fecha + " AS FECHA");
                Mi_SQL.Append(", HIST." + Ope_Tal_Req_Historial.Campo_Usuario_Creo + " AS EMPLEADO");
                Mi_SQL.Append(", HIST." + Ope_Tal_Req_Historial.Campo_Estatus + " AS ESTATUS");
                Mi_SQL.Append(" FROM " + Ope_Tal_Req_Historial.Tabla_Ope_Tal_Req_Historial + " HIST");
                if (!String.IsNullOrEmpty(Parametros.P_Requisicion_ID)) {
                    if (Entro_Where) { Mi_SQL.Append(" AND "); } else { Mi_SQL.Append(" WHERE "); Entro_Where = true; }
                    Mi_SQL.Append("HIST." + Ope_Tal_Req_Historial.Campo_No_Requisicion + " IN ('" + Parametros.P_Requisicion_ID + "')");
                }
                Mi_SQL.Append(" ORDER BY HIST." + Ope_Tal_Req_Historial.Campo_No_Historial + " ASC");
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds_Datos != null) if (Ds_Datos.Tables.Count > 0) Dt_Datos = Ds_Datos.Tables[0];
            }
            catch (Exception Ex) 
            {
                throw new Exception("Consultar_Listado_Requisiciones. " + Ex.Message);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Historial_Observaciones
        ///DESCRIPCIÓN: Hace la Consulta del Historial de Comentarios sobre las Requisiciones
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 04/Septiembre/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Historial_Observaciones(Cls_Ope_Tal_Seguimiento_Requisiciones_Negocio Parametros) {
            DataTable Dt_Datos = new DataTable();
            DataSet Ds_Datos = null;
            Boolean Entro_Where = false;
            try
            {
                StringBuilder Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT HIST." + Ope_Tal_Req_Observaciones.Campo_Estatus + " AS ESTATUS");
                Mi_SQL.Append(", HIST." + Ope_Tal_Req_Observaciones.Campo_Fecha_Creo + " AS FECHA_CREO");
                Mi_SQL.Append(", HIST." + Ope_Tal_Req_Observaciones.Campo_Usuario_Creo + " AS USUARIO_CREO");
                Mi_SQL.Append(", HIST." + Ope_Tal_Req_Observaciones.Campo_Comentario + " AS COMENTARIO");
                Mi_SQL.Append(" FROM " + Ope_Tal_Req_Observaciones.Tabla_Ope_Tal_Req_Observaciones + " HIST");
                if (!String.IsNullOrEmpty(Parametros.P_Requisicion_ID)) {
                    if (Entro_Where) { Mi_SQL.Append(" AND "); } else { Mi_SQL.Append(" WHERE "); Entro_Where = true; }
                    Mi_SQL.Append("HIST." + Ope_Tal_Req_Observaciones.Campo_Requisicion_ID + " IN ('" + Parametros.P_Requisicion_ID + "')");
                }
                Mi_SQL.Append(" ORDER BY HIST." + Ope_Tal_Req_Observaciones.Campo_Requisicion_ID + " ASC");
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds_Datos != null) if (Ds_Datos.Tables.Count > 0) Dt_Datos = Ds_Datos.Tables[0];
            }
            catch (Exception Ex) 
            {
                throw new Exception("Consultar_Listado_Requisiciones. " + Ex.Message);
            }
            return Dt_Datos;
        }
    }
}
