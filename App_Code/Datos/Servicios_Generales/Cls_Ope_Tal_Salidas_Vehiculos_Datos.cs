﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Datos;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Salidas_Vehiculos.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Datos;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Tal_Salidas_Vehiculos_Datos
/// </summary>

namespace JAPAMI.Taller_Mecanico.Operacion_Salidas_Vehiculos.Datos {
    public class Cls_Ope_Tal_Salidas_Vehiculos_Datos {

        #region Metodos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Salida_Vehiculo      
            ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a dar de
            ///                         Alta en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 05/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Alta_Salida_Vehiculo(Cls_Ope_Tal_Salidas_Vehiculos_Negocio Parametros) { 
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                Parametros.P_No_Salida = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Salidas_Vehiculos.Tabla_Ope_Tal_Salidas_Vehiculos, Ope_Tal_Salidas_Vehiculos.Campo_No_Salida, 20));
                try {
                    String Mi_SQL = "INSERT INTO " + Ope_Tal_Salidas_Vehiculos.Tabla_Ope_Tal_Salidas_Vehiculos;
                    Mi_SQL = Mi_SQL + " (" + Ope_Tal_Salidas_Vehiculos.Campo_No_Salida + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Salidas_Vehiculos.Campo_No_Entrada + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Salidas_Vehiculos.Campo_Tipo_Salida + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Salidas_Vehiculos.Campo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Salidas_Vehiculos.Campo_Tipo_Bien + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Salidas_Vehiculos.Campo_Fecha_Salida + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Salidas_Vehiculos.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Salidas_Vehiculos.Campo_Empleado_Recibio_ID + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Salidas_Vehiculos.Campo_Empleado_Entrego_ID + "";
                    if (Parametros.P_Kilometraje > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Salidas_Vehiculos.Campo_Kilometraje + "";
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Salidas_Vehiculos.Campo_Comentarios + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Salidas_Vehiculos.Campo_Usuario_Creo + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Salidas_Vehiculos.Campo_Fecha_Creo + ")";
                    Mi_SQL = Mi_SQL + " VALUES ('" + Parametros.P_No_Salida + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Entrada + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo_Salida + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Vehiculo_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo_Bien + "'";
                    Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Salida) + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Solicitud + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Empleado_Recibio_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Empleado_Entrego_ID + "'";
                    if (Parametros.P_Kilometraje > (-1)) {
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Kilometraje + "'";
                    }
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Comentarios + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Sal_Veh_Det.Tabla_Ope_Tal_Sal_Veh_Det, Ope_Tal_Sal_Veh_Det.Campo_No_Registro, 20));
                    foreach (DataRow Fila_Actual in Parametros.P_Dt_Detalles.Rows) {
                        Mi_SQL = "INSERT INTO " + Ope_Tal_Sal_Veh_Det.Tabla_Ope_Tal_Sal_Veh_Det;
                        Mi_SQL = Mi_SQL + " (" + Ope_Tal_Sal_Veh_Det.Campo_No_Registro + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Sal_Veh_Det.Campo_No_Salida + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Sal_Veh_Det.Campo_Parte_ID + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Sal_Veh_Det.Campo_SubParte_ID + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Sal_Veh_Det.Campo_Valor + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Sal_Veh_Det.Campo_Usuario_Creo + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Sal_Veh_Det.Campo_Fecha_Creo + ")";
                        Mi_SQL = Mi_SQL + " VALUES ('" + No_Registro + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Salida + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila_Actual["PARTE_ID"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila_Actual["SUBPARTE_ID"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila_Actual["VALOR"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        No_Registro = No_Registro + 1;
                    }
                    //Registrar Fotos o imagenes de Automovil al Realizar la Entrada
                    Int32 Numero_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Sal_Veh_Fotos.Tabla_Ope_Tal_Sal_Veh_Fotos, Ope_Tal_Sal_Veh_Fotos.Campo_No_Registro, 20));
                    foreach (DataRow Fila_Actual in Parametros.P_Dt_Archivos.Rows)
                    {                        
                        Mi_SQL = "INSERT INTO " + Ope_Tal_Sal_Veh_Fotos.Tabla_Ope_Tal_Sal_Veh_Fotos;
                        Mi_SQL = Mi_SQL + " (" + Ope_Tal_Sal_Veh_Fotos.Campo_No_Registro + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Sal_Veh_Fotos.Campo_No_Salida + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Sal_Veh_Fotos.Campo_Tipo_Foto + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Sal_Veh_Fotos.Campo_Ruta_Archivo + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Sal_Veh_Fotos.Campo_Usuario_Creo + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Sal_Veh_Fotos.Campo_Fecha_Creo + ")";
                        Mi_SQL = Mi_SQL + " VALUES ('" + Numero_Registro + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Salida + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila_Actual["FOTO"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila_Actual["RUTA_ARCHIVO"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        Numero_Registro = Numero_Registro + 1;
                    }
                    Mi_SQL = "UPDATE " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " = 'ENTREGADO'";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                    Solicitud.P_No_Solicitud = Parametros.P_No_Solicitud;
                    Solicitud.P_Estatus = "ENTREGADO";
                    Solicitud.P_Empleado_Solicito_ID = Parametros.P_Empleado_Entrego_ID;
                    Solicitud.P_Usuario = Parametros.P_Usuario;
                    Cls_Ope_Tal_Solicitud_Servicio_Datos.Alta_Registro_Seguimiento_Solicitud(Solicitud, ref Cmd);

                    Cls_Ope_Tal_Entradas_Vehiculos_Negocio Ent_Negocio = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
                    Ent_Negocio.P_No_Entrada = Parametros.P_No_Entrada;
                    Ent_Negocio.P_Estatus = "CERRADA";
                    Ent_Negocio.P_Usuario = Parametros.P_Usuario;
                    Cls_Ope_Tal_Entradas_Vehiculos_Datos.Actualizacion_Estatus_Entrada(Ent_Negocio, ref Cmd);

                    Trans.Commit();
                } catch (SqlException Ex) {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152) {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 2627) {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    } else if (Ex.Number == 547) {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 515) {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }
            }
            public static DataSet Consultar_Datos_Reporte_Salida_Revista(Cls_Ope_Tal_Salidas_Vehiculos_Negocio Parametros)
            {
                String Mi_SQL = null;
                DataSet Ds_Datos = new DataSet();
                DataTable Dt_Datos = new DataTable();
                DataTable Dt_Img_Entrada = new DataTable();
                DataColumn Entrada_Frente;
                DataColumn Entrada_Atras;
                DataColumn Entrada_Izq;
                DataColumn Entrada_Der;
                DataTable Dt_Img_Salida = new DataTable();
                DataColumn Salida_Frente;
                DataColumn Salida_Atras;
                DataColumn Salida_Izq;
                DataColumn Salida_Der;
                Boolean Entro_Where = false;
                Object Aux; //Variable auxiliar para las consultas
                try
                {
                    //Campos de la Solicitud
                    Mi_SQL = "SELECT cast(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " as varchar) AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ",cast(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Kilometraje + " as varchar) AS KILOMETRAJE";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                    //Mi_SQL = Mi_SQL + ", 'SERVICIO_CORRECTIVO','SERVICIO CORRECTIVO','SERVICIO_PREVENTIVO','SERVICIO PREVENTIVO','REVISTA_MECANICA','REVISTA MECANICA') AS TIPO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                    Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", cast(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " as varchar) AS FECHA_ELABORACION";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL = Mi_SQL + ", cast(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Prog + " as varchar) AS FECHA_RECEPCION_PROG";
                    Mi_SQL = Mi_SQL + ", cast(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " as varchar) AS FECHA_RECEPCION_REAL";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " AS FOLIO";
                    //Campos del vehiculo
                    Mi_SQL = Mi_SQL + ", cast(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " as varchar) AS NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Placas + " AS PLACAS";
                    Mi_SQL = Mi_SQL + ", cast(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Anio_Fabricacion + " as varchar) AS ANIO";
                    //Detalles del Vehiculo
                    Mi_SQL = Mi_SQL + ", TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Descripcion;
                    Mi_SQL = Mi_SQL + " +' Marca:  '+ MARCAS." + Cat_Com_Marcas.Campo_Nombre;
                    Mi_SQL = Mi_SQL + " +' Modelo: '+ VEHICULOS." + Ope_Pat_Vehiculos.Campo_Modelo + " AS VEHICULO_DESCRIPCION";                    
                    //Datos del Diagnostico
                    Mi_SQL = Mi_SQL + ", REVISTA." + Ope_Tal_Rev_Mecanica.Campo_Diagnostico + " AS DIAGNOSTICO";                    
                    //Datos del Mecanico
                    Mi_SQL = Mi_SQL + ", EMPLEADOS." + Cat_Empleados.Campo_Nombre + " +' '+";
                    Mi_SQL = Mi_SQL + " EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + " +' '+";
                    Mi_SQL = Mi_SQL + " EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + " AS NOMBRE_MECANICO";
                    //No_Entrada
                    Mi_SQL = Mi_SQL + ", ENTRADA." + Ope_Tal_Entradas_Vehiculos.Campo_No_Entrada + " AS NO_ENTRADA";
                    //No_Salida
                    Mi_SQL = Mi_SQL + ", SALIDA." + Ope_Tal_Salidas_Vehiculos.Campo_No_Salida + " AS NO_SALIDA";
                    //Joins
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " MARCAS";
                    Mi_SQL = Mi_SQL + " ON MARCAS." + Cat_Com_Marcas.Campo_Marca_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Marca_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Tipos_Vehiculo.Tabla_Cat_Pat_Tipos_Vehiculo + " TIPOS_VEHICULO";
                    Mi_SQL = Mi_SQL + " ON TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Tipo_Vehiculo_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Tipo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Rev_Mecanica.Tabla_Ope_Tal_Rev_Mecanica + " REVISTA";
                    Mi_SQL = Mi_SQL + " ON REVISTA." + Ope_Tal_Rev_Mecanica.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " MECANICOS";
                    Mi_SQL = Mi_SQL + " ON MECANICOS." + Cat_Tal_Mecanicos.Campo_Mecanico_ID + " = REVISTA." + Ope_Tal_Rev_Mecanica.Campo_Mecanico_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS";
                    Mi_SQL = Mi_SQL + " ON EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + " = MECANICOS." + Cat_Tal_Mecanicos.Campo_Empleado_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Entradas_Vehiculos.Tabla_Ope_Tal_Entradas_Vehiculos + " ENTRADA";
                    Mi_SQL = Mi_SQL + " ON ENTRADA." + Ope_Tal_Entradas_Vehiculos.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Salidas_Vehiculos.Tabla_Ope_Tal_Salidas_Vehiculos + " SALIDA";
                    Mi_SQL = Mi_SQL + " ON SALIDA." + Ope_Tal_Salidas_Vehiculos.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                    if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                    }                    
                    if (Parametros.P_No_Solicitud>0)
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = " + Parametros.P_No_Solicitud;
                    }
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                    {
                        Ds_Datos.Tables.Add(SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Copy());
                        Ds_Datos.Tables[0].TableName = "DT_SOLICITUD";
                    }
                    if (Ds_Datos.Tables[0].Rows[0]["NO_ENTRADA"] != null)
                    {
                        string Numero_Entrada = Ds_Datos.Tables[0].Rows[0]["NO_ENTRADA"].ToString();
                        string Numero_Salida = Ds_Datos.Tables[0].Rows[0]["NO_SALIDA"].ToString();
                        Entrada_Frente = new DataColumn();
                        Entrada_Frente.DataType = System.Type.GetType("System.String");
                        Entrada_Frente.ColumnName = "FOTO_ENTRADA_FRENTE";
                        Dt_Img_Entrada.Columns.Add(Entrada_Frente);
                        Entrada_Atras = new DataColumn();
                        Entrada_Atras.DataType = System.Type.GetType("System.String");
                        Entrada_Atras.ColumnName = "FOTO_ENTRADA_ATRAS";
                        Dt_Img_Entrada.Columns.Add(Entrada_Atras);
                        Entrada_Izq = new DataColumn();
                        Entrada_Izq.DataType = System.Type.GetType("System.String");
                        Entrada_Izq.ColumnName = "FOTO_ENTRADA_IZQUIERDA";
                        Dt_Img_Entrada.Columns.Add(Entrada_Izq);
                        Entrada_Der = new DataColumn();
                        Entrada_Der.DataType = System.Type.GetType("System.String");
                        Entrada_Der.ColumnName = "FOTO_ENTRADA_DERECHA";
                        Dt_Img_Entrada.Columns.Add(Entrada_Der);

                        Salida_Frente = new DataColumn();
                        Salida_Frente.DataType = System.Type.GetType("System.String");
                        Salida_Frente.ColumnName = "FOTO_SALIDA_FRENTE";
                        Dt_Img_Salida.Columns.Add(Salida_Frente);
                        Salida_Atras = new DataColumn();
                        Salida_Atras.DataType = System.Type.GetType("System.String");
                        Salida_Atras.ColumnName = "FOTO_SALIDA_ATRAS";
                        Dt_Img_Salida.Columns.Add(Salida_Atras);
                        Salida_Izq = new DataColumn();
                        Salida_Izq.DataType = System.Type.GetType("System.String");
                        Salida_Izq.ColumnName = "FOTO_SALIDA_IZQUIERDA";
                        Dt_Img_Salida.Columns.Add(Salida_Izq);
                        Salida_Der = new DataColumn();
                        Salida_Der.DataType = System.Type.GetType("System.String");
                        Salida_Der.ColumnName = "FOTO_SALIDA_DERECHA";
                        Dt_Img_Salida.Columns.Add(Salida_Der);

                        DataRow Nueva_Fila = Dt_Img_Entrada.NewRow();
                                                
                        //Obtenemos la foto del Frente para la Recepcion del Vehiculo
                        Mi_SQL = "SELECT " + Ope_Tal_Ent_Veh_Fotos.Campo_Ruta_Archivo + " AS FRENTE FROM " + Ope_Tal_Ent_Veh_Fotos.Tabla_Ope_Tal_Ent_Veh_Fotos + " WHERE " + Ope_Tal_Ent_Veh_Fotos.Campo_No_Entrada + " = '" + Numero_Entrada + "' AND " + Ope_Tal_Ent_Veh_Fotos.Campo_Tipo_Foto + " = 'FRENTE'";
                        Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Aux != null)
                            Nueva_Fila["FOTO_ENTRADA_FRENTE"] = Aux.ToString();
                        else
                            Nueva_Fila["FOTO_ENTRADA_FRENTE"] = "";
                        //Obtenemos la foto de la parte trasera para la Recepcion del Vehiculo
                        Mi_SQL = "SELECT " + Ope_Tal_Ent_Veh_Fotos.Campo_Ruta_Archivo + " AS ATRAS FROM " + Ope_Tal_Ent_Veh_Fotos.Tabla_Ope_Tal_Ent_Veh_Fotos + " WHERE " + Ope_Tal_Ent_Veh_Fotos.Campo_No_Entrada + " = '" + Numero_Entrada + "' AND " + Ope_Tal_Ent_Veh_Fotos.Campo_Tipo_Foto + " = 'ATRAS'";
                        Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Aux != null)
                            Nueva_Fila["FOTO_ENTRADA_ATRAS"] = Aux.ToString();
                        else
                            Nueva_Fila["FOTO_ENTRADA_ATRAS"] = "";
                        //Obtenemos la foto del Frente para la Recepcion del Vehiculo
                        Mi_SQL = "SELECT " + Ope_Tal_Ent_Veh_Fotos.Campo_Ruta_Archivo + " AS IZQUIERDA FROM " + Ope_Tal_Ent_Veh_Fotos.Tabla_Ope_Tal_Ent_Veh_Fotos + " WHERE " + Ope_Tal_Ent_Veh_Fotos.Campo_No_Entrada + " = '" + Numero_Entrada + "' AND " + Ope_Tal_Ent_Veh_Fotos.Campo_Tipo_Foto + " = 'IZQUIERDA'";
                        Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Aux != null)
                            Nueva_Fila["FOTO_ENTRADA_IZQUIERDA"] = Aux.ToString();
                        else
                            Nueva_Fila["FOTO_ENTRADA_IZQUIERDA"] = "";
                        //Obtenemos la foto del Frente para la Recepcion del Vehiculo
                        Mi_SQL = "SELECT " + Ope_Tal_Ent_Veh_Fotos.Campo_Ruta_Archivo + " AS DERECHA FROM " + Ope_Tal_Ent_Veh_Fotos.Tabla_Ope_Tal_Ent_Veh_Fotos + " WHERE " + Ope_Tal_Ent_Veh_Fotos.Campo_No_Entrada + " = '" + Numero_Entrada + "' AND " + Ope_Tal_Ent_Veh_Fotos.Campo_Tipo_Foto + " = 'DERECHA'";
                        Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Aux != null)
                            Nueva_Fila["FOTO_ENTRADA_DERECHA"] = Aux.ToString();
                        else
                            Nueva_Fila["FOTO_ENTRADA_DERECHA"] = "";

                        Dt_Img_Entrada.Rows.Add(Nueva_Fila);

                        DataRow Nueva_Fila_Salida = Dt_Img_Salida.NewRow();

                        //Obtenemos la foto del Frente para la Entrega del Vehiculo
                        Mi_SQL = "SELECT " + Ope_Tal_Sal_Veh_Fotos.Campo_Ruta_Archivo + " AS FRENTE FROM " + Ope_Tal_Sal_Veh_Fotos.Tabla_Ope_Tal_Sal_Veh_Fotos + " WHERE " + Ope_Tal_Sal_Veh_Fotos.Campo_No_Salida + " = '" + Numero_Salida + "' AND " + Ope_Tal_Sal_Veh_Fotos.Campo_Tipo_Foto + " = 'FRENTE'";
                        Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Aux != null)
                            Nueva_Fila_Salida["FOTO_SALIDA_FRENTE"] = Aux.ToString();
                        else
                            Nueva_Fila_Salida["FOTO_SALIDA_FRENTE"] = "";
                        //Obtenemos la foto de la parte trasera para la Entrega del Vehiculo
                        Mi_SQL = "SELECT " + Ope_Tal_Sal_Veh_Fotos.Campo_Ruta_Archivo + " AS ATRAS FROM " + Ope_Tal_Sal_Veh_Fotos.Tabla_Ope_Tal_Sal_Veh_Fotos + " WHERE " + Ope_Tal_Sal_Veh_Fotos.Campo_No_Salida + " = '" + Numero_Salida + "' AND " + Ope_Tal_Sal_Veh_Fotos.Campo_Tipo_Foto + " = 'ATRAS'";
                        Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Aux != null)
                            Nueva_Fila_Salida["FOTO_SALIDA_ATRAS"] = Aux.ToString();
                        else
                            Nueva_Fila_Salida["FOTO_SALIDA_ATRAS"] = "";
                        //Obtenemos la foto del Frente para la Entrega del Vehiculo
                        Mi_SQL = "SELECT " + Ope_Tal_Sal_Veh_Fotos.Campo_Ruta_Archivo + " AS IZQUIERDA FROM " + Ope_Tal_Sal_Veh_Fotos.Tabla_Ope_Tal_Sal_Veh_Fotos + " WHERE " + Ope_Tal_Sal_Veh_Fotos.Campo_No_Salida + " = '" + Numero_Salida + "' AND " + Ope_Tal_Sal_Veh_Fotos.Campo_Tipo_Foto + " = 'IZQUIERDA'";
                        Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Aux != null)
                            Nueva_Fila_Salida["FOTO_SALIDA_IZQUIERDA"] = Aux.ToString();
                        else
                            Nueva_Fila_Salida["FOTO_SALIDA_IZQUIERDA"] = "";
                        //Obtenemos la foto del Frente para la Entrega del Vehiculo
                        Mi_SQL = "SELECT " + Ope_Tal_Sal_Veh_Fotos.Campo_Ruta_Archivo + " AS DERECHA FROM " + Ope_Tal_Sal_Veh_Fotos.Tabla_Ope_Tal_Sal_Veh_Fotos + " WHERE " + Ope_Tal_Sal_Veh_Fotos.Campo_No_Salida + " = '" + Numero_Salida + "' AND " + Ope_Tal_Sal_Veh_Fotos.Campo_Tipo_Foto + " = 'DERECHA'";
                        Aux = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Aux != null)
                            Nueva_Fila_Salida["FOTO_SALIDA_DERECHA"] = Aux.ToString();
                        else
                            Nueva_Fila_Salida["FOTO_SALIDA_DERECHA"] = "";

                        Dt_Img_Salida.Rows.Add(Nueva_Fila_Salida);
                        
                    }
                    
                    Ds_Datos.Tables.Add(Dt_Img_Entrada.Copy());
                    Ds_Datos.Tables[1].TableName = "DT_FOTOS_ENTRADA";
                    Ds_Datos.Tables.Add(Dt_Img_Salida.Copy());
                    Ds_Datos.Tables[2].TableName = "DT_FOTOS_SALIDA";

                    if (Ds_Datos != null)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Ds_Datos;
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
            ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
            ///PARAMETROS:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Marzo/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID) {
                String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
                try {
                    String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                    Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals("")) {
                        Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                    }
                } catch (SqlException Ex) {
                    new Exception(Ex.Message);
                }
                return Id;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
            ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
            ///PARAMETROS:     
            ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
            ///             2. Longitud_ID. Longitud que tendra el ID. 
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Marzo/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID) {
                String Retornar = "";
                String Dato = "" + Dato_ID;
                for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++) {
                    Retornar = Retornar + "0";
                }
                Retornar = Retornar + Dato;
                return Retornar;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Salida_Vehiculo
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 
            ///                     1.Parametros.Contiene los parametros que se van a utilizar para
            ///                       hacer la consulta de la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 12/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Cls_Ope_Tal_Salidas_Vehiculos_Negocio Consultar_Detalles_Salida_Vehiculo(Cls_Ope_Tal_Salidas_Vehiculos_Negocio Parametros) {
                String Mi_SQL = null;
                Cls_Ope_Tal_Salidas_Vehiculos_Negocio Obj_Cargado = new Cls_Ope_Tal_Salidas_Vehiculos_Negocio();
                Boolean Entro_Where = false;
                try
                {
                    Mi_SQL = "SELECT * FROM " + Ope_Tal_Salidas_Vehiculos.Tabla_Ope_Tal_Salidas_Vehiculos;
                    if (Parametros.P_No_Salida > (-1)) {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Salidas_Vehiculos.Campo_No_Salida + " = '" + Parametros.P_No_Salida + "'";
                    }
                    if (Parametros.P_No_Entrada > (-1)) {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Salidas_Vehiculos.Campo_No_Entrada + " = '" + Parametros.P_No_Entrada + "'";
                    }
                    if (Parametros.P_No_Solicitud > (-1)) {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Salidas_Vehiculos.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                    }
                    SqlDataReader Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    while (Reader.Read()) {
                        Obj_Cargado.P_No_Salida = (!String.IsNullOrEmpty(Reader[Ope_Tal_Salidas_Vehiculos.Campo_No_Salida].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Salidas_Vehiculos.Campo_No_Salida]) : -1;
                        Obj_Cargado.P_No_Entrada = (!String.IsNullOrEmpty(Reader[Ope_Tal_Salidas_Vehiculos.Campo_No_Entrada].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Salidas_Vehiculos.Campo_No_Entrada]) : -1;
                        Obj_Cargado.P_Tipo_Salida = (!String.IsNullOrEmpty(Reader[Ope_Tal_Salidas_Vehiculos.Campo_Tipo_Salida].ToString())) ? Reader[Ope_Tal_Salidas_Vehiculos.Campo_Tipo_Salida].ToString() : "";
                        Obj_Cargado.P_Vehiculo_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Salidas_Vehiculos.Campo_Vehiculo_ID].ToString())) ? Reader[Ope_Tal_Salidas_Vehiculos.Campo_Vehiculo_ID].ToString() : "";
                        Obj_Cargado.P_Fecha_Salida = (!String.IsNullOrEmpty(Reader[Ope_Tal_Salidas_Vehiculos.Campo_Fecha_Salida].ToString())) ? Convert.ToDateTime(Reader[Ope_Tal_Salidas_Vehiculos.Campo_Fecha_Salida]) : new DateTime();
                        Obj_Cargado.P_No_Solicitud = (!String.IsNullOrEmpty(Reader[Ope_Tal_Salidas_Vehiculos.Campo_No_Solicitud].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Salidas_Vehiculos.Campo_No_Solicitud]) : -1;
                        Obj_Cargado.P_Empleado_Entrego_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Salidas_Vehiculos.Campo_Empleado_Entrego_ID].ToString())) ? Reader[Ope_Tal_Salidas_Vehiculos.Campo_Empleado_Entrego_ID].ToString() : "";
                        Obj_Cargado.P_Empleado_Recibio_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Salidas_Vehiculos.Campo_Empleado_Recibio_ID].ToString())) ? Reader[Ope_Tal_Salidas_Vehiculos.Campo_Empleado_Recibio_ID].ToString() : "";
                        Obj_Cargado.P_Kilometraje = (!String.IsNullOrEmpty(Reader[Ope_Tal_Salidas_Vehiculos.Campo_Kilometraje].ToString())) ? Convert.ToDouble(Reader[Ope_Tal_Salidas_Vehiculos.Campo_Kilometraje]) : -1.0;
                        Obj_Cargado.P_Comentarios = (!String.IsNullOrEmpty(Reader[Ope_Tal_Salidas_Vehiculos.Campo_Comentarios].ToString())) ? Reader[Ope_Tal_Salidas_Vehiculos.Campo_Comentarios].ToString() : "";
                    }
                    Reader.Close();
                    if(Obj_Cargado.P_No_Salida>(-1)) {
                        Mi_SQL = "SELECT DETALLES." + Ope_Tal_Sal_Veh_Det.Campo_No_Registro + " AS NO_REGISTRO";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Sal_Veh_Det.Campo_No_Salida + " AS NO_ENTRADA";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Sal_Veh_Det.Campo_Parte_ID + " AS PARTE_ID";
                        Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Tal_Partes_Veh.Campo_Nombre + " FROM " + Cat_Tal_Partes_Veh.Tabla_Cat_Tal_Partes_Veh + " WHERE " + Cat_Tal_Partes_Veh.Campo_Parte_ID + " = DETALLES." + Ope_Tal_Sal_Veh_Det.Campo_Parte_ID + ") AS PARTE_NOMBRE";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Sal_Veh_Det.Campo_SubParte_ID + " AS SUBPARTE_ID";
                        Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Tal_Partes_Veh.Campo_Nombre + " FROM " + Cat_Tal_Partes_Veh.Tabla_Cat_Tal_Partes_Veh + " WHERE " + Cat_Tal_Partes_Veh.Campo_Parte_ID + " = DETALLES." + Ope_Tal_Sal_Veh_Det.Campo_SubParte_ID + ") AS SUBPARTE_NOMBRE";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Sal_Veh_Det.Campo_Valor + " AS VALOR";
                        Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Sal_Veh_Det.Tabla_Ope_Tal_Sal_Veh_Det + " DETALLES";
                        Mi_SQL = Mi_SQL + " WHERE DETALLES." + Ope_Tal_Sal_Veh_Det.Campo_No_Salida + " = '" + Obj_Cargado.P_No_Salida + "'";
                        DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Datos != null && Ds_Datos.Tables.Count > 0) {
                            Obj_Cargado.P_Dt_Detalles = Ds_Datos.Tables[0];
                        }
                    }
                    if(Obj_Cargado.P_No_Salida>(-1)) {
                        Mi_SQL = "SELECT * FROM " + Ope_Tal_Sal_Veh_Fotos.Tabla_Ope_Tal_Sal_Veh_Fotos + " WHERE " + Ope_Tal_Sal_Veh_Fotos.Campo_No_Salida + " = '" + Obj_Cargado.P_No_Salida + "'";
                        DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Datos != null && Ds_Datos.Tables.Count > 0) {
                            Obj_Cargado.P_Dt_Archivos = Ds_Datos.Tables[0];
                        }
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Obj_Cargado;
            }

        #endregion

	}
}
