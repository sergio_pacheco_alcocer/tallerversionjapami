﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Orden_Compra.Negocio;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Requisiciones.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Tal_Orden_Compra_Datos
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Operacion_Orden_Compra.Datos {
    public class Cls_Ope_Tal_Orden_Compra_Datos {

        #region METODOS

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
            ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
            ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
            ///            2.-Nombre de la tabla
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 10/Enero/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static Int32 Obtener_Consecutivo(String Campo_ID, String Tabla) {
                int Consecutivo = 0;
                String Mi_Sql;
                Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
                Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla;
                Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                Consecutivo = (Convert.ToInt32(Obj) + 1);
                return Consecutivo;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Guardar_Orden_Compra()
            ///DESCRIPCIÓN: Inserta un registro de una Orden de Compra
            ///PARAMETROS: 1.-Objeto de negocio con datos de Orden de Compra
            ///Se debe setear previo la propiedad LISTA_REQUISICIONES
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 19/Enero/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static DataTable Guardar_Orden_Compra(Cls_Ope_Tal_Orden_Compra_Negocio Negocio) {
                String Mi_Sql = "";
                String Proveedor_ID = "";
                String Proveedor_Nombre = "";
                Cls_Tal_Parametros_Negocio Negocio_Parametros = new Cls_Tal_Parametros_Negocio();
                Negocio_Parametros = Negocio_Parametros.Consulta_Parametros_Presupuesto(Negocio.P_Numero_Economico,"VEHICULO");

                DataTable Dt_Orden_Compra = new DataTable("ORDENES_COMPRA");
                DataColumn Dc_Temporal = null;
                Dc_Temporal = new DataColumn("NO_ORDEN_COMPRA", System.Type.GetType("System.String"));
                Dt_Orden_Compra.Columns.Add(Dc_Temporal);
                Dc_Temporal = new DataColumn("FOLIO", System.Type.GetType("System.String"));
                Dt_Orden_Compra.Columns.Add(Dc_Temporal);
                Dc_Temporal = new DataColumn("PROVEEDOR_ID", System.Type.GetType("System.String"));
                Dt_Orden_Compra.Columns.Add(Dc_Temporal);
                Dc_Temporal = new DataColumn("NOMBRE_PROVEEDOR", System.Type.GetType("System.String"));
                Dt_Orden_Compra.Columns.Add(Dc_Temporal);
                Dc_Temporal = new DataColumn("SUBTOTAL", System.Type.GetType("System.String"));
                Dt_Orden_Compra.Columns.Add(Dc_Temporal);
                Dc_Temporal = new DataColumn("IEPS", System.Type.GetType("System.String"));
                Dt_Orden_Compra.Columns.Add(Dc_Temporal);
                Dc_Temporal = new DataColumn("IVA", System.Type.GetType("System.String"));
                Dt_Orden_Compra.Columns.Add(Dc_Temporal);
                Dc_Temporal = new DataColumn("TOTAL", System.Type.GetType("System.String"));
                Dt_Orden_Compra.Columns.Add(Dc_Temporal);
                Dc_Temporal = new DataColumn("LISTA_REQUISICIONES", System.Type.GetType("System.String"));
                Dt_Orden_Compra.Columns.Add(Dc_Temporal);
                DataRow Dr_Temporal = null;
                char[] Ch = {','};          

                String[] Requisiciones = Negocio.P_Lista_Requisiciones.Split(Ch);

                Mi_Sql = "SELECT DISTINCT(" + Ope_Tal_Req_Refaccion.Campo_Proveedor_ID + "), " +
                        Ope_Tal_Req_Refaccion.Campo_Nombre_Proveedor +
                        " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion +
                        " WHERE " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID +
                        " IN (" + Negocio.P_Lista_Requisiciones + ")";
                DataTable Dt_Proveedores = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql).Tables[0];

                Mi_Sql = "SELECT *FROM " +
                        Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion +
                        " WHERE " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID +
                        " IN (" + Negocio.P_Lista_Requisiciones + ")";
                DataTable Dt_Articulos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql).Tables[0];
                Int32 No_Orden_Compra = Obtener_Consecutivo(Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra, Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra);
                SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
                SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
                SqlTransaction Transaccion_SQL;
                SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();                                            //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
                if (Conexion_Base.State != ConnectionState.Open)
                {
                    Conexion_Base.Open(); //Abre la conexión a la base de datos
                }
                Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
                Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
                Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
                try
                {               
                    foreach (DataRow Renglon in Dt_Proveedores.Rows)
                    {
                        Dr_Temporal = Dt_Orden_Compra.NewRow();
                        //Calcular Datos 
                        Negocio.P_Subtotal = 0;
                        Negocio.P_Total_IEPS = 0;
                        Negocio.P_Total_IVA = 0;
                        Negocio.P_Total = 0;
                        String Lista_Requisiciones = "";
                        foreach (DataRow Articulo in Dt_Articulos.Rows) 
                        {
                            if (Articulo["PROVEEDOR_ID"].ToString().Trim() == Renglon["PROVEEDOR_ID"].ToString().Trim()) 
                            {
                                String Str_Aux = "0";
                                Str_Aux = Articulo["SUBTOTAL_COTIZADO"].ToString() != null && 
                                            Articulo["SUBTOTAL_COTIZADO"].ToString().Trim().Length > 0 ? 
                                                Articulo["SUBTOTAL_COTIZADO"].ToString().Trim() : "0";                            
                                Negocio.P_Subtotal += Convert.ToDouble(Str_Aux);
                                Str_Aux = Articulo["IEPS_COTIZADO"].ToString() != null && 
                                            Articulo["IEPS_COTIZADO"].ToString().Trim().Length > 0 ? 
                                                Articulo["IEPS_COTIZADO"].ToString().Trim() : "0"; 
                                Negocio.P_Total_IEPS += Convert.ToDouble(Str_Aux);
                                Str_Aux = Articulo["IVA_COTIZADO"].ToString() != null &&
                                            Articulo["IVA_COTIZADO"].ToString().Trim().Length > 0 ?
                                                Articulo["IVA_COTIZADO"].ToString().Trim() : "0";
                                Negocio.P_Total_IVA += Convert.ToDouble(Str_Aux);
                                Str_Aux = Articulo["TOTAL_COTIZADO"].ToString() != null &&
                                            Articulo["TOTAL_COTIZADO"].ToString().Trim().Length > 0 ?
                                                Articulo["TOTAL_COTIZADO"].ToString().Trim() : "0";
                                Negocio.P_Total += Convert.ToDouble(Str_Aux);
                                Lista_Requisiciones += Articulo["NO_REQUISICION"].ToString().Trim() + ",";
                            }
                        }
                        //Quitar el último caracter
                        if (Lista_Requisiciones.Length > 0)
                        {
                            Lista_Requisiciones = Lista_Requisiciones.Substring(0, Lista_Requisiciones.Length - 1);
                        }
                        //Eliminar Requisas repetidas                    
                        String[] Tmp_Requisas = Lista_Requisiciones.Split(Ch);
                        String Respuesta = "";
                        for (int i = 0; i < Tmp_Requisas.Length; i++)
                        {
                            if (!Respuesta.Contains(Tmp_Requisas[i]))
                            {
                                Respuesta += Tmp_Requisas[i] + ",";
                            }
                        }
                        if (Respuesta.Length > 0)
                        {
                            Respuesta = Respuesta.Substring(0, Respuesta.Length - 1);
                        }
                        //formatear fecha
                        DateTime _DateTime = Convert.ToDateTime(Negocio.P_Fecha_Entrega);
                        Negocio.P_Fecha_Entrega = _DateTime.ToString("dd/MM/yy");
                        Lista_Requisiciones = Respuesta;
                        //LLENAR DATOS DE RESPUESTA
                        Dr_Temporal["NO_ORDEN_COMPRA"] = No_Orden_Compra + "";
                        Dr_Temporal["FOLIO"] = "OC-" + No_Orden_Compra;
                        Dr_Temporal["PROVEEDOR_ID"] = Dt_Articulos.Rows[0]["PROVEEDOR_ID"].ToString().Trim();
                        Dr_Temporal["NOMBRE_PROVEEDOR"] = Dt_Articulos.Rows[0]["NOMBRE_PROVEEDOR"].ToString().Trim();
                        Dr_Temporal["SUBTOTAL"] = Negocio.P_Subtotal.ToString();
                        Dr_Temporal["IEPS"] = Negocio.P_Total_IEPS.ToString();
                        Dr_Temporal["IVA"] = Negocio.P_Total_IVA.ToString();
                        Dr_Temporal["TOTAL"] = Negocio.P_Total.ToString(); 
                        Dr_Temporal["LISTA_REQUISICIONES"] = Lista_Requisiciones;

                        Comando_SQL.CommandText = "set LANGUAGE Spanish";
                        Comando_SQL.ExecuteNonQuery();

                        //Guardar una orden de compra por proveedor
                        Mi_Sql = "INSERT INTO " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra +
                        " (" +
                        Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + "," +
                        Ope_Tal_Ordenes_Compra.Campo_Folio + "," +
                        Ope_Tal_Ordenes_Compra.Campo_Tipo_Proceso + "," +
                        Ope_Tal_Ordenes_Compra.Campo_Estatus + "," +
                        Ope_Tal_Ordenes_Compra.Campo_Proveedor_ID + "," +
                        Ope_Tal_Ordenes_Compra.Campo_Nombre_Proveedor + "," +
                        Ope_Tal_Ordenes_Compra.Campo_Subtotal + "," +
                        Ope_Tal_Ordenes_Compra.Campo_Total_IEPS + "," +
                        Ope_Tal_Ordenes_Compra.Campo_Total_IVA + "," +
                        Ope_Tal_Ordenes_Compra.Campo_Total + "," +
                        Ope_Tal_Ordenes_Compra.Campo_Lista_Requisiciones + "," +
                        Ope_Tal_Ordenes_Compra.Campo_Tipo_Articulo + "," +
                        Ope_Tal_Ordenes_Compra.Campo_Fecha_Entrega + "," +
                        Ope_Tal_Ordenes_Compra.Campo_Usuario_Creo + "," +
                        Ope_Tal_Ordenes_Compra.Campo_Fecha_Creo;
                        if (!String.IsNullOrEmpty(Negocio.P_No_Reserva)) {
                            Mi_Sql += "," + Ope_Tal_Ordenes_Compra.Campo_No_Reserva;
                        }
                        Mi_Sql += ") VALUES (" +
                        No_Orden_Compra + ",'" +
                        "OC-" + No_Orden_Compra + "','" +
                        Negocio.P_Tipo_Compra + "','" +
                        Negocio.P_Estatus + "','" +
                        Renglon["PROVEEDOR_ID"].ToString().Trim() + "','" +
                        Renglon["NOMBRE_PROVEEDOR"].ToString().Trim() + "'," +
                        Negocio.P_Subtotal + "," +
                        Negocio.P_Total_IEPS + "," +
                        Negocio.P_Total_IVA + "," +
                        Negocio.P_Total + ",'" +
                        Lista_Requisiciones + "','" +
                        Negocio.P_Tipo_Articulo + "','" +
                        Negocio.P_Fecha_Entrega + "','" +
                        Cls_Sessiones.Nombre_Empleado + "'," +
                        "GETDATE()";
                        if (!String.IsNullOrEmpty(Negocio.P_No_Reserva))
                        {
                            Mi_Sql += ",'" + Negocio.P_No_Reserva + "'";
                        }                    
                        Mi_Sql += ")";
                        Comando_SQL.CommandText = Mi_Sql;
                        Comando_SQL.ExecuteNonQuery();
                        Proveedor_ID = Renglon["PROVEEDOR_ID"].ToString().Trim();
                        Proveedor_Nombre = Renglon["NOMBRE_PROVEEDOR"].ToString().Trim();
                        Mi_Sql = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " SET " +
                        Ope_Tal_Requisiciones.Campo_No_Orden_Compra + " = " + No_Orden_Compra +
                        " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = " + Negocio.P_No_Requisicion;
                        Comando_SQL.CommandText = Mi_Sql;
                        Comando_SQL.ExecuteNonQuery();

                        Registrar_Movimiento_Historial(No_Orden_Compra.ToString(), Negocio.P_Usuario, "GENERADA", ref Comando_SQL);

                        //Guardar detalles de la orden de compra, los articulos
                        Mi_Sql = "UPDATE " +
                        Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion +
                        " SET " + Ope_Tal_Req_Refaccion.Campo_No_Orden_Compra + " = " + No_Orden_Compra +
                        " WHERE " + Ope_Tal_Req_Refaccion.Campo_Proveedor_ID + " = '" +
                        Renglon["PROVEEDOR_ID"].ToString().Trim() + "' AND " +
                        Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " IN (" + Negocio.P_Lista_Requisiciones + ")";
                        Comando_SQL.CommandText = Mi_Sql;
                        Comando_SQL.ExecuteNonQuery();
                        Dt_Orden_Compra.Rows.Add(Dr_Temporal);
                        No_Orden_Compra++; 
                    }
                    //Actualizar requisiciones
                    Mi_Sql = "UPDATE " +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                    " SET " + Ope_Tal_Requisiciones.Campo_Estatus + " = 'COMPRA', " +
                    Ope_Tal_Requisiciones.Campo_Tipo_Compra + " = '" + Negocio.P_Tipo_Compra + "', " +
                    Ope_Tal_Requisiciones.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "', " +
                    Ope_Tal_Requisiciones.Campo_Fecha_Modifico + " = GETDATE()" +
                    " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " IN (" + Negocio.P_Lista_Requisiciones + ")";
                    Comando_SQL.CommandText = Mi_Sql;
                    Comando_SQL.ExecuteNonQuery();

                    ///************************/
                    ///************************/
                    ///************************/
                    ///************************/


                    //tabla
                    //Cosnsultamos los datos de la Partida
                    //agregamos las columnas al datatable
                    DataTable Dt_Registros = new DataTable();
                    DataRow Fila;
                    Dt_Registros.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("ANIO", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("IMPORTE", System.Type.GetType("System.Double"));

                    Fila = Dt_Registros.NewRow();
                    Fila["FUENTE_FINANCIAMIENTO_ID"] = Negocio_Parametros.P_Fuente_Financiamiento_ID;
                    Fila["PROGRAMA_ID"] = Negocio.P_Programa_ID;
                    Fila["DEPENDENCIA_ID"] = Negocio.P_Dependencia_ID.Trim();
                    Fila["PARTIDA_ID"] = Negocio_Parametros.P_Partida_ID;
                    Fila["ANIO"] = DateTime.Today.Year.ToString();
                    Fila["IMPORTE"] = Negocio.P_Total;
                    Dt_Registros.Rows.Add(Fila);

                    //ACTUALIZAR RESERVA
                    DataTable Dt_Reservas = new DataTable();
                    String Mi_SQL = "SELECT " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ".*" +
                    " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + ", " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                    " WHERE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + "." + Ope_Psp_Reservas.Campo_No_Reserva + " = " +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Numero_Reserva + " AND " +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = " + Negocio.P_No_Requisicion.ToString();
                    Comando_SQL.CommandText = Mi_SQL;
                    Obj_Adaptador.SelectCommand = Comando_SQL;
                    Obj_Adaptador.Fill(Dt_Reservas);

                    if (Dt_Reservas.Rows.Count > 0)
                    {
                        String No_Reserva = Dt_Reservas.Rows[0][Ope_Psp_Reservas.Campo_No_Reserva].ToString();
                        String Total = Dt_Reservas.Rows[0][Ope_Psp_Reservas.Campo_Saldo].ToString();
                        //actualizar proveedor y beneficiario

                        Cls_Tal_Parametros_Negocio Par_Neg = new Cls_Tal_Parametros_Negocio();
                        Par_Neg = Par_Neg.Consulta_Parametros();

                        Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Reserva_De_Compra(
                            Convert.ToInt32(No_Reserva),
                            Proveedor_ID,
                            "P-" + Proveedor_Nombre);
                        //mensuales
                        Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual
                            (Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO,
                            Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO,
                            Dt_Registros,
                            Comando_SQL);
                        //Registrar movimiento presupuestal
                        Cls_Ope_Psp_Manejo_Presupuesto.
                            Registro_Movimiento_Presupuestal(
                                No_Reserva,
                                Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO,
                                Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO,
                                Convert.ToDouble(Total),
                                "",
                                "",
                                "",
                                "", Comando_SQL);
                    }


                    ///************************/
                    ///************************/
                    ///************************/
                    ///************************/


                   
                    //Registar Historial de requisición
                    Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Tal_Requisiciones_Negocio();
                    Requisicion_Negocio.Registrar_Historial("COMPRA",Negocio.P_Lista_Requisiciones);

                    Transaccion_SQL.Commit();
                }
                catch (SqlException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    Dt_Orden_Compra = null;
                    throw new Exception("Información: " + Ex.Message);
                }
                catch (DBConcurrencyException Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    Dt_Orden_Compra = null;
                    throw new Exception("Los datos fueron actualizados por otro Usuario. Información: [" + Ex.Message + "]");
                }
                catch (Exception Ex)
                {
                    if (Transaccion_SQL != null)
                    {
                        Transaccion_SQL.Rollback();
                    }
                    Dt_Orden_Compra = null;
                    throw new Exception("Información: " + Ex.Message);
                }
                finally
                {
                    Conexion_Base.Close();
                } 
                return Dt_Orden_Compra;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Ordenes_Compra
            ///DESCRIPCIÓN: Consulta loas ordenes de compra en base a parametros y devuelve un 
            ///DataTable con la informacion solicitada, se le debe setear FechaInicial, FechaFinal 
            ///Estatus
            ///PARAMETROS: 1.-Objeto de Negocio de Orden de Compra
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static DataTable Consultar_Ordenes_Compra(Cls_Ope_Tal_Orden_Compra_Negocio Negocio)
            {
                String Mi_Sql = "";
                if (Negocio.P_Folio != null && Negocio.P_Folio != "")
                {
                    Negocio.P_Folio = Negocio.P_Folio.Replace("OC-","");
                    Mi_Sql = "SELECT " +
                    Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + ".*," +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Justificacion_Compra + "," +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Codigo_Programatico + " AS CODIGO" +
                    ",(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " +
                    Cat_Dependencias.Campo_Dependencia_ID + " = " +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." +
                    Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                    ") AS NOMBRE_DEPENDENCIA," +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                    " FROM " +
                    Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " JOIN " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                    " ON " +
                    Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = " +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_No_Orden_Compra +
                    " WHERE " +
                    Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = '" + 
                    Negocio.P_Folio + "'";
                }
                else
                {
                    Mi_Sql = "SELECT " +
                    Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + ".*," +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Justificacion_Compra + "," +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Codigo_Programatico + " AS CODIGO" + 
                    ",(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " +
                    Cat_Dependencias.Campo_Dependencia_ID + " = " +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." +
                    Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                    ") AS NOMBRE_DEPENDENCIA," +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                    " FROM " +
                    Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " JOIN " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                    " ON " +
                    Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = " +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_No_Orden_Compra +
                    " WHERE " +
                    "REPLACE(CONVERT(VARCHAR(11)," + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Creo + ",106),' ','/')" +
                            " >= '" + Negocio.P_Fecha_Inicial + "' AND " +
                    "REPLACE(CONVERT(VARCHAR(11)," + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Creo + ",106),' ','/')" +
                            " <= '" + Negocio.P_Fecha_Final + "'";
                    if (!String.IsNullOrEmpty(Negocio.P_Estatus))
                    {
                        Mi_Sql = Mi_Sql + " AND " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." +
                        Ope_Tal_Ordenes_Compra.Campo_Estatus + " IN ('" + Negocio.P_Estatus + "')";
                    }
                }
                DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                DataTable _DataTable = null;
                if (_DataSet != null && _DataSet.Tables.Count > 0 && _DataSet.Tables[0].Rows.Count > 0)
                {
                    _DataTable = _DataSet.Tables[0];
                }
                return _DataTable;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Requisiciones_Directas
            ///DESCRIPCIÓN: Consulta las Requisiciones confirmadas y que son para compra directa
            ///y devuelve DataTable con la informacion solicitada
            ///PARAMETROS: 1.-Objeto de Negocio de Orden de Compra
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static DataTable Consultar_Requisiciones_Directas(Cls_Ope_Tal_Orden_Compra_Negocio Negocio)
            {
                String Mi_Sql = "";

                if (Negocio.P_Folio != null && Negocio.P_Folio != "")
                {
                    Mi_Sql = "SELECT " + "rq.*";
                    Mi_Sql = Mi_Sql + ", (select " + Ope_Pat_Vehiculos.Campo_Numero_Economico + " from " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos;
                    Mi_Sql = Mi_Sql + " where " + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + " = sol." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + ") NUMERO_ECONOMICO ";
                    Mi_Sql = Mi_Sql + ",(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
                    Mi_Sql = Mi_Sql + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = ";
                    Mi_Sql = Mi_Sql + "rq." + Ope_Tal_Requisiciones.Campo_Dependencia_ID + ") AS UNIDAD_RESPONSABLE";
                    Mi_Sql = Mi_Sql + " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " rq";
                    Mi_Sql = Mi_Sql + " INNER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " sol ON rq." + Ope_Tal_Requisiciones.Campo_No_Solicitud + " = sol." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud;
                    Mi_Sql = Mi_Sql + " WHERE rq." + Ope_Tal_Requisiciones.Campo_Folio + " = '" + Negocio.P_Folio + "'";
                    Mi_Sql = Mi_Sql + " AND sol." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien + " = 'VEHICULO'";
                    //Unir con Tabla de Bienes
                    Mi_Sql = Mi_Sql + "SELECT " + "rq.*";
                    Mi_Sql = Mi_Sql + ", (select str(" + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + ") from " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles;
                    Mi_Sql = Mi_Sql + " where " + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + " = sol." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + ") NUMERO_ECONOMICO ";
                    Mi_Sql = Mi_Sql + ",(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
                    Mi_Sql = Mi_Sql + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = ";
                    Mi_Sql = Mi_Sql + "rq." + Ope_Tal_Requisiciones.Campo_Dependencia_ID + ") AS UNIDAD_RESPONSABLE";
                    Mi_Sql = Mi_Sql + " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " rq";
                    Mi_Sql = Mi_Sql + " INNER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " sol ON rq." + Ope_Tal_Requisiciones.Campo_No_Solicitud + " = sol." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud;
                    Mi_Sql = Mi_Sql + " WHERE rq." + Ope_Tal_Requisiciones.Campo_Folio + " = '" + Negocio.P_Folio + "'";
                    Mi_Sql = Mi_Sql + " AND sol." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien + " = 'BIEN_MUEBLE'";

                }
                else
                {
                    Mi_Sql = "SELECT " + "rq.*";
                    Mi_Sql = Mi_Sql + ",(select " + Ope_Pat_Vehiculos.Campo_Numero_Economico;
                    Mi_Sql = Mi_Sql + " from " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos;
                    Mi_Sql = Mi_Sql + " where " + Ope_Pat_Vehiculos.Campo_Vehiculo_ID;
                    Mi_Sql = Mi_Sql + " = sol." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + ") NUMERO_ECONOMICO ";
                    Mi_Sql = Mi_Sql + ",(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
                    Mi_Sql = Mi_Sql + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = ";
                    Mi_Sql = Mi_Sql + "rq." + Ope_Tal_Requisiciones.Campo_Dependencia_ID + ") AS UNIDAD_RESPONSABLE";
                    Mi_Sql = Mi_Sql + " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " rq" ;
                    Mi_Sql = Mi_Sql + " INNER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " sol ON rq." + Ope_Tal_Requisiciones.Campo_No_Solicitud + " = sol." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud;
                    Mi_Sql = Mi_Sql + " WHERE ";
                    Mi_Sql = Mi_Sql + "rq." + Ope_Tal_Requisiciones.Campo_Estatus + " = 'CONFIRMADA'";
                    Mi_Sql = Mi_Sql + " AND sol." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien + " = 'VEHICULO'";
                    if (Negocio.P_Cotizador_ID != null && Negocio.P_Cotizador_ID != "0")
                    {
                        Mi_Sql = Mi_Sql + " AND rq." +
                            Ope_Tal_Requisiciones.Campo_Cotizador_ID + " ='" + Negocio.P_Cotizador_ID + "'";
                    }
                    //Union con Bienes Muebles
                    Mi_Sql = Mi_Sql + " UNION SELECT " + "rq.*";
                    Mi_Sql = Mi_Sql + ",(select str(" + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario;
                    Mi_Sql = Mi_Sql + ") from " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles;
                    Mi_Sql = Mi_Sql + " where " + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID;
                    Mi_Sql = Mi_Sql + " = sol." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + ") NUMERO_ECONOMICO ";
                    Mi_Sql = Mi_Sql + ",(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
                    Mi_Sql = Mi_Sql + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = ";
                    Mi_Sql = Mi_Sql + "rq." + Ope_Tal_Requisiciones.Campo_Dependencia_ID + ") AS UNIDAD_RESPONSABLE";
                    Mi_Sql = Mi_Sql + " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " rq";
                    Mi_Sql = Mi_Sql + " INNER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " sol ON rq." + Ope_Tal_Requisiciones.Campo_No_Solicitud + " = sol." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud;
                    Mi_Sql = Mi_Sql + " WHERE ";
                    Mi_Sql = Mi_Sql + "rq." + Ope_Tal_Requisiciones.Campo_Estatus + " = 'CONFIRMADA'";
                    Mi_Sql = Mi_Sql + " AND sol." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien + " = 'BIEN_MUEBLE'";
                    if (Negocio.P_Cotizador_ID != null && Negocio.P_Cotizador_ID != "0")
                    {
                        Mi_Sql = Mi_Sql + " AND rq." +
                            Ope_Tal_Requisiciones.Campo_Cotizador_ID + " ='" + Negocio.P_Cotizador_ID + "'";
                    }
                }
                    
                
                DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                DataTable _DataTable = null;
                if (_DataSet != null && _DataSet.Tables.Count > 0 && _DataSet.Tables[0].Rows.Count > 0)
                {
                    _DataTable = _DataSet.Tables[0];
                }
                return _DataTable;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Licitaciones
            ///DESCRIPCIÓN: Consulta las Licitaciones que tienen estatus de TERMINADA
            ///y devuelve DataTable con la informacion solicitada
            ///PARAMETROS: 1.-Objeto de Negocio de Orden de Compra
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static DataTable Consultar_Licitaciones(Cls_Ope_Tal_Orden_Compra_Negocio Negocio)
            {
                //String Mi_Sql = "";
                //if (Negocio.P_Folio != null && Negocio.P_Folio != "")
                //{
                //    Mi_Sql = "SELECT *FROM " + Ope_Com_Licitaciones.Tabla_Ope_Com_Licitaciones +
                //    " WHERE " + Ope_Com_Licitaciones.Campo_Folio + " = '" + Negocio.P_Folio + "'";
                //}
                //else
                //{
                //    Mi_Sql = "SELECT *FROM " + Ope_Com_Licitaciones.Tabla_Ope_Com_Licitaciones +
                //    " WHERE " +
                //    Ope_Com_Licitaciones.Campo_Estatus + " IN ('" + Negocio.P_Estatus + "')" +
                //    " ORDER BY " + Ope_Com_Licitaciones.Campo_No_Licitacion + " DESC";
                //}
                //DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                DataTable _DataTable = null;
                //if (_DataSet != null && _DataSet.Tables.Count > 0 && _DataSet.Tables[0].Rows.Count > 0)
                //{
                //    _DataTable = _DataSet.Tables[0];
                //}
                return _DataTable;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Comite_Compras
            ///DESCRIPCIÓN: Consulta Comite de Compra que tienen estatus de TERMINADA
            ///y devuelve DataTable con la informacion solicitada
            ///PARAMETROS: 1.-Objeto de Negocio de Orden de Compra
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static DataTable Consultar_Comite_Compras(Cls_Ope_Tal_Orden_Compra_Negocio Negocio) {
                return null;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Cotizaciones
            ///DESCRIPCIÓN: Consulta Cotizaciones que tienen estatus de TERMINADA
            ///y devuelve DataTable con la informacion solicitada
            ///PARAMETROS: 1.-Objeto de Negocio de Orden de Compra
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static DataTable Consultar_Cotizaciones(Cls_Ope_Tal_Orden_Compra_Negocio Negocio) {
                return null;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Actualizar_Orden_Compra
            ///DESCRIPCIÓN: Actualiza las ordenes de compra
            ///y devuelve DataTable con la informacion solicitada
            ///PARAMETROS: 1.-Objeto de Negocio de Orden de Compra
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static int Actualizar_Orden_Compra(Cls_Ope_Tal_Orden_Compra_Negocio Negocio)
            {
                String Mi_Sql = "";
                Mi_Sql = "UPDATE " +
                Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " SET " +
                Ope_Tal_Ordenes_Compra.Campo_Estatus + " = '" + Negocio.P_Estatus + "', " +
                Ope_Tal_Ordenes_Compra.Campo_Comentarios + " = '" + Negocio.P_Comentarios + "', " +
                Ope_Tal_Ordenes_Compra.Campo_No_Reserva + " = '" + Negocio.P_No_Reserva + "'" +
                " WHERE " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Negocio.P_No_Orden_Compra;
                int Registros_Afectados = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                return Registros_Afectados;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Actualizar_Impresion
            ///DESCRIPCIÓN: Actualiza las ordenes de compra
            ///y devuelve DataTable con la informacion solicitada
            ///PARAMETROS: 1.-Objeto de Negocio de Orden de Compra
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static Int32 Actualizar_Impresion(Cls_Ope_Tal_Orden_Compra_Negocio Negocio)
            {
                int Registros_Afectados = 0;
                try
                {
                    String Mi_Sql = "";
                    Mi_Sql = "UPDATE " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra +
                    " SET " +  Ope_Tal_Ordenes_Compra.Campo_Impresa + " = 'SI'" +
                    " WHERE " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Negocio.P_No_Orden_Compra;
                    Registros_Afectados = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                }
                catch (Exception Ex)
                {
                    Registros_Afectados = 0;
                    throw new Exception(Ex.ToString());
                }
                return Registros_Afectados;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Dias_Plazo
            ///DESCRIPCIÓN: Consulta las Requisiciones confirmadas y que son para compra directa
            ///y devuelve DataTable con la informacion solicitada
            ///PARAMETROS: 1.-Objeto de Negocio de Orden de Compra
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static Int32 Consultar_Dias_Plazo()
            {
                Int32 Respuesta = 0;
                //String Mi_Sql = "SELECT " +
                //Cat_Com_Parametros.Campo_Plazo_Surtir_Orden_Compra +
                //" FROM " + Cat_Com_Parametros.Tabla_Cat_Com_Parametros;
                //DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                //DataTable _DataTable = null;
                //if (_DataSet != null && _DataSet.Tables.Count > 0 && _DataSet.Tables[0].Rows.Count > 0)
                //{
                //    try {
                //        String Dato = 
                //        _DataSet.Tables[0].Rows[0][Cat_Com_Parametros.Campo_Plazo_Surtir_Orden_Compra].ToString().Trim();
                //        Respuesta = int.Parse(Dato);
                //    } 
                //    catch (Exception e)
                //    {
                //        e.ToString();
                //        Respuesta = 0;
                //    }
                //}
                return Respuesta;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Dias_Entrega_Proveedor
            ///DESCRIPCIÓN:
            ///PARAMETROS: 1.-Objeto de Negocio de Orden de Compra
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 12 Oct 2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static Int32 Consultar_Dias_Entrega_Proveedor(Cls_Ope_Tal_Orden_Compra_Negocio Negocio)
            {
                //SELECT TIEMPO_ENTREGA FROM OPE_COM_PROPUESTA_COTIZACION WHERE RESULTADO='ACEPTADO' AND NO_REQUISICION=16
                int Respuesta = 0;
                String Mi_Sql = "SELECT " +
                Ope_Tal_Propuesta_Cotizacion.Campo_Tiempo_Entrega +
                " FROM " +
                Ope_Tal_Propuesta_Cotizacion.Tabla_Ope_Tal_Propuesta_Cotizacion +
                " WHERE " +
                Ope_Tal_Propuesta_Cotizacion.Campo_Resultado + "='ACEPTADA'" +
                " AND " +
                    Ope_Tal_Propuesta_Cotizacion.Campo_No_Requisicion + " = " + Negocio.P_No_Requisicion;
                Object Objeto = null;
                int Dias_Entrega = 0;
                try
                {
                    Objeto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                    Dias_Entrega = int.Parse(Objeto.ToString());            
                }
                catch (Exception e)
                {
                    e.ToString();
                    Dias_Entrega = 0;
                }
                return Dias_Entrega;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Cotizadores
            ///DESCRIPCIÓN:
            ///PARAMETROS: 1.-Objeto de Negocio de Orden de Compra
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 14 Oct 2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static DataTable Consultar_Cotizadores(Cls_Ope_Tal_Orden_Compra_Negocio Negocio)
            {
                //SELECT TIEMPO_ENTREGA FROM OPE_COM_PROPUESTA_COTIZACION WHERE RESULTADO='ACEPTADO' AND NO_REQUISICION=16
                DataTable Dt_Cotizadores = null;
                String Mi_Sql = "SELECT " +
                Cat_Tal_Cotizadores.Campo_Empleado_ID + "," +
                Cat_Tal_Cotizadores.Campo_Nombre_Completo +
                " FROM " +
                Cat_Tal_Cotizadores.Tabla_Cat_Tal_Cotizadores;
                try
                {
                    Dt_Cotizadores = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql).Tables[0];                
                }
                catch (Exception e)
                {
                    Dt_Cotizadores = null;
                    throw new Exception(e.ToString());
                }
                return Dt_Cotizadores;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Codigo_Programatico_RQ
            ///DESCRIPCIÓN:
            ///PARAMETROS: 1.-Objeto de Negocio de Orden de Compra
            ///CREO: Susana Trigueros Armenta
            ///FECHA_CREO: 17 NOV 2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static DataTable Consultar_Codigo_Programatico_RQ(Cls_Ope_Tal_Orden_Compra_Negocio Negocio)
            {
                String Mi_SQL = "";

                Mi_SQL = "SELECT REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Partida_ID +
                                 ",REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID +
                                 ", (SELECT " + Ope_Tal_Requisiciones.Campo_Total +
                                 " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                                 " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                                 "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS TOTAL" +
                                 ", (SELECT " + Ope_Tal_Requisiciones.Campo_Total_Cotizado +
                                 " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                                 " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                                 "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS TOTAL_COTIZADO" +
                                 ", (SELECT " + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                                 " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                                 " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                                 "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS DEPENDENCIA_ID" +
                                 ", REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID +
                                 ", (SELECT NUM_RESERVA" +
                                 " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                                 " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                                 "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS NUM_RESERVA" +
                                 " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " REQ_DET" +
                                 " WHERE REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + "='" + Negocio.P_No_Requisicion + "'";
                DataTable Dt_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Requisicion;

            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Registrar_Movimiento_Presupuesto
            ///DESCRIPCIÓN:
            ///PARAMETROS: 1.-Objeto de Negocio de Orden de Compra
            ///CREO: Susana Trigueros Armenta
            ///FECHA_CREO: 17 NOV 2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static void Registrar_Movimiento_Presupuesto(Cls_Ope_Tal_Orden_Compra_Negocio Negocio) {
                DataTable Dt_Requisicion = Consultar_Codigo_Programatico_RQ(Negocio);

                String Partida_ID = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString().Trim();
                String Proyecto_ID = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString().Trim();
                String Dependencia_ID = Dt_Requisicion.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
                String FF = Dt_Requisicion.Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                String No_Reserva = Dt_Requisicion.Rows[0]["NUM_RESERVA"].ToString().Trim();

                Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(No_Reserva, Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Negocio.P_Total);
                Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(No_Reserva, Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Negocio.P_Total, "", "", "", "");
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Registrar_Movimiento_Historial
            ///DESCRIPCIÓN: Registrar_Movimiento_Historial
            ///PARAMETROS: 
            ///CREO: Susana Trigueros Armenta
            ///FECHA_CREO: 17 NOV 2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 20/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static void Registrar_Movimiento_Historial(String No_OC, String Empleado, String Estatus, ref SqlCommand Cmd) {
                try
                {
                    String Mi_SQL = "";
                    Mi_SQL = "INSERT INTO " + Ope_Tal_Historial_Ord_Compra.Tabla_Ope_Tal_Historial_Ord_Compra + " (" +
                        Ope_Tal_Historial_Ord_Compra.Campo_Estatus + "," +
                        Ope_Tal_Historial_Ord_Compra.Campo_Fecha + "," +
                        Ope_Tal_Historial_Ord_Compra.Campo_Empleado + "," +
                        Ope_Tal_Historial_Ord_Compra.Campo_No_Orden_Compra + ") VALUES ('" +
                        Estatus + "', GETDATE(), '" +
                        Empleado + "', '" +
                        No_OC + "')";
                    Cmd.CommandText = Mi_SQL.Trim();
                    Cmd.ExecuteNonQuery();
                }
                catch (Exception Ex)
                {
                    String Str_Ex = Ex.ToString();
                }
            }

        #endregion

	}
}
