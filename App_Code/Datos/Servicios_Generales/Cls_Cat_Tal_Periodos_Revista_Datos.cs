﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Catalogo_Periodos_Revista.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Cat_Tal_Periodos_Revista_Datos
/// </summary>
namespace JAPAMI.Taller_Mecanico.Catalogo_Periodos_Revista.Datos
{
    public class Cls_Cat_Tal_Periodos_Revista_Datos
    {
        #region Metodos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Periodos_Revista
            ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
            ///PARAMETROS           : 1.- Parametros. Contiene los parametros que se van a dar de
            ///                           alta en la Base de Datos.
            ///CREO                 : Salvador Vázquez Camacho
            ///FECHA_CREO           : 21/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Alta_Periodos_Revista(Cls_Cat_Tal_Periodos_Revista_Negocio Parametros)
            {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                Parametros.P_Periodo_ID = Obtener_ID_Consecutivo(Cat_Tal_Per_Rev_Mec.Tabla_Cat_Tal_Per_Rev_Mec, Cat_Tal_Per_Rev_Mec.Campo_Periodo_ID, 5);
                try
                {
                    String Mi_SQL = "INSERT INTO " + Cat_Tal_Per_Rev_Mec.Tabla_Cat_Tal_Per_Rev_Mec;
                    Mi_SQL = Mi_SQL + " (" + Cat_Tal_Per_Rev_Mec.Campo_Periodo_ID;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Per_Rev_Mec.Campo_Nombre;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Per_Rev_Mec.Campo_Periodo_Mes;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Per_Rev_Mec.Campo_Estatus;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Usuario_Creo;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Fecha_Creo;
                    Mi_SQL = Mi_SQL + ") VALUES ('" + Parametros.P_Periodo_ID;
                    Mi_SQL = Mi_SQL + "', '" + Parametros.P_Nombre;
                    Mi_SQL = Mi_SQL + "',  " + Parametros.P_Periodo_Mes;
                    Mi_SQL = Mi_SQL + " , '" + Parametros.P_Estatus;
                    Mi_SQL = Mi_SQL + "', '" + Parametros.P_Usuario_Creo;
                    Mi_SQL = Mi_SQL + "',  GETDATE())"; 
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Trans.Commit();
                }
                catch (SqlException Ex)
                {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152)
                    {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 2627)
                    {
                        if (Ex.Message.IndexOf("PRIMARY") != -1)
                        {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        }
                        else if (Ex.Message.IndexOf("UNIQUE") != -1)
                        {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        }
                        else
                        {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    }
                    else if (Ex.Number == 547)
                    {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 515)
                    {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                }
                finally
                {
                    Cn.Close();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Eliminar_Periodos_Revista
            ///DESCRIPCIÓN          : Elimina un Registro de la Base de Datos
            ///PARAMETROS           : 1.- Parametros.Contiene los parametros que se van a utilizar para
            ///                           hacer la eliminacion de la Base de Datos.
            ///CREO                 : Salavdor Vázquez Camacho
            ///FECHA_CREO           : 21/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Eliminar_Periodos_Revista(Cls_Cat_Tal_Periodos_Revista_Negocio Parametros)
            {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try
                {
                    String Mi_SQL = "DELETE FROM " + Cat_Tal_Per_Rev_Mec.Tabla_Cat_Tal_Per_Rev_Mec;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Per_Rev_Mec.Campo_Periodo_ID + " = '" + Parametros.P_Periodo_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Trans.Commit();
                }
                catch (SqlException Ex)
                {
                    if (Ex.Number == 547)
                    {
                        Mensaje = "No se puede eliminar el registro, ya que está relacionado con datos. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error al intentar eliminar. Error: [" + Ex.Message + "]";
                    }
                    throw new Exception(Mensaje);
                }
                catch (Exception Ex)
                {
                    Mensaje = "Error al intentar eliminar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                finally
                {
                    Cn.Close();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Periodos_Revista
            ///DESCRIPCIÓN          : Modifica en la Base de Datos el registro indicado
            ///PARAMETROS           : 1.- Parametros. Contiene los parametros que se van hacer la
            ///                           modificación en la Base de Datos.
            ///CREO                 : Salvador Vázquez Camacho
            ///FECHA_CREO           : 21/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Modificar_Periodos_Revista(Cls_Cat_Tal_Periodos_Revista_Negocio Parametros)
            {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try
                {
                    String Mi_SQL = "UPDATE " + Cat_Tal_Per_Rev_Mec.Tabla_Cat_Tal_Per_Rev_Mec;
                    Mi_SQL = Mi_SQL + " SET " + Cat_Tal_Per_Rev_Mec.Campo_Nombre + " = '" + Parametros.P_Nombre + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Per_Rev_Mec.Campo_Periodo_Mes + " = '" + Parametros.P_Periodo_Mes + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Per_Rev_Mec.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario_Modifico + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tarjetas_Gasolina.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Per_Rev_Mec.Campo_Periodo_ID + " = '" + Parametros.P_Periodo_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Trans.Commit();
                }
                catch (SqlException Ex)
                {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152)
                    {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                    }
                    else if (Ex.Number == 2627)
                    {
                        if (Ex.Message.IndexOf("PRIMARY") != -1)
                        {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        }
                        else if (Ex.Message.IndexOf("UNIQUE") != -1)
                        {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        }
                        else
                        {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    }
                    else if (Ex.Number == 547)
                    {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 515)
                    {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error al intentar Modificar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                }
                finally
                {
                    Cn.Close();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Tarjetas_Gasolina
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 1.- Parametros. Contiene los parametros que se van a utilizar para
            ///                           hacer la consulta de la Base de Datos.
            ///CREO                 : Salvador Vázquez Camacho
            ///FECHA_CREO           : 21/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consultar_Periodos_Revista(Cls_Cat_Tal_Periodos_Revista_Negocio Parametros)
            {
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try
                {
                    Mi_SQL = "SELECT PERIODOS_REVISTA." + Cat_Tal_Per_Rev_Mec.Campo_Periodo_ID + " AS PERIODO_ID";
                    Mi_SQL = Mi_SQL + ", PERIODOS_REVISTA." + Cat_Tal_Per_Rev_Mec.Campo_Nombre + " AS NOMBRE";
                    Mi_SQL = Mi_SQL + ", PERIODOS_REVISTA." + Cat_Tal_Per_Rev_Mec.Campo_Periodo_Mes + " AS PERIODO_MES";
                    Mi_SQL = Mi_SQL + ", PERIODOS_REVISTA." + Cat_Tal_Per_Rev_Mec.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Per_Rev_Mec.Tabla_Cat_Tal_Per_Rev_Mec + " PERIODOS_REVISTA";
                    if (!String.IsNullOrEmpty(Parametros.P_Periodo_ID))
                    {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "PERIODOS_REVISTA." + Cat_Tal_Per_Rev_Mec.Campo_Periodo_ID + " = '" + Parametros.P_Periodo_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Nombre))
                    {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "PERIODOS_REVISTA." + Cat_Tal_Per_Rev_Mec.Campo_Nombre + " LIKE '%" + Parametros.P_Nombre.Trim().ToUpper() + "%'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Periodo_Mes))
                    {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "PERIODOS_REVISTA." + Cat_Tal_Per_Rev_Mec.Campo_Periodo_Mes + " = '" + Parametros.P_Periodo_Mes + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                    {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " PERIODOS_REVISTA." + Cat_Tal_Per_Rev_Mec.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                    }
                    Mi_SQL = Mi_SQL + " ORDER BY PERIODO_ID ASC";
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                    {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Periodo_Revista
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 1.- Parametros. Contiene los parametros que se van a utilizar para
            ///                       hacer la consulta de la Base de Datos.
            ///CREO                 : Salvador Vázquez Camacho
            ///FECHA_CREO           : 21/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Cls_Cat_Tal_Periodos_Revista_Negocio Consultar_Detalles_Periodo_Revista(Cls_Cat_Tal_Periodos_Revista_Negocio Parametros)
            {
                String Mi_SQL = null;
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans = null;
                Cls_Cat_Tal_Periodos_Revista_Negocio Obj_Cargado = new Cls_Cat_Tal_Periodos_Revista_Negocio();
                try
                {
                    if (Parametros.P_Cmmd != null)
                    {
                        Cmd = Parametros.P_Cmmd;
                    }
                    else
                    {
                        Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                        Cn.Open();
                        Trans = Cn.BeginTransaction();
                        Cmd.Connection = Trans.Connection;
                        Cmd.Transaction = Trans;
                    }
                    Mi_SQL = "SELECT * FROM " + Cat_Tal_Per_Rev_Mec.Tabla_Cat_Tal_Per_Rev_Mec + " WHERE " + Cat_Tal_Per_Rev_Mec.Campo_Periodo_ID + " = '" + Parametros.P_Periodo_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    while (Reader.Read())
                    {
                        Obj_Cargado.P_Periodo_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Per_Rev_Mec.Campo_Periodo_ID].ToString())) ? Reader[Cat_Tal_Per_Rev_Mec.Campo_Periodo_ID].ToString() : "";
                        Obj_Cargado.P_Nombre = (!String.IsNullOrEmpty(Reader[Cat_Tal_Per_Rev_Mec.Campo_Nombre].ToString())) ? Reader[Cat_Tal_Per_Rev_Mec.Campo_Nombre].ToString() : "";
                        Obj_Cargado.P_Periodo_Mes = (!String.IsNullOrEmpty(Reader[Cat_Tal_Per_Rev_Mec.Campo_Periodo_Mes].ToString())) ? Reader[Cat_Tal_Per_Rev_Mec.Campo_Periodo_Mes].ToString() : "";
                        Obj_Cargado.P_Estatus = (!String.IsNullOrEmpty(Reader[Cat_Tal_Per_Rev_Mec.Campo_Estatus].ToString())) ? Reader[Cat_Tal_Per_Rev_Mec.Campo_Estatus].ToString() : "";
                    }
                    Reader.Close();
                    if (Parametros.P_Cmmd == null)
                    {
                        Trans.Commit();
                    } 
                }
                catch (Exception Ex)
                {
                    if (Parametros.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                finally
                {
                    if (Parametros.P_Cmmd == null)
                    {
                        Cn.Close();
                        Cn = null;
                        Cmd = null;
                        Trans = null;
                    }
                }
                return Obj_Cargado;
            }


            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_ID_Consecutivo
            ///DESCRIPCIÓN          : Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
            ///PARAMETROS           : 1.- Tabla. Nombre de la tabla en la Base de Datos.
            ///                       2.- Campo. Nombre del campo de la tabla donde se va a obtener el ID.
            ///                       3.- Longitu_ID. Longitud del formato en que se desea obtener el ID.
            ///CREO                 : Salavdor Vázquez Camacho
            ///FECHA_CREO           : 21/Junio/2012
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID)
            {
                String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
                try
                {
                    String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                    Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                    {
                        Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                    }
                }
                catch (SqlException Ex)
                {
                    new Exception(Ex.Message);
                }
                return Id;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Convertir_A_Formato_ID
            ///DESCRIPCIÓN          : Pasa un numero entero a Formato de ID.
            ///PARAMETROS           : 1.- Dato_ID. Dato que se desea pasar al Formato de ID.
            ///                       2.- Longitud_ID. Longitud que tendra el ID. 
            ///CREO                 : Salavdor Vázquez Camacho
            ///FECHA_CREO           : 21/Junio/2012
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
        {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
            {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }

        #endregion
    }
}
