﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Reporte_Informe_Tramite_Pagos.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Rpt_Tal_Informe_Tramite_Pagos_Datos
/// </summary>
/// 

namespace JAPAMI.Taller_Mecanico.Reporte_Informe_Tramite_Pagos.Datos{
    public class Cls_Rpt_Tal_Informe_Tramite_Pagos_Datos {

        #region Metodos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Informe_Tramite_Pagos      
            ///DESCRIPCIÓN          : Saca un Listado de Pagos
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a
            ///                         Consultar en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 29/Julio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consultar_Informe_Tramite_Pagos_Serv_Correctivos(Cls_Rpt_Tal_Informe_Tramite_Pagos_Negocio Parametros) { 
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try
                {
                    Mi_SQL = "SELECT SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " AS NO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud_Pago + " AS NO_SOLICITUD_PAGO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Contrarecibo + " AS NO_TRAMITE";
                    Mi_SQL = Mi_SQL + ", TO_NUMBER(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + ") AS ORD_TALLER";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Contrarecibo + " AS FECHA_TALLER";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", PROVEEDORES." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR";
                    Mi_SQL = Mi_SQL + ", SOL_PAGOS." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " AS FECHA_CONTABILIDAD";
                    Mi_SQL = Mi_SQL + ", SOL_PAGOS." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Ejercido + " AS FECHA_FINANZAS";
                    Mi_SQL = Mi_SQL + ", ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Diagnostico + " AS CONCEPTO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Monto_Sol_Pag + " AS IMPORTE_TOTAL";
                    Mi_SQL = Mi_SQL + ", '' AS ORDEN";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES ";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos + " SERVICIOS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SOL_PAGOS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud_Pago + " = SOL_PAGOS." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " ASIG_PROV ON SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " = ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDORES ON ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + " = PROVEEDORES." + Cat_Com_Proveedores.Campo_Proveedor_ID + "";
                    Mi_SQL = Mi_SQL + " WHERE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " = 'SERVICIO_CORRECTIVO' AND ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = 'SERVICIO_CORRECTIVO' AND SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Estatus + " = 'PAGADO' AND ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " = 'REPARADO'";
                    Entro_Where = true;
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Inicial).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Contrarecibo + " >= '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Inicial) + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Final).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Contrarecibo + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Final.AddDays(1)) + "'";
                    }
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Informe_Tramite_Pagos      
            ///DESCRIPCIÓN          : Saca un Listado de Pagos
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a
            ///                         Consultar en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 29/Julio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consultar_Informe_Tramite_Pagos_Serv_Preventivos(Cls_Rpt_Tal_Informe_Tramite_Pagos_Negocio Parametros) { 
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try
                {
                    Mi_SQL = "SELECT SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " AS NO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud_Pago + " AS NO_SOLICITUD_PAGO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Contrarecibo + " AS NO_TRAMITE";
                    Mi_SQL = Mi_SQL + ", TO_NUMBER(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + ") AS ORD_TALLER";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Contrarecibo + " AS FECHA_TALLER";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", PROVEEDORES." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR";
                    Mi_SQL = Mi_SQL + ", SOL_PAGOS." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Autorizo_Rechazo_Contabilidad + " AS FECHA_CONTABILIDAD";
                    Mi_SQL = Mi_SQL + ", SOL_PAGOS." + Ope_Con_Solicitud_Pagos.Campo_Fecha_Recibio_Ejercido + " AS FECHA_FINANZAS";
                    Mi_SQL = Mi_SQL + ", ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Diagnostico + " AS CONCEPTO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Monto_Sol_Pag + " AS IMPORTE_TOTAL";
                    Mi_SQL = Mi_SQL + ", '' AS ORDEN";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES ";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos + " SERVICIOS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Con_Solicitud_Pagos.Tabla_Ope_Con_Solicitud_Pagos + " SOL_PAGOS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud_Pago + " = SOL_PAGOS." + Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " ASIG_PROV ON SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDORES ON ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + " = PROVEEDORES." + Cat_Com_Proveedores.Campo_Proveedor_ID + "";
                    Mi_SQL = Mi_SQL + " WHERE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " = 'SERVICIO_PREVENTIVO' AND ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = 'SERVICIO_PREVENTIVO' AND SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = 'PAGADO' AND ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " = 'REPARADO'";
                    Entro_Where = true;
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Inicial).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Contrarecibo + " >= '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Inicial) + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Final).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Contrarecibo + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Final.AddDays(1)) + "'";
                    }
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

        #endregion	

	}
}