﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Consultas_Generales.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;

/// <summary>
/// Summary description for Cls_Tal_Ope_Consultas_Generales_Datos
/// </summary>

namespace JAPAMI.Taller_Mecanico.Consultas_Generales.Datos {

    public class Cls_Ope_Tal_Consultas_Generales_Datos
    {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Grupos_Unidades_Responsables
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.  Destino_Negocio. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 30/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Grupos_Unidades_Responsables(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT " + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + " AS GRUPO_DEPENDENCIA_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Grupos_Dependencias.Campo_Clave + " AS CLAVE";
                Mi_SQL = Mi_SQL + ", " + Cat_Grupos_Dependencias.Campo_Nombre + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Grupos_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+  " + Cat_Grupos_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Grupos_Dependencias.Campo_Comentarios + " AS COMENTARIOS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias;
                if (Parametros.P_Clave_UR != null && Parametros.P_Clave_UR.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Grupos_Dependencias.Campo_Clave + " LIKE '%" + Parametros.P_Clave_UR + "%'";
                }
                if (Parametros.P_Nombre_UR != null && Parametros.P_Nombre_UR.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Grupos_Dependencias.Campo_Nombre + " LIKE '%" + Parametros.P_Nombre_UR + "%'";
                }
                if (Parametros.P_Dependencia_ID != null && Parametros.P_Dependencia_ID.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID + " IN (";
                    Mi_SQL = Mi_SQL + " SELECT " + Cat_Dependencias.Campo_Grupo_Dependencia_ID + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " IN ('" + Parametros.P_Dependencia_ID + "')";
                    Mi_SQL = Mi_SQL + ")";
                }
                Mi_SQL = Mi_SQL + " ORDER BY CLAVE_NOMBRE";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Unidades_Responsables
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.  Destino_Negocio. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 30/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Unidades_Responsables(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + " AS DEPENDENCIA_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Dependencias.Campo_Clave + " AS CLAVE";
                Mi_SQL = Mi_SQL + ", " + Cat_Dependencias.Campo_Nombre + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+  " + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Dependencias.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + ", " + Cat_Dependencias.Campo_Comentarios + " AS COMENTARIOS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
                if (Parametros.P_Estatus != null && Parametros.P_Estatus.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Dependencias.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                if (Parametros.P_Clave_UR != null && Parametros.P_Clave_UR.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Dependencias.Campo_Clave + " LIKE '%" + Parametros.P_Clave_UR + "%'";
                }
                if (Parametros.P_Nombre_UR != null && Parametros.P_Nombre_UR.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Dependencias.Campo_Nombre + " LIKE '%" + Parametros.P_Nombre_UR + "%'";
                }
                if (Parametros.P_Grupo_Dependencia_ID != null && Parametros.P_Grupo_Dependencia_ID.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Dependencias.Campo_Grupo_Dependencia_ID + " LIKE '%" + Parametros.P_Grupo_Dependencia_ID + "%'";
                }
                Mi_SQL = Mi_SQL + " ORDER BY CLAVE_NOMBRE";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Vehiculos
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.  Destino_Negocio. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 04/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :Consultar_Bien_Mueble
        ///*******************************************************************************
        public static DataTable Consultar_Vehiculos(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + " AS VEHICULO_ID";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " AS NO_ECONOMICO";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Placas + " AS PLACAS";
                Mi_SQL = Mi_SQL + ", cast(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Anio_Fabricacion + " as int) AS ANIO";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Dependencia_ID + " AS DEPENDENCIA_ID";
                Mi_SQL = Mi_SQL + ", TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Descripcion;
                Mi_SQL = Mi_SQL + " +' Marca:  '+ MARCAS." + Cat_Com_Marcas.Campo_Nombre;
                Mi_SQL = Mi_SQL + " +' Modelo: '+ VEHICULOS." + Ope_Pat_Vehiculos.Campo_Modelo + " AS VEHICULO_DESCRIPCION";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " MARCAS";
                Mi_SQL = Mi_SQL + " ON MARCAS." + Cat_Com_Marcas.Campo_Marca_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Marca_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Tipos_Vehiculo.Tabla_Cat_Pat_Tipos_Vehiculo + " TIPOS_VEHICULO";
                Mi_SQL = Mi_SQL + " ON TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Tipo_Vehiculo_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Tipo_Vehiculo_ID + "";
                if (!String.IsNullOrEmpty(Parametros.P_Vehiculo_ID))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + " = '" + Parametros.P_Vehiculo_ID + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_No_Inventario))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " = '" + Parametros.P_No_Inventario + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_No_Economico))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " = '" + Parametros.P_No_Economico + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VEHICULOS." + Ope_Pat_Vehiculos.Campo_Dependencia_ID + " in ('" + Parametros.P_Dependencia_ID + "')";
                }
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Vehiculos
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.  Destino_Negocio. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 04/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :Consultar_Bien_Mueble
        ///*******************************************************************************
        public static DataTable Consultar_Bienes(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + " AS VEHICULO_ID";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " AS NO_ECONOMICO";
                Mi_SQL = Mi_SQL + ", '-' " + " AS PLACAS";
                Mi_SQL = Mi_SQL + ", cast(BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Fecha_Adquisicion + " as int) AS ANIO";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Dependencia_ID + " AS DEPENDENCIA_ID";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Nombre;
                Mi_SQL = Mi_SQL + " +' Marca:  '+ MARCAS." + Cat_Com_Marcas.Campo_Nombre + " AS VEHICULO_DESCRIPCION";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " BIENES_MUEBLES";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " MARCAS";
                Mi_SQL = Mi_SQL + " ON MARCAS." + Cat_Com_Marcas.Campo_Marca_ID + " = BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Marca_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Tipos_Vehiculo.Tabla_Cat_Pat_Tipos_Vehiculo + " TIPOS_VEHICULO";
                Mi_SQL = Mi_SQL + " ON TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Tipo_Vehiculo_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Tipo_Vehiculo_ID + "";
                if (!String.IsNullOrEmpty(Parametros.P_Vehiculo_ID))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + " = '" + Parametros.P_Vehiculo_ID + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_No_Inventario))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " = '" + Parametros.P_No_Inventario + "'";
                }                
                if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                }
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Bien_Mueble
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.  Destino_Negocio. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 04/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Bien_Mueble(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + " AS BIEN_MUEBLE_ID";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie + " AS NUMERO_SERIE";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Dependencia_ID + " AS DEPENDENCIA_ID";
                Mi_SQL = Mi_SQL + ", BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Nombre;
                Mi_SQL = Mi_SQL + " + ' Color:  ' + COLORES." + Cat_Pat_Colores.Campo_Descripcion;
                Mi_SQL = Mi_SQL + " + ' Marca:  ' + MARCAS." + Cat_Com_Marcas.Campo_Nombre;
                Mi_SQL = Mi_SQL + " + ' Modelo:  ' + BIENES_MUEBLES." + Cat_Com_Marcas.Campo_Nombre;
                Mi_SQL = Mi_SQL + " + ' Modelo: ' + MATERIALES." + Cat_Pat_Materiales.Campo_Descripcion + " AS DESCRIPCION_BIEN_MUEBLE";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " BIENES_MUEBLES";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " MARCAS";
                Mi_SQL = Mi_SQL + " ON BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Marca_ID + " = MARCAS." + Cat_Com_Marcas.Campo_Marca_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Colores.Tabla_Cat_Pat_Colores + " COLORES";
                Mi_SQL = Mi_SQL + " ON BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Color_ID + " = COLORES." + Cat_Pat_Colores.Campo_Color_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Materiales.Tabla_Cat_Pat_Materiales + " MATERIALES";
                Mi_SQL = Mi_SQL + " ON BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Material_ID + " = MATERIALES." + Cat_Pat_Materiales.Campo_Material_ID + "";
                if (!String.IsNullOrEmpty(Parametros.P_Bien_Mueble_ID))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + " = '" + Parametros.P_Bien_Mueble_ID + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_No_Inventario))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " = '" + Parametros.P_No_Inventario + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_No_Serie))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie + " = '" + Parametros.P_No_Serie + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Dependencia_ID + " in( '" + Parametros.P_Dependencia_ID + "')";
                }
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Empleados
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un 
        ///                       DataTable sobre la consulta de Empleados.
        ///PARAMETROS           : 1. Paramentros_Negocio.    Contiene los parametros que se 
        ///                         van a utilizar para hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           :09/Mayo/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Empleados(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Consulta_Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " AS EMPLEADO_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_No_Empleado + " AS NO_EMPLEADO";
                Mi_SQL = Mi_SQL + ", LTRIM(RTRIM(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + "";
                Mi_SQL = Mi_SQL + " +' '+ " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + "";
                Mi_SQL = Mi_SQL + " +' '+ " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ")) AS NOMBRE";
                Mi_SQL = Mi_SQL + ", LTRIM(RTRIM(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ")) AS DEPENDENCIA";
                Mi_SQL = Mi_SQL + ", " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias;
                Mi_SQL = Mi_SQL + " ON " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Estatus + " = 'ACTIVO'";
                if (!String.IsNullOrEmpty(Parametros.P_Empleado_ID))
                {
                    Mi_SQL = Mi_SQL + " AND " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " = '" + Parametros.P_Empleado_ID + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_No_Empleado))
                {
                    Mi_SQL = Mi_SQL + " AND " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_No_Empleado + " = '" + String.Format("{0:000000}", Convert.ToInt32(Parametros.P_No_Empleado)) + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Nombre_Empleado))
                {
                    Mi_SQL = Mi_SQL + " AND (LTRIM(RTRIM(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + "";
                    Mi_SQL = Mi_SQL + " +' '+ " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + "";
                    Mi_SQL = Mi_SQL + " +' '+ " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ")) LIKE '%" + Parametros.P_Nombre_Empleado.Trim().ToUpper() + "%'";
                    Mi_SQL = Mi_SQL + " OR LTRIM(RTRIM(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + "";
                    Mi_SQL = Mi_SQL + " +' '+ " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + "";
                    Mi_SQL = Mi_SQL + " +' '+ " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + ")) LIKE '%" + Parametros.P_Nombre_Empleado.Trim().ToUpper() + "%')";
                }
                if (!String.IsNullOrEmpty(Parametros.P_RFC_Empleado))
                {
                    Mi_SQL = Mi_SQL + " AND " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " LIKE '%" + Parametros.P_RFC_Empleado + "%'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                {
                    Mi_SQL = Mi_SQL + " AND " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                }
                Mi_SQL = Mi_SQL + " ORDER BY NOMBRE";
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partes
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 24/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Partes(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT " + Cat_Tal_Partes_Veh.Campo_Parte_ID + " AS PARTE_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Partes_Veh.Campo_Nombre + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Partes_Veh.Campo_Tipo + " AS TIPO";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Partes_Veh.Campo_Parent + " AS PARENT_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Partes_Veh.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Partes_Veh.Tabla_Cat_Tal_Partes_Veh;
                if (!String.IsNullOrEmpty(Parametros.P_Nombre))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Tal_Partes_Veh.Campo_Nombre + " LIKE '%" + Parametros.P_Nombre + "%'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Tal_Partes_Veh.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Tipo))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Tal_Partes_Veh.Campo_Tipo + " = '" + Parametros.P_Tipo + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Parent))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Tal_Partes_Veh.Campo_Parent + " IN (" + Parametros.P_Parent + ")";
                }
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Tal_Partes_Veh.Campo_Parte_ID;
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Proveedor_Servicio
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 28/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Proveedor_Servicio(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT PROV_SERV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " AS NO_ASIGNACION_PROV_SERV";
                Mi_SQL = Mi_SQL + ", PROV_SERV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " AS NO_SERVICIO";
                Mi_SQL = Mi_SQL + ", PROV_SERV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + " AS PROVEEDOR_ID";
                Mi_SQL = Mi_SQL + ", PROV_SERV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Observaciones + " AS OBSERVACIONES";
                Mi_SQL = Mi_SQL + ", PROV_SERV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + ", PROV_SERV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " AS TIPO";
                Mi_SQL = Mi_SQL + ", PROVEEDORES." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " PROV_SERV";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDORES ON PROV_SERV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + " = PROVEEDORES." + Cat_Com_Proveedores.Campo_Proveedor_ID + "";
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " PROV_SERV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Tipo))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " PROV_SERV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " IN ('" + Parametros.P_Tipo + "')";
                }
                if (Parametros.P_No_Servicio > (-1))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " PROV_SERV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " IN ('" + Parametros.P_No_Servicio + "')";
                }
                Mi_SQL = Mi_SQL + " ORDER BY NO_ASIGNACION_PROV_SERV ASC";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Capitulos
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 09/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Capitulos(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " AS CAPITULO_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_SAP_Capitulos.Campo_Clave + " AS CLAVE";
                Mi_SQL = Mi_SQL + ", " + Cat_SAP_Capitulos.Campo_Descripcion + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_SAP_Capitulos.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ " + Cat_SAP_Capitulos.Campo_Descripcion + " AS CLAVE_NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_SAP_Capitulos.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos;
                if (Parametros.P_Estatus != null && Parametros.P_Estatus.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_SAP_Capitulos.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                if (Parametros.P_Clave != null && Parametros.P_Clave.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_SAP_Capitulos.Campo_Clave + " LIKE '%" + Parametros.P_Clave + "%'";
                }
                if (Parametros.P_Descripcion != null && Parametros.P_Descripcion.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_SAP_Capitulos.Campo_Descripcion + " LIKE '%" + Parametros.P_Descripcion + "%'";
                }
                Mi_SQL = Mi_SQL + " ORDER BY CLAVE_NOMBRE";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 09/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Conceptos(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT " + Cat_Sap_Concepto.Campo_Concepto_ID + " AS CONCEPTO_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Concepto.Campo_Clave + " AS CLAVE";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Concepto.Campo_Descripcion + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Concepto.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ " + Cat_Sap_Concepto.Campo_Descripcion + " AS CLAVE_NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Concepto.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto;
                if (Parametros.P_Estatus != null && Parametros.P_Estatus.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                if (Parametros.P_Clave != null && Parametros.P_Clave.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Campo_Clave + " LIKE '%" + Parametros.P_Clave + "%'";
                }
                if (Parametros.P_Descripcion != null && Parametros.P_Descripcion.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Campo_Descripcion + " LIKE '%" + Parametros.P_Descripcion + "%'";
                }
                if (Parametros.P_Capitulo_ID != null && Parametros.P_Capitulo_ID.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Campo_Capitulo_ID + " = '" + Parametros.P_Capitulo_ID + "'";
                }
                Mi_SQL = Mi_SQL + " ORDER BY CLAVE_NOMBRE";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas_Genericas
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 09/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Partidas_Genericas(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT " + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID + " AS PARTIDA_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_SAP_Partida_Generica.Campo_Clave + " AS CLAVE";
                Mi_SQL = Mi_SQL + ", " + Cat_SAP_Partida_Generica.Campo_Descripcion + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_SAP_Partida_Generica.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ " + Cat_SAP_Partida_Generica.Campo_Descripcion + " AS CLAVE_NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_SAP_Partida_Generica.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica;
                if (Parametros.P_Estatus != null && Parametros.P_Estatus.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                if (Parametros.P_Clave != null && Parametros.P_Clave.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Campo_Clave + " LIKE '%" + Parametros.P_Clave + "%'";
                }
                if (Parametros.P_Descripcion != null && Parametros.P_Descripcion.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Campo_Descripcion + " LIKE '%" + Parametros.P_Descripcion + "%'";
                }
                if (Parametros.P_Concepto_ID != null && Parametros.P_Concepto_ID.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Campo_Concepto_ID + " = '" + Parametros.P_Concepto_ID + "'";
                }
                Mi_SQL = Mi_SQL + " ORDER BY CLAVE_NOMBRE";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas_Especificas
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 09/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Partidas_Especificas(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " AS PARTIDA_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS CLAVE";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Partidas_Especificas.Campo_Descripcion + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Partidas_Especificas.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ " + Cat_Sap_Partidas_Especificas.Campo_Descripcion + " AS CLAVE_NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Partidas_Especificas.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
                if (Parametros.P_Estatus != null && Parametros.P_Estatus.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Especificas.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                if (Parametros.P_Clave != null && Parametros.P_Clave.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Especificas.Campo_Clave + " LIKE '%" + Parametros.P_Clave + "%'";
                }
                if (Parametros.P_Partida_Especifica_ID != null && Parametros.P_Partida_Especifica_ID.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " LIKE '%" + Parametros.P_Partida_Especifica_ID + "%'";
                }
                if (Parametros.P_Descripcion != null && Parametros.P_Descripcion.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Especificas.Campo_Descripcion + " LIKE '%" + Parametros.P_Descripcion + "%'";
                }
                if (Parametros.P_Partida_Generica_ID != null && Parametros.P_Partida_Generica_ID.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = '" + Parametros.P_Partida_Generica_ID + "'";
                }
                Mi_SQL = Mi_SQL + " ORDER BY CLAVE_NOMBRE";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Tabla_Relacionada_SGC
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 09/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Tabla_Relacionada_SGC(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT P_CAPITULO." + Cat_SAP_Capitulos.Campo_Capitulo_ID + " AS CAPITULO_ID";
                Mi_SQL = Mi_SQL + ", P_CAPITULO." + Cat_SAP_Capitulos.Campo_Clave + " AS CLAVE_CAPITULO";
                Mi_SQL = Mi_SQL + ", P_CAPITULO." + Cat_SAP_Capitulos.Campo_Descripcion + " AS NOMBRE_CAPITULO";
                Mi_SQL = Mi_SQL + ", P_CONCEPTO." + Cat_Sap_Concepto.Campo_Concepto_ID + " AS CONCEPTO_ID";
                Mi_SQL = Mi_SQL + ", P_CONCEPTO." + Cat_Sap_Concepto.Campo_Clave + " AS CLAVE_CONCEPTO";
                Mi_SQL = Mi_SQL + ", P_CONCEPTO." + Cat_Sap_Concepto.Campo_Descripcion + " AS NOMBRE_CONCEPTO";
                Mi_SQL = Mi_SQL + ", P_GENERICAS." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID + " AS PARTIDA_GENERICA_ID";
                Mi_SQL = Mi_SQL + ", P_GENERICAS." + Cat_SAP_Partida_Generica.Campo_Clave + " AS CLAVE_PARTIDA_GENERICA";
                Mi_SQL = Mi_SQL + ", P_GENERICAS." + Cat_SAP_Partida_Generica.Campo_Descripcion + " AS NOMBRE_PARTIDA_GENERICA";
                Mi_SQL = Mi_SQL + ", P_ESPECIFICAS." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " AS PARTIDA_ESPECIFICA_ID";
                Mi_SQL = Mi_SQL + ", P_ESPECIFICAS." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS CLAVE_PARTIDA_ESPECIFICA";
                Mi_SQL = Mi_SQL + ", P_ESPECIFICAS." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS NOMBRE_PARTIDA_ESPECIFICA";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " P_ESPECIFICAS";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " P_GENERICAS ON P_ESPECIFICAS." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = P_GENERICAS." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " P_CONCEPTO ON P_GENERICAS." + Cat_SAP_Partida_Generica.Campo_Concepto_ID + " = P_CONCEPTO." + Cat_Sap_Concepto.Campo_Concepto_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " P_CAPITULO ON P_CONCEPTO." + Cat_Sap_Concepto.Campo_Capitulo_ID + " = P_CAPITULO." + Cat_SAP_Capitulos.Campo_Capitulo_ID + "";
                if (Parametros.P_Estatus != null && Parametros.P_Estatus.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "P_ESPECIFICAS." + Cat_Sap_Partidas_Especificas.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                if (Parametros.P_Clave != null && Parametros.P_Clave.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "P_ESPECIFICAS." + Cat_Sap_Partidas_Especificas.Campo_Clave + " LIKE '%" + Parametros.P_Clave + "%'";
                }
                if (Parametros.P_Descripcion != null && Parametros.P_Descripcion.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "P_ESPECIFICAS." + Cat_Sap_Partidas_Especificas.Campo_Descripcion + " LIKE '%" + Parametros.P_Descripcion + "%'";
                }
                if (Parametros.P_Partida_Especifica_ID != null && Parametros.P_Partida_Especifica_ID.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "P_ESPECIFICAS." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = '" + Parametros.P_Partida_Especifica_ID + "'";
                }
                Mi_SQL = Mi_SQL + " ORDER BY CAPITULO_ID";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas_Especificas
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 09/Junio/2012
        ///MODIFICO             : Miguel Angel Alvarado Enriquez
        ///FECHA_MODIFICO       : 09/Noviembre/2012
        ///CAUSA_MODIFICACIÓN   : Se cambio la cunsulta de Oralce a SQL
        ///*******************************************************************************
        public static DataTable Consultar_Fuentes_Financiamiento(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " AS FUENTE_FINANCIAMIENTO_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS CLAVE";
                Mi_SQL = Mi_SQL + ", " + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ " + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_SAP_Fuente_Financiamiento.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento;
                if (Parametros.P_Estatus != null && Parametros.P_Estatus.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_SAP_Fuente_Financiamiento.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                if (Parametros.P_Clave != null && Parametros.P_Clave.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " LIKE '%" + Parametros.P_Clave + "%'";
                }
                if (Parametros.P_Descripcion != null && Parametros.P_Descripcion.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " LIKE '%" + Parametros.P_Descripcion + "%'";
                }
                if (Parametros.P_Partida_Especifica_ID != null && Parametros.P_Partida_Especifica_ID.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "" + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " IN ( SELECT " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " "
                    + " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'"
                    + " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Parametros.P_Partida_Especifica_ID + "' AND ANIO = convert(INT,year(GETDATE()),1111))";
                }
                Mi_SQL = Mi_SQL + " ORDER BY CLAVE_NOMBRE";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Proyectos_Programas
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Jesus Toledo
        ///FECHA_CREO           : 28/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Proyectos_Programas(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " AS PROGRAMA_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS CLAVE";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Proyectos_Programas.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ " + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS CLAVE_NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Proyectos_Programas.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas;
                if (Parametros.P_Estatus != null && Parametros.P_Estatus.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Sap_Proyectos_Programas.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                if (Parametros.P_Clave != null && Parametros.P_Clave.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Sap_Proyectos_Programas.Campo_Clave + " LIKE '%" + Parametros.P_Clave + "%'";
                }
                if (Parametros.P_Descripcion != null && Parametros.P_Descripcion.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " LIKE '%" + Parametros.P_Descripcion + "%'";
                }
                if (Parametros.P_Dependencia_ID != null && Parametros.P_Dependencia_ID.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " in( SELECT "
                        + Cat_SAP_Det_Prog_Dependencia.Campo_Proyecto_Programa_ID + " FROM " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia
                        + " WHERE " + Cat_SAP_Det_Prog_Dependencia.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "')";
                }
                Mi_SQL = Mi_SQL + " ORDER BY CLAVE_NOMBRE";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Resguardantes_Vehiculos
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : Objeto de Negocio con los datos a consultar
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 07/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Resguardantes_Vehiculos(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT convert(int,em." + Cat_Empleados.Campo_No_Empleado + ") AS NO_EMPLEADO";
                Mi_SQL = Mi_SQL + ", em." + Cat_Empleados.Campo_Nombre + " +' '+ ";
                Mi_SQL = Mi_SQL + " em." + Cat_Empleados.Campo_Apellido_Paterno + " +' '+ ";
                Mi_SQL = Mi_SQL + " em." + Cat_Empleados.Campo_Apellido_Materno + " AS RESGUARDANTE";
                Mi_SQL = Mi_SQL + ", ve." + Ope_Pat_Vehiculos.Campo_Nombre + " AS TIPO_UNIDAD";
                Mi_SQL = Mi_SQL + ", ve." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + "";
                Mi_SQL = Mi_SQL + ", ve." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " AS NUMERO_ECONOMICO";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " em ";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + " res ";
                Mi_SQL = Mi_SQL + " ON res." + Ope_Pat_Bienes_Resguardos.Campo_Empleado_Resguardo_ID + " = em." + Cat_Empleados.Campo_Empleado_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " ve ";
                Mi_SQL = Mi_SQL + " ON ve." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + " = res." + Ope_Pat_Bienes_Resguardos.Campo_Bien_ID + "";
                if (!String.IsNullOrEmpty(Parametros.P_Tipo))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " res." + Ope_Pat_Bienes_Resguardos.Campo_Tipo + " = '" + Parametros.P_Tipo + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " res." + Ope_Pat_Bienes_Resguardos.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Vehiculo_ID))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " res." + Ope_Pat_Bienes_Resguardos.Campo_Bien_ID + " = '" + Parametros.P_Vehiculo_ID + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_No_Inventario))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " ve." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " = '" + Parametros.P_No_Inventario + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VEHICULOS." + Ope_Pat_Vehiculos.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                }
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Resguardantes_Vehiculos
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : Objeto de Negocio con los datos a consultar
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 07/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Resguardantes_Bienes(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT convert(int,em." + Cat_Empleados.Campo_No_Empleado + ") AS NO_EMPLEADO";
                Mi_SQL = Mi_SQL + ", em." + Cat_Empleados.Campo_Nombre + " +' '+ ";
                Mi_SQL = Mi_SQL + " em." + Cat_Empleados.Campo_Apellido_Paterno + " +' '+ ";
                Mi_SQL = Mi_SQL + " em." + Cat_Empleados.Campo_Apellido_Materno + " AS RESGUARDANTE";
                Mi_SQL = Mi_SQL + ", ve." + Ope_Pat_Bienes_Muebles.Campo_Nombre + " AS TIPO_UNIDAD";
                Mi_SQL = Mi_SQL + ", ve." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + "";
                Mi_SQL = Mi_SQL + ", ve." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie + " AS NUMERO_ECONOMICO";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " em ";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Bienes_Resguardos.Tabla_Ope_Pat_Bienes_Resguardos + " res ";
                Mi_SQL = Mi_SQL + " ON res." + Ope_Pat_Bienes_Resguardos.Campo_Empleado_Resguardo_ID + " = em." + Cat_Empleados.Campo_Empleado_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " ve ";
                Mi_SQL = Mi_SQL + " ON ve." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + " = res." + Ope_Pat_Bienes_Resguardos.Campo_Bien_ID + "";
                if (!String.IsNullOrEmpty(Parametros.P_Tipo))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " res." + Ope_Pat_Bienes_Resguardos.Campo_Tipo + " = '" + Parametros.P_Tipo + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " res." + Ope_Pat_Bienes_Resguardos.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Vehiculo_ID))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " res." + Ope_Pat_Bienes_Resguardos.Campo_Bien_ID + " = '" + Parametros.P_Vehiculo_ID + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_No_Inventario))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " ve." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " = '" + Parametros.P_No_Inventario + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " VEHICULOS." + Ope_Pat_Vehiculos.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                }
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos_Solicitud_Pago
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.  Destino_Negocio. Contiene los parametros que se van a utilizar para
        ///                                     hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 30/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Tipos_Solicitud_Pago(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID + " AS TIPO_SOLICITUD_PAGO_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + " AS DESCRIPCION";
                //Mi_SQL = Mi_SQL + ", " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion_Finanzas + " AS DESCRIPCION_FINANZAS";
                Mi_SQL = Mi_SQL + ", " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + ", " + Cat_Con_Tipo_Solicitud_Pagos.Campo_Comentarios + " AS COMENTARIOS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Con_Tipo_Solicitud_Pagos.Tabla_Cat_Con_Tipo_Solicitud_Pago;
                if (Parametros.P_Estatus != null && Parametros.P_Estatus.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Con_Tipo_Solicitud_Pagos.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                if (Parametros.P_Descripcion != null && Parametros.P_Descripcion.Trim().Length > 0)
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion + " LIKE '%" + Parametros.P_Descripcion + "%'";
                }
                Mi_SQL = Mi_SQL + " ORDER BY DESCRIPCION";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partes
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 24/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Partes_Revista_Mecanica(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT " + Cat_Tal_Detalles_Rev_Mec.Campo_Detalle_ID + " AS PARTE_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Detalles_Rev_Mec.Campo_Nombre + " AS NOMBRE";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Detalles_Rev_Mec.Campo_Tipo + " AS TIPO";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Detalles_Rev_Mec.Campo_Parent + " AS PARENT_ID";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Detalles_Rev_Mec.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Detalles_Rev_Mec.Tabla_Cat_Tal_Detalles_Rev_Mec;
                if (!String.IsNullOrEmpty(Parametros.P_Nombre))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Tal_Detalles_Rev_Mec.Campo_Nombre + " LIKE '%" + Parametros.P_Nombre + "%'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Tal_Detalles_Rev_Mec.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Tipo))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Tal_Detalles_Rev_Mec.Campo_Tipo + " = '" + Parametros.P_Tipo + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Parent))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + Cat_Tal_Detalles_Rev_Mec.Campo_Parent + " IN (" + Parametros.P_Parent + ")";
                }
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Tal_Detalles_Rev_Mec.Campo_Detalle_ID;
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Proveedores
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 24/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Proveedores(Cls_Ope_Tal_Consultas_Generales_Negocio Clase_Negocio)
        {
            String Mi_SQL = "SELECT " + Cat_Com_Proveedores.Campo_Proveedor_ID + " AS PROVEEDOR_ID";
            Mi_SQL = Mi_SQL + ", " + Cat_Com_Proveedores.Campo_Nombre + " AS NOMBRE_PROVEEDOR";
            Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Proveedores.Campo_Estatus + " = 'ACTIVO'";
            Mi_SQL = Mi_SQL + " AND " + Cat_Com_Proveedores.Campo_Tipo;
            Mi_SQL = Mi_SQL + " = 'TALLER'";
            Mi_SQL = Mi_SQL + " ORDER BY NOMBRE_PROVEEDOR ASC";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Proyecto_Programa_Solicitud
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un String.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Jesus Toledo Rdz
        ///FECHA_CREO           : 23/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static string Consultar_Proyecto_Programa_Solicitud(Cls_Ope_Tal_Consultas_Generales_Negocio Parametros)
        {
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            Object Aux; //Variable auxiliar para las consultas
            String Mi_SQL = "";
            String Programa_ID = "0";
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            try
            {
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                //Limpiamos la Variable Mi_SQL
                Mi_SQL = "";
                //Consultamos la tabla de parametros para obtener la partida especifica
                Mi_SQL = "SELECT ISNULL(" + Ope_Tal_Solicitudes_Serv.Campo_Programa_ID + ",0)"; ;
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = " + Parametros.P_No_Solicitud;

                //Ejecutar consulta del consecutivo
                Cmd.CommandText = Mi_SQL;
                Aux = Cmd.ExecuteScalar();
                //Aux = OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0][0];

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                {
                    Programa_ID = string.Format("{0:0000000000}", Aux);                    
                }                
            }
            catch (SqlException Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
            return Programa_ID;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Codigo_Programatico_Servicios
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un String.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Jesus Toledo Rdz
        ///FECHA_CREO           : 22/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static string Consultar_Codigo_Programatico_Servicios(int No_Solicitud,ref SqlCommand P_Cmmd)
        {
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            Object Aux; //Variable auxiliar para las consultas
            String Mi_SQL = "";
            String Programa_ID = "0";
            String Programa = "";
            String Mensaje = "";
            String Dependencia_ID = "";
            String Dependencia = "";
            String Area_Funcional = "";
            String Fuente_Financiamiento = "";
            String Codigo = "";
            String Partida_Especifica = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            Cls_Tal_Parametros_Negocio Parametros_Negocio = new Cls_Tal_Parametros_Negocio();
            try
            {
                if (P_Cmmd != null)
                {
                    Cmd = P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                //Primero se consultan los valores de Fuente de Financiamiento y Partida de la tabla de Parametros para Taller
                Parametros_Negocio.P_Cmmd = Cmd;
                Parametros_Negocio = Parametros_Negocio.Consulta_Parametros();
                //Se consulta Fuente de Financiamiento
                Mi_SQL = "SELECT isnull(" + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ",'-') FUENTE_FINANCIAMIENTO FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = ";
                Mi_SQL += "'" + Parametros_Negocio.P_Fuente_Financiamiento_ID + "'";//Fuente de Financiamiento -- Txt_Fte_Financiamiento.Text.Split(ch)[0] + "-1.1.1-";//area SE SUSTITUYE EN LA CAPA DE DATOS
                Cmd.CommandText = Mi_SQL;
                Fuente_Financiamiento = Cmd.ExecuteScalar().ToString();//Proyecto Programa -- Txt_Programa.Text.Split(ch)[0] + "-";
                //Se consulta Partida Especifica
                Mi_SQL = "SELECT isnull(" + Cat_Sap_Partidas_Especificas.Campo_Clave + ",'-') PARTIDA_ESPECIFICA FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = ";
                Mi_SQL += "'" + Parametros_Negocio.P_Partida_ID + "'";//Fuente de Financiamiento -- Txt_Fte_Financiamiento.Text.Split(ch)[0] + "-1.1.1-";//area SE SUSTITUYE EN LA CAPA DE DATOS
                Cmd.CommandText = Mi_SQL;
                Partida_Especifica = Cmd.ExecuteScalar().ToString();
                    //Dependencia -- Cmb_Dependencia.SelectedItem.Text.Split(ch)[0] + "-";
                    //Partida -- Txt_Partida.Text.Split(ch)[0];
                //Consultamos Dependencia y Proyecto de la solicitud
                //Limpiamos la Variable Mi_SQL
                Mi_SQL = "";
                Mi_SQL = "SELECT ISNULL(" + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + ",0)" + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + ", ISNULL(" + Ope_Tal_Solicitudes_Serv.Campo_Programa_ID + ",0)" + Ope_Tal_Solicitudes_Serv.Campo_Programa_ID; 
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = " + No_Solicitud;

                Cmd.CommandText = Mi_SQL;
                SqlDataReader Reader = Cmd.ExecuteReader();
                //OracleDataReader Reader = OracleHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                while (Reader.Read())
                {
                    Programa_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Solicitudes_Serv.Campo_Programa_ID].ToString())) ? Reader[Ope_Tal_Solicitudes_Serv.Campo_Programa_ID].ToString() : String.Empty;
                    Dependencia_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID].ToString())) ? Reader[Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID].ToString() : String.Empty;
                }
                Reader.Close();
                //Se consulta Clave de Dependencia
                Mi_SQL = "SELECT isnull(" + Cat_Dependencias.Campo_Clave + ",'-') DEPENDENCIA FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = ";
                Mi_SQL += "'" + Dependencia_ID + "'";//Fuente de Financiamiento -- Txt_Fte_Financiamiento.Text.Split(ch)[0] + "-1.1.1-";//area SE SUSTITUYE EN LA CAPA DE DATOS
                Cmd.CommandText = Mi_SQL;
                Dependencia = Cmd.ExecuteScalar().ToString();
                //Se consulta Clave de Programa
                Mi_SQL = "SELECT isnull(" + Cat_Sap_Proyectos_Programas.Campo_Clave + ",'-') PARTIDA_ESPECIFICA FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = ";
                Mi_SQL += "'" + Programa_ID + "'";//Fuente de Financiamiento -- Txt_Fte_Financiamiento.Text.Split(ch)[0] + "-1.1.1-";//area SE SUSTITUYE EN LA CAPA DE DATOS
                Cmd.CommandText = Mi_SQL;
                Programa = Cmd.ExecuteScalar().ToString();
                //Se consulta Area funcional
                Mi_SQL = "SELECT isnull(" + Cat_SAP_Area_Funcional.Campo_Clave + ",'-') FROM " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " WHERE " + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + " = " +
                    "(SELECT " + Cat_Dependencias.Campo_Area_Funcional_ID + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Dependencia_ID + "')";
                Cmd.CommandText = Mi_SQL;
                Area_Funcional = Cmd.ExecuteScalar().ToString();

                Codigo = Fuente_Financiamiento.Trim() + "-" + Area_Funcional + "-" + Programa.Trim() + "-" + Dependencia.Trim() + "-" + Partida_Especifica.Trim();

                //Requisicion_Negocio.P_Codigo_Programatico = "";
                
                Codigo = Codigo.Replace("area", Area_Funcional);
                //Ejecutar consulta del consecutivo
                Cmd.CommandText = Mi_SQL;
                Aux = Cmd.ExecuteScalar();
                //Aux = OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0][0];

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                {
                    Programa_ID = string.Format("{0:0000000000}", Aux);
                }
            }
            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
            return Codigo;
        }
    }
}