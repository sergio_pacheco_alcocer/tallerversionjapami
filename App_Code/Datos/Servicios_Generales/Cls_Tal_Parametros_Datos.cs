﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;

namespace JAPAMI.Taller_Mecanico.Parametros.Datos
{
    public class Cls_Tal_Parametros_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Parametros
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Parametros(Cls_Tal_Parametros_Negocio Parametros)
        {
            String Mensaje = "";
            String Mi_SQL = "";
            int Actualizar;
            Object Aux; //Variable auxiliar para las consultas
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Formar Sentencia para obtener el consecutivo
                Mi_SQL = "";
                Mi_SQL = "SELECT COUNT(*) ";
                Mi_SQL = Mi_SQL + " FROM ";
                Mi_SQL = Mi_SQL + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros;

                //Ejecutar consulta del consecutivo
                Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0][0];

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                {
                    Actualizar = Convert.ToInt32(Aux);
                }
                else
                    Actualizar = 0;

                if (Actualizar > 0)
                {
                    Mi_SQL = "UPDATE " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros;
                    Mi_SQL = Mi_SQL + " SET " + Cat_Tal_Parametros.Campo_Fuente_Financiamiento + " = '" + Parametros.P_Fuente_Financiamiento_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Programa_ID + " = '" + Parametros.P_Programa_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Partida_ID + " = '" + Parametros.P_Partida_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Oficial_Mayor + " = '" + Parametros.P_Oficial_Mayor + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Director + " = '" + Parametros.P_Director + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Coordinador + " = '" + Parametros.P_Coordinador + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Almacen + " = '" + Parametros.P_Almacen + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Proveedor_Gasolina_Id + " = '" + Parametros.P_Proveedor_Gasolina_Id + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID + " = '" + Parametros.P_Tipo_Solicitud_Pago_ID + "'";
                    if (!String.IsNullOrEmpty(Parametros.P_Logo_Municipio))
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Logo_Municipio + " = '" + Parametros.P_Logo_Municipio + "'";
                    if (!String.IsNullOrEmpty(Parametros.P_Logo_Estado)) 
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Logo_Estado + " = '" + Parametros.P_Logo_Estado + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                else
                {
                    //Insertar el nuevo proveedor
                    Mi_SQL = "INSERT INTO " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros;
                    Mi_SQL = Mi_SQL + " (" + Cat_Tal_Parametros.Campo_Fuente_Financiamiento + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Programa_ID + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Partida_ID + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Director + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Coordinador + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Almacen + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Proveedor_Gasolina_Id + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Oficial_Mayor + "";
                    if (!String.IsNullOrEmpty(Parametros.P_Logo_Municipio)) 
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Logo_Municipio + "";
                    if (!String.IsNullOrEmpty(Parametros.P_Logo_Estado)) 
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Logo_Estado + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID + ")";
                    Mi_SQL = Mi_SQL + " VALUES ('" + Parametros.P_Fuente_Financiamiento_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Programa_ID + "','" + Parametros.P_Partida_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Dependencia_ID + "','" + Parametros.P_Director + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Coordinador + "','" + Parametros.P_Almacen + "','" + Parametros.P_Proveedor_Gasolina_Id + "','" + Parametros.P_Oficial_Mayor + "'";
                    if (!String.IsNullOrEmpty(Parametros.P_Logo_Municipio)) 
                        Mi_SQL = Mi_SQL + ",'" + Parametros.P_Logo_Municipio + "'";
                    if (!String.IsNullOrEmpty(Parametros.P_Logo_Estado)) 
                        Mi_SQL = Mi_SQL + ",'" + Parametros.P_Logo_Estado + "'";
                    Mi_SQL = Mi_SQL + ",'" + Parametros.P_Tipo_Solicitud_Pago_ID + "')";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }   
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consulta_Parametros     
        ///DESCRIPCIÓN          : Consultar un registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 31/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Cls_Tal_Parametros_Negocio Consulta_Parametros_Presupuesto(Cls_Tal_Parametros_Negocio Parametros)
        {
            String Mi_SQL = null;
            Cls_Tal_Parametros_Negocio Obj_Cargado = new Cls_Tal_Parametros_Negocio();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            try {
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                if (Parametros.P_Tipo_Bien == "VEHICULO")
                {
                    Mi_SQL = "SELECT vehiculo." + Ope_Pat_Vehiculos.Campo_Partida_ID  +
                        ",vehiculo." + Ope_Pat_Vehiculos.Campo_Dependencia_ID  +
                        ", ( SELECT " + Cat_Tal_Parametros.Campo_Fuente_Financiamiento + " FROM " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros + " ) " + Cat_Tal_Parametros.Campo_Fuente_Financiamiento +
                        ", ( SELECT " + Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID + " FROM " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros + " ) " + Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID +
                    ",programa_dependencia." + Cat_Sap_Det_Prog_Dependencias.Campo_Proyecto_Programa_ID +
                    ",partidas_especificas." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID +
                    ",partidas_genericas." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID  +
                    ",conceptos." + Cat_Sap_Concepto.Campo_Capitulo_ID +
                    " FROM " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " vehiculo" +
                        //Join Para obtener Proyecto ID
                        " LEFT OUTER JOIN " + Cat_Sap_Det_Prog_Dependencias.Tabla_Cat_Sap_Det_Prog_Dependencias + " programa_dependencia ON vehiculo." + Ope_Pat_Vehiculos.Campo_Dependencia_ID + " = programa_dependencia." + Cat_Sap_Det_Prog_Dependencias.Campo_Dependencia_ID +
                        //Join Para obtener Partida_Generica_ID
                        " LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " partidas_especificas ON vehiculo." + Ope_Pat_Vehiculos.Campo_Partida_ID + " = partidas_especificas." + Cat_Tal_Parametros.Campo_Partida_ID +
                        //Join Para obtener Concepto_ID
                        " LEFT OUTER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " partidas_genericas  ON partidas_especificas." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = partidas_genericas." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID +
                        //Join Para obtener Capitulo_ID
                        " LEFT OUTER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " conceptos ON partidas_genericas." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " = conceptos." + Cat_Sap_Concepto.Campo_Concepto_ID +
                    " WHERE vehiculo." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " = '" + Parametros.P_Numero_Economico + "'";
                }
                else if (Parametros.P_Tipo_Bien == "BIEN_MUEBLE")
                {
                    Mi_SQL = "SELECT bien_mueble." + Ope_Pat_Bienes_Muebles.Campo_Partida_ID +
                    ",bien_mueble." + Ope_Pat_Bienes_Muebles.Campo_Dependencia_ID +
                    ", ( SELECT " + Cat_Tal_Parametros.Campo_Fuente_Financiamiento + " FROM " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros + " ) " + Cat_Tal_Parametros.Campo_Fuente_Financiamiento +
                    ", ( SELECT " + Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID + " FROM " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros + " ) " + Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID +
                ",programa_dependencia." + Cat_Sap_Det_Prog_Dependencias.Campo_Proyecto_Programa_ID +
                ",partidas_especificas." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID +
                ",partidas_genericas." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID +
                ",conceptos." + Cat_Sap_Concepto.Campo_Capitulo_ID +
                " FROM " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " bien_mueble" +
                        //Join Para obtener Proyecto ID
                    " LEFT OUTER JOIN " + Cat_Sap_Det_Prog_Dependencias.Tabla_Cat_Sap_Det_Prog_Dependencias + " programa_dependencia ON bien_mueble." + Ope_Pat_Bienes_Muebles.Campo_Dependencia_ID + " = programa_dependencia." + Cat_Sap_Det_Prog_Dependencias.Campo_Dependencia_ID +
                        //Join Para obtener Partida_Generica_ID
                    " LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " partidas_especificas ON bien_mueble." + Ope_Pat_Bienes_Muebles.Campo_Partida_ID + " = partidas_especificas." + Cat_Tal_Parametros.Campo_Partida_ID +
                        //Join Para obtener Concepto_ID
                    " LEFT OUTER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " partidas_genericas  ON partidas_especificas." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = partidas_genericas." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID +
                        //Join Para obtener Capitulo_ID
                    " LEFT OUTER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " conceptos ON partidas_genericas." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " = conceptos." + Cat_Sap_Concepto.Campo_Concepto_ID +
                " WHERE bien_mueble." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " = '" + Parametros.P_Numero_Economico + "'";
                }

                Cmd.CommandText = Mi_SQL;
                SqlDataReader Reader = Cmd.ExecuteReader();
                //OracleDataReader Reader = OracleHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                while (Reader.Read()) 
                {
                    Obj_Cargado.P_Fuente_Financiamiento_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Parametros.Campo_Fuente_Financiamiento].ToString())) ? Reader[Cat_Tal_Parametros.Campo_Fuente_Financiamiento].ToString() : String.Empty;
                    Obj_Cargado.P_Partida_ID = (!String.IsNullOrEmpty(Reader[Ope_Pat_Vehiculos.Campo_Partida_ID].ToString())) ? Reader[Ope_Pat_Vehiculos.Campo_Partida_ID].ToString() : String.Empty;
                    Obj_Cargado.P_Programa_ID = (!String.IsNullOrEmpty(Reader[Cat_Sap_Det_Prog_Dependencias.Campo_Proyecto_Programa_ID].ToString())) ? Reader[Cat_Sap_Det_Prog_Dependencias.Campo_Proyecto_Programa_ID].ToString() : String.Empty;
                    Obj_Cargado.P_Partida_Generica_ID = (!String.IsNullOrEmpty(Reader[Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID].ToString())) ? Reader[Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID].ToString() : String.Empty;
                    Obj_Cargado.P_Concepto_ID = (!String.IsNullOrEmpty(Reader[Cat_Sap_Partidas_Genericas.Campo_Concepto_ID].ToString())) ? Reader[Cat_Sap_Partidas_Genericas.Campo_Concepto_ID].ToString() : String.Empty;
                    Obj_Cargado.P_Capitulo_ID = (!String.IsNullOrEmpty(Reader[Cat_Sap_Concepto.Campo_Capitulo_ID].ToString())) ? Reader[Cat_Sap_Concepto.Campo_Capitulo_ID].ToString() : String.Empty;
                    Obj_Cargado.P_Dependencia_ID = (!String.IsNullOrEmpty(Reader[Ope_Pat_Vehiculos.Campo_Dependencia_ID].ToString())) ? Reader[Ope_Pat_Vehiculos.Campo_Dependencia_ID].ToString() : String.Empty;
                    Obj_Cargado.P_Tipo_Solicitud_Pago_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID].ToString())) ? Reader[Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID].ToString() : String.Empty;
                }
                Reader.Close();
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Commit();
                } 
            } catch (Exception Ex) {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
            return Obj_Cargado;
    }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consulta_Parametros     
        ///DESCRIPCIÓN          : Consultar un registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 31/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Cls_Tal_Parametros_Negocio Consulta_Parametros(Cls_Tal_Parametros_Negocio Parametros)
        {
            String Mi_SQL = null;
            Cls_Tal_Parametros_Negocio Obj_Cargado = new Cls_Tal_Parametros_Negocio();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            try
            {
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                Mi_SQL = "SELECT PA.*" +
                    ",PE." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID +
                    ",PG." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID +
                    ",CO." + Cat_Sap_Concepto.Campo_Capitulo_ID +
                    ",PR." + Cat_Com_Proveedores.Campo_Nombre +
                    " FROM " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros + " PA " +
                    " LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " PE " +
                    " ON PE." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = PA." + Cat_Tal_Parametros.Campo_Partida_ID +
                    " LEFT OUTER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + " PG " +
                    " ON PG." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = PE." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID +
                    " LEFT OUTER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " CO " +
                    " ON CO." + Cat_Sap_Concepto.Campo_Concepto_ID + " = PG." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID +
                    " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PR " +
                    " ON PA." + Cat_Tal_Parametros.Campo_Proveedor_Gasolina_Id + " = PR." + Cat_Com_Proveedores.Campo_Proveedor_ID;

                Cmd.CommandText = Mi_SQL;
                SqlDataReader Reader = Cmd.ExecuteReader();
                //OracleDataReader Reader = OracleHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                while (Reader.Read())
                {
                    Obj_Cargado.P_Fuente_Financiamiento_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Parametros.Campo_Fuente_Financiamiento].ToString())) ? Reader[Cat_Tal_Parametros.Campo_Fuente_Financiamiento].ToString() : String.Empty;
                    Obj_Cargado.P_Partida_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Parametros.Campo_Partida_ID].ToString())) ? Reader[Cat_Tal_Parametros.Campo_Partida_ID].ToString() : String.Empty;
                    Obj_Cargado.P_Programa_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Parametros.Campo_Programa_ID].ToString())) ? Reader[Cat_Tal_Parametros.Campo_Programa_ID].ToString() : String.Empty;
                    Obj_Cargado.P_Partida_Generica_ID = (!String.IsNullOrEmpty(Reader[Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID].ToString())) ? Reader[Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID].ToString() : String.Empty;
                    Obj_Cargado.P_Concepto_ID = (!String.IsNullOrEmpty(Reader[Cat_Sap_Partidas_Genericas.Campo_Concepto_ID].ToString())) ? Reader[Cat_Sap_Partidas_Genericas.Campo_Concepto_ID].ToString() : String.Empty;
                    Obj_Cargado.P_Capitulo_ID = (!String.IsNullOrEmpty(Reader[Cat_Sap_Concepto.Campo_Capitulo_ID].ToString())) ? Reader[Cat_Sap_Concepto.Campo_Capitulo_ID].ToString() : String.Empty;
                    Obj_Cargado.P_Dependencia_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Parametros.Campo_Dependencia_ID].ToString())) ? Reader[Cat_Tal_Parametros.Campo_Dependencia_ID].ToString() : String.Empty;
                    Obj_Cargado.P_Director = (!String.IsNullOrEmpty(Reader[Cat_Tal_Parametros.Campo_Director].ToString())) ? Reader[Cat_Tal_Parametros.Campo_Director].ToString() : String.Empty;
                    Obj_Cargado.P_Coordinador = (!String.IsNullOrEmpty(Reader[Cat_Tal_Parametros.Campo_Coordinador].ToString())) ? Reader[Cat_Tal_Parametros.Campo_Coordinador].ToString() : String.Empty;
                    Obj_Cargado.P_Almacen = (!String.IsNullOrEmpty(Reader[Cat_Tal_Parametros.Campo_Almacen].ToString())) ? Reader[Cat_Tal_Parametros.Campo_Almacen].ToString() : String.Empty;
                    Obj_Cargado.P_Nombre_Proveedor = (!String.IsNullOrEmpty(Reader[Cat_Com_Proveedores.Campo_Nombre].ToString())) ? Reader[Cat_Com_Proveedores.Campo_Nombre].ToString() : String.Empty;
                    Obj_Cargado.P_Oficial_Mayor = (!String.IsNullOrEmpty(Reader[Cat_Tal_Parametros.Campo_Oficial_Mayor].ToString())) ? Reader[Cat_Tal_Parametros.Campo_Oficial_Mayor].ToString() : String.Empty;
                    Obj_Cargado.P_Tipo_Solicitud_Pago_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID].ToString())) ? Reader[Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID].ToString() : String.Empty;
                    Obj_Cargado.P_Proveedor_Gasolina_Id = (!String.IsNullOrEmpty(Reader[Cat_Tal_Parametros.Campo_Proveedor_Gasolina_Id].ToString())) ? Reader[Cat_Tal_Parametros.Campo_Proveedor_Gasolina_Id].ToString() : String.Empty;
                    Obj_Cargado.P_Logo_Municipio = (!String.IsNullOrEmpty(Reader[Cat_Tal_Parametros.Campo_Logo_Municipio].ToString())) ? Reader[Cat_Tal_Parametros.Campo_Logo_Municipio].ToString() : String.Empty;
                    Obj_Cargado.P_Logo_Estado = (!String.IsNullOrEmpty(Reader[Cat_Tal_Parametros.Campo_Logo_Estado].ToString())) ? Reader[Cat_Tal_Parametros.Campo_Logo_Estado].ToString() : String.Empty;
                }
                Reader.Close();
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (Exception Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
            return Obj_Cargado;
        }
  }
}