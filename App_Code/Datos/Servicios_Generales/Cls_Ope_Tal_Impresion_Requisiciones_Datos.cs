﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Impresion_Requisiciones.Negocio;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;

namespace JAPAMI.Taller_Mecanico.Impresion_Requisiciones.Datos
{

    public class Cls_Ope_Tal_Impresion_Requisiciones_Datos
    {
        public Cls_Ope_Tal_Impresion_Requisiciones_Datos()
        {            
        }
        
        public static DataTable Consultar_Requisiciones(Cls_Ope_Tal_Impresion_Requisiciones_Negocio Datos)
        {
            //Declaracion de Variables
            String Mi_SQL = String.Empty; //Variable para las consultas

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + ".*, ";
                Mi_SQL = Mi_SQL + "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM ";
                Mi_SQL = Mi_SQL + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + " = " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Dependencia_ID + ")";
                Mi_SQL = Mi_SQL + " AS UNIDAD_RESPONSABLE, ";
                
                Mi_SQL = Mi_SQL + " ( SELECT ";
                Mi_SQL = Mi_SQL + Cat_Empleados.Tabla_Cat_Empleados + ".";
                Mi_SQL = Mi_SQL + Cat_Empleados.Campo_Nombre + "  +' '+ ";
                Mi_SQL = Mi_SQL + Cat_Empleados.Tabla_Cat_Empleados + ".";
                Mi_SQL = Mi_SQL + Cat_Empleados.Campo_Apellido_Paterno + "  +' '+ ";
                Mi_SQL = Mi_SQL + Cat_Empleados.Tabla_Cat_Empleados + ".";
                Mi_SQL = Mi_SQL + Cat_Empleados.Campo_Apellido_Materno;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " WHERE ";
                Mi_SQL = Mi_SQL + Cat_Empleados.Tabla_Cat_Empleados + ".";
                Mi_SQL = Mi_SQL + Cat_Empleados.Campo_Empleado_ID + " = ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + ".";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Campo_Empleado_Autorizacion_ID + " ) AS EMPLEADO ";

                Mi_SQL = Mi_SQL + ", ( SELECT " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." +  Ope_Tal_Requisiciones.Campo_No_Solicitud + ") AS TIPO_BIEN ";

                Mi_SQL = Mi_SQL + "FROM ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;
                Mi_SQL = Mi_SQL + " WHERE " +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + 
                    Ope_Tal_Requisiciones.Campo_Estatus + " IS NOT NULL ";


                if (!String.IsNullOrEmpty(Datos.P_Requisicion_ID))
                {
                    Mi_SQL = Mi_SQL + " AND ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = '" + Datos.P_Requisicion_ID + "' ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Dependencia_ID))
                {
                    Mi_SQL = Mi_SQL + " AND ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID + "' ";
                }
                if (!String.IsNullOrEmpty(Datos.P_Estatus))
                {
                    Mi_SQL = Mi_SQL + " AND ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Estatus + " = '" + Datos.P_Estatus + "' ";
                }

                if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicial) && !String.IsNullOrEmpty(Datos.P_Fecha_Final))
                {
                    Mi_SQL = Mi_SQL + " AND TO_DATE(TO_CHAR(" + Ope_Tal_Requisiciones.Campo_Fecha_Creo + ",'DD-MM-YYYY'))" +
                            " >= '" + Datos.P_Fecha_Inicial + "' AND " +
                    "TO_DATE(TO_CHAR(" + Ope_Tal_Requisiciones.Campo_Fecha_Creo + ",'DD-MM-YYYY'))" +
                            " <= '" + Datos.P_Fecha_Final + "'";
                }

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }


        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Consultar_Requisiciones_Detalles
        ///DESCRIPCIÓN: Consultar los detalles de la requisicion
        ///PARAMETROS: 
        ///CREO: jtoledo
        ///FECHA_CREO: 04/28/2011 04:37:00 p.m.
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************        
        internal static DataTable Consultar_Requisiciones_Detalles(Cls_Ope_Tal_Impresion_Requisiciones_Negocio Datos)
        {
            //Declaracion de Variables
            String Mi_SQL = String.Empty; //Variable para las consultas
            Mi_SQL = "SELECT " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + "." + Cat_Com_Unidades.Campo_Abreviatura + " AS UNIDAD,";
            Mi_SQL += Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + ".*"; 
            Mi_SQL += " FROM ";
            Mi_SQL += Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion;
            Mi_SQL += " LEFT JOIN " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
            Mi_SQL += " ON " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID;
            Mi_SQL += " = ";
            Mi_SQL += Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Refaccion_ID;
            Mi_SQL += " LEFT JOIN " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " ON ";
            Mi_SQL += Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Unidad_ID + " = ";
            Mi_SQL += Cat_Com_Unidades.Tabla_Cat_Com_Unidades + "." + Cat_Com_Unidades.Campo_Unidad_ID;
            Mi_SQL += " WHERE " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + ".";
            Mi_SQL += Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = '" + Datos.P_Requisicion_ID + "' ";
            try
            {
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
    }
}