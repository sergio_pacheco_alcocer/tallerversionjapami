﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Contrarecibos.Negocio;
using JAPAMI.Constantes;
using System.Data.SqlClient;
using JAPAMI.Sessiones;
using JAPAMI.Bitacora_Eventos;
using System.Collections;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Taller_Mecanico.Requisiciones.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Tal_Contrarecibo_Datos
/// </summary>
namespace JAPAMI.Taller_Mecanico.Operacion_Contrarecibos.Datos {
    public class Cls_Ope_Tal_Contrarecibo_Datos {

        #region (Metodos)

            ///*******************************************************************************
            /// NOMBRE DE LA CLASE:     Consulta_Ordenes_Compra
            /// DESCRIPCION:            Método utilizado para consultar las ordenes de compra que se encuentren en estatus "SURTIDA"
            /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene la información para realizar la consulta
            ///                         
            /// CREO       :            Salvador Hernandez Ramirez
            /// FECHA_CREO :            14/Marzo/2011  
            /// MODIFICO          :     Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO    :     05/Julio/2012
            /// CAUSA_MODIFICACION:     Adecuacion a Taller Mecanico Mpal.
            ///*******************************************************************************/
            public static DataTable Consulta_Ordenes_Compra(Cls_Ope_Tal_Contrarecibo_Negocio Datos)
            {
                String Mi_SQL = String.Empty; //Variable para las consultas

                try
                {
                    // Asignar consulta
                    Mi_SQL = "SELECT " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + ", "; 
                    Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR, "; 
                    Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Creo + " AS FECHA, ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Folio + ", "; 
                    Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Estatus + ", ";
                    Mi_SQL = Mi_SQL + "(select REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Folio + " FROM ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQUISICIONES ";
                    Mi_SQL = Mi_SQL + " WHERE REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Lista_Requisiciones + ") AS NO_REQUISICION, ";

                    Mi_SQL = Mi_SQL + "(select REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Listado_Almacen+ " FROM ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQUISICIONES ";
                    Mi_SQL = Mi_SQL + " WHERE REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Lista_Requisiciones + ") LISTADO_ALMACEN, ";

                    Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Total + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Tipo_Articulo + "";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + ", " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ";
                    Mi_SQL = Mi_SQL + "WHERE " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Proveedor_ID;
                    Mi_SQL = Mi_SQL + " = " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + " ";
                    Mi_SQL = Mi_SQL + "AND " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Estatus + " = 'SURTIDA'";
                    Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_No_Factura_Interno + " is null "; // Cuando no tiene asignado un numero de contra recibo,

                    if (Datos.P_No_Orden_Compra != null)
                    {
                        Mi_SQL = Mi_SQL + "AND " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " like '%" + Datos.P_No_Orden_Compra + "%'";
                    }

                    if (Datos.P_No_Requisicion!= null)
                    {
                        Mi_SQL = Mi_SQL + "AND " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Lista_Requisiciones + " like '%" + Datos.P_No_Requisicion+ "%'";
                    }

                    if (Datos.P_Proveedor_ID != null)
                    {
                        Mi_SQL = Mi_SQL + "AND " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "'";
                    }

                    if ((Datos.P_Fecha_Inicio_B != null) && (Datos.P_Fecha_Fin_B != null))
                    {
                        Mi_SQL = Mi_SQL + " AND TO_DATE(TO_CHAR(" + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Creo + ",'DD/MM/YY')) BETWEEN '" + Datos.P_Fecha_Inicio_B + "'" +
                      " AND '" + Datos.P_Fecha_Fin_B + "'";
                    }

                    Mi_SQL = Mi_SQL + " order by " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra; 

                    //Entregar resultado
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                catch (DBConcurrencyException ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                finally
                {
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA CLASE:     Consulta_Proveedores
            /// DESCRIPCION:            Consultar los datos de los proveedores
            /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene 
            ///                         los datos para la busqueda
            /// CREO       :            Salvador Hernández Ramírez
            /// FECHA_CREO :            16/Marzo/2011 
            /// MODIFICO          :     Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO    :     05/Julio/2012
            /// CAUSA_MODIFICACION:     Adecuacion a Taller Mecanico Mpal.
            ///*******************************************************************************/
            public static DataTable Consulta_Proveedores(Cls_Ope_Tal_Contrarecibo_Negocio Datos) {
                //Declaracion de variables
                String Mi_SQL = String.Empty; //variable apra las consultas

                try {
                    //Consulta
                    Mi_SQL = " SELECT DISTINCT " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ";
                    Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Compañia + " ";
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + ", " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Proveedor_ID;
                    Mi_SQL = Mi_SQL + " = " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + " ";
                    Mi_SQL = Mi_SQL + " and " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Estatus + " = 'SURTIDA' ";
                    Mi_SQL = Mi_SQL + "ORDER BY " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Compañia; // Ordenamiento

                    // Resultado
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                catch (DBConcurrencyException ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                finally
                {
                }
            }


            ///******************************************************************************* // Este se ocupa en la clase "ContraREcibo_Datos"
            /// NOMBRE DE LA CLASE:     Guardar_Contra_Recibo
            /// DESCRIPCION:            Se guardaa la información que forma parte del contra recibo
            /// PARAMETROS :                                 
            /// CREO       :            Salvador Hernández Ramírez  
            /// FECHA_CREO :            16/Marzo/2011 
            /// MODIFICO          :     Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO    :     05/Julio/2012
            /// CAUSA_MODIFICACION:     Adecuacion a Taller Mecanico Mpal.
            ///*******************************************************************************/
            public static Int64 Guardar_Contra_Recibo(Cls_Ope_Tal_Contrarecibo_Negocio Datos) {
                //Declaracion de variables
                String Mensaje = "";
                String Mi_SQL = String.Empty;
                Object Aux, No_Contrarecibo, Marbete, Factura, Registro;
                Int64 No_Marbete, Factura_ID, Registro_ID;
                Double Importe_Total_Facturas =0;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                SqlDataAdapter Adaptador = new SqlDataAdapter();
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;

                try
                {
           
                  // SE GUARDA EL CONTRARECIBO
                    //Asignar consulta para el maximo No_Contrarecibo
                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Tal_Facturas_Proveedores.Campo_No_Factura_Interno + "), 0) "; // Este es el NO_CONTRA_RECIBO
                    Mi_SQL = Mi_SQL + "FROM " + Ope_Tal_Facturas_Proveedores.Tabla_Ope_Tal_Facturas_Proveedores;

                    //Ejecutar consulta
                    //No_Contrarecibo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    Cmd.CommandText = Mi_SQL;
                    No_Contrarecibo = Cmd.ExecuteScalar();
                    //Verificar si no es nulo
                    if (No_Contrarecibo != null && Convert.IsDBNull(No_Contrarecibo) == false)
                        Datos.P_No_Contra_Recibo = Convert.ToInt64(No_Contrarecibo) + 1;
                    else
                        Datos.P_No_Contra_Recibo = 1;

                    //Asignar consulta para ingresar la factura
                    Mi_SQL = "INSERT INTO " + Ope_Tal_Facturas_Proveedores.Tabla_Ope_Tal_Facturas_Proveedores + " (";
                    Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Proveedores.Campo_No_Factura_Interno + ", "; // Es el No de  contra recibo
                    Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Proveedores.Campo_No_Factura_Proveedor + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Proveedores.Campo_Proveedor_ID + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Proveedores.Campo_Fecha_Factura + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Proveedores.Campo_Fecha_Recepcion + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Proveedores.Campo_Fecha_Pago + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Proveedores.Campo_SubTotal_Sin_Impuesto + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Proveedores.Campo_IVA + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Proveedores.Campo_Total + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Proveedores.Campo_Comentarios + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Proveedores.Campo_Usuario_Creo + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Proveedores.Campo_Fecha_Creo + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Proveedores.Campo_Empleado_Almacen_ID + ") ";
                    Mi_SQL = Mi_SQL + "VALUES(" + Datos.P_No_Contra_Recibo + ", '"; 
                    Mi_SQL = Mi_SQL + Datos.P_No_Factura_Proveedor + "', '";
                    Mi_SQL = Mi_SQL + Datos.P_Proveedor_ID + "', ";
                    Mi_SQL = Mi_SQL + "'" + String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Datos.P_Fecha_Factura)) + "', ";//
                    Mi_SQL = Mi_SQL + " GETDATE(), ";
                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Pago))
                        Mi_SQL = Mi_SQL + "'" + String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Datos.P_Fecha_Pago)) + "', ";
                    else
                        Mi_SQL = Mi_SQL + "NULL,";
                    Mi_SQL = Mi_SQL + Datos.P_SubTotal.ToString().Trim() + ", ";
                    Mi_SQL = Mi_SQL + Datos.P_IVA.ToString().Trim() + ", ";
                    Mi_SQL = Mi_SQL + Datos.P_Total.ToString().Trim() + ", '";
                    Mi_SQL = Mi_SQL + Datos.P_Observaciones.ToString().Trim() + "', '";
                    Mi_SQL = Mi_SQL + Datos.P_Usuario_Creo.ToString().Trim() + "', ";
                    Mi_SQL = Mi_SQL + " GETDATE(), '";
                    Mi_SQL = Mi_SQL + Datos.P_Empleado_Almacen_ID + "')";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery(); // Se ejecuta la operación  


                    // SE ACTUALIZA LA ORDEN DE COMPRA (Se le asigna su numero de factura interno, registrada, fecha y usuario modificó)
                    //Consulta para colocar el numero de factura a la orden de compra
                    Mi_SQL = "UPDATE " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " ";
                    Mi_SQL = Mi_SQL + "SET " + Ope_Tal_Ordenes_Compra.Campo_No_Factura_Interno + " = " + Datos.P_No_Contra_Recibo + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Campo_Fecha_Modifico + " = GETDATE() ";

                    if (Datos.P_Tipo_Orden_Compra.Trim() == "LISTADO_ALMACEN") // Si es un listado de almacén
                        Mi_SQL = Mi_SQL +  " , " + Ope_Tal_Ordenes_Compra.Campo_Estatus + " = 'LIS_ALM_SURTIDO' ";

                      Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.Trim() + "";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery(); // Se ejecuta la operación 

                    if (Datos.P_Dt_Documentos_Soporte != null)
                    {
                        //Asignar consulta para el maximo Marbete de la tabla OPE_COM_DET_DOC_SOPORTE
                        Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Tal_Det_Doc_Soporte.Campo_Marbete + "), 0) ";
                        Mi_SQL = Mi_SQL + "FROM " + Ope_Tal_Det_Doc_Soporte.Tabla_Ope_Tal_Det_Doc_Soporte;

                        //Ejecutar consulta
                        Cmd.CommandText = Mi_SQL;
                        Marbete = Cmd.ExecuteScalar(); // Se ejecuta la operación 
                        //Marbete = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                        //Verificar si no es nulo
                        if (Marbete != null && Convert.IsDBNull(Marbete) == false)
                            No_Marbete = Convert.ToInt64(Marbete) + 1;
                        else
                            No_Marbete = 1;

                        //Ciclo para colocar el numero de factura a la orden de compra
                        for (int Cont = 0; Cont < Datos.P_Dt_Documentos_Soporte.Rows.Count; Cont++)
                        {
                            //Asignar consulta para ingresar la factura
                            Mi_SQL = "INSERT INTO " + Ope_Tal_Det_Doc_Soporte.Tabla_Ope_Tal_Det_Doc_Soporte + " (";
                            Mi_SQL = Mi_SQL + Ope_Tal_Det_Doc_Soporte.Campo_Documento_ID + ", ";
                            Mi_SQL = Mi_SQL + Ope_Tal_Det_Doc_Soporte.Campo_No_Factura_Interno + ", "; // Este es el No_Contra_Recibo
                            Mi_SQL = Mi_SQL + Ope_Tal_Det_Doc_Soporte.Campo_Usuario_Creo + ", ";
                            Mi_SQL = Mi_SQL + Ope_Tal_Det_Doc_Soporte.Campo_Fecha_Creo + ", ";
                            Mi_SQL = Mi_SQL + Ope_Tal_Det_Doc_Soporte.Campo_Marbete + ") ";
                            Mi_SQL = Mi_SQL + "VALUES('" + Datos.P_Dt_Documentos_Soporte.Rows[Cont]["DOCUMENTO_ID"].ToString().Trim() + "', '";
                            Mi_SQL = Mi_SQL + Datos.P_No_Contra_Recibo + "', '";
                            Mi_SQL = Mi_SQL + Datos.P_Usuario_Creo + "', ";
                            Mi_SQL = Mi_SQL + " GETDATE(), ";
                            Mi_SQL = Mi_SQL + No_Marbete + ")";

                            // String Agregar_Marbete = Convert.ToString(No_Marbete);
                            // Se da de alta la operación en el método "Alta_Bitacora"
                            //Cls_Bitacora.Alta_Bitacora(Cls_Sessiones.Empleado_ID, Ope_Bitacora.Accion_Alta, "Frm_Ope_Alm_Elaborar_Contrarecibo.aspx", Agregar_Marbete, Mi_SQL);

                            //Ejecutar consulta
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery(); // Se ejecuta la operación
                            No_Marbete = No_Marbete + 1; // Se incrementa
                        }
                    }

                    // SE GUARDAN LAS FACTURAS
                    if (Datos.P_Dt_Facturas_Proveedor != null) {
                            //Asignar consulta para el maximo Marbete de la tabla OPE_COM_DET_DOC_SOPORTE
                            Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Tal_Registro_Facturas.Campo_Factura_ID + "), 0) ";
                            Mi_SQL = Mi_SQL + "FROM " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas;

                            //Ejecutar consulta
                            //Factura = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                            Cmd.CommandText = Mi_SQL;
                            Factura = Cmd.ExecuteScalar();

                            //Verificar si no es nulo
                            if (Factura != null && Convert.IsDBNull(Factura) == false)
                                Factura_ID = Convert.ToInt64(Factura) + 1;
                            else
                                Factura_ID = 1;

                        //Ciclo para colocar los numeros de factura
                        for (int Cont = 0; Cont < Datos.P_Dt_Facturas_Proveedor.Rows.Count; Cont++)
                        {
                            //Asignar consulta para ingresar la factura
                            Mi_SQL = "INSERT INTO " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas + " (";
                            Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_Factura_ID + ", ";
                            Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_Factura_Proveedor + ", ";
                            Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_No_Contra_Recibo + ", ";
                            Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_Importe_Factura + ", ";
                            Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_Fecha_Factura + ", ";
                            Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_Usuario_Creo + ", ";
                            Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_Fecha_Creo + ") ";
                            Mi_SQL = Mi_SQL + "VALUES(" + Factura_ID + ", '";
                            Mi_SQL = Mi_SQL + Datos.P_Dt_Facturas_Proveedor.Rows[Cont]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "', ";
                            Mi_SQL = Mi_SQL + Datos.P_No_Contra_Recibo + ", ";
                            Mi_SQL = Mi_SQL + Datos.P_Dt_Facturas_Proveedor.Rows[Cont]["IMPORTE_FACTURA"].ToString().Trim() + ", '";
                            Mi_SQL = Mi_SQL + String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Datos.P_Dt_Facturas_Proveedor.Rows[Cont]["FECHA_FACTURA"].ToString().Trim())) + "', '";
                            Mi_SQL = Mi_SQL + Datos.P_Usuario_Creo + "', ";
                            Mi_SQL = Mi_SQL + " GETDATE() )";

                            Importe_Total_Facturas= Importe_Total_Facturas + Convert.ToDouble(Datos.P_Dt_Facturas_Proveedor.Rows[Cont]["IMPORTE_FACTURA"].ToString().Trim());

                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery(); // Se ejecuta la operación 
                            Factura_ID = Factura_ID + 1;
                        }
                    }

                    if (Datos.P_Tipo_Orden_Compra.Trim() == "TRANSITORIA") // SI NO ES UNA REQUISICION DE LISTADO DE STOCK
                    {
                            // Consulta para obtener los  montos 
                            Mi_SQL = "SELECT  " + Ope_Tal_Req_Refaccion.Campo_Precio_U_Sin_Imp_Cotizado + " as PRECIO_PRODUCTO";
                            Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID + " as PRODUCTO_ID";
                            Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion;
                            Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_No_Orden_Compra;
                            Mi_SQL = Mi_SQL + " = '" + Datos.P_No_Orden_Compra.Trim() + "'";

                            DataTable Dt_Productos_OC = new DataTable();
                            Cmd.CommandText = Mi_SQL;
                            Adaptador.SelectCommand = Cmd;
                            Adaptador.Fill(Dt_Productos_OC);
                            if (Datos.P_Tipo_Articulo.Trim() != "SERVICIO")   // Si no es una requisicion de servicios y es transitoria, se  agegan los productos a la tabla
                            {

                                // SE GUARDAN LOS PRODUCTOS DEL CONTRA RECIBO
                                if (Datos.P_Dt_Productos_OC != null)
                                {
                                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Tal_Refacciones_Contrarecibo.Campo_No_Registro + "), 0) ";
                                    Mi_SQL = Mi_SQL + "FROM " + Ope_Tal_Refacciones_Contrarecibo.Tabla_Ope_Tal_Refacciones_Contrarecibo;
                                    
                                    //Ejecutar consulta
                                    //Registro = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                                    Cmd.CommandText = Mi_SQL;
                                    Registro = Cmd.ExecuteScalar();

                                    //Verificar si no es nulo
                                    if (Registro != null && Convert.IsDBNull(Registro) == false)
                                        Registro_ID = Convert.ToInt64(Registro) + 1;
                                    else
                                        Registro_ID = 1;

                                    //Ciclo para colocar los numeros de factura
                                    for (int Cont = 0; Cont < Datos.P_Dt_Productos_OC.Rows.Count; Cont++)
                                    {
                                        // Asignar consulta para ingresar la factura
                                        Mi_SQL = "INSERT INTO " + Ope_Tal_Refacciones_Contrarecibo.Tabla_Ope_Tal_Refacciones_Contrarecibo + " (";
                                        Mi_SQL = Mi_SQL + Ope_Tal_Refacciones_Contrarecibo.Campo_No_Registro + ", ";
                                        Mi_SQL = Mi_SQL + Ope_Tal_Refacciones_Contrarecibo.Campo_No_Contra_Recibo + ", ";
                                        Mi_SQL = Mi_SQL + Ope_Tal_Refacciones_Contrarecibo.Campo_Refaccion_ID + ", ";
                                        Mi_SQL = Mi_SQL + Ope_Tal_Refacciones_Contrarecibo.Campo_Resguardo + ", ";
                                        Mi_SQL = Mi_SQL + Ope_Tal_Refacciones_Contrarecibo.Campo_Recibo + ", ";
                                        Mi_SQL = Mi_SQL + Ope_Tal_Refacciones_Contrarecibo.Campo_Unidad + ", ";
                                        Mi_SQL = Mi_SQL + Ope_Tal_Refacciones_Contrarecibo.Campo_Totalidad + ", ";
                                        Mi_SQL = Mi_SQL + Ope_Tal_Refacciones_Contrarecibo.Campo_Recibo_Transitorio + ", ";
                                        Mi_SQL = Mi_SQL + Ope_Tal_Refacciones_Contrarecibo.Campo_Usuario_Creo + ", ";
                                        Mi_SQL = Mi_SQL + Ope_Tal_Refacciones_Contrarecibo.Campo_Fecha_Creo + ") ";
                                        Mi_SQL = Mi_SQL + "VALUES(" + Registro_ID + ", ";
                                        Mi_SQL = Mi_SQL + Datos.P_No_Contra_Recibo + ", '";
                                        Mi_SQL = Mi_SQL + Datos.P_Dt_Productos_OC.Rows[Cont]["PRODUCTO_ID"].ToString().Trim() + "', '";
                                        Mi_SQL = Mi_SQL + Datos.P_Dt_Productos_OC.Rows[Cont]["RESGUARDO"].ToString().Trim() + "', '";
                                        Mi_SQL = Mi_SQL + Datos.P_Dt_Productos_OC.Rows[Cont]["RECIBO"].ToString().Trim() + "', '";
                                        Mi_SQL = Mi_SQL + Datos.P_Dt_Productos_OC.Rows[Cont]["UNIDAD"].ToString().Trim() + "', '";
                                        Mi_SQL = Mi_SQL + Datos.P_Dt_Productos_OC.Rows[Cont]["TOTALIDAD"].ToString().Trim() + "', '";
                                        Mi_SQL = Mi_SQL + Datos.P_Dt_Productos_OC.Rows[Cont]["RECIBO_TRANSITORIO"].ToString().Trim() + "', '";
                                        Mi_SQL = Mi_SQL + Datos.P_Usuario_Creo + "', ";
                                        Mi_SQL = Mi_SQL + " GETDATE())";

                                        //Ejecutar consulta
                                        Cmd.CommandText = Mi_SQL;
                                        Cmd.ExecuteNonQuery(); // Se ejecuta la operación 
                                        Registro_ID = Registro_ID + 1; // Se incrementa

                                        // Se consulta el 
                                        String Recibo = "";
                                        String Resguardo = "";
                                        String Resguardado = "";

                                        Resguardo = "" + Datos.P_Dt_Productos_OC.Rows[Cont]["RESGUARDO"].ToString().Trim();
                                        Recibo = "" + Datos.P_Dt_Productos_OC.Rows[Cont]["RECIBO"].ToString().Trim();

                                        if ((Resguardo == "SI") | (Recibo == "SI"))
                                            Resguardado = "SI";
                                        else
                                            Resguardado = "NO";

                                        Mi_SQL = " UPDATE " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion;
                                        Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Req_Refaccion.Campo_Resguardado + " ='" + Resguardado + "'";
                                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Req_Refaccion.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.Trim();
                                        Mi_SQL = Mi_SQL + " and " + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID + " = '" + Datos.P_Dt_Productos_OC.Rows[Cont]["PRODUCTO_ID"].ToString().Trim() + "'";

                                        //Ejecutar consulta
                                        Cmd.CommandText = Mi_SQL;
                                        Cmd.ExecuteNonQuery(); // Se ejecuta la operación 
                                    }
                                }
                            }
                           
                            // Se realiza el recorrido de la tabla para actualizar su precio unitario de los productos
                            for( int j=0; j< Datos.P_Dt_Productos_OC.Rows.Count; j++){

                                //DataTable Dt_Aux_Precio = new DataTable(); //  Tabla que contendra los productos que se deben actualizar
                                Double Precio_Actualizado = 0;

                                // Verificar si es nulL
                                if (Convert.IsDBNull(Dt_Productos_OC.Rows[j]["PRECIO_PRODUCTO"]) == false)
                                    Precio_Actualizado = Convert.ToDouble(Dt_Productos_OC.Rows[j]["PRECIO_PRODUCTO"].ToString().Trim());

                                    //  Actualizar el precio de la tabla CAT_COM_PRODUCTOS
                                    Mi_SQL = " UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " ";
                                    Mi_SQL = Mi_SQL + " SET " + Cat_Tal_Refacciones.Campo_Costo_Unitario + " = " + Precio_Actualizado + "";
                                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Datos.P_Dt_Productos_OC.Rows[j]["PRODUCTO_ID"].ToString().Trim() + "'";

                                //Ejecutar consulta
                                Cmd.CommandText = Mi_SQL;
                                Cmd.ExecuteNonQuery(); // Se ejecuta la operación 
                            }
                        

                    }
                    else if (Datos.P_Tipo_Orden_Compra.Trim() == "LISTADO_ALMACEN")   // SE AUMENTAN LOS PRODUCTOS en la tabla CAT_COM_PRODUCTOS
                    {
                        DataTable Dt_Productos_Actualzar = new DataTable();    // Se Crea la tabla utilziada para actualizar los productos

                        if (Datos.P_Dt_Actualizar_Productos.Rows.Count > 0) // Si hay productos que deben ser actualziados
                        {
                            Dt_Productos_Actualzar = Datos.P_Dt_Actualizar_Productos;

                            String Producto_ID = "";
                            Int64 Cantidad_Productos = 0;
                            Int64 Existencia =0;
                            Int64 Disponible =0;
                            Double Precio_Cotizado = 0;

                                for (int i=0; i< Dt_Productos_Actualzar.Rows.Count ; i++){

                                    if (Dt_Productos_Actualzar.Rows[i]["PRODUCTO_ID"].ToString().Trim() != "")
                                        Producto_ID = Dt_Productos_Actualzar.Rows[i]["PRODUCTO_ID"].ToString().Trim();
                                    else
                                        Producto_ID = "";

                                    if (Dt_Productos_Actualzar.Rows[i]["CANTIDAD"].ToString().Trim() != "")
                                        Cantidad_Productos = Convert.ToInt64(Dt_Productos_Actualzar.Rows[i]["CANTIDAD"].ToString().Trim());
                                    else
                                        Cantidad_Productos = 0;

                                    if (Dt_Productos_Actualzar.Rows[i]["PRECIO_U"].ToString().Trim() != "")
                                        Precio_Cotizado = double.Parse(Dt_Productos_Actualzar.Rows[i]["PRECIO_U"].ToString().Trim());
                                    else
                                        Precio_Cotizado = 0;

                                       DataTable Dt_Existencia_Productos = new DataTable(); // Tabla creada para consultar las existencias y disponible de los prioductos

                                       // Se Consulta la existencia  y disponibilidad de cada producto que pertenezca a la orden de compra
                                       Mi_SQL = " SELECT  " + Cat_Tal_Refacciones.Campo_Refaccion_ID;
                                       Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Existencia;
                                       Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Disponible;
                                       Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Costo_Unitario;
                                       Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
                                       Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Refaccion_ID;
                                       Mi_SQL = Mi_SQL + " = '" + Producto_ID + "'";

                                       //Dt_Existencia_Productos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                                       Cmd.CommandText = Mi_SQL;
                                       Adaptador.SelectCommand = Cmd;
                                       Adaptador.Fill(Dt_Existencia_Productos);
                                       if (Convert.IsDBNull(Dt_Existencia_Productos.Rows[0][1]) != false) // Si no hay Existencias
                                           Existencia = Cantidad_Productos;
                                       else
                                           Existencia = Convert.ToInt64(Dt_Existencia_Productos.Rows[0][1]) + Cantidad_Productos;

                                       if (Convert.IsDBNull(Dt_Existencia_Productos.Rows[0][2]) != false) // Si no hay Disponible
                                           Disponible = Cantidad_Productos;
                                       else
                                           Disponible = Convert.ToInt64(Dt_Existencia_Productos.Rows[0][2]) + Cantidad_Productos;

                                       //  Actualizar la la existencia y el disponible en la tabla de CAT_COM_PRODUCTOS
                                       Mi_SQL = " UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " ";
                                       Mi_SQL = Mi_SQL + " SET " + Cat_Tal_Refacciones.Campo_Existencia + " = " + Existencia + ",  ";
                                       Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Disponible + " = " + Disponible + ", ";
                                       Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Costo_Unitario + " = " + Precio_Cotizado + " ";

                                       Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Producto_ID + "'";

                                       //Ejecutar consulta
                                       Cmd.CommandText = Mi_SQL;
                                       Cmd.ExecuteNonQuery(); // Se ejecuta la operación 
                                }
                        }
                    }

                    Trans.Commit(); // Se ejecuta la transacciones
                    return Convert.ToInt64(No_Contrarecibo) + 1; // Se regresa el No. de Contra recibo
                }
                catch (SqlException Ex)
                {
                    Trans.Rollback();
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152)
                    {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 2627)
                    {
                        if (Ex.Message.IndexOf("PRIMARY") != -1)
                        {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                        }
                        else if (Ex.Message.IndexOf("UNIQUE") != -1)
                        {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        }
                        else
                        {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    }
                    else if (Ex.Number == 547)
                    {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 515)
                    {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    throw new Exception(Mensaje); // Se indica el mensaje 
                }
                finally
                {
                    Cn.Close();
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA CLASE:     Montos_Orden_Compra
            /// DESCRIPCION:            Se obtienen los montos de la orden de compra seleccionada por el usuario
            /// PARAMETROS :                                 
            /// CREO       :            Salvador Hernández Ramírez  
            /// FECHA_CREO :            16/Marzo/2011 
            /// MODIFICO          :     Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO    :     05/Julio/2012
            /// CAUSA_MODIFICACION:     Adecuacion a Taller Mecanico Mpal.
            ///*******************************************************************************/
            public static DataTable Montos_Orden_Compra(Cls_Ope_Tal_Contrarecibo_Negocio Datos)
            {
                String Mi_SQL = String.Empty; //Variable para las consultas
                try
                {
                    //Asignar consulta
                    Mi_SQL = "SELECT " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + ", " + Ope_Tal_Ordenes_Compra.Campo_Subtotal + ", ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Campo_Total_IEPS + ", " + Ope_Tal_Ordenes_Compra.Campo_Total_IVA + ", " + Ope_Tal_Ordenes_Compra.Campo_Total + " ";
                    Mi_SQL = Mi_SQL + "FROM " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " ";
                    Mi_SQL = Mi_SQL + "WHERE " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra;

                    //Entregar resultado
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                catch (DBConcurrencyException ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                finally
                {
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA CLASE:     Actualizar_Orden_Compra
            /// DESCRIPCION:            Se actualizan las ordenes de compra al estatus "RECIBIDA", y se resguarda la orden de compra
            /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene 
            ///                         los numero de orden de compra
            /// CREO       :            Salvador Hernández Ramírez
            /// FECHA_CREO :            16/Marzo/2011 
            /// MODIFICO          :     Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO    :     05/Julio/2012
            /// CAUSA_MODIFICACION:     Adecuacion a Taller Mecanico Mpal.
            ///*******************************************************************************/
            public static void Actualizar_Orden_Compra(Cls_Ope_Tal_Contrarecibo_Negocio Datos)
            {
                DataTable DataTable_Temporal = null;
                String Mi_SQL;
                DataTable_Temporal = Datos.P_Dt_Ordenes_Compra;

                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;

                try
                {
                    Mi_SQL = "UPDATE " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Ordenes_Compra.Campo_Estatus + "='SURTIDA'";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " =" + Datos.P_No_Orden_Compra;

                    //Ejecutar consulta
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar realizar las  transacción. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA CLASE:     Consultar_Datos_Generales_ContraRecibo
            /// DESCRIPCION:            Método utilizado para consultar 
            /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos para realizar la consulta
            ///                         
            /// CREO       :            Salvador Hernandez Ramirez
            /// FECHA_CREO :            14/Marzo/2011  
            /// MODIFICO          :     Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO    :     05/Julio/2012
            /// CAUSA_MODIFICACION:     Adecuacion a Taller Mecanico Mpal.
            ///*******************************************************************************/
            public static DataTable Consultar_Datos_Generales_ContraRecibo(Cls_Ope_Tal_Contrarecibo_Negocio Datos)
            {
                DataTable Dt_Datos_Generales = new DataTable();
                String Mi_SQL = null;

                // Consulta
                Mi_SQL = "SELECT Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_No_Factura_Interno + " as No_Contrarecibo";
                Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Proveedor_ID + " as No_Proveedor";
                Mi_SQL = Mi_SQL + ", Proveedores." + Cat_Com_Proveedores.Campo_Nombre + " as Proveedor";
                Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Fecha_Recepcion + " as Fecha_Recepcion";
                Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Fecha_Pago + " as Fecha_Pago";
                Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Usuario_Creo + " as Empleado_Almacen";
                Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Comentarios + " as Observaciones";
                Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_SubTotal_Sin_Impuesto + " as SubTotal";
                Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_IVA + " as IVA";
                Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Total + " as Importe";
                Mi_SQL = Mi_SQL + ", Ordenes_Compra." + Ope_Tal_Ordenes_Compra.Campo_Folio + " as Folio";

                Mi_SQL = Mi_SQL + ", (select REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Folio + " FROM ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQUISICIONES ";
                Mi_SQL = Mi_SQL + " WHERE REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = ";
                Mi_SQL = Mi_SQL + "Ordenes_Compra." + Ope_Tal_Ordenes_Compra.Campo_Lista_Requisiciones + ") AS Requisicion  ";

                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Facturas_Proveedores.Tabla_Ope_Tal_Facturas_Proveedores + " Facturas_P";
                Mi_SQL = Mi_SQL + " JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " Proveedores";
                Mi_SQL = Mi_SQL + " ON Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Proveedor_ID;
                Mi_SQL = Mi_SQL + " = Proveedores." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL = Mi_SQL + " JOIN " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " Ordenes_Compra";
                Mi_SQL = Mi_SQL + " ON Ordenes_Compra." + Ope_Tal_Ordenes_Compra.Campo_No_Factura_Interno; // Este es el No_Contra_Recibo
                Mi_SQL = Mi_SQL + " = Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_No_Factura_Interno;// Este es el No_Contra_Recibo
                Mi_SQL = Mi_SQL + " WHERE  Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_No_Factura_Interno + " = " + Datos.P_No_Contra_Recibo + "";
                
                Dt_Datos_Generales = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Dt_Datos_Generales;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA CLASE:     Consultar_Datos_Generales_ContraRecibo
            /// DESCRIPCION:            Método utilizado para consultar 
            /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos para realizar la consulta
            ///                         
            /// CREO       :            Salvador Hernandez Ramirez
            /// FECHA_CREO :            14/Marzo/2011  
            /// MODIFICO          :     Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO    :     05/Julio/2012
            /// CAUSA_MODIFICACION:     Adecuacion a Taller Mecanico Mpal.
            ///*******************************************************************************/
            public static DataTable Consultar_Facturas_ContraRecibo(Cls_Ope_Tal_Contrarecibo_Negocio Datos) {
                String Mi_SQL = null;
                // Consulta
                Mi_SQL = "SELECT  REG_FACTURAS." + Ope_Tal_Registro_Facturas.Campo_No_Contra_Recibo  + " No_Contrarecibo ";
                Mi_SQL = Mi_SQL + ", REG_FACTURAS." + Ope_Tal_Registro_Facturas.Campo_Factura_Proveedor + " as No_Factura_Proveedor ";
                Mi_SQL = Mi_SQL + ", REG_FACTURAS." + Ope_Tal_Registro_Facturas.Campo_Fecha_Factura + " as Fecha_Factura ";
                Mi_SQL = Mi_SQL + ", REG_FACTURAS." + Ope_Tal_Registro_Facturas.Campo_Importe_Factura + " as Importe ";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas + " REG_FACTURAS";
                Mi_SQL = Mi_SQL + " JOIN " + Ope_Tal_Facturas_Proveedores.Tabla_Ope_Tal_Facturas_Proveedores + " FACTURAS_P";
                Mi_SQL = Mi_SQL + " ON FACTURAS_P." + Ope_Tal_Facturas_Proveedores.Campo_No_Factura_Interno + " = ";
                Mi_SQL = Mi_SQL + " REG_FACTURAS." + Ope_Tal_Registro_Facturas.Campo_No_Contra_Recibo + "";
                Mi_SQL = Mi_SQL + " WHERE  FACTURAS_P." + Ope_Tal_Facturas_Proveedores.Campo_No_Factura_Interno + " = " + Datos.P_No_Contra_Recibo.ToString().Trim() + ""; // Este es el No_Contra_Recibo

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0]; //  Entregar resultado
            }

            ///*******************************************************************************
            /// NOMBRE DE LA CLASE:     Mostrar_Contrarecibo
            /// DESCRIPCION:            Consultar de la tabla facturas la informaciòn para generar el ContraRecibo
            /// PARAMETROS :            
            /// CREO       :            Salvador Hernández Ramírez
            /// FECHA_CREO :            16/Marzo/2011 
            /// MODIFICO          :     Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO    :     05/Julio/2012
            /// CAUSA_MODIFICACION:     Adecuacion a Taller Mecanico Mpal.
            ///*******************************************************************************/
            public static DataTable Consultar_Contrarecibo(Cls_Ope_Tal_Contrarecibo_Negocio Datos) {
                String Mi_SQL = String.Empty; // Variable para las consultas

                try {
                    // Consulta
                    Mi_SQL = "SELECT " + "Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_No_ContraRecibo + " as No_Contrarecibo";
                    Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Proveedor_ID + " as No_Proveedor";
                    Mi_SQL = Mi_SQL + ", Proveedores." + Cat_Com_Proveedores.Campo_Compañia + " as Proveedor";
                    Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Fecha_Recepcion + " as Fecha_Recepcion";
                    Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Fecha_Pago + " as Fecha_Pago";
                    Mi_SQL = Mi_SQL + ", Empleado." + Cat_Empleados.Campo_Nombre + "  +' '+";
                    Mi_SQL = Mi_SQL + " Empleado." + Cat_Empleados.Campo_Apellido_Paterno + "  +' '+";
                    Mi_SQL = Mi_SQL + " Empleado." + Cat_Empleados.Campo_Apellido_Materno + "  as Empleado_Almacen";
                    Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Comentarios + " as Observaciones";
                    Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Fecha_Factura + " as Fecha_Factura";
                    Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_No_Factura_Proveedor + " as Factura";
                    Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Total + " as Importe_Factura"; // Totales de la factura, la cual incluye varias ordenes de compra
                    Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_SubTotal_Con_Impuesto + "";
                    Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_SubTotal_Sin_Impuesto + "";
                    Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_IVA + " as IVA_Factura";
                    Mi_SQL = Mi_SQL + ", Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_IEPS + " as IEPS_Factura";
                    Mi_SQL = Mi_SQL + ", Ordenes_Compra." + Ope_Tal_Ordenes_Compra.Campo_Folio + " as Orden_Compra"; 
                    Mi_SQL = Mi_SQL + ", Ordenes_Compra." + Ope_Tal_Ordenes_Compra.Campo_Subtotal + " as Sup_Total_OC"; // Totales y Subtotales de la ordn de compra
                    Mi_SQL = Mi_SQL + ", Ordenes_Compra." + Ope_Tal_Ordenes_Compra.Campo_Total_IEPS + " as Total_IEPS_OC";
                    Mi_SQL = Mi_SQL + ", Ordenes_Compra." + Ope_Tal_Ordenes_Compra.Campo_Total_IVA + " as Total_IVA_OC";
                    Mi_SQL = Mi_SQL + ", Ordenes_Compra." + Ope_Tal_Ordenes_Compra.Campo_Total + " as Total_OC";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Facturas_Proveedores.Tabla_Ope_Tal_Facturas_Proveedores + " Facturas_P";
                    Mi_SQL = Mi_SQL + " JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " Proveedores";
                    Mi_SQL = Mi_SQL + " ON Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Proveedor_ID;
                    Mi_SQL = Mi_SQL + " = Proveedores." + Cat_Com_Proveedores.Campo_Proveedor_ID;
                    Mi_SQL = Mi_SQL + " JOIN " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " Ordenes_Compra";
                    Mi_SQL = Mi_SQL + " ON Ordenes_Compra." + Ope_Tal_Ordenes_Compra.Campo_No_Factura_Interno; // Este es el No_Contra_Recibo
                    Mi_SQL = Mi_SQL + " = Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_No_Factura_Interno;// Este es el No_Contra_Recibo
                    Mi_SQL = Mi_SQL + " JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " Empleado";
                    Mi_SQL = Mi_SQL + " on Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_Usuario_Creo + " ";
                    Mi_SQL = Mi_SQL + " = Empleado." + Cat_Empleados.Campo_Empleado_ID;
                    Mi_SQL = Mi_SQL + " WHERE  Facturas_P." + Ope_Tal_Facturas_Proveedores.Campo_No_ContraRecibo + "= '" + Datos.P_No_Contra_Recibo + "'";

                    //  Entregar resultado
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (SqlException ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                catch (DBConcurrencyException ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                finally
                {
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA CLASE:     Consulta_Montos_Orden_Compra
            /// DESCRIPCION:            Consulta los montos de la orden de compra seleccionada por el usuario
            /// PARAMETROS :            
            /// CREO       :            Salvador Hernández Ramírez
            /// FECHA_CREO :            02/Julio/2011 
            /// MODIFICO          :     Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO    :     05/Julio/2012
            /// CAUSA_MODIFICACION:     Adecuacion a Taller Mecanico Mpal.
            ///*******************************************************************************/
            public static DataTable Consulta_Montos_Orden_Compra(Cls_Ope_Tal_Contrarecibo_Negocio Datos) {
                String Mi_SQL = "";
                
                try {
                    // Consulta
                    Mi_SQL = "SELECT " + " ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Subtotal + " ";
                    Mi_SQL = Mi_SQL + ", ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Total_IVA + "";
                    Mi_SQL = Mi_SQL + ", ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Total + " ";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " ORDEN_COMPRA";
                    Mi_SQL = Mi_SQL + " WHERE  ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = ";
                    Mi_SQL = Mi_SQL + Datos.P_No_Orden_Compra + "";

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0]; //  Entregar resultado
                }
                catch (SqlException ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                catch (DBConcurrencyException ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                finally
                {
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA CLASE:     Consulta_Productos_Orden_Compra
            /// DESCRIPCION:            Consulta los productos de la orden de compra seleccionada por el usuarioa
            /// PARAMETROS :            
            /// CREO       :            Salvador Hernández Ramírez
            /// FECHA_CREO :            01/Julio/2011 
            /// MODIFICO          :     Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO    :     05/Julio/2012
            /// CAUSA_MODIFICACION:     Adecuacion a Taller Mecanico Mpal.
            ///*******************************************************************************/
            public static DataTable Consulta_Productos_Orden_Compra(Cls_Ope_Tal_Contrarecibo_Negocio Datos) {
                String Mi_SQL = String.Empty; // Variable para las consultas

                try {
                    // Consulta
                    Mi_SQL = "SELECT  REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID + " as PRODUCTO_ID";
                    Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Cantidad + "";
                    Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Precio_U_Sin_Imp_Cotizado + " as PRECIO_U ";
                    Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Subtota_Cotizado + " as PRECIO_AC ";
                    Mi_SQL = Mi_SQL + ", REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "";
                    Mi_SQL = Mi_SQL + ", REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Partida_ID + "";
                    Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID + "";

                    Mi_SQL = Mi_SQL + ",(select PRODUCTOS." + Cat_Tal_Refacciones.Campo_Clave + " from ";
                    Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTOS ";
                    Mi_SQL = Mi_SQL + " where REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID + " = PRODUCTOS.";
                    Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Refaccion_ID + ")as CLAVE";

                    if (Datos.P_Tipo_Articulo == "PRODUCTO")
                    {
                        Mi_SQL = Mi_SQL + ",(select PRODUCTOS." + Cat_Tal_Refacciones.Campo_Nombre + " from ";
                        Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTOS ";
                        Mi_SQL = Mi_SQL + " where REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID + " = PRODUCTOS.";
                        Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Refaccion_ID + ")as NOMBRE";

                        Mi_SQL = Mi_SQL + ",(select PRODUCTOS." + Cat_Tal_Refacciones.Campo_Descripcion + " from ";
                        Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTOS ";
                        Mi_SQL = Mi_SQL + " where REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID + " = PRODUCTOS.";
                        Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Refaccion_ID + ")as DESCRIPCION";
                    }
                    else if (Datos.P_Tipo_Articulo == "SERVICIO")
                    {
                        Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Nombre_Producto_Servicio + " AS  NOMBRE ";
                        Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Nombre_Giro + " AS  DESCRIPCION";
                    }

                    Mi_SQL = Mi_SQL + ", ( SELECT " + Cat_Com_Unidades.Campo_Abreviatura + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " WHERE ";
                    Mi_SQL = Mi_SQL + Cat_Com_Unidades.Campo_Unidad_ID + " = ( SELECT " + Cat_Com_Unidades.Campo_Unidad_ID + "  FROM ";
                    Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = ";
                    Mi_SQL = Mi_SQL + "REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID + " )) AS UNIDAD ";

                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " REQ_PRODUCTO, ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQUISICIONES ";
                    
                    Mi_SQL = Mi_SQL + " WHERE  REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = ";
                    Mi_SQL = Mi_SQL + " REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "";
                    Mi_SQL = Mi_SQL + " AND  REQ_PRODUCTO." + Ope_Tal_Requisiciones.Campo_No_Orden_Compra + " = ";
                    Mi_SQL = Mi_SQL + Datos.P_No_Orden_Compra;

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0]; //  Entregar resultado
                }
                catch (SqlException ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                catch (DBConcurrencyException ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                finally
                {
                }
            }


            ///*******************************************************************************
            /// NOMBRE DE LA CLASE:     Consulta_Documentos_Soporte
            /// DESCRIPCION:            Método utilizado para consultar los Documentos de Soporte de la tabla "CAT_TAL_DOCUMENTOS"
            /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos para realizar la consulta
            ///                         
            /// CREO       :            Salvador Hernandez Ramirez
            /// FECHA_CREO :            14/Marzo/2011  
            /// MODIFICO          :     Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO    :     05/Julio/2012
            /// CAUSA_MODIFICACION:     Adecuacion a Taller Mecanico Mpal.
            ///*******************************************************************************/
            public static DataTable Consulta_Documentos_Soporte(Cls_Ope_Tal_Contrarecibo_Negocio Datos)
            {
                DataTable Dt_Doc_Soporte = new DataTable();

                String Mi_SQL = null;
                DataSet Ds_Documentos_S = null;

                Mi_SQL = " SELECT " + "DOCUMENTOS_S." + Cat_Tal_Documentos.Campo_Documento_ID + "";
                Mi_SQL = Mi_SQL + ", DOCUMENTOS_S." + Cat_Tal_Documentos.Campo_Nombre + "";
                Mi_SQL = Mi_SQL + ", DOCUMENTOS_S." + Cat_Tal_Documentos.Campo_Comentarios + " as DESCRIPCION";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Documentos.Tabla_Cat_Tal_Documentos + " DOCUMENTOS_S";
                Mi_SQL = Mi_SQL + " WHERE  DOCUMENTOS_S." + Cat_Tal_Documentos.Campo_Tipo + " = '" + "SOPORTE'";

                if ((Datos.P_Documento_ID != null))
                {
                    Mi_SQL = Mi_SQL + " AND  DOCUMENTOS_S." + Cat_Tal_Documentos.Campo_Documento_ID + "= '" + Datos.P_Documento_ID + "'";
                }

                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Tal_Documentos.Campo_Nombre;

                Ds_Documentos_S = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                Dt_Doc_Soporte = Ds_Documentos_S.Tables[0];

                return Dt_Doc_Soporte;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA CLASE:     Mostrar_Contrarecibo
            /// DESCRIPCION:            Consultar de la tabla facturas la informaciòn para generar el ContraRecibo
            /// PARAMETROS :            
            /// CREO       :            Salvador Hernández Ramírez
            /// FECHA_CREO :            16/Marzo/2011 
            /// MODIFICO          :     Francisco A. Gallardo Castañeda
            /// FECHA_MODIFICO    :     05/Julio/2012
            /// CAUSA_MODIFICACION:     Adecuacion a Taller Mecanico Mpal.
            ///*******************************************************************************/
            public static DataTable Consultar_Documentos_Contrarecibo(Cls_Ope_Tal_Contrarecibo_Negocio Datos)
            {
                String Mi_SQL = String.Empty; // Variable para las consultas

                try
                {
                    DataTable Dt_Documentos = new DataTable();

                    Mi_SQL = "SELECT " + Cat_Tal_Documentos.Tabla_Cat_Tal_Documentos + "." + Cat_Tal_Documentos.Campo_Nombre + ", ";
                    Mi_SQL = Mi_SQL + Cat_Tal_Documentos.Tabla_Cat_Tal_Documentos + "." + Cat_Tal_Documentos.Campo_Comentarios + " ";
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Documentos.Tabla_Cat_Tal_Documentos + ", " + Ope_Tal_Det_Doc_Soporte.Tabla_Ope_Tal_Det_Doc_Soporte + "," + Ope_Tal_Facturas_Proveedores.Tabla_Ope_Tal_Facturas_Proveedores + " ";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Facturas_Proveedores.Tabla_Ope_Tal_Facturas_Proveedores + "." + Ope_Tal_Facturas_Proveedores.Campo_No_Factura_Interno + " = " + Datos.P_No_Contra_Recibo;
                    Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Det_Doc_Soporte.Tabla_Ope_Tal_Det_Doc_Soporte + "." + Ope_Tal_Det_Doc_Soporte.Campo_No_Factura_Interno + " ";
                    Mi_SQL = Mi_SQL + " = " + Ope_Tal_Facturas_Proveedores.Tabla_Ope_Tal_Facturas_Proveedores + "." + Ope_Tal_Facturas_Proveedores.Campo_No_Factura_Interno + "";
                    Mi_SQL = Mi_SQL + " AND " + Cat_Tal_Documentos.Tabla_Cat_Tal_Documentos + "." + Cat_Tal_Documentos.Campo_Documento_ID + " ";
                    Mi_SQL = Mi_SQL + " = " + Ope_Tal_Det_Doc_Soporte.Tabla_Ope_Tal_Det_Doc_Soporte + "." + Ope_Tal_Det_Doc_Soporte.Campo_Documento_ID + "";

                    Dt_Documentos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                    return Dt_Documentos;

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0]; //  Entregar resultado
                }
                catch (SqlException ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                catch (DBConcurrencyException ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error: " + ex.Message);
                }
                finally
                {
                }
            }

        #endregion

	}
}