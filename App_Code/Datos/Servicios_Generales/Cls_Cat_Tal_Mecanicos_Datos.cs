﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Catalogo_Mecanicos.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Cat_Tal_Mecanicos_Datos
/// </summary>
namespace JAPAMI.Taller_Mecanico.Catalogo_Mecanicos.Datos{
    public class Cls_Cat_Tal_Mecanicos_Datos {
        #region Metodos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Mecanico      
            ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a dar de
            ///                         Alta en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 10/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Alta_Mecanico(Cls_Cat_Tal_Mecanicos_Negocio Parametros) {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                String Mecanico_ID = Obtener_ID_Consecutivo(Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos, Cat_Tal_Mecanicos.Campo_Mecanico_ID, 5);
                try {
                    String Mi_SQL = "INSERT INTO " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " (" + Cat_Tal_Mecanicos.Campo_Mecanico_ID;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Mecanicos.Campo_Empleado_ID + ", " + Cat_Tal_Mecanicos.Campo_Estatus;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Mecanicos.Campo_Comentarios;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Mecanicos.Campo_Nivel_ID;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Mecanicos.Campo_Costo_Hora;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Mecanicos.Campo_Usuario_Creo + ", " + Cat_Tal_Mecanicos.Campo_Fecha_Creo;
                    Mi_SQL = Mi_SQL + ") VALUES ('" + Mecanico_ID + "', '" + Parametros.P_Empleado_ID + "','" + Parametros.P_Estatus + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Comentarios + "', '" + Parametros.P_Nivel + "', '" + Parametros.P_Costo_Hora + "', '" + Parametros.P_Usuario + "', GETDATE())"; 
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Trans.Commit();
                } catch (SqlException Ex) {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152) {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 2627) {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    } else if (Ex.Number == 547) {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 515) {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Mecanico
            ///DESCRIPCIÓN          : Modifica en la Base de Datos el registro indicado
            ///PARAMETROS           : 
            ///                     1.Parametros. Contiene los parametros que se van hacer la
            ///                       Modificación en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 10/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Modificar_Mecanico(Cls_Cat_Tal_Mecanicos_Negocio Parametros) {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try {
                    String Mi_SQL = "UPDATE " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos;
                    Mi_SQL = Mi_SQL + " SET " + Cat_Tal_Mecanicos.Campo_Empleado_ID + " = '" + Parametros.P_Empleado_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Mecanicos.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Mecanicos.Campo_Comentarios + " = '" + Parametros.P_Comentarios + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Mecanicos.Campo_Nivel_ID  + " = '" + Parametros.P_Nivel + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Mecanicos.Campo_Costo_Hora + " = '" + Parametros.P_Costo_Hora + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Mecanicos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Mecanicos.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Mecanicos.Campo_Mecanico_ID + " = '" + Parametros.P_Mecanico_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Trans.Commit();
                } catch (SqlException Ex) {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152) {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                    } else if (Ex.Number == 2627) {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    } else if (Ex.Number == 547) {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 515) {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar Modificar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Mecanicos
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 
            ///                     1.Parametros.Contiene los parametros que se van a utilizar para
            ///                       hacer la consulta de la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 10/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consultar_Mecanicos(Cls_Cat_Tal_Mecanicos_Negocio Parametros)  {
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try {
                    Mi_SQL = "SELECT MECANICOS." + Cat_Tal_Mecanicos.Campo_Mecanico_ID + " AS MECANICO_ID";
					Mi_SQL = Mi_SQL + ", MECANICOS." + Cat_Tal_Mecanicos.Campo_Empleado_ID + " AS EMPLEADO_ID";
                    Mi_SQL = Mi_SQL + ", MECANICOS." + Cat_Tal_Mecanicos.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL = Mi_SQL + ", MECANICOS." + Cat_Tal_Mecanicos.Campo_Comentarios + " AS COMENTARIOS";
                    Mi_SQL = Mi_SQL + ", MECANICOS." + Cat_Tal_Mecanicos.Campo_Nivel_ID + " AS NIVEL";
                    Mi_SQL = Mi_SQL + ", MECANICOS." + Cat_Tal_Mecanicos.Campo_Costo_Hora + " AS COSTO_HORA";
                    Mi_SQL = Mi_SQL + ", (EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + " +' - '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno;
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Nombre + ") AS NOMBRE_EMPLEADO";
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " MECANICOS";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS";
                    Mi_SQL = Mi_SQL + " ON MECANICOS." + Cat_Tal_Mecanicos.Campo_Empleado_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID;
                    if (!String.IsNullOrEmpty(Parametros.P_Mecanico_ID)) {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " MECANICOS." + Cat_Tal_Mecanicos.Campo_Mecanico_ID + " = '" + Parametros.P_Mecanico_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Estatus)) {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " MECANICOS." + Cat_Tal_Mecanicos.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_No_Empleado)) {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + " = '" + Parametros.P_No_Empleado + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Nombre_Empleado)) {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " (LTRIM(RTRIM(EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + "";
                        Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + "";
                        Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Nombre + ")) LIKE '%" + Parametros.P_Nombre_Empleado.Trim().ToUpper() + "%'";
                        Mi_SQL = Mi_SQL + " OR LTRIM(RTRIM(EMPLEADOS." + Cat_Empleados.Campo_Nombre + "";
                        Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + "";
                        Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + ")) LIKE '%" + Parametros.P_Nombre_Empleado.Trim().ToUpper() + "%')";
                    }
                    Mi_SQL = Mi_SQL + " ORDER BY MECANICO_ID ASC";
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Mecanico
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 
            ///                     1.Parametros.Contiene los parametros que se van a utilizar para
            ///                       hacer la consulta de la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 10/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Cls_Cat_Tal_Mecanicos_Negocio Consultar_Detalles_Mecanico(Cls_Cat_Tal_Mecanicos_Negocio Parametros)  {
                String Mi_SQL = null;
                Cls_Cat_Tal_Mecanicos_Negocio Obj_Cargado = new Cls_Cat_Tal_Mecanicos_Negocio();
                try {
                    Mi_SQL = "SELECT * FROM " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " WHERE " + Cat_Tal_Mecanicos.Campo_Mecanico_ID + " = '" + Parametros.P_Mecanico_ID + "'";
                    SqlDataReader Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    while (Reader.Read()) {
                        Obj_Cargado.P_Mecanico_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Mecanicos.Campo_Mecanico_ID].ToString())) ? Reader[Cat_Tal_Mecanicos.Campo_Mecanico_ID].ToString() : "";
                        Obj_Cargado.P_Empleado_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Mecanicos.Campo_Empleado_ID].ToString())) ? Reader[Cat_Tal_Mecanicos.Campo_Empleado_ID].ToString() : "";
                        Obj_Cargado.P_Estatus = (!String.IsNullOrEmpty(Reader[Cat_Tal_Mecanicos.Campo_Estatus].ToString())) ? Reader[Cat_Tal_Mecanicos.Campo_Estatus].ToString() : "";
                        Obj_Cargado.P_Comentarios = (!String.IsNullOrEmpty(Reader[Cat_Tal_Mecanicos.Campo_Comentarios].ToString())) ? Reader[Cat_Tal_Mecanicos.Campo_Comentarios].ToString() : "";
                        Obj_Cargado.P_Nivel = (!String.IsNullOrEmpty(Reader[Cat_Tal_Mecanicos.Campo_Nivel_ID].ToString())) ? Reader[Cat_Tal_Mecanicos.Campo_Nivel_ID].ToString() : "";
                        Obj_Cargado.P_Costo_Hora = (!String.IsNullOrEmpty(Reader[Cat_Tal_Mecanicos.Campo_Costo_Hora].ToString())) ? Convert.ToDouble(Reader[Cat_Tal_Mecanicos.Campo_Costo_Hora]) : (-1.0);    
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Obj_Cargado;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Eliminar_Mecanico
            ///DESCRIPCIÓN          : Elimina un Registro
            ///PARAMETROS           : 
            ///                     1.Parametros.Contiene los parametros que se van a utilizar para
            ///                       hacer la eliminacion de la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 10/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Eliminar_Mecanico(Cls_Cat_Tal_Mecanicos_Negocio Parametros) {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try {
                    String Mi_SQL = "DELETE FROM " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Mecanicos.Campo_Mecanico_ID + " = '" + Parametros.P_Mecanico_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Trans.Commit();
                } catch (SqlException Ex) {
                    if (Ex.Number == 547) {
                        Mensaje = "No se puede eliminar el registro, ya que está relacionado con datos. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar eliminar. Error: [" + Ex.Message + "]";
                    }
                    throw new Exception(Mensaje);
                } catch (Exception Ex) {
                    Mensaje = "Error al intentar eliminar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
            ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
            ///PARAMETROS:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Marzo/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID) {
                String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
                try {
                    String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                    Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals("")) {
                        Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                    }
                } catch (SqlException Ex) {
                    new Exception(Ex.Message);
                }
                return Id;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
            ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
            ///PARAMETROS:     
            ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
            ///             2. Longitud_ID. Longitud que tendra el ID. 
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Marzo/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID) {
                String Retornar = "";
                String Dato = "" + Dato_ID;
                for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++) {
                    Retornar = Retornar + "0";
                }
                Retornar = Retornar + Dato;
                return Retornar;
            }            

        #endregion
	}
}