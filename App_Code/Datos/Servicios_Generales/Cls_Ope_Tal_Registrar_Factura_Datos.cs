﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using System.Data.SqlClient;
using JAPAMI.Sessiones;
using JAPAMI.Bitacora_Eventos;
using System.Collections;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Taller_Mecanico.Registrar_Factura.Negocio;
using JAPAMI.Taller_Mecanico.Requisiciones.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Orden_Compra.Datos;

/// <summary>
/// Summary description for Cls_Ope_Alm_Registrar_Factura_Datos
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Registrar_Factura.Datos
{
    public class Cls_Ope_Tal_Registrar_Factura_Datos
    {     

        #region (Metodos)

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Ordenes_Compra
        /// DESCRIPCION:            Método utilizado para consultar las ordenes de compra que se encuentren en estatus "SURTIDA"
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene la información para realizar la consulta
        ///                         
        /// CREO       :            David Herrera Rincon
        /// FECHA_CREO :            22/Enero/2013  
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Ordenes_Compra(Cls_Ope_Tal_Registrar_Factura_Negocio Datos)
        {
            String Mi_SQL = String.Empty; //Variable para las consultas

            try
            {
                // Asignar consulta
                Mi_SQL = "SELECT " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + ", "; 
                Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Nombre + " AS PROVEEDOR, "; 
                Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Creo + " AS FECHA, ";
                Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Folio + ", "; 
                Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Estatus + ", ";
                Mi_SQL = Mi_SQL + "(select REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Folio + " FROM ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQUISICIONES ";
                Mi_SQL = Mi_SQL + " WHERE REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = ";
                Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Lista_Requisiciones + ") AS NO_REQUISICION, ";

                Mi_SQL = Mi_SQL + "(select REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Listado_Almacen+ " FROM ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQUISICIONES ";
                Mi_SQL = Mi_SQL + " WHERE REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = ";
                Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Lista_Requisiciones + ") LISTADO_ALMACEN, ";

                Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Total + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Tipo_Articulo + "";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + ", " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ";
                Mi_SQL = Mi_SQL + "WHERE " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Proveedor_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + " ";
                Mi_SQL = Mi_SQL + "AND " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Estatus + " = 'SURTIDA'";
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_No_Factura_Interno + " is null "; // Cuando no tiene asignado un numero de contra recibo,
                //Validamos los filtros
                if (Datos.P_No_Orden_Compra != null)
                {
                    Mi_SQL = Mi_SQL + "AND " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " like '%" + Datos.P_No_Orden_Compra + "%'";
                }

                if (Datos.P_No_Requisicion!= null)
                {
                    Mi_SQL = Mi_SQL + "AND " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Lista_Requisiciones + " like '%" + Datos.P_No_Requisicion+ "%'";
                }

                if (Datos.P_Proveedor_ID != null)
                {
                    Mi_SQL = Mi_SQL + "AND " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Proveedor_ID + " = '" + Datos.P_Proveedor_ID + "'";
                }

                if ((Datos.P_Fecha_Inicio_B != null) && (Datos.P_Fecha_Fin_B != null))
                {
                    Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Creo + " BETWEEN '" + Datos.P_Fecha_Inicio_B + "'" +
                  " AND '" + Datos.P_Fecha_Fin_B + "'";
                }

                Mi_SQL = Mi_SQL + " order by " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra; 

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Documentos_Soporte
        /// DESCRIPCION:            Método utilizado para consultar los Documentos de Soporte de la tabla "Cat_Tal_Documentos"
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene los datos para realizar la consulta
        ///                         
        /// CREO       :            David Herrera Rincon
        /// FECHA_CREO :            22/Enero/2013  
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Documentos_Soporte(Cls_Ope_Tal_Registrar_Factura_Negocio Datos)
        {
            //Declaracion de variables
            DataTable Dt_Doc_Soporte = new DataTable();
            String Mi_SQL = null;
            DataSet Ds_Documentos_S = null;
            //Realizamos la consulta
            Mi_SQL = " SELECT " + "DOCUMENTOS_S." + Cat_Tal_Documentos.Campo_Documento_ID+ "";
            Mi_SQL = Mi_SQL + ", DOCUMENTOS_S." + Cat_Tal_Documentos.Campo_Nombre + "";
            Mi_SQL = Mi_SQL + ", DOCUMENTOS_S." + Cat_Tal_Documentos.Campo_Comentarios + " as DESCRIPCION";
            Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Documentos.Tabla_Cat_Tal_Documentos + " DOCUMENTOS_S";
            Mi_SQL = Mi_SQL + " WHERE  DOCUMENTOS_S." + Cat_Tal_Documentos.Campo_Tipo + " = '" + "SOPORTE'";
            //Validamos los filtros
            if ((Datos.P_Documento_ID != null))
            {
                Mi_SQL = Mi_SQL + " AND  DOCUMENTOS_S." + Cat_Tal_Documentos.Campo_Documento_ID + "= '" + Datos.P_Documento_ID +"'";
            }

            Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Tal_Documentos.Campo_Nombre;
            //Hacemos la consulta y entregamos el valor
            Ds_Documentos_S = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            Dt_Doc_Soporte = Ds_Documentos_S.Tables[0];
           
            return Dt_Doc_Soporte;
        }


        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Proveedores
        /// DESCRIPCION:            Consultar los datos de los proveedores
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene 
        ///                         los datos para la busqueda
        /// CREO       :            David Herrera Rincon
        /// FECHA_CREO :            22/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Proveedores(Cls_Ope_Tal_Registrar_Factura_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty; //variable apra las consultas

            try
            {
                //Consulta
                Mi_SQL = " SELECT DISTINCT " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Compañia + " ";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + ", " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Proveedor_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + " ";
                Mi_SQL = Mi_SQL + " and " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + "." + Ope_Tal_Ordenes_Compra.Campo_Estatus + " = 'SURTIDA' ";
                Mi_SQL = Mi_SQL + "ORDER BY " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Compañia; // Ordenamiento

                // Resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///******************************************************************************* // Este se ocupa en la clase "ContraREcibo_Datos"
        /// NOMBRE DE LA CLASE:     Guardar_Registro_Factura
        /// DESCRIPCION:            Se guardaa la información que forma parte de la factura
        /// PARAMETROS :                                 
        /// CREO       :            David Herrera Rincón  
        /// FECHA_CREO :            22/Enero/2013 
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static void Guardar_Registro_Factura(Cls_Ope_Tal_Registrar_Factura_Negocio Datos)
        {
            //Declaracion de variables
            String Mensaje = "";
            String Mi_SQL = String.Empty;
            Object Marbete, Factura, Registro;
            Int64 No_Marbete, Factura_ID, Registro_ID = 0;
            Double Importe_Total_Facturas =0;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            SqlDataAdapter da = new SqlDataAdapter(Cmd);
            Object Aux = null;
            Int64 Consecutivo = 0;

            try
            {       
                // SE GUARDA EL RECIBO 
                // SE ACTUALIZA LA ORDEN DE COMPRA (Se le asigna su numero de factura interno, registrada, fecha y usuario modificó)
                //Consulta para colocar el numero de factura a la orden de compra
                Mi_SQL = "UPDATE " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " ";
                Mi_SQL = Mi_SQL + "SET " + Ope_Tal_Ordenes_Compra.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ";
                Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Campo_Fecha_Modifico + " = GETDATE(), ";                
                Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Campo_Estatus + " = 'SURTIDA_FACTURA' ";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.Trim() + "";

                //Ejecutar consulta
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery(); // Se ejecuta la operación 

                if (Datos.P_Tipo_Articulo.Trim() == "SERVICIO")   // Si es una requisicion de servicios
                { 
                    Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " ";
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Requisiciones.Campo_Estatus + " = 'SERVICIOS_SURTIDA' ";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Requisiciones.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.Trim() + "";
                    
                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery(); // Se ejecuta la operación 


                    // Se consulta el Numero de Requisición
                    Mi_SQL = "SELECT distinct " + Ope_Tal_Requisiciones.Campo_Requisicion_ID ;
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Requisiciones.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.Trim() + "";

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Object Req = Cmd.ExecuteScalar();

                    //Verificar si no es nulo
                    if (Req != null && Convert.IsDBNull(Req) == false)
                        Datos.P_No_Requisicion = Convert.ToString(Req) + 1;

                    // Se Guarda el Historial de la requisición
                    //Cls_Ope_Tal_Requisiciones_Negocio Requisiciones = new Cls_Ope_Tal_Requisiciones_Negocio();
                    //Requisiciones.Registrar_Historial("SERVICIOS_SURTIDA", Datos.P_No_Requisicion.ToString().Trim());
                    // Se Guarda el Historial de la requisición
                    //Asignar consulta para el Maximo ID
                    Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Tal_Req_Historial.Campo_No_Historial + "), 0) FROM " + Ope_Tal_Req_Historial.Tabla_Ope_Tal_Req_Historial;

                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Aux = Cmd.ExecuteScalar();

                    //Verificar si no es nulo
                    if (Convert.IsDBNull(Aux) == false)
                        Consecutivo = Convert.ToInt64(Aux) + 1;
                    else
                        Consecutivo = 1;
                    Mi_SQL = "INSERT INTO " + Ope_Tal_Req_Historial.Tabla_Ope_Tal_Req_Historial + " (" +
                           Ope_Tal_Req_Historial.Campo_No_Historial + "," +
                           Ope_Tal_Req_Historial.Campo_No_Requisicion + "," +
                           Ope_Tal_Req_Historial.Campo_Estatus + "," +
                           Ope_Tal_Req_Historial.Campo_Fecha + "," +
                           Ope_Tal_Req_Historial.Campo_Empleado + "," +
                           Ope_Tal_Req_Historial.Campo_Usuario_Creo + "," +
                           Ope_Tal_Req_Historial.Campo_Fecha_Creo + ") VALUES (" +
                           Consecutivo + ", " +
                           Datos.P_No_Requisicion.ToString().Trim() + ", 'SERVICIOS_SURTIDA'," +                           
                           "GETDATE(), '" +
                           Cls_Sessiones.Nombre_Empleado + "', '" +
                           Cls_Sessiones.Nombre_Empleado + "', " +
                           "GETDATE())";
                    //Ejecutar consulta
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                // SE GUARDAN LOS DOCUMENTOS SOPORTE
                if (Datos.P_Dt_Documentos_Soporte != null)
                {
                        //Asignar consulta para el maximo Marbete de la tabla Ope_Tal_Det_Doc_Soporte
                        Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Tal_Det_Doc_Soporte.Campo_Marbete + "), 0) ";
                        Mi_SQL = Mi_SQL + "FROM " + Ope_Tal_Det_Doc_Soporte.Tabla_Ope_Tal_Det_Doc_Soporte;

                        //Ejecutar consulta
                        Cmd.CommandText = Mi_SQL;
                        Marbete = Cmd.ExecuteScalar();

                        //Verificar si no es nulo
                        if (Marbete != null && Convert.IsDBNull(Marbete) == false)
                            No_Marbete = Convert.ToInt64(Marbete) + 1;
                        else
                            No_Marbete = 1;

                    //Ciclo para colocar el numero de factura a la orden de compra
                    for (int Cont = 0; Cont < Datos.P_Dt_Documentos_Soporte.Rows.Count; Cont++)
                    {
                        //Asignar consulta para ingresar la factura
                        Mi_SQL = "INSERT INTO " + Ope_Tal_Det_Doc_Soporte.Tabla_Ope_Tal_Det_Doc_Soporte + " (";
                        Mi_SQL = Mi_SQL + Ope_Tal_Det_Doc_Soporte.Campo_Documento_ID + ", ";
                        Mi_SQL = Mi_SQL + Ope_Tal_Det_Doc_Soporte.Campo_No_Factura_Interno + ", "; // Este es el No_Contra_Recibo
                        Mi_SQL = Mi_SQL + Ope_Tal_Det_Doc_Soporte.Campo_Usuario_Creo + ", ";
                        Mi_SQL = Mi_SQL + Ope_Tal_Det_Doc_Soporte.Campo_Fecha_Creo + ", ";
                        Mi_SQL = Mi_SQL + Ope_Tal_Det_Doc_Soporte.Campo_Marbete + " ";
                        //Mi_SQL = Mi_SQL + Ope_Tal_Det_Doc_Soporte.Campo_No_Orden_Compra + " ";
                        Mi_SQL = Mi_SQL + ") VALUES('" + Datos.P_Dt_Documentos_Soporte.Rows[Cont]["DOCUMENTO_ID"].ToString().Trim() +"', ";
                        Mi_SQL = Mi_SQL +  "NULL, '";
                        Mi_SQL = Mi_SQL + Datos.P_Usuario_Creo + "', ";
                        Mi_SQL = Mi_SQL + " GETDATE(), ";
                        Mi_SQL = Mi_SQL + No_Marbete + ") ";
                        //Mi_SQL = Mi_SQL + Datos.P_No_Orden_Compra + ")";
                        
                        //Ejecutar consulta
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery(); // Se ejecuta la operación
                        No_Marbete = No_Marbete + 1; // Se incrementa
                    }
                }

                // SE GUARDAN LAS FACTURAS
                if (Datos.P_Dt_Facturas_Proveedor != null)
                {
                        //Asignar consulta para el maximo Marbete de la tabla Ope_Tal_Det_Doc_Soporte
                        Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Tal_Registro_Facturas.Campo_Factura_ID + "), 0) ";
                        Mi_SQL = Mi_SQL + "FROM " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas;

                        //Ejecutar consulta
                        Cmd.CommandText = Mi_SQL;
                        Factura = Cmd.ExecuteScalar();

                        //Verificar si no es nulo
                        if (Factura != null && Convert.IsDBNull(Factura) == false)
                            Factura_ID = Convert.ToInt64(Factura) + 1;
                        else
                            Factura_ID = 1;

                    //Ciclo para colocar los numeros de factura
                    for (int Cont = 0; Cont < Datos.P_Dt_Facturas_Proveedor.Rows.Count; Cont++)
                    {
                        //Asignar consulta para ingresar la factura
                        Mi_SQL = "INSERT INTO " + Ope_Tal_Registro_Facturas.Tabla_Ope_Tal_Registro_Facturas + " (";
                        Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_Factura_ID + ", ";
                        Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_Factura_Proveedor + ", ";
                        Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_No_Contra_Recibo + ", ";
                        Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_Importe_Factura + ", ";
                        Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_Iva_Factura + ", ";
                        Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_Total_Factura + ", ";
                        Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_Fecha_Factura + ", ";
                        Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_Usuario_Creo + ", ";
                        Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_Fecha_Creo + ", ";
                        Mi_SQL = Mi_SQL + Ope_Tal_Registro_Facturas.Campo_No_Orden_Compra + ") ";
                        Mi_SQL = Mi_SQL + "VALUES(" + Factura_ID + ", '";
                        Mi_SQL = Mi_SQL + Datos.P_Dt_Facturas_Proveedor.Rows[Cont]["NO_FACTURA_PROVEEDOR"].ToString().Trim() + "', ";
                        Mi_SQL = Mi_SQL + "NULL, ";
                        Mi_SQL = Mi_SQL + Datos.P_Dt_Facturas_Proveedor.Rows[Cont]["IMPORTE_FACTURA"].ToString().Trim().Replace(",","") + ", ";
                        Mi_SQL = Mi_SQL + Datos.P_Dt_Facturas_Proveedor.Rows[Cont]["IVA_FACTURA"].ToString().Trim().Replace(",", "") + ", ";
                        Mi_SQL = Mi_SQL + Datos.P_Dt_Facturas_Proveedor.Rows[Cont]["TOTAL_FACTURA"].ToString().Trim().Replace(",", "") + ", '";
                        Mi_SQL = Mi_SQL + String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Datos.P_Dt_Facturas_Proveedor.Rows[Cont]["FECHA_FACTURA"].ToString().Trim())) + "', '";                        
                        //Mi_SQL = Mi_SQL + Datos.P_Dt_Facturas_Proveedor.Rows[Cont]["FECHA_FACTURA"].ToString().Trim() + "', '";
                        Mi_SQL = Mi_SQL + Datos.P_Usuario_Creo + "', ";
                        Mi_SQL = Mi_SQL + " GETDATE(), ";
                        Mi_SQL = Mi_SQL + Datos.P_No_Orden_Compra + ") ";
                        // Se calcula el importe total de las facturas, dato utilizado por si se va a diminuir el monto de las facturas o se disminulle el monto por cada producto
                        Importe_Total_Facturas= Importe_Total_Facturas + Convert.ToDouble(Datos.P_Dt_Facturas_Proveedor.Rows[Cont]["IMPORTE_FACTURA"].ToString().Trim());
                                                
                        //Ejecutar consulta
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery(); // Se ejecuta la operación 
                        Factura_ID = Factura_ID + 1;
                    }
                }

                if (Datos.P_Tipo_Orden_Compra.Trim() == "TRANSITORIA") // SI NO ES UNA REQUISICION DE LISTADO DE STOCK
                {
                       if (Datos.P_Tipo_Articulo.Trim() != "SERVICIO")   // Si no es una requisicion de servicios y es transitoria, se  agegan los productos a la tabla
                        {
                            // SE GUARDAN LOS PRODUCTOS DE LA ORDEN DE COMPRA
                            if (Datos.P_Dt_Productos_OC != null)
                            {
                                //Ciclo para colocar los numeros de factura
                                for (int Cont = 0; Cont < Datos.P_Dt_Productos_OC.Rows.Count; Cont++)
                                {
                                    //// Asignar consulta para ingresar la factura
                                    //Mi_SQL = "INSERT INTO " + Ope_Tal_Ordenes_Compra_Productos.Tabla_Ope_Tal_Ordenes_Compra_Productos + " (";
                                    //Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra_Productos.Campo_No_Orden_Compra + ", ";
                                    //Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra_Productos.Campo_Refaccion_ID + ", ";
                                    //Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra_Productos.Campo_Resguardo + ", ";
                                    //Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra_Productos.Campo_Custodia + ", ";
                                    //Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra_Productos.Campo_Recibo + ", ";
                                    //Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra_Productos.Campo_Unidad + ", ";
                                    //Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra_Productos.Campo_Totalidad + ", ";
                                    //Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra_Productos.Campo_Usuario_Creo + ", ";
                                    //Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra_Productos.Campo_Fecha_Creo + ") ";
                                    //Mi_SQL = Mi_SQL + "VALUES( ";
                                    //Mi_SQL = Mi_SQL + Datos.P_No_Orden_Compra + ", '";
                                    //Mi_SQL = Mi_SQL + Datos.P_Dt_Productos_OC.Rows[Cont]["PRODUCTO_ID"].ToString().Trim() + "', '";
                                    //Mi_SQL = Mi_SQL + Datos.P_Dt_Productos_OC.Rows[Cont]["RESGUARDO"].ToString().Trim() + "', '";
                                    //Mi_SQL = Mi_SQL + Datos.P_Dt_Productos_OC.Rows[Cont]["CUSTODIA"].ToString().Trim() + "', '";
                                    //Mi_SQL = Mi_SQL + Datos.P_Dt_Productos_OC.Rows[Cont]["RECIBO"].ToString().Trim() + "', '";
                                    //Mi_SQL = Mi_SQL + Datos.P_Dt_Productos_OC.Rows[Cont]["UNIDAD"].ToString().Trim() + "', '";
                                    //Mi_SQL = Mi_SQL + Datos.P_Dt_Productos_OC.Rows[Cont]["TOTALIDAD"].ToString().Trim() + "', '";
                                    //Mi_SQL = Mi_SQL + Datos.P_Usuario_Creo + "', ";
                                    //Mi_SQL = Mi_SQL + " GETDATE())";

                                    ////String Agregar_Marbete = Convert.ToString(No_Marbete);
                                    //// Se da de alta la operación en el método "Alta_Bitacora"
                                    ////Cls_Bitacora.Alta_Bitacora(Cls_Sessiones.Empleado_ID, Ope_Bitacora.Accion_Alta, "Frm_Ope_Alm_Elaborar_Contrarecibo.aspx", Agregar_Marbete, Mi_SQL);

                                    ////Ejecutar consulta
                                    //Cmd.CommandText = Mi_SQL;
                                    //Cmd.ExecuteNonQuery(); // Se ejecuta la operación 
                                    Registro_ID = Registro_ID + 1; // Se incrementa

                                    String Recibo = "";
                                    String Resguardo = "";
                                    String Resguardado = "";

                                    Resguardo = "" + Datos.P_Dt_Productos_OC.Rows[Cont]["RESGUARDO"].ToString().Trim();
                                    Recibo = "" + Datos.P_Dt_Productos_OC.Rows[Cont]["CUSTODIA"].ToString().Trim();

                                    if ((Resguardo == "SI") | (Recibo == "SI"))
                                        Resguardado = "SI";
                                    else
                                        Resguardado = "NO";

                                    Mi_SQL = " UPDATE " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion;
                                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Req_Refaccion.Campo_Resguardado + " ='" + Resguardado + "'";
                                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Req_Refaccion.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra.Trim();
                                    Mi_SQL = Mi_SQL + " and " + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID + " = '" + Datos.P_Dt_Productos_OC.Rows[Cont]["PRODUCTO_ID"].ToString().Trim() + "'";

                                    //Ejecutar consulta
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery(); // Se ejecuta la operación 
                                }
                            }
                        }
                       
                        // Se realiza el recorrido de la tabla para actualizar su precio unitario de los productos
                        for( int j=0; j< Datos.P_Dt_Productos_OC.Rows.Count; j++){

                            DataTable Dt_Aux_Precio = new DataTable(); //  Tabla que contendra los productos que se deben actualizar
                            Double Precio_Actualizado = 0;

                            // Consulta para obtener los  montos 
                            Mi_SQL = "SELECT  " + Ope_Tal_Req_Refaccion.Campo_Precio_U_Sin_Imp_Cotizado + " as PRECIO_PRODUCTO";
                            Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion;
                            Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_No_Orden_Compra;
                            Mi_SQL = Mi_SQL + " = '" + Datos.P_No_Orden_Compra.Trim() + "'";
                            Mi_SQL = Mi_SQL + " and " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID;
                            Mi_SQL = Mi_SQL + " = '" + Datos.P_Dt_Productos_OC.Rows[j]["PRODUCTO_ID"].ToString().Trim() + "'";

                            //Ejecutar consulta
                            Dt_Aux_Precio = new DataTable();
                            Cmd.CommandText = Mi_SQL;
                            da.Fill(Dt_Aux_Precio);

                            // Verificar si es nulL
                            if (Convert.IsDBNull(Dt_Aux_Precio.Rows[0]["PRECIO_PRODUCTO"]) == false)
                                Precio_Actualizado = Convert.ToDouble(Dt_Aux_Precio.Rows[0]["PRECIO_PRODUCTO"].ToString().Trim());


                            if (Datos.P_Tipo_Articulo.Trim() == "SERVICIO")   // Si  es una requisicion de servicios
                            {
                                //  Actualizar el precio de la tabla  CAT_COM_SERVICIOS
                                Mi_SQL = " UPDATE " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + " ";
                                Mi_SQL = Mi_SQL + " SET " + Cat_Com_Servicios.Campo_Costo + " = " + Precio_Actualizado + " ";
                                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Servicios.Campo_Servicio_ID + " = '" + Datos.P_Dt_Productos_OC.Rows[j]["PRODUCTO_ID"].ToString().Trim() + "'";
                            }
                            else  //  Entonces es una requisición de productos
                            {
                                //  Actualizar el precio de la tabla Cat_Tal_Refacciones
                                Mi_SQL = " UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " ";
                                Mi_SQL = Mi_SQL + " SET " + Cat_Tal_Refacciones.Campo_Costo_Unitario + " = " + Precio_Actualizado + " ";
                                //Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Costo_Promedio + " = " + Precio_Actualizado + " ";
                                Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Datos.P_Dt_Productos_OC.Rows[j]["PRODUCTO_ID"].ToString().Trim() + "'";
                            }

                            //Ejecutar consulta
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery(); // Se ejecuta la operación 
                        }                    
                }
                else if (Datos.P_Tipo_Orden_Compra.Trim() == "LISTADO_ALMACEN")   // SE AUMENTAN LOS PRODUCTOS en la tabla Cat_Tal_Refacciones
                {
                    DataTable Dt_Productos_Actualzar = new DataTable();    // Se Crea la tabla utilziada para actualizar los productos

                    if (Datos.P_Dt_Actualizar_Productos.Rows.Count > 0) // Si hay productos que deben ser actualziados
                    {
                        Dt_Productos_Actualzar = Datos.P_Dt_Actualizar_Productos;

                        String Producto_ID = "";
                        Int64 Cantidad_Productos = 0;
                        Int64 Existencia =0;
                        Int64 Disponible =0;
                        Double Precio_Cotizado = 0;

                            for (int i=0; i< Dt_Productos_Actualzar.Rows.Count ; i++){

                                if (Dt_Productos_Actualzar.Rows[i]["PRODUCTO_ID"].ToString().Trim() != "")
                                    Producto_ID = Dt_Productos_Actualzar.Rows[i]["PRODUCTO_ID"].ToString().Trim();
                                else
                                    Producto_ID = "";

                                if (Dt_Productos_Actualzar.Rows[i]["CANTIDAD"].ToString().Trim() != "")
                                    Cantidad_Productos = Convert.ToInt64(Dt_Productos_Actualzar.Rows[i]["CANTIDAD"].ToString().Trim());
                                else
                                    Cantidad_Productos = 0;

                                if (Dt_Productos_Actualzar.Rows[i]["PRECIO_U"].ToString().Trim() != "")
                                    Precio_Cotizado = double.Parse(Dt_Productos_Actualzar.Rows[i]["PRECIO_U"].ToString().Trim());
                                else
                                    Precio_Cotizado = 0;

                                   DataTable Dt_Existencia_Productos = new DataTable(); // Tabla creada para consultar las existencias y disponible de los prioductos

                                   // Se Consulta la existencia  y disponibilidad de cada producto que pertenezca a la orden de compra
                                   Mi_SQL = " SELECT  " + Cat_Tal_Refacciones.Campo_Refaccion_ID;
                                   Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Existencia;
                                   Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Disponible;
                                   Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Costo_Unitario;
                                   Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
                                   Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Refaccion_ID;
                                   Mi_SQL = Mi_SQL + " = '" + Producto_ID + "'";

                                   Dt_Existencia_Productos = new DataTable();
                                   Cmd.CommandText = Mi_SQL;
                                   da.Fill(Dt_Existencia_Productos);

                                   if (Convert.IsDBNull(Dt_Existencia_Productos.Rows[0][1]) != false) // Si no hay Existencias
                                       Existencia = Cantidad_Productos;
                                   else
                                       Existencia = Convert.ToInt64(Dt_Existencia_Productos.Rows[0][1]) + Cantidad_Productos;

                                   if (Convert.IsDBNull(Dt_Existencia_Productos.Rows[0][2]) != false) // Si no hay Disponible
                                       Disponible = Cantidad_Productos;
                                   else
                                       Disponible = Convert.ToInt64(Dt_Existencia_Productos.Rows[0][2]) + Cantidad_Productos;

                                   //  Actualizar la la existencia y el disponible en la tabla de Cat_Tal_Refacciones
                                   Mi_SQL = " UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " ";
                                   Mi_SQL = Mi_SQL + " SET " + Cat_Tal_Refacciones.Campo_Existencia + " = " + Existencia + ",  ";
                                   Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Disponible + " = " + Disponible + ", ";
                                   Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Costo_Unitario + " = " + Precio_Cotizado + " ";
                                   //Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Costo_Promedio + " = " + Precio_Cotizado;

                                   Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Producto_ID + "'";

                                   //Ejecutar consulta
                                   Cmd.CommandText = Mi_SQL;
                                   Cmd.ExecuteNonQuery(); // Se ejecuta la operación 
                            }
                    }
                }
                Cls_Ope_Tal_Orden_Compra_Datos.Registrar_Movimiento_Historial(Datos.P_No_Orden_Compra, Datos.P_Usuario_Creo, "SURTIDA_FACTURA", ref Cmd);
                Trans.Commit(); // Se ejecuta la transacciones                
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave,  Vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Intente nuevamente por favor. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje); // Se indica el mensaje 
            }
            finally
            {
                Cn.Close();
            }
        }
        
        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Montos_Orden_Compra
        /// DESCRIPCION:            Se obtienen los montos de la orden de compra seleccionada por el usuario
        /// PARAMETROS :                                 
        /// CREO       :            David Herrera Rincon  
        /// FECHA_CREO :            22/Enero/2013
        /// MODIFICO          :     
        /// FECHA_MODIFICO    :     
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Montos_Orden_Compra(Cls_Ope_Tal_Registrar_Factura_Negocio Datos)
        {
            String Mi_SQL = String.Empty; //Variable para las consultas

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + ", " + Ope_Tal_Ordenes_Compra.Campo_Subtotal + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Ordenes_Compra.Campo_Total_IEPS + ", " + Ope_Tal_Ordenes_Compra.Campo_Total_IVA + ", " + Ope_Tal_Ordenes_Compra.Campo_Total + " ";
                Mi_SQL = Mi_SQL + "FROM " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " ";
                Mi_SQL = Mi_SQL + "WHERE " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = " + Datos.P_No_Orden_Compra;

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        } 

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Montos_Orden_Compra
        /// DESCRIPCION:            Consulta los montos de la orden de compra seleccionada por el usuario
        /// PARAMETROS :            
        /// CREO       :            David Herrera Rincon
        /// FECHA_CREO :            23/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Montos_Orden_Compra(Cls_Ope_Tal_Registrar_Factura_Negocio Datos)
        {
            String Mi_SQL = "";
            
            try
            {
                // Consulta
                Mi_SQL = "SELECT " + " ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Subtotal + " ";
                Mi_SQL = Mi_SQL + ", ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Total_IVA + "";
                Mi_SQL = Mi_SQL + ", ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_Total + " ";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " ORDEN_COMPRA";
                Mi_SQL = Mi_SQL + " WHERE  ORDEN_COMPRA." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = ";
                Mi_SQL = Mi_SQL + Datos.P_No_Orden_Compra + "";

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0]; //  Entregar resultado
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }


        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Productos_Orden_Compra
        /// DESCRIPCION:            Consulta los productos de la orden de compra seleccionada por el usuarioa
        /// PARAMETROS :            
        /// CREO       :            David Herrera Rincon
        /// FECHA_CREO :            23/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Productos_Orden_Compra(Cls_Ope_Tal_Registrar_Factura_Negocio Datos)
        {
            String Mi_SQL = String.Empty; // Variable para las consultas

            try
            {
                // Consulta
                Mi_SQL = "SELECT  REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID + " as PRODUCTO_ID";
                Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Cantidad + "";
                Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Precio_U_Sin_Imp_Cotizado + " as PRECIO_U ";
                Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Subtota_Cotizado + " as PRECIO_AC ";
                Mi_SQL = Mi_SQL + ", REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "";
                Mi_SQL = Mi_SQL + ", REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Dependencia_ID + "";
                Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Partida_ID + "";
                Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID + "";

                Mi_SQL = Mi_SQL + ",(select PRODUCTOS." + Cat_Tal_Refacciones.Campo_Clave + " from ";
                Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTOS ";
                Mi_SQL = Mi_SQL + " where REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID + " = PRODUCTOS.";
                Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Refaccion_ID + ")as CLAVE";

                if (Datos.P_Tipo_Articulo == "PRODUCTO")
                {
                    Mi_SQL = Mi_SQL + ",(select PRODUCTOS." + Cat_Tal_Refacciones.Campo_Nombre + " from ";
                    Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTOS ";
                    Mi_SQL = Mi_SQL + " where REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID + " = PRODUCTOS.";
                    Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Refaccion_ID + ")as NOMBRE";

                    Mi_SQL = Mi_SQL + ",(select PRODUCTOS." + Cat_Tal_Refacciones.Campo_Descripcion + " from ";
                    Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTOS ";
                    Mi_SQL = Mi_SQL + " where REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID + " = PRODUCTOS.";
                    Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Refaccion_ID + ")as DESCRIPCION";
                }
                else if (Datos.P_Tipo_Articulo == "SERVICIO")
                {
                    Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Nombre_Producto_Servicio + " AS  NOMBRE ";
                    Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Nombre_Giro + " AS  DESCRIPCION";
                }

                Mi_SQL = Mi_SQL + ", ( SELECT " + Cat_Com_Unidades.Campo_Abreviatura + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " WHERE ";
                Mi_SQL = Mi_SQL + Cat_Com_Unidades.Campo_Unidad_ID + " = ( SELECT " + Cat_Com_Unidades.Campo_Unidad_ID + "  FROM ";
                Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = REQ_PRODUCTO.";
                Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID + " )) AS UNIDAD ";

                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " REQ_PRODUCTO, ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQUISICIONES ";
                
                Mi_SQL = Mi_SQL + " WHERE  REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = ";
                Mi_SQL = Mi_SQL + " REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "";
                Mi_SQL = Mi_SQL + " AND  REQ_PRODUCTO." + Ope_Tal_Requisiciones.Campo_No_Orden_Compra + " = ";
                Mi_SQL = Mi_SQL + Datos.P_No_Orden_Compra;

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0]; //  Entregar resultado
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        #endregion
    }
}
