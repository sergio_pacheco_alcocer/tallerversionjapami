﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Manejo_Vales_Express.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Ope_Tal_Manejo_Vales_Express_Datos
/// </summary>
namespace JAPAMI.Taller_Mecanico.Operacion_Manejo_Vales_Express.Datos
{
    public class Cls_Ope_Tal_Manejo_Vales_Express_Datos
    {
        #region Metodos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Vales_Express
            ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
            ///PARAMETROS           : 1.- Parametros. Contiene los parametros que se van a dar de
            ///                           alta en la Base de Datos.
            ///CREO                 : Salvador Vázquez Camacho
            ///FECHA_CREO           : 25/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Cls_Ope_Tal_Manejo_Vales_Express_Negocio Alta_Vales_Express(Cls_Ope_Tal_Manejo_Vales_Express_Negocio Parametros)
            {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                Int32 ID_Consecutivo = Int32.Parse(Obtener_ID_Consecutivo(Ope_Tal_Man_Val_Ref.Tabla_Ope_Tal_Man_Val_Ref, Ope_Tal_Man_Val_Ref.Campo_No_Registro, 10));
                Parametros.P_No_Registro = ID_Consecutivo.ToString();
                try
                {
                    String Mi_SQL = "INSERT INTO " + Ope_Tal_Man_Val_Ref.Tabla_Ope_Tal_Man_Val_Ref;
                    Mi_SQL = Mi_SQL + " (" + Ope_Tal_Man_Val_Ref.Campo_No_Registro;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Vehiculo_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Usuario_Entrega_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Usuario_Recibe_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Dependencia_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Observaciones;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Estatus;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Usuario_Creo;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Fecha_Creo;
                    Mi_SQL = Mi_SQL + ") VALUES (" + Parametros.P_No_Registro;
                    Mi_SQL = Mi_SQL + " , '" + Parametros.P_Vehiculo_ID;
                    Mi_SQL = Mi_SQL + "', '" + Parametros.P_Usuario_Entrega_ID;
                    Mi_SQL = Mi_SQL + "', '" + Parametros.P_Usuario_Recibe_ID;
                    Mi_SQL = Mi_SQL + "', '" + Parametros.P_Dependencia_ID;
                    Mi_SQL = Mi_SQL + "', '" + Parametros.P_Observaciones;
                    Mi_SQL = Mi_SQL + "', '" + Parametros.P_Estatus;
                    Mi_SQL = Mi_SQL + "', '" + Parametros.P_Usuario_Creo;
                    Mi_SQL = Mi_SQL + "', GETDATE() )";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Int32 Cons_Det = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Man_Val_Ref_Det.Tabla_Ope_Tal_Man_Val_Ref_Det, Ope_Tal_Man_Val_Ref_Det.Campo_No_Detalle, 20));
                    if (Parametros.P_Dt_Refacciones != null)
                    {
                        foreach (DataRow Dr_Fila in Parametros.P_Dt_Refacciones.Rows)
                        {
                            Mi_SQL = "INSERT INTO " + Ope_Tal_Man_Val_Ref_Det.Tabla_Ope_Tal_Man_Val_Ref_Det;
                            Mi_SQL = Mi_SQL + " (" + Ope_Tal_Man_Val_Ref_Det.Campo_No_Detalle;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref_Det.Campo_No_Registro;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref_Det.Campo_Refaccion_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref_Det.Campo_Cantidad;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref_Det.Campo_Costo_Unitario;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref_Det.Campo_Costo_Total;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref_Det.Campo_Usuario_Creo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref_Det.Campo_Fecha_Creo;
                            Mi_SQL = Mi_SQL + ") VALUES (" + Cons_Det;
                            Mi_SQL = Mi_SQL + " , '" + Parametros.P_No_Registro;
                            Mi_SQL = Mi_SQL + "', '" + Dr_Fila["REFACCION_ID"].ToString();
                            Mi_SQL = Mi_SQL + "', '" + Dr_Fila["CANTIDAD"].ToString();
                            Mi_SQL = Mi_SQL + "', '" + Dr_Fila["COSTO_UNITARIO"].ToString();
                            Mi_SQL = Mi_SQL + "', '" + Dr_Fila["COSTO_TOTAL"].ToString();
                            Mi_SQL = Mi_SQL + "', '" + Parametros.P_Usuario_Creo;
                            Mi_SQL = Mi_SQL + "', GETDATE() )";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();


                            Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
                            Mi_SQL = Mi_SQL + " SET " + Cat_Tal_Refacciones.Campo_Existencia + " = " + Cat_Tal_Refacciones.Campo_Existencia + " - " + Dr_Fila["CANTIDAD"].ToString();
                            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Disponible + " = " + Cat_Tal_Refacciones.Campo_Disponible + " - " + Dr_Fila["CANTIDAD"].ToString();
                            Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Dr_Fila["REFACCION_ID"].ToString() + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            Cons_Det++;
                        }
                    }


                    Trans.Commit();
                }
                catch (SqlException Ex)
                {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152)
                    {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 2627)
                    {
                        if (Ex.Message.IndexOf("PRIMARY") != -1)
                        {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        }
                        else if (Ex.Message.IndexOf("UNIQUE") != -1)
                        {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        }
                        else
                        {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    }
                    else if (Ex.Number == 547)
                    {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 515)
                    {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                }
                finally
                {
                    Cn.Close();
                }
                return Parametros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Vales_Express
            ///DESCRIPCIÓN          : Modifica en la Base de Datos el registro indicado
            ///PARAMETROS           : 1.- Parametros. Contiene los parametros que se van hacer la
            ///                           modificación en la Base de Datos.
            ///CREO                 : Salvador Vázquez Camacho
            ///FECHA_CREO           : 25/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Cls_Ope_Tal_Manejo_Vales_Express_Negocio Modificar_Vales_Express(Cls_Ope_Tal_Manejo_Vales_Express_Negocio Parametros)
            {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try
                {
                    String Mi_SQL = "UPDATE " + Ope_Tal_Man_Val_Ref.Tabla_Ope_Tal_Man_Val_Ref;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Man_Val_Ref.Campo_Vehiculo_ID + " = '" + Parametros.P_Vehiculo_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Usuario_Entrega_ID + " = '" + Parametros.P_Usuario_Entrega_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Usuario_Recibe_ID + " = '" + Parametros.P_Usuario_Recibe_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Observaciones + " = '" + Parametros.P_Observaciones + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario_Modifico + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Man_Val_Ref.Campo_No_Registro + " = " + Parametros.P_No_Registro;
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Mi_SQL = "DELETE FROM " + Ope_Tal_Man_Val_Ref_Det.Tabla_Ope_Tal_Man_Val_Ref_Det + " WHERE " + Ope_Tal_Man_Val_Ref_Det.Campo_No_Registro + " = '" + Parametros.P_No_Registro + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Int32 Cons_Det = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Man_Val_Ref_Det.Tabla_Ope_Tal_Man_Val_Ref_Det, Ope_Tal_Man_Val_Ref_Det.Campo_No_Detalle, 20));
                    if (Parametros.P_Dt_Refacciones != null) {
                        foreach (DataRow Dr_Fila in Parametros.P_Dt_Refacciones.Rows) {
                            Mi_SQL = "INSERT INTO " + Ope_Tal_Man_Val_Ref_Det.Tabla_Ope_Tal_Man_Val_Ref_Det;
                            Mi_SQL = Mi_SQL + " (" + Ope_Tal_Man_Val_Ref_Det.Campo_No_Detalle;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref_Det.Campo_No_Registro;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref_Det.Campo_Refaccion_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref_Det.Campo_Cantidad;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref_Det.Campo_Costo_Unitario;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref_Det.Campo_Costo_Total;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref_Det.Campo_Usuario_Creo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref_Det.Campo_Fecha_Creo;
                            Mi_SQL = Mi_SQL + ") VALUES (" + Cons_Det;
                            Mi_SQL = Mi_SQL + " , '" + Parametros.P_No_Registro;
                            Mi_SQL = Mi_SQL + "', '" + Dr_Fila["REFACCION_ID"].ToString();
                            Mi_SQL = Mi_SQL + "', '" + Dr_Fila["CANTIDAD"].ToString();
                            Mi_SQL = Mi_SQL + "', '" + Dr_Fila["COSTO_UNITARIO"].ToString();
                            Mi_SQL = Mi_SQL + "', '" + Dr_Fila["COSTO_TOTAL"].ToString();
                            Mi_SQL = Mi_SQL + "', '" + Parametros.P_Usuario_Creo;
                            Mi_SQL = Mi_SQL + "', GETDATE() )";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            Cons_Det++;
                        }
                    }

                    Trans.Commit();
                }
                catch (SqlException Ex)
                {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152)
                    {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                    }
                    else if (Ex.Number == 2627)
                    {
                        if (Ex.Message.IndexOf("PRIMARY") != -1)
                        {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        }
                        else if (Ex.Message.IndexOf("UNIQUE") != -1)
                        {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        }
                        else
                        {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    }
                    else if (Ex.Number == 547)
                    {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 515)
                    {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error al intentar Modificar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                }
                finally
                {
                    Cn.Close();
                }
                return Parametros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cancelar_Vales_Express
            ///DESCRIPCIÓN          : Cancela en la Base de Datos el registro indicado
            ///PARAMETROS           : 1.- Parametros. Contiene los parametros que se van hacer la
            ///                           modificación en la Base de Datos.
            ///CREO                 : Salvador Vázquez Camacho
            ///FECHA_CREO           : 25/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Cls_Ope_Tal_Manejo_Vales_Express_Negocio Cancelar_Vales_Express(Cls_Ope_Tal_Manejo_Vales_Express_Negocio Parametros)
            {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try
                {
                    String Mi_SQL = "UPDATE " + Ope_Tal_Man_Val_Ref.Tabla_Ope_Tal_Man_Val_Ref;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Man_Val_Ref.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario_Modifico + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Man_Val_Ref.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Man_Val_Ref.Campo_No_Registro + " = " + Parametros.P_No_Registro;
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Int32 Cons_Det = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Man_Val_Ref_Det.Tabla_Ope_Tal_Man_Val_Ref_Det, Ope_Tal_Man_Val_Ref_Det.Campo_No_Detalle, 20));
                    if (Parametros.P_Dt_Refacciones != null) {
                        foreach (DataRow Dr_Fila in Parametros.P_Dt_Refacciones.Rows) {
                            Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
                            Mi_SQL = Mi_SQL + " SET " + Cat_Tal_Refacciones.Campo_Existencia + " = " + Cat_Tal_Refacciones.Campo_Existencia + " + " + Dr_Fila["CANTIDAD"].ToString();
                            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Disponible + " = " + Cat_Tal_Refacciones.Campo_Disponible + " + " + Dr_Fila["CANTIDAD"].ToString();
                            Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Dr_Fila["REFACCION_ID"].ToString() + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            Cons_Det++;
                        }
                    }

                    Trans.Commit();
                }
                catch (SqlException Ex)
                {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152)
                    {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                    }
                    else if (Ex.Number == 2627)
                    {
                        if (Ex.Message.IndexOf("PRIMARY") != -1)
                        {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        }
                        else if (Ex.Message.IndexOf("UNIQUE") != -1)
                        {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        }
                        else
                        {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    }
                    else if (Ex.Number == 547)
                    {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 515)
                    {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error al intentar Modificar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                }
                finally
                {
                    Cn.Close();
                }
                return Parametros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Vales_Express
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 1.- Parametros. Contiene los parametros que se van a utilizar para
            ///                           hacer la consulta de la Base de Datos.
            ///CREO                 : Salvador Vázquez Camacho
            ///FECHA_CREO           : 25/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consultar_Vales_Express(Cls_Ope_Tal_Manejo_Vales_Express_Negocio Parametros)
            {
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try
                {
                    Mi_SQL = "SELECT VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_No_Registro + " AS NO_REGISTRO";
                    Mi_SQL = Mi_SQL + ", VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Vehiculo_ID + " AS VEHICULO_ID";
                    Mi_SQL = Mi_SQL + ", VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Usuario_Entrega_ID + " AS USUSARIO_ENTREGA_ID";
                    Mi_SQL = Mi_SQL + ", VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Usuario_Recibe_ID + " AS USUARIO_RECIBE_ID";
                    Mi_SQL = Mi_SQL + ", VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Dependencia_ID + " AS DEPENDENCIA_ID";
                    Mi_SQL = Mi_SQL + ", VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL = Mi_SQL + ", VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Observaciones + " AS OBSERVACIONES";
                    Mi_SQL = Mi_SQL + ", ISNULL(VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Fecha_Modifico + ", VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Fecha_Creo + ") AS FECHA_MOVIMIENTO";
                    Mi_SQL = Mi_SQL + ", ISNULL(VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Fecha_Modifico + ", VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Fecha_Creo + ") AS FECHA";
                    Mi_SQL = Mi_SQL + ", ISNULL(VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Usuario_Modifico + ",VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Usuario_Creo + ")  AS REALIZO_MOVIMIENTO";

                    Mi_SQL = Mi_SQL + ", '0.0' AS KILOMETRAJE";

                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS VEHICULOS_NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " AS VEHICULOS_NO_ECONOMICO";
                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Modelo + " AS VEHICULOS_MODELO";
                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Placas + " AS VEHICULOS_PLACAS";
                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " AS NO_ECONOMICO";
                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Modelo + " AS MODELO";
                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Placas + " AS PLACAS";
                    Mi_SQL = Mi_SQL + ", (SELECT SUM(MOV_DET." + Ope_Tal_Man_Val_Ref_Det.Campo_Costo_Total + ") FROM " + Ope_Tal_Man_Val_Ref_Det.Tabla_Ope_Tal_Man_Val_Ref_Det + " MOV_DET WHERE MOV_DET." + Ope_Tal_Man_Val_Ref_Det.Campo_No_Registro + " = VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_No_Registro + ") AS COSTO";

                    Mi_SQL = Mi_SQL + ", TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Descripcion + " AS TIPO_VEHICULO";

                    Mi_SQL = Mi_SQL + ", (SELECT EMP." + Cat_Empleados.Campo_No_Empleado + " +' - '+";
                    Mi_SQL = Mi_SQL + " EMP." + Cat_Empleados.Campo_Apellido_Paterno + "+' '+";
                    Mi_SQL = Mi_SQL + " EMP." + Cat_Empleados.Campo_Apellido_Materno + "+' '+";
                    Mi_SQL = Mi_SQL + " EMP." + Cat_Empleados.Campo_Nombre + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMP WHERE EMP." + Cat_Empleados.Campo_Empleado_ID + " = VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Usuario_Entrega_ID + ") AS AUTORIZO";


                    Mi_SQL = Mi_SQL + ", (SELECT EMP." + Cat_Empleados.Campo_No_Empleado + " +' - '+";
                    Mi_SQL = Mi_SQL + " EMP." + Cat_Empleados.Campo_Apellido_Paterno + "+' '+";
                    Mi_SQL = Mi_SQL + " EMP." + Cat_Empleados.Campo_Apellido_Materno + "+' '+";
                    Mi_SQL = Mi_SQL + " EMP." + Cat_Empleados.Campo_Nombre + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMP WHERE EMP." + Cat_Empleados.Campo_Empleado_ID + " = VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Usuario_Recibe_ID + ") AS RECIBE";

                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + " AS DEPENDENCIAS_CLAVE";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIAS_NOMBRE";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + " ";
                    Mi_SQL = Mi_SQL + "+' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIAS_CLAVE_NOMBRE";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + " ";
                    Mi_SQL = Mi_SQL + "+' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                    
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Man_Val_Ref.Tabla_Ope_Tal_Man_Val_Ref + " VALES_REFACCIONES";
                    
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS";
                    Mi_SQL = Mi_SQL + " ON VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Vehiculo_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID;

                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
                    Mi_SQL = Mi_SQL + " ON VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID;


                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Tipos_Vehiculo.Tabla_Cat_Pat_Tipos_Vehiculo + " TIPOS_VEHICULO";
                    Mi_SQL = Mi_SQL + " ON VEHICULOS." + Ope_Pat_Vehiculos.Campo_Tipo_Vehiculo_ID + " = TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Tipo_Vehiculo_ID;

                    if (!String.IsNullOrEmpty(Parametros.P_No_Registro))
                    {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_No_Registro + " = " + Parametros.P_No_Registro + " ";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Vehiculo_ID))
                    {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Vehiculo_ID + " = '" + Parametros.P_Vehiculo_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Usuario_Entrega_ID))
                    {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Usuario_Entrega_ID + " = '" + Parametros.P_Usuario_Entrega_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Usuario_Recibe_ID))
                    {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Usuario_Recibe_ID + " = '" + Parametros.P_Usuario_Recibe_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                    {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Observaciones))
                    {
                        if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " VALES_REFACCIONES." + Ope_Tal_Man_Val_Ref.Campo_Observaciones + " LIKE '%" + Parametros.P_Observaciones + "%'";
                    }
                    Mi_SQL = Mi_SQL + " ORDER BY NO_REGISTRO DESC";
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                    {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Vales_Express
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 1.- Parametros. Contiene los parametros que se van a utilizar para
            ///                           hacer la consulta de la Base de Datos.
            ///CREO                 : Salvador Vázquez Camacho
            ///FECHA_CREO           : 25/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Cls_Ope_Tal_Manejo_Vales_Express_Negocio Consultar_Detalles_Vales_Express(Cls_Ope_Tal_Manejo_Vales_Express_Negocio Parametros)
            {
                String Mi_SQL = null;
                Cls_Ope_Tal_Manejo_Vales_Express_Negocio Obj_Cargado = new Cls_Ope_Tal_Manejo_Vales_Express_Negocio();
                try
                {
                    Mi_SQL = "SELECT * FROM " + Ope_Tal_Man_Val_Ref.Tabla_Ope_Tal_Man_Val_Ref + " WHERE " + Ope_Tal_Man_Val_Ref.Campo_No_Registro + " = " + Parametros.P_No_Registro + " ";
                    
                    SqlDataReader Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    while (Reader.Read())
                    {
                        Obj_Cargado.P_No_Registro = (!String.IsNullOrEmpty(Reader[Ope_Tal_Man_Val_Ref.Campo_No_Registro].ToString())) ? Reader[Ope_Tal_Man_Val_Ref.Campo_No_Registro].ToString() : "";
                        Obj_Cargado.P_Vehiculo_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Man_Val_Ref.Campo_Vehiculo_ID].ToString())) ? Reader[Ope_Tal_Man_Val_Ref.Campo_Vehiculo_ID].ToString() : "";
                        Obj_Cargado.P_Usuario_Entrega_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Man_Val_Ref.Campo_Usuario_Entrega_ID].ToString())) ? Reader[Ope_Tal_Man_Val_Ref.Campo_Usuario_Entrega_ID].ToString() : "";
                        Obj_Cargado.P_Usuario_Recibe_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Man_Val_Ref.Campo_Usuario_Recibe_ID].ToString())) ? Reader[Ope_Tal_Man_Val_Ref.Campo_Usuario_Recibe_ID].ToString() : "";
                        Obj_Cargado.P_Dependencia_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Man_Val_Ref.Campo_Dependencia_ID].ToString())) ? Reader[Ope_Tal_Man_Val_Ref.Campo_Dependencia_ID].ToString() : "";
                        Obj_Cargado.P_Observaciones = (!String.IsNullOrEmpty(Reader[Ope_Tal_Man_Val_Ref.Campo_Observaciones].ToString())) ? Reader[Ope_Tal_Man_Val_Ref.Campo_Observaciones].ToString() : "";
                        Obj_Cargado.P_Estatus = (!String.IsNullOrEmpty(Reader[Ope_Tal_Man_Val_Ref.Campo_Estatus].ToString())) ? Reader[Ope_Tal_Man_Val_Ref.Campo_Estatus].ToString() : "";
                    }
                    Reader.Close();

                    Mi_SQL = "SELECT SALIDAS_REFACCIONES." + Ope_Tal_Man_Val_Ref_Det.Campo_No_Detalle + " AS NO_DETALLE";
                    Mi_SQL = Mi_SQL + ", SALIDAS_REFACCIONES." + Ope_Tal_Man_Val_Ref_Det.Campo_No_Registro + " AS NO_REGISTRO";
                    Mi_SQL = Mi_SQL + ", SALIDAS_REFACCIONES." + Ope_Tal_Man_Val_Ref_Det.Campo_Refaccion_ID + " AS REFACCION_ID";
                    Mi_SQL = Mi_SQL + ", SALIDAS_REFACCIONES." + Ope_Tal_Man_Val_Ref_Det.Campo_Cantidad + " AS CANTIDAD";
                    Mi_SQL = Mi_SQL + ", SALIDAS_REFACCIONES." + Ope_Tal_Man_Val_Ref_Det.Campo_Costo_Unitario + " AS COSTO_UNITARIO";
                    Mi_SQL = Mi_SQL + ", SALIDAS_REFACCIONES." + Ope_Tal_Man_Val_Ref_Det.Campo_Costo_Total + " AS COSTO_TOTAL";

                    Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Clave;
                    Mi_SQL = Mi_SQL + " +' - '+ REFACCIONES." + Cat_Tal_Refacciones.Campo_Nombre + " AS REFACCIONES_CLAVE_NOMBRE";

                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Man_Val_Ref_Det.Tabla_Ope_Tal_Man_Val_Ref_Det + " SALIDAS_REFACCIONES";

                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " REFACCIONES";
                    Mi_SQL = Mi_SQL + " ON SALIDAS_REFACCIONES." + Ope_Tal_Man_Val_Ref_Det.Campo_Refaccion_ID + " = REFACCIONES." + Cat_Tal_Refacciones.Campo_Refaccion_ID;

                    Mi_SQL = Mi_SQL + " WHERE SALIDAS_REFACCIONES." + Ope_Tal_Man_Val_Ref_Det.Campo_No_Registro + " = " + Parametros.P_No_Registro + " ";

                    DataSet Ds_Refacciones = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Ds_Refacciones != null && Ds_Refacciones.Tables.Count > 0)
                    {
                        Obj_Cargado.P_Dt_Refacciones = Ds_Refacciones.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Obj_Cargado;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_ID_Consecutivo
            ///DESCRIPCIÓN          : Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
            ///PARAMETROS           : 1.- Tabla. Nombre de la tabla en la Base de Datos.
            ///                       2.- Campo. Nombre del campo de la tabla donde se va a obtener el ID.
            ///                       3.- Longitu_ID. Longitud del formato en que se desea obtener el ID.
            ///CREO                 : Salavdor Vázquez Camacho
            ///FECHA_CREO           : 25/Junio/2012
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID)
            {
                String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
                try
                {
                    String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                    Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                    {
                        Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                    }
                }
                catch (SqlException Ex)
                {
                    new Exception(Ex.Message);
                }
                return Id;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Convertir_A_Formato_ID
            ///DESCRIPCIÓN          : Pasa un numero entero a Formato de ID.
            ///PARAMETROS           : 1.- Dato_ID. Dato que se desea pasar al Formato de ID.
            ///                       2.- Longitud_ID. Longitud que tendra el ID. 
            ///CREO                 : Salavdor Vázquez Camacho
            ///FECHA_CREO           : 25/Junio/2012
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
            {
                String Retornar = "";
                String Dato = "" + Dato_ID;
                for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
                {
                    Retornar = Retornar + "0";
                }
                Retornar = Retornar + Dato;
                return Retornar;
            }

        #endregion
    }
}
