﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Calendarizacion_Revista_Mecanica.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Catalogo_Periodos_Revista.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Datos
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Operacion_Calendarizacion_Revista_Mecanica.Datos
{
    public class Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Datos {

        #region Metodos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Registro      
            ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a dar de
            ///                         Alta en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 22/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Actualizar_Registro(Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio Parametros) {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans = null;                

                try
                {
               
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                    String Mi_SQL = "DELETE FROM " + Ope_Tal_Per_Veh_Rev_Mec.Tabla_Ope_Tal_Per_Veh_REv_Mec +
                                    " WHERE " + Ope_Tal_Per_Veh_Rev_Mec.Campo_Vehiculo_ID + " = '" + Parametros.P_Vehiculo_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Mi_SQL = "INSERT INTO " + Ope_Tal_Per_Veh_Rev_Mec.Tabla_Ope_Tal_Per_Veh_REv_Mec;
                    Mi_SQL = Mi_SQL + " (" + Ope_Tal_Per_Veh_Rev_Mec.Campo_Vehiculo_ID;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Per_Veh_Rev_Mec.Campo_Periodo_ID;
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Ultima_Revision).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()).Trim())) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Per_Veh_Rev_Mec.Campo_Ultima_Revision;
                    }
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Proxima_Revision).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()).Trim())) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Per_Veh_Rev_Mec.Campo_Proxima_Revision;
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Per_Veh_Rev_Mec.Campo_Usuario_Creo +
                                    ", " + Ope_Tal_Per_Veh_Rev_Mec.Campo_Fecha_Creo;
                    Mi_SQL = Mi_SQL + ") VALUES";
                    Mi_SQL = Mi_SQL + " ('" + Parametros.P_Vehiculo_ID + "'";
                    if (!String.IsNullOrEmpty(Parametros.P_Periodo_ID))
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Periodo_ID + "'";
                    else
                        Mi_SQL = Mi_SQL + ", NULL";
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Ultima_Revision).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()).Trim())) {
                        Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Ultima_Revision) + "'";
                    }
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Proxima_Revision).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()).Trim())) { 
                        Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Proxima_Revision) + "'";
                    }
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ",  GETDATE() )"; 
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    if (Parametros.P_Cmmd == null)
                    {
                        Trans.Commit();
                    }
                } catch (SqlException Ex) {
                    if (Parametros.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    } 
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152) {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 2627)
                    {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    }
                    else if (Ex.Number == 547)
                    {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 515)
                    {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                }
                finally
                {
                    if (Parametros.P_Cmmd == null)
                    {
                        Cn.Close();
                        Cn = null;
                        Cmd = null;
                        Trans = null;
                    }
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Registro
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 
            ///                     1.Parametros.Contiene los parametros que se van a utilizar para
            ///                       hacer la consulta de la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 22/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio Consultar_Detalles_Registro(Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio Parametros) {
                String Mi_SQL = null;
                Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio Obj_Cargado = new Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio();
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans = null;
                try
                {
                    if (Parametros.P_Cmmd != null)
                    {
                        Cmd = Parametros.P_Cmmd;
                    }
                    else
                    {
                        Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                        Cn.Open();
                        Trans = Cn.BeginTransaction();
                        Cmd.Connection = Trans.Connection;
                        Cmd.Transaction = Trans;
                    }
                    Mi_SQL = "SELECT * FROM " + Ope_Tal_Per_Veh_Rev_Mec.Tabla_Ope_Tal_Per_Veh_REv_Mec + " WHERE " + Ope_Tal_Per_Veh_Rev_Mec.Campo_Vehiculo_ID + " = '" + Parametros.P_Vehiculo_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    SqlDataReader Reader = Cmd.ExecuteReader();
                    while (Reader.Read()) {
                        Obj_Cargado.P_Vehiculo_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Per_Veh_Rev_Mec.Campo_Vehiculo_ID].ToString())) ? Reader[Ope_Tal_Per_Veh_Rev_Mec.Campo_Vehiculo_ID].ToString() : "";
                        Obj_Cargado.P_Periodo_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Per_Veh_Rev_Mec.Campo_Periodo_ID].ToString())) ? Reader[Ope_Tal_Per_Veh_Rev_Mec.Campo_Periodo_ID].ToString() : "";
                        Obj_Cargado.P_Fecha_Ultima_Revision = (!String.IsNullOrEmpty(Reader[Ope_Tal_Per_Veh_Rev_Mec.Campo_Ultima_Revision].ToString())) ? Convert.ToDateTime(Reader[Ope_Tal_Per_Veh_Rev_Mec.Campo_Ultima_Revision]) : new DateTime();
                        Obj_Cargado.P_Fecha_Proxima_Revision = (!String.IsNullOrEmpty(Reader[Ope_Tal_Per_Veh_Rev_Mec.Campo_Proxima_Revision].ToString())) ? Convert.ToDateTime(Reader[Ope_Tal_Per_Veh_Rev_Mec.Campo_Proxima_Revision]) : new DateTime();
                    }
                    Reader.Close();
                    if (Parametros.P_Cmmd == null)
                    {
                        Trans.Commit();
                    } 
                } catch (Exception Ex) {
                    if (Parametros.P_Cmmd == null)
                    {
                        Trans.Rollback();
                    }
                    Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                finally
                {
                    if (Parametros.P_Cmmd == null)
                    {
                        Cn.Close();
                        Cn = null;
                        Cmd = null;
                        Trans = null;
                    }
                }
                return Obj_Cargado;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Sincronizacion_Revista_Mecanica_Periodo
            ///DESCRIPCIÓN          : Sincroniza la Ultima Revision y Carga la Siguiente.
            ///PARAMETROS           : 
            ///                     1.Parametros.Contiene los parametros que se van a utilizar para
            ///                       hacer la consulta de la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 22/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Sincronizacion_Revista_Mecanica_Periodo(Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio Parametros) { 
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans = null;         
                
                try {
                    if (Parametros.P_Cmmd != null)
                    {
                        Cmd = Parametros.P_Cmmd;
                    }
                    else
                    {
                        Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                        Cn.Open();
                        Trans = Cn.BeginTransaction();
                        Cmd.Connection = Trans.Connection;
                        Cmd.Transaction = Trans;
                    }
                    Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio Registro_Nuevo = new Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio();
                    Registro_Nuevo.P_Vehiculo_ID = Parametros.P_Vehiculo_ID;
                    Registro_Nuevo.P_Fecha_Ultima_Revision = Parametros.P_Fecha_Ultima_Revision;

                    Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio Tmp_Negocio = new Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio();
                    Tmp_Negocio.P_Cmmd = Cmd;
                    Tmp_Negocio.P_Vehiculo_ID = Parametros.P_Vehiculo_ID;
                    Tmp_Negocio = Tmp_Negocio.Consultar_Detalles_Registro();
                    if (!String.IsNullOrEmpty(Tmp_Negocio.P_Periodo_ID)) {
                        Cls_Cat_Tal_Periodos_Revista_Negocio Per_Neg = new Cls_Cat_Tal_Periodos_Revista_Negocio();
                        Per_Neg.P_Periodo_ID = Tmp_Negocio.P_Periodo_ID;
                        Per_Neg.P_Cmmd = Cmd;
                        Per_Neg = Per_Neg.Consultar_Detalles_Periodo_Revista();

                        Registro_Nuevo.P_Periodo_ID = Tmp_Negocio.P_Periodo_ID;
                        Registro_Nuevo.P_Fecha_Proxima_Revision = DateTime.Today.AddMonths(Convert.ToInt32(Per_Neg.P_Periodo_Mes));
                    }
                    Registro_Nuevo.P_Cmmd = Cmd;
                    Registro_Nuevo.Actualizar_Registro();
                } catch (SqlException Ex) {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152) {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 2627)
                    {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    }
                    else if (Ex.Number == 547)
                    {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 515)
                    {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_General_Calendarizacion_Revistas_Mecanicas
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 
            ///                     1.Parametros.Contiene los parametros que se van a utilizar para
            ///                       hacer la consulta de la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 10/Julio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consulta_General_Calendarizacion_Revistas_Mecanicas(Cls_Ope_Tal_Calendarizacion_Revista_Mecanica_Negocio Parametros)  {
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try {
                    Mi_SQL = "SELECT VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + " AS VEHICULO_ID";
                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " AS NO_ECONOMICO";
                    Mi_SQL = Mi_SQL + ", REVISTA." + Ope_Tal_Per_Veh_Rev_Mec.Campo_Ultima_Revision + " AS ULTIMA_REVISION";
                    Mi_SQL = Mi_SQL + ", REVISTA." + Ope_Tal_Per_Veh_Rev_Mec.Campo_Proxima_Revision + " AS PROXIMA_REVISION";
                    Mi_SQL = Mi_SQL + ", TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Descripcion;
                    Mi_SQL = Mi_SQL + "+' '+ MARCAS." + Cat_Com_Marcas.Campo_Nombre;
                    Mi_SQL = Mi_SQL + "+' '+ VEHICULOS." + Ope_Pat_Vehiculos.Campo_Modelo + " AS VEHICULO_DESCRIPCION";
                    Mi_SQL = Mi_SQL + ", ('['+ DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                    Mi_SQL = Mi_SQL + " +'] '+  DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + ") AS CLAVE_NOMBRE_DEPENDENCIA";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Per_Veh_Rev_Mec.Tabla_Ope_Tal_Per_Veh_REv_Mec + " REVISTA ON VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + " = REVISTA." + Ope_Tal_Per_Veh_Rev_Mec.Campo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " MARCAS ON MARCAS." + Cat_Com_Marcas.Campo_Marca_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Marca_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Pat_Tipos_Vehiculo.Tabla_Cat_Pat_Tipos_Vehiculo + " TIPOS_VEHICULO ON TIPOS_VEHICULO." + Cat_Pat_Tipos_Vehiculo.Campo_Tipo_Vehiculo_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Tipo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON VEHICULOS." + Ope_Pat_Vehiculos.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    if (!String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Proxima_Revision).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "REVISTA." + Ope_Tal_Per_Veh_Rev_Mec.Campo_Proxima_Revision + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Proxima_Revision.AddDays(1)) + "'";
                    }
                    Mi_SQL = Mi_SQL + " ORDER BY REVISTA." + Ope_Tal_Per_Veh_Rev_Mec.Campo_Proxima_Revision + " ASC";
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

        #endregion

	}
}