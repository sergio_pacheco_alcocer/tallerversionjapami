﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Datos;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Datos;
/// <summary>
/// Summary description for Cls_Ope_Tal_Servicios_Preventivos_Datos
/// </summary>
/// 

namespace JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Datos
{
    public class Cls_Ope_Tal_Servicios_Preventivos_Datos
    {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Servicio_Preventivo      
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 16/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Servicio_Preventivo(Cls_Ope_Tal_Servicios_Preventivos_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Parametros.P_No_Servicio = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos, Ope_Tal_Serv_Preventivos.Campo_No_Servicio, 20));
            try
            {
                String Mi_SQL = "INSERT INTO " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                Mi_SQL = Mi_SQL + " (" + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_No_Entrada + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Mecanico_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Estatus + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Revision_Inicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Usuario_Creo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Creo + ")";
                Mi_SQL = Mi_SQL + " VALUES (" + Parametros.P_No_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Parametros.P_No_Entrada + "";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Mecanico_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Solicitud + "'";
                Mi_SQL = Mi_SQL + ", GETDATE()";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE())";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Servicio_Preventivo      
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///                     2.  P_Cmd.      Comando en caso de ser remota la ejecucion final.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 16/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Int32 Alta_Servicio_Preventivo(Cls_Ope_Tal_Servicios_Preventivos_Negocio Parametros, ref SqlCommand P_Cmd)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Parametros.P_No_Servicio = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos, Ope_Tal_Serv_Preventivos.Campo_No_Servicio, 20));
            try
            {
                String Mi_SQL = "INSERT INTO " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                Mi_SQL = Mi_SQL + " (" + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_No_Entrada + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Mecanico_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Estatus + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Tipo_Bien + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Revision_Inicio + "";
                if (!String.IsNullOrEmpty(Parametros.P_Diagnostico)) Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Diagnostico + "";
                if (!String.IsNullOrEmpty(Parametros.P_Reparacion)) Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Reparacion + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Usuario_Creo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Creo + ")";
                Mi_SQL = Mi_SQL + " VALUES (" + Parametros.P_No_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Parametros.P_No_Entrada + "";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Mecanico_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo_Bien.Trim().Replace(" ","_") + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Solicitud + "'";
                Mi_SQL = Mi_SQL + ", GETDATE()";
                if (!String.IsNullOrEmpty(Parametros.P_Diagnostico)) Mi_SQL = Mi_SQL + ", '" + Parametros.P_Diagnostico + "'";
                if (!String.IsNullOrEmpty(Parametros.P_Reparacion)) Mi_SQL = Mi_SQL + ", '" + Parametros.P_Reparacion + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE())";
                P_Cmd.CommandText = Mi_SQL;
                P_Cmd.ExecuteNonQuery();

            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
            return Parametros.P_No_Servicio;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modifica_Servicio_Preventivo      
        ///DESCRIPCIÓN          : Modifica los Datos Iniciales del Servicio Preventivo
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 16/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Modifica_Servicio_Preventivo(Cls_Ope_Tal_Servicios_Preventivos_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                String Mi_SQL = "UPDATE " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Preventivos.Campo_Mecanico_ID + " = '" + Parametros.P_Mecanico_ID + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Revision_Inicio + " = GETDATE()";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Cls_Ope_Tal_Entradas_Vehiculos_Negocio Ent_Negocio = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
                Ent_Negocio.P_No_Entrada = Parametros.P_No_Entrada;
                Ent_Negocio.P_Estatus = "PROCESO";
                Ent_Negocio.P_Usuario = Parametros.P_Usuario;
                Cls_Ope_Tal_Entradas_Vehiculos_Datos.Actualizacion_Estatus_Entrada(Ent_Negocio, ref Cmd);

                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Asignar_Diagnostico_Servicio_Preventivo      
        ///DESCRIPCIÓN          : Asigna Diagnostico a Servicio Preventivo      
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 18/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Asignar_Diagnostico_Servicio_Preventivo(Cls_Ope_Tal_Servicios_Preventivos_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                String Mi_SQL = "UPDATE " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Preventivos.Campo_Diagnostico + " = '" + Parametros.P_Diagnostico + "'";
                if (!String.IsNullOrEmpty(Parametros.P_Tipo_Trabajo_ID))
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Tipo_Trabajo_ID + " = '" + Parametros.P_Tipo_Trabajo_ID + "'";
                if (!String.IsNullOrEmpty(Parametros.P_Mecanico_ID))
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Mecanico_ID + " = '" + Parametros.P_Mecanico_ID + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Reparacion + " = '" + Parametros.P_Reparacion + "'";
                if (Parametros.P_Reparacion.Trim().Equals("INTERNA")) {
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Entrega_Probable + " = '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Entrega_Probable) + "'";
                }
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Revision_Fin + " = GETDATE()";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Cls_Ope_Tal_Entradas_Vehiculos_Negocio Ent_Negocio = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
                Ent_Negocio.P_No_Entrada = Parametros.P_No_Entrada;
                Ent_Negocio.P_Estatus = "CERRADA";
                Ent_Negocio.P_Usuario = Parametros.P_Usuario;
                Cls_Ope_Tal_Entradas_Vehiculos_Datos.Actualizacion_Estatus_Entrada(Ent_Negocio, ref Cmd);

                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cerrar_Servicio_Preventivo      
        ///DESCRIPCIÓN          : Cierra el  Servicio Preventivo
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 4/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Cerrar_Servicio_Preventivo(Cls_Ope_Tal_Servicios_Preventivos_Negocio Parametros) {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {
                String Mi_SQL = "UPDATE " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Mi_SQL = "UPDATE " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " = '" + Parametros.P_Estatus_Solicitud + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Solicitud.P_No_Solicitud = Parametros.P_No_Solicitud;
                Solicitud.P_Estatus = "TERMINADO";
                Solicitud.P_Empleado_Solicito_ID = Parametros.P_Usuario_ID;
                Solicitud.P_Usuario = Parametros.P_Usuario;
                Cls_Ope_Tal_Solicitud_Servicio_Datos.Alta_Registro_Seguimiento_Solicitud(Solicitud, ref Cmd);

                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Servicio_Preventivo
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un Objetp.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 17/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Cls_Ope_Tal_Servicios_Preventivos_Negocio Consultar_Detalles_Servicio_Preventivo(Cls_Ope_Tal_Servicios_Preventivos_Negocio Parametros)
        {
            String Mi_SQL = null;
            Cls_Ope_Tal_Servicios_Preventivos_Negocio Obj_Cargado = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
            try
            {
                Mi_SQL = "SELECT * FROM " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                if (Parametros.P_No_Entrada > (-1))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Entrada + " = '" + Parametros.P_No_Entrada + "'";
                }
                else if (Parametros.P_No_Servicio > (-1))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                }
                else if (Parametros.P_No_Solicitud > (-1))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Correctivos.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                }
                SqlDataReader Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                while (Reader.Read())
                {
                    Obj_Cargado.P_No_Servicio = (!String.IsNullOrEmpty(Reader[Ope_Tal_Serv_Preventivos.Campo_No_Servicio].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Serv_Preventivos.Campo_No_Servicio]) : -1;
                    Obj_Cargado.P_No_Entrada = (!String.IsNullOrEmpty(Reader[Ope_Tal_Serv_Preventivos.Campo_No_Entrada].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Serv_Preventivos.Campo_No_Entrada]) : -1;
                    Obj_Cargado.P_Mecanico_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Serv_Preventivos.Campo_Mecanico_ID].ToString())) ? Reader[Ope_Tal_Serv_Preventivos.Campo_Mecanico_ID].ToString() : "";
                    Obj_Cargado.P_Estatus = (!String.IsNullOrEmpty(Reader[Ope_Tal_Serv_Preventivos.Campo_Estatus].ToString())) ? Reader[Ope_Tal_Serv_Preventivos.Campo_Estatus].ToString() : "";
                    Obj_Cargado.P_Fecha_Inicio_Revision = (!String.IsNullOrEmpty(Reader[Ope_Tal_Serv_Preventivos.Campo_Fecha_Revision_Inicio].ToString())) ? Convert.ToDateTime(Reader[Ope_Tal_Serv_Preventivos.Campo_Fecha_Revision_Inicio]) : new DateTime();
                    Obj_Cargado.P_Fecha_Fin_Revision = (!String.IsNullOrEmpty(Reader[Ope_Tal_Serv_Preventivos.Campo_Fecha_Revision_Fin].ToString())) ? Convert.ToDateTime(Reader[Ope_Tal_Serv_Preventivos.Campo_Fecha_Revision_Fin]) : new DateTime();
                    Obj_Cargado.P_Diagnostico = (!String.IsNullOrEmpty(Reader[Ope_Tal_Serv_Preventivos.Campo_Diagnostico].ToString())) ? Reader[Ope_Tal_Serv_Preventivos.Campo_Diagnostico].ToString() : "";
                    Obj_Cargado.P_Reparacion = (!String.IsNullOrEmpty(Reader[Ope_Tal_Serv_Preventivos.Campo_Reparacion].ToString())) ? Reader[Ope_Tal_Serv_Preventivos.Campo_Reparacion].ToString() : "";
                    Obj_Cargado.P_Fecha_Entrega_Probable = (!String.IsNullOrEmpty(Reader[Ope_Tal_Serv_Preventivos.Campo_Fecha_Entrega_Probable].ToString())) ? Convert.ToDateTime(Reader[Ope_Tal_Serv_Preventivos.Campo_Fecha_Entrega_Probable]) : new DateTime();
                    Obj_Cargado.P_Costo_Hora = (!String.IsNullOrEmpty(Reader[Ope_Tal_Serv_Preventivos.Campo_Costo_Hora].ToString())) ? Convert.ToDouble(Reader[Ope_Tal_Serv_Preventivos.Campo_Costo_Hora]) : -1;
                }
                Reader.Close();
                if (Obj_Cargado.P_No_Servicio > (-1)) {
                    Mi_SQL = "SELECT LISTADO." + Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID + " AS NO_LISTADO";
                    Mi_SQL += ", LISTADO." + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID + " AS REFACCION_ID";
                    Mi_SQL += ", LISTADO." + Ope_Tal_Req_Refaccion.Campo_Cantidad + " AS CANTIDAD";
                    Mi_SQL += ", LISTADO." + Ope_Tal_Req_Refaccion.Campo_Monto_Total + " AS TOTAL";
                    Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Clave + " AS CLAVE";
                    Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Nombre + " AS NOMBRE";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " LISTADO";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " REFACCIONES ON LISTADO." + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID + " = REFACCIONES." + Cat_Tal_Refacciones.Campo_Refaccion_ID + "";
                    Mi_SQL = Mi_SQL + " WHERE LISTADO." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " IN( SELECT ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Requisiciones.Campo_No_Solicitud + " = " + Parametros.P_No_Solicitud + " )";

                    //Mi_SQL = "SELECT LISTADO." + Ope_Tal_Listado_Refacciones.Campo_No_Listado + " AS NO_LISTADO";
                    //Mi_SQL = Mi_SQL + ", LISTADO." + Ope_Tal_Listado_Refacciones.Campo_Refaccion_ID + " AS REFACCION_ID";
                    //Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Clave + " AS CLAVE";
                    //Mi_SQL = Mi_SQL + ", REFACCIONES." + Cat_Tal_Refacciones.Campo_Nombre + " AS NOMBRE";
                    //Mi_SQL = Mi_SQL + ", LISTADO." + Ope_Tal_Listado_Refacciones.Campo_Cantidad + " AS CANTIDAD";
                    //Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Listado_Refacciones.Tabla_Ope_Tal_Listado_Refacciones + " LISTADO";
                    //Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " REFACCIONES ON LISTADO." + Ope_Tal_Listado_Refacciones.Campo_Refaccion_ID + " = REFACCIONES." + Cat_Tal_Refacciones.Campo_Refaccion_ID + "";
                    //Mi_SQL = Mi_SQL + " WHERE LISTADO." + Ope_Tal_Listado_Refacciones.Campo_Tipo_Servicio + " = 'SERVICIO_PREVENTIVO'";
                    //Mi_SQL = Mi_SQL + " AND LISTADO." + Ope_Tal_Listado_Refacciones.Campo_No_Servicio + " = '" + Obj_Cargado.P_No_Servicio + "'";
                    DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Ds_Datos != null && Ds_Datos.Tables.Count > 0) {
                        Obj_Cargado.P_Dt_Refacciones = Ds_Datos.Tables[0];
                    }
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Obj_Cargado;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modifica_Servicio_Preventivo_Costo      
        ///DESCRIPCIÓN          : Modifica los Datos Iniciales del Servicio Preventivo
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 16/Mayo/2012
        ///MODIFICO             : Jesus Toledo Rdz
        ///FECHA_MODIFICO       :05/Junio/2012
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Modifica_Servicio_Preventivo_Costo(Cls_Ope_Tal_Servicios_Preventivos_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                String Mi_SQL = "UPDATE " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Preventivos.Campo_Costo_Hora + " = '" + Parametros.P_Costo_Hora + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Servicios_Preventivos
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 21/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Servicios_Preventivos(Cls_Ope_Tal_Servicios_Preventivos_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " AS NO_SERVICIO";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Entrada + " AS NO_ENTRADA";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Solicitud + " AS NO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " AS FOLIO";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " AS FECHA_RECEPCION";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Reparacion + " AS REPARACION";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Costo_Hora + " AS COSTO";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Mecanico_ID + " AS MECANICO_ID";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Estatus + " AS ESTATUS_REAL";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Tipo_Bien + " AS TIPO_BIEN";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                Mi_SQL = Mi_SQL + ", CASE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                Mi_SQL = Mi_SQL + " WHEN 'SERVICIO_CORRECTIVO' THEN 'SERVICIO CORRECTIVO' WHEN 'SERVICIO_PREVENTIVO' THEN 'SERVICIO PREVENTIVO' WHEN 'SERVICIO_GENERAL' THEN 'SERVICIO GENERAL' WHEN 'REVISTA_MECANICA' THEN 'REVISTA MECANICA' WHEN 'VERIFICACION' THEN 'VERIFICACIÓN' END AS TIPO_SERVICIO";
                Mi_SQL = Mi_SQL + ", (DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + ") AS DEPENDENCIA";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " AS NO_ECONOMICO";
                Mi_SQL = Mi_SQL + ", CASE SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Estatus + " WHEN 'PENDIENTE' THEN 'POR ASIGNAR' WHEN 'ASIGNADO_MECANICO' THEN 'ASIGNADO' END AS ESTATUS";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Reserva + " AS NO_RESERVA";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos + " SERVICIOS";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES ON SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + " INNER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";

                if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                Mi_SQL = Mi_SQL + "SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Tipo_Bien + " = 'VEHICULO'";
                
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Estatus + " IN ('" + Parametros.P_Estatus.Trim() + "')";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Procedencia))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Procedencia + " IN ('" + Parametros.P_Procedencia.Trim() + "')";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Tipo_Servicio))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " IN ('" + Parametros.P_Tipo_Servicio.Trim() + "')";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Reparacion))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Reparacion + " IN ('" + Parametros.P_Reparacion.Trim() + "')";
                }
                if (Parametros.P_No_Servicio > 0)
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = " + Parametros.P_No_Servicio + " ";
                }
                if (Parametros.P_No_Inventario > 0) {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " = " + Parametros.P_No_Inventario + " ";
                }
                if (!String.IsNullOrEmpty(Parametros.P_No_Economico))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " = '" + Parametros.P_No_Economico + "' ";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Estatus_Solicitud))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " IN ('" + Parametros.P_Estatus_Solicitud.Trim() + "')";
                }
                if (Parametros.P_Folio_Solicitud > 0)
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " cast(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " as int) = " + Parametros.P_Folio_Solicitud;
                }
                Entro_Where = false;
                //Unir con Servicios de Bien Muebles
                Mi_SQL = Mi_SQL + " UNION SELECT SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " AS NO_SERVICIO";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Entrada + " AS NO_ENTRADA";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Solicitud + " AS NO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " AS FOLIO";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " AS FECHA_RECEPCION";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Reparacion + " AS REPARACION";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Costo_Hora + " AS COSTO";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Mecanico_ID + " AS MECANICO_ID";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Estatus + " AS ESTATUS_REAL";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Tipo_Bien + " AS TIPO_BIEN";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                Mi_SQL = Mi_SQL + ", CASE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                Mi_SQL = Mi_SQL + " WHEN 'SERVICIO_CORRECTIVO' THEN 'SERVICIO CORRECTIVO' WHEN 'SERVICIO_PREVENTIVO' THEN 'SERVICIO PREVENTIVO' WHEN 'SERVICIO_GENERAL' THEN 'SERVICIO GENERAL' WHEN 'REVISTA_MECANICA' THEN 'REVISTA MECANICA' WHEN 'VERIFICACION' THEN 'VERIFICACIÓN' END AS TIPO_SERVICIO";
                Mi_SQL = Mi_SQL + ", (DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + ") AS DEPENDENCIA";
                Mi_SQL = Mi_SQL + ", MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", CONVERT(CHAR,MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + ") AS NO_ECONOMICO";
                Mi_SQL = Mi_SQL + ", CASE SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Estatus + " WHEN 'PENDIENTE' THEN 'POR ASIGNAR' WHEN 'ASIGNADO_MECANICO' THEN 'ASIGNADO' END AS ESTATUS";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Reserva + " AS NO_RESERVA";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos + " SERVICIOS";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES ON SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + " INNER JOIN " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " MUEBLES ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";

                if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                Mi_SQL = Mi_SQL + "SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Tipo_Bien + " = 'BIEN_MUEBLE'";

                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Estatus + " IN ('" + Parametros.P_Estatus.Trim() + "')";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Procedencia))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Procedencia + " IN ('" + Parametros.P_Procedencia.Trim() + "')";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Tipo_Servicio))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " IN ('" + Parametros.P_Tipo_Servicio.Trim() + "')";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Reparacion))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Reparacion + " IN ('" + Parametros.P_Reparacion.Trim() + "')";
                }
                if (Parametros.P_No_Servicio > 0)
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = " + Parametros.P_No_Servicio + " ";
                }
                if (Parametros.P_No_Inventario > 0)
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " = " + Parametros.P_No_Inventario + " ";
                }
                if (!String.IsNullOrEmpty(Parametros.P_No_Economico))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " = '" + Parametros.P_No_Economico + "' ";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Estatus_Solicitud))
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + "SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " IN ('" + Parametros.P_Estatus_Solicitud.Trim() + "')";
                }
                if (Parametros.P_Folio_Solicitud > 0)
                {
                    if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " cast(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " as int) = " + Parametros.P_Folio_Solicitud;
                }

                if (!String.IsNullOrEmpty(Parametros.P_Ordernar_Dinamico))
                {
                    Mi_SQL = Mi_SQL + " ORDER BY " + Parametros.P_Ordernar_Dinamico;
                }
                else
                {
                    Mi_SQL = Mi_SQL + " ORDER BY FECHA_RECEPCION";
                }
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID)
        {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try
            {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            }
            catch (SqlException Ex)
            {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
        ///PARAMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
        {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
            {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Proveedor_Servicio     
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Proveedor_Servicio(Cls_Ope_Tal_Servicios_Preventivos_Negocio Parametros) {
            String Mensaje = "";
            String Mi_SQL = "";
            Object Aux; //Variable auxiliar para las consultas
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Formar Sentencia para obtener el consecutivo
                Mi_SQL = "";
                Mi_SQL = "SELECT ISNULL(MAX(";
                Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + "),0)";
                Mi_SQL = Mi_SQL + " FROM ";
                Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;

                //Ejecutar consulta del consecutivo
                Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0][0];

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                {
                    Parametros.P_No_Asignacion_Proveedor = Convert.ToInt32(Aux) + 1;
                }
                else
                    Parametros.P_No_Asignacion_Proveedor = 1;
                Mi_SQL = "UPDATE " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " = 'RECHAZADO'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Usuario_Creo + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Fecha_Creo + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = '" + (Parametros.P_Tipo.Trim().Equals("PREV") ? "SERVICIO_PREVENTIVO" : "SERVICIO_CORRECIVO") + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                //Insertar el nuevo proveedor
                Mi_SQL = "INSERT INTO " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;
                Mi_SQL = Mi_SQL + " (" + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Observaciones + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Usuario_Creo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Fecha_Creo + ")";
                Mi_SQL = Mi_SQL + " VALUES (" + Parametros.P_No_Asignacion_Proveedor + "";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Proveedor_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Servicio + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Observaciones + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE())";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Actualiza estatus de Servicio
                //Si es preventivo
                if (Parametros.P_Tipo.Contains("PREV"))
                {
                    Mi_SQL = "UPDATE " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = 'ASIGNADO_PROVEEDOR'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                //Si es correctivo
                else if (Parametros.P_Tipo.Contains("CORR"))
                {
                    Mi_SQL = "UPDATE " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Correctivos.Campo_Estatus + " = 'ASIGNADO_PROVEEDOR'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }


    }
}