﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Requisiciones.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Administrar_Requisiciones.Negocio;
using System.Data.SqlClient;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Generar_Reservas.Negocio;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Tal_Requisiciones_Datos
/// </summary>
namespace JAPAMI.Taller_Mecanico.Requisiciones.Datos {    
    public class Cls_Ope_Tal_Requisiciones_Datos 
    {
        public Cls_Ope_Tal_Requisiciones_Datos()
        {

        }
        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************
        #region UTILERIAS

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
            ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
            ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
            ///            2.-Nombre de la tabla
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 10/Enero/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static int Obtener_Consecutivo(String Campo_ID, String Tabla) {
                int Consecutivo = 0;
                String Mi_Sql;
                Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
                Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),0) FROM " + Tabla;
                Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                Consecutivo = (Convert.ToInt32(Obj) + 1);
                return Consecutivo;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Columnas_De_Tabla_BD
            ///DESCRIPCIÓN: Obtiene el nombre de las colmnas de una tabla en la BD
            ///PARAMETROS: 1.-Nombre_Tabla  - Nombre de la Tabla
            ///CREO:  
            ///FECHA_CREO: 
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static DataTable Consultar_Columnas_De_Tabla_BD(String Nombre_Tabla) {
                String Mi_Sql = "SELECT COLUMN_NAME AS COLUMNA FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + Nombre_Tabla + "'";
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql).Tables[0];
            }

        #endregion

        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************

        #region GUARDAR REQUISICION / INSERTAR REQUISICION, GUARDAR SUS DETALLES 
        
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Guardar_Nueva_Requisicion
            ///DESCRIPCIÓN: Guarda la RQ en la Base de Datos
            ///PARAMETROS: 1.-Requisicion_Negocio - Datos para la operacion
            ///CREO:
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static String Guardar_Nueva_Requisicion(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Mensaje = "";
                DataTable Dt_Productos_Almacen = null;
                String Fecha_Creo = DateTime.Now.ToString("dd/MM/yy").ToUpper();
                String Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                String Mi_SQL = "";
                String Capitulo_ID = "0";
                Object Aux = null;
                Int32 Consecutivo_Productos_Requisa = 0;
                DataTable Dt_Area_Funcional = new DataTable();
                //INSERTAR LA REQUISICION   
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();; //Adapatador para el llenado de las tablas
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try {

                    //Requisicion_Negocio.P_Requisicion_ID = Obtener_Consecutivo(Ope_Tal_Requisiciones.Campo_Requisicion_ID, Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones).ToString();
                    //Formar Sentencia para obtener el consecutivo
                    Mi_SQL = "";
                    Mi_SQL = "SELECT ISNULL(MAX(";
                    Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "),0)";
                    Mi_SQL = Mi_SQL + " FROM ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;

                    //Ejecutar consulta del consecutivo
                    Cmd.CommandText = Mi_SQL;
                    Aux = Cmd.ExecuteScalar();

                    //Verificar si no es nulo
                    if (Convert.IsDBNull(Aux) == false)
                    {
                        Requisicion_Negocio.P_Requisicion_ID = (Convert.ToInt32(Aux) + 1).ToString();
                    }
                    else
                        Requisicion_Negocio.P_Requisicion_ID = "1";
                    String Requisicion_Num = Requisicion_Negocio.P_Requisicion_ID;
                    Requisicion_Negocio.P_Folio = "RQ-" + Requisicion_Negocio.P_Requisicion_ID;

                    //Requisicion_Negocio.P_Codigo_Programatico = "";
                    Mi_SQL = "SELECT " + Cat_SAP_Area_Funcional.Campo_Clave + " FROM " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " WHERE " + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + " = " +
                        "(SELECT " + Cat_Dependencias.Campo_Area_Funcional_ID + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Requisicion_Negocio.P_Dependencia_ID + "')";
                    Cmd.CommandText = Mi_SQL;
                    Obj_Adaptador.SelectCommand = Cmd;
                    Obj_Adaptador.Fill(Dt_Area_Funcional);
                    //DataSet Ds_Area_Funcional = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Dt_Area_Funcional.Rows.Count > 0)
                    {
                        String Area = Dt_Area_Funcional.Rows[0]["CLAVE"].ToString();
                        Requisicion_Negocio.P_Codigo_Programatico = Requisicion_Negocio.P_Codigo_Programatico.Replace("1.1.1", Area);
                    }
                    if (Requisicion_Negocio.P_Tipo == "STOCK") {
                        //Consultamos los Productos que hay en el Almacen
                        Mi_SQL = "SELECT " + Cat_Tal_Refacciones.Campo_Refaccion_ID + "," + Cat_Tal_Refacciones.Campo_Existencia + "," + Cat_Tal_Refacciones.Campo_Disponible + "," + Cat_Tal_Refacciones.Campo_Comprometido + "," +
                            Cat_Tal_Refacciones.Campo_Nombre + "," + Cat_Tal_Refacciones.Campo_Comprometido + "," + Cat_Tal_Refacciones.Campo_Descripcion + "," + Cat_Tal_Refacciones.Campo_Comprometido +
                            " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " WHERE " + Cat_Tal_Refacciones.Campo_Tipo + " = 'STOCK'";
                        Cmd.CommandText = Mi_SQL;
                        Obj_Adaptador.SelectCommand = Cmd;
                        Dt_Productos_Almacen = new DataTable();
                        Obj_Adaptador.Fill(Dt_Productos_Almacen);
                        //DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        //if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0) { Dt_Productos_Almacen = Data_Set.Tables[0]; }
                    }
                    //Insertar cuando la requisicion quedo en estatus de contruccion
                    Mi_SQL = "INSERT INTO " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                    " (" + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                    ", " + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                    ", " + Ope_Tal_Requisiciones.Campo_Area_ID +
                    ", " + Ope_Tal_Requisiciones.Campo_Folio +
                    ", " + Ope_Tal_Requisiciones.Campo_Estatus +
                    ", " + Ope_Tal_Requisiciones.Campo_Codigo_Programatico +
                    ", " + Ope_Tal_Requisiciones.Campo_Elemento_PEP +
                    ", " + Ope_Tal_Requisiciones.Campo_Tipo +
                    ", " + Ope_Tal_Requisiciones.Campo_Fase +
                    ", " + Ope_Tal_Requisiciones.Campo_Subtotal +
                    ", " + Ope_Tal_Requisiciones.Campo_IVA +
                    ", " + Ope_Tal_Requisiciones.Campo_IEPS +
                    ", " + Ope_Tal_Requisiciones.Campo_Total +
                    ", " + Ope_Tal_Requisiciones.Campo_Usuario_Creo +
                    ", " + Ope_Tal_Requisiciones.Campo_Fecha_Creo +
                    ", " + Ope_Tal_Requisiciones.Campo_Justificacion_Compra +
                    ", " + Ope_Tal_Requisiciones.Campo_Especificacion_Prod_Serv +
                    ", " + Ope_Tal_Requisiciones.Campo_Verificaion_Entrega +
                    ", " + Ope_Tal_Requisiciones.Campo_Consolidada +
                    ", " + Ope_Tal_Requisiciones.Campo_Tipo_Articulo +
                    ", " + Ope_Tal_Requisiciones.Campo_Partida_ID +
                    ", " + Ope_Tal_Requisiciones.Campo_Especial_Ramo_33 +
                    ", " + Ope_Tal_Requisiciones.Campo_No_Solicitud;
                    if (Requisicion_Negocio.P_Estatus == "GENERADA") { Mi_SQL = Mi_SQL + ", " + Ope_Tal_Requisiciones.Campo_Empleado_Generacion_ID + ", " + Ope_Tal_Requisiciones.Campo_Fecha_Generacion;
                    } else { Mi_SQL = Mi_SQL + ", " + Ope_Tal_Requisiciones.Campo_Empleado_Construccion_ID + ", " + Ope_Tal_Requisiciones.Campo_Fecha_Construccion; }
                    Mi_SQL = Mi_SQL + ") VALUES (" +
                    Requisicion_Negocio.P_Requisicion_ID + ",'" +
                    Requisicion_Negocio.P_Dependencia_ID + "',null,'" +
                    //Cls_Sessiones.Area_ID_Empleado.Trim() + "','" +
                    Requisicion_Negocio.P_Folio + "','" +
                    Requisicion_Negocio.P_Estatus + "','" +
                    Requisicion_Negocio.P_Codigo_Programatico + "','" +
                    Requisicion_Negocio.P_Elemento_PEP + "','" +
                    Requisicion_Negocio.P_Tipo + "','" +
                    Requisicion_Negocio.P_Fase + "'," +
                    Requisicion_Negocio.P_Subtotal + ", " +
                    Requisicion_Negocio.P_IVA + ", " +
                    Requisicion_Negocio.P_IEPS + ", " +
                    Requisicion_Negocio.P_Total + ",'" +
                    Usuario_Creo + "','" +
                    Fecha_Creo + "','" +
                    Requisicion_Negocio.P_Justificacion_Compra + "','" +
                    Requisicion_Negocio.P_Especificacion_Productos + "','" +
                    Requisicion_Negocio.P_Verificacion_Entrega + "','NO','" +
                    Requisicion_Negocio.P_Tipo_Articulo + "','" +
                    Requisicion_Negocio.P_Partida_ID + "','" +
                    Requisicion_Negocio.P_Especial_Ramo33 + "','" +
                    Requisicion_Negocio.P_No_Solicitud + "','" +
                    Cls_Sessiones.Empleado_ID + "','" +
                    Fecha_Creo + "')";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Insertar los productos que se hayn seleccionado  para la requisa validando q si hay productos agregados 
                    //Int32 Consecutivo_Productos_Requisa =  Obtener_Consecutivo(Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID, Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion);
                    //Formar Sentencia para obtener el consecutivo
                    Mi_SQL = "";
                    Mi_SQL = "SELECT ISNULL(MAX(";
                    Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID + "),0)";
                    Mi_SQL = Mi_SQL + " FROM ";
                    Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion;

                    //Ejecutar consulta del consecutivo
                    Cmd.CommandText = Mi_SQL;
                    Aux = Cmd.ExecuteScalar();

                    //Verificar si no es nulo
                    if (Convert.IsDBNull(Aux) == false)
                    {
                        Consecutivo_Productos_Requisa = (Convert.ToInt32(Aux) + 1);
                    }
                    else
                        Requisicion_Negocio.P_Requisicion_ID = "1";
                    foreach (DataRow Renglon in Requisicion_Negocio.P_Dt_Productos_Servicios.Rows) {
                        Mi_SQL = "INSERT INTO " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion +
                            " (" + Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Clave +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Nombre_Producto_Servicio +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Partida_ID +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Cantidad +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Tipo +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Giro_ID +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Nombre_Giro +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Usuario_Creo +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Fecha_Creo +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Precio_Unitario +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Importe +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Monto_IVA +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Monto_IEPS +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Porcentaje_IVA +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Porcentaje_IEPS +
                            ", " + Ope_Tal_Req_Refaccion.Campo_Monto_Total + " ) VALUES " +
                            "(" + Consecutivo_Productos_Requisa +
                            "," + Requisicion_Negocio.P_Requisicion_ID +
                            ",'" + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() +
                            "','" + Renglon["REFACCION_ID"].ToString().Trim() +
                            "','" + Renglon["Clave"].ToString().Trim() +
                            "','" + Renglon["Nombre_Producto_Servicio"].ToString().Trim() +
                            "','" + Renglon["Partida_ID"].ToString().Trim() +
                            "','" + Renglon["Proyecto_Programa_ID"].ToString().Trim() +
                            "', " + Renglon["Cantidad"].ToString().Trim() +
                            ", '" + Renglon["Tipo"].ToString().Trim() +
                            "','" + Renglon["Concepto_ID"].ToString().Trim() +
                            "','" + Renglon["Nombre_Concepto"].ToString().Trim() +
                            "','" + Cls_Sessiones.Nombre_Empleado +
                            "', " + "GETDATE()" +
                            ", " + Renglon["Precio_Unitario"].ToString().Trim() +
                            ", " + Renglon["Monto"].ToString().Trim() +
                            ", " + Renglon["Monto_IVA"].ToString().Trim() +
                            ", " + Renglon["Monto_IEPS"].ToString().Trim() +
                            ", " + Renglon["Porcentaje_IVA"].ToString().Trim() +
                            ", " + Renglon["Porcentaje_IEPS"].ToString().Trim() +
                            ", " + Renglon["Monto_Total"].ToString().Trim() + ")";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        Consecutivo_Productos_Requisa++;
                    }

                    //Se dan de alta las observaciones o comentarios.
                    if (Requisicion_Negocio.P_Comentarios.Length > 0) {
                        Cls_Ope_Tal_Administrar_Requisiciones_Negocio Administrar_Requisicion = new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
                        Administrar_Requisicion.P_Requisicion_ID = Requisicion_Negocio.P_Requisicion_ID;
                        Administrar_Requisicion.P_Comentario = Requisicion_Negocio.P_Comentarios;
                        Administrar_Requisicion.P_Estatus = Requisicion_Negocio.P_Estatus;
                        Administrar_Requisicion.P_Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                        Administrar_Requisicion.Alta_Observaciones(ref Cmd);
                    }

                    //Si la requisición es de STOCK se comprometen los productos
                    if (Requisicion_Negocio.P_Tipo == "STOCK") {
                        if (Dt_Productos_Almacen != null) {
                            Int32 Disponible_Stock = 0;
                            Int32 Comprometido_Stock = 0;
                            Int32 Cantidad = 0;
                            String Producto_ID = "";
                            foreach (DataRow Producto_Requisicion in Requisicion_Negocio.P_Dt_Productos_Servicios.Rows) {
                                Cantidad = int.Parse(Producto_Requisicion["CANTIDAD"].ToString().Trim());
                                Producto_ID = Producto_Requisicion["REFACCION_ID"].ToString().Trim();
                                DataRow[] Dr_Productos_Almacen = Dt_Productos_Almacen.Select("REFACCION_ID ='" + Producto_ID + "'");
                                Disponible_Stock = int.Parse(Dr_Productos_Almacen[0]["DISPONIBLE"].ToString().Trim());
                                Comprometido_Stock = int.Parse(Dr_Productos_Almacen[0]["COMPROMETIDO"].ToString().Trim());
                                if (Disponible_Stock >= Cantidad)  {
                                    //Compromete
                                    Disponible_Stock = Disponible_Stock - Cantidad;
                                    Comprometido_Stock = Comprometido_Stock + Cantidad;
                                    Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones 
                                            + " SET " + Cat_Tal_Refacciones.Campo_Comprometido + " = " + Comprometido_Stock 
                                            + ", " + Cat_Tal_Refacciones.Campo_Disponible + " = " + Disponible_Stock 
                                            + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Producto_ID + "'";
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery();
                                    Mensaje = "EXITO";
                                } else {
                                    //No compromete y manda mensaje
                                    Mensaje = "CANTIDAD INSUFICIENTE DEL PRODUCTO " + Dr_Productos_Almacen[0]["NOMBRE"].ToString().Trim() + ", " +
                                    Dr_Productos_Almacen[0]["DESCRIPCION"].ToString().Trim() + ", DEBIDO A QUE SE COMPROMETIO EN UNA REQUISICION" +
                                    " ANTERIOR A LA DE USTED. DISPONIBLE[" + Disponible_Stock + "]";
                                }
                            }
                        }
                    } else {
                        Mensaje = "EXITO";
                    }
                    if (Mensaje == "EXITO") {
                        //Se consulta el CAPITULO_ID
                        Mi_SQL = "SELECT " + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado;
                        Mi_SQL += " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Requisicion_Negocio.P_Dependencia_ID +"'";
                        Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID +"'";
                        Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Requisicion_Negocio.P_Fuente_Financiamiento +"'";
                        Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Requisicion_Negocio.P_Partida_ID +"'";
                        Cmd.CommandText = Mi_SQL;
                        Aux = Cmd.ExecuteScalar();
                        if (Aux != "")
                            Capitulo_ID = Aux.ToString();
                        //Crear Reserva
                        DataTable Dt_Partidas_Reserva = new DataTable("Dt_Partidas_Reserva");
                        Dt_Partidas_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                        Dt_Partidas_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                        Dt_Partidas_Reserva.Columns.Add("FECHA_CREO", System.Type.GetType("System.DateTime"));
                        Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                        Dt_Partidas_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                        Dt_Partidas_Reserva.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                        Cls_Tal_Parametros_Negocio Parametros = new Cls_Tal_Parametros_Negocio();
                        Parametros.P_Cmmd = Cmd;
                        Parametros = Parametros.Consulta_Parametros();

                        DataRow Dr_Partida = Dt_Partidas_Reserva.NewRow();
                        Dr_Partida["FUENTE_FINANCIAMIENTO_ID"] = Requisicion_Negocio.P_Fuente_Financiamiento;
                        Dr_Partida["PROGRAMA_ID"] = Requisicion_Negocio.P_Proyecto_Programa_ID;
                        Dr_Partida["PARTIDA_ID"] = Requisicion_Negocio.P_Partida_ID;
                        Dr_Partida["FECHA_CREO"] = DateTime.Today;
                        Dr_Partida["DEPENDENCIA_ID"] = Requisicion_Negocio.P_Dependencia_ID;
                        Dr_Partida["IMPORTE"] = Convert.ToDouble(Requisicion_Negocio.P_Total);
                        Dr_Partida["ANIO"] = DateTime.Now.Year.ToString();
                        Dr_Partida["CAPITULO_ID"] = Capitulo_ID;
                        Dt_Partidas_Reserva.Rows.Add(Dr_Partida);
                        //especifica si es RAMO 33 o NORMAL
                        String Recurso = "RECURSO ASIGNADO";//Requisicion_Negocio.P_Ramo_33 == "SI" ? "RAMO 33" : "MUNICIPAL";

                        Int32 No_Reserva = Cls_Ope_Psp_Manejo_Presupuesto.Crear_Reserva(Requisicion_Negocio.P_Dependencia_ID,
                            "GENERADA", "", Requisicion_Negocio.P_Fuente_Financiamiento, Requisicion_Negocio.P_Proyecto_Programa_ID,
                            "RQ-" + Requisicion_Negocio.P_Requisicion_ID, DateTime.Now.Year.ToString(), Convert.ToDouble(Requisicion_Negocio.P_Total),
                            "", "", Parametros.P_Tipo_Solicitud_Pago_ID, Dt_Partidas_Reserva, Recurso,Cmd);

                        Int32 aux = 0;

                        //Comprometer presupuesto partida           
                        if (Requisicion_Negocio.P_Tipo == "TRANSITORIA") {
                            aux = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Dt_Partidas_Reserva, Cmd);

                            //Registrar movimiento presupuestal
                            aux = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(No_Reserva.ToString(), Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Convert.ToDouble(Requisicion_Negocio.P_Total), "", "", "", "", Cmd);
                        } else {
                            aux = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Dt_Partidas_Reserva, Cmd);
                            //Registrar movimiento presupuestal
                            aux = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(No_Reserva.ToString(), Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Convert.ToDouble(Requisicion_Negocio.P_Total), "", "", "", "", Cmd);
                        }

                        //Actualizar tipo de reserva UNICA o ABIERTA
                        Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " SET " + Ope_Psp_Reservas.Campo_Tipo_Reserva + " = 'UNICA'";
                        Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + No_Reserva + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        //Actualizar reserva en requisicion               
                        Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " SET NUM_RESERVA = " + No_Reserva +
                            " WHERE " + Ope_Tal_Requisiciones.Campo_Folio + " ='" + Requisicion_Negocio.P_Folio + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        Mensaje = "Requisición registrada con el folio: " + Requisicion_Negocio.P_Requisicion_ID + " / Número de reserva: " + No_Reserva;
                        Registrar_Historial(Requisicion_Negocio.P_Estatus, Requisicion_Negocio.P_Requisicion_ID, ref Cmd);
                        Trans.Commit();
                    }
                } catch (Exception ex) {
                    ex.ToString();
                    Trans.Rollback();
                    Mensaje = "No se pudo guardar requisición, consulte a su administrador";              
                } finally {
                    Cn.Close();
                }
                return Mensaje;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Comprometer_Disponible_Stock
            ///DESCRIPCIÓN: Compromete el Stock
            ///PARAMETROS: 1.-Requisicion_Negocio - Datos para la operacion
            ///CREO:
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            private String Comprometer_Disponible_Stock(DataTable Dt_Productos_Requisicion)  {
                String Mensaje = "";
                DataTable Dt_Productos_Almacen = null;
                String Mi_SQL = "";
                Mi_SQL =
                "SELECT " + Cat_Tal_Refacciones.Campo_Refaccion_ID + "," + Cat_Tal_Refacciones.Campo_Existencia + "," +
                Cat_Tal_Refacciones.Campo_Disponible + "," + Cat_Tal_Refacciones.Campo_Comprometido +
                " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                " WHERE " + Cat_Tal_Refacciones.Campo_Tipo + " = 'STOCK'";
                DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0) {
                    Dt_Productos_Almacen = Data_Set.Tables[0];
                }
                if (Dt_Productos_Almacen != null)  {
                    int Disponible_Stock = 0;
                    int Comprometido_Stock = 0;
                    int Cantidad = 0;
                    String Producto_ID = "";
                    foreach (DataRow Producto_Requisicion in Dt_Productos_Requisicion.Rows) {
                        Cantidad = int.Parse(Producto_Requisicion["CANTIDAD"].ToString().Trim());
                        Producto_ID = Producto_Requisicion["REFACCION_ID"].ToString().Trim();
                        DataRow[] Dr_Productos_Almacen = Dt_Productos_Almacen.Select("REFACCION_ID ='" + Producto_Requisicion["REFACCION_ID"].ToString().Trim() + "'");
                        Disponible_Stock = int.Parse( Dr_Productos_Almacen[0]["DISPONIBLE"].ToString().Trim());
                        Comprometido_Stock = int.Parse(Dr_Productos_Almacen[0]["COMPROMETIDO"].ToString().Trim());
                        if (Disponible_Stock >= Cantidad) {
                            //Compromete
                            Disponible_Stock = Disponible_Stock - Cantidad;
                            Comprometido_Stock = Comprometido_Stock + Cantidad;
                            Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                                " SET " + Cat_Tal_Refacciones.Campo_Comprometido +
                                " = " + Comprometido_Stock + ", " +
                                Cat_Tal_Refacciones.Campo_Disponible +
                                " = " + Disponible_Stock + "WHERE " +
                                Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" +
                                Producto_ID + "'";
                            //Cmd.CommandText = Mi_SQL;
                            //Cmd.ExecuteNonQuery();
                            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                            Mensaje = "EXITO";
                        } else {
                            //No compromete y manda mensaje
                            Mensaje = "CANTIDAD INSUFICIENTE DEL PRODUCTO " + Producto_Requisicion["NOMBRE"].ToString().Trim() + ", " +
                            Producto_Requisicion["DESCRIPCION"].ToString().Trim() + ", DEBIDO A QUE SE COMPROMETIO EN UNA REQUISICION" +
                            " ANTERIOR A LA DE USTED. DISPONIBLE[" + Disponible_Stock + "]";                        
                        }
                    }
                }
                return Mensaje;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Proceso_Insertar_Requisicion
            ///DESCRIPCIÓN: Manda llamar el de Insertar RQ
            ///PARAMETROS: 1.-Requisicion_Negocio - Datos para la operacion
            ///CREO:
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static String Proceso_Insertar_Requisicion(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                if (Cls_Sessiones.Nombre_Empleado != null && Cls_Sessiones.Area_ID_Empleado != null) {
                    return Guardar_Nueva_Requisicion(Requisicion_Negocio);
                } else {
                    return "No se guardó la requisición, reinicie sesión";
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Insertar_Requisicion
            ///DESCRIPCIÓN: crea una sentencia sql para insertar una Requisa en la base de datos
            ///PARAMETROS: 1.-Clase de Negocio
            ///            2.-Usuario que crea la requisa
            ///CREO: Silvia Morales Portuhondo
            ///FECHA_CREO: Noviembre/2010 
            ///MODIFICO:Gustavo Angeles Cruz
            ///FECHA_MODIFICO: 25/Ene/2011
            ///CAUSA_MODIFICACIÓN
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************            
            private static String Insertar_Requisicion(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Fecha_Creo = DateTime.Now.ToString("dd/MM/yy").ToUpper();
                String Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                String Mi_SQL = "";
                //INSERTAR LA REQUISICION   
             
                Requisicion_Negocio.P_Requisicion_ID = "" + Obtener_Consecutivo(Ope_Tal_Requisiciones.Campo_Requisicion_ID, Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones);
                String Requisicion_Num = Requisicion_Negocio.P_Requisicion_ID + "";
                Requisicion_Negocio.P_Folio = "RQ-" + Requisicion_Negocio.P_Requisicion_ID;
                //Requisicion_Negocio.P_Codigo_Programatico = "";
                Mi_SQL = "SELECT " + Cat_SAP_Area_Funcional.Campo_Clave + " FROM " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " WHERE " + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + " = " +
                    "(SELECT " + Cat_Dependencias.Campo_Area_Funcional_ID + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Requisicion_Negocio.P_Dependencia_ID + "')";
                DataSet Ds_Area_Funcional = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Area_Funcional != null && Ds_Area_Funcional.Tables[0].Rows.Count > 0) {
                    String Area = Ds_Area_Funcional.Tables[0].Rows[0]["CLAVE"].ToString();
                    Requisicion_Negocio.P_Codigo_Programatico = Requisicion_Negocio.P_Codigo_Programatico.Replace("area",Area);
                }
                //insertar cuando la requisicion quedo en estatus de contruccion
                    Mi_SQL = "INSERT INTO " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                    " (" + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                    ", " + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                    ", " + Ope_Tal_Requisiciones.Campo_Area_ID + 
                    ", " + Ope_Tal_Requisiciones.Campo_Folio + 
                    ", " + Ope_Tal_Requisiciones.Campo_Estatus +
                    ", " + Ope_Tal_Requisiciones.Campo_Codigo_Programatico +
                    ", " + Ope_Tal_Requisiciones.Campo_Elemento_PEP +
                    ", " + Ope_Tal_Requisiciones.Campo_Tipo + 
                    ", " + Ope_Tal_Requisiciones.Campo_Fase +
                    ", " + Ope_Tal_Requisiciones.Campo_Subtotal + 
                    ", " + Ope_Tal_Requisiciones.Campo_IVA + 
                    ", " + Ope_Tal_Requisiciones.Campo_IEPS +
                    ", " + Ope_Tal_Requisiciones.Campo_Total + 
                    ", " + Ope_Tal_Requisiciones.Campo_Usuario_Creo + 
                    ", " + Ope_Tal_Requisiciones.Campo_Fecha_Creo + 
                    ", " + Ope_Tal_Requisiciones.Campo_Justificacion_Compra +
                    ", " + Ope_Tal_Requisiciones.Campo_Especificacion_Prod_Serv + 
                    ", " + Ope_Tal_Requisiciones.Campo_Verificaion_Entrega +
                    ", " + Ope_Tal_Requisiciones.Campo_Consolidada +
                    ", " + Ope_Tal_Requisiciones.Campo_Tipo_Articulo +
                    ", " + Ope_Tal_Requisiciones.Campo_Partida_ID;
                    if (Requisicion_Negocio.P_Estatus == "GENERADA") {
                        Mi_SQL = Mi_SQL + 
                                 ", " + Ope_Tal_Requisiciones.Campo_Empleado_Generacion_ID +
                                 ", " + Ope_Tal_Requisiciones.Campo_Fecha_Generacion;
                    } else {
                        Mi_SQL = Mi_SQL + 
                                 ", " + Ope_Tal_Requisiciones.Campo_Empleado_Construccion_ID +
                                 ", " + Ope_Tal_Requisiciones.Campo_Fecha_Construccion;
                    }
                    Mi_SQL = Mi_SQL +
                    ") VALUES (" +
                    Requisicion_Negocio.P_Requisicion_ID + ",'" +
                    Requisicion_Negocio.P_Dependencia_ID + "','" +
                    Cls_Sessiones.Area_ID_Empleado.Trim() + "','" +
                    Requisicion_Negocio.P_Folio + "','" + 
                    Requisicion_Negocio.P_Estatus + "','" +
                    Requisicion_Negocio.P_Codigo_Programatico + "','" +
                    Requisicion_Negocio.P_Elemento_PEP + "','" +
                    Requisicion_Negocio.P_Tipo + "','" + 
                    Requisicion_Negocio.P_Fase + "'," +
                    Requisicion_Negocio.P_Subtotal + ", " + 
                    Requisicion_Negocio.P_IVA + ", " +
                    Requisicion_Negocio.P_IEPS + ", " + 
                    Requisicion_Negocio.P_Total + ",'" + 
                    Usuario_Creo + "','" + 
                    Fecha_Creo + "','" + 
                    Requisicion_Negocio.P_Justificacion_Compra + "','" + 
                    Requisicion_Negocio.P_Especificacion_Productos + "','" + 
                    Requisicion_Negocio.P_Verificacion_Entrega + "','NO','" + 
                    Requisicion_Negocio.P_Tipo_Articulo + "','" +
                    Requisicion_Negocio.P_Partida_ID + "','" +
                    Cls_Sessiones.Empleado_ID + "','" + 
                    Fecha_Creo + "')";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                return Requisicion_Num;
            }
            
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Guarda_Productos_O_Servicios_Requisicion
            ///DESCRIPCIÓN: Guarda los Productos o Serv de la RQ.
            ///PARAMETROS: 1.-Requisicion_Negocio - Datos para la operacion
            ///CREO:
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            //Si se usa, 31 marzo 2011
            private static void Guarda_Productos_O_Servicios_Requisicion(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                //insertar los productos que se hayn seleccionado 
                //para la requisa validando q si hay productos agregados
                String Mi_SQL = "";
                foreach (DataRow Renglon in Requisicion_Negocio.P_Dt_Productos_Servicios.Rows) {
                    Mi_SQL = "INSERT INTO " +
                        Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion +
                        " (" + Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Clave +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Nombre_Producto_Servicio +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Partida_ID +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Cantidad +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Tipo +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Giro_ID +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Nombre_Giro +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Usuario_Creo +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Fecha_Creo +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Precio_Unitario +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Importe +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Monto_IVA +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Monto_IEPS +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Porcentaje_IVA +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Porcentaje_IEPS +
                        ", " + Ope_Tal_Req_Refaccion.Campo_Monto_Total + " ) VALUES " +
                        "(" + Obtener_Consecutivo(Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID, Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion) +
                        "," + Requisicion_Negocio.P_Requisicion_ID +
                        ",'" + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() +
                        "','" + Renglon["REFACCION_ID"].ToString().Trim() +
                        "','" + Renglon["Clave"].ToString().Trim() +
                        "','" + Renglon["Nombre_Producto_Servicio"].ToString().Trim() +
                        "','" + Renglon["Partida_ID"].ToString().Trim() +
                        "','" + Renglon["Proyecto_Programa_ID"].ToString().Trim() +
                        "', " + Renglon["Cantidad"].ToString().Trim() +
                        ", '" + Renglon["Tipo"].ToString().Trim() +
                        "','" + Renglon["Concepto_ID"].ToString().Trim() +
                        "','" + Renglon["Nombre_Concepto"].ToString().Trim() +
                        "','" + Cls_Sessiones.Nombre_Empleado +
                        "', " + "GETDATE()" +
                        ", " + Renglon["Precio_Unitario"].ToString().Trim() +
                        ", " + Renglon["Monto"].ToString().Trim() +
                        ", " + Renglon["Monto_IVA"].ToString().Trim() +
                        ", " + Renglon["Monto_IEPS"].ToString().Trim() +
                        ", " + Renglon["Porcentaje_IVA"].ToString().Trim() +
                        ", " + Renglon["Porcentaje_IEPS"].ToString().Trim() +
                        ", " + Renglon["Monto_Total"].ToString().Trim() + ")";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                //Se compromete el presupuesto de cada servicio o producto pedido
                //Guardar Comentarios
                if (Requisicion_Negocio.P_Comentarios.Length > 0) {
                    Cls_Ope_Tal_Administrar_Requisiciones_Negocio Administrar_Requisicion = 
                        new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
                    Administrar_Requisicion.P_Requisicion_ID = Requisicion_Negocio.P_Requisicion_ID;
                    Administrar_Requisicion.P_Comentario = Requisicion_Negocio.P_Comentarios;
                    Administrar_Requisicion.P_Estatus = Requisicion_Negocio.P_Estatus;
                    Administrar_Requisicion.P_Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                    Administrar_Requisicion.Alta_Observaciones();
                }
            }

        #endregion

        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************
        #region MODIFICAR REQUISICION

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Proceso_Actualizar_Requisicion
            ///DESCRIPCIÓN: Proceso de Actualizar la RQ
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static String Proceso_Actualizar_Requisicion(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Mensaje = "";
                String Listado = "NO";
                if (Requisicion_Negocio.P_Estatus == "CANCELADA PARCIAL" || Requisicion_Negocio.P_Estatus == "CANCELADA TOTAL") {
                    Listado = "SI";
                }
                if (Requisicion_Negocio.P_Listado_Almacen != null && Requisicion_Negocio.P_Listado_Almacen == "SI" && Listado == "SI") {
                    Mensaje = Cancelar_Requisición_De_Listado(Requisicion_Negocio.P_Requisicion_ID, Requisicion_Negocio.P_Comentarios, Requisicion_Negocio.P_Estatus);
                } else {
                    Mensaje = Actualizar_Requisicion_De_Unidad_Responsable(Requisicion_Negocio);
                }
                return Mensaje;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Proceso_Actualizar_Requisicion
            ///DESCRIPCIÓN: Cancela RQ de Listado
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            private static String Cancelar_Requisición_De_Listado(String No_Requisicion, String Comentarios, String Estatus)  {
                String Mi_SQL = "";
                String Mensaje = "EXITO";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try {
                    //UPDATE OPE_COM_REQUISICIONES SET ESTATUS = 'CANCELADA' WHERE NO_REQUISICION = 0
                    Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                    " SET " + Ope_Tal_Requisiciones.Campo_Estatus + " = 'CANCELADA' WHERE " +
                    Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = " + No_Requisicion;
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();                
                    //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    //UPDATE OPE_COM_LISTADO SET ESTATUS = 'AUTORIZADO' WHERE LISTADO_ID = 
                    //(SELECT DISTINCT(NO_LISTADO_ID) FROM OPE_COM_LISTADO_DETALLE WHERE NO_REQUISICION = 0 )
                    if (Estatus == "CANCELADA PARCIAL")
                    {
                        Estatus = "AUTORIZADA";
                        Mi_SQL = "UPDATE " + Ope_Com_Listado.Tabla_Ope_Com_Listado + " SET " + Ope_Com_Listado.Campo_Estatus + " = '" + Estatus + "' WHERE " +
                            Ope_Com_Listado.Campo_Listado_ID + " = (SELECT DISTINCT(" + Ope_Com_Listado_Detalle.Campo_No_Listado_ID + ")" +
                            " FROM " + Ope_Com_Listado_Detalle.Tabla_Ope_Com_Listado_Detalle + " WHERE " + Ope_Com_Listado_Detalle.Campo_No_Requisicion +
                            " = " + No_Requisicion + ")";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        //UPDATE OPE_COM_LISTADO_DETALLE SET NO_REQUISICION = NULL WHERE NO_REQUISICION = 0 
                        Mi_SQL = "UPDATE " + Ope_Com_Listado_Detalle.Tabla_Ope_Com_Listado_Detalle + " SET " +
                            Ope_Com_Listado_Detalle.Campo_No_Requisicion + " = NULL WHERE " +
                            Ope_Com_Listado_Detalle.Campo_No_Requisicion + " = " + No_Requisicion;
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                    if (Comentarios.Length > 0) {
                        Cls_Ope_Tal_Administrar_Requisiciones_Negocio Administrar_Requisicion =
                            new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
                        Administrar_Requisicion.P_Requisicion_ID = No_Requisicion;
                        Administrar_Requisicion.P_Comentario = Comentarios;
                        Administrar_Requisicion.P_Estatus = "CANCELADA";
                        Administrar_Requisicion.P_Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                        Administrar_Requisicion.Alta_Observaciones();
                    }
                    Trans.Commit();
                    Registrar_Historial("CANCELADA", No_Requisicion);
                } catch (Exception ex) {
                    Trans.Rollback();
                    Mensaje = ex.ToString();               
                } finally {
                    Cn.Close();
                }
                return Mensaje;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cancelar_Requisicion
            ///DESCRIPCIÓN: Cancela RQ
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static String Cancelar_Requisicion(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Mensaje = "";
                String Fecha_Creo = DateTime.Now.ToString("dd/MM/yy").ToUpper();
                String Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                if (Requisicion_Negocio.P_Tipo == "STOCK") { 

                } else if (Requisicion_Negocio.P_Tipo == "TRANSITORIA") {
     
                }
                return Mensaje;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Actualizar_Requisicion_De_Unidad_Responsable
            ///DESCRIPCIÓN: Cancela RQ
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static String Actualizar_Requisicion_De_Unidad_Responsable(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {

                //DataTable Dt = Requisicion_Negocio.P_Dt_Productos_Servicios_Aux;
                String Mensaje = "";
                DataTable Dt_Productos_Almacen = null;
                String Fecha_Creo = DateTime.Now.ToString("dd/MM/yy").ToUpper();
                String Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                DataTable Dt_Parametros = new DataTable();
                String Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                String Mi_SQL = "";
                String Partida_ID = "0";//Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Partida_ID].ToString().Trim();
                String Proyecto_ID = "0";//Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID].ToString().Trim();
                String FF = "0";//Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                String Dependencia_ID = "0";
                String Tipo_Solicitud_Pago_ID = "0";
                //ACTUALIZAR LA REQUISICION
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try {

                    String No_Reserva = Consultar_Num_Reserva(Requisicion_Negocio).ToString().Trim();
                    //DataTable Dt_Partidas_Reserva = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Detalles_Reserva(No_Reserva);
                    //Limpiamos la Variable Mi_SQL
                    Mi_SQL = "";
                    //Consultamos la tabla de parametros para obtener la partida especifica
                    Mi_SQL = "SELECT " + Cat_Tal_Parametros.Campo_Fuente_Financiamiento;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Partida_ID;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Dependencia_ID;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Parametros.Campo_Programa_ID;
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros;
                    //Ejecutar Consulta
                    Cmd.CommandText = Mi_SQL;
                    Obj_Adaptador.SelectCommand = Cmd;
                    Obj_Adaptador.Fill(Dt_Parametros);
                    //Dt_Parametros = OracleHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                    if (Dt_Parametros.Rows.Count > 0)
                    {
                        Partida_ID = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Partida_ID].ToString();
                        FF = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Fuente_Financiamiento].ToString();
                        Proyecto_ID = Requisicion_Negocio.P_Proyecto_Programa_ID;
                        //Se ultiliza la Dependencia de la Unidad
                        Dependencia_ID = Requisicion_Negocio.P_Dependencia_ID;
                        Tipo_Solicitud_Pago_ID = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Tipo_Solicitud_Pago_ID].ToString();
                    }
                    
                    DataTable Dt_Partidas_Reserva = new DataTable();
                    Dt_Partidas_Reserva = new DataTable("Dt_Partidas_Reserva");
                    Dt_Partidas_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("FECHA_CREO", System.Type.GetType("System.DateTime"));
                    Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                    DataRow Dr_Partida = Dt_Partidas_Reserva.NewRow();
                    Dr_Partida["FUENTE_FINANCIAMIENTO_ID"] = FF;
                    Dr_Partida["PROGRAMA_ID"] = Proyecto_ID;
                    Dr_Partida["PARTIDA_ID"] = Partida_ID;
                    Dr_Partida["FECHA_CREO"] = DateTime.Today;
                    Dr_Partida["DEPENDENCIA_ID"] = Dependencia_ID;
                    Dr_Partida["ANIO"] = DateTime.Today.Year.ToString();
                    Dr_Partida["IMPORTE"] = ((Requisicion_Negocio.P_Estatus.Trim().Equals("CANCELADA")) ? Convert.ToDouble(Requisicion_Negocio.P_Total) : Convert.ToDouble(Requisicion_Negocio.P_Total_Diferencia));
                    Dt_Partidas_Reserva.Rows.Add(Dr_Partida);

                    Int32 Consecutivo_Productos_Requisa = Obtener_Consecutivo(Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID, Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion);
                    if (Requisicion_Negocio.P_Tipo == "STOCK")
                    {
                        Mi_SQL = "SELECT " + Cat_Tal_Refacciones.Campo_Refaccion_ID + "," + Cat_Tal_Refacciones.Campo_Existencia + "," + Cat_Tal_Refacciones.Campo_Disponible + "," + Cat_Tal_Refacciones.Campo_Comprometido +
                            " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " WHERE " + Cat_Tal_Refacciones.Campo_Tipo + " = 'STOCK'";
                        DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0) { Dt_Productos_Almacen = Data_Set.Tables[0]; }
                    }

                    Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " SET " +
                    Ope_Tal_Requisiciones.Campo_Estatus + " = '" + Requisicion_Negocio.P_Estatus + "', " +
                    Ope_Tal_Requisiciones.Campo_Subtotal + " = " + Requisicion_Negocio.P_Subtotal + ", " +
                    Ope_Tal_Requisiciones.Campo_IVA + " = " + Requisicion_Negocio.P_IVA + ", " +
                    Ope_Tal_Requisiciones.Campo_IEPS + " = " + Requisicion_Negocio.P_IEPS + ", " +
                    Ope_Tal_Requisiciones.Campo_Total + " = " + Requisicion_Negocio.P_Total + ", " +
                    Ope_Tal_Requisiciones.Campo_Usuario_Modifico + " = '" + Usuario + "', " +
                    Ope_Tal_Requisiciones.Campo_Fecha_Modifico + " = GETDATE(), " +
                    Ope_Tal_Requisiciones.Campo_Justificacion_Compra + " = '" + Requisicion_Negocio.P_Justificacion_Compra + "'," +
                    Ope_Tal_Requisiciones.Campo_Especificacion_Prod_Serv + " = '" + Requisicion_Negocio.P_Especificacion_Productos + "'," +
                    Ope_Tal_Requisiciones.Campo_Verificaion_Entrega + "= '" + Requisicion_Negocio.P_Verificacion_Entrega + "'";
                    if (Requisicion_Negocio.P_Estatus == "GENERADA") {
                        Mi_SQL = Mi_SQL + "," + Ope_Tal_Requisiciones.Campo_Empleado_Generacion_ID + " = '" + Cls_Sessiones.Empleado_ID.ToString() + "'," +
                        Ope_Tal_Requisiciones.Campo_Fecha_Generacion + " = GETDATE()";
                    } else if (Requisicion_Negocio.P_Estatus == "CANCELADA") {
                        Mi_SQL = Mi_SQL + "," + Ope_Tal_Requisiciones.Campo_Empleado_Cancelada_ID + " = '" + Cls_Sessiones.Empleado_ID.ToString() + "'," +
                        Ope_Tal_Requisiciones.Campo_Fecha_Cancelada + " = GETDATE()";
                    }
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = '" + Requisicion_Negocio.P_Requisicion_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    if (Requisicion_Negocio.P_Estatus != "CANCELADA") {
                        Mi_SQL = "DELETE FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " WHERE " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = " + Requisicion_Negocio.P_Requisicion_ID;
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                    //Se guardan los detalles, productps o sevicios
                    if (Requisicion_Negocio.P_Estatus != "CANCELADA") {
                        foreach (DataRow Renglon in Requisicion_Negocio.P_Dt_Productos_Servicios.Rows) {
                            Mi_SQL = "INSERT INTO " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion +
                                " (" + Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Clave +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Nombre_Producto_Servicio +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Partida_ID +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Cantidad +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Tipo +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Giro_ID +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Nombre_Giro +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Usuario_Creo +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Fecha_Creo +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Precio_Unitario +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Importe +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Monto_IVA +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Monto_IEPS +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Porcentaje_IVA +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Porcentaje_IEPS +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Monto_Total + " ) VALUES " +
                                "(" + Consecutivo_Productos_Requisa +
                                "," + Requisicion_Negocio.P_Requisicion_ID +
                                ",'" + Renglon["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() +
                                "','" + Renglon["Refaccion_ID"].ToString().Trim() +
                                "','" + Renglon["Clave"].ToString().Trim() +
                                "','" + Renglon["Nombre_Producto_Servicio"].ToString().Trim() +
                                "','" + Renglon["Partida_ID"].ToString().Trim() +
                                "','" + Renglon["Proyecto_Programa_ID"].ToString().Trim() +
                                "', " + Renglon["Cantidad"].ToString().Trim() +
                                ", '" + Renglon["Tipo"].ToString().Trim() +
                                "','" + Renglon["Concepto_ID"].ToString().Trim() +
                                "','" + Renglon["Nombre_Concepto"].ToString().Trim() +
                                "','" + Cls_Sessiones.Nombre_Empleado +
                                "', " + "GETDATE()" +
                                ", " + Renglon["Precio_Unitario"].ToString().Trim() +
                                ", " + Renglon["Monto"].ToString().Trim() +
                                ", " + Renglon["Monto_IVA"].ToString().Trim() +
                                ", " + Renglon["Monto_IEPS"].ToString().Trim() +
                                ", " + Renglon["Porcentaje_IVA"].ToString().Trim() +
                                ", " + Renglon["Porcentaje_IEPS"].ToString().Trim() +
                                ", " + Renglon["Monto_Total"].ToString().Trim() + ")";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            Consecutivo_Productos_Requisa++;
                        }
                        ////Guardar Comentarios
                    }

                    //Si la requisición es de STOCK se comprometen los productos
                    if (Requisicion_Negocio.P_Tipo == "STOCK") {

                        if (Dt_Productos_Almacen != null) {
                            Int32 Disponible_Stock = 0;
                            Int32 Comprometido_Stock = 0;
                            Int32 Cantidad = 0;
                            String Producto_ID = "";

                            foreach (DataRow Producto_Requisicion in Requisicion_Negocio.P_Dt_Productos_Servicios.Rows) {
                                Cantidad = int.Parse(Producto_Requisicion["CANTIDAD"].ToString().Trim());
                                Producto_ID = Producto_Requisicion["REFACCION_ID"].ToString().Trim();
                                DataRow[] Dr_Productos_Almacen = Dt_Productos_Almacen.Select("REFACCION_ID ='" + Producto_ID + "'");
                                Disponible_Stock = int.Parse(Dr_Productos_Almacen[0]["DISPONIBLE"].ToString().Trim());
                                Comprometido_Stock = int.Parse(Dr_Productos_Almacen[0]["COMPROMETIDO"].ToString().Trim());

                                Disponible_Stock = Disponible_Stock + Cantidad;
                                Comprometido_Stock = Comprometido_Stock - Cantidad;
                                Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " SET " + Cat_Tal_Refacciones.Campo_Comprometido + " = " + Comprometido_Stock 
                                    + ", " + Cat_Tal_Refacciones.Campo_Disponible + " = " + Disponible_Stock + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Producto_ID + "'";
                                Cmd.CommandText = Mi_SQL;
                                Cmd.ExecuteNonQuery();
                            }
                        }
                        Mensaje = "EXITO";
                        if (Requisicion_Negocio.P_Estatus != "CANCELADA") {
                            //comprometer
                            if (Dt_Productos_Almacen != null) {
                                Int32 Disponible_Stock = 0;
                                Int32 Comprometido_Stock = 0;
                                Int32 Cantidad = 0;
                                String Producto_ID = "";
                                foreach (DataRow Producto_Requisicion in Requisicion_Negocio.P_Dt_Productos_Servicios.Rows) {
                                    Cantidad = int.Parse(Producto_Requisicion["CANTIDAD"].ToString().Trim());
                                    Producto_ID = Producto_Requisicion["REFACCION_ID"].ToString().Trim();
                                    DataRow[] Dr_Productos_Almacen = Dt_Productos_Almacen.Select("REFACCION_ID ='" + Producto_ID + "'");
                                    Disponible_Stock = int.Parse(Dr_Productos_Almacen[0]["DISPONIBLE"].ToString().Trim());
                                    Comprometido_Stock = int.Parse(Dr_Productos_Almacen[0]["COMPROMETIDO"].ToString().Trim());
                                    if (Disponible_Stock >= Cantidad) {
                                        //Compromete
                                        Disponible_Stock = Disponible_Stock - Cantidad;
                                        Comprometido_Stock = Comprometido_Stock + Cantidad;
                                        Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " SET " + Cat_Tal_Refacciones.Campo_Comprometido + " = " + Comprometido_Stock 
                                            + ", " + Cat_Tal_Refacciones.Campo_Disponible + " = " + Disponible_Stock + "WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Producto_ID + "'";
                                        Cmd.CommandText = Mi_SQL;
                                        Cmd.ExecuteNonQuery(); 
                                        Mensaje = "EXITO";
                                    } else {
                                        //No compromete y manda mensaje
                                        Mensaje = "CANTIDAD INSUFICIENTE DEL PRODUCTO " + Dr_Productos_Almacen[0]["NOMBRE"].ToString().Trim() + ", " +
                                        Dr_Productos_Almacen[0]["DESCRIPCION"].ToString().Trim() + ", DEBIDO A QUE SE COMPROMETIO EN UNA REQUISICION" +
                                        " ANTERIOR A LA DE USTED. DISPONIBLE[" + Disponible_Stock + "]";
                                    }
                                }
                            }//fin de for comprometer
                        }//fin de if cancelar stock
                    } else {                   
                        Mensaje = "EXITO";
                    }
                    if (Mensaje == "EXITO") {
                        //Mensaje += //"-" + Requisicion_Negocio.P_Requisicion_ID;
                        Registrar_Historial(Requisicion_Negocio.P_Estatus, Requisicion_Negocio.P_Requisicion_ID, ref Cmd);
                        //Guardar Comentarios
                        if (Requisicion_Negocio.P_Comentarios.Length > 0) {
                            Cls_Ope_Tal_Administrar_Requisiciones_Negocio Administrar_Requisicion = new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
                            Administrar_Requisicion.P_Requisicion_ID = Requisicion_Negocio.P_Requisicion_ID;
                            Administrar_Requisicion.P_Comentario = Requisicion_Negocio.P_Comentarios;
                            Administrar_Requisicion.P_Estatus = Requisicion_Negocio.P_Estatus;
                            Administrar_Requisicion.P_Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                            Administrar_Requisicion.Alta_Observaciones(ref Cmd);
                        }

                        if (!String.IsNullOrEmpty(No_Reserva) && !No_Reserva.Equals("0")) {
                            if (Requisicion_Negocio.P_Tipo == "TRANSITORIA") 
                            {
                                if (Requisicion_Negocio.P_Estatus == "CANCELADA") 
                                {
                                    Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Dt_Partidas_Reserva, Cmd);
                                    //Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Anio]), Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Convert.ToDouble(Requisicion_Negocio.P_Total), ref Cmd);
                                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(No_Reserva, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Convert.ToDouble(Requisicion_Negocio.P_Total), "", "", "", "", Cmd);

                                    Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas
                                            + " SET " + Ope_Psp_Reservas.Campo_Estatus + " = 'CANCELADA'"
                                            + " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + No_Reserva.Trim() + "'";
                                    Cmd.CommandText = Mi_SQL;
                                    Cmd.ExecuteNonQuery(); 
                                } 
                                else 
                                {
                                    if (Requisicion_Negocio.P_Modificar_RQ)
                                    {
                                        Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Dt_Partidas_Reserva, Cmd);
                                        //Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Dependencia_ID].ToString(), Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString(), Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString(), Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString(), Convert.ToInt32(Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Anio]), Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Convert.ToDouble(Requisicion_Negocio.P_Total), ref Cmd);
                                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(No_Reserva, Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Convert.ToDouble(Requisicion_Negocio.P_Total), "", "", "", "", Cmd);
                                    }
                                }
                            } 
                            else 
                            {
                                if (Requisicion_Negocio.P_Estatus == "CANCELADA") 
                                {
                                    Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Dt_Partidas_Reserva, Cmd);
                                    //Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Dependencia_ID].ToString(), Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Fte_Financimiento_ID].ToString(), Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Proyecto_Programa_ID].ToString(), Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas_Detalles.Campo_Partida_ID].ToString(), Convert.ToInt32(Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Anio]), Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Cls_Ope_Psp_Manejo_Presupuesto.COMPROMETIDO, Convert.ToDouble(Requisicion_Negocio.P_Total), ref Cmd);
                                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(No_Reserva, Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE, Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO, Convert.ToDouble(Requisicion_Negocio.P_Total), "", "", "", "");
                                }
                            }
                            Trans.Commit();
                        }
                    } else {
                        Mensaje = "SIN ACTUALIZACION";
                    }
                } catch (Exception ex) {
                    Trans.Rollback();
                    Mensaje = ex.ToString();
                    throw new Exception(ex.ToString());
                } finally {
                    Cn.Close();
                }
                return Mensaje;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Num_Reserva
            ///DESCRIPCIÓN: Consulta el Numero de Reserva de una RQ
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static Int32 Consultar_Num_Reserva(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                int Num_Reserva = 0;
                try {
                    String Mi_Sql = "";
                    Mi_Sql = "SELECT " + Ope_Tal_Requisiciones.Campo_Numero_Reserva +" FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                        " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = " + Requisicion_Negocio.P_Requisicion_ID;
                    Object Objeto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                    Num_Reserva = int.Parse(Objeto.ToString());
                } catch (Exception Ex) {
                    Ex.ToString();
                }
                return Num_Reserva;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Actualizar_Requisicion
            ///DESCRIPCIÓN: Actualiza una RQ
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            private static void Actualizar_Requisicion(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                String Mi_SQL = "";
                //ACTUALIZAR LA REQUISICION
                Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                " SET " +  
                Ope_Tal_Requisiciones.Campo_Estatus + " = '" + Requisicion_Negocio.P_Estatus + "', " +
                Ope_Tal_Requisiciones.Campo_Subtotal + " = " + Requisicion_Negocio.P_Subtotal + ", " +
                Ope_Tal_Requisiciones.Campo_IVA + " = " + Requisicion_Negocio.P_IVA + ", " +
                Ope_Tal_Requisiciones.Campo_IEPS + " = " + Requisicion_Negocio.P_IEPS + ", " +
                Ope_Tal_Requisiciones.Campo_Total + " = " + Requisicion_Negocio.P_Total + ", " +
                Ope_Tal_Requisiciones.Campo_Usuario_Modifico + " = '" + Usuario + "', " +
                Ope_Tal_Requisiciones.Campo_Fecha_Modifico + " = GETDATE(), " +
                Ope_Tal_Requisiciones.Campo_Justificacion_Compra + " = '" + Requisicion_Negocio.P_Justificacion_Compra + "'," +
                Ope_Tal_Requisiciones.Campo_Especificacion_Prod_Serv + " = '" + Requisicion_Negocio.P_Especificacion_Productos + "'," +
                Ope_Tal_Requisiciones.Campo_Verificaion_Entrega + "= '" + Requisicion_Negocio.P_Verificacion_Entrega + "'";
                if (Requisicion_Negocio.P_Estatus == "GENERADA") {
                    Mi_SQL = Mi_SQL + "," +
                    Ope_Tal_Requisiciones.Campo_Empleado_Generacion_ID + " = '" + Cls_Sessiones.Empleado_ID.ToString() + "'," +
                    Ope_Tal_Requisiciones.Campo_Fecha_Generacion + " = GETDATE()";
                } else if (Requisicion_Negocio.P_Estatus == "CANCELADA") {
                    Mi_SQL = Mi_SQL + "," +
                    Ope_Tal_Requisiciones.Campo_Empleado_Cancelada_ID + " = '" + Cls_Sessiones.Empleado_ID.ToString() + "'," +
                    Ope_Tal_Requisiciones.Campo_Fecha_Cancelada + " = GETDATE()";
                }
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = '" + Requisicion_Negocio.P_Requisicion_ID + "'";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Registrar_Historial
            ///DESCRIPCIÓN: REgistra el historial de una RQ
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static bool Registrar_Historial(String Estatus, String No_Requisicion) {
                No_Requisicion = No_Requisicion.Replace("RQ-","");
                bool Resultado = false;
                try {
                    int Consecutivo = Obtener_Consecutivo(Ope_Tal_Req_Historial.Campo_No_Historial, Ope_Tal_Req_Historial.Tabla_Ope_Tal_Req_Historial);
                    String Mi_SQL = "";
                    Mi_SQL = "INSERT INTO " + Ope_Tal_Req_Historial.Tabla_Ope_Tal_Req_Historial + " (" +
                        Ope_Tal_Req_Historial.Campo_No_Historial + "," +
                        Ope_Tal_Req_Historial.Campo_No_Requisicion + "," +
                        Ope_Tal_Req_Historial.Campo_Estatus + "," +
                        Ope_Tal_Req_Historial.Campo_Fecha + "," +
                        Ope_Tal_Req_Historial.Campo_Empleado + "," +
                        Ope_Tal_Req_Historial.Campo_Usuario_Creo + "," +
                        Ope_Tal_Req_Historial.Campo_Fecha_Creo + ") VALUES (" +
                        Consecutivo + ", " +
                        No_Requisicion + ", '" +
                        Estatus + "', " +
                        "GETDATE(), '" +
                        Cls_Sessiones.Nombre_Empleado + "', '" +
                        Cls_Sessiones.Nombre_Empleado + "', " +
                        "GETDATE())";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    Resultado = true;
                } catch (Exception Ex)  {
                    String Str_Ex = Ex.ToString();
                    Resultado = false;
                }
                return Resultado;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Registrar_Historial
            ///DESCRIPCIÓN: REgistra el historial de una RQ
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static Boolean Registrar_Historial(String Estatus, String No_Requisicion, ref SqlCommand P_Cmd) {
                No_Requisicion = No_Requisicion.Replace("RQ-","").Trim();
                String Mi_SQL = "";
                int Consecutivo = 0;
                Boolean Resultado = false;
                Object Obj = null;
                try {
                    //Consecutivo= Obtener_Consecutivo(Ope_Tal_Req_Historial.Campo_No_Historial, Ope_Tal_Req_Historial.Tabla_Ope_Tal_Req_Historial);
                    Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Tal_Req_Historial.Campo_No_Historial + "),0) FROM " + Ope_Tal_Req_Historial.Tabla_Ope_Tal_Req_Historial;
                    //Ejecutar consulta del consecutivo
                    P_Cmd.CommandText = Mi_SQL;
                    Obj = P_Cmd.ExecuteScalar();

                    //Verificar si no es nulo
                    if (Convert.IsDBNull(Obj) == false)
                    {
                        Consecutivo = (Convert.ToInt32(Obj) + 1);
                    }
                    else
                        Consecutivo = 1;
                    
                    Mi_SQL = "INSERT INTO " + Ope_Tal_Req_Historial.Tabla_Ope_Tal_Req_Historial + " (" +
                        Ope_Tal_Req_Historial.Campo_No_Historial + "," +
                        Ope_Tal_Req_Historial.Campo_No_Requisicion + "," +
                        Ope_Tal_Req_Historial.Campo_Estatus + "," +
                        Ope_Tal_Req_Historial.Campo_Fecha + "," +
                        Ope_Tal_Req_Historial.Campo_Empleado + "," +
                        Ope_Tal_Req_Historial.Campo_Usuario_Creo + "," +
                        Ope_Tal_Req_Historial.Campo_Fecha_Creo + ") VALUES (" +
                        Consecutivo + ", " +
                        No_Requisicion + ", '" +
                        Estatus + "', " +
                        "GETDATE(), '" +
                        Cls_Sessiones.Nombre_Empleado + "', '" +
                        Cls_Sessiones.Nombre_Empleado + "', " +
                        "GETDATE())";
                    P_Cmd.CommandText = Mi_SQL;
                    P_Cmd.ExecuteNonQuery();
                    Resultado = true;
                } catch (Exception Ex)  {
                    String Str_Ex = Ex.ToString();
                    Resultado = false;
                }
                return Resultado;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Historial_Requisicion
            ///DESCRIPCIÓN: Consulta el historial de una RQ
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static DataTable Consultar_Historial_Requisicion(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                try {
                    String Mi_SQL = "";
                    Mi_SQL =
                    "SELECT * FROM " + Ope_Tal_Req_Historial.Tabla_Ope_Tal_Req_Historial +
                    " WHERE " + Ope_Tal_Req_Historial.Campo_No_Requisicion + " = " +
                    Requisicion_Negocio.P_Requisicion_ID +
                    " ORDER BY " + Ope_Tal_Req_Historial.Campo_Fecha + " ASC";
                    DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                    return Data_Table;
                } catch (Exception ex) {
                    return null;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Verificar_Rango_Caja_Chica
            ///DESCRIPCIÓN: ---
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static Boolean Verificar_Rango_Caja_Chica(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                bool Respuesta = false;
                double Monto_Inicio = 0;
                double Monto_Fin = 0;
                String Mi_Sql = "";
                Mi_Sql = "SELECT " + Cat_Com_Monto_Proceso_Compra.Campo_Monto_Compra_Directa_Ini + ", " +
                    Cat_Com_Monto_Proceso_Compra.Campo_Monto_Compra_Directa_Fin + " FROM " +
                    Cat_Com_Monto_Proceso_Compra.Tabla_Cat_Com_Monto_Proceso_Compra +
                    " WHERE " + Cat_Com_Monto_Proceso_Compra.Campo_Tipo + " = '" + Requisicion_Negocio.P_Tipo_Articulo + "'";
                DataSet _DSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                DataTable Dt_Tmp = null;
                if (_DSet != null && _DSet.Tables.Count > 0 && _DSet.Tables[0].Rows.Count > 0) {
                    Dt_Tmp = _DSet.Tables[0];
                    Monto_Inicio = Convert.ToDouble(Dt_Tmp.Rows[0]["MONTO_COMPRA_DIRECTA_INI"].ToString().Trim());
                    Monto_Fin = Convert.ToDouble(Dt_Tmp.Rows[0]["MONTO_COMPRA_DIRECTA_FIN"].ToString().Trim());
                    double Total = Convert.ToDouble(Requisicion_Negocio.P_Total);
                    Respuesta = Total >= Monto_Inicio && Total <= Monto_Fin ? true : false;
                }
                return Respuesta;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Asignar_Reserva
            ///DESCRIPCIÓN: Asigna una reserva manual a la RQ
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static Int32 Asignar_Reserva(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                int Registros = 0;
                try {
                    String Mi_Sql = "";
                    Mi_Sql = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                        " SET NO_RESERVA ='" + Requisicion_Negocio.P_No_Reserva + "'," +
                        Ope_Tal_Requisiciones.Campo_Estatus + " = '" + Requisicion_Negocio.P_Estatus + "'" + 
                        " WHERE " +
                        Ope_Tal_Requisiciones.Campo_Folio + " ='" + Requisicion_Negocio.P_Folio + "'";
                    Registros = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);                                
                } catch(Exception Ex) {
                    Ex.ToString();
                    Registros = 0;
                }
                if (Registros > 0) Registrar_Historial("ALMACEN", Requisicion_Negocio.P_Folio);
                return Registros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Rechaza_Contabilidad
            ///DESCRIPCIÓN: Regresa la RQ rechazada por Contabilidad por falta de Presupuesto
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static Int32 Rechaza_Contabilidad(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                Int32 Registros = 0;
                try {
                    String Mi_Sql = "";
                    Mi_Sql = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                        " SET " + Ope_Tal_Requisiciones.Campo_Estatus + " = '" + Requisicion_Negocio.P_Estatus + "'," +
                        " ALERTA ='ROJA' WHERE " + Ope_Tal_Requisiciones.Campo_Folio + " ='" + Requisicion_Negocio.P_Folio + "'";
                    Registros = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                    if (Registros > 0) {
                        Cls_Ope_Tal_Administrar_Requisiciones_Negocio Admin_Req = new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
                        Admin_Req.P_Requisicion_ID = Requisicion_Negocio.P_Requisicion_ID;
                        Admin_Req.P_Comentario = Requisicion_Negocio.P_Comentarios;
                        Admin_Req.P_Estatus = Requisicion_Negocio.P_Estatus;
                        Admin_Req.Alta_Observaciones();
                    }
                } catch (Exception Ex) {
                    Ex.ToString();
                    Registros = 0;
                }
                if (Registros > 0) Registrar_Historial(Requisicion_Negocio.P_Estatus, Requisicion_Negocio.P_Folio);
                return Registros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Actualizar_Requisicion_Estatus
            ///DESCRIPCIÓN: Actualiza el Estatus de una RQ
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static int Actualizar_Requisicion_Estatus(Cls_Ope_Tal_Requisiciones_Negocio Negocio) {
                String Mi_Sql = "";
                Mi_Sql = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                " SET " + Ope_Tal_Requisiciones.Campo_Estatus + " = '" + Negocio.P_Estatus + "' " +
                " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = " + Negocio.P_Requisicion_ID;
                int Registros_Afectados = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                return Registros_Afectados;
            }

        #endregion

        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************
        #region CONSULTAS / FTE FINANCIAMIENTO, PROYECTOS, PARTIDAS, PRESUPUESTOS

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Fuentes_Financiamiento
            ///DESCRIPCIÓN: Consulta las Fuentes de Financiamiento
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static DataTable Consultar_Fuentes_Financiamiento(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio){
                try {
                    String Mi_SQL = "";
                    Mi_SQL = "SELECT FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + "," +
                    " FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+" +
                    " FUENTE." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS DESCRIPCION" + 
                    " FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FUENTE" + 
                    " WHERE FUENTE." + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID + " IN " +
                    "(SELECT " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID +
                    " FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado +
                    " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = " +
                    "'" + Requisicion_Negocio.P_Dependencia_ID + "')" +
                    " ORDER BY DESCRIPCION ASC";
                    DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                    return Data_Table;
                } catch (Exception ex) {
                    return null;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Proyectos_Programas
            ///DESCRIPCIÓN: Consulta Proyectos y Programas
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            //GUS  
            public static DataTable Consultar_Proyectos_Programas(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Mi_SQL = "";
                Mi_SQL = "SELECT PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + "," +
                " PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Clave + " +' '+" +
                " PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Nombre + "," +
                " PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Elemento_PEP +
                " FROM " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + " PROGRAMA" +            
                " JOIN " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia + " DETALLE" +
                " ON PROGRAMA." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + " = " +
                " DETALLE." + Cat_SAP_Det_Prog_Dependencia.Campo_Proyecto_Programa_ID +
                " WHERE DETALLE." + Cat_SAP_Det_Prog_Dependencia.Campo_Dependencia_ID + " = " +
                "'" + Requisicion_Negocio.P_Dependencia_ID + "' ORDER BY " + 
                Cat_Com_Proyectos_Programas.Campo_Descripcion + " ASC";
                DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Data_Table;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Partidas_De_Un_Programa
            ///DESCRIPCIÓN: Consulta las Partidas de un Programa
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            //CONSULTAR PARTIDAS DE UN PROGRAMA
            public static DataTable Consultar_Partidas_De_Un_Programa(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Mi_SQL = "SELECT PARTIDA." + Cat_Com_Partidas.Campo_Partida_ID + ", " +
                " PARTIDA." + Cat_Com_Partidas.Campo_Clave +" +' '+" +
                " PARTIDA." + Cat_Com_Partidas.Campo_Nombre +
                " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas + " PARTIDA" +
                " JOIN " + Cat_Sap_Det_Prog_Partidas.Tabla_Cat_Sap_Det_Prog_Partidas + " DETALLE" +
                " ON PARTIDA." + Cat_Com_Partidas.Campo_Partida_ID + " = " +
                " DETALLE." + Cat_Sap_Det_Prog_Partidas.Campo_Det_Partida_ID +
                " WHERE DETALLE." + Cat_Sap_Det_Prog_Partidas.Campo_Det_Proyecto_Programa_ID + " = " +
                "'" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'";
                if (Requisicion_Negocio.P_Tipo == "STOCK") {
                    Mi_SQL = Mi_SQL + " AND " + " PARTIDA." + Cat_Com_Partidas.Campo_Partida_ID + " IN " +
                        "(SELECT DISTINCT(" + Cat_Com_Productos.Campo_Partida_ID + ") FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                        " WHERE " + Cat_Com_Productos.Campo_Stock + " = 'SI')";
                }
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Com_Partidas.Campo_Nombre + " ASC";
                DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Data_Table;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Presupuesto_Partidas
            ///DESCRIPCIÓN: Consulta el Presupuesto en las Partidas
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            //OBTINE PARTIDAS ESPECIFICAS CON PRESPUESTOS A PARTIR DE LA DEPENDENCIA Y EL PROYECTO
            public static DataTable Consultar_Presupuesto_Partidas(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Mi_SQL = "";

                Mi_SQL = 
                "SELECT " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ", " +
                    "(SELECT " + Cat_Com_Partidas.Campo_Nombre + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                    " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " + 
                    Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + 
                    Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") NOMBRE, " +

                    "(SELECT " + Cat_Com_Partidas.Campo_Clave + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                    " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " +
                    Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                    Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") CLAVE, " +

                    "(SELECT " + Cat_Com_Partidas.Campo_Clave + " +' '+" + 
                    Cat_Com_Partidas.Campo_Nombre + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                    " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " +
                    Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                    Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") CLAVE_NOMBRE, " +

                Cat_Com_Dep_Presupuesto.Campo_Monto_Presupuestal + ", " +
                Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " MONTO_DISPONIBLE, " +
                Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + ", " +
                Cat_Com_Dep_Presupuesto.Campo_Monto_Ejercido + ", " +
                Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto + ", " +
                Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ", " +
                Cat_Com_Dep_Presupuesto.Campo_Fecha_Creo + 
                //" TO_CHAR(FECHA_CREO ,'DD/MM/YY') FECHA_CREO" + 
                " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID + 
                " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
                " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + 
                " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
                " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
                " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +
                " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                " IN (" + Requisicion_Negocio.P_Partida_ID + ")" +
                " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +
                " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + " = " +
                    "(SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
                    " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + 
                    " WHERE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                                Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                                " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                                Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
                                " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID + 
                                " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +

                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                                " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +

                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                                " IN (" + Requisicion_Negocio.P_Partida_ID + "))";

                DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Data_Table;
            }

        #endregion

        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************
        #region MANEJO DE PRESUPUESTOS

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Comprometer_Presupuesto_Partidas_Usadas_En_Requisicion
            ///DESCRIPCIÓN: Comprometere el Prespuesto en Partidas de la RQ
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            //Si se usa 31 mar 2011
            public static int Comprometer_Presupuesto_Partidas_Usadas_En_Requisicion(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Mi_Sql = "";
                double Comprometido = 0;
                double Disponible = 0;
                int Registros_Afectados = 0;
                String Partida_ID = "";
                try {
                    if (Requisicion_Negocio.P_Dt_Partidas != null && Requisicion_Negocio.P_Dt_Partidas.Rows.Count > 0) {
                        foreach (DataRow Renglon_Partida in Requisicion_Negocio.P_Dt_Partidas.Rows) {
                            Partida_ID = Renglon_Partida["PARTIDA_ID"].ToString().Trim();
                            Comprometido = double.Parse(Renglon_Partida["MONTO_COMPROMETIDO"].ToString().Trim());
                            Disponible = double.Parse(Renglon_Partida["MONTO_DISPONIBLE"].ToString().Trim());
                            Mi_Sql =
                            "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                            " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido +
                            " = " + Comprometido + ", " +
                            Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible +
                            " = " + Disponible +
                            " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                            " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
                            " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
                            " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                            " = '" + Partida_ID + "'" +

                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                            " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + " = " +
                                "(SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
                                " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                                " WHERE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                                            Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                                            " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
                                            " AND " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                                            Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
                                            " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
                                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
                                            " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +

                                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                                            " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +

                                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                                            " = '" + Partida_ID + "')";
                            Registros_Afectados = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                        }
                    }
                } catch (Exception Ex) {
                    String Str = Ex.ToString();
                    throw new Exception(Str);
                }
                return Registros_Afectados;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Descomprometer_Presupuesto_Partidas_Usadas_En_Requisicion
            ///DESCRIPCIÓN:Descompromete el Presupuesto en las Partidas usadas en RQ
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            //Descomprometer presup
            public static int Descomprometer_Presupuesto_Partidas_Usadas_En_Requisicion(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio){//no funciona
                String Mi_Sql = "";
                double Comprometido = 0;
                double Disponible = 0;
                int Registros_Afectados = 0;
                String Partida_ID = "";
                try {
                    if (Requisicion_Negocio.P_Dt_Partidas != null && Requisicion_Negocio.P_Dt_Partidas.Rows.Count > 0)  {
                        foreach (DataRow Renglon_Partida in Requisicion_Negocio.P_Dt_Partidas.Rows) {
                            Partida_ID = Renglon_Partida["PARTIDA_ID"].ToString().Trim();
                            Comprometido = double.Parse(Renglon_Partida["MONTO_COMPROMETIDO"].ToString().Trim());
                            Disponible = double.Parse(Renglon_Partida["MONTO_DISPONIBLE"].ToString().Trim());
                            Mi_Sql =
                            "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                            " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido +
                            " = " + Comprometido + ", " +
                            Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible +
                            " = " + Disponible +
                            " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                            " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
                            " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
                            " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                            " = '" + Partida_ID + "'" +

                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                            " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + " = " +
                                "(SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
                                " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                                " WHERE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                                            Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                                            " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
                                            " AND " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
                                            Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
                                            " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
                                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
                                            " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +

                                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                                            " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +

                                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                                            " = '" + Partida_ID + "')";
                            Registros_Afectados =
                                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                        }
                    }
                } catch (Exception Ex) {
                    String Str = Ex.ToString();
                    throw new Exception(Str);
                }
                return Registros_Afectados;
            }
        
        #endregion

        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************
        #region MANEJO DE PRODUCTOS STOCK / COMPROMETER, DESCOMPROMETER

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Comprometer_Productos_Stock
            ///DESCRIPCIÓN: Compromete los productos de Stock
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            //Si se usa 31 mar 2011
            public static int Comprometer_Productos_Stock(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                int Registros_Afectados = 0;
                try {
                    String Mensaje = "";
                    DataTable Dt_Productos_Almacen = null;
                    String Mi_SQL = "";
                    Mi_SQL =
                    "SELECT " + Cat_Com_Productos.Campo_Producto_ID + "," + Cat_Com_Productos.Campo_Existencia + "," +
                        Cat_Com_Productos.Campo_Disponible + "," + Cat_Com_Productos.Campo_Comprometido +
                        " FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                        " WHERE " + Cat_Com_Productos.Campo_Stock + " = 'SI'";
                    DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0) {
                        Dt_Productos_Almacen = Data_Set.Tables[0];
                    }
                    if (Dt_Productos_Almacen != null) {
                        int Disponible_Stock = 0;
                        int Comprometido_Stock = 0;
                        int Cantidad = 0;
                        String Producto_ID = "";
                        foreach (DataRow Producto_Requisicion in Requisicion_Negocio.P_Dt_Productos_Servicios.Rows) {
                            Cantidad = int.Parse(Producto_Requisicion["CANTIDAD"].ToString().Trim());
                            Producto_ID = Producto_Requisicion["PROD_SERV_ID"].ToString().Trim();
                            DataRow[] Dr_Productos_Almacen = Dt_Productos_Almacen.Select("PRODUCTO_ID ='" + Producto_ID + "'");
                            Disponible_Stock = int.Parse(Dr_Productos_Almacen[0]["DISPONIBLE"].ToString().Trim());
                            Comprometido_Stock = int.Parse(Dr_Productos_Almacen[0]["COMPROMETIDO"].ToString().Trim());
                            Disponible_Stock = Disponible_Stock - Cantidad;
                            Comprometido_Stock = Comprometido_Stock + Cantidad;
                            Mi_SQL = "UPDATE " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                                " SET " + Cat_Com_Productos.Campo_Comprometido +
                                " = " + Comprometido_Stock + ", " +
                                Cat_Com_Productos.Campo_Disponible +
                                " = " + Disponible_Stock + "WHERE " +
                                Cat_Com_Productos.Campo_Producto_ID + " = '" +
                                Producto_ID + "'";
                            Registros_Afectados = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        }
                    }
                } catch (Exception Ex) {
                    String Str = Ex.ToString();
                    throw new Exception(Str);
                }
                return Registros_Afectados;
            }

        #endregion

        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************
        #region BUSQUEDA DE PRODUCTOS Y DETALLES

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Poducto_Por_ID
            ///DESCRIPCIÓN: Consulta Un Producto por su Identificador
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static DataTable Consultar_Poducto_Por_ID(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Mi_SQL = "SELECT " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +".*," +
                Cat_Tal_Refacciones.Campo_Nombre + "  +', '+" + Cat_Tal_Refacciones.Campo_Descripcion + " AS NOMBRE_DESCRIPCION" +
                ", (SELECT " + Cat_Com_Unidades.Campo_Abreviatura + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " WHERE " +
                    Cat_Com_Unidades.Campo_Unidad_ID + " = " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." +
                    Cat_Tal_Refacciones.Campo_Unidad_ID + ") AS UNIDAD " +
                " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID +
                " IN (" + Requisicion_Negocio.P_Producto_ID + ")";
                DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0) {
                    return (Data_Set.Tables[0]);
                } else {
                    return null;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Productos
            ///DESCRIPCIÓN: Consulta de Productos
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            //BUSQUEDA EN MODAL POPUP DE UN PRODUCTO
            public static DataTable Consultar_Productos(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Mi_SQL = "";
                Mi_SQL = "SELECT " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " AS ID, " +
                Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + ".*, " +
                Cat_Tal_Refacciones.Campo_Nombre + "  +'; '+ " + Cat_Tal_Refacciones.Campo_Descripcion + " AS NOMBRE_DESCRIPCION " +
                ", " + Cat_Tal_Refacciones.Campo_Costo_Unitario + " AS COSTO" +
                ",(SELECT " + Cat_Com_Unidades.Campo_Abreviatura + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID + " = " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Unidad_ID + ") AS UNIDAD" +
                ",'' AS MODELO" +
                " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                " WHERE " + Cat_Tal_Refacciones.Campo_Tipo + " = '" + Requisicion_Negocio.P_Tipo + "'" +
                " AND " + Cat_Tal_Refacciones.Campo_Estatus + " = 'VIGENTE'";
                if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Partida_ID)) {
                    Mi_SQL = Mi_SQL + " AND " + Cat_Tal_Refacciones.Campo_Partida_ID + " = '" + Requisicion_Negocio.P_Partida_ID + "'";
                }
                if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Nombre_Producto_Servicio)) {
                    Mi_SQL = Mi_SQL + " AND (UPPER(" + Cat_Tal_Refacciones.Campo_Nombre + ") LIKE UPPER('%" + Requisicion_Negocio.P_Nombre_Producto_Servicio + "%') ";
                    Mi_SQL = Mi_SQL + " OR UPPER(" + Cat_Tal_Refacciones.Campo_Descripcion + ") LIKE UPPER('%" + Requisicion_Negocio.P_Nombre_Producto_Servicio + "%') )";
                }
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Tal_Refacciones.Campo_Nombre + " ASC";
                DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Data_Table;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Impuesto
            ///DESCRIPCIÓN: Hace Consulta de los Presupuestos
            ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************
            public static DataSet Consultar_Impuesto(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Mi_SQL = "SELECT " + Cat_Com_Impuestos.Campo_Nombre + ", " +
                    Cat_Com_Impuestos.Campo_Porcentaje_Impuesto + " FROM " +
                    Cat_Com_Impuestos.Tabla_Cat_Impuestos +
                    " WHERE " + Cat_Com_Impuestos.Campo_Impuesto_ID + " = '" + Requisicion_Negocio.P_Impuesto_ID + "'";
                DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                return Data_Set;
            }


        #endregion

        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************                               
        #region CONSULTA REQUISICIONES

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Proceso_Filtrar
            ///DESCRIPCIÓN: Filtra una RQ
            ///PARAMETROS: 1.-Clase de Negocio
            ///            2.-Usuario que crea la requisa
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///*******************************************************************************         
            public static Boolean Proceso_Filtrar(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                Boolean Actualizado = false;
                try {
                    String Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                        " SET " + Ope_Tal_Requisiciones.Campo_Estatus + " = '" + Requisicion_Negocio.P_Estatus + "'," +
                        Ope_Tal_Requisiciones.Campo_Fecha_Filtrado + " = GETDATE()" +
                        " WHERE " + Ope_Tal_Requisiciones.Campo_Folio + " = '" + Requisicion_Negocio.P_Folio + "'";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    Actualizado = true;
                } catch (Exception Ex) {
                    String Str = Ex.ToString();
                    Actualizado = false;
                }
                return Actualizado;
            }
        
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Requisiciones
            ///DESCRIPCIÓN: crea una sentencia sql para consultar una Requisa en la base de datos
            ///PARAMETROS: 1.-Clase de Negocio
            ///            2.-Usuario que crea la requisa
            ///CREO: Silvia Morales Portuhondo
            ///FECHA_CREO: Noviembre/2010 
            ///MODIFICO:Gustavo Angeles Cruz
            ///FECHA_MODIFICO: 25/Ene/2011
            ///CAUSA_MODIFICACIÓN
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///******************************************************************************* 
            public static DataTable Consultar_Requisiciones(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                //Requisicion_Negocio.P_Fecha_Inicial = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Inicial));
                //Requisicion_Negocio.P_Fecha_Final = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Requisicion_Negocio.P_Fecha_Final));
                if (Requisicion_Negocio.P_Estatus == "CONST ,GENERADA, REVISAR") {
                    Requisicion_Negocio.P_Estatus = "EN CONSTRUCCION,GENERADA,REVISAR,RECHAZADA";
                }
                Requisicion_Negocio.P_Estatus = Requisicion_Negocio.P_Estatus.Replace(",","','");
                Requisicion_Negocio.P_Tipo = Requisicion_Negocio.P_Tipo.Replace(",","','");
                String Mi_Sql = "";
                Mi_Sql = "SELECT " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + ".*, " +
                    "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                    " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                    ") NOMBRE_DEPENDENCIA FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                " WHERE " + Ope_Tal_Requisiciones.Campo_Estatus + " IN ('" + Requisicion_Negocio.P_Estatus + "')" +
                " AND " + Ope_Tal_Requisiciones.Campo_Tipo + " IN ('" + Requisicion_Negocio.P_Tipo + "')";
                if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Dependencia_ID) && Requisicion_Negocio.P_Dependencia_ID != "0") {
                    Mi_Sql += " AND " + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                    " = '" + Requisicion_Negocio.P_Dependencia_ID + "'";
                }
                if (Requisicion_Negocio.P_Estatus == "GENERADA") {
                    Mi_Sql = Mi_Sql + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Generacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + "'"
                                    + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Generacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
                } else if (Requisicion_Negocio.P_Estatus == "CANCELADA") {
                    Mi_Sql = Mi_Sql + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Cancelada + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + "'"
                                    + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Cancelada + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
                } else if (Requisicion_Negocio.P_Estatus == "EN CONSTRUCCION") {
                    Mi_Sql = Mi_Sql + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Construccion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + "'"
                                    + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Construccion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
                } else if (Requisicion_Negocio.P_Estatus == "AUTORIZADA") {
                    Mi_Sql = Mi_Sql + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Autorizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + "'"
                                    + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Autorizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
                } else if (Requisicion_Negocio.P_Estatus == "CONFIRMADA") {
                    Mi_Sql = Mi_Sql + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Confirmacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + "'"
                                    + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Confirmacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
                } else if (Requisicion_Negocio.P_Estatus == "COTIZADA") {
                    Mi_Sql = Mi_Sql + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Cotizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + "'"
                                    + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Cotizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
                } else if (Requisicion_Negocio.P_Estatus == "REVISAR") {
                    Mi_Sql = Mi_Sql + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Autorizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + "'"
                                    + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Autorizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
                } else if (Requisicion_Negocio.P_Estatus == "COMPRA") {
                    Mi_Sql = Mi_Sql + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Cotizacion + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + "'"
                                    + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Cotizacion + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
                } else {
                    Mi_Sql = Mi_Sql + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Creo + " >= '" + Requisicion_Negocio.P_Fecha_Inicial + "'"
                                   + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Creo + " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
                }
                if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Requisicion_ID)) {
                    Mi_Sql += " AND " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = " + Requisicion_Negocio.P_Requisicion_ID.Trim();
                }
                if (Requisicion_Negocio.P_Cotizador_ID != null && Requisicion_Negocio.P_Cotizador_ID != "0") {
                    Mi_Sql += " AND " + Ope_Tal_Requisiciones.Campo_Cotizador_ID + " = '" + Requisicion_Negocio.P_Cotizador_ID.Trim() + "'";
                }
                Mi_Sql = Mi_Sql + " ORDER BY " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " DESC";
                DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0) {
                    return (Data_Set.Tables[0]);
                } else {
                    return null;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Requisiciones_Generales
            ///DESCRIPCIÓN: crea una sentencia sql para consultar una Requisa en la base de datos
            ///PARAMETROS: 1.-Clase de Negocio
            ///            2.-Usuario que crea la requisa
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///******************************************************************************* 
            public static DataTable Consultar_Requisiciones_Generales(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                if (Requisicion_Negocio.P_Tipo == "TODOS") {
                    Requisicion_Negocio.P_Tipo = "STOCK','TRANSITORIA";
                }
                 String Mi_Sql = "";
                Mi_Sql = "SELECT " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + ".*, " +
                    "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                    " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." +Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                    ") NOMBRE_DEPENDENCIA " +
                " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                " WHERE " +
                Ope_Tal_Requisiciones.Campo_Tipo + " IN ('" + Requisicion_Negocio.P_Tipo + "')" +
                " AND  " + Ope_Tal_Requisiciones.Campo_Fecha_Creo + " " +
                            " >= '" + Requisicion_Negocio.P_Fecha_Inicial + "' AND " +
                    " " + Ope_Tal_Requisiciones.Campo_Fecha_Creo + " " +
                            " <= '" + Requisicion_Negocio.P_Fecha_Final + "'";
                if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Dependencia_ID) && Requisicion_Negocio.P_Dependencia_ID != "0") {
                    Mi_Sql = Mi_Sql + " AND " + Ope_Tal_Requisiciones.Campo_Dependencia_ID + " = " +
                        "'" + Requisicion_Negocio.P_Dependencia_ID + "'";
                }            
                if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Requisicion_ID)) {
                    Mi_Sql +=
                    " AND " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                    " = " + Requisicion_Negocio.P_Requisicion_ID;
                }
                if (!string.IsNullOrEmpty(Requisicion_Negocio.P_Estatus)) {
                    Mi_Sql +=
                    " AND " + Ope_Tal_Requisiciones.Campo_Estatus +
                    " IN ('" + Requisicion_Negocio.P_Estatus + "')";
                }

                Mi_Sql = Mi_Sql + " ORDER BY " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " DESC";
                DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0) {
                    return (Data_Set.Tables[0]);
                } else {
                    return null;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Requisiciones_En_Web
            ///DESCRIPCIÓN: Consulta RQ
            ///PARAMETROS: 1.-Clase de Negocio
            ///            2.-Usuario que crea la requisa
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///******************************************************************************* 
            public static DataTable Consultar_Requisiciones_En_Web(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {

                String Mi_Sql = "";
                if (!String.IsNullOrEmpty(Requisicion_Negocio.P_Folio)) {
                    Requisicion_Negocio.P_Folio = Requisicion_Negocio.P_Folio.Trim();
                    Mi_Sql =
                    "SELECT " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + ".*, " +
                        "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                        " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " +
                        Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                        ") NOMBRE_DEPENDENCIA " +
                    " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                    " WHERE " +
                    Ope_Tal_Requisiciones.Campo_Tipo + " IN ('" + Requisicion_Negocio.P_Tipo + "')" +
                    " AND " + Ope_Tal_Requisiciones.Campo_Estatus +
                    " NOT IN ('EN CONSTRUCCION','GENERADA','AUTORIZADA','RECHAZADA','FILTRADA','CANCELADA','REVISAR') " +
                    " AND " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = " + Requisicion_Negocio.P_Folio;
                } else {
                    Mi_Sql =
                    "SELECT " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + ".*, " +
                        "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                        " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " +
                        Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                        ") NOMBRE_DEPENDENCIA " +
                    " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                    " WHERE " +
                    Ope_Tal_Requisiciones.Campo_Tipo + " IN ('" + Requisicion_Negocio.P_Tipo + "')" +
                    " AND " + Ope_Tal_Requisiciones.Campo_Estatus +
                    " IN ('PROVEEDOR','COTIZADA','CONFIRMADA') ";
                }
                if (!String.IsNullOrEmpty(Requisicion_Negocio.P_Cotizador_ID) ) {
                    if (Requisicion_Negocio.P_Cotizador_ID != "0") {
                        Mi_Sql += " AND " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." +
                            Ope_Tal_Requisiciones.Campo_Cotizador_ID + " = '" + Requisicion_Negocio.P_Cotizador_ID + "' ";
                    }
                }
                Mi_Sql += " ORDER BY " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " DESC";
                DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0) {
                    return (Data_Set.Tables[0]);
                } else {
                    return null;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Requisicion_Por_ID
            ///DESCRIPCIÓN: Consulta RQ por ID
            ///PARAMETROS: 1.-Clase de Negocio
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///******************************************************************************* 
            public static DataTable Consultar_Requisicion_Por_ID(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Mi_Sql = "";
                Mi_Sql = "SELECT " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + ".*, " +
                    "(SELECT " + Cat_Dependencias.Campo_Nombre + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                    " WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = " +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                    ") NOMBRE_DEPENDENCIA " +
                " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                " WHERE " +
                Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = " + Requisicion_Negocio.P_Requisicion_ID;
                DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                if (Data_Set != null && Data_Set.Tables[0].Rows.Count > 0) {
                    return (Data_Set.Tables[0]);
                } else {
                    return null;
                } 
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Productos_Servicios_Requisiciones
            ///DESCRIPCIÓN: Consulta los productos de las RQ
            ///PARAMETROS: 1.-Clase de Negocio
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///******************************************************************************* 
            //Consulta los detalles de la requisición
            public static DataTable Consultar_Productos_Servicios_Requisiciones(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Mi_SQL = "";
                if (Requisicion_Negocio.P_Tipo_Articulo != null && Requisicion_Negocio.P_Tipo_Articulo.Trim() == "PRODUCTO") {
                    Mi_SQL = "SELECT " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + ".*," +
                    "(SELECT " + Cat_Com_Unidades.Campo_Abreviatura + " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " WHERE " +
                    Cat_Com_Unidades.Campo_Unidad_ID + " = (SELECT " + Cat_Tal_Refacciones.Campo_Unidad_ID + " FROM " +
                    Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = " +
                    Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID + ")) AS UNIDAD ";
                } else {
                    Mi_SQL = "SELECT " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + ".*," +
                    "'S/U' AS UNIDAD ";
                }
                 Mi_SQL = Mi_SQL + "FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion +
                    " WHERE " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." +
                    Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = '" + Requisicion_Negocio.P_Requisicion_ID + "'";
                DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                DataTable _DataTable = null;
                if (_DataSet != null && _DataSet.Tables.Count > 0 && _DataSet.Tables[0].Rows.Count > 0) {
                    _DataTable = _DataSet.Tables[0];
                }
                return _DataTable;
            }

        #endregion

        //*********************************************************************************************
        //*********************************************************************************************
        //*********************************************************************************************                               
        #region SERVICIOS

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Servicio_Por_ID
            ///DESCRIPCIÓN: Consulta los servicios por ID
            ///PARAMETROS: 1.-Clase de Negocio
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///******************************************************************************* 
            public static DataTable Consultar_Servicio_Por_ID(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                //Cat_Com_Productos.Campo_Nombre + "  +'; '+ " + Cat_Com_Productos.Campo_Descripcion
                String Mi_SQL = "SELECT " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios + ".*, " +  
                    Cat_Com_Servicios.Campo_Nombre + " +', '+" + Cat_Com_Servicios.Campo_Comentarios +
                    " AS NOMBRE_DESCRIPCION, 'S/U' AS UNIDAD " +
                    " FROM " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios +

                    " WHERE " + Cat_Com_Servicios.Campo_Servicio_ID +
                    " IN (" + Requisicion_Negocio.P_Producto_ID + ")";//el Producto ID de Negocio sirve para pasar el Servicio_ID
                DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (_DataSet != null && _DataSet.Tables[0].Rows.Count > 0) {
                    return (_DataSet.Tables[0]);
                } else {
                    return null;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Servicios
            ///DESCRIPCIÓN: Consulta los servicios
            ///PARAMETROS: 1.-Clase de Negocio
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///******************************************************************************* 
            public static DataTable Consultar_Servicios(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                String Mi_SQL = "SELECT " +
                    Cat_Com_Servicios.Campo_Clave + ", " +
                    Cat_Com_Servicios.Campo_Servicio_ID + " AS ID, " +
                    Cat_Com_Servicios.Campo_Nombre + " +', '+" + Cat_Com_Servicios.Campo_Comentarios + " AS NOMBRE_DESCRIPCION, " +
                    Cat_Com_Servicios.Tabla_Cat_Com_Servicios + ".*, '0' AS DISPONIBLE, 'SRV' AS MODELO" +
                    " FROM " + Cat_Com_Servicios.Tabla_Cat_Com_Servicios +
                    " WHERE " + 
                    Cat_Com_Partidas.Campo_Partida_ID + " = '" + Requisicion_Negocio.P_Partida_ID + "'";

                if (Requisicion_Negocio.P_Nombre_Producto_Servicio != null) {
                    Mi_SQL = Mi_SQL + " AND UPPER (" + Cat_Com_Servicios.Campo_Nombre + ") LIKE UPPER ('%" + Requisicion_Negocio.P_Nombre_Producto_Servicio + "%')";
                }
                DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                return Data_Table;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Partida_Presupuestal_Por_Tipo_Requisicion
            ///DESCRIPCIÓN: Consulta las partidas por el tipo de RQ
            ///PARAMETROS: 1.-Clase de Negocio
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///******************************************************************************* 
            public static DataTable Consultar_Partida_Presupuestal_Por_Tipo_Requisicion(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
                DataTable Tabla = null;
                String Mi_SQL =
                "SELECT OPE_SAP_DEP_PRESUPUESTO.FUENTE_FINANCIAMIENTO_ID,OPE_SAP_DEP_PRESUPUESTO.AREA_FUNCIONAL_ID, " +
                "OPE_SAP_DEP_PRESUPUESTO.PROYECTO_PROGRAMA_ID, " +
                "OPE_SAP_DEP_PRESUPUESTO.DEPENDENCIA_ID, OPE_SAP_DEP_PRESUPUESTO.PARTIDA_ID, MONTO_DISPONIBLE, " +
                "(" +
                "REPLACE(CAT_SAP_FTE_FINANCIAMIENTO.CLAVE,' ','') " +
                "+'-'+ " +
                "REPLACE(CAT_SAP_AREA_FUNCIONAL.CLAVE,' ','') " +
                "+'-'+ " +
                "REPLACE(CAT_SAP_PROYECTOS_PROGRAMAS.CLAVE,' ','') " +
                "+'-'+ " +
                "REPLACE(CAT_DEPENDENCIAS.CLAVE,' ','') " +
                "+'-'+ " +
                "REPLACE(CAT_SAP_PARTIDAS_ESPECIFICAS.CLAVE,' ','') " +
                ") " +
                "AS CODIGO, " +
                "(REPLACE(CAT_SAP_PARTIDAS_ESPECIFICAS.CLAVE,' ','') +' - '+ UPPER(CAT_SAP_PARTIDAS_ESPECIFICAS.NOMBRE)) AS PARTIDA " +
                "FROM OPE_SAP_DEP_PRESUPUESTO " +
                "LEFT JOIN CAT_SAP_PARTIDAS_ESPECIFICAS " +
                "ON OPE_SAP_DEP_PRESUPUESTO.PARTIDA_ID = CAT_SAP_PARTIDAS_ESPECIFICAS.PARTIDA_ID " +
                "LEFT JOIN CAT_DEPENDENCIAS " +
                "ON OPE_SAP_DEP_PRESUPUESTO.DEPENDENCIA_ID = CAT_DEPENDENCIAS.DEPENDENCIA_ID " +
                "LEFT JOIN CAT_SAP_PROYECTOS_PROGRAMAS " +
                "ON OPE_SAP_DEP_PRESUPUESTO.PROYECTO_PROGRAMA_ID = CAT_SAP_PROYECTOS_PROGRAMAS.PROYECTO_PROGRAMA_ID " +
                "LEFT JOIN CAT_SAP_AREA_FUNCIONAL " +
                "ON OPE_SAP_DEP_PRESUPUESTO.AREA_FUNCIONAL_ID = CAT_SAP_AREA_FUNCIONAL.AREA_FUNCIONAL_ID " +
                "LEFT JOIN CAT_SAP_FTE_FINANCIAMIENTO " +
                "ON OPE_SAP_DEP_PRESUPUESTO.FUENTE_FINANCIAMIENTO_ID = CAT_SAP_FTE_FINANCIAMIENTO.FUENTE_FINANCIAMIENTO_ID " +
                "WHERE OPE_SAP_DEP_PRESUPUESTO.DEPENDENCIA_ID = '" + Requisicion_Negocio.P_Dependencia_ID + "' " +
                "AND OPE_SAP_DEP_PRESUPUESTO.ANIO_PRESUPUESTO = " + Requisicion_Negocio.P_Anio_Presupuesto;
                if (Requisicion_Negocio.P_Tipo == "STOCK")
                {
                    Mi_SQL = Mi_SQL + " AND OPE_SAP_DEP_PRESUPUESTO.PARTIDA_ID IN " +
                        "(SELECT DISTINCT(" + Cat_Com_Productos.Campo_Partida_ID + ") FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos +
                        " WHERE " + Cat_Com_Productos.Campo_Stock + " = 'SI' AND ESTATUS = 'ACTIVO')";
                }
                else if (Requisicion_Negocio.P_Tipo == "TRANSITORIA" && Requisicion_Negocio.P_Especial_Ramo33 == "SI")
                {
                    Mi_SQL = Mi_SQL + " AND OPE_SAP_DEP_PRESUPUESTO.FUENTE_FINANCIAMIENTO_ID IN " +
                        "(SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + 
                        " FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento +
                        " WHERE " +
                        Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI')";
                }
                else
                {
                    Mi_SQL = Mi_SQL + " AND OPE_SAP_DEP_PRESUPUESTO.FUENTE_FINANCIAMIENTO_ID IN " +
                        "(SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID +
                        " FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento +
                        " WHERE " +
                        Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'NO')";                
                }
                
                Mi_SQL += " ORDER BY CAT_SAP_PARTIDAS_ESPECIFICAS.CLAVE ";

                try
                {
                    Tabla = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                } catch(Exception Ex)
                {
                    throw new Exception(Ex.ToString());
                }
                return Tabla;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Partida_Presupuestal_Por_Tipo_Requisicion
            ///DESCRIPCIÓN: Consulta las partidas por el tipo de RQ
            ///PARAMETROS: 1.-Clase de Negocio
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///******************************************************************************* 
            //OBTINE PARTIDAS ESPECIFICAS CON PRESPUESTOS A PARTIR DE LA DEPENDENCIA Y EL PROYECTO
            //public static DataTable Consultar_Presupuesto_Partidas(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
            //    String Mi_SQL = "";

            //    Mi_SQL =
            //    "SELECT " +
            //    Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ", " +
            //        "(SELECT " + Cat_Com_Partidas.Campo_Nombre + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
            //        " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " +
            //        Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
            //        Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") NOMBRE, " +

            //        "(SELECT " + Cat_Com_Partidas.Campo_Clave + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
            //        " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " +
            //        Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
            //        Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") CLAVE, " +

            //        "(SELECT " + Cat_Com_Partidas.Campo_Clave + " +' '+" +
            //        Cat_Com_Partidas.Campo_Nombre + " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
            //        " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " +
            //        Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
            //        Cat_Com_Dep_Presupuesto.Campo_Partida_ID + ") CLAVE_NOMBRE, " +

            //        Cat_Com_Dep_Presupuesto.Campo_Monto_Presupuestal + ", " +
            //        Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " MONTO_DISPONIBLE, " +
            //        Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + ", " +
            //        Cat_Com_Dep_Presupuesto.Campo_Monto_Ejercido + ", " +
            //        Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto + ", " +
            //        Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ", " +
            //        Cat_Com_Dep_Presupuesto.Campo_Fecha_Creo +
            //            //" TO_CHAR(FECHA_CREO ,'DD/MM/YY') FECHA_CREO" + 
            //        " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
            //        " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
            //        " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
            //        " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
            //        " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
            //        " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
            //        " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +
            //        " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
            //        " IN (" + Requisicion_Negocio.P_Partida_ID + ")" +
            //        " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
            //        " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +
            //        " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + " = " +
            //            "(SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
            //            " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
            //            " WHERE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
            //                        Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
            //                        " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
            //                        " AND " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
            //                        Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
            //                        " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
            //                        " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
            //                        " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +

            //                        " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
            //                        " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +

            //                        " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
            //                        " IN (" + Requisicion_Negocio.P_Partida_ID + "))";

            //    DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            //    return Data_Table;
            //}

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Comprometer_Presupuesto_Partidas_Usadas_En_Requisicion
            ///DESCRIPCIÓN: Consulta las partidas por el tipo de RQ
            ///PARAMETROS: 1.-Clase de Negocio
            ///CREO:  
            ///FECHA_CREO:  
            ///MODIFICO: Francisco Antonio Gallardo Castañeda
            ///FECHA_MODIFICO: 28/Junio/2012
            ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
            ///******************************************************************************* 
            //Si se usa 31 mar 2011
            //public static int Comprometer_Presupuesto_Partidas_Usadas_En_Requisicion(Cls_Ope_Tal_Requisiciones_Negocio Requisicion_Negocio) {
            //    String Mi_Sql = "";
            //    double Comprometido = 0;
            //    double Disponible = 0;
            //    int Registros_Afectados = 0;
            //    String Partida_ID = "";
            //    try {
            //        if (Requisicion_Negocio.P_Dt_Partidas != null && Requisicion_Negocio.P_Dt_Partidas.Rows.Count > 0) {
            //            foreach (DataRow Renglon_Partida in Requisicion_Negocio.P_Dt_Partidas.Rows) {
            //                Partida_ID = Renglon_Partida["PARTIDA_ID"].ToString().Trim();
            //                Comprometido = double.Parse(Renglon_Partida["MONTO_COMPROMETIDO"].ToString().Trim());
            //                Disponible = double.Parse(Renglon_Partida["MONTO_DISPONIBLE"].ToString().Trim());
            //                Mi_Sql =
            //                "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
            //                " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido +
            //                " = " + Comprometido + ", " +
            //                Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible +
            //                " = " + Disponible +
            //                " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
            //                " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
            //                " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
            //                " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
            //                " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
            //                " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +
            //                " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
            //                " = '" + Partida_ID + "'" +

            //                " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
            //                " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +
            //                " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + " = " +
            //                    "(SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
            //                    " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
            //                    " WHERE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
            //                                Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
            //                                " = '" + Requisicion_Negocio.P_Proyecto_Programa_ID + "'" +
            //                                " AND " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." +
            //                                Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
            //                                " = '" + Requisicion_Negocio.P_Dependencia_ID + "'" +
            //                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
            //                                " = '" + Requisicion_Negocio.P_Fuente_Financiamiento + "'" +

            //                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
            //                                " = '" + Requisicion_Negocio.P_Anio_Presupuesto + "'" +

            //                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
            //                                " = '" + Partida_ID + "')";
            //                Registros_Afectados =
            //                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            //            }
            //        }
            //    }
            //    catch (Exception Ex)
            //    {
            //        String Str = Ex.ToString();
            //        throw new Exception(Str);
            //    }
            //    return Registros_Afectados;
            //}

        public static String Elaborar_Requisicion_A_Partir_De_Otra(int No_Requisicion)
        {
            //DataTable Dt_Requisiciones = null;
            DataTable Dt_Aux = null;
            String Mi_SQL = "";
            //Consultar_Columnas_De_Tabla_BD los Proveedores para saber cuantas Requisiciones_Parciales se van a aelaborar
            Mi_SQL = "SELECT DISTINCT(" + Ope_Tal_Req_Refaccion.Campo_Proveedor_ID + "), " +
            Ope_Tal_Req_Refaccion.Campo_Nombre_Proveedor +
            " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion +
            " WHERE " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID +
            " IN (" + No_Requisicion + ")";
            DataTable Dt_Proveedores =
                SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            //Consultar encabezado de la requisición
            //Mi_SQL = "SELECT * FROM " + Ope_Com_Requisiciones.Tabla_Ope_Com_Requisiciones + " WHERE " +
            //Ope_Com_Requisiciones.Campo_Requisicion_ID + " = " + No_Requisicion;
            //Dt_Encabezado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            //Consultar detalles de la requisicion
            //Mi_SQL = "SELECT * FROM " + Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto + " WHERE " +
            //Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion;
            //Dt_Articulos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            //Inicio la transaccion para insertar nuevas requisiciones
            //int No_Orden_Compra = Obtener_Consecutivo(Ope_Com_Ordenes_Compra.Campo_No_Orden_Compra, Ope_Com_Ordenes_Compra.Tabla_Ope_Com_Ordenes_Compra);

            String Requisiciones_Creadas = "";
            Cls_Ope_Tal_Requisiciones_Negocio Requisicion = new Cls_Ope_Tal_Requisiciones_Negocio();
            Cls_Ope_Tal_Administrar_Requisiciones_Negocio Administrar_Requisicion =
                new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
            int Consecutivo_RQ = Obtener_Consecutivo(Ope_Tal_Requisiciones.Campo_Requisicion_ID, Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones);
            int Consecutivo_Productos_Requisa =
                    Obtener_Consecutivo(Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID, Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion);
            SqlConnection Conexion_Base = new SqlConnection(Cls_Constantes.Str_Conexion); //Variable para la conexión para la base de datos        
            SqlCommand Comando_SQL = new SqlCommand();                                    //Sirve para la ejecución de las operaciones a la base de datos
            SqlTransaction Transaccion_SQL;                                                  //Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            if (Conexion_Base.State != ConnectionState.Open)
            {
                Conexion_Base.Open(); //Abre la conexión a la base de datos
            }
            Transaccion_SQL = Conexion_Base.BeginTransaction(IsolationLevel.ReadCommitted);  //Asigna el espacio de memoria para guardar los datos del proceso de manera temporal
            Comando_SQL.Connection = Conexion_Base;                                          //Establece la conexión a la base de datos
            Comando_SQL.Transaction = Transaccion_SQL;                                       //Abre la transacción para la ejecución en la base de datos
            int x = 0;
            try
            {
                //Recorremos todos los pproveedores para crearles su requisicion
                foreach (DataRow Dr_Proveedor in Dt_Proveedores.Rows)
                {
                    //Insertar encabezado para nueva requisicion
                    //cOPIAMOS EL REGISTRO DE LA REQUISICION A UN ATABLA ESPEJO
                    Mi_SQL = "INSERT INTO OPE_COM_REQUISICIONES_COPIA SELECT * FROM " + 
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                    " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " IN (" + No_Requisicion + ")";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    //Comando_SQL.CommandText = Mi_SQL;
                    //x = Comando_SQL.ExecuteNonQuery();

                    //ACTUALIZAMOS ENLA TABLA ESPEJO LOS DATOS DE LA REQUISICION COMO ID Y FOLIO
                    Mi_SQL = "UPDATE OPE_COM_REQUISICIONES_COPIA SET " +
                    Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = " + Consecutivo_RQ + ", " +
                    Ope_Tal_Requisiciones.Campo_Folio + " = 'RQ-" + Consecutivo_RQ + "' " +
                    " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " IN (" + No_Requisicion + ")";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    //Comando_SQL.CommandText = Mi_SQL;
                    //x = Comando_SQL.ExecuteNonQuery();
                    
                    //INSERTAMOS EL NUEVO REGISTRO EN LA TABLA DE REQUISICIONES
                    Mi_SQL = "INSERT INTO " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " SELECT * FROM " +
                    Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "_COPIA " +
                    " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " IN (" + Consecutivo_RQ + ")";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    //Comando_SQL.CommandText = Mi_SQL;
                    //x = Comando_SQL.ExecuteNonQuery();

                    //BORRAMOS EL REGISTRO DE LA TABLA ESPEJO
                    Mi_SQL = "DELETE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "_COPIA WHERE " +
                    Ope_Tal_Requisiciones.Campo_Requisicion_ID + " IN (" + Consecutivo_RQ + ")";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    //Comando_SQL.CommandText = Mi_SQL;
                    //x = Comando_SQL.ExecuteNonQuery();

                    //INSERTAMOS LOS DETALLES DE LA NUEVA REQUISICION

                    //COPIAMOS EL REGISTRO DE LOS DETALLES DE LA REQUISICION A UNA TABLA ESPEJO
                    Mi_SQL = "INSERT INTO " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "_COPIA" + " SELECT * FROM " +
                    Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion +
                    " WHERE " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = " + No_Requisicion + " AND " +
                    Ope_Tal_Req_Refaccion.Campo_Proveedor_ID + " = " + Dr_Proveedor[Ope_Tal_Req_Refaccion.Campo_Proveedor_ID].ToString();
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    //Comando_SQL.CommandText = Mi_SQL;
                    //Comando_SQL.ExecuteNonQuery();

                    //ACTUALIZAMOS EN LA TABLA ESPEJO LOS ID's ID Y FOLIO DE LOS DETALLES PARA NO CAUSAR CONFLICTO CON LOS DE LA TABLA ORIGINAL
                    //BUSCAMOS LOS DETALLES DE LA REQUISICION
                    Mi_SQL = "SELECT * FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "_COPIA " +
                    " WHERE " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = " + No_Requisicion + " AND " +
                    Ope_Tal_Req_Refaccion.Campo_Proveedor_ID + " = " + Dr_Proveedor[Ope_Tal_Req_Refaccion.Campo_Proveedor_ID].ToString();                    
                    Dt_Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                    //Consecutivo_Productos_Requisa =
                    //    Obtener_Consecutivo(Ope_Com_Req_Producto.Campo_Ope_Com_Req_Producto_ID, Ope_Com_Req_Producto.Tabla_Ope_Com_Req_Producto);
                    foreach (DataRow Producto in Dt_Aux.Rows)
                    {
                        Mi_SQL = "UPDATE " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "_COPIA " +
                        " SET " + Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID + " = " + Consecutivo_Productos_Requisa + "," +
                        Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = " + Consecutivo_RQ +
                        " WHERE " +
                        Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID + " = " + Producto[Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID].ToString();

                        //Ope_Com_Req_Producto.Campo_Requisicion_ID + " = " + No_Requisicion + " AND " +
                        //Ope_Com_Req_Producto.Campo_Proveedor_ID + " = " + Dr_Proveedor[Ope_Com_Req_Producto.Campo_Proveedor_ID].ToString();
                        
                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        Consecutivo_Productos_Requisa = Consecutivo_Productos_Requisa + 1;
                    }
                        

                    //INSERTAMOS EL NUEVO REGISTRO EN LA TABLA DE REQUISICIONES
                    Mi_SQL = "INSERT INTO " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " SELECT * FROM " +
                    Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "_COPIA " +
                    " WHERE " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = " + Consecutivo_RQ + " AND " +
                    Ope_Tal_Req_Refaccion.Campo_Proveedor_ID + " = " + Dr_Proveedor[Ope_Tal_Req_Refaccion.Campo_Proveedor_ID].ToString();
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    //Comando_SQL.CommandText = Mi_SQL;
                    //Comando_SQL.ExecuteNonQuery();

                    //BORRAMOS EL REGISTRO DE LA TABLA ESPEJO
                    Mi_SQL = "DELETE " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "_COPIA " +
                    " WHERE " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = " + Consecutivo_RQ + " AND " +
                    Ope_Tal_Req_Refaccion.Campo_Proveedor_ID + " = " + Dr_Proveedor[Ope_Tal_Req_Refaccion.Campo_Proveedor_ID].ToString();
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    //Comando_SQL.CommandText = Mi_SQL;
                    //Comando_SQL.ExecuteNonQuery();

                    //REGISTRAR HISTORIAL A LA NUEVA REQUISICION
                    Registrar_Historial("CONFIRMADA", Consecutivo_RQ.ToString());

                    //PONGO COMENTARIO A LA NUEVA REQUISICION
                    Administrar_Requisicion = new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
                    Administrar_Requisicion.P_Requisicion_ID = Consecutivo_RQ.ToString();
                    Administrar_Requisicion.P_Comentario = "Requisición creada a partir de la requisición RQ-" + No_Requisicion + " por separación de productos";
                    Administrar_Requisicion.P_Estatus = "CONFIRMADA";
                    Administrar_Requisicion.P_Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                    Administrar_Requisicion.Alta_Observaciones();

                    //ACTUALIZO LOS COSTOS DE LA REQUISICION SEGUN LOS PRODUCTOS QUE CONTENGA
                    Mi_SQL = "" +
                    "UPDATE OPE_COM_REQUISICIONES SET " +
                    " SUBTOTAL_COTIZADO = (SELECT SUM(SUBTOTAL_COTIZADO) FROM OPE_COM_REQ_PRODUCTO WHERE NO_REQUISICION = " + Consecutivo_RQ + " AND PROVEEDOR_ID = '" + Dr_Proveedor[Ope_Tal_Req_Refaccion.Campo_Proveedor_ID].ToString() + "')," +
                    " IEPS_COTIZADO = (SELECT SUM(IEPS_COTIZADO) FROM OPE_COM_REQ_PRODUCTO WHERE NO_REQUISICION = " + Consecutivo_RQ + " AND PROVEEDOR_ID = '" + Dr_Proveedor[Ope_Tal_Req_Refaccion.Campo_Proveedor_ID].ToString() + "')," +
                    " IVA_COTIZADO = (SELECT SUM(IVA_COTIZADO) FROM OPE_COM_REQ_PRODUCTO WHERE NO_REQUISICION = " + Consecutivo_RQ + " AND PROVEEDOR_ID = '" + Dr_Proveedor[Ope_Tal_Req_Refaccion.Campo_Proveedor_ID].ToString() + "')," +
                    " TOTAL_COTIZADO = (SELECT SUM(TOTAL_COTIZADO) FROM OPE_COM_REQ_PRODUCTO WHERE NO_REQUISICION = " + Consecutivo_RQ + " AND PROVEEDOR_ID = '" + Dr_Proveedor[Ope_Tal_Req_Refaccion.Campo_Proveedor_ID].ToString() + "'), " +
                    
                    " SUBTOTAL = (SELECT SUM(MONTO) FROM OPE_COM_REQ_PRODUCTO WHERE NO_REQUISICION = " + Consecutivo_RQ + " AND PROVEEDOR_ID = '" + Dr_Proveedor[Ope_Tal_Req_Refaccion.Campo_Proveedor_ID].ToString() + "')," +
                    " IEPS = (SELECT SUM(MONTO_IEPS) FROM OPE_COM_REQ_PRODUCTO WHERE NO_REQUISICION = " + Consecutivo_RQ + " AND PROVEEDOR_ID = '" + Dr_Proveedor[Ope_Tal_Req_Refaccion.Campo_Proveedor_ID].ToString() + "')," +
                    " IVA = (SELECT SUM(MONTO_IVA) FROM OPE_COM_REQ_PRODUCTO WHERE NO_REQUISICION = " + Consecutivo_RQ + " AND PROVEEDOR_ID = '" + Dr_Proveedor[Ope_Tal_Req_Refaccion.Campo_Proveedor_ID].ToString() + "')," +
                    " TOTAL = (SELECT SUM(MONTO_TOTAL) FROM OPE_COM_REQ_PRODUCTO WHERE NO_REQUISICION = " + Consecutivo_RQ + " AND PROVEEDOR_ID = '" + Dr_Proveedor[Ope_Tal_Req_Refaccion.Campo_Proveedor_ID].ToString() + "'), " +
                    " REQ_ORIGEN_ID = " + No_Requisicion + 
                    " WHERE NO_REQUISICION = " + Consecutivo_RQ;
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    Requisiciones_Creadas += "RQ-" + Consecutivo_RQ + ",";
                    Consecutivo_RQ++;                    
                }
                //CANCELO LA REQUISICION ORIGINAL
                Mi_SQL = "UPDATE OPE_COM_REQUISICIONES SET ESTATUS = 'CANCELADA' WHERE NO_REQUISICION = " + No_Requisicion;
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                //REGISTRAR HISTORIAL DE LA REQUISICION ORIGINAL
                Registrar_Historial("CANCELADA", No_Requisicion.ToString());

                //PONGO COMENTARIO DE MOTIVO CANCELACION
                Administrar_Requisicion.P_Requisicion_ID = No_Requisicion.ToString();
                Administrar_Requisicion.P_Comentario = "La Requisición se canceló para crear las nuevas requisiciones " + Requisiciones_Creadas + " por separación de productos";
                Administrar_Requisicion.P_Estatus = "CONFIRMADA";
                Administrar_Requisicion.P_Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                Administrar_Requisicion.Alta_Observaciones();

                //Transaccion_SQL.Commit();
            }
            catch (SqlException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
               // Dt_Orden_Compra = null;
                throw new Exception("Información: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
               // Dt_Orden_Compra = null;
                throw new Exception("Los datos fueron actualizados por otro Usuario. Información: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Transaccion_SQL != null)
                {
                    Transaccion_SQL.Rollback();
                }
              //  Dt_Orden_Compra = null;
                throw new Exception("Información: " + Ex.Message);
            }
            finally
            {
                Conexion_Base.Close();
            } 

            return Requisiciones_Creadas;
        }
        #endregion

    }
}
