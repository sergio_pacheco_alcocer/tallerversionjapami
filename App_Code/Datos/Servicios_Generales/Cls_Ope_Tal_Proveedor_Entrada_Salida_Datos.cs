﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Operacion_Proveedor_Entrada_Salida.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Tal_Proveedor_Entrada_Salida_Datos
/// </summary>
namespace JAPAMI.Taller_Mecanico.Operacion_Proveedor_Entrada_Salida.Datos {
    public class Cls_Ope_Tal_Proveedor_Entrada_Salida_Datos {

        #region Metodos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Registro_Salida      
            ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a dar de
            ///                         Alta en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 29/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Alta_Registro_Salida(Cls_Ope_Tal_Proveedor_Entrada_Salida_Negocio Parametros) {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                Parametros.P_No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Prov_Ent_Sal.Tabla_Ope_Tal_Prov_Ent_Sal, Ope_Tal_Prov_Ent_Sal.Campo_No_Registro, 20));
                try {
                    //Se carga el Registro Principal
                    String Mi_SQL = "INSERT INTO " + Ope_Tal_Prov_Ent_Sal.Tabla_Ope_Tal_Prov_Ent_Sal;
                    Mi_SQL = Mi_SQL + "( " + Ope_Tal_Prov_Ent_Sal.Campo_No_Registro;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal.Campo_No_Asignacion_Prov_Serv;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal.Campo_Fecha_Salida;
                    if (Parametros.P_Kilometraje_Salida > (-1)) Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal.Campo_Kilometraje_Salida;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal.Campo_Comentarios_Salida;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal.Campo_Estatus;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal.Campo_Usuario_Creo;
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal.Campo_Fecha_Creo;
                    Mi_SQL = Mi_SQL + ") VALUES ('" + Parametros.P_No_Registro + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Asignacion + "'";
                    Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Salida) + "'";
                    if (Parametros.P_Kilometraje_Salida > (-1)) Mi_SQL = Mi_SQL + ", '" + Parametros.P_Kilometraje_Salida + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Comentarios_Salida + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Se cargan los detalles de la Entrada-Salida
                    if (Parametros.P_Dt_Detalles_Salida != null) { 
                        Int32 No_Detalle = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Prov_Ent_Sal_Det.Tabla_Ope_Tal_Prov_Ent_Sal_Det, Ope_Tal_Prov_Ent_Sal_Det.Campo_No_Detalle, 20));
                        foreach (DataRow Fila_Detalle in Parametros.P_Dt_Detalles_Salida.Rows) {
                            Mi_SQL = "INSERT INTO " + Ope_Tal_Prov_Ent_Sal_Det.Tabla_Ope_Tal_Prov_Ent_Sal_Det;
                            Mi_SQL = Mi_SQL + " (" + Ope_Tal_Prov_Ent_Sal_Det.Campo_No_Detalle;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal_Det.Campo_No_Registro;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal_Det.Campo_Parte_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal_Det.Campo_SubParte_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal_Det.Campo_Tipo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal_Det.Campo_Valor;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal_Det.Campo_Usuario_Creo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal_Det.Campo_Fecha_Creo;
                            Mi_SQL = Mi_SQL + ") VALUES ('" + No_Detalle + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Registro+ "'";
                            Mi_SQL = Mi_SQL + ", '" + Fila_Detalle["PARTE_ID"].ToString().Trim() + "'";
                            Mi_SQL = Mi_SQL + ", '" + Fila_Detalle["SUBPARTE_ID"].ToString().Trim() + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo + "'";
                            Mi_SQL = Mi_SQL + ", '" + Fila_Detalle["VALOR"].ToString().Trim() + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                            Mi_SQL = Mi_SQL + ", GETDATE())";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            No_Detalle = No_Detalle + 1;
                        }
                    }

                    //Se actualiza el Estatus del Servicio
                    if (Parametros.P_Tipo_Servicio.Trim().Equals("SERVICIO_PREVENTIVO")) {
                        Mi_SQL = "UPDATE " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                        Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = '" + Parametros.P_Estatus_Servicio + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Modifico + " = GETDATE()";
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    } else if (Parametros.P_Tipo_Servicio.Trim().Equals("SERVICIO_CORRECTIVO")) { 
                        Mi_SQL = "UPDATE " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos;
                        Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Correctivos.Campo_Estatus + " = '" + Parametros.P_Estatus_Servicio + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Fecha_Modifico + " = GETDATE()";
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    //Se Ejecutan todas las sentencias.
                    Trans.Commit();
                } catch (SqlException Ex) {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152) {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 2627) {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    } else if (Ex.Number == 547) {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 515) {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Registro_Entrada      
            ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a dar de
            ///                         Alta en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 01/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Alta_Registro_Entrada(Cls_Ope_Tal_Proveedor_Entrada_Salida_Negocio Parametros) {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                //Parametros.P_No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Prov_Ent_Sal.Tabla_Ope_Tal_Prov_Ent_Sal, Ope_Tal_Prov_Ent_Sal.Campo_No_Registro, 20));
                try {
                    //Se carga el Registro Principal
                    String Mi_SQL = "UPDATE " + Ope_Tal_Prov_Ent_Sal.Tabla_Ope_Tal_Prov_Ent_Sal;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Prov_Ent_Sal.Campo_Fecha_Entrada + " = '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Entrada) + "'";
                    if (Parametros.P_Kilometraje_Entrada > -1) Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal.Campo_Kilometraje_Entrada + " = '" + Parametros.P_Kilometraje_Entrada + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal.Campo_Comentarios_Entrada + " = '" + Parametros.P_Comentarios_Entrada + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal.Campo_Usuario_Creo + " = '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal.Campo_Fecha_Creo + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Prov_Ent_Sal.Campo_No_Registro + " = '" + Parametros.P_No_Registro + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Se cargan los detalles de la Entrada-Salida
                    if (Parametros.P_Dt_Detalles_Salida != null) {
                        Int32 No_Detalle = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Prov_Ent_Sal_Det.Tabla_Ope_Tal_Prov_Ent_Sal_Det, Ope_Tal_Prov_Ent_Sal_Det.Campo_No_Detalle, 20));
                        foreach (DataRow Fila_Detalle in Parametros.P_Dt_Detalles_Entrada.Rows) {
                            Mi_SQL = "INSERT INTO " + Ope_Tal_Prov_Ent_Sal_Det.Tabla_Ope_Tal_Prov_Ent_Sal_Det;
                            Mi_SQL = Mi_SQL + " (" + Ope_Tal_Prov_Ent_Sal_Det.Campo_No_Detalle;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal_Det.Campo_No_Registro;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal_Det.Campo_Parte_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal_Det.Campo_SubParte_ID;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal_Det.Campo_Tipo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal_Det.Campo_Valor;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal_Det.Campo_Usuario_Creo;
                            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Prov_Ent_Sal_Det.Campo_Fecha_Creo;
                            Mi_SQL = Mi_SQL + ") VALUES ('" + No_Detalle + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Registro + "'";
                            Mi_SQL = Mi_SQL + ", '" + Fila_Detalle["PARTE_ID"].ToString().Trim() + "'";
                            Mi_SQL = Mi_SQL + ", '" + Fila_Detalle["SUBPARTE_ID"].ToString().Trim() + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo + "'";
                            Mi_SQL = Mi_SQL + ", '" + Fila_Detalle["VALOR"].ToString().Trim() + "'";
                            Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                            Mi_SQL = Mi_SQL + ", GETDATE())";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            No_Detalle = No_Detalle + 1;
                        }
                    }

                    //Se actualiza el Estatus del Servicio
                    if (Parametros.P_Tipo_Servicio.Trim().Equals("SERVICIO_PREVENTIVO")) {
                        Mi_SQL = "UPDATE " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                        Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = '" + Parametros.P_Estatus_Servicio + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Modifico + " = GETDATE()";
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    } else if (Parametros.P_Tipo_Servicio.Trim().Equals("SERVICIO_CORRECTIVO")) { 
                        Mi_SQL = "UPDATE " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos;
                        Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Correctivos.Campo_Estatus + " = '" + Parametros.P_Estatus_Servicio + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Fecha_Modifico + " = GETDATE()";
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    if (Parametros.P_Estatus_Servicio.Trim().Equals("REPARADO")) {
                        //Se carga el Registro Principal
                        Mi_SQL = "UPDATE " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;
                        Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " = 'REPARADO'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Usuario_Creo + " = '" + Parametros.P_Usuario + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Fecha_Creo + " = GETDATE()";
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " = '" + Parametros.P_No_Asignacion + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }

                    //Se Ejecutan todas las sentencias.
                    Trans.Commit();
                } catch (SqlException Ex) {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152) {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 2627) {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    } else if (Ex.Number == 547) {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 515) {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Proveedor_Entrada_Salida
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 
            ///                     1.Parametros.Contiene los parametros que se van a utilizar para
            ///                       hacer la consulta de la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 1/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Cls_Ope_Tal_Proveedor_Entrada_Salida_Negocio Consultar_Detalles_Proveedor_Entrada_Salida(Cls_Ope_Tal_Proveedor_Entrada_Salida_Negocio Parametros)  {
                String Mi_SQL = null;
                Cls_Ope_Tal_Proveedor_Entrada_Salida_Negocio Obj_Cargado = new Cls_Ope_Tal_Proveedor_Entrada_Salida_Negocio();
                try {
                    Mi_SQL = "SELECT * FROM " + Ope_Tal_Prov_Ent_Sal.Tabla_Ope_Tal_Prov_Ent_Sal;
                    if (Parametros.P_No_Asignacion > (-1)) {
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Prov_Ent_Sal.Campo_No_Asignacion_Prov_Serv + " = '" + Parametros.P_No_Asignacion + "'";
                    } else if (Parametros.P_No_Registro > (-1)) {
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Prov_Ent_Sal.Campo_No_Registro + " = '" + Parametros.P_No_Registro + "'";
                    }
                    
                    SqlDataReader Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    while (Reader.Read()) {
                        Obj_Cargado.P_No_Registro = (!String.IsNullOrEmpty(Reader[Ope_Tal_Prov_Ent_Sal.Campo_No_Registro].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Prov_Ent_Sal.Campo_No_Registro]) : (-1);
                        Obj_Cargado.P_No_Asignacion = (!String.IsNullOrEmpty(Reader[Ope_Tal_Prov_Ent_Sal.Campo_No_Asignacion_Prov_Serv].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Prov_Ent_Sal.Campo_No_Asignacion_Prov_Serv]) : (-1);
                        Obj_Cargado.P_Fecha_Salida = (!String.IsNullOrEmpty(Reader[Ope_Tal_Prov_Ent_Sal.Campo_Fecha_Salida].ToString())) ? Convert.ToDateTime(Reader[Ope_Tal_Prov_Ent_Sal.Campo_Fecha_Salida]) : new DateTime();
                        Obj_Cargado.P_Kilometraje_Salida = (!String.IsNullOrEmpty(Reader[Ope_Tal_Prov_Ent_Sal.Campo_Kilometraje_Salida].ToString())) ? Convert.ToDouble(Reader[Ope_Tal_Prov_Ent_Sal.Campo_Kilometraje_Salida]) : (-1.0);
                        Obj_Cargado.P_Comentarios_Salida = (!String.IsNullOrEmpty(Reader[Ope_Tal_Prov_Ent_Sal.Campo_Comentarios_Salida].ToString())) ? Reader[Ope_Tal_Prov_Ent_Sal.Campo_Comentarios_Salida].ToString() : "";
                        Obj_Cargado.P_Fecha_Entrada = (!String.IsNullOrEmpty(Reader[Ope_Tal_Prov_Ent_Sal.Campo_Fecha_Entrada].ToString())) ? Convert.ToDateTime(Reader[Ope_Tal_Prov_Ent_Sal.Campo_Fecha_Entrada]) : new DateTime();
                        Obj_Cargado.P_Kilometraje_Entrada = (!String.IsNullOrEmpty(Reader[Ope_Tal_Prov_Ent_Sal.Campo_Kilometraje_Entrada].ToString())) ? Convert.ToDouble(Reader[Ope_Tal_Prov_Ent_Sal.Campo_Kilometraje_Entrada]) : (-1.0);
                        Obj_Cargado.P_Comentarios_Entrada = (!String.IsNullOrEmpty(Reader[Ope_Tal_Prov_Ent_Sal.Campo_Comentarios_Entrada].ToString())) ? Reader[Ope_Tal_Prov_Ent_Sal.Campo_Comentarios_Entrada].ToString() : "";
                        Obj_Cargado.P_Estatus = (!String.IsNullOrEmpty(Reader[Ope_Tal_Prov_Ent_Sal.Campo_Estatus].ToString())) ? Reader[Ope_Tal_Prov_Ent_Sal.Campo_Estatus].ToString() : "";
                    }
                    Reader.Close();
                    if (Obj_Cargado.P_No_Registro > (-1)) {
                        Mi_SQL = "SELECT DETALLES." + Ope_Tal_Prov_Ent_Sal_Det.Campo_No_Registro + " AS NO_DETALLE";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Prov_Ent_Sal_Det.Campo_No_Registro + " AS NO_REGISTRO";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Prov_Ent_Sal_Det.Campo_Parte_ID + " AS PARTE_ID";
                        Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Tal_Partes_Veh.Campo_Nombre + " FROM " + Cat_Tal_Partes_Veh.Tabla_Cat_Tal_Partes_Veh + " WHERE " + Cat_Tal_Partes_Veh.Campo_Parte_ID + " = DETALLES." + Ope_Tal_Ent_Veh_Det.Campo_Parte_ID + ") AS PARTE_NOMBRE";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Prov_Ent_Sal_Det.Campo_SubParte_ID + " AS SUBPARTE_ID";
                        Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Tal_Partes_Veh.Campo_Nombre + " FROM " + Cat_Tal_Partes_Veh.Tabla_Cat_Tal_Partes_Veh + " WHERE " + Cat_Tal_Partes_Veh.Campo_Parte_ID + " = DETALLES." + Ope_Tal_Ent_Veh_Det.Campo_SubParte_ID + ") AS SUBPARTE_NOMBRE";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Prov_Ent_Sal_Det.Campo_Valor + " AS VALOR";
                        Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Prov_Ent_Sal_Det.Tabla_Ope_Tal_Prov_Ent_Sal_Det + " DETALLES";
                        Mi_SQL = Mi_SQL + " WHERE DETALLES." + Ope_Tal_Prov_Ent_Sal_Det.Campo_No_Registro + " = '" + Obj_Cargado.P_No_Registro + "'";
                        Mi_SQL = Mi_SQL + " AND DETALLES." + Ope_Tal_Prov_Ent_Sal_Det.Campo_Tipo + " = 'SALIDA'";   
                        DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Datos != null && Ds_Datos.Tables.Count > 0) {
                            Obj_Cargado.P_Dt_Detalles_Salida = Ds_Datos.Tables[0];
                        }                     
                    }
                    if (Obj_Cargado.P_No_Registro > (-1)) {
                        Mi_SQL = "SELECT DETALLES." + Ope_Tal_Prov_Ent_Sal_Det.Campo_No_Registro + " AS NO_DETALLE";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Prov_Ent_Sal_Det.Campo_No_Registro + " AS NO_REGISTRO";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Prov_Ent_Sal_Det.Campo_Parte_ID + " AS PARTE_ID";
                        Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Tal_Partes_Veh.Campo_Nombre + " FROM " + Cat_Tal_Partes_Veh.Tabla_Cat_Tal_Partes_Veh + " WHERE " + Cat_Tal_Partes_Veh.Campo_Parte_ID + " = DETALLES." + Ope_Tal_Ent_Veh_Det.Campo_Parte_ID + ") AS PARTE_NOMBRE";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Prov_Ent_Sal_Det.Campo_SubParte_ID + " AS SUBPARTE_ID";
                        Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Tal_Partes_Veh.Campo_Nombre + " FROM " + Cat_Tal_Partes_Veh.Tabla_Cat_Tal_Partes_Veh + " WHERE " + Cat_Tal_Partes_Veh.Campo_Parte_ID + " = DETALLES." + Ope_Tal_Ent_Veh_Det.Campo_SubParte_ID + ") AS SUBPARTE_NOMBRE";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Prov_Ent_Sal_Det.Campo_Valor + " AS VALOR";
                        Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Prov_Ent_Sal_Det.Tabla_Ope_Tal_Prov_Ent_Sal_Det + " DETALLES";
                        Mi_SQL = Mi_SQL + " WHERE DETALLES." + Ope_Tal_Prov_Ent_Sal_Det.Campo_No_Registro + " = '" + Obj_Cargado.P_No_Registro + "'";
                        Mi_SQL = Mi_SQL + " AND DETALLES." + Ope_Tal_Prov_Ent_Sal_Det.Campo_Tipo + " = 'ENTRADA'";   
                        DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Datos != null && Ds_Datos.Tables.Count > 0) {
                            Obj_Cargado.P_Dt_Detalles_Entrada = Ds_Datos.Tables[0];
                        }                     
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Obj_Cargado;
            }
            
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
            ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
            ///PARAMETROS:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Marzo/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID) {
                String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
                try {
                    String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                    Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals("")) {
                        Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                    }
                } catch (SqlException Ex) {
                    new Exception(Ex.Message);
                }
                return Id;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
            ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
            ///PARAMETROS:     
            ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
            ///             2. Longitud_ID. Longitud que tendra el ID. 
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Marzo/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID) {
                String Retornar = "";
                String Dato = "" + Dato_ID;
                for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++) {
                    Retornar = Retornar + "0";
                }
                Retornar = Retornar + Dato;
                return Retornar;
            }

        #endregion

	}
}
