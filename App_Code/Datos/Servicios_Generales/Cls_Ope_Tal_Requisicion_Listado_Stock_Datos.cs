﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Requisiciones_Listado.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Taller_Mecanico.Requisiciones.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Tal_Requisicion_Listado_Stock_Datos
/// </summary>

namespace JAPAMI.Taller_Mecanico.Requisiciones_Listado.Datos {

	public class Cls_Ope_Tal_Requisicion_Listado_Stock_Datos {

        public static DataTable Consulta_Listado_Almacen(Cls_Ope_Tal_Requisicion_Listado_Stock_Negocio Clase_Negocio) {
            String Mi_SQL = "SELECT LISTADO." + Ope_Tal_Listado.Campo_Folio;
            Mi_SQL = Mi_SQL + ", LISTADO." + Ope_Tal_Listado.Campo_Listado_ID;
            Mi_SQL = Mi_SQL + ",  convert(varchar,LISTADO." + Ope_Tal_Listado.Campo_Fecha_Creo + ",103) AS FECHA_CREO";
            Mi_SQL = Mi_SQL + ", LISTADO." + Ope_Tal_Listado.Campo_Tipo;
            Mi_SQL = Mi_SQL + ", LISTADO." + Ope_Tal_Listado.Campo_Estatus;
            Mi_SQL = Mi_SQL + ", LISTADO." + Ope_Tal_Listado.Campo_Total;
            Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado + " LISTADO";
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Listado.Campo_Estatus + " = 'AUTORIZADA'";

            if (Clase_Negocio.P_Listado_ID != null) {
                Mi_SQL = "SELECT * FROM " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado + " WHERE " + Ope_Tal_Listado.Campo_Listado_ID + "='" + Clase_Negocio.P_Listado_ID + "'";
            }
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        public static DataTable Consulta_Listado_Detalle(Cls_Ope_Tal_Requisicion_Listado_Stock_Negocio Clase_Negocio) {

            String Mi_SQL = "SELECT LISTADO." + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID + " AS PRODUCTO_ID";
            Mi_SQL = Mi_SQL + ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Clave;
            Mi_SQL = Mi_SQL + ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Nombre + " AS PRODUCTO_NOMBRE";
            Mi_SQL = Mi_SQL + ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Descripcion;
            Mi_SQL = Mi_SQL + ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Disponible;
            Mi_SQL = Mi_SQL + ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Reorden;
            Mi_SQL = Mi_SQL + ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Partida_ID;
            Mi_SQL = Mi_SQL + ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Costo_Unitario + " AS PRECIO_UNITARIO ";
            Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Sap_Concepto.Campo_Clave + " +' '+ " + Cat_Sap_Concepto.Campo_Descripcion + " FROM ";
            Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + " = (SELECT ";
            Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
            Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
            Mi_SQL = Mi_SQL + "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
            Mi_SQL = Mi_SQL + "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM ";
            Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = PRODUCTO.";
            Mi_SQL = Mi_SQL + Cat_Com_Productos.Campo_Partida_ID + "))) AS CONCEPTO";
            Mi_SQL = Mi_SQL + ",(SELECT " + Cat_Sap_Concepto.Campo_Concepto_ID + " FROM ";
            Mi_SQL = Mi_SQL + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + " WHERE " + Cat_Sap_Concepto.Campo_Concepto_ID + "=(SELECT ";
            Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " FROM ";
            Mi_SQL = Mi_SQL + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + " WHERE ";
            Mi_SQL = Mi_SQL + "CAT_SAP_PARTIDA_GENERICA." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID;
            Mi_SQL = Mi_SQL + "=(SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " FROM ";
            Mi_SQL = Mi_SQL + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = PRODUCTO.";
            Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Partida_ID + "))) AS CONCEPTO_ID";
            Mi_SQL = Mi_SQL + ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Cantidad;
            Mi_SQL = Mi_SQL + ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Costo_Compra;
            Mi_SQL = Mi_SQL + ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Importe;
            Mi_SQL = Mi_SQL + ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Monto_IVA;
            Mi_SQL = Mi_SQL + ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Monto_IEPS;
            Mi_SQL = Mi_SQL + ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IVA;
            Mi_SQL = Mi_SQL + ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IEPS;
            Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle + " LISTADO";
            Mi_SQL = Mi_SQL + " JOIN " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTO";
            Mi_SQL = Mi_SQL + " ON PRODUCTO." + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = LISTADO." + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID;
            Mi_SQL = Mi_SQL + " WHERE LISTADO." + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID + " = '" + Clase_Negocio.P_Listado_ID + "'";
            Mi_SQL = Mi_SQL + " AND LISTADO." + Ope_Tal_Listado_Detalle.Campo_Borrado + " IS NULL ";
            Mi_SQL = Mi_SQL + " AND LISTADO." + Ope_Tal_Listado_Detalle.Campo_No_Requisicion + " IS NULL ";
            Mi_SQL = Mi_SQL + " ORDER BY PRODUCTO." + Cat_Tal_Refacciones.Campo_Nombre;

            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }

        public static Boolean Borrar_Productos_Listado(Cls_Ope_Tal_Requisicion_Listado_Stock_Negocio Clase_Negocio) {
            String Mi_SQL = "";
            bool Operacion_Realizada = false;
            try {
                //Recorremos el listado para eliminar 
                for (int i = 0; i < Clase_Negocio.P_Dt_Productos.Rows.Count; i++) {
                    Mi_SQL = "UPDATE " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Listado_Detalle.Campo_Borrado + " = 'SI'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Listado_Detalle.Campo_Motivo_Borrado + " = '" + Clase_Negocio.P_Motivo_Borrado + "'";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID + " = '" + Clase_Negocio.P_Listado_ID + "'";
                    Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID + " = '" + Clase_Negocio.P_Dt_Productos.Rows[i]["Producto_ID"].ToString().Trim() + "'";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                Operacion_Realizada = true;
            } catch {
                Operacion_Realizada = false;
            }

            return Operacion_Realizada;
        }

        public static String Convertir_Requisicion_Transitoria(Cls_Ope_Tal_Requisicion_Listado_Stock_Negocio Clase_Negocio) {
            String Mensaje = "";
            String Requisicion_ID = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try {
                String Mi_SQL = "SELECT " + Cat_Tal_Parametros.Campo_Dependencia_ID +
                                ", " + Cat_Tal_Parametros.Campo_Partida_ID +
                                ", " + Cat_Tal_Parametros.Campo_Programa_ID +
                                " FROM " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros;
                DataTable Dt_Parametros = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                String Partida_Esp_Almacen_Global = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Partida_ID].ToString();
                String Programa_ID_Almacen = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Programa_ID].ToString();
                String Dependencia_ID_Almacen = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Dependencia_ID].ToString();

                Mi_SQL = "SELECT " + Cat_Sap_Partidas_Especificas.Campo_Nombre + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas
                        + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = '" + Partida_Esp_Almacen_Global + "'";
                Dt_Parametros = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                String Partida_Desc = Dt_Parametros.Rows[0][Cat_Sap_Partidas_Especificas.Campo_Nombre].ToString();

                Mi_SQL = "SELECT LIS." + Ope_Tal_Listado.Campo_Empleado_Autorizacion_ID;
                Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Empleados.Campo_Nombre;
                Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Apellido_Paterno;
                Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Apellido_Materno;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Empleados.Campo_Empleado_ID;
                Mi_SQL = Mi_SQL + " = LIS." + Ope_Tal_Listado.Campo_Empleado_Autorizacion_ID;
                Mi_SQL = Mi_SQL + ") AS EMPLEADO_AUTORIZO";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado + " LIS";
                Mi_SQL = Mi_SQL + " WHERE LIS." + Ope_Tal_Listado.Campo_Listado_ID + " = '" + Clase_Negocio.P_Listado_ID + "'";
                DataTable Dt_Autorizo = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                String Empleado_Autorizo = "";
                if (Dt_Autorizo.Rows.Count != 0) Empleado_Autorizo = Dt_Autorizo.Rows[0][Ope_Com_Listado.Campo_Empleado_Autorizacion_ID].ToString().Trim();

                Requisicion_ID = Obtener_Consecutivo(Ope_Tal_Requisiciones.Campo_Requisicion_ID, Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones).ToString();

                Mi_SQL = "INSERT INTO " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                    " (" + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                    ", " + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                    ", " + Ope_Tal_Requisiciones.Campo_Folio +
                    ", " + Ope_Tal_Requisiciones.Campo_Estatus +
                    ", " + Ope_Tal_Requisiciones.Campo_Tipo +
                    ", " + Ope_Tal_Requisiciones.Campo_Fase +
                    ", " + Ope_Tal_Requisiciones.Campo_Usuario_Creo +
                    ", " + Ope_Tal_Requisiciones.Campo_Fecha_Creo +
                    ", " + Ope_Tal_Requisiciones.Campo_Empleado_Filtrado_ID +
                    ", " + Ope_Tal_Requisiciones.Campo_Fecha_Filtrado +
                    ", " + Ope_Tal_Requisiciones.Campo_Tipo_Articulo +
                    ", " + Ope_Tal_Requisiciones.Campo_Empleado_Construccion_ID +
                    ", " + Ope_Tal_Requisiciones.Campo_Fecha_Construccion +
                    ", " + Ope_Tal_Requisiciones.Campo_Empleado_Generacion_ID +
                    ", " + Ope_Tal_Requisiciones.Campo_Fecha_Generacion +
                    ", " + Ope_Tal_Requisiciones.Campo_Empleado_Autorizacion_ID +
                    ", " + Ope_Tal_Requisiciones.Campo_Fecha_Autorizacion +
                    ", " + Ope_Tal_Requisiciones.Campo_Listado_Almacen +
                    ", " + Ope_Tal_Requisiciones.Campo_Partida_ID +
                    ", " + Ope_Tal_Requisiciones.Campo_Justificacion_Compra +
                    ") VALUES ('" + Requisicion_ID + "','" +
                    Dependencia_ID_Almacen + "','" +
                    "RQ-" + Requisicion_ID + "','" +
                    "PROCESAR','" +
                    "TRANSITORIA','" +
                    "REQUISICION','" +
                    Cls_Sessiones.Nombre_Empleado + "',GETDATE()," +
                    "'" + Cls_Sessiones.Empleado_ID + "',GETDATE(),'PRODUCTO'," +
                    "'" + Cls_Sessiones.Empleado_ID + "',GETDATE()," +
                    "'" + Cls_Sessiones.Empleado_ID + "',GETDATE()," +
                    "'" + Empleado_Autorizo + "',GETDATE(),'SI','" +
                    Clase_Negocio.P_Dt_Productos.Rows[0]["Partida_ID"].ToString().Trim() +
                    "','" + Partida_Desc.ToUpper() + "')";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                for (int i = 0; i < Clase_Negocio.P_Dt_Productos.Rows.Count; i++) {
                    Mi_SQL = "UPDATE " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Listado_Detalle.Campo_No_Requisicion + " = '" + Requisicion_ID + "'";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID + "='";
                    Mi_SQL = Mi_SQL + Clase_Negocio.P_Dt_Productos.Rows[i]["Producto_ID"].ToString().Trim() + "'";
                    Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID + "='";
                    Mi_SQL = Mi_SQL + Clase_Negocio.P_Listado_ID.ToString().Trim() + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                Double IVA_Acumulado = 0;
                Double IEPS_Acumulado = 0;
                Double Subtotal = 0;
                Double Total = 0;
                Double Subtotal_Producto = 0;
                Double IVA_Producto = 0;
                Double IEPS_Producto = 0;

                Int32 Consecutivo = Convert.ToInt32(Obtener_Consecutivo(Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID, Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion));
                if (Clase_Negocio.P_Dt_Productos.Rows.Count != 0) {
                    for (int i = 0; i < Clase_Negocio.P_Dt_Productos.Rows.Count; i++) {

                        Subtotal_Producto = Double.Parse(Clase_Negocio.P_Dt_Productos.Rows[i]["Precio_Unitario"].ToString()) * int.Parse(Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Cantidad].ToString());
                        IVA_Producto = Double.Parse(Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Monto_IVA].ToString()) * int.Parse(Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Cantidad].ToString());
                        IEPS_Producto = Double.Parse(Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Monto_IEPS].ToString()) * int.Parse(Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Cantidad].ToString());

                        Mi_SQL = "INSERT INTO ";
                        Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion;
                        Mi_SQL = Mi_SQL + " (" + Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Partida_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Cantidad;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Usuario_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Fecha_Creo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Precio_Unitario;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Monto_IVA;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Monto_IEPS;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Porcentaje_IVA;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Porcentaje_IEPS;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Importe;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Monto_Total;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Tipo;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Clave;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Nombre_Giro;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Giro_ID;
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Req_Refaccion.Campo_Nombre_Producto_Servicio;
                        Mi_SQL = Mi_SQL + " ) VALUES ";
                        Mi_SQL = Mi_SQL + "('" + Consecutivo.ToString();
                        Mi_SQL = Mi_SQL + "','" + Requisicion_ID;
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Producto_ID"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Partida_ID"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "', '" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Cantidad].ToString();
                        Mi_SQL = Mi_SQL + "','" + Cls_Sessiones.Nombre_Empleado;
                        Mi_SQL = Mi_SQL + "',GETDATE()";
                        Mi_SQL = Mi_SQL + ",'" + Clase_Negocio.P_Dt_Productos.Rows[i]["Precio_Unitario"].ToString();
                        Mi_SQL = Mi_SQL + "','" + IVA_Producto;
                        Mi_SQL = Mi_SQL + "','" + IEPS_Producto;
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Porcentaje_IVA].ToString();
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Porcentaje_IEPS].ToString();
                        Mi_SQL = Mi_SQL + "','" + Subtotal_Producto;
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Importe].ToString();
                        Mi_SQL = Mi_SQL + "','PRODUCTO";
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Clave"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Concepto"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Concepto_ID"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "','" + Clase_Negocio.P_Dt_Productos.Rows[i]["Producto_Nombre"].ToString().Trim();
                        Mi_SQL = Mi_SQL + "' +';'+'" + Clase_Negocio.P_Dt_Productos.Rows[i]["Descripcion"].ToString().Trim() + "')";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        IVA_Acumulado = IVA_Acumulado + IVA_Producto;
                        IEPS_Acumulado = IEPS_Acumulado + IEPS_Producto;
                        //Como el Costo Compra es el precio unitario sinla cantidad se multiplica el presio unitario sin impuesto por la cantidad para obtener el Subtotal
                        Subtotal = Subtotal + Subtotal_Producto;
                        Total = Total + Double.Parse(Clase_Negocio.P_Dt_Productos.Rows[i]["Importe"].ToString());
                        //REgresamos a valores ceros los acumulados
                        IVA_Producto = 0;
                        IEPS_Producto = 0;
                        Subtotal_Producto = 0;

                        Consecutivo++;
                    }
                    //Actualizamos la requisicion con los nuevos valores de IVA, IEPs y subtotal 
                    Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                            " SET " + Ope_Tal_Requisiciones.Campo_IVA + " ='" + IVA_Acumulado.ToString() +
                            "', " + Ope_Tal_Requisiciones.Campo_IEPS + "='" + IEPS_Acumulado.ToString() +
                            "', " + Ope_Tal_Requisiciones.Campo_Subtotal + "='" + Subtotal.ToString() +
                            "', " + Ope_Tal_Requisiciones.Campo_Total + "='" + Total.ToString() +
                            "' WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "='" + Requisicion_ID + "'";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                    //REALIZAMOS EL INSERT EN LA TABLA DE HISTORIAL DE ESTATUS DE REQUISIONES 
                    Cls_Ope_Tal_Requisiciones_Negocio Requisicion = new Cls_Ope_Tal_Requisiciones_Negocio();
                    Requisicion.Registrar_Historial("EN CONSTRUCCION", Requisicion_ID);
                    Requisicion.Registrar_Historial("GENERADA", Requisicion_ID);
                    Requisicion.Registrar_Historial("AUTORIZADA", Requisicion_ID);
                }


                Trans.Commit();
            } catch (SqlException Ex) {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152) {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                } else if (Ex.Number == 2627) {
                    if (Ex.Message.IndexOf("PRIMARY") != -1) {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                } else if (Ex.Number == 547) {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                } else if (Ex.Number == 515) {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                } else {
                    Mensaje = "Error al intentar Modificar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            } finally {
                Cn.Close();
            }
            return Requisicion_ID;
        }

        public static DataTable Consultar_Requisiciones_Listado(Cls_Ope_Tal_Requisicion_Listado_Stock_Negocio Clase_Negocio) {
            String Mi_SQL = "SELECT " + Ope_Tal_Requisiciones.Campo_Folio;
            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Requisiciones.Campo_Total;
            Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;
            Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID;
            Mi_SQL = Mi_SQL + " IN (SELECT " + Ope_Tal_Listado_Detalle.Campo_No_Requisicion + " FROM ";
            Mi_SQL = Mi_SQL + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle + " WHERE ";
            Mi_SQL = Mi_SQL + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID + " = '" + Clase_Negocio.P_Listado_ID.Trim() + "'";
            Mi_SQL = Mi_SQL + " GROUP BY(" + Ope_Tal_Listado_Detalle.Campo_No_Requisicion + "))";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static Int32 Obtener_Consecutivo(String Campo_ID, String Tabla) {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }

        public static Boolean Modificar_Listado(Cls_Ope_Tal_Requisicion_Listado_Stock_Negocio Clase_Negocio) {
            Boolean Operacion_Realizada = false;
            String Mi_SQL = "";
            try {
                Mi_SQL = "UPDATE " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Listado.Campo_Estatus + " = '" + Clase_Negocio.P_Estatus + "'";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Listado.Campo_Listado_ID + " = '" + Clase_Negocio.P_Listado_ID + "'";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                Operacion_Realizada = true;
            } catch {
                Operacion_Realizada = false;
            }
            return Operacion_Realizada;
        }

	}

}