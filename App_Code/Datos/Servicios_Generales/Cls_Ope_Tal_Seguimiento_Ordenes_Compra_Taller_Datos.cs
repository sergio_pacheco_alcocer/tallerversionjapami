﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Seguimiento_Ordenes_Compra_Taller.Negocio;
using System.Text;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;

/// <summary>
/// Summary description for Cls_Ope_Tal_Seguimiento_Ordenes_Compra_Taller_Datos
/// </summary>
/// 
namespace JAPAMI.Seguimiento_Ordenes_Compra_Taller.Datos
{
    public class Cls_Ope_Tal_Seguimiento_Ordenes_Compra_Taller_Datos
    {

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Listado_Ordenes_Compra
        ///DESCRIPCIÓN: Hace la Consulta General sobre las Ordenes de Compra
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 04/Septiembre/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Listado_Ordenes_Compra(Cls_Ope_Tal_Seguimiento_Ordenes_Compra_Taller_Negocio Parametros)
        {
            DataTable Dt_Datos = new DataTable();
            DataSet Ds_Datos = null;
            Boolean Entro_Where = false;
            try
            {
                StringBuilder Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT  OC." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " AS NO_ORDEN_COMPRA");
                Mi_SQL.Append(", OC." + Ope_Tal_Ordenes_Compra.Campo_Folio + " AS FOLIO");
                Mi_SQL.Append(", OC." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Creo + " AS FECHA_CREO");
                Mi_SQL.Append(", OC." + Ope_Tal_Ordenes_Compra.Campo_Estatus + " AS ESTATUS");
                Mi_SQL.Append(", OC." + Ope_Tal_Ordenes_Compra.Campo_Nombre_Proveedor + " AS NOMBRE_PROVEEDOR");
                Mi_SQL.Append(", RTRIM(LTRIM(DP." + Cat_Dependencias.Campo_Nombre + ")) AS DEPENDENCIA");
                Mi_SQL.Append(" FROM " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " OC");
                Mi_SQL.Append(" INNER JOIN " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " RQ ON OC." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = RQ." + Ope_Tal_Requisiciones.Campo_No_Orden_Compra + "");
                Mi_SQL.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DP ON RQ." + Ope_Tal_Requisiciones.Campo_Dependencia_ID + " = DP." + Cat_Dependencias.Campo_Dependencia_ID + "");
                if (!String.IsNullOrEmpty(Parametros.P_No_OC)) {
                    if (Entro_Where) { Mi_SQL.Append(" AND "); } else { Mi_SQL.Append(" WHERE "); Entro_Where = true; }
                    Mi_SQL.Append(" OC." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " IN ('" + Parametros.P_No_OC + "')");
                }
                if (!String.Format("{0:ddMMyyyy}", new DateTime()).Equals(String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Inicio)))
                {
                    if (Entro_Where) { Mi_SQL.Append(" AND "); } else { Mi_SQL.Append(" WHERE "); Entro_Where = true; }
                    Mi_SQL.Append(" OC." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Creo + " >= '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Inicio) + "'");
                }
                if (!String.Format("{0:ddMMyyyy}", new DateTime()).Equals(String.Format("{0:ddMMyyyy}", Parametros.P_Fecha_Fin)))
                {
                    if (Entro_Where) { Mi_SQL.Append(" AND "); } else { Mi_SQL.Append(" WHERE "); Entro_Where = true; }
                    Mi_SQL.Append(" OC." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Creo + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Fin.AddDays(1)) + "'");
                }
                Mi_SQL.Append(" ORDER BY NO_ORDEN_COMPRA");                
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds_Datos != null) if (Ds_Datos.Tables.Count > 0) Dt_Datos = Ds_Datos.Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Consultar_Listado_Requisiciones. " + Ex.Message);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Historial_Ordenes_Compra
        ///DESCRIPCIÓN: Hace la Consulta del Historial sobre las Ordenes de Compra
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: 04/Septiembre/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Historial_Ordenes_Compra(Cls_Ope_Tal_Seguimiento_Ordenes_Compra_Taller_Negocio Parametros)
        {
            DataTable Dt_Datos = new DataTable();
            DataSet Ds_Datos = null;
            Boolean Entro_Where = false;
            try
            {
                StringBuilder Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT HIST." + Ope_Tal_Historial_Ord_Compra.Campo_Fecha + " AS FECHA");
                Mi_SQL.Append(", HIST." + Ope_Tal_Historial_Ord_Compra.Campo_Empleado + " AS EMPLEADO");
                Mi_SQL.Append(", HIST." + Ope_Tal_Historial_Ord_Compra.Campo_Estatus + " AS ESTATUS");
                Mi_SQL.Append(" FROM " + Ope_Tal_Historial_Ord_Compra.Tabla_Ope_Tal_Historial_Ord_Compra + " HIST");
                if (!String.IsNullOrEmpty(Parametros.P_No_OC))
                {
                    if (Entro_Where) { Mi_SQL.Append(" AND "); } else { Mi_SQL.Append(" WHERE "); Entro_Where = true; }
                    Mi_SQL.Append("HIST." + Ope_Tal_Historial_Ord_Compra.Campo_No_Orden_Compra + " IN ('" + Parametros.P_No_OC + "')");
                }
                Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds_Datos != null) if (Ds_Datos.Tables.Count > 0) Dt_Datos = Ds_Datos.Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Consultar_Listado_Requisiciones. " + Ex.Message);
            }
            return Dt_Datos;
        }


    }
}
