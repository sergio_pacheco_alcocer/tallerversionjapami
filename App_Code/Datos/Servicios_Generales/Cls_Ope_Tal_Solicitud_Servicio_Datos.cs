﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Datos;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Tal_Generacion_Solicitud_Servicio_Datos
/// </summary>

namespace JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Datos
{
    public class Cls_Ope_Tal_Solicitud_Servicio_Datos
    {

        #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Solicitud_Servicio      
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 03/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Solicitud_Servicio(Cls_Ope_Tal_Solicitud_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Parametros.P_No_Solicitud = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv, Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud, 20));
            try
            {
                String Mi_SQL = "INSERT INTO " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " (" + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Programa_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Estatus + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Procedencia + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Bateria + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Llantas + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Empleado_Solicito_ID + "";
                if (Parametros.P_Kilometraje > (-1))
                {
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Kilometraje + "";
                }
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Correo_Electronico + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Usuario_Creo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Creo + ")";
                Mi_SQL = Mi_SQL + " VALUES ('" + Parametros.P_No_Solicitud + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Folio_Solicitud + "'";
                Mi_SQL = Mi_SQL + ", '" + String.Format("{0:yyyy/MM/dd}", Parametros.P_Fecha_Elaboracion) + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo_Bien + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo_Servicio + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Dependencia_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Programa_Proyecto_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Descripcion_Servicio + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Procedencia + "'";
                if (Parametros.P_Fecha_Bateria != new DateTime())
                    Mi_SQL = Mi_SQL + ", '" + String.Format("{0:yyyy/MM/dd}", Parametros.P_Fecha_Bateria) + "'";
                else
                    Mi_SQL = Mi_SQL + ", null";
                if (Parametros.P_Fecha_Llantas != new DateTime())
                    Mi_SQL = Mi_SQL + ", " + String.Format("{0:yyyy/MM/dd}", Parametros.P_Fecha_Llantas) + "'";
                else
                    Mi_SQL = Mi_SQL + ", null";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Empleado_Solicito_ID + "'";
                if (Parametros.P_Kilometraje > (-1))
                {
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Kilometraje + "'";
                }
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Correo_Electronico + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE())";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Alta_Registro_Seguimiento_Solicitud(Parametros, ref Cmd);
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Solicitud_Servicio      
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro de forma remota
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 03/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Solicitud_Servicio(Cls_Ope_Tal_Solicitud_Servicio_Negocio Parametros, ref SqlCommand P_Cmd)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Parametros.P_No_Solicitud = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv, Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud, 20));
            try
            {
                String Mi_SQL = "INSERT INTO " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " (" + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Estatus + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Procedencia + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Bateria + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Llantas + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Empleado_Solicito_ID + "";
                if (Parametros.P_Kilometraje > (-1))
                {
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Kilometraje + "";
                }
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Correo_Electronico + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Usuario_Creo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Creo + ")";
                Mi_SQL = Mi_SQL + " VALUES ('" + Parametros.P_No_Solicitud + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Folio_Solicitud + "'";
                Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Elaboracion) + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo_Bien + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo_Servicio + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Dependencia_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Bien_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Descripcion_Servicio + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Procedencia + "'";
                if (Parametros.P_Fecha_Bateria != new DateTime())
                    Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Bateria) + "'";
                else
                    Mi_SQL = Mi_SQL + ", null";
                if (Parametros.P_Fecha_Llantas != new DateTime())
                    Mi_SQL = Mi_SQL + ", " + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Llantas) + "'";
                else
                    Mi_SQL = Mi_SQL + ", null";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Empleado_Solicito_ID + "'";
                if (Parametros.P_Kilometraje > (-1))
                {
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Kilometraje + "'";
                }
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Correo_Electronico + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE())";
                P_Cmd.CommandText = Mi_SQL;
                P_Cmd.ExecuteNonQuery();

                Alta_Registro_Seguimiento_Solicitud(Parametros, ref P_Cmd);
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modifica_Solicitud_Servicio      
        ///DESCRIPCIÓN          : Actualiza en la Base de Datos un registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a
        ///                         Actualizar en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 03/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Modifica_Solicitud_Servicio(Cls_Ope_Tal_Solicitud_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                Cls_Ope_Tal_Solicitud_Servicio_Negocio Tmp = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                Tmp.P_No_Solicitud = Parametros.P_No_Solicitud;
                Tmp = Tmp.Consultar_Detalles_Solicitud_Servicio();

                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                String Mi_SQL = "UPDATE " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " = '" + Parametros.P_Folio_Solicitud + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien + " = '" + Parametros.P_Tipo_Bien + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " = '" + Parametros.P_Tipo_Servicio + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = '" + Parametros.P_Bien_ID + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " = '" + Parametros.P_Descripcion_Servicio + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Empleado_Solicito_ID + " = '" + Parametros.P_Empleado_Solicito_ID + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Motivo_Rechazo + " = '" + Parametros.P_Motivo_Rechazo + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Correo_Electronico + " = '" + Parametros.P_Correo_Electronico + "'";
                if (Parametros.P_Kilometraje > (-1))
                {
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Kilometraje + " = '" + Parametros.P_Kilometraje + "'";
                }
                else
                {
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Kilometraje + " = NULL";
                }
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                if (!Tmp.P_Estatus.Trim().Equals(Parametros.P_Estatus.Trim()))
                {
                    Alta_Registro_Seguimiento_Solicitud(Parametros, ref Cmd);
                }

                if (Parametros.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Parametros.P_Cmmd != null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Solicitud_Servicio      
        ///DESCRIPCIÓN          : Saca el detalle de un Solicitud
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a
        ///                         Consultar en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 03/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Cls_Ope_Tal_Solicitud_Servicio_Negocio Consultar_Detalles_Solicitud_Servicio(Cls_Ope_Tal_Solicitud_Servicio_Negocio Parametros)
        {

            String Mi_SQL = null;
            SqlDataReader Data_Reader;
            SqlTransaction Trans = null;
            SqlCommand Cmd = new SqlCommand();
            SqlConnection Cn = new SqlConnection();
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();

            try
            {
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }

                Mi_SQL = "SELECT * FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + "";
                if (Parametros.P_No_Solicitud > (-1))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                }
                else if (!String.IsNullOrEmpty(Parametros.P_Folio_Solicitud))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " = '" + Parametros.P_Folio_Solicitud + "'";
                }
                Cmd.CommandText = Mi_SQL;
                Data_Reader = Cmd.ExecuteReader();
                while (Data_Reader.Read())
                {
                    Solicitud.P_No_Solicitud = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud].ToString())) ? Convert.ToInt32(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud]) : -1;
                    Solicitud.P_Folio_Solicitud = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud].ToString())) ? Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud].ToString() : "";
                    Solicitud.P_Fecha_Elaboracion = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion].ToString())) ? Convert.ToDateTime(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion]) : new DateTime();
                    Solicitud.P_Tipo_Servicio = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio].ToString())) ? Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio].ToString() : "";
                    Solicitud.P_Dependencia_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID].ToString())) ? Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID].ToString() : "";
                    Solicitud.P_Programa_Proyecto_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Programa_ID].ToString())) ? Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Programa_ID].ToString() : "";
                    Solicitud.P_Bien_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Bien_ID].ToString())) ? Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Bien_ID].ToString() : "";
                    Solicitud.P_Tipo_Bien = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien].ToString())) ? Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien].ToString() : "";
                    Solicitud.P_Descripcion_Servicio = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio].ToString())) ? Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio].ToString() : "";
                    Solicitud.P_Estatus = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Estatus].ToString())) ? Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Estatus].ToString() : "";
                    Solicitud.P_Empleado_Solicito_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Empleado_Solicito_ID].ToString())) ? Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Empleado_Solicito_ID].ToString() : "";
                    Solicitud.P_Empleado_Autorizo_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Empleado_Autorizo_ID].ToString())) ? Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Empleado_Autorizo_ID].ToString() : "";
                    Solicitud.P_Motivo_Rechazo = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Motivo_Rechazo].ToString())) ? Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Motivo_Rechazo].ToString() : "";
                    Solicitud.P_Fecha_Recepcion_Programada = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Prog].ToString())) ? Convert.ToDateTime(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Prog]) : new DateTime();
                    Solicitud.P_Fecha_Recepcion_Real = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real].ToString())) ? Convert.ToDateTime(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real]) : new DateTime();
                    Solicitud.P_Kilometraje = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Kilometraje].ToString())) ? Convert.ToDouble(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Kilometraje]) : -1.0;
                    Solicitud.P_Procedencia = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Procedencia].ToString())) ? Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Procedencia].ToString() : "";
                    Solicitud.P_Correo_Electronico = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Correo_Electronico].ToString())) ? Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Correo_Electronico].ToString() : "";
                    Solicitud.P_No_Contrarecibo = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_No_Contrarecibo].ToString())) ? Convert.ToInt32(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_No_Contrarecibo]) : -1;
                    Solicitud.P_No_Reserva = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_No_Reserva].ToString())) ? Convert.ToInt32(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_No_Reserva]) : -1;
                    Solicitud.P_Fecha_Contrarecibo = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Fecha_Contrarecibo].ToString())) ? Convert.ToDateTime(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Fecha_Contrarecibo]) : new DateTime();
                    Solicitud.P_Fecha_Bateria = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Fecha_Bateria].ToString())) ? Convert.ToDateTime(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Fecha_Bateria]) : new DateTime();
                    Solicitud.P_Fecha_Llantas = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Fecha_Llantas].ToString())) ? Convert.ToDateTime(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Fecha_Llantas]) : new DateTime();
                    Solicitud.P_Cotizador_ID = (!String.IsNullOrEmpty(Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Cotizador_ID].ToString())) ? Data_Reader[Ope_Tal_Solicitudes_Serv.Campo_Cotizador_ID].ToString() : "";
                }
                Data_Reader.Close();
                if (Solicitud.P_No_Solicitud > (-1))
                {
                    Mi_SQL = "SELECT SEGUIMIENTO." + Ope_Tal_Seg_Sol_Serv.Campo_No_Registro + " AS NO_REGISTRO";
                    Mi_SQL = Mi_SQL + ", SEGUIMIENTO." + Ope_Tal_Seg_Sol_Serv.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL = Mi_SQL + ", SEGUIMIENTO." + Ope_Tal_Seg_Sol_Serv.Campo_Fecha_Asigno + " AS FECHA_ASIGNO";
                    Mi_SQL = Mi_SQL + ", (EMPLEADOS." + Cat_Empleados.Campo_No_Empleado;
                    Mi_SQL = Mi_SQL + " +' - '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno;
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno;
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Nombre + ") AS EMPLEADO_ASIGNO";
                    Mi_SQL = Mi_SQL + ", SEGUIMIENTO." + Ope_Tal_Seg_Sol_Serv.Campo_Motivo_Rechazo + " AS MOTIVO_RECHAZO";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Seg_Sol_Serv.Tabla_Ope_Tal_Seg_Sol_Serv + " SEGUIMIENTO";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS ";
                    Mi_SQL = Mi_SQL + " ON SEGUIMIENTO." + Ope_Tal_Seg_Sol_Serv.Campo_Empleado_Asigno_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID;
                    Mi_SQL = Mi_SQL + " WHERE SEGUIMIENTO." + Ope_Tal_Seg_Sol_Serv.Campo_No_Solicitud + " = '" + Solicitud.P_No_Solicitud + "'";
                    Mi_SQL = Mi_SQL + " ORDER BY NO_REGISTRO ASC";
                    //Ejecutar Consulta
                    Cmd.CommandText = Mi_SQL;
                    Obj_Adaptador.SelectCommand = Cmd;
                    Obj_Adaptador.Fill(Solicitud.P_Dt_Seguimiento);
                }
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (Exception Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                String Mensaje = "Error al intentar consultar los datos. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
            return Solicitud;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Listado_Solicitudes_Servicio      
        ///DESCRIPCIÓN          : Saca un listado de las Solicitudes Cargadas
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a
        ///                         Consultar en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 03/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Listado_Solicitudes_Servicio(Cls_Ope_Tal_Solicitud_Servicio_Negocio Parametros)
        {
            String Mi_SQL = "";
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = true;
            try
            {
                if (Parametros.P_Tipo_Bien.Trim().Equals("TODOS") || Parametros.P_Tipo_Bien.Trim().Equals("BIEN_MUEBLE"))
                {
                    Mi_SQL = "SELECT SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", 'BIEN MUEBLE' AS TIPO_BIEN";
                    Mi_SQL = Mi_SQL + ", STR(BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + ") AS NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", STR(BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + ") AS NO_ECONOMICO";
                    Mi_SQL = Mi_SQL + ", CASE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                    Mi_SQL = Mi_SQL + " WHEN 'REPARACION' THEN 'REPARACIÓN' WHEN 'GARANTIA' THEN 'GARANTIA' WHEN 'SERVICIO_GENERAL  ' THEN 'SERVICIO GENERAL' WHEN 'SERVICIO_CORRECTIVO' THEN 'SERVICIO CORRECTIVO' WHEN 'SERVICIO_PREVENTIVO' THEN 'SERVICIO PREVENTIVO' END AS TIPO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                    Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                    Mi_SQL = Mi_SQL + ", CONVERT(char, ISNULL(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + ", ''), 103) AS FECHA_ELABORACION_TBL";  
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Prog + " AS FECHA_RECEPCION_PROG";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " AS FECHA_RECEPCION_REAL";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " AS FOLIO";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Bienes_Muebles.Tabla_Ope_Pat_Bienes_Muebles + " BIENES_MUEBLES";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " WHERE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien + " = 'BIEN_MUEBLE'";
                    if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " in( '" + Parametros.P_Dependencia_ID + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Tipo_Servicio))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " IN ('" + Parametros.P_Tipo_Servicio + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Folio_Solicitud))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " = '" + Parametros.P_Folio_Solicitud + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Bien_ID))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Bien_Mueble_ID + " = '" + Parametros.P_Bien_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Procedencia))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Procedencia + " = '" + Parametros.P_Procedencia + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_No_Economico))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Serie + " = '" + Parametros.P_No_Economico + "'";
                    }
                    if (Parametros.P_No_Inventario > (-1))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " BIENES_MUEBLES." + Ope_Pat_Bienes_Muebles.Campo_Numero_Inventario + " = '" + Parametros.P_No_Inventario + "'";
                    }
                    if (Parametros.P_No_Solicitud > (-1))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_Busq_Fecha_Recepcion_Inicial).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " >= '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Busq_Fecha_Recepcion_Inicial) + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_Busq_Fecha_Recepcion_Final).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Busq_Fecha_Recepcion_Final.AddDays(1)) + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Cotizador_ID))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Cotizador_ID + " = '" + Parametros.P_Cotizador_ID + "'";
                    }
                }
                if (Parametros.P_Tipo_Bien.Trim().Equals("TODOS")) Mi_SQL += " UNION ";
                if (Parametros.P_Tipo_Bien.Trim().Equals("TODOS") || Parametros.P_Tipo_Bien.Trim().Equals("VEHICULO"))
                {
                    Mi_SQL = Mi_SQL + "SELECT SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", 'VEHICULO' AS TIPO_BIEN";
                    Mi_SQL = Mi_SQL + ", STR(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + ") AS NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " AS NO_ECONOMICO";
                    Mi_SQL = Mi_SQL + ", CASE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                    Mi_SQL = Mi_SQL + " WHEN 'SERVICIO_CORRECTIVO' THEN 'SERVICIO CORRECTIVO' WHEN 'SERVICIO_PREVENTIVO' THEN 'SERVICIO PREVENTIVO' WHEN 'SERVICIO_GENERAL' THEN 'SERVICIO GENERAL' WHEN 'REVISTA_MECANICA' THEN 'REVISTA MECANICA' WHEN 'VERIFICACION' THEN 'VERIFICACIÓN' END AS TIPO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                    Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                    Mi_SQL = Mi_SQL + ", CONVERT(char, ISNULL(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + ", ''), 103) AS FECHA_ELABORACION_TBL";  
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Prog + " AS FECHA_RECEPCION_PROG";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " AS FECHA_RECEPCION_REAL";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " AS FOLIO";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " WHERE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien + " = 'VEHICULO'";
                    if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " in( '" + Parametros.P_Dependencia_ID + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Tipo_Servicio))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " IN ('" + Parametros.P_Tipo_Servicio + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Folio_Solicitud))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " = '" + Parametros.P_Folio_Solicitud + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Bien_ID))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + " = '" + Parametros.P_Bien_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Procedencia))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Procedencia + " = '" + Parametros.P_Procedencia + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_No_Economico))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Economico + " = '" + Parametros.P_No_Economico + "'";
                    }
                    if (Parametros.P_No_Inventario > (-1))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " = '" + Parametros.P_No_Inventario + "'";
                    }
                    if (Parametros.P_No_Solicitud > (-1))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_Busq_Fecha_Recepcion_Inicial).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " >= '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Busq_Fecha_Recepcion_Inicial) + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_Busq_Fecha_Recepcion_Final).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Busq_Fecha_Recepcion_Final.AddDays(1)) + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Cotizador_ID))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Cotizador_ID + " = '" + Parametros.P_Cotizador_ID + "'";
                    }
                }
                if (Parametros.P_Tipo_Bien.Trim().Equals("TODOS")) Mi_SQL += " UNION ";
                if (Parametros.P_Tipo_Bien.Trim().Equals("TODOS") || Parametros.P_Tipo_Bien.Trim().Equals("OTRO"))
                {
                    Mi_SQL = Mi_SQL + "SELECT SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", 'OTRO' AS TIPO_BIEN";
                    Mi_SQL = Mi_SQL + ", '-' AS NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", '' AS NO_ECONOMICO";
                    Mi_SQL = Mi_SQL + ", CASE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                    Mi_SQL = Mi_SQL + " WHEN 'MANTENIMIENTO' THEN 'SERVICIO DE MANTENIMIENTO' END AS TIPO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                    Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                    Mi_SQL = Mi_SQL + ", CONVERT(char, ISNULL(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + ", ''), 103) AS FECHA_ELABORACION_TBL";  
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Prog + " AS FECHA_RECEPCION_PROG";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " AS FECHA_RECEPCION_REAL";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " AS FOLIO";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " WHERE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien + " = 'OTRO'";
                    if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " in( '" + Parametros.P_Dependencia_ID + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Tipo_Servicio))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " IN ('" + Parametros.P_Tipo_Servicio + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Folio_Solicitud))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " = '" + Parametros.P_Folio_Solicitud + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Procedencia))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Procedencia + " = '" + Parametros.P_Procedencia + "'";
                    }
                    if (Parametros.P_No_Solicitud > (-1))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_Busq_Fecha_Recepcion_Inicial).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " >= '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Busq_Fecha_Recepcion_Inicial) + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_Busq_Fecha_Recepcion_Final).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Busq_Fecha_Recepcion_Final.AddDays(1)) + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Cotizador_ID))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Cotizador_ID + " = '" + Parametros.P_Cotizador_ID + "'";
                    }
                }
                if (!String.IsNullOrEmpty(Parametros.P_Ordenar_Listado)) {
                    Mi_SQL += " ORDER BY " + Parametros.P_Ordenar_Listado;
                }
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Autorizacion_Interna_Solicitud_Servicio      
        ///DESCRIPCIÓN          : Autoriza o Rechaza la SOlicitud en la Base de Datos un registro
        ///                       de Forma Interna.
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a
        ///                         Actualizar en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 05/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Autorizacion_Interna_Solicitud_Servicio(Cls_Ope_Tal_Solicitud_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                String Mi_SQL = "UPDATE " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Motivo_Rechazo + " = '" + Parametros.P_Motivo_Rechazo + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Parametros.P_Empleado_Solicito_ID = Parametros.P_Empleado_Autorizo_ID;
                Alta_Registro_Seguimiento_Solicitud(Parametros, ref Cmd);
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Autorizacion_Solicitud_Servicio      
        ///DESCRIPCIÓN          : Autoriza o Rechaza la SOlicitud en la Base de Datos un registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a
        ///                         Actualizar en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 05/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Autorizacion_Solicitud_Servicio(Cls_Ope_Tal_Solicitud_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                Parametros.P_Folio_Solicitud = Obtener_Folio_Consecutivo(Parametros.P_Tipo_Servicio);
                String Mi_SQL = "UPDATE " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                if (Parametros.P_Estatus.Trim().Equals("AUTORIZADA"))
                {
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " = '" + Parametros.P_Folio_Solicitud + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Autorizacion + " = GETDATE()";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Empleado_Autorizo_ID + " = '" + Parametros.P_Empleado_Autorizo_ID + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Prog + " = '" + Convert.ToDateTime(Parametros.P_Fecha_Recepcion_Programada).ToString("yyyy/MM/dd") + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " = '" + Parametros.P_Tipo_Servicio + "'";
                }
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Motivo_Rechazo + " = '" + Parametros.P_Motivo_Rechazo + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Parametros.P_Empleado_Solicito_ID = Parametros.P_Empleado_Autorizo_ID;
                Alta_Registro_Seguimiento_Solicitud(Parametros, ref Cmd);
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Autorizacion_Solicitud_Servicio_Interno      
        ///DESCRIPCIÓN          : Autoriza o Rechaza la SOlicitud en la Base de Datos un registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a
        ///                         Actualizar en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 05/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Int32 Autorizacion_Solicitud_Servicio_Interno(Cls_Ope_Tal_Solicitud_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Int32 Numero_Servicio = 0;
            try
            {
                Parametros.P_Folio_Solicitud = Obtener_Folio_Consecutivo(Parametros.P_Tipo_Servicio);
                String Mi_SQL = "UPDATE " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                if (Parametros.P_Estatus.Trim().Equals("AUTORIZADA"))
                {
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " = '" + Parametros.P_Folio_Solicitud + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Autorizacion + " = GETDATE()";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Empleado_Autorizo_ID + " = '" + Parametros.P_Empleado_Autorizo_ID + "'";
                }
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Motivo_Rechazo + " = '" + Parametros.P_Motivo_Rechazo + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Parametros.P_Empleado_Solicito_ID = Parametros.P_Empleado_Autorizo_ID;
                Alta_Registro_Seguimiento_Solicitud(Parametros, ref Cmd);

                if (Parametros.P_Estatus.Trim().Equals("AUTORIZADA"))
                {
                    if (!String.IsNullOrEmpty(Parametros.P_Tipo_Servicio))
                    {
                        if (Parametros.P_Tipo_Servicio.Trim().Equals("SERVICIO_PREVENTIVO"))
                        {
                            Cls_Ope_Tal_Servicios_Preventivos_Negocio Serv_Prev = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
                            Serv_Prev.P_Mecanico_ID = "";
                            Serv_Prev.P_Estatus = "PENDIENTE";
                            Serv_Prev.P_No_Solicitud = Parametros.P_No_Solicitud;
                            Serv_Prev.P_Usuario = Parametros.P_Usuario;
                            Serv_Prev.Alta_Servicio_Preventivo(ref Cmd);
                        }
                        else if (Parametros.P_Tipo_Servicio.Trim().Equals("SERVICIO_CORRECTIVO"))
                        {
                            Cls_Ope_Tal_Servicios_Correctivos_Negocio Serv_Correc = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
                            Serv_Correc.P_Mecanico_ID = "";
                            Serv_Correc.P_Estatus = "PENDIENTE";
                            Serv_Correc.P_No_Solicitud = Parametros.P_No_Solicitud;
                            Serv_Correc.P_Usuario = Parametros.P_Usuario;
                            Numero_Servicio = Serv_Correc.Alta_Servicio_Correctivo(ref Cmd);
                        }
                    }
                }

                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
            return Numero_Servicio;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID)
        {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try
            {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            }
            catch (SqlException Ex)
            {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_Folio_Consecutivo(String Tipo_Servicio)
        {
            String Id = Convertir_A_Formato_ID(1, 10);
            try
            {
                String Mi_SQL = "SELECT MAX(" + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + ") FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                switch (Tipo_Servicio)
                {
                    case "REVISTA_MECANICA":
                        Mi_SQL += " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " = 'REVISTA_MECANICA'";
                        break;
                    case "VERIFICACION":
                        Mi_SQL += " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " = 'VERIFICACION'";
                        break;
                    case "SERVICIO_PREVENTIVO":
                        Mi_SQL += " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " <> 'VERIFICACION' AND " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " <> 'REVISTA_MECANICA'";
                        break;
                    case "SERVICIO_CORRECTIVO":
                        Mi_SQL += " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " <> 'VERIFICACION' AND " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " <> 'REVISTA_MECANICA'";
                        break;
                    case "SERVICIO_GENERAL":
                        Mi_SQL += " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " <> 'VERIFICACION' AND " + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " <> 'REVISTA_MECANICA'";
                        break;
                }
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), 10);
                }
            }
            catch (SqlException Ex)
            {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
        ///PARAMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
        {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
            {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Registro_Seguimiento_Solicitud      
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro en el
        ///                       seguimiento de la solicitud
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///                     2.  Cmd. Manejo de la Transicion.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 04/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Registro_Seguimiento_Solicitud(Cls_Ope_Tal_Solicitud_Servicio_Negocio Parametros, ref SqlCommand Cmd)
        {
            Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Seg_Sol_Serv.Tabla_Ope_Tal_Seg_Sol_Serv, Ope_Tal_Seg_Sol_Serv.Campo_No_Registro, 20));
            try
            {
                String Mi_SQL = "INSERT INTO " + Ope_Tal_Seg_Sol_Serv.Tabla_Ope_Tal_Seg_Sol_Serv;
                Mi_SQL = Mi_SQL + " (" + Ope_Tal_Seg_Sol_Serv.Campo_No_Registro + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seg_Sol_Serv.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seg_Sol_Serv.Campo_Estatus + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seg_Sol_Serv.Campo_Empleado_Asigno_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seg_Sol_Serv.Campo_Fecha_Asigno + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seg_Sol_Serv.Campo_Motivo_Rechazo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seg_Sol_Serv.Campo_Usuario_Creo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Seg_Sol_Serv.Campo_Fecha_Creo + ")";
                Mi_SQL = Mi_SQL + " VALUES ('" + No_Registro + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Solicitud + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Empleado_Solicito_ID + "'";
                Mi_SQL = Mi_SQL + ", GETDATE()";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Motivo_Rechazo + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE())";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
            }
            catch (SqlException Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Asignar_Cotizador_Solicitud      
        ///DESCRIPCIÓN          : Le carga a la solicitud un cotizador
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a
        ///                         Actualizar en la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 05/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Asignar_Cotizador_Solicitud(Cls_Ope_Tal_Solicitud_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                String Mi_SQL = "UPDATE " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Cotizador_ID + " = '" + Parametros.P_Cotizador_ID + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Parametros.P_Empleado_Solicito_ID = Parametros.P_Empleado_Autorizo_ID;
                Alta_Registro_Seguimiento_Solicitud(Parametros, ref Cmd);

                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        #endregion

    }
}
