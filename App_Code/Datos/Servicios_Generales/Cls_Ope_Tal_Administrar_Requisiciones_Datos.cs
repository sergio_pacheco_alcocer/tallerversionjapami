﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Taller_Mecanico.Administrar_Requisiciones.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Stock;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Requisiciones.Datos;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;

/// <summary>
/// Summary description for Cls_Ope_Tal_Administrar_Requisiciones_Datos
/// </summary>
namespace JAPAMI.Taller_Mecanico.Administrar_Requisiciones.Datos
{
    public class Cls_Ope_Tal_Administrar_Requisiciones_Datos
    {

        #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Requisicion
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para modificar una requisicion
        ///PARAMETROS:   1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 10/Noviembre/2010 
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public void Modificar_Requisicion(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Fecha_Creo = DateTime.Now.ToString("dd/MM/yy").ToUpper();
            String Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
            String Mi_SQL = "";
            String Mensaje = "";
            Double Monto_Cotizado = 0;
            Double Monto_Anterior = 0;
            Double Diferencia = 0;
            String Cargo = "";
            String Abono = "";
            String Partida_ID = "0";//Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Partida_ID].ToString().Trim();
            String Proyecto_ID = "0";//Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID].ToString().Trim();
            String FF = "0";//Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID].ToString().Trim();
            String Dependencia_ID = "0";
            String Tipo_Solicitud_Pago_ID = "0";
            String Capitulo_ID = "0";
            DataTable Dt_Parametros = new DataTable();
            DataTable Dt_Productos = new DataTable();
            //INSERTAR LA REQUISICION   
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Cls_Tal_Parametros_Negocio Negocio_Parametros = new Cls_Tal_Parametros_Negocio();
            try
            {

                Mi_SQL = "SELECT * FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " WHERE " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = '" + Requisicion_Negocio.P_Requisicion_ID.Trim() + "'";
                //Ejecutar Consulta
                Cmd.CommandText = Mi_SQL;
                Obj_Adaptador.SelectCommand = Cmd;
                Obj_Adaptador.Fill(Dt_Productos);
                DataTable Dt_Requisicion = Consultar_Codigo_Programatico_RQ(Requisicion_Negocio);
                Negocio_Parametros.P_Cmmd = Cmd;
                Negocio_Parametros = Negocio_Parametros.Consulta_Parametros_Presupuesto(Requisicion_Negocio.P_Numero_Economico, "VEHICULO");
                Partida_ID = Dt_Requisicion.Rows[0]["PARTIDA_ID"].ToString().Trim();//Negocio_Parametros.P_Partida_ID;
                FF = Dt_Requisicion.Rows[0]["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();//Negocio_Parametros.P_Fuente_Financiamiento_ID;
                Capitulo_ID = Negocio_Parametros.P_Capitulo_ID;
                Dependencia_ID = Dt_Requisicion.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();//Requisicion_Negocio.P_Dependencia_ID;
                Proyecto_ID = Dt_Requisicion.Rows[0]["PROYECTO_PROGRAMA_ID"].ToString().Trim();//Requisicion_Negocio.P_Programa_ID;
                Tipo_Solicitud_Pago_ID = Negocio_Parametros.P_Tipo_Solicitud_Pago_ID;
                //Limpiamos la Variable Mi_SQL
                Double.TryParse(Dt_Requisicion.Rows[0]["TOTAL_COTIZADO"].ToString().Trim(), out Monto_Cotizado);
                Double.TryParse(Dt_Requisicion.Rows[0]["TOTAL"].ToString().Trim(), out Monto_Anterior);
                if (Monto_Cotizado > Monto_Anterior)
                {
                    Diferencia = Monto_Cotizado - Monto_Anterior;
                }
                if (Monto_Cotizado < Monto_Anterior)
                {
                    Diferencia = Monto_Anterior - Monto_Cotizado;
                }
                DataTable Dt_Partidas_Reserva = new DataTable();
                Dt_Partidas_Reserva = new DataTable("Dt_Partidas_Reserva");
                Dt_Partidas_Reserva.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("FECHA_CREO", System.Type.GetType("System.DateTime"));
                Dt_Partidas_Reserva.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                Dt_Partidas_Reserva.Columns.Add("ANIO", System.Type.GetType("System.String"));
                DataRow Dr_Partida = Dt_Partidas_Reserva.NewRow();
                Dr_Partida["FUENTE_FINANCIAMIENTO_ID"] = FF;
                Dr_Partida["PROGRAMA_ID"] = Proyecto_ID;
                Dr_Partida["PARTIDA_ID"] = Partida_ID;
                Dr_Partida["FECHA_CREO"] = DateTime.Today;
                Dr_Partida["DEPENDENCIA_ID"] = Dependencia_ID;
                Dr_Partida["CAPITULO_ID"] = Capitulo_ID;
                Dr_Partida["ANIO"] = DateTime.Today.Year.ToString();
                Dr_Partida["IMPORTE"] = Convert.ToDouble(Diferencia);
                Dt_Partidas_Reserva.Rows.Add(Dr_Partida);


                Int32 Anio = DateTime.Today.Year;//Convert.ToInt32(Dt_Partidas_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Anio]);
                String Num_Reserva = Dt_Requisicion.Rows[0]["NUM_RESERVA"].ToString().Trim();

                Double Disponible = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Disponible_Partida(FF, Proyecto_ID, Dependencia_ID, Partida_ID, Anio.ToString(), Ope_Psp_Presupuesto_Aprobado.Campo_Disponible);
                //DE ACUERDO AL ESTATUS MODIFICAMOS LA REQUISICION
                switch (Requisicion_Negocio.P_Estatus.Trim())
                {
                    case "ALMACEN":
                        Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " SET " +
                                Ope_Tal_Requisiciones.Campo_Estatus + " = 'PROCESAR', " +
                                Ope_Tal_Requisiciones.Campo_Empleado_Autorizacion_ID + " = '" + Requisicion_Negocio.P_Empleado_ID + "', " +
                                Ope_Tal_Requisiciones.Campo_Empleado_Filtrado_ID + " = '" + Requisicion_Negocio.P_Empleado_ID + "', " +
                                Ope_Tal_Requisiciones.Campo_Fecha_Autorizacion + " = GETDATE(), " +
                                Ope_Tal_Requisiciones.Campo_Fecha_Filtrado + " = GETDATE()" +
                                " WHERE " + Ope_Tal_Requisiciones.Campo_Folio + "='" + Requisicion_Negocio.P_Folio + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        break;

                    case "RECHAZADA":
                        Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " SET " +
                                Ope_Tal_Requisiciones.Campo_Estatus + "= 'EN CONSTRUCCION', " +
                                Ope_Tal_Requisiciones.Campo_Fecha_Rechazo + " = GETDATE(), " +
                                Ope_Tal_Requisiciones.Campo_Alerta + " = 'AMARILLO', " +
                                Ope_Tal_Requisiciones.Campo_Empleado_Rechazo_ID + " = '" + Requisicion_Negocio.P_Empleado_ID + "'" +
                                " WHERE " + Ope_Tal_Requisiciones.Campo_Folio + " = '" + Requisicion_Negocio.P_Folio + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        break;

                    case "CANCELADA":
                        Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " SET " +
                                Ope_Tal_Requisiciones.Campo_Estatus + "= '" + Requisicion_Negocio.P_Estatus + "', " +
                                Ope_Tal_Requisiciones.Campo_Fecha_Cancelada + " = GETDATE(), " +
                                Ope_Tal_Requisiciones.Campo_Empleado_Cancelada_ID + " = '" + Requisicion_Negocio.P_Empleado_ID + "'" +
                                " WHERE " + Ope_Tal_Requisiciones.Campo_Folio + " = '" + Requisicion_Negocio.P_Folio + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        //REALIZAMOS LA CONSULTA PARA OBTENER TODOS LOS REGISTROS DE LA REQUISICION CON SU PARTIDA CORRESPONDIENTE
                        Cargo = Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE;
                        Abono = Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO;

                        Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cargo, Abono, Dt_Partidas_Reserva, Cmd);
                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Num_Reserva, Cargo, Abono, Monto_Anterior, "", "", "", "", Cmd);

                        //En caso de ser requisicion de Stock liberamos los productos
                        if (Requisicion_Negocio.P_Tipo_Articulo == "PRODUCTO" && Requisicion_Negocio.P_Tipo == "STOCK")
                        {
                            //Cls_Ope_Alm_Stock.Descomprometer_Producto(Dt_Productos, Ope_Tal_Req_Refaccion.Campo_Refaccion_ID, Ope_Tal_Req_Refaccion.Campo_Cantidad, ref Cmd);
                        }
                        break;
                    case "CONFIRMADA":
                        Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " SET " +
                             Ope_Tal_Requisiciones.Campo_Estatus + "= '" + Requisicion_Negocio.P_Estatus + "', " +
                             Ope_Tal_Requisiciones.Campo_Fecha_Confirmacion + " = GETDATE(), " +
                             Ope_Tal_Requisiciones.Campo_Empleado_Confirmacion_ID + " = '" + Requisicion_Negocio.P_Empleado_ID + "'" +
                             " WHERE " + Ope_Com_Requisiciones.Campo_Folio + " = '" + Requisicion_Negocio.P_Folio + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        ///MANEJO DE PRESUPUESTO
                        //1. Primero se consulta las particularidaddes de la reserva, que es partida, dependencia, fte financ,                         
                        Cargo = "";
                        Abono = "";
                        //Double Disponible = Double.Parse(Dt_Presupuesto_Actual.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Disponible].ToString());

                        //2. Verificamos a que momento presupuestal se le hace el cargo o el abono de acuerdo a la direrencia.
                        //Double Diferencia = 0;
                        if (Monto_Cotizado > Monto_Anterior)
                        {

                            Cargo = Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO;
                            Abono = Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE;
                            //Diferencia = Monto_Cotizado - Monto_Anterior;

                            Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cargo, Abono, Dt_Partidas_Reserva, Cmd);
                            Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Num_Reserva, Cargo, Abono, Diferencia, "", "", "", "", Cmd);

                            Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                            Mi_SQL = Mi_SQL + " SET " + Ope_Psp_Reservas.Campo_Importe_Inicial + " = " + Ope_Psp_Reservas.Campo_Importe_Inicial + " + " + Diferencia;
                            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Saldo + " = " + Ope_Psp_Reservas.Campo_Saldo + " + " + Diferencia;
                            Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + Num_Reserva.Trim() + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();

                            Mi_SQL = "UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles;
                            Mi_SQL = Mi_SQL + " SET " + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " = " + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " + " + Diferencia;
                            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " + " + Diferencia;
                            Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = '" + Num_Reserva.Trim() + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();

                        }
                        if (Monto_Cotizado < Monto_Anterior)
                        {
                            Cargo = Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE;
                            Abono = Cls_Ope_Psp_Manejo_Presupuesto.PRE_COMPROMETIDO;
                            //Diferencia = Monto_Anterior - Monto_Cotizado;

                            Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Cargo, Abono, Dt_Partidas_Reserva, Cmd);
                            Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Num_Reserva, Cargo, Abono, Diferencia, "", "", "", "", Cmd);

                            Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                            Mi_SQL = Mi_SQL + " SET " + Ope_Psp_Reservas.Campo_Importe_Inicial + " = " + Ope_Psp_Reservas.Campo_Importe_Inicial + " - " + Diferencia;
                            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas.Campo_Saldo + " = " + Ope_Psp_Reservas.Campo_Saldo + " - " + Diferencia;
                            Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + Num_Reserva.Trim() + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();

                            Mi_SQL = "UPDATE " + Ope_Psp_Reservas_Detalles.Tabla_Ope_Psp_Reservas_Detalles;
                            Mi_SQL = Mi_SQL + " SET " + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " = " + Ope_Psp_Reservas_Detalles.Campo_Importe_Inicial + " - " + Diferencia;
                            Mi_SQL = Mi_SQL + ", " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " = " + Ope_Psp_Reservas_Detalles.Campo_Saldo + " - " + Diferencia;
                            Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Reservas_Detalles.Campo_No_Reserva + " = '" + Num_Reserva.Trim() + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                        }
                        break;
                    case "COTIZADA-RECHAZADA":

                        Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " SET " +
                                Ope_Tal_Requisiciones.Campo_Estatus + " = 'FILTRADA', " +
                                Ope_Tal_Requisiciones.Campo_Fecha_Cotizada_Rechazada + " = GETDATE(), " +
                                Ope_Tal_Requisiciones.Campo_Empleado_Cotizada_Rechazada_ID + " = '" + Requisicion_Negocio.P_Empleado_ID + "'," +
                                Ope_Tal_Requisiciones.Campo_Alerta + " = 'AMARILLO2'" +
                                " WHERE " + Ope_Tal_Requisiciones.Campo_Folio + " = '" + Requisicion_Negocio.P_Folio + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();


                        //Regresamos los valores de los productos pertenecientes a esta requisicion a nulos solo los cotizados y el proveedor para que sean cotizados nuevamente

                        Mi_SQL = "UPDATE " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion +
                             " SET " + Ope_Tal_Req_Refaccion.Campo_Precio_U_Sin_Imp_Cotizado + " = NULL" +
                             ", " + Ope_Tal_Req_Refaccion.Campo_Precio_U_Con_Imp_Cotizado + " = NULL" +
                             ", " + Ope_Tal_Req_Refaccion.Campo_IVA_Cotizado + " = NULL" +
                             ", " + Ope_Tal_Req_Refaccion.Campo_IEPS_Cotizado + " = NULL" +
                             ", " + Ope_Tal_Req_Refaccion.Campo_Subtota_Cotizado + " = NULL" +
                             ", " + Ope_Tal_Req_Refaccion.Campo_Total_Cotizado + " = NULL" +
                             ", " + Ope_Tal_Req_Refaccion.Campo_Proveedor_ID + " = NULL" +
                             ", " + Ope_Tal_Req_Refaccion.Campo_Nombre_Proveedor + " = NULL"
                             + " WHERE " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = '" + Requisicion_Negocio.P_Requisicion_ID + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        //AHORA MODIFICAMOS LOS DETALLES DE LA REQUISICION, COMO LO ES EL MONTO COTIZADO PARA ESTA REQUISICION
                        Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                            " SET " + Ope_Tal_Requisiciones.Campo_IVA_Cotizado + " = NULL" +
                            ", " + Ope_Tal_Requisiciones.Campo_IEPS_Cotizado + " = NULL" +
                            ", " + Ope_Tal_Requisiciones.Campo_Subtotal_Cotizado + " = NULL" +
                            ", " + Ope_Tal_Requisiciones.Campo_Total_Cotizado + " = NULL" +
                            " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = '" + Requisicion_Negocio.P_Requisicion_ID + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        //Modificamos todas las propuestas de Cotizacion a EN CONSTRUCCION para poder modificar los montos en caso de ser necesario
                        Mi_SQL = "UPDATE " + Ope_Tal_Propuesta_Cotizacion.Tabla_Ope_Tal_Propuesta_Cotizacion;
                        Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Propuesta_Cotizacion.Campo_Estatus + " = 'EN CONSTRUCCION'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Resultado + " = NULL";
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Propuesta_Cotizacion.Campo_No_Requisicion + " = '" + Requisicion_Negocio.P_Requisicion_ID.ToString() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        break;
                }
                if (Requisicion_Negocio.P_Comentario.Trim().Length > 0) { Requisicion_Negocio.Alta_Observaciones(ref Cmd); }
                Cls_Ope_Tal_Requisiciones_Datos.Registrar_Historial(Requisicion_Negocio.P_Estatus, Requisicion_Negocio.P_Requisicion_ID, ref Cmd);
                if (Requisicion_Negocio.P_Estatus.Trim().Equals("ALMACEN")) Cls_Ope_Tal_Requisiciones_Datos.Registrar_Historial("FILTRADA", Requisicion_Negocio.P_Requisicion_ID, ref Cmd);
                if (Requisicion_Negocio.P_Estatus == "COTIZADA-RECHAZADA") { Cls_Ope_Tal_Requisiciones_Datos.Registrar_Historial("FILTRADA", Requisicion_Negocio.P_Requisicion_ID, ref Cmd); }
                Trans.Commit();
            }
            catch (Exception ex)
            {
                Trans.Rollback();
                throw new Exception("No se pudo hacer la actualización de la Requisición. Consultar al Administrador... [" + ex.Message + "]");
            }
            finally
            {
                Cn.Close();
            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Codigo_Programatico_RQ
        ///DESCRIPCIÓN: Consulta el Codigo Programatico de la RQ
        ///PARAMETROS:   1.- Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO:  
        ///FECHA_CREO:  
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public DataTable Consultar_Codigo_Programatico_RQ(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "";

            Mi_SQL = "SELECT REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Partida_ID +
                             ",REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID +
                             ", (SELECT " + Ope_Tal_Requisiciones.Campo_Total +
                             " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                             " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS TOTAL" +
                             ", (SELECT " + Ope_Tal_Req_Refaccion.Campo_Total_Cotizado +
                             " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                             " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS TOTAL_COTIZADO" +
                             ", (SELECT " + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                             " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                             " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS DEPENDENCIA_ID" +
                             ", REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID +
                             ", (SELECT NUM_RESERVA" +
                             " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                             " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                             "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS NUM_RESERVA" +
                             " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " REQ_DET" +
                             " WHERE REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + "='" + Requisicion_Negocio.P_Requisicion_ID + "'";
            DataTable Dt_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Requisicion;

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Liberar_Presupuesto_Cancelada
        ///DESCRIPCIÓN: Metodo que consulta los productos que se encunentran en reorden 
        ///PARAMETROS: 1.- Cls_Ope_Com_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 24/Agosto/2010 
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public void Liberar_Presupuesto_Cancelada(DataTable Dt_Partidas_Productos, String Dependencia_ID, Cls_Ope_Tal_Administrar_Requisiciones_Negocio Clase_Negocio)
        {
            String Partida_ID = "";
            String Proyecto_ID = "";
            String FF = "";
            String Monto_Total = "";
            String Mi_SQL = "";

            //Creamos for para recorrer todos los productos de la requisicion 

            for (int i = 0; i < Dt_Partidas_Productos.Rows.Count; i++)
            {
                //Obtenemos la Partida, Programa y Fuente de dinanciamiento correspondiente al Producto o servicio
                Partida_ID = Dt_Partidas_Productos.Rows[i][Ope_Tal_Req_Refaccion.Campo_Partida_ID].ToString();
                Proyecto_ID = Dt_Partidas_Productos.Rows[i][Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID].ToString();
                FF = Dt_Partidas_Productos.Rows[i][Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID].ToString();
                Monto_Total = Dt_Partidas_Productos.Rows[i][Ope_Tal_Req_Refaccion.Campo_Monto_Total].ToString();

                //liberamos el presupuesto que era para este listado
                Mi_SQL = "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                                " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible +
                                " =" + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " + " + Monto_Total +
                                "," + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido +
                                "=" + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " - " + Monto_Total +
                                " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                                "='" + Partida_ID + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                                "='" + Proyecto_ID + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
                                "='" + FF + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio +
                                " = (SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
                                " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                                " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                                "='" + Partida_ID + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                                "='" + Proyecto_ID + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                                "= TO_CHAR(GETDATE(),'YYYY'))" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                                "= TO_CHAR(GETDATE(),'YYYY')" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
                                "=(SELECT " + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                                " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                                " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "='" + Clase_Negocio.P_Requisicion_ID + "')";
                //Sentencia que ejecuta el query
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            }//fin del For


        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Liberar_Productos
        ///DESCRIPCIÓN: Metodo que libera los productos de la requisicion 
        ///PARAMETROS: 1.- Cls_Ope_Com_Listado_Negocio Datos_Listado objeto de la clase negocio
        ///CREO:
        ///FECHA_CREO: 24/Agosto/2010 
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public void Liberar_Productos(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {

            //seleccionamos todos los productos 
            String Mi_SQL = "SELECT " + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID +
                ", " + Ope_Tal_Req_Refaccion.Campo_Cantidad +
                " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion +
                " WHERE " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + "='" + Requisicion_Negocio.P_Requisicion_ID.Trim() + "'";
            DataTable Dt_Productos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

            for (int i = 0; i < Dt_Productos.Rows.Count; i++)
            {
                Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                        " SET " + Cat_Tal_Refacciones.Campo_Disponible +
                        " =" + Cat_Tal_Refacciones.Campo_Disponible + " + " + Dt_Productos.Rows[i][Ope_Tal_Req_Refaccion.Campo_Cantidad].ToString() +
                        ", " + Cat_Tal_Refacciones.Campo_Comprometido +
                        " =" + Cat_Tal_Refacciones.Campo_Comprometido + " - " + Dt_Productos.Rows[i][Ope_Tal_Req_Refaccion.Campo_Cantidad].ToString() +
                        " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID +
                        "='" + Dt_Productos.Rows[i][Ope_Tal_Req_Refaccion.Campo_Refaccion_ID].ToString() + "'";
                //Sentencia que ejecuta el query
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Procesos_Compra
        ///DESCRIPCIÓN: Metodo que modifica el monto del proceso de compra dependiendo en que proceso se encuentre la requisicion 
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 26/enero/2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        /////SEGUN COMENTA SUSY NO SIRVE PARA NADA EL METODO 
        public void Modificar_Procesos_Compra(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "SELECT * " +
                         " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                         " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                         " = '" + Requisicion_Negocio.P_Requisicion_ID + "'";
            DataTable Dt_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            String Tipo_Compra = Dt_Requisicion.Rows[0][Ope_Tal_Requisiciones.Campo_Tipo_Compra].ToString();
            double Monto_Requisicion = double.Parse(Dt_Requisicion.Rows[0][Ope_Tal_Requisiciones.Campo_Total].ToString());
            double Monto_Cotizado_Requisicion = double.Parse(Dt_Requisicion.Rows[0][Ope_Tal_Requisiciones.Campo_Total_Cotizado].ToString());
            //Monto que se quedara en el proceso, despues de haber restado el monto de la requisicion
            double Monto_Compra_Directa = 0;
            double Monto_Cotizado = 0;
            double Monto_Final = 0;
            String Lista_Requisiciones = "";

            switch (Tipo_Compra)
            {
                case "COMPRA DIRECTA":
                    //Cuando es compra directa no se hace nada. 
                    break;
                case "COTIZACION":
                    //Consultamos el Total de la Compra Directa
                    Mi_SQL = " SELECT " + Ope_Com_Cotizaciones.Campo_Total +
                             ", " + Ope_Com_Cotizaciones.Campo_Total_Cotizado +
                             ", " + Ope_Com_Cotizaciones.Campo_Lista_Requisiciones +
                             " FROM " + Ope_Com_Cotizaciones.Tabla_Ope_Com_Cotizaciones +
                             " WHERE " + Ope_Com_Cotizaciones.Campo_No_Cotizacion +
                             " ='" + Dt_Requisicion.Rows[0][Ope_Tal_Requisiciones.Campo_No_Cotizacion].ToString() + "'";
                    DataTable Dt_Cotizacion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                    Monto_Compra_Directa = double.Parse(Dt_Cotizacion.Rows[0][Ope_Com_Cotizaciones.Campo_Total].ToString());
                    //Es el monto de la requisicion que se restara al proceso, ya que se elimino de este proceso por decicion del jefe de dependencia
                    Monto_Final = Monto_Compra_Directa - Monto_Requisicion;
                    Monto_Cotizado = double.Parse(Dt_Cotizacion.Rows[0][Ope_Com_Cotizaciones.Campo_Total_Cotizado].ToString());
                    Monto_Cotizado = Monto_Cotizado - Monto_Cotizado_Requisicion;
                    //Modificamos el listado de requisiciones ya que esta se eliminara de ese listado 
                    Lista_Requisiciones = Generar_Nueva_Lista_Requisiciones(Dt_Cotizacion.Rows[0][Ope_Com_Cotizaciones.Campo_Lista_Requisiciones].ToString(), Requisicion_Negocio.P_Requisicion_ID.Trim());
                    //Ya obtenido el monto que se quedara actualizamos el monto del proceso de Compra_Directa
                    Mi_SQL = "UPDATE " + Ope_Com_Cotizaciones.Tabla_Ope_Com_Cotizaciones +
                             "SET " + Ope_Com_Cotizaciones.Campo_Total +
                             "='" + Monto_Final.ToString() + "'" +
                             ", " + Ope_Com_Cotizaciones.Campo_Total_Cotizado +
                             "='" + Monto_Cotizado.ToString() + "'" +
                             ", " + Ope_Com_Cotizaciones.Campo_Lista_Requisiciones +
                             "='" + Lista_Requisiciones + "'" +
                             " WHERE " + Ope_Com_Cotizaciones.Campo_No_Cotizacion +
                             "='" + Dt_Requisicion.Rows[0][Ope_Tal_Requisiciones.Campo_No_Cotizacion].ToString() + "'";
                    break;

                case "COMITE DE COMPRAS":
                    //Consultamos el Total de la Compra Directa
                    Mi_SQL = " SELECT " + Ope_Com_Comite_Compras.Campo_Monto_Total +
                             ", " + Ope_Com_Comite_Compras.Campo_Total_Cotizado +
                             ", " + Ope_Com_Comite_Compras.Campo_Lista_Requisiciones +
                             " FROM " + Ope_Com_Comite_Compras.Tabla_Ope_Com_Comite_Compras +
                             " WHERE " + Ope_Com_Comite_Compras.Campo_No_Comite_Compras +
                             " ='" + Dt_Requisicion.Rows[0][Ope_Tal_Requisiciones.Campo_No_Comite_Compras].ToString() + "'";
                    DataTable Dt_Comite_Compras = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                    Monto_Compra_Directa = double.Parse(Dt_Comite_Compras.Rows[0][Ope_Com_Comite_Compras.Campo_Monto_Total].ToString());
                    //Es el monto de la requisicion que se restara al proceso, ya que se elimino de este proceso por decicion del jefe de dependencia
                    Monto_Final = Monto_Compra_Directa - Monto_Requisicion;
                    Monto_Cotizado = double.Parse(Dt_Comite_Compras.Rows[0][Ope_Com_Comite_Compras.Campo_Total_Cotizado].ToString());
                    Monto_Cotizado = Monto_Cotizado - Monto_Cotizado_Requisicion;
                    //Modificamos el listado de requisiciones ya que esta se eliminara de ese listado 
                    Lista_Requisiciones = Generar_Nueva_Lista_Requisiciones(Dt_Comite_Compras.Rows[0][Ope_Com_Comite_Compras.Campo_Lista_Requisiciones].ToString(), Requisicion_Negocio.P_Requisicion_ID.Trim());
                    //Ya obtenido el monto que se quedara actualizamos el monto del proceso de Compra_Directa
                    Mi_SQL = "UPDATE " + Ope_Com_Comite_Compras.Tabla_Ope_Com_Comite_Compras +
                             "SET " + Ope_Com_Comite_Compras.Campo_Monto_Total +
                             "='" + Monto_Final.ToString() + "'" +
                             ", " + Ope_Com_Comite_Compras.Campo_Total_Cotizado +
                             "='" + Monto_Cotizado.ToString() + "'" +
                             ", " + Ope_Com_Comite_Compras.Campo_Lista_Requisiciones +
                             "='" + Lista_Requisiciones + "'" +
                             " WHERE " + Ope_Com_Comite_Compras.Campo_No_Comite_Compras +
                             "='" + Dt_Requisicion.Rows[0][Ope_Tal_Requisiciones.Campo_No_Comite_Compras].ToString() + "'";
                    break;
            }//fin del switch

        }//Fin de Modificar_Proceso Compra

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Generar_Nueva_Lista_Requisiciones
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para consultar requisisciones
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 10/Noviembre/2010 
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public String Generar_Nueva_Lista_Requisiciones(String Lista, String Requisicion_Eliminada)
        {
            String Lista_Generada = "";
            String[] Lista_Aux = Lista.Split(',');
            for (int i = 0; i < Lista_Aux.Length; i++)
            {
                if (Lista_Aux[i] != Requisicion_Eliminada)
                {
                    if (i != Lista_Aux.Length - 1)
                        Lista_Generada = Lista_Generada + Lista_Aux[i] + ",";
                    else
                        Lista_Generada = Lista_Generada + Lista_Aux[i];
                }//Fin del if
            }//fin del for
            return Lista_Generada;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Requisiciones
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para consultar requisisciones
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 10/Noviembre/2010 
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public DataSet Consulta_Requisiciones(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL =
            "SELECT REQUISICION." + Ope_Tal_Requisiciones.Campo_Folio +
            ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Tipo +
            ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Estatus +
            ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Tipo_Articulo +
            ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Fecha_Generacion + " AS FECHA_GENERACION" +
            ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Total +
            ", REQUISICION." + Ope_Tal_Requisiciones.Campo_No_Solicitud +
            ", (select " + Ope_Pat_Vehiculos.Campo_Numero_Economico + " from " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " where " + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + " in (select " + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " from " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " where " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = REQUISICION." + Ope_Tal_Requisiciones.Campo_No_Solicitud + ")) NUMERO_ECONOMICO " +
            " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQUISICION " +
            " WHERE ";
            if (!String.IsNullOrEmpty(Requisicion_Negocio.P_Req_Especiales) && Requisicion_Negocio.P_Req_Especiales == "SI")
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Campo_Especial_Ramo_33 + " = 'SI' ";
            else
                Mi_SQL = Mi_SQL + "(" + Ope_Tal_Requisiciones.Campo_Especial_Ramo_33 + " = 'NO' OR " + Ope_Tal_Requisiciones.Campo_Especial_Ramo_33 + " IS NULL OR " + Ope_Tal_Requisiciones.Campo_Especial_Ramo_33 + " = '')";

            if (!String.IsNullOrEmpty(Requisicion_Negocio.P_Dependencia_ID))
            {
                Mi_SQL += " AND " + Ope_Tal_Requisiciones.Campo_Dependencia_ID + " = '" + Requisicion_Negocio.P_Dependencia_ID + "'";
            }

            if (Requisicion_Negocio.P_Estatus_Busqueda != null)
            {

                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Requisiciones.Campo_Estatus + "=" + "'" + Requisicion_Negocio.P_Estatus_Busqueda + "'";
            }
            else
            {
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Requisiciones.Campo_Estatus + " IN ('GENERADA','COTIZADA')";
            }

            if (Requisicion_Negocio.P_Campo_Busqueda != null)
            {
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Requisiciones.Campo_Folio + " LIKE '%" + Requisicion_Negocio.P_Campo_Busqueda + "%'";
            }

            if (Requisicion_Negocio.P_Fecha_Inicial != null)
            {
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Requisiciones.Campo_Fecha_Generacion + " BETWEEN '" + Requisicion_Negocio.P_Fecha_Inicial + "'" +
                    " AND '" + Requisicion_Negocio.P_Fecha_Final + "'";
            }

            if (Requisicion_Negocio.P_Dependencia_ID != null)
            {
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Requisiciones.Campo_Dependencia_ID + " ='" + Requisicion_Negocio.P_Dependencia_ID + "'";
            }
            Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Requisiciones.Campo_Listado_Almacen + " IS NULL " +
                    " OR " + Ope_Tal_Requisiciones.Campo_Listado_Almacen + "='NO'" +
                    " ORDER BY " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " ASC";

            if (Requisicion_Negocio.P_Folio != null)
            {
                Mi_SQL = "SELECT " +
                         " DEPENDENCIA." + Cat_Dependencias.Campo_Nombre +
                         ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Tipo +
                         ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Folio +
                         ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Fecha_Generacion +
                         ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Estatus +
                         ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Subtotal +
                         ", REQUISICION." + Ope_Tal_Requisiciones.Campo_IEPS +
                         ", REQUISICION." + Ope_Tal_Requisiciones.Campo_IVA +
                         ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Total +
                         ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                         ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Justificacion_Compra +
                         ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Especificacion_Prod_Serv +
                         ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Verificaion_Entrega +
                         ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Tipo_Articulo +
                         ", REQUISICION." + Ope_Tal_Requisiciones.Campo_Total_Cotizado +
                         ", (select " + Ope_Pat_Vehiculos.Campo_Numero_Economico + " from " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " where " + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + " in (select " + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " from " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " where " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = REQUISICION." + Ope_Tal_Requisiciones.Campo_No_Solicitud + ")) NUMERO_ECONOMICO " +
                         ", DEPENDENCIA." + Cat_Dependencias.Campo_Dependencia_ID +
                         " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQUISICION " +
                         " JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIA " +
                         " ON REQUISICION." + Ope_Tal_Requisiciones.Campo_Dependencia_ID + "= DEPENDENCIA." +
                         Cat_Dependencias.Campo_Dependencia_ID +
                         " WHERE " + Ope_Tal_Requisiciones.Campo_Folio + "='" + Requisicion_Negocio.P_Folio + "'";
                if (Requisicion_Negocio.P_Req_Especiales == "SI")
                    Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Requisiciones.Campo_Especial_Ramo_33 + " = 'SI' ";
                else
                    Mi_SQL = Mi_SQL + " AND (" + Ope_Tal_Requisiciones.Campo_Especial_Ramo_33 + " = 'NO' OR " + Ope_Tal_Requisiciones.Campo_Especial_Ramo_33 + " IS NULL OR " + Ope_Tal_Requisiciones.Campo_Especial_Ramo_33 + " = '')";
            }

            DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            return Data_Set;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Productos
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para consultar los productos 
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 10/Noviembre/2010 
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public DataSet Consulta_Productos_Requisicion(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "SELECT " +
                   " PRODUCTOS." + Cat_Tal_Refacciones.Campo_Clave +
                   ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Nombre + " AS PRODUCTO" +
                   ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Descripcion +
                   ", REQUISICION_DET." + Ope_Tal_Req_Refaccion.Campo_Cantidad + " AS CANTIDAD" +
                   ", PRODUCTOS." + Cat_Tal_Refacciones.Campo_Costo_Unitario + " AS PRECIO_UNITARIO" +
                   ", REQUISICION_DET." + Ope_Tal_Req_Refaccion.Campo_Monto_Total + " AS IMPORTE_S_I" +
                   " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion +
                   " REQUISICION_DET" +
                   " JOIN " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                   " PRODUCTOS ON PRODUCTOS." + Cat_Tal_Refacciones.Campo_Refaccion_ID +
                   " = REQUISICION_DET." + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID +
                   " JOIN " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQUISICION ON " +
                   "REQUISICION." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "= REQUISICION_DET." +
                   Ope_Tal_Req_Refaccion.Campo_Requisicion_ID +
                   " WHERE REQUISICION." + Ope_Tal_Requisiciones.Campo_Folio + "='" + Requisicion_Negocio.P_Folio + "'";

            DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            return Data_Set;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Productos_Cotizados
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para consultar los productos que ya fueron consolidadas
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 25/Enero/2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public DataTable Consulta_Productos_Cotizados(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "";
            Mi_SQL = "SELECT PRODUCTO." + Cat_Tal_Refacciones.Campo_Nombre +
                    ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Clave +
                    ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Descripcion +
                    ", DET_REQ." + Ope_Tal_Req_Refaccion.Campo_Cantidad +
                    ", DET_REQ." + Ope_Tal_Req_Refaccion.Campo_Precio_U_Sin_Imp_Cotizado +
                    ", DET_REQ." + Ope_Tal_Req_Refaccion.Campo_Precio_U_Con_Imp_Cotizado +
                    ", DET_REQ." + Ope_Tal_Req_Refaccion.Campo_Subtota_Cotizado +
                    ", DET_REQ." + Ope_Tal_Req_Refaccion.Campo_Total_Cotizado +
                    ", DET_REQ." + Ope_Tal_Req_Refaccion.Campo_Nombre_Proveedor +
                    " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " DET_REQ" +
                    " JOIN " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTO" +
                    " ON PRODUCTO." + Cat_Tal_Refacciones.Campo_Refaccion_ID + " =" +
                    " DET_REQ." + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID +
                    " WHERE DET_REQ." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID +
                    "='" + Requisicion_Negocio.P_Requisicion_ID + "'";
            DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Data_Table;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultamos_Presupuesto_Existente
        ///DESCRIPCIÓN: Metodo que consulta si existe presupuesto para los productos seleccionados
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 25/Enero/2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public Boolean Consultamos_Presupuesto_Existente(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {
            Boolean Existe_Presupuesto = false;
            String Mi_SQL = "";
            //Primero Obtenemos todos lo productos pertenecientes a la requisicion:
            Mi_SQL = "SELECT REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Partida_ID +
            ",REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID +
            ", (SELECT " + Ope_Tal_Requisiciones.Campo_Total +
            " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
            " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
            "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS TOTAL" +
             ", (SELECT " + Ope_Tal_Requisiciones.Campo_Total_Cotizado +
             " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
             " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
             "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS TOTAL_COTIZADO" +
             ", (SELECT " + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
             " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
             " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
             "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS DEPENDENCIA_ID" +
             ", REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID +
             " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " REQ_DET" +
             " WHERE REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + "='" + Requisicion_Negocio.P_Requisicion_ID + "'";
            DataTable Dt_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            //Guardamos en la variable Monto_Restante la diferencia de Total_Cotizado y P_Monto_Total inicial del producto
            //Primero checamos si se va a restar o a aumentar el presupuesto
            double Monto_Cotizado = double.Parse(Dt_Requisicion.Rows[0]["TOTAL_COTIZADO"].ToString().Trim());
            double Monto_Anterior = double.Parse(Dt_Requisicion.Rows[0]["TOTAL"].ToString().Trim());
            double Diferencia = 0;
            double Disponible = 0;
            String Partida_ID = Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Partida_ID].ToString().Trim();
            String Proyecto_ID = Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID].ToString().Trim();
            String Dependencia_ID = Dt_Requisicion.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
            String FF = Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID].ToString().Trim();

            // PASO 1 VERIFICAMOS CUAL DE LOS 2 MONTOS ES MAYOR SI EL COTIZADO O  EL ANTERIOR
            if (Monto_Cotizado > Monto_Anterior)
            {
                //Obtenemos la resta
                Diferencia = Monto_Cotizado - Monto_Anterior;
                Mi_SQL = "";
                //Consultamos el Monto presupuestal para ver si es suficiente 
                Mi_SQL = "SELECT " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible;
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID;
                Mi_SQL = Mi_SQL + "='" + Partida_ID + "'";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID;
                Mi_SQL = Mi_SQL + "='" + Proyecto_ID + "'";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID;
                Mi_SQL = Mi_SQL + "='" + FF + "'";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio;
                Mi_SQL = Mi_SQL + " = (SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID;
                Mi_SQL = Mi_SQL + "='" + Partida_ID + "'";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID;
                Mi_SQL = Mi_SQL + "='" + Proyecto_ID + "'";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "='" + Dependencia_ID + "'";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto;
                Mi_SQL = Mi_SQL + "= TO_CHAR(GETDATE(),'YYYY'))";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto;
                Mi_SQL = Mi_SQL + "= TO_CHAR(GETDATE(),'YYYY')";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "='" + Dependencia_ID + "'";

                //Sentencia que ejecuta el query
                DataTable Dt_Presupuestos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                Disponible = double.Parse(Dt_Presupuestos.Rows[0][Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible].ToString().Trim());
                if (Disponible >= Diferencia)
                    Existe_Presupuesto = true;
                else
                    Existe_Presupuesto = false;
            }
            else
                Existe_Presupuesto = true; //Existe presupuesto ya que el monto cotizado es menor que el k se aparto 


            return Existe_Presupuesto;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Presupuesto_Partidas
        ///DESCRIPCIÓN: Metodo que consulta si existe presupuesto 
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 25/Enero/2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public void Modificar_Presupuesto_Partidas(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "";
            //consultamos los detalles pertenecientes a esta requisicion
            Mi_SQL = "SELECT REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Partida_ID +
                    ",REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID +
                    ", (SELECT " + Ope_Tal_Requisiciones.Campo_Total +
                    " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                    " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                    "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS TOTAL" +
                     ", (SELECT " + Ope_Tal_Requisiciones.Campo_Total_Cotizado +
                     " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                     " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                     "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS TOTAL_COTIZADO" +
                     ", (SELECT " + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                     " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                     " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                     "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS DEPENDENCIA_ID" +
                     ", REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID +
                     " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " REQ_DET" +
                     " WHERE REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + "='" + Requisicion_Negocio.P_Requisicion_ID + "'";
            DataTable Dt_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            //Guardamos en la variable Monto_Restante la diferencia de Total_Cotizado y P_Monto_Total inicial del producto
            //Primero checamos si se va a restar o a aumentar el presupuesto
            double Monto_Cotizado = double.Parse(Dt_Requisicion.Rows[0]["TOTAL_COTIZADO"].ToString());
            double Monto_Anterior = double.Parse(Dt_Requisicion.Rows[0]["TOTAL"].ToString());
            double Diferencia = 0;
            String Partida_ID = Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Partida_ID].ToString().Trim();
            String Proyecto_ID = Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID].ToString().Trim();
            String Dependencia_ID = Dt_Requisicion.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
            String FF = Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID].ToString().Trim();
            bool Suma_Diferencia = false;

            // PASO 1 VERIFICAMOS CUAL DE LOS 2 MONTOS ES MAYOR SI EL COTIZADO O  EL ANTERIOR
            if (Monto_Cotizado > Monto_Anterior)
            {
                //Obtenemos la resta
                Diferencia = Monto_Cotizado - Monto_Anterior;
                Suma_Diferencia = true;
            }
            if (Monto_Cotizado < Monto_Anterior)
            {
                //obtener resta
                Diferencia = Monto_Anterior - Monto_Cotizado;
                Suma_Diferencia = false;
            }
            //modificamos el monto dependiendo de si se resta la dir¿ferencia o se suma
            if (Suma_Diferencia == true)
            {
                //se ACTUALIZAN LOS PRESUPUESTOS DE LA DEPENDENCIA, CORRESPONDIENTE AL PROYECTO Y PARTIDA

                Mi_SQL = "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                Mi_SQL = Mi_SQL + " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible;
                Mi_SQL = Mi_SQL + " =" + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " - " + Diferencia.ToString();
                Mi_SQL = Mi_SQL + "," + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido;
                Mi_SQL = Mi_SQL + "=" + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " + " + Diferencia.ToString();
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID;
                Mi_SQL = Mi_SQL + "='" + Partida_ID + "'";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID;
                Mi_SQL = Mi_SQL + "='" + Proyecto_ID + "'";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID;
                Mi_SQL = Mi_SQL + "='" + FF + "'";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto;
                Mi_SQL = Mi_SQL + "= TO_CHAR(GETDATE(),'YYYY')";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "='" + Dependencia_ID + "'";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio;
                Mi_SQL = Mi_SQL + " = (SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID;
                Mi_SQL = Mi_SQL + "='" + Partida_ID + "'";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID;
                Mi_SQL = Mi_SQL + "='" + Proyecto_ID + "'";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + "='" + Dependencia_ID + "'";
                Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto;
                Mi_SQL = Mi_SQL + "= TO_CHAR(GETDATE(),'YYYY'))";

                //Sentencia que ejecuta el query
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }//fin if SumaDiferencia
            else
            {
                //Modificamos el presupuesto, ya que se resta el monto que sobro pues el valor cotizado es menor k el anterior
                //se ACTUALIZAN LOS PRESUPUESTOS DE LA DEPENDENCIA, CORRESPONDIENTE AL PROYECTO Y PARTIDA

                Mi_SQL = "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                    " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible +
                    " =" + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " + " + Diferencia.ToString() +
                    "," + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido +
                    "=" + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " - " + Diferencia.ToString() +
                    " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                    "='" + Partida_ID + "'" +
                    " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                    "='" + Proyecto_ID + "'" +
                    " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
                    "='" + FF + "'" +
                    " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio +
                    " = (SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
                    " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                    " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                    "='" + Partida_ID + "'" +
                    " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                    "='" + Proyecto_ID + "'" +
                    " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                    "= TO_CHAR(GETDATE(),'YYYY'))" +
                    " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                    "= TO_CHAR(GETDATE(),'YYYY')" +
                    " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
                    "='" + Dependencia_ID + "'";
                //Sentencia que ejecuta el query
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Requisicion_Consolidada
        ///DESCRIPCIÓN: Metodo que permite consultar si la requisicion esta consolidad y regresa un booleano
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 25/Enero/2011
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public bool Consultar_Requisicion_Consolidada(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = " SELECT " + Ope_Tal_Requisiciones.Campo_No_Consolidacion +
                            " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                            " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                            " ='" + Requisicion_Negocio.P_Requisicion_ID + "'";
            DataTable Dt_Consolidacion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            bool Consolidada = false;
            if (Dt_Consolidacion.Rows.Count != 0)
            {
                if (Dt_Consolidacion.Rows[0][0] != null)
                    Consolidada = true;
                else
                    Consolidada = false;
            }
            return Consolidada;
        }

        #region Observaciones

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consecutivo
        ///DESCRIPCIÓN: Metodo que verfifica el consecutivo en la tabla y ayuda a generar el nuevo Id. 
        ///PARAMETROS: 
        ///CREO:
        ///FECHA_CREO: 24/Agosto/2010 
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public String Consecutivo()
        {
            String Consecutivo = "";
            String Mi_SQL;         //Obtiene la cadena de inserción hacía la base de datos
            Object Asunto_ID; //Obtiene el ID con la cual se guardo los datos en la base de datos

            Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Tal_Req_Observaciones.Campo_Observacion_ID + "),'0') ";
            Mi_SQL = Mi_SQL + "FROM " + Ope_Tal_Req_Observaciones.Tabla_Ope_Tal_Req_Observaciones;
            Asunto_ID = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

            if (Convert.IsDBNull(Asunto_ID))
            {
                Consecutivo = "1";
            }
            else
            {
                Consecutivo = string.Format("{0:0}", Convert.ToInt32(Asunto_ID) + 1);
            }
            return Consecutivo;
        }//fin de consecutivo

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Productos
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para consultar Observaciones 
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 10/Noviembre/2010 
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public DataSet Consulta_Observaciones(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "SELECT " + Ope_Tal_Req_Observaciones.Campo_Observacion_ID +
                            ", " + Ope_Tal_Req_Observaciones.Campo_Comentario +
                            ", " + Ope_Tal_Req_Observaciones.Campo_Estatus +
                            "," + Ope_Tal_Req_Observaciones.Campo_Fecha_Creo +
                            ", " + Ope_Tal_Req_Observaciones.Campo_Usuario_Creo +
                            " FROM " + Ope_Tal_Req_Observaciones.Tabla_Ope_Tal_Req_Observaciones +
                            " WHERE " + Ope_Tal_Req_Observaciones.Campo_Requisicion_ID + " = '" + ((!String.IsNullOrEmpty(Requisicion_Negocio.P_Requisicion_ID)) ? Requisicion_Negocio.P_Requisicion_ID : "0") + "'" +
                            " ORDER BY " + Ope_Tal_Req_Observaciones.Campo_Observacion_ID + " ASC";
            DataSet Data_Set = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            return Data_Set;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Observaciones
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para dar de alta observaciones
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 10/Noviembre/2010 
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public void Alta_Observaciones(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio, ref SqlCommand P_Cmd)
        {
            //String ID = Consecutivo();
            String Mi_SQL = "INSERT INTO " + Ope_Tal_Req_Observaciones.Tabla_Ope_Tal_Req_Observaciones +
            " (" +
            " " + Ope_Tal_Req_Observaciones.Campo_Requisicion_ID +
            ", " + Ope_Tal_Req_Observaciones.Campo_Comentario +
            ", " + Ope_Tal_Req_Observaciones.Campo_Estatus +
            ", " + Ope_Tal_Req_Observaciones.Campo_Usuario_Creo +
            ", " + Ope_Tal_Req_Observaciones.Campo_Fecha_Creo +
            ") VALUES (" +
             "'" +
            Requisicion_Negocio.P_Requisicion_ID + "','" +
            Requisicion_Negocio.P_Comentario + "','" +
            Requisicion_Negocio.P_Estatus + "','" +
            Cls_Sessiones.Nombre_Empleado + "', GETDATE())";
            P_Cmd.CommandText = Mi_SQL;
            P_Cmd.ExecuteNonQuery();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Observaciones
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para dar de alta observaciones
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 10/Noviembre/2010 
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public void Alta_Observaciones(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {
            //String ID = Consecutivo();
            String Mi_SQL = "INSERT INTO " + Ope_Tal_Req_Observaciones.Tabla_Ope_Tal_Req_Observaciones +
            " (" +
            " " + Ope_Tal_Req_Observaciones.Campo_Requisicion_ID +
            ", " + Ope_Tal_Req_Observaciones.Campo_Comentario +
            ", " + Ope_Tal_Req_Observaciones.Campo_Estatus +
            ", " + Ope_Tal_Req_Observaciones.Campo_Usuario_Creo +
            ", " + Ope_Tal_Req_Observaciones.Campo_Fecha_Creo +
            ") VALUES (" +
             "'" +
            Requisicion_Negocio.P_Requisicion_ID + "','" +
            Requisicion_Negocio.P_Comentario + "','" +
            Requisicion_Negocio.P_Estatus + "','" +
            Cls_Sessiones.Nombre_Empleado + "', GETDATE())";
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Areas
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para consultar las Areas
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 10/Noviembre/2010 
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public DataTable Consultar_Areas(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "SELECT " + Cat_Areas.Campo_Area_ID +
                            ", " + Cat_Areas.Campo_Nombre +
                            " FROM " + Cat_Areas.Tabla_Cat_Areas +
                            " WHERE " + Cat_Areas.Campo_Dependencia_ID + " ='" +
                            Cls_Sessiones.Dependencia_ID_Empleado + "'" +
                            " ORDER BY " + Cat_Areas.Campo_Nombre;
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Dependencias
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para consultar las Areas
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 10/Noviembre/2010 
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public DataTable Consultar_Dependencias(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID +
                            ", " + Cat_Dependencias.Campo_Clave + "+' '+" + Cat_Dependencias.Campo_Nombre +
                            " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias +
                            " ORDER BY " + Cat_Dependencias.Campo_Nombre;
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Dependencias
        ///DESCRIPCIÓN: Metodo que ejecuta la sentencia SQL para consultar las Areas
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Gustavo AC
        ///FECHA_CREO: 13/Marzo/2012
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: Junio 2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Modulo de Taller Mecanico Municipal
        ///*******************************************************************************
        public DataTable Consulta_Dependencias_Con_Programas_Especiales_yRamo33(Cls_Ope_Tal_Administrar_Requisiciones_Negocio Requisicion_Negocio)
        {
            String Mi_SQL =
            "SELECT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID +
            ", " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + "+' '+" +
            Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE " +
            " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " JOIN " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
            " ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + " = " +
            Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
            " WHERE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID + " IN (" +
                " SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento +
                " WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'SI')" +
            " AND " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto + " = " +
            DateTime.Now.Year +
            " GROUP BY (" +
            Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + "," +
            Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + "," +
            Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ") " +
            " ORDER BY " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave;
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        #endregion

        #endregion

    }
}
