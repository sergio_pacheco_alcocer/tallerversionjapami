﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Reporte_Solicitudes_Servicio.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Rpt_Tal_Solicitudes_Servicios_Datos
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Reporte_Solicitudes_Servicio.Datos {
    public class Cls_Rpt_Tal_Solicitudes_Servicios_Datos
    {
        #region Metodos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Servicios_Preventivos      
            ///DESCRIPCIÓN          : Saca un listado de las Solicitudes Cargadas con 
            ///                       Servicio Preventivo
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a
            ///                         Consultar en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 24/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consulta_Servicios_Preventivos(Cls_Rpt_Tal_Solicitudes_Servicios_Negocio Parametros) { 
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try {
                    Mi_SQL = "SELECT SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", ISNULL(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + ", 'NO AUTORIZADA') AS FOLIO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", CASE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                    Mi_SQL = Mi_SQL + " WHEN 'SERVICIO_CORRECTIVO' THEN 'SERVICIO CORRECTIVO' WHEN 'SERVICIO_PREVENTIVO' THEN 'SERVICIO PREVENTIVO' WHEN 'REVISTA_MECANICA' THEN 'REVISTA MECANICA' END AS TIPO_SOLICIUD";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                    Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", CAST(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS CHAR) AS NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", CAST(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS CHAR) AS NO_ECONOMICO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Diagnostico + " AS DIAGNOSTICO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Reparacion + " AS TIPO_REPARACION";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " AS ESTATUS";

                    Mi_SQL = Mi_SQL + ", CASE SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Reparacion + " ";
                    Mi_SQL = Mi_SQL + " WHEN  'INTERNA'";
                    Mi_SQL = Mi_SQL + "THEN LTRIM(RTRIM(EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + "";
                    Mi_SQL = Mi_SQL + " +' - '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Nombre + "))";
                    Mi_SQL = Mi_SQL + "WHEN 'EXTERNA'";
                    Mi_SQL = Mi_SQL + "THEN PROVEEDORES." + Cat_Com_Proveedores.Campo_Nombre + " END AS MECANICO";
                    
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos + " SERVICIOS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " MECANICOS";
                    Mi_SQL = Mi_SQL + " ON SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Mecanico_ID + " = MECANICOS." + Cat_Tal_Mecanicos.Campo_Mecanico_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS";
                    Mi_SQL = Mi_SQL + " ON MECANICOS." + Cat_Tal_Mecanicos.Campo_Empleado_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + "";
                                        
                    Mi_SQL = Mi_SQL + " JOIN " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " ASIGNACION";
                    Mi_SQL = Mi_SQL + " ON ( ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + "";
                    Mi_SQL = Mi_SQL + " AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " IN('SERVICIO_PREVENTIVO') ";
                    Mi_SQL = Mi_SQL + " AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN('ACEPTADO','REPARADO') )";

                    Mi_SQL = Mi_SQL + " JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDORES";
                    Mi_SQL = Mi_SQL + " ON PROVEEDORES." + Cat_Com_Proveedores.Campo_Proveedor_ID + " = ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + "";


                    Mi_SQL = Mi_SQL + " WHERE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " = 'SERVICIO_PREVENTIVO'"; Entro_Where = true;
                    if (!String.IsNullOrEmpty(Parametros.P_Vehiculo_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " IN ('" + Parametros.P_Vehiculo_ID + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Tipo_Reparacion))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Reparacion + " = '" + Parametros.P_Tipo_Reparacion + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Elab_Ini).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " >= '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Elab_Ini) + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Elab_Fin).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Elab_Fin.AddDays(1)) + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Proveedor_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Reparacion + " = 'EXTERNA'";
                        Mi_SQL = Mi_SQL + " AND SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " IN (";
                        Mi_SQL = Mi_SQL + " SELECT ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " ASIG_PROV ";
                        Mi_SQL = Mi_SQL + " WHERE ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = 'SERVICIO_PREVENTIVO'";
                        Mi_SQL = Mi_SQL + " AND ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN ('ACEPTADO', 'GENERADO' , 'REPARADO')";
                        Mi_SQL = Mi_SQL + " AND ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + " IN ('" + Parametros.P_Proveedor_ID + "')";
                        Mi_SQL = Mi_SQL + " )";
                    }
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Servicios_Correctivos      
            ///DESCRIPCIÓN          : Saca un listado de las Solicitudes Cargadas con 
            ///                       Servicio Correctivo
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a
            ///                         Consultar en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 24/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consulta_Servicios_Correctivos(Cls_Rpt_Tal_Solicitudes_Servicios_Negocio Parametros) { 
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try {
                    Mi_SQL = "SELECT SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", ISNULL(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + ", 'NO AUTORIZADA') AS FOLIO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", CASE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                    Mi_SQL = Mi_SQL + " WHEN 'SERVICIO_CORRECTIVO' THEN 'SERVICIO CORRECTIVO' WHEN 'SERVICIO_PREVENTIVO' THEN 'SERVICIO PREVENTIVO' WHEN 'REVISTA_MECANICA' THEN 'REVISTA MECANICA' END AS TIPO_SOLICIUD";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                    Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", CAST(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS CHAR) AS NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", CAST(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS CHAR) AS NO_ECONOMICO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Diagnostico + " AS DIAGNOSTICO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Reparacion + " AS TIPO_REPARACION";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " AS ESTATUS";

                    Mi_SQL = Mi_SQL + ", CASE SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Reparacion + " WHEN ";
                    Mi_SQL = Mi_SQL + " 'INTERNA' THEN";
                    Mi_SQL = Mi_SQL + " RTRIM(LTRIM(EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + "";
                    Mi_SQL = Mi_SQL + " +' - '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Nombre + "))";
                    Mi_SQL = Mi_SQL + " WHEN 'EXTERNA'";
                    Mi_SQL = Mi_SQL + "THEN PROVEEDORES." + Cat_Com_Proveedores.Campo_Nombre + " END AS MECANICO";
                    
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos + " SERVICIOS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " MECANICOS";
                    Mi_SQL = Mi_SQL + " ON SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Mecanico_ID + " = MECANICOS." + Cat_Tal_Mecanicos.Campo_Mecanico_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS";
                    Mi_SQL = Mi_SQL + " ON MECANICOS." + Cat_Tal_Mecanicos.Campo_Empleado_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + "";

                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " ASIGNACION";
                    Mi_SQL = Mi_SQL + " ON ( ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + "";
                    Mi_SQL = Mi_SQL + " AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " IN('SERVICIO_CORRECTIVO') ";
                    Mi_SQL = Mi_SQL + " AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN('ACEPTADO','REPARADO') )";

                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " PROVEEDORES";
                    Mi_SQL = Mi_SQL + " ON PROVEEDORES." + Cat_Com_Proveedores.Campo_Proveedor_ID + " = ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + "";

                    Mi_SQL = Mi_SQL + " WHERE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " = 'SERVICIO_CORRECTIVO'"; Entro_Where = true;
                    if (!String.IsNullOrEmpty(Parametros.P_Vehiculo_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " IN ('" + Parametros.P_Vehiculo_ID + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Tipo_Reparacion))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Reparacion + " = '" + Parametros.P_Tipo_Reparacion + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Elab_Ini).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " >= '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Elab_Ini) + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Elab_Fin).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Elab_Fin.AddDays(1)) + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Proveedor_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Reparacion + " = 'EXTERNA'";
                        Mi_SQL = Mi_SQL + " AND SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " IN (";
                        Mi_SQL = Mi_SQL + " SELECT ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " ASIG_PROV ";
                        Mi_SQL = Mi_SQL + " WHERE ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = 'SERVICIO_CORRECTIVO'";
                        Mi_SQL = Mi_SQL + " AND ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN ('ACEPTADO', 'GENERADO' , 'REPARADO')";
                        Mi_SQL = Mi_SQL + " AND ASIG_PROV." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + " IN ('" + Parametros.P_Proveedor_ID + "')";
                        Mi_SQL = Mi_SQL + " )";
                    }
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Servicios_Revista_Mecanica      
            ///DESCRIPCIÓN          : Saca un listado de las Solicitudes Cargadas con 
            ///                       Servicio de Revista Mecanica
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a
            ///                         Consultar en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 24/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consulta_Servicios_Revista_Mecanica(Cls_Rpt_Tal_Solicitudes_Servicios_Negocio Parametros) { 
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try {
                    Mi_SQL = "SELECT SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", ISNULL(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + ", 'NO AUTORIZADA') AS FOLIO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", CASE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + "";
                    Mi_SQL = Mi_SQL + " WHEN 'SERVICIO_CORRECTIVO' THEN 'SERVICIO CORRECTIVO' WHEN 'SERVICIO_PREVENTIVO' THEN 'SERVICIO PREVENTIVO' WHEN 'REVISTA_MECANICA' THEN 'REVISTA MECANICA' END AS TIPO_SOLICIUD";
                    Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                    Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", CAST(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS CHAR) AS NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", CAST(VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS CHAR) AS NO_ECONOMICO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Descripcion_Servicio + " AS DESCRIPCION_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Rev_Mecanica.Campo_Diagnostico + " AS DIAGNOSTICO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " AS ESTATUS";
                    Mi_SQL = Mi_SQL + ", LTRIM(RTRIM(EMPLEADOS." + Cat_Empleados.Campo_No_Empleado + "";
                    Mi_SQL = Mi_SQL + " +' - '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + "";
                    Mi_SQL = Mi_SQL + " +' '+ EMPLEADOS." + Cat_Empleados.Campo_Nombre + ")) AS MECANICO";
                    
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Rev_Mecanica.Tabla_Ope_Tal_Rev_Mecanica + " SERVICIOS";
                    Mi_SQL = Mi_SQL + " ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = SERVICIOS." + Ope_Tal_Rev_Mecanica.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " MECANICOS";
                    Mi_SQL = Mi_SQL + " ON SERVICIOS." + Ope_Tal_Rev_Mecanica.Campo_Mecanico_ID + " = MECANICOS." + Cat_Tal_Mecanicos.Campo_Mecanico_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS";
                    Mi_SQL = Mi_SQL + " ON MECANICOS." + Cat_Tal_Mecanicos.Campo_Empleado_ID + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + "";
                    Mi_SQL = Mi_SQL + " WHERE SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + " = 'REVISTA_MECANICA'"; Entro_Where = true;
                    if (!String.IsNullOrEmpty(Parametros.P_Vehiculo_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " IN ('" + Parametros.P_Vehiculo_ID + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Dependencia_ID)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = '" + Parametros.P_Dependencia_ID + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Elab_Ini).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " >= '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Elab_Ini) + "'";
                    }
                    if (!(String.Format("{0:ddMMyyyy}", Parametros.P_F_Elab_Fin).Trim().Equals((String.Format("{0:ddMMyyyy}", new DateTime()).Trim())))) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + " SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " < '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_F_Elab_Fin.AddDays(1)) + "'";
                    }
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

        #endregion

    }
}
