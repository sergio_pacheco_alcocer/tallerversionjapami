﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Administrar_Listado.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
/// <summary>
/// Summary description for Cls_Ope_Alm_Com_Administrar_Listado_Datos
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Administrar_Listado.Datos {
    public class Cls_Ope_Tal_Administrar_Listado_Datos {

        #region Metodos
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Listado_Almacen
        ///DESCRIPCIÓN: Consulta_Listado_Almacen
        ///PARAMETROS: Listado_Negocio
        ///CREO:  
        ///FECHA_CREO: 2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public DataTable Consulta_Listado_Almacen(Cls_Ope_Tal_Administrar_Listado_Negocio Listado_Negocio)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT LISTADO." + Ope_Tal_Listado.Campo_Folio +
                                ", REPLACE(CONVERT(VARCHAR(11),LISTADO." + Ope_Tal_Listado.Campo_Fecha_Creo + ",106),' ','/') AS FECHA_CREO" +
                                ", LISTADO." + Ope_Tal_Listado.Campo_Tipo +
                                ", LISTADO." + Ope_Tal_Listado.Campo_Estatus +
                                ", LISTADO." + Ope_Tal_Listado.Campo_Total +
                                " FROM " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado + " LISTADO";
                if (Listado_Negocio.P_Estatus_Busqueda == null)
                {
                    Mi_SQL = Mi_SQL + " WHERE LISTADO." + Ope_Tal_Listado.Campo_Estatus + " IN ('GENERADA')";
                }
                else { Mi_SQL = Mi_SQL + " WHERE LISTADO." + Ope_Tal_Listado.Campo_Estatus + " = '" + Listado_Negocio.P_Estatus_Busqueda + "'"; }

                if (Listado_Negocio.P_Folio != null)
                {
                    Mi_SQL = "SELECT LISTADO." + Ope_Tal_Listado.Campo_Folio +
                             ", (SELECT " + Cat_Com_Proyectos_Programas.Campo_Nombre +
                             " FROM " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas +
                             " WHERE " + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID + " = " +
                             " LISTADO." + Ope_Tal_Listado.Campo_No_Proyecto_ID + " )AS PROYECTO" +
                             ", ( SELECT " + Cat_Com_Partidas.Campo_Nombre +
                             " FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +
                             " WHERE " + Cat_Com_Partidas.Campo_Partida_ID + " = " +
                             " LISTADO." + Ope_Tal_Listado.Campo_No_Partida_ID + ") AS PARTIDA " +
                             ", REPLACE(CONVERT(VARCHAR(11),LISTADO." + Ope_Tal_Listado.Campo_Fecha_Creo + ",106),' ','/') AS FECHA_CREO" +
                             ", LISTADO." + Ope_Tal_Listado.Campo_Estatus +
                             ", LISTADO." + Ope_Tal_Listado.Campo_Tipo +
                             ", LISTADO." + Ope_Tal_Listado.Campo_Total +
                             ", LISTADO." + Ope_Tal_Listado.Campo_Listado_ID +
                             " FROM " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado + " LISTADO " +
                             " WHERE " + Ope_Tal_Listado.Campo_Folio + " like '%" + Listado_Negocio.P_Folio + "%'";
                }
                if (Listado_Negocio.P_Fecha_Inicial != null)
                {
                    Mi_SQL = Mi_SQL + " AND LISTADO." + Ope_Tal_Listado.Campo_Fecha_Creo + " BETWEEN '" + Listado_Negocio.P_Fecha_Inicial + "'" + " AND '" + Listado_Negocio.P_Fecha_Final + "'";
                }

                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consulta_Listado_Detalle
            ///DESCRIPCIÓN: Consulta_Listado_Detalle
            ///PARAMETROS: Listado_Negocio
            ///CREO:  
            ///FECHA_CREO: 2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            public DataTable Consulta_Listado_Detalle(Cls_Ope_Tal_Administrar_Listado_Negocio Listado_Negocio) {
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try { 
                    Mi_SQL = "SELECT LISTADO." + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID + " AS PRODUCTO_ID" +
                             ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Clave +
                             ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Nombre + " AS PRODUCTO_NOMBRE" +
                             ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Descripcion + 
                             ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Existencia +
                             ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Reorden +
                             ", (SELECT " + Cat_Com_Unidades.Campo_Nombre +
                             " FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + 
                             " WHERE " + Cat_Com_Unidades.Campo_Unidad_ID +
                             "=PRODUCTO." + Cat_Tal_Refacciones.Campo_Unidad_ID + ") AS UNIDAD" + 
                             ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Cantidad +
                             ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Costo_Compra + " AS PRECIO_UNITARIO" +
                             ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Importe +
                             ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Monto_IVA +
                             ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Monto_IEPS +
                             ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IVA +
                             ", LISTADO." + Ope_Tal_Listado_Detalle.Campo_Porcentaje_IEPS +
                             " FROM " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle + " LISTADO" +
                             " JOIN " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTO" +
                             " ON PRODUCTO." + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = LISTADO." + Ope_Tal_Listado_Detalle.Campo_No_Producto_ID +
                             " WHERE LISTADO." + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID + " = '" + Listado_Negocio.P_Listado_ID + "'" +
                             " ORDER BY PRODUCTO." + Cat_Tal_Refacciones.Campo_Nombre;

                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Observaciones_Listado
            ///DESCRIPCIÓN: Consultar_Observaciones_Listado
            ///PARAMETROS: Listado_Negocio
            ///CREO:  
            ///FECHA_CREO: 2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            public DataTable Consultar_Observaciones_Listado(Cls_Ope_Tal_Administrar_Listado_Negocio Listado_Negocio) {
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try {
                    Mi_SQL = "SELECT " + Ope_Tal_Obs_Listado.Campo_Comentario +
                                ", " + Ope_Tal_Obs_Listado.Campo_Estatus +
                                ", REPLACE(CONVERT(VARCHAR(11)," + Ope_Tal_Obs_Listado.Campo_Fecha_Creo + ",106),' ','/') AS FECHA_CREO" +
                                ", " + Ope_Tal_Obs_Listado.Campo_Usuario_Creo +
                                " FROM " + Ope_Tal_Obs_Listado.Tabla_Ope_Tal_Obs_Listados +
                                " WHERE " + Ope_Tal_Obs_Listado.Campo_No_Listado_ID +
                                " = '" + Listado_Negocio.P_Listado_ID + "'";

                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0){
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null){
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Modificar_Listado
            ///DESCRIPCIÓN: Modificar_Listado
            ///PARAMETROS: Listado_Negocio
            ///CREO:  
            ///FECHA_CREO: 2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            public String Modificar_Listado(Cls_Ope_Tal_Administrar_Listado_Negocio Listado_Negocio) {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try {
                    String Mi_SQL = "SELECT " + Ope_Tal_Listado.Campo_Listado_ID +
                         ", " + Ope_Tal_Listado.Campo_Folio +
                         ", " + Ope_Tal_Listado.Campo_No_Proyecto_ID +
                         ", " + Ope_Tal_Listado.Campo_No_Partida_ID +
                         ", " + Ope_Tal_Listado.Campo_Tipo +
                         ", " + Ope_Tal_Listado.Campo_Estatus +
                         ", " + Ope_Tal_Listado.Campo_Total +
                         " FROM " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
                         " WHERE " + Ope_Tal_Listado.Campo_Folio + " = '" + Listado_Negocio.P_Folio + "'";
                    DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                    //Guardamos el id del listado 
                    Listado_Negocio.P_Listado_ID = Data_Table.Rows[0][Ope_Tal_Listado.Campo_Listado_ID].ToString();
                    Listado_Negocio.P_Partida_ID = Data_Table.Rows[0][Ope_Tal_Listado.Campo_No_Partida_ID].ToString();

                    //Guardamos los valores de total para restarcelo al presupuesto 
                    Double Presupuesto_Anterior = double.Parse(Data_Table.Rows[0][Ope_Tal_Listado.Campo_Total].ToString());

                    switch (Listado_Negocio.P_Estatus) {
                        case "AUTORIZADA":
                            Mi_SQL = "UPDATE " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
                                    " SET " + Ope_Tal_Listado.Campo_Estatus + " = '" + Listado_Negocio.P_Estatus + "'"+ 
                                    ", " + Ope_Tal_Listado.Campo_Empleado_Autorizacion_ID + " = '" + Listado_Negocio.P_Usuario_ID + "'" +
                                     ", " + Ope_Tal_Listado.Campo_Fecha_Autorizacion + " = GETDATE() ";
                            break;
                        case "RECHAZADA":
                            Mi_SQL = "UPDATE " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
                                     " SET " + Ope_Tal_Listado.Campo_Estatus +
                                     " = 'EN CONSTRUCCION'";

                            break;
                        case "CANCELADA":
                            Mi_SQL = "UPDATE " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
                                     " SET " + Ope_Tal_Listado.Campo_Estatus +
                                     " = 'CANCELADA'";
                            break;
                    }
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Listado.Campo_Listado_ID + " = '" + Listado_Negocio.P_Listado_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Trans.Commit();
                    Alta_Observaciones_Listado(Listado_Negocio);
                    String No_Requisicion_Trancitoria = "";
                    if (Listado_Negocio.P_Estatus == "CANCELADA") {
                        Liberar_Presupuesto_Cancelada(Listado_Negocio);
                    }
                } catch (SqlException Ex) {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152) {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                    } else if (Ex.Number == 2627) {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    } else if (Ex.Number == 547) {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 515) {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar Modificar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }
                return "Exito";
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Alta_Observaciones_Listado
            ///DESCRIPCIÓN: Alta_Observaciones_Listado
            ///PARAMETROS: Listado_Negocio
            ///CREO:  
            ///FECHA_CREO: 2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            public void Alta_Observaciones_Listado(Cls_Ope_Tal_Administrar_Listado_Negocio Listado_Negocio) {
                String Id_Obs_Listado = Obtener_Consecutivo(Ope_Tal_Obs_Listado.Campo_Obs_listado_ID, Ope_Tal_Obs_Listado.Tabla_Ope_Tal_Obs_Listados).ToString();
                String Mi_SQL = "INSERT INTO " + Ope_Tal_Obs_Listado.Tabla_Ope_Tal_Obs_Listados +
                                " (" + Ope_Tal_Obs_Listado.Campo_Obs_listado_ID +
                                ", " + Ope_Tal_Obs_Listado.Campo_No_Listado_ID +
                                ", " + Ope_Tal_Obs_Listado.Campo_Comentario +
                                ", " + Ope_Tal_Obs_Listado.Campo_Estatus +
                                ", " + Ope_Tal_Obs_Listado.Campo_Usuario_Creo +
                                ", " + Ope_Tal_Obs_Listado.Campo_Fecha_Creo +
                                ") VALUES ('"+ Id_Obs_Listado+"','" +
                                Listado_Negocio.P_Listado_ID + "','" +
                                Listado_Negocio.P_Comentario + "','" +
                                Listado_Negocio.P_Estatus + "','" +
                                Listado_Negocio.P_Usuario + "',GETDATE())";
                
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Convertir_Requisicion_Transitoria
            ///DESCRIPCIÓN: Convertir_Requisicion_Transitoria
            ///PARAMETROS: Listado_Negocio
            ///CREO:
            ///FECHA_CREO: 24/Agosto/2010 
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            public String Convertir_Requisicion_Transitoria(Cls_Ope_Tal_Administrar_Listado_Negocio Listado_Negocio) {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                String Requisicion_ID = "";
                try {

                    Requisicion_ID = Obtener_Consecutivo(Ope_Tal_Requisiciones.Campo_Requisicion_ID, Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones).ToString();

                    String Mi_SQL = "SELECT " + Cat_Tal_Parametros.Campo_Dependencia_ID +
                        ", " + Cat_Tal_Parametros.Campo_Partida_ID +
                        ", " + Cat_Tal_Parametros.Campo_Programa_ID +
                        ", " + Cat_Tal_Parametros.Campo_Fuente_Financiamiento +
                        " FROM " + Cat_Tal_Parametros.Tabla_Cat_Tal_Parametros;
                    DataTable Dt_Parametros = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                    String Partida_Esp_Almacen_Global = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Partida_ID].ToString();
                    String Programa_ID_Almacen = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Partida_ID].ToString();
                    String Dependencia_ID_Almacen = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Dependencia_ID].ToString();
                    String Fuente_Financiamiento = Dt_Parametros.Rows[0][Cat_Tal_Parametros.Campo_Fuente_Financiamiento].ToString();

                    Mi_SQL = "INSERT INTO " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                        " (" + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                        ", " + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                        ", " + Ope_Tal_Requisiciones.Campo_Area_ID +
                        ", " + Ope_Tal_Requisiciones.Campo_Folio +
                        ", " + Ope_Tal_Requisiciones.Campo_Estatus +
                        ", " + Ope_Tal_Requisiciones.Campo_Codigo_Programatico +
                        ", " + Ope_Tal_Requisiciones.Campo_Tipo +
                        ", " + Ope_Tal_Requisiciones.Campo_Fase +
                        ", " + Ope_Tal_Requisiciones.Campo_Total +
                        ", " + Ope_Tal_Requisiciones.Campo_Usuario_Creo +
                        ", " + Ope_Tal_Requisiciones.Campo_Fecha_Creo +
                        ", " + Ope_Tal_Requisiciones.Campo_Empleado_Filtrado_ID +
                        ", " + Ope_Tal_Requisiciones.Campo_Fecha_Filtrado +
                        ", " + Ope_Tal_Requisiciones.Campo_Tipo_Articulo +
                        ", " + Ope_Tal_Requisiciones.Campo_Empleado_Construccion_ID +
                        ", " + Ope_Tal_Requisiciones.Campo_Fecha_Construccion +
                        ", " + Ope_Tal_Requisiciones.Campo_Empleado_Generacion_ID +
                        ", " + Ope_Tal_Requisiciones.Campo_Fecha_Generacion +
                        ", " + Ope_Tal_Requisiciones.Campo_Empleado_Autorizacion_ID +
                        ", " + Ope_Tal_Requisiciones.Campo_Fecha_Autorizacion +
                        ", " + Ope_Tal_Requisiciones.Campo_Listado_Almacen +
                        ") VALUES ('" + Requisicion_ID + "','" +
                        Dependencia_ID_Almacen + "','" +
                        Cls_Sessiones.Area_ID_Empleado + "','" +
                        "RQ-" + Requisicion_ID + "','" +
                        "FILTRADA','" +
                        "CODIGO PROGRAMATICO" + "','" +
                        "TRANSITORIA','" +
                        "REQUISICION','" +
                        Listado_Negocio.P_Total + "','" +
                        Cls_Sessiones.Nombre_Empleado + "',GETDATE()," +
                        "'" + Cls_Sessiones.Empleado_ID + "',GETDATE(),'PRODUCTO'," +
                        "'" + Cls_Sessiones.Empleado_ID + "',GETDATE()," +
                        "'" + Cls_Sessiones.Empleado_ID + "',GETDATE()," +
                        "'" + Cls_Sessiones.Empleado_ID + "',GETDATE(),'SI')";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Mi_SQL = "UPDATE " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
                            " SET " + Ope_Tal_Listado.Campo_No_Requisicion_ID + " = '" + Requisicion_ID + "'" +
                            " WHERE " + Ope_Tal_Listado.Campo_Folio + " = '" + Listado_Negocio.P_Folio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Mi_SQL = "SELECT * FROM " + Ope_Tal_Listado_Detalle.Tabla_Ope_Tal_Listado_Detalle +
                             " WHERE " + Ope_Tal_Listado_Detalle.Campo_No_Listado_ID + " = '" + Listado_Negocio.P_Listado_ID + "'";
                    DataTable Data_Productos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                    Double IVA_Acumulado = 0;
                    Double IEPS_Acumulado = 0;
                    Double Subtotal = 0; 
                    
                    if (Data_Productos.Rows.Count != 0) {
                        for (int i = 0; i < Data_Productos.Rows.Count; i++) {

                            Mi_SQL = "INSERT INTO " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion +
                                " (" + Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Partida_ID +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Cantidad +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Usuario_Creo +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Fecha_Creo +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Importe +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Monto_IVA +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Monto_IEPS +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Porcentaje_IVA +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Porcentaje_IEPS +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Monto_Total +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Tipo +
                                ", " + Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID +
                                " ) VALUES " +
                                "('" + Obtener_Consecutivo(Ope_Tal_Req_Refaccion.Campo_Ope_Tal_Req_Refaccion_ID, Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion) + "'" +
                                ", '" + Requisicion_ID + "'" +
                                ", '" + Data_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_No_Producto_ID].ToString() + "'" +
                                ", '" + Partida_Esp_Almacen_Global + "'" +
                                ", '" + Programa_ID_Almacen + "'" +
                                ", '" + Data_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Cantidad].ToString() + "'" +
                                ", '" + Data_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Usuario_Creo].ToString() + "'" +
                                ", GETDATE()" +
                                ", '" + Data_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Costo_Compra].ToString() + "'" +
                                ", '" + Data_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Monto_IVA].ToString() + "'" +
                                ", '" + Data_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Monto_IEPS].ToString() + "'" +
                                ", '" + Data_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Porcentaje_IVA].ToString() + "'" +
                                ", '" + Data_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Porcentaje_IEPS].ToString() + "'" +
                                ", '" + Data_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Importe].ToString() + "'" +
                                ", 'PRODUCTO','" + Fuente_Financiamiento.Trim() + "')";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();

                            String Monto_IVA = Data_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Monto_IVA].ToString();
                            String Monto_IEPS = Data_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Monto_IEPS].ToString();

                            if (Monto_IVA.Trim() != String.Empty) IVA_Acumulado = IVA_Acumulado + double.Parse(Monto_IVA);
                            if (Monto_IEPS.Trim() != String.Empty) IEPS_Acumulado = IEPS_Acumulado + double.Parse(Monto_IEPS);

                            Subtotal = Subtotal + Double.Parse(Data_Productos.Rows[i][Ope_Tal_Listado_Detalle.Campo_Costo_Compra].ToString());

                        }
                        //Actualizamos la requisicion con los nuevos valores de IVA, IEPs y subtotal 
                        Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                                " SET " + Ope_Tal_Requisiciones.Campo_IVA + " = '" + IVA_Acumulado.ToString() + "'"+
                                ", " + Ope_Tal_Requisiciones.Campo_IEPS + " = '" + IEPS_Acumulado.ToString() + "'" +
                                ", " + Ope_Tal_Requisiciones.Campo_Subtotal + " = '" + Subtotal.ToString() +
                                "' WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = '" + Requisicion_ID + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                    Trans.Commit();
                } catch (SqlException Ex) {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152) {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                    } else if (Ex.Number == 2627) {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    } else if (Ex.Number == 547) {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 515) {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar Modificar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }
                return Requisicion_ID;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Liberar_Presupuesto_Cancelada
            ///DESCRIPCIÓN: Metodo que consulta los productos que se encunentran en reorden 
            ///PARAMETROS: Listado_Negocio
            ///CREO:
            ///FECHA_CREO: 24/Agosto/2010 
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            public void Liberar_Presupuesto_Cancelada(Cls_Ope_Tal_Administrar_Listado_Negocio Listado_Negocio) {
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try {
                    String Mi_SQL = "SELECT " + Ope_Tal_Listado.Campo_Listado_ID +
                                 ", " + Ope_Tal_Listado.Campo_Folio +
                                 ", " + Ope_Tal_Listado.Campo_No_Proyecto_ID +
                                 ", " + Ope_Tal_Listado.Campo_No_Partida_ID +
                                 ", " + Ope_Tal_Listado.Campo_Tipo +
                                 ", " + Ope_Tal_Listado.Campo_Estatus +
                                 ", " + Ope_Tal_Listado.Campo_Total +
                                 " FROM " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
                                 " WHERE " + Ope_Tal_Listado.Campo_Listado_ID + " = '" + Listado_Negocio.P_Listado_ID + "'";
                    DataTable Data_Table = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                    Double Monto = double.Parse(Data_Table.Rows[0][Ope_Tal_Listado.Campo_Total].ToString());
                    String Partida = Data_Table.Rows[0][Ope_Tal_Listado.Campo_No_Partida_ID].ToString();
                    String Programa = Data_Table.Rows[0][Ope_Tal_Listado.Campo_No_Proyecto_ID].ToString();

                    Mi_SQL = "UPDATE " + Ope_Tal_Listado.Tabla_Ope_Tal_Listado +
                                        " SET " + Ope_Tal_Listado.Campo_Estatus + " = '" + Listado_Negocio.P_Estatus + "'" +
                                        ", " + Ope_Tal_Listado.Campo_Empleado_Cancelacion_ID + " = '" + Listado_Negocio.P_Usuario_ID + "'" +
                                        ", " + Ope_Tal_Listado.Campo_Fecha_Cancelacion + " = GETDATE()" +
                                        " WHERE " + Ope_Tal_Listado.Campo_Listado_ID + " = '" + Listado_Negocio.P_Listado_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();


                    Mi_SQL = "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                            " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " = " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " + " + Monto +
                            "," + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " = " + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " - " + Monto +
                            " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID + " = '" + Partida + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID + " = '" + Programa + "'" +
                            " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + " = " +
                                " (SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
                                " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                                " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID + " = '" + Partida + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID + " = '" + Programa + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto + " = TO_CHAR(GETDATE(),'YYYY')" +
                            " ) AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto + " = TO_CHAR(GETDATE(),'YYYY')";

                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Trans.Commit();
                } catch (SqlException Ex) {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152) {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                    } else if (Ex.Number == 2627) {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    } else if (Ex.Number == 547) {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 515) {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar Modificar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consecutivo
            ///DESCRIPCIÓN: Metodo que verfifica el consecutivo en la tabla y ayuda a generar el nuevo Id. 
            ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
            ///            2.-Nombre de la tabla
            ///CREO:
            ///FECHA_CREO: 24/Agosto/2010 
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            public static String Consecutivo(String Campo_ID, String Tabla) {
                String Consecutivo = "";
                String Mi_SQL;         //Obtiene la cadena de inserción hacía la base de datos
                Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos

                Mi_SQL = "SELECT ISNULL(MAX (" + Campo_ID + "),'0000000000') ";
                Mi_SQL = Mi_SQL + "FROM " + Tabla;
                Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                if (Convert.IsDBNull(Obj)) {
                    Consecutivo = "0000000001";
                } else {
                    Consecutivo = string.Format("{0:0000000000}", Convert.ToInt32(Obj) + 1);
                }
                return Consecutivo;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
            ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
            ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
            ///            2.-Nombre de la tabla
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 10/Enero/2011
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            public static int Obtener_Consecutivo(String Campo_ID, String Tabla) {
                int Consecutivo = 0;
                String Mi_Sql;
                Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
                Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla;
                Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                Consecutivo = (Convert.ToInt32(Obj) + 1);
                return Consecutivo;
            }

        #endregion
    
    } 
} 