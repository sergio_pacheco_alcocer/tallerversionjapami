﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Taller_Mecanico.Orden_Salida.Negocio;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Bitacora_Eventos;

/// <summary>
/// Summary description for Cls_Ope_Com_Alm_Orden_Salida_Datos
/// </summary>
/// 

namespace JAPAMI.Taller_Mecanico.Orden_Salida.Datos
{
    public class Cls_Ope_Tal_Alm_Orden_Salida_Datos
    {
        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Consulta_Requisicion_Detalles
        /// DESCRIPCION:            Realizar la consulta de los detalles de una requiscion
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene 
        ///                         los criterios de busqueda
        /// CREO       :            Noe Mosqueda Valadez
        /// FECHA_CREO :            12/Noviembre/2010 10:28 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataTable Consulta_Requisicion_Detalles(Cls_Ope_Tal_Alm_Orden_Salida_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;

            try
            {
                //Asignar consulta para la requisicion
                Mi_SQL = "SELECT " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Clave + ", ";
                Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Nombre + " AS PRODUCTO, ";
                Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Cantidad + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Importe + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Monto_Total + ", ";
                Mi_SQL = Mi_SQL + " '' AS MODELO,";
                Mi_SQL = Mi_SQL + " '' AS MARCA,";
                //Mi_SQL = Mi_SQL + "( SELECT NOMBRE FROM " + Cat_Com_Modelos.Tabla_Cat_Com_Modelos + " WHERE MODELO_ID = ( SELECT MODELO_ID FROM ";
                //Mi_SQL = Mi_SQL + Cat_Com_Productos.Tabla_Cat_Com_Productos + " WHERE PRODUCTO_ID = ";
                //Mi_SQL = Mi_SQL + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " )) AS MODELO, ";

                //Mi_SQL = Mi_SQL + "( SELECT NOMBRE FROM " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " WHERE MARCA_ID = ( SELECT MARCA_ID FROM ";
                //Mi_SQL = Mi_SQL + Cat_Com_Productos.Tabla_Cat_Com_Productos + " WHERE PRODUCTO_ID = ";
                //Mi_SQL = Mi_SQL + Ope_Com_Req_Producto.Campo_Prod_Serv_ID + " )) AS MARCA, ";

                Mi_SQL = Mi_SQL + "( SELECT ABREVIATURA FROM " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " WHERE UNIDAD_ID = ( SELECT UNIDAD_ID FROM ";
                Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID +" = ";
                Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID + " )) AS UNIDAD ";

                Mi_SQL = Mi_SQL + "FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + ", " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " ";
                Mi_SQL = Mi_SQL + "WHERE " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Refaccion_ID + " ";
                Mi_SQL = Mi_SQL + "AND " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID;
                Mi_SQL = Mi_SQL + " = " + Datos.P_No_Requisicion.ToString().Trim() + " ";

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Requisiciones
        ///DESCRIPCIÓN:          Método utilizado para consultar las requisiciones de stock de almacén
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           17/Febrero/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Requisiciones(Cls_Ope_Tal_Alm_Orden_Salida_Negocio Datos)
        {
            // Declaración de variables
            String Mi_SQL = null;
            DataSet Ds_Requisiciones = null;
            DataTable Dt_Requisiciones = new DataTable();

            Mi_SQL = "SELECT DISTINCT O_SALIDA." + Tal_Alm_Salidas.Campo_No_Salida + " as NO_ORDEN_SALIDA ";
            Mi_SQL = Mi_SQL + ",REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "";
            Mi_SQL = Mi_SQL + ", REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Estatus+ "";
            Mi_SQL = Mi_SQL + ", REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Tipo + " as TIPO_REQUISICION";
            Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " as DEPENDENCIA";
            Mi_SQL = Mi_SQL + ", AREAS." + Cat_Areas.Campo_Nombre + " as AREA";
            Mi_SQL = Mi_SQL + ", REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Codigo_Programatico + "";
            Mi_SQL = Mi_SQL + ", O_SALIDA." + Tal_Alm_Salidas.Campo_Usuario_Creo + " as EMPLEADO_SURTIO";
            Mi_SQL = Mi_SQL + ", O_SALIDA." + Tal_Alm_Salidas.Campo_Fecha_Creo + " as FECHA_SURTIDO";
            Mi_SQL = Mi_SQL + ", REQUISICIONES." + Tal_Alm_Salidas.Campo_Subtotal + "";
            Mi_SQL = Mi_SQL + ", REQUISICIONES." + Tal_Alm_Salidas.Campo_IVA + "";
            Mi_SQL = Mi_SQL + ", REQUISICIONES." + Tal_Alm_Salidas.Campo_Total + "";
            Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQUISICIONES";
            Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS";
            Mi_SQL = Mi_SQL + " ON REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Dependencia_ID;
            Mi_SQL = Mi_SQL + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID;
            Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Areas.Tabla_Cat_Areas + " AREAS";
            Mi_SQL = Mi_SQL + " ON REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Area_ID;
            Mi_SQL = Mi_SQL + " = AREAS." + Cat_Areas.Campo_Area_ID;
            Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS";
            Mi_SQL = Mi_SQL + " ON REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Empleado_Surtido_ID;
            Mi_SQL = Mi_SQL + " = EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID;

            Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + " O_SALIDA";
            Mi_SQL = Mi_SQL + " ON O_SALIDA." + Tal_Alm_Salidas.Campo_Requisicion_ID;
            Mi_SQL = Mi_SQL + " = REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Requisicion_ID;

            Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Tal_Alm_Salidas_Detalles.Tabla_Tal_Alm_Salidas_Detalles + " SALIDA_DETALLES";
            Mi_SQL = Mi_SQL + " ON SALIDA_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_No_Salida;
            Mi_SQL = Mi_SQL + " = O_SALIDA." + Tal_Alm_Salidas.Campo_No_Salida;

            Mi_SQL = Mi_SQL + " WHERE  REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Tipo + " IS NOT NULL";
            
            if (Datos.P_No_Orden_Salida != null)
            {
                Mi_SQL = Mi_SQL + " and  O_SALIDA." + Tal_Alm_Salidas.Campo_No_Salida + " like '%" + Datos.P_No_Orden_Salida + "%'";
            }
            if (Datos.P_No_Orden_Compra != null)
            {
                Mi_SQL = Mi_SQL + " and  REQUISICIONES." + Ope_Tal_Requisiciones.Campo_No_Orden_Compra + " like '%" + Datos.P_No_Orden_Compra + "%'";
            }
            if (Datos.P_No_Requisicion != null)
            {
                Mi_SQL = Mi_SQL + " and  REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " like '%" + Datos.P_No_Requisicion + "%'";    
            }
            if (Datos.P_Dependencia != null)
            {
                Mi_SQL = Mi_SQL + " and  REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia + "'";
            }
            if (Datos.P_Area != null)
            {
                Mi_SQL = Mi_SQL + " and  REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Area_ID + "= '" + Datos.P_Area + "'";
            }
            if (Datos.P_Tipo_Salida!= null)
            {
                Mi_SQL = Mi_SQL + " and REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Tipo + " = '" + Datos.P_Tipo_Salida + "'";
            }
            if ((Datos.P_Fecha_Inicial != null) && (Datos.P_Fecha_Final != null))
            {
                //Mi_SQL = Mi_SQL + " AND REQUISICIONES." + Tal_Alm_Salidas.Campo_Fecha_Creo + " BETWEEN TO_DATE('" +
                //string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Datos.P_Fecha_Inicial)) + "','DD/MM/YY')" +
                //" AND TO_DATE('" + string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Datos.P_Fecha_Final)) + "','DD/MM/YY')";


                Mi_SQL = Mi_SQL + " AND REQUISICIONES.FECHA_CREO BETWEEN '" + string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Datos.P_Fecha_Inicial)) + "'" +
                    "AND '" + string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Datos.P_Fecha_Final)) + "'";
            }
            if (!String.IsNullOrEmpty(Datos.P_Estatus))
            {
                Mi_SQL = Mi_SQL + " and REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Estatus + " = '" + Datos.P_Estatus + "'";
            }
            Mi_SQL = Mi_SQL + " order by O_SALIDA." + Tal_Alm_Salidas.Campo_No_Salida + " DESC";

            Ds_Requisiciones = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            Dt_Requisiciones = Ds_Requisiciones.Tables[0];
            return Dt_Requisiciones;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consulta_Productos_Requisicion
        ///DESCRIPCIÓN:          Método utilizado para consultar los productos de 
        ///                      las requisiciones de stock de almacén
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           17/Febrero/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Productos_Orden_Salida(Cls_Ope_Tal_Alm_Orden_Salida_Negocio Datos)
        {
            // Declaración de variables
            String Mi_SQL = null;
            DataSet Ds_Productos_Requisicion = null;
            DataTable Dt_Productos_Requisicion = new DataTable();

            Mi_SQL = " SELECT DISTINCT REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID;
            Mi_SQL = Mi_SQL + ", SALIDAS." + Tal_Alm_Salidas.Campo_No_Salida + " ";
            Mi_SQL = Mi_SQL + ", SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Refaccion_ID + "";
            Mi_SQL = Mi_SQL + ", SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Cantidad + " as CANTIDAD_ENTREGADA";
            Mi_SQL = Mi_SQL + ", SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Costo + "";
            Mi_SQL = Mi_SQL + ", SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Importe + "";
            Mi_SQL = Mi_SQL + ", SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Subtotal + "";
            Mi_SQL = Mi_SQL + ", SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_IVA + "";
            Mi_SQL = Mi_SQL + ", REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Cantidad + " as CANTIDAD_SOLICITADA ";

            Mi_SQL = Mi_SQL + ", ( SELECT " + Cat_Tal_Refacciones.Campo_Nombre + " FROM ";
            Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " WHERE ";
            Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = SALIDAS_DETALLES.";
            Mi_SQL = Mi_SQL + Tal_Alm_Salidas_Detalles.Campo_Refaccion_ID + " AND SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_No_Salida;
            Mi_SQL = Mi_SQL + " = " + Datos.P_No_Orden_Salida.Trim() + " ) AS PRODUCTO ";

            Mi_SQL = Mi_SQL + ", ( SELECT " + Cat_Tal_Refacciones.Campo_Descripcion + " FROM ";
            Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " WHERE ";
            Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = SALIDAS_DETALLES.";
            Mi_SQL = Mi_SQL + Tal_Alm_Salidas_Detalles.Campo_Refaccion_ID + " AND SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_No_Salida;
            Mi_SQL = Mi_SQL + " = " + Datos.P_No_Orden_Salida.Trim() + " ) AS DESCRIPCION ";

            Mi_SQL = Mi_SQL + ",(SELECT UNIDADES." + Cat_Com_Unidades.Campo_Abreviatura + " FROM ";
            Mi_SQL = Mi_SQL + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " UNIDADES ";
            Mi_SQL = Mi_SQL + " WHERE UNIDADES." + Cat_Com_Unidades.Campo_Unidad_ID + " = ";
            Mi_SQL = Mi_SQL + " (SELECT PRODUCTOS." + Cat_Tal_Refacciones.Campo_Unidad_ID + " FROM ";
            Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTOS ";
            Mi_SQL = Mi_SQL + " WHERE SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Refaccion_ID + " = ";
            Mi_SQL = Mi_SQL + " PRODUCTOS." + Cat_Tal_Refacciones.Campo_Refaccion_ID + ")) as UNIDAD ";

            Mi_SQL = Mi_SQL + " FROM " + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + " SALIDAS, ";
            Mi_SQL = Mi_SQL + " " + Tal_Alm_Salidas_Detalles.Tabla_Tal_Alm_Salidas_Detalles + " SALIDAS_DETALLES, ";
            Mi_SQL = Mi_SQL + " " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " REQ_PRODUCTO ";

            Mi_SQL = Mi_SQL + " WHERE  SALIDAS." + Tal_Alm_Salidas.Campo_No_Salida + " = ";
            Mi_SQL = Mi_SQL + " SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_No_Salida + "  ";
            Mi_SQL = Mi_SQL + " and SALIDAS." + Tal_Alm_Salidas.Campo_No_Salida + " = ";
            Mi_SQL = Mi_SQL + Datos.P_No_Orden_Salida;
            Mi_SQL = Mi_SQL + " and REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = ";
            Mi_SQL = Mi_SQL + " SALIDAS." + Tal_Alm_Salidas.Campo_Requisicion_ID + "  ";
            Mi_SQL = Mi_SQL + " and REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID + " = ";
            Mi_SQL = Mi_SQL + " SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Refaccion_ID + "  ";
            Ds_Productos_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            Dt_Productos_Requisicion = Ds_Productos_Requisicion.Tables[0];           
            return Dt_Productos_Requisicion;
        }
     
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_DataTable
        ///DESCRIPCIÓN:          Método utilizado para consultar loas dependencias y las áreas
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           18/Febrero/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_DataTable(Cls_Ope_Tal_Alm_Orden_Salida_Negocio Datos)
        {
            // Declaración de Variables
            String Mi_SQL = null;
            DataSet Ds_Consulta = null;
            DataTable Dt_consulta = new DataTable();

            try
            {
                if (Datos.P_Tipo_Data_Table.Equals("DEPENDENCIAS"))
                {
                    Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Dependencia_ID + " AS DEPENDENCIA_ID, ";
                    Mi_SQL = Mi_SQL + Cat_Dependencias.Campo_Dependencia_ID + "  +' '+ ";
                    Mi_SQL = Mi_SQL + Cat_Dependencias.Campo_Comentarios + " AS NOMBRE";
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;

                    Mi_SQL = Mi_SQL + " order by  " + Cat_Dependencias.Campo_Comentarios;
                }
                else if (Datos.P_Tipo_Data_Table.Equals("AREAS"))
                {
                    Mi_SQL = "SELECT AREAS." + Cat_Areas.Campo_Area_ID + " AS AREA_ID";
                    Mi_SQL = Mi_SQL + " , AREAS." + Cat_Areas.Campo_Nombre + " AS NOMBRE ";
                    Mi_SQL = Mi_SQL + " FROM " + Cat_Areas.Tabla_Cat_Areas + " AREAS ";

                    if (Datos.P_Dependencia != "")
                    {
                        Mi_SQL = Mi_SQL + " WHERE AREAS." + Cat_Areas.Campo_Dependencia_ID + " = ";
                        Mi_SQL = Mi_SQL + "'" + Datos.P_Dependencia + "'";
                    }
                    Mi_SQL = Mi_SQL + " order by " + Cat_Areas.Campo_Nombre + " ";
                }

                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Consulta = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Consulta != null)
                {
                    Dt_consulta= Ds_Consulta.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_consulta;
        }



        // PARA LA REIMPRESION



        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Informacion_General_OS
        ///DESCRIPCIÓN:          Método donde se consulta la información general de la orden de salida que se genero
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           12/Julio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Informacion_General_OS(Cls_Ope_Tal_Alm_Orden_Salida_Negocio Datos)
        {
            // Declaración de variables
            String Mi_SQL = null;
            DataTable Dt_Cabecera = new DataTable();

            Mi_SQL = "SELECT " + "SALIDAS." + Tal_Alm_Salidas.Campo_No_Salida + " as NO_ORDEN_SALIDA";
            Mi_SQL = Mi_SQL + ",(select DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " from ";
            Mi_SQL = Mi_SQL + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ";
            Mi_SQL = Mi_SQL + " where SALIDAS." + Tal_Alm_Salidas.Campo_Dependencia_ID + " = DEPENDENCIAS.";
            Mi_SQL = Mi_SQL + Cat_Dependencias.Campo_Dependencia_ID + ")as UNIDAD_RESPONSABLE";

            Mi_SQL = Mi_SQL + ",(select distinct (FINANCIAMIENTO." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ")";
            Mi_SQL = Mi_SQL + " from " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " FINANCIAMIENTO ";
            Mi_SQL = Mi_SQL + "  where FINANCIAMIENTO." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
            Mi_SQL = Mi_SQL + " = (select distinct(REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID + ") from ";
            Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " REQ_PRODUCTO ";
            Mi_SQL = Mi_SQL + " where  REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = ";
            Mi_SQL = Mi_SQL + " SALIDAS." + Tal_Alm_Salidas.Campo_Requisicion_ID + "))as F_FINANCIAMIENTO";

            Mi_SQL = Mi_SQL + ",(select distinct (PROY_PROGRAMAS." + Cat_Com_Proyectos_Programas.Campo_Descripcion + ")";
            Mi_SQL = Mi_SQL + " from " + Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas + " PROY_PROGRAMAS ";
            Mi_SQL = Mi_SQL + "  where PROY_PROGRAMAS." + Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID;
            Mi_SQL = Mi_SQL + " =(select distinct (REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID + ") from ";
            Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " REQ_PRODUCTO ";
            Mi_SQL = Mi_SQL + " where  REQ_PRODUCTO." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = ";
            Mi_SQL = Mi_SQL + " SALIDAS." + Tal_Alm_Salidas.Campo_Requisicion_ID + "))as PROGRAMA";

            Mi_SQL = Mi_SQL + ", REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Folio;
            Mi_SQL = Mi_SQL + ", REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Fecha_Autorizacion;
            Mi_SQL = Mi_SQL + ", REQUISICIONES." + Ope_Tal_Requisiciones.Campo_No_Solicitud;
            Mi_SQL = Mi_SQL + ", SALIDAS." + Tal_Alm_Salidas.Campo_Usuario_Creo + " as ENTREGO ";

            Mi_SQL = Mi_SQL + ", (select EMPLEADOS." + Cat_Empleados.Campo_Apellido_Paterno + " +' '+";
            Mi_SQL = Mi_SQL + " EMPLEADOS." + Cat_Empleados.Campo_Apellido_Materno + " +' '+";
            Mi_SQL = Mi_SQL + " EMPLEADOS." + Cat_Empleados.Campo_Nombre;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados + " EMPLEADOS ";
            Mi_SQL = Mi_SQL + " where EMPLEADOS." + Cat_Empleados.Campo_Empleado_ID + " = ";
            Mi_SQL = Mi_SQL + " SALIDAS." + Tal_Alm_Salidas.Campo_Empleado_Solicito_ID + ") as RECIBIO";

            Mi_SQL = Mi_SQL + " FROM " + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + " SALIDAS ";
            Mi_SQL = Mi_SQL + ", " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQUISICIONES ";
            Mi_SQL = Mi_SQL + " where REQUISICIONES." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = ";
            Mi_SQL = Mi_SQL + " SALIDAS." + Tal_Alm_Salidas.Campo_Requisicion_ID + "";
            Mi_SQL = Mi_SQL + " AND SALIDAS." + Tal_Alm_Salidas.Campo_No_Salida + " = ";
            Mi_SQL = Mi_SQL + Datos.P_No_Orden_Salida;

            Dt_Cabecera = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Cabecera;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Detalles_Orden_Salida
        ///DESCRIPCIÓN:          Método donde se consultan los detalles de la orden de salida que se genero
        ///PARAMETROS:   
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           24/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Detalles_Orden_Salida(Cls_Ope_Tal_Alm_Orden_Salida_Negocio Datos)
        {
            // Declaración de variables
            String Mi_SQL = null;
            DataTable Dt_Detalles = new DataTable();

            Mi_SQL = "SELECT SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_No_Salida + " as NO_ORDEN_SALIDA";
            Mi_SQL = Mi_SQL + ",(select PRODUCTOS." + Cat_Tal_Refacciones.Campo_Clave + " from ";
            Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTOS ";
            Mi_SQL = Mi_SQL + " where SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Refaccion_ID + " = PRODUCTOS.";
            Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Refaccion_ID + ")as CLAVE";

            Mi_SQL = Mi_SQL + ",(select PRODUCTOS." + Cat_Tal_Refacciones.Campo_Nombre + " +' '+ ";
            Mi_SQL = Mi_SQL + " PRODUCTOS." + Cat_Tal_Refacciones.Campo_Descripcion + " from ";
            Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTOS ";
            Mi_SQL = Mi_SQL + " where SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Refaccion_ID + " = PRODUCTOS.";
            Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Refaccion_ID + ")as PRODUCTO";

            Mi_SQL = Mi_SQL + ",(select REQ_PRODUCTOS." + Ope_Tal_Req_Refaccion.Campo_Cantidad + " from ";
            Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " REQ_PRODUCTOS ";
            Mi_SQL = Mi_SQL + " where  REQ_PRODUCTOS." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + " = ";
            Mi_SQL = Mi_SQL + " (select SALIDAS." + Tal_Alm_Salidas.Campo_Requisicion_ID + " from ";
            Mi_SQL = Mi_SQL + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + " SALIDAS ";
            Mi_SQL = Mi_SQL + " where  SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_No_Salida + " = ";
            Mi_SQL = Mi_SQL + " SALIDAS." + Tal_Alm_Salidas.Campo_No_Salida + ")";
            Mi_SQL = Mi_SQL + " and REQ_PRODUCTOS." + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID + " = ";
            Mi_SQL = Mi_SQL + " SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Refaccion_ID + ") as CANTIDAD_SOLICITADA ";

            Mi_SQL = Mi_SQL + ",SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Cantidad + " as CANTIDAD_ENTREGADA";

            Mi_SQL = Mi_SQL + ",(select UNIDADES." + Cat_Com_Unidades.Campo_Abreviatura + " from ";
            Mi_SQL = Mi_SQL + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " UNIDADES ";
            Mi_SQL = Mi_SQL + " where  UNIDADES." + Cat_Com_Unidades.Campo_Unidad_ID + " = ";
            Mi_SQL = Mi_SQL + " (select PRODUCTOS." + Cat_Tal_Refacciones.Campo_Unidad_ID + " from ";
            Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTOS ";
            Mi_SQL = Mi_SQL + " where  SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Refaccion_ID + " = ";
            Mi_SQL = Mi_SQL + " PRODUCTOS." + Cat_Tal_Refacciones.Campo_Refaccion_ID + ")) as UNIDADES";

            Mi_SQL = Mi_SQL + ",SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Costo + " as PRECIO";
            Mi_SQL = Mi_SQL + ",SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Subtotal + "";
            Mi_SQL = Mi_SQL + ",SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_IVA + "";
            Mi_SQL = Mi_SQL + ",SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_Importe + " as TOTAL";

            Mi_SQL = Mi_SQL + " FROM " + Tal_Alm_Salidas_Detalles.Tabla_Tal_Alm_Salidas_Detalles + " SALIDAS_DETALLES";
            Mi_SQL = Mi_SQL + " WHERE SALIDAS_DETALLES." + Tal_Alm_Salidas_Detalles.Campo_No_Salida + " = ";
            Mi_SQL = Mi_SQL + Datos.P_No_Orden_Salida;

            Dt_Detalles = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            return Dt_Detalles;
        }
        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Alta_orden_Salida
        /// DESCRIPCION:            Dar de alta la orden de salida de material
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene 
        ///                         los datos para al operacion
        /// CREO       :            Noe Mosqueda Valadez
        /// FECHA_CREO :            20/Noviembre/2010 9:28 
        /// MODIFICO          :     Salvador Hernández Ramírez
        /// FECHA_MODIFICO    :     10/Mayo/2011
        /// CAUSA_MODIFICACION:     Se asigno codigo para actualizar los Montos y las cantidades de productos
        ///*******************************************************************************/
        public static long Alta_Orden_Salida(Cls_Ope_Tal_Alm_Orden_Salida_Negocio Datos)
        {
            // Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            Object Aux; // Variable auxiliar para las consultas
            String Mensaje = String.Empty; //Variable para el mensaje de error
            DataTable Dt_Aux = new DataTable(); //Tabla auxiliar para las consultas
            SqlDataAdapter Obj_Adaptador; //Adapatador para el llenado de las tablas
            DataTable Dt_Requisiciones_Detalles = new DataTable(); //Tabla para los detalles de las requisiciones

            Double Monto_Comprometido = 0.0; // Variable para el monto comprometido
            Double Monto_Disponible = 0.0;   // Variable para el monto disponible
            Double Monto_Ejercido = 0.0;    // Variable para el monto ejercido

            String No_Asignacion = String.Empty; // Variable para el No de Asignacion            
            String Partida_ID = String.Empty; // Variable para el ID de la partida
            String Proyecto_Programa_ID = String.Empty; // Variable para el ID del programa o proyecto
            String Dependencia_ID = String.Empty; // Variable para el ID de la dependencia
            Double Monto_Total = 0.0; // Variable para el monto total de los detalles de la requisicion
            
            // Variables utilizadas para actualizar los productos
            int Cantidad_Comprometida = 0; // Variable para la cantidad Comprometida
            int Cantidad_Existente = 0; // Variable para la cantidad Existente
            int Cantidad_Disponible = 0; //Variable para la cantidad Disponible

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Adaptador = new SqlDataAdapter();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;               

                //Asignar consulta para el Maximo ID
                Mi_SQL = "SELECT ISNULL(MAX(" + Tal_Alm_Salidas.Campo_No_Salida + "), 0) FROM " + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas;

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Aux = Obj_Comando.ExecuteScalar();

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                    Datos.P_No_Orden_Salida =  (Convert.ToInt64(Aux) + 1).ToString();
                else
                    Datos.P_No_Orden_Salida = "1";

                // Consulta para los ID de la dependencia, area, etc
                Mi_SQL = "SELECT " + Ope_Tal_Requisiciones.Campo_Dependencia_ID + ", " + Ope_Tal_Requisiciones.Campo_Area_ID + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Campo_Empleado_Generacion_ID + " ";
                Mi_SQL = Mi_SQL + "FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " ";
                Mi_SQL = Mi_SQL + "WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = " + Datos.P_No_Requisicion.ToString().Trim() + " ";

                //Ejecutar consulta
                Dt_Aux = new DataTable(); 
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Adaptador.SelectCommand = Obj_Comando;
                Obj_Adaptador.Fill(Dt_Aux);

                //Verificar si la consulta arrojo resultado
                if (Dt_Aux.Rows.Count > 0)
                {
                    // Colocar los valores en las variables
                    Datos.P_Dependencia = Dt_Aux.Rows[0][0].ToString().Trim();
                    Datos.P_Area = Dt_Aux.Rows[0][1].ToString().Trim();
                    Datos.P_Empleado_Solicito_ID = Dt_Aux.Rows[0][2].ToString().Trim();

                    //El tipo de salida es la 1
                    Datos.P_Tipo_Salida = "00001";
                }
                else
                {
                    throw new Exception("Datos no encontrados requisicion no " + Datos.P_No_Requisicion.ToString().Trim());
                }
                
                // Consulta para dar de alta la salida
                //Mi_SQL = "INSERT INTO " + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + " (" + Tal_Alm_Salidas.Campo_No_Salida + ", ";
                //Mi_SQL = Mi_SQL + Tal_Alm_Salidas.Campo_Dependencia_ID + ", " + Tal_Alm_Salidas.Campo_Area_ID + ", ";
                //Mi_SQL = Mi_SQL + Tal_Alm_Salidas.Campo_Empleado_Solicito_ID + ", " + Tal_Alm_Salidas.Campo_Requisicion_ID + ", ";
                //Mi_SQL = Mi_SQL + Tal_Alm_Salidas.Campo_Tipo_Salida_ID + ", " + Tal_Alm_Salidas.Campo_Usuario_Creo + ", ";
                //Mi_SQL = Mi_SQL + Tal_Alm_Salidas.Campo_Fecha_Creo + ", " + Tal_Alm_Salidas.Campo_Empleado_Almacen_ID + ") ";
                //Mi_SQL = Mi_SQL + "VALUES(" + Datos.P_No_Orden_Salida + ", ";
                //Mi_SQL = Mi_SQL + "'" + Datos.P_Dependencia + "', '" + Datos.P_Area + "', '" + Datos.P_Empleado_Solicito + "', ";
                //Mi_SQL = Mi_SQL + Datos.P_No_Requisicion.ToString().Trim() + ", '" + Datos.P_Tipo_Salida + "', ";
                //Mi_SQL = Mi_SQL + "'" + Datos.P_Usuario + "', GETDATE(), '" + Datos.P_Empleado_Surtido_ID + "')";


                // Consulta para dar de alta la salida
                Mi_SQL = "INSERT INTO " + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + " (" + Tal_Alm_Salidas.Campo_No_Salida + ", ";
                Mi_SQL = Mi_SQL + Tal_Alm_Salidas.Campo_Dependencia_ID + ", " + Tal_Alm_Salidas.Campo_Area_ID + ", ";
                Mi_SQL = Mi_SQL + Tal_Alm_Salidas.Campo_Empleado_Solicito_ID + ", " + Tal_Alm_Salidas.Campo_Requisicion_ID + ", ";
                Mi_SQL = Mi_SQL + Tal_Alm_Salidas.Campo_Tipo_Salida_ID + ", " + Tal_Alm_Salidas.Campo_Usuario_Creo + ", ";
                Mi_SQL = Mi_SQL + Tal_Alm_Salidas.Campo_Fecha_Creo + ", " + Tal_Alm_Salidas.Campo_Empleado_Almacen_ID + ") ";
                Mi_SQL = Mi_SQL + "VALUES(" + Datos.P_No_Orden_Salida + ", ";
                Mi_SQL = Mi_SQL + "'" + Datos.P_Dependencia + "', null, '" + Datos.P_Empleado_Solicito + "', ";
                Mi_SQL = Mi_SQL + Datos.P_No_Requisicion.ToString().Trim() + ", '" + Datos.P_Tipo_Salida + "', ";
                Mi_SQL = Mi_SQL + "'" + Datos.P_Usuario + "', GETDATE(), '" + Datos.P_Empleado_Surtido_ID + "')";


                String No_Salida = Convert.ToString(Datos.P_No_Orden_Salida);

                // Se registra  el Insert en la bitacora
                //Cls_Bitacora.Alta_Bitacora(Cls_Sessiones.Empleado_ID, Ope_Bitacora.Accion_Alta, "Frm_Alm_Com_Orden_Salida.aspx", No_Salida, Mi_SQL);

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Consulta para la actualizacion de la requisicion
                Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " ";
                Mi_SQL = Mi_SQL + "SET " + Ope_Tal_Requisiciones.Campo_Estatus + " = 'SURTIDA', ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Campo_Fecha_Surtido + " = GETDATE(), ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Campo_Empleado_Surtido_ID + " = '" + Datos.P_Empleado_Surtido_ID + "' ";
                Mi_SQL = Mi_SQL + "WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = " + Datos.P_No_Requisicion.ToString().Trim() + " ";

                String No_Requisicion = Convert.ToString(Datos.P_No_Requisicion);

                // Se registra  el update en la bitacora
                //Cls_Bitacora.Alta_Bitacora(Cls_Sessiones.Empleado_ID, Ope_Bitacora.Accion_Modificar, "Frm_Alm_Com_Orden_Salida.aspx", No_Requisicion, Mi_SQL);

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Consulta para el maximo ID de las observaciones
                Mi_SQL = "SELECT ISNULL(MAX(" + Ope_Tal_Req_Observaciones.Campo_Observacion_ID + "), 0) ";
                Mi_SQL = Mi_SQL + "FROM " + Ope_Tal_Req_Observaciones.Tabla_Ope_Tal_Req_Observaciones + " ";

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Aux = Obj_Comando.ExecuteScalar();

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                    Datos.P_Observacion_ID = Convert.ToInt64(Aux) + 1;
                else
                    Datos.P_Observacion_ID = 1;

                //Consulta para agregar los comentarios
                Mi_SQL = "INSERT INTO " + Ope_Tal_Req_Observaciones.Tabla_Ope_Tal_Req_Observaciones;
                Mi_SQL = Mi_SQL + " (" + Ope_Tal_Req_Observaciones.Campo_Observacion_ID + ", " + Ope_Tal_Req_Observaciones.Campo_Requisicion_ID + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Req_Observaciones.Campo_Comentario + ", " + Ope_Tal_Req_Observaciones.Campo_Estatus + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Req_Observaciones.Campo_Usuario_Creo + ", " + Ope_Tal_Req_Observaciones.Campo_Fecha_Creo + ") ";
                Mi_SQL = Mi_SQL + "VALUES(" + Datos.P_Observacion_ID + ", " + Datos.P_No_Requisicion.ToString().Trim() + ", '" + Datos.P_Comentarios + "', ";
                Mi_SQL = Mi_SQL + "'SURTIDA', '" + Datos.P_Usuario + "', GETDATE()) ";

                String Observacion_ID = Convert.ToString(Datos.P_Observacion_ID);

                // Se registra  el Insert en la bitacora
                //Cls_Bitacora.Alta_Bitacora(Cls_Sessiones.Empleado_ID, Ope_Bitacora.Accion_Alta, "Frm_Alm_Com_Orden_Salida.aspx", Observacion_ID, Mi_SQL);

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Consulta para los detalles de la requisicion
                Mi_SQL = "SELECT " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Partida_ID + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Monto_Total + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Cantidad + ", ";
                Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Costo_Unitario + ", ";
                Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Costo_Unitario + " AS COSTO_PROMEDIO, ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Dependencia_ID + " ";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + ", " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Refaccion_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Refaccion_ID + " ";
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID;
                Mi_SQL = Mi_SQL + " = " + Datos.P_No_Requisicion.ToString().Trim() + " ";
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Requisicion_ID;
                Mi_SQL = Mi_SQL + " = " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + "." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID;

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Adaptador.SelectCommand = Obj_Comando;
                Obj_Adaptador.Fill(Dt_Requisiciones_Detalles);

                //Verificar si tiene datos
                if (Dt_Requisiciones_Detalles.Rows.Count > 0)
                {
                    //Ciclo para el desplazamiento de la tabla
                    for (int Cont_Elementos=0; Cont_Elementos < Dt_Requisiciones_Detalles.Rows.Count; Cont_Elementos++)
                    {
                        //Consulta para dar de alta los detalles de la salida
                        Mi_SQL = "INSERT INTO " + Tal_Alm_Salidas_Detalles.Tabla_Tal_Alm_Salidas_Detalles + " (" + Tal_Alm_Salidas_Detalles.Campo_No_Salida + ", ";
                        Mi_SQL = Mi_SQL + Tal_Alm_Salidas_Detalles.Campo_Refaccion_ID + ", " + Tal_Alm_Salidas_Detalles.Campo_Cantidad + ", ";
                        Mi_SQL = Mi_SQL + Tal_Alm_Salidas_Detalles.Campo_Costo + ", " + Tal_Alm_Salidas_Detalles.Campo_Costo_Promedio + ", ";
                        Mi_SQL = Mi_SQL + Tal_Alm_Salidas_Detalles.Campo_Importe + ") VALUES(" + Datos.P_No_Orden_Salida + ", ";
                        Mi_SQL = Mi_SQL + "'" + Dt_Requisiciones_Detalles.Rows[Cont_Elementos]["REFACCION_ID"].ToString().Trim() + "', ";
                        Mi_SQL = Mi_SQL + Dt_Requisiciones_Detalles.Rows[Cont_Elementos]["CANTIDAD"].ToString().Trim() + ", ";
                        Mi_SQL = Mi_SQL + Dt_Requisiciones_Detalles.Rows[Cont_Elementos]["COSTO_UNITARIO"].ToString().Trim() + ", ";
                        Mi_SQL = Mi_SQL + Dt_Requisiciones_Detalles.Rows[Cont_Elementos]["COSTO_PROMEDIO"].ToString().Trim() + ", ";
                        //Mi_SQL = Mi_SQL + Convert.ToString(Convert.ToDouble(Dt_Requisiciones_Detalles.Rows[Cont_Elementos]["CANTIDAD"]) * Convert.ToDouble(Dt_Requisiciones_Detalles.Rows[Cont_Elementos]["COSTO_PROMEDIO"])) + ")";
                        Mi_SQL = Mi_SQL + Dt_Requisiciones_Detalles.Rows[Cont_Elementos]["MONTO_TOTAL"].ToString().Trim() + ")";

                        String N_Salida = Convert.ToString(Datos.P_No_Orden_Salida);
                        // Se registra  el Insert en la bitacora
                        //Cls_Bitacora.Alta_Bitacora(Cls_Sessiones.Empleado_ID, Ope_Bitacora.Accion_Alta, "Frm_Alm_Com_Orden_Salida.aspx", N_Salida, Mi_SQL);

                        //Ejecutar consulta
                        Obj_Comando.CommandText = Mi_SQL;
                        Obj_Comando.ExecuteNonQuery();


                // SE ACTUALIZAN LOS MONTOS 
                        // Asignar el ID de la partida y el ID del proyecto o programa
                        Partida_ID = Dt_Requisiciones_Detalles.Rows[Cont_Elementos]["PARTIDA_ID"].ToString().Trim();
                        Proyecto_Programa_ID = Dt_Requisiciones_Detalles.Rows[Cont_Elementos]["PROYECTO_PROGRAMA_ID"].ToString().Trim();
                        Dependencia_ID = Dt_Requisiciones_Detalles.Rows[Cont_Elementos]["DEPENDENCIA_ID"].ToString().Trim();
                        //Verificar si no es nulo
                        if (Convert.IsDBNull(Dt_Requisiciones_Detalles.Rows[Cont_Elementos]["MONTO_TOTAL"]) == false)
                            Monto_Total = Convert.ToDouble(Dt_Requisiciones_Detalles.Rows[Cont_Elementos]["MONTO_TOTAL"]);
                        else
                            Monto_Total = 0;
                        Mi_SQL = "SELECT  MAX (" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ") ";
                        // Consulta para obtener el mayor numero de asignación
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                        Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID;
                        Mi_SQL = Mi_SQL + " = '" + Dependencia_ID+ "'";
                        Mi_SQL = Mi_SQL + " and " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Partida_ID;
                        Mi_SQL = Mi_SQL + " = '" + Partida_ID + "'";
                        Mi_SQL = Mi_SQL + " and " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID;
                        Mi_SQL = Mi_SQL + " = '" + Proyecto_Programa_ID + "'";
                        Mi_SQL = Mi_SQL + " and " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto;
                        Mi_SQL = Mi_SQL + " = YEAR(GETDATE()) ";

                        // Ejecutar consulta
                        Obj_Comando.CommandText = Mi_SQL;
                        Aux = Obj_Comando.ExecuteScalar();

                        // Verificar si es nulo
                        if (Convert.IsDBNull(Aux) == false)
                            No_Asignacion = Aux.ToString().Trim();

                        if (No_Asignacion.Trim().Length == 0)
                            No_Asignacion = "-1";

                        // Consulta para obtener los  montos 
                        Mi_SQL = "SELECT  " + Cat_Com_Dep_Presupuesto.Campo_Monto_Ejercido + ", ";
                        Mi_SQL = Mi_SQL + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + ", ";
                        Mi_SQL = Mi_SQL + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + "";
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                        Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID;
                        Mi_SQL = Mi_SQL + " = '" + Dependencia_ID + "'";
                        Mi_SQL = Mi_SQL + " and " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Partida_ID;
                        Mi_SQL = Mi_SQL + " = '" + Partida_ID + "'";
                        Mi_SQL = Mi_SQL + " and " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID;
                        Mi_SQL = Mi_SQL + " = '" +  Proyecto_Programa_ID + "'";
                        Mi_SQL = Mi_SQL + " and " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto;
                        Mi_SQL = Mi_SQL + " = YEAR(GETDATE()) ";
                        Mi_SQL = Mi_SQL + " and " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + "." + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio;
                        Mi_SQL = Mi_SQL + " = " + No_Asignacion;

                        //Ejecutar consulta
                        DataTable Dt_Aux_Presupuestos = new DataTable();
                        Obj_Comando.CommandText = Mi_SQL;
                        Obj_Adaptador.SelectCommand = Obj_Comando;
                        Obj_Adaptador.Fill(Dt_Aux_Presupuestos);

                        // Verificar si la consulta tiene elementos
                        if (Dt_Aux_Presupuestos.Rows.Count > 0)
                        {
                            if (Convert.IsDBNull(Dt_Aux_Presupuestos.Rows[0]["MONTO_EJERCIDO"]) != false) // Si no tiene un monto ejercido entra
                                Monto_Ejercido = Monto_Total; // Obtener el nuevo monto ejercido 
                            else
                                Monto_Ejercido = Convert.ToDouble(Dt_Aux_Presupuestos.Rows[0]["MONTO_EJERCIDO"]) + Monto_Total;// Obtener el  monto ejercido y lo suma al monto Total del producto


                            if (Convert.IsDBNull(Dt_Aux_Presupuestos.Rows[0]["MONTO_COMPROMETIDO"]) == false)
                                Monto_Comprometido = Convert.ToDouble(Dt_Aux_Presupuestos.Rows[0]["MONTO_COMPROMETIDO"]) - Monto_Total; // Obtener el  MONTO COMPROMETIDO y le resta el MONTO TOTAL del producto
                            else
                                Monto_Comprometido = 0;

                            if (Convert.IsDBNull(Dt_Aux_Presupuestos.Rows[0]["MONTO_DISPONIBLE"]) == false)
                                Monto_Disponible = Convert.ToDouble(Dt_Aux_Presupuestos.Rows[0]["MONTO_DISPONIBLE"]);// Obtener el  MONTO DISPONIBLE y le resta el MONTO TOTAL del producto
                            else
                                Monto_Disponible = 0;

                            // Actualizar la tabla de los presupuestos
                            Mi_SQL = " UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto + " ";
                            Mi_SQL = Mi_SQL + " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Ejercido + " = " + Monto_Ejercido + ", ";
                            Mi_SQL = Mi_SQL + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " = " + Monto_Comprometido + ", ";
                            Mi_SQL = Mi_SQL + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " = " + Monto_Disponible + "";
                            Mi_SQL = Mi_SQL + " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID + " = '" + Dependencia_ID + "'";
                            Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID + " = '" + Partida_ID + "'";
                            Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID + " = '" + Proyecto_Programa_ID + "'";
                            Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto + " = YEAR(GETDATE()) ";
                            Mi_SQL = Mi_SQL + " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + " = " + No_Asignacion;

                            // Se da de alta la operación en el método "Alta_Bitacora"
                            //Cls_Bitacora.Alta_Bitacora(Cls_Sessiones.Empleado_ID, Ope_Bitacora.Accion_Modificar, "Frm_Alm_Com_Recepcion_Material.aspx", Proyecto_Programa_ID, Mi_SQL);

                            //Ejecutar consulta
                            Obj_Comando.CommandText = Mi_SQL;
                            Obj_Comando.ExecuteNonQuery(); // Se ejecuta la operación 
                        }
                        else
                        {
                            // Escribir un mensaje que indica que no se actualizó el presupuesto  
                        }

                  // SE DISMINUYEN LOS PRODUCTOS

                        //Consulta para el campo comprometido
                        Mi_SQL = "SELECT " + Cat_Tal_Refacciones.Campo_Comprometido + ", " + Cat_Tal_Refacciones.Campo_Existencia + " ";
                        Mi_SQL = Mi_SQL + "FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " ";
                        Mi_SQL = Mi_SQL + "WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Dt_Requisiciones_Detalles.Rows[Cont_Elementos][3].ToString().Trim() + "'";

                        //Ejecutar consulta
                        Dt_Aux = new DataTable();
                        Obj_Comando.CommandText = Mi_SQL;
                        Obj_Adaptador.SelectCommand = Obj_Comando;
                        Obj_Adaptador.Fill(Dt_Aux);

                        //Verificar si la consulta arrojo resultado
                        if (Dt_Aux.Rows.Count > 0)
                        {
                            //Asignar los valores de los montos
                            if (Convert.IsDBNull(Dt_Aux.Rows[0][0]) == false)
                                Cantidad_Comprometida = Convert.ToInt32(Dt_Aux.Rows[0][0]);
                            else
                                Cantidad_Comprometida = 0;

                            if (Convert.IsDBNull(Dt_Aux.Rows[0][1]) == false)
                                Cantidad_Existente = Convert.ToInt32(Dt_Aux.Rows[0][1]);
                            else
                                Cantidad_Existente = 0;
                        }
                        else
                        {
                            // Asignar los valores de los montos
                            Cantidad_Comprometida = 0;
                            Cantidad_Existente = 0;
                        }

                        //Realizar los calculos de los montos
                        Cantidad_Comprometida = Cantidad_Comprometida - Convert.ToInt32(Dt_Requisiciones_Detalles.Rows[Cont_Elementos][4]);
                        Cantidad_Existente= Cantidad_Existente - Convert.ToInt32(Dt_Requisiciones_Detalles.Rows[Cont_Elementos][4]);
                        Cantidad_Disponible = Cantidad_Existente;

                        //Consulta para modificar las cantidades en la base de datos
                        Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " ";
                        Mi_SQL = Mi_SQL + "SET " + Cat_Tal_Refacciones.Campo_Comprometido + " = " + Cantidad_Comprometida.ToString().Trim() + ", ";
                        Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Existencia + " = " + Cantidad_Existente.ToString().Trim() + ", ";
                        Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Disponible + " = " + Cantidad_Disponible.ToString().Trim() + " ";
                        Mi_SQL = Mi_SQL + "WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Dt_Requisiciones_Detalles.Rows[Cont_Elementos][3].ToString().Trim() + "'";

                        String Producto_ID = "" + Dt_Requisiciones_Detalles.Rows[Cont_Elementos][3].ToString().Trim();

                        // Se registra  el Update en la bitacora
                        //Cls_Bitacora.Alta_Bitacora(Cls_Sessiones.Empleado_ID, Ope_Bitacora.Accion_Modificar, "Frm_Alm_Com_Orden_Salida.aspx", Producto_ID, Mi_SQL);

                        //Ejecutar consulta
                        Obj_Comando.CommandText = Mi_SQL;
                        Obj_Comando.ExecuteNonQuery();
                    }
                }
                //Ejecutar transaccion
                Obj_Transaccion.Commit();

                //Entregar resultado
                return Convert.ToInt64(Datos.P_No_Orden_Salida);
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }
                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;               
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Imprime_orden_Salida
        /// DESCRIPCION:            Consultar los datos para la impresion de la orden de salida
        /// PARAMETROS :            Datos: Variable de la capa de negocios que contiene 
        ///                         los datos para la operacion
        /// CREO       :            Noe Mosqueda Valadez
        /// FECHA_CREO :            24/Noviembre/2010 16:25 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public static DataSet Imprime_Orden_Salida(Cls_Ope_Tal_Alm_Orden_Salida_Negocio Datos)
        {
            //Declaracion de variables
            DataSet Ds_Orden_Salida = new DataSet(); //Dataset que contiene los datos de la orden de salida
            DataTable Dt_Cabecera = new DataTable("Cabecera"); //Tabla para la cabecera
            DataTable Dt_Detalles = new DataTable("Detalles"); //Tabla para los detalles
            DataTable Dt_Cabecera_tmp = new DataTable("Cabecera"); //Tabla para la cabecera
            DataTable Dt_Detalles_tmp = new DataTable("Detalles"); //Tabla para los detalles
            String Mi_SQL = String.Empty; //Variable para las consultas
            DataRow Renglon; //Renglon para el llenado de las tablas

            try
            {
                //Asignar consulta para la cabecera
                Mi_SQL = "SELECT " + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + "." + Tal_Alm_Salidas.Campo_No_Salida + ", ";
                Mi_SQL = Mi_SQL + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DIRECCION, ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Codigo_Programatico + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Folio + " AS REQUISICION, ";
                Mi_SQL = Mi_SQL + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + "." + Tal_Alm_Salidas.Campo_Fecha_Creo + " AS FECHA " + ", ";
                Mi_SQL = Mi_SQL + "(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + "  +' '+ ";
                Mi_SQL = Mi_SQL + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + "  +' '+ ";
                Mi_SQL = Mi_SQL + "ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + ", '')) AS ENTREGADO, ";
                Mi_SQL = Mi_SQL + "(CAT_EMPLEADOS_2." + Cat_Empleados.Campo_Nombre + "  +' '+ ";
                Mi_SQL = Mi_SQL + "CAT_EMPLEADOS_2." + Cat_Empleados.Campo_Apellido_Paterno + "  +' '+ ";
                Mi_SQL = Mi_SQL + "ISNULL(CAT_EMPLEADOS_2." + Cat_Empleados.Campo_Apellido_Materno + ", '')) AS RESPONSABLE, (ROWNUM - ROWNUM) AS DESCUENTO, ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Subtotal + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_IVA + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_IEPS + ", ";
                Mi_SQL = Mi_SQL + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Total + " ";               
                Mi_SQL = Mi_SQL + "FROM " + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + ", " + Cat_Empleados.Tabla_Cat_Empleados + ", ";
                Mi_SQL = Mi_SQL + Cat_Empleados.Tabla_Cat_Empleados + " CAT_EMPLEADOS_2, " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + ", " + Cat_Dependencias.Tabla_Cat_Dependencias + " ";
                Mi_SQL = Mi_SQL + "WHERE " + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + "." + Tal_Alm_Salidas.Campo_Requisicion_ID;
                Mi_SQL = Mi_SQL + " = " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + "." + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " ";
                Mi_SQL = Mi_SQL + "AND " + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + "." + Tal_Alm_Salidas.Campo_Empleado_Solicito_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + " ";
                Mi_SQL = Mi_SQL + "AND " + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + "." + Tal_Alm_Salidas.Campo_Empleado_Almacen_ID;
                Mi_SQL = Mi_SQL + " = CAT_EMPLEADOS_2." + Cat_Empleados.Campo_Empleado_ID + " ";
                Mi_SQL = Mi_SQL + "AND " + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + "." + Tal_Alm_Salidas.Campo_Dependencia_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + " ";
                Mi_SQL = Mi_SQL + "AND " + Tal_Alm_Salidas.Tabla_Tal_Alm_Salidas + "." + Tal_Alm_Salidas.Campo_No_Salida + " = " + Datos.P_No_Orden_Salida.ToString().Trim() + " ";

                //Ejecutar consulta
                Dt_Cabecera_tmp = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Asignar consulta para los detalles
                Mi_SQL = "SELECT " + Tal_Alm_Salidas_Detalles.Tabla_Tal_Alm_Salidas_Detalles + "." + Tal_Alm_Salidas_Detalles.Campo_No_Salida + ", ";
                Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Clave + ", ";
                Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Nombre + " AS PRODUCTO, ";
                Mi_SQL = Mi_SQL + Tal_Alm_Salidas_Detalles.Tabla_Tal_Alm_Salidas_Detalles + "." + Tal_Alm_Salidas_Detalles.Campo_Cantidad + ", ";
                Mi_SQL = Mi_SQL + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + "." + Cat_Com_Unidades.Campo_Abreviatura + " AS UNIDADES, ";
                Mi_SQL = Mi_SQL + Tal_Alm_Salidas_Detalles.Tabla_Tal_Alm_Salidas_Detalles + "." + Tal_Alm_Salidas_Detalles.Campo_Costo + " AS PRECIO, ";
                Mi_SQL = Mi_SQL + Tal_Alm_Salidas_Detalles.Tabla_Tal_Alm_Salidas_Detalles + "." + Tal_Alm_Salidas_Detalles.Campo_Importe + " AS TOTAL, ";
                Mi_SQL = Mi_SQL + "'' AS MODELO,";
                Mi_SQL = Mi_SQL + "'' AS MARCA";
                //Mi_SQL = Mi_SQL + "( SELECT NOMBRE FROM " + Cat_Com_Modelos.Tabla_Cat_Com_Modelos + " WHERE MODELO_ID = ( SELECT MODELO_ID FROM ";
                //Mi_SQL = Mi_SQL + Cat_Com_Productos.Tabla_Cat_Com_Productos + " WHERE PRODUCTO_ID = ";
                //Mi_SQL = Mi_SQL + Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " )) AS MODELO, ";

                //Mi_SQL = Mi_SQL + "( SELECT NOMBRE FROM " + Cat_Com_Marcas.Tabla_Cat_Com_Marcas + " WHERE MARCA_ID = ( SELECT MARCA_ID FROM ";
                //Mi_SQL = Mi_SQL + Cat_Com_Productos.Tabla_Cat_Com_Productos + " WHERE PRODUCTO_ID = ";
                //Mi_SQL = Mi_SQL +  Alm_Com_Salidas_Detalles.Tabla_Alm_Com_Salidas_Detalles + "." + Alm_Com_Salidas_Detalles.Campo_Producto_ID + " )) AS MARCA  ";

                Mi_SQL = Mi_SQL + " FROM " + Tal_Alm_Salidas_Detalles.Tabla_Tal_Alm_Salidas_Detalles + ", " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + ", ";
                Mi_SQL = Mi_SQL + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + " ";
                Mi_SQL = Mi_SQL + " WHERE " + Tal_Alm_Salidas_Detalles.Tabla_Tal_Alm_Salidas_Detalles + "." + Tal_Alm_Salidas_Detalles.Campo_Refaccion_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Refaccion_ID + " ";

                Mi_SQL = Mi_SQL + "AND " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + "." + Cat_Tal_Refacciones.Campo_Unidad_ID;
                Mi_SQL = Mi_SQL + " = " + Cat_Com_Unidades.Tabla_Cat_Com_Unidades + "." + Cat_Com_Unidades.Campo_Unidad_ID + " ";
                Mi_SQL = Mi_SQL + "AND " + Tal_Alm_Salidas_Detalles.Tabla_Tal_Alm_Salidas_Detalles + "." + Tal_Alm_Salidas_Detalles.Campo_No_Salida;
                Mi_SQL = Mi_SQL + " = " + Datos.P_No_Orden_Salida.ToString().Trim() + " ";

                //Ejecutar consulta
                Dt_Detalles_tmp = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Clonar tablas
                Dt_Cabecera = Dt_Cabecera_tmp.Clone();
                Dt_Cabecera.TableName = "Cabecera";
                Dt_Detalles = Dt_Detalles_tmp.Clone();
                Dt_Detalles.TableName = "Detalles";

                //Llenar las tablas
                for (int Cont_Elementos = 0; Cont_Elementos < Dt_Cabecera_tmp.Rows.Count; Cont_Elementos++)
                {
                    //Instanciar e importar renglon
                    Renglon = Dt_Cabecera_tmp.Rows[Cont_Elementos];
                    Dt_Cabecera.ImportRow(Renglon);
                }

                for (int Cont_Elementos = 0; Cont_Elementos < Dt_Detalles_tmp.Rows.Count; Cont_Elementos++)
                {
                    //Instanciar e importar renglon
                    Renglon = Dt_Detalles_tmp.Rows[Cont_Elementos];
                    Dt_Detalles.ImportRow(Renglon);
                }

                //Colocar tablas en el dataset
                Ds_Orden_Salida.Tables.Add(Dt_Cabecera);
                Ds_Orden_Salida.Tables.Add(Dt_Detalles);

                //Entregar resultado
                return Ds_Orden_Salida;
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
    }
}