﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Operacion_Facturas_Servicio.Negocio;

namespace JAPAMI.Taller_Mecanico.Operacion_Facturas_Servicio.Datos
{
    public class Cls_Ope_Tal_Facturas_Servicio_Datos
    {
        #region [Metodos]
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Proveedor_Servicio     
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Proveedor_Servicio(Cls_Ope_Tal_Facturas_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            String Mi_SQL = "";
            Object Aux; //Variable auxiliar para las consultas
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Formar Sentencia para obtener el consecutivo
                Mi_SQL = "";
                Mi_SQL = "SELECT ISNULL(MAX(";
                Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + "),0)";
                Mi_SQL = Mi_SQL + " FROM ";
                Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;

                //Ejecutar consulta del consecutivo
                Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0][0];

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                {
                    Parametros.P_No_Asignacion_Proveedor = Convert.ToInt32(Aux) + 1;
                }
                else
                    Parametros.P_No_Asignacion_Proveedor = 1;
                Mi_SQL = "UPDATE " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " = 'RECHAZADO'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Usuario_Creo + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Fecha_Creo + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = '" + (Parametros.P_Tipo.Trim().Contains("PREV") ? "SERVICIO_PREVENTIVO" : "SERVICIO_CORRECTIVO") + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                //Insertar el nuevo proveedor
                Mi_SQL = "INSERT INTO " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;
                Mi_SQL = Mi_SQL + " (" + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Observaciones + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Usuario_Creo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Fecha_Creo + ")";
                Mi_SQL = Mi_SQL + " VALUES (" + Parametros.P_No_Asignacion_Proveedor + "";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Proveedor_ID + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Servicio + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Observaciones + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", GETDATE())";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Actualiza estatus de Servicio
                //Si es preventivo
                if (Parametros.P_Tipo.Contains("PREV"))
                {
                    Mi_SQL = "UPDATE " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = '" + Parametros.P_Estatus_Servicio + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                //Si es correctivo
                else if (Parametros.P_Tipo.Contains("CORR"))
                {
                    Mi_SQL = "UPDATE " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Correctivos.Campo_Estatus + " = '" + Parametros.P_Estatus_Servicio + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consulta_Proveedor_Servicio     
        ///DESCRIPCIÓN          : Consultar un registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consulta_Proveedor_Servicio(Cls_Ope_Tal_Facturas_Servicio_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT ";
                if (!String.IsNullOrEmpty(Parametros.P_Campos_Dinamicos))
                {
                    Mi_SQL = Mi_SQL + Parametros.P_Campos_Dinamicos;
                }
                else
                {
                    Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " .* ";
                }
                Mi_SQL = Mi_SQL + "," + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Compañia + " AS NOMBRE_PROVEEDOR ";
                if (!String.IsNullOrEmpty(Parametros.P_Campos_Dinamicos_Proveedor))
                {
                    Mi_SQL = Mi_SQL + Parametros.P_Campos_Dinamicos_Proveedor;
                }
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + " ON " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores + "." + Cat_Com_Proveedores.Campo_Proveedor_ID + " ";
                Mi_SQL = Mi_SQL + " = " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + "." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID;
                if (!String.IsNullOrEmpty(Parametros.P_Filtros_Dinamicos))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Parametros.P_Filtros_Dinamicos;
                }
                else
                {
                    if (Parametros.P_No_Servicio > 0 && !string.IsNullOrEmpty(Parametros.P_Tipo))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + "." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = " + Parametros.P_No_Servicio + " ";
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + "." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = '" + (Parametros.P_Tipo.Trim().Contains("PREV") ? "SERVICIO_PREVENTIVO" : "SERVICIO_CORRECTIVO") + "'";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + "." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN ('" + Parametros.P_Estatus.Trim() + "')";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Proveedor_ID))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + "." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + " IN ('" + Parametros.P_Proveedor_ID.Trim() + "')";
                    }

                    Mi_SQL = Mi_SQL + " ORDER BY 1 DESC";
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                    {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }
        #endregion
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Proveedor_Servicio     
        ///DESCRIPCIÓN          : Mofificar un registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Modificar_Proveedor_Servicio(Cls_Ope_Tal_Facturas_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            String Mi_SQL = "";
            Object Aux; //Variable auxiliar para las consultas
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                Mi_SQL = "UPDATE " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Diagnostico + " = '" + Parametros.P_Diagnostico.ToUpper() + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Observaciones + " = '" + Parametros.P_Observaciones.ToUpper() + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Costo + " = " + Parametros.P_Costo + "";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID + " = '" + Parametros.P_Proveedor_ID.Trim() + "'";
                //Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " = 'GENERADO'";
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = '" + (Parametros.P_Tipo.Trim().Contains("PREV") ? "SERVICIO_PREVENTIVO" : "SERVICIO_CORRECTIVO") + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                //Actualiza estatus de Servicio
                //Si es preventivo
                if (Parametros.P_Tipo.Contains("PREV"))
                {
                    Mi_SQL = "UPDATE " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = '" + Parametros.P_Estatus_Servicio + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                //Si es correctivo
                else if (Parametros.P_Tipo.Contains("CORR"))
                {
                    Mi_SQL = "UPDATE " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Correctivos.Campo_Estatus + " = '" + Parametros.P_Estatus_Servicio + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Asignaciones_Servicios
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 31/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Asignaciones_Servicios(Cls_Ope_Tal_Facturas_Servicio_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " AS NO_ASIGNACION_PROV_SERV";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " AS NO_SERVICIO";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " AS FOLIO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " AS FECHA_RECEPCION_REAL";
                Mi_SQL = Mi_SQL + ", 'SERVICIO CORRECTIVO' AS TIPO_SERVICIO";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " ASIGNACION";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Prov_Ent_Sal.Tabla_Ope_Tal_Prov_Ent_Sal + " ENT_SAL ON ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " = ENT_SAL." + Ope_Tal_Prov_Ent_Sal.Campo_No_Asignacion_Prov_Serv + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos + " SERVICIOS ON ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES ON SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                Mi_SQL = Mi_SQL + " WHERE SERVICIOS." + Ope_Tal_Serv_Correctivos.Campo_Estatus + " = 'ENTRADA_PROVEEDOR' AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = 'SERVICIO_CORRECTIVO'";
                Mi_SQL = Mi_SQL + " AND ENT_SAL." + Ope_Tal_Prov_Ent_Sal.Campo_Estatus + " = 'ABIERTA'";
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    Mi_SQL = Mi_SQL + " AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }

                Mi_SQL = Mi_SQL + " UNION ";

                Mi_SQL = Mi_SQL + "SELECT ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " AS NO_ASIGNACION_PROV_SERV";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " AS NO_SERVICIO";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " AS FOLIO_SOLICITUD";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Elaboracion + " AS FECHA_ELABORACION";
                Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " AS FECHA_RECEPCION_REAL";
                Mi_SQL = Mi_SQL + ", 'SERVICIO CORRECTIVO' AS TIPO_SERVICIO";
                Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                Mi_SQL = Mi_SQL + ", ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " AS ESTATUS";
                Mi_SQL = Mi_SQL + ", DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + "";
                Mi_SQL = Mi_SQL + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA";
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " ASIGNACION";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Prov_Ent_Sal.Tabla_Ope_Tal_Prov_Ent_Sal + " ENT_SAL ON ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion + " = ENT_SAL." + Ope_Tal_Prov_Ent_Sal.Campo_No_Asignacion_Prov_Serv + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos + " SERVICIOS ON ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio + " = SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES ON SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                Mi_SQL = Mi_SQL + " WHERE SERVICIOS." + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = 'ENTRADA_PROVEEDOR' AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio + " = 'SERVICIO_PREVENTIVO'";
                Mi_SQL = Mi_SQL + " AND ENT_SAL." + Ope_Tal_Prov_Ent_Sal.Campo_Estatus + " = 'ABIERTA'";
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    Mi_SQL = Mi_SQL + " AND ASIGNACION." + Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                }
                Mi_SQL = Mi_SQL + " ORDER BY NO_ASIGNACION_PROV_SERV";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Asignacion_Servicio
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un Objeto.
        ///PARAMETROS           : 
        ///                     1.Parametros.Contiene los parametros que se van a utilizar para
        ///                       hacer la consulta de la Base de Datos.
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 1/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Cls_Ope_Tal_Facturas_Servicio_Negocio Consultar_Detalles_Asignacion_Servicio(Cls_Ope_Tal_Facturas_Servicio_Negocio Parametros)
        {
            String Mi_SQL = null;
            Cls_Ope_Tal_Facturas_Servicio_Negocio Obj_Cargado = new Cls_Ope_Tal_Facturas_Servicio_Negocio();
            try
            {
                Mi_SQL = "SELECT * FROM " + Ope_Tal_Asignaion_Proveedor_Servicio.Tabla_Ope_Tal_Asignaion_Proveedor + " WHERE " + Ope_Tal_Prov_Ent_Sal.Campo_No_Asignacion_Prov_Serv + " = '" + Parametros.P_No_Asignacion_Proveedor + "'";

                SqlDataReader Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                while (Reader.Read())
                {
                    Obj_Cargado.P_No_Servicio = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Servicio]) : (-1);
                    Obj_Cargado.P_No_Asignacion_Proveedor = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_No_Asignacion]) : (-1);
                    Obj_Cargado.P_Proveedor_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID].ToString())) ? Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Proveedor_ID].ToString() : "";
                    Obj_Cargado.P_Observaciones = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Observaciones].ToString())) ? Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Observaciones].ToString() : "";
                    Obj_Cargado.P_Estatus = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus].ToString())) ? Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Estatus].ToString() : "";
                    Obj_Cargado.P_Tipo = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio].ToString())) ? Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Tipo_Servicio].ToString() : "";
                    Obj_Cargado.P_Diagnostico = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Diagnostico].ToString())) ? Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Diagnostico].ToString() : "";
                    Obj_Cargado.P_Costo = (!String.IsNullOrEmpty(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Costo].ToString())) ? Convert.ToDouble(Reader[Ope_Tal_Asignaion_Proveedor_Servicio.Campo_Costo]) : (-1.0);
                }
                Reader.Close();
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Obj_Cargado;
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Factura     
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros que se van a dar de
        ///                         Alta en la Base de Datos.
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 12/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Factura(Cls_Ope_Tal_Facturas_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            String Mi_SQL = "";
            Object Aux; //Variable auxiliar para las consultas
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            try
            {
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                //Formar Sentencia para obtener el consecutivo
                Mi_SQL = "";
                Mi_SQL = "SELECT ISNULL(MAX(";
                Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + "),0)";
                Mi_SQL = Mi_SQL + " FROM ";
                Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio;

                //Ejecutar consulta del consecutivo
                Cmd.CommandText = Mi_SQL;
                Aux = Cmd.ExecuteScalar();

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                {
                    Parametros.P_No_Factura_Servicio = Convert.ToInt32(Aux) + 1;
                }
                else
                    Parametros.P_No_Factura_Servicio = 1;



                //Insertar el nuevo registro
                Mi_SQL = "DELETE " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio;
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Facturas_Servicio.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio + " = '" + Parametros.P_Tipo + "'";
                Mi_SQL = Mi_SQL + " AND " + Ope_Tal_Facturas_Servicio.Campo_No_Asignacion + " = '" + Parametros.P_No_Asignacion_Proveedor + "'";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                foreach (DataRow Renglon_Factura in Parametros.P_Dt_Archivos.Rows)
                {
                    //Insertar el nuevo registro
                    Mi_SQL = "INSERT INTO " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio;
                    Mi_SQL = Mi_SQL + "( " + Ope_Tal_Facturas_Servicio.Campo_No_Factura_Servicio + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_No_Factura + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_No_Servicio + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_No_Asignacion + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_Monto_Factura + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_Total_Factura + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_Iva_Factura + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_Nombre_Archivo + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_Ruta_Archivo + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_Comentarios + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_Factura + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_Fecha_Factura + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_Proveedor_Id + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_Folio_Solicitud + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_Costo_Servicio + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_No_Reserva + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_Usuario_Creo + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Facturas_Servicio.Campo_Fecha_Creo + "";
                    Mi_SQL = Mi_SQL + ") VALUES (" + Parametros.P_No_Factura_Servicio + "";
                    Mi_SQL = Mi_SQL + ", " + Renglon_Factura["NUMERO_FACTURA"].ToString() + "";
                    Mi_SQL = Mi_SQL + ", " + Parametros.P_No_Servicio + "";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Asignacion_Proveedor + "'";
                    Mi_SQL = Mi_SQL + ", " + Renglon_Factura["MONTO_FACTURA"].ToString().Replace(",", "") + " ";
                    Mi_SQL = Mi_SQL + ", " + Renglon_Factura["TOTAL_FACTURA"].ToString().Replace(",", "") + " ";
                    Mi_SQL = Mi_SQL + ", " + Renglon_Factura["IVA_FACTURA"].ToString().Replace(",", "") + " ";
                    Mi_SQL = Mi_SQL + ", '" + Renglon_Factura["NOMBRE_ARCHIVO"].ToString() + "'";
                    Mi_SQL = Mi_SQL + ", '" + Renglon_Factura["RUTA_ARCHIVO"].ToString() + "'";
                    Mi_SQL = Mi_SQL + ", '" + Renglon_Factura["COMENTARIOS"].ToString() + "'";
                    Mi_SQL = Mi_SQL + ", '" + Renglon_Factura["FACTURA"].ToString() + "'";
                    Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Renglon_Factura["FECHA_FACTURA"])) + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Proveedor_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Folio_Solicitud + "'";
                    Mi_SQL = Mi_SQL + ", '" + Renglon_Factura["TOTAL_FACTURA"].ToString() + "'";
                    Mi_SQL = Mi_SQL + ", '" + Renglon_Factura["NO_RESERVA"].ToString() + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Parametros.P_No_Factura_Servicio = Parametros.P_No_Factura_Servicio + 1;

                }
                //Se valida si se va a cerrar el servicio
                if (Parametros.P_Cerrar_Servicio)
                {
                    if (Parametros.P_Tipo.Contains("PREV"))
                    {
                        Mi_SQL = "UPDATE " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                        Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = '" + Parametros.P_Estatus_Servicio + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Preventivos.Campo_Fecha_Modifico + " = GETDATE()";
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                    //Si es correctivo
                    else if (Parametros.P_Tipo.Contains("CORR"))
                    {
                        Mi_SQL = "UPDATE " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos;
                        Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Correctivos.Campo_Estatus + " = '" + Parametros.P_Estatus_Servicio + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Serv_Correctivos.Campo_Fecha_Modifico + " = GETDATE()";
                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                }
                
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consulta_Seguimiento_Proveedor     
        ///DESCRIPCIÓN          : Consultar un registro
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 31/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consulta_Factura(Cls_Ope_Tal_Facturas_Servicio_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT ";
                if (!String.IsNullOrEmpty(Parametros.P_Campos_Dinamicos))
                {
                    Mi_SQL = Mi_SQL + Parametros.P_Campos_Dinamicos;
                }
                else
                {
                    Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio + " .* ";
                }
                Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Facturas_Servicio.Tabla_Ope_Tal_Facturas_Servicio;
                if (!String.IsNullOrEmpty(Parametros.P_Filtros_Dinamicos))
                {
                    Mi_SQL = Mi_SQL + " WHERE " + Parametros.P_Filtros_Dinamicos;
                }
                else
                {
                    if (Parametros.P_No_Asignacion_Proveedor > 0)
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Servicio.Campo_No_Asignacion + " = " + Parametros.P_No_Asignacion_Proveedor + " ";
                    }
                    if (Parametros.P_No_Servicio > 0)
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Servicio.Campo_No_Servicio + " = " + Parametros.P_No_Servicio + " ";
                    }
                    if (!String.IsNullOrEmpty(Parametros.P_Tipo))
                    {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + Ope_Tal_Facturas_Servicio.Campo_Tipo_Servicio + " = '" + Parametros.P_Tipo + "' ";
                    }

                    Mi_SQL = Mi_SQL + " ORDER BY " + Ope_Tal_Facturas_Servicio.Campo_No_Factura + " ASC ";
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                    {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null)
                    {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Contrarecibo     
        ///DESCRIPCIÓN          : Carga el no. de Contrarecibo.
        ///PARAMETROS           : 
        ///                     1.  Parametros. Contiene los parametros de la capa de negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 31/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Cls_Ope_Tal_Facturas_Servicio_Negocio Crear_Contrarecibo(Cls_Ope_Tal_Facturas_Servicio_Negocio Parametros)
        {
            String Mensaje = "";
            String Mi_SQL = "";
            Object Aux; //Variable auxiliar para las consultas
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;

            try
            {
                if (Parametros.P_Cmmd != null)
                {
                    Cmd = Parametros.P_Cmmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Trans.Connection;
                    Cmd.Transaction = Trans;
                }
                //Formar Sentencia para obtener el consecutivo
                Mi_SQL = "";
                Mi_SQL = "SELECT ISNULL(MAX(";
                Mi_SQL = Mi_SQL + Ope_Tal_Solicitudes_Serv.Campo_No_Contrarecibo + "),0)";
                Mi_SQL = Mi_SQL + " FROM ";
                Mi_SQL = Mi_SQL + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;

                //Ejecutar consulta del consecutivo
                Cmd.CommandText = Mi_SQL;
                Aux = Cmd.ExecuteScalar();

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                {
                    Parametros.P_No_Contrarecibo = Convert.ToInt32(Aux) + 1;
                }
                else { Parametros.P_No_Contrarecibo = 1; }

                Mi_SQL = "UPDATE " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Solicitudes_Serv.Campo_No_Contrarecibo + " = '" + Parametros.P_No_Contrarecibo + "'";
                Mi_SQL = Mi_SQL + ",  " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Contrarecibo + " = GETDATE()";
                Mi_SQL = Mi_SQL + ",  " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud_Pago + " = '" + Parametros.P_No_Solicitud_Pago + "'";
                Mi_SQL = Mi_SQL + ",  " + Ope_Tal_Solicitudes_Serv.Campo_Monto_Sol_Pag + " = '" + Parametros.P_Monto_Solicitud_Pago + "'";
                Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                if (Parametros.P_Tipo.Trim().Equals("SERVICIO_PREVENTIVO"))
                {
                    Mi_SQL = "UPDATE " + Ope_Tal_Serv_Preventivos.Tabla_Ope_Tal_Serv_Preventivos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Preventivos.Campo_Estatus + " = 'PAGADO'";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Preventivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }
                else if (Parametros.P_Tipo.Trim().Equals("SERVICIO_CORRECTIVO"))
                {
                    Mi_SQL = "UPDATE " + Ope_Tal_Serv_Correctivos.Tabla_Ope_Tal_Serv_Correctivos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Serv_Correctivos.Campo_Estatus + " = 'PAGADO'";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Serv_Correctivos.Campo_No_Servicio + " = '" + Parametros.P_No_Servicio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                }

                if (Parametros.P_Cmmd == null)
                {
                    Trans.Commit();
                }

            }
            catch (SqlException Ex)
            {
                if (Parametros.P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Parametros.P_Cmmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
            return Parametros;
        }

    }
}