﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Taller_Refacciones.Negocio;

namespace JAPAMI.Catalogo_Taller_Refacciones.Datos
{
    public class Cls_Cat_Tal_Refacciones_Datos
    {        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Refaccion
        ///DESCRIPCIÓN: Da de alta en la Base de Datos un nuevo registro
        ///PARAMENTROS: Datos.- Instancia de la Clase de Negocio
        ///CREO: Jesus Toledo Rdz.
        ///FECHA_CREO: 05/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO 
        ///CAUSA_MODIFICACIÓN 
        ///*******************************************************************************
        public static void Alta_Refaccion(Cls_Cat_Tal_Refacciones_Negocio Datos)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Object Aux; //Variable auxiliar para las consultas
            String Mi_SQL = "";
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {

                //Formar Sentencia para obtener el consecutivo de la orden                
                Mi_SQL = "";
                Mi_SQL = "SELECT ISNULL(MAX(";
                Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Campo_Refaccion_ID + "),00000)";
                Mi_SQL = Mi_SQL + " FROM ";
                Mi_SQL = Mi_SQL + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
                
                //Ejecutar consulta del consecutivo
                Aux = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0][0];

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                {
                    Datos.P_Refaccion_ID = String.Format("{0:00000}", Convert.ToInt32(Aux) + 1);
                }
                else
                    Datos.P_Refaccion_ID = "00001";

                //Formar Sentencia para realizar la insercion de la refaccion               
                Mi_SQL = "";
                Mi_SQL = "INSERT INTO " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
                Mi_SQL = Mi_SQL + " (" + Cat_Tal_Refacciones.Campo_Refaccion_ID;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Clave;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Nombre;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Descripcion;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Estatus;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Tipo;
                if (!String.IsNullOrEmpty(Datos.P_Costo_Unitario)) Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Costo_Unitario;
                if (!String.IsNullOrEmpty(Datos.P_Unidad_ID)) Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Unidad_ID;
                if (!String.IsNullOrEmpty(Datos.P_Impuesto_ID)) Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Impuesto_ID;
                if (!String.IsNullOrEmpty(Datos.P_Impuesto_2_ID)) Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Impuesto_2_ID;
                if (!String.IsNullOrEmpty(Datos.P_Ubicacion)) Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Ubicacion;
                if (Datos.P_Maximo > -1) { Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Maximo; }
                if (Datos.P_Minimo > -1) { Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Minimo; }
                if (Datos.P_Reorden > -1) { Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Reorden; }
                if (Datos.P_Existencia > -1) { Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Existencia; }
                if (Datos.P_Disponible > -1) { Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Disponible; }
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Comprometido;
                if (!String.IsNullOrEmpty(Datos.P_Partida_ID)) { Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Partida_ID; }
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Tipo_Refaccion_ID;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Usuario_Creo;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Fecha_Creo;
                Mi_SQL = Mi_SQL + ") VALUES ('" + Datos.P_Refaccion_ID + "', '" + Convert.ToInt32(Datos.P_Refaccion_ID) +"'";
                Mi_SQL = Mi_SQL + ",'" + Datos.P_Nombre.ToUpper() + "'";
                Mi_SQL = Mi_SQL + ",'" + Datos.P_Descripcion.ToUpper() + "'";
                Mi_SQL = Mi_SQL + ",'" + Datos.P_Estatus.ToUpper() + "'";
                Mi_SQL = Mi_SQL + ",'" + Datos.P_Tipo.ToUpper() + "'";
                if (!String.IsNullOrEmpty(Datos.P_Costo_Unitario)) Mi_SQL = Mi_SQL + "," + Convert.ToDouble(Datos.P_Costo_Unitario) + "";
                if (!String.IsNullOrEmpty(Datos.P_Unidad_ID)) Mi_SQL = Mi_SQL + ",'" + Datos.P_Unidad_ID.ToUpper() + "'";
                if (!String.IsNullOrEmpty(Datos.P_Impuesto_ID)) Mi_SQL = Mi_SQL + ",'" + Datos.P_Impuesto_ID.ToUpper() + "'";
                if (!String.IsNullOrEmpty(Datos.P_Impuesto_2_ID)) Mi_SQL = Mi_SQL + ",'" + Datos.P_Impuesto_2_ID.ToUpper() + "'";
                if (!String.IsNullOrEmpty(Datos.P_Ubicacion)) Mi_SQL = Mi_SQL + ",'" + Datos.P_Ubicacion + "'";
                if (Datos.P_Maximo > -1) { Mi_SQL = Mi_SQL + ", '" + Datos.P_Maximo + "'"; }
                if (Datos.P_Minimo > -1) { Mi_SQL = Mi_SQL + ", '" + Datos.P_Minimo + "'"; }
                if (Datos.P_Reorden > -1) { Mi_SQL = Mi_SQL + ", '" + Datos.P_Reorden + "'"; }
                if (Datos.P_Existencia > -1) { Mi_SQL = Mi_SQL + ", '" + Datos.P_Existencia + "'"; }
                if (Datos.P_Disponible > -1) { Mi_SQL = Mi_SQL + ", '" + Datos.P_Disponible + "'"; }
                Mi_SQL = Mi_SQL + ", 0";
                if (!String.IsNullOrEmpty(Datos.P_Partida_ID)) { Mi_SQL = Mi_SQL + ", '" + Datos.P_Partida_ID + "'"; }
                Mi_SQL = Mi_SQL + ",'" + Datos.P_Tipo_Refaccion_ID + "'";
                Mi_SQL = Mi_SQL + ",'" + Cls_Sessiones.Nombre_Empleado.ToUpper() + "'";
                Mi_SQL = Mi_SQL + ", GETDATE()";                
                Mi_SQL = Mi_SQL + ")";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta un Registro. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Cn.State == ConnectionState.Open)
                {
                    Cn.Close();
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modificar_Refaccion
        ///DESCRIPCIÓN: Actualiza en la Base de Datos
        ///PARAMENTROS: Datos.- Instancia de la Clase de Negocio
        ///CREO: Jesus Toledo Rdz.
        ///FECHA_CREO: 05/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO 
        ///CAUSA_MODIFICACIÓN 
        ///*******************************************************************************
        public static void Modificar_Refaccion(Cls_Cat_Tal_Refacciones_Negocio Datos)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                String Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
                Mi_SQL = Mi_SQL + " SET " + Cat_Tal_Refacciones.Campo_Nombre + " = '" + Datos.P_Nombre.ToUpper() + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Refacciones.Campo_Descripcion + " = '" + Datos.P_Descripcion.ToUpper() + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Refacciones.Campo_Estatus + " = '" + Datos.P_Estatus.ToUpper() + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Refacciones.Campo_Tipo + " = '" + Datos.P_Tipo.ToUpper() + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Refacciones.Campo_Ubicacion + " = '" + Datos.P_Ubicacion + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Refacciones.Campo_Tipo_Refaccion_ID + " = '" + Datos.P_Tipo_Refaccion_ID + "'";
                if (!String.IsNullOrEmpty(Datos.P_Costo_Unitario)) Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Costo_Unitario +" = "+ Convert.ToDouble( Datos.P_Costo_Unitario);
                if (!String.IsNullOrEmpty(Datos.P_Unidad_ID)) Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Unidad_ID + " = '" + Datos.P_Unidad_ID + "'";
                if (Datos.P_Maximo > -1) { Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Maximo + " = '" + Datos.P_Maximo + "'"; }
                if (Datos.P_Minimo > -1) { Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Minimo + " = '" + Datos.P_Minimo + "'"; }
                if (Datos.P_Reorden > -1) { Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Reorden + " = '" + Datos.P_Reorden + "'"; }
                if (Datos.P_Existencia > -1) { 
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Existencia + " = '" + Datos.P_Existencia + "'";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Disponible + " = (" + Datos.P_Existencia + " - " + Cat_Tal_Refacciones.Campo_Comprometido + ")"; 
                }
                if (!String.IsNullOrEmpty(Datos.P_Partida_ID)) { Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Partida_ID + " = '" + Datos.P_Partida_ID + "'"; }
                if (!String.IsNullOrEmpty(Datos.P_Impuesto_ID)) { Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Impuesto_ID + " = '" + Datos.P_Impuesto_ID + "'"; }
                if (!String.IsNullOrEmpty(Datos.P_Impuesto_2_ID)) { Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Impuesto_2_ID + " = '" + Datos.P_Impuesto_2_ID + "'"; }

                Mi_SQL = Mi_SQL + "," + Cat_Tal_Refacciones.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado.ToUpper() + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Refacciones.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Datos.P_Refaccion_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar modificar un Registro. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Refaccion
        ///DESCRIPCIÓN: Consulta las refacciones en la Base de datos
        ///PARAMENTROS: Datos.- Instancia de la Clase de Negocio
        ///CREO: Jesus Toledo Rdz.
        ///FECHA_CREO: 05/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO 
        ///CAUSA_MODIFICACIÓN 
        ///*******************************************************************************
        public static DataTable Consultar_Refaccion(Cls_Cat_Tal_Refacciones_Negocio Datos)
        {
            DataTable tabla = new DataTable();
            if(Datos.P_Filtro.Trim().Length==0){
            try
            {
                String Mi_SQL = "SELECT " + Cat_Tal_Refacciones.Campo_Refaccion_ID;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Clave + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Nombre + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Estatus + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Tipo + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Costo_Unitario + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Unidad_ID + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Descripcion + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Partida_ID + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Impuesto_ID + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Impuesto_2_ID + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Existencia + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Minimo + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Comprometido + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Maximo + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Disponible + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Reorden + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Ubicacion + "";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Tipo_Refaccion_ID + "";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Tal_Refacciones.Campo_Refaccion_ID;
                DataSet dataset = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataset != null)
                {
                    tabla = dataset.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
        } else{
                    try
                    {
                        String Mi_SQL = "SELECT " + Cat_Tal_Refacciones.Campo_Refaccion_ID;
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Clave + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Nombre + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Estatus + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Tipo + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Costo_Unitario + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Unidad_ID + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Descripcion + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Partida_ID + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Impuesto_ID + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Impuesto_2_ID + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Existencia + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Minimo + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Comprometido + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Maximo + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Disponible + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Reorden + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Ubicacion + "";
                        Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Tipo_Refaccion_ID + "";
                        Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
                        Mi_SQL = Mi_SQL + " WHERE UPPER(" + Cat_Tal_Refacciones.Campo_Descripcion + ") LIKE UPPER('%" + Datos.P_Filtro + "%')";
                        Mi_SQL = Mi_SQL + " OR UPPER(" + Cat_Tal_Refacciones.Campo_Clave + ") LIKE UPPER('%" + Datos.P_Filtro + "%')";
                        Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Tal_Refacciones.Campo_Refaccion_ID;
                        DataSet dataset = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (dataset != null)
                        {
                            tabla = dataset.Tables[0];
                        }
                    }
                    catch (Exception Ex)
                    {
                        String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                        throw new Exception(Mensaje);
                    }
            }
            return tabla;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Refaccion
        ///DESCRIPCIÓN: Consulta las refacciones en la Base de datos
        ///PARAMENTROS: Datos.- Instancia de la Clase de Negocio
        ///CREO: Jesus Toledo Rdz.
        ///FECHA_CREO: 05/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO 
        ///CAUSA_MODIFICACIÓN 
        ///*******************************************************************************
        public static DataTable Consulta_Busqueda_Refacciones(Cls_Cat_Tal_Refacciones_Negocio Datos)
        {
            DataTable tabla = new DataTable();
            String Mi_SQL = "";
            try
            {
                if (!String.IsNullOrEmpty(Datos.P_Campos_Dinamicos))
                {
                    Mi_SQL = "SELECT " + Datos.P_Campos_Dinamicos;
                }
                else
                {
                    Mi_SQL = "SELECT " + Cat_Tal_Refacciones.Campo_Refaccion_ID;
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Clave + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Nombre + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Estatus + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Tipo + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Costo_Unitario + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Unidad_ID + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Descripcion + "";
                    Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Tipo_Refaccion_ID + "";
                }
            
            Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;

            if (!String.IsNullOrEmpty(Datos.P_Clave))
            {
                if (!Mi_SQL.Contains("WHERE"))                
                    Mi_SQL = Mi_SQL + " WHERE UPPER(" + Cat_Tal_Refacciones.Campo_Clave + ") LIKE UPPER('%" + Datos.P_Clave + "%')";
                else
                    Mi_SQL = Mi_SQL + " AND UPPER(" + Cat_Tal_Refacciones.Campo_Clave + ") LIKE UPPER('%" + Datos.P_Clave + "%')";
            }
            if (!String.IsNullOrEmpty(Datos.P_Nombre))
            {
                if (!Mi_SQL.Contains("WHERE"))
                    Mi_SQL = Mi_SQL + " WHERE UPPER(" + Cat_Tal_Refacciones.Campo_Nombre + ") LIKE UPPER('%" + Datos.P_Nombre + "%')";
                else
                    Mi_SQL = Mi_SQL + " AND UPPER(" + Cat_Tal_Refacciones.Campo_Nombre + ") LIKE UPPER('%" + Datos.P_Nombre + "%')";
            }
            if (!String.IsNullOrEmpty(Datos.P_Descripcion))
            {
                if (!Mi_SQL.Contains("WHERE"))
                    Mi_SQL = Mi_SQL + " WHERE UPPER(" + Cat_Tal_Refacciones.Campo_Descripcion + ") LIKE UPPER('%" + Datos.P_Descripcion + "%')";
                else
                    Mi_SQL = Mi_SQL + " AND UPPER(" + Cat_Tal_Refacciones.Campo_Descripcion + ") LIKE UPPER('%" + Datos.P_Descripcion + "%')";
            }
            if (!String.IsNullOrEmpty(Datos.P_Tipo))
            {
                if (!Mi_SQL.Contains("WHERE"))
                    Mi_SQL = Mi_SQL + " WHERE UPPER(" + Cat_Tal_Refacciones.Campo_Tipo + ") LIKE UPPER('%" + Datos.P_Tipo + "%')";
                else
                    Mi_SQL = Mi_SQL + " AND UPPER(" + Cat_Tal_Refacciones.Campo_Tipo + ") LIKE UPPER('%" + Datos.P_Tipo + "%')";
            }
            if (!String.IsNullOrEmpty(Datos.P_Estatus))
            {
                if (!Mi_SQL.Contains("WHERE"))
                    Mi_SQL = Mi_SQL + " WHERE UPPER(" + Cat_Tal_Refacciones.Campo_Estatus + ") LIKE UPPER('%" + Datos.P_Estatus + "%')";
                else
                    Mi_SQL = Mi_SQL + " AND UPPER(" + Cat_Tal_Refacciones.Campo_Estatus + ") LIKE UPPER('%" + Datos.P_Estatus + "%')";
            }

            Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Tal_Refacciones.Campo_Refaccion_ID;
            DataSet dataset = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            if (dataset != null)
            {
                tabla = dataset.Tables[0];
            }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return tabla;
        }        

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Datos
        ///DESCRIPCIÓN: Obtiene los datos de una refaccion
        ///PARAMENTROS: Instancia de la Clase de Negocio
        ///CREO: Jesus Toledo Rdz.
        ///FECHA_CREO: 05/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO 
        ///CAUSA_MODIFICACIÓN 
        ///*******************************************************************************
        public static Cls_Cat_Tal_Refacciones_Negocio Consultar_Datos(Cls_Cat_Tal_Refacciones_Negocio Datos)
        {
            Cls_Cat_Tal_Refacciones_Negocio Refaccion_Negocio = new Cls_Cat_Tal_Refacciones_Negocio();
            String Mi_SQL = "SELECT " + Cat_Tal_Refacciones.Campo_Refaccion_ID;
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Clave + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Nombre + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Estatus + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Tipo + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Costo_Unitario + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Unidad_ID + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Descripcion + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Partida_ID + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Impuesto_ID + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Impuesto_2_ID + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Existencia + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Minimo + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Comprometido + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Maximo + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Disponible + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Reorden + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Ubicacion + "";
            Mi_SQL = Mi_SQL + ", " + Cat_Tal_Refacciones.Campo_Tipo_Refaccion_ID + "";
            Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Datos.P_Refaccion_ID + "'";
            SqlDataReader Data_Reader;
            try
            {
                Data_Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                Refaccion_Negocio.P_Refaccion_ID = Datos.P_Refaccion_ID;
                while (Data_Reader.Read())
                {
                    Refaccion_Negocio.P_Refaccion_ID = Data_Reader[Cat_Tal_Refacciones.Campo_Refaccion_ID].ToString();
                    Refaccion_Negocio.P_Clave = Data_Reader[Cat_Tal_Refacciones.Campo_Clave].ToString();
                    Refaccion_Negocio.P_Nombre = Data_Reader[Cat_Tal_Refacciones.Campo_Nombre].ToString();
                    Refaccion_Negocio.P_Estatus = Data_Reader[Cat_Tal_Refacciones.Campo_Estatus].ToString();
                    Refaccion_Negocio.P_Tipo = Data_Reader[Cat_Tal_Refacciones.Campo_Tipo].ToString();
                    Refaccion_Negocio.P_Costo_Unitario = Data_Reader[Cat_Tal_Refacciones.Campo_Costo_Unitario].ToString();
                    Refaccion_Negocio.P_Unidad_ID = Data_Reader[Cat_Tal_Refacciones.Campo_Unidad_ID].ToString();
                    Refaccion_Negocio.P_Descripcion = Data_Reader[Cat_Tal_Refacciones.Campo_Descripcion].ToString();
                    Refaccion_Negocio.P_Partida_ID = (!String.IsNullOrEmpty(Data_Reader[Cat_Tal_Refacciones.Campo_Partida_ID].ToString())) ? Data_Reader[Cat_Tal_Refacciones.Campo_Partida_ID].ToString() : "";
                    Refaccion_Negocio.P_Impuesto_ID = (!String.IsNullOrEmpty(Data_Reader[Cat_Tal_Refacciones.Campo_Impuesto_ID].ToString())) ? Data_Reader[Cat_Tal_Refacciones.Campo_Impuesto_ID].ToString() : "";
                    Refaccion_Negocio.P_Impuesto_2_ID = (!String.IsNullOrEmpty(Data_Reader[Cat_Tal_Refacciones.Campo_Impuesto_2_ID].ToString())) ? Data_Reader[Cat_Tal_Refacciones.Campo_Impuesto_2_ID].ToString() : "";
                    Refaccion_Negocio.P_Maximo = (!String.IsNullOrEmpty(Data_Reader[Cat_Tal_Refacciones.Campo_Maximo].ToString())) ? Convert.ToInt32(Data_Reader[Cat_Tal_Refacciones.Campo_Maximo]) : -1;
                    Refaccion_Negocio.P_Minimo = (!String.IsNullOrEmpty(Data_Reader[Cat_Tal_Refacciones.Campo_Minimo].ToString())) ? Convert.ToInt32(Data_Reader[Cat_Tal_Refacciones.Campo_Minimo]) : -1;
                    Refaccion_Negocio.P_Reorden = (!String.IsNullOrEmpty(Data_Reader[Cat_Tal_Refacciones.Campo_Reorden].ToString())) ? Convert.ToInt32(Data_Reader[Cat_Tal_Refacciones.Campo_Reorden]) : -1;
                    Refaccion_Negocio.P_Existencia = (!String.IsNullOrEmpty(Data_Reader[Cat_Tal_Refacciones.Campo_Existencia].ToString())) ? Convert.ToInt32(Data_Reader[Cat_Tal_Refacciones.Campo_Existencia]) : -1;
                    Refaccion_Negocio.P_Comprometido = (!String.IsNullOrEmpty(Data_Reader[Cat_Tal_Refacciones.Campo_Comprometido].ToString())) ? Convert.ToInt32(Data_Reader[Cat_Tal_Refacciones.Campo_Comprometido]) : -1;
                    Refaccion_Negocio.P_Disponible = (!String.IsNullOrEmpty(Data_Reader[Cat_Tal_Refacciones.Campo_Disponible].ToString())) ? Convert.ToInt32(Data_Reader[Cat_Tal_Refacciones.Campo_Disponible]) : -1;
                    Refaccion_Negocio.P_Ubicacion = (!String.IsNullOrEmpty(Data_Reader[Cat_Tal_Refacciones.Campo_Ubicacion].ToString())) ? Data_Reader[Cat_Tal_Refacciones.Campo_Ubicacion].ToString() : "";
                    Refaccion_Negocio.P_Tipo_Refaccion_ID = (!String.IsNullOrEmpty(Data_Reader[Cat_Tal_Refacciones.Campo_Tipo_Refaccion_ID].ToString())) ? Data_Reader[Cat_Tal_Refacciones.Campo_Tipo_Refaccion_ID].ToString() : "";
                }
                Data_Reader.Close();
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar el registro. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Refaccion_Negocio;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Eliminar_Refaccion
        ///DESCRIPCIÓN: Elimina de la Base de Datos un registro
        ///PARAMENTROS: Datos.- Instancia de la Clase de Negocio
        ///CREO: Jesus Toledo Rdz.
        ///FECHA_CREO: 05/Mayo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO 
        ///CAUSA_MODIFICACIÓN 
        ///*******************************************************************************
        public static void Eliminar_Refaccion(Cls_Cat_Tal_Refacciones_Negocio Datos)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                String Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones;
                Mi_SQL = Mi_SQL + " SET " + Cat_Tal_Refacciones.Campo_Estatus + " = 'BAJA'";
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Refacciones.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "'";
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Refacciones.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Datos.P_Refaccion_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                if (Ex.Number == 547)
                {
                    Mensaje = "No se puede eliminar el registro, ya que está relacionado con datos. Error: [" + Ex.Message + "]"; ;
                }
                else
                {
                    Mensaje = "Error al intentar eliminar el registro. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                throw new Exception(Mensaje);
            }
            catch (Exception Ex)
            {
                Mensaje = "Error al intentar eliminar el registro. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
        ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
        ///PARÁMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID)
        {
            String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
            try
            {
                String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
                {
                    Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                }
            }
            catch (SqlException Ex)
            {
                new Exception(Ex.Message);
            }
            return Id;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
        ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
        ///PARÁMETROS:     
        ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
        ///             2. Longitud_ID. Longitud que tendra el ID. 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 10/Marzo/2010 
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
        {
            String Retornar = "";
            String Dato = "" + Dato_ID;
            for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
            {
                Retornar = Retornar + "0";
            }
            Retornar = Retornar + Dato;
            return Retornar;
        }
    }
}