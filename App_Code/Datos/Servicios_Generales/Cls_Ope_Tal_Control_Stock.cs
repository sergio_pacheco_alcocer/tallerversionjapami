﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Sessiones;

/// <summary>
/// Summary description for Cls_Ope_Tal_Control_Stock
/// </summary>
/// 
namespace JAPAMI.Taller_Mecanico.Control_Stock {
    public class Cls_Ope_Tal_Control_Stock
    {

        #region VARIABLES

            public static String COMPROMETER_PRODUCTO = "COMPROMETIDO";
            public static String DESCOMPROMETER_PRODUCTO = "DESCOMPROMETIDO";
            public static String SALIDA_PRODUCTO = "SALIDA";
            public static String ENTRADA_PRODUCTO = "ENTRADA";

        #endregion

        #region MÉTODOS

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Comprometer_Producto
            ///DESCRIPCIÓN: Compromete un producto de stock
            ///PARAMETROS: 1.-Producto_ID
            ///            2.-Cantidad
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 08/Nov/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptar a taller Mecanico
            ///*******************************************************************************
            public static Int32 Comprometer_Producto(String Refaccion_ID, Int32 Cantidad) {
                String Mi_SQL = "";
                int Registros = 0;
                try {
                    //SENTENCIA SQL PARA COMPROMETER
                    Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " SET " +
                        Cat_Tal_Refacciones.Campo_Comprometido + " = " + Cat_Tal_Refacciones.Campo_Comprometido + " + " + Cantidad + "," +
                        Cat_Tal_Refacciones.Campo_Disponible + " = " + Cat_Tal_Refacciones.Campo_Disponible + " - " + Cantidad +
                        " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Refaccion_ID + "'";
                    Registros = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                } catch (Exception Ex) {
                    Registros = 0;
                    throw new Exception(Ex.ToString());
                }
                return Registros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Comprometer_Producto
            ///DESCRIPCIÓN: Compromete varios productos de stock
            ///PARAMETROS: 1.-DataTable con los productos a comprometer
            ///            2.-Columna que contiene ID de los productos
            ///            3.-Columna que contiene la Cantidad de producto con la que se realizará
            ///               la operación
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 08/Nov/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptar a taller Mecanico
            ///*******************************************************************************
            public static Int32 Comprometer_Producto(DataTable Dt_Refacciones, String Nombre_Columna_ID, String Nombre_Columna_Cantidad) {
                String Mi_SQL = "";
                int Registros = 0;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try {
                    foreach (DataRow Dr_Refaccion in Dt_Refacciones.Rows) {
                        //SENTENCIA SQL PARA COMPROMETER
                        Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                            " SET " + Cat_Tal_Refacciones.Campo_Comprometido + " = " + Cat_Tal_Refacciones.Campo_Comprometido + " + " + Dr_Refaccion[Nombre_Columna_Cantidad].ToString()
                            + "," + Cat_Tal_Refacciones.Campo_Disponible + " = " + Cat_Tal_Refacciones.Campo_Disponible + " - " + Dr_Refaccion[Nombre_Columna_Cantidad].ToString() +
                            " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Dr_Refaccion[Nombre_Columna_ID].ToString() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Registros = Cmd.ExecuteNonQuery();
                    }
                    Trans.Commit();
                } catch (Exception Ex) {
                    Trans.Rollback();                                              
                    throw new Exception(Ex.ToString());
                } finally {
                    Cn.Close();
                }
                return Registros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Desomprometer_Producto
            ///DESCRIPCIÓN: Descompromete un producto de stock
            ///PARAMETROS: 1.-Producto_ID
            ///            2.-Cantidad
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 08/Nov/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptar a taller Mecanico
            ///*******************************************************************************
            public static Int32 Descomprometer_Producto(String Refaccion_ID, Int32 Cantidad) {
                String Mi_SQL = "";
                int Registros = 0;
                try {
                    //SENTENCIA SQL PARA DESCOMPROMETER
                    Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                        " SET " + Cat_Tal_Refacciones.Campo_Comprometido + " = " + Cat_Tal_Refacciones.Campo_Comprometido + " - " + Cantidad
                        + "," + Cat_Tal_Refacciones.Campo_Disponible + " = " + Cat_Tal_Refacciones.Campo_Disponible + " + " + Cantidad +
                        " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Refaccion_ID + "'";
                    Registros = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                } catch (Exception Ex) {
                    Registros = 0;
                    throw new Exception(Ex.ToString());
                }
                return Registros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Descomprometer_Producto
            ///DESCRIPCIÓN: Descompromete varios productos de stock
            ///PARAMETROS: 1.-DataTable con los productos a comprometer
            ///            2.-Columna que contiene ID de los productos
            ///            3.-Columna que contiene la Cantidad de producto con la que se realizará
            ///               la operación
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 08/Nov/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptar a taller Mecanico
            ///*******************************************************************************
            public static Int32 Descomprometer_Producto(DataTable Dt_Refacciones, String Nombre_Columna_ID, String Nombre_Columna_Cantidad) {
                String Mi_SQL = "";
                int Registros = 0;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try {
                    foreach (DataRow Dr_Refaccion in Dt_Refacciones.Rows) {
                        //SENTENCIA SQL PARA DESCOMPROMETER
                        Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                            " SET " + Cat_Tal_Refacciones.Campo_Comprometido + " = " + Cat_Tal_Refacciones.Campo_Comprometido + " - " + Dr_Refaccion[Nombre_Columna_Cantidad].ToString()
                            + "," + Cat_Tal_Refacciones.Campo_Disponible + " = " + Cat_Tal_Refacciones.Campo_Disponible + " + " + Dr_Refaccion[Nombre_Columna_Cantidad].ToString() +
                            " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Dr_Refaccion[Nombre_Columna_ID].ToString() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Registros = Cmd.ExecuteNonQuery();
                    }
                    Trans.Commit();
                } catch (Exception Ex) {
                    Trans.Rollback();
                    throw new Exception(Ex.ToString());
                } finally {
                    Cn.Close();
                }
                return Registros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Entrada_Producto
            ///DESCRIPCIÓN: Da entrada de existencia a un solo producto
            ///PARAMETROS: 1.-Producto_ID
            ///            2.-Cantidad
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 08/Nov/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptar a taller Mecanico
            ///*******************************************************************************
            public static Int32 Entrada_Producto(String Refaccion_ID, Int32 Cantidad) {
                String Mi_SQL = "";
                int Registros = 0;
                try {
                    //SENTENCIA SQL PARA ENTRADA
                    Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                        " SET " + Cat_Tal_Refacciones.Campo_Existencia + " = " + Cat_Tal_Refacciones.Campo_Existencia + " + " + Cantidad +
                        "," + Cat_Tal_Refacciones.Campo_Disponible + " = " + Cat_Tal_Refacciones.Campo_Disponible + " + " + Cantidad +
                        " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Refaccion_ID + "'";
                    Registros = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                } catch (Exception Ex) {
                    Registros = 0;
                    throw new Exception(Ex.ToString());
                }
                return Registros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Entrada_Producto
            ///DESCRIPCIÓN: Da entrada a varios productos de stock
            ///PARAMETROS: 1.-DataTable con los productos 
            ///            2.-Columna que contiene ID de los productos
            ///            3.-Columna que contiene la Cantidad de producto con la que se realizará
            ///               la operación
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 08/Nov/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptar a taller Mecanico
            ///*******************************************************************************
            public static Int32 Entrada_Producto(DataTable Dt_Refacciones, String Nombre_Columna_ID, String Nombre_Columna_Cantidad) {
                String Mi_SQL = "";
                Int32 Registros = 0;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try {
                    foreach (DataRow Dr_Refaccion in Dt_Refacciones.Rows) {
                        //SENTENCIA SQL PARA DAR ENTRADAS
                        Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                            " SET " + Cat_Tal_Refacciones.Campo_Comprometido + " = " + Cat_Tal_Refacciones.Campo_Existencia + " + " + Dr_Refaccion[Nombre_Columna_Cantidad].ToString() +
                            "," + Cat_Tal_Refacciones.Campo_Disponible + " = " + Cat_Tal_Refacciones.Campo_Disponible + " + " + Dr_Refaccion[Nombre_Columna_Cantidad].ToString() +
                            " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Dr_Refaccion[Nombre_Columna_ID].ToString() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Registros = Cmd.ExecuteNonQuery();
                    }
                    Trans.Commit();
                } catch (Exception Ex) {
                    Trans.Rollback();
                    throw new Exception(Ex.ToString());
                } finally {
                    Cn.Close();
                }
                return Registros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Salida_Producto
            ///DESCRIPCIÓN: Da salida a un solo producto
            ///PARAMETROS: 1.-Producto_ID
            ///            2.-Cantidad
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 08/Nov/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptar a taller Mecanico
            ///*******************************************************************************
            public static Int32 Salida_Producto(String Refaccion_ID, Int32 Cantidad) {
                String Mi_SQL = "";
                int Registros = 0;
                try {
                    //SENTENCIA SQL PARA SALIDA
                    Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                        " SET " + Cat_Tal_Refacciones.Campo_Existencia + " = " + Cat_Tal_Refacciones.Campo_Existencia + " - " + Cantidad +
                        "," + Cat_Tal_Refacciones.Campo_Disponible + " = " + Cat_Tal_Refacciones.Campo_Disponible + " - " + Cantidad +
                        " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Refaccion_ID + "'";
                    Registros = SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                } catch (Exception Ex) {
                    Registros = 0;
                    throw new Exception(Ex.ToString());
                }
                return Registros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Salida_Producto
            ///DESCRIPCIÓN: Da salida a varios productos de stock
            ///PARAMETROS: 1.-DataTable con los productos 
            ///            2.-Columna que contiene ID de los productos
            ///            3.-Columna que contiene la Cantidad de producto con la que se realizará
            ///               la operación
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 08/Nov/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptar a taller Mecanico
            ///*******************************************************************************
            public static Int32 Salida_Producto(DataTable Dt_Refacciones, String Nombre_Columna_ID, String Nombre_Columna_Cantidad) {
                String Mi_SQL = "";
                int Registros = 0;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try {
                    foreach (DataRow Dr_Refaccion in Dt_Refacciones.Rows) {
                        //SENTENCIA SQL PARA DAR ENTRADAS
                        Mi_SQL = "UPDATE " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                            " SET " + Cat_Tal_Refacciones.Campo_Comprometido + " = " + Cat_Tal_Refacciones.Campo_Existencia + " - " + Dr_Refaccion[Nombre_Columna_Cantidad].ToString() +
                            "," + Cat_Tal_Refacciones.Campo_Disponible + " = " + Cat_Tal_Refacciones.Campo_Disponible + " - " + Dr_Refaccion[Nombre_Columna_Cantidad].ToString() +
                            " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Dr_Refaccion[Nombre_Columna_ID].ToString() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Registros = Cmd.ExecuteNonQuery();
                    }
                    Trans.Commit();
                } catch (Exception Ex) {
                    Trans.Rollback();
                    throw new Exception(Ex.ToString());
                } finally {
                    Cn.Close();
                }
                return Registros;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_EX_DISP_CMP
            ///DESCRIPCIÓN: Consulta existencia, disponible y comprometido de un producto
            /// Devuelve DataTable
            ///PARAMETROS: 1.-Producto_ID
            ///            2.-Cantidad
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 08/Nov/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptar a taller Mecanico
            ///*******************************************************************************
            public static DataTable Consultar_EX_DISP_CMP(String Refaccion_ID) {
                String Mi_SQL = "";
                DataTable _DataTable = null;
                try {
                    //SENTENCIA SQL PARA consultar
                    Mi_SQL = "SELECT " + 
                        Cat_Tal_Refacciones.Campo_Existencia + ", " +
                        Cat_Tal_Refacciones.Campo_Disponible + ", " +
                        Cat_Tal_Refacciones.Campo_Comprometido + " FROM " +
                        Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                        " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Refaccion_ID + "'";
                    _DataTable = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                } catch (Exception Ex) {
                    _DataTable = null;
                    throw new Exception(Ex.ToString());
                }
                return _DataTable;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_EX_DISP_CMP_Arreglo
            ///DESCRIPCIÓN: Consulta existencia, disponible y comprometido de un producto
            /// Devuelve DataTable
            ///PARAMETROS: 1.-Producto_ID
            ///            2.-Cantidad
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 08/Nov/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptar a taller Mecanico
            ///*******************************************************************************
            public static Int32 [] Consultar_EX_DISP_CMP_Arreglo(String Refaccion_ID) {
                String Mi_SQL = "";
                Int32[] Arreglo = new Int32[3];
                try {
                    //SENTENCIA SQL PARA consultar
                    Mi_SQL = "SELECT " +
                        Cat_Tal_Refacciones.Campo_Existencia + ", " +
                        Cat_Tal_Refacciones.Campo_Disponible + ", " +
                        Cat_Tal_Refacciones.Campo_Comprometido + " FROM " +
                        Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                        " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Refaccion_ID + "'";

                    DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (Ds_Datos != null) {
                        if (Ds_Datos.Tables.Count > 0) {
                            DataTable Dt_Datos = Ds_Datos.Tables[0];
                            if (Dt_Datos != null) {
                                if (Dt_Datos.Rows.Count > 0) {
                                    Arreglo[0] = int.Parse(Dt_Datos.Rows[0]["EXISTENCIA"].ToString());
                                    Arreglo[1] = int.Parse(Dt_Datos.Rows[0]["DISPONIBLE"].ToString());
                                    Arreglo[2] = int.Parse(Dt_Datos.Rows[0]["COMPROMETIDO"].ToString());                                    
                                }
                            }
                        }
                    }
                } catch (Exception Ex) {
                    throw new Exception(Ex.ToString());
                }
                return Arreglo;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Existencia
            ///DESCRIPCIÓN: Consulta existencia
            ///PARAMETROS: 1.-Producto_ID
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 08/Nov/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptar a taller Mecanico
            ///*******************************************************************************
            public static Int32 Consultar_Existencia(String Refaccion_ID) {
                String Mi_SQL = "";
                Int32 Numero = 0;
                try {
                    //SENTENCIA SQL PARA CONSULTAR
                    Mi_SQL = "SELECT " +
                        Cat_Tal_Refacciones.Campo_Existencia + " FROM " +
                        Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                        " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Refaccion_ID + "'";
                    Object _Object = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (_Object != null) {
                        Numero = int.Parse(_Object.ToString());
                    }
                } catch (Exception Ex) {                
                    throw new Exception(Ex.ToString());
                }
                return Numero;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Disponible
            ///DESCRIPCIÓN: Consulta disponible de un producto
            ///PARAMETROS: 1.-Producto_ID
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 08/Nov/2011
            ///MODIFICO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_MODIFICO: 16 / Julio / 2012
            ///CAUSA_MODIFICACIÓN: Adaptar a taller Mecanico
            ///*******************************************************************************
            public static Int32 Consultar_Disponible(String Refaccion_ID) {
                String Mi_SQL = "";
                int Numero = 0;
                try {
                    //SENTENCIA SQL PARA CONSULTAR
                    Mi_SQL = "SELECT " +
                        Cat_Tal_Refacciones.Campo_Disponible + " FROM " +
                        Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                        " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Refaccion_ID + "'";
                    Object _Object = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (_Object != null) {
                        Numero = int.Parse(_Object.ToString());
                    }
                } catch (Exception Ex) {
                    throw new Exception(Ex.ToString());
                }
                return Numero;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Comprometido
            ///DESCRIPCIÓN: Consulta Comprometido de un producto
            ///PARAMETROS: 1.-Producto_ID
            ///CREO: Gustavo Angeles Cruz
            ///FECHA_CREO: 08/Nov/2011
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            public static Int32 Consultar_Comprometido(String Refaccion_ID) {
                String Mi_SQL = "";
                int Numero = 0;
                try {
                    //SENTENCIA SQL PARA CONSULTAR
                    Mi_SQL = "SELECT " + Cat_Tal_Refacciones.Campo_Comprometido + " FROM " +
                        Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones +
                        " WHERE " + Cat_Tal_Refacciones.Campo_Refaccion_ID + " = '" + Refaccion_ID + "'";
                    Object _Object = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (_Object != null) {
                        Numero = int.Parse(_Object.ToString());
                    }
                } catch (Exception Ex) {
                    throw new Exception(Ex.ToString());
                }
                return Numero;
            }

        #endregion

	}
}
