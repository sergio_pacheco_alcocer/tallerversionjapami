﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Archivos_Servicios.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Cls_Ope_Tal_Archivos_Servicios_Datos
/// </summary>
namespace JAPAMI.Taller_Mecanico.Operacion_Archivos_Servicios.Datos
{
    public class Cls_Ope_Tal_Archivos_Servicios_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Imagenes_Servicio
        ///DESCRIPCIÓN          : Inserta el nombre y estatus de los archivos subidos en las etapas del proceso de servicio
        ///PARAMETROS           : 
        ///                     1.Parametros. Contiene los parametros que se van hacer la
        ///                       Modificación en la Base de Datos.
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 15/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Imagenes_Servicio(Cls_Ope_Tal_Archivos_Servicios_Negocio Datos)
        {
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlDataAdapter Adaptador = new SqlDataAdapter();
            SqlTransaction Trans = null;
            Object Aux = null;
            String Mi_SQL = "";
            String Mensaje = "";
            try
            {
                if (Datos.P_Cmd != null)
                {
                    Cmd = Datos.P_Cmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Cn;
                    Cmd.Transaction = Trans;
                }
                //Eliminamos los registros Anteriores
                Mi_SQL = "";
                Mi_SQL += "DELETE FROM " + Ope_Tal_Archivos_Servicios.Tabla_Ope_Tal_Archivos_Servicios;
                Mi_SQL += " WHERE " + Ope_Tal_Archivos_Servicios.Campo_No_Solicitud;
                Mi_SQL += " = " + Datos.P_No_Solicitud;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                //Insertamos las nuevas imagenes
                //Obtenemos el siuiente Indice de la tabla
                Mi_SQL = "";
                Mi_SQL += "SELECT ISNULL(MAX(" + Ope_Tal_Archivos_Servicios.Campo_Imagen_Servicio_ID;
                Mi_SQL += "),0) FROM " + Ope_Tal_Archivos_Servicios.Tabla_Ope_Tal_Archivos_Servicios;
                Cmd.CommandText = Mi_SQL;
                Aux = Cmd.ExecuteScalar();
                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                {
                    Datos.P_Imagen_Servicio_ID = Convert.ToInt32(Aux) + 1;
                }
                else
                    Datos.P_Imagen_Servicio_ID = 1;
                foreach (DataRow Dr_Renglon in Datos.P_Dt_Imagenes_Servicio.Rows)
                {
                    //Limpiamos Variable de consulta
                    Mi_SQL = "";
                    Mi_SQL += "INSERT INTO " + Ope_Tal_Archivos_Servicios.Tabla_Ope_Tal_Archivos_Servicios;
                    Mi_SQL += "( " + Ope_Tal_Archivos_Servicios.Campo_Imagen_Servicio_ID;
                    Mi_SQL += ", " + Ope_Tal_Archivos_Servicios.Campo_No_Solicitud;
                    Mi_SQL += ", " + Ope_Tal_Archivos_Servicios.Campo_Estatus;
                    Mi_SQL += ", " + Ope_Tal_Archivos_Servicios.Campo_Nombre_Archivo;
                    Mi_SQL += ", " + Ope_Tal_Archivos_Servicios.Campo_Usuario_Creo;
                    Mi_SQL += ", " + Ope_Tal_Archivos_Servicios.Campo_Fecha_Creo + " )";

                    Mi_SQL += " VALUES( " + Datos.P_Imagen_Servicio_ID + "," + Datos.P_No_Solicitud + ",'" + Dr_Renglon["ESTATUS"].ToString() + "','" + Dr_Renglon["NOMBRE_ARCHIVO"].ToString()
                        + "','" + Dr_Renglon["EMPLEADO"].ToString() + "', '" + string.Format("{0:dd/MM/yyyy hh:mm:ss}", Dr_Renglon["FECHA_ARCHIVO"]) + "')";
                    //Se ejecuta instruccion
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Datos.P_Imagen_Servicio_ID++;
                }
                if (Datos.P_Cmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Datos.P_Cmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consulta_Imagenes_Servicio
        ///DESCRIPCIÓN          : Inserta el nombre y estatus de los archivos subidos en las etapas del proceso de servicio
        ///PARAMETROS           : 
        ///                     1.Parametros. Contiene los parametros que se van hacer la
        ///                       Modificación en la Base de Datos.
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 15/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consulta_Imagenes_Servicio(Cls_Ope_Tal_Archivos_Servicios_Negocio Datos)
        {
            DataTable Dt_Resultado_Consulta = new DataTable();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlDataAdapter Adaptador = new SqlDataAdapter();
            SqlTransaction Trans = null;
            Object Aux = null;
            String Mi_SQL = "";
            String Mensaje = "";
            try
            {
                if (Datos.P_Cmd != null)
                {
                    Cmd = Datos.P_Cmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Cn;
                    Cmd.Transaction = Trans;
                }
                //Formamos la consulta
                Mi_SQL = "SELECT " + Ope_Tal_Archivos_Servicios.Campo_Imagen_Servicio_ID + " IMAGEN_SERVICIO_ID ";
                Mi_SQL += ",'' CHECKSUM";
                Mi_SQL += ", " + Ope_Tal_Archivos_Servicios.Campo_Fecha_Creo + " FECHA_ARCHIVO ";
                Mi_SQL += ", " + Ope_Tal_Archivos_Servicios.Campo_Estatus + " ESTATUS ";
                Mi_SQL += ", " + Ope_Tal_Archivos_Servicios.Campo_Usuario_Creo + " EMPLEADO ";
                Mi_SQL += ", " + Ope_Tal_Archivos_Servicios.Campo_Nombre_Archivo + " NOMBRE_ARCHIVO ";
                Mi_SQL += ", '../../ARCHIVOS_TALLER_MUNICIPAL/FOTOS_SERVICIOS/' + " + Ope_Tal_Archivos_Servicios.Campo_Nombre_Archivo + " RUTA_ARCHIVO ";
                Mi_SQL += " FROM " + Ope_Tal_Archivos_Servicios.Tabla_Ope_Tal_Archivos_Servicios;
                Mi_SQL += " WHERE " + Ope_Tal_Archivos_Servicios.Campo_No_Solicitud + "=" + Datos.P_No_Solicitud;

                Cmd.CommandText = Mi_SQL;
                Adaptador.SelectCommand = Cmd;
                Adaptador.Fill(Dt_Resultado_Consulta);

            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Datos.P_Cmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
            return Dt_Resultado_Consulta;
        }

        public static void Eliminar_Imagen(Cls_Ope_Tal_Archivos_Servicios_Negocio Datos)
        {
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlDataAdapter Adaptador = new SqlDataAdapter();
            SqlTransaction Trans = null;
            Object Aux = null;
            String Mi_SQL = "";
            String Mensaje = "";
            try
            {
                if (Datos.P_Cmd != null)
                {
                    Cmd = Datos.P_Cmd;
                }
                else
                {
                    Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                    Cn.Open();
                    Trans = Cn.BeginTransaction();
                    Cmd.Connection = Cn;
                    Cmd.Transaction = Trans;
                }
                //Eliminamos los registros Anteriores
                Mi_SQL = "";
                Mi_SQL += "DELETE FROM " + Ope_Tal_Archivos_Servicios.Tabla_Ope_Tal_Archivos_Servicios;
                Mi_SQL += " WHERE " + Ope_Tal_Archivos_Servicios.Campo_Imagen_Servicio_ID;
                Mi_SQL += " = " + Datos.P_Imagen_Servicio_ID;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                if (Datos.P_Cmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (Datos.P_Cmd == null)
                {
                    Trans.Rollback();
                }
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                if (Datos.P_Cmd == null)
                {
                    Cn.Close();
                    Cn = null;
                    Cmd = null;
                    Trans = null;
                }
            }
        }
    }
}
