﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Solicitud_Servicio.Datos;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Preventivos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Servicios_Correctivos.Negocio;
using JAPAMI.Taller_Mecanico.Operacion_Revista_Mecanica.Negocio;
/// <summary>
/// Summary description for Cls_Ope_Tal_Entradas_Vehiculos_Datos
/// </summary>

namespace JAPAMI.Taller_Mecanico.Operacion_Entradas_Vehiculos.Datos {
    public class Cls_Ope_Tal_Entradas_Vehiculos_Datos {

        #region Metodos

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Entrada_Vehiculo      
            ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a dar de
            ///                         Alta en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 07/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Int32 Alta_Entrada_Vehiculo(Cls_Ope_Tal_Entradas_Vehiculos_Negocio Parametros) { 
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Int32 No_Servicio = 0;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                Parametros.P_No_Entrada = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Entradas_Vehiculos.Tabla_Ope_Tal_Entradas_Vehiculos, Ope_Tal_Entradas_Vehiculos.Campo_No_Entrada, 20));
                try {
                    String Mi_SQL = "INSERT INTO " + Ope_Tal_Entradas_Vehiculos.Tabla_Ope_Tal_Entradas_Vehiculos;
                    Mi_SQL = Mi_SQL + " (" + Ope_Tal_Entradas_Vehiculos.Campo_No_Entrada + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Tipo_Entrada + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Entrada + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Estatus + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Empleado_Recibio_ID + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Empleado_Entrego_ID + "";
                    if (Parametros.P_Kilometraje > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Kilometraje + "";
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Comentarios + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Usuario_Creo + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Creo + ")";
                    Mi_SQL = Mi_SQL + " VALUES ('" + Parametros.P_No_Entrada + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo_Entrada + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Vehiculo_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Entrada) + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Solicitud + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Empleado_Recibio_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Empleado_Entrego_ID + "'";
                    if (Parametros.P_Kilometraje > (-1)) {
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Kilometraje + "'";
                    }
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Comentarios + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Ent_Veh_Det.Tabla_Ope_Tal_Ent_Veh_Det, Ope_Tal_Ent_Veh_Det.Campo_No_Registro, 20));
                    foreach (DataRow Fila_Actual in Parametros.P_Dt_Detalles.Rows) {
                        Mi_SQL = "INSERT INTO " + Ope_Tal_Ent_Veh_Det.Tabla_Ope_Tal_Ent_Veh_Det;
                        Mi_SQL = Mi_SQL + " (" + Ope_Tal_Ent_Veh_Det.Campo_No_Registro + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Det.Campo_No_Entrada + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Det.Campo_Parte_ID + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Det.Campo_SubParte_ID + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Det.Campo_Valor + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Det.Campo_Usuario_Creo + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Det.Campo_Fecha_Creo + ")";
                        Mi_SQL = Mi_SQL + " VALUES ('" + No_Registro + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Entrada + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila_Actual["PARTE_ID"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila_Actual["SUBPARTE_ID"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila_Actual["VALOR"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        No_Registro = No_Registro + 1;
                    }
                    Int32 Numero_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Ent_Veh_Fotos.Tabla_Ope_Tal_Ent_Veh_Fotos, Ope_Tal_Ent_Veh_Fotos.Campo_No_Registro, 20));
                    //Registrar Fotos o imagenes de Automovil al Realizar la Entrada
                    foreach (DataRow Fila_Actual in Parametros.P_Dt_Archivos.Rows)
                    {                        
                        Mi_SQL = "INSERT INTO " + Ope_Tal_Ent_Veh_Fotos.Tabla_Ope_Tal_Ent_Veh_Fotos;
                        Mi_SQL = Mi_SQL + " (" + Ope_Tal_Ent_Veh_Fotos.Campo_No_Registro + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Fotos.Campo_No_Entrada + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Fotos.Campo_Tipo_Foto + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Fotos.Campo_Ruta_Archivo + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Fotos.Campo_Usuario_Creo + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Fotos.Campo_Fecha_Creo + ")";
                        Mi_SQL = Mi_SQL + " VALUES ('" + Numero_Registro + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Entrada + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila_Actual["FOTO"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila_Actual["RUTA_ARCHIVO"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        Numero_Registro = Numero_Registro + 1;
                    }


                    Mi_SQL = "UPDATE " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " = 'EN_REPARACION'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " = '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Entrada) + "'";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                    Solicitud.P_No_Solicitud = Parametros.P_No_Solicitud;
                    Solicitud.P_Estatus = "RECEPCION";
                    Solicitud.P_Empleado_Solicito_ID = Parametros.P_Empleado_Recibio_ID;
                    Solicitud.P_Usuario = Parametros.P_Usuario;
                    Cls_Ope_Tal_Solicitud_Servicio_Datos.Alta_Registro_Seguimiento_Solicitud(Solicitud, ref Cmd);

                    if (!String.IsNullOrEmpty(Parametros.P_Tipo_Solicitud)) {
                        if (Parametros.P_Tipo_Solicitud.Trim().Equals("SERVICIO_PREVENTIVO")) {
                            Cls_Ope_Tal_Servicios_Preventivos_Negocio Serv_Prev = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
                            Serv_Prev.P_No_Entrada = Parametros.P_No_Entrada;
                            Serv_Prev.P_Mecanico_ID = "";
                            Serv_Prev.P_Estatus = "PENDIENTE";
                            Serv_Prev.P_No_Solicitud = Parametros.P_No_Solicitud;
                            Serv_Prev.P_Tipo_Bien = Parametros.P_Tipo_Bien;
                            Serv_Prev.P_Usuario = Parametros.P_Usuario;
                            No_Servicio = Serv_Prev.Alta_Servicio_Preventivo(ref Cmd);
                        } else if (Parametros.P_Tipo_Solicitud.Trim().Equals("SERVICIO_CORRECTIVO")) {
                            Cls_Ope_Tal_Servicios_Correctivos_Negocio Serv_Correc = new Cls_Ope_Tal_Servicios_Correctivos_Negocio();
                            Serv_Correc.P_No_Entrada = Parametros.P_No_Entrada;
                            Serv_Correc.P_Mecanico_ID = "";
                            Serv_Correc.P_Estatus = "PENDIENTE";
                            Serv_Correc.P_Tipo_Bien = Parametros.P_Tipo_Bien;
                            Serv_Correc.P_No_Solicitud = Parametros.P_No_Solicitud;
                            Serv_Correc.P_Usuario = Parametros.P_Usuario;
                            No_Servicio = Serv_Correc.Alta_Servicio_Correctivo(ref Cmd);
                        } else if (Parametros.P_Tipo_Solicitud.Trim().Equals("REVISTA_MECANICA")) {
                            Cls_Ope_Tal_Revista_Mecanica_Negocio Rev_Mec = new Cls_Ope_Tal_Revista_Mecanica_Negocio();
                            Rev_Mec.P_No_Entrada = Parametros.P_No_Entrada;
                            Rev_Mec.P_Vehiculo_ID = Parametros.P_Vehiculo_ID;
                            Rev_Mec.P_Mecanico_ID = "";
                            Rev_Mec.P_Estatus = "PENDIENTE";
                            Rev_Mec.P_No_Solicitud = Parametros.P_No_Solicitud;
                            Rev_Mec.P_Usuario = Parametros.P_Usuario;
                            Rev_Mec.P_Usuario_ID = Parametros.P_Empleado_Recibio_ID;
                            Rev_Mec.Alta_Revista_Mecanica(ref Cmd);
                        }
                    }

                    Trans.Commit();
                } catch (SqlException Ex) {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152) {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 2627) {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    } else if (Ex.Number == 547) {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 515) {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }
                return No_Servicio;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Entrada_Vehiculo_Verificacion      
            ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a dar de
            ///                         Alta en la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 07/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Alta_Entrada_Vehiculo_Verificacion(Cls_Ope_Tal_Entradas_Vehiculos_Negocio Parametros) { 
                String Mensaje = "";
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                Parametros.P_No_Entrada = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Entradas_Vehiculos.Tabla_Ope_Tal_Entradas_Vehiculos, Ope_Tal_Entradas_Vehiculos.Campo_No_Entrada, 20));
                try {
                    String Mi_SQL = "INSERT INTO " + Ope_Tal_Entradas_Vehiculos.Tabla_Ope_Tal_Entradas_Vehiculos;
                    Mi_SQL = Mi_SQL + " (" + Ope_Tal_Entradas_Vehiculos.Campo_No_Entrada + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Tipo_Entrada + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Vehiculo_ID + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Entrada + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Estatus + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Empleado_Recibio_ID + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Empleado_Entrego_ID + "";
                    if (Parametros.P_Kilometraje > (-1)) {
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Kilometraje + "";
                    }
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Comentarios + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Usuario_Creo + "";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Creo + ")";
                    Mi_SQL = Mi_SQL + " VALUES ('" + Parametros.P_No_Entrada + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Tipo_Entrada + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Vehiculo_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Entrada) + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Solicitud + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Estatus + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Empleado_Recibio_ID + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Empleado_Entrego_ID + "'";
                    if (Parametros.P_Kilometraje > (-1)) {
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Kilometraje + "'";
                    }
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Comentarios + "'";
                    Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Int32 No_Registro = Convert.ToInt32(Obtener_ID_Consecutivo(Ope_Tal_Ent_Veh_Det.Tabla_Ope_Tal_Ent_Veh_Det, Ope_Tal_Ent_Veh_Det.Campo_No_Registro, 20));
                    foreach (DataRow Fila_Actual in Parametros.P_Dt_Detalles.Rows) {
                        Mi_SQL = "INSERT INTO " + Ope_Tal_Ent_Veh_Det.Tabla_Ope_Tal_Ent_Veh_Det;
                        Mi_SQL = Mi_SQL + " (" + Ope_Tal_Ent_Veh_Det.Campo_No_Registro + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Det.Campo_No_Entrada + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Det.Campo_Parte_ID + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Det.Campo_SubParte_ID + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Det.Campo_Valor + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Det.Campo_Usuario_Creo + "";
                        Mi_SQL = Mi_SQL + ", " + Ope_Tal_Ent_Veh_Det.Campo_Fecha_Creo + ")";
                        Mi_SQL = Mi_SQL + " VALUES ('" + No_Registro + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_No_Entrada + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila_Actual["PARTE_ID"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila_Actual["SUBPARTE_ID"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Fila_Actual["VALOR"].ToString().Trim() + "'";
                        Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "'";
                        Mi_SQL = Mi_SQL + ", GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        No_Registro = No_Registro + 1;
                    }


                    Mi_SQL = "UPDATE " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Solicitudes_Serv.Campo_Estatus + " = 'EN_REPARACION'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Solicitudes_Serv.Campo_Fecha_Recepcion_Real + " = '" + String.Format("{0:dd/MM/yyyy}", Parametros.P_Fecha_Entrada) + "'";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Cls_Ope_Tal_Solicitud_Servicio_Negocio Solicitud = new Cls_Ope_Tal_Solicitud_Servicio_Negocio();
                    Solicitud.P_No_Solicitud = Parametros.P_No_Solicitud;
                    Solicitud.P_Estatus = "RECEPCION";                    
                    Solicitud.P_Empleado_Solicito_ID = Parametros.P_Empleado_Recibio_ID;
                    Solicitud.P_Usuario = Parametros.P_Usuario;
                    Cls_Ope_Tal_Solicitud_Servicio_Datos.Alta_Registro_Seguimiento_Solicitud(Solicitud, ref Cmd);

                    Cls_Ope_Tal_Servicios_Preventivos_Negocio Serv_Prev = new Cls_Ope_Tal_Servicios_Preventivos_Negocio();
                    Serv_Prev.P_No_Entrada = Parametros.P_No_Entrada;
                    Serv_Prev.P_Mecanico_ID = "";
                    Serv_Prev.P_Estatus = "REPARACION";
                    Serv_Prev.P_Reparacion = "EXTERNA";
                    Serv_Prev.P_Diagnostico = "VERIFICACION";
                    Serv_Prev.P_Tipo_Bien = "VEHICULO";
                    Serv_Prev.P_No_Solicitud = Parametros.P_No_Solicitud;
                    Serv_Prev.P_Usuario = Parametros.P_Usuario;
                    Serv_Prev.Alta_Servicio_Preventivo(ref Cmd);

                    Trans.Commit();
                } catch (SqlException Ex) {
                    Trans.Rollback();
                    //variable para el mensaje 
                    //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                    if (Ex.Number == 8152)
                    {
                        Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else if (Ex.Number == 2627) {
                        if (Ex.Message.IndexOf("PRIMARY") != -1) {
                            Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                        } else if (Ex.Message.IndexOf("UNIQUE") != -1) {
                            Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                        } else {
                            Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                        }
                    }
                    else if (Ex.Number == 547)
                    {
                        Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Number == 515)
                    {
                        Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    } else {
                        Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    }
                    //Indicamos el mensaje 
                    throw new Exception(Mensaje);
                } finally {
                    Cn.Close();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Actualizacion_Estatus_Entrada      
            ///DESCRIPCIÓN          : Actualiza una Entrada de Forma Remota.
            ///PARAMETROS           : 
            ///                     1.  Parametros. Contiene los parametros que se van a dar de
            ///                         Alta en la Base de Datos.
            ///                     2.  Cmd. Manejo de la Transicion.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 17/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Actualizacion_Estatus_Entrada(Cls_Ope_Tal_Entradas_Vehiculos_Negocio Parametros, ref SqlCommand Cmd) { 
                String Mensaje = "";
                try {
                    String Mi_SQL = "UPDATE " + Ope_Tal_Entradas_Vehiculos.Tabla_Ope_Tal_Entradas_Vehiculos;
                    Mi_SQL = Mi_SQL + " SET " + Ope_Tal_Entradas_Vehiculos.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                    Mi_SQL = Mi_SQL + ", " + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Modifico + " = GETDATE()";
                    Mi_SQL = Mi_SQL + " WHERE " + Ope_Tal_Entradas_Vehiculos.Campo_No_Entrada + " = '" + Parametros.P_No_Entrada + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                } catch (SqlException Ex) {
                    throw new Exception(Ex.Message);
                } 
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Entradas_Vehiculos
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 
            ///                     1.Parametros.Contiene los parametros que se van a utilizar para
            ///                       hacer la consulta de la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 12/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consultar_Entradas_Vehiculos(Cls_Ope_Tal_Entradas_Vehiculos_Negocio Parametros)  {
                String Mi_SQL = null;
                DataSet Ds_Datos = null;
                DataTable Dt_Datos = new DataTable();
                Boolean Entro_Where = false;
                try {
                    Mi_SQL = "SELECT ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_No_Entrada + " AS NO_ENTRADA";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " AS NO_SOLICITUD";
                    Mi_SQL = Mi_SQL + ", (DEPENDENCIAS." + Cat_Dependencias.Campo_Clave + " +' - '+ DEPENDENCIAS." + Cat_Dependencias.Campo_Nombre + ") AS DEPENDENCIA";
                    Mi_SQL = Mi_SQL + ", VEHICULOS." + Ope_Pat_Vehiculos.Campo_Numero_Inventario + " AS NO_INVENTARIO";
                    Mi_SQL = Mi_SQL + ", ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Entrada + " AS FECHA_RECEPCION";
                    Mi_SQL = Mi_SQL + ", DECODE(SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Servicio + ", 'SERVICIO_PREVENTIVO','SERVICIO PREVENTIVO','SERVICIO_CORRECTIVO','SERVICIO CORRECTIVO') AS TIPO_SERVICIO";
                    Mi_SQL = Mi_SQL + ", SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Folio_Solicitud + " AS FOLIO";
                    Mi_SQL = Mi_SQL + ", DECODE(ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Estatus + ", 'PENDIENTE', 'POR ASIGNAR', 'PROCESO', 'ASIGNADO') AS ESTATUS";
                    Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Entradas_Vehiculos.Tabla_Ope_Tal_Entradas_Vehiculos + " ENTRADAS";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOLICITUDES ON ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_No_Solicitud + " = SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias + " DEPENDENCIAS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Dependencia_ID + " = DEPENDENCIAS." + Cat_Dependencias.Campo_Dependencia_ID + "";
                    Mi_SQL = Mi_SQL + " LEFT OUTER JOIN " + Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos + " VEHICULOS ON SOLICITUDES." + Ope_Tal_Solicitudes_Serv.Campo_Bien_ID + " = VEHICULOS." + Ope_Pat_Vehiculos.Campo_Vehiculo_ID + "";
                    if (!String.IsNullOrEmpty(Parametros.P_Estatus)) {
                        if (Entro_Where) { Mi_SQL = Mi_SQL + " AND "; } else { Mi_SQL = Mi_SQL + " WHERE "; Entro_Where = true; }
                        Mi_SQL = Mi_SQL + "ENTRADAS." + Ope_Tal_Entradas_Vehiculos.Campo_Estatus + " IN ('" + Parametros.P_Estatus + "')";
                    }
                    Mi_SQL = Mi_SQL + " ORDER BY NO_ENTRADA";
                    if (Mi_SQL != null && Mi_SQL.Trim().Length > 0) {
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    if (Ds_Datos != null) {
                        Dt_Datos = Ds_Datos.Tables[0];
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Entrada_Vehiculo
            ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
            ///PARAMETROS           : 
            ///                     1.Parametros.Contiene los parametros que se van a utilizar para
            ///                       hacer la consulta de la Base de Datos.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 12/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Cls_Ope_Tal_Entradas_Vehiculos_Negocio Consultar_Detalles_Entrada_Vehiculo(Cls_Ope_Tal_Entradas_Vehiculos_Negocio Parametros)  {
                String Mi_SQL = null;
                Cls_Ope_Tal_Entradas_Vehiculos_Negocio Obj_Cargado = new Cls_Ope_Tal_Entradas_Vehiculos_Negocio();
                try {
                    Mi_SQL = "SELECT * FROM " + Ope_Tal_Entradas_Vehiculos.Tabla_Ope_Tal_Entradas_Vehiculos + " WHERE ";
                    if (Parametros.P_No_Solicitud > (-1)) {
                        Mi_SQL += Ope_Tal_Entradas_Vehiculos.Campo_No_Solicitud + " = '" + Parametros.P_No_Solicitud + "'";
                    } else {
                        Mi_SQL += Ope_Tal_Entradas_Vehiculos.Campo_No_Entrada + " = '" + Parametros.P_No_Entrada + "'";
                    }
                    SqlDataReader Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    while (Reader.Read()) {
                        Obj_Cargado.P_No_Entrada = (!String.IsNullOrEmpty(Reader[Ope_Tal_Entradas_Vehiculos.Campo_No_Entrada].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Entradas_Vehiculos.Campo_No_Entrada]) : -1;
                        Obj_Cargado.P_Tipo_Entrada = (!String.IsNullOrEmpty(Reader[Ope_Tal_Entradas_Vehiculos.Campo_Tipo_Entrada].ToString())) ? Reader[Ope_Tal_Entradas_Vehiculos.Campo_Tipo_Entrada].ToString() : "";
                        Obj_Cargado.P_Vehiculo_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Entradas_Vehiculos.Campo_Vehiculo_ID].ToString())) ? Reader[Ope_Tal_Entradas_Vehiculos.Campo_Vehiculo_ID].ToString() : "";
                        Obj_Cargado.P_Fecha_Entrada = (!String.IsNullOrEmpty(Reader[Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Entrada].ToString())) ? Convert.ToDateTime(Reader[Ope_Tal_Entradas_Vehiculos.Campo_Fecha_Entrada]) : new DateTime();
                        Obj_Cargado.P_No_Solicitud = (!String.IsNullOrEmpty(Reader[Ope_Tal_Entradas_Vehiculos.Campo_No_Solicitud].ToString())) ? Convert.ToInt32(Reader[Ope_Tal_Entradas_Vehiculos.Campo_No_Solicitud]) : -1;
                        Obj_Cargado.P_Empleado_Entrego_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Entradas_Vehiculos.Campo_Empleado_Entrego_ID].ToString())) ? Reader[Ope_Tal_Entradas_Vehiculos.Campo_Empleado_Entrego_ID].ToString() : "";
                        Obj_Cargado.P_Empleado_Recibio_ID = (!String.IsNullOrEmpty(Reader[Ope_Tal_Entradas_Vehiculos.Campo_Empleado_Recibio_ID].ToString())) ? Reader[Ope_Tal_Entradas_Vehiculos.Campo_Empleado_Recibio_ID].ToString() : "";
                        Obj_Cargado.P_Estatus = (!String.IsNullOrEmpty(Reader[Ope_Tal_Entradas_Vehiculos.Campo_Estatus].ToString())) ? Reader[Ope_Tal_Entradas_Vehiculos.Campo_Estatus].ToString() : "";
                        Obj_Cargado.P_Kilometraje = (!String.IsNullOrEmpty(Reader[Ope_Tal_Entradas_Vehiculos.Campo_Kilometraje].ToString())) ? Convert.ToDouble(Reader[Ope_Tal_Entradas_Vehiculos.Campo_Kilometraje]) : -1.0;
                        Obj_Cargado.P_Comentarios = (!String.IsNullOrEmpty(Reader[Ope_Tal_Entradas_Vehiculos.Campo_Comentarios].ToString())) ? Reader[Ope_Tal_Entradas_Vehiculos.Campo_Comentarios].ToString() : "";
                    }
                    Reader.Close();
                    if (Obj_Cargado.P_No_Entrada > (-1)) { 
                        Mi_SQL = "SELECT DETALLES." + Ope_Tal_Ent_Veh_Det.Campo_No_Registro + " AS NO_REGISTRO";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Ent_Veh_Det.Campo_No_Entrada + " AS NO_ENTRADA";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Ent_Veh_Det.Campo_Parte_ID + " AS PARTE_ID";
                        Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Tal_Partes_Veh.Campo_Nombre + " FROM " + Cat_Tal_Partes_Veh.Tabla_Cat_Tal_Partes_Veh + " WHERE " + Cat_Tal_Partes_Veh.Campo_Parte_ID + " = DETALLES." + Ope_Tal_Ent_Veh_Det.Campo_Parte_ID + ") AS PARTE_NOMBRE";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Ent_Veh_Det.Campo_SubParte_ID + " AS SUBPARTE_ID";
                        Mi_SQL = Mi_SQL + ", (SELECT " + Cat_Tal_Partes_Veh.Campo_Nombre + " FROM " + Cat_Tal_Partes_Veh.Tabla_Cat_Tal_Partes_Veh + " WHERE " + Cat_Tal_Partes_Veh.Campo_Parte_ID + " = DETALLES." + Ope_Tal_Ent_Veh_Det.Campo_SubParte_ID + ") AS SUBPARTE_NOMBRE";
                        Mi_SQL = Mi_SQL + ", DETALLES." + Ope_Tal_Ent_Veh_Det.Campo_Valor + " AS VALOR";
                        Mi_SQL = Mi_SQL + ", CASE RTRIM(LTRIM(DETALLES." + Ope_Tal_Ent_Veh_Det.Campo_Valor + "))";
                        Mi_SQL = Mi_SQL + " WHEN 'SI' THEN 'X' END AS VALOR_SI";
                        Mi_SQL = Mi_SQL + ", CASE RTRIM(LTRIM(DETALLES." + Ope_Tal_Ent_Veh_Det.Campo_Valor + "))";
                        Mi_SQL = Mi_SQL + " WHEN 'NO' THEN 'X' END AS VALOR_NO";
                        Mi_SQL = Mi_SQL + " FROM " + Ope_Tal_Ent_Veh_Det.Tabla_Ope_Tal_Ent_Veh_Det + " DETALLES";
                        Mi_SQL = Mi_SQL + " WHERE DETALLES." + Ope_Tal_Ent_Veh_Det.Campo_No_Entrada + " = '" + Obj_Cargado.P_No_Entrada + "'";
                        DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Datos != null && Ds_Datos.Tables.Count > 0) {
                            Obj_Cargado.P_Dt_Detalles = Ds_Datos.Tables[0];
                        }
                    }
                    if(Obj_Cargado.P_No_Entrada>(-1)) {
                        Mi_SQL = "SELECT * FROM " + Ope_Tal_Ent_Veh_Fotos.Tabla_Ope_Tal_Ent_Veh_Fotos + " WHERE " + Ope_Tal_Ent_Veh_Fotos.Campo_No_Entrada + " = '" + Obj_Cargado.P_No_Entrada + "'";
                        DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        if (Ds_Datos != null && Ds_Datos.Tables.Count > 0) {
                            Obj_Cargado.P_Dt_Archivos = Ds_Datos.Tables[0];
                        }
                    }
                } catch (Exception Ex) {
                    String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Obj_Cargado;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
            ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
            ///PARAMETROS:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Marzo/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID) {
                String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
                try {
                    String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
                    Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals("")) {
                        Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
                    }
                } catch (SqlException Ex) {
                    new Exception(Ex.Message);
                }
                return Id;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
            ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
            ///PARAMETROS:     
            ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
            ///             2. Longitud_ID. Longitud que tendra el ID. 
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 10/Marzo/2010 
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN   : 
            ///*******************************************************************************
            private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID) {
                String Retornar = "";
                String Dato = "" + Dato_ID;
                for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++) {
                    Retornar = Retornar + "0";
                }
                Retornar = Retornar + Dato;
                return Retornar;
            }

        #endregion

	}
}