﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Cancelar_Ordenes.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Taller_Mecanico.Administrar_Requisiciones.Negocio;
using JAPAMI.Taller_Mecanico.Requisiciones.Datos;
using JAPAMI.Taller_Mecanico.Parametros.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using System.Data.SqlClient;
using JAPAMI.Taller_Mecanico.Operacion_Orden_Compra.Datos;

/// <summary>
/// Summary description for Cls_Ope_Tal_Cancelar_Ordenes_Compra_Datos
/// </summary>

namespace JAPAMI.Taller_Mecanico.Cancelar_Ordenes.Datos
{
    public class Cls_Ope_Tal_Cancelar_Ordenes_Compra_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Num_Reserva
        ///DESCRIPCIÓN: Consulta el Numero de Reserva de una RQ
        ///PARAMETROS: 1.-Requisicion_Negocio. Datos para la Operacion
        ///CREO:  
        ///FECHA_CREO:  
        ///MODIFICO: Francisco Antonio Gallardo Castañeda
        ///FECHA_MODIFICO: 28/Junio/2012
        ///CAUSA_MODIFICACIÓN: Adecuacion a Taller Mecanico
        ///*******************************************************************************
        public static Int32 Consultar_Num_Reserva(Cls_Ope_Tal_Cancelar_Ordenes_Compra_Negocio Requisicion_Negocio)
        {
            int Num_Reserva = 0;
            try
            {
                String Mi_Sql = "";
                Mi_Sql = "SELECT " + Ope_Tal_Requisiciones.Campo_Numero_Reserva + " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                    " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = " + Requisicion_Negocio.P_No_Requisicio;
                Object Objeto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
                Num_Reserva = int.Parse(Objeto.ToString());
            }
            catch (Exception Ex)
            {
                Ex.ToString();
            }
            return Num_Reserva;
        }

        public static DataTable Consultar_Ordenes_Compra(Cls_Ope_Tal_Cancelar_Ordenes_Compra_Negocio Clase_Negocio)
        {
            String Mi_SQL = "";

            Mi_SQL = "SELECT ORDEN." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra;
            Mi_SQL += ", ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Folio;
            Mi_SQL += ", ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Estatus;
            Mi_SQL += ", ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Creo;
            Mi_SQL += ", ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Total;
            Mi_SQL += ", ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Tipo_Articulo;
            Mi_SQL += ", ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Lista_Requisiciones;
            Mi_SQL += ", REQ." + Ope_Tal_Requisiciones.Campo_Dependencia_ID;
            Mi_SQL += ", SOL." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien;
            Mi_SQL += ", VWS." + Vw_Tal_Servicios.Campo_Numero_Economico;
            Mi_SQL += ", VWS." + Vw_Tal_Servicios.Campo_Numero_Inventario;
            Mi_SQL += " FROM " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " ORDEN ";
            Mi_SQL += " LEFT JOIN " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQ ON ORDEN." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = REQ." + Ope_Tal_Requisiciones.Campo_No_Orden_Compra;
            Mi_SQL += " LEFT JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOL ON REQ." + Ope_Tal_Requisiciones.Campo_No_Solicitud + " = SOL." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud;
            Mi_SQL += " LEFT JOIN " + Vw_Tal_Servicios.Vista_Vw_Tal_Servicios + " VWS ON SOL." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = VWS." + Vw_Tal_Servicios.Campo_No_Solicitud;

            Mi_SQL += " WHERE ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Folio + "  IS NOT NULL ";

            if (Clase_Negocio.P_Estatus != null)
            {
                Mi_SQL += " AND ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Estatus;
                Mi_SQL += "='" + Clase_Negocio.P_Estatus.Trim() + "'";
            }
            else
            {
                Mi_SQL += " AND ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Estatus;
                Mi_SQL += " IN ('RECHAZADA','GENERADA','AUTORIZADA','CANCELACION PARCIAL','CANCELACION TOTAL','SURTIDA','SURTIDA_FACTURA')";

            }

            if (Clase_Negocio.P_Folio_Busqueda != null)
            {
                Mi_SQL += " AND UPPER(ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Folio;
                Mi_SQL += ") LIKE ('%" + Clase_Negocio.P_Folio_Busqueda.Trim() + "%')";

            }

            if (Clase_Negocio.P_No_Requisicio != null)
            {
                Mi_SQL += " AND ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Lista_Requisiciones;
                Mi_SQL += "='" + Clase_Negocio.P_No_Requisicio + "' ";

            }

            if (Clase_Negocio.P_Fecha_Inicio != null)
            {
                Mi_SQL += " AND ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Creo;
                Mi_SQL += " BETWEEN '" + Clase_Negocio.P_Fecha_Inicio + "'";
                Mi_SQL += " AND '" + Clase_Negocio.P_Fecha_Fin + "'";
            }

            Mi_SQL += " ORDER BY ORDEN." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra;

            if (Clase_Negocio.P_No_Orden_Compra != null)
            {
                Mi_SQL = "SELECT ORDEN." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra;
                Mi_SQL += ", ORDEN." + Ope_Tal_Ordenes_Compra.Campo_No_Reserva;
                Mi_SQL += ", CONVERT(CHAR, " + Ope_Tal_Ordenes_Compra.Campo_Fecha_Cancelacion + ", 105) AS " + Ope_Tal_Ordenes_Compra.Campo_Fecha_Cancelacion;
                Mi_SQL += ", (SELECT " + Cat_Com_Proveedores.Campo_Nombre;
                Mi_SQL += " FROM " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores;
                Mi_SQL += " WHERE " + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL += "= ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Proveedor_ID + ") AS PROVEEDOR";
                Mi_SQL += ", ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Estatus;
                Mi_SQL += ", ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Fecha_Cancelacion;
                Mi_SQL += ", ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Motivo_Cancelacion;
                Mi_SQL += ", ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Subtotal;
                Mi_SQL += ", ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Total_IVA;
                Mi_SQL += ", ORDEN." + Ope_Tal_Ordenes_Compra.Campo_Total;
                Mi_SQL += ", REQ." + Ope_Tal_Requisiciones.Campo_Requisicion_ID;
                Mi_SQL += ", REQ." + Ope_Tal_Requisiciones.Campo_Folio + " FOLIO_REQUISICION";
                Mi_SQL += ", REQ." + Ope_Tal_Requisiciones.Campo_Codigo_Programatico;
                Mi_SQL += ", REQ." + Ope_Tal_Requisiciones.Campo_Justificacion_Compra;
                Mi_SQL += ", SOL." + Ope_Tal_Solicitudes_Serv.Campo_Tipo_Bien;
                Mi_SQL += ", VWS." + Vw_Tal_Servicios.Campo_Numero_Economico;
                Mi_SQL += ", VWS." + Vw_Tal_Servicios.Campo_Numero_Inventario;

                Mi_SQL += ", (SELECT " + Ope_Tal_Requisiciones.Campo_Listado_Almacen;
                Mi_SQL += " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;
                Mi_SQL += " WHERE " + Ope_Tal_Requisiciones.Campo_No_Orden_Compra;
                Mi_SQL += "= ORDEN." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + ") AS " + Ope_Tal_Requisiciones.Campo_Listado_Almacen;
                                
                Mi_SQL += " FROM " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra + " ORDEN ";
                Mi_SQL += " LEFT JOIN " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones + " REQ ON ORDEN." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra + " = REQ." + Ope_Tal_Requisiciones.Campo_No_Orden_Compra;
                Mi_SQL += " LEFT JOIN " + Ope_Tal_Solicitudes_Serv.Tabla_Ope_Tal_Solicitudes_Serv + " SOL ON REQ." + Ope_Tal_Requisiciones.Campo_No_Solicitud + " = SOL." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud;
                Mi_SQL += " LEFT JOIN " + Vw_Tal_Servicios.Vista_Vw_Tal_Servicios + " VWS ON SOL." + Ope_Tal_Solicitudes_Serv.Campo_No_Solicitud + " = VWS." + Vw_Tal_Servicios.Campo_No_Solicitud;

                Mi_SQL += " WHERE ORDEN." + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra;
                Mi_SQL += " ='" + Clase_Negocio.P_No_Orden_Compra.Trim() + "'";
            }
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:  Consultar_Productos_Servicios
        ///DESCRIPCIÓN: Metodo que Consulta los detalles de la Requisicion seleccionada, ya sea Producto o servicio.
        ///PARAMETROS: 1.- Cls_Ope_Com_Definir_Cotizadores_Negocio Clase_Negocios, objeto de la clase de negocios
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/JULIO/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Productos_Servicios(Cls_Ope_Tal_Cancelar_Ordenes_Compra_Negocio Clase_Negocio)
        {
            String Mi_SQL = "";
            DataTable Dt_Productos = new DataTable();

            if (Clase_Negocio.P_Estatus != "CANCELACION PARCIAL" && Clase_Negocio.P_Estatus != "CANCELACION TOTAL")
            {
                //Con sultamos primero el tipo de producto de la compra 
                Mi_SQL += "SELECT " + Ope_Tal_Req_Refaccion.Campo_Tipo;
                Mi_SQL += " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion;
                Mi_SQL += " WHERE " + Ope_Tal_Req_Refaccion.Campo_No_Orden_Compra;
                Mi_SQL += "='" + Clase_Negocio.P_No_Orden_Compra.Trim() + "'";
                Mi_SQL += " GROUP BY " + Ope_Tal_Req_Refaccion.Campo_Tipo;
                DataTable Dt_Tipo_Producto = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];


                if (Dt_Tipo_Producto.Rows[0][Ope_Tal_Req_Refaccion.Campo_Tipo].ToString().Trim() != String.Empty)
                {
                    switch (Dt_Tipo_Producto.Rows[0][Ope_Tal_Req_Refaccion.Campo_Tipo].ToString().Trim())
                    {
                        case "PRODUCTO":
                            Mi_SQL = "SELECT PRODUCTO." + Cat_Tal_Refacciones.Campo_Clave +
                                    ", PRODUCTO." + Cat_Tal_Refacciones.Campo_Nombre +
                                    " + ' ' + PRODUCTO." + Cat_Tal_Refacciones.Campo_Descripcion + " AS " + Cat_Tal_Refacciones.Campo_Nombre +
                                    ", DET_REQ." + Ope_Tal_Req_Refaccion.Campo_Cantidad +
                                    ", DET_REQ." + Ope_Tal_Req_Refaccion.Campo_Precio_U_Sin_Imp_Cotizado +
                                    ", DET_REQ." + Ope_Tal_Req_Refaccion.Campo_Precio_U_Con_Imp_Cotizado +
                                    ", DET_REQ." + Ope_Tal_Req_Refaccion.Campo_IVA_Cotizado +
                                    ", DET_REQ." + Ope_Tal_Req_Refaccion.Campo_Subtota_Cotizado +
                                    ", DET_REQ." + Ope_Tal_Req_Refaccion.Campo_Total_Cotizado +
                                    " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " DET_REQ" +
                                    " JOIN " + Cat_Tal_Refacciones.Tabla_Cat_Tal_Refacciones + " PRODUCTO" +
                                    " ON PRODUCTO." + Cat_Tal_Refacciones.Campo_Refaccion_ID + " =" +
                                    " DET_REQ." + Ope_Tal_Req_Refaccion.Campo_Prod_Serv_ID +
                                    " WHERE DET_REQ." + Ope_Tal_Req_Refaccion.Campo_No_Orden_Compra +
                                    "='" + Clase_Negocio.P_No_Orden_Compra + "'";
                            break;
                    }
                    Dt_Productos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }//Fin del IF
            }//FIN DEL IF 
            return Dt_Productos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Liberar_Presupuesto_Cancelacion_Total
        ///DESCRIPCIÓN: Metodo que libera el presupúesto de una orden de compra 
        ///PARAMETROS: 1.- Cls_Ope_Com_Cancelar_Ordenes_Compra_Negocio Clase_Negocio
        ///CREO:
        ///FECHA_CREO: 10/OCT/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Liberar_Presupuesto_Cancelacion_Total(Cls_Ope_Tal_Cancelar_Ordenes_Compra_Negocio Clase_Negocio)
        {
            String Partida_ID = "";
            String Proyecto_ID = "";
            String FF = "";
            double Monto_Total_Cotizado = 0;
            String Mi_SQL = "";
            String Mensaje = "";

            try
            {
                //Consultamos los campos para afectar el presupuesto 
                Mi_SQL = "SELECT " + Ope_Tal_Req_Refaccion.Campo_Partida_ID;
                Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID;
                Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID;
                Mi_SQL += " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion;
                Mi_SQL += " WHERE " + Ope_Tal_Req_Refaccion.Campo_No_Orden_Compra;
                Mi_SQL += "='" + Clase_Negocio.P_No_Orden_Compra.Trim() + "'";
                Mi_SQL += " GROUP BY " + Ope_Tal_Req_Refaccion.Campo_Partida_ID;
                Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID;
                Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID;

                DataTable Dt_Partidas_Productos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Obtenemos la Partida, Programa y Fuente de dinanciamiento correspondiente al Producto o servicio
                Partida_ID = Dt_Partidas_Productos.Rows[0][Ope_Tal_Req_Refaccion.Campo_Partida_ID].ToString();
                Proyecto_ID = Dt_Partidas_Productos.Rows[0][Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID].ToString();
                FF = Dt_Partidas_Productos.Rows[0][Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID].ToString();
                Monto_Total_Cotizado = double.Parse(Clase_Negocio.P_Monto_Total);

                //liberamos el presupuesto que era para este listado
                Mi_SQL = "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                                " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible +
                                " =" + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " + " + Monto_Total_Cotizado +
                                "," + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido +
                                "=" + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " - " + Monto_Total_Cotizado +
                                " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                                "='" + Partida_ID + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                                "='" + Proyecto_ID + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
                                "='" + FF + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio +
                                " = (SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
                                " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                                " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                                "='" + Partida_ID + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                                "='" + Proyecto_ID + "'" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                                "= TO_CHAR(GETDATE(),'YYYY'))" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                                "= TO_CHAR(GETDATE(),'YYYY')" +
                                " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
                                "=(SELECT " + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                                " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                                " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "=" +
                                "(SELECT " + Ope_Tal_Ordenes_Compra.Campo_No_Requisicion +
                                " FROM " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra +
                                " WHERE " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra +
                                "='" + Clase_Negocio.P_No_Orden_Compra + "' GROUP BY " +
                                Ope_Tal_Ordenes_Compra.Campo_No_Requisicion + ") GROUP BY " + Ope_Tal_Requisiciones.Campo_Dependencia_ID + ")";

                //Sentencia que ejecuta el query
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                Mensaje = "Se cancelo exitosamente la Orden de Compra con su requisicion";
            }
            catch (Exception Ex)
            {
                Mensaje = Ex.Message;
            }
            return Mensaje;
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Liberar_Presupuesto_Cancelacion_Total
        ///DESCRIPCIÓN: Metodo que libera el presupúesto de una orden de compra 
        ///PARAMETROS: 1.- Cls_Ope_Com_Cancelar_Ordenes_Compra_Negocio Clase_Negocio
        ///CREO:
        ///FECHA_CREO: 10/OCT/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static String Liberar_Presupuesto_Cancelacion_Parcial(Cls_Ope_Tal_Cancelar_Ordenes_Compra_Negocio Clase_Negocio)
        {
            String Mi_SQL = "";
            String Mensaje_Error = "";
            try
            {
                //consultamos los montos Cotizados y el inicial para hacer un apartado del inicial 
                Mi_SQL = "SELECT REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Partida_ID +
                        ",REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID +
                        ", (SELECT " + Ope_Tal_Requisiciones.Campo_Total +
                        " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                        " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                        "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS TOTAL" +
                         ", (SELECT " + Ope_Tal_Requisiciones.Campo_Total_Cotizado +
                         " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                         " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                         "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS TOTAL_COTIZADO" +
                         ", (SELECT " + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
                         " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                         " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                         "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS DEPENDENCIA_ID" +
                         ", REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID +
                         " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " REQ_DET" +
                         " WHERE REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + "='" + Clase_Negocio.P_No_Requisicio + "'";
                DataTable Dt_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                //Guardamos en la variable Monto_Restante la diferencia de Total_Cotizado y Monto_Total inicial del producto
                //Primero checamos si se va a restar o a aumentar el presupuesto
                double Monto_Cotizado = double.Parse(Dt_Requisicion.Rows[0]["TOTAL_COTIZADO"].ToString());
                double Monto_Anterior = double.Parse(Dt_Requisicion.Rows[0]["TOTAL"].ToString());
                double Diferencia = 0;
                String Partida_ID = Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Partida_ID].ToString().Trim();
                String Proyecto_ID = Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID].ToString().Trim();
                String Dependencia_ID = Dt_Requisicion.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
                String FF = Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID].ToString().Trim();
                bool Suma_Diferencia = false;

                // PASO 1 VERIFICAMOS CUAL DE LOS 2 MONTOS ES MAYOR SI EL COTIZADO O  EL ANTERIOR
                if (Monto_Cotizado > Monto_Anterior)
                {
                    //Obtenemos la resta
                    Diferencia = Monto_Cotizado - Monto_Anterior;
                    Suma_Diferencia = true;
                }
                if (Monto_Cotizado < Monto_Anterior)
                {
                    //obtener resta
                    Diferencia = Monto_Anterior - Monto_Cotizado;
                    Suma_Diferencia = false;
                }
                //modificamos el monto dependiendo de si se resta la diferencia o se suma
                if (Suma_Diferencia == true)
                {
                    //Primero se Verifica que aun exista Presupuesto
                    Mi_SQL = "";
                    Mi_SQL = "SELECT " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible;
                    Mi_SQL += " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                    Mi_SQL += " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID;
                    Mi_SQL += "='" + Partida_ID + "'";
                    Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID;
                    Mi_SQL += "='" + Proyecto_ID + "'";
                    Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID;
                    Mi_SQL += "='" + FF + "'";
                    Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto;
                    Mi_SQL += "= TO_CHAR(GETDATE(),'YYYY')";
                    Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID;
                    Mi_SQL += "='" + Dependencia_ID + "'";
                    Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio;
                    Mi_SQL += " = (SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")";
                    Mi_SQL += " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                    Mi_SQL += " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID;
                    Mi_SQL += "='" + Partida_ID + "'";
                    Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID;
                    Mi_SQL += "='" + Proyecto_ID + "'";
                    Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID;
                    Mi_SQL += "='" + Dependencia_ID + "'";
                    Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto;
                    Mi_SQL += "= TO_CHAR(GETDATE(),'YYYY'))";
                    DataTable Dt_Monto_Disponible = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                    double Monto_Disponible = double.Parse(Dt_Monto_Disponible.Rows[0][Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible].ToString().Trim());
                    if (Monto_Disponible > Diferencia)
                    {
                        //se ACTUALIZAN LOS PRESUPUESTOS DE LA DEPENDENCIA, CORRESPONDIENTE AL PROYECTO Y PARTIDA
                        Mi_SQL = "";
                        Mi_SQL = "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                        Mi_SQL += " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible;
                        Mi_SQL += " =" + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " - " + Diferencia.ToString();
                        Mi_SQL += "," + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido;
                        Mi_SQL += "=" + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " + " + Diferencia.ToString();
                        Mi_SQL += " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID;
                        Mi_SQL += "='" + Partida_ID + "'";
                        Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID;
                        Mi_SQL += "='" + Proyecto_ID + "'";
                        Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID;
                        Mi_SQL += "='" + FF + "'";
                        Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto;
                        Mi_SQL += "= TO_CHAR(GETDATE(),'YYYY')";
                        Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID;
                        Mi_SQL += "='" + Dependencia_ID + "'";
                        Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio;
                        Mi_SQL += " = (SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")";
                        Mi_SQL += " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                        Mi_SQL += " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID;
                        Mi_SQL += "='" + Partida_ID + "'";
                        Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID;
                        Mi_SQL += "='" + Proyecto_ID + "'";
                        Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID;
                        Mi_SQL += "='" + Dependencia_ID + "'";
                        Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto;
                        Mi_SQL += "= TO_CHAR(GETDATE(),'YYYY'))";

                        //Sentencia que ejecuta el query
                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                        //Modificamos los montos cotizados de la requisicion a 0
                        Modificar_Montos_Cotizados_Requisicion(Clase_Negocio);
                        Mensaje_Error = "Se realizo la Cancelacion Parcial Satisfactoriamente";
                    }
                    else
                    {
                        Mensaje_Error = "No existe presupuesto Suficiente para realizar la Cancelacion Parcial";
                    }
                }//fin if SumaDiferencia
                else
                {
                    //Modificamos el presupuesto, ya que se resta el monto que sobro pues el valor cotizado es menor k el anterior
                    //se ACTUALIZAN LOS PRESUPUESTOS DE LA DEPENDENCIA, CORRESPONDIENTE AL PROYECTO Y PARTIDA

                    Mi_SQL = "UPDATE " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                        " SET " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible +
                        " =" + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible + " + " + Diferencia.ToString() +
                        "," + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido +
                        "=" + Cat_Com_Dep_Presupuesto.Campo_Monto_Comprometido + " - " + Diferencia.ToString() +
                        " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                        "='" + Partida_ID + "'" +
                        " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                        "='" + Proyecto_ID + "'" +
                        " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID +
                        "='" + FF + "'" +
                        " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio +
                        " = (SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")" +
                        " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto +
                        " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID +
                        "='" + Partida_ID + "'" +
                        " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID +
                        "='" + Proyecto_ID + "'" +
                        " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                        "= TO_CHAR(GETDATE(),'YYYY'))" +
                        " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto +
                        "= TO_CHAR(GETDATE(),'YYYY')" +
                        " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID +
                        "='" + Dependencia_ID + "'";
                    //Sentencia que ejecuta el query
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    //Modificamos los montos cotizados de la requisicion a 0
                    Modificar_Montos_Cotizados_Requisicion(Clase_Negocio);
                    Mensaje_Error = "Se realizo la Cancelacion Parcial Satisfactoriamente";
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error = Ex.Message;
            }
            return Mensaje_Error;
        }

        public static void Modificar_Montos_Cotizados_Requisicion(Cls_Ope_Tal_Cancelar_Ordenes_Compra_Negocio Clase_Negocio)
        {
            //mODIFICAMOS LOS TOTALES COTIZADOS EN LA TABLA Ope_Tal_Requisiciones
            String Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;
            Mi_SQL += " SET " + Ope_Tal_Requisiciones.Campo_Subtotal_Cotizado + "=0";
            Mi_SQL += ", " + Ope_Tal_Requisiciones.Campo_Total_Cotizado + "=0";
            Mi_SQL += ", " + Ope_Tal_Requisiciones.Campo_IVA_Cotizado + "=0";
            Mi_SQL += ", " + Ope_Tal_Requisiciones.Campo_IEPS_Cotizado + "=0";
            Mi_SQL += " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            //MODIFICAMOS LOS MONTOS COTIZADOS EN EL DETALLE DE PRODUCTOS DE LA REQUISICION
            Mi_SQL = "";
            Mi_SQL = "UPDATE " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion;
            Mi_SQL += " SET " + Ope_Tal_Req_Refaccion.Campo_Precio_U_Sin_Imp_Cotizado + "=0";
            Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Precio_U_Con_Imp_Cotizado + "=0";
            Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Subtota_Cotizado + "=0";
            Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Total_Cotizado + "=0";
            Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Proveedor_ID + "=NULL";
            Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Nombre_Proveedor + "=NULL";
            Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_No_Orden_Compra + "=0";
            Mi_SQL += " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        }

        public static String Modificar_Orden_Compra(Cls_Ope_Tal_Cancelar_Ordenes_Compra_Negocio Clase_Negocio)
        {
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans = null;
            SqlDataAdapter DataAdapter = new SqlDataAdapter();
            String Mi_SQL = "";
            String Mensaje = "";
            Double Monto_Total = 0;
            Double Monto_Cotizado = 0;
            Double Diferencia = 0;
            DataTable Dt_Aux = new DataTable();
            //VAriable que ayudara a mandar llamar el nombre del metodo para dar de alta el estatus en el segumiento de la requisicion
            Cls_Ope_Tal_Requisiciones_Datos Requisicion = new Cls_Ope_Tal_Requisiciones_Datos();
            Cls_Ope_Tal_Administrar_Requisiciones_Negocio Administrar_Requisicion =
                new Cls_Ope_Tal_Administrar_Requisiciones_Negocio();
            try
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Trans.Connection;
                Cmd.Transaction = Trans;

                //En caso de ser cancelacion total
                if (Clase_Negocio.P_Estatus.Trim() == "CANCELACION TOTAL")
                {
                    Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;
                    Mi_SQL += " SET " + Ope_Tal_Requisiciones.Campo_Estatus;
                    Mi_SQL += "='CANCELADA'";
                    Mi_SQL += ", " + Ope_Tal_Requisiciones.Campo_Fecha_Cancelada;
                    Mi_SQL += "=GETDATE()";
                    Mi_SQL += ", " + Ope_Tal_Requisiciones.Campo_Empleado_Cancelada_ID;
                    Mi_SQL += "='" + Cls_Sessiones.Empleado_ID + "'";
                    Mi_SQL += " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID;
                    Mi_SQL += "='" + Clase_Negocio.P_No_Requisicio + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    //Registramos la afectacion al Estatus  de las propuestas de cotizacion ya que se cambia a aceptada, se pone como RECHAZADA
                    Mi_SQL = "UPDATE " + Ope_Tal_Propuesta_Cotizacion.Tabla_Ope_Tal_Propuesta_Cotizacion;
                    Mi_SQL += " SET ESTATUS='RECHAZADA' WHERE " + Ope_Tal_Propuesta_Cotizacion.Campo_No_Requisicion + "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                    Cls_Ope_Tal_Requisiciones_Datos.Registrar_Historial("CANCELADA", Clase_Negocio.P_No_Requisicio);
                    
                    Mi_SQL = "INSERT INTO " + Ope_Tal_Req_Observaciones.Tabla_Ope_Tal_Req_Observaciones;
                    Mi_SQL += " ( " + Ope_Tal_Req_Observaciones.Campo_Requisicion_ID; 
                    Mi_SQL += ", " + Ope_Tal_Req_Observaciones.Campo_Comentario;
                    Mi_SQL += ", " + Ope_Tal_Req_Observaciones.Campo_Estatus;
                    Mi_SQL += ", " + Ope_Tal_Req_Observaciones.Campo_Usuario_Creo;
                    Mi_SQL += ", " + Ope_Tal_Req_Observaciones.Campo_Fecha_Creo;
                    Mi_SQL += ") VALUES ('";
                    Mi_SQL += Clase_Negocio.P_No_Requisicio + "','";
                    Mi_SQL += Clase_Negocio.P_Motivo_Cancelacion + " / Se canceló la orden de compra','";
                    Mi_SQL += "CANCELADA','";
                    Mi_SQL += Cls_Sessiones.Nombre_Empleado.ToString() + "', GETDATE())";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();


                    //Se modifica la propuesta de cotizacion se cambia el estatus a 
                    if (Clase_Negocio.P_Listado_Almacen == null)
                    {
                        //liberamos por completo el presupuesto en caso de no ser unlistado almace
                        //Mensaje = Liberar_Presupuesto_Cancelacion_Total(Clase_Negocio);

                        //Consultamos el numero de reserva para la requisicion.
                        String No_Reserva = "0";
                        Mi_SQL = "SELECT " + Ope_Tal_Requisiciones.Campo_Numero_Reserva + " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;
                        Mi_SQL += " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = " + Clase_Negocio.P_No_Requisicio;
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        Object Objeto = Cmd.ExecuteScalar();

                        if (Objeto != null)
                        {
                            No_Reserva = Convert.ToString(Convert.ToUInt16(Objeto.ToString()));
                        }

                        //Cosnsultamos los datos de la Partida
                        Cls_Tal_Parametros_Negocio Negocio_Parametros = new Cls_Tal_Parametros_Negocio();
                        Negocio_Parametros.P_Cmmd = Cmd;
                        Negocio_Parametros = Negocio_Parametros.Consulta_Parametros_Presupuesto(Clase_Negocio.P_No_Economico, Clase_Negocio.P_Tipo_Bien);
                        //agregamos las columnas al datatable
                        DataTable Dt_Registros = new DataTable();
                        DataRow Fila;
                        Dt_Registros.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("ANIO", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("IMPORTE", System.Type.GetType("System.Double"));
                        Dt_Registros.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("FECHA_CREO", System.Type.GetType("System.DateTime"));

                        Fila = Dt_Registros.NewRow();
                        Fila["FUENTE_FINANCIAMIENTO_ID"] = Negocio_Parametros.P_Fuente_Financiamiento_ID;
                        Fila["PROGRAMA_ID"] = Negocio_Parametros.P_Programa_ID;
                        Fila["DEPENDENCIA_ID"] = Clase_Negocio.P_Dependencia_Id;
                        Fila["PARTIDA_ID"] = Negocio_Parametros.P_Partida_ID;
                        Fila["ANIO"] = DateTime.Today.Year.ToString();
                        Fila["IMPORTE"] = Clase_Negocio.P_Monto_Total;
                        Fila["FECHA_CREO"] = DateTime.Today;
                        Fila["CAPITULO_ID"] = Negocio_Parametros.P_Capitulo_ID;
                        Dt_Registros.Rows.Add(Fila);

                        Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido, Dt_Registros, Cmd);
                        //Registrar movimiento presupuestal
                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(No_Reserva.ToString(), Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido, Convert.ToDouble(Clase_Negocio.P_Monto_Total), String.Empty, String.Empty, String.Empty, Negocio_Parametros.P_Partida_ID, Cmd);


                        Mi_SQL = "UPDATE " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas
                                + " SET " + Ope_Psp_Reservas.Campo_Estatus + " = 'CANCELADA'"
                                + " WHERE " + Ope_Psp_Reservas.Campo_No_Reserva + " = '" + No_Reserva.Trim() + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery(); 
                        //Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(No_Reserva.ToString(), Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido, Dt_Registros);
                        ////Registrar movimiento presupuestal
                        //Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(No_Reserva.ToString(), Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido, Convert.ToDouble(Clase_Negocio.P_Monto_Total), String.Empty, String.Empty, String.Empty, Negocio_Parametros.P_Partida_ID, ref Cmd);
                    }
                    else
                    {
                        Mensaje = "Se realizo la Cancelacion Total Satisfactoriamente";
                    }
                    //Modificamos el estatus de la Requisicion
                    Mi_SQL = "UPDATE " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra;
                    Mi_SQL += " SET " + Ope_Tal_Ordenes_Compra.Campo_Estatus;
                    Mi_SQL += "='CANCELADA'";
                    Mi_SQL += ", " + Ope_Tal_Ordenes_Compra.Campo_Fecha_Cancelacion;
                    Mi_SQL += "=GETDATE()";
                    Mi_SQL += ", " + Ope_Tal_Ordenes_Compra.Campo_Motivo_Cancelacion;
                    Mi_SQL += "='" + Clase_Negocio.P_Motivo_Cancelacion + "'";
                    Mi_SQL += " WHERE " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra;
                    Mi_SQL += "='" + Clase_Negocio.P_No_Orden_Compra.Trim() + "'";
                    //Sentencia que ejecuta el query
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();

                }//Fin del if Cancelacion total
                
                //En caso de Ser cancelacion Parcial 
                if (Clase_Negocio.P_Estatus.Trim() == "CANCELACION PARCIAL")
                {
                    //Consultamos monto total y cotizado para comparar y liberar psp
                    Mi_SQL = " SELECT " + Ope_Tal_Requisiciones.Campo_Total + " AS TOTAL, " +
                                          Ope_Tal_Requisiciones.Campo_Total_Cotizado + " AS TOTAL_COTIZADO" +
                             " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
                             " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
                                            " = " + Clase_Negocio.P_No_Requisicio.Trim();
                    Cmd.CommandText = Mi_SQL;
                    DataAdapter.SelectCommand = Cmd;
                    DataAdapter.Fill(Dt_Aux);
                    if (Dt_Aux.Rows.Count > 0) 
                    {
                        Double.TryParse(Dt_Aux.Rows[0]["TOTAL_COTIZADO"].ToString(), out Monto_Cotizado);
                        Double.TryParse(Dt_Aux.Rows[0]["TOTAL"].ToString(),out Monto_Total);
                    }
                    bool Suma_Diferencia = false;

                // PASO 1 VERIFICAMOS CUAL DE LOS 2 MONTOS ES MAYOR SI EL COTIZADO O  EL ANTERIOR
                if (Monto_Cotizado > Monto_Total)
                {
                    //Obtenemos la resta
                    Diferencia = Monto_Cotizado - Monto_Total;
                    Suma_Diferencia = true;
                }
                if (Monto_Cotizado < Monto_Total)
                {
                    //obtener resta
                    Diferencia = Monto_Total - Monto_Cotizado;
                    Suma_Diferencia = false;
                }
                //modificamos el monto dependiendo de si se resta la diferencia o se suma
                
                    //Cuando es un listado almacen se realiza lo siguiente
                    if (!String.IsNullOrEmpty(Clase_Negocio.P_Listado_Almacen))
                    {
                        Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;
                        Mi_SQL += " SET " + Ope_Tal_Requisiciones.Campo_Estatus;
                        Mi_SQL += "='FILTRADA'";
                        Mi_SQL += ", " + Ope_Tal_Requisiciones.Campo_Alerta + "='ROJA2'";
                        Mi_SQL += " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID;
                        Mi_SQL += "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        //REgistramos el cambio de estatus a Proveedor pero antes una de Reasignar_Proveedor

                        Cls_Ope_Tal_Requisiciones_Datos.Registrar_Historial("R_PROVEEDOR", Clase_Negocio.P_No_Requisicio);
                        Cls_Ope_Tal_Requisiciones_Datos.Registrar_Historial("FILTRADA", Clase_Negocio.P_No_Requisicio);

                        Mi_SQL = "INSERT INTO " + Ope_Tal_Req_Observaciones.Tabla_Ope_Tal_Req_Observaciones;
                        Mi_SQL += " ( " + Ope_Tal_Req_Observaciones.Campo_Requisicion_ID;
                        Mi_SQL += ", " + Ope_Tal_Req_Observaciones.Campo_Comentario;
                        Mi_SQL += ", " + Ope_Tal_Req_Observaciones.Campo_Estatus;
                        Mi_SQL += ", " + Ope_Tal_Req_Observaciones.Campo_Usuario_Creo;
                        Mi_SQL += ", " + Ope_Tal_Req_Observaciones.Campo_Fecha_Creo;
                        Mi_SQL += ") VALUES ('";
                        Mi_SQL += Clase_Negocio.P_No_Requisicio + "','";
                        Mi_SQL += Clase_Negocio.P_Motivo_Cancelacion + " / Se canceló la orden de compra','";
                        Mi_SQL += "FILTRADA','";
                        Mi_SQL += Cls_Sessiones.Nombre_Empleado.ToString() + "', GETDATE())";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();


                        //Modificamos todas las propuestas de Cotizacion al estatus de EN CONSTRUCCION con la finalidad de poder reecotizar los productos

                        //Modificamos todas las propuestas de Cotizacion a EN CONSTRUCCION para poder modificar los montos en caso de ser necesario
                        Mi_SQL = "UPDATE " + Ope_Tal_Propuesta_Cotizacion.Tabla_Ope_Tal_Propuesta_Cotizacion;
                        Mi_SQL += " SET " + Ope_Tal_Propuesta_Cotizacion.Campo_Estatus;
                        Mi_SQL += "='EN CONSTRUCCION'";
                        Mi_SQL += ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Resultado + "=NULL";
                        Mi_SQL += " WHERE " + Ope_Tal_Propuesta_Cotizacion.Campo_No_Requisicion + "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        //mODIFICAMOS LOS TOTALES COTIZADOS EN LA TABLA Ope_Tal_Requisiciones
                        Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;
                        Mi_SQL += " SET " + Ope_Tal_Requisiciones.Campo_Subtotal_Cotizado + "=0";
                        Mi_SQL += ", " + Ope_Tal_Requisiciones.Campo_Total_Cotizado + "=0";
                        Mi_SQL += ", " + Ope_Tal_Requisiciones.Campo_IVA_Cotizado + "=0";
                        Mi_SQL += ", " + Ope_Tal_Requisiciones.Campo_IEPS_Cotizado + "=0";
                        Mi_SQL += " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        //MODIFICAMOS LOS MONTOS COTIZADOS EN EL DETALLE DE PRODUCTOS DE LA REQUISICION
                        Mi_SQL = "";
                        Mi_SQL = "UPDATE " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion;
                        Mi_SQL += " SET " + Ope_Tal_Req_Refaccion.Campo_Precio_U_Sin_Imp_Cotizado + "=0";
                        Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Precio_U_Con_Imp_Cotizado + "=0";
                        Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Subtota_Cotizado + "=0";
                        Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Total_Cotizado + "=0";
                        Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Proveedor_ID + "=NULL";
                        Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_Nombre_Proveedor + "=NULL";
                        Mi_SQL += ", " + Ope_Tal_Req_Refaccion.Campo_No_Orden_Compra + "=0";
                        Mi_SQL += " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();

                        //Modificamos el estatus de la Requisicion
                        Mi_SQL = "UPDATE " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra;
                        Mi_SQL += " SET " + Ope_Tal_Ordenes_Compra.Campo_Estatus;
                        Mi_SQL += "='CANCELADA'";
                        Mi_SQL += ", " + Ope_Tal_Ordenes_Compra.Campo_Fecha_Cancelacion;
                        Mi_SQL += "=GETDATE()";
                        Mi_SQL += ", " + Ope_Tal_Ordenes_Compra.Campo_Motivo_Cancelacion;
                        Mi_SQL += "='" + Clase_Negocio.P_Motivo_Cancelacion + "'";
                        Mi_SQL += " WHERE " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra;
                        Mi_SQL += "='" + Clase_Negocio.P_No_Orden_Compra.Trim() + "'";
                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                        Mensaje = "Se realizo la Cancelacion Parcial Satisfactoriamente";
                    }
                    //VERIFICAMOS PRIMERO SI EXISTE PRESUPUESTO ESTO ES PARA CUANDO NO ES UN LISTADO DE ALMACEN 
                    if (String.IsNullOrEmpty(Clase_Negocio.P_Listado_Almacen))
                    {
                        if (Consultamos_Presupuesto_Existente(Clase_Negocio) == true)
                        {
                            Mi_SQL = "UPDATE " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;
                            Mi_SQL += " SET " + Ope_Tal_Requisiciones.Campo_Estatus;
                            Mi_SQL += "='FILTRADA'";
                            Mi_SQL += ", " + Ope_Tal_Requisiciones.Campo_Alerta + "='ROJA2'";
                            Mi_SQL += " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID;
                            Mi_SQL += "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                            //REgistramos el cambio de estatus a Proveedor pero antes una de Reasignar_Proveedor

                            Cls_Ope_Tal_Requisiciones_Datos.Registrar_Historial("R_PROVEEDOR", Clase_Negocio.P_No_Requisicio);
                            Cls_Ope_Tal_Requisiciones_Datos.Registrar_Historial("FILTRADA", Clase_Negocio.P_No_Requisicio);

                            Mi_SQL = "INSERT INTO " + Ope_Tal_Req_Observaciones.Tabla_Ope_Tal_Req_Observaciones;
                            Mi_SQL += " ( " + Ope_Tal_Req_Observaciones.Campo_Requisicion_ID;
                            Mi_SQL += ", " + Ope_Tal_Req_Observaciones.Campo_Comentario;
                            Mi_SQL += ", " + Ope_Tal_Req_Observaciones.Campo_Estatus;
                            Mi_SQL += ", " + Ope_Tal_Req_Observaciones.Campo_Usuario_Creo;
                            Mi_SQL += ", " + Ope_Tal_Req_Observaciones.Campo_Fecha_Creo;
                            Mi_SQL += ") VALUES ('";
                            Mi_SQL += Clase_Negocio.P_No_Requisicio + "','";
                            Mi_SQL += Clase_Negocio.P_Motivo_Cancelacion + " / Se canceló la orden de compra','";
                            Mi_SQL += "FILTRADA','";
                            Mi_SQL += Cls_Sessiones.Nombre_Empleado.ToString() + "', GETDATE())";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();

                            //Modificamos todas las propuestas de Cotizacion al estatus de EN CONSTRUCCION con la finalidad de poder reecotizar los productos

                            //Modificamos todas las propuestas de Cotizacion a EN CONSTRUCCION para poder modificar los montos en caso de ser necesario
                            Mi_SQL = "UPDATE " + Ope_Tal_Propuesta_Cotizacion.Tabla_Ope_Tal_Propuesta_Cotizacion;
                            Mi_SQL += " SET " + Ope_Tal_Propuesta_Cotizacion.Campo_Estatus;
                            Mi_SQL += "='EN CONSTRUCCION'";
                            Mi_SQL += ", " + Ope_Tal_Propuesta_Cotizacion.Campo_Resultado + "=NULL";
                            Mi_SQL += " WHERE " + Ope_Tal_Propuesta_Cotizacion.Campo_No_Requisicion + "='" + Clase_Negocio.P_No_Requisicio.Trim() + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();

                            if (Clase_Negocio.P_Listado_Almacen == null)
                            {
                                //Consultamos el numero de reserva para la requisicion.
                                String No_Reserva = "0";
                                Mi_SQL = "SELECT " + Ope_Tal_Requisiciones.Campo_Numero_Reserva + " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones;
                                Mi_SQL += " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID + " = " + Clase_Negocio.P_No_Requisicio;
                                Cmd.CommandText = Mi_SQL;
                                Cmd.ExecuteNonQuery();
                                Object Objeto = Cmd.ExecuteScalar();

                                if (Objeto != null)
                                {
                                    No_Reserva = Convert.ToString(Convert.ToUInt16(Objeto.ToString()));
                                }

                                //liberamos por completo el presupuesto en caso de no ser unlistado almace

                                //Liberamos el presupuesto y pasamos a cero todos los valores cotizados de la requisicion para su nueva cotizacion
                                //Mensaje = Liberar_Presupuesto_Cancelacion_Parcial(Clase_Negocio);

                                //Cosnsultamos los datos de la Partida
                                Cls_Tal_Parametros_Negocio Negocio_Parametros = new Cls_Tal_Parametros_Negocio();
                                Negocio_Parametros.P_Cmmd = Cmd;
                                Negocio_Parametros = Negocio_Parametros.Consulta_Parametros_Presupuesto(Clase_Negocio.P_No_Economico, Clase_Negocio.P_Tipo_Bien);
                                //agregamos las columnas al datatable
                                DataTable Dt_Registros = new DataTable();
                                DataRow Fila;
                                Dt_Registros.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                                Dt_Registros.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                                Dt_Registros.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                                Dt_Registros.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                                Dt_Registros.Columns.Add("ANIO", System.Type.GetType("System.String"));
                                Dt_Registros.Columns.Add("IMPORTE", System.Type.GetType("System.Double"));
                                Dt_Registros.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                                Dt_Registros.Columns.Add("FECHA_CREO", System.Type.GetType("System.DateTime"));

                                Fila = Dt_Registros.NewRow();
                                Fila["FUENTE_FINANCIAMIENTO_ID"] = Negocio_Parametros.P_Fuente_Financiamiento_ID;
                                Fila["PROGRAMA_ID"] = Negocio_Parametros.P_Programa_ID;
                                Fila["DEPENDENCIA_ID"] = Clase_Negocio.P_Dependencia_Id;
                                Fila["PARTIDA_ID"] = Negocio_Parametros.P_Partida_ID;
                                Fila["ANIO"] = DateTime.Today.Year.ToString();
                                Fila["IMPORTE"] = Monto_Total;
                                Fila["CAPITULO_ID"] = Negocio_Parametros.P_Capitulo_ID;
                                Fila["FECHA_CREO"] = DateTime.Today;
                                Dt_Registros.Rows.Add(Fila);

                                Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido, Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido, Dt_Registros, Cmd);
                                //Registrar movimiento presupuestal
                                Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(No_Reserva.ToString(), Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido, Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido, Convert.ToDouble(Clase_Negocio.P_Monto_Total), String.Empty, String.Empty, String.Empty, Negocio_Parametros.P_Partida_ID, Cmd);
                                if (Diferencia > 0)
                                {
                                    Dt_Registros.Rows[0]["IMPORTE"] = Diferencia;
                                    Dt_Registros.AcceptChanges();
                                    Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido, Dt_Registros, Cmd);
                                    //Registrar movimiento presupuestal
                                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(No_Reserva.ToString(), Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido, Convert.ToDouble(Clase_Negocio.P_Monto_Total), String.Empty, String.Empty, String.Empty, Negocio_Parametros.P_Partida_ID, Cmd);
                                }
                                //Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales(No_Reserva.ToString(), Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido, Dt_Registros);
                                ////Registrar movimiento presupuestal
                                //Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(No_Reserva.ToString(), Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido, Convert.ToDouble(Clase_Negocio.P_Monto_Total), String.Empty, String.Empty, String.Empty, Negocio_Parametros.P_Partida_ID, ref Cmd);
                            }
                            else
                            {
                                //En caso de ser listado de almacen afectar solo los montos de la requisicion
                                Modificar_Montos_Cotizados_Requisicion(Clase_Negocio);
                                Mensaje = "Se realizo la Cancelacion Parcial Satisfactoriamente";
                            }
                            //Modificamos el estatus de la Requisicion
                            Mi_SQL = "UPDATE " + Ope_Tal_Ordenes_Compra.Tabla_Ope_Tal_Ordenes_Compra;
                            Mi_SQL += " SET " + Ope_Tal_Ordenes_Compra.Campo_Estatus;
                            Mi_SQL += "='" + Clase_Negocio.P_Estatus.Trim() + "'";
                            Mi_SQL += ", " + Ope_Tal_Ordenes_Compra.Campo_Fecha_Cancelacion;
                            Mi_SQL += "=GETDATE()";
                            Mi_SQL += ", " + Ope_Tal_Ordenes_Compra.Campo_Motivo_Cancelacion;
                            Mi_SQL += "='" + Clase_Negocio.P_Motivo_Cancelacion + "'";
                            Mi_SQL += " WHERE " + Ope_Tal_Ordenes_Compra.Campo_No_Orden_Compra;
                            Mi_SQL += "='" + Clase_Negocio.P_No_Orden_Compra.Trim() + "'";
                            Cmd.CommandText = Mi_SQL;
                            Cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            Mensaje = "El presupuesto no permite realizar la cancelacion, es insuficiente";
                        }
                    }//fin Clase_Negocio.P_Listado_Almacen == null
                }//fin del if Cancelacion Parcial
                Cls_Ope_Tal_Orden_Compra_Datos.Registrar_Movimiento_Historial(Clase_Negocio.P_No_Orden_Compra, Clase_Negocio.P_Usuario, "CANCELADA", ref Cmd);
                Trans.Commit();
                Mensaje = "La cancelacion se realizo satisfactoriamente.";
            }
            catch (Exception EX)
            {
                Mensaje = EX.Message;
                Trans.Rollback();
            }
            finally
            {
                Cn.Close();
                Cn = null;
                Cmd = null;
                Trans = null;
            }
            return Mensaje;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultamos_Presupuesto_Existente
        ///DESCRIPCIÓN: Metodo que consulta si existe presupuesto para los productos seleccionados
        ///PARAMETROS:  1.- Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio objeto de la clase negocio
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 25/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static bool Consultamos_Presupuesto_Existente(Cls_Ope_Tal_Cancelar_Ordenes_Compra_Negocio Requisicion_Negocio)
        {
            bool Existe_Presupuesto = false;
            String Mi_SQL = "";
            //Primero Obtenemos todos lo productos pertenecientes a la requisicion:
            Mi_SQL = "SELECT REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Partida_ID +
            ",REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID +
            ", (SELECT " + Ope_Tal_Requisiciones.Campo_Total +
            " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
            " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
            "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS TOTAL" +
             ", (SELECT " + Ope_Tal_Requisiciones.Campo_Total_Cotizado +
             " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
             " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
             "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS TOTAL_COTIZADO" +
             ", (SELECT " + Ope_Tal_Requisiciones.Campo_Dependencia_ID +
             " FROM " + Ope_Tal_Requisiciones.Tabla_Ope_Tal_Requisiciones +
             " WHERE " + Ope_Tal_Requisiciones.Campo_Requisicion_ID +
             "= REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + ") AS DEPENDENCIA_ID" +
             ", REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID +
             " FROM " + Ope_Tal_Req_Refaccion.Tabla_Ope_Tal_Req_Refaccion + " REQ_DET" +
             " WHERE REQ_DET." + Ope_Tal_Req_Refaccion.Campo_Requisicion_ID + "='" + Requisicion_Negocio.P_No_Requisicio + "'";
            DataTable Dt_Requisicion = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            //Guardamos en la variable Monto_Restante la diferencia de Total_Cotizado y Monto_Total inicial del producto
            //Primero checamos si se va a restar o a aumentar el presupuesto
            double Monto_Cotizado = double.Parse(Dt_Requisicion.Rows[0]["TOTAL_COTIZADO"].ToString().Trim());
            double Monto_Anterior = double.Parse(Dt_Requisicion.Rows[0]["TOTAL"].ToString().Trim());
            double Diferencia = 0;
            double Disponible = 0;
            String Partida_ID = Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Partida_ID].ToString().Trim();
            String Proyecto_ID = Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Proyecto_Programa_ID].ToString().Trim();
            String Dependencia_ID = Dt_Requisicion.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();
            String FF = Dt_Requisicion.Rows[0][Ope_Tal_Req_Refaccion.Campo_Fuente_Financiamiento_ID].ToString().Trim();

            // PASO 1 VERIFICAMOS CUAL DE LOS 2 MONTOS ES MAYOR SI EL COTIZADO O  EL ANTERIOR
            if (Monto_Anterior > Monto_Cotizado)
            {
                //Obtenemos la resta
                Diferencia = Monto_Anterior - Monto_Cotizado;
                Mi_SQL = "";
                //Consultamos el Monto presupuestal para ver si es suficiente 
                Mi_SQL = "SELECT " + Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible;
                Mi_SQL += " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                Mi_SQL += " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID;
                Mi_SQL += "='" + Partida_ID + "'";
                Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID;
                Mi_SQL += "='" + Proyecto_ID + "'";
                Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Fuente_Financiamiento_ID;
                Mi_SQL += "='" + FF + "'";
                Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio;
                Mi_SQL += " = (SELECT MAX(" + Cat_Com_Dep_Presupuesto.Campo_No_Asignacion_Anio + ")";
                Mi_SQL += " FROM " + Cat_Com_Dep_Presupuesto.Tabla_Cat_Com_Dep_Presupuesto;
                Mi_SQL += " WHERE " + Cat_Com_Dep_Presupuesto.Campo_Partida_ID;
                Mi_SQL += "='" + Partida_ID + "'";
                Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Proyecto_Programa_ID;
                Mi_SQL += "='" + Proyecto_ID + "'";
                Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID;
                Mi_SQL += "='" + Dependencia_ID + "'";
                Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto;
                Mi_SQL += "= TO_CHAR(GETDATE(),'YYYY'))";
                Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Anio_Presupuesto;
                Mi_SQL += "= TO_CHAR(GETDATE(),'YYYY')";
                Mi_SQL += " AND " + Cat_Com_Dep_Presupuesto.Campo_Dependencia_ID;
                Mi_SQL += "='" + Dependencia_ID + "'";

                //Sentencia que ejecuta el query
                DataTable Dt_Presupuestos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                Disponible = double.Parse(Dt_Presupuestos.Rows[0][Cat_Com_Dep_Presupuesto.Campo_Monto_Disponible].ToString().Trim());
                if (Disponible >= Diferencia)
                    Existe_Presupuesto = true;
                else
                    Existe_Presupuesto = false;
            }
            else
                Existe_Presupuesto = true; //Existe presupuesto ya que el monto cotizado es menor que el k se aparto 
            return Existe_Presupuesto;
        }

    }//fin del class
}//fin del namespace