﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Catalogo_Niveles_Mecanico.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;

namespace JAPAMI.Taller_Mecanico.Catalogo_Niveles_Mecanico.Datos
{
    public class Cls_Cat_Tal_Niveles_Mecanico_Datos
    {
        public Cls_Cat_Tal_Niveles_Mecanico_Datos()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Nivel
        ///DESCRIPCIÓN          : Da de alta en la Base de Datos una nuevo registro
        ///PARAMETROS           : Objeto de Negocio para la insercion
        ///CREO                 : Jesus Toledo Rdz.
        ///FECHA_CREO           : 07/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Alta_Nivel(Cls_Cat_Tal_Niveles_Mecanico_Negocio Parametros)
        {
            String Mi_SQL = "";
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlDataAdapter Obj_Adaptador = new SqlDataAdapter();//Adapatador para el llenado de las tablas
            Object Aux; //Variable auxiliar para las consultas
            DataTable Dt_Aux = new DataTable(); //Tabla auxiliar para las consultas
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            
            try
            {

                //Formar Sentencia para obtener el consecutivo
                Mi_SQL = "";
                Mi_SQL = "SELECT ISNULL(MAX(";
                Mi_SQL = Mi_SQL + Cat_Tal_Niveles_Mecanicos.Campo_Nivel_ID + "),00000)";
                Mi_SQL = Mi_SQL + " FROM ";
                Mi_SQL = Mi_SQL + Cat_Tal_Niveles_Mecanicos.Tabla_Cat_Tal_Niveles_Mecanicos;

                //Ejecutar consulta
                Dt_Aux = new DataTable();
                Cmd.CommandText = Mi_SQL;
                Obj_Adaptador.SelectCommand = Cmd;
                Obj_Adaptador.Fill(Dt_Aux);

                //Verificar si la consulta arrojo resultado
                if (Dt_Aux.Rows.Count > 0)
                {
                    Parametros.P_Nivel_ID = String.Format("{0:00000}", Convert.ToInt32(Dt_Aux.Rows[0][0]) + 1);// Colocar los valores en las variables                    
                }
                else
                {
                    Parametros.P_Nivel_ID = "00001";
                }
                Parametros.P_Clave = Convert.ToInt32(Parametros.P_Nivel_ID).ToString();
                Mi_SQL = "INSERT INTO " + Cat_Tal_Niveles_Mecanicos.Tabla_Cat_Tal_Niveles_Mecanicos + " (" + Cat_Tal_Niveles_Mecanicos.Campo_Nivel_ID;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Niveles_Mecanicos.Campo_Estatus;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Niveles_Mecanicos.Campo_Nombre;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tipos_Refacciones.Campo_Clave;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tipos_Refacciones.Campo_Descripcion;
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Niveles_Mecanicos.Campo_Usuario_Creo + ", " + Cat_Tal_Niveles_Mecanicos.Campo_Fecha_Creo;
                Mi_SQL = Mi_SQL + ") VALUES ('" + Parametros.P_Nivel_ID + "', '" + Parametros.P_Estatus + "','" + Parametros.P_Nombre + "','" + Parametros.P_Clave + "','" + Parametros.P_Descripcion + "'";
                Mi_SQL = Mi_SQL + ", '" + Parametros.P_Usuario + "', GETDATE())";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar dar de Alta. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Mecanico
        ///DESCRIPCIÓN          : Modifica en la Base de Datos el registro indicado
        ///PARAMETROS           : Objeto de Negocio para la modificacion
        ///CREO                 : Jesus Toledo Rdz.
        ///FECHA_CREO           : 07/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Modificar_Nivel_Mecanico(Cls_Cat_Tal_Niveles_Mecanico_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                String Mi_SQL = "UPDATE " + Cat_Tal_Niveles_Mecanicos.Tabla_Cat_Tal_Niveles_Mecanicos;
                Mi_SQL = Mi_SQL + " SET " + Cat_Tal_Niveles_Mecanicos.Campo_Nombre + " = '" + Parametros.P_Nombre + "'";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Niveles_Mecanicos.Campo_Estatus + " = '" + Parametros.P_Estatus + "'"; 
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Tipos_Refacciones.Campo_Descripcion + " = '" + Parametros.P_Descripcion + "'";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Niveles_Mecanicos.Campo_Usuario_Modifico + " = '" + Parametros.P_Usuario + "'";
                Mi_SQL = Mi_SQL + ", " + Cat_Tal_Niveles_Mecanicos.Campo_Fecha_Modifico + " = GETDATE()";
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Niveles_Mecanicos.Campo_Nivel_ID + " = '" + Parametros.P_Nivel_ID + "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();
                Trans.Commit();
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar Modificar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar_Nivel_Mecanico
        ///DESCRIPCIÓN          : Elimina en la Base de Datos el registro indicado
        ///PARAMETROS           : Objeto de Negocio para la modificacion
        ///CREO                 : Jesus Toledo Rdz.
        ///FECHA_CREO           : 07/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static void Eliminar_Nivel_Mecanico(Cls_Cat_Tal_Niveles_Mecanico_Negocio Parametros)
        {
            String Mensaje = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            Boolean Eliminar = true;
            try {
                String Mi_SQL = "SELECT * FROM " + Cat_Tal_Mecanicos.Tabla_Cat_Tal_Mecanicos + " WHERE " + Cat_Tal_Mecanicos.Campo_Nivel_ID + " = '" + Parametros.P_Nivel_ID + "'";
                DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (Ds_Datos != null) {
                    if (Ds_Datos.Tables.Count > 0) {
                        DataTable Dt_Datos = Ds_Datos.Tables[0];
                        if (Dt_Datos != null) {
                            if (Dt_Datos.Rows.Count > 0) {
                                Eliminar = false;
                            }
                        }
                    }
                }

                if (Eliminar) {
                    Mi_SQL = "DELETE " + Cat_Tal_Niveles_Mecanicos.Tabla_Cat_Tal_Niveles_Mecanicos;
                    Mi_SQL = Mi_SQL + " WHERE " + Cat_Tal_Niveles_Mecanicos.Campo_Nivel_ID + " = '" + Parametros.P_Nivel_ID + "'";
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    Trans.Commit();
                } else {
                    throw new Exception("No puede eliminarse el nivel ya que se encuentra asignado a un Mecanico.");
                }
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                //variable para el mensaje 
                //configuracion del mensaje de acuerdo al numero de error devuelto por la MRDB 
                if (Ex.Number == 8152)
                {
                    Mensaje = "Existen datos demasiados extensos, corrija el problema y vuelva a intentar";
                }
                else if (Ex.Number == 2627)
                {
                    if (Ex.Message.IndexOf("PRIMARY") != -1)
                    {
                        Mensaje = "Error por intentar grabar valores duplicados en campos clave, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                    }
                    else if (Ex.Message.IndexOf("UNIQUE") != -1)
                    {
                        Mensaje = "Esta intentando grabar un registro que ya existe, verifiquelo por favor. Error: [" + Ex.Message + "]";
                    }
                    else
                    {
                        Mensaje = "Error general en la base de datos. Error: [" + Ex.Message + "]";
                    }
                }
                else if (Ex.Number == 547)
                {
                    Mensaje = "Esta intentando introducir algún dato que no existe y que esta relacionado con otra tabla. Error: [" + Ex.Message + "]";
                }
                else if (Ex.Number == 515)
                {
                    Mensaje = "Algunos datos no han sido ingresados y son necesarios para completar la operación, corrija el problema y vuelva a intentar. Error: [" + Ex.Message + "]";
                }
                else
                {
                    Mensaje = "Error al intentar Modificar. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                }
                //Indicamos el mensaje 
                throw new Exception(Mensaje);
            }
            finally
            {
                Cn.Close();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Nivel_Mecanicos
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : Objeto de Negocio para la consulta
        ///CREO                 : Jesus Toledo Rdz
        ///FECHA_CREO           : 07/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Nivel_Mecanicos(Cls_Cat_Tal_Niveles_Mecanico_Negocio Parametros)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            Boolean Entro_Where = false;
            try
            {
                Mi_SQL = "SELECT " + Cat_Tal_Niveles_Mecanicos.Campo_Nivel_ID;
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Niveles_Mecanicos.Campo_Nombre;
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Niveles_Mecanicos.Campo_Estatus;
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Tipos_Refacciones.Campo_Clave;
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Tipos_Refacciones.Campo_Descripcion;
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Niveles_Mecanicos.Campo_Usuario_Creo;
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Niveles_Mecanicos.Campo_Fecha_Creo;
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Niveles_Mecanicos.Campo_Usuario_Modifico;
                Mi_SQL = Mi_SQL + "," + Cat_Tal_Niveles_Mecanicos.Campo_Fecha_Modifico;

                Mi_SQL = Mi_SQL + " FROM " + Cat_Tal_Niveles_Mecanicos.Tabla_Cat_Tal_Niveles_Mecanicos;

                if (!String.IsNullOrEmpty(Parametros.P_Nivel_ID))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " " + Cat_Tal_Niveles_Mecanicos.Campo_Nivel_ID + " = '" + Parametros.P_Nivel_ID + "'";
                }
                if (!String.IsNullOrEmpty(Parametros.P_Estatus))
                {
                    if (Entro_Where) { Mi_SQL += " AND "; } else { Mi_SQL += " WHERE "; Entro_Where = true; }
                    Mi_SQL = Mi_SQL + " " + Cat_Tal_Niveles_Mecanicos.Campo_Estatus + " = '" + Parametros.P_Estatus + "'";
                }
                
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Tal_Niveles_Mecanicos.Campo_Nombre + " ASC";
                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Mecanico
        ///DESCRIPCIÓN          : Obtiene datos de la Base de Datos y los regresa en un DataTable.
        ///PARAMETROS           : Objeto de Negocio para la consulta
        ///CREO                 : Jesus Toledo Rdz
        ///FECHA_CREO           : 07/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Cls_Cat_Tal_Niveles_Mecanico_Negocio Consultar_Detalles_Mecanico(Cls_Cat_Tal_Niveles_Mecanico_Negocio Parametros)
        {
            String Mi_SQL = null;
            Cls_Cat_Tal_Niveles_Mecanico_Negocio Obj_Cargado = new Cls_Cat_Tal_Niveles_Mecanico_Negocio();
            try
            {
                Mi_SQL = "SELECT * FROM " + Cat_Tal_Niveles_Mecanicos.Tabla_Cat_Tal_Niveles_Mecanicos + " WHERE " + Cat_Tal_Niveles_Mecanicos.Campo_Nivel_ID + " = '" + Parametros.P_Nivel_ID + "'";
                SqlDataReader Reader = SqlHelper.ExecuteReader(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                while (Reader.Read())
                {
                    Obj_Cargado.P_Nivel_ID = (!String.IsNullOrEmpty(Reader[Cat_Tal_Niveles_Mecanicos.Campo_Nivel_ID].ToString())) ? Reader[Cat_Tal_Niveles_Mecanicos.Campo_Nivel_ID].ToString() : "";
                    Obj_Cargado.P_Nombre = (!String.IsNullOrEmpty(Reader[Cat_Tal_Niveles_Mecanicos.Campo_Nombre].ToString())) ? Reader[Cat_Tal_Niveles_Mecanicos.Campo_Nombre].ToString() : "";
                    Obj_Cargado.P_Estatus = (!String.IsNullOrEmpty(Reader[Cat_Tal_Niveles_Mecanicos.Campo_Estatus].ToString())) ? Reader[Cat_Tal_Niveles_Mecanicos.Campo_Estatus].ToString() : "";
                    Obj_Cargado.P_Clave = (!String.IsNullOrEmpty(Reader[Cat_Tal_Niveles_Mecanicos.Campo_Clave].ToString())) ? Reader[Cat_Tal_Niveles_Mecanicos.Campo_Clave].ToString() : "";
                    Obj_Cargado.P_Descripcion = (!String.IsNullOrEmpty(Reader[Cat_Tal_Niveles_Mecanicos.Campo_Descripcion].ToString())) ? Reader[Cat_Tal_Niveles_Mecanicos.Campo_Descripcion].ToString() : "";  
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Obj_Cargado;
        }
    }
}