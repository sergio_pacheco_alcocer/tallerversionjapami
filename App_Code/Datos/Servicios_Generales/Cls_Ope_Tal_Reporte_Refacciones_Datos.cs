﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Taller_Mecanico.Reporte.Refacciones.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;


namespace JAPAMI.Taller_Mecanico.Reporte.Refacciones.Datos
{

    public class Cls_Ope_Tal_Reporte_Refacciones_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Reporte
        ///DESCRIPCIÓN          : Obtiene los datos especificados con los campos dinamicos y filtros dinamicos
        ///PARAMETROS           : Datos, instancia de Negocio
        ///CREO                 : Jesus Toledo Rodriguez
        ///FECHA_CREO           : 25/Junio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Datos_Reporte(Cls_Ope_Tal_Rep_Refacciones_Negocio Datos)
        {
            DataTable Tabla = new DataTable();
            String Mi_SQL ="";
            String Mi_SQL_Campos_Foraneos = "";
            try
            {
                Mi_SQL = " SELECT ";
                if (!String.IsNullOrEmpty(Datos.P_Campos_Dinamicos))
                {
                    Mi_SQL = Mi_SQL + Datos.P_Campos_Dinamicos;
                }

                if (!String.IsNullOrEmpty( Datos.P_Campos_Foraneos))
                {
                    Mi_SQL = Mi_SQL + Datos.P_Campos_Foraneos;
                }
                
                Mi_SQL += " FROM " + Datos.P_Tabla_Consultar;
                if (Datos.P_Join != null && Datos.P_Join != "")
                {
                    Mi_SQL += " " + Datos.P_Join;
                }
                if (Datos.P_Filtros_Dinamicos != null && Datos.P_Filtros_Dinamicos != "")
                {
                    Mi_SQL += " WHERE " + Datos.P_Filtros_Dinamicos;
                }

                if (Datos.P_Unir_Tablas != null && Datos.P_Unir_Tablas != "")
                {
                    Mi_SQL += " UNION " + Datos.P_Unir_Tablas;
                }

                if (Datos.P_Ordenar_Dinamico != null && Datos.P_Ordenar_Dinamico != "")
                {
                    Mi_SQL += " ORDER BY " + Datos.P_Ordenar_Dinamico;
                }
                DataSet dataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataSet != null)
                {
                    Tabla = dataSet.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de la Cuentas. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Tabla;
        }
    }
}