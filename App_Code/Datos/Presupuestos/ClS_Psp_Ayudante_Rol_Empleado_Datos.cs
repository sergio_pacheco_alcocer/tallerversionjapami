﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;

namespace JAPAMI.Psp_Rol_Empleado.Datos
{
    public class ClS_Psp_Ayudante_Rol_Empleado_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Unidad_Responsable
        ///DESCRIPCIÓN          : consulta para obtener los datos de las unidad responsable
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Unidad_Responsable()
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS NOMBRE, ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_Sql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Estatus + " = 'ACTIVO'");
                Mi_Sql.Append(" ORDER BY NOMBRE ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las unidades responsables. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Empleado
        ///DESCRIPCIÓN          : Consulta para modificar los datos del empleado
        ///PARAMETROS           1 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Boolean Modificar_Empleado(String Rol_ID, String Empleado_Id)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            Boolean Operacion_Exitosa = false;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                Mi_SQL.Append("UPDATE " + Cat_Empleados.Tabla_Cat_Empleados);
                Mi_SQL.Append(" SET " + Cat_Empleados.Campo_Rol_ID + " = '" + Rol_ID + "'");
                Mi_SQL.Append(" WHERE " + Cat_Empleados.Campo_Empleado_ID + " = '" + Empleado_Id + "'");

                Cmd.CommandText = Mi_SQL.ToString();
                Cmd.ExecuteNonQuery();

                Trans.Commit();
                Operacion_Exitosa = true;
            }
            catch (Exception Ex)
            {
                Operacion_Exitosa = false;
                Trans.Rollback();
                throw new Exception("Error al intentar modificar los registros. Error: [" + Ex.Message + "]");
            }
            return Operacion_Exitosa;
        }
    }
}