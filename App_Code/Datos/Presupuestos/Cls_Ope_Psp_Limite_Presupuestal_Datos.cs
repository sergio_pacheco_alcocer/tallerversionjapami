﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Text;
using JAPAMI.Limite_Presupuestal.Negocio;
using JAPAMI.Sessiones;

namespace JAPAMI.Limite_Presupuestal.Datos
{
    public class Cls_Ope_Psp_Limite_Presupuestal_Datos
    {
        #region MÉTODOS
            //*******************************************************************************
            // NOMBRE DE LA FUNCIÓN: Consultar_Unidades_Asignadas
            // DESCRIPCIÓN:  
            // RETORNA: 
            // CREO: Gustavo Angeles Cruz
            // FECHA_CREO: 30/Agosto/2010 
            // MODIFICO:
            // FECHA_MODIFICO
            // CAUSA_MODIFICACIÓN   
            // *******************************************************************************/
            public static DataTable Consultar_Unidades_Asignadas(Cls_Ope_Psp_Limite_Presupuestal_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                DataTable Dt_Unidades = new DataTable();
                DataSet Ds_Unidades = new DataSet();
                try
                {
                    Mi_Sql.Append("SELECT " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Limite_Presupuestal + ", ");
                    Mi_Sql.Append("ISNULL("+Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID + ", '') AS DEPENDENCIA_ID, ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Fte_Financiamiento_ID + ", ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Empleado_ID + ", '') AS EMPLEADO_ID, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Proyecto_Programa_ID + ", '') AS PROYECTO_PROGRAMA_ID, ");
                    Mi_Sql.Append(" (CASE WHEN " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID + " IS NULL");
                    Mi_Sql.Append(" THEN " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_No_Empleado + " +' '+ ");
                    Mi_Sql.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + " +' '+ ");
                    Mi_Sql.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + " +' '+ ");
                    Mi_Sql.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno);
                    Mi_Sql.Append(" ELSE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " END) AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS Fte_Financiamiento ");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID );
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID );
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados );
                    Mi_Sql.Append(" ON " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Empleado_ID);
                    Mi_Sql.Append(" = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento );
                    Mi_Sql.Append(" ON " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal);
                    Mi_Sql.Append(" = " + Negocio.P_Anio_Presupuestal);
                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    Ds_Unidades = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());

                    if (Ds_Unidades != null && Ds_Unidades.Tables[0].Rows.Count > 0)
                    {
                        Dt_Unidades = Ds_Unidades.Tables[0];
                    }
                    else 
                    {
                        Dt_Unidades = Consultar_Unidades_Asignadas_Anteriores(Negocio.P_Anio_Presupuestal);
                    }

                    return Dt_Unidades;
                }
                catch (Exception Ex)
                {
                    Ex.ToString();
                    return null;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Capitulos_Asignados_A_Unidad_Asignada
            ///DESCRIPCIÓN          : consulta para obtener los capitulos de una unidad responsable
            ///PARAMETROS           1 Negocio conexion con la capa de negocio 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 08/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consultar_Capitulos_Asignados_A_Unidad_Asignada(Cls_Ope_Psp_Limite_Presupuestal_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Ope_Psp_Detalle_Lim_Presup.Campo_Capitulo_ID);
                    Mi_Sql.Append(" FROM " + Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Detalle_Lim_Presup.Campo_Anio_presupuestal + " = '" + Negocio.P_Anio_Presupuestal + "'");
                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Detalle_Lim_Presup.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID + "'");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Detalle_Lim_Presup.Campo_Empleado_ID + " = '" + Negocio.P_Empleado_ID + "'");
                    }

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    Ex.ToString();
                    return null;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Unidades_Responsables_Sin_Asignar
            ///DESCRIPCIÓN          : consulta para obtener las unidades responsables que no estan asignadas
            ///PARAMETROS           1 Negocio conexion con la capa de negocio 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 09/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consultar_Unidades_Responsables_Sin_Asignar(Cls_Ope_Psp_Limite_Presupuestal_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                DataTable Dt_Unidades_Sin_Asignar = new DataTable();
                DataTable Dt_Unidades_Asignadas = new DataTable();
                Int32 Contador_Fila;
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    Mi_Sql.Append("SELECT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS NOMBRE, ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia);
                    Mi_Sql.Append(" ON " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia + "." + Cat_SAP_Det_Prog_Dependencia.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Estatus + " = 'ACTIVO'");
                    
                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_Clave)) 
                    {
                        Mi_Sql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave 
                            + " = '" + Negocio.P_Dependencia_Clave + "' ");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID))
                    {
                        Mi_Sql.Append(" AND " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia
                            + "." + Cat_SAP_Det_Prog_Dependencia.Campo_Proyecto_Programa_ID
                            + " = '" + Negocio.P_Programa_ID + "' ");
                    }

                    Mi_Sql.Append(" ORDER BY NOMBRE ASC");
                    
                    Dt_Unidades_Sin_Asignar = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

                    if (Negocio.P_Accion.Equals("Sin_Asignar")) {
                        //OBTENEMOS LAS UNIDADES RESPONSABLES QUE YA ESTAN ASIGNADAS A UN AÑO PRESUPUESTAL
                        Mi_Sql = new StringBuilder();
                        Mi_Sql.Append("SELECT " + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID);
                        Mi_Sql.Append(" FROM " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal);
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal + " = '" + Negocio.P_Anio_Presupuestal + "'");

                        Dt_Unidades_Asignadas = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

                        //RECORREMOS EL DATATABLE DE LAS UNIDADES SIN ASIGNAR PARA ELIMINAR LOS REGISTROS DE LAS UNIDADES 
                        // QUE YA ESTAN ASIGNADAS
                        if (Dt_Unidades_Asignadas != null)
                        {
                            if (Dt_Unidades_Asignadas.Rows.Count > 0)
                            {
                                if (Dt_Unidades_Sin_Asignar != null)
                                {
                                    if (Dt_Unidades_Sin_Asignar.Rows.Count > 0)
                                    {
                                        foreach (DataRow Dr_Unidades_Asignadas in Dt_Unidades_Asignadas.Rows)
                                        {
                                            Contador_Fila = -1;
                                            foreach (DataRow Dr_Unidades_Sin_Asignar in Dt_Unidades_Sin_Asignar.Rows)
                                            {
                                                Contador_Fila++;
                                                if (Dr_Unidades_Asignadas["DEPENDENCIA_ID"].ToString().Equals(Dr_Unidades_Sin_Asignar["DEPENDENCIA_ID"].ToString()))
                                                {
                                                    Dt_Unidades_Sin_Asignar.Rows.RemoveAt(Contador_Fila);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return Dt_Unidades_Sin_Asignar;
                }
                catch (Exception Ex)
                {
                    Ex.ToString();
                    return null;
                }
            }

            ///*******************************************************************************
            // NOMBRE DE LA FUNCIÓN: Guardar_Limites
            // DESCRIPCIÓN:
            // RETORNA: 
            // CREO: Gustavo Angeles Cruz
            // FECHA_CREO: 30/Agosto/2010 
            // MODIFICO:
            // FECHA_MODIFICO
            // CAUSA_MODIFICACIÓN   
            // *******************************************************************************/
            public static String Guardar_Limites(Cls_Ope_Psp_Limite_Presupuestal_Negocio Negocio)
            {
                String Mensaje = "EXITO";
                String Fecha_Creo = DateTime.Now.ToString("dd/MM/yy").ToUpper();
                //String Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                String Mi_SQL = "";
                //INSERTAR LA REQUISICION   
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try
                {
                    Mi_SQL = "INSERT INTO " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + " (";
                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        Mi_SQL = Mi_SQL + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID + ", ";
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        Mi_SQL = Mi_SQL + Ope_Psp_Limite_presupuestal.Campo_Proyecto_Programa_ID + ", ";
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                    {
                        Mi_SQL = Mi_SQL + Ope_Psp_Limite_presupuestal.Campo_Empleado_ID+ ", ";
                    }
                    Mi_SQL = Mi_SQL + Ope_Psp_Limite_presupuestal.Campo_Limite_Presupuestal + ", ";
                    Mi_SQL = Mi_SQL + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal + ", ";
                    Mi_SQL = Mi_SQL + Ope_Psp_Limite_presupuestal.Campo_Fte_Financiamiento_ID + ", ";
                    Mi_SQL = Mi_SQL + Ope_Psp_Limite_presupuestal.Campo_Fecha_Creo + ", ";
                    Mi_SQL = Mi_SQL + Ope_Psp_Limite_presupuestal.Campo_Usuario_Creo + ") VALUES (";
                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        Mi_SQL = Mi_SQL + "'" + Negocio.P_Dependencia_ID + "', ";
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        Mi_SQL = Mi_SQL + "'" + Negocio.P_Programa_ID + "', ";
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                    {
                        Mi_SQL = Mi_SQL + "'" + Negocio.P_Empleado_ID + "', ";
                    }
                    Mi_SQL = Mi_SQL + Negocio.P_Limite_Presupuestal + ", ";
                    Mi_SQL = Mi_SQL + Negocio.P_Anio_Presupuestal + ", '";
                    Mi_SQL = Mi_SQL + Negocio.P_Fuente_Financiamiento_ID + "', GETDATE(), ";
                    Mi_SQL = Mi_SQL + "'" + Cls_Sessiones.Nombre_Empleado + "')";

                  
                    Cmd.CommandText = Mi_SQL;
                    Cmd.ExecuteNonQuery();
                    foreach (DataRow Renglon in Negocio.P_Dt_Capitulos.Rows)
                    {
                        Mi_SQL = "INSERT INTO " + Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal + " (";
                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {
                            Mi_SQL = Mi_SQL + Ope_Psp_Detalle_Lim_Presup.Campo_Dependencia_ID + ", ";
                        }
                        if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                        {
                            Mi_SQL = Mi_SQL + Ope_Psp_Detalle_Lim_Presup.Campo_Empleado_ID + ", ";
                        }
                        Mi_SQL = Mi_SQL + Ope_Psp_Detalle_Lim_Presup.Campo_Capitulo_ID + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal + ") VALUES (";
                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {
                            Mi_SQL = Mi_SQL + "'" + Negocio.P_Dependencia_ID + "'," ;
                        }
                        if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim().Trim()))
                        {
                            Mi_SQL = Mi_SQL + "'" + Negocio.P_Empleado_ID + "',";
                        }

                        Mi_SQL = Mi_SQL + "'" + Renglon["CAPITULO_ID"].ToString().Trim() + "',";
                        Mi_SQL = Mi_SQL + Negocio.P_Anio_Presupuestal + ")";

                        Cmd.CommandText = Mi_SQL;
                        Cmd.ExecuteNonQuery();
                    }
                    Trans.Commit();
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    Trans.Rollback();
                    Mensaje = "No se pudo guardar requisición, consulte a su administrador";
                }
                finally
                {
                    Cn.Close();
                }        
                return Mensaje;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Actualizar_Limites
            ///DESCRIPCIÓN          : consulta para actualizar los datos de una unidad responsable
            ///PARAMETROS           1 Negocio conexion con la capa de negocio 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 08/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Boolean Actualizar_Limites(Cls_Ope_Psp_Limite_Presupuestal_Negocio Negocio)
            {
                Boolean Operacion_Exitosa = false;
                StringBuilder Mi_Sql = new StringBuilder();
                  
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try
                {
                    //MODIFICAMOS LOS DATOS DE LA UNIDAD RESPONSABLE
                    Mi_Sql.Append("UPDATE " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal);
                    Mi_Sql.Append(" SET " + Ope_Psp_Limite_presupuestal.Campo_Limite_Presupuestal + " = '" + Negocio.P_Limite_Presupuestal + "' ,");
                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Campo_Proyecto_Programa_ID + " = '" + Negocio.P_Programa_ID + "' ,");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                    {
                        Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Campo_Fte_Financiamiento_ID+ " = '" + Negocio.P_Fuente_Financiamiento_ID + "' ,");
                    }
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "' ,");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Campo_Fecha_Modifico + " = GETDATE()");
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal + " = '" + Negocio.P_Anio_Presupuestal + "'");
                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID + "'");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Limite_presupuestal.Campo_Empleado_ID + " = '" + Negocio.P_Empleado_ID + "'");
                    }

                    Cmd.CommandText = Mi_Sql.ToString();
                    Cmd.ExecuteNonQuery();

                    Mi_Sql = new StringBuilder();
                    //ELIMINAMOS LOS CAPITULOS ASIGNADOS ANTERIORMETE A ESTA UNIDAD RESPONSABLE
                    Mi_Sql.Append(" DELETE " + Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Detalle_Lim_Presup.Campo_Anio_presupuestal + " = '" + Negocio.P_Anio_Presupuestal + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Detalle_Lim_Presup.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID + "'");
                    Cmd.CommandText = Mi_Sql.ToString();
                    Cmd.ExecuteNonQuery();

                    //INSETAMOS LOS NUEVOS CAPITULOS ASIGNADOS A LA UNIDAD RESPONSABLE
                    foreach (DataRow Renglon in Negocio.P_Dt_Capitulos.Rows)
                    {
                        Mi_Sql = new StringBuilder();
                        Mi_Sql.Append("INSERT INTO " + Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal + " (");
                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {
                            Mi_Sql.Append(Ope_Psp_Detalle_Lim_Presup.Campo_Dependencia_ID + ", ");
                        }
                        if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                        {
                            Mi_Sql.Append(Ope_Psp_Detalle_Lim_Presup.Campo_Empleado_ID + ", ");
                        }
                        Mi_Sql.Append(Ope_Psp_Detalle_Lim_Presup.Campo_Capitulo_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal + ") VALUES (");
                        if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                        {
                            Mi_Sql.Append("'" + Negocio.P_Dependencia_ID + "', ");
                        }
                        if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                        {
                            Mi_Sql.Append("'" + Negocio.P_Empleado_ID + "', ");
                        }
                        Mi_Sql.Append("'" + Renglon["CAPITULO_ID"].ToString().Trim() + "',");
                        Mi_Sql.Append(Negocio.P_Anio_Presupuestal + ")");

                        Cmd.CommandText = Mi_Sql.ToString();
                        Cmd.ExecuteNonQuery();
                    }
                    Trans.Commit();
                    Operacion_Exitosa = true;
                }
                catch(Exception ex){
                    Operacion_Exitosa = false;
                    Trans.Rollback();
                    throw new Exception("Error al tratar de actualizar los datos de la unidad responsable Error[" + ex.Message + "]");
                }
                finally
                {
                    Cn.Close();
                }        
                return Operacion_Exitosa;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Eliminar_Limites
            ///DESCRIPCIÓN          : consulta para eliminar los datos de una unidad responsable
            ///PARAMETROS           1 Negocio conexion con la capa de negocio 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 09/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static Boolean Eliminar_Limites(Cls_Ope_Psp_Limite_Presupuestal_Negocio Negocio)
            {
                Boolean Operacion_Exitosa = false;
                StringBuilder Mi_Sql = new StringBuilder();

                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                try
                {
                    Mi_Sql = new StringBuilder();
                    //ELIMINAMOS LOS CAPITULOS ASIGNADOS ANTERIORMETE A ESTA UNIDAD RESPONSABLE
                    Mi_Sql.Append(" DELETE " + Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Detalle_Lim_Presup.Campo_Anio_presupuestal + " = '" + Negocio.P_Anio_Presupuestal + "'");
                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Detalle_Lim_Presup.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID + "'");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Detalle_Lim_Presup.Campo_Empleado_ID + " = '" + Negocio.P_Empleado_ID + "'");
                    }
                    
                    Cmd.CommandText = Mi_Sql.ToString();
                    Cmd.ExecuteNonQuery();

                    Mi_Sql = new StringBuilder();
                    //ELIMINAMOS LOS DATOS DE LA UNIDAD RESPONSABLE
                    Mi_Sql.Append(" DELETE " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal + " = '" + Negocio.P_Anio_Presupuestal + "'");
                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID + "'");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Limite_presupuestal.Campo_Empleado_ID + " = '" + Negocio.P_Empleado_ID + "'");
                    }
                    Cmd.CommandText = Mi_Sql.ToString();
                    Cmd.ExecuteNonQuery();
                   
                    Trans.Commit();
                    Operacion_Exitosa = true;
                }
                catch (Exception ex)
                {
                    Operacion_Exitosa = false;
                    Trans.Rollback();
                    throw new Exception("Error al tratar de eliminar los datos de la unidad responsable Error[" + ex.Message + "]");
                }
                finally
                {
                    Cn.Close();
                }
                return Operacion_Exitosa;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Programas_Unidades_Responsables
            ///DESCRIPCIÓN          : consulta para obtener los programas de una unidad responsable
            ///PARAMETROS           1 Negocio conexion con la capa de negocio 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 14/Noviembre/2011
            ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
            ///FECHA_MODIFICO       : 05/Septiembre/2012
            ///CAUSA_MODIFICACIÓN   : Se necesito este metodo para verificar el programa en el proceso de carga masiva de
            ///                       la calendarizacion.
            ///*******************************************************************************
            public static DataTable Consultar_Programa_Unidades_Responsables(Cls_Ope_Psp_Limite_Presupuestal_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                DataTable Dt_Programas = new DataTable();
                try
                {
                    Mi_Sql.Append("SELECT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ", ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion+ " AS NOMBRE");
                    Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia);
                    Mi_Sql.Append(" ON " +  Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia + "." + Cat_SAP_Det_Prog_Dependencia.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" WHERE " +  Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia + "." + Cat_SAP_Det_Prog_Dependencia.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                    Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Estatus + " = 'ACTIVO'");
                    if (!String.IsNullOrEmpty(Negocio.P_Programa_Clave)) 
                    {
                        Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " = '" + Negocio.P_Programa_Clave + "' ");
                    }
                    Mi_Sql.Append(" ORDER BY " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas+ "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0]; ;
                }
                catch (Exception Ex)
                {
                    Ex.ToString();
                    return null;
                }
            }

            //*******************************************************************************
            // NOMBRE DE LA FUNCIÓN: Consultar_Unidades_Asignadas_Anteriores
            // DESCRIPCIÓN         :  
            // RETORNA             : 
            // CREO                : LESLIE GONZALEZ VAZQUEZ
            // FECHA_CREO          : 16/MAYO/2010 
            // MODIFICO            :
            // FECHA_MODIFICO      :
            // CAUSA_MODIFICACIÓN  :
            // *******************************************************************************/
            public static DataTable Consultar_Unidades_Asignadas_Anteriores(String Anio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                DataTable Dt_Unidades = new DataTable();
                DataSet Ds_Unidades = new DataSet();
                int Anios = Convert.ToInt32(Anio)-1;
                try
                {
                    //insertamos los datos del limite presupuestal del año anterior con el nuevo año
                    Mi_Sql.Append("INSERT INTO " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "( ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Campo_Limite_Presupuestal + ", ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal + ", ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Campo_Usuario_Creo + ", ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Campo_Fecha_Creo + ", ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Campo_Fte_Financiamiento_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Campo_Proyecto_Programa_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Campo_Empleado_ID+ ") ");
                    Mi_Sql.Append("SELECT " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID + ", ");
                    Mi_Sql.Append(" ISNULL(SUM(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + "),0) AS LIMITE_PRESUPUESTAL, ");
                    Mi_Sql.Append(Anio + ", ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Usuario_Creo + ", ");
                    Mi_Sql.Append("GETDATE(), ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Fte_Financiamiento_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Proyecto_Programa_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Empleado_ID);
                    Mi_Sql.Append(" FROM " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID);
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_Sql.Append(" = " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal + " = " + Anios.ToString());
                    Mi_Sql.Append(" GROUP BY " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Usuario_Creo + ", ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Fte_Financiamiento_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Proyecto_Programa_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Empleado_ID);

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());

                    //insertamos los datos de los detalles del limite presupuestal del año anterior con el nuevo año
                    Mi_Sql = new StringBuilder();
                    Mi_Sql.Append("INSERT INTO " + Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal + "( ");
                    Mi_Sql.Append(Ope_Psp_Detalle_Lim_Presup.Campo_Dependencia_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Detalle_Lim_Presup.Campo_Capitulo_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Detalle_Lim_Presup.Campo_Anio_presupuestal + ", ");
                    Mi_Sql.Append(Ope_Psp_Detalle_Lim_Presup.Campo_Empleado_ID + ") ");
                    Mi_Sql.Append("SELECT " + Ope_Psp_Detalle_Lim_Presup.Campo_Dependencia_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Detalle_Lim_Presup.Campo_Capitulo_ID + ", ");
                    Mi_Sql.Append(Anio + ", ");
                    Mi_Sql.Append(Ope_Psp_Detalle_Lim_Presup.Campo_Empleado_ID);
                    Mi_Sql.Append(" FROM " + Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Detalle_Lim_Presup.Campo_Anio_presupuestal + " = " + Anios.ToString());

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());

                    Mi_Sql = new StringBuilder();
                    Mi_Sql.Append("SELECT " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Limite_Presupuestal + ", ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID + ", '') AS DEPENDENCIA_ID, ");
                    Mi_Sql.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Fte_Financiamiento_ID + ", ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Empleado_ID + ", '') AS EMPLEADO_ID, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Proyecto_Programa_ID + ", '') AS PROYECTO_PROGRAMA_ID, ");
                    Mi_Sql.Append(" (CASE WHEN " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID + " IS NULL");
                    Mi_Sql.Append(" THEN " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_No_Empleado + " +' '+ ");
                    Mi_Sql.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + " +' '+ ");
                    Mi_Sql.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + " +' '+ ");
                    Mi_Sql.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno);
                    Mi_Sql.Append(" ELSE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " END) AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+ ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS Fte_Financiamiento ");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Empleados.Tabla_Cat_Empleados);
                    Mi_Sql.Append(" ON " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Empleado_ID);
                    Mi_Sql.Append(" = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" ON " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal);
                    Mi_Sql.Append(" = " + Anio);
                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    Ds_Unidades = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());

                    if (Ds_Unidades != null && Ds_Unidades.Tables[0].Rows.Count > 0)
                    {
                        Dt_Unidades = Ds_Unidades.Tables[0];
                    }

                    return Dt_Unidades;
                }
                catch (Exception Ex)
                {
                    Ex.ToString();
                    return null;
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Empleados_General
            /// DESCRIPCION : Consulta los empelados del sistema.        
            /// CREO        : Juan Alberto Hernández Negrete
            /// FECHA_CREO  : 13/Diciembre/2010
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            public static DataTable Consulta_Empleados_General(Cls_Ope_Psp_Limite_Presupuestal_Negocio Datos)
            {
                String Mi_SQL = "";//Variable que alamcenara la consulta.
                DataTable Dt_Empleados = null;//Variable que almacenara una lista de empleados.

                try
                {
                    Mi_SQL = "SELECT " + Cat_Empleados.Tabla_Cat_Empleados + ".*, ";

                    Mi_SQL = Mi_SQL + "(ISNULL(" + Cat_Empleados.Campo_Apellido_Paterno + ", '') ";
                    Mi_SQL = Mi_SQL + "+' '+ ISNULL(" + Cat_Empleados.Campo_Apellido_Materno + ", '') ";
                    Mi_SQL = Mi_SQL + "+' '+" + Cat_Empleados.Campo_Nombre + ") AS Empleado, ";

                    Mi_SQL = Mi_SQL + "( '[' + " + Cat_Empleados.Campo_No_Empleado + "  + '] -- ' + ISNULL(" + Cat_Empleados.Campo_Apellido_Paterno + ", '') ";
                    Mi_SQL = Mi_SQL + " +' '+ ISNULL(" + Cat_Empleados.Campo_Apellido_Materno + ", '') ";
                    Mi_SQL = Mi_SQL + " +' '+ " + Cat_Empleados.Campo_Nombre + ") AS EMPLEADOS";

                    Mi_SQL = Mi_SQL + " FROM " + Cat_Empleados.Tabla_Cat_Empleados;

                    if (!string.IsNullOrEmpty(Datos.P_Empleado_ID))
                    {
                        if (Mi_SQL.Contains("WHERE"))
                        {
                            Mi_SQL += " AND " + Cat_Empleados.Campo_Empleado_ID + "='" + Datos.P_Empleado_ID + "'";
                        }
                        else
                        {
                            Mi_SQL += " WHERE " + Cat_Empleados.Campo_Empleado_ID + "='" + Datos.P_Empleado_ID + "'";
                        }
                    }

                    if (!string.IsNullOrEmpty(Datos.P_No_Empleado))
                    {
                        if (Mi_SQL.Contains("WHERE"))
                        {
                            Mi_SQL += " AND " + Cat_Empleados.Campo_No_Empleado + " LIKE '%" + Datos.P_No_Empleado + "%'";
                        }
                        else
                        {
                            Mi_SQL += " WHERE " + Cat_Empleados.Campo_No_Empleado + " LIKE '%" + Datos.P_No_Empleado + "%'";
                        }
                    }

                    if (!string.IsNullOrEmpty(Datos.P_Estatus))
                    {
                        if (Mi_SQL.Contains("WHERE"))
                        {
                            Mi_SQL += " AND " + Cat_Empleados.Campo_Estatus + "='" + Datos.P_Estatus + "'";
                        }
                        else
                        {
                            Mi_SQL += " WHERE " + Cat_Empleados.Campo_Estatus + "='" + Datos.P_Estatus + "'";
                        }
                    }

                    if (!string.IsNullOrEmpty(Datos.P_Nombre))
                    {
                        if (Mi_SQL.Contains("WHERE"))
                        {
                            Mi_SQL += " AND (UPPER(" + Cat_Empleados.Campo_Nombre + "+' '+ ISNULL(" + Cat_Empleados.Campo_Apellido_Paterno + ", '') +' '+ ISNULL(" +
                                 Cat_Empleados.Campo_Apellido_Materno + ", '')) LIKE UPPER('%" + Datos.P_Nombre + "%')";
                            Mi_SQL += " OR UPPER( ISNULL(" + Cat_Empleados.Campo_Apellido_Paterno + ",'') +' '+ ISNULL(" +
                                 Cat_Empleados.Campo_Apellido_Materno + ",'') + ' ' + " + Cat_Empleados.Campo_Nombre + ") LIKE UPPER('%" + Datos.P_Nombre + "%'))";
                        }
                        else
                        {
                            Mi_SQL += " WHERE (UPPER(" + Cat_Empleados.Campo_Nombre + "+' '+ ISNULL(" + Cat_Empleados.Campo_Apellido_Paterno + ",'') +' '+ ISNULL(" +
                                 Cat_Empleados.Campo_Apellido_Materno + ",'')) LIKE UPPER('%" + Datos.P_Nombre + "%')";
                            Mi_SQL += " OR UPPER( ISNULL(" + Cat_Empleados.Campo_Apellido_Paterno + ",'') +' '+ ISNULL(" +
                                 Cat_Empleados.Campo_Apellido_Materno + ",'') + ' ' + " + Cat_Empleados.Campo_Nombre + ") LIKE UPPER('%" + Datos.P_Nombre + "%'))";
                        }
                    }

                    if (!string.IsNullOrEmpty(Datos.P_Dependencia_ID))
                    {
                        if (Mi_SQL.Contains("WHERE"))
                        {
                            Mi_SQL += " AND " + Cat_Empleados.Campo_Dependencia_ID + "='" + Datos.P_Dependencia_ID + "'";
                        }
                        else
                        {
                            Mi_SQL += " WHERE " + Cat_Empleados.Campo_Dependencia_ID + "='" + Datos.P_Dependencia_ID + "'";
                        }
                    }

                    if (!string.IsNullOrEmpty(Datos.P_Programa_ID))
                    {
                        if (Mi_SQL.Contains("WHERE"))
                        {
                            Mi_SQL += " AND " + Cat_Empleados.Campo_Programa_ID + "='" + Datos.P_Programa_ID + "'";
                        }
                        else
                        {
                            Mi_SQL += " WHERE " + Cat_Empleados.Campo_Programa_ID + "='" + Datos.P_Programa_ID + "'";
                        }
                    }
                    
                    //Verificar si la busqueda tiene ID del empleado o numero de empleado
                    if (Datos.P_No_Empleado == null && Datos.P_Empleado_ID == null)
                    {
                        if (Mi_SQL.Contains("WHERE"))
                        {
                            Mi_SQL += " AND " + Cat_Empleados.Campo_Tipo_Empleado + "='EMPLEADO'";
                        }
                        else
                        {
                            Mi_SQL += " WHERE " + Cat_Empleados.Campo_Tipo_Empleado + "='EMPLEADO'";
                        }
                    }

                    Mi_SQL += " ORDER BY " + Cat_Empleados.Campo_Apellido_Paterno + ", " + Cat_Empleados.Campo_Apellido_Materno + ", " + Cat_Empleados.Campo_Nombre + " ASC";

                    Dt_Empleados = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
                return Dt_Empleados;
            }

            //*******************************************************************************
            // NOMBRE DE LA FUNCIÓN: Consultar_Programa
            // DESCRIPCIÓN         :  
            // RETORNA             : 
            // CREO                : Leslie González Vázquez
            // FECHA_CREO          : 27/Junio/2013
            // MODIFICO            :
            // FECHA_MODIFICO      :
            // CAUSA_MODIFICACIÓN  :
            // *******************************************************************************/
            public static DataTable Consultar_Programa()
            {
                StringBuilder Mi_Sql = new StringBuilder();
                DataTable Dt_Programas = new DataTable();
                try
                {
                    Mi_Sql.Append("SELECT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ", ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS NOMBRE");
                    Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "."+ Cat_Sap_Proyectos_Programas.Campo_Estatus + " = 'ACTIVO' ");
                    Mi_Sql.Append(" ORDER BY NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0]; ;
                }
                catch (Exception Ex)
                {
                    Ex.ToString();
                    return null;
                }
            }
        #endregion
    }
}