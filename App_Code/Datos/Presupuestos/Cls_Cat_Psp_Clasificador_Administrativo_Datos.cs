﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Clasificador_Administrativo.Negocio;
using JAPAMI.Sessiones;

namespace JAPAMI.Clasificador_Administrativo.Datos
{
    public class Cls_Cat_Psp_Clasificador_Administrativo_Datos
    {
        public Cls_Cat_Psp_Clasificador_Administrativo_Datos()
        {
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Clasificador
        ///DESCRIPCIÓN: Consulta los clasificadores economicos
        ///PARAMETROS:  1.- Cat_Psp_Clasificador_Economico_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  12/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Clasificador(Cls_Cat_Psp_Clasificador_Administrativo_Negocio Clase_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                //Creamos la consulta
                Mi_SQL = "SELECT * ";
                Mi_SQL += " FROM " + Cat_Psp_Clasificador_Administrativo.Tabla_Cat_Psp_Clasificador_Administrativo;
                //Validamos los filtros por año
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Clave))
                {
                    Mi_SQL += " WHERE " + Cat_Psp_Clasificador_Administrativo.Campo_Clave + " = '" + Clase_Negocio.P_Clave + "'";
                }

                //regresamos los valores
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los calsificadores. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Agregar
        ///DESCRIPCIÓN          : Inserta el registro
        ///PARAMETROS           :  
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 12/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Agregar(Cls_Cat_Psp_Clasificador_Administrativo_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Consultamos el consecutivo
                String Mi_SQL = "SELECT ISNULL(MAX(" + Cat_Psp_Clasificador_Administrativo.Campo_CADM_ID + ") + 1, 1) AS CONSECUTIVO FROM " + Cat_Psp_Clasificador_Administrativo.Tabla_Cat_Psp_Clasificador_Administrativo;
                //Asignamos el valor
                Clase_Negocio.P_CADM_ID = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0]["CONSECUTIVO"].ToString();

                //Insertar un registro de las fechas
                Mi_SQL = "INSERT INTO " + Cat_Psp_Clasificador_Administrativo.Tabla_Cat_Psp_Clasificador_Administrativo +
                    " (" + Cat_Psp_Clasificador_Administrativo.Campo_CADM_ID +
                    ", " + Cat_Psp_Clasificador_Administrativo.Campo_Clave +
                    ", " + Cat_Psp_Clasificador_Administrativo.Campo_Nombre +
                    ", " + Cat_Psp_Clasificador_Administrativo.Campo_Descripcion +
                    ", " + Cat_Psp_Clasificador_Administrativo.Campo_Usuario_Creo +
                    ", " + Cat_Psp_Clasificador_Administrativo.Campo_Fecha_Creo +
                    ") VALUES (" +
                     Clase_Negocio.P_CADM_ID + ",'" +
                     Clase_Negocio.P_Clave + "','" +
                     Clase_Negocio.P_Nombre + "','" +
                     Clase_Negocio.P_Descripcion + "','" +
                     Cls_Sessiones.Nombre_Empleado + "'," +
                     "GETDATE() )";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Trans.Commit();
                Mensaje_Error = Clase_Negocio.P_Clave;

            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo agregar";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Actualizar
        ///DESCRIPCIÓN          : Actualizamos el registro
        ///PARAMETROS           :  
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 12/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Actualizar(Cls_Cat_Psp_Clasificador_Administrativo_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Actualizar las fechas
                String Mi_SQL = "UPDATE " + Cat_Psp_Clasificador_Administrativo.Tabla_Cat_Psp_Clasificador_Administrativo;
                Mi_SQL = Mi_SQL + " SET " + Cat_Psp_Clasificador_Administrativo.Campo_Clave + " = '" + Clase_Negocio.P_Clave + "'";
                Mi_SQL = Mi_SQL + ", " + Cat_Psp_Clasificador_Administrativo.Campo_Nombre + " = '" + Clase_Negocio.P_Nombre + "'";
                Mi_SQL = Mi_SQL + ", " + Cat_Psp_Clasificador_Administrativo.Campo_Descripcion + " = '" + Clase_Negocio.P_Descripcion + "'";
                Mi_SQL = Mi_SQL + ", " + Cat_Psp_Clasificador_Administrativo.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "'";
                Mi_SQL = Mi_SQL + ", " + Cat_Psp_Clasificador_Administrativo.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Psp_Clasificador_Administrativo.Campo_CADM_ID + " = " + Clase_Negocio.P_CADM_ID;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Trans.Commit();
                Mensaje_Error = "Rechazado";
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo actualizar";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar
        ///DESCRIPCIÓN          : eliminamos el registro
        ///PARAMETROS           :  
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 12/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Eliminar(Cls_Cat_Psp_Clasificador_Administrativo_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Actualizar las fechas
                String Mi_SQL = "DELETE " + Cat_Psp_Clasificador_Administrativo.Tabla_Cat_Psp_Clasificador_Administrativo;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Psp_Clasificador_Administrativo.Campo_CADM_ID + " = " + Clase_Negocio.P_CADM_ID;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Trans.Commit();
                Mensaje_Error = "Se elimino el registro.";
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo eliminar el registro, puede que este relacionado";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Validar_Clave
        ///DESCRIPCIÓN: Consulta si ya existe la clave
        ///PARAMETROS:  1.- Cls_Ope_Alm_Fechas_Pago_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  12/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Validar_Clave(Cls_Cat_Psp_Clasificador_Administrativo_Negocio Clase_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                //Creamos la consulta
                Mi_SQL = "SELECT " + Cat_Psp_Clasificador_Administrativo.Campo_Clave;
                Mi_SQL += " FROM " + Cat_Psp_Clasificador_Administrativo.Tabla_Cat_Psp_Clasificador_Administrativo;
                Mi_SQL += " WHERE " + Cat_Psp_Clasificador_Administrativo.Campo_Clave + " = '" + Clase_Negocio.P_Clave + "'";
                
                //regresamos los valores
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las  ultimas fechas. Error: [" + Ex.Message + "]");
            }
        }
    }
}