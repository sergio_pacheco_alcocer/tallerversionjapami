﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Text;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Layout_Cuentas_Contables.Negocios;

namespace JAPAMI.Layout_Cuentas_Contables.Datos
{
    public class Cls_Ope_Psp_Layout_Cuentas_Contables_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Fuente_Financiamiento
        ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Ramon Baeza Yepez
        ///FECHA CREO           : 03/julio/2013
        ///*******************************************************************************
        internal static DataTable Consultar_Fuente_Financiamiento(Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS CAT_SAP_FTE_FINANCIAMIENTO
                Mi_Sql.Append("Select * From " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los tipos. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Area_Funcional
        ///DESCRIPCIÓN          :consulta para obtener los datos de las areas funcionales
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Ramon Baeza Yepez
        ///FECHA CREO           : 03/julio/2013
        ///*******************************************************************************
        internal static DataTable Consultar_Area_Funcional(Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS CAT_SAP_AREA_FUNCIONAL
                Mi_Sql.Append("Select * From " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los tipos. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Programas
        ///DESCRIPCIÓN          :consulta para obtener los datos de los programas
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Ramon Baeza Yepez
        ///FECHA CREO           : 03/julio/2013
        ///*******************************************************************************
        internal static DataTable Consultar_Programas(Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS CAT_SAP_PROYECTOS_PROGRAMAS
                Mi_Sql.Append("Select * From " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los tipos. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Dependencias
        ///DESCRIPCIÓN          :consulta para obtener los datos de las dependencias
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Ramon Baeza Yepez
        ///FECHA CREO           : 03/julio/2013
        ///*******************************************************************************
        internal static DataTable Consultar_Dependencias(Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS CAT_DEPENDENCIAS
                Mi_Sql.Append("Select * From " + Cat_Dependencias.Tabla_Cat_Dependencias);

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los tipos. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas
        ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Ramon Baeza Yepez
        ///FECHA CREO           : 03/julio/2013
        ///*******************************************************************************
        internal static DataTable Consultar_Partidas(Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS CAT_SAP_PARTIDAS_ESPECIFICAS
                Mi_Sql.Append("Select * From " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los tipos. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Cuentas_Contables
        ///DESCRIPCIÓN          : consulta para obtener los datos de las cuentas contables
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Ramon Baeza Yepez
        ///FECHA CREO           : 03/julio/2013
        ///*******************************************************************************
        internal static DataTable Consultar_Cuentas_Contables(Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS CAT_CON_CUENTAS_CONTABLES
                Mi_Sql.Append("Select * From " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los tipos. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta
        ///DESCRIPCIÓN          : Realiza el alta de los datos del layout
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Ramon Baeza Yepez
        ///FECHA CREO           : 03/julio/2013
        ///*******************************************************************************
        public static void Alta(Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            SqlConnection Conexion = new SqlConnection();//Variable de conexión.
            SqlCommand Comando = new SqlCommand();//Variable que ejecuta las sentencias SQL.
            SqlTransaction Transaccion = null;//Variable que ejecutara las transacciones contra la base de datos.

            Conexion.ConnectionString = Cls_Constantes.Str_Conexion;
            Conexion.Open();

            Transaccion = Conexion.BeginTransaction();
            Comando.Connection = Conexion;
            Comando.Transaction = Transaccion;
            try
            {
                foreach (DataRow Linea in Negocio.P_Dt_Presupuesto.Rows)
                {
                    Mi_Sql.Append(" UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID );
                    Mi_Sql.Append(" = '" + Linea["Cuenta_Contable_ID"].ToString().Trim()  + "'");
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_Sql.Append(" = " + Negocio.P_Anio.Trim());
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Linea["Fuente_Financiamiento_ID"].ToString().Trim());
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Linea["Programa_ID"].ToString().Trim());
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Linea["Dependencia_ID"].ToString().Trim());
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_Sql.Append(" = " + Linea["Partida_ID"].ToString().Trim());

                    Comando.CommandText = Mi_Sql.ToString();
                    Comando.ExecuteNonQuery();
                    Mi_Sql = new StringBuilder();
                }

                //Almacenamos los datos en la base de datos
                Transaccion.Commit();
            }
            catch (Exception Ex)
            {
                Transaccion.Rollback();
                throw new Exception("Error en el alta. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Anio
        ///DESCRIPCIÓN          : Realiza el alta de los datos del layout
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Ramon Baeza Yepez
        ///FECHA CREO           : 03/julio/2013
        ///*******************************************************************************
        internal static DataTable Consultar_Anio(Cls_Ope_Psp_Layout_Cuentas_Contables_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS CAT_CON_CUENTAS_CONTABLES
                Mi_Sql.Append("Select (Select " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " From ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " Where ");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = Apr.");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ") As FUENTE_FINANCIAMIENTO,");
                Mi_Sql.Append("(Select " + Cat_SAP_Area_Funcional.Campo_Clave + " From ");
                Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " Where ");
                Mi_Sql.Append(Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + " = Apr.");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + ") As AREA_FUNCIONAL,");
                Mi_Sql.Append("(Select " + Cat_Sap_Proyectos_Programas.Campo_Clave + " From ");
                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " Where ");
                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = Apr.");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ") As PROGRAMA,");
                Mi_Sql.Append("(Select " + Cat_Dependencias.Campo_Clave + " From ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + " Where ");
                Mi_Sql.Append(Cat_Dependencias.Campo_Dependencia_ID + " = Apr.");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ") As DEPENDENCIA,");
                Mi_Sql.Append("(Select " + Cat_Sap_Partidas_Especificas.Campo_Clave + " From ");
                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " Where ");
                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = Apr.");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ") As PARTIDA,");
                Mi_Sql.Append("(Select " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " From ");
                Mi_Sql.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + " Where ");
                Mi_Sql.Append(Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + " = Apr.");
                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + ") As CUENTA_CONTABLE,");
                Mi_Sql.Append("Apr." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " As Apr");
                Mi_Sql.Append(" WHERE Apr." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Negocio.P_Anio);

                Mi_Sql.Append(" ORDER BY FUENTE_FINANCIAMIENTO, AREA_FUNCIONAL, PROGRAMA, DEPENDENCIA, PARTIDA ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los tipos. Error: [" + Ex.Message + "]");
            }
        }

        internal static DataTable Obtener_Anio_Presupuestado() 
        {
            StringBuilder Mi_Sql = new StringBuilder();
            DataSet Ds = new DataSet();
            DataTable Dt = new DataTable();

            try
            {
                Mi_Sql.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " > = " + String.Format("{0:yyyy}", DateTime.Now));

                Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());

                if (Ds != null)
                {
                    Dt = Ds.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al obtener los años presupuestados. Error[" + Ex.Message + "]");
            }
            return Dt;
        }
    }
}