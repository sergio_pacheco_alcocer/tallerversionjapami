﻿using System;
using System.Data;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Rpt_Ingresos_Recaudados.Negocio;


namespace JAPAMI.Rpt_Ingresos_Recaudados.Datos
{
    public class Cs_Rpt_Psp_Ingresos_Recaudados_Datos
    {
        #region(Metodos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Anios
        ///DESCRIPCIÓN          : Obtiene datos de los años del estado analitico de ingresos
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 15/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Anios()
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
            DataSet Ds_Anios = new DataSet(); //Dataset donde obtendremos los datos de la consulta
            DataTable Dt_Anios = new DataTable();
            try
            {
                Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                Mi_SQL.Append(" ORDER BY " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " DESC");


                Ds_Anios = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds_Anios != null)
                {
                    Dt_Anios = Ds_Anios.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de los años. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Anios;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_FF
        ///DESCRIPCIÓN          : Obtiene datos de  las fuentes de financiamiento del estado analitico de ingresos
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 15/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_FF()
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
            DataSet Ds_FF = new DataSet(); //Dataset donde obtendremos los datos de la consulta
            DataTable Dt_FF = new DataTable();
            try
            {
                Mi_SQL.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", ");
                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");


                Ds_FF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds_FF != null)
                {
                    Dt_FF = Ds_FF.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de las fuentes de fianciamiento. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_FF;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Presupuesto_Ingresos
        ///DESCRIPCIÓN          : Obtiene datos de  las unidades responsables del presupuesto de ingresos
        ///PARAMETROS           1 Negocio: conexion con la capa de negocios 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 15/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Presupuesto_Ingresos(Cs_Rpt_Psp_Ingresos_Recaudados_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
            DataSet Ds_Pronostico = new DataSet(); //Dataset donde obtendremos los datos de la consulta
            DataTable Dt_Pronostico = new DataTable();
            try
            {
                Mi_SQL.Append("SELECT (CASE WHEN ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                Mi_SQL.Append(" THEN ");
                Mi_SQL.Append("REPLACE(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ",' ' ,'') +' '+");
                Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                Mi_SQL.Append(" ELSE ");
                Mi_SQL.Append("REPLACE(" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ",' ' ,'') +' '+");
                Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                Mi_SQL.Append(" END) AS CONCEPTO, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Enero + ", 0)) AS IMP_ENERO, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Febrero + ", 0)) AS IMP_FEBRERO, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Marzo + ", 0)) AS IMP_MARZO, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Abril + ", 0)) AS IMP_ABRIL, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Mayo + ", 0)) AS IMP_MAYO, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Junio + ", 0)) AS IMP_JUNIO, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Julio + ", 0)) AS IMP_JULIO, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Agosto + ", 0)) AS IMP_AGOSTO, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Septiembre + ", 0)) AS IMP_SEPTIEMBRE, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Octubre + ", 0)) AS IMP_OCTUBRE, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Noviembre + ", 0)) AS IMP_NOVIEMBRE, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Diciembre + ", 0)) AS IMP_DICIEMBRE, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Enero + ", 0)) + ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Febrero + ", 0)) + ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Marzo + ", 0)) + ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Abril + ", 0)) + ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Mayo + ", 0)) + ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Junio + ", 0)) + ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Julio + ", 0)) + ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Agosto + ", 0)) + ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Septiembre + ", 0)) + ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Octubre + ", 0)) + ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Noviembre + ", 0)) + ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Diciembre + ", 0)) AS ACUMULADO, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", 0)) AS ESTIMADO, ");
                Mi_SQL.Append("'0.00' AS PROYECTADO, ");
                Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS RUBRO, ");
                Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " AS CLAVE_RUBRO, ");
                Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", 0)) - ");
                Mi_SQL.Append("0.00 AS DIFERENCIA, ");
                Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID + ", ");
                Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion  + " AS RUBRO_ING, ");
                Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + ", ");
                Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS TIPO_ING, ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLASE_ING, ");
                Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + ", ");
                Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CONCEPTO_ING ");
                Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                Mi_SQL.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);


                if (!String.IsNullOrEmpty(Negocio.P_Anio))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                    }
                }

                Mi_SQL.Append(" GROUP BY ");
                Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + ", ");
                Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ");
                Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ");
                Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + ", ");
                Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ");
                Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion + ", ");
                Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + ", ");
                Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID + ", ");
                Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + ", ");
                Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ", ");
                Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + ", ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ", ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + ", ");
                Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                Mi_SQL.Append(" ORDER BY CONCEPTO ASC");

                Ds_Pronostico = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                if (Ds_Pronostico != null)
                {
                    Dt_Pronostico = Ds_Pronostico.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros del pronostico de ingresos. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Pronostico;
        }
        #endregion
    }
}
