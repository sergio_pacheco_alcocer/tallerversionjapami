﻿using System;
using System.Data;
using System.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Ope_Psp_Movimientos_Ingresos.Negocio;
using JAPAMI.Ope_Con_Poliza_Ingresos.Datos;

namespace JAPAMI.Ope_Psp_Movimientos_Ingresos.Datos
{
    public class Cls_Ope_Psp_Movimientos_Ingresos_Datos
    {
        #region Metodos
            ///*****************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Rubros
            ///DESCRIPCIÓN          : consulta para obtener los datos de los rubros
            ///PARAMETROS           1 Negocio: Conexion con la capa de negocios
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///******************************************************************************************************
            internal static DataTable Consultar_Rubros(Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    Mi_Sql.Append("SELECT DISTINCT " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                    Mi_Sql.Append(" FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                    Mi_Sql.Append(" = " + String.Format("{0:yyyy}", DateTime.Now));

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                        Mi_Sql.Append("." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                    }
                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");
                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los rubros. Error: [" + Ex.Message + "]");
                }
            }

            ///*****************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_FF
            ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///******************************************************************************************************
            internal static DataTable Consultar_FF()
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    Mi_Sql.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                    Mi_Sql.Append(" = " + String.Format("{0:yyyy}", DateTime.Now));

                    Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]");
                }
            }

            ///******************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos
            ///DESCRIPCIÓN          : consulta para obtener los datos de los conceptos
            ///PARAMETROS           1: Negocio: conexion con la capa de negocios
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************************************
            internal static DataTable Consultar_Conceptos(Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CLAVE_NOM_RUBRO, ");
                    Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID + ", ");
                    Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CLAVE_NOM_TIPO, ");
                    Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + ", ");
                    Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLAVE_NOM_CLASE, ");
                    Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ");
                    Mi_Sql.Append("(CASE WHEN " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL THEN ");
                    Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                    Mi_Sql.Append(" ELSE ");
                    Mi_Sql.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                    Mi_Sql.Append(" END) AS CLAVE_NOM_CONCEPTO, ");
                    Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + ", ");
                    Mi_Sql.Append("ISNULL(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + ", '') AS CLAVE_NOM_PROGRAMA, ");
                    Mi_Sql.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " AS PROYECTO_PROGRAMA_ID, ");
                    Mi_Sql.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ", 0) AS IMP_ENE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero+ ", 0) AS IMP_FEB, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo+ ", 0) AS IMP_MAR, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ", 0) AS IMP_ABR, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ", 0) AS IMP_MAY, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ", 0) AS IMP_JUN, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ", 0) AS IMP_JUL, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ", 0) AS IMP_AGO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ", 0) AS IMP_SEP, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ", 0) AS IMP_OCT, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ", 0) AS IMP_NOV, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ", 0) AS IMP_DIC, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ", 0) AS IMP_TOTAL, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", 0) AS ESTIMADO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ", 0) AS AMPLIACION, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ", 0) AS REDUCCION, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", 0) AS RECAUDADO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ", 0) AS POR_RECAUDAR, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ", 0) AS MODIFICADO ");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                    Mi_Sql.Append(" = " + String.Format("{0:yyyy}", DateTime.Now));

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                    }


                    if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                    {
                        if (Negocio.P_Busqueda.Trim().Equals("CON"))
                        {
                            if (!String.IsNullOrEmpty(Negocio.P_Programa_ID))
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" IS NULL");
                            }
                        }
                        else 
                        {
                            if (!String.IsNullOrEmpty(Negocio.P_Programa_ID))
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Clase_ID))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                    }
                    if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                    }
                    Mi_Sql.Append(" ORDER BY CLAVE_NOM_CONCEPTO ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los rubros. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Movimientos
            ///DESCRIPCIÓN          : consulta para obtener los datos de los movimientos del presupuesto de ingresos
            ///PARAMETROS           1: Negocio: conexion con la capa de negocios
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************************************
            internal static DataTable Consultar_Movimientos(Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Movimiento_Ing_ID + ", ");
                    Mi_Sql.Append("(CASE WHEN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID);
                    Mi_Sql.Append(" IS NULL THEN ");
                    Mi_Sql.Append(" RTRIM(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ") + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                    Mi_Sql.Append(" ELSE ");
                    Mi_Sql.Append(" RTRIM(" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ") + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                    Mi_Sql.Append(" END) AS CLAVE_NOM_CONCEPTO, ");
                    Mi_Sql.Append("(CASE WHEN " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID);
                    Mi_Sql.Append(" IS NULL THEN ");
                    Mi_Sql.Append(" RTRIM(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ") ");
                    Mi_Sql.Append(" ELSE ");
                    Mi_Sql.Append(" RTRIM(" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ") ");
                    Mi_Sql.Append(" END) AS CLAVE_CONCEPTO_ING, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS CLAVE_NOM_PROGRAMA, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS CLAVE_PROGRAMA, ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + ", ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Total + ", 0) AS IMP_TOTAL, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Aprobado + ", 0) AS ESTIMADO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ", 0) AS AMPLIACION, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ", 0) AS REDUCCION, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Modificado + ", 0) AS MODIFICADO, ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus + ", ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID + ", ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID + ", ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID+ ", '') AS SUBCONCEPTO_ING_ID, ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Enero + ", 0) AS IMP_ENE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Febrero + ", 0) AS IMP_FEB, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Marzo + ", 0) AS IMP_MAR, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Abril + ", 0) AS IMP_ABR, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Mayo + ", 0) AS IMP_MAY, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Junio + ", 0) AS IMP_JUN, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Julio + ", 0) AS IMP_JUL, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Agosto + ", 0) AS IMP_AGO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Septiembre + ", 0) AS IMP_SEP, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Octubre + ", 0) AS IMP_OCT, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Noviembre + ", 0) AS IMP_NOV, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Diciembre + ", 0) AS IMP_DIC, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Justificacion + ", '') AS JUSTIFICACION, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Comentario + ", '') AS COMENTARIO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fecha_Modifico + ", '') AS FECHA_MODIFICO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Usuario_Modifico + ", '') AS USUARIO_MODIFICO, ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Concepto + ", ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Proyecto_Programa_ID +  ", ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Nombre_Documento + " AS NOMBRE_DOC, ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Ruta_Documento + " AS RUTA_DOC, ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Extension_Documento + " AS EXTENSION_DOC, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOM_FUENTE, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS CLAVE_FUENTE, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + ", ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio + ", ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                    Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                    Mi_Sql.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                    Mi_Sql.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" ON " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Anio);
                    Mi_Sql.Append(" = " + String.Format("{0:yyyy}", DateTime.Now));

                    if (!String.IsNullOrEmpty(Negocio.P_No_Movimiento_Ing) )
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                        Mi_Sql.Append(" = '" + Negocio.P_No_Movimiento_Ing + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Estatus))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus);
                        Mi_Sql.Append(" IN (" + Negocio.P_Estatus + ")");
                    }

                    Mi_Sql.Append(" ORDER BY " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "." + Ope_Psp_Movimiento_Ing_Det.Campo_Movimiento_Ing_ID +  " DESC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los movimientos. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Movimientos
            ///DESCRIPCIÓN          : consulta para obtener los datos de las modificaciones
            ///PARAMETROS           1: Negocio: conexion con la capa de negocios
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 09/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************************************
            internal static DataTable Consultar_Modificaciones(Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + ", ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing.Campo_Anio + ", ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing.Campo_Total_Modificado + ", ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing.Campo_Fecha_Creo + ", ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing.Campo_Estatus+ " AS ESTATUS, ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing.Campo_Usuario_Creo);
                    Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now));
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " IN (");
                    Mi_Sql.Append(" SELECT DISTINCT " + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                    Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Ing_Det.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now)+")");

                    if (!String.IsNullOrEmpty(Negocio.P_No_Movimiento_Ing))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing);
                        Mi_Sql.Append(" = '" + Negocio.P_No_Movimiento_Ing.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fecha_Inicio) && !String.IsNullOrEmpty(Negocio.P_Fecha_Fin))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Ing.Campo_Fecha_Creo);
                        Mi_Sql.Append(" BETWEEN TO_DATE ('" + Negocio.P_Fecha_Inicio + " 00:00:00', 'MM/DD/YYYY HH24:MI:SS')");
                        Mi_Sql.Append(" AND TO_DATE('" + Negocio.P_Fecha_Fin + " 23:59:00', 'MM/DD/YYYY HH24:MI:SS')");
                    }

                    Mi_Sql.Append(" ORDER BY " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " DESC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las modificaciones. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Movimientos
            ///DESCRIPCIÓN          : consulta para modificar los datos de los movimientos
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 17/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static String Modificar_Movimientos(Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
                String Autorizados = String.Empty;//Estado de la operacion.
                DataTable Dt_Mov_Conceptos = new DataTable();
                DataTable Dt_Mov_Autorizados = new DataTable();
                DataTable Dt_Psp_Ing = new DataTable();
                DataTable Dt_Estatus = new DataTable();
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;

                String[] Datos_Poliza_PSP = null;
                String No_Poliza_PSP = String.Empty;
                String Tipo_Poliza_PSP = String.Empty;
                String Mes_Anio_PSP = String.Empty;

                try
                {
                    Dt_Mov_Conceptos = Negocio.P_Dt_Mov_Conceptos;
                    Dt_Mov_Autorizados = Negocio.P_Dt_Mov_Autorizados;

                    if (Dt_Mov_Conceptos != null && Dt_Mov_Conceptos.Rows.Count > 0 && Dt_Mov_Autorizados != null && Dt_Mov_Autorizados.Rows.Count > 0)
                    {
                        if (Negocio.P_Estatus.Trim().Equals("AUTORIZADO"))
                        {
                            //creamos la poliza presupuestal del movimiento
                            //obtenemos los datos de la poliza presupuestal
                            Datos_Poliza_PSP = Registro_Mov_Autorizado(Cmd, Negocio.P_Dt_Mov_Conceptos, Negocio.P_Dt_Mov_Autorizados,
                                Negocio.P_Anio, Negocio.P_No_Movimiento_Ing.Trim());

                            if (Datos_Poliza_PSP != null)
                            {
                                if (Datos_Poliza_PSP.Length > 0)
                                {
                                    No_Poliza_PSP = Datos_Poliza_PSP[0].Trim();
                                    Tipo_Poliza_PSP = Datos_Poliza_PSP[1].Trim();
                                    Mes_Anio_PSP = Datos_Poliza_PSP[2].Trim();
                                }
                            }
                        }

                        // SE MODIFICA EL ESTATUS DE LA MODIFICACION DE INGRESOS
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                        Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Ing.Campo_Estatus + " = '" + Negocio.P_Estatus.Trim() + "', ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico + "', ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Fecha_Modifico + " = GETDATE()");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " = " + Negocio.P_No_Movimiento_Ing.Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                        Cmd.CommandText = Mi_SQL.ToString();
                        Cmd.ExecuteNonQuery();

                        foreach (DataRow Dr_Au in Dt_Mov_Autorizados.Rows)
                        {
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                            Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus + " = '"+Dr_Au["ESTATUS"].ToString().Trim()+"', ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Comentario + " = '" + Dr_Au["COMENTARIO"].ToString().Trim() + "', ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico + "', ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Fecha_Modifico + " = GETDATE()");
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing_Det.Campo_Movimiento_Ing_ID + " = " + Dr_Au["MOVIMIENTO_ING_ID"].ToString().Trim());
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing + " = " + Negocio.P_No_Movimiento_Ing.Trim());
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_Anio + " = " + Negocio.P_Anio.Trim());

                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();

                            if (Dr_Au["ESTATUS"].ToString().Trim().Equals("RECHAZADO") || Dr_Au["ESTATUS"].ToString().Trim().Equals("CANCELADO"))
                            {
                                foreach (DataRow Dr in Dt_Mov_Conceptos.Rows)
                                {
                                    if (Dr_Au["MOVIMIENTO_ING_ID"].ToString().Equals(Dr["MOVIMIENTO_ING_ID"].ToString()))
                                    {
                                        Mi_SQL = new StringBuilder();
                                        Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                                        Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Ing.Campo_Total_Modificado + " = " + Ope_Psp_Movimiento_Ing.Campo_Total_Modificado);
                                        Mi_SQL.Append(" - " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico + "', ");
                                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Fecha_Modifico + " = GETDATE()");
                                        Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing + " = " + Negocio.P_No_Movimiento_Ing.Trim());
                                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                                        Cmd.CommandText = Mi_SQL.ToString();
                                        Cmd.ExecuteNonQuery();

                                        if (Dr["TIPO_MOVIMIENTO"].ToString().Trim().Equals("AMPLIACION"))
                                        {
                                            if (Dr["TIPO_CONCEPTO"].ToString().Trim().Equals("Nuevo"))
                                            {
                                                Mi_SQL = new StringBuilder();
                                                Mi_SQL.Append("DELETE FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                                Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "' ");
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Dr["RUBRO_ID"].ToString().Trim() + "' ");
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Dr["TIPO_ID"].ToString().Trim() + "' ");
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Dr["CLASE_ING_ID"].ToString().Trim() + "' ");
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "' ");
                                                if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                                                {
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "' ");
                                                }
                                                else 
                                                {
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                                                }
                                                if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                                {
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                                                }
                                                else
                                                {
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL");
                                                }
                                                
                                                Cmd.CommandText = Mi_SQL.ToString();
                                                Cmd.ExecuteNonQuery();
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                            else if (Dr_Au["ESTATUS"].ToString().Trim().Equals("AUTORIZADO"))
                            {
                                foreach (DataRow Dr in Dt_Mov_Conceptos.Rows)
                                {
                                    if (Dr_Au["MOVIMIENTO_ING_ID"].ToString().Equals(Dr["MOVIMIENTO_ING_ID"].ToString()))
                                    {
                                        if (!String.IsNullOrEmpty(No_Poliza_PSP) && !String.IsNullOrEmpty(Tipo_Poliza_PSP)
                                            && !String.IsNullOrEmpty(Mes_Anio_PSP))
                                        {

                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                                            Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Ing_Det.Campo_No_Poliza_Presupuestal + " = '" + No_Poliza_PSP.Trim() + "', ");
                                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Poliza_ID_Presupuestal + " = '" + Tipo_Poliza_PSP.Trim() + "', ");
                                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Mes_Anio_Presupuestal + " = '" + Mes_Anio_PSP.Trim() + "' ");
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing_Det.Campo_Movimiento_Ing_ID + " = " + Dr_Au["MOVIMIENTO_ING_ID"].ToString().Trim());
                                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing + " = " + Negocio.P_No_Movimiento_Ing.Trim());
                                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_Anio + " = " + Negocio.P_Anio.Trim());

                                            Cmd.CommandText = Mi_SQL.ToString();
                                            Cmd.ExecuteNonQuery();
                                        }

                                        if (Dr["TIPO_MOVIMIENTO"].ToString().Trim().Equals("AMPLIACION"))
                                        {
                                            //MODIFICAMOS EL IMPORTE DE LA AMPLIACION EN EL PRESUPUESTO
                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                            Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ",0) + " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ",0) + " + Dr["IMP_ENE"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ",0) + " + Dr["IMP_FEB"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ",0) + " + Dr["IMP_MAR"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ",0) + " + Dr["IMP_ABR"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ",0) + " + Dr["IMP_MAY"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ",0) + " + Dr["IMP_JUN"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ",0) + " + Dr["IMP_JUL"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ",0) + " + Dr["IMP_AGO"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ",0) + " + Dr["IMP_SEP"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ",0) + " + Dr["IMP_OCT"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ",0) + " + Dr["IMP_NOV"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ",0) + " + Dr["IMP_DIC"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ",0) + " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + ",0) + " + Dr["IMP_ENE"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + ",0) + " + Dr["IMP_FEB"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + ",0) + " + Dr["IMP_MAR"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + ",0) + " + Dr["IMP_ABR"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + ",0) + " + Dr["IMP_MAY"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + ",0) + " + Dr["IMP_JUN"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + ",0) + " + Dr["IMP_JUL"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + ",0) + " + Dr["IMP_AGO"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + ",0) + " + Dr["IMP_SEP"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + ",0) + " + Dr["IMP_OCT"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + ",0) + " + Dr["IMP_NOV"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + ",0) + " + Dr["IMP_DIC"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico + "', ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Modifico + " = GETDATE()");
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Dr["RUBRO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Dr["TIPO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Dr["CLASE_ING_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "' ");
                                            if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "' ");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                                            }
                                            if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL");
                                            }

                                            Cmd.CommandText = Mi_SQL.ToString();
                                            Cmd.ExecuteNonQuery();

                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                            Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + " =  " + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + " - ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + " + " + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion);
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Dr["RUBRO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Dr["TIPO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Dr["CLASE_ING_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "' ");
                                            if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "' ");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                                            }
                                            if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL");
                                            }
                                            Cmd.CommandText = Mi_SQL.ToString();
                                            Cmd.ExecuteNonQuery();

                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                            Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + " =  " + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + " - ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado);
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Dr["RUBRO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Dr["TIPO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Dr["CLASE_ING_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "' ");
                                            if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "' ");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                                            }
                                            if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL");
                                            }

                                            Cmd.CommandText = Mi_SQL.ToString();
                                            Cmd.ExecuteNonQuery();

                                            //MODIFICAMOS EL IMPORTE DEL PROGRAMA AL QUE SE LE ISO LA AMPLIACION
                                            if (Dr["TIPO_CONCEPTO"].ToString().Trim().Equals("Existente"))
                                            {
                                                if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                                {
                                                    //MODIFICAMOS EL IMPORTE DEL PROGRAMA
                                                    Mi_SQL = new StringBuilder();
                                                    Mi_SQL.Append("UPDATE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                                                    Mi_SQL.Append(" SET " + Cat_Sap_Proyectos_Programas.Campo_Importe + " = ");
                                                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Importe + " + " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", ""));
                                                    Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()+ "'");
                                                    Cmd.CommandText = Mi_SQL.ToString();
                                                    Cmd.ExecuteNonQuery();

                                                    //MODIFICAMOS EL IMPORTE DEL CONCEPTO Y LA FF DEL PROGRAMA
                                                    Mi_SQL = new StringBuilder();
                                                    Mi_SQL.Append("UPDATE " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa);
                                                    Mi_SQL.Append(" SET " + Cat_Sap_Det_Fte_Programa.Campo_Importe + " = ");
                                                    Mi_SQL.Append(Cat_Sap_Det_Fte_Programa.Campo_Importe + " + " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", ""));
                                                    Mi_SQL.Append(" WHERE " + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                                    Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                                    Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "'");
                                                    Cmd.CommandText = Mi_SQL.ToString();
                                                    Cmd.ExecuteNonQuery();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                            Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ",0) + " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ",0) - " + Dr["IMP_ENE"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ",0) - " + Dr["IMP_FEB"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ",0) - " + Dr["IMP_MAR"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ",0) - " + Dr["IMP_ABR"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ",0) - " + Dr["IMP_MAY"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ",0) - " + Dr["IMP_JUN"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ",0) - " + Dr["IMP_JUL"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ",0) - " + Dr["IMP_AGO"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ",0) - " + Dr["IMP_SEP"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ",0) - " + Dr["IMP_OCT"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ",0) - " + Dr["IMP_NOV"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ",0) - " + Dr["IMP_DIC"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ",0) - " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + ",0) - " + Dr["IMP_ENE"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + ",0) - " + Dr["IMP_FEB"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + ",0) - " + Dr["IMP_MAR"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + ",0) - " + Dr["IMP_ABR"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + ",0) - " + Dr["IMP_MAY"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + ",0) - " + Dr["IMP_JUN"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + ",0) - " + Dr["IMP_JUL"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + ",0) - " + Dr["IMP_AGO"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + ",0) - " + Dr["IMP_SEP"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + ",0) - " + Dr["IMP_OCT"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + ",0) - " + Dr["IMP_NOV"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + ",0) - " + Dr["IMP_DIC"].ToString().Trim().Replace(",", "") + ", ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico + "', ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Modifico + " = GETDATE()");
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Dr["RUBRO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Dr["TIPO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Dr["CLASE_ING_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "' ");
                                            if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "' ");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                                            }
                                            if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL");
                                            }
                                            Cmd.CommandText = Mi_SQL.ToString();
                                            Cmd.ExecuteNonQuery();

                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                            Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + " =  " + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + " - ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + " + " + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion);
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Dr["RUBRO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Dr["TIPO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Dr["CLASE_ING_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "' ");
                                            if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "' ");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                                            }
                                            if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL");
                                            }

                                            Cmd.CommandText = Mi_SQL.ToString();
                                            Cmd.ExecuteNonQuery();

                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                                            Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + " =  " + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + " - ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado);
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Dr["RUBRO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Dr["TIPO_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Dr["CLASE_ING_ID"].ToString().Trim() + "' ");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "' ");
                                            if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "' ");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                                            }
                                            if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                                            }
                                            else
                                            {
                                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL");
                                            }

                                            Cmd.CommandText = Mi_SQL.ToString();
                                            Cmd.ExecuteNonQuery();

                                            //MODIFICAMOS EL IMPORTE DEL PROGRAMA AL QUE SE LE ISO LA AMPLIACION
                                            if (Dr["TIPO_CONCEPTO"].ToString().Trim().Equals("Existente"))
                                            {
                                                if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                                {
                                                    //MODIFICAMOS EL IMPORTE DEL PROGRAMA
                                                    Mi_SQL = new StringBuilder();
                                                    Mi_SQL.Append("UPDATE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                                                    Mi_SQL.Append(" SET " + Cat_Sap_Proyectos_Programas.Campo_Importe + " = ");
                                                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Importe + " + " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", ""));
                                                    Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()+ "'");
                                                    Cmd.CommandText = Mi_SQL.ToString();
                                                    Cmd.ExecuteNonQuery();

                                                    //MODIFICAMOS EL IMPORTE DEL CONCEPTO Y LA FF DEL PROGRAMA
                                                    Mi_SQL = new StringBuilder();
                                                    Mi_SQL.Append("UPDATE " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa);
                                                    Mi_SQL.Append(" SET " + Cat_Sap_Det_Fte_Programa.Campo_Importe + " = ");
                                                    Mi_SQL.Append(Cat_Sap_Det_Fte_Programa.Campo_Importe + " + " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", ""));
                                                    Mi_SQL.Append(" WHERE " + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                                    Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                                    Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "'");
                                                    Cmd.CommandText = Mi_SQL.ToString();
                                                    Cmd.ExecuteNonQuery();
                                                }
                                            }
                                            //Cmd.CommandText = Mi_SQL.ToString();
                                            //Cmd.ExecuteNonQuery();

                                            //MODIFICAMOS EL IMPORTE DEL PROGRAMA AL QUE SE LE ISO LA AMPLIACION
                                            if (Dr["TIPO_CONCEPTO"].ToString().Trim().Equals("Existente"))
                                            {
                                                if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                                {
                                                    //MODIFICAMOS EL IMPORTE DEL PROGRAMA
                                                    Mi_SQL = new StringBuilder();
                                                    Mi_SQL.Append("UPDATE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                                                    Mi_SQL.Append(" SET " + Cat_Sap_Proyectos_Programas.Campo_Importe + " = ");
                                                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Importe + " - " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", ""));
                                                    Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                                    Cmd.CommandText = Mi_SQL.ToString();
                                                    Cmd.ExecuteNonQuery();

                                                    //MODIFICAMOS EL IMPORTE DEL CONCEPTO Y LA FF DEL PROGRAMA
                                                    Mi_SQL = new StringBuilder();
                                                    Mi_SQL.Append("UPDATE " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa);
                                                    Mi_SQL.Append(" SET " + Cat_Sap_Det_Fte_Programa.Campo_Importe + " = ");
                                                    Mi_SQL.Append(Cat_Sap_Det_Fte_Programa.Campo_Importe + " - " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", ""));
                                                    Mi_SQL.Append(" WHERE " + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                                    Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                                    Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "'");
                                                    Cmd.CommandText = Mi_SQL.ToString();
                                                    Cmd.ExecuteNonQuery();
                                                }
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        if (Negocio.P_Estatus.Trim().Equals("PREAUTORIZADO"))
                        {
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                            Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus + " = '" + Negocio.P_Estatus.Trim() + "', ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico + "', ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Fecha_Modifico + " = GETDATE()");
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing + " = " + Negocio.P_No_Movimiento_Ing.Trim());
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus + " = 'ACEPTADO'");

                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();
                        }
                        else if (Negocio.P_Estatus.Trim().Equals("AUTORIZADO"))
                        {
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("INSERT  INTO " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp + "(");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Fuente_Financiamiento_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Proyecto_Programa_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Dependencia_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Rubro_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Tipo_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Clase_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Concepto_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_SubConcepto_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Importe_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Importe_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Importe_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Importe_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Importe_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Importe_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Importe_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Importe_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Importe_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Importe_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Importe_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Importe_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Importe_Total + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Aprobado + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Ampliacion + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Reduccion + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Modificado + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Devengado + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Recaudado + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Por_Recaudar + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Por_Recaudar_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Por_Recaudar_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Por_Recaudar_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Por_Recaudar_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Por_Recaudar_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Por_Recaudar_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Por_Recaudar_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Por_Recaudar_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Por_Recaudar_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Por_Recaudar_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Por_Recaudar_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Por_Recaudar_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Devengado_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Devengado_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Devengado_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Devengado_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Devengado_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Devengado_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Devengado_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Devengado_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Devengado_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Devengado_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Devengado_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Devengado_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Recaudado_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Recaudado_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Recaudado_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Recaudado_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Recaudado_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Recaudado_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Recaudado_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Recaudado_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Recaudado_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Recaudado_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Recaudado_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Recaudado_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Usuario_Creo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Fecha_Creo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Usuario_Modifico + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Fecha_Modifico + ") ");
                            Mi_SQL.Append("SELECT " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Dependencia_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                            Mi_SQL.Append("'" + Negocio.P_No_Movimiento_Ing.Trim() + "' AS NO_MODIFICACION, ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Creo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Creo + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Modifico + ", ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Modifico);
                            Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = " + Negocio.P_Anio.Trim());

                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();

                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp);
                            Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ing_Esp.Campo_Modificado + " =  " + Ope_Psp_Presupuesto_Ing_Esp.Campo_Aprobado + " - ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Reduccion + " + " + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion);
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio + " = " + Negocio.P_Anio);
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion + " = " + Negocio.P_No_Movimiento_Ing);

                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();

                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ing_Esp.Tabla_Ope_Psp_Presupuesto_Ing_Esp);
                            Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ing_Esp.Campo_Por_Recaudar + " =  " + Ope_Psp_Presupuesto_Ing_Esp.Campo_Modificado + " - ");
                            Mi_SQL.Append(Ope_Psp_Presupuesto_Ing_Esp.Campo_Recaudado);
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ing_Esp.Campo_Anio + " = " + Negocio.P_Anio);
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ing_Esp.Campo_No_Modificacion + " = " + Negocio.P_No_Movimiento_Ing);
                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();
                        }
                    }
                    Trans.Commit();
                    Autorizados = "SI";
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al ejecutar los la modificación de los movimeintos de ingresos. Error: [" + Ex.Message + "]");
                }
                return Autorizados;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Movimientos
            ///DESCRIPCIÓN          : consulta para guardar los datos de los movimientos
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 17/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static String Alta_Movimientos(Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
                String Presupuesto_Ing_ID = String.Empty;
                Int32 Presupuesto_ID = 0;
                Object No_Modificacion;
                Int32 No_Mod = 0;
                DataTable Dt_Mov = new DataTable();
                String Estatus = String.Empty;
                Boolean Accesible = true;
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                Boolean Actualizado = false;
                String Actual_Psp = String.Empty;
                Int32 Id_Movimiento = Convert.ToInt32(Consecutivo_ID(Ope_Psp_Movimiento_Ing_Det.Campo_Movimiento_Ing_ID, Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det, "10", Cmd));

                try
                {
                    //obtenemos el estatus de actualizacion del presupuesto por si tenemos que dar de alta algun concepto en el presupuesto
                    Actualizado = Cls_Ope_Con_Poliza_Ingresos_Datos.Presupuesto_Actualizado(Cmd);
                    if (Actualizado)
                    {
                        Actual_Psp = "SI";
                    }
                    else
                    {
                        Actual_Psp = "NO";
                    }

                    //obtenemos el consecutivo del numero de modificacion por año
                    Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + "), '0')");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + Negocio.P_Anio);

                    Cmd.CommandText = Mi_SQL.ToString().Trim();
                    No_Modificacion = Cmd.ExecuteScalar();

                    if (Convert.IsDBNull(No_Modificacion))
                    {
                        No_Mod = 0;
                    }
                    else 
                    {
                        No_Mod = Convert.ToInt32(No_Modificacion);
                    }

                    if (No_Mod > 0)
                    {
                        //CONSULTAMOS EL ESTATUS DEL MOdificacion
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("SELECT " + Ope_Psp_Movimiento_Ing.Campo_Estatus);
                        Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + Negocio.P_Anio);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " = " + No_Modificacion.ToString().Trim());
                        
                        Cmd.CommandText = Mi_SQL.ToString().Trim();
                        Estatus = (String)Cmd.ExecuteScalar();

                        if (Estatus.Trim().Equals("GENERADO"))
                        { //si no se tiene q crear un nuevo numero de modificacion ponemos false
                            Accesible = false;
                        }
                        else
                        { //si se tiene q crear un nuevo numero de modificacion ponemos true
                            Accesible = true;
                        }
                    }
                    else 
                    {
                        Accesible = true;
                    }

                    

                    //si es accesible creamos el nuevo numero de modificacion
                    if (Accesible)
                    {
                        if (Convert.IsDBNull(No_Modificacion))
                        {
                            No_Mod = 1;
                        }
                        else
                        {
                            No_Mod = Convert.ToInt32(No_Modificacion) + 1;
                        }

                        //insertamos los datos de la modificacion
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("INSERT INTO " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing + "(");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + ", ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Anio + ", ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Estatus + ", ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Total_Modificado + ", ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Usuario_Creo + ", ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Fecha_Creo + ") VALUES(");
                        Mi_SQL.Append(No_Mod + ", ");
                        Mi_SQL.Append(Negocio.P_Anio + ", ");
                        Mi_SQL.Append("'" + Negocio.P_Estatus + "', ");
                        Mi_SQL.Append(Negocio.P_Total_Modificado.Replace("$", "") + ", ");
                        Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "', ");
                        Mi_SQL.Append("GETDATE())");

                        Cmd.CommandText = Mi_SQL.ToString();
                        Cmd.ExecuteNonQuery();
                    }
                    else 
                    {
                        if (Convert.IsDBNull(No_Modificacion))
                        {
                            No_Mod = 1;
                        }
                        else
                        {
                            No_Mod = Convert.ToInt32(No_Modificacion);
                        }

                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append(" UPDATE " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                        Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Ing.Campo_Total_Modificado + " = " + Ope_Psp_Movimiento_Ing.Campo_Total_Modificado);
                        Mi_SQL.Append(" + " + Negocio.P_Total_Modificado.Replace("$", "") + ", ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Estatus + " = '" + Negocio.P_Estatus + "', ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico.Trim() + "', ");
                        Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Fecha_Modifico + " = GETDATE() ");
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + Negocio.P_Anio);
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " = " + No_Modificacion.ToString().Trim());
                        Cmd.CommandText = Mi_SQL.ToString();
                        Cmd.ExecuteNonQuery();
                    }
                    

                    //Obtenemos los datos de los movimientos de los conceptos de la modificacion
                    // y verificamos que el datatable tenga datos
                    Dt_Mov = Negocio.P_Dt_Mov_Conceptos;
                    if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
                    {
                        Presupuesto_Ing_ID = Consecutivo_ID(Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID, Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos, "10", Cmd);
                        Presupuesto_ID = Convert.ToInt32(Presupuesto_Ing_ID);

                        foreach (DataRow Dr in Dt_Mov.Rows)
                        {
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("INSERT INTO " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "(");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Movimiento_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Proyecto_Programa_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Estatus + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Justificacion + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Total + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Aprobado + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Modificado + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Anio + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Concepto + ", ");
                            if (!String.IsNullOrEmpty(Dr["NOMBRE_DOC"].ToString().Trim()))
                            {
                                Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Nombre_Documento + ", ");
                                Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Ruta_Documento + ", ");
                                Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Extension_Documento + ", ");
                            }
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Usuario_Creo + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Fecha_Creo + ") VALUES( ");
                            Mi_SQL.Append("'" + String.Format("{0:0000000000}", Id_Movimiento) + "', ");
                            Mi_SQL.Append("'" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr["RUBRO_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr["TIPO_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr["CLASE_ING_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "', ");

                            if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                            {
                                Mi_SQL.Append("'" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                            {
                                Mi_SQL.Append("'" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            Mi_SQL.Append("'" + Dr["ESTATUS"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr["TIPO_MOVIMIENTO"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr["JUSTIFICACION"].ToString().Trim() + "', ");
                            Mi_SQL.Append(Dr["IMP_ENE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_FEB"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_MAR"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_ABR"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_MAY"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_JUN"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_JUL"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_AGO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_SEP"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_OCT"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_NOV"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_DIC"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["ESTIMADO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["AMPLIACION"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["REDUCCION"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["MODIFICADO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Negocio.P_Anio + ", ");
                            Mi_SQL.Append(No_Mod + ", ");
                            Mi_SQL.Append("'" + Dr["TIPO_CONCEPTO"].ToString().Trim() + "', ");
                            if (!String.IsNullOrEmpty(Dr["NOMBRE_DOC"].ToString().Trim()))
                            {
                                Mi_SQL.Append("'" + Dr["NOMBRE_DOC"].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Dr["RUTA_DOC"].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Dr["EXTENSION_DOC"].ToString().Trim() + "', ");
                            }
                            Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "', ");
                            Mi_SQL.Append("GETDATE()) ");

                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();

                            Id_Movimiento++;

                            if (Dr["TIPO_MOVIMIENTO"].ToString().Trim().Equals("AMPLIACION"))
                            {
                                if (Dr["TIPO_CONCEPTO"].ToString().Trim().Equals("Nuevo"))
                                {
                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("INSERT INTO " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + " (");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Dependencia_ID + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + ", ");
                                    if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                    {
                                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + ", ");
                                    }
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", ");

                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Enero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Febrero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Marzo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Abril + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Mayo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Junio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Julio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Agosto + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Septiembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Octubre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Noviembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Diciembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Enero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Febrero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Marzo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Abril + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Mayo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Junio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Julio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Agosto + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Septiembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Octubre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Noviembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Diciembre + ", ");

                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Actualizado + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Creo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Creo + ") VALUES (");
                                    Mi_SQL.Append("'" + Presupuesto_ID.ToString().Trim() + "', ");
                                    Mi_SQL.Append("'" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "', ");
                                    Mi_SQL.Append("NULL, ");
                                    Mi_SQL.Append("'" + Dr["RUBRO_ID"].ToString().Trim() + "', ");
                                    Mi_SQL.Append("'" + Dr["TIPO_ID"].ToString().Trim() + "', ");
                                    Mi_SQL.Append("'" + Dr["CLASE_ING_ID"].ToString().Trim() + "', ");
                                    Mi_SQL.Append("'" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "', ");
                                    if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                                    {
                                        Mi_SQL.Append("'" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "', ");
                                    }
                                    else
                                    {
                                        Mi_SQL.Append("NULL, ");
                                    }
                                    if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim())) 
                                    {
                                        Mi_SQL.Append("'" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "', ");
                                    }
                                    Mi_SQL.Append("'" + Negocio.P_Anio + "',");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");/*dic*/
                                    Mi_SQL.Append("0.00, ");/*tot*/
                                    Mi_SQL.Append("0.00, ");/*Apro*/
                                    Mi_SQL.Append("0.00, ");/*amp*/
                                    Mi_SQL.Append("0.00, ");/*red*/
                                    Mi_SQL.Append("0.00, ");/*mod*/
                                    Mi_SQL.Append("0.00, ");/*por_rec*/
                                    Mi_SQL.Append("0.00, ");/*dev*/
                                    Mi_SQL.Append("0.00, ");/*rec*/

                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");/*dic*/
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");/*dic*/
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");

                                    Mi_SQL.Append("'" + Actual_Psp.Trim() + "', ");
                                    Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "',");
                                    Mi_SQL.Append("GETDATE())");
                                    Cmd.CommandText = Mi_SQL.ToString();
                                    Cmd.ExecuteNonQuery();

                                    Presupuesto_ID++;
                                }
                            }
                        }
                    }

                    Trans.Commit();
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al ejecutar el alta de los movimientos de ingresos. Error: [" + Ex.Message + "]");
                }
                return No_Mod.ToString();
            }

            //*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos_Ing
            ///DESCRIPCIÓN          : consulta para obtener los datos de los tipos y las clases, rubros y conceptos
            ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 17/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Conceptos_Ing(Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    //Mi_Sql.Append("SELECT " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID + ", ");
                    //Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + ", ");
                    //Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ");
                    //Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                    //Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CLAVE_NOM_CONCEPTO, ");
                    //Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + ", ");
                    //Mi_Sql.Append("'' AS SUBCONCEPTO_ING_ID, ");
                    //Mi_Sql.Append("'0.00' AS ESTIMADO, ");
                    //Mi_Sql.Append("'0.00' AS RECAUDADO, ");
                    //Mi_Sql.Append("'0.00' AS POR_RECAUDAR, ");
                    //Mi_Sql.Append("'0.00' AS MODIFICADO, ");
                    //Mi_Sql.Append("'0.00' AS AMPLIACION, ");
                    //Mi_Sql.Append("'0.00' AS REDUCCION, ");
                    //if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    //{
                    //    Mi_Sql.Append(Cat_Sap_Det_Fte_Programa .Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID + " AS FUENTE_FINANCIAMIENTO_ID, ");
                    //    Mi_Sql.Append(Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID + " AS PROYECTO_PROGRAMA_ID, ");
                    //    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                    //    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS CLAVE_NOM_PROGRAMA, ");
                    //}
                    //else 
                    //{
                    //    Mi_Sql.Append("'' AS FUENTE_FINANCIAMIENTO_ID, ");
                    //    Mi_Sql.Append("'' AS PROYECTO_PROGRAMA_ID, ");
                    //    Mi_Sql.Append("'' AS CLAVE_NOM_PROGRAMA, ");
                    //}
                    //Mi_Sql.Append("'0.00' AS IMP_ENE, ");
                    //Mi_Sql.Append("'0.00' AS IMP_FEB, ");
                    //Mi_Sql.Append("'0.00' AS IMP_MAR, ");
                    //Mi_Sql.Append("'0.00' AS IMP_ABR, ");
                    //Mi_Sql.Append("'0.00' AS IMP_MAY, ");
                    //Mi_Sql.Append("'0.00' AS IMP_JUN, ");
                    //Mi_Sql.Append("'0.00' AS IMP_JUL, ");
                    //Mi_Sql.Append("'0.00' AS IMP_AGO, ");
                    //Mi_Sql.Append("'0.00' AS IMP_SEP, ");
                    //Mi_Sql.Append("'0.00' AS IMP_OCT, ");
                    //Mi_Sql.Append("'0.00' AS IMP_NOV, ");
                    //Mi_Sql.Append("'0.00' AS IMP_DIC ");
                    //Mi_Sql.Append(" FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                    //Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                    //Mi_Sql.Append(" ON " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Rubro_ID);
                    //Mi_Sql.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                    //Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                    //Mi_Sql.Append(" ON " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Tipo_ID);
                    //Mi_Sql.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                    //Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                    //Mi_Sql.Append(" ON " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID);
                    //Mi_Sql.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                    //if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    //{
                    //    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa);
                    //    Mi_Sql.Append(" ON " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID);
                    //    Mi_Sql.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                    //    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    //    Mi_Sql.Append(" ON " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID);
                    //    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    //    Mi_Sql.Append(" WHERE " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID);
                    //    Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim()+ "'");
                    //    Mi_Sql.Append(" AND " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID);
                    //    Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                    //}


                    //if (!String.IsNullOrEmpty(Negocio.P_Clase_ID))
                    //{
                    //    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    //    {
                    //        Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                    //        Mi_Sql.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                    //    }
                    //    else
                    //    {
                    //        Mi_Sql.Append(" WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                    //        Mi_Sql.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                    //    }
                    //}

                    //if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID))
                    //{
                    //    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    //    {
                    //        Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                    //        Mi_Sql.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    //    }
                    //    else
                    //    {
                    //        Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                    //        Mi_Sql.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    //    }
                    //}


                    //if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID))
                    //{
                    //    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    //    {
                    //        Mi_Sql.Append(" AND " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                    //        Mi_Sql.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                    //    }
                    //    else
                    //    {
                    //        Mi_Sql.Append(" WHERE " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                    //        Mi_Sql.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                    //    }
                    //}

                    //if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID))
                    //{
                    //    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    //    {
                    //        Mi_Sql.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                    //        Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                    //    }
                    //    else
                    //    {
                    //        Mi_Sql.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                    //        Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                    //    }
                    //}

                    //if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID))
                    //{
                    //    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    //    {
                    //        Mi_Sql.Append(" AND " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                    //        Mi_Sql.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                    //    }
                    //    else
                    //    {
                    //        Mi_Sql.Append(" WHERE " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                    //        Mi_Sql.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                    //    }
                    //}


                    //if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    //{
                    //    Mi_Sql.Append(" AND  " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                    //    Mi_Sql.Append(" NOT IN('00')");
                    //}
                    //else
                    //{
                    //    Mi_Sql.Append(" WHERE  " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                    //    Mi_Sql.Append(" NOT IN('00')");
                    //}

                    //if (String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    //{
                    //    Mi_Sql.Append(" UNION ");

                        Mi_Sql.Append("SELECT " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID + ", ");
                        Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + ", ");
                        Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ");
                        Mi_Sql.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + " + ' ' + ");
                        Mi_Sql.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion + " AS CLAVE_NOM_CONCEPTO, ");
                        Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + ", ");
                        Mi_Sql.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + ", ");
                        Mi_Sql.Append("'0.00' AS ESTIMADO, ");
                        Mi_Sql.Append("'0.00' AS RECAUDADO, ");
                        Mi_Sql.Append("'0.00' AS POR_RECAUDAR, ");
                        Mi_Sql.Append("'0.00' AS MODIFICADO, ");
                        Mi_Sql.Append("'0.00' AS AMPLIACION, ");
                        Mi_Sql.Append("'0.00' AS REDUCCION, ");
                        Mi_Sql.Append("'' AS FUENTE_FINANCIAMIENTO_ID, ");
                        Mi_Sql.Append("'' AS PROYECTO_PROGRAMA_ID, ");
                        Mi_Sql.Append("'' AS CLAVE_NOM_PROGRAMA, ");
                        Mi_Sql.Append("'0.00' AS IMP_ENE, ");
                        Mi_Sql.Append("'0.00' AS IMP_FEB, ");
                        Mi_Sql.Append("'0.00' AS IMP_MAR, ");
                        Mi_Sql.Append("'0.00' AS IMP_ABR, ");
                        Mi_Sql.Append("'0.00' AS IMP_MAY, ");
                        Mi_Sql.Append("'0.00' AS IMP_JUN, ");
                        Mi_Sql.Append("'0.00' AS IMP_JUL, ");
                        Mi_Sql.Append("'0.00' AS IMP_AGO, ");
                        Mi_Sql.Append("'0.00' AS IMP_SEP, ");
                        Mi_Sql.Append("'0.00' AS IMP_OCT, ");
                        Mi_Sql.Append("'0.00' AS IMP_NOV, ");
                        Mi_Sql.Append("'0.00' AS IMP_DIC ");
                        Mi_Sql.Append(" FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                        Mi_Sql.Append(" ON " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Rubro_ID);
                        Mi_Sql.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                        Mi_Sql.Append(" ON " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Tipo_ID);
                        Mi_Sql.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                        Mi_Sql.Append(" ON " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID);
                        Mi_Sql.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_Sql.Append(" INNER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                        Mi_Sql.Append(" ON " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_Sql.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);



                        if (!String.IsNullOrEmpty(Negocio.P_Clase_ID))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                            }
                        }


                        if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" AND " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                            }
                        }

                        if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID))
                        {
                            if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                            {
                                Mi_Sql.Append(" AND " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                            }
                            else
                            {
                                Mi_Sql.Append(" WHERE " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                                Mi_Sql.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                            }
                        }


                        //if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                        //{
                        //    Mi_Sql.Append(" AND  " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                        //    Mi_Sql.Append(" NOT IN('00')");
                        //}
                        //else
                        //{
                        //    Mi_Sql.Append(" WHERE  " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave);
                        //    Mi_Sql.Append(" NOT IN('00')");
                        //}
                    //}

                    Mi_Sql.Append(" ORDER BY CLAVE_NOM_CONCEPTO ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los datos de los ingresos. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Actualizar_Movimientos
            ///DESCRIPCIÓN          : consulta para actualizar los datos de los movimientos
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 05/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static Boolean Actualizar_Movimientos(Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
                Boolean Operacion_Completa = false;//Estado de la operacion.
                String Presupuesto_Ing_ID = String.Empty;
                Int32 Presupuesto_ID = 0;
                DataTable Dt_Mov = new DataTable();
                DataTable Dt_Mov_Nuevos = new DataTable();
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;
                Boolean Actualizado = false;
                String Actual_Psp = String.Empty;
                SqlDataAdapter Da_Datos = new SqlDataAdapter();
                DataSet Ds_Datos = new DataSet();
                

                try
                {
                    //obtenemos el estatus de actualizacion del presupuesto por si tenemos que dar de alta algun concepto en el presupuesto
                    Actualizado = Cls_Ope_Con_Poliza_Ingresos_Datos.Presupuesto_Actualizado(Cmd);
                    if (Actualizado)
                    {
                        Actual_Psp = "SI";
                    }
                    else
                    {
                        Actual_Psp = "NO";
                    }

                    //limpiamos las variables
                    Da_Datos = new SqlDataAdapter();
                    Ds_Datos = new DataSet();
                    Dt_Mov_Nuevos = new DataTable();

                    //obtenemos los movimientos que son de conceptos nuevos
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing_Det.Campo_Anio + " = " + Negocio.P_Anio);
                    Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing + " = " + Negocio.P_No_Movimiento_Ing.Trim());
                    Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Concepto + " = 'Nuevo'");

                    Cmd.CommandText = Mi_SQL.ToString().Trim();
                    Da_Datos = new SqlDataAdapter(Cmd);
                    Da_Datos.Fill(Ds_Datos);

                    if (Ds_Datos != null)
                    {
                        Dt_Mov_Nuevos = Ds_Datos.Tables[0];
                    }

                    //eliminamos los datos de los conceptos nuevos del presupuesto que ya no se ocuparan
                    if (Dt_Mov_Nuevos != null && Dt_Mov_Nuevos.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Mov_Nuevos.Rows) 
                        {
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("DELETE FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "' ");
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Dr["RUBRO_ID"].ToString().Trim() + "' ");
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Dr["TIPO_ID"].ToString().Trim() + "' ");
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Dr["CLASE_ING_ID"].ToString().Trim() + "' ");
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "' ");
                            if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "' ");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                            }
                            if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                            }
                            else
                            {
                                Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL");
                            }
                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();
                        }
                    }

                    //Eliminamos los datos de los movimientos
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("DELETE FROM " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing_Det.Campo_Anio + " = " + Negocio.P_Anio);
                    Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing + " = " + Negocio.P_No_Movimiento_Ing.Trim());
                    Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus + " = 'GENERADO'");

                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();

                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                    Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Ing.Campo_Total_Modificado + " = " + Negocio.P_Total_Modificado.Trim().Replace(",", "") + ", ");
                    Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico + "', ");
                    Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Fecha_Modifico + " = GETDATE()");
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing + " = " + Negocio.P_No_Movimiento_Ing.Trim());
                    Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();

                    Int32 Id_Movimiento = Convert.ToInt32(Consecutivo_ID(Ope_Psp_Movimiento_Ing_Det.Campo_Movimiento_Ing_ID, Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det, "10", Cmd));

                    //Obtenemos los datos de los movimientos de los conceptos de la modificacion
                    // y verificamos que el datatable tenga datos
                    Dt_Mov = Negocio.P_Dt_Mov_Conceptos;
                    if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
                    {
                        Presupuesto_Ing_ID = Consecutivo_ID(Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID, Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos, "10", Cmd);
                        Presupuesto_ID = Convert.ToInt32(Presupuesto_Ing_ID);

                        foreach (DataRow Dr in Dt_Mov.Rows)
                        {
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("INSERT INTO " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det + "(");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Movimiento_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Fuente_Financiamiento_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Rubro_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Clase_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Concepto_Ing_ID + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_SubConcepto_Ing_ID + ", ");
                            if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                            {
                                Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Proyecto_Programa_ID + ", ");
                            }
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Estatus + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Movimiento + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Justificacion + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Enero + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Febrero + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Marzo + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Abril + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Mayo + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Junio + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Julio + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Agosto + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Septiembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Octubre + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Noviembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Diciembre + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Total + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Aprobado + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Ampliacion + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Reduccion + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Importe_Modificado + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Anio + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Concepto + ", ");
                            if (!String.IsNullOrEmpty(Dr["NOMBRE_DOC"].ToString().Trim()))
                            {
                                Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Nombre_Documento + ", ");
                                Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Ruta_Documento + ", ");
                                Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Extension_Documento + ", ");
                            }
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Usuario_Creo + ", ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Fecha_Creo + ") VALUES( ");
                            Mi_SQL.Append("'" + String.Format("{0:0000000000}", Id_Movimiento) + "', ");
                            Mi_SQL.Append("'" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr["RUBRO_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr["TIPO_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr["CLASE_ING_ID"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "', ");
                            
                            if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                            {
                                Mi_SQL.Append("'" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "', ");
                            }
                            else 
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                            {
                                Mi_SQL.Append("'" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "', ");
                            }
                            Mi_SQL.Append("'" + Dr["ESTATUS"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr["TIPO_MOVIMIENTO"].ToString().Trim() + "', ");
                            Mi_SQL.Append("'" + Dr["JUSTIFICACION"].ToString().Trim() + "', ");
                            Mi_SQL.Append(Dr["IMP_ENE"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_FEB"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_MAR"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_ABR"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_MAY"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_JUN"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_JUL"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_AGO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_SEP"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_OCT"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_NOV"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_DIC"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["ESTIMADO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["AMPLIACION"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["REDUCCION"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Dr["MODIFICADO"].ToString().Trim().Replace(",", "") + ", ");
                            Mi_SQL.Append(Negocio.P_Anio + ", ");
                            Mi_SQL.Append(Negocio.P_No_Movimiento_Ing.Trim() + ", ");
                            Mi_SQL.Append("'" + Dr["TIPO_CONCEPTO"].ToString().Trim() + "', ");
                            if (!String.IsNullOrEmpty(Dr["NOMBRE_DOC"].ToString().Trim()))
                            {
                                Mi_SQL.Append("'" + Dr["NOMBRE_DOC"].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Dr["RUTA_DOC"].ToString().Trim() + "', ");
                                Mi_SQL.Append("'" + Dr["EXTENSION_DOC"].ToString().Trim() + "', ");
                            }
                            Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "', ");
                            Mi_SQL.Append("GETDATE()) ");

                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();

                            Id_Movimiento++;

                            if (Dr["TIPO_MOVIMIENTO"].ToString().Trim().Equals("AMPLIACION"))
                            {
                                if (Dr["TIPO_CONCEPTO"].ToString().Trim().Equals("Nuevo"))
                                {
                                    Mi_SQL = new StringBuilder();
                                    Mi_SQL.Append("INSERT INTO " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + " (");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Dependencia_ID + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + ", ");
                                    if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                    {
                                        Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + ", ");
                                    }
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ", ");

                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Enero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Febrero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Marzo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Abril + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Mayo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Junio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Julio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Agosto + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Septiembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Octubre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Noviembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Diciembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Enero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Febrero + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Marzo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Abril + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Mayo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Junio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Julio + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Agosto + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Septiembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Octubre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Noviembre + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Diciembre + ", ");

                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Actualizado + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Creo + ", ");
                                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Creo + ") VALUES (");
                                    Mi_SQL.Append("'" + Presupuesto_ID.ToString().Trim() + "', ");
                                    Mi_SQL.Append("'" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "', ");
                                    Mi_SQL.Append("NULL, ");
                                    Mi_SQL.Append("'" + Dr["RUBRO_ID"].ToString().Trim() + "', ");
                                    Mi_SQL.Append("'" + Dr["TIPO_ID"].ToString().Trim() + "', ");
                                    Mi_SQL.Append("'" + Dr["CLASE_ING_ID"].ToString().Trim() + "', ");
                                    Mi_SQL.Append("'" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "', ");

                                    if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                                    {
                                        Mi_SQL.Append("'" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "', ");
                                    }
                                    else
                                    {
                                        Mi_SQL.Append("NULL, ");
                                    }
                                    
                                    if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                    {
                                        Mi_SQL.Append("'" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "', ");
                                    }
                                    Mi_SQL.Append("'" + Negocio.P_Anio + "',");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");/*dic*/
                                    Mi_SQL.Append("0.00, ");/*tot*/
                                    Mi_SQL.Append("0.00, ");/*Apro*/
                                    Mi_SQL.Append("0.00, ");/*amp*/
                                    Mi_SQL.Append("0.00, ");/*red*/
                                    Mi_SQL.Append("0.00, ");/*mod*/
                                    Mi_SQL.Append("0.00, ");/*por_rec*/
                                    Mi_SQL.Append("0.00, ");/*dev*/
                                    Mi_SQL.Append("0.00, ");/*rec*/

                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");/*dic*/
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");/*dic*/
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");
                                    Mi_SQL.Append("0.00, ");

                                    Mi_SQL.Append("'" + Actual_Psp.Trim() + "', ");
                                    Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "',");
                                    Mi_SQL.Append("GETDATE())");
                                    Cmd.CommandText = Mi_SQL.ToString();
                                    Cmd.ExecuteNonQuery();

                                    Presupuesto_ID++;
                                }
                            }
                        }
                    }

                    Trans.Commit();
                    Operacion_Completa = true;
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al ejecutar el alta de los movimientos de ingresos. Error: [" + Ex.Message + "]");
                }
                return Operacion_Completa;
            }

            ///*******************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Estatus_Movimientos
            ///DESCRIPCIÓN          : consulta para obtener el estatus de la ultima modificacion
            ///PARAMETROS           1: Negocio: conexion con la capa de negocios
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************************************
            internal static String Consultar_Estatus_Movimientos(Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                String Estatus = String.Empty;
                try
                {
                    Mi_Sql.Append("SELECT ISNULL(" + Ope_Psp_Movimiento_Ing.Campo_Estatus + ", 'AUTORIZADO') AS ESTATUS ");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now));
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " = (");
                    Mi_Sql.Append(" SELECT MAX(" + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + ")");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now) + ")");

                    Estatus =  (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());

                    if(String.IsNullOrEmpty(Estatus))
                    {
                        Estatus = "AUTORIZADO";
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar el estatus de la ultima modificacion. Error: [" + Ex.Message + "]");
                }
                return Estatus;
            }

            ///*******************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Mov_Egresos
            ///DESCRIPCIÓN          : consulta para obtener el estatus de la ultima modificacion de egresos
            ///PARAMETROS           1: Negocio: conexion con la capa de negocios
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************************************
            internal static DataTable Consultar_Mov_Egresos()
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " AS NO_MOVIMIENTO, ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Egr.Campo_Estatus + ", ");
                    Mi_Sql.Append(" 'EGRESOS' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now));
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = (");
                    Mi_Sql.Append(" SELECT MAX(" + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + ")");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now) + ")");

                    Mi_Sql.Append(" UNION ");

                    Mi_Sql.Append("SELECT " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " AS NO_MOVIMIENTO, ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing.Campo_Estatus + ", ");
                    Mi_Sql.Append(" 'INGRESOS' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now));
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " = (");
                    Mi_Sql.Append(" SELECT MAX(" + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + ")");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now) + ")");

                    return (SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0]);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar el estatus de la ultima modificacion. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Estatus_Mov_Ing
            ///DESCRIPCIÓN          : consulta para obtener el estatus de la ultima modificacion
            ///PARAMETROS           1: Negocio: conexion con la capa de negocios
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 05/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************************************
            internal static String Consultar_Estatus_Mov_Ing(Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Ope_Psp_Movimiento_Ing.Campo_Estatus);
                    Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now));
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " = " + Negocio.P_No_Movimiento_Ing);

                    return (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar el estatus de la ultima modificacion. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Estatus_Mov
            ///DESCRIPCIÓN          : consulta para modificar el estatus de la modificacion
            ///PARAMETROS           1: 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************************************
           public static String Modificar_Estatus_Mov(String Anio, String No_Movimiento, String Usuario) 
            {
                StringBuilder Mi_SQL = new StringBuilder();
                DataTable Dt_Estatus = new DataTable();
                SqlConnection Cn = new SqlConnection();
                SqlCommand Cmd = new SqlCommand();
                SqlTransaction Trans;
                String Autorizados = String.Empty;
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmd.Connection = Cn;
                Cmd.Transaction = Trans;

                try
                {
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT ISNULL(" + Ope_Psp_Movimiento_Ing.Campo_Estatus_Ingresos + ", 'AUTORIZADO') AS EST_ING, ");
                    Mi_SQL.Append("ISNULL(" + Ope_Psp_Movimiento_Ing.Campo_Estatus_Egr_Municipal + ", 'AUTORIZADO') AS EST_EGR_RAMO");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " = " + No_Movimiento.Trim());
                    Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + Anio.Trim());

                    Dt_Estatus = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                    if (Dt_Estatus != null && Dt_Estatus.Rows.Count > 0)
                    {
                        if (Dt_Estatus.Rows[0]["EST_ING"].ToString().Trim().Equals("AUTORIZADO") && Dt_Estatus.Rows[0]["EST_EGR_MUN"].ToString().Trim().Equals("AUTORIZADO"))
                        {
                            Autorizados = "SI";

                            // SE MODIFICA EL ESTATUS DE LA MODIFICACION DE INGRESOS
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                            Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Ing.Campo_Estatus + " = 'AUTORIZADO', ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Usuario_Modifico + " = '" + Usuario.Trim() + "', ");
                            Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Fecha_Modifico + " = GETDATE()");
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " = " + No_Movimiento.Trim());
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + Anio.Trim());
                            Cmd.CommandText = Mi_SQL.ToString();
                            Cmd.ExecuteNonQuery();
                        }
                        else 
                        {
                            if (Dt_Estatus.Rows[0]["EST_ING"].ToString().Trim().Equals("AUTORIZADO"))
                            {
                                Autorizados = "Falta por Autorizar los movimientos del presupuesto de egresos";
                            }
                            else
                            {
                                Autorizados = "Falta por Autorizar los movimientos del presupuesto de ingresos";
                            }
                        }
                    }
                    Trans.Commit();
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar el estatus de la ultima modificacion. Error: [" + Ex.Message + "]");
                }
                return Autorizados;
            }

           ///*******************************************************************************************************
           ///NOMBRE DE LA FUNCIÓN : PreModificar_Estatus_Mov
           ///DESCRIPCIÓN          : consulta para modificar el estatus de la modificacion
           ///PARAMETROS           1: 
           ///CREO                 : Leslie González Vázquez
           ///FECHA_CREO           : 12/Junio/2012
           ///MODIFICO             :
           ///FECHA_MODIFICO       :
           ///CAUSA_MODIFICACIÓN   :
           ///*******************************************************************************************************
           public static String PreModificar_Estatus_Mov(String Anio, String No_Movimiento, String Usuario)
           {
               StringBuilder Mi_SQL = new StringBuilder();
               DataTable Dt_Estatus = new DataTable();
               SqlConnection Cn = new SqlConnection();
               SqlCommand Cmd = new SqlCommand();
               SqlTransaction Trans;
               String Autorizados = String.Empty;
               Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
               Cn.Open();
               Trans = Cn.BeginTransaction();
               Cmd.Connection = Cn;
               Cmd.Transaction = Trans;

               try
               {
                   Mi_SQL = new StringBuilder();
                   Mi_SQL.Append("SELECT ISNULL(" + Ope_Psp_Movimiento_Ing.Campo_Estatus_Ingresos + ", 'PREAUTORIZADO') AS EST_ING, ");
                   Mi_SQL.Append("ISNULL(" + Ope_Psp_Movimiento_Ing.Campo_Estatus_Egr_Municipal + ", 'PREAUTORIZADO') AS EST_EGR_MUN");
                   Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                   Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " = " + No_Movimiento.Trim());
                   Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + Anio.Trim());

                   Dt_Estatus = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                   if (Dt_Estatus != null && Dt_Estatus.Rows.Count > 0)
                   {
                       if (Dt_Estatus.Rows[0]["EST_ING"].ToString().Trim().Equals("PREAUTORIZADO") && Dt_Estatus.Rows[0]["EST_EGR_MUN"].ToString().Trim().Equals("PREAUTORIZADO"))
                       {
                           Autorizados = "SI";
                           // SE MODIFICA EL ESTATUS DE LA MODIFICACION DE INGRESOS
                           Mi_SQL = new StringBuilder();
                           Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                           Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Ing.Campo_Estatus + " = 'PREAUTORIZADO', ");
                           Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Usuario_Modifico + " = '" + Usuario.Trim() + "', ");
                           Mi_SQL.Append(Ope_Psp_Movimiento_Ing.Campo_Fecha_Modifico + " = GETDATE()");
                           Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing + " = " + No_Movimiento.Trim());
                           Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_Anio + " = " + Anio.Trim());
                           Cmd.CommandText = Mi_SQL.ToString();
                           Cmd.ExecuteNonQuery();
                       }
                       else
                       {
                           if (Dt_Estatus.Rows[0]["EST_ING"].ToString().Trim().Equals("PREAUTORIZADO"))
                           {
                               Autorizados = "Falta por Cerrar el periodo de los movimientos del presupuesto de egresos";
                           }
                           else
                           {
                               Autorizados = "Falta por Cerrar el periodo de los movimientos del presupuesto de ingresos";
                           }
                       }
                   }
                   Trans.Commit();
               }
               catch (Exception Ex)
               {
                   throw new Exception("Error al intentar consultar el estatus de la ultima modificacion. Error: [" + Ex.Message + "]");
               }
               return Autorizados;
           }

           ///*****************************************************************************************************
           ///NOMBRE DE LA FUNCIÓN : Consultar_Programas
           ///DESCRIPCIÓN          : consulta para obtener los datos de los programas
           ///PARAMETROS           :
           ///CREO                 : Leslie González Vázquez
           ///FECHA_CREO           : 16/Julio/2012
           ///MODIFICO             :
           ///FECHA_MODIFICO       :
           ///CAUSA_MODIFICACIÓN   :
           ///******************************************************************************************************
           internal static DataTable Consultar_Programas(Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio)
           {
               StringBuilder Mi_Sql = new StringBuilder();
               try
               {
                   //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                   //Mi_Sql.Append("SELECT DISTINCT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                   //Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                   //Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ", ");
                   //Mi_Sql.Append("ISNULL(SUM(" + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Importe + "),0) AS IMPORTE ");
                   //Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                   //Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa);
                   //Mi_Sql.Append(" ON " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID);
                   //Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);

                   //if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                   //{
                   //    Mi_Sql.Append(" WHERE " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID);
                   //    Mi_Sql.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                   //}

                   //if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                   //{
                   //    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                   //    {
                   //        Mi_Sql.Append(" AND " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID);
                   //        Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                   //    }
                   //    else
                   //    {
                   //        Mi_Sql.Append(" WHERE " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "." + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID);
                   //        Mi_Sql.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                   //    }
                   //}

                   Mi_Sql.Append("SELECT DISTINCT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                   Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                   Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ", ");
                   Mi_Sql.Append(" 0 AS IMPORTE ");
                   Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);

                   if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                   {
                       if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                       {
                           Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                           Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                       }
                       else 
                       {
                           Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                           Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                       }
                   }

                   if (!String.IsNullOrEmpty(Negocio.P_Busqueda.Trim()))
                   {
                       if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                       {
                           Mi_Sql.Append(" AND (" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                           Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion);
                           Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                           Mi_Sql.Append(" OR " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                           Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion);
                           Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                           Mi_Sql.Append(" OR " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                           Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion);
                           Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToLower() + "%')");
                       }
                       else 
                       {
                           Mi_Sql.Append(" WHERE (" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                           Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion);
                           Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToUpper() + "%'");
                           Mi_Sql.Append(" OR " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                           Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion);
                           Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim() + "%'");
                           Mi_Sql.Append(" OR " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                           Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion);
                           Mi_Sql.Append(" LIKE '%" + Negocio.P_Busqueda.Trim().ToLower() + "%')");
                       }
                   }

                   //Mi_Sql.Append(" GROUP BY " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ", ");
                   //Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + ", ");
                   //Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);

                   Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                   return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
               }
               catch (Exception Ex)
               {
                   throw new Exception("Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]");
               }
           }

           ///*******************************************************************************
           ///NOMBRE DE LA FUNCIÓN : Consulta_Fte_Financiamiento
           ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento
           ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
           ///CREO                 : Leslie Gonzalez Vázquez
           ///FECHA_CREO           : 13/Abril/2012
           ///MODIFICO             :
           ///FECHA_MODIFICO       :
           ///CAUSA_MODIFICACIÓN   :
           ///*******************************************************************************
           internal static DataTable Consulta_Fte_Financiamiento()
           {
               StringBuilder Mi_Sql = new StringBuilder();
               try
               {
                   //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                   Mi_Sql.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                   Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                   Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                   Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                   Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Estatus + " = 'ACTIVO'");
                   //Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Campo_Anio+ " = '" + String.Format("{0:yyyy}", DateTime.Now) + "'");

                   Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                   return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
               }
               catch (Exception Ex)
               {
                   throw new Exception("Error al intentar consultar los registros de las fuentes de finannciamiento. Error: [" + Ex.Message + "]");
               }
           }

           ///*******************************************************************************
           ///NOMBRE DE LA FUNCIÓN : Consulta_Fte_Financiamiento_Ramo_XXXIII
           ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento de ramo 33
           ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
           ///CREO                 : Leslie Gonzalez Vázquez
           ///FECHA_CREO           : 23/Noviembre/2012
           ///MODIFICO             :
           ///FECHA_MODIFICO       :
           ///CAUSA_MODIFICACIÓN   :
           ///*******************************************************************************
           internal static DataTable Consulta_Fte_Financiamiento_Ramo_XXXIII()
           {
               StringBuilder Mi_Sql = new StringBuilder();
               try
               {
                   //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                   Mi_Sql.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                   Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                   Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                   Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                   Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Estatus + " = 'ACTIVO'");
                   Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33+ " = 'SI'");

                   Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                   return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
               }
               catch (Exception Ex)
               {
                   throw new Exception("Error al intentar consultar los registros de las fuentes de finannciamiento de ramo XXXIII. Error: [" + Ex.Message + "]");
               }
           }

           ///*******************************************************************************
           ///NOMBRE DE LA FUNCIÓN : Alta_Movimientos_Ramo33
           ///DESCRIPCIÓN          : consulta para registrar los datos de los movimientos de ramo 33
           ///PARAMETROS           1 Negocio: conexion con la capa de negocios
           ///CREO                 : Leslie Gonzalez Vázquez
           ///FECHA_CREO           : 23/Noviembre/2012
           ///MODIFICO             :
           ///FECHA_MODIFICO       :
           ///CAUSA_MODIFICACIÓN   :
           ///*******************************************************************************
           internal static String Alta_Movimientos_Ramo33(Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio)
           {
               StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
               String Autorizados = String.Empty;//Estado de la operacion.
               DataTable Dt_Mov_Conceptos = new DataTable();
               DataTable Dt_Psp_Ing = new DataTable();
               DataTable Dt_Estatus = new DataTable();
               String[] Datos_Poliza_PSP = null;
               String No_Poliza_PSP = String.Empty;
               String Tipo_Poliza_PSP = String.Empty;
               String Mes_Anio_PSP = String.Empty;

               SqlConnection Cn = new SqlConnection();
               SqlCommand Cmd = new SqlCommand();
               SqlTransaction Trans;
               Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
               Cn.Open();
               Trans = Cn.BeginTransaction();
               Cmd.Connection = Cn;
               Cmd.Transaction = Trans;

               try
               {
                   Dt_Mov_Conceptos = Negocio.P_Dt_Mov_Conceptos;

                   if (Dt_Mov_Conceptos != null && Dt_Mov_Conceptos.Rows.Count > 0)
                   {
                       //creamos la poliza presupuestal del movimiento
                       //obtenemos los datos de la poliza presupuestal
                       Datos_Poliza_PSP = Registro_Mov_Autorizado(Cmd, Negocio.P_Dt_Mov_Conceptos, null,
                           Negocio.P_Anio, Negocio.P_No_Movimiento_Ing.Trim());

                       if (Datos_Poliza_PSP != null)
                       {
                           if (Datos_Poliza_PSP.Length > 0)
                           {
                               No_Poliza_PSP = Datos_Poliza_PSP[0].Trim();
                               Tipo_Poliza_PSP = Datos_Poliza_PSP[1].Trim();
                               Mes_Anio_PSP = Datos_Poliza_PSP[2].Trim();
                           }
                       }

                       foreach (DataRow Dr in Dt_Mov_Conceptos.Rows)
                       {
                           //modificamos el estatus del movimiento de ingresos
                           Mi_SQL = new StringBuilder();
                           Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                           Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Ing_Det.Campo_Estatus + " = '" + Dr["ESTATUS"].ToString().Trim() + "', ");
                           Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico + "', ");

                           if (!String.IsNullOrEmpty(No_Poliza_PSP) && !String.IsNullOrEmpty(Tipo_Poliza_PSP)
                                           && !String.IsNullOrEmpty(Mes_Anio_PSP))
                           {
                               Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_No_Poliza_Presupuestal + " = '" + No_Poliza_PSP.Trim() + "', ");
                               Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Tipo_Poliza_ID_Presupuestal + " = '" + Tipo_Poliza_PSP.Trim() + "', ");
                               Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Mes_Anio_Presupuestal + " = '" + Mes_Anio_PSP.Trim() + "', ");
                           }

                           Mi_SQL.Append(Ope_Psp_Movimiento_Ing_Det.Campo_Fecha_Modifico + " = GETDATE()");
                           Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Ing_Det.Campo_Movimiento_Ing_ID + " = " + Dr["MOVIMIENTO_ING_ID"].ToString().Trim());
                           Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing + " = " + Negocio.P_No_Movimiento_Ing.Trim());
                           Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Ing_Det.Campo_Anio + " = " + Negocio.P_Anio.Trim());

                           Cmd.CommandText = Mi_SQL.ToString();
                           Cmd.ExecuteNonQuery();

                           if (Dr["TIPO_MOVIMIENTO"].ToString().Trim().Equals("AMPLIACION"))
                           {
                               //MODIFICAMOS EL IMPORTE DE LA AMPLIACION EN EL PRESUPUESTO
                               Mi_SQL = new StringBuilder();
                               Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                               Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ",0) + " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ",0) + " + Dr["IMP_ENE"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ",0) + " + Dr["IMP_FEB"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ",0) + " + Dr["IMP_MAR"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ",0) + " + Dr["IMP_ABR"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ",0) + " + Dr["IMP_MAY"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ",0) + " + Dr["IMP_JUN"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ",0) + " + Dr["IMP_JUL"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ",0) + " + Dr["IMP_AGO"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ",0) + " + Dr["IMP_SEP"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ",0) + " + Dr["IMP_OCT"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ",0) + " + Dr["IMP_NOV"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ",0) + " + Dr["IMP_DIC"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ",0) + " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + ",0) + " + Dr["IMP_ENE"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + ",0) + " + Dr["IMP_FEB"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + ",0) + " + Dr["IMP_MAR"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + ",0) + " + Dr["IMP_ABR"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + ",0) + " + Dr["IMP_MAY"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + ",0) + " + Dr["IMP_JUN"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + ",0) + " + Dr["IMP_JUL"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + ",0) + " + Dr["IMP_AGO"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + ",0) + " + Dr["IMP_SEP"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + ",0) + " + Dr["IMP_OCT"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + ",0) + " + Dr["IMP_NOV"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + ",0) + " + Dr["IMP_DIC"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico + "', ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Modifico + " = GETDATE()");
                               Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Dr["RUBRO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Dr["TIPO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Dr["CLASE_ING_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "' ");
                               if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "' ");
                               }
                               else
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                               }
                               if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                               }
                               else
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL");
                               }

                               Cmd.CommandText = Mi_SQL.ToString();
                               Cmd.ExecuteNonQuery();

                               Mi_SQL = new StringBuilder();
                               Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                               Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + " =  " + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + " - ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + " + " + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion);
                               Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Dr["RUBRO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Dr["TIPO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Dr["CLASE_ING_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "' ");
                               if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "' ");
                               }
                               else
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                               }
                               if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                               }
                               else
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL");
                               }
                               Cmd.CommandText = Mi_SQL.ToString();
                               Cmd.ExecuteNonQuery();

                               Mi_SQL = new StringBuilder();
                               Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                               Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + " =  " + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + " - ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado);
                               Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Dr["RUBRO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Dr["TIPO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Dr["CLASE_ING_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "' ");
                               if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "' ");
                               }
                               else
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                               }
                               if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                               }
                               else
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL");
                               }

                               Cmd.CommandText = Mi_SQL.ToString();
                               Cmd.ExecuteNonQuery();

                               //MODIFICAMOS EL IMPORTE DEL PROGRAMA AL QUE SE LE ISO LA AMPLIACION
                               if (Dr["TIPO_CONCEPTO"].ToString().Trim().Equals("Existente"))
                               {
                                   if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                   {
                                       //MODIFICAMOS EL IMPORTE DEL PROGRAMA
                                       Mi_SQL = new StringBuilder();
                                       Mi_SQL.Append("UPDATE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                                       Mi_SQL.Append(" SET " + Cat_Sap_Proyectos_Programas.Campo_Importe + " = ");
                                       Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Importe + " + " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", ""));
                                       Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                       Cmd.CommandText = Mi_SQL.ToString();
                                       Cmd.ExecuteNonQuery();

                                       //MODIFICAMOS EL IMPORTE DEL CONCEPTO Y LA FF DEL PROGRAMA
                                       Mi_SQL = new StringBuilder();
                                       Mi_SQL.Append("UPDATE " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa);
                                       Mi_SQL.Append(" SET " + Cat_Sap_Det_Fte_Programa.Campo_Importe + " = ");
                                       Mi_SQL.Append(Cat_Sap_Det_Fte_Programa.Campo_Importe + " + " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", ""));
                                       Mi_SQL.Append(" WHERE " + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                       Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                       Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "'");
                                       Cmd.CommandText = Mi_SQL.ToString();
                                       Cmd.ExecuteNonQuery();
                                   }
                               }
                           }
                           else
                           { //modificamos la reduccion del movimiento
                               Mi_SQL = new StringBuilder();
                               Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                               Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ",0) + " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ",0) - " + Dr["IMP_ENE"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ",0) - " + Dr["IMP_FEB"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ",0) - " + Dr["IMP_MAR"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ",0) - " + Dr["IMP_ABR"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ",0) - " + Dr["IMP_MAY"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ",0) - " + Dr["IMP_JUN"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ",0) - " + Dr["IMP_JUL"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ",0) - " + Dr["IMP_AGO"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ",0) - " + Dr["IMP_SEP"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ",0) - " + Dr["IMP_OCT"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ",0) - " + Dr["IMP_NOV"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ",0) - " + Dr["IMP_DIC"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ",0) - " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + ",0) - " + Dr["IMP_ENE"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + ",0) - " + Dr["IMP_FEB"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + ",0) - " + Dr["IMP_MAR"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + ",0) - " + Dr["IMP_ABR"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + ",0) - " + Dr["IMP_MAY"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + ",0) - " + Dr["IMP_JUN"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + ",0) - " + Dr["IMP_JUL"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + ",0) - " + Dr["IMP_AGO"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + ",0) - " + Dr["IMP_SEP"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + ",0) - " + Dr["IMP_OCT"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + ",0) - " + Dr["IMP_NOV"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + ",0) - " + Dr["IMP_DIC"].ToString().Trim().Replace(",", "") + ", ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico + "', ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Modifico + " = GETDATE()");
                               Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Dr["RUBRO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Dr["TIPO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Dr["CLASE_ING_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "' ");
                               if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "' ");
                               }
                               else
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                               }
                               if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                               }
                               else
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL");
                               }
                               Cmd.CommandText = Mi_SQL.ToString();
                               Cmd.ExecuteNonQuery();

                               //modificamos la columna de modificado con el nuevo monto
                               Mi_SQL = new StringBuilder();
                               Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                               Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + " =  " + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + " - ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + " + " + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion);
                               Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Dr["RUBRO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Dr["TIPO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Dr["CLASE_ING_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "' ");
                               if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "' ");
                               }
                               else
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                               }
                               if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                               }
                               else
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL");
                               }

                               Cmd.CommandText = Mi_SQL.ToString();
                               Cmd.ExecuteNonQuery();

                               //modificamos el por recaudar del presupuesto
                               Mi_SQL = new StringBuilder();
                               Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                               Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + " =  " + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + " - ");
                               Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado);
                               Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = '" + Negocio.P_Anio + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " = '" + Dr["RUBRO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = '" + Dr["TIPO_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = '" + Dr["CLASE_ING_ID"].ToString().Trim() + "' ");
                               Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "' ");
                               if (!String.IsNullOrEmpty(Dr["SUBCONCEPTO_ING_ID"].ToString().Trim()))
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " = '" + Dr["SUBCONCEPTO_ING_ID"].ToString().Trim() + "' ");
                               }
                               else
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                               }
                               if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "' ");
                               }
                               else
                               {
                                   Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID + " IS NULL");
                               }

                               Cmd.CommandText = Mi_SQL.ToString();
                               Cmd.ExecuteNonQuery();

                               //MODIFICAMOS EL IMPORTE DEL PROGRAMA AL QUE SE LE ISO LA reduccion
                               if (Dr["TIPO_CONCEPTO"].ToString().Trim().Equals("Existente"))
                               {
                                   if (!String.IsNullOrEmpty(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                   {
                                       //MODIFICAMOS EL IMPORTE DEL PROGRAMA
                                       Mi_SQL = new StringBuilder();
                                       Mi_SQL.Append("UPDATE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                                       Mi_SQL.Append(" SET " + Cat_Sap_Proyectos_Programas.Campo_Importe + " = ");
                                       Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Importe + " - " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", ""));
                                       Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                       Cmd.CommandText = Mi_SQL.ToString();
                                       Cmd.ExecuteNonQuery();

                                       //MODIFICAMOS EL IMPORTE DEL CONCEPTO Y LA FF DEL PROGRAMA
                                       Mi_SQL = new StringBuilder();
                                       Mi_SQL.Append("UPDATE " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa);
                                       Mi_SQL.Append(" SET " + Cat_Sap_Det_Fte_Programa.Campo_Importe + " = ");
                                       Mi_SQL.Append(Cat_Sap_Det_Fte_Programa.Campo_Importe + " - " + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", ""));
                                       Mi_SQL.Append(" WHERE " + Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                       Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                       Mi_SQL.Append(" AND " + Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID + " = '" + Dr["CONCEPTO_ING_ID"].ToString().Trim() + "'");
                                       Cmd.CommandText = Mi_SQL.ToString();
                                       Cmd.ExecuteNonQuery();
                                   }
                               }
                           }
                       }
                   }
                   Trans.Commit();
                   Autorizados = "SI";
               }
               catch (Exception Ex)
               {
                   throw new Exception("Error al ejecutar los la modificación de los movimeintos de ingresos. Error: [" + Ex.Message + "]");
               }
               return Autorizados;
           }

           ///*******************************************************************************
           ///NOMBRE DE LA FUNCIÓN : Consecutivo_ID
           ///DESCRIPCIÓN          : consulta para obtener el consecutivo de una tabla
           ///PARAMETROS           1 Campo_Id: campo del que se obtendra el consecutivo
           ///                     2 Tabla: tabla del que se obtendra el consecutivo
           ///                     3 Tamaño: longitud del campo 
           ///CREO                 : Leslie Gonzalez Vázquez
           ///FECHA_CREO           : 15/Marzo/2012
           ///MODIFICO             :
           ///FECHA_MODIFICO       :
           ///CAUSA_MODIFICACIÓN   :
           ///*******************************************************************************
           internal static String Consecutivo_ID(String Campo_Id, String Tabla, String Tamaño, SqlCommand Comando)
           {
               String Consecutivo = "";
               StringBuilder Mi_SQL = new StringBuilder();
               object Id; //Obtiene el ID con la cual se guardo los datos en la base de datos

               if (Tamaño.Equals("5"))
               {
                   Mi_SQL.Append("SELECT ISNULL(MAX (" + Campo_Id + "), '00000')");
                   Mi_SQL.Append(" FROM " + Tabla);

                   Comando.CommandText = Mi_SQL.ToString().Trim();
                   Id = Comando.ExecuteScalar();

                   if (Convert.IsDBNull(Id))
                   {
                       Consecutivo = "00001";
                   }
                   else
                   {
                       Consecutivo = string.Format("{0:00000}", Convert.ToInt32(Id) + 1);
                   }
               }
               else if (Tamaño.Equals("10"))
               {
                   Mi_SQL.Append("SELECT ISNULL(MAX (" + Campo_Id + "), '0000000000')");
                   Mi_SQL.Append(" FROM " + Tabla);

                   Comando.CommandText = Mi_SQL.ToString().Trim();
                   Id = Comando.ExecuteScalar();

                   if (Convert.IsDBNull(Id))
                   {
                       Consecutivo = "0000000001";
                   }
                   else
                   {
                       Consecutivo = string.Format("{0:0000000000}", Convert.ToInt32(Id) + 1);
                   }
               }

               return Consecutivo;
           }

           ///*******************************************************************************
           ///NOMBRE DE LA FUNCIÓN : Registro_Mov_Autorizado
           ///DESCRIPCIÓN          : Metodo para insertar la poliza presupuestal de autorizado
           ///PARAMETROS           :
           ///CREO                 : Leslie Gonzalez Vázquez
           ///FECHA_CREO           : 04/Abril/2013
           ///MODIFICO             :
           ///FECHA_MODIFICO       :
           ///CAUSA_MODIFICACIÓN   :
           ///*******************************************************************************
           internal static String[] Registro_Mov_Autorizado(SqlCommand Cmd, DataTable Dt_Conceptos, DataTable Dt_Autorizados, 
               String Anio, String No_Modificacion)
           {
               String[] No_Poliza = null;
               DataTable Dt_Psp_Ing = new DataTable();
               DataRow Fila;

               try
               {
                   //creamos el datatable de los registros
                   Dt_Psp_Ing.Columns.Add("Fte_Financiamiento_ID", typeof(System.String));
                   Dt_Psp_Ing.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                   Dt_Psp_Ing.Columns.Add(Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID, typeof(System.String));
                   Dt_Psp_Ing.Columns.Add(Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID, typeof(System.String));
                   Dt_Psp_Ing.Columns.Add("Anio", typeof(System.String));
                   Dt_Psp_Ing.Columns.Add("Importe", typeof(System.String));

                   if (Dt_Autorizados != null)
                   {
                       //unificamos el datatable 
                       foreach (DataRow Dr_Au in Dt_Autorizados.Rows)
                       {
                           if (Dr_Au["ESTATUS"].ToString().Trim().Equals("AUTORIZADO"))
                           {
                               if (Dt_Conceptos != null)
                               {
                                   foreach (DataRow Dr in Dt_Conceptos.Rows)
                                   {
                                       if (Dr_Au["MOVIMIENTO_ING_ID"].ToString().Equals(Dr["MOVIMIENTO_ING_ID"].ToString()))
                                       {
                                           Fila = Dt_Psp_Ing.NewRow();
                                           Fila[0] = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                           Fila[1] = Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim();
                                           Fila[2] = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                                           Fila[3] = Dr["SUBCONCEPTO_ING_ID"].ToString().Trim();
                                           Fila[4] = Anio.Trim();

                                           if (Dr["TIPO_MOVIMIENTO"].ToString().Trim().Equals("AMPLIACION"))
                                           {
                                               Fila[5] = Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "");
                                           }
                                           else
                                           {
                                               Fila[5] = "-" + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "");
                                           }

                                           Dt_Psp_Ing.Rows.Add(Fila);

                                           break;
                                       }
                                   }
                               }
                           }
                       }
                   }
                   else 
                   {
                       if (Dt_Conceptos != null)
                       {
                           foreach (DataRow Dr in Dt_Conceptos.Rows)
                           {
                               Fila = Dt_Psp_Ing.NewRow();
                               Fila[0] = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                               Fila[1] = Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim();
                               Fila[2] = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                               Fila[3] = Dr["SUBCONCEPTO_ING_ID"].ToString().Trim();
                               Fila[4] = Anio.Trim();

                               if (Dr["TIPO_MOVIMIENTO"].ToString().Trim().Equals("AMPLIACION"))
                               {
                                   Fila[5] = Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "");
                               }
                               else
                               {
                                   Fila[5] = "-" + Dr["IMP_TOTAL"].ToString().Trim().Replace(",", "");
                               }

                               Dt_Psp_Ing.Rows.Add(Fila);
                            }
                       }
                   }

                   

                   if (Dt_Psp_Ing.Rows.Count > 0)
                   {
                       No_Poliza = Cls_Ope_Con_Poliza_Ingresos_Datos.Alta_Movimientos_Presupuestales(Dt_Psp_Ing, Cmd, "Registro " + No_Modificacion + "a. Modificación Presupuesto Ingresos", "", "", "",
                       Ope_Psp_Presupuesto_Ingresos.Campo_Modificado, Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar);
                   }
               }
               catch (Exception Ex)
               {
                   throw new Exception("Error al guardar los datos del registro de movimiento. Error[" + Ex.Message + "]");
               }
               return No_Poliza;
           }
        #endregion
    }
}
