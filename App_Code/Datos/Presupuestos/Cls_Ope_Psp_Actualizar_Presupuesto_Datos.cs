﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Actualizar_Presupuesto.Negocio;
using JAPAMI.Constantes;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using System.Text;

/// <summary>
/// Summary description for Cls_Ope_Psp_Actualizar_Presupuesto_Datos
/// </summary>
namespace JAPAMI.Actualizar_Presupuesto.Datos
{
    public class Cls_Ope_Psp_Actualizar_Presupuesto_Datos
    {

        public static DataTable Consultar_Fte_Financiamiento()
        {
            String Mi_SQL = "SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Clave;
            Mi_SQL = Mi_SQL +  " FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Estatus + "='ACTIVO'";

            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        public static DataTable Consultar_Areas_Funcionales()
        {
            String Mi_SQL = "SELECT " + Cat_SAP_Area_Funcional.Campo_Clave;
            Mi_SQL = Mi_SQL + " FROM " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_SAP_Area_Funcional.Campo_Estatus + "='ACTIVO'";
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        public static DataTable Consultar_Programas()
        {
            String Mi_SQL = "SELECT " + Cat_Sap_Proyectos_Programas.Campo_Clave;
            Mi_SQL = Mi_SQL + " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Estatus + "='ACTIVO'";
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        public static DataTable Consultar_Unidad_Responsable()
        {
            String Mi_SQL = "SELECT " + Cat_Dependencias.Campo_Clave + " FROM " + Cat_Dependencias.Tabla_Cat_Dependencias;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Dependencias.Campo_Estatus + "='ACTIVO'";
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        public static DataTable Consultar_Partidas()
        {
            String Mi_SQL = "SELECT " + Cat_Sap_Partidas_Especificas.Campo_Clave + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas;
            Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Estatus + "='ACTIVO'";
            return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
        }

        public static String Actualizar_Presupuesto(Cls_Ope_Psp_Actualizar_Presupuesto_Negocio Clase_Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            String Msj = "";
            int Registros_Nuevos = 0;
            int Actualizaciones = 0;
            int Registro_Invalido = 0;
            if (Clase_Negocio.P_Dt_Presupuesto.Rows.Count > 0)
            {
                try{
                for (int i = 0; i < Clase_Negocio.P_Dt_Presupuesto.Rows.Count; i++)
                {
                    if(Clase_Negocio.P_Dt_Presupuesto.Rows[i]["CORRECTO"].ToString().Trim() != "NO")
                    {
                        //HAcemos la consulta para verificar que este exista 
                    Mi_SQL.Append("SELECT " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado +"." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre);
                    Mi_SQL.Append(", " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total);
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_SQL.Append("," + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_SQL.Append("," + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                    Mi_SQL.Append("," + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_SQL.Append("," + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_SQL.Append("," + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_SQL.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_SQL.Append("=" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_SQL.Append(" AND " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                    Mi_SQL.Append("=" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID);
                    Mi_SQL.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_SQL.Append("=" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_SQL.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_SQL.Append("=" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_SQL.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_SQL.Append("=" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_SQL.Append(" = " + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["AÑO"].ToString().Trim());
                    
                    Mi_SQL.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                    Mi_SQL.Append(" = '" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["FF"].ToString().Trim() +"'");
                    Mi_SQL.Append(" AND " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave);
                    Mi_SQL.Append(" = '" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["AF"].ToString().Trim() + "'");
                    Mi_SQL.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas+ "." + Cat_Sap_Proyectos_Programas.Campo_Clave);
                    Mi_SQL.Append(" = '" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["PROGRAMA"].ToString().Trim() + "'");
                    Mi_SQL.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);
                    Mi_SQL.Append(" = '" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["UR"].ToString().Trim() + "'");
                    Mi_SQL.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave);
                    Mi_SQL.Append(" = '" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["PARTIDA"].ToString().Trim() + "'");

                    DataTable Dt_Presupuesto = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                    //Validamos si existe algun registro con los filtros realizamos un UPDATE 
                    if (Dt_Presupuesto.Rows.Count > 0)
                    {

                        //limpiamos la variable 
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero);
                        Mi_SQL.Append("=" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["ENERO"].ToString().Trim());
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero);
                        Mi_SQL.Append("=" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["FEBRERO"].ToString().Trim());
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo);
                        Mi_SQL.Append("=" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["MARZO"].ToString().Trim());
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril);
                        Mi_SQL.Append("=" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["ABRIL"].ToString().Trim());
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo);
                        Mi_SQL.Append("=" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["MAYO"].ToString().Trim());
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio);
                        Mi_SQL.Append("=" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["JUNIO"].ToString().Trim());
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio);
                        Mi_SQL.Append("=" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["JULIO"].ToString().Trim());
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto);
                        Mi_SQL.Append("=" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["AGOSTO"].ToString().Trim());
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre);
                        Mi_SQL.Append("=" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["SEPTIEMBRE"].ToString().Trim());
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre);
                        Mi_SQL.Append("=" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["OCTUBRE"].ToString().Trim());
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre);
                        Mi_SQL.Append("=" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["NOVIEMBRE"].ToString().Trim());
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre);
                        Mi_SQL.Append("=" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["DICIEMBRE"].ToString().Trim());
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total);
                        Mi_SQL.Append("=" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["TOTAL"].ToString().Trim());
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append("=" + Dt_Presupuesto.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Anio].ToString().Trim());
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_SQL.Append(" = '" + Dt_Presupuesto.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID].ToString().Trim()+"'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID );
                        Mi_SQL.Append("='" + Dt_Presupuesto.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID].ToString().Trim() + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_SQL.Append("='" + Dt_Presupuesto.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID].ToString().Trim() + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append("='" + Dt_Presupuesto.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID].ToString().Trim() + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_SQL.Append("='" + Dt_Presupuesto.Rows[0][Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID].ToString().Trim() + "'");

                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        Actualizaciones = Actualizaciones + 1;

                    }
                    //SI NO EXISTE ningun registro realizamos un INSERT
                    else
                    {
                        //limpiamos la variable 
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("INSERT INTO " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_SQL.Append("(" + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre);
                        Mi_SQL.Append("," + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre);
                        Mi_SQL.Append(") VALUES (");
                        Mi_SQL.Append(Clase_Negocio.P_Dt_Presupuesto.Rows[i]["AÑO"].ToString().Trim());
                        Mi_SQL.Append(",( SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                        Mi_SQL.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                        Mi_SQL.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Clave);
                        Mi_SQL.Append(" ='" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["FF"].ToString().Trim() + "')");
                        Mi_SQL.Append(", (SELECT " + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                        Mi_SQL.Append(" FROM " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                        Mi_SQL.Append(" WHERE " + Cat_SAP_Area_Funcional.Campo_Clave);
                        Mi_SQL.Append("='" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["AF"].ToString().Trim() +"')");
                        Mi_SQL.Append(", (SELECT " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                        Mi_SQL.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                        Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Clave);
                        Mi_SQL.Append("='" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["PROGRAMA"].ToString().Trim() + "')");
                        Mi_SQL.Append(", (SELECT " + Cat_Dependencias.Campo_Dependencia_ID);
                        Mi_SQL.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                        Mi_SQL.Append(" WHERE " + Cat_Dependencias.Campo_Clave);
                        Mi_SQL.Append("='" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["UR"].ToString().Trim() + "')");
                        Mi_SQL.Append(", (SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID );
                        Mi_SQL.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                        Mi_SQL.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Campo_Clave);
                        Mi_SQL.Append("='" + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["PARTIDA"].ToString().Trim() + "')");
                        Mi_SQL.Append(", " + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["ENERO"].ToString().Trim());
                        Mi_SQL.Append(", " + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["FEBRERO"].ToString().Trim());
                        Mi_SQL.Append(", " + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["MARZO"].ToString().Trim());
                        Mi_SQL.Append(", " + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["ABRIL"].ToString().Trim());
                        Mi_SQL.Append(", " + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["MAYO"].ToString().Trim());
                        Mi_SQL.Append(", " + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["JUNIO"].ToString().Trim());
                        Mi_SQL.Append(", " + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["JULIO"].ToString().Trim());
                        Mi_SQL.Append(", " + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["AGOSTO"].ToString().Trim());
                        Mi_SQL.Append(", " + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["SEPTIEMBRE"].ToString().Trim());
                        Mi_SQL.Append(", " + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["OCTUBRE"].ToString().Trim());
                        Mi_SQL.Append(", " + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["NOVIEMBRE"].ToString().Trim());
                        Mi_SQL.Append(", " + Clase_Negocio.P_Dt_Presupuesto.Rows[i]["DICIEMBRE"].ToString().Trim());
                        Mi_SQL.Append(")");


                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        Registros_Nuevos = Registros_Nuevos + 1;




                    }
                }//fin del If Global 
                else
                {
                        Registro_Invalido = Registro_Invalido + 1;
                }
                }
                }catch(Exception ex)
                {
                    Msj = ex.Message.ToString();
                }

            }
            else
            {
                Msj = "No se encontro lista de presupuestos.";
            }
            Msj = Msj + "Se actualizaron " + Actualizaciones + " registros.</br>";
            Msj = Msj + "Se dieron de alta " + Registros_Nuevos + " registros nuevos.</br>";
            Msj = Msj + "Se detectaron " + Registro_Invalido + " registros invalidos.";
            return Msj;
        }
    }
}