﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Presupuesto_Ingresos.Negocio;
 
namespace JAPAMI.Presupuesto_Ingresos.Datos
{
    public class Cls_Ope_Psp_Presupuesto_Ingresos_Datos
    {
        #region METODOS
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Anios
            ///DESCRIPCIÓN          : Obtiene datos de los años del estado analitico de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Anios()
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds_Anios = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt_Anios = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" ORDER BY " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " DESC");


                    Ds_Anios = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds_Anios != null)
                    {
                        Dt_Anios = Ds_Anios.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de los años. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Anios;
            }
            
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_FF
            ///DESCRIPCIÓN          : Obtiene datos de  las fuentes de financiamiento del estado analitico de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_FF(Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds_FF = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt_FF = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " || ' ' || ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion+ " AS CLAVE_NOMBRE ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                    Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim())) 
                    { 
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim()); 
                    }
                    
                    Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    Ds_FF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds_FF != null)
                    {
                        Dt_FF = Ds_FF.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de las fuentes de fianciamiento. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_FF;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Rubros
            ///DESCRIPCIÓN          : Obtiene datos de  las los rubros del presupuesto de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Rubros(Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro .Campo_Rubro_ID + ", ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " || ' ' || ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                    }

                    Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de los rubros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos
            ///DESCRIPCIÓN          : Obtiene datos de los tipos del presupuesto de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Tipos(Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + ", ");
                    Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " || ' ' || ");
                    Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim()  + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                    }

                    Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de los tipos Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Clases
            ///DESCRIPCIÓN          : Obtiene datos de  las clases del presupuesto de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Clases(Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ");
                    Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " || ' ' || ");
                    Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing );
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                    }

                    Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de las clases Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos
            ///DESCRIPCIÓN          : Obtiene datos de  los conceptos del presupuesto de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Conceptos(Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + ", ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " || ' ' || ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Clase_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                        }
                    }

                    Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de los conceptos Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_SubConceptos
            ///DESCRIPCIÓN          : Obtiene datos de  los subconceptos del presupuesto de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 19/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_SubConceptos(Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + ", ");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + " || ' ' || ");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Clase_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                        }
                    }

                    Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de los conceptos Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_FF_Ing
            ///DESCRIPCIÓN          : Obtiene datos de  las fuentes de financiamiento del estado analitico de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_FF_Ing(Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds_FF = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt_FF = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " || '_' || ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " AS ID, ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " || ' ' || ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ") AS ESTIMADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ") AS AMPLIACION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ") AS REDUCCION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ") AS MODIFICADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS RECAUDADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ") AS POR_RECAUDAR ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                    Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Clase_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                        }
                    }
                    Mi_SQL.Append(" GROUP BY ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                    Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    Ds_FF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds_FF != null)
                    {
                        Dt_FF = Ds_FF.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de las fuentes de fianciamiento. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_FF;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Rubros_Ing
            ///DESCRIPCIÓN          : Obtiene datos de  las los rubros del presupuesto de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Rubros_Ing(Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID+ " || '_' || ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID + " || '_' || ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " AS ID, ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " || ' ' || ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ") AS ESTIMADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ") AS AMPLIACION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ") AS REDUCCION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ") AS MODIFICADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS RECAUDADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ") AS POR_RECAUDAR ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Clase_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                        }
                    }

                    Mi_SQL.Append(" GROUP BY ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);

                    Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de los rubros. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos_Ing
            ///DESCRIPCIÓN          : Obtiene datos de los tipos del presupuesto de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Tipos_Ing(Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " || '_' || ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " || '_' || ");
                    Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + " || '_' || ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " AS ID, ");
                    Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " || ' ' || ");
                    Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ") AS ESTIMADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ") AS AMPLIACION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ") AS REDUCCION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ") AS MODIFICADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS RECAUDADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ") AS POR_RECAUDAR ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Clase_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                        }
                    }

                    Mi_SQL.Append(" GROUP BY ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                    Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                    Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);

                    Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de los tipos Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Clases_Ing
            ///DESCRIPCIÓN          : Obtiene datos de  las clases del presupuesto de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Clases_Ing(Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " || '_' || ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " || '_' || ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " || '_' || ");
                    Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " || '_' || ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " AS ID, ");
                    Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " || ' ' || ");
                    Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ") AS ESTIMADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ") AS AMPLIACION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ") AS REDUCCION, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ") AS MODIFICADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") AS RECAUDADO, ");
                    Mi_SQL.Append("SUM(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ") AS POR_RECAUDAR ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Clase_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                        }
                    }

                    Mi_SQL.Append(" GROUP BY ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                    Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                    Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);

                    Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de las clases Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos_Ing
            ///DESCRIPCIÓN          : Obtiene datos de  los conceptos del presupuesto de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Conceptos_Ing(Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " || '_' || ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + " || '_' || ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " || '_' || ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " || '_' || ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " || '_' || ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " AS ID, ");
                    Mi_SQL.Append("(CASE WHEN " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL THEN ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " || ' ' || ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                    Mi_SQL.Append(" ELSE ");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + " || ' ' || ");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                    Mi_SQL.Append(" END) AS CLAVE_NOMBRE, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ");
                    Mi_SQL.Append("(CASE WHEN " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL THEN ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                    Mi_SQL.Append(" ELSE ");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                    Mi_SQL.Append(" END) AS CONCEPTO_ING_ID, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + " AS ESTIMADO, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + " AS AMPLIACION, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + " AS REDUCCION, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + " AS MODIFICADO, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + " AS RECAUDADO, ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + " AS POR_RECAUDAR ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Clase_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Clase_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Concepto_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Concepto_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_SubConcepto_ID.Trim()))
                    {
                        if (!Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_SubConcepto_ID.Trim() + "'");
                        }
                    }

                    Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de los conceptos Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }
        #endregion
    }
}
