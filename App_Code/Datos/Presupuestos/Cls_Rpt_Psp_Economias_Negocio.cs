﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Rpt_Psp_Economias.Datos;

namespace JAPAMI.Rpt_Psp_Economias.Negocio
{
    public class Cls_Rpt_Psp_Economias_Negocio
    {
        #region(Variables Privadas)
            private String Anio;
            private String Dependencia_ID;
            private String Programa_ID;
            private String Area_Funcional_ID;
            private String Fte_Financiamiento_ID;
            private String Partida_ID;
            private String Estatus;
        #endregion

        #region(Variables Publicas)
            //get y set de P_Anio
            public String P_Anio
            {
                get { return Anio; }
                set { Anio = value; }
            }

            //get y set de P_Estatus
            public String P_Estatus
            {
                get { return Estatus; }
                set { Estatus = value; }
            }

            //get y set de P_Dependencia_ID
            public String P_Dependencia_ID
            {
                get { return Dependencia_ID; }
                set { Dependencia_ID = value; }
            }

            //get y set de P_Programa_ID
            public String P_Programa_ID
            {
                get { return Programa_ID; }
                set { Programa_ID = value; }
            }

            //get y set de P_Area_Funcional_ID
            public String P_Area_Funcional_ID
            {
                get { return Area_Funcional_ID; }
                set { Area_Funcional_ID = value; }
            }

            //get y set de P_Fte_Financiamiento_ID
            public String P_Fte_Financiamiento_ID
            {
                get { return Fte_Financiamiento_ID; }
                set { Fte_Financiamiento_ID = value; }
            }

            //get y set de P_Partida_ID
            public String P_Partida_ID
            {
                get { return Partida_ID; }
                set { Partida_ID = value; }
            }
        #endregion

        #region Metodos_Aprobado
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Anios
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los años
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Anios()
            {
                return Cls_Rpt_Psp_Economias_Datos.Consultar_Anios();
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_FF
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_FF()
            {
                return Cls_Rpt_Psp_Economias_Datos.Consultar_FF(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_UR
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las unidades responsables
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_UR()
            {
                return Cls_Rpt_Psp_Economias_Datos.Consultar_UR(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Programas
            ///DESCRIPCIÓN          : Metodo para obtener los datos de los programas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Programas()
            {
                return Cls_Rpt_Psp_Economias_Datos.Consultar_Programas(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_FF
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las partidas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Partidas()
            {
                return Cls_Rpt_Psp_Economias_Datos.Consultar_Partida(this);
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Economias
            ///DESCRIPCIÓN          : Metodo para obtener los datos de las economias
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Abril/2012 
            ///*********************************************************************************************************
            public DataTable Consultar_Economias()
            {
                return Cls_Rpt_Psp_Economias_Datos.Consultar_Economias(this);
            }

        #endregion
    }
}