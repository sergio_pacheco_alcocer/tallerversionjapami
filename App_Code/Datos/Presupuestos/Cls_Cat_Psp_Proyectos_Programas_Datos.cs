﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Proyectos_Programas.Negocio;
using JAPAMI.Sessiones;
using System.Text;

namespace JAPAMI.Proyectos_Programas.Datos
{
    public class Cls_Cat_Psp_Proyectos_Programas_Datos
    {
        public Cls_Cat_Psp_Proyectos_Programas_Datos()
        {
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Clasificador
        ///DESCRIPCIÓN: Consulta los clasificadores economicos
        ///PARAMETROS:  1.- Cls_Cat_Psp_Proyectos_Programas_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  14/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Proyectos(Cls_Cat_Psp_Proyectos_Programas_Negocio Clase_Negocio)
        {
            String Mi_SQL = string.Empty;
            try
            {
                //Creamos la consulta
                Mi_SQL = "SELECT PROYECTOS.*, AREA." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS AREA";
                Mi_SQL += " FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " PROYECTOS";
                Mi_SQL += ", " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " AREA";
                Mi_SQL += " WHERE PROYECTOS." + Cat_Sap_Proyectos_Programas.Campo_Area_Funcional_ID + " =";
                Mi_SQL += " AREA." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID;
                //Validamos los filtros por año
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Nombre))
                {
                    Mi_SQL += " AND " + Cat_Sap_Proyectos_Programas.Campo_Nombre + " LIKE '%" + Clase_Negocio.P_Nombre + "%'";
                }

                //regresamos los valores
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los calsificadores. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Agregar
        ///DESCRIPCIÓN          : Inserta el registro
        ///PARAMETROS           :  
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 14/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Agregar(Cls_Cat_Psp_Proyectos_Programas_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Consultamos el consecutivo
                String Mi_SQL = "SELECT ISNULL(MAX(" + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ") + 1, 1) AS CONSECUTIVO FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas;
                //Asignamos el valor
                Clase_Negocio.P_ID = int.Parse(SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0].Rows[0]["CONSECUTIVO"].ToString()).ToString("0000000000");

                //Insertar un registro de las fechas
                Mi_SQL = "INSERT INTO " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas +
                    " (" + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                    ", " + Cat_Sap_Proyectos_Programas.Campo_Clave +
                    ", " + Cat_Sap_Proyectos_Programas.Campo_Nombre +
                    ", " + Cat_Sap_Proyectos_Programas.Campo_Estatus +
                    ", " + Cat_Sap_Proyectos_Programas.Campo_Area_Funcional_ID;
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Descripcion))
                    Mi_SQL += ", " + Cat_Sap_Proyectos_Programas.Campo_Descripcion;

                Mi_SQL += ", " + Cat_Sap_Proyectos_Programas.Campo_Usuario_Creo +
                    ", " + Cat_Sap_Proyectos_Programas.Campo_Fecha_Creo +
                    ") VALUES ('" +
                     Clase_Negocio.P_ID + "','" +
                     Clase_Negocio.P_Clave + "','" +
                     Clase_Negocio.P_Nombre + "','" +
                     Clase_Negocio.P_Estatus + "','" +
                     Clase_Negocio.P_Area_ID + "','";
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Descripcion))
                   Mi_SQL += Clase_Negocio.P_Descripcion + "','";
                   Mi_SQL += Cls_Sessiones.Nombre_Empleado + "'," +
                     "GETDATE() )";

                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Trans.Commit();
                Mensaje_Error = Clase_Negocio.P_Clave;

            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo agregar";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Actualizar
        ///DESCRIPCIÓN          : Actualizamos el registro
        ///PARAMETROS           :  
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 14/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Actualizar(Cls_Cat_Psp_Proyectos_Programas_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Actualizar las fechas
                String Mi_SQL = "UPDATE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas;
                Mi_SQL = Mi_SQL + " SET " + Cat_Sap_Proyectos_Programas.Campo_Clave + " = '" + Clase_Negocio.P_Clave + "'";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Proyectos_Programas.Campo_Nombre + " = '" + Clase_Negocio.P_Nombre + "'";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Proyectos_Programas.Campo_Estatus + " = '" + Clase_Negocio.P_Estatus + "'";
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Area_ID))
                    Mi_SQL = Mi_SQL + ", " + Cat_Sap_Proyectos_Programas.Campo_Area_Funcional_ID + " = '" + Clase_Negocio.P_Area_ID + "'";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " = '" + Clase_Negocio.P_Descripcion + "'";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Proyectos_Programas.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "'";
                Mi_SQL = Mi_SQL + ", " + Cat_Sap_Proyectos_Programas.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = '" + Clase_Negocio.P_ID+ "'";
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Trans.Commit();
                Mensaje_Error = "Rechazado";
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo actualizar";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar
        ///DESCRIPCIÓN          : eliminamos el registro
        ///PARAMETROS           :  
        ///CREO                 : David Herrera Rincon
        ///FECHA_CREO           : 14/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static String Eliminar(Cls_Cat_Psp_Proyectos_Programas_Negocio Clase_Negocio)
        {
            String Mensaje_Error = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            try
            {
                //Actualizar las fechas
                String Mi_SQL = "DELETE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = " + Clase_Negocio.P_ID;
                Cmd.CommandText = Mi_SQL;
                Cmd.ExecuteNonQuery();

                Trans.Commit();
                Mensaje_Error = "Se elimino el registro.";
            }
            catch (Exception ex)
            {
                ex.ToString();
                Trans.Rollback();
                Mensaje_Error = "No se pudo eliminar el registro, puede que este relacionado";
                throw new Exception(ex.Message);
            }
            finally
            {
                Cn.Close();
            }
            return Mensaje_Error;
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Consultar_Areas
        ///DESCRIPCIÓN: Consulta las areas funcionales
        ///PARAMETROS:  1.- Cls_Cat_Psp_Proyectos_Programas_Negocio
        ///CREO:        David Herrera Rincon
        ///FECHA_CREO:  14/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consultar_Areas(Cls_Cat_Psp_Proyectos_Programas_Negocio Clase_Negocio)
        {            
            try
            {
                StringBuilder Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID + ",");
                Mi_SQL.Append(Cat_SAP_Area_Funcional.Campo_Clave + " + ' ' + " + Cat_SAP_Area_Funcional.Campo_Descripcion);
                Mi_SQL.Append(" AS " + Cat_SAP_Area_Funcional.Campo_Descripcion);
                Mi_SQL.Append(" FROM " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                if (!String.IsNullOrEmpty(Clase_Negocio.P_Area_ID))
                    Mi_SQL.Append(" WHERE " + Cat_SAP_Area_Funcional.Campo_Clave + " LIKE '%" + Clase_Negocio.P_Area_ID.Trim() + "%'");
                
                DataTable Dt_Clasificador_Economico = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
                return Dt_Clasificador_Economico;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las  ultimas fechas. Error: [" + Ex.Message + "]");
            }
        }        
    }
}