﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Movimiento_Presupuestal.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Ope_Psp_Movimientos_Ingresos.Datos;
using System.Net.Mail;

namespace JAPAMI.Movimiento_Presupuestal.Datos
{
    public class Cls_Ope_Psp_Movimiento_Presupuestal_Datos
    {
        #region(Metodos)
        /// ********************************************************************************************************************
        /// NOMBRE: Alta_Movimiento
        /// 
        /// COMENTARIOS: Esta operación inserta un nuevo registro de un movimiento presupuestal en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a insertar en la tabla de OPE_COM_SOLICITUD_TRANSF 
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ: 18/Octubre/2011 
        /// USUARIO MODIFICO: Leslie Gonzalez Vazquez
        /// FECHA MODIFICO: 22/marzo/2012
        /// CAUSA DE LA MODIFICACIÓN:se acomodo la insercion de los movimientos
        /// ********************************************************************************************************************
        public static DataTable Alta_Movimiento(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
        {

            StringBuilder MI_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion;//Variable para la conexión para la base de datos   
            SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            DataTable Dt_Mov = new DataTable();
            Int32 No_Movimiento_Egr;
            Int32 No_Solicitud;
            String Estatus_Movimiento = String.Empty;
            Boolean Igual_Mov_Egr = false;
            DataTable Dt_Datos = new DataTable();
            DataRow Fila;
            DataTable Dt_Anexos = new DataTable();
            Int32 Anexo_ID;
            String Psp_Actualizado = String.Empty;
            Boolean Psp_Actualizados = false;
            String Solicitande = String.Empty;
            String Director = String.Empty;
            String Puesto_Solicitande = String.Empty;
            String Puesto_Director = String.Empty;
            String[] Datos_Poliza_PSP = null;
            String No_Poliza_PSP = String.Empty;
            String Tipo_Poliza_PSP = String.Empty;
            String Mes_Anio_PSP = String.Empty;
            Double Ampliacion = 0.00;
            Double Reduccion = 0.00;
            Double Traspaso = 0.00;
            String Importes = String.Empty;
            DataTable Dt_Origen = new DataTable();

            Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Comando = new SqlCommand();
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Transaction = Transaccion;
            Comando.Connection = Conexion;

            String Anio = String.Format("{0:yyyy}", DateTime.Now);

            try
            {
                //obtenemos si el presupuesto es actualizado o no
                Psp_Actualizados = Cls_Ope_Psp_Manejo_Presupuesto.Presupuesto_Actualizado(Comando);
                if (Psp_Actualizados)
                {
                    Psp_Actualizado = "SI";
                }
                else 
                {
                    Psp_Actualizado = "NO";
                }

                //obtenemos el id maximo de la tabla de movimientos de egresos
                MI_SQL.Append("SELECT ISNULL(MAX(" + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + "), 0)");
                MI_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio);

                No_Movimiento_Egr = Convert.ToInt32(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, MI_SQL.ToString()));

                //SI EL MOVIMIENTO ES CERO LE ASIGNAMOS EL NUMERO UNO Y COMO NO ES REPETIDO NUESTRA VARIABLE ES FALSE
                if (No_Movimiento_Egr == 0)
                {
                    No_Movimiento_Egr = 1;
                    Igual_Mov_Egr = false;
                }
                else
                {
                    //CONSULTAMOS EL ESTATUS DE NUESTRO MOVIMIENTO PARA VALIDAR SI CREAMOS UN NUEVO MOVIMIENTO
                    //ESTO SI EL ESTATUS ES GENERADO AUN PODEMOS SEGUIR INSERTANDO CON ESE MOVIMIENTO Y SI EL ESTATUS ES
                    //AUTORIZADO O PREAUTORIZADO SE INSERTA UNA NUEVA MODIFICACION
                    MI_SQL = new StringBuilder();
                    MI_SQL.Append("SELECT " + Ope_Psp_Movimiento_Egr.Campo_Estatus);
                    MI_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                    MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio);
                    MI_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + No_Movimiento_Egr.ToString().Trim());

                    if (Convert.IsDBNull(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, MI_SQL.ToString())))
                    {
                        MI_SQL = new StringBuilder();
                        MI_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                        MI_SQL.Append(" SET " + Ope_Psp_Movimiento_Egr.Campo_Estatus + " = 'GENERADO'");
                        MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio);
                        MI_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + No_Movimiento_Egr);

                        Comando.CommandText = MI_SQL.ToString();
                        Comando.ExecuteNonQuery();
                        Igual_Mov_Egr = true;
                        Estatus_Movimiento = "GENERADO";
                    }
                    else
                    {
                        Estatus_Movimiento = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, MI_SQL.ToString());
                        if (Estatus_Movimiento.Trim().Equals("GENERADO"))
                        {
                            Igual_Mov_Egr = true;
                        }
                        else
                        {
                            No_Movimiento_Egr += 1;
                            Igual_Mov_Egr = false;
                        }
                    }
                }

                if (!Igual_Mov_Egr)
                {
                    //insertamos los datos de la nueva modificacion
                    MI_SQL = new StringBuilder();
                    MI_SQL.Append("INSERT INTO " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "(");
                    MI_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + ", ");
                    MI_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Anio + ", ");
                    MI_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Estatus + ", ");
                    MI_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ", ");
                    MI_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Usuario_Creo + ", ");
                    MI_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Fecha_Creo + ") ");
                    MI_SQL.Append(" VALUES(" + No_Movimiento_Egr + ", ");
                    MI_SQL.Append(Anio + ", 'GENERADO', ");
                    MI_SQL.Append(Datos.P_Total_Modificado.Replace(",", "").Replace("$", "") + ", ");
                    MI_SQL.Append("'" + Datos.P_Usuario_Creo + "', GETDATE())");

                    Comando.CommandText = MI_SQL.ToString();
                    Comando.ExecuteNonQuery();
                }
                else
                {
                    //QUERY PARA ACTUALIZAR EL TOTAL DEL MOVIMIENTO 
                    MI_SQL = new StringBuilder();
                    MI_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                    MI_SQL.Append(" SET " + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + " = ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0)");
                    MI_SQL.Append(" + " + Datos.P_Total_Modificado.Trim().Replace(",", ""));
                    MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio);
                    MI_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + No_Movimiento_Egr);

                    Comando.CommandText = MI_SQL.ToString();
                    Comando.ExecuteNonQuery();
                }

                //OBTENEMOS LOS DATOS DEL SOLICITANTE Y DEL DIRECTOR DEL AREA
                if (Datos.P_Dt_Solicitante != null)
                {
                    if (Datos.P_Dt_Solicitante.Rows.Count > 0)
                    {
                        Solicitande = Datos.P_Dt_Solicitante.Rows[0]["Solicitante"].ToString().Trim();
                        Director = Datos.P_Dt_Solicitante.Rows[0]["Dr"].ToString().Trim();
                        Puesto_Solicitande = Datos.P_Dt_Solicitante.Rows[0]["Puesto_Solicitante"].ToString().Trim();
                        Puesto_Director = Datos.P_Dt_Solicitante.Rows[0]["Puesto_Dr"].ToString().Trim();
                    }
                }
                
                //insertamos los movimientos
                Dt_Mov = Datos.P_Dt_Mov;

                if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
                {
                    //OBTENEMOS EL ID MAXIMO DE LA SOLICITUD
                    No_Solicitud = Convert.ToInt32(Datos.P_No_Solicitud.Trim());

                    //validamos si es movimiento de ramo 33 generamos la poliza presupuestal
                    //para posteriormente guardar los datos de la poliza
                    if (Datos.P_Tipo_Usuario.Trim().Equals("Ramo33"))
                    {
                        //obtenemos el monto de la ampliacion
                        Dt_Origen = (from x in Dt_Mov.AsEnumerable()
                                     where x.Field<String>("TIPO_PARTIDA") == "Origen"
                                     && x.Field<String>("TIPO_OPERACION") == "AMPLIACION"
                                     select x).AsDataView().ToTable();

                        Ampliacion = Dt_Origen.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<String>("IMPORTE_TOTAL").ToString()) ? "0" : x.Field<String>("IMPORTE_TOTAL").ToString()));

                        //obtenemos el monto de la reduccion
                        Dt_Origen = new DataTable();
                        Dt_Origen = (from x in Dt_Mov.AsEnumerable()
                                     where x.Field<String>("TIPO_PARTIDA") == "Origen"
                                     && x.Field<String>("TIPO_OPERACION") == "REDUCCION"
                                     select x).AsDataView().ToTable();

                        Reduccion = Dt_Origen.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<String>("IMPORTE_TOTAL").ToString()) ? "0" : x.Field<String>("IMPORTE_TOTAL").ToString()));

                        //obtenemos el monto del traspaso
                        Dt_Origen = new DataTable();
                        Dt_Origen = (from x in Dt_Mov.AsEnumerable()
                                     where x.Field<String>("TIPO_PARTIDA") == "Origen"
                                     && x.Field<String>("TIPO_OPERACION") == "TRASPASO"
                                     select x).AsDataView().ToTable();

                        Traspaso = Dt_Origen.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<String>("IMPORTE_TOTAL").ToString()) ? "0" : x.Field<String>("IMPORTE_TOTAL").ToString()));

                        //Juntamos los importes de la ampliacion, reduccion y reduccion interna;
                        Importes = Ampliacion.ToString().Trim() + ";" + Reduccion.ToString().Trim() + ";" + Traspaso.ToString().Trim();

                        //creamos la poliza presupuestal
                        Datos_Poliza_PSP = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimientos_Presupuestales(Importes, Ope_Psp_Presupuesto_Aprobado.Campo_Modificado,
                           Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, 0.00, "", "", "", "", Comando);

                        //obtenemos los id de la poliza presupuestal creada
                        if (Datos_Poliza_PSP != null)
                        {
                            if (Datos_Poliza_PSP.Length > 0)
                            {
                                No_Poliza_PSP = Datos_Poliza_PSP[0].Trim();
                                Tipo_Poliza_PSP = Datos_Poliza_PSP[1].Trim();
                                Mes_Anio_PSP = Datos_Poliza_PSP[2].Trim();
                            }
                        }

                    }

                    //INSERTAMOS LOS DATOS DE LOS DETALLES
                    foreach (DataRow Dr in Dt_Mov.Rows)
                    {
                        MI_SQL = new StringBuilder();
                        MI_SQL.Append("INSERT INTO " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "(");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Movimiento_ID + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Area_Funcional_ID + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Anio + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Enero + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Febrero + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Marzo + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Abril + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Mayo + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Junio + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Julio + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Agosto + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Septiembre + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Octubre + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Noviembre + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Diciembre + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Aprobado + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Ampliacion + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Reduccion + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Modificado + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Egreso + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Movimiento + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Usuario + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Solicitante + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Director + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Solicitante + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Director + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Justificacion + ", ");

                        if (Datos.P_Tipo_Usuario.Trim().Equals("Ramo33"))
                        {
                            if (!String.IsNullOrEmpty(No_Poliza_PSP))
                            {
                                MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_No_Poliza_Presupuestal + ", ");
                            }

                            if (!String.IsNullOrEmpty(Tipo_Poliza_PSP))
                            {
                                MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Poliza_ID_Presupuestal + ", ");
                            }

                            if (!String.IsNullOrEmpty(Mes_Anio_PSP))
                            {
                                MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Mes_Anio_Presupuestal + ", ");
                            }
                        }

                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Creo + ", ");
                        MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo + ") ");
                        MI_SQL.Append(" VALUES(" + No_Solicitud + ", ");
                        MI_SQL.Append(Dr["MOVIMIENTO_ID"].ToString().Trim() + ", ");
                        MI_SQL.Append("'" + Dr["FF_ID"].ToString().Trim() + "', ");
                        MI_SQL.Append("'" + Dr["UR_ID"].ToString().Trim() + "', ");
                        MI_SQL.Append("'" + Dr["PROGRAMA_ID"].ToString().Trim() + "', ");
                        MI_SQL.Append("'" + Dr["PARTIDA_ID"].ToString().Trim() + "', ");
                        MI_SQL.Append("'" + Dr["AF_ID"].ToString().Trim() + "', ");
                        MI_SQL.Append(No_Movimiento_Egr + ", ");
                        MI_SQL.Append(Anio + ", ");
                        MI_SQL.Append("'" + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["APROBADO"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["AMPLIACION"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["REDUCCION"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["MODIFICADO"].ToString().Trim().Replace(",", "") + "', ");
                        MI_SQL.Append("'" + Dr["ESTATUS"].ToString().Trim() + "', ");
                        MI_SQL.Append("'" + Dr["TIPO_OPERACION"].ToString().Trim() + "', ");
                        MI_SQL.Append("'" + Dr["TIPO_PARTIDA"].ToString().Trim() + "', ");
                        MI_SQL.Append("'" + Dr["TIPO_EGRESO"].ToString().Trim() + "', ");
                        MI_SQL.Append("'" + Dr["TIPO_MOVIMIENTO"].ToString().Trim() + "', ");
                        MI_SQL.Append("'" + Datos.P_Tipo_Usuario.Trim().ToUpper() + "', ");
                        
                        if (!String.IsNullOrEmpty(Solicitande.Trim()))
                        {
                            MI_SQL.Append("'" + Solicitande.Trim() + "', ");
                        }
                        else
                        {
                            MI_SQL.Append("NULL, ");
                        }

                        if (!String.IsNullOrEmpty(Director.Trim()))
                        {
                            MI_SQL.Append("'" + Director.Trim() + "', ");
                        }
                        else
                        {
                            MI_SQL.Append("NULL, ");
                        }

                        if (!String.IsNullOrEmpty(Puesto_Solicitande.Trim()))
                        {
                            MI_SQL.Append("'" + Puesto_Solicitande.Trim() + "', ");
                        }
                        else
                        {
                            MI_SQL.Append("NULL, ");
                        }

                        if (!String.IsNullOrEmpty(Puesto_Director.Trim()))
                        {
                            MI_SQL.Append("'" + Puesto_Director.Trim() + "', ");
                        }
                        else
                        {
                            MI_SQL.Append("NULL, ");
                        }

                        MI_SQL.Append("'" + Dr["JUSTIFICACION"].ToString().Trim().ToUpper() + "', ");

                        if (Datos.P_Tipo_Usuario.Trim().Equals("Ramo33"))
                        {
                            if (!String.IsNullOrEmpty(No_Poliza_PSP))
                            {
                                MI_SQL.Append("'" + No_Poliza_PSP.Trim() + "', ");
                            }

                            if (!String.IsNullOrEmpty(Tipo_Poliza_PSP))
                            {
                                MI_SQL.Append("'" + Tipo_Poliza_PSP.Trim() + "', ");
                            }

                            if (!String.IsNullOrEmpty(Mes_Anio_PSP))
                            {
                                MI_SQL.Append("'" + Mes_Anio_PSP.Trim() + "', ");
                            }
                        }

                        MI_SQL.Append("'" + Datos.P_Usuario_Creo.Trim() + "', ");
                        MI_SQL.Append("GETDATE()) ");

                        Comando.CommandText = MI_SQL.ToString();
                        Comando.ExecuteNonQuery();

                        if (Dr["TIPO_MOVIMIENTO"].ToString().Trim().Equals("Nueva"))
                        {
                            if (Datos.P_Tipo_Usuario.Trim().Equals("Ramo33"))
                            {
                                MI_SQL = new StringBuilder();
                                MI_SQL.Append("INSERT INTO " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                MI_SQL.Append("(" + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Anio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Saldo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ", ");

                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Enero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Febrero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Marzo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Abril + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Mayo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Junio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Julio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Agosto + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Septiembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Octubre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Noviembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Diciembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Enero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Febrero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Marzo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Abril + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Mayo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Junio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Julio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Agosto + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Septiembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Octubre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Noviembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Diciembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Enero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Febrero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Marzo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Abril + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Mayo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Junio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Julio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Agosto + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Septiembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Octubre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Noviembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Diciembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Enero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Febrero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Marzo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Abril + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Mayo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Junio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Julio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Agosto + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Septiembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Octubre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Noviembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Diciembre + ", ");

                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Creo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Creo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado + ") VALUES( ");
                                MI_SQL.Append("'" + Dr["UR_ID"].ToString().Trim() + "', ");
                                MI_SQL.Append("'" + Dr["FF_ID"].ToString().Trim() + "', ");
                                MI_SQL.Append("'" + Dr["PROGRAMA_ID"].ToString().Trim() + "', ");
                                MI_SQL.Append("'" + Dr["CAPITULO_ID"].ToString().Trim() + "', ");
                                MI_SQL.Append("'" + Dr["PARTIDA_ID"].ToString().Trim() + "', ");
                                MI_SQL.Append("'" + Dr["AF_ID"].ToString().Trim() + "', ");
                                MI_SQL.Append(String.Format("{0:yyyy}", DateTime.Now) + ", ");
                                MI_SQL.Append("0, ");

                                if (Dr["TIPO_OPERACION"].ToString().Trim().Equals("AMPLIACION"))
                                {
                                    MI_SQL.Append("'" + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + "', ");
                                    MI_SQL.Append("0, ");
                                    MI_SQL.Append("'" + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + "', ");
                                }
                                else if (Dr["TIPO_OPERACION"].ToString().Trim().Equals("TRASPASO") && Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                                {
                                    MI_SQL.Append("0, ");
                                    MI_SQL.Append("'" + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + "', ");
                                    MI_SQL.Append("'" + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + "', ");
                                }
                                else if (Dr["TIPO_OPERACION"].ToString().Trim().Equals("TRASPASO") && Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Destino"))
                                {
                                    MI_SQL.Append("'" + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + "', ");
                                    MI_SQL.Append("0, ");
                                    MI_SQL.Append("'" + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + "', ");
                                }

                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("'" + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("'" + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                                MI_SQL.Append("'" + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                                
                                MI_SQL.Append("0.00, ");/*ene*/
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");/*dic*/
                                MI_SQL.Append("0.00, ");/*ene*/
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");/*dic*/
                                MI_SQL.Append("0.00, ");/*ene*/
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");/*dic*/
                                MI_SQL.Append("0.00, ");/*ene*/
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");/*dic*/
                                MI_SQL.Append("0.00, ");/*ene*/
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");/*dic*/

                                MI_SQL.Append("'" + Cls_Sessiones.Nombre_Empleado + "', ");
                                MI_SQL.Append("GETDATE(), ");
                                MI_SQL.Append("'" + Psp_Actualizado.Trim() + "') ");

                                //Ejecutar consulta
                                Comando.CommandText = MI_SQL.ToString().Trim();
                                Comando.ExecuteNonQuery();
                            }
                            else
                            {
                                MI_SQL = new StringBuilder();
                                MI_SQL.Append("INSERT INTO " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                MI_SQL.Append("(" + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Anio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Saldo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ", ");

                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Enero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Febrero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Marzo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Abril + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Mayo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Junio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Julio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Agosto + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Septiembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Octubre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Noviembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Diciembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Enero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Febrero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Marzo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Abril + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Mayo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Junio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Julio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Agosto + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Septiembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Octubre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Noviembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Diciembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Enero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Febrero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Marzo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Abril + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Mayo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Junio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Julio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Agosto + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Septiembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Octubre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Noviembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Diciembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Enero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Febrero + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Marzo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Abril + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Mayo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Junio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Julio + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Agosto + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Septiembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Octubre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Noviembre + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Diciembre + ", ");

                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Creo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Creo + ", ");
                                MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado + ") VALUES( ");
                                MI_SQL.Append("'" + Dr["UR_ID"].ToString().Trim() + "', ");
                                MI_SQL.Append("'" + Dr["FF_ID"].ToString().Trim() + "', ");
                                MI_SQL.Append("'" + Dr["PROGRAMA_ID"].ToString().Trim() + "', ");
                                MI_SQL.Append("'" + Dr["CAPITULO_ID"].ToString().Trim() + "', ");
                                MI_SQL.Append("'" + Dr["PARTIDA_ID"].ToString().Trim() + "', ");
                                MI_SQL.Append("'" + Dr["AF_ID"].ToString().Trim() + "', ");
                                MI_SQL.Append(String.Format("{0:yyyy}", DateTime.Now) + ", ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");
                                MI_SQL.Append("0, ");

                                MI_SQL.Append("0.00, ");/*ene*/
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");/*dic*/
                                MI_SQL.Append("0.00, ");/*ene*/
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");/*dic*/
                                MI_SQL.Append("0.00, ");/*ene*/
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");/*dic*/
                                MI_SQL.Append("0.00, ");/*ene*/
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");/*dic*/
                                MI_SQL.Append("0.00, ");/*ene*/
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");/*dic*/
                                MI_SQL.Append("0.00, ");/*ene*/
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");
                                MI_SQL.Append("0.00, ");/*dic*/

                                MI_SQL.Append("'" + Cls_Sessiones.Nombre_Empleado + "', ");
                                MI_SQL.Append("GETDATE(), ");
                                MI_SQL.Append("'" + Psp_Actualizado + "') ");

                                //Ejecutar consulta
                                Comando.CommandText = MI_SQL.ToString().Trim();
                                Comando.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            if (Datos.P_Tipo_Usuario.Trim().Equals("Ramo33"))
                            {
                                if (Dr["TIPO_OPERACION"].ToString().Trim().Equals("REDUCCION"))
                                {
                                    MI_SQL = new StringBuilder();
                                    MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                    MI_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible   + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) - " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$","") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ",0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ",0) - " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ",0) - " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ",0) - " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ",0) - " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ",0) - " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ",0) - " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ",0) - " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ",0) - " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ",0) - " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ",0) - " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ",0) - " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ",0) - " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " =  ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ",0) - " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) - " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) - " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) - " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) - " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) - " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) - " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) - " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) - " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) - " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) - " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) - " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) - " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                    MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["UR_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FF_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                    Comando.CommandText = MI_SQL.ToString();
                                    Comando.ExecuteNonQuery();

                                    MI_SQL = new StringBuilder();
                                    MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                    MI_SQL.Append(" SET  MODIFICADO = APROBADO + AMPLIACION - " + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " ");
                                    MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["UR_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FF_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                    Comando.CommandText = MI_SQL.ToString();
                                    Comando.ExecuteNonQuery();

                                }
                                else if (Dr["TIPO_OPERACION"].ToString().Trim().Equals("AMPLIACION"))
                                {
                                    MI_SQL = new StringBuilder();
                                    MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                    MI_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(" AMPLIACION = ISNULL(AMPLIACION,0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ",0) + " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ",0) + " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ",0) + " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ",0) + " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ",0) + " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ",0) + " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ",0) + " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ",0) + " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ",0) + " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ",0) + " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ",0) + " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ",0) + " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ",0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) + " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) + " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) + " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) + " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) + " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) + " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) + " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) + " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) + " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) + " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) + " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) + " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                                    MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                    MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["UR_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FF_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                    Comando.CommandText = MI_SQL.ToString();
                                    Comando.ExecuteNonQuery();

                                    MI_SQL = new StringBuilder();
                                    MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                    MI_SQL.Append(" SET  MODIFICADO = ISNULL(APROBADO,0) + ISNULL(AMPLIACION,0) - ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ",0) ");
                                    MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["UR_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FF_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                    Comando.CommandText = MI_SQL.ToString();
                                    Comando.ExecuteNonQuery();
                                }
                                else if (Dr["TIPO_OPERACION"].ToString().Trim().Equals("TRASPASO"))
                                {
                                    if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                                    {
                                        MI_SQL = new StringBuilder();
                                        MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                        MI_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) - " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(" " + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ",0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ",0) - " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ",0) - " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ",0) - " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ",0) - " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ",0) - " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ",0) - " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ",0) - " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ",0) - " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ",0) - " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ",0) - " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ",0) - " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ",0) - " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ",0) - " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) - " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) - " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) - " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) - " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) - " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) - " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) - " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) - " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) - " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) - " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) - " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) - " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                        MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["UR_ID"].ToString().Trim() + "'");
                                        MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FF_ID"].ToString().Trim() + "'");
                                        MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                                        MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                        MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                        Comando.CommandText = MI_SQL.ToString();
                                        Comando.ExecuteNonQuery();
                                    }
                                    else
                                    {
                                        MI_SQL = new StringBuilder();
                                        MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                        MI_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(" AMPLIACION = ISNULL(AMPLIACION,0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ",0) + " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ",0) + " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ",0) + " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ",0) + " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ",0) + " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ",0) + " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ",0) + " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ",0) + " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ",0) + " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ",0) + " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ",0) + " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ",0) + " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ",0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) + " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) + " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) + " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) + " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) + " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) + " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) + " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) + " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) + " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) + " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) + " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) + " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                        MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["UR_ID"].ToString().Trim() + "'");
                                        MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FF_ID"].ToString().Trim() + "'");
                                        MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                                        MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                        MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                        Comando.CommandText = MI_SQL.ToString();
                                        Comando.ExecuteNonQuery();
                                    }
                                    MI_SQL = new StringBuilder();
                                    MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                    MI_SQL.Append(" SET  MODIFICADO = ISNULL(APROBADO,0) + ISNULL(AMPLIACION,0) - ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ",0) ");
                                    MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["UR_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FF_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                    MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                    Comando.CommandText = MI_SQL.ToString();
                                    Comando.ExecuteNonQuery();
                                }
                            }
                            else
                            {
                                if (Dr["TIPO_OPERACION"].ToString().Trim().Equals("REDUCCION") || Dr["TIPO_OPERACION"].ToString().Trim().Equals("TRASPASO"))
                                {
                                    if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                                    {
                                        //PRE_COMPROMETEMOS EL RECURSO
                                        MI_SQL = new StringBuilder();
                                        MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                        MI_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = ");
                                        MI_SQL.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", 0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ");
                                        MI_SQL.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) - " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) - " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) - " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) - " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) - " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) - " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) - " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) - " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) - " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) - " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) - " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) - " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) - " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ",0) + " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ",0) + " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ",0) + " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ",0) + " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ",0) + " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ",0) + " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ",0) + " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ",0) + " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ",0) + " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ",0) + " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ",0) + " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                        MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ",0) + " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", ""));
                                        MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FF_ID"].ToString().Trim() + "'");
                                        MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["UR_ID"].ToString().Trim() + "'");
                                        MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                                        MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                        MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);
                                        Comando.CommandText = MI_SQL.ToString();
                                        Comando.ExecuteNonQuery();
                                    }
                                }
                            }
                        }
                    }

                    //insertamos los anexos del movimiento
                    Dt_Anexos = Datos.P_Dt_Anexos;
                    if (Dt_Anexos != null && Dt_Anexos.Rows.Count > 0)
                    {
                        //OBTENEMOS EL NUMERO DE ANEXO
                        MI_SQL = new StringBuilder();
                        MI_SQL.Append("SELECT ISNULL(MAX(" + Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Anexo_ID + "), 0)");
                        MI_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det_Doc.Tabla_Ope_Psp_Movimiento_Egr_Det_Doc);

                        Comando.CommandText = MI_SQL.ToString().Trim();

                        Anexo_ID = Convert.ToInt32(Comando.ExecuteScalar());
                        Anexo_ID = Anexo_ID + 1;

                        //INSERTAMOS LOS ANEXOS
                        foreach (DataRow Dr_Doc in Dt_Anexos.Rows)
                        {
                            MI_SQL = new StringBuilder();
                            MI_SQL.Append("INSERT INTO " + Ope_Psp_Movimiento_Egr_Det_Doc.Tabla_Ope_Psp_Movimiento_Egr_Det_Doc + "(");
                            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Anexo_ID + ", ");
                            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Nombre + ", ");
                            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Ruta_Documento + ", ");
                            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Extension + ", ");
                            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Solicitud_ID + ", ");
                            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Movimiento_ID + ", ");
                            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Usuario_Creo + ", ");
                            MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Usuario_Modifico + ") VALUES( ");
                            MI_SQL.Append(Anexo_ID.ToString().Trim() + ", ");
                            MI_SQL.Append("'" + Dr_Doc["NOMBRE"].ToString().Trim() + "', ");
                            MI_SQL.Append("'" + Dr_Doc["RUTA_DOCUMENTO"].ToString().Trim() + "', ");
                            MI_SQL.Append("'" + Dr_Doc["EXTENSION"].ToString().Trim() + "', ");
                            MI_SQL.Append(No_Solicitud.ToString().Trim() + ", ");
                            MI_SQL.Append("1, ");
                            MI_SQL.Append("'" + Datos.P_Usuario_Creo + "', ");
                            MI_SQL.Append("GETDATE())");

                            Comando.CommandText = MI_SQL.ToString();
                            Comando.ExecuteNonQuery();
                            Anexo_ID++;
                        }
                    }

                    Dt_Datos.Columns.Add("No_Movimiento");
                    Dt_Datos.Columns.Add("No_Solicitud");

                    Fila = Dt_Datos.NewRow();
                    Fila["No_Movimiento"] = No_Movimiento_Egr;
                    Fila["No_Solicitud"] = No_Solicitud.ToString();
                   
                    Dt_Datos.Rows.Add(Fila);
                }

                Transaccion.Commit();
            }
            catch (SqlException Ex)
            {
                Transaccion.Rollback();
               
                Mensaje = "Error:  [" + Ex.Message + "]";

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Conexion.Close();
                Comando = null;
                Conexion = null;
                Transaccion = null;
            }
            return Dt_Datos;
        }

        /// ********************************************************************************************************************
        /// NOMBRE: Alta_Comentario
        /// 
        /// COMENTARIOS: Esta operación inserta un nuevo registro de un comentario presupuestal en la tabla de Ope_Psp_Cierre_Presup
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a insertar en la tabla de 
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ: 17/Noviembre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static Boolean Alta_Comentario(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
        {

            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion;//Variable para la conexión para la base de datos   
            SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;
            StringBuilder MI_SQL = new StringBuilder();
            string Tipo_Presupuesto = string.Empty;
            Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Comando = new SqlCommand();
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Transaction = Transaccion;
            Comando.Connection = Conexion;

            try
            {

                Mi_SQL.Append("UPDATE " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + " SET ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + "='" + Datos.P_Estatus + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Fecha_Modifico + "=GETDATE() ");
                Mi_SQL.Append("WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + " = " + Datos.P_No_Solicitud);

                Comando.CommandText = Mi_SQL.ToString();
                Comando.ExecuteNonQuery();

                if (Datos.P_Estatus.Trim().Equals("RECHAZADA"))
                {
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Ope_Psp_Comentarios_Mov.Tabla_Ope_Psp_Comentarios_Mov + " (");
                    Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Numero_Solicitud + ", ");
                    Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Comentario + ", ");
                    Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Fecha + ", ");
                    Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Fecha_Creo + ") ");
                    Mi_SQL.Append("VALUES (" + Datos.P_No_Solicitud + ", ");
                    Mi_SQL.Append("'" + Datos.P_Comentario + "', ");
                    Mi_SQL.Append("GETDATE() , ");
                    Mi_SQL.Append("'" + Datos.P_Usuario_Creo + "', ");
                    Mi_SQL.Append("GETDATE() )");

                    Comando.CommandText = Mi_SQL.ToString();
                    Comando.ExecuteNonQuery();
               }

                Transaccion.Commit();
                Conexion.Close();
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                Transaccion.Rollback();
                Mensaje = "Error:  [" + Ex.Message + "]";

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Conexion.Close();
                Comando = null;
                Conexion = null;
                Transaccion = null;
            }
            return Operacion_Completa;
        }

        /// ********************************************************************************************************************
        /// NOMBRE: Modificar_Movimiento
        /// 
        /// COMENTARIOS: Esta operación actualiza un registro del movimiento en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a Modificar en la tabla de OPE_COM_SOLICITUD_TRANSF 
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  18/Octubre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static Boolean Modificar_Movimiento(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion;//Variable para la conexión para la base de datos   
            SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;

            Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Comando = new SqlCommand();
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Transaction = Transaccion;
            Comando.Connection = Conexion;
            try
            {
                //Mi_SQL.Append("UPDATE " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + " SET ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Codigo1 + "='" + Datos.P_Codigo_Programatico_De + "',");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Codigo2 + "='" + Datos.P_Codigo_Programatico_Al + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Importe + "=" + Datos.P_Monto + ", ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Justificacion + "='" + Datos.P_Justificacion + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + "='" + Datos.P_Estatus + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Tipo_Operacion + "='" + Datos.P_Tipo_Operacion + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Area_Funcional_Id + "='" + Datos.P_Origen_Area_Funcional_Id + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id + "='" + Datos.P_Origen_Dependencia_Id + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Fuente_Financiamiento_Id + "='" + Datos.P_Origen_Fuente_Financiamiento_Id + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Partida_Id + "='" + Datos.P_Origen_Partida_Id  + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Programa_Id + "='" + Datos.P_Origen_Programa_Id + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Area_Funcional_Id + "='" + Datos.P_Destino_Area_Funcional_Id + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Dependencia_Id  + "='" + Datos.P_Destino_Dependencia_Id + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Fuente_Financiamiento_Id + "='" + Datos.P_Destino_Fuente_Financiamiento_Id + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Partida_Id + "='" + Datos.P_Destino_Partida_Id + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Programa_Id + "='" + Datos.P_Destino_Programa_Id + "', ");
                //Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Fecha_Modifico + "=GETDATE() ");
                //Mi_SQL.Append("WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + "=" + Datos.P_No_Solicitud);

                //Ejecutar consulta
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Conexion.Close();
                Operacion_Completa = true;
            }
            catch (SqlException Ex)
            {
                Transaccion.Rollback();
                Mensaje = "Error:  [" + Ex.Message + "]";

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Conexion.Close();
                Comando = null;
                Conexion = null;
                Transaccion = null;
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Consecutivo
        ///DESCRIPCIÓN: Obtiene el numero consecutivo para las tablas ocupadas en esta clase
        ///PARAMETROS: 1.-Campo del cual se obtendra el consecutivo
        ///            2.-Nombre de la tabla
        ///CREO: Gustavo Angeles Cruz
        ///FECHA_CREO: 10/Enero/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static int Obtener_Consecutivo(String Campo_ID, String Tabla)
        {
            int Consecutivo = 0;
            String Mi_Sql;
            Object Obj; //Obtiene el ID con la cual se guardo los datos en la base de datos
            Mi_Sql = "SELECT ISNULL(MAX (" + Campo_ID + "),'00000') FROM " + Tabla;
            Obj = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            Consecutivo = (Convert.ToInt32(Obj) + 1);
            return Consecutivo;
        }

        /// ********************************************************************************************************************
        /// NOMBRE: Eliminar_Movimiento
        /// 
        /// COMENTARIOS: Esta operación eliminara un registro del movimiento que se haya realizado en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a eliminar en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  14/Octubre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static Boolean Eliminar_Movimiento(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
            SqlConnection Conexion;//Variable para la conexión para la base de datos   
            SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
            String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
            Boolean Operacion_Completa = false;

            Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Comando = new SqlCommand();
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Transaction = Transaccion;
            Comando.Connection = Conexion;
            
            try
            {
                

                Mi_SQL.Append("Delete From " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + " ");
                Mi_SQL.Append("where " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + "=" + Datos.P_No_Solicitud);
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
            }
            catch (SqlException Ex)
            {
                Transaccion.Rollback();
                Mensaje = "Error:  [" + Ex.Message + "]";

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Conexion.Close();
                Comando = null;
                Conexion = null;
                Transaccion = null;
            }
            return Operacion_Completa;
        }

        /// ********************************************************************************************************************
        /// NOMBRE: Consultar_Movimiento
        /// 
        /// COMENTARIOS: Consulta el movimiento presupuestal que se haya llevado en la tabla OPE_COM_SOLICITUD_TRANSF  
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a consultar en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  14/Octubre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Movimiento(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            DataTable Dt_Movimiento = new DataTable();
            String Anio = String.Format("{0:yyyy}", DateTime.Now);

            try
            {
                Mi_SQL.Append("SELECT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre+ " AS CLAVE_NOM_PARTIDA, ");
                Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre+ " AS CLAVE_NOM_PROGRAMA, ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Aprobado + " AS APROBADO, ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + " AS IMPORTE, ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Ampliacion + " AS AMPLIACION, ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Reduccion + " AS REDUCCION, ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Modificado + " AS MODIFICADO, ");
                Mi_SQL.Append(" '0' AS DISPONIBLE, ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Enero + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Febrero + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Marzo + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Abril + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Mayo+ ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Junio+ ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Julio + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Agosto + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Septiembre + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Octubre + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Noviembre + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Diciembre + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Egreso + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Movimiento + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID + " AS PROGRAMA_ID, ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID + " AS FF_ID, ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Area_Funcional_ID + " AS AF_ID, ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID + " AS UR_ID, ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Movimiento_ID + " AS MOVIMIENTO_ID, ");
                Mi_SQL.Append(" ''  AS CAPITULO_ID, ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Justificacion);
                Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas  + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);

                if (!string.IsNullOrEmpty(Datos.P_No_Solicitud))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        //no llevan comilla simple es entero el numero de solicitud
                        Mi_SQL.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + "=" + Datos.P_No_Solicitud + "");
                    }
                     else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + "=" + Datos.P_No_Solicitud + "");
                    }
                }
                if (!string.IsNullOrEmpty(Datos.P_Importe))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + "=" + Datos.P_Importe + "");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + "=" + Datos.P_Importe + "");
                    }
                }
                if (!string.IsNullOrEmpty(Datos.P_Estatus))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + "='" + Datos.P_Estatus + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + "='" + Datos.P_Estatus + "'");
                    }
                }


                if (Mi_SQL.ToString().Contains("WHERE"))
                {
                    Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + "='" + Anio + "'");
                }
                else
                {
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + "='" + Datos.P_Anio + "'");
                }

                Mi_SQL.Append(" Order by " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " asc");
                Dt_Movimiento = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
               
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
            return Dt_Movimiento;
        }

        /// ********************************************************************************************************************
        /// NOMBRE: Consulta_Movimiento_Fecha
        /// 
        /// COMENTARIOS: Consulta el movimiento presupuestal que se haya llevado en la tabla OPE_COM_SOLICITUD_TRANSF por fecha  
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a consultar en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  18/Noviembre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consulta_Movimiento_Fecha(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            DataTable Dt_Movimiento = new DataTable();
            try
            {
                Mi_SQL.Append("Select " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + ".* ");
                Mi_SQL.Append("From " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf);


                if ((!string.IsNullOrEmpty(Datos.P_Fecha_Inicio)) && (string.IsNullOrEmpty(Datos.P_Fecha_Final)))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf .Campo_Fecha_Creo);
                        Mi_SQL.Append(" BETWEEN TO_DATE ('" + Datos.P_Fecha_Inicio + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                        Mi_SQL.Append(" AND TO_DATE('" + Datos.P_Fecha_Inicio + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Fecha_Creo);
                        Mi_SQL.Append(" BETWEEN TO_DATE ('" + Datos.P_Fecha_Inicio + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                        Mi_SQL.Append(" AND TO_DATE('" + Datos.P_Fecha_Inicio + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                    }
                }
                else
                {
                    if ((!string.IsNullOrEmpty(Datos.P_Fecha_Inicio)) && !string.IsNullOrEmpty(Datos.P_Fecha_Final))
                    {
                        if (Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Campo_Fecha_Creo);
                            Mi_SQL.Append(" BETWEEN TO_DATE ('" + Datos.P_Fecha_Inicio + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                            Mi_SQL.Append(" AND TO_DATE('" + Datos.P_Fecha_Final + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                        }
                        else
                        {
                            Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Fecha_Creo);
                            Mi_SQL.Append(" BETWEEN TO_DATE ('" + Datos.P_Fecha_Inicio + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')");
                            Mi_SQL.Append(" AND TO_DATE('" + Datos.P_Fecha_Final + " 23:59:00', 'DD/MM/YYYY HH24:MI:SS')");
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Datos.P_Dependencia_ID_Busqueda))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Dependencia_ID_Busqueda + "'");
                        Mi_SQL.Append(" OR " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Dependencia_ID_Busqueda + "'");

                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Dependencia_ID_Busqueda + "'");
                        Mi_SQL.Append(" OR " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Dependencia_ID_Busqueda + "'");

                    }
                }

                if (Mi_SQL.ToString().Contains("WHERE"))
                {
                    Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + " IN ('AUTORIZADA','RECHAZADA','GENERADA')");
                }
                else
                {
                    Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + " IN ('AUTORIZADA','RECHAZADA','GENERADA')");
                }


                Mi_SQL.Append(" Order by " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + " asc");
                Dt_Movimiento = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
               
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
            return Dt_Movimiento;
        }
        /// ********************************************************************************************************************
        /// NOMBRE: Consulta_Movimiento_Btn_Busqueda
        /// 
        /// COMENTARIOS: Consulta el movimiento presupuestal que se haya llevado en la tabla OPE_COM_SOLICITUD_TRANSF por fecha  
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a consultar en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  18/Noviembre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consulta_Movimiento_Btn_Busqueda(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            DataTable Dt_Movimiento = new DataTable();
            try
            {
                Mi_SQL.Append("Select " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + ".* ");
                Mi_SQL.Append("From " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf);


                if (!string.IsNullOrEmpty(Datos.P_No_Solicitud))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        //no llevan comilla simple es entero el numero de solicitud
                        Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + "=" + Datos.P_No_Solicitud + " ");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + "=" + Datos.P_No_Solicitud + " ");
                    }
                }

                if (!string.IsNullOrEmpty(Datos.P_Dependencia_ID_Busqueda))
                {
                    if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Dependencia_ID_Busqueda + "'");
                        Mi_SQL.Append(" OR " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Dependencia_ID_Busqueda + "'");

                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Dependencia_ID_Busqueda + "'");
                        Mi_SQL.Append(" OR " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Dependencia_Id);
                        Mi_SQL.Append(" = '" + Datos.P_Dependencia_ID_Busqueda + "'");

                    }
                }

                if (Mi_SQL.ToString().Contains("WHERE"))
                {
                    Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + " IN ('AUTORIZADA','RECHAZADA','GENERADA')");
                }
                else
                {
                    Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + " IN ('AUTORIZADA','RECHAZADA','GENERADA')");
                }

                Mi_SQL.Append(" Order by " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + " asc");
                Dt_Movimiento = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
            return Dt_Movimiento;
        }
        /// ********************************************************************************************************************
        /// NOMBRE: Consultar_programa
        /// 
        /// COMENTARIOS: Consulta el programa al que pertenece de la tabla CAT_SAP_PROYECTOS_PROGRAMAS  
        /// 
        /// PARÁMETROS: Datos.- Valor de los campos a consultar en la tabla de CAT_SAP_PROYECTOS_PROGRAMAS 
        /// 
        /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:  14/Octubre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Programa(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
        {
            DataTable Dt_Consulta_Programa = new DataTable();
            StringBuilder Mi_SQL;
            try
            {
                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("Select *");
                Mi_SQL.Append(" from " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);


                if (!string.IsNullOrEmpty(Datos.P_Programa_ID))
                {
                    Mi_SQL.Append(" where " + Cat_Sap_Proyectos_Programas.Campo_Clave + "='" + Datos.P_Programa_ID + "'");
                }
                Dt_Consulta_Programa = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception ex)

            {
                throw new Exception("Error: [" + ex.Message + "]");
            }
            return Dt_Consulta_Programa;
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Area_Funcional
        /// DESCRIPCION : Consulta la clave en la  CAT_SAP_AREA_FUNCIONAL  
        /// PARAMETROS  :  Datos.- Valor de los campos a insertar en la tabla de CAT_SAP_AREA_FUNCIONAL
        /// CREO        : Hugo Enrique Ramirez Aguilera 
        /// FECHA_CREO  : 14-Octubre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Area_Funcional(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
        {
            StringBuilder Mi_SQL;
            try
            {
                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("Select *");
                Mi_SQL.Append(" From " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                if (!string.IsNullOrEmpty(Datos.P_Area_Funcional_ID))
                {
                    Mi_SQL.Append(" where " + Cat_SAP_Area_Funcional.Campo_Clave + "='" + Datos.P_Area_Funcional_ID + "'");
                }
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Like_Movimiento
        /// DESCRIPCION : Consulta la clave que contenga algun valor paresido en el registro en la  CAT_SAP_AREA_FUNCIONAL  
        /// PARAMETROS  :  Datos.- Valor de los campos a insertar en la tabla de OPE_COM_SOLICITUD_TRANSF
        /// CREO        : Hugo Enrique Ramirez Aguilera 
        /// FECHA_CREO  : 29-Octubre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Like_Movimiento(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
               
                Mi_SQL.Append("SELECT " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + ", ");
                Mi_SQL.Append(" SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total+ ", 0)) AS IMPORTE, ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + ", ");
                Mi_SQL.Append(" CONVERT(VARCHAR(25)," + Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo + ", 101) AS FECHA_CREO, ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Creo + ", ");
                Mi_SQL.Append(" '' AS " + Ope_Psp_Movimiento_Egr_Det.Campo_Justificacion + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Comentario + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr);
                Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);

                if (!String.IsNullOrEmpty(Datos.P_Tipo_Egreso.Trim()))
                {
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Egreso + " = '" + Datos.P_Tipo_Egreso + "'");
                    Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Origen'");
                    Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + " = " + String.Format("{0:yyyy}",DateTime.Now));

                    if (!String.IsNullOrEmpty(Datos.P_Dependencia_ID_Busqueda.Trim()))
                    {
                        if (Datos.P_Tipo_Usuario.Trim().Equals("Administrador"))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Usuario + " = '" + Datos.P_Tipo_Usuario.Trim().ToUpper() + "'");
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID_Busqueda + "'");
                        }
                        else if (Datos.P_Tipo_Usuario.Trim().Equals("Ramo33"))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID_Busqueda + "'");
                        }
                        else if (Datos.P_Tipo_Usuario.Trim().Equals("Usuario"))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Usuario + " = '" + Datos.P_Tipo_Usuario.Trim().ToUpper() + "'");
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID_Busqueda + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Datos.P_No_Solicitud.Trim()))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Datos.P_No_Solicitud.Trim());
                    }

                    if (!String.IsNullOrEmpty(Datos.P_Fecha_Inicio))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo);
                        Mi_SQL.Append(" BETWEEN CONVERT(VARCHAR, '" + Datos.P_Fecha_Inicio + " 00:00:00', 120)");
                        Mi_SQL.Append(" AND CONVERT(VARCHAR, '" + Datos.P_Fecha_Final + " 23:59:00', 120)");
                    }

                }

                Mi_SQL.Append(" GROUP BY  " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + ", ");
                Mi_SQL.Append(" CONVERT(VARCHAR(25)," + Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo + ", 101), ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Creo + ", ");
                //Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Justificacion + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Comentario + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + ", ");
                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr);

                Mi_SQL.Append(" ORDER BY " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " DESC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consultar_Dependencia_Ordenada
        /// DESCRIPCION : Consulta la clave que contenga algun valor paresido en el registro en la  CAT_SAP_AREA_FUNCIONAL  
        /// PARAMETROS  :  Datos.- Valor de los campos a insertar en la tabla de CAT_DEPENDENCIA    
        /// CREO        : Hugo Enrique Ramirez Aguilera 
        /// FECHA_CREO  : 29-Octubre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consultar_Dependencia_Ordenada(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
        {
            StringBuilder Mi_SQL= new StringBuilder();
            try
            {
                Mi_SQL.Append("Select distinct " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " ");
                Mi_SQL.Append("from " + Cat_Dependencias.Tabla_Cat_Dependencias + " ");
                Mi_SQL.Append("Order by " + Cat_Dependencias.Campo_Clave + " ASC");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Consulta_Datos_Partidas
        /// DESCRIPCION : Consulta la clave que contenga algun valor paresido en el registro en la  CAT_SAP_AREA_FUNCIONAL  
        /// PARAMETROS  :  Datos.- Valor de los campos a insertar en la tabla de CAT_DEPENDENCIA    
        /// CREO        : Hugo Enrique Ramirez Aguilera 
        /// FECHA_CREO  : 29-Octubre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
         public static DataTable Consulta_Datos_Partidas(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {
            StringBuilder Mi_SQL = new StringBuilder(); //Variable para la consulta SQL

            try
            {
                Mi_SQL.Append("SELECT * ");
                Mi_SQL.Append(" FROM " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas);

                if (!string.IsNullOrEmpty(Datos.P_Partida_Especifica_ID))
                {
                    Mi_SQL.Append(" where " + Cat_Com_Partidas.Campo_Clave + " like '" + Datos.P_Partida_Especifica_ID + "%'");
                }

                Mi_SQL.Append("Order by " + Cat_Com_Partidas.Campo_Clave + " ASC");
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error: [" + Ex.Message + "]");
            }
        }
         ///*******************************************************************************
         /// NOMBRE DE LA FUNCION: Consulta_Datos_Comentarios
         /// DESCRIPCION : Consulta la clave que contenga algun valor paresido en el registro en la 
         ///                tabla Ope_Psp_Comentarios_Mov
         /// PARAMETROS  :  Datos.- Valor de los campos a insertar en la tabla de CAT_DEPENDENCIA    
         /// CREO        : Hugo Enrique Ramirez Aguilera 
         /// FECHA_CREO  : 17-noviembre-2011
         /// MODIFICO          :
         /// FECHA_MODIFICO    :
         /// CAUSA_MODIFICACION:
         ///*******************************************************************************
         public static DataTable Consulta_Datos_Comentarios(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {
             StringBuilder Mi_SQL = new StringBuilder(); //Variable para la consulta SQL

             try
             {
                 Mi_SQL.Append("SELECT * ");
                 Mi_SQL.Append(" FROM " + Ope_Psp_Comentarios_Mov.Tabla_Ope_Psp_Comentarios_Mov);

                 if (!string.IsNullOrEmpty(Datos.P_No_Solicitud))
                 {
                     Mi_SQL.Append(" where " + Ope_Psp_Comentarios_Mov.Campo_Numero_Solicitud + "=" + Datos.P_No_Solicitud);
                 }
                  Mi_SQL.Append(" Order by " + Ope_Psp_Comentarios_Mov.Campo_Fecha +" DESC");
                 
                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

             }
             catch (Exception Ex)
             {
                 throw new Exception("Error: [" + Ex.Message + "]");
             }
         }

        #endregion

        #region Metodos_Aprobado
         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Fuente_Financiamiento
         ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 30/Noviembre/2011
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Fuente_Financiamiento(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS nombre, ");
                 Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " AS id ");
                 Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                 Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                 Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                 Mi_Sql.Append(" = '" + Negocio.P_Anio + "'");

                 if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID_Busqueda.Trim()))
                 {
                     Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                     Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                     Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                     Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                     Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID_Busqueda + "'");
                 }
                 if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                 {
                     if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                     {
                         Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID);
                         Mi_Sql.Append(" NOT IN(SELECT " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Clave + " = '1000'" + ")");
                         Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " NOT IN ");
                         Mi_Sql.Append(" (SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                         Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " LIKE '%participaciones%'" );
                         Mi_Sql.Append(" OR " + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " LIKE '%Participaciones%'");
                         Mi_Sql.Append(" OR " + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " LIKE '%PARTICIPACIONES%')");
                     }
                 }
                 else 
                 {
                     Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " NOT IN('00001')");
                 }

                 Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'NO'");
                
                 Mi_Sql.Append(" ORDER BY nombre ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Programas
         ///DESCRIPCIÓN          : consulta para obtener los datos de los programas
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 30/Noviembre/2011
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Programas_Aprobados(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT DISTINCT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS nombre, ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " AS Programa_id ");
                 Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                 Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                 Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                 Mi_Sql.Append(" = '" + Negocio.P_Anio + "'");
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Fuente_Financiamiento_ID + "'");
                 Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID + "'");
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID_Busqueda + "'");

                 Mi_Sql.Append(" ORDER BY nombre ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de los programas. Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Partida_Especifica
         ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas especificas
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 30/Noviembre/2011
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Partida_Especifica(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS nombre, ");
                 Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " AS Partida_id, ");
                 Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + ", ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS PROGRAMA, ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas  + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " AS PROGRAMA_ID, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ",0) AS APROBADO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) AS DISPONIBLE, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion+ ",0) AS AMPLIACION, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion+ ",0) AS REDUCCION, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ",0) AS MODIFICADO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ", 0) AS IMPORTE_ENERO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ", 0) AS IMPORTE_FEBRERO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ", 0) AS IMPORTE_MARZO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ", 0) AS IMPORTE_ABRIL, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ", 0) AS IMPORTE_MAYO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ", 0) AS IMPORTE_JUNIO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ", 0) AS IMPORTE_JULIO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ", 0) AS IMPORTE_AGOSTO, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ", 0) AS IMPORTE_SEPTIEMBRE, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ", 0) AS IMPORTE_OCTUBRE, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ", 0) AS IMPORTE_NOVIEMBRE, ");
                 Mi_Sql.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ", 0) AS IMPORTE_DICIEMBRE, ");
                 Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " AS FF_ID, ");
                 Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + " AS AF_ID, ");
                 Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " AS UR_ID ");
                 Mi_Sql.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                 Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                 Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                 Mi_Sql.Append(" = '" + Negocio.P_Anio + "'");
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID_Busqueda + "'");

                 if (!String.IsNullOrEmpty(Negocio.P_Fuente_Financiamiento_ID))
                 {
                     Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                     Mi_Sql.Append(" = '" + Negocio.P_Fuente_Financiamiento_ID + "'");
                 }

                 if (!String.IsNullOrEmpty(Negocio.P_Partida_Especifica_ID))
                 {
                     Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                     Mi_Sql.Append(" = '" + Negocio.P_Partida_Especifica_ID + "'");
                 }
                
                 Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas );
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                 Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                 if (!String.IsNullOrEmpty(Negocio.P_Programa_ID))
                 {
                     Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                     Mi_Sql.Append(" = '" + Negocio.P_Programa_ID + "'");
                 }
                 Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Area_Funcional_ID + "'");


                 if (!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                 {
                     if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                     {
                         Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID);
                         Mi_Sql.Append(" NOT IN(SELECT " +Cat_SAP_Capitulos.Campo_Capitulo_ID + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Clave + " = '1000'" + ")");
                     }
                 }
                 else 
                 {
                     Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID);
                     Mi_Sql.Append(" NOT IN(SELECT " + Cat_SAP_Capitulos.Campo_Capitulo_ID + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + " WHERE " + Cat_SAP_Capitulos.Campo_Clave + " = '1000'" + ")");
                 }

                 Mi_Sql.Append(" ORDER BY nombre ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de las partidas especificas. Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Capitulos
         ///DESCRIPCIÓN          : consulta para obtener los datos de los capitulos
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 05/Diciembre/2011
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Capitulos(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT DISTINCT " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + " AS nombre, ");
                 Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + " AS id ");
                 Mi_Sql.Append(" FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                 Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID);
                 Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                 Mi_Sql.Append(" = '" + Negocio.P_Anio + "'");
                 Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID_Busqueda + "'");
                 Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                 Mi_Sql.Append(" = '"+Negocio.P_Programa_ID +"'");
                 Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                 Mi_Sql.Append(" = '" + Negocio.P_Fuente_Financiamiento_ID + "'");
                
                 Mi_Sql.Append(" ORDER BY nombre ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de los capitulos. Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Detalles_Importes
         ///DESCRIPCIÓN          : consulta para obtener los datos de los detalles de los importes
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 09/Marzo/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Detalles_Importes(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT * FROM " + Ope_Com_Solicitud_Transf_Det.Tabla_Ope_Psp_Hist_Calendar_Presu);
                 Mi_Sql.Append(" WHERE " + Ope_Com_Solicitud_Transf_Det.Campo_No_Solicitud + " = '"+Negocio.P_No_Solicitud +"'");
                 
                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de los capitulos. Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Obtener_Capitulos
         ///DESCRIPCIÓN          : consulta para obtener los datos de los capitulos
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 20/Marzo/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Obtener_Capitulos(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT DISTINCT " +  Cat_SAP_Capitulos.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_SAP_Capitulos.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                 Mi_Sql.Append(Cat_SAP_Capitulos.Campo_Capitulo_ID);
                 Mi_Sql.Append(" FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                 Mi_Sql.Append(" WHERE  " + Cat_SAP_Capitulos.Campo_Estatus + " = 'ACTIVO'");

                 if(!String.IsNullOrEmpty(Negocio.P_Tipo_Usuario.Trim()))
                 {
                     if(Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                     {
                         Mi_Sql.Append(" AND " + Cat_SAP_Capitulos.Campo_Clave + " NOT IN ('1000')");
                     }
                 }

                 Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de los capitulos. Error: [" + Ex.Message + "]");
             }
         }


         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Fuente_Financiamiento
         ///DESCRIPCIÓN          : Obtiene datos de las fuentes de financiamiento de la Base de Datos y los regresa en un DataTable.
         ///PARAMETROS           : 
         ///CREO                 : Leslie González Vázquez
         ///FECHA_CREO           : 11/Noviembre/2011
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Fuente_Financiamiento_Ramo33()
         {
             StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
             DataSet Ds_Fuente = new DataSet(); //Dataset donde obtendremos los datos de la consulta
             DataTable Dt_Fuente = new DataTable();
             try
             {
                 Mi_SQL.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", ");
                 Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Clave + "+' " + " " + "'+ " + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS NOMBRE ");
                 Mi_SQL.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                 Mi_SQL.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Estatus + " = 'ACTIVO'");
                 Mi_SQL.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = 'NO'");
                 Mi_SQL.Append(" ORDER BY " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " ASC");

                 Ds_Fuente = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                 if (Ds_Fuente != null)
                 {
                     Dt_Fuente = Ds_Fuente.Tables[0];
                 }
             }
             catch (Exception Ex)
             {
                 String Mensaje = "Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                 throw new Exception(Mensaje);
             }
             return Dt_Fuente;
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Usuario_Ramo33
         ///DESCRIPCIÓN          : consulta para obtener los datos del usuario de ramo33
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 15/Mayo/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Usuario_Ramo33()
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT " + Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv + ", ");
                 Mi_Sql.Append(Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv_2);
                 Mi_Sql.Append(" FROM " + Cat_Con_Parametros.Tabla_Cat_Con_Parametros);
                
                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los usuarios de ramo33 Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Movimientos
         ///DESCRIPCIÓN          : consulta para obtener los datos de los movimientos
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 26/Mayo/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Consultar_Datos_Movimientos(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {
             StringBuilder Mi_SQL = new StringBuilder();
             try
             {

                 Mi_SQL.Append("SELECT " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + ", ");
                 Mi_SQL.Append(" SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + ", 0)) AS IMPORTE, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + ", ");
                 Mi_SQL.Append(" CONVERT(VARCHAR, " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo + ", 101) AS FECHA_CREO, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Creo + ", ");
                 Mi_SQL.Append("'T' + '' + ISNULL(CONVERT(VARCHAR," + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + "),0) + '-' + ");
                 Mi_SQL.Append(" CONVERT(VARCHAR," + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ") + '-' + ");
                 Mi_SQL.Append(" CONVERT(VARCHAR," + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + ") AS NO_SOLICITUD ");
                 Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                 Mi_SQL.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." +Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID);
                 Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                 Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID);
                 Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                 Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                 Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID);
                 Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                 Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                 Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID);
                 Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);

                 if (!String.IsNullOrEmpty(Datos.P_Estatus.Trim()))
                 {
                     Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + " IN (" + Datos.P_Estatus.Trim() + ")");
                 }

                 if (!String.IsNullOrEmpty(Datos.P_Tipo_Egreso.Trim()))
                 {
                     if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                     {
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Egreso + " = '" + Datos.P_Tipo_Egreso + "'");
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Origen'");
                     }
                     else 
                     {
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Egreso + " = '" + Datos.P_Tipo_Egreso + "'");
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Origen'");
                     }
                 }

                 if (!String.IsNullOrEmpty(Datos.P_No_Solicitud.Trim()))
                 {
                     if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                     {
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim());
                     }
                     else 
                     {
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim());
                     }
                 }

                 Mi_SQL.Append(" GROUP BY  " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + ", ");
                 Mi_SQL.Append(" CONVERT(VARCHAR, " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo + ", 101), ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Creo + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio);

                 Mi_SQL.Append(" ORDER BY " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " DESC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

             }
             catch (Exception Ex)
             {
                 throw new Exception("Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Movimientos_Det
         ///DESCRIPCIÓN          : consulta para obtener los datos de los movimientos
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 26/Mayo/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Consultar_Datos_Movimientos_Det(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {
             StringBuilder Mi_SQL = new StringBuilder();
             try
             {
                 Mi_SQL.Append("SELECT " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Egreso + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Justificacion + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Movimiento_ID + " AS NO_MOVIMIENTO, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + " AS IMPORTE, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Aprobado + " AS APROBADO, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Ampliacion + " AS AMPLIACION, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Reduccion + " AS REDUCCION, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Modificado + " AS MODIFICADO, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Enero + " AS ENERO, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Febrero + " AS FEBRERO, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Marzo + " AS MARZO, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Abril + " AS ABRIL, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Mayo + " AS MAYO, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Junio + " AS JUNIO, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Julio + " AS JULIO, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Agosto + " AS AGOSTO, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Septiembre + " AS SEPTIEMBRE, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Octubre + " AS OCTUBRE, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Noviembre + " AS NOVIEMBRE, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Diciembre + " AS DICIEMBRE, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Justificacion + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Solicitante + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Solicitante + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Director + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Director + ", ");
                 Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                 Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS PP, ");
                 Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' ' + ");
                 Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS UR, ");
                 Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + ");
                 Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA, ");
                 Mi_SQL.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS AF ");
                 Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                 Mi_SQL.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID);
                 Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                 Mi_SQL.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                 Mi_SQL.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                 Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                 Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID);
                 Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                 Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                 Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID);
                 Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                 Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                 Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID);
                 Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);

                 if (!String.IsNullOrEmpty(Datos.P_No_Solicitud.Trim()))
                 {
                     Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = '" + Datos.P_No_Solicitud + "'");
                 }

                 Mi_SQL.Append(" ORDER BY " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Movimiento_ID + " DESC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

             }
             catch (Exception Ex)
             {
                 throw new Exception("Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Movimientos
         ///DESCRIPCIÓN          : consulta para obtener los datos de los movimientos
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 26/Mayo/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Consultar_Datos_Modificaciones(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {
             StringBuilder Mi_SQL = new StringBuilder();
             try
             {
                 Mi_SQL.Append("SELECT " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " AS NO_MOVIMIENTO_EGR , ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Anio + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + " AS TOTAL_MODIFICADO, ");
                 Mi_SQL.Append(" CONVERT(VARCHAR(25)," + Ope_Psp_Movimiento_Egr.Campo_Fecha_Creo + ", 101) AS FECHA_CREO, ");
                 if (!String.IsNullOrEmpty(Datos.P_Tipo_Egreso.Trim()))
                 {
                     if (Datos.P_Tipo_Egreso.Trim().Equals("MUNICIPAL"))
                     {
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Estatus + " AS ESTATUS, ");
                     }
                 }
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Usuario_Creo);
                 Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                 Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now));
                 
                 Mi_SQL.Append(" ORDER BY NO_MOVIMIENTO_EGR DESC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Modificar_Autorizacion_Mov
         ///DESCRIPCIÓN          : consulta para actualizar los datos de los movimientos
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 26/Mayo/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static String Modificar_Autorizacion_Mov(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {
             StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
             SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
             SqlConnection Conexion;//Variable para la conexión para la base de datos   
             SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
             String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
             String Autorizados = String.Empty;
             DataTable Dt_Movimientos = new DataTable();
             String Operacion = String.Empty;
             DataTable Dt_Mov = new DataTable();
             String Anio = string.Format("{0:yyyy}", DateTime.Now);
             Double Total = 0.00;
             Object Total_Psp;
             SqlDataAdapter Da_Datos = new SqlDataAdapter(); //
             DataSet Ds_Datos = new DataSet();
             String[] Datos_Poliza_PSP = null;
             String No_Poliza_PSP = String.Empty;
             String Tipo_Poliza_PSP = String.Empty;
             String Mes_Anio_PSP = String.Empty;

             Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
             Comando = new SqlCommand();
             Conexion.Open();
             Transaccion = Conexion.BeginTransaction();
             Comando.Transaction = Transaccion;
             Comando.Connection = Conexion;

             try
             {
                 //OBTENEMOS EL CONSECUTIVO POR SI TENEMOS QUE ABRIR UNA NUEVA MODIFICACION
                 Int32 No_Movimiento_Egr;
                 //obtenemos el id maximo de la tabla de movimientos de egresos
                 Mi_SQL = new StringBuilder();
                 Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + "), 0)");
                 Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                 Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio);
                 
                 Comando.CommandText = Mi_SQL.ToString();

                 No_Movimiento_Egr = Convert.ToInt32(Comando.ExecuteScalar()) + 1;

                 Mi_SQL = new StringBuilder();
                 Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                 if (Datos.P_Tipo_Egreso.Trim().Equals("MUNICIPAL"))
                 {
                     Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Egr.Campo_Estatus + " = '" + Datos.P_Estatus.Trim() + "', ");
                 }
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Fecha_Modifico + " = GETDATE()");
                 Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim());
                 Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Datos.P_Anio.Trim());
                 Comando.CommandText = Mi_SQL.ToString();
                 Comando.ExecuteNonQuery();

                 Dt_Movimientos = Datos.P_Dt_Mov;

                 if (Dt_Movimientos != null && Dt_Movimientos.Rows.Count > 0)
                 {
                     if (Datos.P_Estatus.Trim().Equals("AUTORIZADO"))
                     {
                         //creamos la poliza presupuestal
                         Datos_Poliza_PSP = Genera_Poliza_Autorizacion_Modificacion(Dt_Movimientos, Comando);
                         //obtenemos los id de la poliza presupuestal creada
                         if (Datos_Poliza_PSP != null)
                         {
                             if (Datos_Poliza_PSP.Length > 0)
                             {
                                 No_Poliza_PSP = Datos_Poliza_PSP[0].Trim();
                                 Tipo_Poliza_PSP = Datos_Poliza_PSP[1].Trim();
                                 Mes_Anio_PSP = Datos_Poliza_PSP[2].Trim();
                             }
                         }

                         foreach (DataRow Dr in Dt_Movimientos.Rows)
                         {
                             if (Dr["ESTATUS"].ToString().Trim().Equals("AUTORIZADO"))
                             {
                                 //limpiamos las variables
                                 Dt_Mov = new DataTable();
                                 Da_Datos = new SqlDataAdapter();
                                 Ds_Datos = new DataSet();

                                 Mi_SQL = new StringBuilder();
                                 Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                                 Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                                 Comando.CommandText = Mi_SQL.ToString();
                                 Da_Datos = new SqlDataAdapter(Comando);
                                 Da_Datos.Fill(Ds_Datos);

                                 if (Ds_Datos != null)
                                 {
                                     Dt_Mov = Ds_Datos.Tables[0];
                                 }

                                 if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
                                 {
                                     //actualizamos los datos de la modificacion
                                     Mi_SQL = new StringBuilder();
                                     Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + " SET ");
                                     Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + "='" + Dr["ESTATUS"].ToString().Trim() + "', ");
                                     Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Comentario + "='" + Dr["COMENTARIO"].ToString().Trim() + "', ");

                                     if (!String.IsNullOrEmpty(No_Poliza_PSP))
                                     {
                                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_No_Poliza_Presupuestal + " = '" + No_Poliza_PSP.Trim() + "', ");
                                     }
                                     if (!String.IsNullOrEmpty(Tipo_Poliza_PSP))
                                     {
                                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Poliza_ID_Presupuestal + " = '" + Tipo_Poliza_PSP.Trim() + "', ");
                                     }
                                     if (!String.IsNullOrEmpty(Mes_Anio_PSP))
                                     {
                                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Mes_Anio_Presupuestal + " = '" + Mes_Anio_PSP.Trim() + "', ");
                                     }

                                     Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                                     Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + "=GETDATE() ");
                                     Mi_SQL.Append("WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                                     Comando.CommandText = Mi_SQL.ToString();
                                     Comando.ExecuteNonQuery();

                                     foreach (DataRow Dr_Mov in Dt_Mov.Rows)
                                     {
                                         if (Dr_Mov["TIPO_OPERACION"].ToString().Trim().Equals("REDUCCION"))
                                         {
                                             Mi_SQL = new StringBuilder();
                                             Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                             Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ",0) - " + Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ",0) + " + Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ",0) - " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ",0) - " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ",0) - " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ",0) - " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ",0) - " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ",0) - " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ",0) - " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ",0) - " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ",0) - " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ",0) - " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ",0) - " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ",0) - " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ",0) - " + Dr_Mov["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ",0) - " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ",0) - " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ",0) - " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ",0) - " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ",0) - " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ",0) - " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ",0) - " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ",0) - " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ",0) - " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ",0) - " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ",0) - " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ",0) - " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                             Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                             Comando.CommandText = Mi_SQL.ToString();
                                             Comando.ExecuteNonQuery();

                                             Mi_SQL = new StringBuilder();
                                             Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                             Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ",0) ");
                                             Mi_SQL.Append(" + ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ",0) - ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ",0)");
                                             Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                             Comando.CommandText = Mi_SQL.ToString();
                                             Comando.ExecuteNonQuery();

                                         }
                                         else if (Dr_Mov["TIPO_OPERACION"].ToString().Trim().Equals("AMPLIACION"))
                                         {
                                             Mi_SQL = new StringBuilder();
                                             Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                             Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) + " + Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ",0) + " + Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ",0) ");
                                             Mi_SQL.Append(" + ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ",0) - ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ",0), ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ",0) + " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ",0) + " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ",0) + " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ",0) + " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ",0) + " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ",0) + " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ",0) + " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ",0) + " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ",0) + " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ",0) + " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ",0) + " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ",0) + " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ",0) + " + Dr_Mov["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) + " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) + " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) + " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) + " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) + " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) + " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) + " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) + " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) + " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) + " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) + " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) + " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                             Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                             Comando.CommandText = Mi_SQL.ToString();
                                             Comando.ExecuteNonQuery();

                                             Mi_SQL = new StringBuilder();
                                             Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                             Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ",0)");
                                             Mi_SQL.Append(" + ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ",0) - ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ",0)");
                                             Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                             Comando.CommandText = Mi_SQL.ToString();
                                             Comando.ExecuteNonQuery();
                                         }
                                         else if (Dr_Mov["TIPO_OPERACION"].ToString().Trim().Equals("TRASPASO"))
                                         {
                                             if (Dr_Mov["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                                             {
                                                 Mi_SQL = new StringBuilder();
                                                 Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                                 Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ",0) - " + Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ",0) + " + Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ",0) - " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ",0) - " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ",0) - " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ",0) - " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ",0) - " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ",0) - " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ",0) - " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ",0) - " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ",0) - " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ",0) - " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ",0) - " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ",0) - " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ",0) - " + Dr_Mov["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ",0) - " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ",0) - " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ",0) - " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ",0) - " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = ISNULL( " + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ",0) - " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ",0) - " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ",0) - " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ",0) - " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ",0) - " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ",0) - " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ",0) - " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ",0) - " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                                 Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                                 Comando.CommandText = Mi_SQL.ToString();
                                                 Comando.ExecuteNonQuery();
                                             }
                                             else
                                             {
                                                 Mi_SQL = new StringBuilder();
                                                 Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                                 Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) + " + Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ",0) + " + Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ",0) + " + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " - " + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ",0) + " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ",0) + " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ",0) + " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ",0) + " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ",0) + " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ",0) + " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ",0) + " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ",0) + " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ",0) + " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ",0) + " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ",0) + " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ",0) + " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ",0) + " + Dr_Mov["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) + " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) + " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) + " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) + " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) + " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) + " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) + " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) + " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) + " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) + " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) + " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) + " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                                 Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                                 Comando.CommandText = Mi_SQL.ToString();
                                                 Comando.ExecuteNonQuery();
                                             }
                                             Mi_SQL = new StringBuilder();
                                             Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                             Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ",0) + ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ",0) - ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ",0) ");
                                             Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                             Comando.CommandText = Mi_SQL.ToString();
                                             Comando.ExecuteNonQuery();
                                         }
                                     }
                                 }
                             }
                             else
                             {
                                 Mi_SQL = new StringBuilder();
                                 Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + " SET ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + "='" + Dr["ESTATUS"].ToString().Trim() + "', ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Comentario + "='" + Dr["COMENTARIO"].ToString().Trim() + "', ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + "=GETDATE() ");
                                 Mi_SQL.Append("WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                                 Comando.CommandText = Mi_SQL.ToString();
                                 Comando.ExecuteNonQuery();

                                 if (Dr["ESTATUS"].ToString().Trim().Equals("RECHAZADO"))
                                 {
                                     //limpiamos las variables
                                     Dt_Mov = new DataTable();
                                     Da_Datos = new SqlDataAdapter();
                                     Ds_Datos = new DataSet();

                                     Mi_SQL = new StringBuilder();
                                     Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                                     Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                                     Comando.CommandText = Mi_SQL.ToString();
                                     Da_Datos = new SqlDataAdapter(Comando);
                                     Da_Datos.Fill(Ds_Datos);
                                     if (Ds_Datos != null)
                                     {
                                         Dt_Mov = Ds_Datos.Tables[0];
                                     }

                                     if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
                                     {
                                         foreach (DataRow Dr_Mov in Dt_Mov.Rows)
                                         {
                                             if (Dr_Mov["TIPO_MOVIMIENTO"].ToString().Trim().Equals("Nueva")
                                                 && Convert.ToDouble(String.IsNullOrEmpty(Dr_Mov["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr_Mov["IMPORTE_TOTAL"]) != 0.00)
                                             {
                                                 Mi_SQL = new StringBuilder();
                                                 Mi_SQL.Append("DELETE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                                 Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                                 Comando.CommandText = Mi_SQL.ToString();
                                                 Comando.ExecuteNonQuery();

                                                 //QUERY AL MOMENTO DE MODIFICAR UNA PARTIDA NUEVA
                                                 Mi_SQL = new StringBuilder();
                                                 Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + " SET ");
                                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + "=ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0) - ");
                                                 Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"]);
                                                 Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud);
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio + "");
                                                 Comando.CommandText = Mi_SQL.ToString();
                                                 Comando.ExecuteNonQuery();
                                             }
                                             else
                                             {
                                                 if (Dr_Mov["TIPO_OPERACION"].ToString().Trim().Equals("TRASPASO") || Dr_Mov["TIPO_OPERACION"].ToString().Trim().Equals("REDUCCION"))
                                                 {
                                                     if (Dr_Mov["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                                                     {
                                                         Mi_SQL = new StringBuilder();
                                                         Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " SET ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ",0) - ");
                                                         Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) + ");
                                                         Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ",0) - " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ",0) - " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ",0) - " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ",0) - " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ",0) - " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ",0) - " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ",0) - " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ",0) - " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ",0) - " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ",0) - " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ",0) - " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ",0) - " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) + " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) + " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) + " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) + " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) + " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) + " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) + " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) + " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) + " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) + " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) + " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) + " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", ""));
                                                         Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                                         Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                                         Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                                         Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                                         Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                                         Comando.CommandText = Mi_SQL.ToString();
                                                         Comando.ExecuteNonQuery();

                                                         //QUERY PARA QUITAR SI ES UN TRASPASO DE TIPO ORIGEN Y LA REDUCCION
                                                         Mi_SQL = new StringBuilder();
                                                         Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + " SET ");
                                                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + " = ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0) - ");
                                                         Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"]);
                                                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud);
                                                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio + "");
                                                         Comando.CommandText = Mi_SQL.ToString();
                                                         Comando.ExecuteNonQuery();
                                                     }
                                                 }
                                                 else if (Dr_Mov["TIPO_OPERACION"].ToString().Trim().Equals("AMPLIACION"))
                                                 {
                                                     //QUERY PARA LA AMPLIACION
                                                     Mi_SQL = new StringBuilder();
                                                     Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + " SET ");
                                                     Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + " = ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0) - ");
                                                     Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"]);
                                                     Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud);
                                                     Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio + "");
                                                     Comando.CommandText = Mi_SQL.ToString();
                                                     Comando.ExecuteNonQuery();
                                                 }
                                             }
                                         }
                                     }
                                 }
                             }
                         }

                         Mi_SQL = new StringBuilder();
                         Mi_SQL.Append("INSERT  INTO " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "(");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Fte_Financiamiento_ID + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Proyecto_Programa_ID + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Dependencia_ID + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Partida_ID + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Capitulo_ID + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Anio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_No_Modificacion + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Enero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Febrero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Marzo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Abril + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Mayo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Junio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Julio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Agosto + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Septiembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Octubre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Noviembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Diciembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Total + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Aprobado + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Ampliacion + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Reduccion + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Modificado + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Devengado + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Ejercido + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pagado + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Comprometido + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pre_Comprometido + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Disponible + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Disponible_Enero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Disponible_Febrero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Disponible_Marzo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Disponible_Abril + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Disponible_Mayo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Disponible_Junio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Disponible_Julio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Disponible_Agosto + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Disponible_Septiembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Disponible_Octubre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Disponible_Noviembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Disponible_Diciembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pre_Comprometido_Enero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pre_Comprometido_Febrero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pre_Comprometido_Marzo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pre_Comprometido_Abril + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pre_Comprometido_Mayo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pre_Comprometido_Junio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pre_Comprometido_Julio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pre_Comprometido_Agosto + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pre_Comprometido_Septiembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pre_Comprometido_Octubre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pre_Comprometido_Noviembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pre_Comprometido_Diciembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Comprometido_Enero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Comprometido_Febrero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Comprometido_Marzo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Comprometido_Abril + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Comprometido_Mayo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Comprometido_Junio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Comprometido_Julio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Comprometido_Agosto + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Comprometido_Septiembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Comprometido_Octubre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Comprometido_Noviembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Comprometido_Diciembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Devengado_Enero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Devengado_Febrero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Devengado_Marzo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Devengado_Abril + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Devengado_Mayo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Devengado_Junio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Devengado_Julio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Devengado_Agosto + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Devengado_Septiembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Devengado_Octubre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Devengado_Noviembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Devengado_Diciembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Ejercido_Enero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Ejercido_Febrero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Ejercido_Marzo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Ejercido_Abril + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Ejercido_Mayo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Ejercido_Junio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Ejercido_Julio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Ejercido_Agosto + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Ejercido_Septiembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Ejercido_Octubre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Ejercido_Noviembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Ejercido_Diciembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pagado_Enero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pagado_Febrero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pagado_Marzo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pagado_Abril + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pagado_Mayo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pagado_Junio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pagado_Julio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pagado_Agosto + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pagado_Septiembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pagado_Octubre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pagado_Noviembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Pagado_Diciembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Saldo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Usuario_Creo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Fecha_Creo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Usuario_Modifico + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Fecha_Modifico + ") ");
                         Mi_SQL.Append("SELECT " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Anio + ", ");
                         Mi_SQL.Append("'" + Datos.P_No_Solicitud.Trim() + "' AS NO_MOVIMIENTO, ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Enero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Febrero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Marzo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Abril + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Mayo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Junio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Julio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Agosto + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Septiembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Octubre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Noviembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Diciembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Enero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Febrero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Marzo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Abril + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Mayo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Junio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Julio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Agosto + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Septiembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Octubre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Noviembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Diciembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Enero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Febrero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Marzo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Abril + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Mayo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Junio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Julio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Agosto + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Septiembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Octubre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Noviembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Diciembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Enero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Febrero + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Marzo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Abril + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Mayo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Junio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Julio + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Agosto + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Septiembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Octubre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Noviembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Diciembre + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Saldo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Creo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Creo + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + ", ");
                         Mi_SQL.Append(Ope_Psp_Presupuesto_Egr_Esp.Campo_Fecha_Modifico);
                         Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Datos.P_Anio.Trim());

                         Comando.CommandText = Mi_SQL.ToString();
                         Comando.ExecuteNonQuery();
                     }
                     else if (Datos.P_Estatus.Trim().Equals("GENERADO"))
                     {
                         foreach (DataRow Dr in Dt_Movimientos.Rows)
                         {
                             Mi_SQL = new StringBuilder();
                             Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + " SET ");
                             Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + "='" + Dr["ESTATUS"].ToString().Trim() + "', ");
                             Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Comentario + "='" + Dr["COMENTARIO"].ToString().Trim() + "', ");
                             Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                             Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + "=GETDATE() ");
                             Mi_SQL.Append("WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                             Comando.CommandText = Mi_SQL.ToString();
                             Comando.ExecuteNonQuery();

                             if (Dr["ESTATUS"].ToString().Trim().Equals("CANCELADO"))
                             {
                                 //limpiamos la variable
                                 Dt_Mov = new DataTable();
                                 Ds_Datos = new DataSet();
                                 Da_Datos = new SqlDataAdapter();

                                 Mi_SQL = new StringBuilder();
                                 Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                                 Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                                 Comando.CommandText = Mi_SQL.ToString();

                                 Da_Datos = new SqlDataAdapter(Comando);
                                 Da_Datos.Fill(Ds_Datos);
                                 if (Ds_Datos != null)
                                 {
                                     Dt_Mov = Ds_Datos.Tables[0];
                                 }

                                 if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
                                 {
                                     foreach (DataRow Dr_Mov in Dt_Mov.Rows)
                                     {
                                         if (Dr_Mov["TIPO_MOVIMIENTO"].ToString().Trim().Equals("Nueva")
                                             && Convert.ToDouble(String.IsNullOrEmpty(Dr_Mov["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr_Mov["IMPORTE_TOTAL"]) != 0.00)
                                         {
                                             Mi_SQL = new StringBuilder();
                                             Mi_SQL.Append("DELETE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                             Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                             Comando.CommandText = Mi_SQL.ToString();
                                             Comando.ExecuteNonQuery();

                                             Mi_SQL = new StringBuilder();
                                             Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + " SET ");
                                             Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + "= ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0) - ");
                                             Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"]);
                                             Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud);
                                             Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio + "");
                                             Comando.CommandText = Mi_SQL.ToString();
                                             Comando.ExecuteNonQuery();
                                         }
                                         else
                                         {
                                             if (Dr_Mov["TIPO_OPERACION"].ToString().Trim().Equals("TRASPASO") || Dr_Mov["TIPO_OPERACION"].ToString().Trim().Equals("REDUCCION"))
                                             {
                                                 if (Dr_Mov["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                                                 {
                                                     Mi_SQL = new StringBuilder();
                                                     Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " SET ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ",0) - ");
                                                     Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) + ");
                                                     Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) + " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) + " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) + " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) + " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) + " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) + " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) + " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) + " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) + " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) + " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) + " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) + " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ",0) - " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ",0) - " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ",0) - " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ",0) - " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ",0) - " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ",0) - " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ",0) - " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ",0) - " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ",0) - " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ",0) - " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ",0) - " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ",0) - " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", ""));
                                                     Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                                     Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                                     Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                                     Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                                     Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                                     Comando.CommandText = Mi_SQL.ToString();
                                                     Comando.ExecuteNonQuery();

                                                     Mi_SQL = new StringBuilder();
                                                     Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + " SET ");
                                                     Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + "=ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0) - ");
                                                     Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"]);
                                                     Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud);
                                                     Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio + "");
                                                     Comando.CommandText = Mi_SQL.ToString();
                                                     Comando.ExecuteNonQuery();
                                                 }
                                             }
                                             else if (Dr_Mov["TIPO_OPERACION"].ToString().Trim().Equals("AMPLIACION"))
                                             {
                                                 Mi_SQL = new StringBuilder();
                                                 Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + " SET ");
                                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + "= ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0) - ");
                                                 Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"]);
                                                 Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud);
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio + "");
                                                 Comando.CommandText = Mi_SQL.ToString();
                                                 Comando.ExecuteNonQuery();
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                     else if (Datos.P_Estatus.Trim().Equals("PREAUTORIZADO"))
                     {
                         foreach (DataRow Dr in Dt_Movimientos.Rows)
                         {
                             if (Dr["ESTATUS"].ToString().Trim().Equals("CANCELADO"))
                             {
                                 Mi_SQL = new StringBuilder();
                                 Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + " SET ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + "='" + Dr["ESTATUS"].ToString().Trim() + "', ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Comentario + "='" + Dr["COMENTARIO"].ToString().Trim() + "', ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + "=GETDATE() ");
                                 Mi_SQL.Append("WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                                 Comando.CommandText = Mi_SQL.ToString();
                                 Comando.ExecuteNonQuery();

                                 //limpiamos la variable
                                 Dt_Mov = new DataTable();
                                 Ds_Datos = new DataSet();
                                 Da_Datos = new SqlDataAdapter();

                                 Mi_SQL = new StringBuilder();
                                 Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                                 Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                                 Comando.CommandText = Mi_SQL.ToString();
                                 Da_Datos = new SqlDataAdapter(Comando);
                                 Da_Datos.Fill(Ds_Datos);
                                 if (Ds_Datos != null)
                                 {
                                     Dt_Mov = Ds_Datos.Tables[0];
                                 }

                                 if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
                                 {
                                     foreach (DataRow Dr_Mov in Dt_Mov.Rows)
                                     {
                                         if (Dr_Mov["TIPO_MOVIMIENTO"].ToString().Trim().Equals("Nueva")
                                             && Convert.ToDouble(String.IsNullOrEmpty(Dr_Mov["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr_Mov["IMPORTE_TOTAL"]) != 0.00)
                                         {
                                             Mi_SQL = new StringBuilder();
                                             Mi_SQL.Append("DELETE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                             Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                             Comando.CommandText = Mi_SQL.ToString();
                                             Comando.ExecuteNonQuery();

                                             Mi_SQL = new StringBuilder();
                                             Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + " SET ");
                                             Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + "=" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + " - ");
                                             Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"]);
                                             Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud);
                                             Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio + "");
                                             Comando.CommandText = Mi_SQL.ToString();
                                             Comando.ExecuteNonQuery();
                                         }
                                         else
                                         {
                                             if (Dr_Mov["TIPO_OPERACION"].ToString().Trim().Equals("TRASPASO") || Dr_Mov["TIPO_OPERACION"].ToString().Trim().Equals("REDUCCION"))
                                             {
                                                 if (Dr_Mov["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                                                 {
                                                     Mi_SQL = new StringBuilder();
                                                     Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " SET ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ",0) - ");
                                                     Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) + ");
                                                     Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) + " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) + " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) + " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) + " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) + " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) + " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) + " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) + " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) + " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) + " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) + " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) + " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ",0) - " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ",0) - " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ",0) - " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ",0) - " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ",0) - " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ",0) - " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ",0) - " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ",0) - " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ",0) - " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ",0) - " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ",0) - " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                     Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ",0) - " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", ""));
                                                     Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                                     Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                                     Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                                     Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                                     Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                                     Comando.CommandText = Mi_SQL.ToString();
                                                     Comando.ExecuteNonQuery();

                                                     Mi_SQL = new StringBuilder();
                                                     Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + " SET ");
                                                     Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + "=ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0) - ");
                                                     Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"]);
                                                     Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud);
                                                     Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio + "");
                                                     Comando.CommandText = Mi_SQL.ToString();
                                                     Comando.ExecuteNonQuery();
                                                 }
                                             }
                                             else if (Dr_Mov["TIPO_OPERACION"].ToString().Trim().Equals("AMPLIACION"))
                                             {
                                                 Mi_SQL = new StringBuilder();
                                                 Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + " SET ");
                                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + "=" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + " - ");
                                                 Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"]);
                                                 Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud);
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio + "");
                                                 Comando.CommandText = Mi_SQL.ToString();
                                                 Comando.ExecuteNonQuery();
                                             }
                                         }
                                     }
                                 }
                             }
                             else
                             {
                                 Mi_SQL = new StringBuilder();
                                 Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + " SET ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + "='" + Datos.P_Estatus.Trim() + "', ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + "=GETDATE() ");
                                 Mi_SQL.Append("WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                                 Comando.CommandText = Mi_SQL.ToString();
                                 Comando.ExecuteNonQuery();
                             }
                         }
                         
                         //BUSCAMOS SI HAY SOLICITUDES EN LA BANDEJA DE RECEPCION DE DOCUMENTOS
                         Mi_SQL = new StringBuilder();
                         Mi_SQL.Append("SELECT ISNULL(SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + ",0)),0) AS TOTAL");
                         Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud);
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + " = 'GENERADO'");
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + " = " + Datos.P_Anio.Trim());

                         Comando.CommandText = Mi_SQL.ToString();
                         Total_Psp = Comando.ExecuteScalar();

                         if(Convert.IsDBNull(Total_Psp))
                         {
                            Total = 0.00;
                         }
                         else
                         {
                             Total = Convert.ToDouble(String.IsNullOrEmpty(Total_Psp.ToString().Trim()) ? "0" : Total_Psp.ToString().Trim());
                         }

                         if (Total > 0)
                         {
                                //insertamos los datos de la nueva modificacion
                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("INSERT INTO " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "(");
                                Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + ", ");
                                Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Anio + ", ");
                                Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Estatus + ", ");
                                Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ", ");
                                Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Usuario_Creo + ", ");
                                Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Fecha_Creo + ") ");
                                Mi_SQL.Append(" VALUES(" + No_Movimiento_Egr + ", ");
                                Mi_SQL.Append(Anio + ", 'GENERADO', ");
                                Mi_SQL.Append(Total.ToString().Trim() + ", ");
                                Mi_SQL.Append("'" + Datos.P_Usuario_Creo + "', GETDATE())");

                                Comando.CommandText = Mi_SQL.ToString();
                                Comando.ExecuteNonQuery();

                                // MODIFICAMOS LAS SOLICITUDES QUE SE ENCUENTRAN EN RECEPCION DE DOCUMENTOS CON EL NUEVO NUMERO DE MODIFICACION
                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + " SET ");
                                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + No_Movimiento_Egr.ToString().Trim() + ", ");
                                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                                Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + "=GETDATE() ");
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim());
                                Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + " = 'GENERADO'");
                                Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + " = " + Datos.P_Anio.Trim());

                                Comando.CommandText = Mi_SQL.ToString();
                                Comando.ExecuteNonQuery();

                                //MODIFICAMOS EL MONTO MODIFICADO DE LA MODIFICACION A CERRAR
                                Mi_SQL = new StringBuilder();
                                Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                                Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + " = ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0)");
                                Mi_SQL.Append(" - " + Total.ToString().Trim());
                                Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio);
                                Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim());

                                Comando.CommandText = Mi_SQL.ToString();
                                Comando.ExecuteNonQuery();
                         }
                     }
                     else if (Datos.P_Estatus.Trim().Equals("RECIBIDO"))
                     {
                         Mi_SQL = new StringBuilder();
                         Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                         Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Egr.Campo_Estatus + " = 'GENERADO', ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Fecha_Modifico + " = GETDATE()");
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim());
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Datos.P_Anio.Trim());
                         Comando.CommandText = Mi_SQL.ToString();
                         Comando.ExecuteNonQuery();

                         foreach (DataRow Dr in Dt_Movimientos.Rows)
                         {
                             Mi_SQL = new StringBuilder();
                             Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + " SET ");
                             Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + "='" + Dr["ESTATUS"].ToString().Trim() + "', ");
                             Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                             Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + "=GETDATE() ");
                             Mi_SQL.Append("WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                             Comando.CommandText = Mi_SQL.ToString();
                             Comando.ExecuteNonQuery();
                         }
                     }
                 }

                 if (Datos.P_Estatus.Trim().Equals("REABRIR"))
                 {
                     Int32 No_Mod;
                     //VERIFICAMOS SI HAY OTRA MODIFICACION ABIERTA Y OBTENEMOS EL NUMERO
                     Mi_SQL = new StringBuilder();
                     Mi_SQL.Append("SELECT " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                     Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                     Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Estatus + " = 'GENERADO'");
                     Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Datos.P_Anio.Trim());

                     Comando.CommandText = Mi_SQL.ToString();
                     No_Mod = Convert.ToInt32(Comando.ExecuteScalar());

                     if (No_Mod > 0)
                     {
                         //LOS MOVIMIENTOS DE EGRESOS DE LA MODIFICACION NUEVA LOS CAMBIAMOS POR EL NUMERO DE MODIFICACION A REABRIR
                         Mi_SQL = new StringBuilder();
                         Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + " SET ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim() + ", ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + " = 'GENERADO', ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + " = GETDATE() ");
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + No_Mod.ToString().Trim());
                         Mi_SQL.Append(" AND  " + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + " = " + Datos.P_Anio.Trim());

                         Comando.CommandText = Mi_SQL.ToString();
                         Comando.ExecuteNonQuery();

                         //LOS MOVIMIENTOS DE EGRESOS DE RAMO 33 DE LA MODIFICACION NUEVA LOS CAMBIAMOS POR EL NUMERO DE MODIFICACION A REABRIR
                         Mi_SQL = new StringBuilder();
                         Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + " SET ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim() + ", ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + " = GETDATE() ");
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + No_Mod.ToString().Trim());
                         Mi_SQL.Append(" AND  " + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + " = " + Datos.P_Anio.Trim());
                         Mi_SQL.Append(" AND  " + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + " = 'AUTORIZADO'");

                         Comando.CommandText = Mi_SQL.ToString();
                         Comando.ExecuteNonQuery();

                         //LOS MOVIMIENTOS DE INGRESOS DE LA MODIFICACION NUEVA LOS CAMBIAMOS POR EL NUMERO DE MODIFICACION A REABRIR
                         Mi_SQL = new StringBuilder();
                         Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + " SET ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim() + ", ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + " = GETDATE() ");
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + No_Mod.ToString().Trim());
                         Mi_SQL.Append(" AND  " + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + " = " + Datos.P_Anio.Trim());

                         Comando.CommandText = Mi_SQL.ToString();
                         Comando.ExecuteNonQuery();

                         //MODIFICAMOS EL ESTATUS DE LA MODIFICACION A REABRIR A GENERADA DE VUELTA
                         Mi_SQL = new StringBuilder();
                         Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                         Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Egr.Campo_Estatus + " = 'GENERADO', ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + " = ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0) + ");
                         Mi_SQL.Append(" (SELECT ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0)");
                         Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + No_Mod.ToString().Trim());
                         Mi_SQL.Append(" AND  " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Datos.P_Anio.Trim() + "),");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Fecha_Modifico + " = GETDATE()");
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim());
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Datos.P_Anio.Trim());
                         Comando.CommandText = Mi_SQL.ToString();
                         Comando.ExecuteNonQuery();


                         //Mi_SQL = new StringBuilder();
                         //Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                         //Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Egr.Campo_Estatus + " = 'GENERADO', ");
                         //Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + " = ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0) + ");
                         //Mi_SQL.Append(" (SELECT ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0)");
                         //Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                         //Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + No_Mod.ToString().Trim());
                         //Mi_SQL.Append(" AND  " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Datos.P_Anio.Trim() + "),");
                         //Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                         //Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Fecha_Modifico + " = GETDATE()");
                         //Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim());
                         //Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Datos.P_Anio.Trim());
                         //Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Estatus + " IS NOT NULL ");
                         //Comando.CommandText = Mi_SQL.ToString();
                         //Comando.ExecuteNonQuery();

                         //ELIMINAMOS LA MODIFICACION NUEVA
                         Mi_SQL = new StringBuilder();
                         Mi_SQL.Append("DELETE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + No_Mod.ToString().Trim());
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Datos.P_Anio.Trim());
                         Comando.CommandText = Mi_SQL.ToString();
                         Comando.ExecuteNonQuery();

                     }
                     else
                     {
                         //MODIFICAMOS EL ESTATUS DE LA MODIFICACION A REABRIR A GENERADA DE VUELTA
                         Mi_SQL = new StringBuilder();
                         Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                         Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Egr.Campo_Estatus + " = 'GENERADO', ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Campo_Fecha_Modifico + " = GETDATE()");
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim());
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Datos.P_Anio.Trim());
                         Comando.CommandText = Mi_SQL.ToString();
                         Comando.ExecuteNonQuery();
                     }

                     //LOS MOVIMIENTOS DEL LA MODIFICACION Q REABRIREMOS LOS CAMBIAMOS A ESTATUS ACEPTADO
                     Mi_SQL = new StringBuilder();
                     Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                     Mi_SQL.Append(" SET " + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + " = 'ACEPTADO' ");
                     Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim());
                     Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + " = " + Datos.P_Anio.Trim());
                     Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + " = 'PREAUTORIZADO'");
                     Comando.CommandText = Mi_SQL.ToString();
                     Comando.ExecuteNonQuery();
                 }

                 Autorizados = "SI";
                 Transaccion.Commit();

                 Conexion.Close();

             }
             catch (SqlException Ex)
             {
                Transaccion.Rollback();
                Mensaje = "Error:  [" + Ex.Message + "]";
                throw new Exception(Mensaje, Ex);
             }
             finally
             {
                 Conexion.Close();
                 Comando = null;
                 Conexion = null;
                 Transaccion = null;
             }
             return Autorizados;
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Obtener_Coordinador_UR
         ///DESCRIPCIÓN          : consulta para obtener los datos del coordinador de la unidad responsable
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 27/Junio/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Obtener_Coordinador_UR(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {
             StringBuilder Mi_SQL = new StringBuilder();
             try
             {
                 Mi_SQL.Append("SELECT " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID + ", ");
                 Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + " + ' ' + ");
                 Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + " + ' ' + ");
                 Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + " AS NOMBRE, ");
                 Mi_SQL.Append("ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Correo_Electronico + ",'') AS EMAIL, ");
                 Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS UR, ");
                 Mi_SQL.Append(Cat_Puestos.Tabla_Cat_Puestos + "." + Cat_Puestos.Campo_Nombre + " AS TIPO, ");
                 Mi_SQL.Append(" 'USUARIO' AS CLAVE ");
                 Mi_SQL.Append(" FROM " + Cat_Empleados.Tabla_Cat_Empleados);
                 Mi_SQL.Append(" INNER JOIN " + Cat_Puestos.Tabla_Cat_Puestos);
                 Mi_SQL.Append(" ON " + Cat_Puestos.Tabla_Cat_Puestos + "." + Cat_Puestos.Campo_Puesto_ID);
                 Mi_SQL.Append(" = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Puesto_ID);
                 Mi_SQL.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_SQL.Append(" ON " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Dependencia_ID);
                 Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_SQL.Append(" WHERE " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);
                 Mi_SQL.Append(" = '" + Datos.P_Busqueda.Trim() + "'");

                 Mi_SQL.Append(" UNION ");

                 Mi_SQL.Append("SELECT " + Cat_Organigrama.Tabla_Cat_Organigrama + "." + Cat_Organigrama.Campo_Empleado_ID + ", ");
                 Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + " + ' ' + ");
                 Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + " + ' ' + ");
                 Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + " AS NOMBRE, ");
                 Mi_SQL.Append("ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Correo_Electronico + ",'') AS EMAIL, ");
                 Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS UR, ");
                 Mi_SQL.Append(Cat_Organigrama.Tabla_Cat_Organigrama + "." + Cat_Organigrama.Campo_Tipo + ", ");
                 Mi_SQL.Append(" 'COORDINADOR PSP' AS CLAVE ");
                 Mi_SQL.Append(" FROM " + Cat_Organigrama.Tabla_Cat_Organigrama);
                 Mi_SQL.Append(" INNER JOIN " + Cat_Empleados.Tabla_Cat_Empleados);
                 Mi_SQL.Append(" ON " + Cat_Organigrama.Tabla_Cat_Organigrama + "." + Cat_Organigrama.Campo_Empleado_ID);
                 Mi_SQL.Append(" = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);
                 Mi_SQL.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_SQL.Append(" ON " + Cat_Organigrama.Tabla_Cat_Organigrama + "." + Cat_Organigrama.Campo_Dependencia_ID);
                 Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_SQL.Append(" WHERE " + Cat_Organigrama.Tabla_Cat_Organigrama + "." + Cat_Organigrama.Campo_Tipo);
                 Mi_SQL.Append(" = 'COORDINADOR PRESUPUESTOS'");

                 Mi_SQL.Append(" UNION ");
                
                 Mi_SQL.Append("SELECT " + Cat_Organigrama.Tabla_Cat_Organigrama + "." + Cat_Organigrama.Campo_Empleado_ID + ", ");
                 Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + " + ' ' + ");
                 Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + " + ' ' + ");
                 Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + " AS NOMBRE, ");
                 Mi_SQL.Append("ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Correo_Electronico + ",'') AS EMAIL, ");
                 Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS UR, ");
                 Mi_SQL.Append(Cat_Organigrama.Tabla_Cat_Organigrama + "." + Cat_Organigrama.Campo_Tipo + ", ");
                 Mi_SQL.Append(" 'OTROS' AS CLAVE ");
                 Mi_SQL.Append(" FROM " + Cat_Organigrama.Tabla_Cat_Organigrama);
                 Mi_SQL.Append(" INNER JOIN " + Cat_Empleados.Tabla_Cat_Empleados);
                 Mi_SQL.Append(" ON " + Cat_Organigrama.Tabla_Cat_Organigrama + "." + Cat_Organigrama.Campo_Empleado_ID);
                 Mi_SQL.Append(" = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);
                 Mi_SQL.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_SQL.Append(" ON " + Cat_Organigrama.Tabla_Cat_Organigrama + "." + Cat_Organigrama.Campo_Dependencia_ID);
                 Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_SQL.Append(" WHERE " + Cat_Organigrama.Tabla_Cat_Organigrama + "." + Cat_Organigrama.Campo_Dependencia_ID);
                 Mi_SQL.Append(" = '" + Datos.P_Dependencia_ID_Busqueda.Trim() + "'");
                 Mi_SQL.Append(" AND " + Cat_Organigrama.Tabla_Cat_Organigrama + ".MODULO = 'NOMINA'");

                 Mi_SQL.Append(" ORDER BY " + Cat_Organigrama.Campo_Tipo + " ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Movimientos
         ///DESCRIPCIÓN          : consulta para obtener los datos de los movimientos
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 26/Mayo/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Consultar_Datos_Modificacion_Tipo(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {
             StringBuilder Mi_SQL = new StringBuilder();
             try
             {
                 Mi_SQL.Append("SELECT " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + ", ");
                 Mi_SQL.Append(" SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + ", 0)) AS IMPORTE, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + ", ");
                 Mi_SQL.Append(" CONVERT(VARCHAR," + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo + ", 101) AS FECHA_CREO, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Creo + ", ");
                 Mi_SQL.Append("'' AS UR, '' AS PP, '' AS FF, ");
                 Mi_SQL.Append("'T' + '' + ISNULL(CONVERT(VARCHAR," + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + "),0) + '-' + ");
                 Mi_SQL.Append(" CONVERT(VARCHAR," + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ") + '-' + ");
                 Mi_SQL.Append(" CONVERT(VARCHAR," + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + ") AS NO_SOLICITUD ");
                 Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);

                 if (!String.IsNullOrEmpty(Datos.P_Estatus.Trim()))
                 {
                     Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + " IN (" + Datos.P_Estatus.Trim() + ")");
                 }

                 if (!String.IsNullOrEmpty(Datos.P_Tipo_Egreso.Trim()))
                 {
                     if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                     {
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Egreso + " = '" + Datos.P_Tipo_Egreso + "'");
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Origen'");
                     }
                     else
                     {
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Egreso + " = '" + Datos.P_Tipo_Egreso + "'");
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Origen'");
                     }
                 }

                 if (!String.IsNullOrEmpty(Datos.P_No_Solicitud.Trim()))
                 {
                     if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                     {
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim());
                     }
                     else
                     {
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Solicitud.Trim());
                     }
                 }

                 if (!String.IsNullOrEmpty(Datos.P_Tipo_Solicitud.Trim()))
                 {
                     if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                     {
                         Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + " = '" + Datos.P_Tipo_Solicitud.Trim() + "'");
                     }
                     else
                     {
                         Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + " = '" + Datos.P_Tipo_Solicitud.Trim() + "'");
                     }
                 }

                 Mi_SQL.Append(" GROUP BY  " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + ", ");
                 Mi_SQL.Append(" CONVERT(VARCHAR," + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo + ", 101), ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Creo + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio);
                 Mi_SQL.Append(" ORDER BY " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " DESC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Programas_Unidades_Responsables
         ///DESCRIPCIÓN          : consulta para obtener los programas de una unidad responsable
         ///PARAMETROS           1 Negocio conexion con la capa de negocio 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 14/Noviembre/2011
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Consultar_Programa_Unidades_Responsables(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             DataTable Dt_Programas = new DataTable();
             try
             {
                 Mi_Sql.Append("SELECT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ", ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                 Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS NOMBRE, ");
                 Mi_Sql.Append(" 0 AS IMPORTE, ");
                 Mi_Sql.Append(" 'PROGRAMA' AS TIPO ");
                 Mi_Sql.Append(" FROM " +  Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                 Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Estatus + " = 'ACTIVO'");

                 if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                 {
                     Mi_Sql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                     Mi_Sql.Append(" = '" + Negocio.P_Programa_ID + "'");
                 }
                 Mi_Sql.Append(" ORDER BY NOMBRE ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0]; ;
             }
             catch (Exception Ex)
             {
                 Ex.ToString();
                 return null;
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Modif_Solicitud_Mov
         ///DESCRIPCIÓN          : consulta para obtener los programas de una unidad responsable
         ///PARAMETROS           1 Negocio conexion con la capa de negocio 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 09/Julio/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Consultar_Modif_Solicitud_Mov(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {

             StringBuilder MI_SQL = new StringBuilder();//Variable que almacenara la consulta.
             SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
             SqlConnection Conexion;//Variable para la conexión para la base de datos   
             SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
             String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
             DataTable Dt_Mov = new DataTable();
             Int32 No_Solicitud;
             String Estatus_Movimiento = String.Empty;
             DataTable Dt_Datos = new DataTable();
             DataRow Fila;
             Double Total_Solicitud = 0.00;
             DataTable Dt_Mov_Anteriores = new DataTable();
             DataTable Dt_Anexos = new DataTable();
             Int32 Anexo_ID;
             String Psp_Actualizado = String.Empty;
             Boolean Psp_Actualizados = false;
             String Capitulo = String.Empty;
             SqlDataAdapter Da_Datos = new SqlDataAdapter();
             DataSet Ds_Datos = new DataSet();
             String Solicitande = String.Empty;
             String Director = String.Empty;
             String Puesto_Solicitande = String.Empty;
             String Puesto_Director = String.Empty;

             Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
             Comando = new SqlCommand();
             Conexion.Open();
             Transaccion = Conexion.BeginTransaction();
             Comando.Transaction = Transaccion;
             Comando.Connection = Conexion;

             String Anio = String.Format("{0:yyyy}", DateTime.Now);

             try
             {
                 //obtenemos si el presupuesto es actualizado o no
                 Psp_Actualizados = Cls_Ope_Psp_Manejo_Presupuesto.Presupuesto_Actualizado(Comando);
                 if (Psp_Actualizados)
                 {
                     Psp_Actualizado = "SI";
                 }
                 else
                 {
                     Psp_Actualizado = "NO";
                 }

                 //obtenemos el total q descontaremos de las solicitudes a la modificacion
                 MI_SQL = new StringBuilder();
                 MI_SQL.Append("SELECT SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + ", 0))");
                 MI_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                 MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Datos.P_No_Solicitud);
                 MI_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + " = " + Anio);
                 MI_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Origen'");

                 Comando.CommandText = MI_SQL.ToString();

                 Total_Solicitud = Convert.ToDouble(Comando.ExecuteScalar());

                 Dt_Mov = Datos.P_Dt_Mov;

                 if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
                 {
                     //limpiamos las variables
                     Dt_Mov_Anteriores = new DataTable();
                     Ds_Datos = new DataSet();
                     Da_Datos = new SqlDataAdapter();

                     //OBTENEMOS LOS DATOS DE LA SOLICITUD PARA REGRESSAR EL COMPROMETIDO AL DISPONIBLE ANTES DE ELIMINAR
                     MI_SQL = new StringBuilder();
                     MI_SQL.Append("SELECT * FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                     MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Datos.P_No_Solicitud.Trim());

                     Comando.CommandText = MI_SQL.ToString().Trim();
                     Da_Datos = new SqlDataAdapter(Comando);
                     Da_Datos.Fill(Ds_Datos);

                     if (Ds_Datos != null)
                     {
                         Dt_Mov_Anteriores = Ds_Datos.Tables[0];
                     }

                     if (Dt_Mov_Anteriores != null && Dt_Mov_Anteriores.Rows.Count > 0)
                     {
                         if (!Dt_Mov_Anteriores.Rows[0]["ESTATUS"].ToString().Trim().Equals("CANCELADO"))
                         {
                             //MODIFICAMOS EL monto del modificado de egresos
                             MI_SQL = new StringBuilder();
                             MI_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                             MI_SQL.Append(" SET " + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + " = ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0)");
                             MI_SQL.Append(" - " + Total_Solicitud.ToString().Trim());
                             MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio);
                             MI_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Modificacion.Trim());

                             Comando.CommandText = MI_SQL.ToString();
                             Comando.ExecuteNonQuery();
                         }

                         foreach (DataRow Dr in Dt_Mov_Anteriores.Rows)
                         {
                             if (!Dr["ESTATUS"].ToString().Trim().Equals("CANCELADO"))
                             {
                                 if (Dr["TIPO_OPERACION"].ToString().Trim().Equals("REDUCCION") || Dr["TIPO_OPERACION"].ToString().Trim().Equals("TRASPASO"))
                                 {
                                     if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                                     {
                                         //COMPROMETEMOS EL RECURSO
                                         MI_SQL = new StringBuilder();
                                         MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " SET ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ",0) - ");
                                         MI_SQL.Append(Dr["IMPORTE_TOTAL"] + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) + ");
                                         MI_SQL.Append(Dr["IMPORTE_TOTAL"] + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) + " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) + " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) + " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) + " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) + " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) + " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) + " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) + " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) + " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) + " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) + " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) + " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ",0) - " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ",0) - " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ",0) - " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ",0) - " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ",0) - " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ",0) - " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ",0) - " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ",0) - " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ",0) - " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ",0) - " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ",0) - " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                         MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ",0) - " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", ""));
                                         MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                         MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                         MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                         MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                         MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Dr["ANIO"].ToString().Trim());
                                         Comando.CommandText = MI_SQL.ToString();
                                         Comando.ExecuteNonQuery();
                                     }
                                     else if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Destino") && Dr["TIPO_MOVIMIENTO"].ToString().Trim().Equals("Nueva"))
                                     {
                                         MI_SQL = new StringBuilder();
                                         MI_SQL.Append("DELETE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                         MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                         MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                         MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                         MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                         MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);
                                         Comando.CommandText = MI_SQL.ToString();
                                         Comando.ExecuteNonQuery();
                                     }
                                 }
                                 else
                                 {
                                     if (Dr["TIPO_MOVIMIENTO"].ToString().Trim().Equals("Nueva"))
                                     {
                                         //COMPROMETEMOS EL RECURSO
                                         MI_SQL = new StringBuilder();
                                         MI_SQL.Append("DELETE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                         MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                         MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                         MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                         MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                         MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);
                                         Comando.CommandText = MI_SQL.ToString();
                                         Comando.ExecuteNonQuery();
                                     }
                                 }
                             }
                         }
                     }

                     //eliminamos los anexos para no duplicar registros
                     MI_SQL = new StringBuilder();
                     MI_SQL.Append("DELETE " + Ope_Psp_Movimiento_Egr_Det_Doc.Tabla_Ope_Psp_Movimiento_Egr_Det_Doc);
                     MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Solicitud_ID + " = " + Datos.P_No_Solicitud.Trim());

                     Comando.CommandText = MI_SQL.ToString();
                     Comando.ExecuteNonQuery();

                     //eliminamos los datos de las solicitudes para no duplicar registros
                     MI_SQL = new StringBuilder();
                     MI_SQL.Append("DELETE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                     MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Datos.P_No_Solicitud.Trim());

                     Comando.CommandText = MI_SQL.ToString();
                     Comando.ExecuteNonQuery();

                     //MODIFICAMOS EL monto del modificado de egresos
                     MI_SQL = new StringBuilder();
                     MI_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                     MI_SQL.Append(" SET " + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + " = ISNULL(" + Ope_Psp_Movimiento_Egr.Campo_Total_Modificado + ",0)");
                     MI_SQL.Append(" + " + Datos.P_Total_Modificado.ToString().Trim().Replace(",", "").Replace("$", ""));
                     MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + Anio);
                     MI_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + Datos.P_No_Modificacion.Trim());

                     Comando.CommandText = MI_SQL.ToString();
                     Comando.ExecuteNonQuery();

                     No_Solicitud = Convert.ToInt32(Datos.P_No_Solicitud.Trim());

                     if (Datos.P_Estatus.Trim().Equals("CANCELADO"))
                     {
                         Datos.P_Estatus = "GENERADO";
                     }

                     //OBTENEMOS LOS DATOS DEL SOLICITANTE Y DEL DIRECTOR DEL AREA
                     if (Datos.P_Dt_Solicitante != null)
                     {
                         if (Datos.P_Dt_Solicitante.Rows.Count > 0)
                         {
                             Solicitande = Datos.P_Dt_Solicitante.Rows[0]["Solicitante"].ToString().Trim();
                             Director = Datos.P_Dt_Solicitante.Rows[0]["Dr"].ToString().Trim();
                             Puesto_Solicitande = Datos.P_Dt_Solicitante.Rows[0]["Puesto_Solicitante"].ToString().Trim();
                             Puesto_Director = Datos.P_Dt_Solicitante.Rows[0]["Puesto_Dr"].ToString().Trim();
                         }
                     }

                     //INSERTAMOS LOS DATOS DE LOS DETALLES
                     foreach (DataRow Dr in Dt_Mov.Rows)
                     {
                         MI_SQL = new StringBuilder();
                         MI_SQL.Append("INSERT INTO " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "(");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Movimiento_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Area_Funcional_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Anio + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Enero + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Febrero + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Marzo + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Abril + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Mayo + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Junio + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Julio + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Agosto + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Septiembre + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Octubre + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Noviembre + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Diciembre + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Aprobado + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Ampliacion + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Reduccion + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Modificado + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Egreso + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Movimiento + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Usuario + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Justificacion + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Solicitante + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Director + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Solicitante + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Director + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Creo + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo + ") ");
                         MI_SQL.Append(" VALUES(" + No_Solicitud + ", ");
                         MI_SQL.Append(Dr["MOVIMIENTO_ID"].ToString().Trim() + ", ");
                         MI_SQL.Append("'" + Dr["FF_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["UR_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["PROGRAMA_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["PARTIDA_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["AF_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append(Datos.P_No_Modificacion.Trim() + ", ");
                         MI_SQL.Append(Anio + ", ");
                         MI_SQL.Append("'" + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["APROBADO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["AMPLIACION"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["REDUCCION"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["MODIFICADO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Datos.P_Estatus.Trim() + "', ");
                         MI_SQL.Append("'" + Dr["TIPO_OPERACION"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["TIPO_PARTIDA"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["TIPO_EGRESO"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["TIPO_MOVIMIENTO"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Datos.P_Tipo_Usuario.Trim().ToUpper() + "', ");
                         MI_SQL.Append("'" + Dr["JUSTIFICACION"].ToString().Trim().ToUpper() + "', ");
                         if (!String.IsNullOrEmpty(Solicitande.Trim()))
                         {
                             MI_SQL.Append("'" + Solicitande.Trim() + "', ");
                         }
                         else
                         {
                             MI_SQL.Append("NULL, ");
                         }

                         if (!String.IsNullOrEmpty(Director.Trim()))
                         {
                             MI_SQL.Append("'" + Director.Trim() + "', ");
                         }
                         else
                         {
                             MI_SQL.Append("NULL, ");
                         }

                         if (!String.IsNullOrEmpty(Puesto_Solicitande.Trim()))
                         {
                             MI_SQL.Append("'" + Puesto_Solicitande.Trim() + "', ");
                         }
                         else
                         {
                             MI_SQL.Append("NULL, ");
                         }

                         if (!String.IsNullOrEmpty(Puesto_Director.Trim()))
                         {
                             MI_SQL.Append("'" + Puesto_Director.Trim() + "', ");
                         }
                         else
                         {
                             MI_SQL.Append("NULL, ");
                         }
                         MI_SQL.Append("'" + Datos.P_Usuario_Creo.Trim() + "', ");
                         MI_SQL.Append("GETDATE()) ");

                         Comando.CommandText = MI_SQL.ToString();
                         Comando.ExecuteNonQuery();

                         if (Dr["TIPO_MOVIMIENTO"].ToString().Trim().Equals("Nueva"))
                         {
                             Capitulo = String.Empty;

                             //obtenemos el capitulo de la partida
                             MI_SQL = new StringBuilder();
                             MI_SQL.Append("SELECT " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                             MI_SQL.Append(" FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                             MI_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                             MI_SQL.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                             MI_SQL.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                             MI_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                             MI_SQL.Append(" ON " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                             MI_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                             MI_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                             MI_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                             MI_SQL.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                             MI_SQL.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                             MI_SQL.Append(" = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");

                             Comando.CommandText = MI_SQL.ToString();
                             Capitulo = Comando.ExecuteScalar().ToString().Trim();

                             MI_SQL = new StringBuilder();
                             MI_SQL.Append("INSERT INTO " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                             MI_SQL.Append("(" + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Anio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Saldo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ", ");

                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Enero + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Febrero + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Marzo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Abril + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Mayo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Junio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Julio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Agosto + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Septiembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Octubre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Noviembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Diciembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Enero + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Febrero + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Marzo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Abril + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Mayo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Junio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Julio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Agosto + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Septiembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Octubre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Noviembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Diciembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Enero + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Febrero + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Marzo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Abril + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Mayo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Junio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Julio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Agosto + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Septiembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Octubre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Noviembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Diciembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Enero + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Febrero + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Marzo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Abril + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Mayo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Junio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Julio + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Agosto + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Septiembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Octubre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Noviembre + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Diciembre + ", ");

                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Creo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Creo + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado + ") VALUES( ");
                             MI_SQL.Append("'" + Dr["UR_ID"].ToString().Trim() + "', ");
                             MI_SQL.Append("'" + Dr["FF_ID"].ToString().Trim() + "', ");
                             MI_SQL.Append("'" + Dr["PROGRAMA_ID"].ToString().Trim() + "', ");
                             if (!String.IsNullOrEmpty(Dr["CAPITULO_ID"].ToString().Trim()))
                             {
                                 MI_SQL.Append("'" + Dr["CAPITULO_ID"].ToString().Trim() + "', ");
                             }
                             else
                             {
                                 if (!String.IsNullOrEmpty(Capitulo.Trim()))
                                 {
                                     MI_SQL.Append("'" + Capitulo.Trim() + "', ");
                                 }
                                 else
                                 {
                                     MI_SQL.Append("NULL, ");
                                 }
                             }
                             MI_SQL.Append("'" + Dr["PARTIDA_ID"].ToString().Trim() + "', ");
                             MI_SQL.Append("'" + Dr["AF_ID"].ToString().Trim() + "', ");
                             MI_SQL.Append(String.Format("{0:yyyy}", DateTime.Now) + ", ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");
                             MI_SQL.Append("0, ");

                             MI_SQL.Append("0.00, ");/*ene*/
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");/*dic*/
                             MI_SQL.Append("0.00, ");/*ene*/
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");/*dic*/
                             MI_SQL.Append("0.00, ");/*ene*/
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");/*dic*/
                             MI_SQL.Append("0.00, ");/*ene*/
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");/*dic*/
                             MI_SQL.Append("0.00, ");/*ene*/
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");/*dic*/
                             MI_SQL.Append("0.00, ");/*ene*/
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");
                             MI_SQL.Append("0.00, ");/*dic*/

                             MI_SQL.Append("'" + Cls_Sessiones.Nombre_Empleado + "', ");
                             MI_SQL.Append("GETDATE(), ");
                             MI_SQL.Append("'" + Psp_Actualizado.Trim() + "') ");

                             //Ejecutar consulta
                             Comando.CommandText = MI_SQL.ToString().Trim();
                             Comando.ExecuteNonQuery();
                         }
                         else
                         {
                             if (Dr["TIPO_OPERACION"].ToString().Trim().Equals("REDUCCION") || Dr["TIPO_OPERACION"].ToString().Trim().Equals("TRASPASO"))
                             {
                                 if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                                 {
                                     //COMPROMETEMOS EL RECURSO
                                     //PRE_COMPROMETEMOS EL RECURSO
                                     MI_SQL = new StringBuilder();
                                     MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                     MI_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = ");
                                     MI_SQL.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", 0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ");
                                     MI_SQL.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) - " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) - " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) - " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) - " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) - " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) - " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) - " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) - " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) - " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) - " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) - " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) - " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) - " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ",0) + " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ",0) + " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ",0) + " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ",0) + " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ",0) + " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ",0) + " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ",0) + " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ",0) + " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ",0) + " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ",0) + " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ",0) + " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ",0) + " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", ""));
                                     MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FF_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["UR_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);
                                     Comando.CommandText = MI_SQL.ToString();
                                     Comando.ExecuteNonQuery();
                                 }
                             }
                         }

                     }
                     Dt_Datos.Columns.Add("No_Movimiento");
                     Dt_Datos.Columns.Add("No_Solicitud");

                     Fila = Dt_Datos.NewRow();
                     Fila["No_Movimiento"] = Datos.P_No_Modificacion.Trim();
                     Fila["No_Solicitud"] = No_Solicitud.ToString();
                     Dt_Datos.Rows.Add(Fila);

                     Dt_Anexos = Datos.P_Dt_Anexos;
                     if (Dt_Anexos != null && Dt_Anexos.Rows.Count > 0)
                     {
                         //OBTENEMOS EL NUMERO DE ANEXO
                         MI_SQL = new StringBuilder();
                         MI_SQL.Append("SELECT ISNULL(MAX(" + Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Anexo_ID + "), 0)");
                         MI_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det_Doc.Tabla_Ope_Psp_Movimiento_Egr_Det_Doc);

                         Comando.CommandText = MI_SQL.ToString().Trim();

                         Anexo_ID = Convert.ToInt32(Comando.ExecuteScalar());
                         Anexo_ID = Anexo_ID + 1;

                         //INSERTAMOS LOS ANEXOS
                         foreach (DataRow Dr_Doc in Dt_Anexos.Rows)
                         {
                             MI_SQL = new StringBuilder();
                             MI_SQL.Append("INSERT INTO " + Ope_Psp_Movimiento_Egr_Det_Doc.Tabla_Ope_Psp_Movimiento_Egr_Det_Doc + "(");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Anexo_ID + ", ");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Nombre + ", ");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Ruta_Documento + ", ");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Extension + ", ");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Solicitud_ID + ", ");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Movimiento_ID + ", ");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Usuario_Creo + ", ");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Usuario_Modifico + ") VALUES( ");
                             MI_SQL.Append(Anexo_ID.ToString().Trim() + ", ");
                             MI_SQL.Append("'" + Dr_Doc["NOMBRE"].ToString().Trim() + "', ");
                             MI_SQL.Append("'" + Dr_Doc["RUTA_DOCUMENTO"].ToString().Trim() + "', ");
                             MI_SQL.Append("'" + Dr_Doc["EXTENSION"].ToString().Trim() + "', ");
                             MI_SQL.Append(No_Solicitud.ToString().Trim() + ", ");
                             MI_SQL.Append("1, ");
                             MI_SQL.Append("'" + Datos.P_Usuario_Creo + "', ");
                             MI_SQL.Append("GETDATE())");

                             Comando.CommandText = MI_SQL.ToString();
                             Comando.ExecuteNonQuery();
                             Anexo_ID++;
                         }
                     }

                     //

                 }

                 Transaccion.Commit();
             }
             catch (SqlException Ex)
             {
                 Transaccion.Rollback();
                 Mensaje = "Error:  [" + Ex.Message + "]";
                 throw new Exception(Mensaje, Ex);
             }
             finally
             {
                 Conexion.Close();
                 Comando = null;
                 Conexion = null;
                 Transaccion = null;
             }
             return Dt_Datos;
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Estatus_Modificacion
         ///DESCRIPCIÓN          : consulta para obtener los programas de una unidad responsable
         ///PARAMETROS           1 Negocio conexion con la capa de negocio 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 09/Julio/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Consultar_Estatus_Modificacion(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {
             StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
             DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
             DataTable Dt = new DataTable();
             try
             {
                 Mi_SQL.Append("SELECT " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Estatus + " AS ESTATUS, ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " AS NO_MODIFICACION ");
                 Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                 Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                 Mi_SQL.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr);
                 Mi_SQL.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                 Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio);
                 Mi_SQL.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                 Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID);
                 Mi_SQL.Append(" = " + Datos.P_No_Solicitud);
                 
                 Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                 if (Ds != null)
                 {
                     Dt = Ds.Tables[0];
                 }
             }
             catch (Exception Ex)
             {
                 String Mensaje = "Error al intentar consultar el estatus de la modificacion. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                 throw new Exception(Mensaje);
             }
             return Dt;
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Estatus_Mod_Sig
         ///DESCRIPCIÓN          : consulta para obtener el estatus de la modificacion siguiente para saber si se puede reabrir o no
         ///PARAMETROS           1 Negocio conexion con la capa de negocio 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 20/Julio/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static Boolean Consultar_Estatus_Mod_Sig(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {
             StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
             DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
             DataTable Dt = new DataTable();
             Int32 No_Movimiento_Egr;
             Boolean Datos_Validos = true;
             String Estatus = String.Empty;

             try
             {
                //obtenemos el id maximo de la tabla de movimientos de egresos
                Mi_SQL.Append("SELECT ISNULL(MAX(" + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + "), 0)");
                Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now));

                No_Movimiento_Egr = Convert.ToInt32(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()));

                //SI EL MOVIMIENTO ES CERO LE ASIGNAMOS EL NUMERO UNO Y COMO NO ES REPETIDO NUESTRA VARIABLE ES FALSE
                if (No_Movimiento_Egr == 0)
                {
                    No_Movimiento_Egr = 1;
                }

                if (No_Movimiento_Egr.ToString().Trim().Equals(Datos.P_No_Modificacion))
                {
                    if (No_Movimiento_Egr > 1)
                    {
                        if (Datos.P_Busqueda.Trim().Equals("ANTES"))
                        {
                            //consultamos si la modificacion anterios ya esta autorizada
                            //obtenemos el id maximo de la tabla de movimientos de egresos
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("SELECT " + Ope_Psp_Movimiento_Egr.Campo_Estatus );
                            Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now));
                            Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + (No_Movimiento_Egr - 1));

                            Estatus = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                            if (!String.IsNullOrEmpty(Estatus))
                            {
                                if (Estatus.Trim().Equals("PREAUTORIZADO"))
                                {
                                    Datos_Validos = false;
                                }
                            }
                        }
                    }
                }
                else 
                {
                    if (Datos.P_Busqueda.Trim().Equals("DESPUES"))
                    {
                        No_Movimiento_Egr = Convert.ToInt32(Datos.P_No_Modificacion);

                        //consultamos si la modificacion anterios ya esta autorizada
                        //obtenemos el id maximo de la tabla de movimientos de egresos
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("SELECT " + Ope_Psp_Movimiento_Egr.Campo_Estatus);
                        Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Campo_Anio + " = " + String.Format("{0:yyyy}", DateTime.Now));
                        Mi_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr + " = " + (No_Movimiento_Egr + 1));

                        Estatus = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                        if (!String.IsNullOrEmpty(Estatus))
                        {
                            if (Estatus.Trim().Equals("PREAUTORIZADO"))
                            {
                                Datos_Validos = false;
                            }
                        }
                    }
                }
             }
             catch (Exception Ex)
             {
                 String Mensaje = "Error al intentar consultar el estatus de la modificacion. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                 throw new Exception(Mensaje);
             }
             return Datos_Validos;
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Anexos
         ///DESCRIPCIÓN          : consulta para obtener los anexos de la solicitud
         ///PARAMETROS           1 Negocio conexion con la capa de negocio 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 06/Septiembre/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Consultar_Anexos(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {
             StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
             DataSet Ds = new DataSet(); //Dataset donde obtendremos los datos de la consulta
             DataTable Dt = new DataTable();

             try
             {
                 //obtenemos el id maximo de la tabla de movimientos de egresos
                 Mi_SQL.Append("SELECT " + Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Nombre + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Anexo_ID + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Ruta_Documento + ", ");
                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Extension);
                 Mi_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det_Doc.Tabla_Ope_Psp_Movimiento_Egr_Det_Doc);
                 Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Solicitud_ID);
                 Mi_SQL.Append(" = " + Datos.P_No_Solicitud.Trim());

                 Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                
                 if (Ds != null)
                 {
                     Dt = Ds.Tables[0];
                 }
             }
             catch (Exception Ex)
             {
                 String Mensaje = "Error al intentar consultar los anexos de la solicitud. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                 throw new Exception(Mensaje);
             }
             return Dt;
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_No_Solicitud
         ///DESCRIPCIÓN          : consulta para obtener el numero de solicitud
         ///PARAMETROS           :
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 06/Septiembre/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static Int32 Consultar_No_Solicitud() 
         {
             StringBuilder MI_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
             Int32 No_Solicitud;

             try
             {
                 //OBTENEMOS EL ID MAXIMO DE LA SOLICITUD
                 MI_SQL = new StringBuilder();
                 MI_SQL.Append("SELECT ISNULL(MAX(" + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + "), 0)");
                 MI_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);

                 No_Solicitud = Convert.ToInt32(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, MI_SQL.ToString()));
                 No_Solicitud += 1;
             }
             catch (Exception Ex)
             {
                 String Mensaje = "Error al intentar consultar el numero de solicitud. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                 throw new Exception(Mensaje);
             }
             return No_Solicitud;
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Convenio_Programa
         ///DESCRIPCIÓN          : consulta para obtener los datos del convenio de un programa
         ///PARAMETROS           :
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 19/Septiembre/2012
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Consultar_Convenio_Programa(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio)
         {
             StringBuilder MI_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
             DataTable Dt = new DataTable();
             DataSet Ds = new DataSet();
             try
             {
                 //MI_SQL.Append("SELECT ISNULL(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Convenio_ID + ", '') AS CONVENIO_ID, ");
                 //MI_SQL.Append(Cat_Con_Convenio.Tabla_Cat_Con_Convenio + "." + Cat_Con_Convenio.Campo_Cuenta_Banco_Convenio + ", ");
                 //MI_SQL.Append(Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Cuenta_Contable_ID + ", ");
                 //MI_SQL.Append(Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta);
                 //MI_SQL.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                 //MI_SQL.Append(" INNER JOIN " + Cat_Con_Convenio.Tabla_Cat_Con_Convenio);
                 //MI_SQL.Append(" ON " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Convenio_ID);
                 //MI_SQL.Append(" = " + Cat_Con_Convenio.Tabla_Cat_Con_Convenio + "." + Cat_Con_Convenio.Campo_Convenio_ID);
                 //MI_SQL.Append(" INNER JOIN " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos);
                 //MI_SQL.Append(" ON " + Cat_Con_Convenio.Tabla_Cat_Con_Convenio + "." + Cat_Con_Convenio.Campo_Cuenta_Banco_Convenio);
                 //MI_SQL.Append(" = " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Banco_ID);
                 //MI_SQL.Append(" INNER JOIN " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                 //MI_SQL.Append(" ON " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos + "." + Cat_Nom_Bancos.Campo_Cuenta_Contable_ID);
                 //MI_SQL.Append(" = " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables + "." + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID);
                 
                 if(!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                 {
                     MI_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                     MI_SQL.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                 }

                 Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MI_SQL.ToString());
                 
                 if(Ds != null)
                 {
                    Dt = Ds.Tables[0];
                 }
             }
             catch (Exception Ex)
             {
                 String Mensaje = "Error al intentar consultar el número de convenio Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                 throw new Exception(Mensaje);
             }
             return Dt;
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Ur_Programas_Psp
         ///DESCRIPCIÓN          : consulta para obtener los datos de las dependencias de los programas
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 19/Marzo/2013
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Ur_Programas_Psp(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT DISTINCT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS UR, ");
                 Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID );
                 Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                 Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                 Mi_Sql.Append(" = '" + Negocio.P_Anio + "'");

                 if (!String.IsNullOrEmpty(Negocio.P_Programa_ID))
                 {
                     Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                     Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                 }

                 if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID_Busqueda))
                 {
                     Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                     Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID_Busqueda.Trim() + "'");
                 }
                 

                 Mi_Sql.Append(" ORDER BY UR ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de los programas. Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Ur_Programas_Psp
         ///DESCRIPCIÓN          : consulta para obtener los datos de las dependencias de los programas
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 19/Marzo/2013
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Ur_Programas(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT DISTINCT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS UR, ");
                 Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                 Mi_Sql.Append(" FROM " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia);
                 Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 Mi_Sql.Append(" ON " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia + "." + Cat_SAP_Det_Prog_Dependencia.Campo_Dependencia_ID);
                 Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);

                 if (!String.IsNullOrEmpty(Negocio.P_Programa_ID))
                 {
                     if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                     {
                         Mi_Sql.Append(" AND " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia + "." + Cat_SAP_Det_Prog_Dependencia.Campo_Proyecto_Programa_ID);
                         Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                     }
                     else 
                     {
                         Mi_Sql.Append(" WHERE " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia + "." + Cat_SAP_Det_Prog_Dependencia.Campo_Proyecto_Programa_ID);
                         Mi_Sql.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                     }
                 }

                 if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID_Busqueda))
                 {
                     if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                     {
                         Mi_Sql.Append(" AND " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia + "." + Cat_SAP_Det_Prog_Dependencia.Campo_Dependencia_ID);
                         Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID_Busqueda.Trim() + "'");
                     }
                     else 
                     {
                         Mi_Sql.Append(" WHERE " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia + "." + Cat_SAP_Det_Prog_Dependencia.Campo_Dependencia_ID);
                         Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID_Busqueda.Trim() + "'");
                     }
                 }

                 Mi_Sql.Append(" ORDER BY UR ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de los programas. Error: [" + Ex.Message + "]");
             }
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Genera_Poliza_Autorizacion_Modificacion
         ///DESCRIPCIÓN          : metodo para generar la poliza grupal de las modificaciones autorizadas
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 08/Marzo/2013
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static String[] Genera_Poliza_Autorizacion_Modificacion(DataTable Dt_Movimientos, SqlCommand Comando)
         {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            SqlDataAdapter Da_Datos = new SqlDataAdapter(); //
            DataSet Ds_Datos = new DataSet();
            String[] Datos_Poliza_PSP = null;
            Double Ampliacion = 0.00;
            Double Reduccion = 0.00;
            Double Traspaso = 0.00;
            String Importes = String.Empty;
            DataTable Dt_Origen = new DataTable();
            DataTable Dt_Mov = new DataTable();
            String Solicitud_Id = String.Empty;

            try
            {
                //obtenemos las solicitudes autorizadas
                Dt_Origen = (from x in Dt_Movimientos.AsEnumerable()
                             where x.Field<String>("ESTATUS") == "AUTORIZADO"
                             select x).AsDataView().ToTable();

                if (Dt_Origen != null) 
                {
                    if (Dt_Origen.Rows.Count > 0)
                    {
                        //recorremos el datatable para obtener el numero de solicitud
                        foreach (DataRow Dr in Dt_Origen.Rows)
                        {
                            Solicitud_Id += Dr["SOLICITUD_ID"].ToString().Trim() + ",";
                        }

                        Solicitud_Id = Solicitud_Id.Substring(0, Solicitud_Id.Length - 1);

                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " IN (" + Solicitud_Id + ")");

                        Comando.CommandText = Mi_SQL.ToString();
                        Da_Datos = new SqlDataAdapter(Comando);
                        Da_Datos.Fill(Ds_Datos);

                        if (Ds_Datos != null)
                        {
                            Dt_Mov = Ds_Datos.Tables[0];
                        }

                        //obtenemos el monto de la ampliacion
                        Dt_Origen = new DataTable();
                        Dt_Origen = (from x in Dt_Mov.AsEnumerable()
                                     where x.Field<String>("TIPO_PARTIDA") == "Origen"
                                     && x.Field<String>("TIPO_OPERACION") == "AMPLIACION"
                                     select x).AsDataView().ToTable();

                        Ampliacion = Dt_Origen.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<Decimal>("IMPORTE_TOTAL").ToString()) ? "0" : x.Field<Decimal>("IMPORTE_TOTAL").ToString()));

                        //obtenemos el monto de la reduccion
                        Dt_Origen = new DataTable();
                        Dt_Origen = (from x in Dt_Mov.AsEnumerable()
                                     where x.Field<String>("TIPO_PARTIDA") == "Origen"
                                     && x.Field<String>("TIPO_OPERACION") == "REDUCCION"
                                     select x).AsDataView().ToTable();

                        Reduccion = Dt_Origen.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<Decimal>("IMPORTE_TOTAL").ToString()) ? "0" : x.Field<Decimal>("IMPORTE_TOTAL").ToString()));

                        //obtenemos el monto del traspaso
                        Dt_Origen = new DataTable();
                        Dt_Origen = (from x in Dt_Mov.AsEnumerable()
                                     where x.Field<String>("TIPO_PARTIDA") == "Origen"
                                     && x.Field<String>("TIPO_OPERACION") == "TRASPASO"
                                     select x).AsDataView().ToTable();

                        Traspaso = Dt_Origen.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<Decimal>("IMPORTE_TOTAL").ToString()) ? "0" : x.Field<Decimal>("IMPORTE_TOTAL").ToString()));

                        //Juntamos los importes de la ampliacion, reduccion y reduccion interna;
                        Importes = Ampliacion.ToString().Trim() + ";" + Reduccion.ToString().Trim() + ";" + Traspaso.ToString().Trim();

                        //creamos la poliza presupuestal
                        Datos_Poliza_PSP = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimientos_Presupuestales(Importes, Ope_Psp_Presupuesto_Aprobado.Campo_Modificado,
                           Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, 0.00, "", "", "", "", Comando);

                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar la poliza de autorización de las modificaciones al presupuesto. Error[" + Ex.Message + "]");
            }
            return Datos_Poliza_PSP;
        }

        #endregion

        #region (Adecuaciones)
         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Alta_Adecuaciones
         ///DESCRIPCIÓN          : consulta para guardar los datos de las adecuaciones al presupuestos
         ///PARAMETROS           1 Negocio conexion con la capa de negocio 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 11/Febrero/2013
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Alta_Adecuaciones(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {

             StringBuilder MI_SQL = new StringBuilder();//Variable que almacenara la consulta.
             SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
             SqlConnection Conexion;//Variable para la conexión para la base de datos   
             SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
             String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
             DataTable Dt_Mov = new DataTable();
             Int32 No_Solicitud;
             String Estatus_Movimiento = String.Empty;
             DataTable Dt_Datos = new DataTable();
             DataRow Fila;
             DataTable Dt_Anexos = new DataTable();
             Int32 Anexo_ID;

             String Solicitande = String.Empty;
             String Director = String.Empty;
             String Puesto_Solicitande = String.Empty;
             String Puesto_Director = String.Empty;

             Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
             Comando = new SqlCommand();
             Conexion.Open();
             Transaccion = Conexion.BeginTransaction();
             Comando.Transaction = Transaccion;
             Comando.Connection = Conexion;

             String Anio = String.Format("{0:yyyy}", DateTime.Now);

             try
             {
                 //OBTENEMOS LOS DATOS DEL SOLICITANTE Y DEL DIRECTOR DEL AREA
                 if (Datos.P_Dt_Solicitante != null)
                 {
                     if (Datos.P_Dt_Solicitante.Rows.Count > 0)
                     {
                         Solicitande = Datos.P_Dt_Solicitante.Rows[0]["Solicitante"].ToString().Trim();
                         Director = Datos.P_Dt_Solicitante.Rows[0]["Dr"].ToString().Trim();
                         Puesto_Solicitande = Datos.P_Dt_Solicitante.Rows[0]["Puesto_Solicitante"].ToString().Trim();
                         Puesto_Director = Datos.P_Dt_Solicitante.Rows[0]["Puesto_Dr"].ToString().Trim();
                     }
                 }

                 Dt_Mov = Datos.P_Dt_Mov;

                 if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
                 {
                     No_Solicitud = Convert.ToInt32(Datos.P_No_Solicitud.Trim());

                     //INSERTAMOS LOS DATOS DE LOS DETALLES
                     foreach (DataRow Dr in Dt_Mov.Rows)
                     {
                         MI_SQL = new StringBuilder();
                         MI_SQL.Append("INSERT INTO " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "(");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Movimiento_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Area_Funcional_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Anio + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Enero + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Febrero + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Marzo + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Abril + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Mayo + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Junio + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Julio + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Agosto + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Septiembre + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Octubre + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Noviembre + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Diciembre + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Aprobado + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Ampliacion + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Reduccion + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Modificado + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Egreso + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Movimiento + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Usuario + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Justificacion + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Solicitante + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Director + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Solicitante + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Director + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Creo + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo + ") ");
                         MI_SQL.Append(" VALUES(" + No_Solicitud + ", ");
                         MI_SQL.Append(Dr["MOVIMIENTO_ID"].ToString().Trim() + ", ");
                         MI_SQL.Append("'" + Dr["FF_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["UR_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["PROGRAMA_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["PARTIDA_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["AF_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append("NULL, ");
                         MI_SQL.Append(Anio + ", ");
                         MI_SQL.Append("'" + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["APROBADO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["AMPLIACION"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["REDUCCION"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["MODIFICADO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["ESTATUS"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["TIPO_OPERACION"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["TIPO_PARTIDA"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["TIPO_EGRESO"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["TIPO_MOVIMIENTO"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Datos.P_Tipo_Usuario.Trim().ToUpper() + "', ");
                         MI_SQL.Append("'" + Dr["JUSTIFICACION"].ToString().Trim().ToUpper() + "', ");

                         if (!String.IsNullOrEmpty(Solicitande.Trim()))
                         {
                             MI_SQL.Append("'" + Solicitande.Trim() + "', ");
                         }
                         else 
                         {
                             MI_SQL.Append("NULL, ");
                         }

                         if (!String.IsNullOrEmpty(Director.Trim()))
                         {
                             MI_SQL.Append("'" + Director.Trim() + "', ");
                         }
                         else
                         {
                             MI_SQL.Append("NULL, ");
                         }

                         if (!String.IsNullOrEmpty(Puesto_Solicitande.Trim()))
                         {
                             MI_SQL.Append("'" + Puesto_Solicitande.Trim() + "', ");
                         }
                         else
                         {
                             MI_SQL.Append("NULL, ");
                         }

                         if (!String.IsNullOrEmpty(Puesto_Director.Trim()))
                         {
                             MI_SQL.Append("'" + Puesto_Director.Trim() + "', ");
                         }
                         else
                         {
                             MI_SQL.Append("NULL, ");
                         }

                         MI_SQL.Append("'" + Datos.P_Usuario_Creo.Trim() + "', ");
                         MI_SQL.Append("GETDATE()) ");

                         Comando.CommandText = MI_SQL.ToString();
                         Comando.ExecuteNonQuery();

                         if (Datos.P_Tipo_Usuario.Trim().Equals("Ramo33"))
                         {
                             if (Dr["TIPO_OPERACION"].ToString().Trim().Equals("ADECUACION"))
                             {
                                 if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                                 {
                                     MI_SQL = new StringBuilder();
                                     MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                     MI_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) - " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(" " + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ",0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ",0) - " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ",0) - " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ",0) - " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ",0) - " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ",0) - " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ",0) - " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ",0) - " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ",0) - " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ",0) - " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ",0) - " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ",0) - " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ",0) - " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ",0) - " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) - " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) - " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) - " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) - " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) - " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) - " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) - " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) - " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) - " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) - " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) - " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) - " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                     MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["UR_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FF_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                     Comando.CommandText = MI_SQL.ToString();
                                     Comando.ExecuteNonQuery();
                                 }
                                 else
                                 {
                                     MI_SQL = new StringBuilder();
                                     MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                     MI_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(" AMPLIACION = AMPLIACION,0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ",0) + " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ",0) + " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ",0) + " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ",0) + " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ",0) + " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ",0) + " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ",0) + " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ",0) + " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ",0) + " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ",0) + " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ",0) + " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ",0) + " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ",0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) + " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) + " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) + " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) + " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) + " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) + " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) + " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) + " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) + " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) + " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) + " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) + " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                     MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["UR_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FF_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                     Comando.CommandText = MI_SQL.ToString();
                                     Comando.ExecuteNonQuery();
                                 }
                                 MI_SQL = new StringBuilder();
                                 MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                 MI_SQL.Append(" SET  MODIFICADO = ISNULL(APROBADO,0) + ISNULL(AMPLIACION,0) - ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ",0) ");
                                 MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["UR_ID"].ToString().Trim() + "'");
                                 MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FF_ID"].ToString().Trim() + "'");
                                 MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                                 MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                 MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                 Comando.CommandText = MI_SQL.ToString();
                                 Comando.ExecuteNonQuery();
                             }
                         }
                         else
                         {
                             if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                             {
                                 //PRE_COMPROMETEMOS EL RECURSO
                                 MI_SQL = new StringBuilder();
                                 MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                 MI_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = ");
                                 MI_SQL.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", 0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ");
                                 MI_SQL.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) - " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) - " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) - " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) - " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) - " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) - " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) - " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) - " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) - " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) - " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) - " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) - " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) - " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ",0) + " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ",0) + " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ",0) + " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ",0) + " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ",0) + " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ",0) + " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ",0) + " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ",0) + " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ",0) + " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ",0) + " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ",0) + " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                 MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ",0) + " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", ""));
                                 MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FF_ID"].ToString().Trim() + "'");
                                 MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["UR_ID"].ToString().Trim() + "'");
                                 MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                                 MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                 MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);
                                 Comando.CommandText = MI_SQL.ToString();
                                 Comando.ExecuteNonQuery();
                             }
                         }

                         //insertamos los anexos del movimiento
                         Dt_Anexos = Datos.P_Dt_Anexos;
                         if (Dt_Anexos != null && Dt_Anexos.Rows.Count > 0)
                         {
                             //OBTENEMOS EL NUMERO DE ANEXO
                             MI_SQL = new StringBuilder();
                             MI_SQL.Append("SELECT ISNULL(MAX(" + Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Anexo_ID + "), 0)");
                             MI_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det_Doc.Tabla_Ope_Psp_Movimiento_Egr_Det_Doc);

                             Comando.CommandText = MI_SQL.ToString().Trim();

                             Anexo_ID = Convert.ToInt32(Comando.ExecuteScalar());
                             Anexo_ID = Anexo_ID + 1;

                             //INSERTAMOS LOS ANEXOS
                             foreach (DataRow Dr_Doc in Dt_Anexos.Rows)
                             {
                                 MI_SQL = new StringBuilder();
                                 MI_SQL.Append("INSERT INTO " + Ope_Psp_Movimiento_Egr_Det_Doc.Tabla_Ope_Psp_Movimiento_Egr_Det_Doc + "(");
                                 MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Anexo_ID + ", ");
                                 MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Nombre + ", ");
                                 MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Ruta_Documento + ", ");
                                 MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Extension + ", ");
                                 MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Solicitud_ID + ", ");
                                 MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Movimiento_ID + ", ");
                                 MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Usuario_Creo + ", ");
                                 MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Usuario_Modifico + ") VALUES( ");
                                 MI_SQL.Append(Anexo_ID.ToString().Trim() + ", ");
                                 MI_SQL.Append("'" + Dr_Doc["NOMBRE"].ToString().Trim() + "', ");
                                 MI_SQL.Append("'" + Dr_Doc["RUTA_DOCUMENTO"].ToString().Trim() + "', ");
                                 MI_SQL.Append("'" + Dr_Doc["EXTENSION"].ToString().Trim() + "', ");
                                 MI_SQL.Append(No_Solicitud.ToString().Trim() + ", ");
                                 MI_SQL.Append("1, ");
                                 MI_SQL.Append("'" + Datos.P_Usuario_Creo + "', ");
                                 MI_SQL.Append("GETDATE())");

                                 Comando.CommandText = MI_SQL.ToString();
                                 Comando.ExecuteNonQuery();
                                 Anexo_ID++;
                             }
                         }
                     }

                     Dt_Datos.Columns.Add("No_Movimiento");
                     Dt_Datos.Columns.Add("No_Solicitud");

                     Fila = Dt_Datos.NewRow();
                     Fila["No_Movimiento"] = String.Empty;
                     Fila["No_Solicitud"] = No_Solicitud.ToString();

                     Dt_Datos.Rows.Add(Fila);
                 }
                 Transaccion.Commit();
             }
             catch (SqlException Ex)
             {
                 Transaccion.Rollback();

                 Mensaje = "Error:  [" + Ex.Message + "]";

                 throw new Exception(Mensaje, Ex);
             }
             finally
             {
                 Conexion.Close();
                 Comando = null;
                 Conexion = null;
                 Transaccion = null;
             }
             return Dt_Datos;
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Modificar_Adecuaciones
         ///DESCRIPCIÓN          : consulta para modificar los datos de las adecuaciones del presupuesto
         ///PARAMETROS           1 Negocio conexion con la capa de negocio 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 12/Febrero/2013
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Modificar_Adecuaciones(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {

             StringBuilder MI_SQL = new StringBuilder();//Variable que almacenara la consulta.
             SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
             SqlConnection Conexion;//Variable para la conexión para la base de datos   
             SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
             String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
             DataTable Dt_Mov = new DataTable();
             Int32 No_Solicitud;
             String Estatus_Movimiento = String.Empty;
             DataTable Dt_Datos = new DataTable();
             DataRow Fila;
             Double Total_Solicitud = 0.00;
             DataTable Dt_Mov_Anteriores = new DataTable();
             DataTable Dt_Anexos = new DataTable();
             Int32 Anexo_ID;
             String Capitulo = String.Empty;
             SqlDataAdapter Da_Datos = new SqlDataAdapter();
             DataSet Ds_Datos = new DataSet();

             String Solicitande = String.Empty;
             String Director = String.Empty;
             String Puesto_Solicitande = String.Empty;
             String Puesto_Director = String.Empty;

             Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
             Comando = new SqlCommand();
             Conexion.Open();
             Transaccion = Conexion.BeginTransaction();
             Comando.Transaction = Transaccion;
             Comando.Connection = Conexion;

             String Anio = String.Format("{0:yyyy}", DateTime.Now);

             try
             {
                 //obtenemos el total q descontaremos de las solicitudes a la modificacion
                 MI_SQL = new StringBuilder();
                 MI_SQL.Append("SELECT SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + ", 0))");
                 MI_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                 MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Datos.P_No_Solicitud);
                 MI_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + " = " + Anio);
                 MI_SQL.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + " = 'Origen'");

                 Comando.CommandText = MI_SQL.ToString();

                 Total_Solicitud = Convert.ToDouble(Comando.ExecuteScalar());

                 Dt_Mov = Datos.P_Dt_Mov;

                 if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
                 {
                     //limpiamos las variables
                     Dt_Mov_Anteriores = new DataTable();
                     Ds_Datos = new DataSet();
                     Da_Datos = new SqlDataAdapter();

                     //OBTENEMOS LOS DATOS DE LA SOLICITUD PARA REGRESSAR EL COMPROMETIDO AL DISPONIBLE ANTES DE ELIMINAR
                     MI_SQL = new StringBuilder();
                     MI_SQL.Append("SELECT * FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                     MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Datos.P_No_Solicitud.Trim());

                     Comando.CommandText = MI_SQL.ToString().Trim();
                     Da_Datos = new SqlDataAdapter(Comando);
                     Da_Datos.Fill(Ds_Datos);

                     if (Ds_Datos != null)
                     {
                         Dt_Mov_Anteriores = Ds_Datos.Tables[0];
                     }

                     if (Dt_Mov_Anteriores != null && Dt_Mov_Anteriores.Rows.Count > 0)
                     {
                         foreach (DataRow Dr in Dt_Mov_Anteriores.Rows)
                         {
                             if (!Dr["ESTATUS"].ToString().Trim().Equals("CANCELADO"))
                             {
                                  if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                                 {
                                     //COMPROMETEMOS EL RECURSO
                                     MI_SQL = new StringBuilder();
                                     MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " SET ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ",0) - ");
                                     MI_SQL.Append(Dr["IMPORTE_TOTAL"] + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) + ");
                                     MI_SQL.Append(Dr["IMPORTE_TOTAL"] + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) + " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) + " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) + " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) + " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) + " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) + " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) + " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) + " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) + " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) + " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) + " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) + " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ",0) - " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ",0) - " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ",0) - " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ",0) - " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ",0) - " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ",0) - " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ",0) - " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ",0) - " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ",0) - " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ",0) - " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ",0) - " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                     MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ",0) - " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", ""));
                                     MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                                     MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Dr["ANIO"].ToString().Trim());
                                     Comando.CommandText = MI_SQL.ToString();
                                     Comando.ExecuteNonQuery();
                                 }
                             }
                         }
                     }

                     //eliminamos los anexos para no duplicar registros
                     MI_SQL = new StringBuilder();
                     MI_SQL.Append("DELETE " + Ope_Psp_Movimiento_Egr_Det_Doc.Tabla_Ope_Psp_Movimiento_Egr_Det_Doc);
                     MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Solicitud_ID + " = " + Datos.P_No_Solicitud.Trim());

                     Comando.CommandText = MI_SQL.ToString();
                     Comando.ExecuteNonQuery();

                     //eliminamos los datos de las solicitudes para no duplicar registros
                     MI_SQL = new StringBuilder();
                     MI_SQL.Append("DELETE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                     MI_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Datos.P_No_Solicitud.Trim());

                     Comando.CommandText = MI_SQL.ToString();
                     Comando.ExecuteNonQuery();

                     No_Solicitud = Convert.ToInt32(Datos.P_No_Solicitud.Trim());

                     if (Datos.P_Estatus.Trim().Equals("CANCELADO"))
                     {
                         Datos.P_Estatus = "GENERADO";
                     }

                     //OBTENEMOS LOS DATOS DEL SOLICITANTE Y DEL DIRECTOR DEL AREA
                     if (Datos.P_Dt_Solicitante != null)
                     {
                         if (Datos.P_Dt_Solicitante.Rows.Count > 0)
                         {
                             Solicitande = Datos.P_Dt_Solicitante.Rows[0]["Solicitante"].ToString().Trim();
                             Director = Datos.P_Dt_Solicitante.Rows[0]["Dr"].ToString().Trim();
                             Puesto_Solicitande = Datos.P_Dt_Solicitante.Rows[0]["Puesto_Solicitante"].ToString().Trim();
                             Puesto_Director = Datos.P_Dt_Solicitante.Rows[0]["Puesto_Dr"].ToString().Trim();
                         }
                     }

                     //INSERTAMOS LOS DATOS DE LOS DETALLES
                     foreach (DataRow Dr in Dt_Mov.Rows)
                     {
                         MI_SQL = new StringBuilder();
                         MI_SQL.Append("INSERT INTO " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "(");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Movimiento_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Area_Funcional_ID + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Anio + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Enero + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Febrero + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Marzo + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Abril + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Mayo + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Junio + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Julio + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Agosto + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Septiembre + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Octubre + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Noviembre + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Diciembre + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Total + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Aprobado + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Ampliacion + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Reduccion + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Modificado + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Partida + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Egreso + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Movimiento + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Usuario + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Justificacion + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Solicitante + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Director + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Solicitante + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Director + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Creo + ", ");
                         MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Creo + ") ");
                         MI_SQL.Append(" VALUES(" + No_Solicitud + ", ");
                         MI_SQL.Append(Dr["MOVIMIENTO_ID"].ToString().Trim() + ", ");
                         MI_SQL.Append("'" + Dr["FF_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["UR_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["PROGRAMA_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["PARTIDA_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["AF_ID"].ToString().Trim() + "', ");
                         MI_SQL.Append("NULL, ");
                         MI_SQL.Append(Anio + ", ");
                         MI_SQL.Append("'" + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["APROBADO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["AMPLIACION"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["REDUCCION"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Dr["MODIFICADO"].ToString().Trim().Replace(",", "") + "', ");
                         MI_SQL.Append("'" + Datos.P_Estatus.Trim() + "', ");
                         MI_SQL.Append("'" + Dr["TIPO_OPERACION"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["TIPO_PARTIDA"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["TIPO_EGRESO"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Dr["TIPO_MOVIMIENTO"].ToString().Trim() + "', ");
                         MI_SQL.Append("'" + Datos.P_Tipo_Usuario.Trim().ToUpper() + "', ");
                         MI_SQL.Append("'" + Dr["JUSTIFICACION"].ToString().Trim().ToUpper() + "', ");
                         if (!String.IsNullOrEmpty(Solicitande.Trim()))
                         {
                             MI_SQL.Append("'" + Solicitande.Trim() + "', ");
                         }
                         else
                         {
                             MI_SQL.Append("NULL, ");
                         }

                         if (!String.IsNullOrEmpty(Director.Trim()))
                         {
                             MI_SQL.Append("'" + Director.Trim() + "', ");
                         }
                         else
                         {
                             MI_SQL.Append("NULL, ");
                         }

                         if (!String.IsNullOrEmpty(Puesto_Solicitande.Trim()))
                         {
                             MI_SQL.Append("'" + Puesto_Solicitande.Trim() + "', ");
                         }
                         else
                         {
                             MI_SQL.Append("NULL, ");
                         }

                         if (!String.IsNullOrEmpty(Puesto_Director.Trim()))
                         {
                             MI_SQL.Append("'" + Puesto_Director.Trim() + "', ");
                         }
                         else
                         {
                             MI_SQL.Append("NULL, ");
                         }
                         MI_SQL.Append("'" + Datos.P_Usuario_Creo.Trim() + "', ");
                         MI_SQL.Append("GETDATE()) ");

                         Comando.CommandText = MI_SQL.ToString();
                         Comando.ExecuteNonQuery();

                         if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                         {
                             //COMPROMETEMOS EL RECURSO
                             //PRE_COMPROMETEMOS EL RECURSO
                             MI_SQL = new StringBuilder();
                             MI_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                             MI_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = ");
                             MI_SQL.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", 0) + " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ");
                             MI_SQL.Append(" ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) - " + Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) - " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) - " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) - " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) - " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) - " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) - " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) - " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) - " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) - " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) - " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) - " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) - " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ",0) + " + Dr["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ",0) + " + Dr["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ",0) + " + Dr["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ",0) + " + Dr["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ",0) + " + Dr["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ",0) + " + Dr["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ",0) + " + Dr["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ",0) + " + Dr["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ",0) + " + Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ",0) + " + Dr["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ",0) + " + Dr["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                             MI_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ",0) + " + Dr["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", ""));
                             MI_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr["FF_ID"].ToString().Trim() + "'");
                             MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr["UR_ID"].ToString().Trim() + "'");
                             MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr["PROGRAMA_ID"].ToString().Trim() + "'");
                             MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr["PARTIDA_ID"].ToString().Trim() + "'");
                             MI_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);
                             Comando.CommandText = MI_SQL.ToString();
                             Comando.ExecuteNonQuery();
                         }
                     }

                     Dt_Datos.Columns.Add("No_Movimiento");
                     Dt_Datos.Columns.Add("No_Solicitud");

                     Fila = Dt_Datos.NewRow();
                     Fila["No_Movimiento"] = String.Empty;
                     Fila["No_Solicitud"] = No_Solicitud.ToString();
                     Dt_Datos.Rows.Add(Fila);

                     Dt_Anexos = Datos.P_Dt_Anexos;
                     if (Dt_Anexos != null && Dt_Anexos.Rows.Count > 0)
                     {
                         //OBTENEMOS EL NUMERO DE ANEXO
                         MI_SQL = new StringBuilder();
                         MI_SQL.Append("SELECT ISNULL(MAX(" + Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Anexo_ID + "), 0)");
                         MI_SQL.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det_Doc.Tabla_Ope_Psp_Movimiento_Egr_Det_Doc);

                         Comando.CommandText = MI_SQL.ToString().Trim();

                         Anexo_ID = Convert.ToInt32(Comando.ExecuteScalar());
                         Anexo_ID = Anexo_ID + 1;

                         //INSERTAMOS LOS ANEXOS
                         foreach (DataRow Dr_Doc in Dt_Anexos.Rows)
                         {
                             MI_SQL = new StringBuilder();
                             MI_SQL.Append("INSERT INTO " + Ope_Psp_Movimiento_Egr_Det_Doc.Tabla_Ope_Psp_Movimiento_Egr_Det_Doc + "(");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Anexo_ID + ", ");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Nombre + ", ");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Ruta_Documento + ", ");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Extension + ", ");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Solicitud_ID + ", ");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Movimiento_ID + ", ");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Usuario_Creo + ", ");
                             MI_SQL.Append(Ope_Psp_Movimiento_Egr_Det_Doc.Campo_Usuario_Modifico + ") VALUES( ");
                             MI_SQL.Append(Anexo_ID.ToString().Trim() + ", ");
                             MI_SQL.Append("'" + Dr_Doc["NOMBRE"].ToString().Trim() + "', ");
                             MI_SQL.Append("'" + Dr_Doc["RUTA_DOCUMENTO"].ToString().Trim() + "', ");
                             MI_SQL.Append("'" + Dr_Doc["EXTENSION"].ToString().Trim() + "', ");
                             MI_SQL.Append(No_Solicitud.ToString().Trim() + ", ");
                             MI_SQL.Append("1, ");
                             MI_SQL.Append("'" + Datos.P_Usuario_Creo + "', ");
                             MI_SQL.Append("GETDATE())");

                             Comando.CommandText = MI_SQL.ToString();
                             Comando.ExecuteNonQuery();
                             Anexo_ID++;
                         }
                     }
                 }

                 Transaccion.Commit();
             }
             catch (SqlException Ex)
             {
                 Transaccion.Rollback();
                 Mensaje = "Error:  [" + Ex.Message + "]";
                 throw new Exception(Mensaje, Ex);
             }
             finally
             {
                 Conexion.Close();
                 Comando = null;
                 Conexion = null;
                 Transaccion = null;
             }
             return Dt_Datos;
         }

         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Autorizacion_Adecuaciones
         ///DESCRIPCIÓN          : consulta para actualizar los datos de los movimientos de las adecuaciones
         ///PARAMETROS           : 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 12/Febrero/2013
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static String Autorizacion_Adecuaciones(Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Datos)
         {
             StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
             SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
             SqlConnection Conexion;//Variable para la conexión para la base de datos   
             SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
             String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
             String Autorizados = String.Empty;
             DataTable Dt_Movimientos = new DataTable();
             String Operacion = String.Empty;
             DataTable Dt_Mov = new DataTable();
             String Anio = string.Format("{0:yyyy}", DateTime.Now);
             SqlDataAdapter Da_Datos = new SqlDataAdapter(); 
             DataSet Ds_Datos = new DataSet();
             String[] Datos_Poliza_PSP = null;
             String No_Poliza_PSP = String.Empty;
             String Tipo_Poliza_PSP = String.Empty;
             String Mes_Anio_PSP = String.Empty;
             Double Importe = 0.00;
             String Importes = String.Empty;
             DataTable Dt_Origen = new DataTable();

             Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
             Comando = new SqlCommand();
             Conexion.Open();
             Transaccion = Conexion.BeginTransaction();
             Comando.Transaction = Transaccion;
             Comando.Connection = Conexion;

             try
             {
                 Dt_Movimientos = Datos.P_Dt_Mov;

                 if (Dt_Movimientos != null && Dt_Movimientos.Rows.Count > 0)
                 {
                     if (Datos.P_Estatus.Trim().Equals("AUTORIZADO"))
                     {
                         foreach (DataRow Dr in Dt_Movimientos.Rows)
                         {
                             if (Dr["ESTATUS"].ToString().Trim().Equals("AUTORIZADO"))
                             {
                                 //limpiamos las variables
                                 Dt_Mov = new DataTable();
                                 Da_Datos = new SqlDataAdapter();
                                 Ds_Datos = new DataSet();

                                 Mi_SQL = new StringBuilder();
                                 Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                                 Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                                 Comando.CommandText = Mi_SQL.ToString();
                                 Da_Datos = new SqlDataAdapter(Comando);
                                 Da_Datos.Fill(Ds_Datos);

                                 if (Ds_Datos != null)
                                 {
                                     Dt_Mov = Ds_Datos.Tables[0];
                                 }

                                 if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
                                 {
                                     Dt_Origen = (from x in Dt_Mov.AsEnumerable()
                                                        where x.Field<String>("TIPO_PARTIDA") == "Origen"
                                                        select x).AsDataView().ToTable();

                                     //obtenemos el importe de la adecuacion
                                     Importe = Dt_Origen.AsEnumerable().Sum(x => Convert.ToDouble(String.IsNullOrEmpty(x.Field<Decimal>("IMPORTE_TOTAL").ToString()) ? "0" : x.Field<Decimal>("IMPORTE_TOTAL").ToString()));

                                     //Juntamos los importes de la ampliacion, reduccion y reduccion interna;
                                     Importes = "0;0;" + Importe.ToString().Trim();

                                     //creamos la poliza presupuestal
                                     Datos_Poliza_PSP = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimientos_Presupuestales(Importes, Ope_Psp_Presupuesto_Aprobado.Campo_Modificado,
                                        Ope_Psp_Presupuesto_Aprobado.Campo_Disponible, 0.00, "", "", "", "", Comando);

                                     //obtenemos los id de la poliza presupuestal creada
                                     if (Datos_Poliza_PSP != null)
                                     {
                                         if (Datos_Poliza_PSP.Length > 0)
                                         {
                                             No_Poliza_PSP = Datos_Poliza_PSP[0].Trim();
                                             Tipo_Poliza_PSP = Datos_Poliza_PSP[1].Trim();
                                             Mes_Anio_PSP = Datos_Poliza_PSP[2].Trim();
                                         }
                                     }

                                     Mi_SQL = new StringBuilder();
                                     Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + " SET ");
                                     Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + "='" + Dr["ESTATUS"].ToString().Trim() + "', ");
                                     Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Comentario + "='" + Dr["COMENTARIO"].ToString().Trim() + "', ");

                                     if (!String.IsNullOrEmpty(No_Poliza_PSP))
                                     {
                                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_No_Poliza_Presupuestal + " = '" + No_Poliza_PSP.Trim() + "', ");
                                     }
                                     if (!String.IsNullOrEmpty(Tipo_Poliza_PSP))
                                     {
                                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Poliza_ID_Presupuestal + " = '" + Tipo_Poliza_PSP.Trim() + "', ");
                                     }
                                     if (!String.IsNullOrEmpty(Mes_Anio_PSP))
                                     {
                                         Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Mes_Anio_Presupuestal + " = '" + Mes_Anio_PSP.Trim() + "', ");
                                     }

                                     Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                                     Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + "=GETDATE() ");
                                     Mi_SQL.Append("WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                                     Comando.CommandText = Mi_SQL.ToString();
                                     Comando.ExecuteNonQuery();

                                     foreach (DataRow Dr_Mov in Dt_Mov.Rows)
                                     {
                                         if (Dr_Mov["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                                         {
                                             Mi_SQL = new StringBuilder();
                                             Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                             Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ",0) - " + Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ",0) - " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ",0) - " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ",0) - " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ",0) - " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ",0) - " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ",0) - " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ",0) - " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ",0) - " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ",0) - " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ",0) - " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ",0) - " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ",0) - " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ",0) - " + Dr_Mov["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ",0) - " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ",0) - " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ",0) - " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ",0) - " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = ISNULL( " + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ",0) - " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ",0) - " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ",0) - " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ",0) - " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ",0) - " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ",0) - " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ",0) - " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ",0) - " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                             Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                             Comando.CommandText = Mi_SQL.ToString();
                                             Comando.ExecuteNonQuery();
                                         }
                                         else
                                         {
                                             Mi_SQL = new StringBuilder();
                                             Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                             Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) + " + Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ",0) + " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ",0) + " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ",0) + " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ",0) + " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ",0) + " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ",0) + " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ",0) + " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ",0) + " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ",0) + " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ",0) + " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ",0) + " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ",0) + " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ",0) + " + Dr_Mov["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) + " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) + " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) + " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) + " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) + " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) + " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) + " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) + " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) + " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) + " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) + " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) + " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                                             Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ");
                                             Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                             Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                             Comando.CommandText = Mi_SQL.ToString();
                                             Comando.ExecuteNonQuery();
                                         }

                                     }
                                 }
                             }
                             else
                             {
                                 Mi_SQL = new StringBuilder();
                                 Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + " SET ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + "='" + Dr["ESTATUS"].ToString().Trim() + "', ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Comentario + "='" + Dr["COMENTARIO"].ToString().Trim() + "', ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                                 Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + "=GETDATE() ");
                                 Mi_SQL.Append("WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                                 Comando.CommandText = Mi_SQL.ToString();
                                 Comando.ExecuteNonQuery();

                                 if (Dr["ESTATUS"].ToString().Trim().Equals("RECHAZADO"))
                                 {
                                     //limpiamos las variables
                                     Dt_Mov = new DataTable();
                                     Da_Datos = new SqlDataAdapter();
                                     Ds_Datos = new DataSet();

                                     Mi_SQL = new StringBuilder();
                                     Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                                     Mi_SQL.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                                     Comando.CommandText = Mi_SQL.ToString();
                                     Da_Datos = new SqlDataAdapter(Comando);
                                     Da_Datos.Fill(Ds_Datos);
                                     if (Ds_Datos != null)
                                     {
                                         Dt_Mov = Ds_Datos.Tables[0];
                                     }

                                     if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
                                     {
                                         foreach (DataRow Dr_Mov in Dt_Mov.Rows)
                                         {
                                             if (Dr_Mov["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                                             {
                                                 Mi_SQL = new StringBuilder();
                                                 Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + " SET ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ",0) - ");
                                                 Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ",0) + ");
                                                 Mi_SQL.Append(Dr_Mov["IMPORTE_TOTAL"] + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ",0) - " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ",0) - " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ",0) - " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ",0) - " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ",0) - " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ",0) - " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ",0) - " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ",0) - " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ",0) - " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ",0) - " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ",0) - " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ",0) - " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ",0) + " + Dr_Mov["IMPORTE_ENERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ",0) + " + Dr_Mov["IMPORTE_FEBRERO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ",0) + " + Dr_Mov["IMPORTE_MARZO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ",0) + " + Dr_Mov["IMPORTE_ABRIL"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ",0) + " + Dr_Mov["IMPORTE_MAYO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ",0) + " + Dr_Mov["IMPORTE_JUNIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ",0) + " + Dr_Mov["IMPORTE_JULIO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ",0) + " + Dr_Mov["IMPORTE_AGOSTO"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ",0) + " + Dr_Mov["IMPORTE_SEPTIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ",0) + " + Dr_Mov["IMPORTE_OCTUBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ",0) + " + Dr_Mov["IMPORTE_NOVIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", "") + ", ");
                                                 Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ",0) + " + Dr_Mov["IMPORTE_DICIEMBRE"].ToString().Trim().Replace(",", "").Replace("$", ""));
                                                 Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dr_Mov["DEPENDENCIA_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dr_Mov["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dr_Mov["PROYECTO_PROGRAMA_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dr_Mov["PARTIDA_ID"].ToString().Trim() + "'");
                                                 Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                                 Comando.CommandText = Mi_SQL.ToString();
                                                 Comando.ExecuteNonQuery();
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                     else if (Datos.P_Estatus.Trim().Equals("RECIBIDO"))
                     {
                         foreach (DataRow Dr in Dt_Movimientos.Rows)
                         {
                             Mi_SQL = new StringBuilder();
                             Mi_SQL.Append("UPDATE " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + " SET ");
                             Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Estatus + "='" + Dr["ESTATUS"].ToString().Trim() + "', ");
                             Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                             Mi_SQL.Append(Ope_Psp_Movimiento_Egr_Det.Campo_Fecha_Modifico + "=GETDATE() ");
                             Mi_SQL.Append("WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Solicitud_ID + " = " + Dr["SOLICITUD_ID"].ToString().Trim());

                             Comando.CommandText = Mi_SQL.ToString();
                             Comando.ExecuteNonQuery();
                         }
                     }
                 }

                 Autorizados = "SI";
                 Transaccion.Commit();

                 Conexion.Close();

             }
             catch (SqlException Ex)
             {
                 Transaccion.Rollback();
                 Mensaje = "Error:  [" + Ex.Message + "]";
                 throw new Exception(Mensaje, Ex);
             }
             finally
             {
                 Conexion.Close();
                 Comando = null;
                 Conexion = null;
                 Transaccion = null;
             }
             return Autorizados;
         }
        #endregion
    }
}