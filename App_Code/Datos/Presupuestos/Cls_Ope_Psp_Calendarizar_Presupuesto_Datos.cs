﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Calendarizar_Presupuesto.Negocio;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Sessiones;

namespace JAPAMI.Calendarizar_Presupuesto.Datos
{
    public class Cls_Ope_Psp_Calendarizar_Presupuesto_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Unidad_Responsable
        ///DESCRIPCIÓN          : consulta para obtener los datos de las unidad responsable
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 10/Noviembre/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Unidad_Responsable()
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS NOMBRE, ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_Sql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal);
                Mi_Sql.Append(" ON " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID);
                Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros);
                Mi_Sql.Append(" ON " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal);
                Mi_Sql.Append(" = " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Anio_Presupuestar);
                Mi_Sql.Append(" AND " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Estatus + " = 'ACTIVO'");
                Mi_Sql.Append(" ORDER BY " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " ASC");
                
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las unidades responsables. Error: [" + Ex.Message + "]");
            }
        }

        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Capitulos
        ///DESCRIPCIÓN          : consulta para obtener los capitulos de una unidad responsable
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 10/Noviembre/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Capitulos(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " +' '+ ");
                Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + " AS NOMBRE, ");
                Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                Mi_Sql.Append(" FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal);
                Mi_Sql.Append(" ON " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                Mi_Sql.Append(" = " + Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Detalle_Lim_Presup.Campo_Capitulo_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros);
                Mi_Sql.Append(" ON " + Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Detalle_Lim_Presup.Campo_Anio_presupuestal);
                Mi_Sql.Append(" = " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Anio_Presupuestar);
                Mi_Sql.Append(" AND " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Estatus + " = 'ACTIVO'");

                if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                {
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Detalle_Lim_Presup.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                }

                if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID))
                {
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Detalle_Lim_Presup.Campo_Empleado_ID);
                    Mi_Sql.Append(" = '" + Negocio.P_Empleado_ID + "'");
                }

                Mi_Sql.Append(" ORDER BY " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " ASC");



                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los capitulos. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas_Especificas
        ///DESCRIPCIÓN          : consulta para obtener los capitulos de una unidad responsable
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 10/Noviembre/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Partidas_Especificas(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append("SELECT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + "+' " + " " + "'+ ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS NOMBRE ");
                Mi_SQL.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + ", ");
                Mi_SQL.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + ", ");
                Mi_SQL.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + ", ");
                Mi_SQL.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                Mi_SQL.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                Mi_SQL.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                Mi_SQL.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                Mi_SQL.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Estatus + " = 'ACTIVO'");
                Mi_SQL.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID + "'");
                Mi_SQL.Append(" ORDER BY " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las partidas especificas. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Generales
        ///DESCRIPCIÓN          : consulta para obtener los datos del año presupuestal
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 14/Noviembre/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Presupuesto_Dependencia(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append("SELECT " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Anio_Presupuestar + ", ");
                Mi_SQL.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Fte_Financiamiento_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Limite_Presupuestal + ", ");
                Mi_SQL.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Proyecto_Programa_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID+ ", ");
                Mi_SQL.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Empleado_ID+ ", ");
                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FTE_FINACIAMIENTO, ");
                Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS PROGRAMA, ");
                Mi_SQL.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus + ", '') AS ESTATUS");
                Mi_SQL.Append(" FROM " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros);
                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal);
                Mi_SQL.Append(" ON " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal);
                Mi_SQL.Append(" = " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Anio_Presupuestar);
                Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_SQL.Append(" ON " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Fte_Financiamiento_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_SQL.Append(" ON " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Proyecto_Programa_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                Mi_SQL.Append(" = " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Anio_Presupuestar);
               
                if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                {
                   Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID + "'");
                   Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " IS NULL ");
                }
                if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID))
                {
                   Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " = '" + Negocio.P_Empleado_ID + "'");
                }

                Mi_SQL.Append(" WHERE " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Estatus + " = 'ACTIVO'");

                if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                {
                    Mi_SQL.Append(" AND " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID + "'");
                }
                if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID))
                {
                    Mi_SQL.Append(" AND " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Empleado_ID + " = '" + Negocio.P_Empleado_ID + "'");
                }

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los datos del año presupuestal. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Productos
        ///DESCRIPCIÓN          : consulta para obtener los productos de una partida
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 11/Noviembre/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Programas(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append("SELECT " + Cat_Com_Productos.Campo_Clave + ", ");
                Mi_SQL.Append(Cat_Com_Productos.Campo_Producto_ID + ", ");
                Mi_SQL.Append(Cat_Com_Productos.Campo_Costo + ", ");
                Mi_SQL.Append(Cat_Com_Productos.Campo_Nombre + ", ");
                Mi_SQL.Append(Cat_Com_Productos.Campo_Descripcion + ", ");
                Mi_SQL.Append(Cat_Com_Productos.Campo_Nombre + "  +' - '+  " + Cat_Com_Productos.Campo_Descripcion + " AS DESCRIPCION_PRODUCTO, ");
                Mi_SQL.Append(Cat_Com_Productos.Campo_Clave +" + '' + "+Cat_Com_Productos.Campo_Nombre + " + '-' + " + Cat_Com_Productos.Campo_Descripcion + " AS CLAVE_PRODUCTO");
                Mi_SQL.Append(" FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos);
                Mi_SQL.Append(" WHERE " + Cat_Com_Productos.Campo_Partida_ID + " = '" + Negocio.P_Partida_ID + "'");
                Mi_SQL.Append(" AND " + Cat_Com_Productos.Campo_Estatus + " = 'ACTIVO'");

                if (!String.IsNullOrEmpty(Negocio.P_Producto_ID)) 
                {
                    Mi_SQL.Append(" AND " + Cat_Com_Productos.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "'");
                }

                Mi_SQL.Append(" ORDER BY " + Cat_Com_Productos.Campo_Clave + " ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los productos. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Productos
        ///DESCRIPCIÓN          : consulta para obtener los productos de una partida
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 11/Noviembre/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Productos(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append("SELECT " + Cat_Com_Productos.Campo_Clave + ", ");
                Mi_SQL.Append(Cat_Com_Productos.Campo_Producto_ID + ", ");
                Mi_SQL.Append(Cat_Com_Productos.Campo_Costo + ", ");
                Mi_SQL.Append(Cat_Com_Productos.Campo_Nombre + ", ");
                Mi_SQL.Append(Cat_Com_Productos.Campo_Descripcion + ", ");
                Mi_SQL.Append(Cat_Com_Productos.Campo_Nombre + "  + ' - ' +  ISNULL(" + Cat_Com_Productos.Campo_Descripcion + ", '') AS DESCRIPCION_PRODUCTO, ");
                Mi_SQL.Append(Cat_Com_Productos.Campo_Clave + " + ' ' + " + Cat_Com_Productos.Campo_Nombre + " + ' - ' + ISNULL(" 
                    + Cat_Com_Productos.Campo_Descripcion + ", '') AS CLAVE_PRODUCTO");
                Mi_SQL.Append(" FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos);
                Mi_SQL.Append(" WHERE " + Cat_Com_Productos.Campo_Partida_ID + " = '" + Negocio.P_Partida_ID + "'");
                Mi_SQL.Append(" AND " + Cat_Com_Productos.Campo_Estatus + " = 'ACTIVO'");

                if (!String.IsNullOrEmpty(Negocio.P_Producto_ID))
                {
                    Mi_SQL.Append(" AND " + Cat_Com_Productos.Campo_Producto_ID + " = '" + Negocio.P_Producto_ID + "'");
                }

                if (!String.IsNullOrEmpty(Negocio.P_Clave_Producto))
                {
                    Mi_SQL.Append(" AND " + Cat_Com_Productos.Campo_Clave + " LIKE '%" + Negocio.P_Clave_Producto.ToUpper() + "%'");
                }

                if (!String.IsNullOrEmpty(Negocio.P_Nombre_Producto))
                {
                    Mi_SQL.Append(" AND " + Cat_Com_Productos.Campo_Nombre + "  + ' - ' +  ISNULL(" + Cat_Com_Productos.Campo_Descripcion + ", '') ");
                    Mi_SQL.Append(" LIKE '%" + Negocio.P_Nombre_Producto.ToUpper() + "%'");
                }

                Mi_SQL.Append(" ORDER BY " + Cat_Com_Productos.Campo_Clave + " ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los productos. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partida_Stock
        ///DESCRIPCIÓN          : consulta para obtener si la partida es de stock o no
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 14/Noviembre/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Partida_Stock(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append("SELECT " + Cat_Psp_Parametros_Detalles.Tabla_Cat_Psp_Parametros_Detalles + "." + Cat_Psp_Parametros_Detalles.Campo_Partida_ID + " ");
                Mi_SQL.Append(" FROM " + Cat_Psp_Parametros_Detalles.Tabla_Cat_Psp_Parametros_Detalles + ", ");
                Mi_SQL.Append(Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros);
                Mi_SQL.Append(" WHERE " + Cat_Psp_Parametros_Detalles.Tabla_Cat_Psp_Parametros_Detalles + "." +Cat_Psp_Parametros_Detalles.Campo_Parametro_ID);
                Mi_SQL.Append(" = " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Parametro_ID);
                Mi_SQL.Append(" AND " + Cat_Psp_Parametros_Detalles.Tabla_Cat_Psp_Parametros_Detalles + "." + Cat_Psp_Parametros_Detalles.Campo_Partida_ID + " = '" + Negocio.P_Partida_ID + "'");
                Mi_SQL.Append(" AND " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Anio_Presupuestar + " = '" + Negocio.P_Anio_Presupuesto + "'");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las partidas de stock. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partida_Stock
        ///DESCRIPCIÓN          : consulta para obtener si la partida es de stock o no
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 14/Noviembre/2011
        ///MODIFICO             : Jennyfer Ivonne Ceja Lemus 
        ///FECHA_MODIFICO       : 22/Agosto/2012
        ///CAUSA_MODIFICACIÓN   : Se agrego el campo Subnivel Presupuestal a la tabla
        ///*******************************************************************************
        internal static Boolean Guardar_Partidas_Asignadas(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            Boolean Operacion_Exitosa = false;
            Double Ene = 0.00;
            Double Feb = 0.00;
            Double Mar = 0.00;
            Double Abr = 0.00;
            Double May = 0.00;
            Double Jun = 0.00;
            Double Jul = 0.00;
            Double Ago = 0.00;
            Double Sep = 0.00;
            Double Oct = 0.00;
            Double Nov = 0.00;
            Double Dic = 0.00;
            Double Precio = 0.00;
            String Area_Funcional_ID = String.Empty;


            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {

                foreach (DataRow Renglon in Negocio.P_Dt_Datos.Rows)
                {
                    Ene = 0.00;
                    Feb = 0.00;
                    Mar = 0.00;
                    Abr = 0.00;
                    May = 0.00;
                    Jun = 0.00;
                    Jul = 0.00;
                    Ago = 0.00;
                    Sep = 0.00;
                    Oct = 0.00;
                    Nov = 0.00;
                    Dic = 0.00;
                    Precio = 0.00;

                    Precio = Convert.ToDouble(String.IsNullOrEmpty(Renglon["PRECIO"].ToString().Trim()) ? "0" : Renglon["PRECIO"].ToString().Trim());

                    if (Precio > 0)
                    {
                        Ene = Convert.ToDouble(String.IsNullOrEmpty(Renglon["ENERO"].ToString().Trim()) ? "0" : Renglon["ENERO"].ToString().Trim()) / Precio;
                        Feb = Convert.ToDouble(String.IsNullOrEmpty(Renglon["FEBRERO"].ToString().Trim()) ? "0" : Renglon["FEBRERO"].ToString().Trim()) / Precio;
                        Mar = Convert.ToDouble(String.IsNullOrEmpty(Renglon["MARZO"].ToString().Trim()) ? "0" : Renglon["MARZO"].ToString().Trim()) / Precio;
                        Abr = Convert.ToDouble(String.IsNullOrEmpty(Renglon["ABRIL"].ToString().Trim()) ? "0" : Renglon["ABRIL"].ToString().Trim()) / Precio;
                        May = Convert.ToDouble(String.IsNullOrEmpty(Renglon["MAYO"].ToString().Trim()) ? "0" : Renglon["MAYO"].ToString().Trim()) / Precio;
                        Jun = Convert.ToDouble(String.IsNullOrEmpty(Renglon["JUNIO"].ToString().Trim()) ? "0" : Renglon["JUNIO"].ToString().Trim()) / Precio;
                        Jul = Convert.ToDouble(String.IsNullOrEmpty(Renglon["JULIO"].ToString().Trim()) ? "0" : Renglon["JULIO"].ToString().Trim()) / Precio;
                        Ago = Convert.ToDouble(String.IsNullOrEmpty(Renglon["AGOSTO"].ToString().Trim()) ? "0" : Renglon["AGOSTO"].ToString().Trim()) / Precio;
                        Sep = Convert.ToDouble(String.IsNullOrEmpty(Renglon["SEPTIEMBRE"].ToString().Trim()) ? "0" : Renglon["SEPTIEMBRE"].ToString().Trim()) / Precio;
                        Oct = Convert.ToDouble(String.IsNullOrEmpty(Renglon["OCTUBRE"].ToString().Trim()) ? "0" : Renglon["OCTUBRE"].ToString().Trim()) / Precio;
                        Nov = Convert.ToDouble(String.IsNullOrEmpty(Renglon["NOVIEMBRE"].ToString().Trim()) ? "0" : Renglon["NOVIEMBRE"].ToString().Trim()) / Precio;
                        Dic = Convert.ToDouble(String.IsNullOrEmpty(Renglon["DICIEMBRE"].ToString().Trim()) ? "0" : Renglon["DICIEMBRE"].ToString().Trim()) / Precio;
                    }

                    //Obtenemos el area funcional
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT " + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_SQL.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_SQL.Append(" WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Renglon["DEPENDENCIA_ID"].ToString() + "'");
                    Cmd.CommandText = Mi_SQL.ToString();
                    Area_Funcional_ID = (String)Cmd.ExecuteScalar();

                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu  + " (" + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Capitulo_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Producto_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Anio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Justificacion + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Enero + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Febrero + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Marzo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Abril + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Mayo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Junio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Julio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Agosto + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Septiembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Octubre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Noviembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Diciembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Estatus + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Area_Funcional_ID + ", ");
                    if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim())) 
                    {
                        Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + ", ");
                    }
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID_Creo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Fecha_Creo + ") VALUES (");
                    Mi_SQL.Append("'" + Renglon["DEPENDENCIA_ID"].ToString() + "', ");
                    Mi_SQL.Append("'" + Negocio.P_Fuente_Financiamiento_ID + "',");
                    Mi_SQL.Append("'" + Renglon["PROYECTO_ID"].ToString() + "', ");
                    Mi_SQL.Append("'" + Renglon["CAPITULO_ID"].ToString() + "', ");
                    Mi_SQL.Append("'" + Renglon["PARTIDA_ID"].ToString() + "', ");

                    if (String.IsNullOrEmpty(Renglon["PRODUCTO_ID"].ToString()))
                    {
                        Mi_SQL.Append(" NULL, ");
                    }
                    else 
                    {
                        Mi_SQL.Append("'" + Renglon["PRODUCTO_ID"].ToString() + "', ");
                    }

                    Mi_SQL.Append("'" + Negocio.P_Anio_Presupuesto + "',");
                    Mi_SQL.Append("'" + Renglon["JUSTIFICACION"].ToString() + "', ");
                    Mi_SQL.Append(Convert.ToString(Ene) + ", ");
                    Mi_SQL.Append(Convert.ToString(Feb) + ", ");
                    Mi_SQL.Append(Convert.ToString(Mar) + ", ");
                    Mi_SQL.Append(Convert.ToString(Abr) + ", ");
                    Mi_SQL.Append(Convert.ToString(May) + ", ");
                    Mi_SQL.Append(Convert.ToString(Jun) + ", ");
                    Mi_SQL.Append(Convert.ToString(Jul) + ", ");
                    Mi_SQL.Append(Convert.ToString(Ago) + ", ");
                    Mi_SQL.Append(Convert.ToString(Sep) + ", ");
                    Mi_SQL.Append(Convert.ToString(Oct) + ", ");
                    Mi_SQL.Append(Convert.ToString(Nov) + ", ");
                    Mi_SQL.Append(Convert.ToString(Dic) + ", ");
                    Mi_SQL.Append(Renglon["ENERO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["FEBRERO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["MARZO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["ABRIL"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["MAYO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["JUNIO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["JULIO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["AGOSTO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["SEPTIEMBRE"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["OCTUBRE"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["NOVIEMBRE"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["DICIEMBRE"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["IMPORTE_TOTAL"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append("'" + Negocio.P_Estatus + "',");
                    Mi_SQL.Append("'" + Area_Funcional_ID + "',");
                    if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                    {
                        Mi_SQL.Append("'" + Negocio.P_Empleado_ID + "',");
                    }
                    Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "',");
                    Mi_SQL.Append("'" + Cls_Sessiones.Empleado_ID.Trim() + "',");
                    Mi_SQL.Append("GETDATE())");
                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();
                }
                Trans.Commit();
                Operacion_Exitosa = true;
            }
            catch (Exception Ex)
            {
                Operacion_Exitosa = false;
                Trans.Rollback();
                throw new Exception("Error al intentar insertar los registros. Error: [" + Ex.Message + "]");
            }
            return Operacion_Exitosa;
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partida_Asignadas
        ///DESCRIPCIÓN          : consulta para obtener las partidas asignadas
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Noviembre/2011
        ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
        ///FECHA_MODIFICO       : 22/Agosto/2012
        ///CAUSA_MODIFICACIÓN   : Se agrego un nivel presupuestal para permitir asignar presupuesto a otro tipo de actividades como salarios
        ///*******************************************************************************
        internal static DataTable Consultar_Partida_Asignadas(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append(" SELECT " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID + " AS PROYECTO_ID, ");
                Mi_SQL.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID + ", ");
                Mi_SQL.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Producto_ID + ", '') AS PRODUCTO_ID, ");
                //Cambio
                Mi_SQL.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Subnivel_Presupuestal_ID + ", 0) AS SUBNIVEL_ID, ");//

                //obtenemos el precio
                Mi_SQL.Append("(CASE ");
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Enero + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Enero);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Febrero + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Febrero);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Marzo + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Marzo);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Abril + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Abril);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Mayo + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Mayo);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Junio + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Junio);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Julio + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Julio);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Agosto + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto+ "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Agosto);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Septiembre + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Septiembre);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Octubre + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Octubre);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Noviembre + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Noviembre);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Diciembre + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Diciembre);
                Mi_SQL.Append(" ELSE 0 END) AS PRECIO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Justificacion + ", ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CLAVE_PARTIDA, ");
                Mi_SQL.Append("ISNULL(" + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Clave + ", '') + ' ' + ");
                Mi_SQL.Append("ISNULL(" + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Nombre + ", '') AS CLAVE_PRODUCTO, ");
                Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS UR, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero + " AS ENERO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero + " AS FEBRERO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo + " AS MARZO , ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril + " AS ABRIL, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo + " AS MAYO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio + " AS JUNIO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio + " AS JULIO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto + " AS AGOSTO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre + " AS SEPTIEMBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre + " AS OCTUBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre + " AS NOVIEMBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre + " AS DICIEMBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + " AS IMPORTE_TOTAL, '' AS ID ");
                Mi_SQL.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Producto_ID);
                Mi_SQL.Append(" = " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Producto_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias );
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica);
                Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                Mi_SQL.Append(" ON " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                Mi_SQL.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                Mi_SQL.Append(" = " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Anio_Presupuestar);
                Mi_SQL.Append(" AND " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Estatus + " = 'ACTIVO'");

                if(!string.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " IS NULL ");
                    }
                    else 
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " IS NULL ");
                    }
                }

                if (!string.IsNullOrEmpty(Negocio.P_Empleado_ID))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Empleado_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Empleado_ID + "'");
                    }
                }

                if (!string.IsNullOrEmpty(Negocio.P_Estatus))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_SQL.Append(" = '" + Negocio.P_Estatus + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_SQL.Append(" = '" + Negocio.P_Estatus + "'");
                    }
                }
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las partidas asignadas. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Partida_Asignadas
        ///DESCRIPCIÓN          : Consulta para modificar los datos de las partidas asignadas
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Noviembre/2011
        ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
        ///FECHA_MODIFICO       : 22/Agosto/2012
        ///CAUSA_MODIFICACIÓN   : Se agrego el campo subnivel presupuestal a la calendarizacion
        ///*******************************************************************************
        internal static Boolean Modificar_Partida_Asignadas(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            Boolean Operacion_Exitosa = false;
            Double Ene = 0.00;
            Double Feb = 0.00;
            Double Mar = 0.00;
            Double Abr = 0.00;
            Double May = 0.00;
            Double Jun = 0.00;
            Double Jul = 0.00;
            Double Ago = 0.00;
            Double Sep = 0.00;
            Double Oct = 0.00;
            Double Nov = 0.00;
            Double Dic = 0.00;
            Double Precio = 0.00;

            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;
            String Area_Funcional_ID = String.Empty;

            try
            {
                if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                {
                    Mi_SQL.Append("DELETE FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " = '" + Negocio.P_Empleado_ID + "'");
                    Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Campo_Anio + " = '" + Negocio.P_Anio_Presupuesto + "'");
                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();
                }
                if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                {
                    Mi_SQL.Append("DELETE FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID + "'");
                    Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Campo_Anio + " = '" + Negocio.P_Anio_Presupuesto + "'");
                    Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " IS NULL ");
                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();
                }

                foreach (DataRow Renglon in Negocio.P_Dt_Datos.Rows)
                {
                    Mi_SQL = new StringBuilder();
                    Ene = 0.00;
                    Feb = 0.00;
                    Mar = 0.00;
                    Abr = 0.00;
                    May = 0.00;
                    Jun = 0.00;
                    Jul = 0.00;
                    Ago = 0.00;
                    Sep = 0.00;
                    Oct = 0.00;
                    Nov = 0.00;
                    Dic = 0.00;
                    Precio = 0.00;

                    Precio = Convert.ToDouble(String.IsNullOrEmpty(Renglon["PRECIO"].ToString().Trim()) ? "0" : Renglon["PRECIO"].ToString().Trim());

                    if (Precio > 0)
                    {
                        Ene = Convert.ToDouble(String.IsNullOrEmpty(Renglon["ENERO"].ToString().Trim()) ? "0" : Renglon["ENERO"].ToString().Trim()) / Precio;
                        Feb = Convert.ToDouble(String.IsNullOrEmpty(Renglon["FEBRERO"].ToString().Trim()) ? "0" : Renglon["FEBRERO"].ToString().Trim()) / Precio;
                        Mar = Convert.ToDouble(String.IsNullOrEmpty(Renglon["MARZO"].ToString().Trim()) ? "0" : Renglon["MARZO"].ToString().Trim()) / Precio;
                        Abr = Convert.ToDouble(String.IsNullOrEmpty(Renglon["ABRIL"].ToString().Trim()) ? "0" : Renglon["ABRIL"].ToString().Trim()) / Precio;
                        May = Convert.ToDouble(String.IsNullOrEmpty(Renglon["MAYO"].ToString().Trim()) ? "0" : Renglon["MAYO"].ToString().Trim()) / Precio;
                        Jun = Convert.ToDouble(String.IsNullOrEmpty(Renglon["JUNIO"].ToString().Trim()) ? "0" : Renglon["JUNIO"].ToString().Trim()) / Precio;
                        Jul = Convert.ToDouble(String.IsNullOrEmpty(Renglon["JULIO"].ToString().Trim()) ? "0" : Renglon["JULIO"].ToString().Trim()) / Precio;
                        Ago = Convert.ToDouble(String.IsNullOrEmpty(Renglon["AGOSTO"].ToString().Trim()) ? "0" : Renglon["AGOSTO"].ToString().Trim()) / Precio;
                        Sep = Convert.ToDouble(String.IsNullOrEmpty(Renglon["SEPTIEMBRE"].ToString().Trim()) ? "0" : Renglon["SEPTIEMBRE"].ToString().Trim()) / Precio;
                        Oct = Convert.ToDouble(String.IsNullOrEmpty(Renglon["OCTUBRE"].ToString().Trim()) ? "0" : Renglon["OCTUBRE"].ToString().Trim()) / Precio;
                        Nov = Convert.ToDouble(String.IsNullOrEmpty(Renglon["NOVIEMBRE"].ToString().Trim()) ? "0" : Renglon["NOVIEMBRE"].ToString().Trim()) / Precio;
                        Dic = Convert.ToDouble(String.IsNullOrEmpty(Renglon["DICIEMBRE"].ToString().Trim()) ? "0" : Renglon["DICIEMBRE"].ToString().Trim()) / Precio;
                    }

                    //Obtenemos el area funcional
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT " + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_SQL.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_SQL.Append(" WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Renglon["DEPENDENCIA_ID"].ToString() + "'");
                    Cmd.CommandText = Mi_SQL.ToString();
                    Area_Funcional_ID = (String)Cmd.ExecuteScalar();

                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + " (" + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Capitulo_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Producto_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Anio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Justificacion + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Enero + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Febrero + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Marzo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Abril + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Mayo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Junio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Julio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Agosto + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Septiembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Octubre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Noviembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Diciembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Estatus + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Area_Funcional_ID + ", ");
                    if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                    {
                        Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + ", ");
                    }
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID_Creo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Fecha_Creo + ") VALUES (");
                    Mi_SQL.Append("'" + Renglon["DEPENDENCIA_ID"].ToString() + "', ");
                    Mi_SQL.Append("'" + Negocio.P_Fuente_Financiamiento_ID + "',");
                    Mi_SQL.Append("'" + Renglon["PROYECTO_ID"].ToString() + "', ");
                    Mi_SQL.Append("'" + Renglon["CAPITULO_ID"].ToString() + "', ");
                    Mi_SQL.Append("'" + Renglon["PARTIDA_ID"].ToString() + "', ");

                    if (String.IsNullOrEmpty(Renglon["PRODUCTO_ID"].ToString()))
                    {
                        Mi_SQL.Append(" NULL, ");
                    }
                    else
                    {
                        Mi_SQL.Append("'" + Renglon["PRODUCTO_ID"].ToString() + "', ");
                    }

                    Mi_SQL.Append("'" + Negocio.P_Anio_Presupuesto + "',");
                    Mi_SQL.Append("'" + Renglon["JUSTIFICACION"].ToString() + "', ");
                    Mi_SQL.Append(Convert.ToString(Ene) + ", ");
                    Mi_SQL.Append(Convert.ToString(Feb) + ", ");
                    Mi_SQL.Append(Convert.ToString(Mar) + ", ");
                    Mi_SQL.Append(Convert.ToString(Abr) + ", ");
                    Mi_SQL.Append(Convert.ToString(May) + ", ");
                    Mi_SQL.Append(Convert.ToString(Jun) + ", ");
                    Mi_SQL.Append(Convert.ToString(Jul) + ", ");
                    Mi_SQL.Append(Convert.ToString(Ago) + ", ");
                    Mi_SQL.Append(Convert.ToString(Sep) + ", ");
                    Mi_SQL.Append(Convert.ToString(Oct) + ", ");
                    Mi_SQL.Append(Convert.ToString(Nov) + ", ");
                    Mi_SQL.Append(Convert.ToString(Dic) + ", ");
                    Mi_SQL.Append(Renglon["ENERO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["FEBRERO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["MARZO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["ABRIL"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["MAYO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["JUNIO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["JULIO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["AGOSTO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["SEPTIEMBRE"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["OCTUBRE"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["NOVIEMBRE"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["DICIEMBRE"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["IMPORTE_TOTAL"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append("'" + Negocio.P_Estatus + "',");
                    Mi_SQL.Append("'" + Area_Funcional_ID + "',");
                    if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                    {
                        Mi_SQL.Append("'" + Negocio.P_Empleado_ID + "',");
                    }
                    Mi_SQL.Append("'" + Negocio.P_Usuario_Modifico + "',");
                    Mi_SQL.Append("'" + Cls_Sessiones.Empleado_ID.Trim() + "',");
                    Mi_SQL.Append("GETDATE())");

                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();
                }
                Trans.Commit();
                Operacion_Exitosa = true;
            }
            catch (Exception Ex)
            {
                Operacion_Exitosa = false;
                Trans.Rollback();
                throw new Exception("Error al intentar modificar los registros. Error: [" + Ex.Message + "]");
            }
            return Operacion_Exitosa;
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Dependencias_Presupuestadas
        ///DESCRIPCIÓN          : consulta para obtener las dependencias presupuestadas
        ///PARAMETROS           :
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 23/Noviembre/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Dependencias_Presupuestadas()
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append(" SELECT " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio+ ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID_Creo+ ", ");
                Mi_SQL.Append("SUM(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ") AS TOTAL, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus + ", ");
                Mi_SQL.Append("("+Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + "  + ' ' + ");
                Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ") AS CLAVE_NOMBRE, ");
                Mi_SQL.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Limite_Presupuestal+", " );
                Mi_SQL.Append("'Dependencia' AS TIPO_CALENDARIO");
                Mi_SQL.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                Mi_SQL.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " IS NULL");
                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                Mi_SQL.Append(" = " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID);
                Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                Mi_SQL.Append(" = " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal);
                Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros);
                Mi_SQL.Append(" ON " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Anio_Presupuestar);
                Mi_SQL.Append(" = " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal);
                Mi_SQL.Append(" AND " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Estatus + " = 'ACTIVO'");
                Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus + " = 'GENERADO'");
                Mi_SQL.Append(" GROUP BY " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus+ ", ");
                Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ", ");
                Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID_Creo + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Limite_Presupuestal);

                Mi_SQL.Append(" UNION ");

                Mi_SQL.Append(" SELECT '' AS DEPENDENCIA_ID, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio + ", ");
                Mi_SQL.Append(" ''  AS PROYECTO_PROGRAMA_ID, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID_Creo + ", ");
                Mi_SQL.Append("SUM(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ") AS TOTAL, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus + ", ");
                Mi_SQL.Append("(" + Cat_Empleados.Tabla_Cat_Empleados  + "." + Cat_Empleados.Campo_No_Empleado  + " + ' ' + ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + " + ' ' + ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + " + ' ' + ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + ") AS CLAVE_NOMBRE, ");
                Mi_SQL.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Limite_Presupuestal+", ");
                Mi_SQL.Append("'Empleado' AS TIPO_CALENDARIO");
                Mi_SQL.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                Mi_SQL.Append(" INNER JOIN " + Cat_Empleados.Tabla_Cat_Empleados );
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID);
                Mi_SQL.Append(" = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID );
                Mi_SQL.Append(" INNER JOIN " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID);
                Mi_SQL.Append(" = " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Empleado_ID);
                Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                Mi_SQL.Append(" = " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal);
                Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros);
                Mi_SQL.Append(" ON " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Anio_Presupuestar);
                Mi_SQL.Append(" = " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal);
                Mi_SQL.Append(" AND " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Estatus + " = 'ACTIVO'");
                Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus + " = 'GENERADO'");
                Mi_SQL.Append(" GROUP BY " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus + ", ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_No_Empleado + ", ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + ", ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + ", ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID_Creo + ", ");
                Mi_SQL.Append(Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Limite_Presupuestal);

                Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las partidas asignadas. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Guardar_Historial_Calendario
        ///DESCRIPCIÓN          : Consulta para guardar los comentarios del presupuesto
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 23/Noviembre/2011
        ///MODIFICO             : Jennyer Ivonne Ceja Lemus
        ///FECHA_MODIFICO       : 22/Agosto/2012
        ///CAUSA_MODIFICACIÓN   : Debido a que se agrego al  calendario presupuestal el campo Subnivel_Presupuestal
        ///*******************************************************************************
        internal static Boolean Guardar_Historial_Calendario(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            String Usuario_Creo = String.Empty;
            String Fecha_Creo = String.Format("{0:MM/dd/yyyy}", DateTime.Now );
            DataTable Dt_Datos = new DataTable();
            Boolean Operacion_Exitosa = false;
            Double Ene = 0.00;
            Double Feb = 0.00;
            Double Mar = 0.00;
            Double Abr = 0.00;
            Double May = 0.00;
            Double Jun = 0.00;
            Double Jul = 0.00;
            Double Ago = 0.00;
            Double Sep = 0.00;
            Double Oct = 0.00;
            Double Nov = 0.00;
            Double Dic = 0.00;
            Double Precio = 0.00;
            String Area_Funcional_ID = String.Empty;

            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {
                //insertamos los datos de los comentarios
                Mi_SQL.Append("INSERT INTO " + Ope_Psp_Hist_Calendar_Presu.Tabla_Ope_Psp_Hist_Calendar_Presu + "(");
                Mi_SQL.Append(Ope_Psp_Hist_Calendar_Presu.Campo_Anio + ", ");
                if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim())) 
                {
                    Mi_SQL.Append(Ope_Psp_Hist_Calendar_Presu.Campo_Empleado_ID + ", ");
                }
                else if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                {
                    Mi_SQL.Append(Ope_Psp_Hist_Calendar_Presu.Campo_Dependencia_ID + ", ");
                }
                Mi_SQL.Append(Ope_Psp_Hist_Calendar_Presu.Campo_Comentario + ", ");
                Mi_SQL.Append(Ope_Psp_Hist_Calendar_Presu.Campo_Usuario_Creo + ", ");
                Mi_SQL.Append(Ope_Psp_Hist_Calendar_Presu.Campo_Fecha_Creo + ") VALUES(");
                Mi_SQL.Append("'" + Negocio.P_Anio_Presupuesto + "', ");
                if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                {
                    Mi_SQL.Append("'" + Negocio.P_Empleado_ID + "', ");
                }
                else if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                {
                    Mi_SQL.Append("'" + Negocio.P_Dependencia_ID + "', ");
                }
                Mi_SQL.Append("'" + Negocio.P_Comentario + "', ");
                Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "', ");
                Mi_SQL.Append("GETDATE())");
                Cmd.CommandText = Mi_SQL.ToString();
                Cmd.ExecuteNonQuery();

                //obtenemos los datos anteriores del presupuesto
                //eliminamos los datos del presupuesto
                if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                {
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " = '" + Negocio.P_Empleado_ID + "'");
                    Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Campo_Anio + " = '" + Negocio.P_Anio_Presupuesto + "'");
                    Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("DELETE FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " = '" + Negocio.P_Empleado_ID + "'");
                    Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Campo_Anio + " = '" + Negocio.P_Anio_Presupuesto + "'");
                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();
                }
                if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                {
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID + "'");
                    Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Campo_Anio + " = '" + Negocio.P_Anio_Presupuesto + "'");
                    Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " IS NULL ");
                    Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("DELETE FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID + "'");
                    Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Campo_Anio + " = '" + Negocio.P_Anio_Presupuesto + "'");
                    Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " IS NULL ");
                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();
                }

                //obtenemos los datos del usuario creo y fecha creo para mantener el historial del presupuesto
                if(Dt_Datos != null)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        Usuario_Creo = Dt_Datos.Rows[0]["USUARIO_CREO"].ToString().Trim();
                        Fecha_Creo = String.Format("{0:dd/MM/yyyy}", Dt_Datos.Rows[0]["FECHA_CREO"]);
                    }
                }

                //insertams los datos del nuevo presupuesto
                foreach (DataRow Renglon in Negocio.P_Dt_Datos.Rows)
                {
                    Mi_SQL = new StringBuilder();
                    Ene = 0.00;
                    Feb = 0.00;
                    Mar = 0.00;
                    Abr = 0.00;
                    May = 0.00;
                    Jun = 0.00;
                    Jul = 0.00;
                    Ago = 0.00;
                    Sep = 0.00;
                    Oct = 0.00;
                    Nov = 0.00;
                    Dic = 0.00;
                    Precio = 0.00;

                    Precio = Convert.ToDouble(String.IsNullOrEmpty(Renglon["PRECIO"].ToString().Trim()) ? "0" : Renglon["PRECIO"].ToString().Trim());

                    if (Precio > 0)
                    {
                        Ene = Convert.ToDouble(String.IsNullOrEmpty(Renglon["ENERO"].ToString().Trim()) ? "0" : Renglon["ENERO"].ToString().Trim()) / Precio;
                        Feb = Convert.ToDouble(String.IsNullOrEmpty(Renglon["FEBRERO"].ToString().Trim()) ? "0" : Renglon["FEBRERO"].ToString().Trim()) / Precio;
                        Mar = Convert.ToDouble(String.IsNullOrEmpty(Renglon["MARZO"].ToString().Trim()) ? "0" : Renglon["MARZO"].ToString().Trim()) / Precio;
                        Abr = Convert.ToDouble(String.IsNullOrEmpty(Renglon["ABRIL"].ToString().Trim()) ? "0" : Renglon["ABRIL"].ToString().Trim()) / Precio;
                        May = Convert.ToDouble(String.IsNullOrEmpty(Renglon["MAYO"].ToString().Trim()) ? "0" : Renglon["MAYO"].ToString().Trim()) / Precio;
                        Jun = Convert.ToDouble(String.IsNullOrEmpty(Renglon["JUNIO"].ToString().Trim()) ? "0" : Renglon["JUNIO"].ToString().Trim()) / Precio;
                        Jul = Convert.ToDouble(String.IsNullOrEmpty(Renglon["JULIO"].ToString().Trim()) ? "0" : Renglon["JULIO"].ToString().Trim()) / Precio;
                        Ago = Convert.ToDouble(String.IsNullOrEmpty(Renglon["AGOSTO"].ToString().Trim()) ? "0" : Renglon["AGOSTO"].ToString().Trim()) / Precio;
                        Sep = Convert.ToDouble(String.IsNullOrEmpty(Renglon["SEPTIEMBRE"].ToString().Trim()) ? "0" : Renglon["SEPTIEMBRE"].ToString().Trim()) / Precio;
                        Oct = Convert.ToDouble(String.IsNullOrEmpty(Renglon["OCTUBRE"].ToString().Trim()) ? "0" : Renglon["OCTUBRE"].ToString().Trim()) / Precio;
                        Nov = Convert.ToDouble(String.IsNullOrEmpty(Renglon["NOVIEMBRE"].ToString().Trim()) ? "0" : Renglon["NOVIEMBRE"].ToString().Trim()) / Precio;
                        Dic = Convert.ToDouble(String.IsNullOrEmpty(Renglon["DICIEMBRE"].ToString().Trim()) ? "0" : Renglon["DICIEMBRE"].ToString().Trim()) / Precio;
                    }

                    //Obtenemos el area funcional
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT " + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_SQL.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_SQL.Append(" WHERE " + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Renglon["DEPENDENCIA_ID"].ToString() + "'");
                    Cmd.CommandText = Mi_SQL.ToString();
                    Area_Funcional_ID = (String)Cmd.ExecuteScalar();

                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + " (" + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Capitulo_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Producto_ID + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Anio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Justificacion + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Enero + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Febrero + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Marzo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Abril + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Mayo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Junio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Julio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Agosto + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Septiembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Octubre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Noviembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Diciembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Estatus + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Area_Funcional_ID + ", ");
                    if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                    {
                        Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + ", ");
                    }
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID_Creo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Usuario_Modifico + ", ");
                    Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Campo_Fecha_Modifico + ") VALUES (");
                    Mi_SQL.Append("'" + Renglon["DEPENDENCIA_ID"].ToString() + "', ");
                    Mi_SQL.Append("'" + Negocio.P_Fuente_Financiamiento_ID + "',");
                    Mi_SQL.Append("'" + Renglon["PROYECTO_ID"].ToString() + "', ");
                    Mi_SQL.Append("'" + Renglon["CAPITULO_ID"].ToString() + "', ");
                    Mi_SQL.Append("'" + Renglon["PARTIDA_ID"].ToString() + "', ");

                    if (String.IsNullOrEmpty(Renglon["PRODUCTO_ID"].ToString()))
                    {
                        Mi_SQL.Append(" NULL, ");
                    }
                    else
                    {
                        Mi_SQL.Append("'" + Renglon["PRODUCTO_ID"].ToString() + "', ");
                    }

                    Mi_SQL.Append("'" + Negocio.P_Anio_Presupuesto + "',");
                    Mi_SQL.Append("'" + Renglon["JUSTIFICACION"].ToString() + "', ");
                    Mi_SQL.Append(Convert.ToString(Ene) + ", ");
                    Mi_SQL.Append(Convert.ToString(Feb) + ", ");
                    Mi_SQL.Append(Convert.ToString(Mar) + ", ");
                    Mi_SQL.Append(Convert.ToString(Abr) + ", ");
                    Mi_SQL.Append(Convert.ToString(May) + ", ");
                    Mi_SQL.Append(Convert.ToString(Jun) + ", ");
                    Mi_SQL.Append(Convert.ToString(Jul) + ", ");
                    Mi_SQL.Append(Convert.ToString(Ago) + ", ");
                    Mi_SQL.Append(Convert.ToString(Sep) + ", ");
                    Mi_SQL.Append(Convert.ToString(Oct) + ", ");
                    Mi_SQL.Append(Convert.ToString(Nov) + ", ");
                    Mi_SQL.Append(Convert.ToString(Dic) + ", ");
                    Mi_SQL.Append(Renglon["ENERO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["FEBRERO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["MARZO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["ABRIL"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["MAYO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["JUNIO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["JULIO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["AGOSTO"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["SEPTIEMBRE"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["OCTUBRE"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["NOVIEMBRE"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["DICIEMBRE"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append(Renglon["IMPORTE_TOTAL"].ToString().Replace(",", "") + ", ");
                    Mi_SQL.Append("'" + Negocio.P_Estatus + "',");
                    Mi_SQL.Append("'" + Area_Funcional_ID + "',");
                    if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID.Trim()))
                    {
                        Mi_SQL.Append("'" + Negocio.P_Empleado_ID + "',");
                    }
                    Mi_SQL.Append("'" + Usuario_Creo + "',");
                    Mi_SQL.Append("'" + Negocio.Usuario_ID_Creo + "',");
                    Mi_SQL.Append("'" + Fecha_Creo + "',");
                    Mi_SQL.Append("'" + Negocio.P_Usuario_Modifico + "',");
                    Mi_SQL.Append("GETDATE())");

                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();
                }

                Trans.Commit();
                Operacion_Exitosa = true;
            }
            catch (Exception Ex)
            {
                Operacion_Exitosa = false;
                Trans.Rollback();
                throw new Exception("Error al intentar guardar el historial de los registros. Error: [" + Ex.Message + "]");
            }
            return Operacion_Exitosa;
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Dependencias_Presupuestadas
        ///DESCRIPCIÓN          : consulta para obtener las dependencias presupuestadas
        ///PARAMETROS           1 Negocio: conexion con la capa de negocios
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 23/Noviembre/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Comentarios(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append("SELECT " + Ope_Psp_Hist_Calendar_Presu.Campo_Comentario);
                Mi_SQL.Append(" FROM " + Ope_Psp_Hist_Calendar_Presu.Tabla_Ope_Psp_Hist_Calendar_Presu);

                if (!String.IsNullOrEmpty(Negocio.P_Anio_Presupuesto))
                {
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Hist_Calendar_Presu.Campo_Anio + " = " + Negocio.P_Anio_Presupuesto);
                }
                else 
                {
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Hist_Calendar_Presu.Campo_Anio + " IS NULL");
                }

                if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                {
                    Mi_SQL.Append(" AND " + Ope_Psp_Hist_Calendar_Presu.Campo_Dependencia_ID + " = '" + Negocio.P_Dependencia_ID + "'");
                
                }
                else
                {
                    Mi_SQL.Append(" AND " + Ope_Psp_Hist_Calendar_Presu.Campo_Dependencia_ID + " IS NULL ");
                
                }

                if (!String.IsNullOrEmpty(Negocio.P_Empleado_ID))
                {
                    Mi_SQL.Append(" AND " + Ope_Psp_Hist_Calendar_Presu.Campo_Empleado_ID + " = '" + Negocio.P_Empleado_ID + "'");
                }
                else
                {
                    Mi_SQL.Append(" AND " + Ope_Psp_Hist_Calendar_Presu.Campo_Empleado_ID + " IS NULL");
                }

                
                Mi_SQL.Append(" ORDER BY " + Ope_Psp_Hist_Calendar_Presu.Campo_Fecha_Creo + " DESC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las partidas asignadas. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Accesos_Calendario_Usuario
        ///DESCRIPCIÓN          : consulta para obtener si el usuario puede calendarizar el presupuesto de las dependencias
        ///PARAMETROS           :
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 28/Febrero/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Accesos_Calendario_Usuario(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las partidas asignadas. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partida_Asignadas_Anteriores
        ///DESCRIPCIÓN          : consulta para obtener las partidas asignadas  del año pasado
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 12/Mayo/2012
        ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
        ///FECHA_MODIFICO       : 22/Agosto/2012
        ///CAUSA_MODIFICACIÓN   : Se agrego el campo subnvel presupuestal a la calendarizacion
        ///*******************************************************************************
        internal static DataTable Consultar_Partida_Asignadas_Anteriores(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            int Anio;
            try
            {
                //obtenemos el año que estamos presupuestando
                Mi_SQL.Append("SELECT " + Cat_Psp_Parametros.Campo_Anio_Presupuestar + " FROM " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros);
                Mi_SQL.Append(" WHERE " + Cat_Psp_Parametros.Campo_Estatus + " = 'ACTIVO'");

                Anio = Convert.ToInt32(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()))-1;

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append(" SELECT " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID + " AS PROYECTO_ID, ");
                Mi_SQL.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + ", ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID + ", ");
                //CAMBIO
                Mi_SQL.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Subnivel_Presupuestal_ID+ ", 0) AS SUBNIVEL_ID, ");
                //
                Mi_SQL.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Producto_ID + ", '') AS PRODUCTO_ID, ");
                //obtenemos el precio
                Mi_SQL.Append("(CASE ");
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Enero + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Enero);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Febrero + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Febrero);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Marzo + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Marzo);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Abril + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Abril);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Mayo + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Mayo);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Junio + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Junio);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Julio + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Julio);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Agosto + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Agosto);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Septiembre + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Septiembre);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Octubre + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Octubre);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Noviembre + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Noviembre);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Diciembre + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Diciembre);
                Mi_SQL.Append(" ELSE 0 END) AS PRECIO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Justificacion + ", ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS CLAVE_PARTIDA, ");
                Mi_SQL.Append("ISNULL(" + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Clave + ", '') AS CLAVE_PRODUCTO, ");
                Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS UR, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero + " AS ENERO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero + " AS FEBRERO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo + " AS MARZO , ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril + " AS ABRIL, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo + " AS MAYO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio + " AS JUNIO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio + " AS JULIO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto + " AS AGOSTO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre + " AS SEPTIEMBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre + " AS OCTUBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre + " AS NOVIEMBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre + " AS DICIEMBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + " AS IMPORTE_TOTAL, '' AS ID ");
                Mi_SQL.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Producto_ID);
                Mi_SQL.Append(" = " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Producto_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica);
                Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                Mi_SQL.Append(" ON " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                Mi_SQL.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                Mi_SQL.Append(" = " + Anio);

                if (!string.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " IS NULL ");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " IS NULL ");
                    }
                }

                if (!string.IsNullOrEmpty(Negocio.P_Empleado_ID))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Empleado_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Empleado_ID + "'");
                    }
                }

                if (!string.IsNullOrEmpty(Negocio.P_Estatus))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_SQL.Append(" = '" + Negocio.P_Estatus + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_SQL.Append(" = '" + Negocio.P_Estatus + "'");
                    }
                }
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las partidas asignadas. Error: [" + Ex.Message + "]");
            }
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Programas_Unidades_Responsables
        ///DESCRIPCIÓN          : consulta para obtener el sunivel presupuestal en caso de que exista
        ///                       asociado a una unidad responsable, fuente de finaciamiento, programa,
        ///                       partida especifica y  a una area funcional
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 21/Agosto/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Sunbivel_Presupuetal(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                DataTable Dt_Subnivel = new DataTable();
                try
                { 

                    //OBTENEMOS LOS SUBNIVELES PRESUPUESTALES
                    Mi_Sql.Append("SELECT " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID+ ", ");
                    Mi_Sql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal+ " + ' ' + ");
                    Mi_Sql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Descripcion + " AS NOMBRE ");
                    Mi_Sql.Append(" FROM " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos);
                    Mi_Sql.Append(" WHERE " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                    Mi_Sql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Proyecto_Programa_ID + " = '" + Negocio.P_Programa_ID + "' ");
                    Mi_Sql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_FTE_Fincanciamiento_ID + " = '" + Negocio.P_Fuente_Financiamiento_ID + "' ");
                    Mi_Sql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Partida_ID + " = '"  + Negocio.P_Partida_ID + "' ");
                    Mi_Sql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Estatus+ " = 'ACTIVO' ");
                  // Mi_Sql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Area_Funcional_ID + " = " + Negocio.P_Area_Funcional_ID);
                    if (!String.IsNullOrEmpty(Negocio.P_Subnivel_Clave)) 
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal + " = '" + Negocio.P_Subnivel_Clave + "' ");
                    }
                    Mi_Sql.Append(" ORDER BY  NOMBRE ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0]; ;
                }
                catch (Exception Ex)
                {
                    Ex.ToString();
                    return null;
                }
            }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Unidad_Responsable_Carga_Masiva
        ///DESCRIPCIÓN          : consulta para obtener los datos de las unidad responsable para compararlos con
        ///                       cada registro a ingresar de la carga masiva
        ///PARAMETROS           : 
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 05/Septiembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Unidad_Responsable_Carga_Masiva(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +' '+ ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS NOMBRE, ");
                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_Sql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal);
                Mi_Sql.Append(" ON " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Dependencia_ID);
                Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros);
                Mi_Sql.Append(" ON " + Ope_Psp_Limite_presupuestal.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Limite_presupuestal.Campo_Anio_presupuestal);
                Mi_Sql.Append(" = " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Anio_Presupuestar);
                Mi_Sql.Append(" AND " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Estatus + " = 'ACTIVO'");
                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " = '" + Negocio.P_Dependencia_Clave + "' ");
                Mi_Sql.Append(" ORDER BY " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las unidades responsables. Error: [" + Ex.Message + "]");
            }
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partidas_Especificas_Capitulo_Carga_Masiva
        ///DESCRIPCIÓN          : consulta para obtener los capitulos y partidas 
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 05/Septiembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Partidas_Especificas_Capitulo_Carga_Masiva(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append("SELECT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + "+' " + " " + "'+ ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS NOMBRE, ");
                Mi_SQL.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                Mi_SQL.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + ", ");
                Mi_SQL.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + ", ");
                Mi_SQL.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + ", ");
                Mi_SQL.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + ", ");
                Mi_SQL.Append(Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal+ ", ");
                Mi_SQL.Append(Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros);
                Mi_SQL.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                Mi_SQL.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                Mi_SQL.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                Mi_SQL.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                Mi_SQL.Append(" = " + Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Detalle_Lim_Presup.Campo_Capitulo_ID);
                Mi_SQL.Append(" AND " +  Ope_Psp_Detalle_Lim_Presup.Tabla_Ope_Psp_Limite_presupuestal + "." + Ope_Psp_Detalle_Lim_Presup.Campo_Anio_presupuestal);
                Mi_SQL.Append(" = " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Anio_Presupuestar);
                Mi_SQL.Append(" AND " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Estatus + " = 'ACTIVO'");
                Mi_SQL.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Estatus + " = 'ACTIVO'");
                Mi_SQL.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " = '" + Negocio.P_Partida_Clave + "'");
                Mi_SQL.Append(" ORDER BY " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " ASC");

               

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las partidas especificas. Error: [" + Ex.Message + "]");
            }
        }
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Fuente_Financiamiento_Carga_Masiva
        ///DESCRIPCIÓN          : Consulta para revizar si la fuente de financiamiento de la carga masiva
        ///                       existe
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 06/Septiembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Fuente_Financiamiento_Carga_Masiva(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                DataTable Dt_Subnivel = new DataTable();
                try
                { 
                    //OBTENEMOS EL ID DE LA FUENTE DE FINANCIAMIENTO
                    Mi_Sql.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID );
                    Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Clave  + " = '" + Negocio.P_Fuente_Financiamiento_Clave + "'" );
                
                    if (!String.IsNullOrEmpty(Negocio.P_Fuente_Financiamiento_ID)) 
                    {
                        Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = '" + Negocio.P_Fuente_Financiamiento_ID + "' ");
                    }

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0]; ;
                }
                catch (Exception Ex)
                {
                    Ex.ToString();
                    return null;
                }
            }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Email_Empleado_Creo
        ///DESCRIPCIÓN          : consulta para obtener el correo del usuario que calendarizo
        ///PARAMETROS           :
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 02/Marzo/2013 12:24 pm
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Email_Empleado_Creo(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append("SELECT ISNULL(" + Cat_Empleados.Campo_Correo_Electronico + ", '') AS EMAIL, ");
                Mi_SQL.Append(" ISNULL(" + Cat_Empleados.Campo_Nombre + ", '') + ' ' + ");
                Mi_SQL.Append(" ISNULL(" + Cat_Empleados.Campo_Apellido_Paterno + ", '') + ' ' + ");
                Mi_SQL.Append(" ISNULL(" + Cat_Empleados.Campo_Apellido_Materno + ", '') AS NOMBRE, ");
                Mi_SQL.Append(" 'EMPLEADO' AS TIPO");
                Mi_SQL.Append(" FROM " + Cat_Empleados.Tabla_Cat_Empleados );
                Mi_SQL.Append(" WHERE " + Cat_Empleados.Campo_Empleado_ID + " = '" + Negocio.P_Empleado_ID.Trim() + "'");

                Mi_SQL.Append(" UNION ");

                Mi_SQL.Append("SELECT ISNULL(" + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Correo_Electronico + ",'') AS EMAIL, ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Nombre + " + ' ' + ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Paterno + " + ' ' + ");
                Mi_SQL.Append(Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Apellido_Materno + " AS NOMBRE, ");
                Mi_SQL.Append(" 'COORDINADOR_PSP' AS TIPO ");
                Mi_SQL.Append(" FROM " + Cat_Organigrama.Tabla_Cat_Organigrama);
                Mi_SQL.Append(" INNER JOIN " + Cat_Empleados.Tabla_Cat_Empleados);
                Mi_SQL.Append(" ON " + Cat_Organigrama.Tabla_Cat_Organigrama + "." + Cat_Organigrama.Campo_Empleado_ID);
                Mi_SQL.Append(" = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);
                Mi_SQL.Append(" WHERE " + Cat_Organigrama.Tabla_Cat_Organigrama + "." + Cat_Organigrama.Campo_Tipo);
                Mi_SQL.Append(" = 'COORDINADOR PRESUPUESTOS'");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar el correo del usuario que calendarizo. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partida_Asignadas_Reporte
        ///DESCRIPCIÓN          : consulta para obtener las partidas asignadas  para el reporte
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 20/Junio/2013
        ///MODIFICO             : 
        ///FECHA_MODIFICO       : 
        ///CAUSA_MODIFICACIÓN   : 
        ///*******************************************************************************
        internal static DataTable Consultar_Partida_Asignadas_Reporte(Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append(" SELECT ");
                Mi_SQL.Append(" LTRIM(RTRIM(" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ")) AS FUENTE_FINANCIAMIENTO, ");
                Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS DESCRIPCION_FUENTE_FINANCIAMIENTO, ");
                Mi_SQL.Append(" LTRIM(RTRIM(" + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ")) AS AREA_FUNCIONAL, ");
                Mi_SQL.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS DESCRIPCION_AREA_FUNCIONAL, ");
                Mi_SQL.Append(" LTRIM(RTRIM(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ")) AS PROGRAMA, ");
                Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS DESCRIPCION_PROGRAMA, ");
                Mi_SQL.Append(" LTRIM(RTRIM(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ")) AS UNIDAD_RESPONSABLE, ");
                Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DESCRIPCION_UNIDAD_RESPONSABLE, ");
                Mi_SQL.Append(" LTRIM(RTRIM(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ")) AS PARTIDA, ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS DESCRIPCION_PARTIDA, ");
                Mi_SQL.Append(" LTRIM(RTRIM(" + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Clave + ")) AS PRODUCTO_CLAVE, ");
                Mi_SQL.Append(Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Nombre + " AS PRODUCTO_DESCRIPCION, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Justificacion + " AS JUSTIFICACION, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero + " AS ENERO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero + " AS FEBRERO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo + " AS MARZO , ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril + " AS ABRIL, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo + " AS MAYO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio + " AS JUNIO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio + " AS JULIO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto + " AS AGOSTO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre + " AS SEPTIEMBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre + " AS OCTUBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre + " AS NOVIEMBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre + " AS DICIEMBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + " AS TOTAL");
                Mi_SQL.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Producto_ID);
                Mi_SQL.Append(" = " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Producto_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Area_Funcional_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                Mi_SQL.Append(" = " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Anio_Presupuestar);
                Mi_SQL.Append(" AND " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Estatus + " = 'ACTIVO'");

                if (!string.IsNullOrEmpty(Negocio.P_Dependencia_ID))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " IS NULL ");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Dependencia_ID + "'");
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID + " IS NULL ");
                    }
                }

                if (!string.IsNullOrEmpty(Negocio.P_Empleado_ID))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Empleado_ID + "'");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID);
                        Mi_SQL.Append(" = '" + Negocio.P_Empleado_ID + "'");
                    }
                }

                if (!string.IsNullOrEmpty(Negocio.P_Estatus))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_SQL.Append(" IN(" + Negocio.P_Estatus + ")");
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_SQL.Append(" IN(" + Negocio.P_Estatus + ")");
                    }
                }
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las partidas asignadas. Error: [" + Ex.Message + "]");
            }
        }
    }
}