﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Reporte_Presupuesto_Egresos.Negocio;
using JAPAMI.Grupos_Dependencias.Negocio;

namespace JAPAMI.Reporte_Presupuesto_Egresos.Datos
{
    public class Cls_Rpt_Presupuesto_Egresos_Datos
    {
        #region(Metodos)
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Dependencias
        /// COMENTARIOS:    Se realiza una consulta de las dependencias que tienen el cierto grupo de dependnecia id
        /// PARÁMETROS:     
        /// USUARIO CREÓ:   Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:     05/Diciembre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Dependencias(Cls_Rpt_Presupuesto_Egresos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            DataTable Dt_Presupuesto = new DataTable();
            try
            {
                Mi_SQL.Append("Select * from " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_SQL.Append( " Where " +Cat_Dependencias.Campo_Grupo_Dependencia_ID +"='" +Datos.P_Grupo_Dependencia_ID +"'");
                Dt_Presupuesto = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
            return Dt_Presupuesto;
        }

        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Presupuesto_Dependencias
        /// COMENTARIOS:    Se realiza una consulta de las dependencias que tienen en el presupuesto
        /// PARÁMETROS:     
        /// USUARIO CREÓ:   Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:     05/Diciembre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Presupuesto_Dependencias(Cls_Rpt_Presupuesto_Egresos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            DataTable Dt_Presupuesto = new DataTable();
            try
            {
                Mi_SQL.Append("Select * from " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_SQL.Append(" Where " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + "='" + Datos.P_Dependencia_ID + "'");
                Mi_SQL.Append(" and " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + "=" + Datos.P_Anio);
                Dt_Presupuesto = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
            return Dt_Presupuesto;
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Presupuesto_Programa
        /// COMENTARIOS:    Se realiza una consulta de los programas que tienen en el presupuesto
        /// PARÁMETROS:     
        /// USUARIO CREÓ:   Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:     07/Diciembre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Presupuesto_Programa(Cls_Rpt_Presupuesto_Egresos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            DataTable Dt_Presupuesto = new DataTable();
            try
            {
                Mi_SQL.Append("Select * from " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_SQL.Append(" Where " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + "='" + Datos.P_Programa_ID + "'");
                Mi_SQL.Append(" and " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + "=" + Datos.P_Anio);
                Dt_Presupuesto = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
            return Dt_Presupuesto;
        }
        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Presupuesto_Partida
        /// COMENTARIOS:    Se realiza una consulta de las PARTIDAS que tienen se encuentren dentro del presupuesto
        /// PARÁMETROS:     
        /// USUARIO CREÓ:   Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:     07/Diciembre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Presupuesto_Partida(Cls_Rpt_Presupuesto_Egresos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            DataTable Dt_Presupuesto = new DataTable();
            try
            {
                Mi_SQL.Append("Select * from " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_SQL.Append(" Where " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + "='" + Datos.P_Partida_ID + "'");
                Mi_SQL.Append(" and " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + "=" + Datos.P_Anio);
                Dt_Presupuesto = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
            return Dt_Presupuesto;
        }

        /// ********************************************************************************************************************
        /// NOMBRE:         Consultar_Presupuesto_Partida
        /// COMENTARIOS:    Se realiza una consulta de las PARTIDAS que tienen se encuentren dentro del presupuesto
        /// PARÁMETROS:     
        /// USUARIO CREÓ:   Hugo Enrique Ramirez Aguilera
        /// FECHA CREÓ:     07/Diciembre/2011 
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA DE LA MODIFICACIÓN:
        /// ********************************************************************************************************************
        public static DataTable Consultar_Presupuesto_Año(Cls_Rpt_Presupuesto_Egresos_Negocio Datos)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
            DataTable Dt_Presupuesto = new DataTable();
            try
            {
                Mi_SQL.Append("Select distinct " +Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                Mi_SQL.Append(" from " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                Mi_SQL.Append(" Order by " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " DESC");
                Dt_Presupuesto = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
            }
            return Dt_Presupuesto;
        }
        #endregion

        #region Metodos Aprobados
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Tipo_Reporte
            ///DESCRIPCIÓN          : consulta para obtener los datos del numero de modificaciones del presupuesto
            ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 28/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Tipo_Reporte(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT DISTINCT 'MODIFICACION ' + ' ' + ");
                    Mi_Sql.Append(" CONVERT(VARCHAR," + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + ") AS MODIFICACION , ");
                    Mi_Sql.Append("CONVERT(VARCHAR," + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + ") AS NO_MOVIMIENTO ");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Egr_Det.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + " IS NOT NULL ");

                    Mi_Sql.Append(" ORDER BY CONVERT(VARCHAR," + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr + ") ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las clases. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Dependencia_PSP
            ///DESCRIPCIÓN          : consulta para obtener los datos de las dependencias
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Datos_Dependencia_PSP(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();

                try
                {
                    Mi_Sql.Append("SELECT ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS AF, '' AS CA, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + "  AS PP,");
                    Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Clave + " AS CODIGO, ");
                    Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + " AS DEPENDENCIA, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'CONCEPTO' AS TIPO");
                    Mi_Sql.Append(" FROM " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " ON " +
                        Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID +
                        " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento +
                        " ON " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID +
                        " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas +
                       " ON " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                       " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                    }

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());
                    Mi_Sql.Append(" AND " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre);
                    Mi_Sql.Append(" NOT IN('DIRECCION GENERAL')");
                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + ", ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);

                    Mi_Sql.Append(" UNION ");

                    Mi_Sql.Append("SELECT ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS AF, '' AS CA, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + "  AS PP,");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " AS CODIGO, ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'CONCEPTO' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias);
                    Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " ON " +
                        Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID +
                        " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento +
                        " ON " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID +
                        " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas +
                       " ON " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                       " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());
                    Mi_Sql.Append(" AND " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre);
                    Mi_Sql.Append(" IN('DIRECCION GENERAL')");
                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ", ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);

                    Mi_Sql.Append(" UNION ");

                    Mi_Sql.Append("SELECT '' AS FF, '' AS AF, '' AS CA, '' AS PP, '' AS CODIGO, 'TOTAL PRESUPUESTO' AS DEPENDENCIA, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'TOTAL' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" ORDER BY CODIGO ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las depencencias. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Dependencia_CAL
            ///DESCRIPCIÓN          : consulta para obtener los datos de las DEPENDENCIAS
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Datos_Dependencia_CAL(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();

                try
                {
                    Mi_Sql.Append("SELECT ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS AF, '' AS CA, ");
                    Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Clave + " AS CODIGO, ");
                    Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + " AS DEPENDENCIA, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'CONCEPTO' AS TIPO ");
                    Mi_Sql.Append(" FROM " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_Sql.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " ON " +
                        Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID +
                        " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + 
                        " ON " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID +
                        " = " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                    Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                        Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                    }

                    Mi_Sql.Append(Mi_Sql_Filtros.ToString().Trim());
                    Mi_Sql.Append(" AND " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre);
                    Mi_Sql.Append(" NOT IN('DIRECCION GENERAL')");
                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + ", ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave);

                    Mi_Sql.Append(" UNION ");

                    Mi_Sql.Append("SELECT ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS AF, ''  AS CA, ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " AS CODIGO, ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'CONCEPTO' AS TIPO ");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias);
                    Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " ON " +
                        Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID +
                        " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento +
                        " ON " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID +
                        " = " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());
                    Mi_Sql.Append(" AND " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre);
                    Mi_Sql.Append(" IN('DIRECCION GENERAL')");
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    }
                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ", ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave);

                    Mi_Sql.Append(" UNION ");
                    Mi_Sql.Append("SELECT  '' AS FF, '' AS AF, '' AS CA, '' AS CODIGO, 'TOTAL PRESUPUESTO' AS DEPENDENCIA, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'TOTAL' AS TIPO  ");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + " ");

                    Mi_Sql.Append(Mi_Sql_Filtros.ToString().Trim());

                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    }
                    else 
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    }

                    Mi_Sql.Append(" ORDER BY CODIGO ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las depencencias. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Unidad_Responsable_PSP
            ///DESCRIPCIÓN          : consulta para obtener los datos de las unidades responsables
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 28/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Datos_UR_PSP(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();

                try
                {
                    Mi_Sql.Append("SELECT ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS AF, '' AS CA, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + "  AS PP,");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " AS CODIGO, ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado+", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'CONCEPTO' AS TIPO");
                    Mi_Sql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " ON " +
                        Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID +
                        " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento +
                        " ON " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID +
                        " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas +
                       " ON " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                       " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                    }

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre+ ", ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);

                    Mi_Sql.Append(" UNION ");
                    Mi_Sql.Append("SELECT '' AS FF, '' AS AF, '' AS CA, '' AS PP, '' AS CODIGO, 'TOTAL PRESUPUESTO' AS DEPENDENCIA, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'TOTAL' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" ORDER BY CODIGO ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las depencencias. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Unidad_Responsable_CAL
            ///DESCRIPCIÓN          : consulta para obtener los datos de las unidades responsables
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Datos_UR_CAL(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();

                try
                {
                    Mi_Sql.Append("SELECT ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS AF, '' AS CA, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + "  AS PP,");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " AS CODIGO, ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu .Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'CONCEPTO' AS TIPO");
                    Mi_Sql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_Sql.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                    Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " ON " +
                        Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID +
                        " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento +
                        " ON " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID +
                        " = " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas +
                       " ON " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id +
                       " = " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                        Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                    }

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ", ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave);

                    Mi_Sql.Append(" UNION ");
                    Mi_Sql.Append("SELECT '' AS FF, '' AS AF, '' AS CA, '' AS PP,'' AS CODIGO, 'TOTAL PRESUPUESTO' AS DEPENDENCIA, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'TOTAL' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    }

                    Mi_Sql.Append(" ORDER BY CODIGO ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las depencencias. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Programas_PSP
            ///DESCRIPCIÓN          : consulta para obtener los datos de los programas
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Datos_Programas_PSP(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();

                try
                {
                    Mi_Sql.Append("SELECT ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS AF, '' AS CA, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS CODIGO, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS PROGRAMA, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'CONCEPTO' AS TIPO");
                    Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias +
                      " ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID +
                      " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " ON " +
                        Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID +
                        " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento +
                        " ON " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID +
                        " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                    }

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + ", ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave);

                    Mi_Sql.Append(" UNION ");
                    Mi_Sql.Append("SELECT '' AS FF, '' AS AF, '' AS CA,'' AS CODIGO, 'TOTAL PRESUPUESTO' AS PROGRAMA, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'TOTAL' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" ORDER BY CODIGO ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los programas. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Programas_CAL
            ///DESCRIPCIÓN          : consulta para obtener los datos de los programas
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Datos_Programas_CAL(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();

                try
                {
                    Mi_Sql.Append("SELECT ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS AF, '' AS CA, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS CODIGO, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS PROGRAMA, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'CONCEPTO' AS TIPO");
                    Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_Sql.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                    Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias  + " ON " +
                        Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID +
                        " = " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + " ON " +
                       Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID +
                       " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento +
                        " ON " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID +
                        " = " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                        Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                    }

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + ", ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave);

                    Mi_Sql.Append(" UNION ");
                    Mi_Sql.Append("SELECT '' AS FF, '' AS AF, '' AS CA,'' AS CODIGO, 'TOTAL PRESUPUESTO' AS PROGRAMA, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'TOTAL' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    }

                    Mi_Sql.Append(" ORDER BY CODIGO ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los programas. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Fte_Financiamiento
            ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Datos_Fte_Financiamiento_PSP(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();

                try
                {
                    Mi_Sql.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS CODIGO, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FUENTE_FINANCIAMIENTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'CONCEPTO' AS TIPO");
                    Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                    }

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion);

                    Mi_Sql.Append(" UNION ");
                    Mi_Sql.Append("SELECT '' AS CODIGO, 'TOTAL PRESUPUESTO' AS FUENTE_FINANCIAMIENTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'TOTAL' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" ORDER BY CODIGO ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Fte_Financiamiento_CAL
            ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentees de financiamiento
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Datos_Fte_Financiamiento_CAL(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();

                try
                {
                    Mi_Sql.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS CODIGO, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FUENTE_FINANCIAMIENTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'CONCEPTO' AS TIPO");
                    Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_Sql.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                    Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                        Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                    }

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion);

                    Mi_Sql.Append(" UNION ");
                    Mi_Sql.Append("SELECT '' AS CODIGO, 'TOTAL PRESUPUESTO' AS FUENTE_FINANCIAMIENTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'TOTAL' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    }

                    Mi_Sql.Append(" ORDER BY CODIGO ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Partida
            ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Datos_Partida_PSP(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();

                try
                {
                    Mi_Sql.Append("SELECT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS CODIGO, ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CONCEPTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", 0)) AS PSP_EGRESO_AUTORIZADO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", 0)) AS TRANSFERENCIAS_AUMENTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", 0)) AS TRANSFERENCIAS_DISMINUCION, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'CONCEPTO' AS TIPO");
                    Mi_Sql.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                    }

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());
                    
                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre);

                    Mi_Sql.Append(" UNION ");

                    Mi_Sql.Append("SELECT " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " AS CODIGO, ");
                    Mi_Sql.Append(" 'TOTAL' + ' ' +" + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + " AS CONCEPTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", 0)) AS PSP_EGRESO_AUTORIZADO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", 0)) AS TRANSFERENCIAS_AUMENTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", 0)) AS TRANSFERENCIAS_DISMINUCION, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'TOTAL' AS TIPO");
                    Mi_Sql.Append(" FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion);

                    Mi_Sql.Append(" UNION ");
                    Mi_Sql.Append("SELECT '' AS CODIGO, 'TOTAL PRESUPUESTO' AS CONCEPTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", 0)) AS PSP_EGRESO_AUTORIZADO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", 0)) AS TRANSFERENCIAS_AUMENTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", 0)) AS TRANSFERENCIAS_DISMINUCION, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'TOTAL_PSP' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" ORDER BY CODIGO ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las partidas. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Partida_CAL
            ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Datos_Partida_CAL(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();

                try
                {
                    Mi_Sql.Append("SELECT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS CODIGO, ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CONCEPTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'CONCEPTO' AS TIPO");
                    Mi_Sql.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_Sql.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_Sql.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                    Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                        Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                    }

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre);

                    Mi_Sql.Append(" UNION ");
                    Mi_Sql.Append("SELECT " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " AS CODIGO, ");
                    Mi_Sql.Append(" 'TOTAL' + ' ' +" + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + " AS CONCEPTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu  + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'TOTAL' AS TIPO");
                    Mi_Sql.Append(" FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_Sql.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Capitulo_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    }

                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion);

                    Mi_Sql.Append(" UNION ");
                    Mi_Sql.Append("SELECT '' AS CODIGO, 'TOTAL PRESUPUESTO' AS CONCEPTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", 0)) AS PRESUPUESTO_EGRESO, ");
                    Mi_Sql.Append("'TOTAL_PSP' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                        Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");
                    }
                    
                    Mi_Sql.Append(" ORDER BY CODIGO ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las partidas. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Analitico_PSP
            ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas detallados
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Datos_Analitico_PSP(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();

                try
                {
                    Mi_Sql.Append("SELECT ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + '-' + ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " + '-31120-' + ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave +" + '-' + ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave +" + '-' + ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave +"  AS CODIGO_PROGRAMATICO, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS SF, '' AS CA, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS PROGRAMA, ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " AS UR, ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS PARTIDA, ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CONCEPTO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", '0') AS PRESUPUESTO_APROBADO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion+ ", '0') AS AMPLIACION, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", '0') AS REDUCCION, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", '0') AS MODIFICADO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", '0') AS DEVENGADO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ", '0') AS EJERCIDO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", '0') AS PAGADO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", '0') AS PRE_COMPROMETIDO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", '0') AS COMPROMETIDO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", '0') AS DISPONIBLE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", '0') - ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Enero + ", '0') AS ENERO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", '0') - ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Febrero + ", '0') AS FEBRERO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", '0') - ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Marzo + ", '0') AS MARZO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", '0') - ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Abril + ", '0') AS ABRIL, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", '0') - ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Mayo+ ", '0') AS MAYO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", '0') - ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Junio  + ", '0') AS JUNIO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", '0') - ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Julio + ", '0') AS JULIO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", '0') - ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Agosto + ", '0') AS AGOSTO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", '0') - ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Septiembre + ", '0') AS SEPTIEMBRE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", '0') - ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Octubre + ", '0') AS OCTUBRE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", '0')  - ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Noviembre + ", '0') AS NOVIEMBRE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", '0') - ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Diciembre + ", '0') AS DICIEMBRE, ");
                    Mi_Sql.Append("'CONCEPTO' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                    Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento );
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);


                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                        Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                    {
                        if (!Mi_Sql_Filtros.ToString().Contains("WHERE"))
                        {
                            Mi_Sql_Filtros.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                        }
                    }

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" UNION ");

                    Mi_Sql.Append("SELECT ''  AS CODIGO_PROGRAMATICO, '' AS FF, '' AS SF, '' AS CA, '' AS PROGRAMA, ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " AS UR, ");
                    Mi_Sql.Append(" '' AS PARTIDA, ");
                    Mi_Sql.Append("'TOTAL' + ' ' + " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS CONCEPTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", '0')) AS PRESUPUESTO_APROBADO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", '0')) AS AMPLIACION, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", '0')) AS REDUCCION, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", '0')) AS MODIFICADO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", '0')) AS DEVENGADO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ", '0')) AS EJERCIDO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", '0')) AS PAGADO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", '0')) AS PRE_COMPROMETIDO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", '0')) AS COMPROMETIDO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", '0')) AS DISPONIBLE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Enero + ", '0')) AS ENERO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Febrero + ", '0')) AS FEBRERO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Marzo + ", '0')) AS MARZO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Abril + ", '0')) AS ABRIL, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Mayo + ", '0')) AS MAYO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Junio + ", '0')) AS JUNIO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Julio + ", '0')) AS JULIO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Agosto + ", '0')) AS AGOSTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Septiembre + ", '0')) AS SEPTIEMBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Octubre + ", '0')) AS OCTUBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", '0'))  - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Noviembre + ", '0')) AS NOVIEMBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Diciembre + ", '0')) AS DICIEMBRE, ");
                    Mi_Sql.Append(" 'TOTAL' AS TIPO ");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ", ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);

                    Mi_Sql.Append(" UNION ");

                    Mi_Sql.Append("SELECT ''  AS CODIGO_PROGRAMATICO, '' AS FF, '' AS SF, '' AS CA, '' AS PROGRAMA, '' AS UR,  '' AS PARTIDA, 'TOTAL PRESUPUESTO' AS CONCEPTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", '0')) AS PRESUPUESTO_APROBADO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", '0')) AS AMPLIACION, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", '0')) AS REDUCCION, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", '0')) AS MODIFICADO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", '0')) AS DEVENGADO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ", '0')) AS EJERCIDO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", '0')) AS PAGADO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", '0')) AS PRE_COMPROMETIDO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", '0')) AS COMPROMETIDO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", '0')) AS DISPONIBLE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Enero + ", '0')) AS ENERO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Febrero + ", '0')) AS FEBRERO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Marzo + ", '0')) AS MARZO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Abril + ", '0')) AS ABRIL, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Mayo + ", '0')) AS MAYO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Junio + ", '0')) AS JUNIO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Julio + ", '0')) AS JULIO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Agosto + ", '0')) AS AGOSTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Septiembre + ", '0')) AS SEPTIEMBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Octubre + ", '0')) AS OCTUBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", '0'))  - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Noviembre + ", '0')) AS NOVIEMBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", '0')) - ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Afectado_Diciembre + ", '0')) AS DICIEMBRE, ");
                    Mi_Sql.Append(" 'TOTAL_PSP' AS TIPO ");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" ORDER BY  UR, PARTIDA ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las partidas detallado. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Analitico_CAL
            ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas detallados
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Datos_Analitico_CAL(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();

                try
                {
                    Mi_Sql.Append("SELECT ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + '-' + ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " + '-31120-' + ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + '-' + ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + '-' + ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + "  AS CODIGO_PROGRAMATICO, ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS SF, '' AS CA, ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS PROGRAMA, ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " AS UR, ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS PARTIDA, ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CONCEPTO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", '0') AS TOTAL, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero + ", '0') AS ENERO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero + ", '0') AS FEBRERO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo + ", '0') AS MARZO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril + ", '0') AS ABRIL, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo + ", '0') AS MAYO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio + ", '0') AS JUNIO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio + ", '0') AS JULIO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto + ", '0') AS AGOSTO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre + ", '0') AS SEPTIEMBRE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre + ", '0') AS OCTUBRE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre + ", '0') AS NOVIEMBRE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre + ", '0') AS DICIEMBRE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", '0') AS TOTAL_PSP, ");
                    Mi_Sql.Append("'CONCEPTO' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_Sql.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                    Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                    Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");

                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                        Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                        Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                        Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                        Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                        Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                    }

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" UNION ");

                    Mi_Sql.Append("SELECT ''  AS CODIGO_PROGRAMATICO, '' AS FF, '' AS SF, '' AS CA, '' AS PROGRAMA, ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " AS UR, ");
                    Mi_Sql.Append(" '' AS PARTIDA, ");
                    Mi_Sql.Append("'TOTAL' + ' ' + " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS CONCEPTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", '0')) AS TOTAL, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero + ", '0')) AS ENERO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero + ", '0')) AS FEBRERO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo + ", '0')) AS MARZO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril + ", '0')) AS ABRIL, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo + ", '0')) AS MAYO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio + ", '0')) AS JUNIO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio + ", '0')) AS JULIO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto + ", '0')) AS AGOSTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre + ", '0')) AS SEPTIEMBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre + ", '0')) AS OCTUBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre + ", '0')) AS NOVIEMBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre + ", '0')) AS DICIEMBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", '0')) AS TOTAL, ");
                    Mi_Sql.Append(" 'TOTAL' AS TIPO ");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                    Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ", ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave);

                    Mi_Sql.Append(" UNION ");

                    Mi_Sql.Append("SELECT ''  AS CODIGO_PROGRAMATICO, '' AS FF, '' AS SF, '' AS CA, '' AS PROGRAMA, '' AS UR, '' AS PARTIDA, ");
                    Mi_Sql.Append("'TOTAL PRESUPUESTO' AS CONCEPTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", '0')) AS TOTAL, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero + ", '0')) AS ENERO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero + ", '0')) AS FEBRERO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo + ", '0')) AS MARZO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril + ", '0')) AS ABRIL, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo + ", '0')) AS MAYO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio + ", '0')) AS JUNIO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio + ", '0')) AS JULIO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto + ", '0')) AS AGOSTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre + ", '0')) AS SEPTIEMBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre + ", '0')) AS OCTUBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre + ", '0')) AS NOVIEMBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre + ", '0')) AS DICIEMBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ", '0')) AS TOTAL, ");
                    Mi_Sql.Append(" 'TOTAL_PSP' AS TIPO ");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                    Mi_Sql.Append(" IN( " + Negocio.P_Tipo_Reporte + ")");

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());


                    Mi_Sql.Append(" ORDER BY  UR, PARTIDA ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las partidas detallado. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Analitico_MOD
            ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas detallados
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 29/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Datos_Analitico_MOD(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();
                String No_Mov = String.Empty;
                Int32 No_Movim;

                try
                {
                    Mi_Sql.Append("SELECT " + Ope_Psp_Presupuesto_Egr_Esp.Campo_No_Modificacion);
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Egr_Esp.Campo_No_Modificacion + " = " + Negocio.P_Tipo_Reporte);
                    Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Egr_Esp.Campo_Anio + " = " + Negocio.P_Anio.Trim());

                    No_Movim = Convert.ToInt32(SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()));
                    No_Mov = No_Movim.ToString().Trim();

                    Mi_Sql = new StringBuilder();

                    if (No_Movim > 0)
                    {
                        #region (Parametros)
                            if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                            {
                                Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                                Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                            {
                                Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Fte_Financiamiento_ID);
                                Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                            {
                                Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Dependencia_ID);
                                Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                            {
                                Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Proyecto_Programa_ID);
                                Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                            {
                                Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Partida_ID);
                                Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                            }
                        #endregion

                        #region (Modificacion 1)
                            if (No_Movim == 1)
                            {
                                Mi_Sql.Append("SELECT ");
                                Mi_Sql.Append("REPLACE(" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ",' ', '') + '-' + ");
                                Mi_Sql.Append("REPLACE(" + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ",' ', '') + '-31120-' + ");
                                Mi_Sql.Append("REPLACE(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ",' ', '') + '-' + ");
                                Mi_Sql.Append("REPLACE(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ",' ', '') + '-' + ");
                                Mi_Sql.Append("REPLACE(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ",' ', '')  AS CODIGO_PROGRAMATICO, ");
                                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS NOMBRE_FF, ");
                                Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS SF, ");
                                Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS NOMBRE_SF, ");
                                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS PROGRAMA, ");
                                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS NOMBRE_PROGRAMA, ");
                                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " AS UR, ");
                                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS NOMBRE_UR, ");
                                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS PARTIDA, ");
                                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CONCEPTO, ");
                                Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " AS CAPITULO, ");
                                Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + " AS NOMBRE_CAPITULO, ");
                                Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Clave + " AS GPO_DEP, ");
                                Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + " AS NOMBRE_GPO_DEP, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Aprobado + ", '0') AS PRESUPUESTO_APROBADO, ");
                                Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Ampliacion + ", '0')) AS AMPLIACION, ");
                                Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Reduccion + ", '0')) AS REDUCCION, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Modificado + ", '0') AS MODIFICADO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Enero + ", '0') AS ENERO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Febrero + ", '0') AS FEBRERO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Marzo + ", '0') AS MARZO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Abril + ", '0') AS ABRIL, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Mayo + ", '0') AS MAYO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Junio + ", '0') AS JUNIO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Julio + ", '0') AS JULIO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Agosto + ", '0') AS AGOSTO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Septiembre + ", '0') AS SEPTIEMBRE, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Octubre + ", '0') AS OCTUBRE, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Noviembre + ", '0') AS NOVIEMBRE, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Diciembre + ", '0') AS DICIEMBRE, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Modificado + ", '0') AS MODIFICADO_TOTAL, ");
                                Mi_Sql.Append("'CONCEPTO' AS TIPO");
                                Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp);
                                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Partida_ID);
                                Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Capitulo_ID);
                                Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                                Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Dependencia_ID);
                                Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                                Mi_Sql.Append(" INNER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias );
                                Mi_Sql.Append(" ON " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID);
                                Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                                Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                                Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Anio);
                                Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_No_Modificacion);
                                Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                                Mi_Sql.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                                Mi_Sql.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio);
                                Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr);
                                Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus);
                                Mi_Sql.Append(" = 'AUTORIZADO'");
                                if (!String.IsNullOrEmpty(Negocio.P_Tipo_Operacion.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion);
                                    Mi_Sql.Append(" IN (" + Negocio.P_Tipo_Operacion.Trim() + ")");
                                }
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID);
                                Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID);
                                Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID);
                                Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                                Mi_Sql.Append(" = " + Negocio.P_Tipo_Reporte.Trim());

                                Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                                Mi_Sql.Append(" GROUP BY ");
                                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                                Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ", ");
                                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ", ");
                                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ", ");
                                Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + ", ");
                                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + ", ");
                                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ", ");
                                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ", ");
                                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ", ");
                                Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Clave + ", ");
                                Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + ", ");
                                Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + ", ");
                                Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + ", ");
                                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Aprobado + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Modificado + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Enero + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Febrero + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Marzo + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Abril + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Mayo + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Junio + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Julio + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Agosto + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Septiembre + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Octubre + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Noviembre + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Diciembre);
                            }
                        #endregion
                        #region (Modificacion N Autorizada)
                            else 
                            {
                                Mi_Sql.Append("SELECT ");
                                Mi_Sql.Append("REPLACE(" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ",' ', '') + '-' + ");
                                Mi_Sql.Append("REPLACE(" + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ",' ', '') + '-31120-' + ");
                                Mi_Sql.Append("REPLACE(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ",' ', '') + '-' + ");
                                Mi_Sql.Append("REPLACE(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ",' ', '') + '-' + ");
                                Mi_Sql.Append("REPLACE(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ",' ', '')  AS CODIGO_PROGRAMATICO, ");
                                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                                Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS SF, ");
                                Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS NOMBRE_SF, ");
                                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS PROGRAMA, ");
                                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS NOMBRE_PROGRAMA, ");
                                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " AS UR, ");
                                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS NOMBRE_UR, ");
                                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS PARTIDA, ");
                                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CONCEPTO, ");
                                Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Clave + " AS GPO_DEP, ");
                                Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + " AS NOMBRE_GPO_DEP, ");
                                Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " AS CAPITULO, ");
                                Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + " AS NOMBRE_CAPITULO, ");
                                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS NOMBRE_FF, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Aprobado + ", '0') AS PRESUPUESTO_APROBADO, ");
                                Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Ampliacion + ", '0')) AS AMPLIACION, ");
                                Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Reduccion + ", '0')) AS REDUCCION, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Modificado + ", '0') AS MODIFICADO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Enero + ", '0') AS ENERO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Febrero + ", '0') AS FEBRERO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Marzo + ", '0') AS MARZO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Abril + ", '0') AS ABRIL, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Mayo + ", '0') AS MAYO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Junio + ", '0') AS JUNIO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Julio + ", '0') AS JULIO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Agosto + ", '0') AS AGOSTO, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Septiembre + ", '0') AS SEPTIEMBRE, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Octubre + ", '0') AS OCTUBRE, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Noviembre + ", '0') AS NOVIEMBRE, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Diciembre + ", '0') AS DICIEMBRE, ");
                                Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Modificado + ", '0') AS MODIFICADO_TOTAL, ");
                                Mi_Sql.Append("'CONCEPTO' AS TIPO");
                                Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp);
                                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Partida_ID);
                                Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Capitulo_ID);
                                Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                                Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                                Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Dependencia_ID);
                                Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                                Mi_Sql.Append(" INNER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias);
                                Mi_Sql.Append(" ON " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID);
                                Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                                Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Fte_Financiamiento_ID);
                                Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                                Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                                Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Anio);
                                Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                                Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_No_Modificacion);
                                Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                                Mi_Sql.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                                Mi_Sql.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio);
                                Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr);
                                Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus);
                                Mi_Sql.Append(" = 'AUTORIZADO'");
                                if (!String.IsNullOrEmpty(Negocio.P_Tipo_Operacion.Trim()))
                                {
                                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion);
                                    Mi_Sql.Append(" IN (" + Negocio.P_Tipo_Operacion.Trim() + ")");
                                }
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID);
                                Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID);
                                Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID);
                                Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID);
                                Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                                Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                                Mi_Sql.Append(" = " + Negocio.P_Tipo_Reporte.Trim());

                                Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                                Mi_Sql.Append(" GROUP BY ");
                                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                                Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ", ");
                                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ", ");
                                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ", ");
                                Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + ", ");
                                Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + ", ");
                                Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ", ");
                                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ", ");
                                Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ", ");
                                Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Clave + ", ");
                                Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + ", ");
                                Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + ", ");
                                Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + ", ");
                                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Aprobado + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Modificado + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Enero + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Febrero + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Marzo + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Abril + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Mayo + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Junio + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Julio + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Agosto + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Septiembre + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Octubre + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Noviembre + ", ");
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Importe_Diciembre);
                            }
                        #endregion
                    }
                    else 
                    {
                        #region (Parametros)
                            if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                            {
                                Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                                Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                            {
                                Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                            {
                                Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                                Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                            {
                                Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                            {
                                Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                                Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                            }
                        #endregion
                        #region (Modificacion 1)
                        if (No_Movim == 1)
                        {
                            Mi_Sql.Append("SELECT ");
                            Mi_Sql.Append("REPLACE(" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ",' ', '') + '-' + ");
                            Mi_Sql.Append("REPLACE(" + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ",' ', '') + '-31120-' + ");
                            Mi_Sql.Append("REPLACE(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ",' ', '') + '-' + ");
                            Mi_Sql.Append("REPLACE(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ",' ', '') + '-' + ");
                            Mi_Sql.Append("REPLACE(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ",' ', '')  AS CODIGO_PROGRAMATICO, ");
                            Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                            Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS SF, ");
                            Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS NOMBRE_SF, ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS PROGRAMA, ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS NOMBRE_PROGRAMA, ");
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " AS UR, ");
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS NOMBRE_UR, ");
                            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS PARTIDA, ");
                            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CONCEPTO, ");
                            Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Clave + " AS GPO_DEP, ");
                            Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + " AS NOMBRE_GPO_DEP, ");
                            Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " AS CAPITULO, ");
                            Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + " AS NOMBRE_CAPITULO, ");
                            Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS NOMBRE_FF, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", '0') AS PRESUPUESTO_APROBADO, ");
                            Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Ampliacion + ", '0')) AS AMPLIACION, ");
                            Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Reduccion + ", '0')) AS REDUCCION, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", '0') AS MODIFICADO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", '0') AS ENERO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", '0') AS FEBRERO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", '0') AS MARZO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", '0') AS ABRIL, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", '0') AS MAYO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", '0') AS JUNIO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", '0') AS JULIO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", '0') AS AGOSTO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", '0') AS SEPTIEMBRE, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", '0') AS OCTUBRE, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", '0') AS NOVIEMBRE, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", '0') AS DICIEMBRE, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", '0') AS MODIFICADO_TOTAL, ");
                            Mi_Sql.Append("'CONCEPTO' AS TIPO");
                            Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                            Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                            Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID);
                            Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                            Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                            Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                            Mi_Sql.Append(" INNER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias);
                            Mi_Sql.Append(" ON " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID);
                            Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                            Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                            Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                            Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                            Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                            Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                            Mi_Sql.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                            Mi_Sql.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio);
                            Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                            Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr);
                            Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                            Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus);
                            Mi_Sql.Append(" IN ('PREAUTORIZADO','GENERADO', 'AUTORIZADO','RECIBIDO') ");
                            if (!String.IsNullOrEmpty(Negocio.P_Tipo_Operacion.Trim()))
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion);
                                Mi_Sql.Append(" IN (" + Negocio.P_Tipo_Operacion.Trim() + ")");
                            }
                            Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Area_Funcional_ID);
                            Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                            Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                            Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID);
                            Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                            Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID);
                            Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                            Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID);
                            Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                            Mi_Sql.Append(" = " + Negocio.P_Tipo_Reporte.Trim());

                            Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                            Mi_Sql.Append(" GROUP BY ");
                            Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                            Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ", ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ", ");
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ", ");
                            Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + ", ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + ", ");
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ", ");
                            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ", ");
                            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ", ");
                            Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Clave + ", ");
                            Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + ", ");
                            Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + ", ");
                            Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + ", ");
                            Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre);
                        }
                        #endregion
                        #region (Modificacion N Autorizada)
                        else
                        {
                            Mi_Sql.Append("SELECT ");
                            Mi_Sql.Append("REPLACE(" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ",' ', '') + '-' + ");
                            Mi_Sql.Append("REPLACE(" + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ",' ', '') + '-31120-' + ");
                            Mi_Sql.Append("REPLACE(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ",' ', '') + '-' + ");
                            Mi_Sql.Append("REPLACE(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ",' ', '') + '-' + ");
                            Mi_Sql.Append("REPLACE(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ",' ', '')  AS CODIGO_PROGRAMATICO, ");
                            Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " AS FF, ");
                            Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS SF, ");
                            Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS NOMBRE_SF, ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " AS PROGRAMA, ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS NOMBRE_PROGRAMA, ");
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " AS UR, ");
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS NOMBRE_UR, ");
                            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS PARTIDA, ");
                            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS CONCEPTO, ");
                            Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Clave + " AS GPO_DEP, ");
                            Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + " AS NOMBRE_GPO_DEP, ");
                            Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " AS CAPITULO, ");
                            Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + " AS NOMBRE_CAPITULO, ");
                            Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS NOMBRE_FF, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", '0') AS PRESUPUESTO_APROBADO, ");
                            Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Ampliacion + ", '0')) AS AMPLIACION, ");
                            Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Reduccion + ", '0')) AS REDUCCION, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", '0')  + ");
                            Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Ampliacion + ", '0')) - ");
                            Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Reduccion + ", '0')) AS MODIFICADO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", '0') AS ENERO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", '0') AS FEBRERO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", '0') AS MARZO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", '0') AS ABRIL, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", '0') AS MAYO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", '0') AS JUNIO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", '0') AS JULIO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", '0') AS AGOSTO, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", '0') AS SEPTIEMBRE, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", '0') AS OCTUBRE, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", '0') AS NOVIEMBRE, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", '0') AS DICIEMBRE, ");
                            Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", '0') AS MODIFICADO_TOTAL, ");
                            Mi_Sql.Append("'CONCEPTO' AS TIPO");
                            Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                            Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                            Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                            Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID);
                            Mi_Sql.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                            Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                            Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                            Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                            Mi_Sql.Append(" INNER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias);
                            Mi_Sql.Append(" ON " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID);
                            Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                            Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                            Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                            Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                            Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                            Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                            Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                            Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                            Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                            Mi_Sql.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                            Mi_Sql.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio);
                            Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                            Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr);
                            Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                            Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus);
                            Mi_Sql.Append(" IN ('PREAUTORIZADO','GENERADO','AUTORIZADO','RECIBIDO') ");
                            if (!String.IsNullOrEmpty(Negocio.P_Tipo_Operacion.Trim()))
                            {
                                Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion);
                                Mi_Sql.Append(" IN (" + Negocio.P_Tipo_Operacion.Trim() + ")");
                            }
                            Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Area_Funcional_ID);
                            Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                            Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID);
                            Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                            Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID);
                            Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                            Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID);
                            Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                            Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID);
                            Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                            Mi_Sql.Append(" = " + Negocio.P_Tipo_Reporte.Trim());

                            Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                            Mi_Sql.Append(" GROUP BY ");
                            Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                            Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ", ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ", ");
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ", ");
                            Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + ", ");
                            Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + ", ");
                            Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + ", ");
                            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ", ");
                            Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ", ");
                            Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Clave + ", ");
                            Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + ", ");
                            Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + ", ");
                            Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + ", ");
                            Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre);
                            
                        }
                        #endregion
                    }
                    Mi_Sql.Append(" ORDER BY  GPO_DEP, UR, PROGRAMA, PARTIDA ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las partidas detallado. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Gpo_Dependencia
            ///DESCRIPCIÓN          : consulta para obtener los datos del grupo dependencia
            ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 18/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Gpo_Dependencia(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS UR, ");
                    Mi_Sql.Append(Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Nombre + " AS GPO_DEP ");
                    Mi_Sql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias);
                    Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias +"." + Cat_Dependencias.Campo_Grupo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Grupos_Dependencias.Tabla_Cat_Grupos_Dependencias + "." + Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID);
                    Mi_Sql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros del ramo. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Importes_Datos_Analitico_MOD
            ///DESCRIPCIÓN          : consulta para obtener los importes de los datos de las partidas detallados
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 22/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Importes_Datos_Analitico_MOD(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();

                try
                {
                    #region (Parametros)
                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                        Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                        Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                        Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                        Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                        Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                    }
                    #endregion

                    Mi_Sql = new StringBuilder();
                    Mi_Sql.Append("SELECT ");
                    Mi_Sql.Append("REPLACE(" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ",' ', '') + '-' + ");
                    Mi_Sql.Append("REPLACE(" + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ",' ', '') + '-31120-' + ");
                    Mi_Sql.Append("REPLACE(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ",' ', '') + '-' + ");
                    Mi_Sql.Append("REPLACE(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ",' ', '') + '-' + ");
                    Mi_Sql.Append("REPLACE(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ",' ', '')  AS CODIGO_PROGRAMATICO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Ampliacion + ", '0')) AS MONTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Enero + ", '0')) AS ENERO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Febrero + ", '0')) AS FEBRERO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Marzo + ", '0')) AS MARZO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Abril + ", '0')) AS ABRIL, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Mayo + ", '0')) AS MAYO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Junio + ", '0')) AS JUNIO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Julio + ", '0')) AS JULIO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Agosto + ", '0')) AS AGOSTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Septiembre + ", '0')) AS SEPTIEMBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Octubre + ", '0')) AS OCTUBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Noviembre + ", '0')) AS NOVIEMBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Diciembre + ", '0')) AS DICIEMBRE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", '0') AS MODIFICADO_TOTAL, ");
                    Mi_Sql.Append("'AMPLIACION' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                    Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                    Mi_Sql.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio);
                    Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr);
                    Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus);
                    Mi_Sql.Append(" IN ('PREAUTORIZADO','GENERADO', 'RECIBIDO') ");
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion);
                    Mi_Sql.Append(" IN ('TRASPASO','AMPLIACION')");
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                    Mi_Sql.Append(" = " + Negocio.P_Tipo_Reporte.Trim());

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Ampliacion + " != 0.00 ");
                    Mi_Sql.Append(" AND (" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Enero + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Febrero + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Marzo + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Abril + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Mayo + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Junio + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Julio + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Agosto + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Septiembre + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Octubre + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Noviembre + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Diciembre + " != 0.00) ");
                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ", ");
                    Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado);

                    Mi_Sql.Append(" UNION ");

                    Mi_Sql.Append("SELECT ");
                    Mi_Sql.Append("REPLACE(" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ",' ', '') + '-' + ");
                    Mi_Sql.Append("REPLACE(" + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ",' ', '') + '-31120-' + ");
                    Mi_Sql.Append("REPLACE(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ",' ', '') + '-' + ");
                    Mi_Sql.Append("REPLACE(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ",' ', '') + '-' + ");
                    Mi_Sql.Append("REPLACE(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ",' ', '')  AS CODIGO_PROGRAMATICO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Reduccion + ", '0')) AS MONTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Enero + ", '0')) AS ENERO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Febrero + ", '0')) AS FEBRERO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Marzo + ", '0')) AS MARZO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Abril + ", '0')) AS ABRIL, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Mayo + ", '0')) AS MAYO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Junio + ", '0')) AS JUNIO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Julio + ", '0')) AS JULIO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Agosto + ", '0')) AS AGOSTO, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Septiembre + ", '0')) AS SEPTIEMBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Octubre + ", '0')) AS OCTUBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Noviembre + ", '0')) AS NOVIEMBRE, ");
                    Mi_Sql.Append("SUM(ISNULL(" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Diciembre + ", '0')) AS DICIEMBRE, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", '0') AS MODIFICADO_TOTAL, ");
                    Mi_Sql.Append("'REDUCCION' AS TIPO");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                    Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" INNER JOIN " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                    Mi_Sql.Append(" LEFT OUTER JOIN " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det);
                    Mi_Sql.Append(" ON " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Anio);
                    Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_Anio);
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_No_Movimiento_Egr);
                    Mi_Sql.Append(" = " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Estatus);
                    Mi_Sql.Append(" IN ('PREAUTORIZADO','GENERADO', 'RECIBIDO') ");
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Tipo_Operacion);
                    Mi_Sql.Append(" IN ('TRASPASO','REDUCCION')");
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Partida_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Egr.Tabla_Ope_Psp_Movimiento_Egr + "." + Ope_Psp_Movimiento_Egr.Campo_No_Movimiento_Egr);
                    Mi_Sql.Append(" = " + Negocio.P_Tipo_Reporte.Trim());

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Reduccion + " != 0.00 ");
                    Mi_Sql.Append(" AND (" + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Enero + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Febrero + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Marzo + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Abril + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Mayo + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Junio + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Julio + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Agosto + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Septiembre + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Octubre + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Noviembre + " != 0.00 ");
                    Mi_Sql.Append(" OR " + Ope_Psp_Movimiento_Egr_Det.Tabla_Ope_Psp_Movimiento_Egr_Det + "." + Ope_Psp_Movimiento_Egr_Det.Campo_Importe_Diciembre + " != 0.00) ");

                    Mi_Sql.Append(" GROUP BY ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ", ");
                    Mi_Sql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ", ");
                    Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado);

                    Mi_Sql.Append(" ORDER BY TIPO, CODIGO_PROGRAMATICO ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las partidas detallado. Error: [" + Ex.Message + "]");
                }
            }


            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Modificaciones_Analitico_MOD
            ///DESCRIPCIÓN          : consulta para obtener los datos de las modificaciones de las partidas
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 11/Marzo/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Modificaciones_Analitico_MOD(Cls_Rpt_Presupuesto_Egresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder();
                StringBuilder Mi_Sql_Filtros = new StringBuilder();

                try
                {
                    #region (Parametros)
                    if (!String.IsNullOrEmpty(Negocio.P_Anio.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Anio);
                        Mi_Sql_Filtros.Append(" = " + Negocio.P_Anio.Trim());
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Fte_Financiamiento_ID);
                        Mi_Sql_Filtros.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Dependencia_ID.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Dependencia_ID);
                        Mi_Sql_Filtros.Append(" = '" + Negocio.P_Dependencia_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Programa_ID.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Proyecto_Programa_ID);
                        Mi_Sql_Filtros.Append(" = '" + Negocio.P_Programa_ID.Trim() + "'");
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Partida_ID.Trim()))
                    {
                        Mi_Sql_Filtros.Append(" AND " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Partida_ID);
                        Mi_Sql_Filtros.Append(" = '" + Negocio.P_Partida_ID.Trim() + "'");
                    }
                    #endregion

                    Mi_Sql.Append("SELECT ");
                    Mi_Sql.Append("REPLACE(" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ",' ', '') + '-' + ");
                    Mi_Sql.Append("REPLACE(" + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ",' ', '') + '-31120-' + ");
                    Mi_Sql.Append("REPLACE(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ",' ', '') + '-' + ");
                    Mi_Sql.Append("REPLACE(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ",' ', '') + '-' + ");
                    Mi_Sql.Append("REPLACE(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ",' ', '')  AS CODIGO_PROGRAMATICO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Modificado + ", '0') AS MODIFICADO, ");
                    Mi_Sql.Append("ISNULL(" + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_No_Modificacion + ", '0') AS NO_MODIFICACION ");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Partida_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Proyecto_Programa_ID);
                    Mi_Sql.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                    Mi_Sql.Append(" INNER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Dependencia_ID);
                    Mi_Sql.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                    Mi_Sql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                    Mi_Sql.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_Sql.Append(" ON " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_Fte_Financiamiento_ID);
                    Mi_Sql.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Egr_Esp.Tabla_Ope_Psp_Presupuesto_Egr_Esp + "." + Ope_Psp_Presupuesto_Egr_Esp.Campo_No_Modificacion);
                    Mi_Sql.Append(" < " + Negocio.P_Tipo_Reporte.Trim());

                    Mi_Sql.Append(" " + Mi_Sql_Filtros.ToString().Trim());

                    Mi_Sql.Append(" ORDER BY  CODIGO_PROGRAMATICO, NO_MODIFICACION ASC");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las modificaciones. Error: [" + Ex.Message + "]");
                }
            }
        #endregion

        #region (Obtener Clasificacion Administrativa)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Clasificacion_Administrativa
            ///DESCRIPCIÓN          : consulta para obtener los datos de la clasificacion administrativa
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 11/Marzo/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static String Consultar_Clasificacion_Administrativa()
            {
                StringBuilder Mi_Sql = new StringBuilder();
                DataSet Ds_Datos_Consulta = new DataSet();
                DataTable Dt_Datos_Consulta = new DataTable();
                String Clasificacion_Adm = String.Empty;

                try
                {

                    Mi_Sql.Append("SELECT " + Cat_Psp_Clasificador_Administrativo.Tabla_Cat_Psp_Clasificador_Administrativo + "." +
                        Cat_Psp_Clasificador_Administrativo.Campo_Clave);
                    Mi_Sql.Append(" FROM " + Cat_Psp_Clasificador_Administrativo.Tabla_Cat_Psp_Clasificador_Administrativo);
                    Mi_Sql.Append(", " + Cat_Psp_Parametros_Presupuestales.Tabla_Cat_Psp_Parametro_Presupuestal);
                    Mi_Sql.Append(" WHERE " + Cat_Psp_Parametros_Presupuestales.Tabla_Cat_Psp_Parametro_Presupuestal + "." + 
                        Cat_Psp_Parametros_Presupuestales.Campo_Clasificador_Adm_ID);
                    Mi_Sql.Append(" = " + Cat_Psp_Clasificador_Administrativo.Tabla_Cat_Psp_Clasificador_Administrativo + "." + 
                        Cat_Psp_Clasificador_Administrativo.Campo_CADM_ID);

                    Ds_Datos_Consulta = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());

                    if (Ds_Datos_Consulta != null)
                    {
                        Dt_Datos_Consulta = Ds_Datos_Consulta.Tables[0];

                        if (Dt_Datos_Consulta.Rows.Count > 0)
                        {
                            Clasificacion_Adm = Dt_Datos_Consulta.Rows[0][Cat_Psp_Clasificador_Administrativo.Campo_Clave].ToString().Trim();
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las modificaciones. Error: [" + Ex.Message + "]");
                }
                return Clasificacion_Adm;
            }
        #endregion
    }
}