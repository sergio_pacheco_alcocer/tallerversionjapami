﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Actualizar_Presupuesto_Aprobado.Negocios;
/// <summary>
/// Summary description for Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Datos
/// </summary>
/// 
namespace JAPAMI.Actualizar_Presupuesto_Aprobado.Datos
{
    public class Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Datos
    {
        public Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Datos()
        {

        }
        #region METODOS
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consultar_Dependencia
        /// 	DESCRIPCION:    Consulta la dependeci de acuerdo a la clave proporcionada asi como su area funcional
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Jennyfer Ivonne Ceja Lemus
        /// 	FECHA_CREO:     17/Septiembre/2012 11:00
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        ///
        public static DataTable Consultar_Dependencia(Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio Datos)
        {
            StringBuilder MiSql = new StringBuilder();
            try
            {
                MiSql.Append("SELECT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + ", ");
                MiSql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + ", ");
                MiSql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " AS CLAVE_AREA_FUNCIONAL ");
                MiSql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias + " JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                MiSql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + " = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                MiSql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " = '" + Datos.P_Clave_Dependencia+ "' ");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consultar_
        /// 	DESCRIPCION:    Consulta la fuente de financiamiento de acuerdo a su clave
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Jennyfer Ivonne Ceja Lemus
        /// 	FECHA_CREO:     17/Septiembre/2012 11:26
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        ///
        public static DataTable Consultar_Fte_Financiamiento(Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio Datos)
        {
            StringBuilder MiSql = new StringBuilder();
            try
            {
                MiSql.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID );
                MiSql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                MiSql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " = '" + Datos.P_Clave_Fuente_Financiamiento+ "' ");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consultar_Programa
        /// 	DESCRIPCION:    Consulta el programa por medio de la clave para verificar si existe
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Jennyfer Ivonne Ceja Lemus
        /// 	FECHA_CREO:     17/Septiembre/2012 11:35
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        ///
        public static DataTable Consultar_Programa(Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio Datos)
        {
            StringBuilder MiSql = new StringBuilder();
            try
            {
                MiSql.Append("SELECT " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ", ");
                MiSql.Append(Cat_Sap_Proyectos_Programas.Campo_Clave);
                MiSql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                MiSql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Clave + " = '" + Datos.P_Clave_Programa+ "' ");
                MiSql.Append(" AND " + Cat_Sap_Proyectos_Programas.Campo_Estatus + " = 'ACTIVO'" );

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consultar_Relacion_Programa_Dependencia
        /// 	DESCRIPCION:    Consulta si existe una relacion entre la dependencia y el programa
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Jennyfer Ivonne Ceja Lemus
        /// 	FECHA_CREO:     17/Septiembre/2012 11:38
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        ///
        public static DataTable Consultar_Relacion_Programa_Dependencia(Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio Datos)
        {
            StringBuilder MiSql = new StringBuilder();
            try
            {
                MiSql.Append("SELECT * " );
                MiSql.Append(" FROM " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia);
                MiSql.Append(" WHERE " + Cat_SAP_Det_Prog_Dependencia.Campo_Dependencia_ID + " = '" + Datos.P_Dependencia_ID+ "' ");
                MiSql.Append(" AND " + Cat_Sap_Det_Prog_Dependencias.Campo_Proyecto_Programa_ID + " = '" + Datos.P_Programa_ID + "'");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consultar_Capitulo
        /// 	DESCRIPCION:    Consulta el capitulo de acuerdo a su clave
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Jennyfer Ivonne Ceja Lemus
        /// 	FECHA_CREO:     17/Septiembre/2012 11:51
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        ///
        public static DataTable Consultar_Capitulo(Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio Datos)
        {
            StringBuilder MiSql = new StringBuilder();
            try
            {
                MiSql.Append("SELECT " + Cat_SAP_Capitulos.Campo_Capitulo_ID + ", " );
                MiSql.Append(Cat_SAP_Capitulos.Campo_Clave);
                MiSql.Append(" FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                MiSql.Append(" WHERE " + Cat_SAP_Capitulos.Campo_Clave + " = '" + Datos.P_Clave_Capitulo+ "' ");
                MiSql.Append(" AND " + Cat_SAP_Capitulos.Campo_Estatus + " = 'ACTIVO'");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consultar_Relacion_Partida_Capitulo
        /// 	DESCRIPCION:    Consulta la partida y el capitulo para ver si tienen relacion
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Jennyfer Ivonne Ceja Lemus
        /// 	FECHA_CREO:     17/Septiembre/2012 12:07
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        ///
        public static DataTable Consultar_Partida(Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio Datos)
        {
            StringBuilder MiSql = new StringBuilder();
            try
            {
                MiSql.Append("SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", " );
                MiSql.Append(Cat_Sap_Partidas_Especificas.Campo_Clave);
                MiSql.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                MiSql.Append(" WHERE " + Cat_SAP_Capitulos.Campo_Clave + " = '" + Datos.P_Clave_Partidaa+ "' ");
                MiSql.Append(" AND " + Cat_SAP_Capitulos.Campo_Estatus + " = 'ACTIVO'");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }

         ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consultar_Partida
        /// 	DESCRIPCION:    Consulta la partida de acuerdo a su clave
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Jennyfer Ivonne Ceja Lemus
        /// 	FECHA_CREO:     17/Septiembre/2012 12:43
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        ///
        public static DataTable Consultar_Relacion_Partida_Capitulo(Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio Datos)
        {
            StringBuilder MiSql = new StringBuilder();
            try
            {
                MiSql.Append("SELECT " +  Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", " );
                MiSql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                MiSql.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                MiSql.Append(" JOIN " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas);
                MiSql.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID + " = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID );
                MiSql.Append(" JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                MiSql.Append(" ON " +  Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID + " = " +  Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                MiSql.Append(" JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                MiSql.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID + " = " +  Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
               
                MiSql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Estatus + " = 'ACTIVO' ");
                if(!String.IsNullOrEmpty(Datos.P_Clave_Partidaa))
                {
                    MiSql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave+ " = '"+ Datos.P_Clave_Partidaa +"'");
                }
                if(!String.IsNullOrEmpty(Datos.P_Clave_Capitulo))
                {
                    MiSql.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave+ " = '"+ Datos.P_Clave_Capitulo +"'");
                }
               
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }

          ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consultar_Presupuesto_Aprobado
        /// 	DESCRIPCION:    Consulta el presupuesto
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Jennyfer Ivonne Ceja Lemus
        /// 	FECHA_CREO:     17/Septiembre/2012 01:35
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        ///
        public static DataTable Consultar_Presupuesto_Aprobado(Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio Datos)
        {
            StringBuilder MiSql = new StringBuilder();
            try
            {
                MiSql.Append("SELECT " +  Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado+ "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", " );
                MiSql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ", ");
                MiSql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", ");
                MiSql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + ", ");
                MiSql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                MiSql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + ", ");
                MiSql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Subnivel_Presupuestal_ID + ", ");
                MiSql.Append(Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Nivel_Presupuesto);

                MiSql.Append(" FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                MiSql.Append(" JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                MiSql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID+ " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                MiSql.Append(" JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                MiSql.Append(" ON " +  Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = " +  Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                MiSql.Append(" JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                MiSql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = " +  Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                MiSql.Append(" JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas );
                MiSql.Append(" ON " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = " +  Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);

                if(!String.IsNullOrEmpty(Datos.P_Clave_Dependencia))
                {
                    if(!MiSql.ToString().Contains("WHERE"))
                        MiSql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " = '" + Datos.P_Clave_Dependencia + "' ");
                    else
                        MiSql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " = '" + Datos.P_Clave_Dependencia + "' ");
                }
                if(!String.IsNullOrEmpty(Datos.P_Clave_Fuente_Financiamiento))
                {
                    if(!MiSql.ToString().Contains("WHERE"))
                        MiSql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento+ "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " = '" + Datos.P_Clave_Fuente_Financiamiento+ "' ");
                    else
                        MiSql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " = '" + Datos.P_Clave_Fuente_Financiamiento + "' ");
                }
                 if(!String.IsNullOrEmpty(Datos.P_Clave_Programa))
                {
                    if(!MiSql.ToString().Contains("WHERE"))
                        MiSql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas+ "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " = '" + Datos.P_Clave_Programa+ "' ");
                    else
                        MiSql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " = '" + Datos.P_Clave_Programa + "' ");
                }
                 if(!String.IsNullOrEmpty(Datos.P_Clave_Partidaa))
                {
                    if(!MiSql.ToString().Contains("WHERE"))
                        MiSql.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas+ "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " = '" + Datos.P_Clave_Partidaa+ "' ");
                    else
                        MiSql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " = '" + Datos.P_Clave_Partidaa + "' ");
                }
                 if (!String.IsNullOrEmpty(Datos.P_Clave_Subnivel))
                 {
                     if (!MiSql.ToString().Contains("WHERE"))
                     {
                        MiSql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Subnivel_Presupuestal_ID + " = (");
                                MiSql.Append(" SELECT " + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID + " FROM " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos);
                                MiSql.Append(" WHERE " + Cat_Psp_SubNivel_Presupuestos.Campo_Dependencia_ID + " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID );
                                MiSql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Campo_FTE_Fincanciamiento_ID + " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                MiSql.Append(" AND " +  Cat_Psp_SubNivel_Presupuestos.Campo_Proyecto_Programa_ID + " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                MiSql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Campo_Partida_ID + " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                                MiSql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal + " = '" + Datos.P_Clave_Subnivel + "')");
                     }
                     else
                     {
                         MiSql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Subnivel_Presupuestal_ID + " = (");
                                MiSql.Append(" SELECT " + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID + " FROM " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos);
                                MiSql.Append(" WHERE " + Cat_Psp_SubNivel_Presupuestos.Campo_Dependencia_ID + " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID );
                                MiSql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Campo_FTE_Fincanciamiento_ID + " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID);
                                MiSql.Append(" AND " +  Cat_Psp_SubNivel_Presupuestos.Campo_Proyecto_Programa_ID + " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID);
                                MiSql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Campo_Partida_ID + " = " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID);
                                MiSql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal + " = '" + Datos.P_Clave_Subnivel + "') ");
                     }
                         
                     
                 }
                 if (!String.IsNullOrEmpty(Datos.P_Anio)) 
                 {
                     if (!MiSql.ToString().Contains("WHERE"))
                         MiSql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = '" + Datos.P_Anio + "' ");
                     else
                         MiSql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "." + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = '" + Datos.P_Anio + "' ");
                 }
               return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los presupuestos. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCION: Consultar_Subnivel_Presupuestal
        /// 	DESCRIPCION:    Consulta el subnivel presupuestal
        /// 	PARAMETROS:     Datos: variable de la capa de negocios
        /// 	CREO:           Jennyfer Ivonne Ceja Lemus
        /// 	FECHA_CREO:     17/Septiembre/2012 12:07
        /// 	MODIFICO: 
        /// 	FECHA_MODIFICO: 
        /// 	CAUSA_MODIFICACION:
        ///*******************************************************************************************************
        ///
        public static DataTable Consultar_Subnivel_Presupuestal(Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio Datos)
        {
            StringBuilder MiSql = new StringBuilder();
            try
            {
 
                MiSql.Append("SELECT " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID + ", ");
                MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal + ", ");
                MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_FTE_Fincanciamiento_ID + ", ");
                MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Area_Funcional_ID + ", ");
                MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Dependencia_ID + ", ");
                MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Proyecto_Programa_ID + ", ");
                MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Partida_ID );
                MiSql.Append(" FROM " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + ", ");
                MiSql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + ", ");
                MiSql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + ", ");
                MiSql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + ", ");
                MiSql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + ", ");
                MiSql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas );

                MiSql.Append(" WHERE "+ Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Area_Funcional_ID + " = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                MiSql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_FTE_Fincanciamiento_ID + " = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                MiSql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Dependencia_ID + " = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                MiSql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Proyecto_Programa_ID + " = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                MiSql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Partida_ID + " = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                if (!String.IsNullOrEmpty(Datos.P_Clave_Dependencia))
                    MiSql.Append(" AND " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " = '" + Datos.P_Clave_Dependencia + "' ");
                if (!String.IsNullOrEmpty(Datos.P_Clave_Fuente_Financiamiento))
                    MiSql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " = '" + Datos.P_Clave_Fuente_Financiamiento + "' ");
                if (!String.IsNullOrEmpty(Datos.P_Clave_Area_Funcional))
                    MiSql.Append(" AND " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " = '" + Datos.P_Clave_Area_Funcional + "' ");
                if (!String.IsNullOrEmpty(Datos.P_Clave_Programa))
                    MiSql.Append(" AND " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " = '" + Datos.P_Clave_Programa + "' ");
                if (!String.IsNullOrEmpty(Datos.P_Clave_Partidaa))
                    MiSql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " = '" + Datos.P_Clave_Partidaa + "' ");
                if(!String.IsNullOrEmpty(Datos.P_Clave_Subnivel))
                    MiSql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal + " = '" + Datos.P_Clave_Subnivel + "' ");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }

        ///****************************************************************************************
        /// NOMBRE DE LA FUNCION:  Alta_Presupuesto_Aprobado
        /// DESCRIPCION :          1. Da de Alta el Presupuesto en la BD con los datos proporcionados por el usuario
        /// PARAMETROS  :          Datos: Variable que contiene los datos que serán insertados en la base de datos
        /// CREO        :          Susana Trigueros Armenta 
        /// FECHA_CREO  :          10/Nov/2011
        /// MODIFICO          : Jennyfer Ivonne Ceja Lemus
        /// FECHA_MODIFICO    : 19/Sept/2012
        /// CAUSA_MODIFICACION: Adaptar el metodo para que funcione para una carga masiva de presupuesto aprobado
        ///****************************************************************************************/
        public static String Alta_Presupuesto_Aprobado(Cls_Cat_Psp_Actualizar_Presupuesto_Aprobado_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            Object Aux; //Variable auxiliar para las consultas
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {

                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;
                DataTable dt_Insertar = Datos.P_Dt_Insertar_Presupuesto;
                DataTable dt_Actualizar = Datos.P_Dt_Actualizar_Presupuesto;
                Int32 Contador = 0;

                if (dt_Insertar != null && dt_Insertar.Rows.Count > 0)
                {
                    foreach (DataRow Registro in dt_Insertar.Rows)//For para insertar proveedores
                    {
                        Contador++;
                        
                        //Asignar consulta para la insercion
                        Mi_SQL = "INSERT INTO " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado +
                    "(" +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + ", " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + "," ;
                     if (!String.IsNullOrEmpty(Registro["SUBNIVEL_ID"].ToString())) 
                     {
                         Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Subnivel_Presupuestal_ID + "," +
                         Ope_Psp_Presupuesto_Aprobado.Campo_Nivel_Presupuesto + ", ";
                     }
                    Mi_SQL = Mi_SQL +  Ope_Psp_Presupuesto_Aprobado.Campo_Anio + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ", " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ", " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Saldo + ", " +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Creo + "," +
                    Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Creo +
                    ") VALUES (" +
                    "'" + Registro["DEPENDENCIA_ID"].ToString() + "'," +
                    "'" + Registro["FUENTE_FINANCIAMIENTO_ID"].ToString() + "'," +
                    "'" + Registro["AREA_FUNCIONAL_ID"].ToString() + "'," +
                    "'" + Registro["PROYECTO_PTOGRAMA_ID"].ToString() + "'," +
                    "'" + Registro["CAPITULO_ID"].ToString() + "'," +
                    "'" + Registro["PARTIDA_ID"].ToString() + "'," ;
                     if (!String.IsNullOrEmpty(Registro["SUBNIVEL_ID"].ToString())) 
                     {
                         Mi_SQL = Mi_SQL + Registro["SUBNIVEL_ID"].ToString() + "," +
                           "'" + Registro["NIVEL_PRESUPUESTAL"].ToString() + "',";
                     }
                    Mi_SQL = Mi_SQL + Registro["ANIO"].ToString() + "," +
                    Registro["ENERO"].ToString() + "," +
                    Registro["FEBRERO"].ToString() + "," +
                    Registro["MARZO"].ToString() + "," +
                    Registro["ABRIL"].ToString() + "," +
                    Registro["MAYO"].ToString() + "," +
                    Registro["JUNIO"].ToString() + "," +
                    Registro["JULIO"].ToString() + "," +
                    Registro["AGOSTO"].ToString() + "," +
                    Registro["SEPTIEMBRE"].ToString() + "," +
                    Registro["OCTUBRE"].ToString() + "," +
                    Registro["NOVIEMBRE"].ToString() + "," +
                    Registro["DICIEMBRE"].ToString() + "," +
                    Registro["TOTAL"].ToString() + "," +
                    Registro["APROBADO"].ToString() + "," +
                    Registro["AMPLIACION"].ToString() + "," +
                    Registro["REDUCCION"].ToString() + "," +
                    Registro["MODIFICADO"].ToString() + "," +
                    Registro["DEVENGADO"].ToString() + "," +
                    Registro["EJERCIDO"].ToString() + "," +
                    Registro["PAGADO"].ToString() + "," +
                    Registro["PRE_COMPROMETIDO"].ToString() + "," +
                    Registro["COMPROMETIDO"].ToString() + "," +
                    Registro["DISPONIBLE"].ToString() + "," +
                    Registro["SALDO"].ToString() + "," +
                    "'" + Cls_Sessiones.Nombre_Empleado + "'," +
                    "GETDATE())";
                        //Ejecutar consulta
                        Obj_Comando.CommandText = Mi_SQL;
                        Obj_Comando.ExecuteNonQuery();
                    }//Fin forEach Insertar
                }//Fin if Dt_Insertar 
                if (dt_Actualizar != null && dt_Actualizar.Rows.Count > 0)
                {
                    foreach (DataRow Registro in dt_Actualizar.Rows)//For para insertar proveedores
                    {

                        //Asignar consulta para modificar los datos del proveedor
                        Mi_SQL = "UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado+ " ";
                        Mi_SQL = Mi_SQL + "SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Registro["DEPENDENCIA_ID"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Registro["FUENTE_FINANCIAMIENTO_ID"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + "='" + Registro["AREA_FUNCIONAL_ID"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Registro["PROYECTO_PTOGRAMA_ID"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " = '" + Registro["CAPITULO_ID"].ToString() + "', ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Registro["PARTIDA_ID"].ToString() + "', ";
                        if (!String.IsNullOrEmpty(Registro["SUBNIVEL_ID"].ToString())) 
                        {
                            Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Subnivel_Presupuestal_ID + "= " + Registro["SUBNIVEL_ID"].ToString() + ", ";
                            Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Nivel_Presupuesto + " = '" + Registro["NIVEL_PRESUPUESTAL"].ToString() + "', ";
                        }
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Registro["ANIO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = " + Registro["ENERO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = " + Registro["FEBRERO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = " + Registro["MARZO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = " + Registro["ABRIL"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = " + Registro["MAYO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = " + Registro["JUNIO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = " + Registro["JULIO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = " + Registro["AGOSTO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = " + Registro["SEPTIEMBRE"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = " + Registro["OCTUBRE"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = " + Registro["NOVIEMBRE"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = " + Registro["DICIEMBRE"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " = " + Registro["TOTAL"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + " = " + Registro["APROBADO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " = " + Registro["AMPLIACION"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " = " + Registro["REDUCCION"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + " = " + Registro["MODIFICADO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + " = " + Registro["DEVENGADO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + " = " + Registro["EJERCIDO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + " = " + Registro["PAGADO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = " + Registro["PRE_COMPROMETIDO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + " = " + Registro["COMPROMETIDO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = " + Registro["DISPONIBLE"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Saldo + " = " + Registro["SALDO"].ToString() + ", ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "', ";
                        Mi_SQL = Mi_SQL + Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " =  GETDATE() ";

                        Mi_SQL = Mi_SQL + " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Registro["FUENTE_FINANCIAMIENTO_ID"].ToString() + "'";
                        Mi_SQL = Mi_SQL + " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Registro["DEPENDENCIA_ID"].ToString() + "' ";
                        Mi_SQL = Mi_SQL + " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Registro["PROYECTO_PTOGRAMA_ID"].ToString() + "' ";
                        Mi_SQL = Mi_SQL + " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " = '" + Registro["CAPITULO_ID"].ToString() + "'";
                        Mi_SQL = Mi_SQL + " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Registro["PARTIDA_ID"].ToString() + "'";
                        if (!String.IsNullOrEmpty(Registro["SUBNIVEL_ID"].ToString()))
                        {
                            Mi_SQL = Mi_SQL + " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Subnivel_Presupuestal_ID + " = " + Registro["SUBNIVEL_ID"].ToString();
                        }
                        //Ejecutar consulta
                        Obj_Comando.CommandText = Mi_SQL;
                        Obj_Comando.ExecuteNonQuery();

                    }//fin  del for
                }//Fin del if dtActualizar
                
                //Mi_SQL = "UPDATE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu;
                //Mi_SQL += " SET " + Ope_Psp_Calendarizacion_Presu.Campo_Estatus + " = 'CARGADO'";
                //Mi_SQL += " WHERE " + Ope_Psp_Calendarizacion_Presu.Campo_Anio + " = '" + Anio + "'";
                //Mi_SQL += " AND " + Ope_Psp_Calendarizacion_Presu.Campo_Estatus + " = 'AUTORIZADO'";
                //Obj_Comando.CommandText = Mi_SQL;
                //Obj_Comando.ExecuteNonQuery();

                //Ejecutar transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
                Mensaje = "Se dio de alta exitosamente el presupuesto";
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
            return Mensaje;
        }
        #endregion

    }
}
