﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JAPAMI.Catalogo_Psp_Conceptos.Negocio;
using System.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Text;


namespace JAPAMI.Catalogo_Psp_Conceptos.Datos
{
    public class Cls_Cat_Psp_Conceptos_Datos
    {
        public Cls_Cat_Psp_Conceptos_Datos()
        {
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Alta_Concepto
        ///DESCRIPCIÓN: Dar de alta un registro en la tabla de CAT_Psp_CONCEPTO
        ///PARAMETROS: 
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 20/marzo/2012
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************        
        public static void Alta_Concepto(Cls_Cat_Psp_Conceptos_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            Object Aux; //Variable auxiliar para las consultas
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Consultas para el ID
                Mi_SQL = "SELECT ISNULL(MAX(" + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + "), '0000000000') FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing;

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Aux = Obj_Comando.ExecuteScalar();

                //Verificar si no es nulo
                if (Convert.IsDBNull(Aux) == false)
                    Datos.P_Concepto_ID = String.Format("{0:0000000000}", Convert.ToInt32(Aux) + 1);
                else
                    Datos.P_Concepto_ID = "0000000001";

                //Asignar consulta para la insercion
                Mi_SQL = "INSERT INTO " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " (" + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + ",";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID + "," + Cat_Psp_Concepto_Ing.Campo_Clave + ",";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Estatus + "," + Cat_Psp_Concepto_Ing.Campo_Descripcion + ",";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Cuenta_Contable_ID + ",";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Anio + ",";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Banco_ID + ",";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Usuario_Creo + "," + Cat_Psp_Concepto_Ing.Campo_Fecha_Creo + ") ";
                Mi_SQL = Mi_SQL + "VALUES('" + Datos.P_Concepto_ID + "','" + Datos.P_Clase_ID + "','" + Datos.P_Clave + "',";
                Mi_SQL = Mi_SQL + "'" + Datos.P_Estatus + "','" + Datos.P_Descripcion + "',";
                Mi_SQL = Mi_SQL + "'" + Datos.P_Cuenta_Contable_ID + "', ";
                Mi_SQL = Mi_SQL + "" + Datos.P_Anio + ", ";

                if (!String.IsNullOrEmpty(Datos.P_Banco_ID))
                {
                    Mi_SQL = Mi_SQL + "'" + Datos.P_Banco_ID + "', ";
                }
                else
                {
                    Mi_SQL = Mi_SQL + " NULL, ";
                }

                Mi_SQL = Mi_SQL + "'" + Datos.P_Usuario + "',GETDATE())";

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Asignar consulta para la insercion
                foreach (DataRow Fila in Datos.P_Dt_Fuentes_Financiamiento.Rows)
                {
                    Mi_SQL = "INSERT INTO " + Cat_Sap_Det_Fte_Concepto.Tabla_Cat_Sap_Det_Fte_Concepto + " (" + Cat_Sap_Det_Fte_Concepto.Campo_Concepto_Ing_ID + ",";
                    Mi_SQL = Mi_SQL + Cat_Sap_Det_Fte_Concepto.Campo_Fuente_Financiamiento_ID + ") ";
                    Mi_SQL = Mi_SQL + "VALUES('" + Datos.P_Concepto_ID + "','" + Fila["Fuente_Financiamiento_ID"].ToString() + "')";

                    //Ejecutar consulta
                    Obj_Comando.CommandText = Mi_SQL;
                    Obj_Comando.ExecuteNonQuery();
                }
                //Ejecutar transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Baja_Concepto
        ///DESCRIPCIÓN: Dar de baja un registro en la tabla de CAT_Psp_CONCEPTO
        ///PARAMETROS: 
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 20/marzo/2012
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Baja_Concepto(Cls_Cat_Psp_Conceptos_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Asignar consulta para la eliminacion
                //Mi_SQL = "DELETE FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing  + " ";
                //Mi_SQL = Mi_SQL + "WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Datos.P_Concepto_ID + "'";

                Mi_SQL = "UPDATE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " ";
                Mi_SQL = Mi_SQL + " SET " + Cat_Psp_Concepto_Ing.Campo_Estatus + " = 'INACTIVO'";
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Datos.P_Concepto_ID + "'";

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Ejecutar transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cambio_Concepto
        ///DESCRIPCIÓN: Modificar un registro en la tabla de CAT_Psp_CONCEPTO
        ///PARAMETROS: 
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 20/marzo/2012
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static void Cambio_Concepto(Cls_Cat_Psp_Conceptos_Negocio Datos)
        {
            //Declaracion de variables
            SqlTransaction Obj_Transaccion = null;
            SqlConnection Obj_Conexion;
            SqlCommand Obj_Comando;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty; //Variable para el mensaje de error

            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Obj_Comando = new SqlCommand();
                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;

                //Asignar consulta para la modificacion
                Mi_SQL = "UPDATE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + " SET " + Cat_Psp_Concepto_Ing.Campo_Clave + " = '" + Datos.P_Clave + "',";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID + " = '" + Datos.P_Clase_ID + "', ";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Estatus + " = '" + Datos.P_Estatus + "', ";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Cuenta_Contable_ID + " = '" + Datos.P_Cuenta_Contable_ID + "', ";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Anio + " = '" + Datos.P_Anio + "', ";

                if (!String.IsNullOrEmpty(Datos.P_Banco_ID))
                {
                    Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Banco_ID + " = '" + Datos.P_Banco_ID + "', ";
                }
                else 
                {
                    Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Banco_ID + " = NULL, ";
                }

                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Descripcion + " = '" + Datos.P_Descripcion + "', ";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario + "', ";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Campo_Fecha_Modifico + " = GETDATE() ";
                Mi_SQL = Mi_SQL + "WHERE " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Datos.P_Concepto_ID + "'";

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();
                // Eliminar la relacion Anterior 
                //Asignar consulta para la baja
                Mi_SQL = "DELETE FROM " + Cat_Sap_Det_Fte_Concepto.Tabla_Cat_Sap_Det_Fte_Concepto + " ";
                Mi_SQL = Mi_SQL + "WHERE " + Cat_Sap_Det_Fte_Concepto.Campo_Concepto_Ing_ID + " = '" + Datos.P_Concepto_ID + "'";

                //Ejecutar consulta
                Obj_Comando.CommandText = Mi_SQL;
                Obj_Comando.ExecuteNonQuery();

                //Asignar consulta para la insercion

                foreach (DataRow Fila in Datos.P_Dt_Fuentes_Financiamiento.Rows)
                {
                    Mi_SQL = "INSERT INTO " + Cat_Sap_Det_Fte_Concepto.Tabla_Cat_Sap_Det_Fte_Concepto + " (" + Cat_Sap_Det_Fte_Concepto.Campo_Concepto_Ing_ID + ",";
                    Mi_SQL = Mi_SQL + Cat_Sap_Det_Fte_Concepto.Campo_Fuente_Financiamiento_ID + ") ";
                    Mi_SQL = Mi_SQL + "VALUES('" + Datos.P_Concepto_ID + "','" + Fila["Fuente_Financiamiento_ID"].ToString() + "')";

                    //Ejecutar consulta
                    Obj_Comando.CommandText = Mi_SQL;
                    Obj_Comando.ExecuteNonQuery();
                }

                //Ejecutar transaccion
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
            }
            catch (SqlException Ex)
            {
                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();
                }
                switch (Ex.Number.ToString())
                {
                    case "2291":
                        Mensaje = "Error: No existe un registro relacionado con esta operacion [" + Ex.Message + "]";
                        break;
                    case "923":
                        Mensaje = "Error: Consulta SQL [" + Ex.Message + "]";
                        break;
                    case "12170":
                        Mensaje = "Error: Conexion con el Servidor [" + Ex.Message + "]";
                        break;
                    default:
                        Mensaje = "Error:  [" + Ex.Message + "]";
                        break;
                }

                throw new Exception(Mensaje, Ex);
            }
            finally
            {
                Obj_Comando = null;
                Obj_Conexion = null;
                Obj_Transaccion = null;
            }
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Consulta_Concepto
        ///DESCRIPCIÓN: Realizar una consulta de uno o mas registros de la tabla de CAT_PSP_CONCEPTO
        ///PARAMETROS: 
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 20/marzo/2012
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Concepto(Cls_Cat_Psp_Conceptos_Negocio Datos)
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;

            try
            {
                //Asignar consulta para los Subfamilias
                Mi_SQL = "SELECT " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + ",";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID + ",";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ",";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Estatus + ",";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + ", ";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Anio + ", ";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Dependencia_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Cuenta_Contable_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Banco_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " + ' ' + ";
                Mi_SQL = Mi_SQL + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLASE_CLAVE";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + ", " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing;
                Mi_SQL = Mi_SQL + " WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID + " ";


                if (Datos.P_Concepto_ID != "" && Datos.P_Concepto_ID != String.Empty && Datos.P_Concepto_ID != null)
                {
                    Mi_SQL = Mi_SQL + "AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Datos.P_Concepto_ID + "' ";
                }
                else if (Datos.P_Clave != "" && Datos.P_Clave != String.Empty && Datos.P_Clave != null)
                {
                    Mi_SQL = Mi_SQL + "AND (" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " LIKE '%" + Datos.P_Clave + "%' ";
                    Mi_SQL = Mi_SQL + "OR " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " LIKE '%" + Datos.P_Clave + "%' ";
                    Mi_SQL = Mi_SQL + "OR upper( " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + ") LIKE upper('%" + Datos.P_Clave + "%') )";
                }

                //Ordenar
                Mi_SQL = Mi_SQL + "ORDER BY " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave;

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Consulta_Clases
        ///DESCRIPCIÓN: Realizar una consulta de uno o mas registros de la tabla de capitulos para llenar el combo de clases
        ///PARAMETROS: 
        ///CREO: Sergio Manuel Gallardo Andrade
        ///FECHA_CREO: 20/marzo/2012
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public static DataTable Consulta_Clases()
        {
            //Declaracion de variables
            String Mi_SQL = String.Empty;

            try
            {
                //Asignar consulta para los Subfamilias
                Mi_SQL = "SELECT " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ";
                Mi_SQL = Mi_SQL + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + "  +' - '+  ";
                Mi_SQL = Mi_SQL + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " as clave_descripicion";
                Mi_SQL = Mi_SQL + " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing;

                //Ordenar
                Mi_SQL = Mi_SQL + " ORDER BY " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave;

                //Entregar resultado
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (SqlException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (DBConcurrencyException ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
            finally
            {
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos_Ing
        ///DESCRIPCIÓN          : Obtiene los Conceptos de acuerdo a los filtros establecidos en la interfaz
        ///PARAMETROS           : Conceptos, instancia de Cls_Cat_Psp_SubConceptos_Negocio
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 21/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Conceptos_Ing(Cls_Cat_Psp_Conceptos_Negocio Conceptos)
        {
            DataTable Tabla = new DataTable();
            String Mi_SQL;
            String Mi_SQL_Campos_Foraneos = "";
            try
            {
                if (Conceptos.P_Campos_Dinamicos != null && Conceptos.P_Campos_Dinamicos != "")
                {
                    Mi_SQL = "SELECT " + Mi_SQL_Campos_Foraneos + Conceptos.P_Campos_Dinamicos;
                }
                else
                {
                    Mi_SQL = "SELECT ";
                    Mi_SQL += Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + ", ";
                    Mi_SQL += Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Cuenta_Contable_ID + ", ";
                    Mi_SQL += Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID + ", ";
                    Mi_SQL += Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Anio + ", ";
                    Mi_SQL += Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ";
                    Mi_SQL += Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + ", ";
                    Mi_SQL += Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Estatus + ", ";
                    if (Mi_SQL.EndsWith(", "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 2);
                    }
                }
                Mi_SQL += " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing;
                if (Conceptos.P_Unir_Tablas != null && Conceptos.P_Unir_Tablas != "")
                {
                    Mi_SQL += ", " + Conceptos.P_Unir_Tablas;
                }
                else
                {
                    if (Conceptos.P_Join != null && Conceptos.P_Join != "")
                    {
                        Mi_SQL += " " + Conceptos.P_Join;
                    }
                }
                if (Conceptos.P_Filtros_Dinamicos != null && Conceptos.P_Filtros_Dinamicos != "")
                {
                    Mi_SQL += " WHERE " + Conceptos.P_Filtros_Dinamicos;
                }
                else
                {
                    Mi_SQL += " WHERE ";
                    if (Conceptos.P_Concepto_ID != null && Conceptos.P_Concepto_ID != "")
                    {
                        Mi_SQL += Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Conceptos.P_Concepto_ID + "' AND ";
                    }
                    if (Conceptos.P_Cuenta_Contable_ID != null && Conceptos.P_Cuenta_Contable_ID != "")
                    {
                        Mi_SQL += Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Cuenta_Contable_ID + " = '" + Conceptos.P_Cuenta_Contable_ID + "' AND ";
                    }
                    if (Conceptos.P_Clase_ID != null && Conceptos.P_Clase_ID != "")
                    {
                        Mi_SQL += Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID + " = '" + Conceptos.P_Clase_ID + "' AND ";
                    }
                    if (Conceptos.P_Anio != null && Conceptos.P_Anio != "")
                    {
                        Mi_SQL += Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Anio + " = " + Conceptos.P_Anio + " AND ";
                    }
                    if (Conceptos.P_Clave != null && Conceptos.P_Clave != "")
                    {
                        Mi_SQL += Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " = '" + Conceptos.P_Clave + "' AND ";
                    }
                    if (Conceptos.P_Estatus != null && Conceptos.P_Estatus != "")
                    {
                        Mi_SQL += Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Estatus + Validar_Operador_Comparacion(Conceptos.P_Estatus) + " AND ";
                    }
                    if (Mi_SQL.EndsWith(" AND "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 5);
                    }
                    if (Mi_SQL.EndsWith(" WHERE "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 7);
                    }
                }
                if (Conceptos.P_Agrupar_Dinamico != null && Conceptos.P_Agrupar_Dinamico != "")
                {
                    Mi_SQL += " GROUP BY " + Conceptos.P_Agrupar_Dinamico;
                }
                if (Conceptos.P_Ordenar_Dinamico != null && Conceptos.P_Ordenar_Dinamico != "")
                {
                    Mi_SQL += " ORDER BY " + Conceptos.P_Ordenar_Dinamico;
                }
                DataSet dataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataSet != null)
                {
                    Tabla = dataSet.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de la Cuentas. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Tabla;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Operador_Comparacion
        ///DESCRIPCIÓN          : Devuelve una cadena adecuada al operador indicado en la capa de Negocios
        ///PARAMETROS           : 
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 20/Agosto/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private static String Validar_Operador_Comparacion(String Filtro)
        {
            String Cadena_Validada;
            if (Filtro.Trim().StartsWith("<")
               || Filtro.Trim().StartsWith(">")
               || Filtro.Trim().StartsWith("<>")
               || Filtro.Trim().StartsWith("<=")
               || Filtro.Trim().StartsWith(">=")
               || Filtro.Trim().StartsWith("=")
               || Filtro.Trim().ToUpper().StartsWith("BETWEEN")
               || Filtro.Trim().ToUpper().StartsWith("LIKE")
               || Filtro.Trim().ToUpper().StartsWith("IN"))
            {
                Cadena_Validada = " " + Filtro + " ";
            }
            else
            {
                if (Filtro.Trim().ToUpper().StartsWith("NULL"))
                {
                    Cadena_Validada = " IS " + Filtro + " ";
                }
                else
                {
                    Cadena_Validada = " = '" + Filtro + "' ";
                }
            }
            return Cadena_Validada;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_UR
        ///DESCRIPCIÓN          : consulta para obtener los datos de las ur's
        ///PARAMETROS           :
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 20/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_UR()
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT " + Cat_Dependencias.Campo_Clave + " + ' ' + ");
                Mi_Sql.Append(Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE, ");
                Mi_Sql.Append(Cat_Dependencias.Campo_Dependencia_ID);
                Mi_Sql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_Sql.Append(" WHERE " + Cat_Dependencias.Campo_Estatus + " = 'ACTIVO'");

                Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las clases. Error: [" + Ex.Message + "]");
            }
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Banco
        ///DESCRIPCIÓN          : consulta para obtener los datos de los bancos
        ///PARAMETROS           :
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 16/junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Banco()
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT DISTINCT " + Cat_Nom_Bancos.Campo_Nombre + ", ");
                Mi_Sql.Append(Cat_Nom_Bancos.Campo_Banco_ID);
                Mi_Sql.Append(" FROM " + Cat_Nom_Bancos.Tabla_Cat_Nom_Bancos);
                Mi_Sql.Append(" WHERE " + Cat_Nom_Bancos.Campo_Tipo + " = 'NOMINA'");

                Mi_Sql.Append(" ORDER BY " + Cat_Nom_Bancos.Campo_Nombre + " ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los bancos. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Fuentes_Financiamiento
        ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento
        ///PARAMETROS           :
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 22/junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Fuentes_Financiamiento()
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", (");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + " + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ") AS DESCRIPCION ");
                Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Estatus + " = 'ACTIVO'");
                //Mi_Sql.Append(" AND " + Cat_SAP_Fuente_Financiamiento.Campo_Anio + " =  year(GETDATE()) ");
                Mi_Sql.Append(" ORDER BY " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los bancos. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Fuentes_Por_Concepto
        ///DESCRIPCIÓN          : Obtiene los Conceptos de acuerdo a los filtros establecidos en la interfaz
        ///PARAMETROS           : Conceptos, instancia de Cls_Cat_Psp_SubConceptos_Negocio
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 22/junio/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Fuentes_Por_Concepto(Cls_Cat_Psp_Conceptos_Negocio Datos)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DFuentes de Financiamiento relacionadas con el concepto
                Mi_Sql.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", (");
                Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + " + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ") AS DESCRIPCION ");
                Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " Fuentes");
                Mi_Sql.Append(" LEFT OUTER JOIN  " + Cat_Sap_Det_Fte_Concepto.Tabla_Cat_Sap_Det_Fte_Concepto + " Relacion ON Fuentes." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + "= Relacion.");
                Mi_Sql.Append(Cat_Sap_Det_Fte_Concepto.Campo_Fuente_Financiamiento_ID);
                Mi_Sql.Append(" WHERE " + Cat_Sap_Det_Fte_Concepto.Campo_Concepto_Ing_ID + " = '" + Datos.P_Concepto_ID + "'");
                Mi_Sql.Append(" ORDER BY " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los bancos. Error: [" + Ex.Message + "]");
            }
        }
    }
}