﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Reporte_Reservas.Negocio;
using System.Data.SqlClient;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;

/// <summary>
/// Summary description for Cls_Rpt_Psp_Reservas_Datos
/// </summary>
namespace JAPAMI.Reporte_Reservas.Datos
{
    public class Cls_Rpt_Psp_Reservas_Datos
    {
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: consultar_Anio()
        ///DESCRIPCIÓN:          Realiza la consulta para llenar el combo de años.
        ///PROPIEDADES:     
        ///CREO:                 Luis Daniel Guzmán Malagón
        ///FECHA_CREO:           20/Septiembre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable consultar_Anio()
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            try
            {
                Mi_SQL = "select distinct " + Ope_Psp_Reservas.Campo_Anio + ", convert(numeric," + Ope_Psp_Reservas.Campo_Anio + ") AS ANNO from " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;

                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }

            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar Años. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: consultar_unidad_responsable()
        ///DESCRIPCIÓN:          Realiza la consulta para llenar el combo de unidad Responsable.
        ///PROPIEDADES:          El año que seleccionó el usuario atravez del objeto parametros_negocio
        ///CREO:                 Luis Daniel Guzmán Malagón
        ///FECHA_CREO:           20/Septiembre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Unidad_Responsable(Cls_Rpt_Psp_Reservas_Negocio Parametros_Negocio)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            try
            {
                Mi_SQL = "SELECT DISTINCT (" + Cat_Dependencias.Campo_Clave + " + ' - ' + " + Cat_Dependencias.Campo_Nombre + ") as DEPENDENCIA, dependencia." + Cat_Dependencias.Campo_Dependencia_ID + " from " +
                    Cat_Dependencias.Tabla_Cat_Dependencias +
                        " dependencia join " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas +
                            " calen on calen." + Ope_Psp_Reservas.Campo_Dependencia_ID + " = dependencia." + Cat_Dependencias.Campo_Dependencia_ID + "";
                if (!String.IsNullOrEmpty(Parametros_Negocio.P_Anio))
                    Mi_SQL += " WHERE calen." + Ope_Psp_Reservas.Campo_Anio + " = '" + Parametros_Negocio.P_Anio + "'";



                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al consultar Las Unidades Rsponsables. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }

            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: consultar_partida()
        ///DESCRIPCIÓN:          Realiza la consulta para llenar el combo Partida.
        ///PROPIEDADES:          El año y la unidad responsable que seleccionó el usuario atravez del objeto parametros_negocio
        ///CREO:                 Luis Daniel Guzmán Malagón
        ///FECHA_CREO:           20/Septiembre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Partida(Cls_Rpt_Psp_Reservas_Negocio Parametros_Negocio)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            try
            {
                Mi_SQL = "select DISTINCT(partida." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' - ' + partida." + Cat_Sap_Partidas_Especificas.Campo_Nombre + ")as PARTIDA, partida." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " from " +
                    Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + " partida join " +
                        Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " calen on partida." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = calen." + Ope_Psp_Reservas.Campo_Partida_ID + " join " +
                            Cat_Dependencias.Tabla_Cat_Dependencias + " dependencia on dependencia." + Cat_Dependencias.Campo_Dependencia_ID + " = calen." + Ope_Psp_Reservas.Campo_Dependencia_ID + "";
                if (!String.IsNullOrEmpty(Parametros_Negocio.P_Anio))
                    Mi_SQL += " where calen." + Ope_Psp_Reservas.Campo_Anio + " = '" + Parametros_Negocio.P_Anio + "'";

                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }

            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al consultar las Partidas. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }

            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: consultar_calendarizacion()
        ///DESCRIPCIÓN:          Realiza la consulta para generar la calendarizacion .
        ///PROPIEDADES:          Los filtros seleccionó el usuario atravez del objeto parametros_negocio
        ///CREO:                 Luis Daniel Guzmán Malagón
        ///FECHA_CREO:           20/Septiembre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consulta_Reservas(Cls_Rpt_Psp_Reservas_Negocio Parametros_Negocio)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            try
            {
                
                Mi_SQL = "SELECT * FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;

                //if (!String.IsNullOrEmpty(Parametros_Negocio.P_Anio))
                Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_Anio + " = " + Parametros_Negocio.P_Anio + "";

                if (!String.IsNullOrEmpty(Parametros_Negocio.P_Unidad_Responsable))
                    Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Dependencia_ID + " = '" + Parametros_Negocio.P_Unidad_Responsable + "'";

                if (!String.IsNullOrEmpty(Parametros_Negocio.P_Partida))
                    Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Partida_ID + " = '" + Parametros_Negocio.P_Partida + "'";

                if (!String.IsNullOrEmpty(Parametros_Negocio.P_Fecha_Inicial))
                {
                    if (!String.IsNullOrEmpty(Parametros_Negocio.P_Fecha_Final))
                        Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Fecha_Creo + " BETWEEN '" + Parametros_Negocio.P_Fecha_Inicial + "' AND '" + Parametros_Negocio.P_Fecha_Final + "'";
                    else
                        Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Fecha_Creo + " BETWEEN '" + Parametros_Negocio.P_Fecha_Inicial + "' AND GETDATE()";
                }
                else
                {
                    if (!String.IsNullOrEmpty(Parametros_Negocio.P_Fecha_Final))
                        Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Fecha_Creo + " < '" + Parametros_Negocio.P_Fecha_Final + "'";
                }

                Mi_SQL += " ORDER BY " + Ope_Psp_Reservas.Campo_No_Reserva + " DESC";

                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
                //Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                return Dt_Datos;

            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al consultar las Reservas. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }

            //return Dt_Datos;
        }

        public static DataTable Consultar_Historial_Reservas(int No_Reserva)
        {
            DataTable Dt_Historial = null;
            String Mi_SQL = "SELECT * FROM " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos;
            if (No_Reserva > 0)
            {
                Mi_SQL += " WHERE " + Ope_Psp_Registro_Movimientos.Campo_No_Reserva + " = " + No_Reserva;
            }
            try
            {
                Dt_Historial = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return Dt_Historial;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: consultar_Reservas_Para_Reporte()
        ///DESCRIPCIÓN:          Realiza la consulta para generar el reporte .
        ///PROPIEDADES:          Los filtros seleccionó el usuario atravez del objeto parametros_negocio
        ///CREO:                 Luis Daniel Guzmán Malagón
        ///FECHA_CREO:           3/Octubre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consulta_Reservas_Para_Reporte(Cls_Rpt_Psp_Reservas_Negocio Parametros_Negocio)
        {
            String Mi_SQL = null;
            DataSet Ds_Datos = null;
            DataTable Dt_Datos = new DataTable();
            try
            {
                Mi_SQL = "SELECT " + Ope_Psp_Reservas.Campo_No_Reserva+" AS FOLIO";
                Mi_SQL += ", replace(convert(varchar(11)," + Ope_Psp_Reservas.Campo_Fecha_Creo + ",106),'','/') AS FECHA";
                Mi_SQL += ", convert(varchar(8)," + Ope_Psp_Reservas.Campo_Fecha_Creo + ",114) AS HORA";
                Mi_SQL += ", " + Ope_Psp_Reservas.Campo_Concepto + " AS CONCEPTO";
                Mi_SQL += ", " + Ope_Psp_Reservas.Campo_Estatus + " AS ESTATUS";
                Mi_SQL += ", " +  "MODIFICO = NULL"; 
                Mi_SQL += ", " + Ope_Psp_Registro_Movimientos.Campo_Cargo+" = NULL";
                Mi_SQL += ", " + Ope_Psp_Registro_Movimientos.Campo_Abono + " = NULL";
                Mi_SQL += ", " + Ope_Psp_Registro_Movimientos.Campo_Importe + " = NULL";
                Mi_SQL += ", " + "FECHA_DETALLE = NULL";
                Mi_SQL += ", " + "HORA_DETALLE = NULL";
                Mi_SQL += ", " + "POLIZA = NULL";
                Mi_SQL += " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas + " WHERE " + Ope_Psp_Reservas.Campo_Anio + " = " + Parametros_Negocio.P_Anio + "";

                if (!String.IsNullOrEmpty(Parametros_Negocio.P_Unidad_Responsable))
                    Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Dependencia_ID + " = '" + Parametros_Negocio.P_Unidad_Responsable + "'";

                if (!String.IsNullOrEmpty(Parametros_Negocio.P_Partida))
                    Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Partida_ID + " = '" + Parametros_Negocio.P_Partida + "'";

                if (!String.IsNullOrEmpty(Parametros_Negocio.P_Fecha_Inicial))
                {
                    if (!String.IsNullOrEmpty(Parametros_Negocio.P_Fecha_Final))
                        Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Fecha_Creo + " BETWEEN '" + Parametros_Negocio.P_Fecha_Inicial + "' AND '" + Parametros_Negocio.P_Fecha_Final + "'";
                    else
                        Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Fecha_Creo + " BETWEEN '" + Parametros_Negocio.P_Fecha_Inicial + "' AND GETDATE()";
                }
                else
                {
                    if (!String.IsNullOrEmpty(Parametros_Negocio.P_Fecha_Final))
                        Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Fecha_Creo + " < '" + Parametros_Negocio.P_Fecha_Final + "'";
                }
                
                Mi_SQL +=" UNION ALL ";

                Mi_SQL += " SELECT " +  Ope_Psp_Registro_Movimientos.Campo_No_Reserva + " AS FOLIO";
                Mi_SQL += ", " +  "FECHA = NULL";
                Mi_SQL += ", " +  "HORA = NULL";
                Mi_SQL += ", " + Ope_Psp_Reservas.Campo_Concepto + " = NULL";
                Mi_SQL += ", " + Ope_Psp_Reservas.Campo_Estatus + " = NULL";
                Mi_SQL += ", " + Ope_Psp_Registro_Movimientos.Campo_Usuario_Creo + " AS MODIFICO";
                Mi_SQL += ", " + Ope_Psp_Registro_Movimientos.Campo_Cargo + " AS CARGO";
                Mi_SQL += ", " + Ope_Psp_Registro_Movimientos.Campo_Abono + " AS ABONO";
                Mi_SQL += ", " + Ope_Psp_Registro_Movimientos.Campo_Importe + " AS IMPORTE";
                Mi_SQL += ", replace(convert(varchar(11)," + Ope_Psp_Registro_Movimientos.Campo_Fecha_Creo + ",106),'','/') AS FECHA_DETALLE";
                Mi_SQL += ", convert(varchar(8)," + Ope_Psp_Registro_Movimientos.Campo_Fecha_Creo + ",114) AS HORA_DETALLE";
                Mi_SQL += ", " + Ope_Psp_Registro_Movimientos.Campo_No_Poliza + " AS POLIZA";

                Mi_SQL += " FROM "+Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos;

                Mi_SQL += " WHERE " + Ope_Psp_Registro_Movimientos.Campo_No_Reserva + " IN (SELECT " + Ope_Psp_Reservas.Campo_No_Reserva + " FROM " + Ope_Psp_Reservas.Tabla_Ope_Psp_Reservas;
                Mi_SQL += " WHERE " + Ope_Psp_Reservas.Campo_Anio + " = " + Parametros_Negocio.P_Anio;

                if (!String.IsNullOrEmpty(Parametros_Negocio.P_Unidad_Responsable))
                    Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Dependencia_ID + " = '" + Parametros_Negocio.P_Unidad_Responsable + "'";

                if (!String.IsNullOrEmpty(Parametros_Negocio.P_Partida))
                    Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Partida_ID + " = '" + Parametros_Negocio.P_Partida + "'";

                if (!String.IsNullOrEmpty(Parametros_Negocio.P_Fecha_Inicial))
                {
                    if (!String.IsNullOrEmpty(Parametros_Negocio.P_Fecha_Final))
                        Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Fecha_Creo + " BETWEEN '" + Parametros_Negocio.P_Fecha_Inicial + "' AND '" + Parametros_Negocio.P_Fecha_Final + "'";
                    else
                        Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Fecha_Creo + " BETWEEN '" + Parametros_Negocio.P_Fecha_Inicial + "' AND GETDATE()";
                }
                else
                {
                    if (!String.IsNullOrEmpty(Parametros_Negocio.P_Fecha_Final))
                        Mi_SQL += " AND " + Ope_Psp_Reservas.Campo_Fecha_Creo + " < '" + Parametros_Negocio.P_Fecha_Final + "'";
                }

                Mi_SQL += ")";

                //Mi_SQL += " ORDER BY " + Ope_Psp_Reservas.Campo_No_Reserva + " DESC";

                if (Mi_SQL != null && Mi_SQL.Trim().Length > 0)
                {
                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (Ds_Datos != null)
                {
                    Dt_Datos = Ds_Datos.Tables[0];
                }
                //Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                return Dt_Datos;

            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al consultar las Reservas parael reporte. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }

            //return Dt_Datos;
        }
    }
}
