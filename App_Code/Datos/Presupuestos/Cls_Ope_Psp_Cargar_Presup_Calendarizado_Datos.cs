﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Cargar_Presupuesto_Calendarizado.Negocio;
using System.Text;
/// <summary>
/// Summary description for Cls_Ope_Psp_Cargar_Presup_Calendarizado_Datos
/// </summary>
/// 
namespace JAPAMI.Cargar_Presupuesto_Calendarizado.Datos
{
    public class Cls_Ope_Psp_Cargar_Presup_Calendarizado_Datos
    {
        #region VARIABLES

        public static String TIPO_CALENDARIZADO = "CALENDARIZADO";
        public static String TIPO_APROBADO = "APROBADO";

        public Cls_Ope_Psp_Cargar_Presup_Calendarizado_Datos()
        {
        }
        public int Consultar_Presupuesto_Calendarizado()
        {
            return 0;
        }
        public static DataTable Consultar_Anios_Presupuestados()
        {
            String Mi_SQL = "";
            DataTable Dt_Anios = null;
            try
            {
                    Mi_SQL = "SELECT DISTINCT (" + Ope_Psp_Calendarizacion_Presu.Campo_Anio + ") FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu;
                    Mi_SQL += " WHERE " +Ope_Psp_Calendarizacion_Presu.Campo_Estatus + " = 'AUTORIZADO'" ;
                    Mi_SQL += " ORDER BY " + Ope_Psp_Calendarizacion_Presu.Campo_Anio + " DESC";
                    Dt_Anios = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString() + "[NO SE PUDO CONSULTAR PRESUPUESTO]");
            }
            return Dt_Anios;
        }
        public static double Consultar_Importe_Presupuesto_Aprobado(Cls_Ope_Psp_Cargar_Presup_Calendarizado_Negocio Negocio)
        {
            String Mi_SQL = "";
            double Importe = 0;
            try
            {
                Mi_SQL = "SELECT SUM (" + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + ") FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu;
                Mi_SQL += " WHERE " + Ope_Psp_Calendarizacion_Presu.Campo_Anio + " = " + Negocio.P_Anio;
                Mi_SQL += " AND " + Ope_Psp_Calendarizacion_Presu.Campo_Estatus + " = 'AUTORIZADO'" ;
                Object Objeto = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                Importe = Convert.ToDouble(Objeto);
            }
            catch (Exception Ex)
            {
                Importe = 0;
                //throw new Exception(Ex.ToString() + "[NO SE PUDO CONSULTAR PRESUPUESTO]");
            }
            return Importe;
        }

        internal static Boolean  Consultar_Presupuestos_Generados(Cls_Ope_Psp_Cargar_Presup_Calendarizado_Negocio Negocio) 
        {
            Boolean Existente = false;
            StringBuilder Mi_SQL = new StringBuilder();
            DataTable Dt_Datos = new DataTable();
            try
            {
                Mi_SQL.Append("SELECT * FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Campo_Estatus + " = 'GENERADO'");
                Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Campo_Anio + " = '"+Negocio.P_Anio +"'");
                Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                if (Dt_Datos != null)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        Existente = true;
                    }
                }
            }
            catch (Exception e)
            {

                throw new Exception("Error al consultar los presupuestos sin autorizar. Error[" + e.Message + "]");
            }
            return Existente;
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Partida_Asignadas
        ///DESCRIPCIÓN          : consulta para obtener las partidas asignadas
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Noviembre/2011
        ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
        ///FECHA_MODIFICO       : 22/Agosto/2012
        ///CAUSA_MODIFICACIÓN   : Se agrego un nivel presupuestal para permitir asignar presupuesto a otro tipo de actividades como salarios
        ///*******************************************************************************
        internal static DataTable Consultar_Partida_Asignadas(Cls_Ope_Psp_Cargar_Presup_Calendarizado_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            try
            {
                Mi_SQL.Append(" SELECT RTRIM(" + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + ") +'-'+ ");
                Mi_SQL.Append("( SELECT RTRIM(" + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + ")" );
                Mi_SQL.Append(" FROM " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                    Mi_SQL.Append(" WHERE " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                    Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + ") + '-' + ");
                Mi_SQL.Append(" RTRIM(" + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + ") + '-' + ");
                Mi_SQL.Append(" RTRIM(" + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + ") + '-' + ");
                Mi_SQL.Append(" RTRIM(" + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + ") AS CODIGO_PROGRAMATICO, ");
                Mi_SQL.Append("ISNULL(" + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Producto_ID + ", '') AS PRODUCTO_ID, ");
                Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' - ' + " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID + ", ");
                //obtenemos el precio
                Mi_SQL.Append("(CASE ");
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Enero + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Enero);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Febrero + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Febrero);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Marzo + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Marzo);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Abril + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Abril);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Mayo + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Mayo);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Junio + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Junio);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Julio + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Julio);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Agosto + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Agosto);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Septiembre + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Septiembre);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Octubre + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Octubre);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Noviembre + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Noviembre);
                Mi_SQL.Append(" WHEN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Diciembre + " > 0 THEN ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre + "/");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Cantidad_Diciembre);
                Mi_SQL.Append(" ELSE 0 END) AS PRECIO, ");
                Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " AS CLAVE_PARTIDA, ");
                Mi_SQL.Append("ISNULL(" + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Clave + ", '') AS CLAVE_PRODUCTO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero + " AS ENERO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero + " AS FEBRERO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo + " AS MARZO , ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril + " AS ABRIL, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo + " AS MAYO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio + " AS JUNIO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio + " AS JULIO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto + " AS AGOSTO, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre + " AS SEPTIEMBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre + " AS OCTUBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre + " AS NOVIEMBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre + " AS DICIEMBRE, ");
                Mi_SQL.Append(Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total + " AS IMPORTE_TOTAL, '' AS ID ");
                Mi_SQL.Append(" FROM " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Com_Productos.Tabla_Cat_Com_Productos);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Producto_ID);
                Mi_SQL.Append(" = " + Cat_Com_Productos.Tabla_Cat_Com_Productos + "." + Cat_Com_Productos.Campo_Producto_ID);
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID);
                Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID);
                
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);

                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
               
                Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                
                
                //Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica);
                //Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                //Mi_SQL.Append(" = " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                //Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                //Mi_SQL.Append(" ON " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID);
                //Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                //Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                //Mi_SQL.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                //Mi_SQL.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                //Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros);
                //Mi_SQL.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                //Mi_SQL.Append(" = " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Anio_Presupuestar);
                //Mi_SQL.Append(" AND " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros + "." + Cat_Psp_Parametros.Campo_Estatus + " = 'ACTIVO'");

                Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus + " = 'AUTORIZADO' ");
                if (!string.IsNullOrEmpty(Negocio.P_Anio))
                {
                    if (Mi_SQL.ToString().Contains("WHERE"))
                    {
                        Mi_SQL.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio );
                    }
                    else
                    {
                        Mi_SQL.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                        Mi_SQL.Append(" = " + Negocio.P_Anio );
                    }
                }
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las partidas asignadas. Error: [" + Ex.Message + "]");
            }
        }
        public static DataTable Consultar_Clave_Subnivel(Cls_Ope_Psp_Cargar_Presup_Calendarizado_Negocio Negocio)
        {
            String Mi_SQL = "";
            DataTable Dt_Clave = null;
            try
            {
                Mi_SQL = "SELECT " + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal + " AS CLAVE  FROM " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos;
                Mi_SQL += " WHERE " + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID + " = " +  Negocio.P_Subnnivel_Presupuestal_ID;
         
                Dt_Clave = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString() + "[NO SE PUDO CONSULTAR PRESUPUESTO]");
            }
            return Dt_Clave;
        }
        #endregion
    }
}