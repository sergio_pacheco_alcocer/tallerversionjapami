﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Subniveles_Presupuesto.Negocio;

/// <summary>
/// Summary description for Cls_Cat_Psp_Subnivel_Presupuesto_Datos
/// </summary>
/// 
namespace JAPAMI.Subniveles_Presupuesto.Datos
{
    public class Cls_Cat_Psp_Subnivel_Presupuesto_Datos
    {
        #region Metodos
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_SubNivel_Presupuestos_Para_GrdiView
        ///DESCRIPCIÓN          : Consulta para obtener los datos de los SubNivel_Presupuestos
        ///PARAMETROS           : Objeto de la clase Cls_Cat_Psp_SubNivel_Presupuesto_Negocio
        ///CREO                 : Luis Daniel Guzmán Malagón
        ///FECHA_CREO           : 16/Julio/2012
        ///MODIFICO             :Jennyfer Ivoone Ceja lemus
        ///FECHA_MODIFICO       :17/Agosto/2012 05:18 pm
        ///CAUSA_MODIFICACIÓN   : Porque se cambio toda la tabla de PSP_Subnivel_Presupuestal
        ///                       Devido a q se necesita para usarse en la calendarizacion presupuestal
        ///*******************************************************************************
        public static DataTable consulta_SubNivel_Presupuestos_Para_GridView(Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sbn_Psp_Negocio)
        {
            StringBuilder MiSql = new StringBuilder();

            try {
                //MiSql.Append("SELECT " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal + ", ");
                //MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Partida_ID + ", ");
                //MiSql.Append(Cat_Sap_Partidas_Especificas.Campo_Nombre + ", ");
                //MiSql.Append(Cat_Sap_Partidas_Especificas.Campo_Descripcion + ", ");
                //MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Estatus + ", ");
                //MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Anio + " FROM ");
                //MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos);
                //MiSql.Append(" JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                //MiSql.Append(" ON ");
                //MiSql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " like " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal);
                MiSql.Append("SELECT " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave + " +'-'+ " );
                MiSql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Descripcion + " AS AREA_FUNCIONAL, ");
                
                MiSql.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " +'-' + " + Cat_Dependencias.Tabla_Cat_Dependencias+ "." + Cat_Dependencias.Campo_Nombre + " AS  UR, ");
                MiSql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + '-' + " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FTE_FINANCIAMIENTO, ");
                MiSql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + "+ '-' + " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS PROGRAMA, " );
                MiSql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + '-' + " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA, ");
                MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID + " AS CLAVE, ");
                MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal+ ", ");
                MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Descripcion + ", ");
                MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Anio + ", ");
                MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Estatus + ", " );
                MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Codigo_Programatico);

                MiSql.Append(" FROM " );
                MiSql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + " JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                MiSql.Append(" ON " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Dependencia_ID + " = " +Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID );
                MiSql.Append(" JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                MiSql.Append(" ON " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_FTE_Fincanciamiento_ID + " = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                MiSql.Append(" JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas );
                MiSql.Append(" ON " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Proyecto_Programa_ID + " = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id );
                MiSql.Append(" JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                MiSql.Append(" ON " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Partida_ID + " = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                MiSql.Append(" JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                MiSql.Append(" ON " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Area_Funcional_ID + " = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "."  + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                MiSql.Append(" ORDER BY  UR, FTE_FINANCIAMIENTO, PROGRAMA, PARTIDA, " + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal);

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }catch(Exception Ex){
                throw new Exception("Error al intentar consultar SubNivel Persupuesto Para GridView. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_SubNivel_Presupuestos
        ///DESCRIPCIÓN          : Consulta para obtener los datos de los SubNivel_Presupuestos
        ///PARAMETROS           : Objeto de la clase Cls_Cat_Psp_SubNivel_Presupuesto_Negocio
        ///CREO                 : Luis Daniel Guzmán Malagón
        ///FECHA_CREO           : 12/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        ///
        public static DataTable consulta_SubNivel_Presupuestos(Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sbn_Psp_Negocio)
        {
            StringBuilder MySql = new StringBuilder();

            try
            {
                //OBTENER LAS DEPENCIAS DEL CATALOGO
                MySql.Append("SELECT " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID + ", ");
                MySql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal + ", ");
                MySql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Dependencia_ID+ ", ");
                MySql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Area_Funcional_ID + ", ");
                MySql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_FTE_Fincanciamiento_ID + ", ");
                MySql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Proyecto_Programa_ID + ", ");
                MySql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Partida_ID + ", ");

                MySql.Append("( SELECT " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + " FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + ", " );
                    MySql.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + ", " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + ", ");
                    MySql.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    MySql.Append(" WHERE " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                    MySql.Append(" AND " +Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID + " = " +  Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID);
                    MySql.Append(" AND " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID + " = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas +  "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID );
                    MySql.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + " = " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Partida_ID + ") AS CAPITULO_ID, ");

                MySql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Descripcion + ", ");
                MySql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Anio + ", ");
                MySql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Estatus + ", ");
                MySql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Codigo_Programatico+ ", ");
                MySql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Fecha_Creo + ", ");
                MySql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Usuario_Creo + ", ");
                MySql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Fecha_Modifico + ", ");
                MySql.Append(Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Usuario_Modifico);
                MySql.Append(" FROM "+ Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos);
                MySql.Append(" WHERE " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID + " = " + Sbn_Psp_Negocio.P_Subnivel_Presupuestal_ID.Trim());
                
                ////ADDICIONAR IGUALACIONES EN LA SENTENCIA CON EL CAMPO CLAVE
                //if(!String.IsNullOrEmpty(Sbn_Psp_Negocio.P_Subnivel_Presupuestal))
                //{
                //    if (MySql.ToString().Trim().Contains("WHERE"))
                //    {
                //        MySql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal + " = '" + Sbn_Psp_Negocio.P_Subnivel_Presupuestal.Trim() + "'");
                //    }
                //    else
                //    {
                //        MySql.Append(" WHERE " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal + " = '" + Sbn_Psp_Negocio.P_Subnivel_Presupuestal.Trim() + "'");
                //    }
                //}

                //ADDICIONAR IGUALACIONES EN LA SENTENCIA CON EL CAMPO ANIO
                if (!String.IsNullOrEmpty(Sbn_Psp_Negocio.P_Anio))
                {
                    if (MySql.ToString().Trim().Contains("WHERE"))
                    {
                        MySql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Anio + " = '" + Sbn_Psp_Negocio.P_Anio.Trim() + "'");
                    }
                    else
                    {
                        MySql.Append(" WHERE " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Anio + " = '" + Sbn_Psp_Negocio.P_Anio.Trim() + "'");
                    }
                }

                //ADDICIONAR IGUALACIONES EN LA SENTENCIA CON EL CAMPO ESTATUS
                if (!String.IsNullOrEmpty(Sbn_Psp_Negocio.P_Estatus))
                {
                    if (MySql.ToString().Trim().Contains("WHERE"))
                    {
                        MySql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Estatus + " = '" + Sbn_Psp_Negocio.P_Estatus.Trim() + "'");
                    }
                    else
                    {
                        MySql.Append(" WHERE " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Estatus + " = '" + Sbn_Psp_Negocio.P_Estatus.Trim() + "'");
                    }
                }

                //ADDICIONAR IGUALACIONES EN LA SENTENCIA CON EL CAMPO FECHA_CREO
                if (!String.IsNullOrEmpty(Sbn_Psp_Negocio.P_Fecha_Creo))
                {
                    if (MySql.ToString().Trim().Contains("WHERE"))
                    {
                        MySql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Fecha_Creo + " = '" + Sbn_Psp_Negocio.P_Fecha_Creo.Trim() + "'");
                    }
                    else
                    {
                        MySql.Append(" WHERE " + Cat_Psp_SubNivel_Presupuestos.Campo_Fecha_Creo + " = '" + Sbn_Psp_Negocio.P_Fecha_Creo.Trim() + "'");
                    }
                }

                //ADDICIONAR IGUALACIONES EN LA SENTENCIA CON EL CAMPO USUARIO_CREO
                if (!String.IsNullOrEmpty(Sbn_Psp_Negocio.P_Usuario_Creo))
                {
                    if (MySql.ToString().Trim().Contains("WHERE"))
                    {
                        MySql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Usuario_Creo + " = '" + Sbn_Psp_Negocio.P_Usuario_Creo.Trim() + "'");
                    }
                    else
                    {
                        MySql.Append(" WHERE " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Usuario_Creo + " = '" + Sbn_Psp_Negocio.P_Usuario_Creo.Trim() + "'");
                    }
                }

                //ADDICIONAR IGUALACIONES EN LA SENTENCIA CON EL CAMPO FECHA_MODIFICO
                if (!String.IsNullOrEmpty(Sbn_Psp_Negocio.P_Fecha_Modifico))
                {
                    if (MySql.ToString().Trim().Contains("WHERE"))
                    {
                        MySql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Fecha_Modifico + " = '" + Sbn_Psp_Negocio.P_Fecha_Modifico.Trim() + "'");
                    }
                    else
                    {
                        MySql.Append(" WHERE " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Fecha_Modifico + " = '" + Sbn_Psp_Negocio.P_Fecha_Modifico.Trim() + "'");
                    }
                }

                //ADDICIONAR IGUALACIONES EN LA SENTENCIA CON EL CAMPO USUARIO_MODIFICO
                if (!String.IsNullOrEmpty(Sbn_Psp_Negocio.P_Usuario_Modifico))
                {
                    if (MySql.ToString().Trim().Contains("WHERE"))
                    {
                        MySql.Append(" AND " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Usuario_Modifico + " = '" + Sbn_Psp_Negocio.P_Usuario_Modifico.Trim() + "'");
                    }
                    else
                    {
                        MySql.Append(" WHERE " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Usuario_Modifico + " = '" + Sbn_Psp_Negocio.P_Usuario_Modifico.Trim() + "'");
                    }
                }

                //ORDENAR EL RESULTADO DE LA SENTENCIA POR EL CAMPO CLAVE
                MySql.Append(" ORDER BY " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "." + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal + " ASC");

                //REGRESAR LA CONSULTA
                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MySql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar SubNivel Persupuesto. Error: [" + Ex.Message + "]");
            }    
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar_SubNivel_Presupuesto
        ///DESCRIPCIÓN          : Consulta para eliminar(Poner Inactivo) SubNivel Presupuesto
        ///PARAMETROS           1 Objeto de la Clase Cls_Cat_Psp_SubNivel_Presupuesto
        ///CREO                 : Luis Daniel Guzmán Malagón
        ///FECHA_CREO           : 12/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static Boolean Eliminar_SubNivel_Presupuestos(Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sbn_Psp_Negocios)
        {
            //Variable que almacenará la consulta.
            StringBuilder Mi_SQL = new StringBuilder();
            //Estado de la operacion.
            Boolean Operacion_Completa = false;

            try
            {
                Mi_SQL.Append("UPDATE " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos);
                Mi_SQL.Append(" SET " + Cat_Psp_SubNivel_Presupuestos.Campo_Estatus + " = 'INACTIVO'");
                Mi_SQL.Append(" WHERE " + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID+ " = " + Sbn_Psp_Negocios.P_Subnivel_Presupuestal_ID );


                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al tratar de Eliminar SubNivel Presupuesto. Error: [" + Ex.Message + "]");
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_SubNivel_Presupuestos
        ///DESCRIPCIÓN          : Modifica los datos de SubNivel_Presupuestos
        ///PARAMETROS           1 Objeto de la Clase Cls_Cat_Psp_SubNivel_Presupuestos a modificar
        ///CREO                 : Luis Daniel Guzmán Malagón
        ///FECHA_CREO           : 12/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static Boolean Modificar_SubNivel_Presupuestos(Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sbn_Psp_Negocios)
        {
            //Variable que almacenará la consulta.
            StringBuilder Mi_SQL = new StringBuilder();
            //Estado de la operacion.
            Boolean Operacion_Completa = false;

            try
            {
                Mi_SQL.Append("UPDATE " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos);
                Mi_SQL.Append(" SET " );
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Dependencia_ID + " = '" + Sbn_Psp_Negocios.P_Dependencia_ID + "', ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Area_Funcional_ID + " = '" + Sbn_Psp_Negocios.P_Area_Funcional_ID + "', ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_FTE_Fincanciamiento_ID + " = '" + Sbn_Psp_Negocios.P_FTE_Financiamiento_ID + "', ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Proyecto_Programa_ID + "= '" + Sbn_Psp_Negocios.P_Programa_ID + "', ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Partida_ID + " = '" + Sbn_Psp_Negocios.P_Partida_ID + "', ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal + " = '" + Sbn_Psp_Negocios.P_Subnivel_Presupuestal + "', ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Descripcion + " = '" + Sbn_Psp_Negocios.P_Descripcion + "', ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Anio + " = '" + Sbn_Psp_Negocios.P_Anio + "', ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Estatus + " = '" + Sbn_Psp_Negocios.P_Estatus + "', ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Codigo_Programatico + " = '" + Sbn_Psp_Negocios.P_Codigo_Programatico + "', ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Usuario_Modifico + " = '" + Sbn_Psp_Negocios.P_Usuario_Modifico + "', ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID + " = '" + Sbn_Psp_Negocios.P_Subnivel_Presupuestal_ID + "' ");

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar moficar  SubNivel Presupuesto. Error: [" + Ex.Message + "]");
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_SubNivel_Presupuestos
        ///DESCRIPCIÓN          : Inserta SubNivel Presupuesto
        ///PARAMETROS           1 Objeto de la Clase Cls_Cat_Psp_SubNivel_Presupuesto_Negocio a insertar
        ///CREO                 : Luis Daniel Guzmán Malagón
        ///FECHA_CREO           : 12/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static Boolean Alta_SubNivel_Presupuestos(Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sbn_Psp_Negocios)
        {
            //Variable que almacenará la consulta.
            StringBuilder Mi_SQL = new StringBuilder();
            //Estado de la operacion.
            Boolean Operacion_Completa = false;
            
            
            String Id = Consecutivo_ID(Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID, Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos, "10");

            try
            {
                Mi_SQL.Append("INSERT INTO " + Cat_Psp_SubNivel_Presupuestos.Tabla_Cat_PSP_Subnivel_Presupuestos + "(");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Dependencia_ID + ", ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_FTE_Fincanciamiento_ID + ", ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Proyecto_Programa_ID + ", ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Partida_ID + ", ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Area_Funcional_ID + ", ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID + ", ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal + ", ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Descripcion + ", ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Anio + ", ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Estatus + ", ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Codigo_Programatico + ", ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Fecha_Creo + ", ");
                Mi_SQL.Append(Cat_Psp_SubNivel_Presupuestos.Campo_Usuario_Creo + ") VALUES( ");
              
                Mi_SQL.Append("'" + Sbn_Psp_Negocios.P_Dependencia_ID + "', ");
                Mi_SQL.Append("'" + Sbn_Psp_Negocios.P_FTE_Financiamiento_ID + "', ");
                Mi_SQL.Append("'" + Sbn_Psp_Negocios.P_Programa_ID + "', ");
                Mi_SQL.Append("'" + Sbn_Psp_Negocios.P_Partida_ID + "', ");
                Mi_SQL.Append("'" + Sbn_Psp_Negocios.P_Area_Funcional_ID + "', ");
                Mi_SQL.Append(Id + ", ");
                Mi_SQL.Append("'" + Sbn_Psp_Negocios.P_Subnivel_Presupuestal + "', ");
                Mi_SQL.Append("'" + Sbn_Psp_Negocios.P_Descripcion + "', ");
                Mi_SQL.Append("'" + Sbn_Psp_Negocios.P_Anio + "', ");
                Mi_SQL.Append("'" + Sbn_Psp_Negocios.P_Estatus + "', ");
                Mi_SQL.Append("'" + Sbn_Psp_Negocios.P_Codigo_Programatico + "', ");
                Mi_SQL.Append("GETDATE(),");
                Mi_SQL.Append("'" + Sbn_Psp_Negocios.P_Usuario_Creo + "') ");
               

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al tratar de Ingresar SubNivel Presupuesto. Error: [" + Ex.Message + "]");
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consecutivo_ID
        ///DESCRIPCIÓN          : consulta para obtener el consecutivo de una tabla
        ///PARAMETROS           1 Campo_Id: campo del que se obtendra el consecutivo
        ///                     2 Tabla: tabla del que se obtendra el consecutivo
        ///                     3 Tamaño: longitud del campo 
        ///CREO                 : Luis Daniel Guzmán Malagón
        ///FECHA_CREO           : 13/Julio/2012
        ///*******************************************************************************
        internal static String Consecutivo_ID(String Campo_Id, String Tabla, String Tamaño)
        {
            String Consecutivo = "";
            StringBuilder Mi_SQL = new StringBuilder();
            object Id; //Obtiene el ID con la cual se guardo los datos en la base de datos

            if (Tamaño.Equals("1"))
            {
                Mi_SQL.Append("SELECT ISNULL(MAX (" + Campo_Id + "), 0)");
                Mi_SQL.Append(" FROM " + Tabla);

                Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (Convert.IsDBNull(Id))
                {
                    Consecutivo = "1";
                }
                else
                {
                    Consecutivo = "" + (Convert.ToInt32(Id) + 1);
                }
            }
            else if (Tamaño.Equals("5"))
            {
                Mi_SQL.Append("SELECT ISNULL(MAX (" + Campo_Id + "), '00000')");
                Mi_SQL.Append(" FROM " + Tabla);

                Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (Convert.IsDBNull(Id))
                {
                    Consecutivo = "00001";
                }
                else
                {
                    Consecutivo = string.Format("{0:00000}", Convert.ToInt32(Id) + 1);
                }
            }
            else if (Tamaño.Equals("10"))
            {
                Mi_SQL.Append("SELECT ISNULL(MAX (" + Campo_Id + "), '0000000000')");
                Mi_SQL.Append(" FROM " + Tabla);

                Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (Convert.IsDBNull(Id))
                {
                    Consecutivo = "0000000001";
                }
                else
                {
                    Consecutivo = string.Format("{0:0000000000}", Convert.ToInt32(Id) + 1);
                }
            }

            return Consecutivo;
        }
         ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Unidades_Responsables
        ///DESCRIPCIÓN          : Consulta las Dependencias registradas en el sistema
        ///PARAMETROS           : Objeto de la clase Cls_Cat_Psp_SubNivel_Presupuesto_Negocio
        ///CREO                 : Jennyfer Ivonne Cceja Lemus
        ///FECHA_CREO           : 17/Agosto/2012 12:11 pm
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_Unidades_Responsables(Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sbn_Psp_Negocio)
        {
            StringBuilder MiSql = new StringBuilder();

            try {
                MiSql.Append("SELECT " + Cat_Dependencias.Campo_Dependencia_ID + ", ");
                MiSql.Append(Cat_Dependencias.Campo_Clave + " + '-' + " + Cat_Dependencias.Campo_Nombre + " AS CLAVE_NOMBRE ");
                MiSql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                MiSql.Append(" ORDER BY CLAVE_NOMBRE ");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }catch(Exception Ex){
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }
           ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_FTE_Financiamiento
        ///DESCRIPCIÓN          : Consulta lasfuentes de finaciamiento registradas en el sistema
        ///PARAMETROS           : Objeto de la clase Cls_Cat_Psp_SubNivel_Presupuesto_Negocio
        ///CREO                 : Jennyfer Ivonne Cceja Lemus
        ///FECHA_CREO           : 17/Agosto/2012 12:21 pm
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public static DataTable Consultar_FTE_Financiamiento(Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sbn_Psp_Negocio)
        {
            StringBuilder MiSql = new StringBuilder();

            try {
                MiSql.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", ");
                MiSql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' - ' + ");
                MiSql.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento+ "."+ Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                MiSql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + " LEFT JOIN " + Cat_SAP_Det_Fte_Dependencia.Tabla_Cat_SAP_Det_Fte_Financiamiento_Dependencia);
                MiSql.Append(" ON " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = " + Cat_SAP_Det_Fte_Dependencia.Tabla_Cat_SAP_Det_Fte_Financiamiento_Dependencia + "." + Cat_SAP_Det_Fte_Dependencia.Campo_Fuente_Financiamiento_ID);
                
                if (!String.IsNullOrEmpty(Sbn_Psp_Negocio.P_Dependencia_ID))
                {
                    if (MiSql.ToString().Trim().Contains("WHERE"))
                    {
                        MiSql.Append(" AND " + Cat_SAP_Det_Fte_Dependencia.Tabla_Cat_SAP_Det_Fte_Financiamiento_Dependencia + "." + Cat_SAP_Det_Fte_Dependencia.Campo_Dependencia_ID + " = '" + Sbn_Psp_Negocio.P_Dependencia_ID + "'");
                    }
                    else
                    {
                        MiSql.Append(" WHERE " + Cat_SAP_Det_Fte_Dependencia.Tabla_Cat_SAP_Det_Fte_Financiamiento_Dependencia + "." + Cat_SAP_Det_Fte_Dependencia.Campo_Dependencia_ID + " = '" + Sbn_Psp_Negocio.P_Dependencia_ID + "'");
                    }
                }

                MiSql.Append(" ORDER BY CLAVE_NOMBRE ");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }catch(Exception Ex){
                throw new Exception("Error al intentar consultar la fuente de finaiamiento Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Programa
        ///DESCRIPCIÓN          : Consulta los programas registrados en el sistema
        ///PARAMETROS           : Objeto de la clase Cls_Cat_Psp_SubNivel_Presupuesto_Negocio
        ///CREO                 : Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO           : 17/Agosto/2012 12:41 pm
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
         public static DataTable Consultar_Programa(Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sbn_Psp_Negocio)
        {
            StringBuilder MiSql = new StringBuilder();

            try {
                MiSql.Append("SELECT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ", ");
                MiSql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' - ' + ");
                MiSql.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas+ "."+ Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                MiSql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + " JOIN " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia);
                MiSql.Append(" ON " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + " = " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia + "." + Cat_SAP_Det_Prog_Dependencia.Campo_Proyecto_Programa_ID);
                
                if (!String.IsNullOrEmpty(Sbn_Psp_Negocio.P_Dependencia_ID))
                {
                    if (MiSql.ToString().Trim().Contains("WHERE"))
                    {
                        MiSql.Append(" AND " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia+ "." + Cat_SAP_Det_Prog_Dependencia.Campo_Dependencia_ID + " = '" + Sbn_Psp_Negocio.P_Dependencia_ID + "'");
                    }
                    else
                    {
                        MiSql.Append(" WHERE " + Cat_SAP_Det_Prog_Dependencia.Tabla_Cat_SAP_Det_Programa_Dependencia + "." + Cat_SAP_Det_Prog_Dependencia.Campo_Dependencia_ID + " = '" + Sbn_Psp_Negocio.P_Dependencia_ID + "'");
                    }
                }

                MiSql.Append(" ORDER BY CLAVE_NOMBRE ");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

            }catch(Exception Ex){
                throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
            }
        }
         ///********************************************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Capitulos
         ///DESCRIPCIÓN          : Metod para consultar los capitulos
         ///PROPIEDADES          :
         ///CREO                 : Jennyfer Ivonne Ceja Lemus
         ///FECHA_CREO           : 17/Agosto/2012 13:00 PM
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN...:
         ///*********************************************************************************************************
         public static DataTable Consultar_Capitulos(Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Negocio)
         {
             StringBuilder Mi_Sql = new StringBuilder();
             try
             {
                 Mi_Sql.Append("SELECT " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + ", ");
                   Mi_Sql.Append( Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Clave + " +' '+ ");
                 Mi_Sql.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Descripcion + " AS NOMBRE ");
                 Mi_Sql.Append(" FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                 Mi_Sql.Append(" ORDER BY NOMBRE ");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de los capitulos. Error: [" + Ex.Message + "]");
             }
         }
         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Consultar_Capitulos
         ///DESCRIPCIÓN          : consulta para obtener los capitulos de una unidad responsable
         ///PARAMETROS           1 Negocio conexion con la capa de negocio 
         ///CREO                 : Leslie Gonzalez Vázquez
         ///FECHA_CREO           : 10/Noviembre/2011
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         internal static DataTable Consultar_Partidas_Especificas(Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Negocio)
         {
             StringBuilder Mi_SQL = new StringBuilder();
             try
             {
                 Mi_SQL.Append("SELECT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ", ");
                 Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + "+' - '+ ");
                 Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS NOMBRE ");
                 Mi_SQL.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + ", ");
                 Mi_SQL.Append(Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + ", ");
                 Mi_SQL.Append(Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + ", ");
                 Mi_SQL.Append(Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                 Mi_SQL.Append(" WHERE " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                 Mi_SQL.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID);
                 Mi_SQL.Append(" AND " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                 Mi_SQL.Append(" = " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas + "." + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID);
                 Mi_SQL.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                 Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                 Mi_SQL.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Estatus + " = 'ACTIVO'");
                 Mi_SQL.Append(" AND " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID + " = '" + Negocio.P_Capitulo_ID + "'");
                 Mi_SQL.Append(" ORDER BY " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " ASC");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar los registros de las partidas especificas. Error: [" + Ex.Message + "]");
             }
         }
         ///*******************************************************************************
         ///NOMBRE DE LA FUNCIÓN : Obtener_Area_Funcional
         ///DESCRIPCIÓN          : Consulta las Dependencias registradas en el sistema
         ///PARAMETROS           : Objeto de la clase Cls_Cat_Psp_SubNivel_Presupuesto_Negocio
         ///CREO                 : Jennyfer Ivonne Cceja Lemus
         ///FECHA_CREO           : 17/Agosto/2012 12:11 pm
         ///MODIFICO             :
         ///FECHA_MODIFICO       :
         ///CAUSA_MODIFICACIÓN   :
         ///*******************************************************************************
         public static DataTable Obtener_Area_Funcional(Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sbn_Psp_Negocio)
         {
             StringBuilder MiSql = new StringBuilder();

             try
             {
                 MiSql.Append("SELECT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + ", " );
                 MiSql.Append(Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Clave);
                 MiSql.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                 MiSql.Append(" JOIN " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
                 MiSql.Append(" ON " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Area_Funcional_ID + " = " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional + "." + Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID);
                 MiSql.Append(" WHERE " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + " = '" + Sbn_Psp_Negocio.P_Dependencia_ID + "'");

                 return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MiSql.ToString()).Tables[0];

             }
             catch (Exception Ex)
             {
                 throw new Exception("Error al intentar consultar las Unidades Responsables. Error: [" + Ex.Message + "]");
             }
         }

        #endregion

    }
}
