﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Tipo_Presupuesto.Negocio;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using System.Text;
using JAPAMI.Manejo_Presupuesto.Datos;

namespace JAPAMI.Tipo_Presupuesto.Datos
{
    public class Cls_Ope_Psp_Tipo_Presupuesto_Datos
    {
        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Anio
        ///DESCRIPCIÓN          : consulta para obtener los datos del año presupuestado
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 21/Febrero/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Anio()
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT " + Cat_Psp_Parametros.Campo_Anio_Presupuestar);
                Mi_Sql.Append(" FROM  " + Cat_Psp_Parametros.Tabla_Cat_Psp_Parametros);
                Mi_Sql.Append(" WHERE   " + Cat_Psp_Parametros.Campo_Estatus + " = 'ACTIVO'");
                Mi_Sql.Append(" AND " + Cat_Psp_Parametros.Campo_Anio_Presupuestar + " NOT IN ");
                Mi_Sql.Append(" (SELECT " + Cat_Parametros_Ejercer_Psp.Campo_Anio);
                Mi_Sql.Append(" FROM " + Cat_Parametros_Ejercer_Psp.Tabla_Cat_Parametros_Ejercer_Psp + ")");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los anios. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Guardar_Parametros
        ///DESCRIPCIÓN          : Consulta para guardar los parametros del ejercer del presupuesto
        ///PARAMETROS           1 Negocio conexion con la capa de negocio 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 21/Febrero/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static Boolean Guardar_Parametros(Cls_Ope_Psp_Tipo_Presupuesto_Negocio Tipo_Presupuesto_Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            Boolean Operacion_Exitosa = false;
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            SqlTransaction Trans;
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmd.Connection = Cn;
            Cmd.Transaction = Trans;

            try
            {

                Mi_SQL.Append("INSERT INTO " + Cat_Parametros_Ejercer_Psp.Tabla_Cat_Parametros_Ejercer_Psp  + "(");
                Mi_SQL.Append(Cat_Parametros_Ejercer_Psp.Campo_Anio + ", ");
                Mi_SQL.Append(Cat_Parametros_Ejercer_Psp.Campo_Tipo_De_Consulta +") VALUES(");
                Mi_SQL.Append("'" + Tipo_Presupuesto_Negocio.P_Anio + "', ");
                Mi_SQL.Append("'" + Tipo_Presupuesto_Negocio.P_Tipo_Presupuesto.ToUpper() + "') ");
                Cmd.CommandText = Mi_SQL.ToString();
                Cmd.ExecuteNonQuery();

                Mi_SQL = new StringBuilder();
                if (Tipo_Presupuesto_Negocio.P_Tipo_Presupuesto.ToUpper().Equals("ANUAL"))
                {
                    Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_SQL.Append( " SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible);
                    Mi_SQL.Append( "=" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total);
                    Mi_SQL.Append(", ACTUALIZADO ='SI'");
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_SQL.Append( "=EXTRACT(YEAR FROM GETDATE())");
                    Mi_SQL.Append( " AND ACTUALIZADO='NO'");
                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();
                }
                else if (Tipo_Presupuesto_Negocio.P_Tipo_Presupuesto.ToUpper().Equals("MENSUAL") || Tipo_Presupuesto_Negocio.P_Tipo_Presupuesto.ToUpper().Equals("ACUMULADO MENSUAL"))
                {
                    Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                    Mi_SQL.Append(" SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Disponible);
                    Mi_SQL.Append("=" + Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero);
                    Mi_SQL.Append(", ACTUALIZADO ='SI'");
                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio);
                    Mi_SQL.Append("=EXTRACT(YEAR FROM GETDATE())");
                    Mi_SQL.Append(" AND ACTUALIZADO='NO'");
                    Cmd.CommandText = Mi_SQL.ToString();
                    Cmd.ExecuteNonQuery();
                }

                Trans.Commit();
                Operacion_Exitosa = true;
            }
            catch (Exception Ex)
            {
                Operacion_Exitosa = false;
                Trans.Rollback();
                throw new Exception("Error al intentar guardar el historial de los registros. Error: [" + Ex.Message + "]");
            }
            return Operacion_Exitosa;
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Parametros
        ///DESCRIPCIÓN          : consulta para obtener los parametros del ejercer del presupuesto
        ///PARAMETROS           : 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 21/Febrero/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Parametros()
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append(" SELECT * FROM " + Cat_Parametros_Ejercer_Psp.Tabla_Cat_Parametros_Ejercer_Psp);
                Mi_Sql.Append(" ORDER BY " + Cat_Parametros_Ejercer_Psp.Campo_Anio + " DESC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los parametros. Error: [" + Ex.Message + "]");
            }
        }
    }
}
