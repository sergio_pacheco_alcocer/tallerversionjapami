﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Cat_Psp_Clases_Ing.Negocio;
using JAPAMI.Cat_Psp_Tipos.Negocio;
using JAPAMI.Cat_Psp_Rubros.Datos;

namespace JAPAMI.Cat_Psp_Clases_Ing.Datos
{
    public class Cls_Cat_Psp_Clase_Ing_Datos
    {
        #region Metodos
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Clase_Ing
        ///DESCRIPCIÓN          : consulta para obtener los datos de las clases
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 20/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Clase_Ing(Cls_Cat_Psp_Clase_Ing_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT " + Cat_Psp_Clase_Ing.Campo_Clave + " + ' ' + ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Campo_Clave + ", ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Campo_Descripcion + ", ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Campo_Estatus + ", ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Campo_Tipo_ID);
                Mi_Sql.Append(" FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);

                if (!String.IsNullOrEmpty(Negocio.P_Clave))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Campo_Clave + " = '" + Negocio.P_Clave.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Clase_Ing.Campo_Clave + " = '" + Negocio.P_Clave.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Descripcion))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Campo_Descripcion + " = '" + Negocio.P_Descripcion.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Clase_Ing.Campo_Descripcion + " = '" + Negocio.P_Descripcion.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Estatus))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Campo_Estatus + " = '" + Negocio.P_Estatus.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Clase_Ing.Campo_Estatus + " = '" + Negocio.P_Estatus.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Clase_Ing.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Anio))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Campo_Anio + " = '" + Negocio.P_Anio.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Clase_Ing.Campo_Anio + " = '" + Negocio.P_Anio.Trim() + "'");
                    }
                }

                Mi_Sql.Append(" ORDER BY " + Cat_Psp_Clase_Ing.Campo_Clave + " ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las clases. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos_Clase_Ing
        ///DESCRIPCIÓN          : consulta para obtener los datos de los tipos y las clases
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 20/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Tipos_Clase_Ing(Cls_Cat_Psp_Clase_Ing_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " + ' ' + ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CLAVE_NOMBRE_TIPO, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " AS CLAVE_TIPOS, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS DESCRIPCION_TIPO, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Estatus + " AS ESTATUS_TIPO, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Anio + " AS ANIO_TIPO, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + ", ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " + ' ' + ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE_CLASE, ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " AS CLAVE_CLASE, ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS DESCRIPCION_CLASE, ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Estatus + " AS ESTATUS_CLASE, ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Anio + " AS ANIO_CLASE, ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                Mi_Sql.Append(" FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                Mi_Sql.Append(" ON " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                Mi_Sql.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Tipo_ID);

                if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    }
                }

                Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE_CLASE ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de las clases y los tipos. Error: [" + Ex.Message + "]");
            }
        }

        //*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Datos_Ing
        ///DESCRIPCIÓN          : consulta para obtener los datos de los tipos y las clases, rubros y conceptos
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 20/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Datos_Ing(Cls_Cat_Psp_Clase_Ing_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " + ' ' + ");
                Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CLAVE_NOMBRE_RUBRO, ");
                Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " AS CLAVE_RUBRO, ");
                Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS DESCRIPCION_RUBRO, ");
                Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Estatus + " AS ESTATUS_RUBRO, ");
                Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID + ", ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " + ' ' + ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CLAVE_NOMBRE_TIPO, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " AS CLAVE_TIPOS, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS DESCRIPCION_TIPO, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Estatus + " AS ESTATUS_TIPO, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + ", ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " + ' ' + ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE_CLASE, ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " AS CLAVE_CLASE, ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS DESCRIPCION_CLASE, ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Estatus + " AS ESTATUS_CLASE, ");
                Mi_Sql.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ");
                Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " + ' ' + ");
                Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CLAVE_NOMBRE_CONCEPTO, ");
                Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + " AS CLAVE_CONCEPTO, ");
                Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS DESCRIPCION_CONCEPTO, ");
                Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Estatus + " AS ESTATUS_CONCEPTO, ");
                Mi_Sql.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                Mi_Sql.Append(" FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                Mi_Sql.Append(" ON " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Rubro_ID);
                Mi_Sql.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                Mi_Sql.Append(" ON " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Tipo_ID);
                Mi_Sql.Append(" = " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                Mi_Sql.Append(" LEFT OUTER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                Mi_Sql.Append(" ON " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID);
                Mi_Sql.Append(" = " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);


                if (!String.IsNullOrEmpty(Negocio.P_Clase_Ing_ID))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Clase_Ing_ID.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    }
                }


                if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Concepto_Ing_ID))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Concepto_Ing_ID.Trim() + "'");
                    }
                }

                Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE_RUBRO ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los datos de los ingresos. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar_Clase_Ing
        ///DESCRIPCIÓN          : consulta para eliminar los datos de las clases
        ///PARAMETROS           1 Negocio: conexion con la capa de negocios
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 20/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static Boolean Eliminar_Clase_Ing(Cls_Cat_Psp_Clase_Ing_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
            Boolean Operacion_Completa = false;//Estado de la operacion.

            try
            {
                Mi_SQL.Append("UPDATE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                Mi_SQL.Append(" SET " + Cat_Psp_Clase_Ing.Campo_Estatus + " = 'INACTIVO'");
                Mi_SQL.Append(" WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID + "'");

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al ejecutar Eliminar las clases. Error: [" + Ex.Message + "]");
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Clase_Ing
        ///DESCRIPCIÓN          : consulta para modificar los datos de las clases
        ///PARAMETROS           1 Negocio: conexion con la capa de negocios
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 20/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static Boolean Modificar_Clase_Ing(Cls_Cat_Psp_Clase_Ing_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
            Boolean Operacion_Completa = false;//Estado de la operacion.

            try
            {
                Mi_SQL.Append("UPDATE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                Mi_SQL.Append(" SET " + Cat_Psp_Clase_Ing.Campo_Clave + " = '" + Negocio.P_Clave + "', ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Campo_Descripcion + " = '" + Negocio.P_Descripcion + "', ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Campo_Anio + " = " + Negocio.P_Anio + ", ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Campo_Estatus + " = '" + Negocio.P_Estatus + "', ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID + "', ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico + "', ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Negocio.P_Clase_Ing_ID + "' ");

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al ejecutar los la modificación de las clases. Error: [" + Ex.Message + "]");
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Clase_Ing
        ///DESCRIPCIÓN          : consulta para guardar los datos de las clases
        ///PARAMETROS           1 Negocio: conexion con la capa de negocios
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 20/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static Boolean Alta_Clase_Ing(Cls_Cat_Psp_Clase_Ing_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
            Boolean Operacion_Completa = false;//Estado de la operacion.
            String Id = Cls_Cat_Psp_Rubros_Datos.Consecutivo_ID(Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID, Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing, "10");

            try
            {
                Mi_SQL.Append("INSERT INTO " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "(");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Campo_Tipo_ID + ", ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Campo_Clave + ", ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Campo_Descripcion + ", ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Campo_Anio + ", ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Campo_Estatus + ", ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Campo_Usuario_Creo + ", ");
                Mi_SQL.Append(Cat_Psp_Clase_Ing.Campo_Fecha_Creo + ") VALUES( ");
                Mi_SQL.Append("'" + Id + "', ");
                Mi_SQL.Append("'" + Negocio.P_Tipo_ID + "', ");
                Mi_SQL.Append("'" + Negocio.P_Clave + "', ");
                Mi_SQL.Append("'" + Negocio.P_Descripcion + "', ");
                Mi_SQL.Append(Negocio.P_Anio + ", ");
                Mi_SQL.Append("'" + Negocio.P_Estatus + "', ");
                Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "', ");
                Mi_SQL.Append("GETDATE()) ");

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al ejecutar el alta de las clases. Error: [" + Ex.Message + "]");
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Conceptos_Ing
        ///DESCRIPCIÓN          : Obtiene los Conceptos de acuerdo a los filtros establecidos en la interfaz
        ///PARAMETROS           : Conceptos, instancia de Cls_Cat_Psp_SubConceptos_Negocio
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 21/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Clases_Ing(Cls_Cat_Psp_Clase_Ing_Negocio Clases)
        {
            DataTable Tabla = new DataTable();
            String Mi_SQL;
            String Mi_SQL_Campos_Foraneos = "";
            try
            {
                if (Clases.P_Campos_Dinamicos != null && Clases.P_Campos_Dinamicos != "")
                {
                    Mi_SQL = "SELECT " + Mi_SQL_Campos_Foraneos + Clases.P_Campos_Dinamicos;
                }
                else
                {
                    Mi_SQL = "SELECT ";
                    Mi_SQL += Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ";
                    Mi_SQL += Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Tipo_ID + ", ";
                    Mi_SQL += Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Anio + ", ";
                    Mi_SQL += Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ", ";
                    Mi_SQL += Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + ", ";
                    Mi_SQL += Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Estatus + ", ";
                    if (Mi_SQL.EndsWith(", "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 2);
                    }
                }
                Mi_SQL += " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing;
                if (Clases.P_Unir_Tablas != null && Clases.P_Unir_Tablas != "")
                {
                    Mi_SQL += ", " + Clases.P_Unir_Tablas;
                }
                else
                {
                    if (Clases.P_Join != null && Clases.P_Join != "")
                    {
                        Mi_SQL += " " + Clases.P_Join;
                    }
                }
                if (Clases.P_Filtros_Dinamicos != null && Clases.P_Filtros_Dinamicos != "")
                {
                    Mi_SQL += " WHERE " + Clases.P_Filtros_Dinamicos;
                }
                else
                {
                    Mi_SQL += " WHERE ";
                    if (Clases.P_Clase_Ing_ID != null && Clases.P_Clase_Ing_ID != "")
                    {
                        Mi_SQL += Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Clases.P_Clase_Ing_ID + "' AND ";
                    }
                    if (Clases.P_Tipo_ID != null && Clases.P_Tipo_ID != "")
                    {
                        Mi_SQL += Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Tipo_ID + " = '" + Clases.P_Tipo_ID + "' AND ";
                    }
                    if (Clases.P_Anio != null && Clases.P_Anio != "")
                    {
                        Mi_SQL += Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Anio + " = " + Clases.P_Anio + " AND ";
                    }
                    if (Clases.P_Clave != null && Clases.P_Clave != "")
                    {
                        Mi_SQL += Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + " = '" + Clases.P_Clave + "' AND ";
                    }
                    if (Clases.P_Estatus != null && Clases.P_Estatus != "")
                    {
                        Mi_SQL += Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Estatus + Validar_Operador_Comparacion(Clases.P_Estatus) + " AND ";
                    }
                    if (Mi_SQL.EndsWith(" AND "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 5);
                    }
                    if (Mi_SQL.EndsWith(" WHERE "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 7);
                    }
                }
                if (Clases.P_Agrupar_Dinamico != null && Clases.P_Agrupar_Dinamico != "")
                {
                    Mi_SQL += " GROUP BY " + Clases.P_Agrupar_Dinamico;
                }
                if (Clases.P_Ordenar_Dinamico != null && Clases.P_Ordenar_Dinamico != "")
                {
                    Mi_SQL += " ORDER BY " + Clases.P_Ordenar_Dinamico;
                }
                DataSet dataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataSet != null)
                {
                    Tabla = dataSet.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de la Cuentas. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Tabla;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Operador_Comparacion
        ///DESCRIPCIÓN          : Devuelve una cadena adecuada al operador indicado en la capa de Negocios
        ///PARAMETROS           : 
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 20/Agosto/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private static String Validar_Operador_Comparacion(String Filtro)
        {
            String Cadena_Validada;
            if (Filtro.Trim().StartsWith("<")
               || Filtro.Trim().StartsWith(">")
               || Filtro.Trim().StartsWith("<>")
               || Filtro.Trim().StartsWith("<=")
               || Filtro.Trim().StartsWith(">=")
               || Filtro.Trim().StartsWith("=")
               || Filtro.Trim().ToUpper().StartsWith("BETWEEN")
               || Filtro.Trim().ToUpper().StartsWith("LIKE")
               || Filtro.Trim().ToUpper().StartsWith("IN"))
            {
                Cadena_Validada = " " + Filtro + " ";
            }
            else
            {
                if (Filtro.Trim().ToUpper().StartsWith("NULL"))
                {
                    Cadena_Validada = " IS " + Filtro + " ";
                }
                else
                {
                    Cadena_Validada = " = '" + Filtro + "' ";
                }
            }
            return Cadena_Validada;
        }
        #endregion
    }
}
