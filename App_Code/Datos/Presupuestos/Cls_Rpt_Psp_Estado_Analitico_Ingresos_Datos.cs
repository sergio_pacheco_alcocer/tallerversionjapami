﻿using System;
using System.Data;
using System.Text;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Rpt_Psp_Estado_Analitico_Ingresos.Negocio;

namespace JAPAMI.Rpt_Psp_Estado_Analitico_Ingresos.Datos
{
    public class Cls_Rpt_Psp_Estado_Analitico_Ingresos_Datos
    {
        #region(Metodos)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Anios
            ///DESCRIPCIÓN          : Obtiene datos de los años del estado analitico de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 02/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Anios()
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds_Anios = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt_Anios = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Ope_Psp_Pronostico_Ingresos.Campo_Anio);
                    Mi_SQL.Append(" FROM " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos);
                    Mi_SQL.Append(" ORDER BY " + Ope_Psp_Pronostico_Ingresos.Campo_Anio + " DESC");
                   

                    Ds_Anios = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds_Anios != null)
                    {
                        Dt_Anios = Ds_Anios.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de los años. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Anios;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_FF
            ///DESCRIPCIÓN          : Obtiene datos de  las fuentes de financiamiento del estado analitico de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 162/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_FF()
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds_FF = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt_FF = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_SQL.Append(" ON " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Fuente_Financiamiento_ID);
                    Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");


                    Ds_FF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds_FF != null)
                    {
                        Dt_FF = Ds_FF.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de las fuentes de fianciamiento. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_FF;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Pronostico_Ingresos
            ///DESCRIPCIÓN          : Obtiene datos de  las unidades responsables del estado analitico de ingresos
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 02/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Pronostico_Ingresos(Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio Negocio )
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds_Pronostico = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt_Pronostico = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT (CASE WHEN ");
                    Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                    Mi_SQL.Append(" THEN ");
                    Mi_SQL.Append("REPLACE(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ' ', '') +' '+");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                    Mi_SQL.Append(" ELSE ");
                    Mi_SQL.Append("REPLACE(" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ' ', '') +' '+");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion); 
                    Mi_SQL.Append(" END) AS CONCEPTO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Enero + ", 0)) AS IMP_ENERO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Febrero + ", 0)) AS IMP_FEBRERO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Marzo + ", 0)) AS IMP_MARZO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Abril + ", 0)) AS IMP_ABRIL, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Mayo + ", 0)) AS IMP_MAYO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Junio + ", 0)) AS IMP_JUNIO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Julio + ", 0)) AS IMP_JULIO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Agosto + ", 0)) AS IMP_AGOSTO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Septiembre + ", 0)) AS IMP_SEPTIEMBRE, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Octubre + ", 0)) AS IMP_OCTUBRE, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Noviembre + ", 0)) AS IMP_NOVIEMBRE, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Diciembre + ", 0)) AS IMP_DICIEMBRE, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Importe_Total + ", 0)) AS IMP_TOTAL, ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS RUBRO, ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " AS CLAVE_RUBRO, ");
                    Mi_SQL.Append("'0' AS ACUMULADO_ANIO, ");
                    Mi_SQL.Append("RTRIM(LTRIM(" + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ")) + ' ' + "
                        + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CLAVE_NOM_RUBRO, ");
                    Mi_SQL.Append("RTRIM(LTRIM(" + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ")) + ' ' + "
                        + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CLAVE_NOM_TIPO, ");
                    Mi_SQL.Append("RTRIM(LTRIM(" +Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ")) + ' ' + "
                        + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLAVE_NOM_CLASE, ");
                    Mi_SQL.Append("RTRIM(LTRIM(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ")) + ' ' + "
                        + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CLAVE_NOM_CONCEPTO ");
                    //Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", 0)) AS ACUMULADO_ANIO ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                    Mi_SQL.Append(" ON " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Rubro_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                    Mi_SQL.Append(" ON " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Concepto_Ing_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                    Mi_SQL.Append(" ON " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_SubConcepto_Ing_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " ON " + 
                        Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Tipo_ID + " = " + 
                        Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " ON " + 
                        Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Clase_Ing_ID + " = " + 
                        Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);


                    if (!String.IsNullOrEmpty(Negocio.P_Anio)) 
                    {
                        if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }
                        else 
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID))
                    {
                        if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Estatus))
                    {
                        if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Estatus);
                            Mi_SQL.Append(" IN(" + Negocio.P_Estatus.Trim().ToUpper() + ")");
                        }
                        else
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_Estatus);
                            Mi_SQL.Append(" IN(" + Negocio.P_Estatus.Trim().ToUpper() + ")");
                        }
                    }
                    Mi_SQL.Append(" GROUP BY " );
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos + "." + Ope_Psp_Pronostico_Ingresos.Campo_SubConcepto_Ing_ID+ ", ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", "
                       + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ", "
                        + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ", "
                        + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", "
                        + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                    Mi_SQL.Append(" ORDER BY CONCEPTO ASC");

                    Ds_Pronostico = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds_Pronostico != null)
                    {
                        Dt_Pronostico = Ds_Pronostico.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros del pronostico de ingresos. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Pronostico;
            }


            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Presupuesto_Ingresos
            ///DESCRIPCIÓN          : Obtiene datos de  las unidades responsables del presupuesto de ingresos
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 19/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Presupuesto_Ingresos(Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds_Pronostico = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt_Pronostico = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT (CASE WHEN ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                    Mi_SQL.Append(" THEN ");
                    Mi_SQL.Append("REPLACE(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ",' ' ,'') +' '+");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                    Mi_SQL.Append(" ELSE ");
                    Mi_SQL.Append("REPLACE(" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ",' ' ,'') +' '+");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                    Mi_SQL.Append(" END) AS CONCEPTO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ", 0)) AS IMP_ENERO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ", 0)) AS IMP_FEBRERO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ", 0)) AS IMP_MARZO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ", 0)) AS IMP_ABRIL, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ", 0)) AS IMP_MAYO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ", 0)) AS IMP_JUNIO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ", 0)) AS IMP_JULIO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ", 0)) AS IMP_AGOSTO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ", 0)) AS IMP_SEPTIEMBRE, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ", 0)) AS IMP_OCTUBRE, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ", 0)) AS IMP_NOVIEMBRE, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ", 0)) AS IMP_DICIEMBRE, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ", 0)) AS IMP_TOTAL, ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS RUBRO, ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " AS CLAVE_RUBRO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", 0)) AS APROBADO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ", 0)) AS AMPLIACION, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ", 0)) AS REDUCCION, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ", 0)) AS MODIFICADO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", 0)) AS ACUMULADO_ANIO, ");
                    Mi_SQL.Append("SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ", 0)) AS POR_RECAUDAR, ");
                    Mi_SQL.Append("RTRIM(LTRIM(" + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ")) + ' ' + "
                        + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CLAVE_NOM_RUBRO, ");
                    Mi_SQL.Append("RTRIM(LTRIM(" + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ")) + ' ' + "
                        + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CLAVE_NOM_TIPO, ");
                    Mi_SQL.Append("RTRIM(LTRIM(" + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ")) + ' ' + "
                        + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + " AS CLAVE_NOM_CLASE, ");
                    Mi_SQL.Append("RTRIM(LTRIM(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ")) + ' ' + "
                        + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + " AS CLAVE_NOM_CONCEPTO ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " ON " +
                       Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + " = " +
                       Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + " ON " +
                        Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + " = " +
                        Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);

                    if (!String.IsNullOrEmpty(Negocio.P_Anio))
                    {
                        if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }
                        else
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID))
                    {
                        if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                    }

                    Mi_SQL.Append(" GROUP BY ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + ", ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", "
                        + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ", "
                        + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Clave + ", "
                        + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing + "." + Cat_Psp_Clase_Ing.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", "
                        + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);

                    Mi_SQL.Append(" ORDER BY CONCEPTO ASC");

                    Ds_Pronostico = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds_Pronostico != null)
                    {
                        Dt_Pronostico = Ds_Pronostico.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros del pronostico de ingresos. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Pronostico;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_FF
            ///DESCRIPCIÓN          : Obtiene datos de  las fuentes de financiamiento del estado analitico de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 162/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_FF_Presupuestos()
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds_FF = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt_FF = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT DISTINCT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS CLAVE_NOMBRE ");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_SQL.Append(" ON " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + "." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                    Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                    Mi_SQL.Append(" ORDER BY CLAVE_NOMBRE ASC");


                    Ds_FF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds_FF != null)
                    {
                        Dt_FF = Ds_FF.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de las fuentes de fianciamiento. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_FF;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Mov_Ingresos
            ///DESCRIPCIÓN          : Obtiene datos de los movimientos del presupuesto de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Mov_Ingresos(Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_Sql = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds= new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt = new DataTable();
                try
                {
                    Mi_Sql.Append("SELECT DISTINCT 'MODIFICACION ' + ' ' + ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " AS MODIFICACION , ");
                    Mi_Sql.Append(Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " AS NO_MOVIMIENTO ");
                    Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Ing.Tabla_Ope_Psp_Movimiento_Ing);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Ing.Campo_Anio + " = " + Negocio.P_Anio.Trim());
                    Mi_Sql.Append(" AND " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " IN (");
                    Mi_Sql.Append(" SELECT " + Ope_Psp_Movimiento_Ing_Det.Campo_No_Movimiento_Ing);
                    Mi_Sql.Append(" FROM " + Ope_Psp_Movimiento_Ing_Det.Tabla_Ope_Psp_Movimiento_Ing_Det);
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Movimiento_Ing_Det.Campo_Anio + " = " + Negocio.P_Anio.Trim() + ")");

                    Mi_Sql.Append(" ORDER BY " + Ope_Psp_Movimiento_Ing.Campo_No_Movimiento_Ing + " ASC");

                    Ds = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                    if (Ds != null)
                    {
                        Dt = Ds.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de los movimientos de ingresos. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Resumen_Presupuesto_Ingresos
            ///DESCRIPCIÓN          : Obtiene datos de  las unidades responsables del presupuesto de ingresos
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 19/Abril/2012
            ///MODIFICO             : Armando Zavala Moreno
            ///FECHA_MODIFICO       : 06/Agosto/2012
            ///CAUSA_MODIFICACIÓN   : Se agregaron nuevas columnas 
            ///*******************************************************************************
            internal static DataTable Consultar_Resumen_Presupuesto_Ingresos(Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds_Pronostico = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt_Pronostico = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT (CASE WHEN ");
                    Mi_SQL.Append("ING." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + " IS NULL");
                    Mi_SQL.Append(" THEN ");
                    Mi_SQL.Append("REPLACE(" + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ",' ' ,'') +' '+");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion);
                    Mi_SQL.Append(" ELSE ");
                    Mi_SQL.Append("REPLACE(" + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ",' ' ,'') +' '+");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion);
                    Mi_SQL.Append(" END) AS CONCEPTO, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ", 0)) AS IMP_ENERO, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ", 0)) AS IMP_FEBRERO, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ", 0)) AS IMP_MARZO, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ", 0)) AS IMP_ABRIL, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ", 0)) AS IMP_MAYO, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ", 0)) AS IMP_JUNIO, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ", 0)) AS IMP_JULIO, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ", 0)) AS IMP_AGOSTO, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ", 0)) AS IMP_SEPTIEMBRE, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ", 0)) AS IMP_OCTUBRE, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ", 0)) AS IMP_NOVIEMBRE, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ", 0)) AS IMP_DICIEMBRE, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ", 0)) AS IMP_TOTAL, ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS RUBRO, ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " AS CLAVE_RUBRO, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", 0)) AS APROBADO, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ", 0)) AS AMPLIACION, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ", 0)) AS REDUCCION, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ", 0)) AS MODIFICADO, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", 0)) AS ACUMULADO_ANIO, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ", 0)) AS POR_RECAUDAR, ");
                    //NUEVAS COLUMNAS
                    Mi_SQL.Append("SUM(ISNULL(ING." + Negocio.P_Acumulado_Mes + ", 0)) ACUMULADO_MES_ACTUAL, ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Negocio.P_Importe_Mes + ", 0)) PRESUPUESTO_MENSUAL, ");
                    Mi_SQL.Append("(SUM(ISNULL(ING." + Negocio.P_Importe_Mes + ", 0)) - ");
                    Mi_SQL.Append("SUM(ISNULL(ING." + Negocio.P_Acumulado_Mes + ", 0))) PRESUPUESTO_RECAUDAR_MES, ");

                    //PORCENTAJE POR RECAUDAR EN EL MES ACTUAL
                    Mi_SQL.Append("(CASE WHEN ING." + Negocio.P_Importe_Mes + "=0 THEN 0 ELSE ((SUM(ISNULL(ING." + Negocio.P_Importe_Mes + ", 0)) - SUM(ISNULL(ING." + Negocio.P_Acumulado_Mes
                        + ", 0)))*100/(SUM(ISNULL(ING." + Negocio.P_Importe_Mes + ", 0)))) END)POR_RECAUDAR_EN_EL_MES, ");

                    //PORCENTAJE POR RECAUDAR EN EL ANIO ACUTAL
                    Mi_SQL.Append("ROUND((CASE WHEN SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", 0))=0 THEN 0 ELSE (SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar
                        + ", 0))*100/(SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", 0)))) END),2)POR_RECAUDAR_EN_EL_ANIO, ");

                    //ACUMULADO ANIO ANTERIOR
                    Mi_SQL.Append("ISNULL((SELECT SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", 0)) FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos
                        + " WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + "=" + (Convert.ToInt32(Negocio.P_Anio) - 1).ToString() + " AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID
                        + "=ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + "=ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + "),0)ACU_ANIO_ANTERIOR, ");

                    //COMPARATIVO DE ACUMULADO AL ANIO ANTERIOR CON ANIO ACTUAL
                    Mi_SQL.Append("(SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", 0))- ISNULL((SELECT SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", 0)) FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos
                        + " WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + "=" + (Convert.ToInt32(Negocio.P_Anio) - 1).ToString() + " AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID
                        + "=ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + "=ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + "),0))COM_ACU_ANIOS, ");

                    //PORCENTAJE DEL COMPARATIVO
                    Mi_SQL.Append("ROUND((CASE WHEN SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", 0))=0 THEN 0 ELSE ((ISNULL((SELECT SUM(ISNULL(" + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", 0)) FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos
                        + " WHERE " + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + "=" + (Convert.ToInt32(Negocio.P_Anio) - 1).ToString() + " AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID
                        + "=ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + " AND " + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + "=ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + "),0)*100)/(SUM(ISNULL(ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", 0)))) END),2)POR_DEL_COMPARATIVO");

                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + " ING");
                    Mi_SQL.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                    Mi_SQL.Append(" ON ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                    Mi_SQL.Append(" ON ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing);
                    Mi_SQL.Append(" ON ING." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID);
                    Mi_SQL.Append(" = " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID);


                    if (!String.IsNullOrEmpty(Negocio.P_Anio))
                    {
                        if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" AND ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }
                        else
                        {
                            Mi_SQL.Append(" WHERE ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio);
                            Mi_SQL.Append(" = " + Negocio.P_Anio.Trim());
                        }
                    }

                    if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID))
                    {
                        if (Mi_SQL.ToString().Trim().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" AND ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" WHERE ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID);
                            Mi_SQL.Append(" = '" + Negocio.P_Fte_Financiamiento_ID.Trim() + "'");
                        }
                    }

                    Mi_SQL.Append(" GROUP BY ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing + "." + Cat_Psp_Concepto_Ing.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing + "." + Cat_Psp_SubConcepto_Ing.Campo_Descripcion + ", ");
                    Mi_SQL.Append("ING." + Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + ", ");
                    Mi_SQL.Append("ING." + Negocio.P_Importe_Mes + ", ");
                    Mi_SQL.Append("ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + ", ");
                    Mi_SQL.Append("ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append("ING." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado);
                    Mi_SQL.Append(" ORDER BY CONCEPTO ASC");

                    Ds_Pronostico = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds_Pronostico != null)
                    {
                        Dt_Pronostico = Ds_Pronostico.Tables[0];
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros del pronostico de ingresos. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Pronostico;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Estado
            ///DESCRIPCIÓN          : Obtiene datos de los años del estado analitico de ingresos
            ///PARAMETROS           : 
            ///CREO                 : Ramón Baeza Yépez
            ///FECHA_CREO           : 11/Junio/2013
            ///*******************************************************************************
            internal static DataTable Consultar_Estado(Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder(); //Para fomar el query que contendra la consulta
                DataSet Ds_Datos = new DataSet(); //Dataset donde obtendremos los datos de la consulta
                DataTable Dt_Datos = new DataTable();
                DataRow Nueva_Linea;
                DataTable Dt_Ing = new DataTable();
                bool Repetido = false;

                try
                {
                    Mi_SQL.Append("Select Convert(Char(2), (Select " + Cat_Psp_Rubro.Campo_Clave + " From " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                    Mi_SQL.Append(" Where " + Cat_Psp_Rubro.Campo_Rubro_ID + " = ");
                    Mi_SQL.Append("Pres." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + "),1) As Clave_Rubro");
                    Mi_SQL.Append(", (Select " + Cat_Psp_Rubro.Campo_Descripcion + " From " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                    Mi_SQL.Append(" Where " + Cat_Psp_Rubro.Campo_Rubro_ID + " = ");
                    Mi_SQL.Append("Pres." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ") As Rubro");
                    Mi_SQL.Append(",Convert(Char(2), (Select " + Cat_Psp_Tipo.Campo_Clave + " From " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                    Mi_SQL.Append(" Where " + Cat_Psp_Tipo.Campo_Tipo_ID + " = ");
                    Mi_SQL.Append("Pres." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + "),1) As Clave_Tipo");
                    Mi_SQL.Append(", (Select " + Cat_Psp_Tipo.Campo_Descripcion + " From " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                    Mi_SQL.Append(" Where " + Cat_Psp_Tipo.Campo_Tipo_ID + " = ");
                    Mi_SQL.Append("Pres." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ") As Tipo");
                    Mi_SQL.Append(", Sum(Pres." + Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ") As lEY_INGRESOS_ESTIMADA");
                    Mi_SQL.Append(", Sum(Pres." + Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ") As MODIFICADO_A");
                    Mi_SQL.Append(", Sum(Pres." + Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ") As DEVENGADO");
                    Mi_SQL.Append(", Sum(Pres." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") As RECAUDADO_B");
                    Mi_SQL.Append(", (Sum(Pres." + Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ") / Sum(Pres.");
                    Mi_SQL.Append(Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + "))As AVANCE_DE_RECAUDACION_B_A");
                    Mi_SQL.Append(" From " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos + " As Pres");
                    Mi_SQL.Append(" Where Pres." + Ope_Psp_Presupuesto_Ingresos.Campo_Anio + " = " + Datos.P_Anio);
                    Mi_SQL.Append(" Group by Pres." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                    Mi_SQL.Append("Pres." + Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID);
                    Mi_SQL.Append(" Order By Pres." + Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID);

                    Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    if (Ds_Datos != null)
                    {
                        Dt_Datos = Ds_Datos.Tables[0].Copy();
                        Dt_Ing = Ds_Datos.Tables[0].Copy();
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("SELECT Convert(CHAR (2),(Rub." + Cat_Psp_Rubro.Campo_Clave + "), 1) AS Clave_Rubro");
                        Mi_SQL.Append(",Rub." + Cat_Psp_Rubro.Campo_Descripcion + " AS Rubro");
                        Mi_SQL.Append(",Convert(CHAR (2),(Tip." + Cat_Psp_Tipo.Campo_Clave + "), 1) AS Clave_Tipo");
                        Mi_SQL.Append(",Tip." + Cat_Psp_Tipo.Campo_Descripcion + " AS Tipo ");
                        Mi_SQL.Append(" From " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + " As Tip ");
                        Mi_SQL.Append(" RIGHT OUTER JOIN  " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + " As Rub ");
                        Mi_SQL.Append(" on Rub." + Cat_Psp_Rubro.Campo_Rubro_ID + " = Tip." + Cat_Psp_Tipo.Campo_Rubro_ID);
                        Mi_SQL.Append(" Where Rub." + Cat_Psp_Rubro.Campo_Anio + " = " + Datos.P_Anio);
                        Mi_SQL.Append(" ORDER BY Tip." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                        if (Ds_Datos != null)
                        {
                            foreach (DataRow Tipo in Ds_Datos.Tables[0].Rows)
                            {
                                for (int Cont_Rubro = 0; Cont_Rubro < Dt_Datos.Rows.Count; Cont_Rubro++)
                                {
                                    if (Convert.ToInt16(Tipo["Clave_Tipo"]) < Convert.ToInt16(Dt_Datos.Rows[Cont_Rubro]["Clave_Tipo"]))
                                    {
                                        foreach (DataRow Dr in Dt_Ing.Rows)
                                        {
                                            if (Convert.ToInt16(Tipo["Clave_Tipo"]) == Convert.ToInt16(Dr["Clave_Tipo"])
                                            && Convert.ToInt16(Tipo["Clave_Rubro"]) == Convert.ToInt16(Dr["Clave_Rubro"]))
                                            {
                                                Repetido = true;
                                                break;
                                            }
                                        }

                                        if (!Repetido)
                                        {

                                            Nueva_Linea = Dt_Datos.NewRow();
                                            Nueva_Linea["Clave_Rubro"] = Tipo["Clave_Rubro"].ToString();
                                            Nueva_Linea["Rubro"] = Tipo["Rubro"].ToString();
                                            Nueva_Linea["Clave_Tipo"] = Tipo["Clave_Tipo"].ToString();
                                            Nueva_Linea["Tipo"] = Tipo["Tipo"].ToString();
                                            Dt_Datos.Rows.InsertAt(Nueva_Linea, Cont_Rubro);
                                        }
                                        Repetido = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    String Mensaje = "Error al intentar consultar los registros de los años. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                    throw new Exception(Mensaje);
                }
                return Dt_Datos;
            }
        #endregion
    }
}
