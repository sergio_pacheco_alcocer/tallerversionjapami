﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Autorizar_Traspaso_Presupuestal.Negocio;
using JAPAMI.Sessiones;

namespace JAPAMI.Movimiento_Presupuestal.Datos
{
    public class Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Datos
    {
        #region(Metodods)
            /// ********************************************************************************************************************
            /// NOMBRE: Alta_Autorizacion_Traspaso
            /// 
            /// COMENTARIOS: Esta operación inserta un nuevo registro de un movimiento presupuestal en la tabla de 
            /// 
            /// PARÁMETROS: Datos.- Valor de los campos a insertar en la tabla de 
            /// 
            /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
            /// FECHA CREÓ: 22/Octubre/2011 
            /// USUARIO MODIFICO:
            /// FECHA MODIFICO:
            /// CAUSA DE LA MODIFICACIÓN:
            /// ********************************************************************************************************************
            public static Boolean Alta_Autorizacion_Traspaso(Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Negocio Datos)
            {

                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
                SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
                SqlConnection Conexion;//Variable para la conexión para la base de datos   
                SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
                Object No_Solicitud_Max;//Identificador el elemento de la busque (cual es el mayor id de solicitud)
                String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
                Boolean Operacion_Completa = false;

                Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Comando = new SqlCommand();
                Conexion.Open();
                Transaccion = Conexion.BeginTransaction();
                Comando.Transaction = Transaccion;
                Comando.Connection = Conexion;

                try
                {

                    Mi_SQL.Append("SELECT ISNULL(MAX (" + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + "),0) ");
                    Mi_SQL.Append("FROM " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf);
                    //realizar consulta
                    No_Solicitud_Max = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    
                    
                    if (Convert.IsDBNull(No_Solicitud_Max) == false)
                    {

                        Datos.P_Numero_Solicitud = String.Format("{0:00000}", Convert.ToInt32(No_Solicitud_Max) + 1);
                    }
                    else
                    {
                        //si no tiene registro se le asigna 1
                        Datos.P_Numero_Solicitud = "00001";
                    }
                    Mi_SQL = new StringBuilder();

                    //cadena para insertar el la base de datos
                    Mi_SQL.Append("INSERT INTO " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + " (");
                    Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + ", ");
                    Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Codigo1 + ", ");
                    Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Codigo2 + ", ");
                    Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Importe + ", ");
                    Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Justificacion + ", ");
                    Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + ", ");
                    Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Fecha_Creo + ") ");
                    Mi_SQL.Append("VALUES (" + Datos.P_Numero_Solicitud + ", ");
                    Mi_SQL.Append("'" + Datos.P_Codigo_Programatico_Origen + "', ");
                    Mi_SQL.Append("'" + Datos.P_Codigo_Programatico_Destino + "', ");
                    Mi_SQL.Append(Datos.P_Importe + ", ");
                    Mi_SQL.Append("'" + Datos.P_Justificacion + "', ");
                    Mi_SQL.Append("'" + Datos.P_Estatus + "',");
                    Mi_SQL.Append("'" + Datos.P_Usuario_Creo + "',");
                    Mi_SQL.Append("SYSDATE)");

                    //Ejecutar consulta
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    Conexion.Close();
                    Operacion_Completa = true;

                    //Ejecutar consulta
                    //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    Conexion.Close();
                    Operacion_Completa = true;
                    
                }
                catch (SqlException Ex)
                {
                    Transaccion.Rollback();
                    Mensaje = "Error:  [" + Ex.Message + "]";
                    throw new Exception(Mensaje, Ex);
                }
                finally
                {
                    Conexion.Close();
                    Comando = null;
                    Conexion = null;
                    Transaccion = null;
                }
                return Operacion_Completa;
            }
            /// ********************************************************************************************************************
            /// NOMBRE: Modificar_Comentario
            /// 
            /// COMENTARIOS: Esta operación inserta un nuevo registro de un comentario presupuestal en la tabla de Ope_Psp_Cierre_Presup
            /// 
            /// PARÁMETROS: Datos.- Valor de los campos a insertar en la tabla de 
            /// 
            /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
            /// FECHA CREÓ: 17/Noviembre/2011 
            /// USUARIO MODIFICO:
            /// FECHA MODIFICO:
            /// CAUSA DE LA MODIFICACIÓN:
            /// ********************************************************************************************************************
            public static Boolean Modificar_Comentario(Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Negocio Datos)
            {

                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
                SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
                SqlConnection Conexion;//Variable para la conexión para la base de datos   
                SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
                Object No_Solicitud_Max;//Identificador el elemento de la busque (cual es el mayor id de solicitud)
                String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
                Boolean Operacion_Completa = false;

                Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Comando = new SqlCommand();
                Conexion.Open();
                Transaccion = Conexion.BeginTransaction();
                Comando.Transaction = Transaccion;
                Comando.Connection = Conexion;

                try
                {
                    Mi_SQL.Append("UPDATE " + Ope_Psp_Comentarios_Mov.Tabla_Ope_Psp_Comentarios_Mov  +" SET ");
                    Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Comentario +"='" +Datos.P_Comentario + "', ");
                    Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Fecha +"=SYSDATE" + ", ");
                    Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Usuario_Modifico +"='" +Datos.P_Usuario_Creo  + "', ");
                    Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Fecha_Modifico + "=SYSDATE" + " ");
                    Mi_SQL.Append("WHERE " + Ope_Psp_Comentarios_Mov.Campo_Numero_Solicitud + " = " + Datos.P_Numero_Solicitud);
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    Conexion.Close();
                    Operacion_Completa = true;
                }
                catch (SqlException Ex)
                {
                   Transaccion.Rollback();
                   Mensaje = "Error:  [" + Ex.Message + "]";
                   throw new Exception(Mensaje, Ex);
                }
                finally
                {
                    Conexion.Close();
                    Comando = null;
                    Conexion = null;
                    Transaccion = null;
                }
                return Operacion_Completa;
            }
            /// ********************************************************************************************************************
            /// NOMBRE: Modificar_Autorizacion_Traspaso
            /// 
            /// COMENTARIOS: Esta operación actualiza un registro del movimiento en la tabla de 
            /// 
            /// PARÁMETROS: Datos.- Valor de los campos a Modificar en la tabla de  
            /// 
            /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
            /// FECHA CREÓ:  22/Octubre/2011 
            /// USUARIO MODIFICO:
            /// FECHA MODIFICO:
            /// CAUSA DE LA MODIFICACIÓN:
            /// ********************************************************************************************************************
            public static Boolean Modificar_Autorizacion_Traspaso(Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
                SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
                SqlConnection Conexion;//Variable para la conexión para la base de datos   
                SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
                String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
                Boolean Operacion_Completa = false;
                DataTable Dt_Movimientos = new DataTable();
                String Operacion = String.Empty;
                DataTable Dt_Importes = new DataTable();
                String Anio = string.Format("{0:yyyy}", DateTime.Now);

                Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Comando = new SqlCommand();
                Conexion.Open();
                Transaccion = Conexion.BeginTransaction();
                Comando.Transaction = Transaccion;
                Comando.Connection = Conexion;
                try
                {
                    Mi_SQL.Append("UPDATE " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + " SET ");
                    Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + "='" + Datos.P_Estatus + "', ");
                    Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                    Mi_SQL.Append(Cat_Ope_Com_Solicitud_Transf.Campo_Fecha_Modifico + "=SYSDATE ");
                    Mi_SQL.Append("WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud+ " = " + Datos.P_Numero_Solicitud);

                    Comando.CommandText = Mi_SQL.ToString();
                    Comando.ExecuteNonQuery();

                    if (!String.IsNullOrEmpty(Datos.P_Comentario)) 
                    {
                        Mi_SQL = new StringBuilder();
                        Mi_SQL.Append("INSERT INTO " + Ope_Psp_Comentarios_Mov.Tabla_Ope_Psp_Comentarios_Mov + "(");
                        Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Comentario + ", ");
                        Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Numero_Solicitud + ", ");
                        Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Fecha + ", ");
                        Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Usuario_Creo + ", ");
                        Mi_SQL.Append(Ope_Psp_Comentarios_Mov.Campo_Fecha_Creo + ") VALUES (");
                        Mi_SQL.Append("'"+Datos.P_Comentario +"', ");
                        Mi_SQL.Append("'" + Datos.P_Numero_Solicitud + "', ");
                        Mi_SQL.Append("SYSDATE, ");
                        Mi_SQL.Append("'" + Cls_Sessiones.Nombre_Empleado + "', ");
                        Mi_SQL.Append("SYSDATE)");
                        
                        Comando.CommandText = Mi_SQL.ToString();
                        Comando.ExecuteNonQuery();
                    }


                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT * FROM " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf);
                    Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + " = " + Datos.P_Numero_Solicitud);

                    Dt_Movimientos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                    if (Dt_Movimientos != null)
                    {
                        if (Dt_Movimientos.Rows.Count > 0)
                        {
                            Mi_SQL = new StringBuilder();
                            Mi_SQL.Append("SELECT * FROM " + Ope_Com_Solicitud_Transf_Det.Tabla_Ope_Psp_Hist_Calendar_Presu);
                            Mi_SQL.Append(" WHERE " + Ope_Com_Solicitud_Transf_Det.Campo_No_Solicitud + " = " + Datos.P_Numero_Solicitud);

                            Dt_Importes = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                            Operacion = Dt_Movimientos.Rows[0]["TIPO_OPERACION"].ToString().Trim();

                            if (Operacion.Equals("TRASPASO"))
                            {
                                if (Dt_Importes != null)
                                {
                                    if (Dt_Importes.Rows.Count > 0)
                                    {
                                        if (Datos.P_Estatus.Trim().Equals("AUTORIZADA"))
                                        {

                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                            Mi_SQL.Append(" SET DISPONIBLE = DISPONIBLE + " + Dt_Movimientos.Rows[0]["IMPORTE"] + ", ");
                                            Mi_SQL.Append(" AMPLIACION = AMPLIACION + " + Dt_Movimientos.Rows[0]["IMPORTE"] + ", ");
                                            foreach (DataRow Dr in Dt_Importes.Rows)
                                            {
                                                if (Dr["TIPO"].ToString().Trim().Equals("Partida_Destino") || Dr["TIPO"].ToString().Trim().Equals("Partida_Destino_Nueva"))
                                                {
                                                    Mi_SQL.Append(" IMPORTE_ENERO = IMPORTE_ENERO  + " + Dr["IMPORTE_ENERO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_FEBRERO = IMPORTE_FEBRERO + " + Dr["IMPORTE_FEBRERO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_MARZO = IMPORTE_MARZO  + " + Dr["IMPORTE_MARZO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_ABRIL = IMPORTE_ABRIL  + " + Dr["IMPORTE_ABRIL"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_MAYO = IMPORTE_MAYO  + " + Dr["IMPORTE_MAYO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_JUNIO = IMPORTE_JUNIO  + " + Dr["IMPORTE_JUNIO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_JULIO = IMPORTE_JULIO  + " + Dr["IMPORTE_JULIO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_AGOSTO = IMPORTE_AGOSTO  + " + Dr["IMPORTE_AGOSTO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_SEPTIEMBRE = IMPORTE_SEPTIEMBRE  + " + Dr["IMPORTE_SEPTIEMBRE"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_OCTUBRE = IMPORTE_OCTUBRE  + " + Dr["IMPORTE_OCTUBRE"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_NOVIEMBRE = IMPORTE_NOVIEMBRE  + " + Dr["IMPORTE_NOVIEMBRE"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_DICIEMBRE = IMPORTE_DICIEMBRE  + " + Dr["IMPORTE_DICIEMBRE"] + ", ");
                                                }
                                            }
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + "=SYSDATE ");
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dt_Movimientos.Rows[0]["DESTINO_DEPENDENCIA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dt_Movimientos.Rows[0]["DESTINO_FTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dt_Movimientos.Rows[0]["DESTINO_PROGRAMA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dt_Movimientos.Rows[0]["DESTINO_PARTIDA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                            Comando.CommandText = Mi_SQL.ToString();
                                            Comando.ExecuteNonQuery();


                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                            Mi_SQL.Append(" SET MODIFICADO = APROBADO + AMPLIACION - REDUCCION ");
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dt_Movimientos.Rows[0]["DESTINO_DEPENDENCIA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dt_Movimientos.Rows[0]["DESTINO_FTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dt_Movimientos.Rows[0]["DESTINO_PROGRAMA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dt_Movimientos.Rows[0]["DESTINO_PARTIDA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                            Comando.CommandText = Mi_SQL.ToString();
                                            Comando.ExecuteNonQuery();

                                            //modificamos la partida origen
                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                            Mi_SQL.Append(" SET DISPONIBLE = DISPONIBLE - " + Dt_Movimientos.Rows[0]["IMPORTE"] + ", ");
                                            Mi_SQL.Append(" REDUCCION = REDUCCION + " + Dt_Movimientos.Rows[0]["IMPORTE"] + ", ");

                                            foreach (DataRow Dr in Dt_Importes.Rows)
                                            {
                                                if (Dr["TIPO"].ToString().Trim().Equals("Partida_Origen") || Dr["TIPO"].ToString().Trim().Equals("Partida_Origen_Nueva"))
                                                {
                                                    Mi_SQL.Append(" IMPORTE_ENERO = IMPORTE_ENERO  - " + Dr["IMPORTE_ENERO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_FEBRERO = IMPORTE_FEBRERO - " + Dr["IMPORTE_FEBRERO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_MARZO = IMPORTE_MARZO  - " + Dr["IMPORTE_MARZO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_ABRIL = IMPORTE_ABRIL  - " + Dr["IMPORTE_ABRIL"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_MAYO = IMPORTE_MAYO  - " + Dr["IMPORTE_MAYO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_JUNIO = IMPORTE_JUNIO  - " + Dr["IMPORTE_JUNIO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_JULIO = IMPORTE_JULIO  - " + Dr["IMPORTE_JULIO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_AGOSTO = IMPORTE_AGOSTO  - " + Dr["IMPORTE_AGOSTO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_SEPTIEMBRE = IMPORTE_SEPTIEMBRE  - " + Dr["IMPORTE_SEPTIEMBRE"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_OCTUBRE = IMPORTE_OCTUBRE  - " + Dr["IMPORTE_OCTUBRE"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_NOVIEMBRE = IMPORTE_NOVIEMBRE  - " + Dr["IMPORTE_NOVIEMBRE"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_DICIEMBRE = IMPORTE_DICIEMBRE  - " + Dr["IMPORTE_DICIEMBRE"] + ", ");
                                                }
                                            }
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + "='" + Datos.P_Usuario_Creo + "', ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + "=SYSDATE ");
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_DEPENDENCIA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_FTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_PROGRAMA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_PARTIDA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                            Comando.CommandText = Mi_SQL.ToString();
                                            Comando.ExecuteNonQuery();


                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                            Mi_SQL.Append(" SET MODIFICADO = APROBADO + AMPLIACION - REDUCCION ");
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_DEPENDENCIA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_FTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_PROGRAMA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_PARTIDA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                            Comando.CommandText = Mi_SQL.ToString();
                                            Comando.ExecuteNonQuery();
                                        }
                                        else if (Datos.P_Estatus.Trim().Equals("RECHAZADA"))
                                        {
                                            foreach (DataRow Dr in Dt_Importes.Rows)
                                            {
                                                if (Dr["TIPO"].ToString().Trim().Equals("Partida_Destino_Nueva"))
                                                {
                                                    Mi_SQL = new StringBuilder();
                                                    Mi_SQL.Append("DELETE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dt_Movimientos.Rows[0]["DESTINO_DEPENDENCIA_ID"].ToString().Trim() + "'");
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dt_Movimientos.Rows[0]["DESTINO_FTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dt_Movimientos.Rows[0]["DESTINO_PROGRAMA_ID"].ToString().Trim() + "'");
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dt_Movimientos.Rows[0]["DESTINO_PARTIDA_ID"].ToString().Trim() + "'");
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                                    Comando.CommandText = Mi_SQL.ToString();
                                                    Comando.ExecuteNonQuery();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (Operacion.Equals("REDUCIR"))
                            {
                                if (Dt_Importes != null)
                                {
                                    if (Dt_Importes.Rows.Count > 0)
                                    {
                                        if (Datos.P_Estatus.Trim().Equals("AUTORIZADA"))
                                        {
                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                            Mi_SQL.Append(" SET DISPONIBLE = DISPONIBLE - " + Dt_Movimientos.Rows[0]["IMPORTE"] + ", ");
                                            Mi_SQL.Append(" REDUCCION = REDUCCION + " + Dt_Movimientos.Rows[0]["IMPORTE"] + ", ");

                                            foreach (DataRow Dr in Dt_Importes.Rows)
                                            {
                                                if (Dr["TIPO"].ToString().Trim().Equals("Partida_Origen") || Dr["TIPO"].ToString().Trim().Equals("Partida_Origen_Nueva"))
                                                {
                                                    Mi_SQL.Append(" IMPORTE_ENERO = IMPORTE_ENERO" + " - " + Dr["IMPORTE_ENERO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_FEBRERO = IMPORTE_FEBRERO" + " - " + Dr["IMPORTE_FEBRERO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_MARZO = IMPORTE_MARZO" + " - " + Dr["IMPORTE_MARZO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_ABRIL = IMPORTE_ABRIL" + " - " + Dr["IMPORTE_ABRIL"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_MAYO = IMPORTE_MAYO" + " - " + Dr["IMPORTE_MAYO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_JUNIO = IMPORTE_JUNIO" + " - " + Dr["IMPORTE_JUNIO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_JULIO = IMPORTE_JULIO" + " - " + Dr["IMPORTE_JULIO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_AGOSTO = IMPORTE_AGOSTO" + " - " + Dr["IMPORTE_AGOSTO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_SEPTIEMBRE = IMPORTE_SEPTIEMBRE" + " - " + Dr["IMPORTE_SEPTIEMBRE"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_OCTUBRE = IMPORTE_OCTUBRE" + " - " + Dr["IMPORTE_OCTUBRE"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_NOVIEMBRE = IMPORTE_NOVIEMBRE" + " - " + Dr["IMPORTE_NOVIEMBRE"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_DICIEMBRE = IMPORTE_DICIEMBRE" + " - " + Dr["IMPORTE_DICIEMBRE"] + ", ");
                                                }
                                            }

                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = SYSDATE ");
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_DEPENDENCIA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_FTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_PROGRAMA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_PARTIDA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                            Comando.CommandText = Mi_SQL.ToString();
                                            Comando.ExecuteNonQuery();

                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                            Mi_SQL.Append(" SET  MODIFICADO = APROBADO + AMPLIACION - REDUCCION ");
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_DEPENDENCIA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_FTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_PROGRAMA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_PARTIDA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                            Comando.CommandText = Mi_SQL.ToString();
                                            Comando.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }
                            else if (Operacion.Equals("AMPLIAR"))
                            {
                                if (Dt_Importes != null)
                                {
                                    if (Dt_Importes.Rows.Count > 0)
                                    {
                                        if (Datos.P_Estatus.Trim().Equals("AUTORIZADA"))
                                        {
                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                            Mi_SQL.Append(" SET DISPONIBLE = DISPONIBLE + " + Dt_Movimientos.Rows[0]["IMPORTE"] + ", ");
                                            Mi_SQL.Append(" AMPLIACION = AMPLIACION + " + Dt_Movimientos.Rows[0]["IMPORTE"] + ", ");
                                            Mi_SQL.Append(" MODIFICADO = APROBADO + AMPLIACION - REDUCCION, ");

                                            foreach (DataRow Dr in Dt_Importes.Rows)
                                            {
                                                if (Dr["TIPO"].ToString().Trim().Equals("Partida_Origen") || Dr["TIPO"].ToString().Trim().Equals("Partida_Origen_Nueva"))
                                                {
                                                    Mi_SQL.Append(" IMPORTE_ENERO = IMPORTE_ENERO" + " + " + Dr["IMPORTE_ENERO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_FEBRERO = IMPORTE_FEBRERO" + " + " + Dr["IMPORTE_FEBRERO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_MARZO = IMPORTE_MARZO" + " + " + Dr["IMPORTE_MARZO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_ABRIL = IMPORTE_ABRIL" + " + " + Dr["IMPORTE_ABRIL"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_MAYO = IMPORTE_MAYO" + " + " + Dr["IMPORTE_MAYO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_JUNIO = IMPORTE_JUNIO" + " + " + Dr["IMPORTE_JUNIO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_JULIO = IMPORTE_JULIO" + " + " + Dr["IMPORTE_JULIO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_AGOSTO = IMPORTE_AGOSTO" + " + " + Dr["IMPORTE_AGOSTO"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_SEPTIEMBRE = IMPORTE_SEPTIEMBRE" + " + " + Dr["IMPORTE_SEPTIEMBRE"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_OCTUBRE = IMPORTE_OCTUBRE" + " + " + Dr["IMPORTE_OCTUBRE"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_NOVIEMBRE = IMPORTE_NOVIEMBRE" + " + " + Dr["IMPORTE_NOVIEMBRE"] + ", ");
                                                    Mi_SQL.Append(" IMPORTE_DICIEMBRE = IMPORTE_DICIEMBRE" + " + " + Dr["IMPORTE_DICIEMBRE"] + ", ");
                                                }
                                            }
                                            
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Datos.P_Usuario_Creo + "', ");
                                            Mi_SQL.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = SYSDATE ");
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_DEPENDENCIA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_FTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_PROGRAMA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_PARTIDA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                            Comando.CommandText = Mi_SQL.ToString();
                                            Comando.ExecuteNonQuery();

                                            Mi_SQL = new StringBuilder();
                                            Mi_SQL.Append("UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                            Mi_SQL.Append(" SET  MODIFICADO = APROBADO + AMPLIACION - REDUCCION ");
                                            Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_DEPENDENCIA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_FTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_PROGRAMA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_PARTIDA_ID"].ToString().Trim() + "'");
                                            Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                            Comando.CommandText = Mi_SQL.ToString();
                                            Comando.ExecuteNonQuery();
                                        }
                                        else if (Datos.P_Estatus.Trim().Equals("RECHAZADA"))
                                        {
                                            foreach (DataRow Dr in Dt_Importes.Rows)
                                            {
                                                if (Dr["TIPO"].ToString().Trim().Equals("Partida_Origen_Nueva"))
                                                {
                                                    Mi_SQL = new StringBuilder();
                                                    Mi_SQL.Append("DELETE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                                                    Mi_SQL.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_DEPENDENCIA_ID"].ToString().Trim() + "'");
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_FTE_FINANCIAMIENTO_ID"].ToString().Trim() + "'");
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_PROGRAMA_ID"].ToString().Trim() + "'");
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Dt_Movimientos.Rows[0]["ORIGEN_PARTIDA_ID"].ToString().Trim() + "'");
                                                    Mi_SQL.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = " + Anio);

                                                    Comando.CommandText = Mi_SQL.ToString();
                                                    Comando.ExecuteNonQuery();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    Transaccion.Commit();
                    Conexion.Close();
                    Operacion_Completa = true;
                }
                catch (SqlException Ex)
                {
                    Transaccion.Rollback();
                    Mensaje = "Error:  [" + Ex.Message + "]";
                    throw new Exception(Mensaje, Ex);
                }
                finally
                {
                    Conexion.Close();
                    Comando = null;
                    Conexion = null;
                    Transaccion = null;
                }
                return Operacion_Completa;
            }
            /// ********************************************************************************************************************
            /// NOMBRE: Eliminar_Autorizacion_Traspaso
            /// 
            /// COMENTARIOS: Esta operación eliminara un registro del movimiento que se haya realizado en la tabla de 
            /// 
            /// PARÁMETROS: Datos.- Valor de los campos a insertar en la tabla de 
            /// 
            /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
            /// FECHA CREÓ:  22/Octubre/2011 
            /// USUARIO MODIFICO:
            /// FECHA MODIFICO:
            /// CAUSA DE LA MODIFICACIÓN:
            /// ********************************************************************************************************************
            public static Boolean Eliminar_Autorizacion_Traspaso(Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
                SqlTransaction Transaccion = null;////Sirve para guardar la transacción en memoria hasta que se ejecute completo el proceso        
                SqlConnection Conexion;//Variable para la conexión para la base de datos   
                SqlCommand Comando;//Sirve para la ejecución de las operaciones a la base de datos
                String Mensaje = String.Empty; //Variable que almacena el mensaje de estado de la operación
                Boolean Operacion_Completa = false;

                Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Comando = new SqlCommand();
                Conexion.Open();
                Transaccion = Conexion.BeginTransaction();
                Comando.Transaction = Transaccion;
                Comando.Connection = Conexion;

                try
                {
                    Mi_SQL.Append("Delete From " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + " ");
                    Mi_SQL.Append("where " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud+ "=" + Datos.P_Numero_Solicitud);
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }
                catch (SqlException Ex)
                {
                    Transaccion.Rollback();
                    Mensaje = "Error:  [" + Ex.Message + "]";
                    throw new Exception(Mensaje, Ex);
                }
                finally
                {
                    Conexion.Close();
                    Comando = null;
                    Conexion = null;
                    Transaccion = null;
                }
                return Operacion_Completa;
            }
            /// ********************************************************************************************************************
            /// NOMBRE: Consulta_Autorizacion_Traspaso
            /// 
            /// COMENTARIOS: Consulta el movimiento presupuestal que se haya llevado en la tabla   
            /// 
            /// PARÁMETROS: Datos.- Valor de los campos a insertar en la tabla de 
            /// 
            /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
            /// FECHA CREÓ:  24/Octubre/2011 
            /// USUARIO MODIFICO:
            /// FECHA MODIFICO:
            /// CAUSA DE LA MODIFICACIÓN:
            /// ********************************************************************************************************************
            public static DataTable Consulta_Autorizacion_Traspaso(Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
                DataTable Dt_Movimiento = new DataTable();
                try
                {
                    Mi_SQL.Append("Select " +Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf +".* ");
                    Mi_SQL.Append("From " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + " ");

                    if (!string.IsNullOrEmpty(Datos.P_Numero_Solicitud))
                    {
                        if (Mi_SQL.ToString().Contains("WHERE"))
                        {
                            //no llevan comilla simple es entero el numero de solicitud
                            Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + "=" + Datos.P_Numero_Solicitud + "");
                        }
                        else
                        {
                            Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud + "=" + Datos.P_Numero_Solicitud + "");
                        }
                    }
                    if (!string.IsNullOrEmpty(Datos.P_Importe))
                    {
                        if (Mi_SQL.ToString().Contains("WHERE"))
                        {
                            //no llevan comilla simple es entero el numero de solicitud
                            Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Campo_Importe + "=" + Datos.P_Importe + "");
                        }
                        else
                        {
                            Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Importe + "=" + Datos.P_Importe + "");
                        }
                    }
                    
                    if (!string.IsNullOrEmpty(Datos.P_Estatus))
                    {
                        if (Mi_SQL.ToString().Contains("WHERE"))
                        {
                            Mi_SQL.Append(" AND " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + "='" + Datos.P_Estatus + "'");
                        }
                        else
                        {
                            Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Campo_Estatus + "='" + Datos.P_Estatus + "'");
                        }
                    }
                   Mi_SQL.Append( " Order by " +Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud  +" asc");

                Dt_Movimiento = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
               
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
                }
                return Dt_Movimiento;
            }
            /// ********************************************************************************************************************
            /// NOMBRE: Consulta_Datos_Partidas
            /// 
            /// COMENTARIOS: Consulta  la partida especifica
            /// 
            /// PARÁMETROS: Datos.- Valor de los campos a insertar en la tabla de 
            /// 
            /// USUARIO CREÓ: Hugo Enrique Ramirez Aguilera
            /// FECHA CREÓ:  17/noviembre/2011 
            /// USUARIO MODIFICO:
            /// FECHA MODIFICO:
            /// CAUSA DE LA MODIFICACIÓN:
            /// ********************************************************************************************************************
            public static DataTable Consulta_Datos_Partidas(Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
                DataTable Dt_Movimiento = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT * ");
                    Mi_SQL.Append("From " + Cat_Com_Partidas.Tabla_Cat_Com_Partidas +" ");
                    Mi_SQL.Append("where " + Cat_Com_Partidas.Campo_Clave +"='"  +Datos.P_Partida +"'");
                    Dt_Movimiento = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];
               
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
                }
                return Dt_Movimiento;
            }


            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consulta_Autorizacion_Movimientos
            ///DESCRIPCIÓN          : consulta para obtener los datos de los movimientos
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 12/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static DataTable Consulta_Autorizacion_Movimientos(Cls_Ope_Psp_Autorizar_Solicitud_Traspaso_Presupuestal_Negocio Datos)
            {
                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenara la consulta.
                DataTable Dt_Movimiento = new DataTable();
                try
                {
                    Mi_SQL.Append("SELECT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' ' + ");
                    Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre + " AS DEPENDENCIA_ORIGEN, ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS FTE_FINANCIAMIENTO_ORIGEN, ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre + " AS PROGRAMA_ORIGEN, ");
                    Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + ");
                    Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre + " AS PARTIDA_ORIGEN, ");
                    Mi_SQL.Append("(SELECT " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Clave + " + ' ' + ");
                    Mi_SQL.Append(Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Nombre);
                    Mi_SQL.Append(" FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Dependencia_Id);
                    Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID + ") AS DEPENDENCIA_DESTINO,");
                    Mi_SQL.Append("(SELECT " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' ' + ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion);
                    Mi_SQL.Append(" FROM " +Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_SQL.Append(" WHERE  " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Fuente_Financiamiento_Id);
                    Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ") AS FTE_FINANCIAMIENTO_DESTINO, ");
                    Mi_SQL.Append("(SELECT " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' ' + ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Nombre );
                    Mi_SQL.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Programa_Id);
                    Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ") AS PROGRAMA_DESTINO, ");
                    Mi_SQL.Append("(SELECT " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Clave + " + ' ' + ");
                    Mi_SQL.Append(Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Nombre );
                    Mi_SQL.Append(" FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Destino_Partida_Id);
                    Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID + ") AS PARTIDA_DESTINO ");
                    Mi_SQL.Append(" FROM " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf);
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Dependencias.Tabla_Cat_Dependencias);
                    Mi_SQL.Append(" ON " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Dependencia_Id);
                    Mi_SQL.Append(" = " + Cat_Dependencias.Tabla_Cat_Dependencias + "." + Cat_Dependencias.Campo_Dependencia_ID );
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_SQL.Append(" ON " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Fuente_Financiamiento_Id);
                    Mi_SQL.Append(" = " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "." + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID );
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas );
                    Mi_SQL.Append(" ON " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Programa_Id);
                    Mi_SQL.Append(" = " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "." + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id );
                    Mi_SQL.Append(" LEFT OUTER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                    Mi_SQL.Append(" ON " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_Origen_Partida_Id);
                    Mi_SQL.Append(" = " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                    Mi_SQL.Append(" WHERE " + Cat_Ope_Com_Solicitud_Transf.Tabla_Cat_Ope_Com_Solicitud_Transf + "." + Cat_Ope_Com_Solicitud_Transf.Campo_No_Solicitud );
                    Mi_SQL.Append(" = " + Datos.P_Numero_Solicitud);

                    Dt_Movimiento = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString()).Tables[0];

                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al consultar los Movimientos Presupuestales que existen actualmente en el sistema. Error: [" + Ex.Message + "]");
                }
                return Dt_Movimiento;
            }
        #endregion

       
    }
}