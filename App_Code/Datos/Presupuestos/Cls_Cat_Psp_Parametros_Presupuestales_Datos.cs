﻿using System;
using System.Data;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Constantes;
using JAPAMI.Parametros_Presupuestales.Negocio;


namespace JAPAMI.Parametros_Presupuestales.Datos
{
    public class Cls_Cat_Psp_Parametros_Presupuestales_Datos
    {
        #region (Metodos)
            //*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Parametros_Presupuestales
            ///DESCRIPCIÓN          : consulta para obtener los datos de los parametros presupuestales
            ///PARAMETROS           :
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 27/Marzo/2013 01:47 PM
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Parametros_Presupuestales()
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                    Mi_Sql.Append("SELECT ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Fte_Financiamiento_ID_Ingresos + ", '') AS FTE_FINANCIAMIENTO_ID_INGRESOS, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Programa_ID_Ingresos + ", '') AS PROGRAMA_ID_INGRESOS, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Fte_Financiamiento_ID_Inversiones + ", '') AS FTE_FINANCIAMIENTO_ID_INVERSIONES, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Programa_ID_Inversiones + ", '') AS PROGRAMA_ID_INVERSIONES, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Aplicar_Disponible_Acumulado + ", 'NO') AS APLICAR_DISPONIBLE_ACUMULADO, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Estimado + ", '') AS CTA_ORDEN_ING_ESTIMADO, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Ampliacion + ", '') AS CTA_ORDEN_ING_AMPLIACION, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Reduccion + ", '') AS CTA_ORDEN_ING_REDUCCION, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Modificado + ", '') AS CTA_ORDEN_ING_MODIFICADO, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Por_Recaudar + ", '') AS CTA_ORDEN_ING_POR_RECAUDAR, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Devengado + ", '') AS CTA_ORDEN_ING_DEVENGADO, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Recaudado + ", '') AS CTA_ORDEN_ING_RECAUDADO, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Aprobado + ", '') AS CTA_ORDEN_EGR_APROBADO, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ampliacion + ", '') AS CTA_ORDEN_EGR_AMPLIACION, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Reduccion+ ", '') AS CTA_ORDEN_EGR_REDUCCION, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ampliacion_Interna + ", '') AS CTA_ORDEN_EGR_AMPLIACION_INTERNA, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Reduccion_Interna + ", '') AS CTA_ORDEN_EGR_REDUCCION_INTERNA, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Modificado + ", '') AS CTA_ORDEN_EGR_MODIFICADO, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Por_Ejercer + ", '') AS CTA_ORDEN_EGR_POR_EJERCER, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Comprometido + ", '') AS CTA_ORDEN_EGR_COMPROMETIDO, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Devengado + ", '') AS CTA_ORDEN_EGR_DEVENGADO, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ejercido + ", '') AS CTA_ORDEN_EGR_EJERCIDO, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Pagado + ", '') AS CTA_ORDEN_EGR_PAGADO, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Egr + ", '') AS TIPO_POLIZA_ID_PSP_EGR, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Ing + ", '') AS TIPO_POLIZA_ID_PSP_ING, ");
                    Mi_Sql.Append(" ISNULL(" + Cat_Psp_Parametros_Presupuestales.Campo_Clasificador_Adm_ID + ", '') AS CLASIFICADOR_ADM_ID ");

                    Mi_Sql.Append(" FROM " + Cat_Psp_Parametros_Presupuestales.Tabla_Cat_Psp_Parametro_Presupuestal);

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los datos de los patametros. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Cuentas_Contables
            ///DESCRIPCIÓN          : consulta para obtener los datos de las cuentas contables
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 27/Marzo/2013 04:30 PM
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Cuentas_Contables()
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID + ", ");
                    Mi_Sql.Append(Cat_Con_Cuentas_Contables.Campo_Cuenta + " + ' - ' + ");
                    Mi_Sql.Append(Cat_Con_Cuentas_Contables.Campo_Descripcion + " AS CUENTA ");
                    Mi_Sql.Append(" FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
                    Mi_Sql.Append(" WHERE " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " LIKE '8%'");
                    Mi_Sql.Append(" ORDER BY " + Cat_Con_Cuentas_Contables.Campo_Cuenta + " ASC ");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los datos de las cuentas contables. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Fuente_Financiamiento
            ///DESCRIPCIÓN          : consulta para obtener los datos de las fuentes de financiamiento
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 27/Marzo/2013 04:34 PM
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Fuente_Financiamiento()
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Clave + " + ' - ' + ");
                    Mi_Sql.Append(Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " AS NOMBRE ");
                    Mi_Sql.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);

                    Mi_Sql.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Estatus + " = 'ACTIVO'");

                    Mi_Sql.Append(" ORDER BY " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " ASC ");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las fuentes de financiamiento. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Programas
            ///DESCRIPCIÓN          : consulta para obtener los datos de los programas
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 27/Marzo/2013 04:37 PM
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Programas()
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ", ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Campo_Clave + " + ' - ' + ");
                    Mi_Sql.Append(Cat_Sap_Proyectos_Programas.Campo_Descripcion + " AS NOMBRE ");
                    Mi_Sql.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);

                    Mi_Sql.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Estatus + " = 'ACTIVO'");

                    Mi_Sql.Append(" ORDER BY " + Cat_Sap_Proyectos_Programas.Campo_Clave + " ASC ");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los programas. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Parametros_Presupuestales
            ///DESCRIPCIÓN          : consulta para modificar los datos de las clases
            ///PARAMETROS           1 Negocio: conexion con la capa de negocios
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 27/Marzo/2013 04:27 PM
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static Boolean Modificar_Parametros_Presupuestales(Cls_Cat_Psp_Parametros_Presupuestales_Negocio Negocio)
            {
                StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
                Boolean Operacion_Completa = false;//Estado de la operacion.
                DataTable Dt_Param = new DataTable(); 

                try
                {
                    //verificamos si existen los datos de los paramentros
                    Dt_Param = Consultar_Parametros_Presupuestales();

                    if (Dt_Param != null)
                    {
                        if (Dt_Param.Rows.Count > 0)
                        {
                            Mi_SQL.Append("UPDATE " + Cat_Psp_Parametros_Presupuestales.Tabla_Cat_Psp_Parametro_Presupuestal);
                            Mi_SQL.Append(" SET ");

                            if (!String.IsNullOrEmpty(Negocio.P_Clasificacion_Administrativa))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Clasificador_Adm_ID + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Clasificacion_Administrativa.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Clasificador_Adm_ID + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID_Ingresos))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Fte_Financiamiento_ID_Ingresos + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Fte_Financiamiento_ID_Ingresos.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Fte_Financiamiento_ID_Ingresos + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Programa_ID_Ingresos))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Programa_ID_Ingresos + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Programa_ID_Ingresos.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Programa_ID_Ingresos + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID_Inversiones))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Fte_Financiamiento_ID_Inversiones + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Fte_Financiamiento_ID_Inversiones.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Fte_Financiamiento_ID_Inversiones + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Programa_ID_Inversiones))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Programa_ID_Inversiones + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Programa_ID_Inversiones.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Programa_ID_Inversiones + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Aplicar_Disponible_Acumulado.Trim()))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Aplicar_Disponible_Acumulado + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Aplicar_Disponible_Acumulado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Aplicar_Disponible_Acumulado + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Ing_Estimado))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Estimado + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Ing_Estimado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Estimado + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Ing_Ampliacion))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Ampliacion + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Ing_Ampliacion.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Ampliacion + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Ing_Reduccion))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Reduccion + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Ing_Reduccion.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Reduccion + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Ing_Modificado))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Modificado + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Ing_Modificado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Modificado + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Ing_Por_Recaudar))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Por_Recaudar + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Ing_Por_Recaudar.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Por_Recaudar + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Ing_Devengado))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Devengado + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Ing_Devengado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Devengado + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Ing_Recaudado))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Recaudado + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Ing_Recaudado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Recaudado + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Aprobado))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Aprobado + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Aprobado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Aprobado + " = NULL, ");
                            }



                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Ampliacion))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ampliacion + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Ampliacion.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ampliacion + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Reduccion))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Reduccion + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Reduccion.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Reduccion + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Ampliacion_Interna))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ampliacion_Interna + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Ampliacion_Interna.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ampliacion_Interna + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Reduccion_Interna))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Reduccion_Interna + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Reduccion_Interna.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Reduccion_Interna + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Modificado))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Modificado + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Modificado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Modificado + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Por_Ejercer))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Por_Ejercer + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Por_Ejercer.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Por_Ejercer + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Comprometido))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Comprometido + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Comprometido.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Comprometido + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Devengado))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Devengado + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Devengado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Devengado + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Ejercido))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ejercido + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Ejercido.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ejercido + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Pagado))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Pagado + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Pagado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Pagado + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Tipo_Poliza_ID_Psp_Egr))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Egr + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Tipo_Poliza_ID_Psp_Egr.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Egr + " = NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Tipo_Poliza_ID_Psp_Ing))
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Ing + " = ");
                                Mi_SQL.Append("'" + Negocio.P_Tipo_Poliza_ID_Psp_Ing.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Ing + " = NULL, ");
                            }

                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Usuario_Modifico + " = ");
                            Mi_SQL.Append("'" + Negocio.P_Usuario_Modifico.Trim() + "', ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Fecha_Modifico + " = GETDATE()");

                        }
                        else 
                        {
                            Mi_SQL.Append(" INSERT INTO " + Cat_Psp_Parametros_Presupuestales.Tabla_Cat_Psp_Parametro_Presupuestal + "(");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Fte_Financiamiento_ID_Ingresos + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Programa_ID_Ingresos + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Fte_Financiamiento_ID_Inversiones + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Programa_ID_Inversiones + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Aplicar_Disponible_Acumulado + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Estimado + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Ampliacion + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Reduccion + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Modificado + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Por_Recaudar + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Devengado + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Recaudado + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Aprobado + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ampliacion + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Reduccion + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ampliacion_Interna + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Reduccion_Interna + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Modificado + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Por_Ejercer + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Comprometido + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Devengado + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ejercido + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Pagado + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Ing + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Egr + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Clasificador_Adm_ID + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Usuario_Creo + ", ");
                            Mi_SQL.Append(Cat_Psp_Parametros_Presupuestales.Campo_Fecha_Creo + ")VALUES(");

                            if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID_Ingresos))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Fte_Financiamiento_ID_Ingresos.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Programa_ID_Ingresos))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Programa_ID_Ingresos.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Fte_Financiamiento_ID_Inversiones))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Fte_Financiamiento_ID_Inversiones.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Programa_ID_Inversiones))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Programa_ID_Inversiones.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Aplicar_Disponible_Acumulado))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Aplicar_Disponible_Acumulado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Ing_Estimado))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Ing_Estimado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Ing_Ampliacion))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Ing_Ampliacion.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Ing_Reduccion))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Ing_Reduccion.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Ing_Modificado))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Ing_Modificado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Ing_Por_Recaudar))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Ing_Por_Recaudar.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Ing_Devengado))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Ing_Devengado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Ing_Recaudado))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Ing_Recaudado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Aprobado))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Aprobado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Ampliacion))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Ampliacion.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Reduccion))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Reduccion.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Ampliacion_Interna))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Ampliacion_Interna.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Reduccion_Interna))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Reduccion_Interna.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Modificado))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Modificado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Por_Ejercer))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Por_Ejercer.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Comprometido))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Comprometido.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Devengado))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Devengado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Ejercido))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Ejercido.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Cta_Orden_Egr_Pagado))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Cta_Orden_Egr_Pagado.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Tipo_Poliza_ID_Psp_Ing))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Tipo_Poliza_ID_Psp_Ing.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Tipo_Poliza_ID_Psp_Egr))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Tipo_Poliza_ID_Psp_Egr.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            if (!String.IsNullOrEmpty(Negocio.P_Clasificacion_Administrativa))
                            {
                                Mi_SQL.Append("'" + Negocio.P_Clasificacion_Administrativa.Trim() + "', ");
                            }
                            else
                            {
                                Mi_SQL.Append("NULL, ");
                            }

                            Mi_SQL.Append("'" + Negocio.P_Usuario_Modifico.Trim() + "', ");
                            Mi_SQL.Append("GETDATE())");
                        }
                    }

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                    Operacion_Completa = true;
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al ejecutar los la modificación de los parametros. Error: [" + Ex.Message + "]");
                }
                return Operacion_Completa;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Tipo_Poliza
            ///DESCRIPCIÓN          : consulta para obtener los datos de los tipos de polizas
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 02/Abril/2013 10:00 AM
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Tipo_Poliza()
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID + ", ");
                    Mi_Sql.Append(Cat_Con_Tipo_Polizas.Campo_Abreviatura + " + ' - ' + ");
                    Mi_Sql.Append(Cat_Con_Tipo_Polizas.Campo_Descripcion + " AS NOMBRE ");
                    Mi_Sql.Append(" FROM " + Cat_Con_Tipo_Polizas.Tabla_Cat_Con_Tipo_Polizas);

                    Mi_Sql.Append(" ORDER BY " + Cat_Con_Tipo_Polizas.Campo_Abreviatura + " ASC ");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de los tipos de polizas. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Clasificacion_Administrativa
            ///DESCRIPCIÓN          : consulta para obtener los datos de las clasificaciones administrativas
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 23/Mayo/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            internal static DataTable Consultar_Clasificacion_Administrativa()
            {
                StringBuilder Mi_Sql = new StringBuilder();
                try
                {
                    Mi_Sql.Append("SELECT " + Cat_Psp_Clasificador_Administrativo.Campo_CADM_ID  + ", ");
                    Mi_Sql.Append(Cat_Psp_Clasificador_Administrativo.Campo_Clave + " + ' - ' + ");
                    Mi_Sql.Append(Cat_Psp_Clasificador_Administrativo.Campo_Nombre + " AS NOMBRE ");
                    Mi_Sql.Append(" FROM " + Cat_Psp_Clasificador_Administrativo.Tabla_Cat_Psp_Clasificador_Administrativo);

                    Mi_Sql.Append(" ORDER BY NOMBRE ASC ");

                    return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al intentar consultar los registros de las clasificaciones administrativas. Error: [" + Ex.Message + "]");
                }
            }
        #endregion
    }
}