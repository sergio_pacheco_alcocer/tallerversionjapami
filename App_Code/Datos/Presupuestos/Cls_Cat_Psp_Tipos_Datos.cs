﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using System.Text;
using JAPAMI.Cat_Psp_Rubros.Datos;
using JAPAMI.Cat_Psp_Tipos.Negocio;

namespace JAPAMI.Cat_Psp_Tipos.Datos
{
    public class Cls_Cat_Psp_Tipos_Datos
    {
        #region Metodos
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_tipos
        ///DESCRIPCIÓN          : consulta para obtener los datos de los tipos
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Tipos(Cls_Cat_Psp_Tipos_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT " + Cat_Psp_Tipo.Campo_Clave + " + ' ' + ");
                Mi_Sql.Append(Cat_Psp_Tipo.Campo_Descripcion + " AS CLAVE_NOMBRE, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Campo_Clave + ", ");
                Mi_Sql.Append(Cat_Psp_Tipo.Campo_Descripcion + ", ");
                Mi_Sql.Append(Cat_Psp_Tipo.Campo_Estatus + ", ");
                Mi_Sql.Append(Cat_Psp_Tipo.Campo_Anio + ", ");
                Mi_Sql.Append(Cat_Psp_Tipo.Campo_Rubro_ID + ", ");
                Mi_Sql.Append(Cat_Psp_Tipo.Campo_Tipo_ID);
                Mi_Sql.Append(" FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);

                if (!String.IsNullOrEmpty(Negocio.P_Clave))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Campo_Clave + " = '" + Negocio.P_Clave.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Campo_Clave + " = '" + Negocio.P_Clave.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Descripcion))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Campo_Descripcion + " = '" + Negocio.P_Descripcion.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Campo_Descripcion + " = '" + Negocio.P_Descripcion.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Estatus))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Campo_Estatus + " = '" + Negocio.P_Estatus.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Campo_Estatus + " = '" + Negocio.P_Estatus.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Anio))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Campo_Anio + " = '" + Negocio.P_Anio.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Campo_Anio + " = '" + Negocio.P_Anio.Trim() + "'");
                    }
                }

                Mi_Sql.Append(" ORDER BY " + Cat_Psp_Tipo.Campo_Clave + " ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los tipos. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos_Rubros
        ///DESCRIPCIÓN          : consulta para obtener los datos de los tipos y los rubros
        ///PARAMETROS           1 Negocio:. conexion con la capa de negocios 
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static DataTable Consultar_Tipos_Rubros(Cls_Cat_Psp_Tipos_Negocio Negocio)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            try
            {
                //OBTENEMOS LAS DEPENDENCIAS DEL CATALOGO
                Mi_Sql.Append("SELECT " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " + ' ' + ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS CLAVE_NOMBRE_TIPO, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + " AS CLAVE_TIPOS, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + " AS DESCRIPCION_TIPO, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Estatus + " AS ESTATUS_TIPO, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Anio + " AS ANIO_TIPO, ");
                Mi_Sql.Append(Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + ", ");
                Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " + ' ' + ");
                Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS CLAVE_NOMBRE_RUBRO, ");
                Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Clave + " AS CLAVE_RUBRO, ");
                Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Descripcion + " AS DESCRIPCION_RUBRO, ");
                Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Estatus + " AS ESTATUS_RUBRO, ");
                Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Anio + " AS ANIO_RUBRO, ");
                Mi_Sql.Append(Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);
                Mi_Sql.Append(" FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                Mi_Sql.Append(" INNER JOIN " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                Mi_Sql.Append(" ON " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Rubro_ID);
                Mi_Sql.Append(" = " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro + "." + Cat_Psp_Rubro.Campo_Rubro_ID);

                if (!String.IsNullOrEmpty(Negocio.P_Rubro_ID))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Rubro_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Rubro_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Rubro_ID.Trim() + "'");
                    }
                }

                if (!String.IsNullOrEmpty(Negocio.P_Tipo_ID))
                {
                    if (Mi_Sql.ToString().Trim().Contains("WHERE"))
                    {
                        Mi_Sql.Append(" AND " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    }
                    else
                    {
                        Mi_Sql.Append(" WHERE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID);
                        Mi_Sql.Append(" = '" + Negocio.P_Tipo_ID.Trim() + "'");
                    }
                }

                Mi_Sql.Append(" ORDER BY CLAVE_NOMBRE_TIPO ASC");

                return SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al intentar consultar los registros de los tipos. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Tipos2
        ///DESCRIPCIÓN          : Obtiene los Tipos de acuerdo a los filtros establecidos en la interfaz
        ///PARAMETROS           : Tipos, instancia de Cls_Cat_Psp_Tipos_Negocio 
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 21/Mayo/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public static DataTable Consultar_Tipos2(Cls_Cat_Psp_Tipos_Negocio Tipos)
        {
            DataTable Tabla = new DataTable();
            String Mi_SQL;
            String Mi_SQL_Campos_Foraneos = "";
            try
            {
                if (Tipos.P_Campos_Dinamicos != null && Tipos.P_Campos_Dinamicos != "")
                {
                    Mi_SQL = "SELECT " + Mi_SQL_Campos_Foraneos + Tipos.P_Campos_Dinamicos;
                }
                else
                {
                    Mi_SQL = "SELECT ";
                    Mi_SQL += Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + ", ";
                    Mi_SQL += Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Rubro_ID + ", ";
                    Mi_SQL += Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Anio + ", ";
                    Mi_SQL += Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + ", ";
                    Mi_SQL += Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Descripcion + ", ";
                    Mi_SQL += Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Estatus + ", ";
                    if (Mi_SQL.EndsWith(", "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 2);
                    }
                }
                Mi_SQL += " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo;
                if (Tipos.P_Unir_Tablas != null && Tipos.P_Unir_Tablas != "")
                {
                    Mi_SQL += ", " + Tipos.P_Unir_Tablas;
                }
                else
                {
                    if (Tipos.P_Join != null && Tipos.P_Join != "")
                    {
                        Mi_SQL += " " + Tipos.P_Join;
                    }
                }
                if (Tipos.P_Filtros_Dinamicos != null && Tipos.P_Filtros_Dinamicos != "")
                {
                    Mi_SQL += " WHERE " + Tipos.P_Filtros_Dinamicos;
                }
                else
                {
                    Mi_SQL += " WHERE ";
                    if (Tipos.P_Tipo_ID != null && Tipos.P_Tipo_ID != "")
                    {
                        Mi_SQL += Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Tipos.P_Tipo_ID + "' AND ";
                    }
                    if (Tipos.P_Rubro_ID != null && Tipos.P_Rubro_ID != "")
                    {
                        Mi_SQL += Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Rubro_ID + " = '" + Tipos.P_Rubro_ID + "' AND ";
                    }
                    if (Tipos.P_Anio != null && Tipos.P_Anio != "")
                    {
                        Mi_SQL += Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Anio + " = " + Tipos.P_Anio + " AND ";
                    }
                    if (Tipos.P_Clave != null && Tipos.P_Clave != "")
                    {
                        Mi_SQL += Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Clave + Validar_Operador_Comparacion(Tipos.P_Clave) + " AND ";
                    }
                    if (Tipos.P_Estatus != null && Tipos.P_Estatus != "")
                    {
                        Mi_SQL += Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "." + Cat_Psp_Tipo.Campo_Estatus + Validar_Operador_Comparacion(Tipos.P_Estatus) + " AND ";
                    }
                    if (Mi_SQL.EndsWith(" AND "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 5);
                    }
                    if (Mi_SQL.EndsWith(" WHERE "))
                    {
                        Mi_SQL = Mi_SQL.Substring(0, Mi_SQL.Length - 7);
                    }
                }
                if (Tipos.P_Agrupar_Dinamico != null && Tipos.P_Agrupar_Dinamico != "")
                {
                    Mi_SQL += " GROUP BY " + Tipos.P_Agrupar_Dinamico;
                }
                if (Tipos.P_Ordenar_Dinamico != null && Tipos.P_Ordenar_Dinamico != "")
                {
                    Mi_SQL += " ORDER BY " + Tipos.P_Ordenar_Dinamico;
                }
                DataSet dataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (dataSet != null)
                {
                    Tabla = dataSet.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                String Mensaje = "Error al intentar consultar los registros de la Cuentas. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
                throw new Exception(Mensaje);
            }
            return Tabla;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Eliminar_Tipos
        ///DESCRIPCIÓN          : consulta para eliminar los datos de los tipos
        ///PARAMETROS           1 Negocio: conexion con la capa de negocios
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static Boolean Eliminar_Tipo(Cls_Cat_Psp_Tipos_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
            Boolean Operacion_Completa = false;//Estado de la operacion.

            try
            {
                Mi_SQL.Append("UPDATE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                Mi_SQL.Append(" SET " + Cat_Psp_Tipo.Campo_Estatus + " = 'INACTIVO'");
                Mi_SQL.Append(" WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID + "'");

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al ejecutar Eliminar los tipos. Error: [" + Ex.Message + "]");
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Modificar_Tipo
        ///DESCRIPCIÓN          : consulta para modificar los datos de los Tipo
        ///PARAMETROS           1 Negocio: conexion con la capa de negocios
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static Boolean Modificar_Tipo(Cls_Cat_Psp_Tipos_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
            Boolean Operacion_Completa = false;//Estado de la operacion.

            try
            {
                Mi_SQL.Append("UPDATE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                Mi_SQL.Append(" SET " + Cat_Psp_Tipo.Campo_Clave + " = '" + Negocio.P_Clave + "', ");
                Mi_SQL.Append(Cat_Psp_Tipo.Campo_Descripcion + " = '" + Negocio.P_Descripcion + "', ");
                Mi_SQL.Append(Cat_Psp_Tipo.Campo_Estatus + " = '" + Negocio.P_Estatus + "', ");
                Mi_SQL.Append(Cat_Psp_Tipo.Campo_Anio + " = " + Negocio.P_Anio + ", ");
                Mi_SQL.Append(Cat_Psp_Tipo.Campo_Rubro_ID + " = '" + Negocio.P_Rubro_ID + "', ");
                Mi_SQL.Append(Cat_Psp_Tipo.Campo_Usuario_Modifico + " = '" + Negocio.P_Usuario_Modifico + "', ");
                Mi_SQL.Append(Cat_Psp_Tipo.Campo_Fecha_Modifico + " = GETDATE()");
                Mi_SQL.Append(" WHERE " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Negocio.P_Tipo_ID + "' ");

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al ejecutar los la modificación de los tipos. Error: [" + Ex.Message + "]");
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Alta_Tipos
        ///DESCRIPCIÓN          : consulta para guardar los datos de los tipos
        ///PARAMETROS           1 Negocio: conexion con la capa de negocios
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 15/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        internal static Boolean Alta_Tipos(Cls_Cat_Psp_Tipos_Negocio Negocio)
        {
            StringBuilder Mi_SQL = new StringBuilder();//Variable que almacenará la consulta.
            Boolean Operacion_Completa = false;//Estado de la operacion.
            String Id = Cls_Cat_Psp_Rubros_Datos.Consecutivo_ID(Cat_Psp_Tipo.Campo_Tipo_ID, Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo, "5");

            try
            {
                Mi_SQL.Append("INSERT INTO " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo + "(");
                Mi_SQL.Append(Cat_Psp_Tipo.Campo_Tipo_ID + ", ");
                Mi_SQL.Append(Cat_Psp_Tipo.Campo_Rubro_ID + ", ");
                Mi_SQL.Append(Cat_Psp_Tipo.Campo_Clave + ", ");
                Mi_SQL.Append(Cat_Psp_Tipo.Campo_Anio + ", ");
                Mi_SQL.Append(Cat_Psp_Tipo.Campo_Descripcion + ", ");
                Mi_SQL.Append(Cat_Psp_Tipo.Campo_Estatus + ", ");
                Mi_SQL.Append(Cat_Psp_Tipo.Campo_Usuario_Creo + ", ");
                Mi_SQL.Append(Cat_Psp_Tipo.Campo_Fecha_Creo + ") VALUES( ");
                Mi_SQL.Append("'" + Id + "', ");
                Mi_SQL.Append("'" + Negocio.P_Rubro_ID + "', ");
                Mi_SQL.Append("'" + Negocio.P_Clave + "', ");
                Mi_SQL.Append("" + Negocio.P_Anio + ", ");
                Mi_SQL.Append("'" + Negocio.P_Descripcion + "', ");
                Mi_SQL.Append("'" + Negocio.P_Estatus + "', ");
                Mi_SQL.Append("'" + Negocio.P_Usuario_Creo + "', ");
                Mi_SQL.Append("GETDATE()) ");

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                Operacion_Completa = true;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al ejecutar el alta de los tipos. Error: [" + Ex.Message + "]");
            }
            return Operacion_Completa;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Operador_Comparacion
        ///DESCRIPCIÓN          : Devuelve una cadena adecuada al operador indicado en la capa de Negocios
        ///PARAMETROS           : 
        ///CREO                 : Antonio Salvador Benavides Guardado
        ///FECHA_CREO           : 20/Agosto/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private static String Validar_Operador_Comparacion(String Filtro)
        {
            String Cadena_Validada;
            if (Filtro.Trim().StartsWith("<")
               || Filtro.Trim().StartsWith(">")
               || Filtro.Trim().StartsWith("<>")
               || Filtro.Trim().StartsWith("<=")
               || Filtro.Trim().StartsWith(">=")
               || Filtro.Trim().StartsWith("=")
               || Filtro.Trim().ToUpper().StartsWith("BETWEEN")
               || Filtro.Trim().ToUpper().StartsWith("LIKE")
               || Filtro.Trim().ToUpper().StartsWith("IN")
               || Filtro.Trim().ToUpper().StartsWith("NOT IN"))
            {
                Cadena_Validada = " " + Filtro + " ";
            }
            else
            {
                if (Filtro.Trim().ToUpper().StartsWith("NULL")
                    || Filtro.Trim().ToUpper().StartsWith("NOT NULL"))
                {
                    Cadena_Validada = " IS " + Filtro + " ";
                }
                else
                {
                    if (Filtro.Trim().ToUpper().StartsWith("(") && Filtro.Trim().ToUpper().EndsWith(")"))
                    {
                        Cadena_Validada = " = " + Filtro;
                    }
                    else
                    {
                        Cadena_Validada = " = '" + Filtro + "' ";
                    }
                }
            }
            return Cadena_Validada;
        }
        #endregion
    }
}
