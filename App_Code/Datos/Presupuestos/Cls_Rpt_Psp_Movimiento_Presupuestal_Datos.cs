﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;
using System.Data;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Reporte_Movimiento_Presupuestal.Negocio;


/// <summary>
/// Summary description for Cls_Rpt_Psp_Movimiento_Presupuestal_Datos
/// </summary>
namespace JAPAMI.Reporte_Movimiento_Presupuestal.Datos
{
    public class Cls_Rpt_Psp_Movimiento_Presupuestal_Datos
    {
        public Cls_Rpt_Psp_Movimiento_Presupuestal_Datos()
        {
        }

        #region (Metodos)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION:   Consulta_Reporte_Movimiento_Presupuestal
        /// DESCRIPCION :           Consulta para los movimientos presupuestales de acuerdo a diversos criterios
        /// PARAMETROS  :           Datos: Variable de la capa de negocio
        /// CREO        :           Noe Mosqueda Valadez
        /// FECHA_CREO  :           01/Junio/2012 12:40
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        public static DataTable Consulta_Reporte_Movimiento_Presupuestal(Cls_Rpt_Psp_Movimiento_Presupuestal_Negocio Datos)
        {
            //Declaracion de variables
            DataTable Dt_Resultado = new DataTable(); //Tabla para el resultado
            String Mi_SQL; //variable para la consulta
            Boolean Where_Utilizado = false; //variable que indica si la clausula where ya ha sido utilizada

            try
            {
                //Asignar consulta
                Mi_SQL = "SELECT Ope_Com_Solicitud_Transf.No_Solicitud,  Ope_Com_Solicitud_Transf.Importe, Ope_Com_Solicitud_Transf.Justificacion, " +
                    "Ope_Com_Solicitud_Transf.Justificacion_Solicitud, Ope_Com_Solicitud_Transf.P_Estatus, Ope_Com_Solicitud_Transf.Tipo_Operacion, " + 
                    "Ope_Com_Solicitud_Transf.Codigo1, (Cat_Sap_Area_Funcional.Clave +  ' ' + Cat_Sap_Area_Funcional.Descripcion) AS Area_Funcional_Origen, " + 
                    "(Cat_Sap_Proyectos_Programas.Clave + ' ' + Cat_Sap_Proyectos_Programas.Descripcion) AS Programa_Origen, " + 
                    "(Cat_Sap_Partidas_Especificas.Clave + ' ' + Cat_Sap_Partidas_Especificas.Descripcion) AS Partida_Origen, " + 
                    "(Cat_Dependencias.Clave + ' ' + Cat_Dependencias.Comentarios) AS Unidad_Responsable_Origen, " + 
                    "(Cat_Sap_Fte_Financiamiento.Clave + ' ' + Cat_Sap_Fte_Financiamiento.Descripcion) AS Financiamiento_Origen, " + 
                    "(Cat_Sap_Capitulo.Clave + ' ' + Cat_Sap_Capitulo.Descripcion) AS Capitulo_Origen, " +
                    "Ope_Com_Solicitud_Transf.Codigo2, (Cat_Sap_Area_Funcional_2.Clave +  ' ' + Cat_Sap_Area_Funcional_2.Descripcion) AS Area_Funcional_Destino, " + 
                    "(Cat_Sap_Proyectos_Programas_2.Clave + ' ' + Cat_Sap_Proyectos_Programas_2.Descripcion) AS Programa_Destino, " + 
                    "(Cat_Sap_Partidas_Especificas_2.Clave + ' ' + Cat_Sap_Partidas_Especificas_2.Descripcion) AS Partida_Destino, " + 
                    "(Cat_Dependencias_2.Clave + ' ' + Cat_Dependencias_2.Comentarios) AS Unidad_Responsable_Destino, " + 
                    "(Cat_Sap_Fte_Financiamiento_2.Clave + ' ' + Cat_Sap_Fte_Financiamiento_2.Descripcion) AS Financiamiento_Destino, " + 
                    "(Cat_Sap_Capitulo_2.Clave + ' ' + Cat_Sap_Capitulo_2.Descripcion) AS Capitulo_Destino " + 
                    "FROM Ope_Com_Solicitud_Transf " + 
                    "INNER JOIN Cat_Sap_Area_Funcional ON Ope_Com_Solicitud_Transf.Origen_Area_Funcional_ID = Cat_Sap_Area_Funcional.Area_Funcional_ID " + 
                    "INNER JOIN Cat_Sap_Area_Funcional Cat_Sap_Area_Funcional_2 ON Ope_Com_Solicitud_Transf.Destino_Area_Funcional_ID = Cat_Sap_Area_Funcional_2.Area_Funcional_ID " + 
                    "INNER JOIN Cat_Sap_Proyectos_Programas ON Ope_Com_Solicitud_Transf.Origen_Programa_ID = Cat_Sap_Proyectos_Programas.Proyecto_Programa_ID " + 
                    "INNER JOIN Cat_Sap_Proyectos_Programas Cat_Sap_Proyectos_Programas_2 ON Ope_Com_Solicitud_Transf.Destino_Programa_ID = Cat_Sap_Proyectos_Programas_2.Proyecto_Programa_ID " + 
                    "INNER JOIN Cat_Sap_Partidas_Especificas ON Ope_Com_Solicitud_Transf.Origen_Partida_ID = Cat_Sap_Partidas_Especificas.Partida_ID " + 
                    "INNER JOIN Cat_Sap_Partidas_Especificas Cat_Sap_Partidas_Especificas_2 ON Ope_Com_Solicitud_Transf.Destino_Partida_ID = Cat_Sap_Partidas_Especificas_2.Partida_ID " + 
                    "INNER JOIN Cat_Dependencias ON Ope_Com_Solicitud_Transf.Origen_Dependencia_ID = Cat_Dependencias.Dependencia_ID " + 
                    "INNER JOIN Cat_Dependencias Cat_Dependencias_2 ON Ope_Com_Solicitud_Transf.Destino_Dependencia_ID = Cat_Dependencias_2.Dependencia_ID " + 
                    "INNER JOIN Cat_Sap_Fte_Financiamiento ON Ope_Com_Solicitud_Transf.Origen_Fte_Financiamiento_ID = Cat_Sap_Fte_Financiamiento.Fuente_Financiamiento_ID " + 
                    "INNER JOIN Cat_Sap_Fte_Financiamiento Cat_Sap_Fte_Financiamiento_2 ON Ope_Com_Solicitud_Transf.Destino_Fte_Financiamiento_ID = Cat_Sap_Fte_Financiamiento_2.Fuente_Financiamiento_ID " +
                    "INNER JOIN Cat_Sap_Partida_Generica ON Cat_Sap_Partidas_Especificas.Partida_Generica_ID = Cat_Sap_Partida_Generica.Partida_Generica_ID " +
                    "INNER JOIN Cat_Sap_Partida_Generica Cat_Sap_Partida_Generica_2 ON Cat_Sap_Partidas_Especificas_2.Partida_Generica_ID = Cat_Sap_Partida_Generica_2.Partida_Generica_ID " +
                    "INNER JOIN Cat_Sap_Concepto ON Cat_Sap_Partida_Generica.Concepto_ID = Cat_Sap_Concepto.Concepto_ID " +
                    "INNER JOIN Cat_Sap_Concepto Cat_Sap_Concepto_2 ON Cat_Sap_Partida_Generica_2.Concepto_ID = Cat_Sap_Concepto_2.Concepto_ID " +
                    "INNER JOIN Cat_Sap_Capitulo ON Cat_Sap_Concepto.Capitulo_ID = Cat_Sap_Capitulo.Capitulo_ID " +
                    "INNER JOIN Cat_Sap_Capitulo Cat_Sap_Capitulo_2 ON Cat_Sap_Concepto_2.Capitulo_ID = Cat_Sap_Capitulo_2.Capitulo_ID ";

                //Filtros
                //Tipo de Operacion
                if (String.IsNullOrEmpty(Datos.P_Tipo_Operacion) == false)
                {
                    Mi_SQL += "Ope_Com_Solicitud_Transf.Tipo_Operacion = '" + Datos.P_Tipo_Operacion + "' ";

                    Where_Utilizado = true;
                }

                //Unidad responsable
                if (String.IsNullOrEmpty(Datos.P_Unidad_Responsable_ID_Origen) == false)
                {
                    //Verificar si la clausula where ya ha sido utilizada
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    Mi_SQL += "Ope_Com_Solicitud_Transf.Origen_Dependencia_ID = '" + Datos.P_Unidad_Responsable_ID_Origen + "' ";
                }

                if (String.IsNullOrEmpty(Datos.P_Unidad_Responsable_ID_Destino) == false)
                {
                    //Verificar si la clausula where ya ha sido utilizada
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    Mi_SQL += "Ope_Com_Solicitud_Transf.Destino_Dependencia_ID = '" + Datos.P_Unidad_Responsable_ID_Destino + "' ";
                }

                //Fuente de financiamiento
                if (String.IsNullOrEmpty(Datos.P_Fuente_Financiamiento_ID_Origen) == false)
                {
                    //Verificar si la clausula where ya ha sido utilizada
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    Mi_SQL += "Ope_Com_Solicitud_Transf.Origen_Fte_Financiamiento_ID = '" + Datos.P_Fuente_Financiamiento_ID_Origen + "' ";
                }

                if (String.IsNullOrEmpty(Datos.P_Fuente_Financiamiento_ID_Destino) == false)
                {
                    //Verificar si la clausula where ya ha sido utilizada
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    Mi_SQL += "Ope_Com_Solicitud_Transf.Destino_Fte_Financiamiento_ID = '" + Datos.P_Fuente_Financiamiento_ID_Destino + "' ";
                }

                //Programa
                if (String.IsNullOrEmpty(Datos.P_Programa_ID_Origen) == false)
                {
                                        //Verificar si la clausula where ya ha sido utilizada
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    Mi_SQL += "Ope_Com_Solicitud_Transf.Origen_Programa_ID = '" + Datos.P_Programa_ID_Origen + "' ";
                }

                if (String.IsNullOrEmpty(Datos.P_Programa_ID_Destino) == false)
                {
                    //Verificar si la clausula where ya ha sido utilizada
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    Mi_SQL += "Ope_Com_Solicitud_Transf.Destino_Programa_ID = '" + Datos.P_Programa_ID_Destino + "' ";
                }

                //Capitulo
                if (String.IsNullOrEmpty(Datos.P_Capitulo_ID_Origen) == false)
                {
                    //Verificar si la clausula where ya ha sido utilizada
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    Mi_SQL += "Cat_Sap_Capitulo.Capitulo_ID = '" + Datos.P_Capitulo_ID_Origen + "' ";
                }

                if (String.IsNullOrEmpty(Datos.P_Capitulo_ID_Destino) == false)
                {
                    //Verificar si la clausula where ya ha sido utilizada
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    Mi_SQL += "Cat_Sap_Capitulo_2.Capitulo_ID = '" + Datos.P_Capitulo_ID_Destino + "' ";
                }

                //Partida
                if (String.IsNullOrEmpty(Datos.P_Partida_ID_Origen) == false)
                {
                    //Verificar si la clausula where ya ha sido utilizada
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    Mi_SQL += "Ope_Com_Solicitud_Transf.Origen_Partida_ID = '" + Datos.P_Partida_ID_Origen + "' ";
                }

                if (String.IsNullOrEmpty(Datos.P_Partida_ID_Destino) == false)
                {
                    //Verificar si la clausula where ya ha sido utilizada
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    Mi_SQL += "Ope_Com_Solicitud_Transf.Destino_Partida_ID = '" + Datos.P_Partida_ID_Destino + "' ";
                }

                //Fechas
                if (Datos.P_Fecha_Inicial.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    //Verificar si la clausula where ya ha sido utilizada
                    if (Where_Utilizado == true)
                    {
                        Mi_SQL += "AND ";
                    }
                    else
                    {
                        Mi_SQL += "WHERE ";
                        Where_Utilizado = true;
                    }

                    Mi_SQL += "Ope_Com_Solicitud_Transf.Fecha_Creo >= '" + Datos.P_Fecha_Inicial.ToString("dd/MM/yyyy") + " 00:00:00' ";

                    if (Datos.P_Fecha_Final.ToString("dd/MM/yyyy") != "01/01/0001")
                    {
                        Mi_SQL += "AND Ope_Com_Solicitud_Transf.Fecha_Creo <= '" + Datos.P_Fecha_Inicial.ToString("dd/MM/yyyy") + " 23:59:59' ";
                    }
                }

                //Ejecutar la consulta
                Dt_Resultado = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];

                //Entregar resultado
                return Dt_Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
        #endregion
    }
}